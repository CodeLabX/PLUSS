﻿using System;

namespace DAL
{
    public class Audit
    {

        public int AuditId { get; set; }
        public string UserId { get; set; }
        public DateTime ActionDateAndTime { get; set; }
        public string RequestedUrl { get; set; }
        public string IpAddress { get; set; }
        public string HostName { get; set; }
        public string Activity { get; set; }

        public string Description { get; set; }
        public string UserRole { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
    }
}