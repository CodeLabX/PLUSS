﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{

    public class BankInformationGateway
    {
        private DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;

        public BankInformationGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public Int32 GetBankId(string bankName)
        {
            Int32 bankId = 0;
            try
            {
                string queryString = String.Format("SELECT BANK_ID, REPLACE(UPPER(TRIM(BANK_NM)), ' ', '') AS BANK_NM  FROM t_pluss_bank WHERE REPLACE(UPPER(TRIM(BANK_NM)), ' ', '') = REPLACE(UPPER(TRIM('{0}')),' ', '') ", bankName);
                bankId = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(queryString));
            }
            catch
            {
                bankId = 0;
            }
            return bankId;
        }
        public List<BankInformation> GetAllBankInfo()
        {
            List<BankInformation> bankInformationListObj = new List<BankInformation>();
            string queryString = "SELECT BANK_ID, BANK_CODE, BANK_NM, BANK_STATUS, (CASE BANK_STATUS WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' END) BANK_STATUS_MSG FROM t_pluss_bank ORDER BY BANK_NM ASC ";
            //ADODB.Recordset xRs = new ADODB.Recordset();
            //xRs = dbMySQL.DbOpenRset(queryString);
            var data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {

                    BankInformation bankInformationObj = new BankInformation();
                    bankInformationObj.Id = Convert.ToInt32(xRs["BANK_ID"]);
                    bankInformationObj.Code = Convert.ToString(xRs["BANK_CODE"]);
                    bankInformationObj.Name = Convert.ToString(xRs["BANK_NM"]);
                    bankInformationObj.Status = Convert.ToInt32(xRs["BANK_STATUS"]);
                    bankInformationObj.StatusMsg = Convert.ToString(xRs["BANK_STATUS_MSG"]);
                    bankInformationListObj.Add(bankInformationObj);
                }
                xRs.Close();
            }
           
            return bankInformationListObj;
        }

        public int InsertORUpdateBankInfo(BankInformation bankInformationObj)
        {
            int insertORUpdateValue = 0;
            if (!IsExistsBankInfo(Convert.ToInt32(bankInformationObj.Id)))
            {
                insertORUpdateValue = InsertBankInformation(bankInformationObj);
            }
            else
            {
                insertORUpdateValue = UpdateBankInformation(bankInformationObj);
            }
            return insertORUpdateValue;
        }

        public int InsertBankInformation(BankInformation bankInformationObj)
        {
            string queryString = String.Format("INSERT INTO t_pluss_bank (BANK_NM, BANK_STATUS) " +
                                                "VALUES( '{0}','{1}') ",
                                                EscapeCharacter(bankInformationObj.Name), bankInformationObj.Status);

            try
            {
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch
            {
                return ERROR;
            }
        }

        private int UpdateBankInformation(BankInformation bankInformationObj)
        {
            string queryString = String.Format("UPDATE t_pluss_bank SET " +
                                                "BANK_NM = '{0}', BANK_STATUS = {1} " +
                                                "WHERE BANK_ID = {2} ",
                                                EscapeCharacter(bankInformationObj.Name),
                                                bankInformationObj.Status, bankInformationObj.Id);
            try
            {
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch
            {
                return ERROR;
            }
        }

        private bool IsExistsBankInfo(Int32 bankId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*)as Count FROM t_pluss_bank WHERE BANK_ID = {0}", bankId);
            try
            {
                var banks=DbQueryManager.GetTable(queryString);

                if (Convert.ToInt32(banks.Rows[0][0] )> 0)
                {
                    returnValue = true;
                }
                else
                {
                    returnValue = false;
                }
            }
            catch
            {
                returnValue = false;
            }
            return returnValue;
        }

        public bool IsExistsBankCode(string bankCode, int bankId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_bank WHERE UPPER(REPLACE(TRIM(BANK_CODE),' ', '')) = UPPER(REPLACE(TRIM('{0}'),' ','')) AND BANK_ID != {1}", EscapeCharacter(bankCode), bankId);
            try
            {
                Int32 brankCount = Convert.ToInt32("0" + dbMySQL.DbGetValue(queryString));
                if (brankCount > 0)
                {
                    returnValue = true;
                }
                else
                {
                    returnValue = false;
                }
            }
            catch
            {
                returnValue = false;
            }
            return returnValue;
        }

        public bool IsExistsBankName(string bankName, int bankId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_bank WHERE UPPER(REPLACE(TRIM(BANK_NM),' ', '')) = UPPER(REPLACE(TRIM('{0}'),' ','')) AND BANK_ID != {1}", EscapeCharacter(bankName), bankId);
            try
            {
                Int32 brankCount = Convert.ToInt32("0" + DbQueryManager.GetTable(queryString).Rows.Count);
                if (brankCount > 0)
                {
                    returnValue = true;
                }
                else
                {
                    returnValue = false;
                }
            }
            catch
            {
                returnValue = false;
            }
            return returnValue;
        }

        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                returnMessage = fieldString;
            }
            return returnMessage;
        }
    }
}
