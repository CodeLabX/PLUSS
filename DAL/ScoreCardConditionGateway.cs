﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{
    public class ScoreCardConditionGateway
    {
        /// <summary>
        /// Score card condition gateway class.
        /// </summary>
        public ScoreCardConditionGateway()
        {
            //Constructor logic here.
        }
        #region GetScoreCardCondition(string searchKey)
        /// <summary>
        /// This method populate all score card condition information.
        /// </summary>   
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns a score cared condition list object.</returns>
        public List<ScoreCardCondition> GetScoreCardCondition(string searchKey)
        {
            List<ScoreCardCondition> scoreCardConditionListObj = new List<ScoreCardCondition>();

            string queryString = "SELECT SCCA_ID, SCCA_VARIABLENAME_ID, VN.VARI_NAME, SCCA_COMPOP, SCCA_COMPVALUE, SCCA_LOGICOP, " +
                                 "SCCA_SCOREVALUE, SCCA_CUTOFFSCORE, SCCA_INTERCEPTVALUE, SCCA_SCORNEGATIVEVALUE,SCCA_STATUS  " +
                                 "FROM t_pluss_scorecard AS SC " +
                                 "INNER JOIN t_pluss_variablename AS VN ON SC.SCCA_VARIABLENAME_ID = VN.VARI_ID " +
                                 "WHERE UPPER(SCCA_ID) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(SCCA_VARIABLENAME_ID) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                 "UPPER(SCCA_COMPOP) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(SCCA_COMPVALUE) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                 "UPPER(SCCA_LOGICOP) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(SCCA_SCOREVALUE) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                 "UPPER(SCCA_CUTOFFSCORE) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(SCCA_INTERCEPTVALUE) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                 "UPPER(VN.VARI_NAME) LIKE(UPPER('%" + searchKey + "%')) OR UPPER(SCCA_SCORNEGATIVEVALUE) LIKE(UPPER('%" + searchKey + "%')) ORDER BY VN.VARI_NAME ASC";
            DataTable scoreCardConditionDataTableObj = new DataTable();
            try
            {
                scoreCardConditionDataTableObj = DbQueryManager.GetTable(queryString);


                if (scoreCardConditionDataTableObj != null)
                {
                    DataTableReader scoreCardConditionDataTableReaderObj = scoreCardConditionDataTableObj.CreateDataReader();
                    if (scoreCardConditionDataTableReaderObj != null)
                    {
                        while (scoreCardConditionDataTableReaderObj.Read())
                        {
                            ScoreCardCondition scoreCardConditionObj = new ScoreCardCondition();
                            scoreCardConditionObj.Id = Convert.ToInt32(scoreCardConditionDataTableReaderObj["SCCA_ID"].ToString());
                            scoreCardConditionObj.VariableId = Convert.ToInt32(scoreCardConditionDataTableReaderObj["SCCA_VARIABLENAME_ID"].ToString());
                            scoreCardConditionObj.VariableName.Name = Convert.ToString(scoreCardConditionDataTableReaderObj["VARI_NAME"].ToString());
                            scoreCardConditionObj.CompareOperator = Convert.ToString(scoreCardConditionDataTableReaderObj["SCCA_COMPOP"].ToString());
                            scoreCardConditionObj.CompareValue = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_COMPVALUE"].ToString());
                            scoreCardConditionObj.LogicalOperator = Convert.ToString(scoreCardConditionDataTableReaderObj["SCCA_LOGICOP"].ToString());
                            scoreCardConditionObj.ScoreValue = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_SCOREVALUE"]);
                            scoreCardConditionObj.NegativeValue = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_SCORNEGATIVEVALUE"]);
                            scoreCardConditionObj.CuttOffScore = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_CUTOFFSCORE"].ToString());
                            scoreCardConditionObj.InterceptValue = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_INTERCEPTVALUE"].ToString());
                            scoreCardConditionObj.Status = Convert.ToString(scoreCardConditionDataTableReaderObj["SCCA_STATUS"].ToString());
                            scoreCardConditionListObj.Add(scoreCardConditionObj);
                        }
                        scoreCardConditionDataTableReaderObj.Close();
                    }
                    scoreCardConditionDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return scoreCardConditionListObj;
        }

        public List<ScoreCardCondition> GetScoreCardCondition()
        {
            List<ScoreCardCondition> scoreCardConditionListObj = new List<ScoreCardCondition>();

            string queryString = "SELECT SCCA_ID, SCCA_VARIABLENAME_ID, VN.VARI_NAME, SCCA_COMPOP, SCCA_COMPVALUE, SCCA_LOGICOP, " +
                                 "SCCA_SCOREVALUE, SCCA_CUTOFFSCORE, SCCA_INTERCEPTVALUE, SCCA_SCORNEGATIVEVALUE,SCCA_STATUS  " +
                                 "FROM t_pluss_scorecard AS SC " +
                                 "INNER JOIN t_pluss_variablename AS VN ON SC.SCCA_VARIABLENAME_ID = VN.VARI_ID " +
                                 "WHERE SCCA_STATUS !='Inactive' ORDER BY VN.VARI_NAME ASC";
            DataTable scoreCardConditionDataTableObj = new DataTable();
            try
            {
                scoreCardConditionDataTableObj = DbQueryManager.GetTable(queryString);


                if (scoreCardConditionDataTableObj != null)
                {
                    DataTableReader scoreCardConditionDataTableReaderObj = scoreCardConditionDataTableObj.CreateDataReader();
                    if (scoreCardConditionDataTableReaderObj != null)
                    {
                        while (scoreCardConditionDataTableReaderObj.Read())
                        {
                            ScoreCardCondition scoreCardConditionObj = new ScoreCardCondition();
                            scoreCardConditionObj.Id = Convert.ToInt32(scoreCardConditionDataTableReaderObj["SCCA_ID"].ToString());
                            scoreCardConditionObj.VariableId = Convert.ToInt32(scoreCardConditionDataTableReaderObj["SCCA_VARIABLENAME_ID"].ToString());
                            scoreCardConditionObj.VariableName.Name = Convert.ToString(scoreCardConditionDataTableReaderObj["VARI_NAME"].ToString());
                            scoreCardConditionObj.CompareOperator = Convert.ToString(scoreCardConditionDataTableReaderObj["SCCA_COMPOP"].ToString());
                            scoreCardConditionObj.CompareValue = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_COMPVALUE"].ToString());
                            scoreCardConditionObj.LogicalOperator = Convert.ToString(scoreCardConditionDataTableReaderObj["SCCA_LOGICOP"].ToString());
                            scoreCardConditionObj.ScoreValue = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_SCOREVALUE"]);
                            scoreCardConditionObj.NegativeValue = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_SCORNEGATIVEVALUE"]);
                            scoreCardConditionObj.CuttOffScore = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_CUTOFFSCORE"].ToString());
                            scoreCardConditionObj.InterceptValue = Convert.ToDouble(scoreCardConditionDataTableReaderObj["SCCA_INTERCEPTVALUE"].ToString());
                            scoreCardConditionObj.Status = Convert.ToString(scoreCardConditionDataTableReaderObj["SCCA_STATUS"].ToString());
                            scoreCardConditionListObj.Add(scoreCardConditionObj);
                        }
                        scoreCardConditionDataTableReaderObj.Close();
                    }
                    scoreCardConditionDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return scoreCardConditionListObj;
        }
        #endregion

        #region GetAllVariableName()
        /// <summary>
        /// This method populate all variable name.
        /// </summary>
        /// <returns>Returns a variable name list object.</returns>
        public List<VariableName> GetAllVariableName()
        {
            List<VariableName> variableNameListObj = new List<VariableName>();
            string queryString = "SELECT VARI_ID, VARI_NAME FROM t_pluss_variablename ORDER BY VARI_NAME ASC ";
            DataTable variableNameDataTableObj = new DataTable();
            try
            {
                variableNameDataTableObj = DbQueryManager.GetTable(queryString);


                if (variableNameDataTableObj != null)
                {
                    DataTableReader variableNameDataTableReaderObj = variableNameDataTableObj.CreateDataReader();
                    if (variableNameDataTableReaderObj != null)
                    {
                        while (variableNameDataTableReaderObj.Read())
                        {
                            VariableName variableNameObj = new VariableName();
                            variableNameObj.Id = Convert.ToInt32(variableNameDataTableReaderObj["VARI_ID"].ToString());
                            variableNameObj.Name = Convert.ToString(variableNameDataTableReaderObj["VARI_NAME"].ToString());
                            variableNameListObj.Add(variableNameObj);
                        }
                        variableNameDataTableReaderObj.Close();
                    }
                    variableNameDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return variableNameListObj;
        }
        #endregion

        #region InsertORUpdateScoreCardConditionInfo(ScoreCardCondition scoreCardConditionObj)
        /// <summary>
        /// This method provide insert or update score card condition information.
        /// </summary>
        /// <param name="scoreCardConditionObj">Gets score card condition object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateScoreCardConditionInfo(ScoreCardCondition scoreCardConditionObj)
        {
            int insertORUpdateValue = -1;
            if (!IsExistsScoreCardCondition(Convert.ToInt32(scoreCardConditionObj.Id)))
            {
                insertORUpdateValue = InsertScoreCardConditionInformation(scoreCardConditionObj);
            }
            else
            {
                insertORUpdateValue = UpdateScoreCardConditionInformation(scoreCardConditionObj);
            }
            return insertORUpdateValue;
        }

        private int InsertScoreCardConditionInformation(ScoreCardCondition scoreCardConditionObj)
        {
            int insertRow = -1;
            string queryString = String.Format("INSERT INTO t_pluss_scorecard (SCCA_VARIABLENAME_ID, SCCA_COMPOP, SCCA_COMPVALUE, SCCA_LOGICOP, " +
                                                "SCCA_SCOREVALUE, SCCA_CUTOFFSCORE, SCCA_INTERCEPTVALUE, SCCA_SCORNEGATIVEVALUE,SCCA_STATUS) " +
                                                "VALUES ({0}, '{1}', {2}, '{3}', {4}, {5},{6},{7},'{8}')",
                                                scoreCardConditionObj.VariableId, EscapeCharacter(scoreCardConditionObj.CompareOperator), scoreCardConditionObj.CompareValue, 
                                                scoreCardConditionObj.LogicalOperator, scoreCardConditionObj.ScoreValue, scoreCardConditionObj.CuttOffScore,scoreCardConditionObj.InterceptValue, scoreCardConditionObj.NegativeValue,scoreCardConditionObj.Status);
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (insertRow > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }

        private int UpdateScoreCardConditionInformation(ScoreCardCondition scoreCardConditionObj)
        {
            int updateRow = -1;
            string queryString = String.Format("UPDATE t_pluss_scorecard SET " +
                                                "SCCA_VARIABLENAME_ID = {0} , SCCA_COMPOP = '{1}' , SCCA_COMPVALUE = {2} , " +
                                                "SCCA_LOGICOP = '{3}' , SCCA_SCOREVALUE = {4} , SCCA_CUTOFFSCORE = {5}, SCCA_INTERCEPTVALUE = {6}, SCCA_SCORNEGATIVEVALUE = {7},SCCA_STATUS='{8}' " +
                                                "WHERE SCCA_ID = {9} ",
                                                scoreCardConditionObj.VariableId, EscapeCharacter(scoreCardConditionObj.CompareOperator), scoreCardConditionObj.CompareValue,
                                                EscapeCharacter(scoreCardConditionObj.LogicalOperator), scoreCardConditionObj.ScoreValue, scoreCardConditionObj.CuttOffScore,
                                                scoreCardConditionObj.InterceptValue, scoreCardConditionObj.NegativeValue, scoreCardConditionObj.Status, scoreCardConditionObj.Id);
            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (updateRow > 0)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
        #endregion

        #region IsExistsScoreCardCondition(Int32 Id)
        /// <summary>
        /// This method provide score card condition exists or not.
        /// </summary>
        /// <param name="Id">Get a Id</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsScoreCardCondition(Int32 Id)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_scorecard WHERE SCCA_ID = {0}", Id);
            object scoreCardConditionObj;
            try
            {
                scoreCardConditionObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(scoreCardConditionObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region EscapeCharacter
        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                returnMessage = fieldString;
            }
            return returnMessage;
        }
        #endregion
    }
}
