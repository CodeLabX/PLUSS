﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using BusinessEntities;
using Bat.Common;
using System.Data;
using System.Globalization;
using AzUtilities;
using System.ComponentModel;
using System.Data.SqlClient;
using AzUtilities.Common;

namespace DAL
{
    public class DeDupInfoGateway
    {
        public List<DeDupInfo> deDupInfoObjList = null;
        public DeDupInfo deDupInfo = null;
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        public static long uploadDataCount = 0;


        public DeDupInfoGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public List<DeDupInfo> GetAllDeDupInfo(string nameList, string fatherName, string motherName, string businessEmployeeName, string dob, List<string> contact, List<string> id, List<string> ac, /*List<string> bankBranch,*/ string tin, int type)
        {
            string condition = "";
            string finalCondition = "";
            if (nameList.Length > 0)
            {
                int firstIndex = 0;
                string nameSecondPart = "";
                string[] splitName = nameList.Split(' ');
                if (splitName.Length > 0)
                {
                    foreach (string nameObj in splitName)
                    {
                        if (nameObj.Length > 2)
                        {
                            if (firstIndex == 0)
                            {
                                if (condition != "")
                                {
                                    condition +=
                                        " OR SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Upper(DI.NM),'Mohammad',''),'Mohamad',''),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                                        AddSlash(nameObj.ToUpper()) + "') or Upper(DI.NM) LIKE '%" +
                                        AddSlash(nameObj.ToUpper()) + "%'";
                                    firstIndex++;
                                }
                                else
                                {
                                    condition +=
                                        " SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Upper(DI.NM),'Mohammad',''),'Mohamad',''),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                                        AddSlash(nameObj.ToUpper()) + "') or Upper(DI.NM) LIKE '%" +
                                        AddSlash(nameObj.ToUpper()) + "%'";
                                    firstIndex++;
                                }

                            }
                            else
                            {
                                if (splitName.Length > 2)
                                {
                                    if (nameObj.Length > 2)
                                    {
                                        nameSecondPart += nameObj + " ";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (condition != "")
                    {
                        condition +=
                            " or SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Upper(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                            AddSlash(nameList.Trim().ToUpper()) + "') OR Upper(DI.NM) LIKE '%" +
                            AddSlash(nameList.Trim().ToUpper()) + "%'";
                    }
                    else
                    {
                        condition += " SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Upper(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" + AddSlash(nameList.Trim().ToUpper()) + "') OR Upper(DI.NM) LIKE '%" + AddSlash(nameList.Trim().ToUpper()) + "%'";
                    }
                }
                if (nameSecondPart.Length > 0)
                {
                    if (condition != "")
                    {
                        condition +=
                            " or SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Upper(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                            AddSlash(nameSecondPart.Trim().ToUpper()) + "') OR Upper(DI.NM) LIKE '%" +
                            AddSlash(nameSecondPart.Trim().ToUpper()) + "%'";

                    }
                    else
                    {
                        condition +=
                            " SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Upper(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                            AddSlash(nameSecondPart.Trim().ToUpper()) + "') OR Upper(DI.NM) LIKE '%" +
                            AddSlash(nameSecondPart.Trim().ToUpper()) + "%'";
                    }
                }
            }
            if (fatherName.Length > 0)
            {
                if (condition != "")
                {
                    condition +=
                        " or SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.FATHE_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                        AddSlash(fatherName.ToUpper()) + "') and Upper(DI.FATHE_NM) LIKE '%" +
                        AddSlash(fatherName.ToUpper()) + "%'";
                }
                else
                {
                    condition +=
                        " SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.FATHE_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                        AddSlash(fatherName.ToUpper()) + "') and Upper(DI.FATHE_NM) LIKE '%" +
                        AddSlash(fatherName.ToUpper()) + "%'";
                }
            }
            if (motherName.Length > 0)
            {
                if (condition != "")
                {
                    condition +=
                        " or SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.MOTHE_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                        AddSlash(motherName.ToUpper()) + "') and UPPER(DI.MOTHE_NM) LIKE '%" +
                        AddSlash(motherName.ToUpper()) + "%'";
                }
                else
                {
                    condition +=
                       " SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.MOTHE_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                       AddSlash(motherName.ToUpper()) + "') and UPPER(DI.MOTHE_NM) LIKE '%" +
                       AddSlash(motherName.ToUpper()) + "%'";
                }
            }
            if (businessEmployeeName.Length > 0)
            {
                if (condition != "")
                {
                    condition +=
                        " or SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.BUSIN_EMPLO_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                        AddSlash(businessEmployeeName.ToUpper()) + "') OR Upper(DI.BUSIN_EMPLO_NM) LIKE '%" +
                        AddSlash(businessEmployeeName.ToUpper()) + "%'";
                    condition +=
                        " or SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                        AddSlash(businessEmployeeName.ToUpper()) + "') OR UPPER(DI.NM) LIKE '%" +
                        AddSlash(businessEmployeeName.ToUpper()) + "%'";

                }
                else
                {
                    condition +=
                        " SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.BUSIN_EMPLO_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                        AddSlash(businessEmployeeName.ToUpper()) + "') OR Upper(DI.BUSIN_EMPLO_NM) LIKE '%" +
                        AddSlash(businessEmployeeName.ToUpper()) + "%'";
                    condition +=
                        " or SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" +
                        AddSlash(businessEmployeeName.ToUpper()) + "') OR UPPER(DI.NM) LIKE '%" +
                        AddSlash(businessEmployeeName.ToUpper()) + "%'";
                }
            }
            if (dob.Length > 0)
            {
                if (condition != "")
                {
                    condition += " OR DI.DOB like ('" +
                                 Convert.ToDateTime(dob, new System.Globalization.CultureInfo("ru-RU"))
                                     .ToString("yyyy-MM-dd") + "')";
                }
                else
                {
                    condition += " DI.DOB like ('" +
                                 Convert.ToDateTime(dob, new System.Globalization.CultureInfo("ru-RU"))
                                     .ToString("yyyy-MM-dd") + "')";
                }
            }

            if (contact.Count > 0)
            {
                foreach (string contactNo in contact)
                {
                    if (contactNo.Length > 0)
                    {
                        if (condition != "")
                        {
                            condition += " OR DI.MOB1 like '%" + contactNo + "%' ";
                            condition += " OR DI.MOB2 like '%" + contactNo + "%' ";
                            condition += " OR DI.PH1 like '%" + contactNo + "%' ";
                            condition += " OR DI.PH2 like '%" + contactNo + "%' ";
                        }
                        else
                        {
                            condition += " DI.MOB1 like '%" + contactNo + "%' ";
                            condition += " OR DI.MOB2 like '%" + contactNo + "%' ";
                            condition += " OR DI.PH1 like '%" + contactNo + "%' ";
                            condition += " OR DI.PH2 like '%" + contactNo + "%' ";
                        }
                    }
                }
            }


            if (id.Count > 0)
            {
                foreach (string idNo in id)
                {
                    if (idNo.Length > 0)
                    {
                        if (condition != "")
                        {
                            condition += " OR DI.ID_NO LIKE '%" + idNo + "%' ";
                            condition += " OR DI.DEDU_IDNO2 LIKE '%" + idNo + "%' ";
                            condition += " OR DI.DEDU_IDNO3 LIKE '%" + idNo + "%' ";
                            condition += " OR DI.DEDU_IDNO4 LIKE '%" + idNo + "%' ";
                            condition += " OR DI.DEDU_IDNO5 LIKE '%" + idNo + "%' ";
                        }
                        else
                        {
                            condition += " DI.ID_NO LIKE '%" + idNo + "%' ";
                            condition += " OR DI.DEDU_IDNO2 LIKE '%" + idNo + "%' ";
                            condition += " OR DI.DEDU_IDNO3 LIKE '%" + idNo + "%' ";
                            condition += " OR DI.DEDU_IDNO4 LIKE '%" + idNo + "%' ";
                            condition += " OR DI.DEDU_IDNO5 LIKE '%" + idNo + "%' ";
                        }

                    }
                }
            }
            if (ac.Count > 0)
            {
                foreach (string masterAcNo in ac)
                {
                    if (masterAcNo.Length > 0)
                    {
                        if (condition != "")
                        {
                            condition += " OR DI.SCB_MST_NO LIKE '%" + masterAcNo + "%' ";
                        }
                        else
                        {
                            condition += " DI.SCB_MST_NO LIKE '%" + masterAcNo + "%' ";
                        }

                    }
                }
            }
            if (tin.Length > 0)
            {
                if (condition != "")
                {
                    condition += " OR DI.TIN LIKE '%" + tin + "%' ";
                }
                else
                {
                    condition += " DI.TIN LIKE '%" + tin + "%' ";
                }
            }

            if (type == 1)
            {
                if (condition.Length > 0)
                {
                    finalCondition = " WHERE  (" + condition + ") AND DI.STATU != 'D' ";
                }
            }
            else
            {
                if (condition.Length > 0)
                {
                    finalCondition = " WHERE  (" + condition + ") AND DI.STATU = 'D' ";
                }
            }
            string mSQL = String.Format(@"SELECT DI.DE_DUP_ID
	,DI.NM
	,DI.FATHE_NM
	,DI.MOTHE_NM
	,DI.SCB_MST_NO
	,DI.DOB
	,DI.PROFE
	,DI.PROD_ID
	,DI.DECLI_REASO
	,DI.DECLI_DT
	,DI.BUSIN_EMPLO_NM
	,DI.ADD1
	,DI.ADD2
	,DI.PH1
	,DI.PH2
	,DI.MOB1
	,DI.MOB2
	,DI.BANK_BRANC
	,DI.SOURC
	,DI.STATU
	,DI.REMAR
	,DI.TIN
	,DI.ID_TYPE
	,DI.DEDU_IDTYPE2
	,DI.DEDU_IDTYPE3
	,DI.DEDU_IDTYPE4
	,DI.DEDU_IDTYPE5
	,DI.ID_NO
	,DI.DEDU_IDNO2
	,DI.DEDU_IDNO3
	,DI.DEDU_IDNO4
	,DI.DEDU_IDNO5
	,DI.CRITE_TYPE
	,DI.ACTIV
	,DI.DEDU_MARITALSTATUS
	,DI.DEDU_EDUCATIONLEVEL
	,DI.BUSIN_EMPLO_NM
	,DI.ADD2
	,DI.DEDU_RESIDENTIALSTATUS
	,DI.ID_TYPE
	,DI.DEDU_IDTYPE2
	,DI.DEDU_IDTYPE3
	,DI.DEDU_IDTYPE4
	,DI.DEDU_IDTYPE5
	,DI.DEDU_IDNO2
	,DI.DEDU_IDNO3
	,DI.DEDU_IDNO4
	,DI.DEDU_IDNO5 
FROM de_dup_info DI" + finalCondition + " ORDER BY DI.NM");

            var q = mSQL.Replace("'", "''");
            //var savesql = string.Format(@"INSERT INTO SqlTable (Query,Remarks) VALUES ('{0}','{1}')", q, "Dedup Negative List");
            //DbQueryManager.ExecuteNonQuery(savesql);

            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            if (finalCondition.Length == 0)
            {
                return deDupInfoObjList;
            }
            else
            {
                try
                {
                    DbDataReader deDupInfoDataReader = DbQueryManager.ExecuteReader(mSQL);

                    LogFile.WriteLine(mSQL);

                    string t = (string)System.Web.HttpContext.Current.Session["UserId"];

                    while (deDupInfoDataReader.Read())
                    {
                        deDupInfo = new DeDupInfo();

                        //deDupInfoObj.Id = Convert.ToInt32(deDupInfoDataReader["DE_DUP_ID"]);
                        deDupInfo.Id = ConvertionUtilities.GetInteger(deDupInfoDataReader["DE_DUP_ID"].ToString());

                        deDupInfo.Name = deDupInfoDataReader["NM"].ToString();
                        deDupInfo.FatherName = deDupInfoDataReader["FATHE_NM"].ToString();
                        deDupInfo.MotherName = deDupInfoDataReader["MOTHE_NM"].ToString();
                        deDupInfo.ScbMaterNo = deDupInfoDataReader["SCB_MST_NO"].ToString();
                        deDupInfo.DateOfBirth = ConvertionUtilities.GetDateTime(deDupInfoDataReader["DOB"].ToString());
                        deDupInfo.Profession = deDupInfoDataReader["PROFE"].ToString();
                        deDupInfo.ProductId = ConvertionUtilities.GetInteger(deDupInfoDataReader["PROD_ID"].ToString());
                        deDupInfo.DeclineReason = deDupInfoDataReader["DECLI_REASO"].ToString();
                        deDupInfo.DeclineDate = ConvertionUtilities.GetDateTime(deDupInfoDataReader["DECLI_DT"].ToString());
                        deDupInfo.BusinessEmployeeName = deDupInfoDataReader["BUSIN_EMPLO_NM"].ToString();
                        deDupInfo.Address1 = deDupInfoDataReader["ADD1"].ToString();
                        deDupInfo.Address2 = deDupInfoDataReader["ADD2"].ToString();
                        deDupInfo.Phone1 = deDupInfoDataReader["PH1"].ToString();
                        deDupInfo.Phone2 = deDupInfoDataReader["PH2"].ToString();
                        deDupInfo.Mobile1 = deDupInfoDataReader["MOB1"].ToString();
                        deDupInfo.Mobile2 = deDupInfoDataReader["MOB1"].ToString();
                        deDupInfo.BankBranch = deDupInfoDataReader["BANK_BRANC"].ToString();
                        deDupInfo.Source = deDupInfoDataReader["SOURC"].ToString();
                        deDupInfo.Status = deDupInfoDataReader["STATU"].ToString();
                        deDupInfo.Remarks = deDupInfoDataReader["REMAR"].ToString();
                        deDupInfo.Tin = deDupInfoDataReader["TIN"].ToString();
                        deDupInfo.ApplicantId.Type1 = deDupInfoDataReader["ID_TYPE"].ToString();
                        deDupInfo.ApplicantId.Type2 = deDupInfoDataReader["DEDU_IDTYPE2"].ToString();
                        string tlk = deDupInfoDataReader["DEDU_IDTYPE3"].ToString();
                        deDupInfo.ApplicantId.Type3 = tlk;//deDupInfoDataReader["DEDU_IDTYPE3"].ToString();
                        deDupInfo.ApplicantId.Type4 = deDupInfoDataReader["DEDU_IDTYPE4"].ToString();
                        deDupInfo.ApplicantId.Type5 = deDupInfoDataReader["DEDU_IDTYPE5"].ToString();
                        deDupInfo.ApplicantId.Id1 = deDupInfoDataReader["ID_NO"].ToString();
                        deDupInfo.ApplicantId.Id2 = deDupInfoDataReader["DEDU_IDNO2"].ToString();
                        deDupInfo.ApplicantId.Id3 = deDupInfoDataReader["DEDU_IDNO3"].ToString();
                        deDupInfo.ApplicantId.Id4 = deDupInfoDataReader["DEDU_IDNO4"].ToString();
                        deDupInfo.ApplicantId.Id5 = deDupInfoDataReader["DEDU_IDNO5"].ToString();
                        deDupInfo.CriteriaType = deDupInfoDataReader["CRITE_TYPE"].ToString();
                        deDupInfo.Active = deDupInfoDataReader["ACTIV"].ToString();
                        deDupInfo.MaritalStatus = deDupInfoDataReader["DEDU_MARITALSTATUS"].ToString();
                        deDupInfo.EducationalLevel = deDupInfoDataReader["DEDU_EDUCATIONLEVEL"].ToString();
                        deDupInfo.ResidentaileStatus = deDupInfoDataReader["DEDU_RESIDENTIALSTATUS"].ToString();


                        //Adding Contact NO's
                        deDupInfo.ContactNumber.ContactNo1 = deDupInfoDataReader["PH1"].ToString();
                        deDupInfo.ContactNumber.ContactNo2 = deDupInfoDataReader["PH2"].ToString();
                        deDupInfo.ContactNumber.ContactNo3 = deDupInfoDataReader["MOB1"].ToString();
                        deDupInfo.ContactNumber.ContactNo4 = deDupInfoDataReader["MOB2"].ToString();

                        if (String.IsNullOrEmpty(deDupInfo.ScbMaterNo) || deDupInfo.ScbMaterNo == null)
                        {
                            deDupInfo.AccountNo.PreAccNo1 = "";
                            deDupInfo.AccountNo.MasterAccNo1 = "";
                            deDupInfo.AccountNo.PostAccNo1 = "";
                        }
                        else
                        {
                            string accountNo1 = deDupInfo.ScbMaterNo;

                            if (accountNo1.Length == 11)
                            {
                                string preAccNo1 = accountNo1.Substring(0, 2).ToString();
                                string masterAccNo1 = accountNo1.Substring(2, 7).ToString();
                                string postAccNo1 = accountNo1.Substring(9, 2).ToString();
                                deDupInfo.AccountNo.PreAccNo1 = preAccNo1;
                                deDupInfo.AccountNo.MasterAccNo1 = masterAccNo1;
                                deDupInfo.AccountNo.PostAccNo1 = postAccNo1;
                            }
                            else if (accountNo1.Length < 11)
                            {
                                deDupInfo.AccountNo.PreAccNo1 = "";
                                deDupInfo.AccountNo.MasterAccNo1 = accountNo1;
                                deDupInfo.AccountNo.PostAccNo1 = "";
                            }
                            else
                            {
                                if (accountNo1.ToString().Contains(","))
                                {
                                    deDupInfo.AccountNo.PreAccNo1 = "";
                                    deDupInfo.AccountNo.MasterAccNo1 = accountNo1;
                                    deDupInfo.AccountNo.PostAccNo1 = "";
                                }
                                else
                                {
                                    deDupInfo.AccountNo.MasterAccNo1 = accountNo1;
                                }
                            }
                        }
                        deDupInfoObjList.Add(deDupInfo);
                    }

                    deDupInfoDataReader.Close();
                }
                catch (Exception ex)
                {
                    LogFile.WriteLog(ex);
                    string exceptionMessage = ex.Message;
                    throw new Exception(ex.Message);
                }

                return deDupInfoObjList;
            }

        }

        public DataTable GetDedupNegativeListTempDataTable()
        {
            try
            {
                DataTable table = DbQueryManager.GetDataTableWithSp("spGetNegativeListTemp");

                return table;
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("ApproveNegativeListTemp : Dedupinfogateway");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Negative List Approval Problem");
            }
            return null;
        }

        public string ApproveDeDupNegativListTemp(User actionUser)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@CHECKER_PS_ID", actionUser.UserId));

                int count = DbQueryManager.ExecuteSP("spApproveNegativeList", parameters);
                if (count > 0)
                    return Operation.Success.ToString();
                return Operation.Failed.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("ApproveNegativeListTemp : Dedupinfogateway");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Negative List Approval Problem");

                return Operation.Error.ToString();
            }
        }

        public string FlushDeDupNegativListTemp()
        {
            try
            {
                string sql = "DELETE FROM de_dup_info_negative_temp";
                DbQueryManager.ExecuteNonQuery(sql);
                return Operation.Success.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("FlushDeDupNegativListTemp : Dedupinfogateway");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");

                return Operation.Error.ToString();
            }
        }

        public string FlushCustomerBulkDataTemp()
        {
            try
            {
                string sql = "DELETE FROM CustomerBulkData_temp";
                DbQueryManager.ExecuteNonQuery(sql);
                return Operation.Success.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("FlushCustomerBulkDataTemp : Dedupinfogateway");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");

                return Operation.Error.ToString();
            }
        }

        public DataTable GetCustomerBulkDataTempListTable()
        {
            try
            {
                DataTable table = DbQueryManager.GetDataTableWithSp("spGetCustomerBulkDataListTemp");

                return table;
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("GetCustomerBulkDataTempListTable : Dedupinfogateway");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Customer Bulk Data List ");
            }
            return null;
        }

        #region Get Dedup Negative List

        //public List<DeDupInfo> GetTempNegativeList()
        //{
        //    List<DeDupInfo> list = new List<DeDupInfo>();
        //    DeDupInfo dedupinfo = null;
        //    string query = "SELECT * FROM t_pluss_negativelistemployer_temp";
        //    try
        //    {
        //        DbDataReader reader = DbQueryManager.ExecuteReader(query);
        //        while (reader.Read())
        //        {
        //            dedupinfo = new DeDupInfo();
        //            dedupinfo.Id = Convert.ToInt32(reader["DE_DUP_ID"]);
        //            dedupinfo.ApplicantId.Id1 = Convert.ToString(reader["ID_NO"]);
        //            dedupinfo.ApplicantId.Id2 = Convert.ToString(reader["DEDU_IDNO2"]);
        //            dedupinfo.ApplicantId.Id3 = Convert.ToString(reader["DEDU_IDNO3"]);
        //            dedupinfo.ApplicantId.Id4 = Convert.ToString(reader["DEDU_IDNO4"]);
        //            dedupinfo.ApplicantId.Id5 = Convert.ToString(reader["DEDU_IDNO5"]);
        //            dedupinfo.ApplicantId.Type1 = Convert.ToString(reader["ID_TYPE"]);
        //            dedupinfo.ApplicantId.Type2 = Convert.ToString(reader["DEDU_IDTYPE2"]);
        //            dedupinfo.ApplicantId.Type3 = Convert.ToString(reader["DEDU_IDTYPE3"]);
        //            dedupinfo.ApplicantId.Type4 = Convert.ToString(reader["DEDU_IDTYPE4"]);
        //            dedupinfo.ApplicantId.Type5 = Convert.ToString(reader["DEDU_IDTYPE5"]);
        //            if (String.IsNullOrEmpty(reader["SCB_MST_NO"].ToString()))
        //            {
        //                dedupinfo.AccountNo.MasterAccNo1 = "";
        //            }
        //            else
        //            {
        //                string accountNo1 = Convert.ToString(reader["SCB_MST_NO"]);
        //                dedupinfo.AccountNo.MasterAccNo1 = accountNo1;
        //            }

        //            dedupinfo.ApplicantId.Type1 = reader["ID_TYPE"].ToString();
        //            dedupinfo.ApplicantId.Id1 = reader["ID_NO"].ToString();
        //            dedupinfo.ApplicantId.Type2 = reader["DEDU_IDTYPE2"].ToString();
        //            dedupinfo.ApplicantId.Id2 = reader["DEDU_IDNO2"].ToString();
        //            dedupinfo.ApplicantId.Type3 = reader["DEDU_IDTYPE3"].ToString();
        //            dedupinfo.ApplicantId.Id3 = reader["DEDU_IDNO3"].ToString();
        //            dedupinfo.ApplicantId.Type4 = reader["DEDU_IDTYPE4"].ToString();
        //            dedupinfo.ApplicantId.Id4 = reader["DEDU_IDNO4"].ToString();
        //            dedupinfo.ApplicantId.Type5 = reader["DEDU_IDTYPE5"].ToString();
        //            dedupinfo.ApplicantId.Id5 = reader["DEDU_IDNO5"].ToString();
        //            dedupinfo.ContactNumber.ContactNo1 = Convert.ToString(reader["MOB1"]);
        //            dedupinfo.ContactNumber.ContactNo2 = Convert.ToString(reader["MOB2"]);
        //            dedupinfo.ContactNumber.ContactNo3 = Convert.ToString(reader["PH1"]);
        //            dedupinfo.ContactNumber.ContactNo4 = Convert.ToString(reader["PH2"]);
        //            dedupinfo.Name = Convert.ToString(reader["NM"]);
        //            dedupinfo.FatherName = Convert.ToString(reader["FATHE_NM"]);
        //            dedupinfo.MotherName = Convert.ToString(reader["MOTHE_NM"]);
        //            dedupinfo.DateOfBirth = Convert.ToDateTime(reader["DOB"]);
        //            dedupinfo.Profession = Convert.ToString(reader["PROFE"]);
        //            dedupinfo.ProductId = Convert.ToInt32(reader["PROD_ID"]);
        //            dedupinfo.DeclineReason = Convert.ToString(reader["DECLI_REASO"]);
        //            dedupinfo.DeclineDate = Convert.ToDateTime(reader["DECLI_DT"]);
        //            dedupinfo.BusinessEmployeeName = Convert.ToString(reader["BUSIN_EMPLO_NM"]);
        //            dedupinfo.Address1 = Convert.ToString(reader["ADD1"]);
        //            dedupinfo.Address2 = Convert.ToString(reader["ADD2"]);
        //            dedupinfo.Source = Convert.ToString(reader["SOURC"]);
        //            dedupinfo.Status = Convert.ToString(reader["STATU"]);
        //            dedupinfo.Remarks = Convert.ToString(reader["REMAR"]);
        //            dedupinfo.Tin = Convert.ToString(reader["TIN"]);
        //            dedupinfo.CriteriaType = Convert.ToString(reader["CRITE_TYPE"]);
        //            dedupinfo.Active = Convert.ToString(reader["ACTIV"]);
        //            dedupinfo.EducationalLevel = Convert.ToString(reader["DEDU_EDUCATIONLEVEL"]);
        //            dedupinfo.MaritalStatus = Convert.ToString(reader["DEDU_MARITALSTATUS"]);
        //            dedupinfo.ResidentaileStatus = Convert.ToString(reader["DEDU_RESIDENTIALSTATUS"]);
        //            dedupinfo.LoanAccountNo = Convert.ToString(reader["DEDU_LOANACCOUNTNO"]);
        //            dedupinfo.MakerPsId = Convert.ToString(reader["MAKER_PS_ID"]);
        //            dedupinfo.MakeDate = reader["MAKE_DATE"] as DateTime? ?? DateTime.MinValue;
        //            dedupinfo.CheckerPsId = Convert.ToString(reader["CHECKER_PS_ID"]);
        //            //if (reader["CHECK_DATE"] != null)
        //            dedupinfo.CheckDate = reader["CHECK_DATE"] as DateTime? ?? DateTime.MinValue;
        //            list.Add(dedupinfo);
        //        }

        //        reader.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string exceptionMessage = ex.Message;
        //    }
        //    return list;
        //}  
        #endregion
        public List<DeDupInfo> GetTempDedupNegativeList()
        {
            List<DeDupInfo> list = new List<DeDupInfo>();
            DeDupInfo dedupinfo = null;
            string query = "SELECT * FROM de_dup_info_negative_temp";
            try
            {
                DbDataReader reader = DbQueryManager.ExecuteReader(query);
                while (reader.Read())
                {
                    dedupinfo = new DeDupInfo();
                    //dedupinfo.Id = Convert.ToInt32(reader["DE_DUP_ID"]);
                    dedupinfo.ApplicantId.Id1 = Convert.ToString(reader["ID_NO"]);
                    dedupinfo.ApplicantId.Id2 = Convert.ToString(reader["DEDU_IDNO2"]);
                    dedupinfo.ApplicantId.Id3 = Convert.ToString(reader["DEDU_IDNO3"]);
                    dedupinfo.ApplicantId.Id4 = Convert.ToString(reader["DEDU_IDNO4"]);
                    dedupinfo.ApplicantId.Id5 = Convert.ToString(reader["DEDU_IDNO5"]);
                    dedupinfo.ApplicantId.Type1 = Convert.ToString(reader["ID_TYPE"]);
                    dedupinfo.ApplicantId.Type2 = Convert.ToString(reader["DEDU_IDTYPE2"]);
                    dedupinfo.ApplicantId.Type3 = Convert.ToString(reader["DEDU_IDTYPE3"]);
                    dedupinfo.ApplicantId.Type4 = Convert.ToString(reader["DEDU_IDTYPE4"]);
                    dedupinfo.ApplicantId.Type5 = Convert.ToString(reader["DEDU_IDTYPE5"]);
                    if (String.IsNullOrEmpty(reader["SCB_MST_NO"].ToString()))
                    {
                        dedupinfo.AccountNo.MasterAccNo1 = "";
                    }
                    else
                    {
                        string accountNo1 = Convert.ToString(reader["SCB_MST_NO"]);
                        dedupinfo.AccountNo.MasterAccNo1 = accountNo1;
                    }

                    dedupinfo.ApplicantId.Type1 = reader["ID_TYPE"].ToString();
                    dedupinfo.ApplicantId.Id1 = reader["ID_NO"].ToString();
                    dedupinfo.ApplicantId.Type2 = reader["DEDU_IDTYPE2"].ToString();
                    dedupinfo.ApplicantId.Id2 = reader["DEDU_IDNO2"].ToString();
                    dedupinfo.ApplicantId.Type3 = reader["DEDU_IDTYPE3"].ToString();
                    dedupinfo.ApplicantId.Id3 = reader["DEDU_IDNO3"].ToString();
                    dedupinfo.ApplicantId.Type4 = reader["DEDU_IDTYPE4"].ToString();
                    dedupinfo.ApplicantId.Id4 = reader["DEDU_IDNO4"].ToString();
                    dedupinfo.ApplicantId.Type5 = reader["DEDU_IDTYPE5"].ToString();
                    dedupinfo.ApplicantId.Id5 = reader["DEDU_IDNO5"].ToString();
                    dedupinfo.ContactNumber.ContactNo1 = Convert.ToString(reader["MOB1"]);
                    dedupinfo.ContactNumber.ContactNo2 = Convert.ToString(reader["MOB2"]);
                    dedupinfo.ContactNumber.ContactNo3 = Convert.ToString(reader["PH1"]);
                    dedupinfo.ContactNumber.ContactNo4 = Convert.ToString(reader["PH2"]);
                    dedupinfo.Name = Convert.ToString(reader["NM"]);
                    dedupinfo.FatherName = Convert.ToString(reader["FATHE_NM"]);
                    dedupinfo.MotherName = Convert.ToString(reader["MOTHE_NM"]);
                    dedupinfo.DateOfBirth = Convert.ToDateTime(reader["DOB"]);
                    dedupinfo.Profession = Convert.ToString(reader["PROFE"]);
                    dedupinfo.ProductId = Convert.ToInt32(reader["PROD_ID"]);
                    dedupinfo.DeclineReason = Convert.ToString(reader["DECLI_REASO"]);
                    dedupinfo.DeclineDate = Convert.ToDateTime(reader["DECLI_DT"]);
                    dedupinfo.BusinessEmployeeName = Convert.ToString(reader["BUSIN_EMPLO_NM"]);
                    dedupinfo.Address1 = Convert.ToString(reader["ADD1"]);
                    dedupinfo.Address2 = Convert.ToString(reader["ADD2"]);
                    dedupinfo.Source = Convert.ToString(reader["SOURC"]);
                    dedupinfo.Status = Convert.ToString(reader["STATU"]);
                    dedupinfo.Remarks = Convert.ToString(reader["REMAR"]);
                    dedupinfo.Tin = Convert.ToString(reader["TIN"]);
                    dedupinfo.CriteriaType = Convert.ToString(reader["CRITE_TYPE"]);
                    dedupinfo.Active = Convert.ToString(reader["ACTIV"]);
                    dedupinfo.EducationalLevel = Convert.ToString(reader["DEDU_EDUCATIONLEVEL"]);
                    dedupinfo.MaritalStatus = Convert.ToString(reader["DEDU_MARITALSTATUS"]);
                    dedupinfo.ResidentaileStatus = Convert.ToString(reader["DEDU_RESIDENTIALSTATUS"]);
                    dedupinfo.LoanAccountNo = Convert.ToString(reader["DEDU_LOANACCOUNTNO"]);
                    dedupinfo.MakerPsId = Convert.ToString(reader["MAKER_PS_ID"]);
                    dedupinfo.MakeDate = reader["MAKE_DATE"] as DateTime? ?? DateTime.MinValue;
                    dedupinfo.CheckerPsId = Convert.ToString(reader["CHECKER_PS_ID"]);
                    //if (reader["CHECK_DATE"] != null)
                    dedupinfo.CheckDate = reader["CHECK_DATE"] as DateTime? ?? DateTime.MinValue;
                    list.Add(dedupinfo);
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
            }
            return list;
        }


        //public List<DeDupInfo> GetAllDeDupInfo(string nameList, string fatherName, string motherName, string businessEmployeeName, string dob, List<string> contact, List<string> id, List<string> ac, /*List<string> bankBranch,*/ string tin, int type)
        //{
        //    string condition = "";
        //    string finalCondition = "";
        //    if (nameList.Length > 0)
        //    {
        //        int firstIndex = 0;
        //        string nameSecondPart = "";
        //        string[] splitName = nameList.Split(' ');
        //        if (splitName.Length > 0)
        //        {
        //            foreach (string nameObj in splitName)
        //            {
        //                if (nameObj.Length > 2)
        //                {
        //                    if (firstIndex == 0)
        //                    {
        //                        condition += " OR SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(upper(DI.NM),'Mohammad',''),'Mohamad',''),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" + AddSlash(nameObj.ToUpper()) + "') OR upper(DI.NM) LIKE '%" + AddSlash(nameObj.ToUpper()) + "%'";
        //                        firstIndex++;
        //                    }
        //                    else
        //                    {
        //                        if (splitName.Length > 2)
        //                        {
        //                            if (nameObj.Length > 2)
        //                            {
        //                                nameSecondPart += nameObj + " ";
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            condition += " OR SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" + AddSlash(nameList.Trim().ToUpper()) + "') OR UPPER(DI.NM) LIKE '%" + AddSlash(nameList.Trim().ToUpper()) + "%'";
        //        }
        //        if (nameSecondPart.Length > 0)
        //        {
        //            condition += " OR SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" + AddSlash(nameSecondPart.Trim().ToUpper()) + "') OR UPPER(DI.NM) LIKE '%" + AddSlash(nameSecondPart.Trim().ToUpper()) + "%'";
        //        }
        //    }
        //    if (fatherName.Length > 0)
        //    {
        //        condition += " OR SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.FATHE_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" + AddSlash(fatherName.ToUpper()) + "') OR UPPER(DI.FATHE_NM) LIKE '%" + AddSlash(fatherName.ToUpper()) + "%'";
        //    }
        //    if (motherName.Length > 0)
        //    {
        //        condition += " OR SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.MOTHE_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" + AddSlash(motherName.ToUpper()) + "') OR UPPER(DI.MOTHE_NM) LIKE '%" + AddSlash(motherName.ToUpper()) + "%'";
        //    }
        //    if (businessEmployeeName.Length > 0)
        //    {
        //        condition += " OR SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.BUSIN_EMPLO_NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" + AddSlash(businessEmployeeName.ToUpper()) + "') OR UPPER(DI.BUSIN_EMPLO_NM) LIKE '%" + AddSlash(businessEmployeeName.ToUpper()) + "%'";
        //        condition += " OR SOUNDEX(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(DI.NM),'MD',''),'MD.',''),'MR',''),'MR.',''),'MRS',''),'MRS.',''),'MIS',''),'MIS.',''),'M/S',''),'M/S.',''),'DR.',''),'LATE',''))) = SOUNDEX('" + AddSlash(businessEmployeeName.ToUpper()) + "') OR UPPER(DI.NM) LIKE '%" + AddSlash(businessEmployeeName.ToUpper()) + "%'";
        //    }
        //    if (dob.Length > 0)
        //    {
        //        condition += " OR DI.DOB like ('" + Convert.ToDateTime(dob, new System.Globalization.CultureInfo("ru-RU")).ToString("yyyy-MM-dd") + "')";
        //    }

        //    if (contact.Count > 0)
        //    {
        //        foreach (string contactNo in contact)
        //        {
        //            if (contactNo.Length > 0)
        //            {
        //                condition += " OR DI.MOB1 like '%" + contactNo + "%' ";
        //                condition += " OR DI.MOB2 like '%" + contactNo + "%' ";
        //                condition += " OR DI.PH1 like '%" + contactNo + "%' ";
        //                condition += " OR DI.PH2 like '%" + contactNo + "%' ";
        //            }
        //        }
        //    }


        //    if (id.Count > 0)
        //    {
        //        foreach (string idNo in id)
        //        {
        //            if (idNo.Length > 0)
        //            {
        //                condition += " OR DI.ID_NO LIKE '%" + idNo + "%' ";
        //                condition += " OR DI.DEDU_IDNO2 LIKE '%" + idNo + "%' ";
        //                condition += " OR DI.DEDU_IDNO3 LIKE '%" + idNo + "%' ";
        //                condition += " OR DI.DEDU_IDNO4 LIKE '%" + idNo + "%' ";
        //                condition += " OR DI.DEDU_IDNO5 LIKE '%" + idNo + "%' ";
        //            }
        //        }
        //    }
        //    if (ac.Count > 0)
        //    {
        //        foreach (string masterAcNo in ac)
        //        {
        //            if (masterAcNo.Length > 0)
        //            {
        //                condition += " OR DI.SCB_MST_NO LIKE '%" + masterAcNo + "%' ";
        //            }
        //        }
        //    }
        //    if (tin.Length > 0)
        //    {
        //        condition += " OR DI.TIN LIKE '%" + tin + "%' ";
        //    }

        //    if (type == 1)
        //    {
        //        if (condition.Length > 0)
        //        {
        //            finalCondition = "WHERE  (0=0 " + condition + ") AND DI.STATU != 'D' ";
        //        }
        //    }
        //    else
        //    {
        //        if (condition.Length > 0)
        //        {
        //            finalCondition = "WHERE  (0=0 " + condition + ") AND DI.STATU = 'D' ";
        //        }
        //    }
        //string mSQL = "SELECT DI.DE_DUP_ID, DI.NM, DI.FATHE_NM, DI.MOTHE_NM, DI.SCB_MST_NO,isnull((Case DI.DOB WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(DI.DOB AS CHAR) END),'') AS DOB, DI.PROFE, DI.PROD_ID, DI.DECLI_REASO, " +
        //" isnull((Case DI.DECLI_DT WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(DI.DECLI_DT AS CHAR) END),'') AS DECLI_DT, DI.BUSIN_EMPLO_NM, DI.ADD1, DI.ADD2, DI.PH1, DI.PH2, DI.MOB1, DI.MOB2, DI.BANK_BRANC, DI.SOURC," +
        //" DI.STATU, DI.REMAR, DI.TIN, DI.ID_TYPE, DI.DEDU_IDTYPE2, DI.DEDU_IDTYPE3, DI.DEDU_IDTYPE4, " +
        //" DI.DEDU_IDTYPE5, DI.ID_NO, DI.DEDU_IDNO2, DI.DEDU_IDNO3, DI.DEDU_IDNO4, " +
        //" DI.DEDU_IDNO5, DI.CRITE_TYPE, DI.ACTIV, DI.DEDU_MARITALSTATUS, DI.DEDU_EDUCATIONLEVEL, DI.BUSIN_EMPLO_NM, DI.ADD2, DI.DEDU_RESIDENTIALSTATUS, " +
        //" DI.ID_TYPE, DI.DEDU_IDTYPE2, DI.DEDU_IDTYPE3, DI.DEDU_IDTYPE4, DI.DEDU_IDTYPE5, " +
        //" DI.DEDU_IDNO2, DI.DEDU_IDNO3, DI.DEDU_IDNO4, DI.DEDU_IDNO5 " +
        //" FROM de_dup_info AS DI " + finalCondition + " ORDER BY DI.NM";

        //    List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
        //    if (finalCondition.Length == 0)
        //    {
        //        return deDupInfoObjList;
        //    }
        //    else
        //    {
        //        try
        //        {
        //            DbDataReader deDupInfoDataReader = DbQueryManager.ExecuteReader(mSQL);
        //            while (deDupInfoDataReader.Read())
        //            {
        //                deDupInfoObj = new DeDupInfo();

        //                deDupInfoObj.Id = Convert.ToInt32(deDupInfoDataReader["DE_DUP_ID"]);

        //                deDupInfoObj.ApplicantId.Id1 = Convert.ToString(deDupInfoDataReader["ID_NO"]);
        //                deDupInfoObj.ApplicantId.Id2 = Convert.ToString(deDupInfoDataReader["DEDU_IDNO2"]);
        //                deDupInfoObj.ApplicantId.Id3 = Convert.ToString(deDupInfoDataReader["DEDU_IDNO3"]);
        //                deDupInfoObj.ApplicantId.Id4 = Convert.ToString(deDupInfoDataReader["DEDU_IDNO4"]);
        //                deDupInfoObj.ApplicantId.Id5 = Convert.ToString(deDupInfoDataReader["DEDU_IDNO5"]);
        //                deDupInfoObj.ApplicantId.Type1 = Convert.ToString(deDupInfoDataReader["ID_TYPE"]);
        //                deDupInfoObj.ApplicantId.Type2 = Convert.ToString(deDupInfoDataReader["DEDU_IDTYPE2"]);
        //                deDupInfoObj.ApplicantId.Type3 = Convert.ToString(deDupInfoDataReader["DEDU_IDTYPE3"]);
        //                deDupInfoObj.ApplicantId.Type4 = Convert.ToString(deDupInfoDataReader["DEDU_IDTYPE4"]);
        //                deDupInfoObj.ApplicantId.Type5 = Convert.ToString(deDupInfoDataReader["DEDU_IDTYPE5"]);


        //                if (String.IsNullOrEmpty(deDupInfoDataReader["SCB_MST_NO"].ToString()) || deDupInfoDataReader["SCB_MST_NO"] == null)
        //                {
        //                    deDupInfoObj.AccountNo.PreAccNo1 = "";
        //                    deDupInfoObj.AccountNo.MasterAccNo1 = "";
        //                    deDupInfoObj.AccountNo.PostAccNo1 = "";
        //                }
        //                else
        //                {
        //                    string accountNo1 = Convert.ToString(deDupInfoDataReader["SCB_MST_NO"]);
        //                    if (accountNo1.Length == 11)
        //                    {
        //                        string preAccNo1 = accountNo1.Substring(0, 2).ToString();
        //                        string masterAccNo1 = accountNo1.Substring(2, 7).ToString();
        //                        string postAccNo1 = accountNo1.Substring(9, 2).ToString();
        //                        deDupInfoObj.AccountNo.PreAccNo1 = preAccNo1;
        //                        deDupInfoObj.AccountNo.MasterAccNo1 = masterAccNo1;
        //                        deDupInfoObj.AccountNo.PostAccNo1 = postAccNo1;
        //                    }
        //                    else if (accountNo1.Length < 11)
        //                    {
        //                        deDupInfoObj.AccountNo.PreAccNo1 = "";
        //                        deDupInfoObj.AccountNo.MasterAccNo1 = accountNo1;
        //                        deDupInfoObj.AccountNo.PostAccNo1 = "";
        //                    }
        //                    else
        //                    {
        //                        if (accountNo1.ToString().Contains(","))
        //                        {
        //                            deDupInfoObj.AccountNo.PreAccNo1 = "";
        //                            deDupInfoObj.AccountNo.MasterAccNo1 = accountNo1;
        //                            deDupInfoObj.AccountNo.PostAccNo1 = "";
        //                        }
        //                        else
        //                        {
        //                            deDupInfoObj.AccountNo.MasterAccNo1 = accountNo1;
        //                        }
        //                    }
        //                }
        //                deDupInfoObj.ApplicantId.Type1 = deDupInfoDataReader["ID_TYPE"].ToString();
        //                deDupInfoObj.ApplicantId.Id1 = deDupInfoDataReader["ID_NO"].ToString();
        //                deDupInfoObj.ApplicantId.Type2 = deDupInfoDataReader["DEDU_IDTYPE2"].ToString();
        //                deDupInfoObj.ApplicantId.Id2 = deDupInfoDataReader["DEDU_IDNO2"].ToString();
        //                deDupInfoObj.ApplicantId.Type3 = deDupInfoDataReader["DEDU_IDTYPE3"].ToString();
        //                deDupInfoObj.ApplicantId.Id3 = deDupInfoDataReader["DEDU_IDNO3"].ToString();
        //                deDupInfoObj.ApplicantId.Type4 = deDupInfoDataReader["DEDU_IDTYPE4"].ToString();
        //                deDupInfoObj.ApplicantId.Id4 = deDupInfoDataReader["DEDU_IDNO4"].ToString();
        //                deDupInfoObj.ApplicantId.Type5 = deDupInfoDataReader["DEDU_IDTYPE5"].ToString();
        //                deDupInfoObj.ApplicantId.Id5 = deDupInfoDataReader["DEDU_IDNO5"].ToString();
        //                deDupInfoObj.ContactNumber.ContactNo1 = Convert.ToString(deDupInfoDataReader["MOB1"]);
        //                deDupInfoObj.ContactNumber.ContactNo2 = Convert.ToString(deDupInfoDataReader["MOB2"]);
        //                deDupInfoObj.ContactNumber.ContactNo3 = Convert.ToString(deDupInfoDataReader["PH1"]);
        //                deDupInfoObj.ContactNumber.ContactNo4 = Convert.ToString(deDupInfoDataReader["PH2"]);
        //                deDupInfoObj.Name = Convert.ToString(deDupInfoDataReader["NM"]);
        //                deDupInfoObj.FatherName = Convert.ToString(deDupInfoDataReader["FATHE_NM"]);
        //                deDupInfoObj.MotherName = Convert.ToString(deDupInfoDataReader["MOTHE_NM"]);
        //                if (!String.IsNullOrEmpty(deDupInfoDataReader["DOB"].ToString()) || deDupInfoDataReader["DOB"].ToString() != "")
        //                {
        //                    try
        //                    {
        //                        deDupInfoObj.DateOfBirth = Convert.ToDateTime(deDupInfoDataReader["DOB"]);
        //                    }
        //                    catch
        //                    {
        //                    }
        //                }
        //                deDupInfoObj.Profession = Convert.ToString(deDupInfoDataReader["PROFE"]);
        //                deDupInfoObj.ProductId = Convert.ToInt32(deDupInfoDataReader["PROD_ID"]);
        //                deDupInfoObj.DeclineReason = Convert.ToString(deDupInfoDataReader["DECLI_REASO"]);
        //                if (!String.IsNullOrEmpty(deDupInfoDataReader["DECLI_DT"].ToString()) || deDupInfoDataReader["DECLI_DT"].ToString() != "")
        //                {
        //                    try
        //                    {
        //                        deDupInfoObj.DeclineDate = Convert.ToDateTime(deDupInfoDataReader["DECLI_DT"]);
        //                    }
        //                    catch
        //                    {
        //                    }
        //                }
        //                deDupInfoObj.BusinessEmployeeName = Convert.ToString(deDupInfoDataReader["BUSIN_EMPLO_NM"]);
        //                deDupInfoObj.Address1 = Convert.ToString(deDupInfoDataReader["ADD1"]);
        //                deDupInfoObj.Address2 = Convert.ToString(deDupInfoDataReader["ADD2"]);
        //                deDupInfoObj.Source = Convert.ToString(deDupInfoDataReader["SOURC"]);
        //                deDupInfoObj.Status = Convert.ToString(deDupInfoDataReader["STATU"]);
        //                deDupInfoObj.Remarks = Convert.ToString(deDupInfoDataReader["REMAR"]);
        //                deDupInfoObj.Tin = Convert.ToString(deDupInfoDataReader["TIN"]);
        //                deDupInfoObj.CriteriaType = Convert.ToString(deDupInfoDataReader["CRITE_TYPE"]);
        //                deDupInfoObj.Active = Convert.ToString(deDupInfoDataReader["ACTIV"]);
        //                deDupInfoObj.EducationalLevel = Convert.ToString(deDupInfoDataReader["DEDU_EDUCATIONLEVEL"]);
        //                deDupInfoObj.MaritalStatus = Convert.ToString(deDupInfoDataReader["DEDU_MARITALSTATUS"]);
        //                deDupInfoObj.ResidentaileStatus = Convert.ToString(deDupInfoDataReader["DEDU_RESIDENTIALSTATUS"]);
        //                deDupInfoObjList.Add(deDupInfoObj);
        //            }

        //            deDupInfoDataReader.Close();
        //        }
        //        catch (Exception ex)
        //        {
        //            string exceptionMessage = ex.Message;
        //        }
        //        return deDupInfoObjList;
        //    }

        //}

        public Int32 GetDedupId(string loanAccountNo, string status)
        {
            // string mSQL = "SELECT DE_DUP_ID FROM de_dup_info WHERE REPLACE(Upper(RTRIM(DEDU_LOANACCOUNTNO)) as DEDU_LOANACCOUNTNO,' ','')='" + AddSlash(loanAccountNo.Trim().ToUpper().Replace(" ", "")) + "' AND Upper(RTRIM(STATU)) as STATU='" + status.Trim().ToUpper() + "'";

            string mSQL = "SELECT DE_DUP_ID FROM de_dup_info_decline_temp WHERE DEDU_LOANACCOUNTNO='" + AddSlash(loanAccountNo) + "' AND STATU='" + AddSlash(status) + "'";

            return Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
        }
        public Int32 GetDeclineDedupId(string loanAccountNo, string status)
        {
            // string mSQL = "SELECT DE_DUP_ID FROM de_dup_info WHERE REPLACE(Upper(RTRIM(DEDU_LOANACCOUNTNO)) as DEDU_LOANACCOUNTNO,' ','')='" + AddSlash(loanAccountNo.Trim().ToUpper().Replace(" ", "")) + "' AND Upper(RTRIM(STATU)) as STATU='" + status.Trim().ToUpper() + "'";

            string mSQL = "SELECT DE_DUP_ID FROM de_dup_info_decline_temp WHERE DEDU_LOANACCOUNTNO='" + AddSlash(loanAccountNo) + "' AND STATU='" + AddSlash(status) + "'";

            return Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
        }
        public int GetProductId(string productName)
        {
            string mSQL = "";
            string pname = AddSlash(productName);
            if (pname == "PL")
            {
                mSQL = "select PROD_ID from t_pluss_product where PROD_ID=1";
            }
            else if (pname == "CC")
            {
                mSQL = "select PROD_ID from t_pluss_product where PROD_ID=16";
            }
            else
            {
                mSQL = String.Format(@"SELECT PROD_ID FROM t_pluss_product WHERE REPLACE(Upper(PROD_NAME),' ','') LIKE REPLACE(Upper('" + AddSlash(productName) + "%'),' ','') or REPLACE(Upper(PROD_CODE),' ','') LIKE REPLACE(Upper('" + AddSlash(productName) + "%'),' ','') ");
            }

            return Convert.ToInt32(DbQueryManager.ExecuteScalar(mSQL));
        }
        public int SendDedupDeclineInfoInToDB(List<DeDupInfo> deDupInfoObjList, User user)
        {
            int returnValue = 0;
            LogFile.WriteLine("Testing line 987  :: " + "asi");
            try
            {
                
                List<DeDupInfo> insertDeDupInfoObjList = new List<DeDupInfo>();
                List<DeDupInfo> updateDeDupInfoObjList = new List<DeDupInfo>();
                LogFile.WriteLine("Testing line 993  :: " + "asi");
                if (deDupInfoObjList.Count > 0)
                {
                    LogFile.WriteLine("Testing line 996  :: " + deDupInfoObjList.Count);
                    foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
                    {
                        LogFile.WriteLine("Testing line 999  :: " + "asi");
                        DeDupInfo existingDeDupInfoObj = new DeDupInfo();
                        existingDeDupInfoObj.Id = GetDedupId(deDupInfoObj.LoanAccountNo, deDupInfoObj.Status);
                        LogFile.WriteLine("Testing line 998  :: " + existingDeDupInfoObj.Id);
                        if (existingDeDupInfoObj.Id > 0)
                        {
                            LogFile.WriteLine("Testing line 997  :: " + existingDeDupInfoObj.Id);
                            deDupInfoObj.Id = existingDeDupInfoObj.Id;
                            updateDeDupInfoObjList.Add(deDupInfoObj);
                        }
                        else
                        {
                            LogFile.WriteLine("Testing line 1004  :: " + existingDeDupInfoObj.Id);
                            insertDeDupInfoObjList.Add(deDupInfoObj);
                        }
                    }
                    if (insertDeDupInfoObjList.Count > 0)
                    {
                        LogFile.WriteLine("Testing line 1009  :: " + "asi");
                        returnValue += InsertDedupDeclineInfo(insertDeDupInfoObjList, user);
                    }
                    if (updateDeDupInfoObjList.Count > 0)
                    {
                        LogFile.WriteLine("Testing line 1014  :: " + "asi");
                        returnValue += UpdateDedupDeclineInfo(updateDeDupInfoObjList, user);
                        LogFile.WriteLine("Testing line 1016  :: " + returnValue);
                    }

                }
            }


            catch (Exception ex)
            {
                LogFile.WriteLine("Testing line 1029  :: " + ex);
            }

            return returnValue;

        }

        public int SendDedupInfoInToDB(List<DeDupInfo> deDupInfoObjList)
        {
            int returnValue = 0;
            List<DeDupInfo> insertDeDupInfoObjList = new List<DeDupInfo>();
            List<DeDupInfo> updateDeDupInfoObjList = new List<DeDupInfo>();
            if (deDupInfoObjList.Count > 0)
            {
                foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
                {
                    DeDupInfo existingDeDupInfoObj = new DeDupInfo();
                    existingDeDupInfoObj.Id = GetDeclineDedupId(deDupInfoObj.LoanAccountNo, deDupInfoObj.Status);
                    if (existingDeDupInfoObj.Id > 0)
                    {
                        deDupInfoObj.Id = existingDeDupInfoObj.Id;
                        updateDeDupInfoObjList.Add(deDupInfoObj);
                    }
                    else
                    {
                        insertDeDupInfoObjList.Add(deDupInfoObj);
                    }
                }
                if (insertDeDupInfoObjList.Count > 0)
                {
                    returnValue += InsertDedupInfo(insertDeDupInfoObjList);
                }
                if (updateDeDupInfoObjList.Count > 0)
                {
                    returnValue += UpdateDedupInfo(updateDeDupInfoObjList);
                }

            }
            return returnValue;
        }
        private int InsertDedupInfo(List<DeDupInfo> deDupInfoObjList)
        {
            long insertCounter = 0;
            long tempListCount = 0;
            string mSQLString = "";
            string mSQL = "";

            mSQLString = "INSERT INTO de_dup_info (NM, FATHE_NM, MOTHE_NM," +
                         "SCB_MST_NO, DOB, PROFE, PROD_ID," +
                         "DECLI_REASO, DECLI_DT, BUSIN_EMPLO_NM," +
                         "ADD1, ADD2, PH1, PH2, MOB1, MOB2," +
                         "BANK_BRANC, SOURC, STATU, REMAR, " +
                         "TIN, ID_TYPE, DEDU_IDTYPE2, DEDU_IDTYPE3, " +
                         "DEDU_IDTYPE4, DEDU_IDTYPE5, ID_NO, " +
                         "DEDU_IDNO2, DEDU_IDNO3, DEDU_IDNO4," +
                         "DEDU_IDNO5, DEDU_RESIDENTIALSTATUS," +
                         "DEDU_EDUCATIONLEVEL, DEDU_MARITALSTATUS," +
                         "CRITE_TYPE, ENTRY_DT, ACTIV,DEDU_LOANACCOUNTNO) VALUES";
            foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
            {
                System.Threading.Thread.Sleep(10);
                uploadDataCount++;
                System.Threading.Thread.Sleep(10);
                mSQL = mSQL + "('" +
                AddSlash(deDupInfoObj.Name) + "','" +
                AddSlash(deDupInfoObj.FatherName) + "','" +
                AddSlash(deDupInfoObj.MotherName) + "','" +
                AddSlash(deDupInfoObj.ScbMaterNo) + "','" +
                deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" +
                AddSlash(deDupInfoObj.Profession) + "'," +
                deDupInfoObj.ProductId + ",'" +
                AddSlash(deDupInfoObj.DeclineReason) + "','" +
                deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" +
                AddSlash(deDupInfoObj.BusinessEmployeeName) + "','" +
                AddSlash(deDupInfoObj.Address1) + "','" +
                AddSlash(deDupInfoObj.Address2) + "','" +
                AddSlash(deDupInfoObj.Phone1) + "','" +
                AddSlash(deDupInfoObj.Phone2) + "','" +
                AddSlash(deDupInfoObj.Mobile1) + "','" +
                AddSlash(deDupInfoObj.Mobile2) + "','" +
                AddSlash(deDupInfoObj.BankBranch) + "','" +
                AddSlash(deDupInfoObj.Source) + "','" +
                AddSlash(deDupInfoObj.Status) + "','" +
                AddSlash(deDupInfoObj.Remarks) + "','" +
                AddSlash(deDupInfoObj.Tin) + "','" +
                AddSlash(deDupInfoObj.IdType) + "','" +
                AddSlash(deDupInfoObj.IdType1) + "','" +
                AddSlash(deDupInfoObj.IdType2) + "','" +
                AddSlash(deDupInfoObj.IdType3) + "','" +
                AddSlash(deDupInfoObj.IdType4) + "','" +
                AddSlash(deDupInfoObj.IdNo) + "','" +
                AddSlash(deDupInfoObj.IdNo1) + "','" +
                AddSlash(deDupInfoObj.IdNo2) + "','" +
                AddSlash(deDupInfoObj.IdNo3) + "','" +
                AddSlash(deDupInfoObj.IdNo4) + "','" +
                deDupInfoObj.ResidentaileStatus + "','" +
                deDupInfoObj.EducationalLevel + "','" +
                deDupInfoObj.MaritalStatus + "','" +
                deDupInfoObj.CriteriaType + "','" +
                deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" +
                deDupInfoObj.Active + "','" +
                AddSlash(deDupInfoObj.LoanAccountNo) + "'),";

                insertCounter = insertCounter + 1;


                if (insertCounter == 500)
                {
                    mSQL = mSQLString + mSQL;
                    mSQL = mSQL.Substring(0, mSQL.Length - 1);
                    if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                    {
                        tempListCount = tempListCount + insertCounter;
                        insertCounter = 0;
                        mSQL = "";
                    }
                    else
                    {
                        tempListCount = tempListCount + insertCounter;
                        insertCounter = 0;
                        mSQL = "";
                    }
                }
            } //end foreach

            if (insertCounter < 500)
            {
                mSQL = mSQLString + mSQL;
                mSQL = mSQL.Substring(0, mSQL.Length - 1);
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    tempListCount = tempListCount + insertCounter;
                    insertCounter = 0;
                    mSQL = "";
                }
                else
                {
                    tempListCount = tempListCount + insertCounter;
                    insertCounter = 0;
                    mSQL = "";
                }
            }
            if (tempListCount == deDupInfoObjList.Count)
            {
                tempListCount = 0;
                return INSERTE;
            }
            else
            {
                return ERROR;
            }
        }

        private int InsertDedupDeclineInfo(List<DeDupInfo> deDupInfoObjList, User user)
        {
            long insertCounter = 0;
            long tempListCount = 0;
            string mSQLString = "";
            string mSQL = "";

            mSQLString = "INSERT INTO de_dup_info_decline_temp (NM, FATHE_NM, MOTHE_NM," +
                         "SCB_MST_NO, DOB, PROFE, PROD_ID," +
                         "DECLI_REASO, DECLI_DT, BUSIN_EMPLO_NM," +
                         "ADD1, ADD2, PH1, PH2, MOB1, MOB2," +
                         "BANK_BRANC, SOURC, STATU, REMAR, " +
                         "TIN, ID_TYPE, DEDU_IDTYPE2, DEDU_IDTYPE3, " +
                         "DEDU_IDTYPE4, DEDU_IDTYPE5, ID_NO, " +
                         "DEDU_IDNO2, DEDU_IDNO3, DEDU_IDNO4," +
                         "DEDU_IDNO5, DEDU_RESIDENTIALSTATUS," +
                         "DEDU_EDUCATIONLEVEL, DEDU_MARITALSTATUS," +
                         //"CRITE_TYPE, ENTRY_DT, ACTIV,DEDU_LOANACCOUNTNO, STATU, MAKER_PS_ID, MAKE_DATE) VALUES";
                         "CRITE_TYPE, ENTRY_DT, ACTIV,DEDU_LOANACCOUNTNO,MultiLocatorRemarksID, MAKER_PS_ID, MAKE_DATE) VALUES";
            foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
            {
                System.Threading.Thread.Sleep(10);
                uploadDataCount++;
                System.Threading.Thread.Sleep(10);
                mSQL = mSQL + "('" +
                //deDupInfo.Id + "','" +
                //deDupInfoObj.Id + "','" +
                AddSlash(deDupInfoObj.Name) + "','" +
                AddSlash(deDupInfoObj.FatherName) + "','" +
                AddSlash(deDupInfoObj.MotherName) + "','" +
                AddSlash(deDupInfoObj.ScbMaterNo) + "','" +
                deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" +
                AddSlash(deDupInfoObj.Profession) + "'," +
                deDupInfoObj.ProductId + ",'" +
                AddSlash(deDupInfoObj.DeclineReason) + "','" +
                deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" +
                AddSlash(deDupInfoObj.BusinessEmployeeName) + "','" +
                AddSlash(deDupInfoObj.Address1) + "','" +
                AddSlash(deDupInfoObj.Address2) + "','" +
                AddSlash(deDupInfoObj.Phone1) + "','" +
                AddSlash(deDupInfoObj.Phone2) + "','" +
                AddSlash(deDupInfoObj.Mobile1) + "','" +
                AddSlash(deDupInfoObj.Mobile2) + "','" +
                AddSlash(deDupInfoObj.BankBranch) + "','" +
                AddSlash(deDupInfoObj.Source) + "','" +
                AddSlash(deDupInfoObj.Status) + "','" +
                AddSlash(deDupInfoObj.Remarks) + "','" +
                AddSlash(deDupInfoObj.Tin) + "','" +
                AddSlash(deDupInfoObj.IdType) + "','" +
                AddSlash(deDupInfoObj.IdType1) + "','" +
                AddSlash(deDupInfoObj.IdType2) + "','" +
                AddSlash(deDupInfoObj.IdType3) + "','" +
                AddSlash(deDupInfoObj.IdType4) + "','" +
                AddSlash(deDupInfoObj.IdNo) + "','" +
                AddSlash(deDupInfoObj.IdNo1) + "','" +
                AddSlash(deDupInfoObj.IdNo2) + "','" +
                AddSlash(deDupInfoObj.IdNo3) + "','" +
                AddSlash(deDupInfoObj.IdNo4) + "','" +
                deDupInfoObj.ResidentaileStatus + "','" +
                deDupInfoObj.EducationalLevel + "','" +
                deDupInfoObj.MaritalStatus + "','" +
                deDupInfoObj.CriteriaType + "','" +
                deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" +
                deDupInfoObj.Active + "','" +
                AddSlash(deDupInfoObj.LoanAccountNo) + "','" +
                AddSlash(deDupInfoObj.MultiLocatorRemarksID) + "','" +
                //"'"+ deDupInfo.Status + "'" +
                //deDupInfoObj.Status + "','" +
                user.UserId + "','" +
                //"" + user.UserId + "" +
                "" + DateTime.Now.ToString("yyyy-MM-dd") + "'),";

                insertCounter = insertCounter + 1;


                if (insertCounter == 500)
                {
                    mSQL = mSQLString + mSQL;
                    mSQL = mSQL.Substring(0, mSQL.Length - 1);
                    if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                    {
                        tempListCount = tempListCount + insertCounter;
                        insertCounter = 0;
                        mSQL = "";
                    }
                    else
                    {
                        tempListCount = tempListCount + insertCounter;
                        insertCounter = 0;
                        mSQL = "";
                    }
                }
            } //end foreach

            if (insertCounter < 500)
            {
                mSQL = mSQLString + mSQL;
                mSQL = mSQL.Substring(0, mSQL.Length - 1);
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {

                    tempListCount = tempListCount + insertCounter;
                    insertCounter = 0;
                    mSQL = "";
                    LogFile.WriteLine("Testing line 1267  :: " + mSQL);
                }
                else
                {
                    tempListCount = tempListCount + insertCounter;
                    insertCounter = 0;
                    mSQL = "";
                }
            }
            if (tempListCount == deDupInfoObjList.Count)
            {
                tempListCount = 0;
                return INSERTE;
            }
            else
            {
                return ERROR;
            }
        }
        public DataTable GetDeDupDeclinedData()
        {
            try
            {
                string sql = "SELECT * FROM de_dup_info_decline_temp";

                return DbQueryManager.GetDataTable(sql);
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                return null;
            }
        }
        private int UpdateDedupInfo(List<DeDupInfo> deDupInfoObjList)
        {
            long updateCounter = 0;
            long tempListCount = 0;
            string mSQL = "";
            foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
            {

                mSQL = mSQL + "UPDATE de_dup_info SET " +
                      "NM='" + AddSlash(deDupInfoObj.Name) + "'," +
                      "FATHE_NM='" + AddSlash(deDupInfoObj.FatherName) + "'," +
                      "MOTHE_NM='" + AddSlash(deDupInfoObj.MotherName) + "'," +
                      "SCB_MST_NO='" + AddSlash(deDupInfoObj.ScbMaterNo) + "'," +
                      "DOB='" + deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "'," +
                      "PROFE='" + AddSlash(deDupInfoObj.Profession) + "'," +
                      "PROD_ID=" + deDupInfoObj.ProductId + "," +
                      "DECLI_REASO='" + AddSlash(deDupInfoObj.DeclineReason) + "'," +
                      "DECLI_DT='" + deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "'," +
                      "BUSIN_EMPLO_NM='" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "'," +
                      "ADD1='" + AddSlash(deDupInfoObj.Address1) + "'," +
                      "ADD2='" + AddSlash(deDupInfoObj.Address2) + "'," +
                      "PH1='" + AddSlash(deDupInfoObj.Phone1) + "'," +
                      "PH2='" + AddSlash(deDupInfoObj.Phone2) + "'," +
                      "MOB1='" + AddSlash(deDupInfoObj.Mobile1) + "'," +
                      "MOB2='" + AddSlash(deDupInfoObj.Mobile2) + "'," +
                      "BANK_BRANC='" + AddSlash(deDupInfoObj.BankBranch) + "'," +
                      "SOURC='" + AddSlash(deDupInfoObj.Source) + "'," +
                      "STATU='" + deDupInfoObj.Status + "'," +
                      "REMAR='" + AddSlash(deDupInfoObj.Remarks) + "'," +
                      "TIN='" + AddSlash(deDupInfoObj.Tin) + "'," +
                      "ID_TYPE='" + AddSlash(deDupInfoObj.IdType) + "'," +
                      "DEDU_IDTYPE2='" + AddSlash(deDupInfoObj.IdType1) + "'," +
                      "DEDU_IDTYPE3='" + AddSlash(deDupInfoObj.IdType2) + "'," +
                      "DEDU_IDTYPE4='" + AddSlash(deDupInfoObj.IdType3) + "'," +
                      "DEDU_IDTYPE5='" + AddSlash(deDupInfoObj.IdType4) + "'," +
                      "ID_NO='" + AddSlash(deDupInfoObj.IdNo) + "'," +
                      "DEDU_IDNO2='" + AddSlash(deDupInfoObj.IdNo1) + "'," +
                      "DEDU_IDNO3='" + AddSlash(deDupInfoObj.IdNo2) + "'," +
                      "DEDU_IDNO4='" + AddSlash(deDupInfoObj.IdNo3) + "'," +
                      "DEDU_IDNO5='" + AddSlash(deDupInfoObj.IdNo4) + "'," +
                      "DEDU_RESIDENTIALSTATUS='" + deDupInfoObj.ResidentaileStatus + "'," +
                      "DEDU_EDUCATIONLEVEL='" + deDupInfoObj.EducationalLevel + "'," +
                      "DEDU_MARITALSTATUS='" + deDupInfoObj.MaritalStatus + "'," +
                      "CRITE_TYPE='" + deDupInfoObj.CriteriaType + "'," +
                      "ENTRY_DT='" + deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                      "ACTIV='" + deDupInfoObj.Active + "'," +
                      "DEDU_LOANACCOUNTNO='" + AddSlash(deDupInfoObj.LoanAccountNo) + "' WHERE DE_DUP_ID=" + deDupInfoObj.Id;// +";";

                updateCounter = updateCounter + 1;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    tempListCount = tempListCount + 1;
                    mSQL = "";
                }
                else
                {
                    mSQL = "";
                }
                //if (updateCounter == 500)
                //{
                //    mSQL = mSQL.Substring(0, mSQL.Length - 1);
                //    if (dbMySQL.DbExecute(mSQL) > -1)
                //    {
                //        tempListCount = tempListCount + updateCounter;
                //        updateCounter = 0;
                //        mSQL = "";
                //    }
                //    else
                //    {
                //        tempListCount = tempListCount + updateCounter;
                //        updateCounter = 0;
                //        mSQL = "";
                //    }
                //}

            } //end foreach

            //if (updateCounter < 500)
            //{
            //    mSQL = mSQL.Substring(0, mSQL.Length - 1);
            //    if (dbMySQL.DbExecute(mSQL) > -1)
            //    {
            //        tempListCount = tempListCount + updateCounter;
            //        updateCounter = 0;
            //        mSQL = "";
            //    }
            //    else
            //    {
            //        tempListCount = tempListCount + updateCounter;
            //        updateCounter = 0;
            //        mSQL = "";
            //    }
            //}
            //if (tempListCount == deDupInfoObjList.Count)
            if (tempListCount > 0)
            {
                tempListCount = 0;
                return INSERTE;
            }
            else
            {
                return ERROR;
            }

        }
        private int UpdateDedupDeclineInfo(List<DeDupInfo> deDupInfoObjList, User user)
        {
            long updateCounter = 0;
            long tempListCount = 0;
            string mSQL = "";
            LogFile.WriteLine("Testing line 1414  :: " + "asi");
            foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
            {

                mSQL = mSQL + "UPDATE de_dup_info_decline_temp SET " +
                      "NM='" + AddSlash(deDupInfoObj.Name) + "'," +
                      "FATHE_NM='" + AddSlash(deDupInfoObj.FatherName) + "'," +
                      "MOTHE_NM='" + AddSlash(deDupInfoObj.MotherName) + "'," +
                      "SCB_MST_NO='" + AddSlash(deDupInfoObj.ScbMaterNo) + "'," +
                      "DOB='" + deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "'," +
                      "PROFE='" + AddSlash(deDupInfoObj.Profession) + "'," +
                      "PROD_ID=" + deDupInfoObj.ProductId + "," +
                      "DECLI_REASO='" + AddSlash(deDupInfoObj.DeclineReason) + "'," +
                      "DECLI_DT='" + deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "'," +
                      "BUSIN_EMPLO_NM='" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "'," +
                      "ADD1='" + AddSlash(deDupInfoObj.Address1) + "'," +
                      "ADD2='" + AddSlash(deDupInfoObj.Address2) + "'," +
                      "PH1='" + AddSlash(deDupInfoObj.Phone1) + "'," +
                      "PH2='" + AddSlash(deDupInfoObj.Phone2) + "'," +
                      "MOB1='" + AddSlash(deDupInfoObj.Mobile1) + "'," +
                      "MOB2='" + AddSlash(deDupInfoObj.Mobile2) + "'," +
                      "BANK_BRANC='" + AddSlash(deDupInfoObj.BankBranch) + "'," +
                      "SOURC='" + AddSlash(deDupInfoObj.Source) + "'," +
                      "STATU='" + deDupInfoObj.Status + "'," +
                      "REMAR='" + AddSlash(deDupInfoObj.Remarks) + "'," +
                      "TIN='" + AddSlash(deDupInfoObj.Tin) + "'," +
                      "ID_TYPE='" + AddSlash(deDupInfoObj.IdType) + "'," +
                      "DEDU_IDTYPE2='" + AddSlash(deDupInfoObj.IdType1) + "'," +
                      "DEDU_IDTYPE3='" + AddSlash(deDupInfoObj.IdType2) + "'," +
                      "DEDU_IDTYPE4='" + AddSlash(deDupInfoObj.IdType3) + "'," +
                      "DEDU_IDTYPE5='" + AddSlash(deDupInfoObj.IdType4) + "'," +
                      "ID_NO='" + AddSlash(deDupInfoObj.IdNo) + "'," +
                      "DEDU_IDNO2='" + AddSlash(deDupInfoObj.IdNo1) + "'," +
                      "DEDU_IDNO3='" + AddSlash(deDupInfoObj.IdNo2) + "'," +
                      "DEDU_IDNO4='" + AddSlash(deDupInfoObj.IdNo3) + "'," +
                      "DEDU_IDNO5='" + AddSlash(deDupInfoObj.IdNo4) + "'," +
                      "DEDU_RESIDENTIALSTATUS='" + deDupInfoObj.ResidentaileStatus + "'," +
                      "DEDU_EDUCATIONLEVEL='" + deDupInfoObj.EducationalLevel + "'," +
                      "DEDU_MARITALSTATUS='" + deDupInfoObj.MaritalStatus + "'," +
                      "CRITE_TYPE='" + deDupInfoObj.CriteriaType + "'," +
                      "ENTRY_DT='" + deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                      "ACTIV='" + deDupInfoObj.Active + "'," +
                      "MAKER_PS_ID='" + user.Id + "'," +
                      "MAKE_DATE='" + DateTime.Now.ToString("yyyy-MM-dd") + "'," +
                      "MultiLocatorRemarksID='" + AddSlash(deDupInfoObj.MultiLocatorRemarksID) + "'," +
                      "DEDU_LOANACCOUNTNO='" + AddSlash(deDupInfoObj.LoanAccountNo) + "' WHERE DE_DUP_ID=" + deDupInfoObj.Id;// +";";

                updateCounter = updateCounter + 1;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    tempListCount = tempListCount + 1;
                    mSQL = "";
                }
                else
                {
                    mSQL = "";
                }
                //if (updateCounter == 500)
                //{
                //    mSQL = mSQL.Substring(0, mSQL.Length - 1);
                //    if (dbMySQL.DbExecute(mSQL) > -1)
                //    {
                //        tempListCount = tempListCount + updateCounter;
                //        updateCounter = 0;
                //        mSQL = "";
                //    }
                //    else
                //    {
                //        tempListCount = tempListCount + updateCounter;
                //        updateCounter = 0;
                //        mSQL = "";
                //    }
                //}

            } //end foreach

            //if (updateCounter < 500)
            //{
            //    mSQL = mSQL.Substring(0, mSQL.Length - 1);
            //    if (dbMySQL.DbExecute(mSQL) > -1)
            //    {
            //        tempListCount = tempListCount + updateCounter;
            //        updateCounter = 0;
            //        mSQL = "";
            //    }
            //    else
            //    {
            //        tempListCount = tempListCount + updateCounter;
            //        updateCounter = 0;
            //        mSQL = "";
            //    }
            //}
            //if (tempListCount == deDupInfoObjList.Count)
            if (tempListCount > 0)
            {
                tempListCount = 0;
                return INSERTE;
                LogFile.WriteLine("Testing line 1510  :: " + mSQL);

            }
            else
            {
                return ERROR;
            }

        }


        public List<DeDupInfo> GetUploadResult(DateTime startDate, DateTime endDate, string type, Int32 offset, Int32 rowsPerPage)
        {
            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            DeDupInfo deDupInfoObj = null;
            string query = "SELECT * FROM de_dup_info WHERE ENTRY_DT BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "' AND RTRIM(LTRIM(REPLACE(Upper(STATU),' ','')))='" + type.ToUpper().Trim() + "' order by DE_DUP_ID OFFSET  " + offset + " ROWS FETCH NEXT " + rowsPerPage + " ROWS ONLY";
            try
            {
                DbDataReader deDupInfoDataReader = DbQueryManager.ExecuteReader(query);
                while (deDupInfoDataReader.Read())
                {
                    deDupInfoObj = new DeDupInfo();
                    deDupInfoObj.Id = Convert.ToInt32(deDupInfoDataReader["DE_DUP_ID"]);
                    deDupInfoObj.ApplicantId.Id1 = Convert.ToString(deDupInfoDataReader["ID_NO"]);
                    deDupInfoObj.ApplicantId.Id2 = Convert.ToString(deDupInfoDataReader["DEDU_IDNO2"]);
                    deDupInfoObj.ApplicantId.Id3 = Convert.ToString(deDupInfoDataReader["DEDU_IDNO3"]);
                    deDupInfoObj.ApplicantId.Id4 = Convert.ToString(deDupInfoDataReader["DEDU_IDNO4"]);
                    deDupInfoObj.ApplicantId.Id5 = Convert.ToString(deDupInfoDataReader["DEDU_IDNO5"]);
                    deDupInfoObj.ApplicantId.Type1 = Convert.ToString(deDupInfoDataReader["ID_TYPE"]);
                    deDupInfoObj.ApplicantId.Type2 = Convert.ToString(deDupInfoDataReader["DEDU_IDTYPE2"]);
                    deDupInfoObj.ApplicantId.Type3 = Convert.ToString(deDupInfoDataReader["DEDU_IDTYPE3"]);
                    deDupInfoObj.ApplicantId.Type4 = Convert.ToString(deDupInfoDataReader["DEDU_IDTYPE4"]);
                    deDupInfoObj.ApplicantId.Type5 = Convert.ToString(deDupInfoDataReader["DEDU_IDTYPE5"]);
                    if (String.IsNullOrEmpty(deDupInfoDataReader["SCB_MST_NO"].ToString()))
                    {
                        deDupInfoObj.AccountNo.MasterAccNo1 = "";
                    }
                    else
                    {
                        string accountNo1 = Convert.ToString(deDupInfoDataReader["SCB_MST_NO"]);
                        deDupInfoObj.AccountNo.MasterAccNo1 = accountNo1;
                    }

                    deDupInfoObj.ApplicantId.Type1 = deDupInfoDataReader["ID_TYPE"].ToString();
                    deDupInfoObj.ApplicantId.Id1 = deDupInfoDataReader["ID_NO"].ToString();
                    deDupInfoObj.ApplicantId.Type2 = deDupInfoDataReader["DEDU_IDTYPE2"].ToString();
                    deDupInfoObj.ApplicantId.Id2 = deDupInfoDataReader["DEDU_IDNO2"].ToString();
                    deDupInfoObj.ApplicantId.Type3 = deDupInfoDataReader["DEDU_IDTYPE3"].ToString();
                    deDupInfoObj.ApplicantId.Id3 = deDupInfoDataReader["DEDU_IDNO3"].ToString();
                    deDupInfoObj.ApplicantId.Type4 = deDupInfoDataReader["DEDU_IDTYPE4"].ToString();
                    deDupInfoObj.ApplicantId.Id4 = deDupInfoDataReader["DEDU_IDNO4"].ToString();
                    deDupInfoObj.ApplicantId.Type5 = deDupInfoDataReader["DEDU_IDTYPE5"].ToString();
                    deDupInfoObj.ApplicantId.Id5 = deDupInfoDataReader["DEDU_IDNO5"].ToString();
                    deDupInfoObj.ContactNumber.ContactNo1 = Convert.ToString(deDupInfoDataReader["MOB1"]);
                    deDupInfoObj.ContactNumber.ContactNo2 = Convert.ToString(deDupInfoDataReader["MOB2"]);
                    deDupInfoObj.ContactNumber.ContactNo3 = Convert.ToString(deDupInfoDataReader["PH1"]);
                    deDupInfoObj.ContactNumber.ContactNo4 = Convert.ToString(deDupInfoDataReader["PH2"]);
                    deDupInfoObj.Name = Convert.ToString(deDupInfoDataReader["NM"]);
                    deDupInfoObj.FatherName = Convert.ToString(deDupInfoDataReader["FATHE_NM"]);
                    deDupInfoObj.MotherName = Convert.ToString(deDupInfoDataReader["MOTHE_NM"]);
                    deDupInfoObj.DateOfBirth = Convert.ToDateTime(deDupInfoDataReader["DOB"]);
                    deDupInfoObj.Profession = Convert.ToString(deDupInfoDataReader["PROFE"]);
                    deDupInfoObj.ProductId = Convert.ToInt32(deDupInfoDataReader["PROD_ID"]);
                    deDupInfoObj.DeclineReason = Convert.ToString(deDupInfoDataReader["DECLI_REASO"]);
                    deDupInfoObj.DeclineDate = Convert.ToDateTime(deDupInfoDataReader["DECLI_DT"]);
                    deDupInfoObj.BusinessEmployeeName = Convert.ToString(deDupInfoDataReader["BUSIN_EMPLO_NM"]);
                    deDupInfoObj.Address1 = Convert.ToString(deDupInfoDataReader["ADD1"]);
                    deDupInfoObj.Address2 = Convert.ToString(deDupInfoDataReader["ADD2"]);
                    deDupInfoObj.Source = Convert.ToString(deDupInfoDataReader["SOURC"]);
                    deDupInfoObj.Status = Convert.ToString(deDupInfoDataReader["STATU"]);
                    deDupInfoObj.Remarks = Convert.ToString(deDupInfoDataReader["REMAR"]);
                    deDupInfoObj.Tin = Convert.ToString(deDupInfoDataReader["TIN"]);
                    deDupInfoObj.CriteriaType = Convert.ToString(deDupInfoDataReader["CRITE_TYPE"]);
                    deDupInfoObj.Active = Convert.ToString(deDupInfoDataReader["ACTIV"]);
                    deDupInfoObj.EducationalLevel = Convert.ToString(deDupInfoDataReader["DEDU_EDUCATIONLEVEL"]);
                    deDupInfoObj.MaritalStatus = Convert.ToString(deDupInfoDataReader["DEDU_MARITALSTATUS"]);
                    deDupInfoObj.ResidentaileStatus = Convert.ToString(deDupInfoDataReader["DEDU_RESIDENTIALSTATUS"]);
                    deDupInfoObj.LoanAccountNo = Convert.ToString(deDupInfoDataReader["DEDU_LOANACCOUNTNO"]);
                    deDupInfoObjList.Add(deDupInfoObj);
                }

                deDupInfoDataReader.Close();
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;
            }
            return deDupInfoObjList;
        }
        public Int32 GetTolalRecord(DateTime startDate, DateTime endDate, string type)
        {
            Int32 returnValue = 0;
            string mSQL = "SELECT COUNT(*) as TotalRecord FROM de_dup_info WHERE ENTRY_DT BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "' AND LTRIM(RTRIM(REPLACE(Upper(STATU),' ','')))='" + type.ToUpper().Trim() + "'";
            returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }
        public string GetLoanLocatorUserId(string userId)
        {
            string mSQL = "SELECT ID_LEARNER FROM learner_details_table WHERE USERNAME='" + userId.Trim() + "'";
            // string lonaLocatorUserId = dbMySQL.DbGetValue(mSQL, "conStringLoanLocator");
            string lonaLocatorUserId = DbQueryManager.ExecuteScalar(mSQL).ToString();
            return lonaLocatorUserId;
        }

        public string DeleteDeclineTemp(string DE_DUP_ID)
        {
            string mSQL = "DELETE de_dup_info_decline_temp WHERE DE_DUP_ID ='" + DE_DUP_ID.Trim() + "'";
            string deleteDeclineTempId = DbQueryManager.ExecuteNonQuery(mSQL).ToString();
            return deleteDeclineTempId;
        }

        public void DeleteTempData(string status)
        {
            string mSQL = "DELETE FROM t_pluss_dedupetemp WHERE STATU='" + status.Trim() + "'";
            DbQueryManager.ExecuteNonQuery(mSQL);
        }
        public int SendDedupeInToDB(List<DeDupInfo> deDupInfoObjList)
        {
            long insertCounter = 0;
            long tempListCount = 0;
            string mSQLString = "";
            string mSQL = "";

            mSQLString = "INSERT INTO t_pluss_dedupetemp (NM, FATHE_NM, MOTHE_NM," +
                         "SCB_MST_NO, DOB, PROFE, PROD_ID," +
                         "DECLI_REASO, DECLI_DT, BUSIN_EMPLO_NM," +
                         "ADD1, ADD2, PH1, PH2, MOB1, MOB2," +
                         "BANK_BRANC, SOURC, STATU, REMAR, " +
                         "TIN, ID_TYPE, DEDU_IDTYPE2, DEDU_IDTYPE3, " +
                         "DEDU_IDTYPE4, DEDU_IDTYPE5, ID_NO, " +
                         "DEDU_IDNO2, DEDU_IDNO3, DEDU_IDNO4," +
                         "DEDU_IDNO5, DEDU_RESIDENTIALSTATUS," +
                         "DEDU_EDUCATIONLEVEL, DEDU_MARITALSTATUS," +
                         "CRITE_TYPE, ENTRY_DT, ACTIV,DEDU_LOANACCOUNTNO) ";
            #region  test
            //uploadDataCount = 0;
            //foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
            //{
            //    System.Threading.Thread.Sleep(10);
            //    uploadDataCount++;
            //    System.Threading.Thread.Sleep(10);
            //    var prof = deDupInfoObj.Profession ?? "";
            //    decimal tin = 0;
            //    if (deDupInfoObj.MfuTin > 0)
            //    {
            //        tin = deDupInfoObj.MfuTin;
            //    }
            //    else
            //    {
            //        tin = Convert.ToDecimal('0' + deDupInfoObj.Tin);
            //    }

            //    mSQL = mSQL + "('" + AddSlash(deDupInfoObj.Name) + "','" + AddSlash(deDupInfoObj.FatherName) + "','" + AddSlash(deDupInfoObj.MotherName) + "','" +
            //          deDupInfoObj.ScbMaterNo + "','" + deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" + prof + "'," + deDupInfoObj.ProductId + ",'" +
            //          AddSlash(deDupInfoObj.DeclineReason) + "','" + deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "','" +
            //          AddSlash(deDupInfoObj.Address1) + "','" + AddSlash(deDupInfoObj.Address2) + "','" + deDupInfoObj.Phone1 + "','" + deDupInfoObj.Phone2 + "','" +
            //          deDupInfoObj.Mobile1 + "','" + deDupInfoObj.Mobile2 + "','" + AddSlash(deDupInfoObj.BankBranch) + "','" + deDupInfoObj.Source + "','" +
            //          deDupInfoObj.Status + "','" + AddSlash(deDupInfoObj.Remarks) + "'," + tin + ",'" + deDupInfoObj.IdType + "','" +
            //          deDupInfoObj.IdType1 + "','" + deDupInfoObj.IdType2 + "','" + deDupInfoObj.IdType3 + "','" + deDupInfoObj.IdType4 + "','" + AddSlash(deDupInfoObj.IdNo) + "','" +
            //          AddSlash(deDupInfoObj.IdNo1) + "','" + AddSlash(deDupInfoObj.IdNo2) + "','" + AddSlash(deDupInfoObj.IdNo3) + "','" + AddSlash(deDupInfoObj.IdNo4) + "','" +
            //          deDupInfoObj.ResidentaileStatus + "','" + deDupInfoObj.EducationalLevel + "','" + deDupInfoObj.MaritalStatus + "','" + deDupInfoObj.CriteriaType + "','" +
            //          deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" + deDupInfoObj.Active + "','" + deDupInfoObj.LoanAccountNo + "'),";

            //    insertCounter = insertCounter + 1;
            //}
            //mSQL = mSQLString + mSQL;
            //mSQL = mSQL.Substring(0, mSQL.Length - 1);
            //mSQL = mSQL + ";";
            #endregion

            uploadDataCount = 0;

            int repeat = deDupInfoObjList.Count / 2000;
            int remainder = deDupInfoObjList.Count % 2000;

            for (int i = 0; i < repeat; i++)
            {
                mSQL = string.Empty;
                for (int k = i * 2000; k < ((i + 1) * 2000); k++)
                {
                    DeDupInfo deDupInfoObj = deDupInfoObjList[k];
                    var prof = deDupInfoObj.Profession ?? "";
                    if (!string.IsNullOrEmpty(mSQL))
                    {
                        mSQL += " union all ";
                    }

                    mSQL += " select '" + AddSlash(deDupInfoObj.Name) + "','" + AddSlash(deDupInfoObj.FatherName) + "','" + AddSlash(deDupInfoObj.MotherName) + "','" +
                          deDupInfoObj.ScbMaterNo + "','" + deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" + prof + "'," + deDupInfoObj.ProductId + ",'" +
                          AddSlash(deDupInfoObj.DeclineReason) + "','" + deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "','" +
                          AddSlash(deDupInfoObj.Address1) + "','" + AddSlash(deDupInfoObj.Address2) + "','" + deDupInfoObj.Phone1 + "','" + deDupInfoObj.Phone2 + "','" +
                          deDupInfoObj.Mobile1 + "','" + deDupInfoObj.Mobile2 + "','" + AddSlash(deDupInfoObj.BankBranch) + "','" + deDupInfoObj.Source + "','" +
                          deDupInfoObj.Status + "','" + AddSlash(deDupInfoObj.Remarks) + "'," + deDupInfoObj.Tin + ",'" + deDupInfoObj.IdType + "','" +
                          deDupInfoObj.IdType1 + "','" + deDupInfoObj.IdType2 + "','" + deDupInfoObj.IdType3 + "','" + deDupInfoObj.IdType4 + "','" + AddSlash(deDupInfoObj.IdNo) + "','" +
                          AddSlash(deDupInfoObj.IdNo1) + "','" + AddSlash(deDupInfoObj.IdNo2) + "','" + AddSlash(deDupInfoObj.IdNo3) + "','" + AddSlash(deDupInfoObj.IdNo4) + "','" +
                          deDupInfoObj.ResidentaileStatus + "','" + deDupInfoObj.EducationalLevel + "','" + deDupInfoObj.MaritalStatus + "','" + deDupInfoObj.CriteriaType + "','" +
                          deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" + deDupInfoObj.Active + "','" + deDupInfoObj.LoanAccountNo + "'";
                }
                mSQL = mSQLString + mSQL;
                DbQueryManager.ExecuteNonQuery(mSQL);


            }

            mSQL = string.Empty;
            for (int k = 0; k < remainder; k++)
            {
                DeDupInfo deDupInfoObj = deDupInfoObjList[k];
                var prof = deDupInfoObj.Profession ?? "";
                if (!string.IsNullOrEmpty(mSQL))
                {
                    mSQL += " union all ";
                }

                mSQL += " select '" + AddSlash(deDupInfoObj.Name) + "','" + AddSlash(deDupInfoObj.FatherName) + "','" + AddSlash(deDupInfoObj.MotherName) + "','" +
                      deDupInfoObj.ScbMaterNo + "','" + deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" + prof + "'," + deDupInfoObj.ProductId + ",'" +
                      AddSlash(deDupInfoObj.DeclineReason) + "','" + deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "','" +
                      AddSlash(deDupInfoObj.Address1) + "','" + AddSlash(deDupInfoObj.Address2) + "','" + deDupInfoObj.Phone1 + "','" + deDupInfoObj.Phone2 + "','" +
                      deDupInfoObj.Mobile1 + "','" + deDupInfoObj.Mobile2 + "','" + AddSlash(deDupInfoObj.BankBranch) + "','" + deDupInfoObj.Source + "','" +
                      deDupInfoObj.Status + "','" + AddSlash(deDupInfoObj.Remarks) + "'," + deDupInfo.MfuTin + ",'" + deDupInfoObj.IdType + "','" +
                      deDupInfoObj.IdType1 + "','" + deDupInfoObj.IdType2 + "','" + deDupInfoObj.IdType3 + "','" + deDupInfoObj.IdType4 + "','" + AddSlash(deDupInfoObj.IdNo) + "','" +
                      AddSlash(deDupInfoObj.IdNo1) + "','" + AddSlash(deDupInfoObj.IdNo2) + "','" + AddSlash(deDupInfoObj.IdNo3) + "','" + AddSlash(deDupInfoObj.IdNo4) + "','" +
                      deDupInfoObj.ResidentaileStatus + "','" + deDupInfoObj.EducationalLevel + "','" + deDupInfoObj.MaritalStatus + "','" + deDupInfoObj.CriteriaType + "','" +
                      deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" + deDupInfoObj.Active + "','" + deDupInfoObj.LoanAccountNo + "'";
            }
            mSQL = mSQLString + mSQL;
            DbQueryManager.ExecuteNonQuery(mSQL);

            return deDupInfoObjList.Count;

            //    foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
            //    {
            //        var prof = deDupInfoObj.Profession ?? "";
            //        decimal tin = 0;
            //        if (deDupInfoObj.MfuTin > 0)
            //        {
            //            tin = deDupInfoObj.MfuTin;
            //        }
            //        else
            //        {
            //            tin = Convert.ToDecimal('0' + deDupInfoObj.Tin);
            //        }
            //        if (!string.IsNullOrEmpty(mSQL))
            //        {
            //            mSQL += " union all ";
            //        }

            //        mSQL += " select '" + AddSlash(deDupInfoObj.Name) + "','" + AddSlash(deDupInfoObj.FatherName) + "','" + AddSlash(deDupInfoObj.MotherName) + "','" +
            //              deDupInfoObj.ScbMaterNo + "','" + deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" + prof + "'," + deDupInfoObj.ProductId + ",'" +
            //              AddSlash(deDupInfoObj.DeclineReason) + "','" + deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "','" +
            //              AddSlash(deDupInfoObj.Address1) + "','" + AddSlash(deDupInfoObj.Address2) + "','" + deDupInfoObj.Phone1 + "','" + deDupInfoObj.Phone2 + "','" +
            //              deDupInfoObj.Mobile1 + "','" + deDupInfoObj.Mobile2 + "','" + AddSlash(deDupInfoObj.BankBranch) + "','" + deDupInfoObj.Source + "','" +
            //              deDupInfoObj.Status + "','" + AddSlash(deDupInfoObj.Remarks) + "'," + tin + ",'" + deDupInfoObj.IdType + "','" +
            //              deDupInfoObj.IdType1 + "','" + deDupInfoObj.IdType2 + "','" + deDupInfoObj.IdType3 + "','" + deDupInfoObj.IdType4 + "','" + AddSlash(deDupInfoObj.IdNo) + "','" +
            //              AddSlash(deDupInfoObj.IdNo1) + "','" + AddSlash(deDupInfoObj.IdNo2) + "','" + AddSlash(deDupInfoObj.IdNo3) + "','" + AddSlash(deDupInfoObj.IdNo4) + "','" +
            //              deDupInfoObj.ResidentaileStatus + "','" + deDupInfoObj.EducationalLevel + "','" + deDupInfoObj.MaritalStatus + "','" + deDupInfoObj.CriteriaType + "','" +
            //              deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" + deDupInfoObj.Active + "','" + deDupInfoObj.LoanAccountNo + "'";


            //        insertCounter = insertCounter + 1;
            //    }
            //mSQL = mSQLString + mSQL;
            ////mSQL = mSQL.Substring(0, mSQL.Length - 1);
            ////mSQL = mSQL + ";";

            //return DbQueryManager.ExecuteNonQuery(mSQL);

        }
        public string SendUploadDataToDB(DataTable uploadDat, string status)
        {
            string returnString = string.Empty;
            long insertCounter = 0;
            long tempListCount = 0;
            string mSQLString = "";
            string mSQL = "";

            DeleteData();

            mSQLString = "INSERT INTO t_pluss_dedupeupload (NM, FATHE_NM, MOTHE_NM," +
                         "SCB_MST_NO, DOB, PROFE, PROD_ID," +
                         "DECLI_REASO, DECLI_DT, BUSIN_EMPLO_NM," +
                         "ADD1, ADD2, PH1, PH2, MOB1, MOB2," +
                         "BANK_BRANC, SOURC, STATU, REMAR, " +
                         "TIN, ID_TYPE, DEDU_IDTYPE2, DEDU_IDTYPE3, " +
                         "DEDU_IDTYPE4, DEDU_IDTYPE5, ID_NO, " +
                         "DEDU_IDNO2, DEDU_IDNO3, DEDU_IDNO4," +
                         "DEDU_IDNO5, DEDU_RESIDENTIALSTATUS," +
                         "DEDU_EDUCATIONLEVEL, DEDU_MARITALSTATUS," +
                         "CRITE_TYPE, ENTRY_DT, ACTIV,DEDU_LOANACCOUNTNO) VALUES";

            foreach (DataRow dr in uploadDat.Rows)
            {
                System.Threading.Thread.Sleep(10);
                uploadDataCount++;
                System.Threading.Thread.Sleep(10);
                DeDupInfo deDupInfoObj = new DeDupInfo();
                deDupInfoObj.Name = dr["NAME"].ToString();
                deDupInfoObj.FatherName = dr["Fathers_Name"].ToString();
                deDupInfoObj.MotherName = dr["Mothers_Name"].ToString();
                try
                {
                    deDupInfoObj.DateOfBirth = Convert.ToDateTime(dr["Date_of_Birth"].ToString());
                }
                catch (Exception)
                {

                    deDupInfoObj.DateOfBirth = DateTime.Now;
                }

                deDupInfoObj.ScbMaterNo = dr["MASTER"].ToString();
                deDupInfoObj.DeclineReason = dr["Declined_reason"].ToString();
                try
                {
                    deDupInfoObj.DeclineDate = Convert.ToDateTime(dr["Declined_date"].ToString());
                }
                catch (Exception)
                {
                    deDupInfoObj.DeclineDate = DateTime.Now;
                }
                deDupInfoObj.BusinessEmployeeName = dr["BusinessOrEmployer_Name"].ToString();
                deDupInfoObj.Address1 = dr["ADD1"].ToString();
                deDupInfoObj.Address2 = dr["ADD2"].ToString();
                deDupInfoObj.Phone1 = dr["TELC"].ToString();
                deDupInfoObj.Phone2 = dr["TELR"].ToString();
                deDupInfoObj.Mobile1 = dr["Mob_1"].ToString();
                deDupInfoObj.Mobile2 = dr["Mob_2"].ToString();
                if (!String.IsNullOrEmpty(dr["PRODUCT"].ToString()))
                {
                    deDupInfoObj.ProductId = GetProductId(dr["PRODUCT"].ToString());
                }
                else
                {
                    deDupInfoObj.ProductId = 0;
                }

                deDupInfoObj.BankBranch = dr["Bank_Branch"].ToString();
                deDupInfoObj.Source = dr["Source"].ToString();
                deDupInfoObj.Status = status;
                deDupInfoObj.Remarks = dr["Remarks"].ToString();
                deDupInfoObj.Tin = dr["TIN"].ToString();
                deDupInfoObj.IdType = dr["ID_TYPE1"].ToString();
                deDupInfoObj.IdNo = dr["ID_NO1"].ToString();
                deDupInfoObj.IdType1 = dr["ID_TYPE2"].ToString();
                deDupInfoObj.IdNo1 = dr["ID_NO2"].ToString();
                deDupInfoObj.IdType2 = dr["ID_TYPE3"].ToString();
                deDupInfoObj.IdNo2 = dr["ID_NO3"].ToString();
                deDupInfoObj.IdType3 = dr["ID_TYPE4"].ToString();
                deDupInfoObj.IdNo3 = dr["ID_NO4"].ToString();
                deDupInfoObj.IdType4 = dr["ID_TYPE5"].ToString();
                deDupInfoObj.IdNo4 = dr["ID_NO5"].ToString();
                deDupInfoObj.ResidentaileStatus = dr["RESIDENTIALSTATUS"].ToString();
                deDupInfoObj.EducationalLevel = dr["EDUCATIONLEVEL"].ToString();
                deDupInfoObj.MaritalStatus = dr["MARITALSTATUS"].ToString();
                deDupInfoObj.LoanAccountNo = dr["LOANACCOUNTNO"].ToString();
                deDupInfoObj.Active = "Y";
                deDupInfoObj.CriteriaType = dr["Status"].ToString();
                deDupInfoObj.EntryDate = DateTime.Now;

                DeleteDuplicatData(deDupInfoObj.LoanAccountNo);

                mSQL = mSQL + "('" +
                      deDupInfoObj.Name.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.FatherName.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.MotherName.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.ScbMaterNo.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" +
                      deDupInfoObj.Profession.SpecialCharacterRemover() + "'," +
                      deDupInfoObj.ProductId + ",'" +
                      deDupInfoObj.DeclineReason.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" +
                      deDupInfoObj.BusinessEmployeeName.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.Address1.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.Address2.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.Phone1 + "','" +
                      deDupInfoObj.Phone2 + "','" +
                      deDupInfoObj.Mobile1 + "','" +
                      deDupInfoObj.Mobile2 + "','" +
                      deDupInfoObj.BankBranch.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.Source.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.Status + "','" +
                      deDupInfoObj.Remarks.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.Tin + "','" +
                      deDupInfoObj.IdType.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.IdType1.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.IdType2.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.IdType3.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.IdType4.SpecialCharacterRemover() + "','" +
                      deDupInfoObj.IdNo + "','" +
                      deDupInfoObj.IdNo1 + "','" +
                      deDupInfoObj.IdNo2 + "','" +
                      deDupInfoObj.IdNo3 + "','" +
                      deDupInfoObj.IdNo4 + "','" +
                      deDupInfoObj.ResidentaileStatus + "','" +
                      deDupInfoObj.EducationalLevel + "','" +
                      deDupInfoObj.MaritalStatus + "','" +
                      deDupInfoObj.CriteriaType + "','" +
                      deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" +
                      deDupInfoObj.Active + "','" +
                      deDupInfoObj.LoanAccountNo + "'),";

                insertCounter = insertCounter + 1;
                if (insertCounter == 100)
                {
                    mSQL = mSQLString + mSQL;
                    mSQL = mSQL.Substring(0, mSQL.Length - 1);
                    if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                    {
                        System.Threading.Thread.Sleep(5);
                        uploadDataCount = uploadDataCount + insertCounter;
                        insertCounter = 0;
                        mSQL = "";
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(5);
                        uploadDataCount = uploadDataCount + insertCounter;
                        insertCounter = 0;
                        mSQL = "";
                    }
                }
            }
            if (insertCounter < 100)
            {
                mSQL = mSQLString + mSQL;
                mSQL = mSQL.Substring(0, mSQL.Length - 1);
                if (dbMySQL.DbExecute(mSQL) > -1)
                {
                    System.Threading.Thread.Sleep(5);
                    uploadDataCount = uploadDataCount + insertCounter;
                    insertCounter = 0;
                    mSQL = "";
                }
                else
                {
                    System.Threading.Thread.Sleep(5);
                    uploadDataCount = uploadDataCount + insertCounter;
                    insertCounter = 0;
                    mSQL = "";
                }
            }
            if (uploadDataCount > 0)
            {
                InsertToMainTable();
            }
            returnString = uploadDataCount.ToString() + " record upload successfully";
            uploadDataCount = 0;
            return returnString;

        }
        public string IsDate(string strinDate)
        {
            if (string.IsNullOrEmpty(strinDate) || strinDate == "")
            {
                strinDate = DateTime.Now.ToString("dd/MM/yyyy");
            }
            return strinDate;
        }
        public void InsertToMainTable()
        {
            string mSQL =
                "INSERT INTO de_dup_info (NM,FATHE_NM,MOTHE_NM,SCB_MST_NO,DOB,PROFE,PROD_ID,DECLI_REASO,DECLI_DT,BUSIN_EMPLO_NM,ADD1,ADD2,PH1,PH2,MOB1,MOB2,BANK_BRANC,SOURC,STATU,REMAR,TIN,ID_TYPE,DEDU_IDTYPE2,DEDU_IDTYPE3,DEDU_IDTYPE4,DEDU_IDTYPE5,ID_NO,DEDU_IDNO2,DEDU_IDNO3,DEDU_IDNO4,DEDU_IDNO5,DEDU_RESIDENTIALSTATUS,DEDU_EDUCATIONLEVEL,DEDU_MARITALSTATUS,CRITE_TYPE,ENTRY_DT,ACTIV,DEDU_LOANACCOUNTNO)" +
                "SELECT NM,FATHE_NM,MOTHE_NM,SCB_MST_NO,DOB,PROFE,PROD_ID,DECLI_REASO,DECLI_DT,BUSIN_EMPLO_NM,ADD1,ADD2,PH1,PH2,MOB1,MOB2,BANK_BRANC,SOURC,STATU,REMAR,TIN,ID_TYPE,DEDU_IDTYPE2,DEDU_IDTYPE3,DEDU_IDTYPE4,DEDU_IDTYPE5,ID_NO,DEDU_IDNO2,DEDU_IDNO3,DEDU_IDNO4,DEDU_IDNO5,DEDU_RESIDENTIALSTATUS,DEDU_EDUCATIONLEVEL,DEDU_MARITALSTATUS,CRITE_TYPE,ENTRY_DT,ACTIV,DEDU_LOANACCOUNTNO FROM t_pluss_dedupeupload";
            DbQueryManager.ExecuteNonQuery(mSQL);
            DeleteData();
        }
        public void DeleteDuplicatData(string loanAcNo)
        {
            string mSQL = "DELETE  FROM de_dup_info WHERE DEDU_LOANACCOUNTNO='" + loanAcNo.Trim() + "'";
            DbQueryManager.ExecuteNonQuery(mSQL);
        }
        public void DeleteData()
        {

            string mSQL = "truncate table t_pluss_dedupeupload";
            DbQueryManager.ExecuteNonQuery(mSQL);
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (!string.IsNullOrEmpty(sMessage))
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }


        public void SaveMFU_UploadResult(string fileName, DateTime uploadDate, string userId, int rowNom, string activity, string fileType)
        {
            try
            {
                //string query = String.Format(@"INSERT INTO MFU_Status (File_Name,Upload_Time,Uploader_User_Id,Row_Nom,Activity_Name,fileType ) VALUES ('{0}','{1}','{2}',{3},'{4}','{5}')", fileName, uploadDate.ToString("yyyy-MM-dd HH:mm:ss"), userId, rowNom, activity, fileType);
                string query = String.Format(@"INSERT INTO MFU_Status (File_Name,Upload_Time,Uploader_User_Id,Row_Nom,Activity_Name,fileType ) VALUES ('{0}',getDate(),'{1}',{2},'{3}','{4}')", fileName, userId, rowNom, activity, fileType);
                DbQueryManager.ExecuteNonQuery(query);
            }
            catch (Exception e)
            {

            }
        }

        #region Old SC
        //public int SendDedupeInToFinalTable(List<DeDupInfo> deDupInfoObjList)
        //{
        //    try
        //    {
        //        long insertCounter = 0;
        //        long tempListCount = 0;
        //        string mSQLString = "";
        //        string mSQL = "";

        //        mSQLString = "INSERT INTO de_dup_info (NM, FATHE_NM, MOTHE_NM," +
        //                     "SCB_MST_NO, DOB, PROFE, PROD_ID," +
        //                     "DECLI_REASO, DECLI_DT, BUSIN_EMPLO_NM," +
        //                     "ADD1, ADD2, PH1, PH2, MOB1, MOB2," +
        //                     "BANK_BRANC, SOURC, STATU, REMAR, " +
        //                     "TIN, ID_TYPE, DEDU_IDTYPE2, DEDU_IDTYPE3, " +
        //                     "DEDU_IDTYPE4, DEDU_IDTYPE5, ID_NO, " +
        //                     "DEDU_IDNO2, DEDU_IDNO3, DEDU_IDNO4," +
        //                     "DEDU_IDNO5, DEDU_RESIDENTIALSTATUS," +
        //                     "DEDU_EDUCATIONLEVEL, DEDU_MARITALSTATUS," +
        //                     "CRITE_TYPE, ENTRY_DT, ACTIV,DEDU_LOANACCOUNTNO) ";

        //        uploadDataCount = 0;

        //        int repeat = deDupInfoObjList.Count / 2000;
        //        int remainder = deDupInfoObjList.Count % 2000;

        //        for (int i = 0; i < repeat; i++)
        //        {
        //            mSQL = string.Empty;
        //            for (int k = i * 2000; k < ((i + 1) * 2000); k++)
        //            {
        //                DeDupInfo deDupInfoObj = deDupInfoObjList[k];
        //                var prof = deDupInfoObj.Profession ?? "";
        //                string tin = "";
        //                if (deDupInfoObj.MfuTin > 0)
        //                {
        //                    tin = deDupInfoObj.MfuTin.ToString();
        //                }

        //                if (!string.IsNullOrEmpty(mSQL))
        //                {
        //                    mSQL += " union all ";
        //                }

        //                mSQL += " select '" + AddSlash(deDupInfoObj.Name) + "','" + AddSlash(deDupInfoObj.FatherName) + "','" + AddSlash(deDupInfoObj.MotherName) + "','" +
        //                      deDupInfoObj.ScbMaterNo + "','" + deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" + prof + "'," + deDupInfoObj.ProductId + ",'" +
        //                      AddSlash(deDupInfoObj.DeclineReason) + "','" + deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "','" +
        //                      AddSlash(deDupInfoObj.Address1) + "','" + AddSlash(deDupInfoObj.Address2) + "','" + deDupInfoObj.Phone1 + "','" + deDupInfoObj.Phone2 + "','" +
        //                      deDupInfoObj.Mobile1 + "','" + deDupInfoObj.Mobile2 + "','" + AddSlash(deDupInfoObj.BankBranch) + "','" + deDupInfoObj.Source + "','" +
        //                      deDupInfoObj.Status + "','" + AddSlash(deDupInfoObj.Remarks) + "','" + tin + "','" + deDupInfoObj.IdType + "','" +
        //                      deDupInfoObj.IdType1 + "','" + deDupInfoObj.IdType2 + "','" + deDupInfoObj.IdType3 + "','" + deDupInfoObj.IdType4 + "','" + AddSlash(deDupInfoObj.IdNo) + "','" +
        //                      AddSlash(deDupInfoObj.IdNo1) + "','" + AddSlash(deDupInfoObj.IdNo2) + "','" + AddSlash(deDupInfoObj.IdNo3) + "','" + AddSlash(deDupInfoObj.IdNo4) + "','" +
        //                      deDupInfoObj.ResidentaileStatus + "','" + deDupInfoObj.EducationalLevel + "','" + deDupInfoObj.MaritalStatus + "','" + deDupInfoObj.CriteriaType + "','" +
        //                      deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" + deDupInfoObj.Active + "','" + deDupInfoObj.LoanAccountNo + "'";
        //            }
        //            mSQL = mSQLString + mSQL;

        //            DbQueryManager.ExecuteNonQuery(mSQL);
        //        }

        //        mSQL = string.Empty;
        //        for (int k = 0; k < remainder; k++)
        //        {
        //            DeDupInfo deDupInfoObj = deDupInfoObjList[k];
        //            var prof = deDupInfoObj.Profession ?? "";
        //            string tin = "";
        //            if (deDupInfoObj.MfuTin > 0)
        //            {
        //                tin = deDupInfoObj.MfuTin.ToString();
        //            }

        //            if (!string.IsNullOrEmpty(mSQL))
        //            {
        //                mSQL += " union all ";
        //            }

        //            mSQL += " select '" + AddSlash(deDupInfoObj.Name) + "','" + AddSlash(deDupInfoObj.FatherName) + "','" + AddSlash(deDupInfoObj.MotherName) + "','" +
        //                  deDupInfoObj.ScbMaterNo + "','" + deDupInfoObj.DateOfBirth.ToString("yyyy-MM-dd") + "','" + prof + "'," + deDupInfoObj.ProductId + ",'" +
        //                  AddSlash(deDupInfoObj.DeclineReason) + "','" + deDupInfoObj.DeclineDate.ToString("yyyy-MM-dd") + "','" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "','" +
        //                  AddSlash(deDupInfoObj.Address1) + "','" + AddSlash(deDupInfoObj.Address2) + "','" + deDupInfoObj.Phone1 + "','" + deDupInfoObj.Phone2 + "','" +
        //                  deDupInfoObj.Mobile1 + "','" + deDupInfoObj.Mobile2 + "','" + AddSlash(deDupInfoObj.BankBranch) + "','" + deDupInfoObj.Source + "','" +
        //                  deDupInfoObj.Status + "','" + AddSlash(deDupInfoObj.Remarks) + "','" + tin + "','" + deDupInfoObj.IdType + "','" +
        //                  deDupInfoObj.IdType1 + "','" + deDupInfoObj.IdType2 + "','" + deDupInfoObj.IdType3 + "','" + deDupInfoObj.IdType4 + "','" + AddSlash(deDupInfoObj.IdNo) + "','" +
        //                  AddSlash(deDupInfoObj.IdNo1) + "','" + AddSlash(deDupInfoObj.IdNo2) + "','" + AddSlash(deDupInfoObj.IdNo3) + "','" + AddSlash(deDupInfoObj.IdNo4) + "','" +
        //                  deDupInfoObj.ResidentaileStatus + "','" + deDupInfoObj.EducationalLevel + "','" + deDupInfoObj.MaritalStatus + "','" + deDupInfoObj.CriteriaType + "','" +
        //                  deDupInfoObj.EntryDate.ToString("yyyy-MM-dd") + "','" + deDupInfoObj.Active + "','" + deDupInfoObj.LoanAccountNo + "'";
        //        }
        //        mSQL = mSQLString + mSQL;

        //        //var query = mSQL.Replace("'", "''");
        //        //String fullQuery = String.Format(@"Insert into MFU_Query (Query) values ('{0}')", query);
        //        //DbQueryManager.ExecuteNonQuery(fullQuery);

        //        DbQueryManager.ExecuteNonQuery(mSQL);
        //        return deDupInfoObjList.Count;

        //    }
        //    catch (Exception ex)
        //    {
        //        LogFile.WriteLog(ex);
        //        CustomException.Save(ex, "MFU Upload Query Failed in DeDupInfoGateway");
        //        throw ex;
        //    }


        //} 
        #endregion

        #region Slow Insertion
        //public int SendDedupeInToFinalTable(List<DeDupInfo> dedupList)
        //{
        //    try
        //    {
        //        string sql = "";// "BEGIN TRANSACTION INSERTION;";

        //        sql += "INSERT INTO de_dup_info_temp (NM, FATHE_NM, MOTHE_NM," +
        //                     "SCB_MST_NO, DOB, PROFE, PROD_ID," +
        //                     "DECLI_REASO, DECLI_DT, BUSIN_EMPLO_NM," +
        //                     "ADD1, ADD2, PH1, PH2, MOB1, MOB2," +
        //                     "BANK_BRANC, SOURC, STATU, REMAR, " +
        //                     "TIN, ID_TYPE, DEDU_IDTYPE2, DEDU_IDTYPE3, " +
        //                     "DEDU_IDTYPE4, DEDU_IDTYPE5, ID_NO, " +
        //                     "DEDU_IDNO2, DEDU_IDNO3, DEDU_IDNO4," +
        //                     "DEDU_IDNO5, DEDU_RESIDENTIALSTATUS," +
        //                     "DEDU_EDUCATIONLEVEL, DEDU_MARITALSTATUS," +
        //                     "CRITE_TYPE, ENTRY_DT, ACTIV,DEDU_LOANACCOUNTNO) ";

        //        DeDupInfo dedupTem = dedupList[0];

        //        sql += " SELECT '" + AddSlash(dedupTem.Name) + "','" + AddSlash(dedupTem.FatherName) + "','" + AddSlash(dedupTem.MotherName) + "','" +
        //                      dedupTem.ScbMaterNo + "','" + dedupTem.DateOfBirth.ToString("yyyy-MM-dd") + "','" + dedupTem.Profession + "'," + dedupTem.ProductId + ",'" +
        //                      AddSlash(dedupTem.DeclineReason) + "','" + dedupTem.DeclineDate.ToString("yyyy-MM-dd") + "','" + AddSlash(dedupTem.BusinessEmployeeName) + "','" +
        //                      AddSlash(dedupTem.Address1) + "','" + AddSlash(dedupTem.Address2) + "','" + dedupTem.Phone1 + "','" + dedupTem.Phone2 + "','" +
        //                      dedupTem.Mobile1 + "','" + dedupTem.Mobile2 + "','" + AddSlash(dedupTem.BankBranch) + "','" + dedupTem.Source + "','" +
        //                      dedupTem.Status + "','" + AddSlash(dedupTem.Remarks) + "','" + dedupTem.MfuTin + "','" + dedupTem.IdType + "','" +
        //                      dedupTem.IdType1 + "','" + dedupTem.IdType2 + "','" + dedupTem.IdType3 + "','" + dedupTem.IdType4 + "','" + AddSlash(dedupTem.IdNo) + "','" +
        //                      AddSlash(dedupTem.IdNo1) + "','" + AddSlash(dedupTem.IdNo2) + "','" + AddSlash(dedupTem.IdNo3) + "','" + AddSlash(dedupTem.IdNo4) + "','" +
        //                      dedupTem.ResidentaileStatus + "','" + dedupTem.EducationalLevel + "','" + dedupTem.MaritalStatus + "','" + dedupTem.CriteriaType + "','" +
        //                      dedupTem.EntryDate.ToString("yyyy-MM-dd") + "','" + dedupTem.Active + "','" + dedupTem.LoanAccountNo + "'";

        //        for (int i = 1; i < dedupList.Count; i++)
        //        {
        //            DeDupInfo dedup = dedupList[i];

        //            sql += "UNION ALL SELECT '" + AddSlash(dedup.Name) + "','" + AddSlash(dedup.FatherName) + "','" + AddSlash(dedup.MotherName) + "','" +
        //                          dedup.ScbMaterNo + "','" + dedup.DateOfBirth.ToString("yyyy-MM-dd") + "','" + dedup.Profession + "'," + dedup.ProductId + ",'" +
        //                          AddSlash(dedup.DeclineReason) + "','" + dedup.DeclineDate.ToString("yyyy-MM-dd") + "','" + AddSlash(dedup.BusinessEmployeeName) + "','" +
        //                          AddSlash(dedup.Address1) + "','" + AddSlash(dedup.Address2) + "','" + dedup.Phone1 + "','" + dedup.Phone2 + "','" +
        //                          dedup.Mobile1 + "','" + dedup.Mobile2 + "','" + AddSlash(dedup.BankBranch) + "','" + dedup.Source + "','" +
        //                          dedup.Status + "','" + AddSlash(dedup.Remarks) + "','" + dedup.MfuTin + "','" + dedup.IdType + "','" +
        //                          dedup.IdType1 + "','" + dedup.IdType2 + "','" + dedup.IdType3 + "','" + dedup.IdType4 + "','" + AddSlash(dedup.IdNo) + "','" +
        //                          AddSlash(dedup.IdNo1) + "','" + AddSlash(dedup.IdNo2) + "','" + AddSlash(dedup.IdNo3) + "','" + AddSlash(dedup.IdNo4) + "','" +
        //                          dedup.ResidentaileStatus + "','" + dedup.EducationalLevel + "','" + dedup.MaritalStatus + "','" + dedup.CriteriaType + "','" +
        //                          dedup.EntryDate.ToString("yyyy-MM-dd") + "','" + dedup.Active + "','" + dedup.LoanAccountNo + "'";

        //        }
        //        //sql = sql.Remove(sql.Length - 1, 1);

        //        //sql += ";COMMIT TRANSACTION INSERTION;";

        //        DbQueryManager.ExecuteNonQuery(sql);
        //        return dedupList.Count;

        //    }
        //    catch (Exception ex)
        //    {
        //        LogFile.WriteLog(ex);
        //        CustomException.Save(ex, "MFU Upload Query Failed in DeDupInfoGateway");
        //        throw ex;
        //    }


        //}
        #endregion

        public int SendDedupeInToFinalTable(List<DeDupInfo> dedupList)
        {
            try
            {
                DataTable table = ListDataTableManager.GetListToDataTable<DeDupInfo>(dedupList);

                for (int i = 0; i < 5; i++)
                {
                    DataColumn tmp = table.Columns[0];

                    table.Columns.Remove(tmp);

                }

                string con = Connection.GetConnection().ConnectionString;
                SqlConnection connection = new SqlConnection(con);
                connection.Open();


                using (SqlBulkCopy copier = new SqlBulkCopy(connection))
                {
                    copier.DestinationTableName = "de_dup_info_temp";

                    copier.ColumnMappings.Add("Name", "NM");
                    copier.ColumnMappings.Add("FatherName", "FATHE_NM");
                    copier.ColumnMappings.Add("MotherName", "MOTHE_NM");
                    copier.ColumnMappings.Add("ScbMaterNo", "SCB_MST_NO");
                    copier.ColumnMappings.Add("DateOfBirth", "DOB");
                    copier.ColumnMappings.Add("Profession", "PROFE");
                    copier.ColumnMappings.Add("ProductId", "PROD_ID");
                    copier.ColumnMappings.Add("DeclineReason", "DECLI_REASO");
                    copier.ColumnMappings.Add("DeclineDate", "DECLI_DT");
                    copier.ColumnMappings.Add("BusinessEmployeeName", "BUSIN_EMPLO_NM");
                    copier.ColumnMappings.Add("Address1", "ADD1");
                    copier.ColumnMappings.Add("Address2", "ADD2");
                    copier.ColumnMappings.Add("Phone1", "PH1");
                    copier.ColumnMappings.Add("Phone2", "PH2");
                    copier.ColumnMappings.Add("Mobile1", "MOB1");
                    copier.ColumnMappings.Add("Mobile2", "MOB2");
                    copier.ColumnMappings.Add("BankBranch", "BANK_BRANC");
                    copier.ColumnMappings.Add("Source", "SOURC");
                    copier.ColumnMappings.Add("Status", "STATU");
                    copier.ColumnMappings.Add("Remarks", "REMAR");
                    copier.ColumnMappings.Add("MfuTin", "TIN");
                    copier.ColumnMappings.Add("IdType", "ID_TYPE");
                    copier.ColumnMappings.Add("IdType1", "DEDU_IDTYPE2");
                    copier.ColumnMappings.Add("IdType2", "DEDU_IDTYPE3");
                    copier.ColumnMappings.Add("IdType3", "DEDU_IDTYPE4");
                    copier.ColumnMappings.Add("IdType4", "DEDU_IDTYPE5");
                    copier.ColumnMappings.Add("IdNo", "ID_NO");
                    copier.ColumnMappings.Add("IdNo1", "DEDU_IDNO2");
                    copier.ColumnMappings.Add("IdNo2", "DEDU_IDNO3");
                    copier.ColumnMappings.Add("IdNo3", "DEDU_IDNO4");
                    copier.ColumnMappings.Add("IdNo4", "DEDU_IDNO5");
                    copier.ColumnMappings.Add("ResidentaileStatus", "DEDU_RESIDENTIALSTATUS");
                    copier.ColumnMappings.Add("EducationalLevel", "DEDU_EDUCATIONLEVEL");
                    copier.ColumnMappings.Add("MaritalStatus", "DEDU_MARITALSTATUS");
                    copier.ColumnMappings.Add("CriteriaType", "CRITE_TYPE");
                    copier.ColumnMappings.Add("EntryDate", "ENTRY_DT");
                    copier.ColumnMappings.Add("Active", "ACTIV");
                    copier.ColumnMappings.Add("LoanAccountNo", "DEDU_LOANACCOUNTNO");

                    copier.BatchSize = 50;

                    copier.WriteToServer(table);
                }

                connection.Close();

                return dedupList.Count;

            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "MFU Upload Query Failed in DeDupInfoGateway");
                throw ex;
            }


        }


        public int SendDedupeInToFinalTable(DataTable table, string fileType, User user)
        {
            try
            {
                string dbTableName = "de_dup_info_temp";
                if (fileType.EndsWith("CC_CHARGEOFF"))
                {
                    dbTableName = "de_dup_info_temp_CC_CHARGEOFF";
                }
                else if (fileType.EndsWith("CC_DISBURSED"))
                {
                    dbTableName = "de_dup_info_temp_CC_DISBURSED";
                }
                else if (fileType.EndsWith("LOAN_CHARGEOFF"))
                {
                    dbTableName = "de_dup_info_temp_LOAN_CHARGEOFF";
                }
                else if (fileType.EndsWith("LOAN_DISBURSED"))
                {
                    dbTableName = "de_dup_info_temp_LOAN_DISBURSED";
                }
                else if (fileType.EndsWith("LOAN_EBBS_CHARGEOFF"))
                {
                    dbTableName = "de_dup_info_temp_LOAN_EBBS_CHARGEOFF";
                }
                else if (fileType.EndsWith("LOAN_EBBS_DISBURSED"))
                {
                    dbTableName = "de_dup_info_temp_LOAN_EBBS_DISBURSED";
                }

                using (SqlBulkCopy copier = new SqlBulkCopy(Connection.GetConnection().ConnectionString))
                {
                    copier.DestinationTableName = dbTableName;

                    copier.ColumnMappings.Add("0", "NM");
                    copier.ColumnMappings.Add("1", "FATHE_NM");
                    copier.ColumnMappings.Add("2", "MOTHE_NM");
                    copier.ColumnMappings.Add("3", "SCB_MST_NO");
                    copier.ColumnMappings.Add("4", "DOB");
                    copier.ColumnMappings.Add("5", "PROFE");
                    copier.ColumnMappings.Add("6", "PROD_ID");
                    copier.ColumnMappings.Add("7", "DECLI_REASO");
                    copier.ColumnMappings.Add("8", "DECLI_DT");
                    copier.ColumnMappings.Add("9", "BUSIN_EMPLO_NM");
                    copier.ColumnMappings.Add("10", "ADD1");
                    copier.ColumnMappings.Add("11", "ADD2");
                    copier.ColumnMappings.Add("12", "PH1");
                    copier.ColumnMappings.Add("13", "PH2");
                    copier.ColumnMappings.Add("14", "MOB1");
                    copier.ColumnMappings.Add("15", "MOB2");
                    copier.ColumnMappings.Add("16", "BANK_BRANC");
                    copier.ColumnMappings.Add("17", "SOURC");
                    copier.ColumnMappings.Add("18", "STATU");
                    copier.ColumnMappings.Add("19", "REMAR");
                    copier.ColumnMappings.Add("20", "TIN");
                    copier.ColumnMappings.Add("21", "ID_TYPE");
                    copier.ColumnMappings.Add("22", "DEDU_IDTYPE2");
                    copier.ColumnMappings.Add("23", "DEDU_IDTYPE3");
                    copier.ColumnMappings.Add("24", "DEDU_IDTYPE4");
                    copier.ColumnMappings.Add("25", "DEDU_IDTYPE5");
                    copier.ColumnMappings.Add("26", "ID_NO");
                    copier.ColumnMappings.Add("27", "DEDU_IDNO2");
                    copier.ColumnMappings.Add("28", "DEDU_IDNO3");
                    copier.ColumnMappings.Add("29", "DEDU_IDNO4");
                    copier.ColumnMappings.Add("30", "DEDU_IDNO5");
                    copier.ColumnMappings.Add("31", "DEDU_RESIDENTIALSTATUS");
                    copier.ColumnMappings.Add("32", "DEDU_EDUCATIONLEVEL");
                    copier.ColumnMappings.Add("33", "DEDU_MARITALSTATUS");
                    copier.ColumnMappings.Add("34", "CRITE_TYPE");
                    copier.ColumnMappings.Add("35", "ENTRY_DT");
                    copier.ColumnMappings.Add("36", "ACTIV");
                    copier.ColumnMappings.Add("37", "DEDU_LOANACCOUNTNO");
                    copier.ColumnMappings.Add("MAKER_PS_ID", "MAKER_PS_ID");
                    copier.ColumnMappings.Add("MAKE_DATE", "MAKE_DATE");

                    copier.BatchSize = 50000;

                    copier.WriteToServer(table);
                }
                return table.Rows.Count;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "MFU Upload Query Failed in DeDupInfoGateway");
                throw ex;
            }
        }

        public DataTable GetMFU_FileUploadStatus()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_ALLFileUploadStatus");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting 'GetMFU_FileUploadStatus' ");
                throw ex;
            }
            return table;
        }

        #region Clear Tempoary Data Table 

        public bool ClearTempTableForCC_CHARGEOFF()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_TempDataDelete_CC_CHARGEOFF");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public bool ClearTempTableForCC_DISBURSED()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_TempDataDelete_CC_DISBURSED");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public bool ClearTempTableForLOAN_CHARGEOFF()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_TempDataDelete_LOAN_CHARGEOFF");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public bool ClearTempTableForLOAN_DISBURSED()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_TempDataDelete_LOAN_DISBURSED");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public bool ClearTempTableForLOAN_EBBS_CHARGEOFF()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_TempDataDelete_LOAN_EBBS_CHARGEOFF");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public bool ClearTempTableForLOAN_EBBS_DISBURSED()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_TempDataDelete_LOAN_EBBS_DISBURSED");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }


        #endregion


        #region    Checking /process data 
         
        public bool CheckDataForCC_CHARGEOFF()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_DataChecking_CC_CHARGEOFF");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }


        public bool CheckDataForCC_DISBURSED()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_DataChecking_CC_DISBURSED");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public bool CheckDataForLOAN_CHARGEOFF()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_DataChecking_LOAN_CHARGEOFF");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public bool CheckDataForLOAN_DISBURSED()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_DataChecking_LOAN_DISBURSED");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }


        public bool CheckDataForLOAN_EBBS_CHARGEOFF()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_DataChecking_LOAN_EBBS_CHARGEOFF");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public bool CheckDataForLOAN_EBBS_DISBURSED()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_DataChecking_LOAN_EBBS_DISBURSED");
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        #endregion

        #region Move To main or Approve.. 

        public bool MoveToMainForCC_CHARGEOFF( string userid)
        {
            try
            {
                List<SqlParameter> list = new List<SqlParameter>();
                list.Add(new SqlParameter
                {
                    ParameterName = "userid",
                    Value = userid
                });
                DbQueryManager.ExecuteSP("pMFU_MoveToMain_CC_CHARGEOFF", list);
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw;
            }
        }
        public bool MoveToMainForCC_DISBURSED(string userid)
        {
            try
            {
                List<SqlParameter> list = new List<SqlParameter>();
                list.Add(new SqlParameter
                {
                    ParameterName = "userid",
                    Value = userid
                });
                DbQueryManager.ExecuteSP("pMFU_MoveToMain_CC_DISBURSED", list);
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw;
            }
        }
        public bool MoveToMainForLOAN_CHARGEOFF(string userid)
        {
            try
            {
                List<SqlParameter> list = new List<SqlParameter>();
                list.Add(new SqlParameter
                {
                    ParameterName = "userid",
                    Value = userid
                });
                DbQueryManager.ExecuteSP("pMFU_MoveToMain_LOAN_CHARGEOFF", list);
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw;
            }
        }
        public bool MoveToMainForLOAN_DISBURSED(string userid)
        {
            try
            {
                List<SqlParameter> list = new List<SqlParameter>();
                list.Add(new SqlParameter
                {
                    ParameterName = "userid",
                    Value = userid
                });
                DbQueryManager.ExecuteSP("pMFU_MoveToMain_LOAN_DISBURSED", list);
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw;
            }
        }
        public bool MoveToMainForLOAN_EBBS_CHARGEOFF(string userid)
        {
            try
            {
                List<SqlParameter> list = new List<SqlParameter>();
                list.Add(new SqlParameter
                {
                    ParameterName = "userid",
                    Value = userid
                });
                DbQueryManager.ExecuteSP("pMFU_MoveToMain_LOAN_EBBS_CHARGEOFF", list);
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw;
            }
        }
        public bool MoveToMainForLOAN_EBBS_DISBURSED(string userid)
        {
            try
            {
                List<SqlParameter> list = new List<SqlParameter>();
                list.Add(new SqlParameter
                {
                    ParameterName = "userid",
                    Value = userid
                });
                DbQueryManager.ExecuteSP("pMFU_MoveToMain_LOAN_EBBS_DISBURSED", list);
                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw;
            }
        }

        #endregion

        #region Get Error File Data..


        public DataTable GetErrorData()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetErrorData");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting data for CSV");
                throw ex;
            }
            return table;
        }


        public DataTable GetErrorDataForCC_CHARGEOFF()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetErrorData_CC_CHARGEOFF");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting data for CSV");
                throw ex;
            }
            return table;
        }

        public DataTable GetErrorDataForCC_DISBURSED()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetErrorData_CC_DISBURSED");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting data for CSV");
                throw ex;
            }
            return table;
        }

        public DataTable GetErrorDataForLOAN_CHARGEOFF()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetErrorData_LOAN_CHARGEOFF");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting data for CSV");
                throw ex;
            }
            return table;
        }
        public DataTable GetErrorDataForLOAN_DISBURSED()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetErrorData_LOAN_DISBURSED");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting data for CSV");
                throw ex;
            }
            return table;
        }
        public DataTable GetErrorDataForLOAN_EBBS_CHARGEOFF()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetErrorData_LOAN_EBBS_CHARGEOFF");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting data for CSV");
                throw ex;
            }
            return table;
        }
        public DataTable GetErrorDataForLOAN_EBBS_DISBURSED()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetErrorData_LOAN_EBBS_DISBURSED");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting data for CSV");
                throw ex;
            }
            return table;
        }

        #endregion


        #region Download temp data.  
        
        public DataTable GetTempDataCSVForCC_CHARGEOFF()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetTempData_CC_CHARGEOFF");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting temp data for CSV");
                throw ex;
            }
            return table;
        }

        public DataTable GetTempDataCSVForCC_DISBURSED()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetTempData_CC_DISBURSED");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting temp data for CSV");
                throw ex;
            }
            return table;
        }

        public DataTable GetTempDataCSVForLOAN_CHARGEOFF()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetTempData_LOAN_CHARGEOFF");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting temp data for CSV");
                throw ex;
            }
            return table;
        }

        public DataTable GetTempDataCSVForLOAN_DISBURSED()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetTempData_LOAN_DISBURSED");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting temp data for CSV");
                throw ex;
            }
            return table;
        }

        public DataTable GetTempDataCSVForLOAN_EBBS_CHARGEOFF()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetTempData_LOAN_EBBS_CHARGEOFF");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting temp data for CSV");
                throw ex;
            }
            return table;
        }

        public DataTable GetTempDataCSVForLOAN_EBBS_DISBURSED()
        {
            DataTable table = new DataTable();
            try
            {
                table = DbQueryManager.GetDataTableWithSp("pMFU_GetTempData_LOAN_EBBS_DISBURSED");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Error while getting temp data for CSV");
                throw ex;
            }
            return table;
        }
        #endregion

        public bool DeleteErrorsFromtemp()
        {
            try
            {
                DbQueryManager.ExecuteSP("pMFU_RemoveErrorFromTemp");

                return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Temp data Clearing Problem");
                throw ex;
            }
        }

        public int InsertIntoDeDupNegativeTemp(DataTable table)
        {
            int returnValue = 0;

            returnValue = InsertIntoNegativeTemp(table);

            return returnValue;
        }

        private int InsertIntoNegativeTemp(DataTable table)
        {
            int returnValue = 0;
            try
            {
                using (SqlBulkCopy copier = new SqlBulkCopy(Connection.GetConnection().ConnectionString))
                {
                    copier.DestinationTableName = "de_dup_info_negative_temp";

                    copier.ColumnMappings.Add("Name", "NM");
                    copier.ColumnMappings.Add("ScbMaterNo", "SCB_MST_NO");
                    copier.ColumnMappings.Add("FatherName", "FATHE_NM");
                    copier.ColumnMappings.Add("MotherName", "MOTHE_NM");
                    copier.ColumnMappings.Add("DateOfBirth", "DOB");
                    copier.ColumnMappings.Add("DeclineReason", "DECLI_REASO");
                    copier.ColumnMappings.Add("DeclineDate", "DECLI_DT");
                    copier.ColumnMappings.Add("BusinessEmployeeName", "BUSIN_EMPLO_NM");
                    copier.ColumnMappings.Add("Address1", "ADD1");
                    copier.ColumnMappings.Add("Address2", "ADD2");
                    copier.ColumnMappings.Add("Phone1", "PH1");
                    copier.ColumnMappings.Add("Phone2", "PH2");
                    copier.ColumnMappings.Add("Mobile1", "MOB1");
                    copier.ColumnMappings.Add("Mobile2", "MOB2");
                    copier.ColumnMappings.Add("ProductId", "PROD_ID");
                    copier.ColumnMappings.Add("BankBranch", "BANK_BRANC");
                    copier.ColumnMappings.Add("Source", "SOURC");
                    copier.ColumnMappings.Add("CriteriaType", "CRITE_TYPE");
                    copier.ColumnMappings.Add("Remarks", "REMAR");
                    copier.ColumnMappings.Add("MfuTin", "TIN");
                    copier.ColumnMappings.Add("IdType", "ID_TYPE");
                    copier.ColumnMappings.Add("IdNo", "ID_NO");
                    copier.ColumnMappings.Add("IdType1", "DEDU_IDTYPE2");
                    copier.ColumnMappings.Add("IdNo1", "DEDU_IDNO2");
                    copier.ColumnMappings.Add("IdType2", "DEDU_IDTYPE3");
                    copier.ColumnMappings.Add("IdNo2", "DEDU_IDNO3");
                    copier.ColumnMappings.Add("IdType3", "DEDU_IDTYPE4");
                    copier.ColumnMappings.Add("IdNo3", "DEDU_IDNO4");
                    copier.ColumnMappings.Add("IdType4", "DEDU_IDTYPE5");
                    copier.ColumnMappings.Add("IdNo4", "DEDU_IDNO5");
                    copier.ColumnMappings.Add("ResidentaileStatus", "DEDU_RESIDENTIALSTATUS");
                    copier.ColumnMappings.Add("EducationalLevel", "DEDU_EDUCATIONLEVEL");
                    copier.ColumnMappings.Add("MaritalStatus", "DEDU_MARITALSTATUS");
                    copier.ColumnMappings.Add("LoanAccountNo", "DEDU_LOANACCOUNTNO");
                    copier.ColumnMappings.Add("MAKER_PS_ID", "MAKER_PS_ID");
                    copier.ColumnMappings.Add("MAKE_DATE", "MAKE_DATE");


                    copier.BatchSize = 50000;
                    copier.WriteToServer(table);
                }
                //return table.Rows.Count;
                return INSERTE;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "Negative List Insertion");
                //throw ex;
                //throw ERROR
                returnValue = ERROR;
            }
            return returnValue;
        }


        public bool ApproveDeDupDecline(int dedupId, User user)
        {
            try
            {
                List<SqlParameter> list = new List<SqlParameter>();
                list.Add(new SqlParameter
                {
                    ParameterName = "dedupId",
                    Value = dedupId
                });

                list.Add(new SqlParameter
                {
                    ParameterName = "CheckerPsId",
                    Value = user.UserId
                });
                int rowAffected = DbQueryManager.ExecuteSP("sp_ApproveDedupDecline", list);

                if (rowAffected > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);

                return false;
            }
        }
    }
}
