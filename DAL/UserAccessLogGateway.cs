﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;
using BusinessEntities.SecurityMatrixEntities;

namespace DAL
{
    public class UserAccessLogGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        public UserAccessLogGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public List<UserAccessLog> GetAccessLog()
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            string mSQL = "SELECT * FROM t_pluss_accesslog LEFT JOIN t_pluss_user ON ACLO_USERLOGIID=USER_CODE WHERE USER_STATUS !=0";
            return userAccessLogObjList = GetListData(mSQL);
        }
        public List<UserAccessLog> GetAccessLog(string loginId)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            string mSQL = "SELECT * FROM t_pluss_accesslog LEFT JOIN t_pluss_user ON ACLO_USERLOGIID=USER_CODE WHERE USER_STATUS !=0 AND ACLO_USERLOGIID='" + loginId.Trim() + "'";
            return userAccessLogObjList = GetListData(mSQL);
        }
        public List<UserAccessLog> GetLoginHistory(string loginId)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            //string mSQL = "SELECT * FROM t_pluss_accesslog LEFT JOIN t_pluss_user ON ACLO_USERLOGIID=USER_CODE WHERE ACLO_USERLOGIID='" + loginId.Trim() + "' AND (ACLO_LOGINSUCCESSSTATUS=1 OR ACLO_LOGINUNSUCCESSSTATUS=1) ORDER BY ACLO_LOGINDATETIME DESC LIMIT 0, 1";
            string mSQL = "SELECT top 1 * FROM t_pluss_accesslog LEFT JOIN t_pluss_user ON ACLO_USERLOGIID=USER_CODE WHERE ACLO_USERLOGIID='" + loginId.Trim() + "'  ORDER BY ACLO_LOGINDATETIME DESC";
            return userAccessLogObjList = GetListData(mSQL);
        }

        public List<UserAccessLog> GetAccessLog(string loginId,int logInStatus)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            string mSQL = "SELECT * FROM t_pluss_accesslog LEFT JOIN t_pluss_user ON ACLO_USERLOGIID=USER_CODE WHERE USER_STATUS !=0 AND ACLO_USERLOGIID='" + loginId.Trim() + "' AND ACLO_LOGINSTATUS=" + logInStatus;
            return userAccessLogObjList = GetListData(mSQL);
        }
        public List<UserAccessLog> GetAccessLog(string loginId,DateTime startDate,DateTime endDate)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            string mSQL = "SELECT * FROM t_pluss_accesslog LEFT JOIN t_pluss_user ON ACLO_USERLOGIID=USER_CODE WHERE USER_STATUS !=0 AND ACLO_USERLOGIID='" + loginId.Trim() + "' AND FORMAT(ACLO_LOGINDATETIME,'yyyy-MM-dd') BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "'";
            return userAccessLogObjList = GetListData(mSQL);
        }
        public List<UserAccessLog> GetAccessLog(DateTime startDate, DateTime endDate)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            string mSQL = "SELECT * FROM t_pluss_accesslog LEFT JOIN t_pluss_user ON ACLO_USERLOGIID=USER_CODE WHERE USER_STATUS !=0 AND FORMAT(ACLO_LOGINDATETIME,'yyyy-MM-dd') BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "'";
            return userAccessLogObjList = GetListData(mSQL);
        }
        public UserAccessLog GetAccessLog(string loginId,DateTime logInDate)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            UserAccessLog userAccessLogObj = null;
            string mSQL = "SELECT top 1 * FROM t_pluss_accesslog LEFT JOIN t_pluss_user ON ACLO_USERLOGIID=USER_CODE WHERE USER_STATUS !=0 AND ACLO_USERLOGIID='" + loginId + "' AND FORMAT (ACLO_LOGINDATETIME, 'yyyy-MM-dd')='" + logInDate.ToString("yyyy-MM-dd") + "' order by ACLO_ID desc";
            userAccessLogObjList = GetListData(mSQL);
            if (userAccessLogObjList.Count > 0)
            {
                userAccessLogObj = new UserAccessLog();
                userAccessLogObj = userAccessLogObjList.Find(findUserAccessLogObj => findUserAccessLogObj.UserLogInId == loginId && findUserAccessLogObj.LogInDateTime.ToString("dd-MM-yyyy") == logInDate.ToString("dd-MM-yyyy"));
            }
            return userAccessLogObj;
        }

        public int SendDataInToDB(UserAccessLog userAccessLogObj)
        {
            int returnValue = 0;
            UserAccessLog userExistingAccessLogObj = new UserAccessLog();
            userExistingAccessLogObj = GetAccessLog(userAccessLogObj.UserLogInId, userAccessLogObj.LogInDateTime);
            //if (userExistingAccessLogObj != null)
            //{
            //    userAccessLogObj.AcessLogId = userExistingAccessLogObj.AcessLogId;
            //    userAccessLogObj.LogInCount = userExistingAccessLogObj.LogInCount + 1;
            //    userAccessLogObj.userLoginSuccessStatus = userAccessLogObj.userLoginSuccessStatus;//+ userExistingAccessLogObj.userLoginSuccessStatus;
            //    userAccessLogObj.userLoginUnSuccessStatus = userAccessLogObj.userLoginUnSuccessStatus;//+ userExistingAccessLogObj.userLoginUnSuccessStatus;
            //    if (userAccessLogObj.AccessFor == "LOGIN")
            //    {
            //        returnValue = UpdateUserAccessLog(userAccessLogObj);
            //    }
            //    else
            //    {
            //        returnValue = UpdateLogOutUserAccessLog(userAccessLogObj);
            //    }
            //}
            //else
            //{
            //    userAccessLogObj.LogInCount = 1;
            //    returnValue = InsertUserAccessLog(userAccessLogObj);
            //}
            userAccessLogObj.LogInCount = 1;
            returnValue = InsertUserAccessLog(userAccessLogObj);
            return returnValue;
        }
        private int InsertUserAccessLog(UserAccessLog userAccessLogObj)
        {
            int returnValue = 0;
            string mSQL = "INSERT INTO t_pluss_accesslog (ACLO_USERLOGIID,ACLO_IP,ACLO_LOGINDATETIME,ACLO_LOGOUTDATETIME,ACLO_LOGINCOUNT,ACLO_LOGINSUCCESSSTATUS,ACLO_LOGINUNSUCCESSSTATUS,ACLO_LOGINSTATUS) VALUES(" +
                        "'" + userAccessLogObj.UserLogInId + "','" +
                        userAccessLogObj.IP + "','" +
                        userAccessLogObj.LogInDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "','" +
                        userAccessLogObj.LogOutDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'," +
                        userAccessLogObj.LogInCount + "," +
                        userAccessLogObj.userLoginSuccessStatus + "," +
                        userAccessLogObj.userLoginUnSuccessStatus + ","+
                        userAccessLogObj.userLoginStatus +")";
            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                returnValue = INSERTE;
            }
            else
            {
                returnValue = ERROR;
            }
            return returnValue;
        }
        private int UpdateUserAccessLog(UserAccessLog userAccessLogObj)
        {
            int returnValue = 0;
            string mSQL = "UPDATE t_pluss_accesslog SET " +
            "ACLO_USERLOGIID='" + userAccessLogObj.UserLogInId + "'," +
            "ACLO_IP='" + userAccessLogObj.IP + "'," +
            "ACLO_LOGINDATETIME='" + userAccessLogObj.LogInDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'," +
            "ACLO_LOGOUTDATETIME='" + userAccessLogObj.LogOutDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "',"+
            "ACLO_LOGINCOUNT=" + userAccessLogObj.LogInCount + ","+
            "ACLO_LOGINSUCCESSSTATUS=" + userAccessLogObj.userLoginSuccessStatus + ","+
            "ACLO_LOGINUNSUCCESSSTATUS=" + userAccessLogObj.userLoginUnSuccessStatus + ","+
            "ACLO_LOGINSTATUS=" + userAccessLogObj.userLoginStatus + " WHERE ACLO_ID=" + userAccessLogObj.AcessLogId;

            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                returnValue = UPDATE;
            }
            else
            {
                returnValue = ERROR;
            }
            return returnValue;
        }
        private int UpdateLogOutUserAccessLog(UserAccessLog userAccessLogObj)
        {
            int returnValue = 0;
            string mSQL = "UPDATE t_pluss_accesslog SET " +
            "ACLO_LOGOUTDATETIME='" + userAccessLogObj.LogOutDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'," +
            "ACLO_LOGINSTATUS=" + userAccessLogObj.userLoginStatus + " WHERE ACLO_ID=" + userAccessLogObj.AcessLogId;

            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                returnValue = UPDATE;
            }
            else
            {
                returnValue = ERROR;
            }
            return returnValue;
        }

        private List<UserAccessLog> GetListData(string mSQL)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            UserAccessLog userAccessLogObj = null;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    userAccessLogObj = new UserAccessLog();
                    userAccessLogObj.AcessLogId = Convert.ToInt32(xRs["ACLO_ID"]);
                    userAccessLogObj.UserLogInId = Convert.ToString(xRs["ACLO_USERLOGIID"]);
                    userAccessLogObj.IP = Convert.ToString(xRs["ACLO_IP"]);
                    userAccessLogObj.LogInDateTime = Convert.ToDateTime(xRs["ACLO_LOGINDATETIME"]);
                    try
                    {
                        userAccessLogObj.LogOutDateTime = Convert.ToDateTime(xRs["ACLO_LOGOUTDATETIME"]);
                    }
                    catch { }
                    userAccessLogObj.LogInCount = Convert.ToInt32(xRs["ACLO_LOGINCOUNT"]);
                    userAccessLogObj.userLoginSuccessStatus = Convert.ToInt32(xRs["ACLO_LOGINSUCCESSSTATUS"]);
                    userAccessLogObj.userLoginUnSuccessStatus = Convert.ToInt32(xRs["ACLO_LOGINUNSUCCESSSTATUS"]);
                    userAccessLogObj.userLoginStatus = Convert.ToInt32(xRs["ACLO_LOGINSTATUS"]);
                    userAccessLogObjList.Add(userAccessLogObj);
                }
                xRs.Close();
            }
           
            return userAccessLogObjList;
        }

        public int UpdateLoginStatus(UserAccessLog userAccessLogObj)
        {
            int returnValue = 0;
            string mSQL = "UPDATE t_pluss_accesslog SET " +
            "ACLO_LOGOUTDATETIME='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'," +
            "ACLO_LOGINSTATUS=" + userAccessLogObj.userLoginStatus + " WHERE ACLO_ID=" + userAccessLogObj.AcessLogId;
            
            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                returnValue = UPDATE;
            }
            else
            {
                returnValue = ERROR;
            }
            return returnValue;
        }


        public DataTable GetUnSuccessfullLoginHistory(string loginId)
        {
            try
            {
                var sql = string.Format(@"SELECT count(*)Total FROM t_pluss_accesslog 
                            WHERE  ACLO_USERLOGIID='{0}' and ACLO_LOGINUNSUCCESSSTATUS=1 and ACLO_LOGINDATETIME>(select min(ACLO_LOGINDATETIME) from (
                            SELECT top 2 * FROM t_pluss_accesslog 
                            WHERE  ACLO_USERLOGIID='{0}' and ACLO_LOGINSUCCESSSTATUS=1
                            order by ACLO_ID desc) a)",loginId);
                return DbQueryManager.GetTable(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DisableUserIf90DaysNotLogin()
        {
            try
            {
                var sql = string.Format(@"Update t_pluss_user set USER_STATUS={0} 
                        where USER_CODE in(
                        select l.ACLO_USERLOGIID  from --,DATEDIFF(DAY, ACLO_LOGINDATETIME, GETDATE()) as daydiff,u.USER_STATUS
                        (select ACLO_USERLOGIID,MAX(ACLO_LOGINDATETIME) as ACLO_LOGINDATETIME from t_pluss_accesslog 
                        group by ACLO_USERLOGIID) as l 
                        left join t_pluss_user u on l.ACLO_USERLOGIID=u.USER_CODE
                        where DATEDIFF(DAY, ACLO_LOGINDATETIME, GETDATE())>=90 and u.USER_STATUS=1)", (int)UserStatus.Dormant);
                DbQueryManager.ExecuteNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
