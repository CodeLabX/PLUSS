﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// SegmentGateway Class.
    /// </summary>
    public class SegmentGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor
        /// </summary>
        public SegmentGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select segment list
        /// </summary>
        /// <returns>Returns a Segment list object.</returns>
        public List<Segment> SelectSegmentList()
        {
            List<Segment> segmentObjList = new List<Segment>();
            Segment segmentObj = null;
            string mSQL = "SELECT * FROM t_pluss_segment";
  
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    segmentObj = new Segment();
                    segmentObj.Id = Convert.ToInt32(xRs["SEGM_ID"]);
                    segmentObj.Code = Convert.ToString(xRs["SEGM_CODE"]);
                    segmentObj.Name = Convert.ToString(xRs["SEGM_NAME"]);
                    segmentObj.ProfessionId = Convert.ToInt32(xRs["SECM_PROFESSION_ID"]);
                    segmentObj.MaxLoanAmoutn = Convert.ToInt64(xRs["SECM_MAXLOANAMOUNT"]);
                    segmentObj.MinLoanAmoutn = Convert.ToInt64(xRs["SECM_MINLOANAMOUNT"]);
                    segmentObj.IR = Convert.ToDouble(xRs["SECM_IR"]);
                    segmentObj.Tenor = Convert.ToInt32(xRs["SECM_TENOR"]);
                    segmentObj.DBR29 = Convert.ToInt32(xRs["SECM_DBR29"]);
                    segmentObj.DBR50 = Convert.ToInt32(xRs["SECM_DBR50"]);
                    segmentObj.DBR99 = Convert.ToInt32(xRs["SECM_DBR99"]);
                    segmentObj.DBR1K = Convert.ToInt32(xRs["SECM_DBR1K"]);
                    segmentObj.L1MultiPlire29 = Convert.ToInt32(xRs["SECM_L1MULTIPLIER29"]);
                    segmentObj.L1MultiPlire50 = Convert.ToInt32(xRs["SECM_L1MULTIPLIER50"]);
                    segmentObj.L1MultiPlire99 = Convert.ToInt32(xRs["SECM_L1MULTIPLIER99"]);
                    segmentObj.L1MultiPlire1K = Convert.ToInt32(xRs["SECM_L1MULTIPLIER1K"]);
                    segmentObj.L2MultiPlire29 = Convert.ToInt32(xRs["SECM_L2MULTIPLIER29"]);
                    segmentObj.L2MultiPlire50 = Convert.ToInt32(xRs["SECM_L2MULTIPLIER50"]);
                    segmentObj.L2MultiPlire99 = Convert.ToInt32(xRs["SECM_L2MULTIPLIER99"]);
                    segmentObj.L2MultiPlire1K = Convert.ToInt32(xRs["SECM_L2MULTIPLIER1K"]);
                    segmentObj.L2TopMultiPlire29 = Convert.ToInt32(xRs["SECM_L2TOPMULTIPLIER29"]);
                    segmentObj.L2TopMultiPlire50 = Convert.ToInt32(xRs["SECM_L2TOPMULTIPLIER50"]);
                    segmentObj.L2TopMultiPlire99 = Convert.ToInt32(xRs["SECM_L2TOPMULTIPLIER99"]);
                    segmentObj.L2TopMultiPlire1K = Convert.ToInt32(xRs["SECM_L2TOPMULTIPLIER1K"]);
                    segmentObj.FirstPay = Convert.ToInt32(xRs["SECM_FIRSTPAY"]);
                    segmentObj.MultiPlire = Convert.ToInt32(xRs["SECM_MULTIPLIER"]);
                    segmentObj.MUEMul = Convert.ToInt32(xRs["SECM_MUEMUL"]);
                    segmentObj.MUELar = Convert.ToInt32(xRs["SECM_MUELAR"]);
                    segmentObj.MUELoc = Convert.ToInt32(xRs["SECM_MUELOC"]);
                    segmentObj.MinAge = Convert.ToInt32(xRs["SECM_MINAGE"]);
                    segmentObj.MaxAge = Convert.ToInt32(xRs["SECM_MAXAGE"]);
                    segmentObj.Description = Convert.ToString(xRs["SECM_DESCRIPTION"]);
                    segmentObj.MinIncome = Convert.ToInt64(xRs["SECM_MININCOME"]);
                    segmentObj.MaxAgeRemarks = Convert.ToString(xRs["SECM_MAXAGEREMARKS"]);

                    segmentObj.Experience = Convert.ToString(xRs["SECM_EXPERIENCE"]);
                    segmentObj.CriteriaCode = Convert.ToString(xRs["SECM_CRITERIACODE"]);
                    segmentObj.AssetCode = Convert.ToString(xRs["SECM_ASSETCODE"]);
                    segmentObj.ACRel = Convert.ToString(xRs["SECM_ACREL"]);
                    segmentObj.PhysicalVerification = Convert.ToString(xRs["SECM_PHYSICALVERIFICATION"]);
                    segmentObj.GuranteeReferenc = Convert.ToString(xRs["SECM_GURANTEEREFERENCE"]);
                    segmentObj.TelePhone = Convert.ToString(xRs["SECM_TELEPHONE"]);

                    segmentObjList.Add(segmentObj);
                }
                xRs.Close();
            }
            return segmentObjList;
        }
        /// <summary>
        /// This method select segment list
        /// </summary>
        /// <param name="professionId">Gets a profession id.</param>
        /// <returns>Returns a Segment list object.</returns>
        public List<Segment> SelectSegmentList(int professionId)
        {
            List<Segment> segmentObjList = new List<Segment>();
            Segment segmentObj = null;
            string mSQL = "SELECT * FROM t_pluss_segment WHERE SECM_PROFESSION_ID=" + professionId;
            ADODB.Recordset xRs = new ADODB.Recordset();
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                segmentObj = new Segment();
                segmentObj.Id = Convert.ToInt32(xRs.Fields["SEGM_ID"].Value);
                segmentObj.Code = Convert.ToString(xRs.Fields["SEGM_CODE"].Value);
                segmentObj.Name = Convert.ToString(xRs.Fields["SEGM_NAME"].Value);
                segmentObj.ProfessionId = Convert.ToInt32(xRs.Fields["SECM_PROFESSION_ID"].Value);
                segmentObj.MaxLoanAmoutn = Convert.ToInt64(xRs.Fields["SECM_MAXLOANAMOUNT"].Value);
                segmentObj.MinLoanAmoutn = Convert.ToInt64(xRs.Fields["SECM_MINLOANAMOUNT"].Value);
                segmentObj.IR = Convert.ToDouble(xRs.Fields["SECM_IR"].Value);
                segmentObj.Tenor = Convert.ToInt32(xRs.Fields["SECM_TENOR"].Value);
                segmentObj.DBR29 = Convert.ToInt32(xRs.Fields["SECM_DBR29"].Value);
                segmentObj.DBR50 = Convert.ToInt32(xRs.Fields["SECM_DBR50"].Value);
                segmentObj.DBR99 = Convert.ToInt32(xRs.Fields["SECM_DBR99"].Value);
                segmentObj.DBR1K = Convert.ToInt32(xRs.Fields["SECM_DBR1K"].Value);
                segmentObj.L1MultiPlire29 = Convert.ToInt32(xRs.Fields["SECM_L1MULTIPLIER29"].Value);
                segmentObj.L1MultiPlire50 = Convert.ToInt32(xRs.Fields["SECM_L1MULTIPLIER50"].Value);
                segmentObj.L1MultiPlire99 = Convert.ToInt32(xRs.Fields["SECM_L1MULTIPLIER99"].Value);
                segmentObj.L1MultiPlire1K = Convert.ToInt32(xRs.Fields["SECM_L1MULTIPLIER1K"].Value);
                segmentObj.L2MultiPlire29 = Convert.ToInt32(xRs.Fields["SECM_L2MULTIPLIER29"].Value);
                segmentObj.L2MultiPlire50 = Convert.ToInt32(xRs.Fields["SECM_L2MULTIPLIER50"].Value);
                segmentObj.L2MultiPlire99 = Convert.ToInt32(xRs.Fields["SECM_L2MULTIPLIER99"].Value);
                segmentObj.L2MultiPlire1K = Convert.ToInt32(xRs.Fields["SECM_L2MULTIPLIER1K"].Value);
                segmentObj.L2TopMultiPlire29 = Convert.ToInt32(xRs.Fields["SECM_L2TOPMULTIPLIER29"].Value);
                segmentObj.L2TopMultiPlire50 = Convert.ToInt32(xRs.Fields["SECM_L2TOPMULTIPLIER50"].Value);
                segmentObj.L2TopMultiPlire99 = Convert.ToInt32(xRs.Fields["SECM_L2TOPMULTIPLIER99"].Value);
                segmentObj.L2TopMultiPlire1K = Convert.ToInt32(xRs.Fields["SECM_L2TOPMULTIPLIER1K"].Value);
                segmentObj.FirstPay = Convert.ToInt32(xRs.Fields["SECM_FIRSTPAY"].Value);
                segmentObj.MultiPlire = Convert.ToInt32(xRs.Fields["SECM_MULTIPLIER"].Value);
                segmentObj.MUEMul = Convert.ToInt32(xRs.Fields["SECM_MUEMUL"].Value);
                segmentObj.MUELar = Convert.ToInt32(xRs.Fields["SECM_MUELAR"].Value);
                segmentObj.MUELoc = Convert.ToInt32(xRs.Fields["SECM_MUELOC"].Value);
                segmentObj.MinAge = Convert.ToInt32(xRs.Fields["SECM_MINAGE"].Value);
                segmentObj.MaxAge = Convert.ToInt32(xRs.Fields["SECM_MAXAGE"].Value);
                segmentObj.Description = Convert.ToString(xRs.Fields["SECM_DESCRIPTION"].Value);
                segmentObj.MinIncome = Convert.ToInt64(xRs.Fields["SECM_MININCOME"].Value);
                segmentObj.MaxAgeRemarks = Convert.ToString(xRs.Fields["SECM_MAXAGEREMARKS"].Value);

                segmentObj.Experience = Convert.ToString(xRs.Fields["SECM_EXPERIENCE"].Value);
                segmentObj.CriteriaCode = Convert.ToString(xRs.Fields["SECM_CRITERIACODE"].Value);
                segmentObj.AssetCode = Convert.ToString(xRs.Fields["SECM_ASSETCODE"].Value);
                segmentObj.ACRel = Convert.ToString(xRs.Fields["SECM_ACREL"].Value);
                segmentObj.PhysicalVerification = Convert.ToString(xRs.Fields["SECM_PHYSICALVERIFICATION"].Value);
                segmentObj.GuranteeReferenc = Convert.ToString(xRs.Fields["SECM_GURANTEEREFERENCE"].Value);
                segmentObj.TelePhone = Convert.ToString(xRs.Fields["SECM_TELEPHONE"].Value);

                segmentObjList.Add(segmentObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return segmentObjList;
        }
        /// <summary>
        /// This method select segment
        /// </summary>
        /// <param name="segmentId">Gets a segment id</param>
        /// <returns>Returns a Segment object.</returns>
        public Segment SelectSegment(int segmentId)
        {
            Segment segmentObj = new Segment();
            string mSQL = "SELECT * FROM t_pluss_segment WHERE SEGM_ID=" + segmentId;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    segmentObj.Id = Convert.ToInt32(xRs["SEGM_ID"]);
                    segmentObj.Code = Convert.ToString(xRs["SEGM_CODE"]);
                    segmentObj.Name = Convert.ToString(xRs["SEGM_NAME"]);
                    segmentObj.ProfessionId = Convert.ToInt32(xRs["SECM_PROFESSION_ID"]);
                    segmentObj.MaxLoanAmoutn = Convert.ToInt64(xRs["SECM_MAXLOANAMOUNT"]);
                    segmentObj.MinLoanAmoutn = Convert.ToInt64(xRs["SECM_MINLOANAMOUNT"]);
                    segmentObj.IR = Convert.ToDouble(xRs["SECM_IR"]);
                    segmentObj.Tenor = Convert.ToInt32(xRs["SECM_TENOR"]);
                    segmentObj.DBR29 = Convert.ToInt32(xRs["SECM_DBR29"]);
                    segmentObj.DBR50 = Convert.ToInt32(xRs["SECM_DBR50"]);
                    segmentObj.DBR99 = Convert.ToInt32(xRs["SECM_DBR99"]);
                    segmentObj.DBR1K = Convert.ToInt32(xRs["SECM_DBR1K"]);
                    segmentObj.L1MultiPlire29 = Convert.ToInt32(xRs["SECM_L1MULTIPLIER29"]);
                    segmentObj.L1MultiPlire50 = Convert.ToInt32(xRs["SECM_L1MULTIPLIER50"]);
                    segmentObj.L1MultiPlire99 = Convert.ToInt32(xRs["SECM_L1MULTIPLIER99"]);
                    segmentObj.L1MultiPlire1K = Convert.ToInt32(xRs["SECM_L1MULTIPLIER1K"]);
                    segmentObj.L2MultiPlire29 = Convert.ToInt32(xRs["SECM_L2MULTIPLIER29"]);
                    segmentObj.L2MultiPlire50 = Convert.ToInt32(xRs["SECM_L2MULTIPLIER50"]);
                    segmentObj.L2MultiPlire99 = Convert.ToInt32(xRs["SECM_L2MULTIPLIER99"]);
                    segmentObj.L2MultiPlire1K = Convert.ToInt32(xRs["SECM_L2MULTIPLIER1K"]);
                    segmentObj.L2TopMultiPlire29 = Convert.ToInt32(xRs["SECM_L2TOPMULTIPLIER29"]);
                    segmentObj.L2TopMultiPlire50 = Convert.ToInt32(xRs["SECM_L2TOPMULTIPLIER50"]);
                    segmentObj.L2TopMultiPlire99 = Convert.ToInt32(xRs["SECM_L2TOPMULTIPLIER99"]);
                    segmentObj.L2TopMultiPlire1K = Convert.ToInt32(xRs["SECM_L2TOPMULTIPLIER1K"]);
                    segmentObj.FirstPay = Convert.ToInt32(xRs["SECM_FIRSTPAY"]);
                    segmentObj.MultiPlire = Convert.ToInt32(xRs["SECM_MULTIPLIER"]);
                    segmentObj.MUEMul = Convert.ToInt32(xRs["SECM_MUEMUL"]);
                    segmentObj.MUELar = Convert.ToInt32(xRs["SECM_MUELAR"]);
                    segmentObj.MUELoc = Convert.ToInt32(xRs["SECM_MUELOC"]);
                    segmentObj.MinAge = Convert.ToInt32(xRs["SECM_MINAGE"]);
                    segmentObj.MaxAge = Convert.ToInt32(xRs["SECM_MAXAGE"]);
                    segmentObj.Description = Convert.ToString(xRs["SECM_DESCRIPTION"]);
                    segmentObj.MinIncome = Convert.ToInt64(xRs["SECM_MININCOME"]);
                    segmentObj.MaxAgeRemarks = Convert.ToString(xRs["SECM_MAXAGEREMARKS"]);

                    segmentObj.Experience = Convert.ToString(xRs["SECM_EXPERIENCE"]);
                    segmentObj.CriteriaCode = Convert.ToString(xRs["SECM_CRITERIACODE"]);
                    segmentObj.AssetCode = Convert.ToString(xRs["SECM_ASSETCODE"]);
                    segmentObj.ACRel = Convert.ToString(xRs["SECM_ACREL"]);
                    segmentObj.PhysicalVerification = Convert.ToString(xRs["SECM_PHYSICALVERIFICATION"]);
                    segmentObj.GuranteeReferenc = Convert.ToString(xRs["SECM_GURANTEEREFERENCE"]);
                    segmentObj.TelePhone = Convert.ToString(xRs["SECM_TELEPHONE"]);

                }
                xRs.Close();
            }
            return segmentObj;
        }
        /// <summary>
        /// This method provide the operation of insert segment info
        /// </summary>
        /// <param name="segmentObj">Gets a Segment object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendSegmentInfoInToDB(Segment segmentObj)
        {
            int returnValue = 0;
            Segment existiongSegmentObj = SelectSegment(segmentObj.Id);
            if (existiongSegmentObj.Id > 0)
            {
                returnValue = UpdateSegment(segmentObj);
            }
            else
            {
                returnValue = InsertSegment(segmentObj);
            }
            return returnValue;
        }
        private int InsertSegment(Segment segmentObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_segment (SEGM_CODE, SEGM_NAME,SECM_DESCRIPTION," +
                              "SECM_PROFESSION_ID, SECM_MAXLOANAMOUNT, " +
                              "SECM_MINLOANAMOUNT, SECM_MININCOME ,SECM_IR, SECM_TENOR, " +
                              "SECM_DBR29, SECM_DBR50, SECM_DBR99, " +
                              "SECM_DBR1K, SECM_L1MULTIPLIER29, " +
                              "SECM_L1MULTIPLIER50, SECM_L1MULTIPLIER99, " +
                              "SECM_L1MULTIPLIER1K, SECM_L2MULTIPLIER29, " +
                              "SECM_L2MULTIPLIER50, SECM_L2MULTIPLIER99, " +
                              "SECM_L2MULTIPLIER1K, SECM_L2TOPMULTIPLIER29," +
                              "SECM_L2TOPMULTIPLIER50, SECM_L2TOPMULTIPLIER99," +
                              "SECM_L2TOPMULTIPLIER1K, SECM_FIRSTPAY, " +
                              "SECM_MULTIPLIER, SECM_MUEMUL, SECM_MUELAR," +
                              "SECM_MUELOC, SECM_MINAGE, SECM_MAXAGE,SECM_MAXAGEREMARKS, SECM_PHYSICALVERIFICATION," +
                              "SECM_GURANTEEREFERENCE, SECM_EXPERIENCE,	SECM_ACREL, SECM_CRITERIACODE," +
                              "SECM_ASSETCODE, SECM_TELEPHONE) VALUES('" +
                              segmentObj.Code + "','" +
                              segmentObj.Name + "','" +
                              segmentObj.Description + "'," +
                              segmentObj.ProfessionId + "," +
                              segmentObj.MaxLoanAmoutn + "," +
                              segmentObj.MinLoanAmoutn + "," +
                              segmentObj.MinIncome + "," +
                              segmentObj.IR + "," +
                              segmentObj.Tenor + "," +
                              segmentObj.DBR29 + "," +
                              segmentObj.DBR50 + "," +
                              segmentObj.DBR99 + "," +
                              segmentObj.DBR1K + "," +
                              segmentObj.L1MultiPlire29 + "," +
                              segmentObj.L1MultiPlire50 + "," +
                              segmentObj.L1MultiPlire99 + "," +
                              segmentObj.L1MultiPlire1K + "," +
                              segmentObj.L2MultiPlire29 + "," +
                              segmentObj.L2MultiPlire50 + "," +
                              segmentObj.L2MultiPlire99 + "," +
                              segmentObj.L2MultiPlire1K + "," +
                              segmentObj.L2TopMultiPlire29 + "," +
                              segmentObj.L2TopMultiPlire50 + "," +
                              segmentObj.L2TopMultiPlire99 + "," +
                              segmentObj.L2TopMultiPlire1K + "," +
                              segmentObj.FirstPay + "," +
                              segmentObj.MultiPlire + "," +
                              segmentObj.MUEMul + "," +
                              segmentObj.MUELar + "," +
                              segmentObj.MUELoc + "," +
                              segmentObj.MinAge + "," +
                              segmentObj.MaxAge + ",'" +
                              segmentObj.MaxAgeRemarks + "','" +
                              segmentObj.PhysicalVerification + "','" +
                              segmentObj.GuranteeReferenc + "','" +
                              segmentObj.Experience + "','" +
                              segmentObj.ACRel + "','" +
                              segmentObj.CriteriaCode + "','" +
                              segmentObj.AssetCode + "','" +
                              segmentObj.TelePhone + "')";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide the operation of update segment info
        /// </summary>
        /// <param name="segmentObj">Gets a Segment object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateSegment(Segment segmentObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_segment SET " +
                              "SEGM_CODE='" + segmentObj.Code + "'," +
                              "SEGM_NAME='" + segmentObj.Name + "'," +
                              "SECM_DESCRIPTION='" + segmentObj.Description + "'," +
                              "SECM_PROFESSION_ID=" + segmentObj.ProfessionId + "," +
                              "SECM_MAXLOANAMOUNT=" + segmentObj.MaxLoanAmoutn + "," +
                              "SECM_MINLOANAMOUNT=" + segmentObj.MinLoanAmoutn + "," +
                              "SECM_MININCOME=" + segmentObj.MinIncome + "," +
                              "SECM_IR=" + segmentObj.IR + "," +
                              "SECM_TENOR=" + segmentObj.Tenor + "," +
                              "SECM_DBR29=" + segmentObj.DBR29 + "," +
                              "SECM_DBR50=" + segmentObj.DBR50 + "," +
                              "SECM_DBR99=" + segmentObj.DBR99 + "," +
                              "SECM_DBR1K=" + segmentObj.DBR1K + "," +
                              "SECM_L1MULTIPLIER29=" + segmentObj.L1MultiPlire29 + "," +
                              "SECM_L1MULTIPLIER50=" + segmentObj.L1MultiPlire50 + "," +
                              "SECM_L1MULTIPLIER99=" + segmentObj.L1MultiPlire99 + "," +
                              "SECM_L1MULTIPLIER1K=" + segmentObj.L1MultiPlire1K + "," +
                              "SECM_L2MULTIPLIER29=" + segmentObj.L2MultiPlire29 + "," +
                              "SECM_L2MULTIPLIER50=" + segmentObj.L2MultiPlire50 + "," +
                              "SECM_L2MULTIPLIER99=" + segmentObj.L2MultiPlire99 + "," +
                              "SECM_L2MULTIPLIER1K=" + segmentObj.L2MultiPlire1K + "," +
                              "SECM_L2TOPMULTIPLIER29=" + segmentObj.L2TopMultiPlire29 + "," +
                              "SECM_L2TOPMULTIPLIER50=" + segmentObj.L2TopMultiPlire50 + "," +
                              "SECM_L2TOPMULTIPLIER99=" + segmentObj.L2TopMultiPlire99 + "," +
                              "SECM_L2TOPMULTIPLIER1K=" + segmentObj.L2TopMultiPlire1K + "," +
                              "SECM_FIRSTPAY=" + segmentObj.FirstPay + "," +
                              "SECM_MULTIPLIER=" + segmentObj.MultiPlire + "," +
                              "SECM_MUEMUL=" + segmentObj.MUEMul + "," +
                              "SECM_MUELAR=" + segmentObj.MUELar + "," +
                              "SECM_MUELOC=" + segmentObj.MUELoc + "," +
                              "SECM_MINAGE=" + segmentObj.MinAge + "," +
                              "SECM_MAXAGE=" + segmentObj.MaxAge + "," +
                              "SECM_MAXAGEREMARKS='" + segmentObj.MaxAgeRemarks + "'," +
                              "SECM_PHYSICALVERIFICATION='" + segmentObj.PhysicalVerification + "'," +
                              "SECM_GURANTEEREFERENCE='" + segmentObj.GuranteeReferenc + "'," +
                              "SECM_EXPERIENCE='" + segmentObj.Experience + "'," +
                              "SECM_ACREL='" + segmentObj.ACRel + "'," +
                              "SECM_CRITERIACODE='" + segmentObj.CriteriaCode + "'," +
                              "SECM_ASSETCODE='" + segmentObj.AssetCode + "'," +
                              "SECM_TELEPHONE='" + segmentObj.TelePhone + "' WHERE SEGM_ID=" + segmentObj.Id;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method select segment code
        /// </summary>
        /// <returns>Returns list of segment code.</returns>
        public List<string> SelectSegmentCode()
        {
            List<string> segmentCodeList = new List<string>();
            string mSQL = "SELECT distinct SEGM_CODE FROM t_pluss_segment";
            ADODB.Recordset xRs = new ADODB.Recordset();
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                segmentCodeList.Add(Convert.ToString(xRs.Fields["SEGM_CODE"].Value));
                xRs.MoveNext();
            }
            xRs.Close();
            return segmentCodeList;
        }
        /// <summary>
        /// This method select profession list
        /// </summary>
        /// <returns>Returns a Profession list object.</returns>
        public List<Profession> SelectProfessionList()
        {
            List<Profession> professionObjList = new List<Profession>();
            string mSQL = "SELECT * FROM t_pluss_profession";
            Profession professionObj = null;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    professionObj = new Profession();
                    professionObj.Id = Convert.ToInt32(xRs["PROF_ID"]);
                    professionObj.Name = Convert.ToString(xRs["PROF_NAME"]);
                    professionObjList.Add(professionObj);
                }
                xRs.Close();
            }
            return professionObjList;
        }
    }
}
