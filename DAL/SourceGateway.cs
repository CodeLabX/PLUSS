﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    public class SourceGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        public SourceGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public Int32 GetSourceMaxId()
        {
            string mSQL = "SELECT MAX(SCSO_ID) as SCSO_ID FROM t_pluss_scbsource";
            return Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
        }
        public List<Source> GetSourceList()
        {
            List<Source> sourceObjList = new List<Source>();
            string mSQL = "SELECT * FROM t_pluss_scbsource";
            return sourceObjList = GetDataList(mSQL);
        }
        public List<Source> GetSourceList(string searchKey)
        {
            List<Source> sourceObjList = new List<Source>();
            string mSQL = "SELECT * FROM t_pluss_scbsource WHERE SCSO_NAME LIKE '%" + searchKey + "%' OR SCSO_REGIONNAME LIKE '%" + searchKey + "%'";
            return sourceObjList = GetDataList(mSQL);

        }
        public Source GetSource(Int32 sourceId)
        {
            string mSQL = "SELECT * FROM t_pluss_scbsource WHERE SCSO_ID=" + sourceId;
            return GetSourceData(mSQL);
        }

        public int SendSourceToDB(Source sourceObj)
        {
            int returnValue = 0;
            Source existingSourceObj = GetSource(sourceObj.SourceId);
            if (existingSourceObj != null)
            {
                sourceObj.SourceId = existingSourceObj.SourceId;
                returnValue = UpdateSource(sourceObj);
            }
            else
            {
                returnValue = InsertSource(sourceObj);
            }
            return returnValue;
        }
        private int InsertSource(Source sourceObj)
        {
            int returnValue = 0;
            int action_id = 1;
            int module_id = 3;

            try
            {
                string mSQL = "INSERT INTO t_pluss_scbsource (SCSO_NAME,SCSO_REGIONNAME) VALUES(" +
                              "'" + AddSlash(sourceObj.SourceName) + "'," +
                              "'" + AddSlash(sourceObj.SourceRegionName) + "')";
                Audit(action_id, module_id, mSQL, sourceObj);
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    returnValue = INSERTE;
                }
                else
                {
                    returnValue = ERROR;
                }
            }
            catch (Exception ex)
            {
                returnValue = ERROR;
            }
            return returnValue;
        }
        private int UpdateSource(Source sourceObj)
        {
            int returnValue = 0;
            int action_id = 2;
            int module_id = 3;

            try
            {
                string mSQL = "UPDATE t_pluss_scbsource SET " +
                "SCSO_NAME='" + AddSlash(sourceObj.SourceName) + "'," +
                "SCSO_REGIONNAME='" + AddSlash(sourceObj.SourceRegionName) + "' " +
                "WHERE SCSO_ID=" + sourceObj.SourceId;
                //for loan locator audit trail
                Audit(action_id, module_id, mSQL,sourceObj);
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    returnValue = UPDATE;
                }
                else
                {
                    returnValue = ERROR;
                }
            }
            catch (Exception ex)
            {
                returnValue = ERROR;
            }
            return returnValue;
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private List<Source> GetDataList(string mSQL)
        {
            List<Source> sourceObjList = new List<Source>();
            Source sourceObj = null;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    sourceObj = new Source();
                    sourceObj.SourceId = Convert.ToInt32(xRs["SCSO_ID"]);
                    sourceObj.SourceName = Convert.ToString(xRs["SCSO_NAME"]);
                    sourceObj.SourceRegionName = Convert.ToString(xRs["SCSO_REGIONNAME"]);
                    sourceObjList.Add(sourceObj);
                }
                xRs.Close();
            }
            return sourceObjList;
        }
        private Source GetSourceData(string mSQL)
        {
            Source sourceObj = null;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    sourceObj = new Source();
                    sourceObj.SourceId = Convert.ToInt32(xRs["SCSO_ID"]);
                    sourceObj.SourceName = Convert.ToString(xRs["SCSO_NAME"]);
                    sourceObj.SourceRegionName = Convert.ToString(xRs["SCSO_REGIONNAME"]);
                }
                xRs.Close();
            }
            return sourceObj;
        }

        public void Audit(int action_id, int module_id, string str_sql1, Source sourceObj)
        {
            string query= "insert into audit_trail(ID_LEARNER,ACTION_ID,MODULE_ID,QRY_DATE,QRY_TIME,QRY_STRING) " +
                          "values (" + sourceObj.UserId + "," + action_id + "," + module_id + ", CAST(getdate() as DATE) ,CAST (getdate() as Time),'" + AddSlash(str_sql1) + "')";
            DbQueryManager.ExecuteNonQuery(query);
        }
    }
}
