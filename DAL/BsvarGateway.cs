﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    /// <summary>
    /// BsvarGateway Class.
    /// </summary>
    public class BsvarGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;

        /// <summary>
        /// Constructor.
        /// </summary>
        public BsvarGateway()
        {
            dbMySQL = new DatabaseConnection();
        }

        //-------------------------------------------------------------
        /// <summary>
        /// This method provide insertion or update BSVER master info.
        /// </summary>
        /// <param name="bsverMasterObj">Gets a BSVERMaster object.</param>
        /// <returns>Returns a positive value if success.</returns>
        ///  bsverMasterObj.StatementType_Id,
        public int SendBSVERMasterInfoInToDB(BSVERMaster bsverMasterObj)
        {
            int returnValue = 0;
            BSVERMaster existingbsverMasterObj = SelectBSVERMasterInfo(bsverMasterObj.LlId, bsverMasterObj.Bank_Id, bsverMasterObj.Branch_Id, bsverMasterObj.Subject_Id, bsverMasterObj.AcNo);
            if (existingbsverMasterObj.Id == 0)
            {
                returnValue = InsertMasterInfo(bsverMasterObj);
            }
            else
            {
                bsverMasterObj.Id = existingbsverMasterObj.Id;
                returnValue = UpdateMasterInfo(bsverMasterObj);
            }
            return returnValue;
        }
        /// <summary>
        /// This method provide insetion of BSVER master info.
        /// </summary>
        /// <param name="bsverMasterObj">Gets a BSVERMaster object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertMasterInfo(BSVERMaster bsverMasterObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_bsver (BSVR_LLID," +
                    "BSVR_STATEMENTTYPE_ID," +
                    "BSVR_BANK_ID," +
                    "BSVR_BRANCH_ID," +
                    "BSVR_SUBJECT_ID," +
                    "BSVR_DEAR," +
                    "BSVR_EMAILADDRESS," +
                    "BSVR_SCBEMAILADDRESS," +
                    "BSVR_EMAILTEXT," +
                    "BSVR_CUSTOMERNAME," +
                    "BSVR_CUSTOMERADDRESS," +
                    "BSVR_ACNO," +
                    "BSVR_ACTYPE_ID," +
                    "BSVR_ACNAME," +
                    "BSVR_APPROVEDBY_ID," +
                    "BSVR_USER_ID," +
                    "BSVR_ENTRYDATE) VALUES( " +
                    bsverMasterObj.LlId + "," +
                    bsverMasterObj.StatementType_Id + "," +
                    bsverMasterObj.Bank_Id + "," +
                    bsverMasterObj.Branch_Id + "," +
                    bsverMasterObj.Subject_Id + ",'" +
                    AddSlash(bsverMasterObj.Dear) + "','" +
                    AddSlash(bsverMasterObj.EmailAddress) + "','" +
                    AddSlash(bsverMasterObj.SCBEmailAddress) + "','" +
                    AddSlash(bsverMasterObj.EmailText) + "','" +
                    AddSlash(bsverMasterObj.CustomerName) + "','" +
                    AddSlash(bsverMasterObj.CustomerAddress) + "','" +
                    bsverMasterObj.AcNo + "'," +
                    bsverMasterObj.AcType_Id + ",'" +
                    AddSlash(bsverMasterObj.AcName) + "'," +
                    bsverMasterObj.Approved_Id + "," +
                    bsverMasterObj.User_Id + ",'" +
                    bsverMasterObj.EntryDate.ToString("yyyy-MM-dd") + "')";

                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide update of BSVER master info.
        /// </summary>
        /// <param name="bsverMasterObj">Gets a BSVERMaster object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateMasterInfo(BSVERMaster bsverMasterObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_bsver SET " +
                    "BSVR_LLID=" + bsverMasterObj.LlId + "," +
                    "BSVR_STATEMENTTYPE_ID=" + bsverMasterObj.StatementType_Id + "," +
                    "BSVR_BANK_ID=" + bsverMasterObj.Bank_Id + "," +
                    "BSVR_BRANCH_ID=" + bsverMasterObj.Branch_Id + "," +
                    "BSVR_SUBJECT_ID=" + bsverMasterObj.Subject_Id + "," +
                    "BSVR_DEAR='" + AddSlash(bsverMasterObj.Dear) + "'," +
                    "BSVR_EMAILADDRESS='" + AddSlash(bsverMasterObj.EmailAddress) + "'," +
                    "BSVR_SCBEMAILADDRESS='" + AddSlash(bsverMasterObj.SCBEmailAddress) + "'," +
                    "BSVR_EMAILTEXT='" + AddSlash(bsverMasterObj.EmailText) + "'," +
                    "BSVR_CUSTOMERNAME='" + AddSlash(bsverMasterObj.CustomerName) + "'," +
                    "BSVR_CUSTOMERADDRESS='" + AddSlash(bsverMasterObj.CustomerAddress) + "'," +
                    "BSVR_ACNO='" + bsverMasterObj.AcNo + "'," +
                    "BSVR_ACTYPE_ID=" + bsverMasterObj.AcType_Id + "," +
                    "BSVR_ACNAME='" + AddSlash(bsverMasterObj.AcName) + "'," +
                    "BSVR_APPROVEDBY_ID=" + bsverMasterObj.Approved_Id + "," +
                    "BSVR_USER_ID=" + bsverMasterObj.User_Id + "," +
                    "BSVR_ENTRYDATE='" + bsverMasterObj.EntryDate.ToString("yyyy-MM-dd") + "' WHERE BSVR_ID=" + bsverMasterObj.Id;

                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        //-------------------------------------------------------------
        /// <summary>
        /// This method provide insertion or update BSVER details info.
        /// </summary>
        /// <param name="bsverDetailsObjList">Gets a BSVERDetails list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendBSVERDetailsInfoInToDB(List<BSVERDetails> bsverDetailsObjList)
        {
            List<BSVERDetails> insertBSVERDetailsList = new List<BSVERDetails>();
            List<BSVERDetails> updateBSVERDetailsList = new List<BSVERDetails>();
            int returnValue = 0;
            foreach (BSVERDetails tempBSVERDetails in bsverDetailsObjList)
            {
                if (tempBSVERDetails.OldId > 0)
                {
                    updateBSVERDetailsList.Add(tempBSVERDetails);
                }
                else
                {
                    insertBSVERDetailsList.Add(tempBSVERDetails);
                }
            }
            if (insertBSVERDetailsList.Count > 0)
            {
                returnValue = InsertDetailsInfo(insertBSVERDetailsList);
            }
            if (updateBSVERDetailsList.Count > 0)
            {
                returnValue = UpdateDetailsInfo(updateBSVERDetailsList);
            }
            return returnValue;

        }
        /// <summary>
        /// This method provide insertion of BSVER details info.
        /// </summary>
        /// <param name="bsverDetailsObj">Gets a  BSVERDetails list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertDetailsInfo(List<BSVERDetails> bsverDetailsObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_bsverdetails(" +
                    "BSDE_BSVER_ID," +
                    "BSDE_SLNO," +
                    "BSDE_PARTICULAR," +
                    "BSDE_DATE," +
                    "BSDE_DESCRIPTION," +
                    "BSDE_WITHDRAWL," +
                    "BSDE_DIPOSIT," +
                    "BSDE_BALANCE)VALUES ";
                for (int i = 0; i < bsverDetailsObj.Count; i++)
                {
                    mSQL = mSQL + "(" +
                    bsverDetailsObj[i].BasVerMaster_Id + "," +
                    bsverDetailsObj[i].SlNo + ",'" +
                    bsverDetailsObj[i].Particular + "','" +
                    bsverDetailsObj[i].Date.ToString("yyyy-MM-dd") + "','" +
                    AddSlash(bsverDetailsObj[i].Description) + "'," +
                    bsverDetailsObj[i].Withdrawl + "," +
                    bsverDetailsObj[i].Diposit + "," +
                    bsverDetailsObj[i].Balance + "),";
                }
                mSQL = mSQL.Substring(0, mSQL.Length - 1);
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method provide update of BSVER details info.
        /// </summary>
        /// <param name="bsverDetailsObj">Gets a  BSVERDetails list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateDetailsInfo(List<BSVERDetails> bsverDetailsObj)
        {
            bool flag = false;
            try
            {
                for (int i = 0; i < bsverDetailsObj.Count; i++)
                {
                    string mSQL = "";
                    mSQL = "UPDATE t_pluss_bsverdetails SET " +
                    "BSDE_BSVER_ID=" + bsverDetailsObj[i].BasVerMaster_Id + "," +
                    "BSDE_SLNO=" + bsverDetailsObj[i].SlNo + "," +
                    "BSDE_PARTICULAR='" + bsverDetailsObj[i].Particular + "'," +
                    "BSDE_DATE='" + bsverDetailsObj[i].Date.ToString("yyyy-MM-dd") + "'," +
                    "BSDE_DESCRIPTION='" + AddSlash(bsverDetailsObj[i].Description) + "'," +
                    "BSDE_WITHDRAWL=" + bsverDetailsObj[i].Withdrawl + "," +
                    "BSDE_DIPOSIT=" + bsverDetailsObj[i].Diposit + "," +
                    "BSDE_BALANCE=" + bsverDetailsObj[i].Balance + " WHERE BSDE_ID=" + bsverDetailsObj[i].OldId;
                    if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                    {
                        flag = true;
                    }
                }
                if (flag == true)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method select BSVer master id.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <param name="statementTypeId">Gets a statement type id</param>
        /// <param name="bankId">Gets a bank id.</param>
        /// <param name="branchId">Gets a branch id.</param>
        /// <param name="subjectId">Gets a subject id.</param>
        /// <param name="acNo">Gets a account no.</param>
        /// <returns>Returns a BSVER master id.</returns>
        public Int64 SelectBSVerMasterId(Int64 lLId, Int16 statementTypeId, Int16 bankId, Int16 branchId, Int16 subjectId, string acNo)
        {
            string mSQL = "SELECT BSVR_ID FROM t_pluss_bsver WHERE BSVR_LLID=" + lLId + " AND BSVR_STATEMENTTYPE_ID=" + statementTypeId + " AND BSVR_BANK_ID=" + bankId + " AND BSVR_BRANCH_ID=" + branchId + " AND BSVR_SUBJECT_ID=" + subjectId + " AND BSVR_ACNO='" + acNo + "'";
            return Convert.ToInt64("0" + DbQueryManager.ExecuteScalar(mSQL));
        }
        /// <summary>
        /// This method select account name.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <param name="bankId">Gets a bank id.</param>
        /// <param name="branchId">Gets a branch id.</param>
        /// <param name="acNo">Gets a account no.</param>
        /// <returns>Returns a account name.</returns>
        public string SelectAccountName(Int64 lLId, Int16 bankId, Int16 branchId, string acNo)
        {
            string mSQL = "SELECT BAST_ACNAME FROM t_pluss_bankstatement WHERE BAST_LLID=" + lLId + " AND BAST_BANK_ID=" + bankId + " AND BAST_BRANCH_ID=" + branchId + " AND BAST_ACNO='" + acNo + "'";

            return (string) DbQueryManager.ExecuteScalar(mSQL);
        }
        /// <summary>
        /// This method select customer name address.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a customer name address.</returns>
        public string SelectCustomerNameAddress(Int64 lLId)
        {
            string returnString = null;
            string mSQL = "SELECT CUST_NM,RES_ADD FROM loan_app_info WHERE LOAN_APPLI_ID=" + lLId;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    returnString = xRs["CUST_NM"].ToString() + "#" + xRs["RES_ADD"].ToString();
                }
                xRs.Close();
            }
            return returnString;
        }
        /// <summary>
        /// This method select BSVER master info.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <param name="statementTypeId">Gets a statement type id</param>
        /// <param name="bankId">Gets a bank id.</param>
        /// <param name="branchId">Gets a branch id.</param>
        /// <param name="subjectId">Gets subject id.</param>
        /// <param name="acNo">Gets a account no.</param>
        /// <returns>Returns a BSVERMaster object.</returns>
        /// AND BSVR_STATEMENTTYPE_ID=" + statementTypeId + "
        public BSVERMaster SelectBSVERMasterInfo(Int64 lLId, Int16 bankId, Int16 branchId, Int16 subjectId, string acNo)
        {
            BSVERMaster bsverMasterObj = new BSVERMaster();
            string mSQL = "SELECT * FROM t_pluss_bsver WHERE BSVR_LLID=" + lLId + " AND BSVR_BANK_ID=" + bankId + " AND BSVR_BRANCH_ID=" + branchId + " AND BSVR_SUBJECT_ID=" + subjectId + " AND BSVR_ACNO='" + acNo + "'";
             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                if (xRs.Read())
                {
                    bsverMasterObj.Id = Convert.ToInt64(xRs["BSVR_ID"]);
                    bsverMasterObj.LlId = Convert.ToInt64(xRs["BSVR_LLID"]);
                    bsverMasterObj.StatementType_Id = Convert.ToInt16(xRs["BSVR_STATEMENTTYPE_ID"]);
                    bsverMasterObj.Bank_Id = Convert.ToInt16(xRs["BSVR_BANK_ID"]);
                    bsverMasterObj.Branch_Id = Convert.ToInt16(xRs["BSVR_BRANCH_ID"]);
                    bsverMasterObj.Subject_Id = Convert.ToInt16(xRs["BSVR_SUBJECT_ID"]);
                    bsverMasterObj.Dear = Convert.ToString(xRs["BSVR_DEAR"]);
                    bsverMasterObj.EmailAddress = Convert.ToString(xRs["BSVR_EMAILADDRESS"]);
                    bsverMasterObj.SCBEmailAddress = Convert.ToString(xRs["BSVR_SCBEMAILADDRESS"]);
                    bsverMasterObj.EmailText = Convert.ToString(xRs["BSVR_EMAILTEXT"]);
                    bsverMasterObj.CustomerName = Convert.ToString(xRs["BSVR_CUSTOMERNAME"]);
                    bsverMasterObj.CustomerAddress = Convert.ToString(xRs["BSVR_CUSTOMERADDRESS"]);
                    bsverMasterObj.AcNo = Convert.ToString(xRs["BSVR_ACNO"]);
                    bsverMasterObj.AcType_Id = Convert.ToInt16(xRs["BSVR_ACTYPE_ID"]);
                    bsverMasterObj.AcName = Convert.ToString(xRs["BSVR_ACNAME"]);
                    bsverMasterObj.Approved_Id = Convert.ToInt32(xRs["BSVR_APPROVEDBY_ID"]);
                    bsverMasterObj.User_Id = Convert.ToInt32(xRs["BSVR_USER_ID"]);
                    bsverMasterObj.EntryDate = Convert.ToDateTime(xRs["BSVR_ENTRYDATE"]);

                }
                xRs.Close();
            }
            return bsverMasterObj;
        }
        /// <summary>
        /// This method select BASVER report info list.
        /// </summary>
        /// <param name="bsVerMasterId">Gets a bsver master id.</param>
        /// <returns>Returns a BASVERReportInfo list object.</returns>
        public List<BASVERReportInfo> SelectBASVERReportInfoList(Int64 bsVerMasterId)
        {
            List<BASVERReportInfo> bASVERReportInfoObjLIst = new List<BASVERReportInfo>();
            BASVERReportInfo bASVERReportInfoObj = null;
            //Int64 bsVerMasterId = 0;
            //string mSQL = "SELECT * FROM t_pluss_bsver WHERE BSVR_LLID=" + lLId + " AND BSVR_STATEMENTTYPE_ID=" + statementTypeId + " AND BSVR_BANK_ID=" + bankId + " AND BSVR_BRANCH_ID=" + branchId + " AND BSVR_SUBJECT_ID=" + subjectId + " AND BSVR_ACNO='" + acNo + "'";
            string mSQL = "SELECT BSVR_ID,BSVR_LLID,CUST_NM,COMPA_NM,BSVR_STATEMENTTYPE_ID,BSVR_BANK_ID,BANK_NM,BSVR_BRANCH_ID,BRAN_NAME,BRAN_ADDRESS," +
            "LOCA_NAME,BSVR_SUBJECT_ID,SUBJ_NAME,BSVR_DEAR,BSVR_EMAILADDRESS,BSVR_SCBEMAILADDRESS,BSVR_EMAILTEXT,BSVR_CUSTOMERNAME,BSVR_CUSTOMERADDRESS,BSVR_ACNO," +
            "BSVR_ACTYPE_ID,BSVR_ACNAME,BSVR_APPROVEDBY_ID,USER_NAME,USER_DESIGNATION,BSDE_ID,BSDE_SLNO,BSDE_PARTICULAR,BSDE_DATE,BSDE_DESCRIPTION,BSDE_WITHDRAWL,BSDE_DIPOSIT," +
            "BSDE_BALANCE,BSVR_ENTRYDATE FROM t_pluss_bsver LEFT JOIN t_pluss_bank ON BSVR_BANK_ID=BANK_ID LEFT JOIN t_pluss_branch ON BSVR_BRANCH_ID=BRAN_ID LEFT JOIN t_pluss_subject ON " +
            "BSVR_SUBJECT_ID=SUBJ_ID LEFT JOIN t_pluss_user ON BSVR_APPROVEDBY_ID=USER_ID LEFT JOIN t_pluss_location ON " +
            "BRAN_LOCATION_ID=LOCA_ID LEFT JOIN loan_app_info ON  BSVR_LLID=LOAN_APPLI_ID LEFT JOIN t_pluss_bsverdetails ON BSVR_ID=BSDE_BSVER_ID WHERE BSDE_BSVER_ID=" + bsVerMasterId;
             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    bASVERReportInfoObj = new BASVERReportInfo();
                    bASVERReportInfoObj.BasVerMasterId = Convert.ToInt64(xRs["BSVR_ID"]);
                    bASVERReportInfoObj.LlId = Convert.ToInt64(xRs["BSVR_LLID"]);
                    bASVERReportInfoObj.StatementType_Id = Convert.ToInt16(xRs["BSVR_STATEMENTTYPE_ID"]);
                    bASVERReportInfoObj.Bank_Id = Convert.ToInt16(xRs["BSVR_BANK_ID"]);
                    bASVERReportInfoObj.BankName = Convert.ToString(xRs["BANK_NM"]);
                    bASVERReportInfoObj.Branch_Id = Convert.ToInt16(xRs["BSVR_BRANCH_ID"]);
                    bASVERReportInfoObj.BranchName = Convert.ToString(xRs["BRAN_NAME"]);
                    bASVERReportInfoObj.BranchAddress = Convert.ToString(xRs["BRAN_ADDRESS"]);
                    bASVERReportInfoObj.LocationName = Convert.ToString(xRs["LOCA_NAME"]);
                    bASVERReportInfoObj.Subject_Id = Convert.ToInt16(xRs["BSVR_SUBJECT_ID"]);
                    bASVERReportInfoObj.SubjectName = Convert.ToString(xRs["SUBJ_NAME"]);
                    bASVERReportInfoObj.Dear = Convert.ToString(xRs["BSVR_DEAR"]);
                    bASVERReportInfoObj.EmailAddress = Convert.ToString(xRs["BSVR_EMAILADDRESS"]);
                    bASVERReportInfoObj.SCBEmailAddress = Convert.ToString(xRs["BSVR_SCBEMAILADDRESS"]);
                    bASVERReportInfoObj.EmailText = Convert.ToString(xRs["BSVR_EMAILTEXT"]);
                    bASVERReportInfoObj.CustomerName = Convert.ToString(xRs["BSVR_CUSTOMERNAME"]);
                    bASVERReportInfoObj.BusinessName = Convert.ToString(xRs["COMPA_NM"]);
                    bASVERReportInfoObj.CustomerAddress = Convert.ToString(xRs["BSVR_CUSTOMERADDRESS"]);
                    bASVERReportInfoObj.AcNo = Convert.ToString(xRs["BSVR_ACNO"]);
                    bASVERReportInfoObj.AcType_Id = Convert.ToInt16(xRs["BSVR_ACTYPE_ID"]);
                    bASVERReportInfoObj.AcName = Convert.ToString(xRs["BSVR_ACNAME"]);
                    bASVERReportInfoObj.Approved_Id = Convert.ToInt32(xRs["BSVR_APPROVEDBY_ID"]);
                    bASVERReportInfoObj.ApproverName = Convert.ToString(xRs["USER_NAME"]);
                    bASVERReportInfoObj.ApproverDesignation = Convert.ToString(xRs["USER_DESIGNATION"]);
                    bASVERReportInfoObj.BasVerDetailsId = Convert.ToInt64(xRs["BSDE_ID"]);
                    bASVERReportInfoObj.SlNo = Convert.ToInt16(xRs["BSDE_SLNO"]);
                    bASVERReportInfoObj.Particular = Convert.ToString(xRs["BSDE_PARTICULAR"]);
                    bASVERReportInfoObj.Date = Convert.ToDateTime(xRs["BSDE_DATE"]);
                    bASVERReportInfoObj.Description = Convert.ToString(xRs["BSDE_DESCRIPTION"]);
                    bASVERReportInfoObj.Withdrawl = Convert.ToDouble(xRs["BSDE_WITHDRAWL"]);
                    bASVERReportInfoObj.Diposit = Convert.ToDouble(xRs["BSDE_DIPOSIT"]);
                    bASVERReportInfoObj.Balance = Convert.ToDouble(xRs["BSDE_BALANCE"]);
                    bASVERReportInfoObj.EntryDate = Convert.ToDateTime(xRs["BSVR_ENTRYDATE"]);
                    bASVERReportInfoObjLIst.Add(bASVERReportInfoObj);
                }
                xRs.Close();
            }
            return bASVERReportInfoObjLIst;
        }
        public BASVERReportInfo SelectBASVERReportInfo(Int64 lLId, Int16 bankId, Int16 branchId, string acNo)
        {
            BASVERReportInfo bASVERReportInfoObj = null;
            string mSQL = "SELECT BSVR_ID,BSVR_LLID,CUST_NM,COMPA_NM,BSVR_STATEMENTTYPE_ID,BSVR_BANK_ID,BANK_NM,BSVR_BRANCH_ID,BRAN_NAME,BRAN_ADDRESS," +
            "LOCA_NAME,BSVR_SUBJECT_ID,SUBJ_NAME,BSVR_DEAR,BSVR_EMAILADDRESS,BSVR_SCBEMAILADDRESS,BSVR_EMAILTEXT,BSVR_CUSTOMERNAME,BSVR_CUSTOMERADDRESS,BSVR_ACNO," +
            "BSVR_ACTYPE_ID,BSVR_ACNAME,BSVR_APPROVEDBY_ID,USER_NAME,USER_DESIGNATION,BSDE_ID,BSDE_SLNO,BSDE_PARTICULAR,BSDE_DATE,BSDE_DESCRIPTION,BSDE_WITHDRAWL,BSDE_DIPOSIT," +
            "BSDE_BALANCE,BSVR_ENTRYDATE FROM t_pluss_bsver LEFT JOIN t_pluss_bank ON BSVR_BANK_ID=BANK_ID LEFT JOIN t_pluss_branch ON BSVR_BRANCH_ID=BRAN_ID LEFT JOIN t_pluss_subject ON " +
            "BSVR_SUBJECT_ID=SUBJ_ID LEFT JOIN t_pluss_user ON BSVR_APPROVEDBY_ID=USER_ID LEFT JOIN t_pluss_location ON " +
            "BRAN_LOCATION_ID=LOCA_ID LEFT JOIN loan_app_info ON  BSVR_LLID=LOAN_APPLI_ID LEFT JOIN t_pluss_bsverdetails ON BSVR_ID=BSDE_BSVER_ID WHERE BSVR_LLID=" + lLId + " AND BSVR_BANK_ID=" + bankId + " AND BSVR_BRANCH_ID=" + branchId +
            " AND BSVR_ACNO='" + acNo + "'";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                if (xRs.Read())
                {
                    bASVERReportInfoObj = new BASVERReportInfo();
                    bASVERReportInfoObj.BasVerMasterId = Convert.ToInt64(xRs["BSVR_ID"]);
                    bASVERReportInfoObj.LlId = Convert.ToInt64(xRs["BSVR_LLID"]);
                    bASVERReportInfoObj.StatementType_Id = Convert.ToInt16(xRs["BSVR_STATEMENTTYPE_ID"]);
                    bASVERReportInfoObj.Bank_Id = Convert.ToInt16(xRs["BSVR_BANK_ID"]);
                    bASVERReportInfoObj.BankName = Convert.ToString(xRs["BANK_NM"]);
                    bASVERReportInfoObj.Branch_Id = Convert.ToInt16(xRs["BSVR_BRANCH_ID"]);
                    bASVERReportInfoObj.BranchName = Convert.ToString(xRs["BRAN_NAME"]);
                    bASVERReportInfoObj.BranchAddress = Convert.ToString(xRs["BRAN_ADDRESS"]);
                    bASVERReportInfoObj.LocationName = Convert.ToString(xRs["LOCA_NAME"]);
                    bASVERReportInfoObj.Subject_Id = Convert.ToInt16(xRs["BSVR_SUBJECT_ID"]);
                    bASVERReportInfoObj.SubjectName = Convert.ToString(xRs["SUBJ_NAME"]);
                    bASVERReportInfoObj.Dear = Convert.ToString(xRs["BSVR_DEAR"]);
                    bASVERReportInfoObj.EmailAddress = Convert.ToString(xRs["BSVR_EMAILADDRESS"]);
                    bASVERReportInfoObj.SCBEmailAddress = Convert.ToString(xRs["BSVR_SCBEMAILADDRESS"]);
                    bASVERReportInfoObj.EmailText = Convert.ToString(xRs["BSVR_EMAILTEXT"]);
                    bASVERReportInfoObj.CustomerName = Convert.ToString(xRs["BSVR_CUSTOMERNAME"]);
                    bASVERReportInfoObj.BusinessName = Convert.ToString(xRs["COMPA_NM"]);
                    bASVERReportInfoObj.CustomerAddress = Convert.ToString(xRs["BSVR_CUSTOMERADDRESS"]);
                    bASVERReportInfoObj.AcNo = Convert.ToString(xRs["BSVR_ACNO"]);
                    bASVERReportInfoObj.AcType_Id = Convert.ToInt16(xRs["BSVR_ACTYPE_ID"]);
                    bASVERReportInfoObj.AcName = Convert.ToString(xRs["BSVR_ACNAME"]);
                    bASVERReportInfoObj.Approved_Id = Convert.ToInt32(xRs["BSVR_APPROVEDBY_ID"]);
                    bASVERReportInfoObj.ApproverName = Convert.ToString(xRs["USER_NAME"]);
                    bASVERReportInfoObj.ApproverDesignation = Convert.ToString(xRs["USER_DESIGNATION"]);
                    bASVERReportInfoObj.BasVerDetailsId = Convert.ToInt64(xRs["BSDE_ID"]);
                    bASVERReportInfoObj.SlNo = Convert.ToInt16(xRs["BSDE_SLNO"]);
                    bASVERReportInfoObj.Particular = Convert.ToString(xRs["BSDE_PARTICULAR"]);
                    bASVERReportInfoObj.Date = Convert.ToDateTime(xRs["BSDE_DATE"]);
                    bASVERReportInfoObj.Description = Convert.ToString(xRs["BSDE_DESCRIPTION"]);
                    bASVERReportInfoObj.Withdrawl = Convert.ToDouble(xRs["BSDE_WITHDRAWL"]);
                    bASVERReportInfoObj.Diposit = Convert.ToDouble(xRs["BSDE_DIPOSIT"]);
                    bASVERReportInfoObj.Balance = Convert.ToDouble(xRs["BSDE_BALANCE"]);
                    bASVERReportInfoObj.EntryDate = Convert.ToDateTime(xRs["BSVR_ENTRYDATE"]);
                }
                xRs.Close();
            }
            return bASVERReportInfoObj;
        }
        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
