﻿using System;
using System.Data.Common;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    public class LoanCounterGateway
    {
        public LoanCounter GetLastCounterObject()
        {
            LoanCounter aLoanCounter = null;
            string queryString =
                "SELECT DAR_ID, DAR_DATE, DAR_RCFL, " +
                "DAR_RCPLSAL, DAR_RCPLBIZ, DAR_RCIPF, " +
                "DAR_RCSTFL, DAR_PEFL, DAR_PEPLSAL, " +
                "DAR_PEPLBIZ, DAR_PEIPF, DAR_PESTFL, " +
                "DAR_POFL, DAR_POPLSAL, DAR_POPLBIZ, " +
                "DAR_POIPF, DAR_POSTFL " +
                "FROM t_pluss_dailyactivityreport " +
                "WHERE DAR_ID = " +
                "(SELECT MAX(DAR_ID) FROM t_pluss_dailyactivityreport)";

            try
            {
                DbDataReader dr = DbQueryManager.ExecuteReader(queryString);

                if (dr != null && dr.Read())
                {
                    aLoanCounter =
                        new LoanCounter
                        {
                            Id = Convert.ToInt32(dr["DAR_ID"].ToString()),
                            DATE = Convert.ToDateTime(dr["DAR_DATE"].ToString()),
                            RCFL = Convert.ToInt32(dr["RCFL"].ToString()),
                            RCPL_SAL = Convert.ToInt32(dr["RCPL_SAL"].ToString()),
                            RCPL_BIZ = Convert.ToInt32(dr["RCPL_BIZ"].ToString()),
                            RCIPF = Convert.ToInt32(dr["RCIPF"].ToString()),
                            RCSTFL = Convert.ToInt32(dr["RCSTFL"].ToString()),
                            PEFL = Convert.ToInt32(dr["PEFL"].ToString()),
                            PEPL_SAL = Convert.ToInt32(dr["PEPL_SAL"].ToString()),
                            PEPL_BIZ = Convert.ToInt32(dr["PEPL_BIZ"].ToString()),
                            PEIPF = Convert.ToInt32(dr["PEIPF"].ToString()),
                            PESTFL = Convert.ToInt32(dr["PESTFL"].ToString()),
                            POFL = Convert.ToInt32(dr["POFL"].ToString()),
                            POPL_SAL = Convert.ToInt32(dr["POPL_SAL"].ToString()),
                            POPL_BIZ = Convert.ToInt32(dr["POPL_BIZ"].ToString()),
                            POIPF = Convert.ToInt32(dr["POIPF"].ToString()),
                            POSTFL = Convert.ToInt32(dr["POSTFL"].ToString())
                        };
                }
                return aLoanCounter;
            }
            catch { return aLoanCounter; }
        }

        public int UpdateReceivedLoan()
        {
            LoanCounter lastReceivedObject = GetLastCounterObject();

            int status = 0;
            string updateQueryString =
                "UPDATE t_pluss_dailyactivityreport " +
                "SET " +
                "DAR_RCFL = '" + (lastReceivedObject.RCFL + 1) + "', " +
                "DAR_RCPL_SAL = '" + (lastReceivedObject.RCPL_SAL + 1) + "', " +
                "DAR_RCPL_BIZ = '" + (lastReceivedObject.RCPL_BIZ + 1) + "', " +
                "DAR_RCIPF = '" + (lastReceivedObject.RCIPF + 1) + "', " +
                "DAR_RCSTFL = '" + (lastReceivedObject.RCSTFL + 1) + "'" +
                "WHERE " +
                "DAR_ID = '" + lastReceivedObject.Id + "'";

            try
            {
                status = DbQueryManager.ExecuteNonQuery(updateQueryString);
            }
            catch (Exception exp)
            {
                return status;
            }
            
            return status;
        }

        public int InsertReceivedLoan()
        {
            LoanCounter lastReceivedObject = GetLastCounterObject();

            int status = 0;
            string insertQueryString =
                "INSERT INTO t_pluss_dailyactivityreport " +
                "(DAR_DATE, DAR_RCFL, DAR_RCPLSAL, DAR_RCPLBIZ, DAR_RCIPF, DAR_RCSTFL) " +
                "VALUES " +
                "('" + DateTime.Now + "','" + (lastReceivedObject.RCFL + 1) + "','" + (lastReceivedObject.RCPL_SAL + 1) +
                "','" + (lastReceivedObject.RCPL_BIZ + 1) + "','" + (lastReceivedObject.RCIPF + 1) + "', " +
                "'" + (lastReceivedObject.RCSTFL + 1) + "')";

            try
            {
                status = DbQueryManager.ExecuteNonQuery(insertQueryString);
            }
            catch (Exception exp)
            {
                return status;
            }

            return status;
        }
        

        public int UpdatePendingLoan()
        {
            LoanCounter lastPendingObject = GetLastCounterObject();

            int status = 0;
            string updateQueryString =
                "UPDATE t_pluss_dailyactivityreport " +
                "SET " +
                "DAR_PEFL = '" + (lastPendingObject.PEFL + 1) + "', " +
                "DAR_PEPL_SAL = '" + (lastPendingObject.PEPL_SAL + 1) + "', " +
                "DAR_PEPL_BIZ = '" + (lastPendingObject.PEPL_BIZ + 1) + "', " +
                "DAR_PEIPF = '" + (lastPendingObject.PEIPF + 1) + "', " +
                "DAR_PESTFL = '" + (lastPendingObject.PESTFL + 1) + "'" +
                "WHERE " +
                "DAR_ID = '" + lastPendingObject.Id + "'";

            try
            {
                status = DbQueryManager.ExecuteNonQuery(updateQueryString);
            }
            catch (Exception exp)
            {
                return status;
            }

            return status;
        }

        public int InsertPendingLoan()
        {
            LoanCounter lastPendingObject = GetLastCounterObject();

            int status = 0;
            string insertQueryString =
                "INSERT INTO t_pluss_dailyactivityreport " +
                "(DAR_DATE, DAR_PEFL, DAR_PEPLSAL, DAR_PEPLBIZ, DAR_PEIPF, DAR_PESTFL) " +
                "VALUES " +
                "('" + DateTime.Now + "','" + (lastPendingObject.PEFL + 1) + "','" + (lastPendingObject.PEPL_SAL + 1) +
                "','" + (lastPendingObject.PEPL_BIZ + 1) + "','" + (lastPendingObject.PEIPF + 1) + "', " +
                "'" + (lastPendingObject.PESTFL + 1) + "')";

            try
            {
                status = DbQueryManager.ExecuteNonQuery(insertQueryString);
            }
            catch (Exception exp)
            {
                return status;
            }

            return status;
        }


        public int UpdateProcessingLoan()
        {
            LoanCounter lastPendingObject = GetLastCounterObject();

            int status = 0;
            string updateQueryString =
                "UPDATE t_pluss_dailyactivityreport " +
                "SET " +
                "DAR_POFL = '" + (lastPendingObject.POFL + 1) + "', " +
                "DAR_POPL_SAL = '" + (lastPendingObject.POPL_SAL + 1) + "', " +
                "DAR_POPL_BIZ = '" + (lastPendingObject.POPL_BIZ + 1) + "', " +
                "DAR_POIPF = '" + (lastPendingObject.POIPF + 1) + "', " +
                "DAR_POSTFL = '" + (lastPendingObject.POSTFL + 1) + "'" +
                "WHERE " +
                "DAR_ID = '" + lastPendingObject.Id + "'";

            try
            {
                status = DbQueryManager.ExecuteNonQuery(updateQueryString);
            }
            catch (Exception exp)
            {
                return status;
            }

            return status;
        }

        public int InsertProcessingLoan()
        {
            LoanCounter lastProcessingObject = GetLastCounterObject();

            int status = 0;
            string insertQueryString =
                "INSERT INTO t_pluss_dailyactivityreport " +
                "(DAR_DATE, DAR_POFL, DAR_POPLSAL, DAR_POPLBIZ, DAR_POIPF, DAR_POSTFL) " +
                "VALUES " +
                "('" + DateTime.Now + "','" + (lastProcessingObject.POFL + 1) + "','" + (lastProcessingObject.POPL_SAL + 1) +
                "','" + (lastProcessingObject.POPL_BIZ + 1) + "','" + (lastProcessingObject.POIPF + 1) + "', " +
                "'" + (lastProcessingObject.POSTFL + 1) + "')";

            try
            {
                status = DbQueryManager.ExecuteNonQuery(insertQueryString);
            }
            catch (Exception exp)
            {
                return status;
            }

            return status;
        }

        
    }
}
