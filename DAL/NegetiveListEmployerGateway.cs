﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data;
using Bat.Common;
using Utilities;
using AzUtilities;
using AzUtilities.Common;
using System.Data.SqlClient;

namespace DAL
{
    /// <summary>
    /// NegetiveListEmployerGateway Class.
    /// </summary>
    public class NegetiveListEmployerGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor
        /// </summary>
        public NegetiveListEmployerGateway()
        {
            dbMySQL = new DatabaseConnection();
        }

        public List<NegetiveListEmployer> GetUploadedWatchListTempData()
        {
            List<NegetiveListEmployer> negetiveListEmployerListObj = new List<NegetiveListEmployer>();
            DataTable negetiveEmployerDataTableObj = null;
            NegetiveListEmployer negetiveListEmployerObj = null;
            string mSQL = "SELECT * FROM t_pluss_negativelistemployer_temp";
            negetiveEmployerDataTableObj = DbQueryManager.GetTable(mSQL);
            try
            {
                if (negetiveEmployerDataTableObj != null)
                {
                    DataTableReader negetiveEmployerDataReaderObj = negetiveEmployerDataTableObj.CreateDataReader();
                    if (negetiveEmployerDataReaderObj != null)
                    {
                        while (negetiveEmployerDataReaderObj.Read())
                        {
                            negetiveListEmployerObj = new NegetiveListEmployer();
                            negetiveListEmployerObj.ID = Convert.ToInt32(negetiveEmployerDataReaderObj["NELE_ID"].ToString());
                            negetiveListEmployerObj.BankCompany = negetiveEmployerDataReaderObj["NELE_BANKCOMPANY"].ToString();
                            negetiveListEmployerObj.Branch = negetiveEmployerDataReaderObj["NELE_BRANCH"].ToString();
                            negetiveListEmployerObj.Address = negetiveEmployerDataReaderObj["NELE_ADDRESS"].ToString();
                            negetiveListEmployerObj.NegetiveList = negetiveEmployerDataReaderObj["NELE_NEGATIVELIST"].ToString();
                            negetiveListEmployerObj.WatchList = negetiveEmployerDataReaderObj["NELE_WATCHLIST"].ToString();
                            negetiveListEmployerObj.Exclusion = negetiveEmployerDataReaderObj["NELE_EXCLUSION"].ToString();
                            negetiveListEmployerObj.Delist = negetiveEmployerDataReaderObj["NELE_DELIST"].ToString();
                            negetiveListEmployerObj.Restriction = negetiveEmployerDataReaderObj["NELE_RESTRICTION"].ToString();
                            negetiveListEmployerObj.Remarks = negetiveEmployerDataReaderObj["NELE_REMARKS"].ToString();
                            negetiveListEmployerObj.MakerPsId = negetiveEmployerDataReaderObj["MAKER_PS_ID"].ToString();

                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.InclusionDate = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"].ToString());
                                }
                                catch { negetiveListEmployerObj.InclusionDate = DateTime.Now; }
                            }
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.ChangeDate2 = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"]).ToString("dd-MM-yyyy");
                                }
                                catch { negetiveListEmployerObj.ChangeDate2 = DateTime.Now.ToString("dd-MM-yyyy"); }
                            }
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_ENTRYDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.EntryDate = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_ENTRYDATE"]);
                                }
                                catch { negetiveListEmployerObj.EntryDate = DateTime.Now; }
                            }
                            negetiveListEmployerListObj.Add(negetiveListEmployerObj);
                        }
                        negetiveEmployerDataReaderObj.Close();
                    }
                }
                return negetiveListEmployerListObj;
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("NegativeListEmployerGateWay : GetUploadedData");
                LogFile.WriteLog(ex);

                return null;
            }
        }

        public string FlushTempData()
        {
            try
            {
                string sql = "DELETE FROM t_pluss_negativelistemployer_temp";

                int count = DbQueryManager.ExecuteNonQuery(sql);
                return Operation.Success.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("NegetiveListEmployerGateway : FlushTempData");
                LogFile.WriteLog(ex);
                return Operation.Error.ToString();
            }
        }

        public DataTable GetTempWatchListDataTable()
        {
            try
            {
                DataTable table = DbQueryManager.GetTable(@"
SELECT NELE_ID SL,
NELE_BANKCOMPANY [BANK COMPANY],
NELE_BRANCH BRANCH,
NELE_ADDRESS ADDRESS,
NELE_NEGATIVELIST NEGATIVELIST,
NELE_WATCHLIST WATCHLIST,
NELE_EXCLUSION EXCLUSION,
NELE_DELIST DELIST,
NELE_RESTRICTION RESTRICTION,
NELE_REMARKS REMARKS,
NELE_INCLUSIONDATE [INCLUSION DATE],
NELE_CHANGEDATE [CHANGE DATE],
NELE_ENTRYDATE [ENTRY DATE],
MAKER_PS_ID [MAKER PS]
 FROM t_pluss_negativelistemployer_temp
");
                return table;
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("NegetiveListEmployerGateway :: GetTempDataTable");
                LogFile.WriteLog(ex);
            }
            return null;
        }

        public string ApproveWatchList(User actionUser)
        {
            try
            {
                List<SqlParameter> pars = new List<SqlParameter>();

                pars.Add(new SqlParameter("@CHECKER_PS_ID", actionUser.UserId));

                int rowAfftected = DbQueryManager.ExecuteSP("spApproveWatchList", pars);

                if (rowAfftected > 0)
                    return Operation.Success.ToString();
                return Operation.Failed.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("NegetiveListEmployerGateway :: ApproveNegetiveList");
                LogFile.WriteLog(ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// This method select the negetive list employer info.
        /// </summary>
        /// <param name="business">Gets a business name.</param>
        /// <param name="bank">Gets a bank list.</param>
        /// <param name="branch">Gets a branch list.</param>
        /// <returns>Returns a NegetiveListEmployer list object.</returns>
        public List<NegetiveListEmployer> GetNegetiveListEmployer(string business, List<string> bank, List<string> branch)
        {
            List<NegetiveListEmployer> negetiveListEmployerListObj = new List<NegetiveListEmployer>();
            DataTable negetiveEmployerDataTableObj = null;
            NegetiveListEmployer negetiveListEmployerObj;

            string searchCondition = "";
            if (!String.IsNullOrEmpty(business))
            {
                if (searchCondition != "")
                {
                    searchCondition += " OR (RTRIM(LTRIM(Upper(NELE_BANKCOMPANY)))  LIKE '%" + AddSlash(business.ToUpper().Trim()) + "%')";
                }
                else
                {
                    searchCondition += " And (RTRIM(LTRIM(Upper(NELE_BANKCOMPANY)))  LIKE '%" + AddSlash(business.ToUpper().Trim()) + "%')";
                }

            }
            if (!String.IsNullOrEmpty(bank[0].ToString()))
            {
                if (searchCondition != "")
                {
                    searchCondition += " OR (RTRIM(Upper(NELE_BANKCOMPANY)) LIKE '%" + AddSlash(bank[0].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[0].ToUpper().Trim()) + "%')";
                }
                else
                {
                    searchCondition += " And (RTRIM(Upper(NELE_BANKCOMPANY)) LIKE '%" + AddSlash(bank[0].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[0].ToUpper().Trim()) + "%')";
                }

            }
            if (!String.IsNullOrEmpty(bank[1].ToString()))
            {
                if (searchCondition != "")
                {
                    searchCondition += " OR (RTRIM(Upper(NELE_BANKCOMPANY))  LIKE '%" + AddSlash(bank[1].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[1].ToUpper().Trim()) + "%')";
                }
                else
                {
                    searchCondition += " And (RTRIM(Upper(NELE_BANKCOMPANY))  LIKE '%" + AddSlash(bank[1].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[1].ToUpper().Trim()) + "%')";
                }

            }
            if (!String.IsNullOrEmpty(bank[2].ToString()))
            {
                if (searchCondition != "")
                {
                    searchCondition += " OR (RTRIM(Upper(NELE_BANKCOMPANY))  LIKE '%" + AddSlash(bank[2].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[2].ToUpper().Trim()) + "%')";
                }
                else
                {
                    searchCondition += " And (RTRIM(Upper(NELE_BANKCOMPANY))  LIKE '%" + AddSlash(bank[2].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[2].ToUpper().Trim()) + "%')";
                }

            }
            if (!String.IsNullOrEmpty(bank[3].ToString()))
            {
                if (searchCondition != "")
                {
                    searchCondition += " OR (RTRIM(Upper(NELE_BANKCOMPANY))  LIKE '%" + AddSlash(bank[3].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[3].ToUpper().Trim()) + "%')";
                }
                else
                {
                    searchCondition += " And (RTRIM(Upper(NELE_BANKCOMPANY))  LIKE '%" + AddSlash(bank[3].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[3].ToUpper().Trim()) + "%')";
                }

            }
            if (!String.IsNullOrEmpty(bank[4].ToString()))
            {
                if (searchCondition != "")
                {
                    searchCondition += " OR (RTRIM(Upper(NELE_BANKCOMPANY))  LIKE '%" + AddSlash(bank[4].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[4].ToUpper().Trim()) + "%')";
                }
                else
                {
                    searchCondition += " And (RTRIM(Upper(NELE_BANKCOMPANY))  LIKE '%" + AddSlash(bank[4].ToUpper().Trim()) + "%' AND RTRIM(Upper(NELE_BRANCH))  LIKE '%" + AddSlash(branch[4].ToUpper().Trim()) + "%')";
                }

            }

            string queryString = String.Format("SELECT NELE_ID, NELE_BANKCOMPANY, NELE_BRANCH, NELE_ADDRESS, NELE_NEGATIVELIST, " +
                                 "NELE_WATCHLIST, NELE_EXCLUSION, NELE_DELIST, NELE_RESTRICTION, NELE_REMARKS, " +
                                 "CAST(NELE_INCLUSIONDATE AS CHAR) AS NELE_INCLUSIONDATE, CAST(NELE_CHANGEDATE AS CHAR) AS NELE_CHANGEDATE, CAST(NELE_ENTRYDATE AS CHAR) AS NELE_ENTRYDATE " +
                                 "FROM t_pluss_negativelistemployer WHERE 0=0 {0}", searchCondition);

            negetiveEmployerDataTableObj = DbQueryManager.GetTable(queryString);
            try
            {
                if (negetiveEmployerDataTableObj != null)
                {
                    DataTableReader negetiveEmployerDataReaderObj = negetiveEmployerDataTableObj.CreateDataReader();
                    if (negetiveEmployerDataReaderObj != null)
                    {
                        while (negetiveEmployerDataReaderObj.Read())
                        {
                            negetiveListEmployerObj = new NegetiveListEmployer();
                            negetiveListEmployerObj.ID = Convert.ToInt32(negetiveEmployerDataReaderObj["NELE_ID"].ToString());
                            negetiveListEmployerObj.BankCompany = negetiveEmployerDataReaderObj["NELE_BANKCOMPANY"].ToString();
                            negetiveListEmployerObj.Branch = negetiveEmployerDataReaderObj["NELE_BRANCH"].ToString();
                            negetiveListEmployerObj.Address = negetiveEmployerDataReaderObj["NELE_ADDRESS"].ToString();
                            negetiveListEmployerObj.NegetiveList = negetiveEmployerDataReaderObj["NELE_NEGATIVELIST"].ToString();
                            negetiveListEmployerObj.WatchList = negetiveEmployerDataReaderObj["NELE_WATCHLIST"].ToString();
                            negetiveListEmployerObj.Exclusion = negetiveEmployerDataReaderObj["NELE_EXCLUSION"].ToString();
                            negetiveListEmployerObj.Delist = negetiveEmployerDataReaderObj["NELE_DELIST"].ToString();
                            negetiveListEmployerObj.Restriction = negetiveEmployerDataReaderObj["NELE_RESTRICTION"].ToString();
                            negetiveListEmployerObj.Remarks = negetiveEmployerDataReaderObj["NELE_REMARKS"].ToString();
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.InclusionDate = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"].ToString());
                                }
                                catch { negetiveListEmployerObj.InclusionDate = DateTime.Now; }
                            }
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.ChangeDate2 = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"]).ToString("dd-MM-yyyy");
                                }
                                catch { negetiveListEmployerObj.ChangeDate2 = DateTime.Now.ToString("dd-MM-yyyy"); }
                            }
                            negetiveListEmployerListObj.Add(negetiveListEmployerObj);
                        }
                        negetiveEmployerDataReaderObj.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return negetiveListEmployerListObj;
        }
        /// <summary>
        /// This method convert date time to string.
        /// </summary>
        /// <param name="dateTime">Gets a date time.</param>
        /// <returns>Returns a converted date time.</returns>
        public string ToDate(DateTime dateTime)
        {
            return dateTime.ToString("dd/MM/yyyy");
        }
        /// <summary>
        /// This method select negetive list employer info.
        /// </summary>
        /// <param name="bankCompanyName">Gets a bank company name.</param>
        /// <param name="branch">Gets a branch name.</param>
        /// <returns>Returns a NegetiveListEmployer object.</returns>
        public NegetiveListEmployer SelectNegetiveListEmployer(string bankCompanyName, string branch)
        {
            DataTable negetiveEmployerDataTableObj = null;
            NegetiveListEmployer negetiveListEmployerObj = new NegetiveListEmployer();
            string searchCondition = "";
            if (!String.IsNullOrEmpty(bankCompanyName))
            {
                searchCondition += " OR UPPER(NELE_BANKCOMPANY) LIKE(UPPER('%" + AddSlash(bankCompanyName) + "%')) ";
            }
            if (!String.IsNullOrEmpty(branch))
            {
                // searchCondition += " OR UPPER(NELE_BRANCH) LIKE(UPPER('%" + AddSlash(branch) + "%')) ";
                //tarek
                searchCondition += " AND UPPER(NELE_BRANCH) LIKE(UPPER('%" + AddSlash(branch) + "%')) ";
            }
            string queryString = String.Format("SELECT NELE_ID, NELE_BANKCOMPANY, NELE_BRANCH, NELE_ADDRESS, NELE_NEGATIVELIST, " +
                                 "NELE_WATCHLIST, NELE_EXCLUSION, NELE_DELIST, NELE_RESTRICTION, NELE_REMARKS, " +
                                 "CAST(NELE_INCLUSIONDATE AS CHAR) AS NELE_INCLUSIONDATE, CAST(NELE_CHANGEDATE AS CHAR) AS NELE_CHANGEDATE, CAST(NELE_ENTRYDATE AS CHAR) AS NELE_ENTRYDATE " +
                                 "FROM t_pluss_negativelistemployer WHERE 0 {0}", searchCondition);

            negetiveEmployerDataTableObj = DbQueryManager.GetTable(queryString);
            try
            {
                if (negetiveEmployerDataTableObj != null)
                {
                    DataTableReader negetiveEmployerDataReaderObj = negetiveEmployerDataTableObj.CreateDataReader();
                    if (negetiveEmployerDataReaderObj != null)
                    {
                        while (negetiveEmployerDataReaderObj.Read())
                        {
                            negetiveListEmployerObj = new NegetiveListEmployer();
                            negetiveListEmployerObj.ID = Convert.ToInt32(negetiveEmployerDataReaderObj["NELE_ID"].ToString());
                            negetiveListEmployerObj.BankCompany = negetiveEmployerDataReaderObj["NELE_BANKCOMPANY"].ToString();
                            negetiveListEmployerObj.Branch = negetiveEmployerDataReaderObj["NELE_BRANCH"].ToString();
                            negetiveListEmployerObj.Address = negetiveEmployerDataReaderObj["NELE_ADDRESS"].ToString();
                            negetiveListEmployerObj.NegetiveList = negetiveEmployerDataReaderObj["NELE_NEGATIVELIST"].ToString();
                            negetiveListEmployerObj.WatchList = negetiveEmployerDataReaderObj["NELE_WATCHLIST"].ToString();
                            negetiveListEmployerObj.Exclusion = negetiveEmployerDataReaderObj["NELE_EXCLUSION"].ToString();
                            negetiveListEmployerObj.Delist = negetiveEmployerDataReaderObj["NELE_DELIST"].ToString();
                            negetiveListEmployerObj.Restriction = negetiveEmployerDataReaderObj["NELE_RESTRICTION"].ToString();
                            negetiveListEmployerObj.Remarks = negetiveEmployerDataReaderObj["NELE_REMARKS"].ToString();
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.InclusionDate = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"]);
                                }
                                catch { negetiveListEmployerObj.InclusionDate = DateTime.Now; }
                            }
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.ChangeDate2 = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"]).ToString("dd-MM-yyyy");
                                }
                                catch { negetiveListEmployerObj.ChangeDate2 = DateTime.Now.ToString("dd-MM-yyyy"); }
                            }

                        }
                        negetiveEmployerDataReaderObj.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return negetiveListEmployerObj;
        }

        /// <summary>
        /// This method provide the insertion or update operation of watch list.
        /// </summary>
        /// <param name="negetiveListEmployerObjList">Gets a NegetiveListEmployer list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendWatchListInToDB(List<NegetiveListEmployer> negetiveListEmployerObjList)
        {
            int returnValue = 0;
            List<NegetiveListEmployer> insertNegetiveListEmployerList = new List<NegetiveListEmployer>();
            List<NegetiveListEmployer> updateNegetiveListEmployerList = new List<NegetiveListEmployer>();
            if (negetiveListEmployerObjList.Count > 0)
            {
                foreach (NegetiveListEmployer negetiveListEmployerObj in negetiveListEmployerObjList)
                {
                    NegetiveListEmployer existingNegetiveListEmployerObj = SelectNegetiveListEmployer(negetiveListEmployerObj.BankCompany, negetiveListEmployerObj.Branch);
                    if (existingNegetiveListEmployerObj.ID > 0)
                    {
                        negetiveListEmployerObj.ID = existingNegetiveListEmployerObj.ID;
                        returnValue = UpdateNegetiveListEmployerList(negetiveListEmployerObj);
                    }
                    else
                    {
                        returnValue = InsertNegetiveListEmployerList(negetiveListEmployerObj);
                    }
                }
                //if (updateNegetiveListEmployerList.Count > 0)
                //{
                //    returnValue = UpdateNegetiveListEmployerList(updateNegetiveListEmployerList);
                //}
                //if (insertNegetiveListEmployerList.Count > 0)
                //{
                //    returnValue = InsertNegetiveListEmployerList(insertNegetiveListEmployerList);
                //}
            }
            return returnValue;
        }

        /// <summary>
        /// This method provide the isnertion of negetive list employer info.
        /// </summary>
        /// <param name="negetiveListEmployerObjList">Gets  a NegetiveListEmployer list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertNegetiveListEmployerList(NegetiveListEmployer negetiveListEmployerObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_negativelistemployer (NELE_BANKCOMPANY, NELE_BRANCH," +
                              "NELE_ADDRESS, NELE_NEGATIVELIST, " +
                              "NELE_WATCHLIST, NELE_EXCLUSION," +
                              "NELE_DELIST, NELE_RESTRICTION," +
                              "NELE_REMARKS, NELE_INCLUSIONDATE," +
                              "NELE_CHANGEDATE,NELE_ENTRYDATE," +
                              "NELE_USER_ID) VALUES";
                //for (int i = 0; i < negetiveListEmployerObjList.Count; i++)
                //{
                mSQL = mSQL + "('" +
                      AddSlash(negetiveListEmployerObj.BankCompany) + "','" +
                      AddSlash(negetiveListEmployerObj.Branch) + "','" +
                      AddSlash(negetiveListEmployerObj.Address) + "','" +
                      negetiveListEmployerObj.NegetiveList + "','" +
                      negetiveListEmployerObj.WatchList + "','" +
                      negetiveListEmployerObj.Exclusion + "','" +
                      negetiveListEmployerObj.Delist + "','" +
                      negetiveListEmployerObj.Restriction + "','" +
                      AddSlash(negetiveListEmployerObj.Remarks) + "','" +
                      negetiveListEmployerObj.InclusionDate.ToString("yyyy-MM-dd") + "','" +
                      negetiveListEmployerObj.ChangeDate.ToString("yyyy-MM-dd") + "','" +
                      negetiveListEmployerObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                      negetiveListEmployerObj.UserId + ")";
                //}
                //mSQL = mSQL.Substring(0, mSQL.Length - 1);
                if (dbMySQL.DbExecute(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method provide the update of negetive list employer info.
        /// </summary>
        /// <param name="negetiveListEmployerObjList">Gets  a NegetiveListEmployer list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateNegetiveListEmployerList(NegetiveListEmployer negetiveListEmployerObj)
        {
            bool flag = false;
            try
            {
                //for (int i = 0; i < negetiveListEmployerObjList.Count; i++)
                //{
                string mSQL = "";
                mSQL = "UPDATE t_pluss_negativelistemployer SET " +
                       "NELE_BANKCOMPANY='" + AddSlash(negetiveListEmployerObj.BankCompany) + "'," +
                       "NELE_BRANCH='" + AddSlash(negetiveListEmployerObj.Branch) + "'," +
                       "NELE_ADDRESS='" + AddSlash(negetiveListEmployerObj.Address) + "'," +
                       "NELE_NEGATIVELIST='" + negetiveListEmployerObj.NegetiveList + "'," +
                       "NELE_WATCHLIST='" + negetiveListEmployerObj.WatchList + "'," +
                       "NELE_EXCLUSION='" + negetiveListEmployerObj.Exclusion + "'," +
                       "NELE_DELIST='" + negetiveListEmployerObj.Delist + "'," +
                       "NELE_RESTRICTION='" + negetiveListEmployerObj.Restriction + "'," +
                       "NELE_REMARKS='" + AddSlash(negetiveListEmployerObj.Remarks) + "'," +
                       "NELE_INCLUSIONDATE='" + negetiveListEmployerObj.InclusionDate.ToString("yyyy-MM-dd") + "'," +
                       "NELE_CHANGEDATE='" + negetiveListEmployerObj.ChangeDate.ToString("yyyy-MM-dd") + "'," +
                       "NELE_ENTRYDATE ='" + negetiveListEmployerObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                       "NELE_USER_ID =" + negetiveListEmployerObj.UserId + " WHERE NELE_ID=" + negetiveListEmployerObj.ID;
                if (dbMySQL.DbExecute(mSQL) > -1)
                {
                    flag = true;
                }
                //}
                if (flag == true)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets a string.</param>
        /// <returns>Returns a converted string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        public List<NegetiveListEmployer> GetUploadResult(DateTime startDate, DateTime endDate, Int32 offset, Int32 rowsPerPage)
        {
            List<NegetiveListEmployer> negetiveListEmployerListObj = new List<NegetiveListEmployer>();
            DataTable negetiveEmployerDataTableObj = null;
            NegetiveListEmployer negetiveListEmployerObj = null;
            string mSQL = "SELECT * FROM t_pluss_negativelistemployer_temp WHERE NELE_ENTRYDATE BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "'order by NELE_ID OFFSET  " + offset + " ROWS FETCH NEXT " + rowsPerPage + " ROWS ONLY ";
            negetiveEmployerDataTableObj = DbQueryManager.GetTable(mSQL);
            try
            {
                if (negetiveEmployerDataTableObj != null)
                {
                    DataTableReader negetiveEmployerDataReaderObj = negetiveEmployerDataTableObj.CreateDataReader();
                    if (negetiveEmployerDataReaderObj != null)
                    {
                        while (negetiveEmployerDataReaderObj.Read())
                        {
                            negetiveListEmployerObj = new NegetiveListEmployer();
                            negetiveListEmployerObj.ID = Convert.ToInt32(negetiveEmployerDataReaderObj["NELE_ID"].ToString());
                            negetiveListEmployerObj.BankCompany = negetiveEmployerDataReaderObj["NELE_BANKCOMPANY"].ToString();
                            negetiveListEmployerObj.Branch = negetiveEmployerDataReaderObj["NELE_BRANCH"].ToString();
                            negetiveListEmployerObj.Address = negetiveEmployerDataReaderObj["NELE_ADDRESS"].ToString();
                            negetiveListEmployerObj.NegetiveList = negetiveEmployerDataReaderObj["NELE_NEGATIVELIST"].ToString();
                            negetiveListEmployerObj.WatchList = negetiveEmployerDataReaderObj["NELE_WATCHLIST"].ToString();
                            negetiveListEmployerObj.Exclusion = negetiveEmployerDataReaderObj["NELE_EXCLUSION"].ToString();
                            negetiveListEmployerObj.Delist = negetiveEmployerDataReaderObj["NELE_DELIST"].ToString();
                            negetiveListEmployerObj.Restriction = negetiveEmployerDataReaderObj["NELE_RESTRICTION"].ToString();
                            negetiveListEmployerObj.Remarks = negetiveEmployerDataReaderObj["NELE_REMARKS"].ToString();
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.InclusionDate = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"].ToString());
                                }
                                catch { negetiveListEmployerObj.InclusionDate = DateTime.Now; }
                            }
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.ChangeDate2 = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"]).ToString("dd-MM-yyyy");
                                }
                                catch { negetiveListEmployerObj.ChangeDate2 = DateTime.Now.ToString("dd-MM-yyyy"); }
                            }
                            negetiveListEmployerListObj.Add(negetiveListEmployerObj);
                        }
                        negetiveEmployerDataReaderObj.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return negetiveListEmployerListObj;
        }

        public Int32 GetTolalRecord(DateTime startDate, DateTime endDate)
        {
            Int32 returnValue = 0;
            string mSQL = "SELECT COUNT(*) as TotalRecord FROM t_pluss_negativelistemployer WHERE NELE_ENTRYDATE BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "'";
            returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }

    }
}
