﻿using System;
using System.Collections.Generic;
using System.Data;
using Bat.Common;
using BusinessEntities;
namespace DAL
{
    public class BankStatementGateway
    {
        public DatabaseConnection dbMySQL = null;
        private BankStatement bankStatementObj=null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;

        public BankStatementGateway()
        {
            bankStatementObj= new BankStatement();
            dbMySQL = new DatabaseConnection();
        }

        public string SelectACTypeId(string acTypeName)
        {
            return dbMySQL.DbGetValue("SELECT ACTY_ID FROM t_pluss_accounttype WHERE ACTY_NAME='" + acTypeName + "'");
        }

        public string GetLLIdEntryUserId(Int64 LLId)
        {
            string returnValue = "";
            string mSQL = "SELECT USER_NAME FROM t_pluss_user LEFT JOIN loan_app_info ON  USER_ID=LOAP_USERID WHERE LOAN_APPLI_ID=" + LLId;
            DataTable table = DbQueryManager.GetTable(mSQL);

            if (table !=null&& (!Convert.IsDBNull((string)table.Rows[0][0])))
            {
                returnValue = (string) (table.Rows[0][0]);
            }
            return returnValue;
        }

        public BankStatement GetBankStatement(Int64 LLID, Int16 bankId, Int16 branchId, string acNo)
        {
            BankStatement bankStatementObj = new BankStatement();
            string mSQL = "SELECT * FROM t_pluss_bankstatement WHERE BAST_LLID=" + LLID + " AND BAST_BANK_ID=" + bankId + " AND BAST_BRANCH_ID=" + branchId + " AND BAST_ACNO='" + acNo + "'";
            return bankStatementObj = GetData(mSQL);    
        }

        public List<BankStatement> GetBankStatementList(Int64 LLID)
        {
            List<BankStatement> bankStatementObjList = new List<BankStatement>();
            string mSQL = "SELECT * FROM t_pluss_bankstatement WHERE BAST_LLID=" + LLID;
            return bankStatementObjList = GetDataList(mSQL);
        }

        private BankStatement GetData(string mSQL)
        {
            BankStatement bankStatementObj = null;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                if (xRs.Read())
                {
                    bankStatementObj = new BankStatement();
                    bankStatementObj.BankStatementId = Convert.ToInt64(xRs["BAST_ID"]);
                    bankStatementObj.LlId = Convert.ToInt64(xRs["BAST_LLID"]);
                    bankStatementObj.Bank_ID = Convert.ToInt16(xRs["BAST_BANK_ID"]);
                    bankStatementObj.Branch_ID = Convert.ToInt16(xRs["BAST_BRANCH_ID"]);
                    bankStatementObj.ACNO = Convert.ToString(xRs["BAST_ACNO"]);
                    bankStatementObj.ACType_Id = Convert.ToInt16(xRs["BAST_ACTYPE_ID"]);
                    bankStatementObj.ACName = Convert.ToString(xRs["BAST_ACNAME"]);
                    bankStatementObj.StartMonth = Convert.ToString(xRs["BAST_STARTMONTH"]);
                    bankStatementObj.EndMonth = Convert.ToString(xRs["BAST_ENDMONTH"]);
                    bankStatementObj.Statement = Convert.ToString(xRs["BAST_STATEMENT"]);
                    bankStatementObj.EntryDate = Convert.ToDateTime(xRs["BAST_ENTRYDATE"]);
                    bankStatementObj.UserId = Convert.ToInt32(xRs["BAST_USER_ID"]);
                }
                xRs.Close();
            }
            return bankStatementObj;

        }
        private List<BankStatement> GetDataList(string mSQL)
        {
            List<BankStatement> bankStatementObjList = new List<BankStatement>();
            BankStatement bankStatementObj = null;
            ADODB.Recordset xRs = new ADODB.Recordset();
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                bankStatementObj = new BankStatement();
                bankStatementObj.BankStatementId = Convert.ToInt64(xRs.Fields["BAST_ID"].Value);
                bankStatementObj.LlId = Convert.ToInt64(xRs.Fields["BAST_LLID"].Value);
                bankStatementObj.Bank_ID = Convert.ToInt16(xRs.Fields["BAST_BANK_ID"].Value);
                bankStatementObj.Branch_ID = Convert.ToInt16(xRs.Fields["BAST_BRANCH_ID"].Value);
                bankStatementObj.ACNO = Convert.ToString(xRs.Fields["BAST_ACNO"].Value);
                bankStatementObj.ACType_Id = Convert.ToInt16(xRs.Fields["BAST_ACTYPE_ID"].Value);
                bankStatementObj.ACName = Convert.ToString(xRs.Fields["BAST_ACNAME"].Value);
                bankStatementObj.StartMonth = Convert.ToString(xRs.Fields["BAST_STARTMONTH"].Value);
                bankStatementObj.EntryDate = Convert.ToDateTime(xRs.Fields["BAST_ENTRYDATE"].Value);
                bankStatementObj.UserId = Convert.ToInt32(xRs.Fields["BAST_USER_ID"].Value);
                bankStatementObjList.Add(bankStatementObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return bankStatementObjList;

        }
        
        public int SendBankStatementInToDB(BankStatement bankStatementObj)
        {
            BankStatement bankStatementExist = GetBankStatement(bankStatementObj.LlId, bankStatementObj.Bank_ID, bankStatementObj.Branch_ID, bankStatementObj.ACNO);
            if (bankStatementExist == null)
            {
                return InsertBankStatement(bankStatementObj);
            }
            else
            {
                bankStatementObj.BankStatementId = bankStatementExist.BankStatementId;
                return UpdateBankStatement(bankStatementObj);
            }
        }
        private int InsertBankStatement(BankStatement bankStatementObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_bankstatement (BAST_LLID,BAST_BANK_ID,BAST_BRANCH_ID, BAST_ACNO, BAST_ACTYPE_ID,BAST_ACNAME, BAST_STARTMONTH,BAST_ENDMONTH,BAST_STATEMENT,BAST_ENTRYDATE,BAST_USER_ID) VALUES( " +
                    bankStatementObj.LlId + "," + 
                    bankStatementObj.Bank_ID + "," + 
                    bankStatementObj.Branch_ID + ",'" + 
                    bankStatementObj.ACNO + "'," + 
                    bankStatementObj.ACType_Id + ",'" + 
                    bankStatementObj.ACName + "','" +
                    bankStatementObj.StartMonth + "','" +
                    bankStatementObj.EndMonth + "','" +
                    bankStatementObj.Statement + "','" + 
                    bankStatementObj.EntryDate.ToString("yyyy-MM-dd") + "'," + 
                    bankStatementObj.UserId + ")";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }

        }

        private int UpdateBankStatement(BankStatement bankStatementObj)
        {
            string mSQL = "UPDATE t_pluss_bankstatement SET " +
                          "BAST_BANK_ID=" + bankStatementObj.Bank_ID + "," +
                          "BAST_BRANCH_ID=" + bankStatementObj.Branch_ID + "," +
                          "BAST_ACNO='" + bankStatementObj.ACNO + "'," +
                          "BAST_ACTYPE_ID=" + bankStatementObj.ACType_Id + "," +
                          "BAST_ACNAME='" + bankStatementObj.ACName + "'," +
                          "BAST_STARTMONTH='" + bankStatementObj.StartMonth + "'," +
                          "BAST_ENDMONTH='" + bankStatementObj.EndMonth + "'," +
                          "BAST_STATEMENT='" + bankStatementObj.Statement + "'," +
                          "BAST_ENTRYDATE='" + bankStatementObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                          "BAST_USER_ID=" + bankStatementObj.UserId + " WHERE BAST_ID=" + bankStatementObj.BankStatementId;
            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                return UPDATE;
            }
            else
            {
                return ERROR;
            }
        }
        private int DeleteBankStatement(BankStatement bankStatementObj)
        {
            string mSQL = "DELETE FROM t_pluss_bankstatement WHERE BAST_ID=" + bankStatementObj.BankStatementId;
            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                return DELETE;
            }
            else
            {
                return ERROR;
            }
        }
        public ApplicantOtherBankAccount SelectLoanApplicationOthreBankInfo(Int64 lLID)
        {
            ApplicantOtherBankAccount otherBankAccountObj = null;// new ApplicantOtherBankAccount();
            string mSQL = "SELECT LOAN_APPLI_ID, CUST_NM,LOAN_STATU_ID,OTHER_ACC_BANK_ID_1, OTHER_ACC_BR_ID_1, OTHER_ACC_NO_1," +
                            "OTHER_ACC_TYPE_1, OTHER_ACC_BANK_ID_2, OTHER_ACC_BR_ID_2, OTHER_ACC_NO_2, OTHER_ACC_TYPE_2," +
                            "OTHER_ACC_BANK_ID_3, OTHER_ACC_BR_ID_3, OTHER_ACC_NO_3, OTHER_ACC_TYPE_3, LOAP_OTHERACWITHBANK_ID4," +
                            "LOAP_OTHERACWITHBRANCH_ID4, LOAP_OTHERACWITHACNO4, LOAP_ORHREACTYPE_ID4, LOAP_OTHERACWITHBANK_ID5," +
                            "LOAP_OTHERACWITHBRANCH_ID5, LOAP_OTHERACWITHACNO5, LOAP_ORHREACTYPE_ID5, BK1.BANK_NM AS OTHBANK1,BH1.BRAN_NAME AS OTHBRNCH1," +
                            "BH1.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS1, BK2.BANK_NM AS OTHBANK2,BH2.BRAN_NAME AS OTHBRNCH2, BH2.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS2," +
                            "BK3.BANK_NM AS OTHBANK3,BH3.BRAN_NAME AS OTHBRNCH3, BH3.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS3, BK4.BANK_NM AS OTHBANK4,BH4.BRAN_NAME AS OTHBRNCH4," +
                            "BH4.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS4, BK5.BANK_NM AS OTHBANK5,BH5.BRAN_NAME AS OTHBRNCH5, BH5.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS5 " +
                            "FROM loan_app_info AS LA LEFT JOIN t_pluss_bank AS BK1 ON LA.OTHER_ACC_BANK_ID_1 = BK1.BANK_ID LEFT JOIN t_pluss_branch AS BH1 ON LA.OTHER_ACC_BR_ID_1 = " +
                            "BH1.BRAN_ID LEFT JOIN t_pluss_bank AS BK2 ON LA.OTHER_ACC_BANK_ID_2 = BK2.BANK_ID LEFT JOIN t_pluss_branch AS BH2 ON LA.OTHER_ACC_BR_ID_2 = " +
                            "BH2.BRAN_ID LEFT JOIN t_pluss_bank AS BK3 ON LA.OTHER_ACC_BANK_ID_3 = BK3.BANK_ID LEFT JOIN t_pluss_branch AS BH3 ON LA.OTHER_ACC_BR_ID_3 = " +
                            "BH3.BRAN_ID LEFT JOIN t_pluss_bank AS BK4 ON LA.LOAP_OTHERACWITHBANK_ID4 = BK4.BANK_ID LEFT JOIN t_pluss_branch AS BH4 ON LA.LOAP_OTHERACWITHBRANCH_ID4 = " +
                            "BH4.BRAN_ID LEFT JOIN t_pluss_bank AS BK5 ON LA.LOAP_OTHERACWITHBANK_ID5 = BK5.BANK_ID LEFT JOIN t_pluss_branch AS BH5 ON LA.LOAP_OTHERACWITHBRANCH_ID5 = " +
                            "BH5.BRAN_ID WHERE LOAN_APPLI_ID=" + lLID;
             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    otherBankAccountObj = new ApplicantOtherBankAccount();
                    otherBankAccountObj.OtherBankId1 = Convert.ToInt32("0" + xRs["OTHER_ACC_BANK_ID_1"]);
                    otherBankAccountObj.OtherBankId2 = Convert.ToInt32("0" + xRs["OTHER_ACC_BANK_ID_2"]);
                    otherBankAccountObj.OtherBankId3 = Convert.ToInt32("0" + xRs["OTHER_ACC_BANK_ID_3"]);
                    otherBankAccountObj.OtherBankId4 =
                        Convert.ToInt32("0" + xRs["LOAP_OTHERACWITHBANK_ID4"]);
                    otherBankAccountObj.OtherBankId5 =
                        Convert.ToInt32("0" + xRs["LOAP_OTHERACWITHBANK_ID5"]);
                    otherBankAccountObj.OtherBank1 = Convert.ToString(xRs["OTHBANK1"]);
                    otherBankAccountObj.OtherBank2 = Convert.ToString(xRs["OTHBANK2"]);
                    otherBankAccountObj.OtherBank3 = Convert.ToString(xRs["OTHBANK3"]);
                    otherBankAccountObj.OtherBank4 = Convert.ToString(xRs["OTHBANK4"]);
                    otherBankAccountObj.OtherBank5 = Convert.ToString(xRs["OTHBANK5"]);
                    otherBankAccountObj.OtherBranchId1 = Convert.ToInt32("0" + xRs["OTHER_ACC_BR_ID_1"]);
                    otherBankAccountObj.OtherBranchId2 = Convert.ToInt32("0" + xRs["OTHER_ACC_BR_ID_2"]);
                    otherBankAccountObj.OtherBranchId3 = Convert.ToInt32("0" + xRs["OTHER_ACC_BR_ID_3"]);
                    otherBankAccountObj.OtherBranchId4 =
                        Convert.ToInt32("0" + xRs["LOAP_OTHERACWITHBRANCH_ID4"]);
                    otherBankAccountObj.OtherBranchId5 =
                        Convert.ToInt32("0" + xRs["LOAP_OTHERACWITHBRANCH_ID5"]);
                    otherBankAccountObj.OtherBranch1 = Convert.ToString(xRs["OTHBRNCH1"]);
                    otherBankAccountObj.OtherBranch2 = Convert.ToString(xRs["OTHBRNCH2"]);
                    otherBankAccountObj.OtherBranch3 = Convert.ToString(xRs["OTHBRNCH3"]);
                    otherBankAccountObj.OtherBranch4 = Convert.ToString(xRs["OTHBRNCH4"]);
                    otherBankAccountObj.OtherBranch5 = Convert.ToString(xRs["OTHBRNCH5"]);
                    otherBankAccountObj.OtherAccNo1 = Convert.ToString(xRs["OTHER_ACC_NO_1"]);
                    otherBankAccountObj.OtherAccNo2 = Convert.ToString(xRs["OTHER_ACC_NO_2"]);
                    otherBankAccountObj.OtherAccNo3 = Convert.ToString(xRs["OTHER_ACC_NO_3"]);
                    otherBankAccountObj.OtherAccNo4 = Convert.ToString(xRs["LOAP_OTHERACWITHACNO4"]);
                    otherBankAccountObj.OtherAccNo5 = Convert.ToString(xRs["LOAP_OTHERACWITHACNO5"]);
                    otherBankAccountObj.OtherAccType1 = Convert.ToString(xRs["OTHER_ACC_TYPE_1"]);
                    otherBankAccountObj.OtherAccType2 = Convert.ToString(xRs["OTHER_ACC_TYPE_2"]);
                    otherBankAccountObj.OtherAccType3 = Convert.ToString(xRs["OTHER_ACC_TYPE_3"]);
                    otherBankAccountObj.OtherAccType4 = Convert.ToString(xRs["LOAP_ORHREACTYPE_ID4"]);
                    otherBankAccountObj.OtherAccType5 = Convert.ToString(xRs["LOAP_ORHREACTYPE_ID5"]);
                    otherBankAccountObj.ApplicationStatus = Convert.ToInt32("0" + xRs["LOAN_STATU_ID"]);
                }
                xRs.Close();
            }
            return otherBankAccountObj;
        }
    }
}
