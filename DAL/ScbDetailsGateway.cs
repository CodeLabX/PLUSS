﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using System.Data;
using Bat.Common;


namespace DAL
{
    /// <summary>
    /// ScbDetailsGateway Class
    /// </summary>
    public class ScbDetailsGateway
    {
        private List<ScbDetails> scbDetailsObjList = new List<ScbDetails>();
        private ScbDetails scbDetailsObj = new ScbDetails();
        /// <summary>
        /// Constructor
        /// </summary>
        public ScbDetailsGateway()
        {
        }
        /// <summary>
        /// This method select the all scb details info
        /// </summary>
        /// <returns>Returns a ScbDetails list object.</returns>
        public List<ScbDetails> GetAllScbDetailsInfo()
        {
            DataTable scbDetailsDataTableObj = null;

            string queryString = "select SCBD_ID, SCBD_SCBMASTER_ID, SCBD_PERIOD, SCBD_MONTHENDLEDGERBALANCE, " +
            "SCBD_MAXLEDGERBALANCE, SCBD_MINLEDGERBALANCE, SCBD_AVERAGELEDGERBALANCE, " +
            "SCBD_ADJMONTAVERAGEBALANCE, SCBD_CREDITTRANSACTIONCOUNT, " +
            "SCBD_TOTALTRANSACTIONCOUNT, SCBD_DEBITTURNOVER, " +
            "SCBD_CREDITTURNOVER, SCBD_ADJUSTMENT, SCBD_ADJUSTMENTCTO, " +
            "SCBD_ADJMONTHCTO " +
            "from t_pluss_scbdetails ";

            scbDetailsDataTableObj = DbQueryManager.GetTable(queryString);
            if (scbDetailsDataTableObj != null)
            {
                DataTableReader scbDetailsDataReaderObj = scbDetailsDataTableObj.CreateDataReader();

                while (scbDetailsDataReaderObj.Read())
                {
                    scbDetailsObj = new ScbDetails();
                    scbDetailsObj.scbdID = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_ID"]);
                    scbDetailsObj.scbdScbmasterID = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_SCBMASTER_ID"]);

                    scbDetailsObj.scbdPeriod = Convert.ToDateTime(scbDetailsDataReaderObj["SCBD_PERIOD"]);
                    scbDetailsObj.scbdMonthendledgerbalance = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_MONTHENDLEDGERBALANCE"]);
                    scbDetailsObj.scbdMaxledgerbalance = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_MAXLEDGERBALANCE"]);
                    scbDetailsObj.scbdMinledgerbalance = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_MINLEDGERBALANCE"]);
                    scbDetailsObj.scbdAverageledgerbalance = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_AVERAGELEDGERBALANCE"]);
                    scbDetailsObj.scbdAdjmontaveragebalance = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_ADJMONTAVERAGEBALANCE"]);
                    
                    scbDetailsObj.scbdCredittransactioncount = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_CREDITTRANSACTIONCOUNT"]);
                    scbDetailsObj.scbdTotaltransactioncount = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_TOTALTRANSACTIONCOUNT"]);
                    scbDetailsObj.scbdDebitturnover = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_DEBITTURNOVER"]);
                    scbDetailsObj.scbdCreditturnover = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_CREDITTURNOVER"]);
                    scbDetailsObj.scbdAdjustment = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_ADJUSTMENT"]);
                    scbDetailsObj.scbdAdjustmentcto = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_ADJUSTMENTCTO"]);
                    scbDetailsObj.scbdAdjmonthcto = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_ADJMONTHCTO"]);

                    scbDetailsObjList.Add(scbDetailsObj);
                }
                scbDetailsDataReaderObj.Close();

            }
            return scbDetailsObjList;
        }
        /// <summary>
        /// This method select all scb details info by master id
        /// </summary>
        /// <param name="scbMasterId">Gets a scb master id.</param>
        /// <returns>Returns a ScbDetails list object.</returns>
        public List<ScbDetails> GetAllScbDetailsInfoByMasterId(int scbMasterId)
        {
            DataTable scbDetailsDataTableObj = null;

            string queryString = "select SCBD_ID, SCBD_SCBMASTER_ID, SCBD_PERIOD, SCBD_MONTHENDLEDGERBALANCE, " +
            "SCBD_MAXLEDGERBALANCE, SCBD_MINLEDGERBALANCE, SCBD_AVERAGELEDGERBALANCE, " +
            "SCBD_ADJMONTAVERAGEBALANCE, SCBD_CREDITTRANSACTIONCOUNT, " +
            "SCBD_TOTALTRANSACTIONCOUNT, SCBD_DEBITTURNOVER, " +
            "SCBD_CREDITTURNOVER, SCBD_ADJUSTMENT, SCBD_ADJUSTMENTCTO, " +
            "SCBD_ADJMONTHCTO " +
            "from t_pluss_scbdetails where SCBD_ID = " + scbMasterId;

            scbDetailsDataTableObj = DbQueryManager.GetTable(queryString);
            if (scbDetailsDataTableObj != null)
            {
                DataTableReader scbDetailsDataReaderObj = scbDetailsDataTableObj.CreateDataReader();

                while (scbDetailsDataReaderObj.Read())
                {
                    scbDetailsObj = new ScbDetails();
                    scbDetailsObj.scbdID = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_ID"]);
                    scbDetailsObj.scbdScbmasterID = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_SCBMASTER_ID"]);

                    scbDetailsObjList.Add(scbDetailsObj);
                }
                scbDetailsDataReaderObj.Close();

            }
            return scbDetailsObjList;
        }
        /// <summary>
        /// This method select max scb master id
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public int GetLastScbMasterId()
        {
            DataTable scbMasterDataTableObj = null;

            string queryString = "select max(SCBM_ID) as id " +
            "from t_pluss_scbmaster ";

            scbMasterDataTableObj = DbQueryManager.GetTable(queryString);
            if (scbMasterDataTableObj != null)
            {
                DataTableReader scbMasterDataReaderObj = scbMasterDataTableObj.CreateDataReader();

                while (scbMasterDataReaderObj.Read())
                {
                    int i = Convert.ToInt32(scbMasterDataReaderObj["id"].ToString());
                    return i;

                }
                scbMasterDataReaderObj.Close();

            }
            return 0;
        }

        /// <summary>
        /// This method provide the operation of insertion of scb details
        /// </summary>
        /// <param name="scbDetailsObjList">Gets a ScbDetails list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertScbDetails(List<ScbDetails> scbDetailsObjList)
        {
            foreach (ScbDetails scbDetailsObj in scbDetailsObjList)
            {
                try
                {
                    string sqlInsert = "INSERT INTO t_pluss_scbdetails ( SCBD_SCBMASTER_ID, " +
                    "SCBD_PERIOD, SCBD_MONTHENDLEDGERBALANCE, SCBD_MAXLEDGERBALANCE, SCBD_MINLEDGERBALANCE, " +
                    "SCBD_AVERAGELEDGERBALANCE, SCBD_ADJMONTAVERAGEBALANCE, " +
                    "SCBD_CREDITTRANSACTIONCOUNT, SCBD_TOTALTRANSACTIONCOUNT, " +
                    "SCBD_DEBITTURNOVER, SCBD_CREDITTURNOVER, " +
                    "SCBD_ADJUSTMENT, SCBD_ADJUSTMENTCTO, " +
                    "SCBD_ADJMONTHCTO ) " +
                    "VALUES ( " + this.GetLastScbMasterId() + ", '" +
                    Convert.ToDateTime(scbDetailsObj.scbdPeriod.ToString()).ToString("yyyy-MM-dd") + "', " + scbDetailsObj.scbdMonthendledgerbalance + ", " + scbDetailsObj.scbdMaxledgerbalance + ", " + scbDetailsObj.scbdMinledgerbalance + ", " +
                    scbDetailsObj.scbdAverageledgerbalance + ", " + scbDetailsObj.scbdAdjmontaveragebalance + ", " +
                    scbDetailsObj.scbdCredittransactioncount + ", " + scbDetailsObj.scbdTotaltransactioncount + ", " +
                    scbDetailsObj.scbdDebitturnover + ", " + scbDetailsObj.scbdCreditturnover + ", " +
                    scbDetailsObj.scbdAdjustment + ", " + scbDetailsObj.scbdAdjustmentcto + ", " +
                    scbDetailsObj.scbdAdjmonthcto + ")";

                    int insert = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                    //queryText = sqlInsert.Replace('\'', '\"');
                    //queryType = "Insert";
                    if (insert == 0)
                    {
                        return insert;
                    }
                }
                catch
                {
                    return 0;
                }
            }
            return 1;
        }

        /// <summary>
        /// This method provide the operation of update of scb details
        /// </summary>
        /// <param name="scbDetailsObjList">Gets a ScbDetails list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdateScbDetails(ScbDetails scbDetailsObj)
        {
            try
            {
                string sqlInsert = "UPDATE t_pluss_scbdetails SET SCBD_SCBMASTER_ID = "+scbDetailsObj.scbdScbmasterID+", " +
            "SCBD_PERIOD = "+scbDetailsObj.scbdPeriod+", SCBD_MONTHENDLEDGERBALANCE = "+scbDetailsObj.scbdMonthendledgerbalance+", " +
            "SCBD_MAXLEDGERBALANCE = "+scbDetailsObj.scbdMaxledgerbalance+", SCBD_MINLEDGERBALANCE = "+scbDetailsObj.scbdMinledgerbalance+", " +
            "SCBD_AVERAGELEDGERBALANCE = "+scbDetailsObj.scbdAverageledgerbalance+", SCBD_ADJMONTAVERAGEBALANCE = "+scbDetailsObj.scbdAdjmontaveragebalance+", " +
            "SCBD_CREDITTRANSACTIONCOUNT = "+scbDetailsObj.scbdCredittransactioncount+", SCBD_TOTALTRANSACTIONCOUNT = "+scbDetailsObj.scbdTotaltransactioncount+", " +
            "SCBD_DEBITTURNOVER = "+scbDetailsObj.scbdDebitturnover+", SCBD_CREDITTURNOVER = "+scbDetailsObj.scbdCreditturnover+", " +
            "SCBD_ADJUSTMENT = "+scbDetailsObj.scbdAdjustment+", SCBD_ADJUSTMENTCTO = "+scbDetailsObj.scbdAdjustmentcto+", " +
            "SCBD_ADJMONTHCTO = "+scbDetailsObj.scbdAdjmonthcto +
            "WHERE SCBD_ID = "+scbDetailsObj.scbdID + ")";

                int update = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                //queryText = sqlInsert.Replace('\'', '\"');
                //queryType = "Update";
                return update;
            }
            catch
            {
                return 0;
            }
            return 0;
        }
        public List<ScbDetails> GetScbDetailsData(Int32 masterId, DateTime startManth, DateTime endMonth)
        {
            DataTable scbDetailsDataTableObj = null;

            string queryString = "SELECT * FROM t_pluss_scbdetails where SCBD_SCBMASTER_ID=" + masterId + " AND SCBD_PERIOD BETWEEN '" + startManth.ToString("yyyy-MM-dd") + "' AND '" + endMonth.ToString("yyyy-MM-dd") + "'";

            scbDetailsDataTableObj = DbQueryManager.GetTable(queryString);
            if (scbDetailsDataTableObj != null)
            {
                DataTableReader scbDetailsDataReaderObj = scbDetailsDataTableObj.CreateDataReader();

                while (scbDetailsDataReaderObj.Read())
                {
                    scbDetailsObj = new ScbDetails();
                    scbDetailsObj.scbdID = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_ID"]);
                    scbDetailsObj.scbdScbmasterID = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_SCBMASTER_ID"]);

                    scbDetailsObj.scbdPeriod = Convert.ToDateTime(scbDetailsDataReaderObj["SCBD_PERIOD"]);
                    scbDetailsObj.scbdMonthendledgerbalance = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_MONTHENDLEDGERBALANCE"]);
                    scbDetailsObj.scbdMaxledgerbalance = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_MAXLEDGERBALANCE"]);
                    scbDetailsObj.scbdMinledgerbalance = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_MINLEDGERBALANCE"]);
                    scbDetailsObj.scbdAverageledgerbalance = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_AVERAGELEDGERBALANCE"]);
                    scbDetailsObj.scbdAdjmontaveragebalance = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_ADJMONTAVERAGEBALANCE"]);

                    scbDetailsObj.scbdCredittransactioncount = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_CREDITTRANSACTIONCOUNT"]);
                    scbDetailsObj.scbdTotaltransactioncount = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_TOTALTRANSACTIONCOUNT"]);
                    scbDetailsObj.scbdDebitturnover = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_DEBITTURNOVER"]);
                    scbDetailsObj.scbdCreditturnover = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_CREDITTURNOVER"]);
                    scbDetailsObj.scbdAdjustment = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_ADJUSTMENT"]);
                    scbDetailsObj.scbdAdjustmentcto = Convert.ToDouble(scbDetailsDataReaderObj["SCBD_ADJUSTMENTCTO"]);
                    scbDetailsObj.scbdAdjmonthcto = Convert.ToInt32(scbDetailsDataReaderObj["SCBD_ADJMONTHCTO"]);

                    scbDetailsObjList.Add(scbDetailsObj);
                }
                scbDetailsDataReaderObj.Close();

            }
            return scbDetailsObjList;
            
        }
        /// <summary>
        /// This method provide the operation of delete of scb details
        /// </summary>
        /// <param name="scbDetailsObjList">Gets a scb master id.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeleteScbDetailsObject(Int64 scbMasterId)
        {
            string queryString = "DELETE FROM t_pluss_scbdetails WHERE SCBD_SCBMASTER_ID = " + scbMasterId;

            try
            {
                int status = DbQueryManager.ExecuteNonQuery(queryString);
                return status;
            }
            catch { return 0; }
        }
    }
}

