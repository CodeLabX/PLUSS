﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    public class BankStvPersonGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        public BankStvPersonGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public BankStvPerson SelectBankStvPersonInfo(Int16 bankId)
        {
            BankStvPerson bankStvPersonObj = null;
            //ADODB.Recordset xRs = new ADODB.Recordset();
            string mSQL = "SELECT * FROM t_pluss_bankstatementverfyperson WHERE BSVP_BANK_ID=" + bankId + " AND BSVP_STATUS=1";
            // xRs = dbMySQL.DbOpenRset(mSQL);
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    bankStvPersonObj = new BankStvPerson();
                    bankStvPersonObj.Id = Convert.ToInt32(xRs["BSVP_ID"]);
                    bankStvPersonObj.Bank_Id = Convert.ToInt16(xRs["BSVP_BANK_ID"]);
                    bankStvPersonObj.Branch_Id = Convert.ToInt16(xRs["BSVP_BRANCH_ID"]);
                    bankStvPersonObj.Name = Convert.ToString(xRs["BSVP_NAME"]);
                    bankStvPersonObj.EmailAddress = Convert.ToString(xRs["BSVP_EMAILADDRESS"]);
                    bankStvPersonObj.Status = Convert.ToInt32(xRs["BSVP_STATUS"]);
                }
                xRs.Close();
            }
            return bankStvPersonObj;
        }
        public Int32 GetBSVPersonId(Int32 bankId)
        {
            Int32 returnValue = 0;
            string mSQL = "SELECT BSVP_ID FROM t_pluss_bankstatementverfyperson WHERE BSVP_BANK_ID=" + bankId;
            returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }
        public List<BankStvPerson> SelectBankStvPersonList(Int16 bankId, Int16 branchId)
        {
            List<BankStvPerson> bankStvPersonObjList = new List<BankStvPerson>();
            BankStvPerson bankStvPersonObj = null;
            ADODB.Recordset xRs = new ADODB.Recordset();
            string mSQL = "SELECT * FROM t_pluss_bankstatementverfyperson WHERE BSVP_BANK_ID=" + bankId + " AND BSVP_BRANCH_ID=" + branchId + " AND BSVP_STATUS=1";
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                bankStvPersonObj = new BankStvPerson();
                bankStvPersonObj.Id = Convert.ToInt32(xRs.Fields["BSVP_ID"].Value);
                bankStvPersonObj.Bank_Id = Convert.ToInt16(xRs.Fields["BSVP_BANK_ID"].Value);
                bankStvPersonObj.Branch_Id = Convert.ToInt16(xRs.Fields["BSVP_BRANCH_ID"].Value);
                bankStvPersonObj.Name = Convert.ToString(xRs.Fields["BSVP_NAME"].Value);
                bankStvPersonObj.EmailAddress = Convert.ToString(xRs.Fields["BSVP_EMAILADDRESS"].Value);
                bankStvPersonObj.Status = Convert.ToInt32(xRs.Fields["BSVP_STATUS"].Value);
                bankStvPersonObjList.Add(bankStvPersonObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return bankStvPersonObjList;
        }

        public int InsertBankStvPerson(BankStvPerson bankStvPersonObj)
        {
            try
            {
                string sqlInsert = "insert into t_pluss_bankstatementverfyperson " +
                "(BSVP_BANK_ID, BSVP_BRANCH_ID, " +
                "BSVP_NAME, BSVP_EMAILADDRESS, BSVP_STATUS) " +
                "values " +
                "(" + bankStvPersonObj.Bank_Id + ", " + bankStvPersonObj.Branch_Id + ", '" +
                bankStvPersonObj.Name + "', '" + bankStvPersonObj.EmailAddress + "', " +
                bankStvPersonObj.Status + ")";

                int insert = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                return insert;
            }
            catch
            {
                return 0;
            }
            //return 0;
        }

        public int UpdateBankStvPerson(BankStvPerson bankStvPersonObj)
        {
            try
            {
                string sqlInsert = "UPDATE t_pluss_bankstatementverfyperson SET BSVP_NAME = '" +
                bankStvPersonObj.Name + "', BSVP_BANK_ID = " + bankStvPersonObj.Bank_Id + ", BSVP_BRANCH_ID = " +
                bankStvPersonObj.Branch_Id + ", BSVP_EMAILADDRESS = '" +
                bankStvPersonObj.EmailAddress + "', BSVP_STATUS = " +
                bankStvPersonObj.Status + " WHERE BSVP_ID = " + bankStvPersonObj.Id;

                int update = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                return update;
            }
            catch
            {
                return 0;
            }
            //return 0;
        }

        public int DeleteBankStvPerson(int bankStvPersonId)
        {
            string queryString = "DELETE FROM t_pluss_bankstatementverfyperson WHERE BSVP_ID = " + bankStvPersonId;

            try
            {
                int status = DbQueryManager.ExecuteNonQuery(queryString);
                return status;
            }
            catch { return 0; }
        }



        public List<BankStvPerson> SelectBankStvPersonList()
        {
            List<BankStvPerson> bankStvPersonObjList = new List<BankStvPerson>();
            BankStvPerson bankStvPersonObj = null;
            //ADODB.Recordset xRs = new ADODB.Recordset();
            string mSQL =
                "SELECT * from t_pluss_bankstatementverfyperson as T1 Left Join t_pluss_bank  as T2 ON BSVP_BANK_ID=BANK_ID";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    bankStvPersonObj = new BankStvPerson();
                    bankStvPersonObj.Id = Convert.ToInt32(xRs["BSVP_ID"]);
                    bankStvPersonObj.Bank_Id = Convert.ToInt16(xRs["BSVP_BANK_ID"]);
                    //bankStvPersonObj.Branch_Id = Convert.ToInt16(xRs.Fields["BSVP_BRANCH_ID"].Value);
                    bankStvPersonObj.Name = Convert.ToString(xRs["BSVP_NAME"]);
                    bankStvPersonObj.EmailAddress = Convert.ToString(xRs["BSVP_EMAILADDRESS"]);
                    bankStvPersonObj.Status = Convert.ToInt32(xRs["BSVP_STATUS"]);
                    bankStvPersonObj.BankName = Convert.ToString(xRs["BANK_NM"]);
                    //bankStvPersonObj.BranchName = Convert.ToString(xRs.Fields["BRAN_NAME"].Value);
                    bankStvPersonObjList.Add(bankStvPersonObj);
                }
                xRs.Close();

            }
            return bankStvPersonObjList;
        }

        public List<BankStvPerson> SelectBankStvPersonList(string searchingKey)
        {
            List<BankStvPerson> bankStvPersonObjList = new List<BankStvPerson>();
            BankStvPerson bankStvPersonObj = null;
            string mSQL =
                "SELECT * from t_pluss_bankstatementverfyperson as T1 Left Join t_pluss_bank  as T2 ON BSVP_BANK_ID=BANK_ID where BSVP_NAME like ('%" +
                searchingKey + "%') or BANK_NM like ('%" + searchingKey + "%') or " +
                " BSVP_EMAILADDRESS like ('%" + searchingKey + "%')";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();

                while (xRs.Read())
                {
                    bankStvPersonObj = new BankStvPerson();
                    bankStvPersonObj.Id = Convert.ToInt32(xRs["BSVP_ID"]);
                    bankStvPersonObj.Bank_Id = Convert.ToInt16(xRs["BSVP_BANK_ID"]);
                    //bankStvPersonObj.Branch_Id = Convert.ToInt16(xRs.Fields["BSVP_BRANCH_ID"].Value);
                    bankStvPersonObj.Name = Convert.ToString(xRs["BSVP_NAME"]);
                    bankStvPersonObj.EmailAddress = Convert.ToString(xRs["BSVP_EMAILADDRESS"]);
                    bankStvPersonObj.Status = Convert.ToInt32(xRs["BSVP_STATUS"]);
                    bankStvPersonObj.BankName = Convert.ToString(xRs["BANK_NM"]);
                    //bankStvPersonObj.BranchName = Convert.ToString(xRs.Fields["BRAN_NAME"].Value);
                    bankStvPersonObjList.Add(bankStvPersonObj);
                }
                xRs.Close();
            }
            return bankStvPersonObjList;

        }

        public List<string> GetBankBrnchNameById(int bankId, int branchId)
        {
            List<string> info = new List<string>();
            ADODB.Recordset xRs = new ADODB.Recordset();
            string mSQL = "select* from t_pluss_bank inner join t_pluss_branch on " +
                "t_pluss_bank.BANK_ID=t_pluss_branch.BRAN_BANK_ID " +
                "where BANK_ID=" + bankId + " AND BRAN_ID= " + branchId +
                " order by t_pluss_bank.BANK_ID";
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                info.Add(Convert.ToString(xRs.Fields["BANK_NM"].Value));
                info.Add(Convert.ToString(xRs.Fields["BRAN_NAME"].Value));
                xRs.MoveNext();
            }
            xRs.Close();
            return info;
        }
    }
}
