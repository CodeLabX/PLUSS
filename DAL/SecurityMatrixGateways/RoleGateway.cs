﻿using BusinessEntities.SecurityMatrixEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilities;

namespace DAL.SecurityMatrixGateways
{
    public class RoleGateway
    {
        public List<RoleModel> Get()
        {
            string sql = "SELECT * FROM Roles";

            List<RoleModel> list = Data<RoleModel>.DataSource(sql);

            return list;
        }

        public RoleModel Get(int id)
        {
            string sql = "SELECT * FROM Roles WHERE Id=" + id;

            RoleModel role = Data<RoleModel>.DataSource(sql).FirstOrDefault();

            return role;
        }
    }
}
