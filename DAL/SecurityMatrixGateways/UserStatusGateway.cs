﻿using BusinessEntities.SecurityMatrixEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilities;

namespace DAL.SecurityMatrixGateways
{
    public class UserStatusGateway
    {
        public List<UserStatusModel> Get()
        {
            List<UserStatusModel> list = new List<UserStatusModel>();


            Array itemValues = System.Enum.GetValues(typeof(UserStatus));
            Array itemNames = System.Enum.GetNames(typeof(UserStatus));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                list.Add(new UserStatusModel
                {
                    Value = Convert.ToInt32(itemValues.GetValue(i)),
                    Name = itemNames.GetValue(i).ToString()
                });
            }

            return list;
        } 
        public UserStatusModel Get(int value)
        {
            List<UserStatusModel> list = new List<UserStatusModel>();

            UserStatusModel model = null;


            Array itemValues = System.Enum.GetValues(typeof(UserStatus));
            Array itemNames = System.Enum.GetNames(typeof(UserStatus));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                int val = Convert.ToInt32(itemValues.GetValue(i));
                if (val == value)
                {
                    model = new UserStatusModel
                    {
                        Value = val,
                        Name = itemNames.GetValue(i).ToString()
                    }; 
                }
            }

            return model;
        }
    }
}
