﻿using AzUtilities;
using Bat.Common;
using BusinessEntities;
using BusinessEntities.SecurityMatrixEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Utilities;

namespace DAL.SecurityMatrixGateways
{
    public class PageAccessPermission
    {
        private string GetAccessibleModuleConditionForRole(int rid)
        {
            try
            {
                string sql = string.Format(@"
SELECT s.* FROM SecurityMatrix s
INNER JOIN Roles r ON s.RoleId=r.Id
WHERE r.Id={0}", rid);

                DataTable table = DbQueryManager.GetTable(sql);

                string condition = "";

                DataRow row = table.Rows[0];

                for (int index = 0; index < table.Columns.Count; index++)
                {
                    if (Convert.ToBoolean(row[index]))
                        condition += ",'" + table.Columns[index].ColumnName + "'";
                }

                condition = condition.Remove(0, 1);
                return condition;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                return "";
            }
        }
        public int GetMenuId(HttpContext context)
        {
            try
            {
                string uri = context.Request.Url.AbsolutePath;

                string sql = string.Format(@"SELECT * FROM [dbo].[Pages] WHERE Url='{0}'", uri);

                DataTable table = DbQueryManager.GetTable(sql);

                if (table == null)
                    return 0;

                return Convert.ToInt32(table.Rows[0]["Id"]);
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }

            return 0;
        }

        public bool HasUserAccess(HttpContext context, User user)
        {
            try
            {
                DataTable table = GetMenusForUser(user);
                int menuId = GetMenuId(context);

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (Convert.ToInt32(table.Rows[i]["MenuId"]) == menuId)
                        return true;
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
            return false;
        }

        public DataTable GetMenusForUser(User user)
        {
            try
            {
                string condition = GetAccessibleModuleConditionForRole(user.Type);

                string sql = string.Format(@"
SELECT  p.Id AS MenuId, ParentId, Page AS Title, Page AS Description, Url AS URL, '' AS Level FROM Modules m
INNER JOIN ModuleWisePages mp  ON m.Id=mp.ModuleId
INNER JOIN Pages p ON p.Id=mp.PageId
WHERE m.Code IN ({0})", condition);

                return DbQueryManager.GetTable(sql);
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                return null;
            }

        }
        public List<MenuModel> GetMenuListForUser(User user)
        {
            try
            {
                string condition = GetAccessibleModuleConditionForRole(user.Type);

                string sql = string.Format(@"
SELECT  p.Id AS MenuId, ParentId, Page AS Title, Page AS Description, Url AS URL, '' AS Level FROM Modules m
INNER JOIN ModuleWisePages mp  ON m.Id=mp.ModuleId
INNER JOIN Pages p ON p.Id=mp.PageId
WHERE m.Code IN ({0})", condition);

                return Data<MenuModel>.DataSource(sql);
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                return null;
            }

        }

        public DataTable BindMenuData(int parentmenuId, User user)
        {
            //declaration of variable used  
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt = GetMenusForUser(user);

            if (dt.Rows.Count > 0)
            {
                ds.Tables.Add(dt);
                var dv = ds.Tables[0].DefaultView;
                dv.RowFilter = "ParentId='" + parentmenuId + "'";
                DataSet ds1 = new DataSet();
                var newdt = dv.ToTable();
                return newdt;
            }
            else
            {
                return null;
            }
        }

        /// <summary>  
        /// This is a recursive function to fetchout the data to create a menu from data table  
        /// </summary>  
        /// <param name="dt">datatable</param>  
        /// <param name="parentMenuId">parent menu Id of integer type</param>  
        /// <param name="parentMenuItem"> Menu Item control</param>  
        public void DynamicMenuControlPopulation(DataTable dt, int parentMenuId, MenuItem parentMenuItem, Menu menu, User user)
        {
            string currentPage = Path.GetFileName(HttpContext.Current.Request.Url.AbsolutePath);
            foreach (DataRow row in dt.Rows)
            {
                MenuItem menuItem = new MenuItem
                {
                    Value = row["MenuId"].ToString(),
                    Text = row["Title"].ToString(),
                    NavigateUrl = row["URL"].ToString(),
                    Selected = row["URL"].ToString().EndsWith(currentPage, StringComparison.CurrentCultureIgnoreCase)
                };
                if (parentMenuId == 0)
                {
                    menu.Items.Add(menuItem);
                    DataTable dtChild = BindMenuData(int.Parse(menuItem.Value), user);
                    DynamicMenuControlPopulation(dtChild, int.Parse(menuItem.Value), menuItem, null, user);
                }
                else
                {

                    parentMenuItem.ChildItems.Add(menuItem);
                    DataTable dtChild = BindMenuData(int.Parse(menuItem.Value), user);
                    DynamicMenuControlPopulation(dtChild, int.Parse(menuItem.Value), menuItem, null, user);

                }
            }
        }

        public int GetUserRoleId(User user)
        {
            try
            {
                string sql = string.Format(@"SELECT USER_LEVEL FROM [dbo].[t_pluss_user]
WHERE USER_ID={0}", user.UserId);

                DataTable table = DbQueryManager.GetTable(sql);

                return 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
