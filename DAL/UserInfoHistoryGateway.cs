﻿using System;
using System.Collections.Generic;
using Bat.Common;
using BusinessEntities;
using Utilities;

namespace DAL
{
    public class UserInfoHistoryGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        public UserInfoHistoryGateway()
        {
            dbMySQL = new DatabaseConnection();
        }

        public List<UserInfoHistory> GetUserInfoHistory()
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            string mSQL = "SELECT * FROM t_pluss_userinfohistory";
            return userInfoHistoryObjList = GetListData(mSQL);
        }
        public List<UserInfoHistory> GetUserInfoHistory(string loginId)
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            string mSQL = "SELECT * FROM t_pluss_userinfohistory WHERE USIH_USERLOGIID='" + loginId + "'";
            return userInfoHistoryObjList = GetListData(mSQL);
        }
        public List<UserInfoHistory> GetUserInfoHistory(string loginId,DateTime changeDate)
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            string mSQL = "SELECT * FROM t_pluss_userinfohistory WHERE USIH_USERLOGIID='" + loginId + "' AND DATE_FORMAT(USIH_CHANGEDATE,'%Y-%m-%d')='" + changeDate.ToString("yyyy-MM-dd") + "'";
            return userInfoHistoryObjList = GetListData(mSQL);
        }
        public List<UserInfoHistory> GetUserInfoHistory(string loginId,DateTime startDate, DateTime endDate)
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            string mSQL = "SELECT * FROM t_pluss_userinfohistory WHERE USIH_USERLOGIID='" + loginId + "' AND DATE_FORMAT(USIH_CHANGEDATE,'%Y-%m-%d') BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' AND '" + endDate.ToString("yyyy-MM-dd") + "'";
            return userInfoHistoryObjList = GetListData(mSQL);
        }

        public List<UserInfoHistory> GetLastChangeUserInfoHistory(string loginId,int numberOfChange)
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            string mSQL = string.Format("SELECT TOP {1} * FROM t_pluss_userinfohistory WHERE USIH_USERLOGIID='{0}' ORDER BY USIH_ID DESC", loginId, numberOfChange);
            return userInfoHistoryObjList = Data<UserInfoHistory>.DataSource(mSQL);
        }

        public int SendDataInToDB(UserInfoHistory userInfoHistoryObj)
        {
            int returnValue = 0;
            if (userInfoHistoryObj == null)
            {
                returnValue=0;
            }
            else
            {
                returnValue = InsertUserInfoHistory(userInfoHistoryObj);
            }
            return returnValue;
        }
        private int InsertUserInfoHistory(UserInfoHistory userInfoHistoryObj)
        {
            int returnValue = 0;
            string mSQL = "INSERT INTO t_pluss_userinfohistory (USIH_USERLOGIID,USIH_USERPASSWORD,USIH_CHANGEDATE) VALUES(" +
                        "'" + userInfoHistoryObj.UserLogInId + "','" +
                        userInfoHistoryObj.PassWord + "','" +
                        userInfoHistoryObj.ChangeDate.ToString("yyyy-MM-dd HH:mm:ss") + "')";

            
            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                returnValue = INSERTE;
            }
            else
            {
                returnValue = ERROR;
            }
            return returnValue;
        }
        public UserInfoHistory GetLastPsswordChangedDate(string loginId)
        {
            UserInfoHistory userInfoHistoryObj = null;
            string mSQL = "SELECT * FROM t_pluss_userinfohistory WHERE USIH_USERLOGIID='" + loginId + "' ORDER BY USIH_ID DESC LIMIT 0,1";
            List<UserInfoHistory> userInfoHistoryObjList = GetListData(mSQL);
            userInfoHistoryObj = new UserInfoHistory();
            return userInfoHistoryObj = (UserInfoHistory)userInfoHistoryObjList.Find(findUserInfoHistory => findUserInfoHistory.UserLogInId == loginId);

        }
        private List<UserInfoHistory> GetListData(string mSQL)
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            UserInfoHistory userInfoHistoryObj = null;
            ADODB.Recordset xRs = new ADODB.Recordset();
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                userInfoHistoryObj = new UserInfoHistory();
                userInfoHistoryObj.UserHistoryId = Convert.ToInt32(xRs.Fields["USIH_ID"].Value);
                userInfoHistoryObj.UserLogInId = Convert.ToString(xRs.Fields["USIH_USERLOGIID"].Value);
                userInfoHistoryObj.PassWord = Convert.ToString(xRs.Fields["USIH_USERPASSWORD"].Value);
                userInfoHistoryObj.ChangeDate = Convert.ToDateTime(xRs.Fields["USIH_CHANGEDATE"].Value);
                userInfoHistoryObjList.Add(userInfoHistoryObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return userInfoHistoryObjList;
        }
    }
}
