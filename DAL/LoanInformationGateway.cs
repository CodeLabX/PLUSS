﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Bat.Common;
using System.Data.SqlClient;

namespace DAL
{
    /// <summary>
    /// Loan information gateway class.
    /// </summary>
    public class LoanInformationGateway
    {
        private LoanInformation loanInformationObj=null;
        private const int SCBACCNO_LENGTH = 11;
        
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LoanInformationGateway()
        {
            //Constructor logic here. 
           
        }
        #endregion

        #region Particular Applicant Loan information
        /// <summary>
        /// This method give particular applicant loan information.
        /// </summary>
        /// <param name="llId">Gets applicant LLID</param>
        /// <returns>Returns loan information object</returns>
        public LoanInformation GetApplicantLoanInfo(Int32 llId)
        {
            string queryString = String.Format("SELECT LOAN_APPLI_ID, CUST_NM, FATHE_NM, MOTHE_NM, COMPA_NM, " +
                                               "IsNULL((Case DOB WHEN '' THEN '' WHEN '' THEN ''" +
                                               " ELSE CAST(DOB AS CHAR) END),'') AS DOB , TIN_NO," +
                                               " MOBIL, OFF_PH, RES_PH, LOAN_CONTACTNO4, " +
                                               "LOAN_CONTACTNO5, ACC_NO, LOAP_ACNOFORSCB2, " +
                                               "LOAP_ACNOFORSCB3, LOAP_ACNOFORSCB4, " +
                                               "LOAP_ACNOFORSCB5, ID_TYPE, ID_NO, " +
                                               "IsNULL((Case ID_EXPIR WHEN '' THEN '' WHEN '' " +
                                               "THEN '' ELSE CAST(ID_EXPIR AS CHAR) END),'') " +
                                               "AS ID_EXPIR, LOAP_IDTYPE2, LOAP_IDNO2,IsNULL((Case LOAP_EXPIRYDATE2" +
                                               " WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(LOAP_EXPIRYDATE2 AS CHAR) END),'')" +
                                               " AS LOAP_EXPIRYDATE2 , LOAP_IDTYPE3,  LOAP_IDNO3, IsNULL((Case LOAP_EXPIRYDATE3 WHEN '' " +
                                               "THEN '' WHEN '' THEN '' ELSE CAST(LOAP_EXPIRYDATE3 AS CHAR) END),'') AS LOAP_EXPIRYDATE3,LOAP_IDTYPE4,LOAP_IDNO4," +
                                               "IsNULL((Case LOAP_EXPIRYDATE4 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(LOAP_EXPIRYDATE4 AS CHAR) END),'')" +
                                               " AS LOAP_EXPIRYDATE4,LOAP_IDTYPE5,LOAP_IDNO5,  IsNULL((Case LOAP_EXPIRYDATE5 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(LOAP_EXPIRYDATE5 AS CHAR) END),'') " +
                                               "AS LOAP_EXPIRYDATE5, OTHER_ACC_BANK_ID_1, OTHER_ACC_BR_ID_1, OTHER_ACC_NO_1, OTHER_ACC_TYPE_1, OTHER_ACC_BANK_ID_2, OTHER_ACC_BR_ID_2, OTHER_ACC_NO_2, OTHER_ACC_TYPE_2," +
                                               " OTHER_ACC_BANK_ID_3, OTHER_ACC_BR_ID_3, OTHER_ACC_NO_3, OTHER_ACC_TYPE_3, LOAP_OTHERACWITHBANK_ID4, LOAP_OTHERACWITHBRANCH_ID4, LOAP_OTHERACWITHACNO4, LOAP_ORHREACTYPE_ID4," +
                                               " LOAP_OTHERACWITHBANK_ID5, LOAP_OTHERACWITHBRANCH_ID5, LOAP_OTHERACWITHACNO5, LOAP_ORHREACTYPE_ID5, BK1.BANK_NM AS OTHBANK1,BH1.BRAN_NAME AS OTHBRNCH1, BH1.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS1," +
                                               " BK2.BANK_NM AS OTHBANK2,BH2.BRAN_NAME AS OTHBRNCH2, BH2.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS2, BK3.BANK_NM AS OTHBANK3,BH3.BRAN_NAME AS OTHBRNCH3, BH3.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS3, BK4.BANK_NM" +
                                               " AS OTHBANK4,BH4.BRAN_NAME AS OTHBRNCH4, BH4.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS4, BK5.BANK_NM AS OTHBANK5,BH5.BRAN_NAME AS OTHBRNCH5, BH5.BRAN_BRABCH_STSTUS AS OTHBRNCHSTS5 FROM " +
                                               "loan_app_info AS LA LEFT JOIN t_pluss_bank AS BK1 ON " +
                                               "LA.OTHER_ACC_BANK_ID_1 = BK1.BANK_ID LEFT JOIN t_pluss_branch" +
                                               " AS BH1 ON LA.OTHER_ACC_BR_ID_1 = BH1.BRAN_ID LEFT JOIN t_pluss_bank " +
                                               "AS BK2 ON LA.OTHER_ACC_BANK_ID_2 = BK2.BANK_ID LEFT JOIN t_pluss_branch AS BH2 ON" +
                                               " LA.OTHER_ACC_BR_ID_2 = BH2.BRAN_ID LEFT JOIN t_pluss_bank AS BK3 ON LA.OTHER_ACC_BANK_ID_3 = BK3.BANK_ID" +
                                               " LEFT JOIN t_pluss_branch AS BH3 ON LA.OTHER_ACC_BR_ID_3 = BH3.BRAN_ID LEFT JOIN t_pluss_bank AS BK4 ON " +
                                               "LA.LOAP_OTHERACWITHBANK_ID4 = BK4.BANK_ID LEFT JOIN t_pluss_branch AS BH4 ON LA.LOAP_OTHERACWITHBRANCH_ID4 = " +
                                               "BH4.BRAN_ID LEFT JOIN t_pluss_bank AS BK5 ON LA.LOAP_OTHERACWITHBANK_ID5 = BK5.BANK_ID LEFT JOIN t_pluss_branch AS " +
                                               "BH5 ON LA.LOAP_OTHERACWITHBRANCH_ID5 = BH5.BRAN_ID WHERE LOAN_APPLI_ID ={0}", llId);
            try
            {
                DbDataReader loanInfoDataReaderObj = DbQueryManager.ExecuteReader(queryString);

                while (loanInfoDataReaderObj.Read())
                {
                    loanInformationObj = new LoanInformation();
                    loanInformationObj.LLId = Convert.ToInt32(loanInfoDataReaderObj["LOAN_APPLI_ID"]);
                    loanInformationObj.ApplicantName = Convert.ToString(loanInfoDataReaderObj["CUST_NM"]);
                    loanInformationObj.FatherName = Convert.ToString(loanInfoDataReaderObj["FATHE_NM"]);
                    loanInformationObj.MotherName = Convert.ToString(loanInfoDataReaderObj["MOTHE_NM"]);
                    loanInformationObj.Business = Convert.ToString(loanInfoDataReaderObj["COMPA_NM"]);

                    if (!String.IsNullOrEmpty(loanInfoDataReaderObj["DOB"].ToString()))
                    {
                        loanInformationObj.DOB = Convert.ToDateTime(loanInfoDataReaderObj["DOB"]); 
                    }
                   

                    loanInformationObj.TINNo = loanInfoDataReaderObj["TIN_NO"].ToString();
                    loanInformationObj.ContactNumber.ContactNo1 = loanInfoDataReaderObj["MOBIL"].ToString();
                    loanInformationObj.ContactNumber.ContactNo2 = loanInfoDataReaderObj["OFF_PH"].ToString();
                    loanInformationObj.ContactNumber.ContactNo3 = Convert.ToString(loanInfoDataReaderObj["RES_PH"]);
                    loanInformationObj.ContactNumber.ContactNo4 = Convert.ToString(loanInfoDataReaderObj["LOAN_CONTACTNO4"]);
                    loanInformationObj.ContactNumber.ContactNo5 = Convert.ToString(loanInfoDataReaderObj["LOAN_CONTACTNO5"]);

                    if (String.IsNullOrEmpty(loanInfoDataReaderObj["ACC_NO"].ToString()))
                    {
                        loanInformationObj.AccountNo.PreAccNo1 = "";
                        loanInformationObj.AccountNo.MasterAccNo1 = "";
                        loanInformationObj.AccountNo.PostAccNo1 = "";
                    }
                    else
                    {
                        if (loanInfoDataReaderObj["ACC_NO"].ToString().Length.Equals(SCBACCNO_LENGTH))
                        {
                            string accountNo1 = Convert.ToString(loanInfoDataReaderObj["ACC_NO"]);
                            string preAccNo1 = accountNo1.Substring(0, 2).ToString();
                            string masterAccNo1 = accountNo1.Substring(2, 7).ToString();
                            string postAccNo1 = accountNo1.Substring(9, 2).ToString();
                            loanInformationObj.AccountNo.PreAccNo1 = preAccNo1;
                            loanInformationObj.AccountNo.MasterAccNo1 = masterAccNo1;
                            loanInformationObj.AccountNo.PostAccNo1 = postAccNo1;
                        }
                        else
                        {
                            loanInformationObj.AccountNo.PreAccNo1 = "";
                            loanInformationObj.AccountNo.MasterAccNo1 = loanInfoDataReaderObj["ACC_NO"].ToString();
                            loanInformationObj.AccountNo.PostAccNo1 = "";
                        }
                    }
                    if (String.IsNullOrEmpty(loanInfoDataReaderObj["LOAP_ACNOFORSCB2"].ToString()))
                    {
                        loanInformationObj.AccountNo.PreAccNo2 = "";
                        loanInformationObj.AccountNo.MasterAccNo2 = "";
                        loanInformationObj.AccountNo.PostAccNo2 = "";
                    }
                    else
                    {
                        if (loanInfoDataReaderObj["LOAP_ACNOFORSCB2"].ToString().Length.Equals(SCBACCNO_LENGTH))
                        {
                            string accountNo2 = Convert.ToString(loanInfoDataReaderObj["LOAP_ACNOFORSCB2"]);
                            string preAccNo2 = accountNo2.Substring(0, 2).ToString();
                            string masterAccNo2 = accountNo2.Substring(2, 7).ToString();
                            string postAccNo2 = accountNo2.Substring(9, 2).ToString();
                            loanInformationObj.AccountNo.PreAccNo2 = preAccNo2;
                            loanInformationObj.AccountNo.MasterAccNo2 = masterAccNo2;
                            loanInformationObj.AccountNo.PostAccNo2 = postAccNo2;
                        }
                        else
                        {
                            loanInformationObj.AccountNo.PreAccNo2 = "";
                            loanInformationObj.AccountNo.MasterAccNo2 = loanInfoDataReaderObj["LOAP_ACNOFORSCB2"].ToString();
                            loanInformationObj.AccountNo.PostAccNo2 = "";
                        }
                    }
                    if (String.IsNullOrEmpty(loanInfoDataReaderObj["LOAP_ACNOFORSCB3"].ToString()))
                    {
                        loanInformationObj.AccountNo.PreAccNo3 = "";
                        loanInformationObj.AccountNo.MasterAccNo3 = "";
                        loanInformationObj.AccountNo.PostAccNo3 = "";
                    }
                    else
                    {
                        if (loanInfoDataReaderObj["LOAP_ACNOFORSCB3"].ToString().Length.Equals(SCBACCNO_LENGTH))
                        {
                            string accountNo3 = Convert.ToString(loanInfoDataReaderObj["LOAP_ACNOFORSCB3"]);
                            string preAccNo3 = accountNo3.Substring(0, 2).ToString();
                            string masterAccNo3 = accountNo3.Substring(2, 7).ToString();
                            string postAccNo3 = accountNo3.Substring(9, 2).ToString();
                            loanInformationObj.AccountNo.PreAccNo3 = preAccNo3;
                            loanInformationObj.AccountNo.MasterAccNo3 = masterAccNo3;
                            loanInformationObj.AccountNo.PostAccNo3 = postAccNo3;
                        }
                        else
                        {
                            loanInformationObj.AccountNo.PreAccNo3 = "";
                            loanInformationObj.AccountNo.MasterAccNo3 = loanInfoDataReaderObj["LOAP_ACNOFORSCB3"].ToString();
                            loanInformationObj.AccountNo.PostAccNo3 = "";
                        }
                    }
                    if (String.IsNullOrEmpty(loanInfoDataReaderObj["LOAP_ACNOFORSCB4"].ToString()))
                    {
                        loanInformationObj.AccountNo.PreAccNo4 = "";
                        loanInformationObj.AccountNo.MasterAccNo4 = "";
                        loanInformationObj.AccountNo.PostAccNo4 = "";
                    }
                    else
                    {
                        if (loanInfoDataReaderObj["LOAP_ACNOFORSCB4"].ToString().Length.Equals(SCBACCNO_LENGTH))
                        {
                            string accountNo4 = Convert.ToString(loanInfoDataReaderObj["LOAP_ACNOFORSCB4"]);
                            string preAccNo4 = accountNo4.Substring(0, 2).ToString();
                            string masterAccNo4 = accountNo4.Substring(2, 7).ToString();
                            string postAccNo4 = accountNo4.Substring(9, 2).ToString();
                            loanInformationObj.AccountNo.PreAccNo4 = preAccNo4;
                            loanInformationObj.AccountNo.MasterAccNo4 = masterAccNo4;
                            loanInformationObj.AccountNo.PostAccNo4 = postAccNo4;
                        }
                        else
                        {
                            loanInformationObj.AccountNo.PreAccNo4 = "";
                            loanInformationObj.AccountNo.MasterAccNo4 = loanInfoDataReaderObj["LOAP_ACNOFORSCB4"].ToString();
                            loanInformationObj.AccountNo.PostAccNo4 = "";
                        }
                    }
                    if (String.IsNullOrEmpty(loanInfoDataReaderObj["LOAP_ACNOFORSCB5"].ToString()))
                    {
                        loanInformationObj.AccountNo.PreAccNo5 = "";
                        loanInformationObj.AccountNo.MasterAccNo5 = "";
                        loanInformationObj.AccountNo.PostAccNo5 = "";
                    }
                    else
                    {
                        if (loanInfoDataReaderObj["LOAP_ACNOFORSCB5"].ToString().Length.Equals(SCBACCNO_LENGTH))
                        {
                            string accountNo5 = Convert.ToString(loanInfoDataReaderObj["LOAP_ACNOFORSCB5"]);
                            string preAccNo5 = accountNo5.Substring(0, 2).ToString();
                            string masterAccNo5 = accountNo5.Substring(2, 7).ToString();
                            string postAccNo5 = accountNo5.Substring(9, 2).ToString();
                            loanInformationObj.AccountNo.PreAccNo5 = preAccNo5;
                            loanInformationObj.AccountNo.MasterAccNo5 = masterAccNo5;
                            loanInformationObj.AccountNo.PostAccNo5 = postAccNo5;
                        }
                        else
                        {
                            loanInformationObj.AccountNo.PreAccNo5 = "";
                            loanInformationObj.AccountNo.MasterAccNo5 = loanInfoDataReaderObj["LOAP_ACNOFORSCB5"].ToString();
                            loanInformationObj.AccountNo.PostAccNo5 = "";
                        }
                    }
                    loanInformationObj.ApplicantId.Type1 = loanInfoDataReaderObj["ID_TYPE"].ToString();
                    loanInformationObj.ApplicantId.Id1 = loanInfoDataReaderObj["ID_NO"].ToString();

                    if (!String.IsNullOrEmpty(loanInfoDataReaderObj["ID_EXPIR"].ToString()))
                    {
                        loanInformationObj.ApplicantId.ExpiryDate1 = Convert.ToDateTime(loanInfoDataReaderObj["ID_EXPIR"].ToString());
                    }
                  

                    loanInformationObj.ApplicantId.Type2 = loanInfoDataReaderObj["LOAP_IDTYPE2"].ToString();
                    loanInformationObj.ApplicantId.Id2 = loanInfoDataReaderObj["LOAP_IDNO2"].ToString();


                    if (!String.IsNullOrEmpty(loanInfoDataReaderObj["LOAP_EXPIRYDATE2"].ToString()))
                    {
                        loanInformationObj.ApplicantId.ExpiryDate2 = Convert.ToDateTime(loanInfoDataReaderObj["LOAP_EXPIRYDATE2"].ToString());
                    }
                  
                   

                    loanInformationObj.ApplicantId.Type3 = loanInfoDataReaderObj["LOAP_IDTYPE3"].ToString();
                    loanInformationObj.ApplicantId.Id3 = loanInfoDataReaderObj["LOAP_IDNO3"].ToString();

                    if (!String.IsNullOrEmpty(loanInfoDataReaderObj["LOAP_EXPIRYDATE3"].ToString()))
                    {
                        loanInformationObj.ApplicantId.ExpiryDate3 = Convert.ToDateTime(loanInfoDataReaderObj["LOAP_EXPIRYDATE3"].ToString());
                    }
                    

                    loanInformationObj.ApplicantId.Type4 = loanInfoDataReaderObj["LOAP_IDTYPE4"].ToString();
                    loanInformationObj.ApplicantId.Id4 = loanInfoDataReaderObj["LOAP_IDNO4"].ToString();

                    if (!String.IsNullOrEmpty(loanInfoDataReaderObj["LOAP_EXPIRYDATE4"].ToString()))
                    {
                        loanInformationObj.ApplicantId.ExpiryDate4 = Convert.ToDateTime(loanInfoDataReaderObj["LOAP_EXPIRYDATE4"].ToString());
                    }

                  

                    loanInformationObj.ApplicantId.Type5 = loanInfoDataReaderObj["LOAP_IDTYPE5"].ToString();
                    loanInformationObj.ApplicantId.Id5 = loanInfoDataReaderObj["LOAP_IDNO5"].ToString();

                    

                    if (!String.IsNullOrEmpty(loanInfoDataReaderObj["LOAP_EXPIRYDATE5"].ToString()))
                    {
                        loanInformationObj.ApplicantId.ExpiryDate5 = Convert.ToDateTime(loanInfoDataReaderObj["LOAP_EXPIRYDATE5"].ToString());
                    }

                   

                    loanInformationObj.OtherBankAccount.OtherBank1 = loanInfoDataReaderObj["OTHBANK1"].ToString();
                    loanInformationObj.OtherBankAccount.OtherBranch1 = loanInfoDataReaderObj["OTHBRNCH1"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccNo1 = loanInfoDataReaderObj["OTHER_ACC_NO_1"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccType1 = loanInfoDataReaderObj["OTHER_ACC_TYPE_1"].ToString();
                    loanInformationObj.OtherBankAccount.OtherStatus1 = loanInfoDataReaderObj["OTHBRNCHSTS1"].ToString();

                    loanInformationObj.OtherBankAccount.OtherBank2 = loanInfoDataReaderObj["OTHBANK2"].ToString();
                    loanInformationObj.OtherBankAccount.OtherBranch2 = loanInfoDataReaderObj["OTHBRNCH2"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccNo2 = loanInfoDataReaderObj["OTHER_ACC_NO_2"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccType2 = loanInfoDataReaderObj["OTHER_ACC_TYPE_2"].ToString();
                    loanInformationObj.OtherBankAccount.OtherStatus2 = loanInfoDataReaderObj["OTHBRNCHSTS2"].ToString();

                    loanInformationObj.OtherBankAccount.OtherBank3 = loanInfoDataReaderObj["OTHBANK3"].ToString();
                    loanInformationObj.OtherBankAccount.OtherBranch3 = loanInfoDataReaderObj["OTHBRNCH3"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccNo3 = loanInfoDataReaderObj["OTHER_ACC_NO_3"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccType3 = loanInfoDataReaderObj["OTHER_ACC_TYPE_3"].ToString();
                    loanInformationObj.OtherBankAccount.OtherStatus3 = loanInfoDataReaderObj["OTHBRNCHSTS3"].ToString();

                    loanInformationObj.OtherBankAccount.OtherBank4 = loanInfoDataReaderObj["OTHBANK4"].ToString();
                    loanInformationObj.OtherBankAccount.OtherBranch4 = loanInfoDataReaderObj["OTHBRNCH4"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccNo4 = loanInfoDataReaderObj["LOAP_OTHERACWITHACNO4"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccType4 = loanInfoDataReaderObj["LOAP_ORHREACTYPE_ID4"].ToString();
                    loanInformationObj.OtherBankAccount.OtherStatus4 = loanInfoDataReaderObj["OTHBRNCHSTS4"].ToString();

                    loanInformationObj.OtherBankAccount.OtherBank5 = loanInfoDataReaderObj["OTHBANK5"].ToString();
                    loanInformationObj.OtherBankAccount.OtherBranch5 = loanInfoDataReaderObj["OTHBRNCH5"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccNo5 = loanInfoDataReaderObj["LOAP_OTHERACWITHACNO5"].ToString();
                    loanInformationObj.OtherBankAccount.OtherAccType5 = loanInfoDataReaderObj["LOAP_ORHREACTYPE_ID5"].ToString();
                    loanInformationObj.OtherBankAccount.OtherStatus5 = loanInfoDataReaderObj["OTHBRNCHSTS5"].ToString();

               }
               loanInfoDataReaderObj.Close();
            }
            catch (SqlException sqlException)
            {
                throw sqlException;
            }
            catch (FormatException formatException)
            {
                throw formatException;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //loanInfoDataTableObj.Clear();
            }
            return loanInformationObj;
        }

        #endregion
    }
}
