﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{
    public class ProductGateway
    {
        public DatabaseConnection dbMySQL = null;
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ProductGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        #endregion
        public Int32 GetProductId(string productName, string productType)
        {
            Int32 returnValue = 0;
            string mSQL = "SELECT PROD_ID FROM t_pluss_product WHERE REPLACE(UPPER(PROD_NAME),' ','')='" + productName.ToUpper().Trim().Replace(" ", "") + "'";
            returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }

        #region Private Members
        private List<Product> productObjList = new List<Product>();
        private Product productObj = new Product();
        #endregion

        /// <summary>
        /// Get all products from database
        /// </summary>
        /// <returns></returns>
        public List<Product> GetAllProductInfo()
        {
            DataTable productDataTableObj = null;

            string queryString = "SELECT PROD_ID ProdID, PROD_NAME ProdName, PROD_CODE ProdCode, " +
            "PROD_TYPE ProdType, PROD_STATUS ProdStatus FROM t_pluss_product ";

            productDataTableObj = DbQueryManager.GetTable(queryString);
            if (productDataTableObj != null)
            {
                DataTableReader productDataReaderObj = productDataTableObj.CreateDataReader();

                while (productDataReaderObj.Read())
                {
                    productObj = new Product();
                    productObj.ProdID = Convert.ToInt32(productDataReaderObj["ProdID"]);
                    productObj.ProdName = Convert.ToString(productDataReaderObj["ProdName"]);
                    productObj.ProdCode = Convert.ToString(productDataReaderObj["ProdCode"]);
                    productObj.ProdType = Convert.ToString(productDataReaderObj["ProdType"]);
                    productObj.ProdStatus = Convert.ToInt32(productDataReaderObj["ProdStatus"]);
                    if (productObj.ProdStatus == 0)
                    { productObj.ProdStatusName = "Inactive"; }
                    else { productObj.ProdStatusName = "Active"; }

                    productObjList.Add(productObj);
                }
                productDataReaderObj.Close();
            }
            return productObjList;
        }


        public List<Product> GetAllProductInfoById(int productCode)
        {
            DataTable productDataTableObj = null;

            string queryString = "SELECT PROD_ID ProdID, PROD_NAME ProdName, PROD_CODE ProdCode, " +
            "PROD_TYPE ProdType, PROD_STATUS ProdStatus FROM t_pluss_product " +
            "where PROD_CODE = " + productCode;

            productDataTableObj = DbQueryManager.GetTable(queryString);
            if (productDataTableObj != null)
            {
                DataTableReader productDataReaderObj = productDataTableObj.CreateDataReader();

                while (productDataReaderObj.Read())
                {
                    productObj = new Product();
                    productObj.ProdID = Convert.ToInt32(productDataReaderObj["ProdID"]);
                    productObj.ProdName = Convert.ToString(productDataReaderObj["ProdName"]);
                    productObj.ProdCode = Convert.ToString(productDataReaderObj["ProdCode"]);
                    productObj.ProdType = Convert.ToString(productDataReaderObj["ProdType"]);
                    productObj.ProdStatus = Convert.ToInt32(productDataReaderObj["ProdStatus"]);

                    productObjList.Add(productObj);
                }
                productDataReaderObj.Close();
            }
            return productObjList;
        }

        public List<Product> GetAllDistinctProductInfoById(int productId)
        {
            DataTable productDataTableObj = null;

            string queryString = "SELECT PROD_ID ProdID, PROD_NAME ProdName, PROD_CODE ProdCode, " +
            "PROD_TYPE ProdType, PROD_STATUS ProdStatus FROM t_pluss_product " +
            "where PROD_ID = " + productId;

            productDataTableObj = DbQueryManager.GetTable(queryString);
            if (productDataTableObj != null)
            {
                DataTableReader productDataReaderObj = productDataTableObj.CreateDataReader();

                while (productDataReaderObj.Read())
                {
                    productObj = new Product();
                    productObj.ProdID = Convert.ToInt32(productDataReaderObj["ProdID"]);
                    productObj.ProdName = Convert.ToString(productDataReaderObj["ProdName"]);
                    productObj.ProdCode = Convert.ToString(productDataReaderObj["ProdCode"]);
                    productObj.ProdType = Convert.ToString(productDataReaderObj["ProdType"]);
                    productObj.ProdStatus = Convert.ToInt32(productDataReaderObj["ProdStatus"]);
                    switch (Convert.ToString(productDataReaderObj["ProdType"]))
                    {
                        case "U":
                            productObj.ProdTypeName = "Unsecure";
                            break;
                        case "S":
                            productObj.ProdTypeName = "Secure";
                            break;
                        case "C":
                            productObj.ProdTypeName = "Cash Secure";
                            break;

                    }

                    productObjList.Add(productObj);
                }
                productDataReaderObj.Close();
            }
            return productObjList;
        }


        public List<Product> GetAllProductTypes()
        {
            DataTable productDataTableObj = null;
            var productTypeList = new List<Product>();

            string queryString = "select distinct PROD_TYPE from t_pluss_product";

            productDataTableObj = DbQueryManager.GetTable(queryString);
            if (productDataTableObj != null)
            {
                DataTableReader productDataReaderObj = productDataTableObj.CreateDataReader();

                while (productDataReaderObj.Read())
                {
                    productTypeList.Add(ProductTypeConverter(Convert.ToString(productDataReaderObj["PROD_TYPE"])));
                }
                productDataReaderObj.Close();
            }
            return productTypeList;
        }
        public Product ProductTypeConverter(string productType)
        {
            var objProduct = new Product();
            objProduct.ProdType = productType;
            switch (productType)
            {
                case "U":
                    objProduct.ProductName = "Unsecure";
                    break;
                case "S":
                    objProduct.ProductName = "Secure";
                    break;
                case "C":
                    objProduct.ProductName = "Cash Secure";
                    break;
            }
            return objProduct;
        }
        


        public int InsertProduct(Product productObj)
        {
            int action_id = 1;
            int module_id = 2;

            try
            {
                string sqlInsert = "insert into t_pluss_product (" +
                    "PROD_NAME, PROD_CODE, PROD_TYPE, " +
                    "PROD_STATUS) " +
                    "values('" + productObj.ProdName.SpecialCharacterRemover() + "', '" +
                    productObj.ProdCode + "', '" + productObj.ProdType.SpecialCharacterRemover() + "', " + productObj.ProdStatus + ")";
                //for loan locator audit trial
                Audit(action_id, module_id, sqlInsert, productObj);
                int insert = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                //queryText = sqlInsert.Replace('\'', '\"');
                //queryType = "Insert";
                return insert;
            }
            catch
            {
                return 0;
            }
        }


        public int DeleteProduct(int productCode)
        {
            string queryString = "DELETE FROM t_pluss_product WHERE PROD_CODE = " + productCode;

            try
            {
                int status = DbQueryManager.ExecuteNonQuery(queryString);
                return status;
            }
            catch { return 0; }
        }


        public int UpdateProduct(Product productObj)
        {
            int action_id = 2;
            int module_id = 2;

            try
            {
                string sqlInsert = "UPDATE t_pluss_product SET PROD_NAME = '" +
                    productObj.ProdName + "', PROD_CODE = '" +
                    productObj.ProdCode + "', PROD_TYPE = '" +
                    productObj.ProdType + "', PROD_STATUS = " +
                    productObj.ProdStatus + " WHERE PROD_ID = " +
                    productObj.ProdID;
                //for loan locator audit trial
                Audit(action_id, module_id, sqlInsert, productObj);
                int update = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                //queryText = sqlInsert.Replace('\'', '\"');
                //queryType = "Insert";
                return update;
            }
            catch
            {
                return 0;
            }
            return 0;
        }



        public List<Product> GetAllProductsBySearchingKey(string key)
        {
            DataTable productDataTableObj = null;
            productObjList = new List<Product>();

            string queryString = "SELECT PROD_ID, PROD_NAME, PROD_CODE , " +
            "PROD_TYPE , PROD_STATUS FROM t_pluss_product " +
            "where PROD_CODE like('%" + key.SpecialCharacterRemover() + "%') or " +
            "PROD_NAME like('%" + key.SpecialCharacterRemover() + "%') or " +
            "PROD_TYPE  like('%" + key.SpecialCharacterRemover() + "%')";

            productDataTableObj = DbQueryManager.GetTable(queryString);
            if (productDataTableObj != null)
            {
                DataTableReader productDataReaderObj = productDataTableObj.CreateDataReader();

                while (productDataReaderObj.Read())
                {
                    productObj = new Product();
                    productObj.ProdID = Convert.ToInt32(productDataReaderObj["PROD_ID"]);
                    productObj.ProdName = Convert.ToString(productDataReaderObj["PROD_NAME"]);
                    productObj.ProdCode = Convert.ToString(productDataReaderObj["PROD_CODE"]);
                    productObj.ProdType = Convert.ToString(productDataReaderObj["PROD_TYPE"]);
                    productObj.ProdStatus = Convert.ToInt32(productDataReaderObj["PROD_STATUS"]);

                    productObjList.Add(productObj);
                }
                productDataReaderObj.Close();
            }
            return productObjList;
        }

        public List<string> GetAllDistinctProductInfo()
        {
            DataTable productDataTableObj = null;
            List<string> productTypeList = new List<string>();

            string queryString = "SELECT distinct PROD_TYPE ProdType FROM t_pluss_product ";

            productDataTableObj = DbQueryManager.GetTable(queryString);
            if (productDataTableObj != null)
            {
                DataTableReader productDataReaderObj = productDataTableObj.CreateDataReader();

                while (productDataReaderObj.Read())
                {
                    productTypeList.Add(Convert.ToString(productDataReaderObj["ProdType"]));

                }
                productDataReaderObj.Close();
            }
            return productTypeList;
        }
        public string GetProductName(int productId)
        {
            string returnValue = "";
            string mSQL = "SELECT PROD_NAME FROM t_pluss_product WHERE PROD_ID = " + productId;
            return returnValue = DbQueryManager.ExecuteScalar(mSQL).ToString();
         
        }

        public void Audit(int action_id, int module_id, string str_sql1, Product productObj)
        {
            string query = "insert into audit_trail(ID_LEARNER,ACTION_ID,MODULE_ID,QRY_DATE,QRY_TIME,QRY_STRING) " +
                          "values (" + productObj.IdLearner + "," + action_id + "," + module_id + ", CAST(getdate() as DATE) ,CAST (getdate() as Time),'" + AddSlash(str_sql1) + "')";
            DbQueryManager.ExecuteNonQuery(query);
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
