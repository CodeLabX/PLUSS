﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;
using Utilities;

namespace DAL
{
    public class DumpGateway
    {
        SqlCommand cmd;
        SqlConnection con;

        public DumpGateway()
        {
            cmd = new SqlCommand();
            var cocn = new CommonConnection();
            con = new SqlConnection(cocn.GetConnectionString());
        }
        public DataTable GetCiDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("p_GetCIChecklistDump", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable GetLamsDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("p_GetLAMSDump", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetMisDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("p_GetDailyMISDump", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetNegativeListDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("pGetNegativelistEmployerByDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetLoanAppInfoDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("pGetLoanAppInfoByDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetDedupInfoDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("pGetDedupInfoByDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetRTOBInfoDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("pGetRTOBInfoByDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetMFUUploadHistoryDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("pGetMFUUploadHistoryByDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetMFUDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("pGetMFUDumpByDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetReportViewersInfoDump(DateTime fromDate, DateTime toDate, User user)
        {
            try
            {
                cmd = new SqlCommand("pGetReportViewersInfoByDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                con.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                dataTable.Load(reader);
                con.Close();
                return dataTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
