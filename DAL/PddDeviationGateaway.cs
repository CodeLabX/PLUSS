﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// PddDeviationGateaway Classs.
    /// </summary>
    public class PddDeviationGateaway
    {
        PddDeviation pddDeviationObj;
        List<PddDeviation> pddDeviationObjList;

        /// <summary>
        /// This method select all pdd deviaton.
        /// </summary>
        /// <returns>Returns  a PddDeviation list object.</returns>
        public List<PddDeviation> GetAllPddDeviation()
        {
            pddDeviationObjList = new List<PddDeviation>();

            DataTable pddDeviationDataTableObj = null;

            string queryString = "SELECT * from t_pluss_pdddeviation ";

            pddDeviationDataTableObj = DbQueryManager.GetTable(queryString);
            if (pddDeviationDataTableObj != null)
            {
                DataTableReader pddDeviationDataReaderObj = pddDeviationDataTableObj.CreateDataReader();

                while (pddDeviationDataReaderObj.Read())
                {
                    pddDeviationObj = new PddDeviation();
                    pddDeviationObj.Id = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_ID"]);
                    pddDeviationObj.Code = Convert.ToString(pddDeviationDataReaderObj["PDEV_CODE"]);
                    pddDeviationObj.DescriptionId = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_DESCRIPTIONID"]);
                    pddDeviationObj.Description = Convert.ToString(pddDeviationDataReaderObj["PDEV_DESCRIPTION"]);
                    pddDeviationObj.Level = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_LEVEL"]);

                    pddDeviationObjList.Add(pddDeviationObj);
                }
                pddDeviationDataReaderObj.Close();
            }
            return pddDeviationObjList;
        }


        //public PddDeviation GetPddDeviationOnLevel(int level)
        //{
        //    DataTable pddDeviationDataTableObj = null;

        //    string queryString = "select * " +
        //                "from t_pluss_pdddeviation where PDEV_LEVEL = " + level;

        //    pddDeviationDataTableObj = DbQueryManager.GetTable(queryString);
        //    if (pddDeviationDataTableObj != null)
        //    {
        //        DataTableReader pddDeviationDataReaderObj = pddDeviationDataTableObj.CreateDataReader();

        //        while (pddDeviationDataReaderObj.Read())
        //        {
        //            pddDeviationObj = new PddDeviation();
        //            pddDeviationObj.Id = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_ID"]);
        //            pddDeviationObj.Code = Convert.ToString(pddDeviationDataReaderObj["PDEV_CODE"]);
        //            pddDeviationObj.DescriptionId = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_DESCRIPTIONID"]);
        //            pddDeviationObj.Description = Convert.ToString(pddDeviationDataReaderObj["PDEV_DESCRIPTION"]);
        //            pddDeviationObj.Level = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_LEVEL"]);
        //        }
        //        pddDeviationDataReaderObj.Close();
        //    }
        //    return pddDeviationObj;
        //}


        /// <summary>
        /// This method select pdd deviation info against level.
        /// </summary>
        /// <param name="level">Gets a level id</param>
        /// <returns>Returns a PddDeviation list object.</returns>
        public List<PddDeviation> GetPddDeviationOnLevel(int level)
        {
            pddDeviationObjList = new List<PddDeviation>();

            DataTable pddDeviationDataTableObj = null;

            string queryString = "select * " +
                        "from t_pluss_pdddeviation where PDEV_LEVEL = " + level;

            pddDeviationDataTableObj = DbQueryManager.GetTable(queryString);
            if (pddDeviationDataTableObj != null)
            {
                DataTableReader pddDeviationDataReaderObj = pddDeviationDataTableObj.CreateDataReader();

                while (pddDeviationDataReaderObj.Read())
                {
                    pddDeviationObj = new PddDeviation();
                    pddDeviationObj.Id = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_ID"]);
                    pddDeviationObj.Code = Convert.ToString(pddDeviationDataReaderObj["PDEV_CODE"]);
//                    pddDeviationObj.DescriptionId = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_DESCRIPTIONID"]);
                    pddDeviationObj.Description = Convert.ToString(pddDeviationDataReaderObj["PDEV_DESCRIPTION"]);
                    pddDeviationObj.Level = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_LEVEL"]);

                    pddDeviationObjList.Add(pddDeviationObj);
                }
                pddDeviationDataReaderObj.Close();
            }
            return pddDeviationObjList;
        }


        /// <summary>
        /// This method select pdd deviation code
        /// </summary>
        /// <param name="descriptionId">Gets a description id.</param>
        /// <returns>Returns a pdd deviation code.</returns>
        public string GetPddDeviationCode(string descriptionId)
        {
            DataTable pddDeviationDataTableObj = null;

            string queryString = "select * " +
                        "from t_pluss_pdddeviation where PDEV_ID = " + descriptionId;

            pddDeviationDataTableObj = DbQueryManager.GetTable(queryString);
            if (pddDeviationDataTableObj != null)
            {
                DataTableReader pddDeviationDataReaderObj = pddDeviationDataTableObj.CreateDataReader();

                while (pddDeviationDataReaderObj.Read())
                {
                    pddDeviationObj = new PddDeviation();
                    pddDeviationObj.Id = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_ID"]);
                    pddDeviationObj.Code = Convert.ToString(pddDeviationDataReaderObj["PDEV_CODE"]);
                    //pddDeviationObj.DescriptionId = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_DESCRIPTIONID"]);
                    pddDeviationObj.Description = Convert.ToString(pddDeviationDataReaderObj["PDEV_DESCRIPTION"]);
                    pddDeviationObj.Level = Convert.ToInt32(pddDeviationDataReaderObj["PDEV_LEVEL"]);
                }
                pddDeviationDataReaderObj.Close();
            }
            return pddDeviationObj.Code;
        }
    }
}
