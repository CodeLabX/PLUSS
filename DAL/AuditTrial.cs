using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    public class AuditTrial
    {


        public static bool SaveAudit(string url, string host, string userId, string activity, string description,string UserType)
        {
            try
            {
                var newAudit = new Audit();
                
                var xx = System.Net.Dns.GetHostName();
                newAudit.HostName = xx;// host;
                newAudit.UserId = userId;
                newAudit.ActionDateAndTime = DateTime.Now;
                newAudit.RequestedUrl = url;
                try
                {
                    newAudit.IpAddress = GetIpAddress(newAudit.HostName);
                }
                catch (Exception)
                {
                    throw;
                }
                newAudit.Activity = activity;
                newAudit.Description = description;
                newAudit.UserRole = UserType;

                return Save(newAudit);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        public static bool SaveAudit(string url, string host, string userId, string activity, string description, string oldValues, string newValues, string UserType)
        {
            try
            {
                var newAudit = new Audit();
                var xx = System.Net.Dns.GetHostName();
                newAudit.HostName = xx;// host;
                newAudit.UserId = userId;
                newAudit.ActionDateAndTime = DateTime.Now;
                newAudit.RequestedUrl = url;
                try
                {
                    newAudit.IpAddress = GetIpAddress(newAudit.HostName);
                }
                catch (Exception)
                {
                    throw;
                }
                newAudit.Activity = activity;
                newAudit.Description = description;
                newAudit.OldValues = oldValues;
                newAudit.NewValues = newValues;
                newAudit.UserRole = UserType;

                return Save(newAudit, true);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static bool Save(Audit newAudit)
        {
            try
            {
                string mSql = "INSERT INTO t_pluss_audittrails (UserId,ActionDateAndTime,RequestedUrl,IpAddress,HostName,Activity,Description , UserType) VALUES(" +
                              "'" + newAudit.UserId + "'" + ",'" + newAudit.ActionDateAndTime.ToString("yyyy-MM-dd HH:mm:ss") + "','" + newAudit.RequestedUrl + "','" + newAudit.IpAddress + "','" + newAudit.HostName + "','" + newAudit.Activity + "','" + newAudit.Description + "','" + newAudit.UserRole + "')";
                return DbQueryManager.ExecuteNonQuery(mSql) > -1;
            }
            catch (Exception e)
            {
                throw new Exception("Error ! while save Audit Trail");
            }


        } 
        private static bool Save(Audit newAudit, bool hasOldNewValues)
        {
            try
            {
                string mSql = "INSERT INTO t_pluss_audittrails (UserId,ActionDateAndTime,RequestedUrl,IpAddress,HostName,Activity,Description, OldValues, NewValues, UserType) VALUES(" +
                              "'" + newAudit.UserId + "'" + ",'" + newAudit.ActionDateAndTime.ToString("yyyy-MM-dd HH:mm:ss") + "','" + newAudit.RequestedUrl + "','" + newAudit.IpAddress + "','" + newAudit.HostName + "','" + newAudit.Activity + "','" + newAudit.Description + "','" + newAudit.OldValues + "','" + newAudit.NewValues + "','" + newAudit.UserRole + "')";
                return DbQueryManager.ExecuteNonQuery(mSql) > -1;
            }
            catch (Exception e)
            {
                throw new Exception("Error ! while save Audit Trail");
            }


        }

        private static string GetIpAddress(string host)
        {
            try
            {
                IPHostEntry ipHostInfo = Dns.GetHostEntry(host);
                IPAddress ipAddress = ipHostInfo.AddressList
                    .FirstOrDefault(a => a.AddressFamily == AddressFamily.InterNetwork);

                return ipAddress.ToString();
            } catch (Exception ex) {
                throw new Exception("Error while converting Host to IP Address of host ("+host+")");
            }
        }
    }

}