﻿using System;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Data.Common;
using ADODB;
using WebConfigEncryption;
using System.Web;

namespace DAL
{
    public class DatabaseConnection
    {
        ADODB.Connection connectionPluss = new ADODB.Connection();
        ADODB.Connection connectionLoanLocator = new ADODB.Connection();

        public DatabaseConnection()
        {
            
        }
   
        public int DbOpen()
        {
            string connectionString = "";
            try
            {
                if (connectionPluss.State != 1)
                {
                    connectionString = WebConfigurationManager.ConnectionStrings["conString"].ToString();
                    var con = new ConnectionStringManager().DecryptConstring("conString");
                    string conString = @"driver={MySQL ODBC 3.51 Driver};" + con;
                    connectionPluss.ConnectionString = conString;
                    connectionPluss.Open(connectionPluss.ConnectionString, "", "", 0);
                }
                return connectionPluss.State;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Connection Error " + ex.Message.ToString());
            }
        }

        public int DbOpen(string connectionStringName)
        {
            string connectionString = "";
            try
            {
                if (connectionLoanLocator.State != 1)
                {
                    connectionString = WebConfigurationManager.ConnectionStrings[connectionStringName].ToString();
                    ConnectionStringManager manager = new ConnectionStringManager();
                    var con = manager.DecryptConstring("conStringLoanLocator");
                    string conString = @"driver={MySQL ODBC 3.51 Driver};" + con;
                    connectionLoanLocator.ConnectionString = conString;
                    connectionLoanLocator.Open(connectionLoanLocator.ConnectionString, "", "", 0);
                }
                return connectionLoanLocator.State;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Connection Error " + ex.Message.ToString());
            }

        }

        public void dbClose()
        {
            if (connectionPluss.State == 1)
            {
                connectionPluss.Close();
            }
        }
        public void dbClose(string connectionStringName)
        {
            if (connectionLoanLocator.State == 1)
            {
                connectionLoanLocator.Close();
            }
        }

        public int DbExecute(string sSQL)
        {
            try
            {
                object objAffected;
                DbOpen();
                connectionPluss.Execute(sSQL, out objAffected, 0);
                //dbClose();
                return Convert.ToInt32(objAffected);
            }
            catch (Exception ex)
            {
                return -1;
                throw new ApplicationException("SQL Error " + ex.Message.ToString());
            }

        }
        public int DbExecute(string sSQL, string connectionStringName)
        {
            try
            {
                object objAffected;
                DbOpen(connectionStringName);
                connectionLoanLocator.Execute(sSQL, out objAffected, 0);
                //dbClose(connectionStringName);
                return 0;
            }
            catch (Exception ex)
            {
                return -1;
                throw new ApplicationException("SQL Error " + ex.Message.ToString());
            }

        }

        public ADODB.Recordset DbOpenRset(string sSQL)
        {
            ADODB.Recordset recordSetObj = new Recordset();
            try
            {
                object objAffected;
                DbOpen();
                recordSetObj = connectionPluss.Execute(sSQL, out objAffected, 0);
                //dbClose();
                return recordSetObj;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("SQL Error " + ex.Message.ToString());
            }
        }

        public ADODB.Recordset DbOpenRset(string sSQL, string connectionStringName)
        {
            ADODB.Recordset recordSetObj = new Recordset();
            try
            {
                object objAffected;
                DbOpen(connectionStringName);
                recordSetObj = connectionLoanLocator.Execute(sSQL, out objAffected, 0);
                //dbClose(connectionStringName);
                return recordSetObj;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("SQL Error " + ex.Message.ToString());
            }
        }

        public string DbGetValue(string sSQL)
        {
            string sStr = null;
            try
            {
                ADODB.Recordset rs = new ADODB.Recordset();
                rs = DbOpenRset(sSQL);
                if (!rs.EOF)
                {
                    sStr = "" + rs.Fields[0].Value.ToString();
                }
                rs.Close();                
                //dbClose();
                return sStr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("SQL Error " + ex.Message.ToString());
            }
        }
        public string DbGetValue(string sSQL, string connectionStringName)
        {
            string sStr = "";
            try
            {
                ADODB.Recordset rs = new ADODB.Recordset();
                rs = DbOpenRset(sSQL, connectionStringName);
                if (!rs.EOF)
                {
                    sStr = "" + rs.Fields[0].Value.ToString();
                }
                rs.Close();                
                //dbClose(connectionStringName);
                return sStr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("SQL Error " + ex.Message.ToString());
            }
        }
    }
}
