﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities;
using System.Data;
using Bat.Common;
using AutoLoanService.Entity;
using AzUtilities;
using Utilities;
using AzUtilities.Common;
using BusinessEntities.SecurityMatrixEntities;
using DAL.SecurityMatrixGateways;
using System.Data.SqlClient;

namespace DAL
{
    public class UserGateway
    {
        enum ERRORMSGTYPE : short
        {
            DatabaseFail = 1,
            InvalidUserId = 2,
            InvalidPassword = 3
        }

        private enum ModuleInfo
        {
            action_id = 1, // action_id=1 is ADD action.
            module_id = 1 // module_id=1 is PERSON module.
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public UserGateway()
        {
            //Constructor logic here.
        }


        #region GetUserInfo

        /// <summary>
        /// This method populate user info.
        /// </summary>
        /// <param name="loginId">Gets login id.</param>
        /// <param name="passsword"></param>
        /// <param name="password">Gets user password.</param>
        /// <returns></returns>
        public User GetUserLoginInfo(string loginId, string passsword = null)
        {
            User userObj = new User();
            if (!IsValidUserId(loginId, passsword))
            {
                userObj.ErrorMsgType = (short)ERRORMSGTYPE.InvalidUserId;
                return userObj;
            }

            userObj.IsExists = false;
            string queryString = "";
            if (string.IsNullOrEmpty(passsword))
            {
                queryString = "SELECT USER_ID,USER_CODE, USER_NAME, USER_DESIGNATION, USER_PASSWORD, USER_LEVEL,USER_LEVELCODE,USER_LOGINSTATUS,AUTO_USER_LEVEL,IsAccessBoth, Roles.RoleCode FROM t_pluss_user INNER JOIN Roles ON Roles.Id=t_pluss_user.USER_LEVEL " +
                    "WHERE USER_CODE = '" + loginId + "' AND USER_STATUS=1 AND IS_APPROVED=1 AND (IS_DELETED=0 or IS_DELETED is NULL)";
            }
            else
            {
                queryString = "SELECT USER_ID, USER_CODE, USER_NAME, USER_DESIGNATION, USER_PASSWORD, USER_LEVEL,USER_LEVELCODE,USER_LOGINSTATUS,AUTO_USER_LEVEL,IsAccessBoth, Roles.RoleCode FROM t_pluss_user INNER JOIN Roles ON Roles.Id=t_pluss_user.USER_LEVEL " +
                    "WHERE USER_CODE = '" + loginId + "' AND USER_STATUS=1 AND IS_APPROVED=1  AND (IS_DELETED=0 or IS_DELETED is NULL) AND " +
                    "USER_PASSWORD = CONVERT(VARCHAR(32), HashBytes('MD5', '" + passsword + "'), 2)";
            }
            DataTable userDataTableObj = new DataTable();
            try
            {
                userDataTableObj = DbQueryManager.GetTable(queryString);
            }
            catch
            {
                userObj.ErrorMsgType = (short)ERRORMSGTYPE.DatabaseFail;
                return userObj;
            }
            if (userDataTableObj != null)
            {
                DataTableReader userDataTableReaderObj = userDataTableObj.CreateDataReader();
                if (userDataTableReaderObj != null)
                {
                    while (userDataTableReaderObj.Read())
                    {
                        userObj.IsExists = true;
                        userObj.Id = Convert.ToInt32(userDataTableReaderObj["USER_ID"].ToString());
                        userObj.UserId = Convert.ToString(userDataTableReaderObj["USER_CODE"].ToString());
                        userObj.Name = Convert.ToString(userDataTableReaderObj["USER_NAME"].ToString());
                        userObj.Designation = Convert.ToString(userDataTableReaderObj["USER_DESIGNATION"].ToString());
                        userObj.Password = Convert.ToString(userDataTableReaderObj["USER_PASSWORD"].ToString());
                        userObj.Type = Convert.ToInt16(userDataTableReaderObj["USER_LEVEL"].ToString());
                        userObj.UserLevelCode = Convert.ToString(userDataTableReaderObj["USER_LEVELCODE"].ToString());
                        userObj.userLoginStatus = Convert.ToInt32(userDataTableReaderObj["USER_LOGINSTATUS"].ToString());
                        userObj.RoleCode = Convert.ToString(userDataTableReaderObj["RoleCode"].ToString());
                        
                        if (userDataTableReaderObj["AUTO_USER_LEVEL"].ToString() != "")
                        {
                            userObj.AutoUserLevel_Id = Convert.ToInt32(userDataTableReaderObj["AUTO_USER_LEVEL"].ToString());
                        }
                        userObj.IsAccessBoth = userDataTableReaderObj["IsAccessBoth"] != DBNull.Value ? Convert.ToInt32(userDataTableReaderObj["IsAccessBoth"]) : 0;

                    }
                    userDataTableReaderObj.Close();
                }
                userDataTableObj.Clear();
            }
            return userObj;
        }

        #endregion
        public void UpdateLoginStatus(string userId)
        {
            string updateQueryString = "UPDATE t_pluss_user SET USER_STATUS=0 WHERE USER_CODE='" + userId + "'";
            int updateLoginStatus = DbQueryManager.ExecuteNonQuery(updateQueryString);
        }
        #region IsValidUserId
        /// <summary>
        /// This method check user id is valid or not.
        /// </summary>
        /// <param name="loginId">Gets login id.</param>
        /// <returns>True if match else false.</returns>
        public bool IsValidUserId(string loginId, string passsword = null)
        {
            bool returnValue = false;
            string queryString = "";
            if (string.IsNullOrEmpty(passsword))
            {
                queryString = "SELECT COUNT(*) FROM t_pluss_user WHERE USER_CODE = '" + loginId + "'";
            }
            else
            {
                queryString = "SELECT COUNT(*) FROM t_pluss_user WHERE USER_CODE = '" + loginId + "' and USER_PASSWORD = CONVERT(VARCHAR(32), HashBytes('MD5', '" + passsword + "'), 2)";
            }
            object userIdObj;
            try
            {
                userIdObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(userIdObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }
        #endregion

        #region IsValidPassword
        /// <summary>
        /// This method check password is valid or not.
        /// </summary>
        /// <param name="loginId">Gets login id.</param>
        /// <returns>True if match else false.</returns>
        public bool IsValidPassword(string loginId, string password)
        {
            bool returnValue = false;
            string queryString = "SELECT COUNT(*) FROM t_pluss_user WHERE USER_PASSWORD = MD5('" + password + "') AND USER_CODE = '" + loginId + "'";
            object userPasswordObj;
            try
            {
                userPasswordObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(userPasswordObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region GetUserInfo()
        /// <summary>
        /// This method populate all user info.
        /// </summary>  
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns a user list object.</returns>
        public List<User> GetUserInfo(string searchKey)
        {
            List<User> userListObj = new List<User>();

            //Coaded By Mazhar @21-11-2012
            string queryString = String.Format("SELECT distinct USER_ID, USER_CODE, USER_NAME, USER_DESIGNATION, USER_PASSWORD, USER_LEVELCODE, u.USER_LEVEL, UT.USLE_NAME, USER_STATUS, USER_EMAILADDRESS,IS_DELETED," +
                                               "learn.LOAN_DEPT_ID,IsAccessBoth,learn.ST_USER_LEVEL,  CASE USER_STATUS WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' END AS  USER_STATUS_MSG," +
                                               "CASE Is_Deleted WHEN 0 THEN 'False' WHEN 1 THEN 'True' END AS  IsDeleteMsg,IsNULL(U.AUTO_USER_LEVEL,0) as AUTO_USER_LEVEL,AUT.USLE_NAME AS AUTO_USLE_NAME, Module " +
                                               "FROM t_pluss_user AS U LEFT OUTER JOIN t_pluss_usertype AS UT ON U.USER_LEVEL = UT.USLE_ID " +
                                               "LEFT OUTER JOIN t_pluss_usertype AS AUT ON U.AUTO_USER_LEVEL = AUT.USLE_ID left outer join learner_details_table as Learn on U.USER_CODE=Learn.USERNAME" +
                                               " WHERE UPPER(USER_ID) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(USER_CODE) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                               " UPPER(USER_NAME) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(USER_EMAILADDRESS) LIKE(UPPER('%" + searchKey + "%')) OR" +
                                               " UPPER(USER_LEVELCODE) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(u.USER_LEVEL) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                               " UPPER(USER_STATUS) LIKE(UPPER('%" + searchKey + "%')) OR UPPER(UT.USLE_NAME) LIKE(UPPER('%" + searchKey + "%')) ORDER BY USER_NAME ASC");


            DataTable userDataTableObj = new DataTable();
            try
            {
                userDataTableObj = DbQueryManager.GetTable(queryString);


                if (userDataTableObj != null)
                {
                    DataTableReader userDataTableReaderObj = userDataTableObj.CreateDataReader();
                    if (userDataTableReaderObj != null)
                    {
                        while (userDataTableReaderObj.Read())
                        {
                            User userObj = new User();
                            userObj.Id = Convert.ToInt32(userDataTableReaderObj["USER_ID"].ToString());
                            userObj.UserId = Convert.ToString(userDataTableReaderObj["USER_CODE"].ToString());
                            userObj.Name = Convert.ToString(userDataTableReaderObj["USER_NAME"].ToString());
                            userObj.Designation = Convert.ToString(userDataTableReaderObj["USER_DESIGNATION"].ToString());
                            userObj.Password = Convert.ToString(userDataTableReaderObj["USER_PASSWORD"].ToString());
                            userObj.Type = Convert.ToInt16(userDataTableReaderObj["USER_LEVEL"].ToString());
                            userObj.UserLevelCode = Convert.ToString(userDataTableReaderObj["USER_LEVELCODE"].ToString());
                            userObj.UserType.Name = Convert.ToString(userDataTableReaderObj["USLE_NAME"]);
                            userObj.Status = Convert.ToInt16(userDataTableReaderObj["USER_STATUS"].ToString());
                            userObj.Email = Convert.ToString(userDataTableReaderObj["USER_EMAILADDRESS"]);
                            if (userDataTableReaderObj["IsAccessBoth"] != DBNull.Value)
                            {
                                userObj.IsAccessBoth = Convert.ToInt32(userDataTableReaderObj["IsAccessBoth"]);
                            }
                            if (userDataTableReaderObj["Module"] != DBNull.Value)
                            {
                                userObj.Module = Convert.ToInt32(userDataTableReaderObj["Module"]);
                            }

                            if (userDataTableReaderObj["ST_USER_LEVEL"] != DBNull.Value)
                            {
                                userObj.ST_USER_LEVEL = Convert.ToInt32(userDataTableReaderObj["ST_USER_LEVEL"]);
                            }
                            if (userDataTableReaderObj["LOAN_DEPT_ID"] != DBNull.Value)
                            {
                                userObj.LOAN_DEPT_ID = Convert.ToInt32(userDataTableReaderObj["LOAN_DEPT_ID"]);
                            }
                            // var isDeleted = Convert.ToInt16(userDataTableReaderObj["IS_DELETED"]); //commnent Rubel

                            var value = userDataTableReaderObj["IS_DELETED"];
                            var isDeleted = 0;
                            if (value == DBNull.Value)
                            {
                                isDeleted = 0;
                            }
                            else
                            {
                                isDeleted = Convert.ToInt16(userDataTableReaderObj["IS_DELETED"]);
                            }

                            if (isDeleted == 1)
                            {
                                userObj.StatusMsg = "Deleted";
                            }
                            else
                            {
                                userObj.StatusMsg = Convert.ToString(userDataTableReaderObj["USER_STATUS_MSG"].ToString());
                            }

                            userObj.AutoUserLevel_Id = Convert.ToInt32(userDataTableReaderObj["AUTO_USER_LEVEL"].ToString());
                            userObj.AutoUserLevel_Name = Convert.ToString(userDataTableReaderObj["AUTO_USLE_NAME"].ToString());
                            userObj.IsDeleteMsg = Convert.ToString(userDataTableReaderObj["IsDeleteMsg"].ToString());
                            userListObj.Add(userObj);
                        }
                        userDataTableReaderObj.Close();
                    }
                    userDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return userListObj;
        }

        public UserTModel GetUserByTempId(string id)
        {
            string sql = string.Format(@"SELECT * FROM t_pluss_user_temp WHERE Id='{0}' AND IS_REJECTED != 1", id);

            var temp = Data<UserTModel>.DataSource(sql).FirstOrDefault();

            if (temp != null)
                return temp;

            return null;
        }

        public UserTModel GetUserByTempLoginId(string id)
        {
            string sql = string.Format(@"SELECT * FROM t_pluss_user_temp WHERE USER_CODE='{0}' AND IS_REJECTED != 1", id);

            var temp = Data<UserTModel>.DataSource(sql).FirstOrDefault();

            if (temp != null)
                return temp;

            return null;
        }

        public string RejectUserCreation(UserTModel user, User currentUser)
        {
            try
            {
                //string deleteSql = string.Format(@"UPDATE FROM t_pluss_user_temp SET IS_DELETED=1 WHERE Id='{0}' AND USER_CODE='{1}' AND (IS_DELETED != 1 OR IS_DELETED IS NULL)", user.Id, user.USER_CODE);
                // DbQueryManager.ExecuteNonQuery(deleteSql);

                string sql = string.Format(@"UPDATE [dbo].[t_pluss_user_temp]
           SET [USER_STATUS] = '{0}'
            ,[IS_APPROVED] = '{1}'
           ,[IS_REJECTED] = '{2}'
           ,[APPROVER_COMMENT] = '{3}'
WHERE Id='{4}'
    ", (int)UserStatus.Rejected, 1, 1, user.APPROVER_COMMENT, user.Id);

                int rowAffected = DbQueryManager.ExecuteNonQuery(sql);

                if (rowAffected > 0)
                    return Operation.Success.ToString();

                return Operation.Failed.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                return Operation.Error.ToString();
            }
        }

        public UserTModel GetUserFromTempByLoginId(string loginId)
        {
            string sql = string.Format(@"SELECT * FROM t_pluss_user_temp WHERE USER_CODE='{0}' AND IS_REJECTED != 1", loginId);

            var temp = Data<UserTModel>.DataSource(sql).FirstOrDefault();

            if (temp != null)
                return temp;

            return null;
        }

        public UserTModel GetUserByLoginIdFromMain(string loginId)
        {
            //string tempCheck = string.Format(@"SELECT * FROM t_pluss_user_temp WHERE USER_CODE='{0}' AND IS_REJECTED!=1 ", loginId);

            //var tempFound = Data<UserTModel>.DataSource(tempCheck).FirstOrDefault();

            //if (tempFound != null)
            //{
            //    return tempFound;
            //}

            string sql = string.Format(@"SELECT * FROM t_pluss_user WHERE USER_CODE='{0}'", loginId);

            var temp = Data<UserTModel>.DataSource(sql).FirstOrDefault();

            if (temp != null)
                return temp;

            return null;
        }

        public bool UserExistByStaffId(string staffId, string tableName)
        {
            try
            {

                string sql = string.Format(@"SELECT * fROM [dbo].[{1}] WHERE USER_CODE='{0}' AND IS_REJECTED!=1", staffId, tableName);

                var temp = Data<UserTModel>.DataSource(sql);

                if (temp.Count > 0)
                    return true;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }

            return false;
        }

        public List<User> GetUnApproveUserInfo(string searchKey)
        {
            List<User> userListObj = new List<User>();

            //Coaded By Ashraful @13-10-2015
            string queryString = "SELECT distinct USER_ID, USER_CODE, USER_NAME,IsAccessBoth, USER_DESIGNATION, USER_PASSWORD, USER_LEVELCODE, U.USER_LEVEL, UT.USLE_NAME, USER_STATUS," +
                                 " USER_EMAILADDRESS,IS_DELETED,learn.LOAN_DEPT_ID,learn.ST_USER_LEVEL, CASE USER_STATUS WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' " +
                                 "END AS  USER_STATUS_MSG,isnull(U.AUTO_USER_LEVEL,0) as AUTO_USER_LEVEL,AUT.USLE_NAME AS AUTO_USLE_NAME, Module FROM t_pluss_user AS U LEFT" +
                                 " OUTER JOIN t_pluss_usertype AS UT ON U.USER_LEVEL = UT.USLE_ID LEFT OUTER JOIN t_pluss_usertype AS AUT ON U.AUTO_USER_LEVEL = " +
                                 "AUT.USLE_ID left outer join learner_details_table as Learn on U.USER_CODE=Learn.USERNAME WHERE IS_APPROVED = 0 and( UPPER(USER_ID) " +
                                 "LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(USER_CODE) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                "UPPER(USER_NAME) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(USER_EMAILADDRESS) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                "UPPER(USER_LEVELCODE) LIKE(UPPER('%" + searchKey + "%')) OR  UPPER(U.USER_LEVEL) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                "UPPER(USER_STATUS) LIKE(UPPER('%" + searchKey + "%')) OR UPPER(UT.USLE_NAME) LIKE(UPPER('%" + searchKey + "%'))) " +

                                 "ORDER BY USER_NAME ASC";


            DataTable userDataTableObj = new DataTable();
            try
            {
                userDataTableObj = DbQueryManager.GetTable(queryString);


                if (userDataTableObj != null)
                {
                    DataTableReader userDataTableReaderObj = userDataTableObj.CreateDataReader();
                    if (userDataTableReaderObj != null)
                    {
                        while (userDataTableReaderObj.Read())
                        {
                            User userObj = new User();
                            userObj.Id = Convert.ToInt32(userDataTableReaderObj["USER_ID"].ToString());
                            userObj.UserId = Convert.ToString(userDataTableReaderObj["USER_CODE"].ToString());
                            userObj.Name = Convert.ToString(userDataTableReaderObj["USER_NAME"].ToString());
                            userObj.Designation = Convert.ToString(userDataTableReaderObj["USER_DESIGNATION"].ToString());
                            userObj.Password = Convert.ToString(userDataTableReaderObj["USER_PASSWORD"].ToString());
                            userObj.Type = Convert.ToInt16(userDataTableReaderObj["USER_LEVEL"].ToString());
                            userObj.UserLevelCode = Convert.ToString(userDataTableReaderObj["USER_LEVELCODE"].ToString());
                            userObj.UserType.Name = Convert.ToString(userDataTableReaderObj["USLE_NAME"]);
                            userObj.Status = Convert.ToInt16(userDataTableReaderObj["USER_STATUS"].ToString());
                            userObj.Email = Convert.ToString(userDataTableReaderObj["USER_EMAILADDRESS"]);
                            if (userDataTableReaderObj["ST_USER_LEVEL"] != DBNull.Value)
                            {
                                userObj.ST_USER_LEVEL = Convert.ToInt32(userDataTableReaderObj["ST_USER_LEVEL"]);
                            }
                            if (userDataTableReaderObj["Module"] != DBNull.Value)
                            {
                                userObj.Module = Convert.ToInt32(userDataTableReaderObj["Module"]);
                            }
                            if (userDataTableReaderObj["LOAN_DEPT_ID"] != DBNull.Value)
                            {
                                userObj.LOAN_DEPT_ID = Convert.ToInt32(userDataTableReaderObj["LOAN_DEPT_ID"]);
                            }
                            if (userDataTableReaderObj["IsAccessBoth"] != DBNull.Value)
                            {
                                userObj.IsAccessBoth = Convert.ToInt32(userDataTableReaderObj["IsAccessBoth"]);
                            }
                            //var isDeleted = Convert.ToInt16(userDataTableReaderObj["IS_DELETED"]);
                            var value = userDataTableReaderObj["IS_DELETED"];
                            var isDeleted = 0;
                            if (value == DBNull.Value)
                            {
                                isDeleted = 0;
                            }
                            else
                            {
                                isDeleted = Convert.ToInt16(userDataTableReaderObj["IS_DELETED"]);
                            }

                            if (isDeleted == 1)
                            {
                                userObj.StatusMsg = "Deleted";
                            }
                            else
                            {
                                userObj.StatusMsg = Convert.ToString(userDataTableReaderObj["USER_STATUS_MSG"].ToString());
                            }
                            userObj.AutoUserLevel_Id = Convert.ToInt32(userDataTableReaderObj["AUTO_USER_LEVEL"].ToString());
                            userObj.AutoUserLevel_Name = Convert.ToString(userDataTableReaderObj["AUTO_USLE_NAME"].ToString());
                            userListObj.Add(userObj);
                        }
                        userDataTableReaderObj.Close();
                    }
                    userDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return userListObj;
        }

        public User GetUserInfo(Int32 userId)
        {
            User userObj = null;
            string queryString = "SELECT USER_ID, USER_CODE, USER_NAME, USER_DESIGNATION, USER_PASSWORD, USER_LEVELCODE, USER_LEVEL, UT.USLE_NAME, USER_STATUS, USER_EMAILADDRESS, " +
                                 "CASE USER_STATUS WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' END AS  USER_STATUS_MSG " +
                                 "FROM t_pluss_user AS U " +
                                 "INNER JOIN t_pluss_usertype AS UT ON U.USER_LEVEL = UT.USLE_ID " +
                                 "WHERE USER_ID=" + userId;
            DataTable userDataTableObj = new DataTable();
            try
            {
                userDataTableObj = DbQueryManager.GetTable(queryString);


                if (userDataTableObj != null)
                {
                    DataTableReader userDataTableReaderObj = userDataTableObj.CreateDataReader();
                    if (userDataTableReaderObj != null)
                    {
                        while (userDataTableReaderObj.Read())
                        {
                            userObj = new User();
                            userObj.Id = Convert.ToInt32(userDataTableReaderObj["USER_ID"].ToString());
                            userObj.UserId = Convert.ToString(userDataTableReaderObj["USER_CODE"].ToString());
                            userObj.Name = Convert.ToString(userDataTableReaderObj["USER_NAME"].ToString());
                            userObj.Designation = Convert.ToString(userDataTableReaderObj["USER_DESIGNATION"].ToString());
                            userObj.Password = Convert.ToString(userDataTableReaderObj["USER_PASSWORD"].ToString());
                            userObj.Type = Convert.ToInt16(userDataTableReaderObj["USER_LEVEL"].ToString());
                            userObj.UserLevelCode = Convert.ToString(userDataTableReaderObj["USER_LEVELCODE"].ToString());
                            userObj.UserType.Name = Convert.ToString(userDataTableReaderObj["USLE_NAME"]);
                            userObj.Status = Convert.ToInt16(userDataTableReaderObj["USER_STATUS"].ToString());
                            userObj.Email = Convert.ToString(userDataTableReaderObj["USER_EMAILADDRESS"]);
                            userObj.StatusMsg = Convert.ToString(userDataTableReaderObj["USER_STATUS_MSG"].ToString());
                        }
                        userDataTableReaderObj.Close();
                    }
                    userDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return userObj;
        }
        #endregion

        #region GetAllUserType()
        /// <summary>
        /// This method populate all user type.
        /// </summary>       
        /// <returns>Returns a user type list object.</returns>
        public List<UserType> GetAllUserType()
        {
            List<UserType> userTypeListObj = new List<UserType>();
            string queryString = "SELECT USLE_ID, USLE_CODE, USLE_NAME FROM t_pluss_usertype where usle_code != 'AT' ORDER BY USLE_NAME ";

            DataTable userTypeDataTableObj = new DataTable();
            try
            {
                userTypeDataTableObj = DbQueryManager.GetTable(queryString);


                if (userTypeDataTableObj != null)
                {
                    DataTableReader userTypeDataTableReaderObj = userTypeDataTableObj.CreateDataReader();
                    if (userTypeDataTableReaderObj != null)
                    {
                        while (userTypeDataTableReaderObj.Read())
                        {
                            UserType userTypeObj = new UserType();
                            userTypeObj.Id = Convert.ToInt32(userTypeDataTableReaderObj["USLE_ID"].ToString());
                            userTypeObj.Code = Convert.ToString(userTypeDataTableReaderObj["USLE_CODE"].ToString());
                            userTypeObj.Name = Convert.ToString(userTypeDataTableReaderObj["USLE_NAME"].ToString());
                            userTypeListObj.Add(userTypeObj);
                        }
                        userTypeDataTableReaderObj.Close();
                    }
                    userTypeDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return userTypeListObj;
        }
        #endregion

        //code by arif start

        #region GetAllAutoUserType()
        public List<autoUserType> GetAllAutoUserType()
        {
            List<autoUserType> userTypeListObj = new List<autoUserType>();
            string queryString = "SELECT USLE_NAME,USLE_ID FROM t_pluss_usertype where usle_code = 'AT' ORDER BY USLE_NAME ";
            DataTable userTypeDataTableObj = new DataTable();
            try
            {
                userTypeDataTableObj = DbQueryManager.GetTable(queryString);


                if (userTypeDataTableObj != null)
                {
                    DataTableReader userTypeDataTableReaderObj = userTypeDataTableObj.CreateDataReader();
                    if (userTypeDataTableReaderObj != null)
                    {
                        while (userTypeDataTableReaderObj.Read())
                        {
                            autoUserType userTypeObj = new autoUserType();
                            //userTypeObj.Id = Convert.ToInt32(userTypeDataTableReaderObj["USLE_ID"].ToString());
                            //userTypeObj.Code = Convert.ToString(userTypeDataTableReaderObj["USLE_CODE"].ToString());
                            userTypeObj.Name = Convert.ToString(userTypeDataTableReaderObj["USLE_NAME"].ToString());
                            userTypeObj.USLE_ID = Convert.ToString(userTypeDataTableReaderObj["USLE_ID"].ToString());
                            userTypeListObj.Add(userTypeObj);
                        }
                        userTypeDataTableReaderObj.Close();
                    }
                    userTypeDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return userTypeListObj;
        }
        #endregion


        #region getUserGroupIDs
        public List<autoUserType> GetAllAutoUserType(int userId)
        {
            List<autoUserType> userTypeListObj = new List<autoUserType>();
            string queryString = "SELECT USLE_NAME,USLE_ID FROM t_pluss_usertype where usle_code = 'AT' ORDER BY USLE_NAME ";
            DataTable userTypeDataTableObj = new DataTable();
            try
            {
                userTypeDataTableObj = DbQueryManager.GetTable(queryString);


                if (userTypeDataTableObj != null)
                {
                    DataTableReader userTypeDataTableReaderObj = userTypeDataTableObj.CreateDataReader();
                    if (userTypeDataTableReaderObj != null)
                    {
                        while (userTypeDataTableReaderObj.Read())
                        {
                            autoUserType userTypeObj = new autoUserType();
                            //userTypeObj.Id = Convert.ToInt32(userTypeDataTableReaderObj["USLE_ID"].ToString());
                            //userTypeObj.Code = Convert.ToString(userTypeDataTableReaderObj["USLE_CODE"].ToString());
                            userTypeObj.Name = Convert.ToString(userTypeDataTableReaderObj["USLE_NAME"].ToString());
                            userTypeObj.USLE_ID = Convert.ToString(userTypeDataTableReaderObj["USLE_ID"].ToString());
                            userTypeListObj.Add(userTypeObj);
                        }
                        userTypeDataTableReaderObj.Close();
                    }
                    userTypeDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return userTypeListObj;
        }
        #endregion


        public List<AutoUserTypeVsWorkflow> getAutoLoanStatePermission(short userType)
        {
            List<AutoUserTypeVsWorkflow> AutoUserTypeVsWorkflowList = new List<AutoUserTypeVsWorkflow>();

            string queryString = "SELECT * FROM auto_usertype_vs_workflow where UserTypeID = " + userType;
            DataTable dt = new DataTable();
            try
            {
                dt = DbQueryManager.GetTable(queryString);

                if (dt != null)
                {
                    DataTableReader AutoUserTypeVsWorkflowDataTableReaderObj = dt.CreateDataReader();
                    if (AutoUserTypeVsWorkflowDataTableReaderObj != null)
                    {
                        while (AutoUserTypeVsWorkflowDataTableReaderObj.Read())
                        {
                            AutoUserTypeVsWorkflow autoUserTypeVsWorkflow = new AutoUserTypeVsWorkflow();
                            //userTypeObj.Id = Convert.ToInt32(userTypeDataTableReaderObj["USLE_ID"].ToString());
                            //userTypeObj.Code = Convert.ToString(userTypeDataTableReaderObj["USLE_CODE"].ToString());
                            autoUserTypeVsWorkflow.ID = Convert.ToInt32(AutoUserTypeVsWorkflowDataTableReaderObj[0].ToString());
                            autoUserTypeVsWorkflow.UserTypeID = Convert.ToInt32(AutoUserTypeVsWorkflowDataTableReaderObj[1].ToString());
                            autoUserTypeVsWorkflow.WorkFlowID = Convert.ToInt32(AutoUserTypeVsWorkflowDataTableReaderObj[2].ToString());

                            AutoUserTypeVsWorkflowList.Add(autoUserTypeVsWorkflow);
                        }
                        AutoUserTypeVsWorkflowDataTableReaderObj.Close();
                    }
                    dt.Clear();
                }
            }
            catch
            {

            }
            return AutoUserTypeVsWorkflowList;
        }

        #region InsertORUpdateCreateUserInfo(User userObj)
        /// <summary>
        /// This method provide insert update create user information.
        /// </summary>
        /// <param name="loanInformationObj">Gets user object.</param>
        /// <returns>Returns a int value.</returns>
        //public int InsertORUpdateCreateUserInfo(User userObj, List<GroupMember> groupMemeberList)
        //{
        //    int insertORUpdateValue = -1;
        //    if (!IsExistsUserInfo(Convert.ToInt32(userObj.Id)))
        //    {
        //        insertORUpdateValue = InsertCreateUserInformation(userObj, groupMemeberList);
        //        ////code by arif
        //        //insertORUpdateValue = InsertCreateAutoUserInformation(userObj);
        //    }
        //    else
        //    {
        //        for (int i = 0; i < groupMemeberList.Count; i++)
        //        {
        //            groupMemeberList.ElementAt(i).UserID = userObj.Id;
        //        }
        //        insertORUpdateValue = UpdateCreateUserInformation(userObj, groupMemeberList);
        //    }
        //    return insertORUpdateValue;
        //}

        public void Audit(int action_id, int module_id, string str_sql1, User user)
        {
            string query = "insert into audit_trail(ID_LEARNER,ACTION_ID,MODULE_ID,QRY_DATE,QRY_TIME,QRY_STRING) " +
                          "values (" + user.UsersId + "," + action_id + "," + module_id + ", CAST(getdate() as DATE) ,CAST (getdate() as Time),'" + AddSlash(str_sql1) + "')";
            try
            {
                DbQueryManager.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("audit_trail :: " + query);
                LogFile.WriteLog(ex);
                throw new Exception("audit_trail Table insertion failure");
            }
        }

        public string GetAuditSql(int action_id, int module_id, string str_sql1, User user)
        {
            //return "insert into audit_trail(ID_LEARNER,ACTION_ID,MODULE_ID,QRY_DATE,QRY_TIME,QRY_STRING) " + "values (" + user.UsersId + "," + action_id + "," + module_id + ", CAST(getdate() as DATE) ,CAST (getdate() as Time),'" + AddSlash(str_sql1) + "');";
            return "";
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        public int InsertORUpdateCreateUserInfo(User userObj)
        {
            int insertORUpdateValue = -1;
            if (!IsExistsUserInfo(Convert.ToInt32(userObj.Id)))
            {
                insertORUpdateValue = InsertCreateUserInformation(userObj);
                ////code by arif
                //insertORUpdateValue = InsertCreateAutoUserInformation(userObj);
            }
            else
            {

                insertORUpdateValue = UpdateCreateUserInformation(userObj);
            }
            return insertORUpdateValue;
        }

        private int InsertCreateUserInformation(User userObj)
        {
            int action_id = 1; // action_id=1 is ADD action.
            int module_id = 1;  // module_id=1 is PERSON module.
            int insertRow = -1;
            int insert = -2;
            string query = "";
            string queryString = String.Format("INSERT INTO t_pluss_user (USER_CODE, USER_NAME, USER_PASSWORD, USER_LEVEL, USER_STATUS, USER_ENTRYDATE, USER_USER_ID, USER_LEVELCODE, USER_EMAILADDRESS, USER_DESIGNATION, AUTO_USER_LEVEL,IS_APPROVED,IS_DELETED,IsAccessBoth,Module,USER_LOGINSTATUS) " +
                                 "VALUES ('{0}', '{1}',CONVERT(VARCHAR(32), HashBytes('MD5', '{2}'), 2), {3}, {4}, getdate(),{5},'{6}','{7}','{8}',{9},{10},{11},{12},{13},{14})",
                                 EscapeCharacter(userObj.UserId), EscapeCharacter(userObj.Name), EscapeCharacter(userObj.Password),
                                 userObj.UserType.Id, userObj.Status, userObj.CreateUserId, EscapeCharacter(userObj.UserLevelCode), EscapeCharacter(userObj.Email), EscapeCharacter(userObj.Designation), userObj.AutoUserLevel_Id, 0, 0, userObj.IsAccessBoth, userObj.Module, 0);
            if (userObj.Module == 2 || userObj.IsAccessBoth == 1)
            {

                query =
                    String.Format(
                        "insert into learner_details_table(NAME_LEARNER ,USERNAME,EMAIL_LEARNER,LOAN_DEPT_ID,ST_USER_LEVEL,PASSWORD  ) " +
                        "values('{0}','{1}','{2}','{3}','{4}',CONVERT(VARCHAR(32), HashBytes('MD5', '{5}'), 2))", EscapeCharacter(userObj.Name),
                        EscapeCharacter(userObj.UserId), EscapeCharacter(userObj.Email), EscapeCharacter(userObj.LOAN_DEPT_ID.ToString()), EscapeCharacter(userObj.ST_USER_LEVEL.ToString()), EscapeCharacter("12345"));
                Audit(action_id, module_id, query, userObj);

            }
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(queryString);

                insert = DbQueryManager.ExecuteNonQuery(query);


                if (insertRow > 0 || insert > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }



        private int UpdateCreateUserInformation(User userObj)
        {
            int updateRow = -1;
            string query = "";
            int row = -2;
            int action_id = 2; // action_id=1 is ADD action.
            int module_id = 1;  // module_id=1 is PERSON module.
            string queryString = String.Format("UPDATE t_pluss_user SET USER_CODE = '{0}' , USER_NAME = '{1}' , USER_LEVEL = {2} ,USER_STATUS = {3} ," +
                                               " USER_CHANGDATE = getdate() , USER_USER_ID = {4},USER_LEVELCODE='{5}', USER_EMAILADDRESS = '{6}', USER_DESIGNATION = '{7}' ," +
                                               " AUTO_USER_LEVEL = {9} ,IS_APPROVED={10},IS_DELETED={11},IsAccessBoth={12}, Module={13}, USER_LOGINSTATUS=0 WHERE USER_ID = {8}",
                                                EscapeCharacter(userObj.UserId), EscapeCharacter(userObj.Name), userObj.UserType.Id,
                                                userObj.Status, userObj.CreateUserId, EscapeCharacter(userObj.UserLevelCode), EscapeCharacter(userObj.Email), EscapeCharacter(userObj.Designation), userObj.Id, userObj.AutoUserLevel_Id, userObj.IsApproved, 0, userObj.IsAccessBoth, userObj.Module);

            if (userObj.Module == 2 || userObj.IsAccessBoth == 1)
            {

                query =
                    String.Format(
                        "update learner_details_table SET NAME_LEARNER='{0}',USERNAME='{1}',EMAIL_LEARNER='{2}'," +
                        "LOAN_DEPT_ID= '{3}',ST_USER_LEVEL='{4}' where username='{5}'", EscapeCharacter(userObj.Name),
                        EscapeCharacter(userObj.UserId), EscapeCharacter(userObj.Email), EscapeCharacter(userObj.LOAN_DEPT_ID.ToString()), EscapeCharacter(userObj.ST_USER_LEVEL.ToString()), userObj.UserId);
                Audit(action_id, module_id, query, userObj);

            }

            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                row = DbQueryManager.ExecuteNonQuery(query);


                if (updateRow > 0 || row > 0)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }

        private int DeleteGroupMember(int userID)
        {
            string queryStringForAuto = String.Format("delete from pluss_groupmember where UserID = {0}", userID);
            return DbQueryManager.ExecuteNonQuery(queryStringForAuto);
        }
        #endregion

        #region IsExistsUserInfo(Int32 userId)
        /// <summary>
        /// This method provide user information exists or not.
        /// </summary>
        /// <param name="userId">Get user Id</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsUserInfo(Int32 userId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_user WHERE USER_ID = {0}", userId);
            object userObj;
            try
            {
                userObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(userObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion



        //code by arif start

        #region getUserID(Int32 userId)
        /// <summary>
        /// This method provide user information exists or not.
        /// </summary>
        /// <param name="userId">Get user Id</param>
        /// <returns>Returns true if exists else false.</returns>
        public int getUserID(Int32 userId)
        {
            int returnValue = -1;
            string queryString = String.Format("SELECT USER_ID FROM t_pluss_user WHERE USER_CODE = {0}", userId);
            //object userObj;
            try
            {
                returnValue = Convert.ToInt32(DbQueryManager.ExecuteScalar(queryString));
                if (returnValue > 0)
                {
                    return returnValue;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        //code by arif end





        #region IsExistsUserId(string userId)
        /// <summary>
        /// This method provide user id exists or not.
        /// </summary>
        /// <param name="userId">Get user id</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsUserId(string userId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_user WHERE USER_CODE = '{0}'", userId);
            object userObj;
            try
            {
                userObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(userObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region ResetUserPassword(Int32 userId, string password)
        /// <summary>
        /// This method update user password.
        /// </summary>
        /// <param name="userId">Gets user id.</param>
        /// <param name="password">Gets default password.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int ResetUserPassword(Int32 userId, string password)
        {
            int updateRow = -1;
            string queryString = String.Format("UPDATE t_pluss_user SET USER_STATUS=1," +
                                                "USER_PASSWORD = MD5('{0}') " +
                                                "WHERE USER_ID = {1} ",
                                                EscapeCharacter(password), userId);
            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (updateRow > 0)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
        #endregion

        #region ChangePassword(Int32 userId, string password)
        /// <summary>
        /// This method update user password.
        /// </summary>
        /// <param name="userId">Gets user id.</param>
        /// <param name="password">Gets default password.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int ChangePassword(string loginId, string password)
        {
            int updateRow = -1;
            string queryString = String.Format("UPDATE t_pluss_user SET " +
                                                "USER_PASSWORD = CONVERT(VARCHAR(32),HashBytes('MD5','{0}'), 2) " +
                                                "WHERE USER_CODE = '{1}' ",
                                                EscapeCharacter(password), EscapeCharacter(loginId));
            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (updateRow > 0)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
        #endregion

        #region IsOldPasswordMatch
        /// <summary>
        /// This method provide old password is match or not.
        /// </summary>
        /// <param name="loginId">Get login id</param>
        /// <param name="oldPassword">Gets old password.</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsOldPasswordMatch(string loginId, string oldPassword)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(USER_PASSWORD) FROM t_pluss_user WHERE USER_CODE = '{0}' AND USER_PASSWORD = CONVERT(VARCHAR(32), HashBytes('MD5', '{1}'), 2) ", loginId, oldPassword);
            object userObj;
            try
            {
                userObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToUInt32(userObj) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region EscapeCharacter
        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                returnMessage = fieldString;
            }
            return returnMessage;
        }
        #endregion

        /// <summary>
        /// This method provide all user information.
        /// </summary>
        /// <returns>Returns a user list object.</returns>
        public List<User> GetAllUserInfo()
        {
            User userObj;
            List<User> userListObj = new List<User>();

            string queryString = "select USER_ID, USER_CODE, USER_NAME, USER_LEVELCODE , " +
                        "USER_PASSWORD, USER_CHANGDATE, USER_CHANGBY, " +
                        "USER_LEVEL, USER_STATUS, USER_ENTRYDATE, USER_USER_ID	" +
                        "from t_pluss_user " +
                        "where USER_LEVEL=4 or USER_LEVEL=2 or USER_LEVEL=7 " +
                        "order by USER_LEVEL,USER_NAME";

            DataTable userDataTableObj = new DataTable();
            try
            {
                userDataTableObj = DbQueryManager.GetTable(queryString);
                if (userDataTableObj != null)
                {
                    DataTableReader userDataTableReaderObj = userDataTableObj.CreateDataReader();
                    if (userDataTableReaderObj != null)
                    {
                        while (userDataTableReaderObj.Read())
                        {
                            userObj = new User();
                            userObj.Id = Convert.ToInt32(userDataTableReaderObj["USER_ID"].ToString());
                            //userObj.UserId = Convert.ToString(userDataTableReaderObj["USER_CODE"].ToString());
                            userObj.Name = Convert.ToString(userDataTableReaderObj["USER_NAME"].ToString());
                            //userObj.Password = Convert.ToString(userDataTableReaderObj["USER_PASSWORD"].ToString());
                            //userObj.Type = Convert.ToInt16(userDataTableReaderObj["USER_LEVEL"].ToString());
                            //userObj.UserLevelCode = Convert.ToString(userDataTableReaderObj["USER_LEVELCODE"].ToString());
                            //userObj.UserType.Name = Convert.ToString(userDataTableReaderObj["USLE_NAME"]);
                            //userObj.Status = Convert.ToInt16(userDataTableReaderObj["USER_STATUS"].ToString());
                            //userObj.StatusMsg = Convert.ToString(userDataTableReaderObj["USER_STATUS_MSG"].ToString());
                            userListObj.Add(userObj);
                        }
                        userDataTableReaderObj.Close();
                    }
                    userDataTableObj.Clear();
                }
            }
            catch
            {
            }
            return userListObj;
        }

        //code at end of the day 19-12-2011
        public List<GroupMember> GetGroupID(int UserId)
        {
            int i = 0;
            //GroupMember groupMemberObj;
            List<GroupMember> groupMemberListObj = new List<GroupMember>();
            string queryString = "select GroupID from pluss_groupmember where UserId = " + UserId;

            DataTable groupMemberDataTableObj = new DataTable();
            try
            {
                groupMemberDataTableObj = DbQueryManager.GetTable(queryString);
                if (groupMemberDataTableObj != null)
                {
                    DataTableReader groupMemberDataTableReaderObj = groupMemberDataTableObj.CreateDataReader();
                    while (groupMemberDataTableReaderObj.Read())
                    {
                        var groupMember = new GroupMember();
                        groupMember.GroupID = Convert.ToInt32(groupMemberDataTableReaderObj["GroupId"].ToString());
                        groupMember.UserID = UserId;

                        groupMemberListObj.Add(groupMember);
                    }
                }
            }
            catch
            {
            }


            return groupMemberListObj;
        }
        // code at end of the day 19-12-2011

        public List<Department> GetAllDeptInfo()
        {
            var deptListObj = new List<Department>();
            string query = "select * from loan_dept_info";
            DataTable depTable = new DataTable();
            depTable = DbQueryManager.GetTable(query);
            if (depTable != null)
            {
                DataTableReader depTableReader = depTable.CreateDataReader();
                if (depTableReader != null)
                {
                    while (depTableReader.Read())
                    {
                        Department dept = new Department();
                        dept.LOAN_DEPT_ID = (int)depTableReader["LOAN_DEPT_ID"];
                        dept.LOAN_DEPT_NAME = (string)depTableReader["LOAN_DEPT_NAME"];
                        dept.LOAN_DEPT_TYPE = (int)depTableReader["LOAN_DEPT_TYPE"];
                        deptListObj.Add(dept);
                    }
                    depTableReader.Close();
                }
                depTable.Clear();
            }
            return deptListObj;
        }


        public List<User> GetAllUsers()
        {
            var userListObj = new List<User>();
            string queryString = "";
            try
            {

                queryString = string.Format(@"SELECT u.USER_ID, u.USER_CODE, u.USER_NAME, u.USER_DESIGNATION, u.USER_PASSWORD, u.USER_LEVELCODE, u.USER_LEVEL,
(case when u.USER_LEVEL=19 then d.LOAN_DEPT_NAME  else UT.USLE_NAME end) as USLE_NAME, u.USER_STATUS, u.USER_EMAILADDRESS, 
                                (CASE WHEN u.USER_STATUS=0 THEN 
	                                (Case u.Is_Deleted When 1 Then 
						                                (Case u.Is_Approved When 1 Then  'Deleted' When 0 then 'InActive' End) 
					                                When 0 then 'InActive' end)  
					                                when u.USER_STATUS=1 then 'Active'  END)   USER_STATUS_MSG,
                                ISNULL(U.AUTO_USER_LEVEL,0) as AUTO_USER_LEVEL,AUT.USLE_NAME AS AUTO_USLE_NAME,t_pluss_accesslog.ACLO_LOGINDATETIME,u.USER_ENTRYDATE as MakeDate,maker.USER_CODE as MakeBy,
                                u.APPROVED_BY as CheckBy,u.APPROVED_DATE as CheckDate,d.LOAN_DEPT_NAME 
                                FROM t_pluss_user AS U 
                                left join t_pluss_user maker on u.USER_USER_ID=maker.USER_ID
                                LEFT JOIN t_pluss_usertype AS UT ON U.USER_LEVEL = UT.USLE_ID 
                                LEFT JOIN t_pluss_usertype AS AUT ON U.AUTO_USER_LEVEL = AUT.USLE_ID 
                                LEFT JOIN (select ACLO_USERLOGIID,MAX(ACLO_LOGINDATETIME) as ACLO_LOGINDATETIME from t_pluss_accesslog l where ACLO_LOGINSUCCESSSTATUS=1 group by ACLO_USERLOGIID) as t_pluss_accesslog ON t_pluss_accesslog.Aclo_userlogiid=U.USER_CODE 
                                left join (select l.USERNAME,d.LOAN_DEPT_NAME from learner_details_table l
                                left join loan_dept_info d on l.LOAN_DEPT_ID=d.LOAN_DEPT_ID) d on u.USER_CODE=d.USERNAME
                                where u.USER_CODE is not null and u.USER_CODE <>'' and u.Is_Deleted <>1
                                ORDER BY USER_NAME ASC");


                DataTable userDataTableObj = new DataTable();
                userDataTableObj = DbQueryManager.GetTable(queryString);
                if (userDataTableObj != null)
                {
                    DataTableReader userDataTableReaderObj = userDataTableObj.CreateDataReader();
                    if (userDataTableReaderObj != null)
                    {
                        while (userDataTableReaderObj.Read())
                        {
                            User userObj = new User();

                            if (userDataTableReaderObj["USER_ID"] != DBNull.Value)
                            {
                                userObj.Id = Convert.ToInt32(userDataTableReaderObj["USER_ID"].ToString());
                            }
                            if (userDataTableReaderObj["USER_CODE"] != DBNull.Value)
                            {
                                userObj.UserId = Convert.ToString(userDataTableReaderObj["USER_CODE"].ToString());
                            }

                            if (userDataTableReaderObj["USER_NAME"] != DBNull.Value)
                            {
                                userObj.Name = Convert.ToString(userDataTableReaderObj["USER_NAME"].ToString());
                            }

                            if (userDataTableReaderObj["USER_DESIGNATION"] != DBNull.Value)
                            {
                                userObj.Designation = Convert.ToString(userDataTableReaderObj["USER_DESIGNATION"].ToString());
                            }

                            if (userDataTableReaderObj["USER_PASSWORD"] != DBNull.Value)
                            {
                                userObj.Password = Convert.ToString(userDataTableReaderObj["USER_PASSWORD"].ToString());
                            }

                            if (userDataTableReaderObj["USER_LEVEL"] != DBNull.Value)
                            {
                                userObj.Type = Convert.ToInt16(userDataTableReaderObj["USER_LEVEL"].ToString());
                            }

                            if (userDataTableReaderObj["USER_LEVELCODE"] != DBNull.Value)
                            {
                                userObj.UserLevelCode = Convert.ToString(userDataTableReaderObj["USER_LEVELCODE"].ToString());
                            }

                            if (userDataTableReaderObj["USLE_NAME"] != DBNull.Value)
                            {
                                userObj.UserType.Name = Convert.ToString(userDataTableReaderObj["USLE_NAME"]);
                            }

                            if (userDataTableReaderObj["USLE_NAME"] != DBNull.Value)
                            {
                                userObj.UserTypeName = Convert.ToString(userDataTableReaderObj["USLE_NAME"]);
                            }

                            if (userDataTableReaderObj["USER_STATUS"] != DBNull.Value)
                            {
                                userObj.Status = Convert.ToInt16(userDataTableReaderObj["USER_STATUS"].ToString());
                            }

                            if (userDataTableReaderObj["USER_EMAILADDRESS"] != DBNull.Value)
                            {
                                userObj.Email = Convert.ToString(userDataTableReaderObj["USER_EMAILADDRESS"]);
                            }

                            if (userDataTableReaderObj["USER_STATUS_MSG"] != DBNull.Value)
                            {
                                userObj.StatusMsg = Convert.ToString(userDataTableReaderObj["USER_STATUS_MSG"].ToString());
                            }

                            if (userDataTableReaderObj["AUTO_USER_LEVEL"] != DBNull.Value)
                            {
                                userObj.AutoUserLevel_Id = Convert.ToInt32(userDataTableReaderObj["AUTO_USER_LEVEL"].ToString());
                            }

                            if (userDataTableReaderObj["AUTO_USLE_NAME"] != DBNull.Value)
                            {
                                userObj.AutoUserLevel_Name = Convert.ToString(userDataTableReaderObj["AUTO_USLE_NAME"].ToString());
                            }
                            if (userDataTableReaderObj["ACLO_LOGINDATETIME"] != DBNull.Value)
                            {
                                userObj.LastLoginDateTime = Convert.ToDateTime(userDataTableReaderObj["ACLO_LOGINDATETIME"]);
                            }

                            if (userDataTableReaderObj["MakeBy"] != DBNull.Value)
                            {
                                userObj.MakerId = Convert.ToString(userDataTableReaderObj["MakeBy"].ToString());
                            }
                            if (userDataTableReaderObj["MakeDate"] != DBNull.Value)
                            {
                                userObj.MakeDate = Convert.ToDateTime(userDataTableReaderObj["MakeDate"].ToString());
                            }
                            if (userDataTableReaderObj["CheckDate"] != DBNull.Value)
                            {
                                userObj.CheckDate = Convert.ToDateTime(userDataTableReaderObj["CheckDate"].ToString());
                            }

                            if (userDataTableReaderObj["CheckBy"] != DBNull.Value)
                            {
                                userObj.CheckerId = Convert.ToString(userDataTableReaderObj["CheckBy"].ToString());
                            }

                            if (userDataTableReaderObj["LOAN_DEPT_NAME"] != DBNull.Value)
                            {
                                userObj.LOAN_DEPT_NAME = Convert.ToString(userDataTableReaderObj["LOAN_DEPT_NAME"].ToString());
                            }

                            userListObj.Add(userObj);
                        }
                        userDataTableReaderObj.Close();
                    }
                    userDataTableObj.Clear();
                }

            }
            catch (Exception e)
            {

            }
            return userListObj;
        }

        public List<Audit> GetAuditTrailData(string startDate, string endDate)
        {

            var dbMySql = new DatabaseConnection();
            var auditList = new List<Audit>();
            var sSQL = @"
                Select t_pluss_audittrails.*,Usle_Name from t_pluss_audittrails
                left outer join t_pluss_user on t_pluss_user.User_Code =t_pluss_audittrails.userId
                left outer join t_pluss_usertype on t_pluss_usertype.Usle_Id=t_pluss_user.User_level 
                Where DATE(ActionDateAndTime) Between '" + startDate + "' And '" +
                       endDate + "'";
            DataTable auditDataTableObj = new DataTable();
            auditDataTableObj = DbQueryManager.GetTable(sSQL);
            if (auditDataTableObj != null)
            {
                DataTableReader auditDataTableReaderObj = auditDataTableObj.CreateDataReader();
                if (auditDataTableReaderObj != null)
                {
                    while (auditDataTableReaderObj.Read())
                    {
                        Audit audit = new Audit();

                        audit.UserId = Convert.ToString(auditDataTableReaderObj["UserId"].ToString());
                        audit.ActionDateAndTime = Convert.ToDateTime(auditDataTableReaderObj["ActionDateAndTime"].ToString());
                        audit.RequestedUrl = Convert.ToString(auditDataTableReaderObj["RequestedUrl"].ToString());
                        audit.IpAddress = Convert.ToString(auditDataTableReaderObj["IpAddress"].ToString());
                        audit.HostName = Convert.ToString(auditDataTableReaderObj["HostName"].ToString());
                        audit.Activity = Convert.ToString(auditDataTableReaderObj["Activity"].ToString());
                        audit.Description = Convert.ToString(auditDataTableReaderObj["Description"]);
                        audit.UserRole = Convert.ToString(auditDataTableReaderObj["Usle_Name"]);

                        auditList.Add(audit);
                    }
                    auditDataTableReaderObj.Close();
                }
                auditDataTableObj.Clear();
            }
            return auditList;
        }

        public DataTable GetAuditTrailDataTable(string startDate, string endDate)
        {
            DataTable auditDataTableObj;
            try
            {
                var sSQL = @"
                            SELECT 
	                            a.UserId, 
	                            a.ActionDateAndTime, 
	                            a.RequestedUrl, 
	                            a.IpAddress, 
	                            a.HostName, 
	                            a.Activity, 
	                            a.Description, 
	                            a.AuditTrailId, 
	                            a.OldValues, 
	                            a.NewValues,
	                            Roles.Name AS UserRole
                            FROM t_pluss_audittrails a
	                            INNER JOIN t_pluss_user b ON b.User_Code = a.userId
	                            INNER JOIN Roles ON Roles.Id = b.USER_LEVEL
                            Where a.UserType='SA' and a.ActionDateAndTime  Between '" + startDate + "' and '" + endDate + " 23:59:59'  order by ActionDateAndTime asc";

                LogFile.WriteLine(sSQL);
                auditDataTableObj = new DataTable();
                auditDataTableObj = DbQueryManager.GetTable(sSQL);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return auditDataTableObj;
        }

        public void UserApproval(string userCode, string loginUser)
        {
            try
            {
                var sSQL = String.Format(@"Update t_pluss_user Set IS_APPROVED=1,USER_LOGINSTATUS=0,APPROVED_BY='{0}',APPROVED_DATE= getdate() WHERE USER_CODE='{1}'", loginUser, userCode);
                DbQueryManager.ExecuteNonQuery(sSQL);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void UserDelete(string userCode, string loginUser)
        {
            try
            {
                var sSQL = String.Format(@"Update t_pluss_user Set IS_DELETED=1,USER_STATUS=0, IS_APPROVED=0,DELETED_BY='{0}' WHERE USER_CODE='{1}'", loginUser, userCode);
                DbQueryManager.ExecuteNonQuery(sSQL);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void UserDeleteFromTemp(string userCode)
        {
            try
            {
                var sSQL = String.Format(@"Delete from t_pluss_user_temp WHERE Id ='{0}'", userCode);
                DbQueryManager.ExecuteNonQuery(sSQL);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public LoanLocatorUser GetLoanLocatorUser(string userName)
        {
            var sql = string.Format(@"select * from old_learner_details_table where USERNAME='{0}'", userName); // and LOAN_DEPT_ID>0 and USER_CURR_STAT=0
            return Data<LoanLocatorUser>.DataSource(sql).FirstOrDefault();
        }

        public DataTable GetLoginAccess(DateTime fromDateTime, DateTime toDateTime)
        {
            try
            {
                var sql = string.Format(@"select ACLO_USERLOGIID as UserId ,(case when ACLO_LOGINSTATUS=1 then 'True' else 'False' end) as Status 
                        ,ACLO_IP as IpAddress,ACLO_LOGINDATETIME as LoginDate, '{0}' as [from], '{1}' as [to]
                        from t_pluss_accesslog where ACLO_LOGINDATETIME between '{0}' and '{1}  23:59:59'", fromDateTime.ToString("yyyy-MM-dd"), toDateTime.ToString("yyyy-MM-dd"));
                return DbQueryManager.GetTable(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /*
         * NA
         */

        public List<UserTModel> GetAllPendingUsers()
        {
            try
            {

                string sql = string.Format(@"
                    SELECT r.Name AS USER_LEVEL_NAME, temp.*, us.Status as USER_STATUS_NAME
                    FROM t_pluss_user_temp temp
	                    INNER JOIN Roles r ON temp.USER_LEVEL=r.Id
	                    INNER JOIN UserStatus us on us.Id=temp.USER_STATUS
                    WHERE IS_REJECTED != 1 ");

                LogFile.WriteLine("DAL - Pending Users - " + sql.ToString());
                var users = Data<UserTModel>.DataSource(sql);

                return users;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
            return null;
        }

        public string CreateUser(UserTModel user, User userSession)
        {
            try
            {
                string sql = string.Empty;

                if (user.Id > 0)
                {
                    string temp = string.Format(@"DELETE FROM t_pluss_user_temp WHERE Id='{0}'", user.Id);
                    int checkline = DbQueryManager.ExecuteNonQuery(temp);
                }

                sql = string.Format(@"INSERT INTO [dbo].[t_pluss_user_temp]
                            ([USER_CODE]
                            ,[USER_NAME]
                            ,[USER_DESIGNATION]
                            ,[USER_EMAILADDRESS]
                            ,[USER_LEVELCODE]
                            ,[USER_PASSWORD]
                            ,[USER_LEVEL]
                            ,[USER_STATUS]
                            ,[USER_ENTRYDATE]
                            ,[USER_ID]
                            ,[USER_LOGINSTATUS]
                            ,[IS_APPROVED]
                            ,[USER_USER_ID]
                            ,[IS_DELETED]
                            ,[DELETED_BY])
                    VALUES
                        ('{0}','{1}','{2}','{3}','{4}','{5}','{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}',{13},'{14}')"
                                , user.USER_CODE, user.USER_NAME, user.USER_DESIGNATION, user.USER_EMAILADDRESS, user.USER_LEVELCODE, user.USER_PASSWORD, user.USER_LEVEL, user.USER_STATUS, DateTime.Now, user.USER_ID, user.USER_LOGINSTATUS, 0, userSession.Id, user.IS_DELETED,user.DELETED_BY);
                int count = DbQueryManager.ExecuteNonQuery(sql);
                if (count > 0)
                    return Operation.Success.ToString();
                return Operation.Failed.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                return ex.Message;
            }

        }

        public string UpdateExistingUser(UserTModel user)
        {
            try
            {
                string sql = string.Empty;
                if (user.Id > 0)
                {
                    string temp = string.Format(@"DELETE FROM t_pluss_user_temp WHERE Id='{0}'", user.Id);
                    int tempCount = DbQueryManager.ExecuteNonQuery(temp);
                }

                sql = string.Format(@"INSERT INTO [dbo].[t_pluss_user_temp]
                   ([USER_CODE]
                   ,[USER_NAME]
                   ,[USER_DESIGNATION]
                   ,[USER_EMAILADDRESS]
                   ,[USER_LEVELCODE]
                   ,[USER_LEVEL]
                   ,[USER_STATUS]
                   ,[USER_ENTRYDATE]
                   ,[USER_ID]
                   ,[IS_APPROVED]
                    ,[USER_CHANGDATE]
                   ,[USER_CHANGBY])
             VALUES
                   ('{0}','{1}','{2}','{3}','{4}','{5}','{6}', '{7}', '{8}', '{9}')"
                           , user.USER_CODE, user.USER_NAME, user.USER_DESIGNATION, user.USER_EMAILADDRESS, user.USER_LEVELCODE, user.USER_LEVEL, user.USER_STATUS, DateTime.Now,
                   user.USER_ID, 0);

                int count = DbQueryManager.ExecuteNonQuery(sql);
                if (count > 0)
                    return Operation.Success.ToString();
                else
                    return Operation.Failed.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                return ex.Message;
            }

        }


        public string ApproveUser(UserTModel user, User loggedInUser)
        {
            try
            {
                if (user.USER_ID == 0)
                {
                    string sql = string.Format(@"INSERT INTO [dbo].[t_pluss_user]
                       ([USER_CODE]
                       ,[USER_NAME]
                       ,[USER_DESIGNATION]
                       ,[USER_EMAILADDRESS]
                       ,[USER_LEVELCODE]
                       ,[USER_LEVEL]
                       ,[USER_STATUS]
                       ,[USER_ENTRYDATE]
                       ,[USER_LOGINSTATUS]
                       ,[IS_APPROVED]
                       ,[APPROVED_BY]
                       ,[APPROVED_DATE]
                       ,[APPROVER_COMMENT]
                       ,[USER_PASSWORD]
                       ,[USER_USER_ID]
            )
                 VALUES
                       ('{0}','{1}','{2}','{3}','{4}','{5}','{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}');"
                       , user.USER_CODE, user.USER_NAME, user.USER_DESIGNATION, user.USER_EMAILADDRESS, user.USER_LEVELCODE, user.USER_LEVEL, user.USER_STATUS, user.USER_ENTRYDATE,
                       0, 1, loggedInUser.UserId, DateTime.Now, user.APPROVER_COMMENT, user.USER_PASSWORD, user.USER_USER_ID);

                    string query1 = "";

                    //InsertionAudit
                    string mainTableAudit = GetAuditSql((int)ModuleInfo.action_id, (int)ModuleInfo.module_id, sql.Replace(@"'", @"^"), loggedInUser);
                    //LeanerAudit
                    string learnerTableAudit = GetAuditSql((int)ModuleInfo.action_id, (int)ModuleInfo.module_id, query1.Replace(@"'", @"^"), loggedInUser);
                    
                    string deleteSqlIn = string.Format(@"DELETE FROM t_pluss_user_temp WHERE Id='{0}';", user.Id);

                    string fullSql = string.Format(@"
                                        BEGIN TRANSACTION [ApproveUser]
                                        BEGIN TRY
                                        {0}
                                        {1}
                                        {2}
                                        {3}
                                        {4}
                                        COMMIT TRANSACTION [ApproveUser];
                                        END TRY
                                        BEGIN CATCH
	                                        ROLLBACK TRANSACTION [ApproveUser];
	                                        THROW;
                                        END CATCH", sql, query1, mainTableAudit, learnerTableAudit, deleteSqlIn);
                    LogFile.WriteLine(fullSql);
                    DbQueryManager.ExecuteNonQuery(fullSql);
                }
                else
                {
                    string sql = string.Format(@"UPDATE main
                                SET main.USER_NAME = TEMP.USER_NAME
	                                ,main.USER_DESIGNATION = TEMP.USER_DESIGNATION
	                                ,main.USER_EMAILADDRESS = TEMP.USER_EMAILADDRESS
	                                ,main.USER_LEVEL = TEMP.USER_LEVEL
	                                ,main.USER_LEVELCODE = TEMP.USER_LEVELCODE
	                                ,main.USER_STATUS = TEMP.USER_STATUS
	                                ,main.USER_ENTRYDATE = TEMP.USER_ENTRYDATE
	                                ,main.USER_CHANGDATE = TEMP.USER_CHANGDATE
	                                ,main.USER_CHANGBY = TEMP.USER_USER_ID
	                                ,main.USER_LOGINSTATUS = TEMP.USER_LOGINSTATUS
	                                ,main.APPROVER_COMMENT = '{2}'
	                                ,main.IS_APPROVED = 1
	                                ,main.IS_DELETED=temp.IS_DELETED
	                                ,main.DELETED_BY=temp.DELETED_BY
	                                ,main.APPROVED_BY='{0}'
	                                ,main.APPROVED_DATE=GETDATE()
                                FROM t_pluss_user main
                                LEFT JOIN t_pluss_user_temp TEMP ON main.USER_CODE = TEMP.USER_CODE
                                WHERE temp.Id='{1}'", loggedInUser.UserId, user.Id, user.APPROVER_COMMENT);

                    string deleteSqlIn = string.Format(@"DELETE FROM t_pluss_user_temp WHERE Id='{0}';", user.Id);
                    string mainTableAudit = GetAuditSql((int)ModuleInfo.action_id, (int)ModuleInfo.module_id, sql, loggedInUser);

                    string fullSql = string.Format(@"
                        BEGIN TRANSACTION [ApproveUser]
                        BEGIN TRY
                        {0}
                        {1}
                        {2}
                        COMMIT TRANSACTION [ApproveUser];
                        END TRY
                        BEGIN CATCH
	                        ROLLBACK TRANSACTION [ApproveUser];
	                        THROW;
                        END CATCH", sql, deleteSqlIn, mainTableAudit);
                    
                    DbQueryManager.ExecuteNonQuery(fullSql);
                }
                return Operation.Success.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                return ex.Message;
            }
        }


        public List<UserTModel> GetAllInActiveUsers()
        {
            try
            {
                string sql = string.Format(@"
                SELECT 
                    u.*, 
                    Roles.Name ROLE_NAME,
	                CASE WHEN (u.USER_STATUS = 0) THEN 'INACTIVE'
		                WHEN (u.USER_STATUS = 1) THEN 'ACTIVE'
		                WHEN (u.USER_STATUS = 2) THEN 'DELETED'
		                WHEN (u.USER_STATUS = 3) THEN 'DORMANT' 
	                END AS USER_STATUS_NAME
                FROM t_pluss_user u
                    INNER JOIN Roles ON USER_LEVEL=Id 
                WHERE USER_STATUS='0' AND USER_CODE<>'1499074' AND
                    (IS_DELETED IS NULL OR IS_DELETED=0)", (int)UserStatus.Inactive);

                var users = Data<UserTModel>.DataSource(sql);
                return users;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
            return null;
        }

        public List<UserTModel> GetAllInActiveUsers(string sPSID)
        {
            try
            {
                string sql = string.Format(@"
                SELECT 
                    t_pluss_user.*, 
                    Roles.Name ROLE_NAME 
                FROM t_pluss_user 
                    INNER JOIN Roles ON USER_LEVEL = Id 
                WHERE   USER_STATUS='0' AND 
                        USER_CODE='{1}' AND 
                        (IS_DELETED IS NULL OR IS_DELETED=0)", (int)UserStatus.Inactive, sPSID);

                var users = Data<UserTModel>.DataSource(sql);
                return users;
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
            return null;
        }

        public string ActivateUser(string id)
        {
            try
            {

                string sql = string.Format(@"UPDATE t_pluss_user SET USER_STATUS='{0}' WHERE USER_ID='{1}'", (int)UserStatus.Active, id);

                int rowAffected = DbQueryManager.ExecuteNonQuery(sql);

                if (rowAffected > 0)
                {
                    return Operation.Success.ToString();
                }
                return Operation.Failed.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }

            return Operation.Error.ToString();
        }


        public DataTable GetUserListReportData(User user)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@UserId";
                parameter.Value = user.UserId.ToString();
                parameters.Add(parameter);

                DataTable table = DbQueryManager.GetDataTableWithSpAndParam("sp_getUserList", parameters);

                return table;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
