﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// RemarksGateway Class.
    /// </summary>
    public class RemarksGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor.
        /// </summary>
        public RemarksGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select remarks list.
        /// </summary>
        /// <returns>Returns a Remarks list object.</returns>
        public List<Remarks> SelectRemarksList()
        {
            List<Remarks> remarksList = new List<Remarks>();
            Remarks remarksObj = null;
            //ADODB.Recordset xRs = new ADODB.Recordset();
            string mSQL = "SELECT REMA_ID,REMA_PROD_ID,REMA_DTYPE,REMA_NAME,REMA_NO,REMA_FOR,REFO_NAME,REMA_STATUS FROM t_pluss_remarks LEFT JOIN t_pluss_remarksfor ON REMA_FOR=REFO_ID";
            //xRs = dbMySQL.DbOpenRset(mSQL);
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    remarksObj = new Remarks();
                    remarksObj.Id = Convert.ToInt32(xRs["REMA_ID"]);
                    remarksObj.ProductId = Convert.ToInt32("0" + xRs["REMA_PROD_ID"]);
                    remarksObj.Dtype = Convert.ToInt32("0" + xRs["REMA_DTYPE"]);
                    remarksObj.Name = Convert.ToString(xRs["REMA_NAME"]);
                    remarksObj.RemarksNo = Convert.ToInt32("0" + xRs["REMA_NO"]);
                    remarksObj.RemarksFor = Convert.ToInt32("0" + xRs["REMA_FOR"]);
                    remarksObj.RemarksForName.Id = Convert.ToInt32("0" + xRs["REMA_FOR"]);
                    remarksObj.RemarksForName.Name = Convert.ToString(xRs["REFO_NAME"]);
                    remarksObj.Status = Convert.ToInt32(xRs["REMA_STATUS"]);
                    remarksList.Add(remarksObj);
                }
                xRs.Close();
            }
            return remarksList;
        }

        /// <summary>
        /// This method select remarks list against search string.
        /// </summary>
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns a Remarks list object.</returns>
        public List<Remarks> SelectRemarksList(string searchKey)
        {
            List<Remarks> remarksList = new List<Remarks>();
            Remarks remarksObj = null;
            string mSQL = "SELECT REMA_ID,REMA_PROD_ID,REMA_DTYPE,REMA_NAME,REMA_NO,REMA_FOR,REFO_NAME,REMA_STATUS FROM t_pluss_remarks LEFT JOIN t_pluss_remarksfor ON REMA_FOR=REFO_ID WHERE" +
                          " REMA_NAME LIKE '%" + searchKey + "%' OR REMA_FOR LIKE '%" + searchKey + "%' OR REMA_STATUS LIKE '%" + searchKey + "%'";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    remarksObj = new Remarks();
                    remarksObj.Id = Convert.ToInt32(xRs["REMA_ID"]);
                    remarksObj.ProductId = Convert.ToInt32("0" + xRs["REMA_PROD_ID"]);
                    remarksObj.Dtype = Convert.ToInt32("0" + xRs["REMA_DTYPE"]);
                    remarksObj.Name = Convert.ToString(xRs["REMA_NAME"]);
                    remarksObj.RemarksNo = Convert.ToInt32("0" + xRs["REMA_NO"]);
                    remarksObj.RemarksFor = Convert.ToInt32("0" + xRs["REMA_FOR"]);
                    remarksObj.RemarksForName.Id = Convert.ToInt32("0" + xRs["REMA_FOR"]);
                    remarksObj.RemarksForName.Name = Convert.ToString(xRs["REFO_NAME"]);
                    remarksObj.Status = Convert.ToInt32(xRs["REMA_STATUS"]);
                    remarksList.Add(remarksObj);
                }
                xRs.Close();
            }
            return remarksList;
        }

        /// <summary>
        /// This method select remarks list.
        /// </summary>
        /// <param name="remarksFor">Gets a remarks for.</param>
        /// <returns>Returns a Remarks list object.</returns>
        public List<Remarks> SelectRemarksList(int remarksFor)
        {
            List<Remarks> remarksList = new List<Remarks>();
            Remarks remarksObj = null;
            string queryString =
                   "SELECT REMA_ID,REMA_PROD_ID,REMA_DTYPE,REMA_NAME,REMA_NO,REMA_FOR,REFO_NAME,REMA_STATUS FROM t_pluss_remarks LEFT JOIN t_pluss_remarksfor ON REMA_FOR=REFO_ID WHERE REMA_FOR=" +
                   remarksFor + " AND REMA_STATUS=" + 1;
             DataTable data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    remarksObj = new Remarks();
                    remarksObj.Id = Convert.ToInt32(xRs["REMA_ID"]);
                    remarksObj.ProductId = Convert.ToInt32("0" + xRs["REMA_PROD_ID"]);
                    remarksObj.Dtype = Convert.ToInt32("0" + xRs["REMA_DTYPE"]);
                    remarksObj.Name = Convert.ToString(xRs["REMA_NAME"]);
                    remarksObj.RemarksNo = Convert.ToInt32("0" + xRs["REMA_NO"]);
                    remarksObj.RemarksForName.Id = Convert.ToInt32("0" + xRs["REMA_FOR"]);
                    remarksObj.RemarksForName.Name = Convert.ToString(xRs["REFO_NAME"]);
                    remarksObj.RemarksFor = Convert.ToInt32("0" + xRs["REMA_FOR"]);
                    remarksObj.Status = Convert.ToInt32(xRs["REMA_STATUS"]);
                    remarksList.Add(remarksObj);
                    
                }
                xRs.Close();
            }
            return remarksList;
        }

        /// <summary>
        /// This method select remarks against id.
        /// </summary>
        /// <param name="remarksId">Gets a remarks id.</param>
        /// <returns>Returns a Remarks object.</returns>
        public Remarks SelectRemarks(Int32 remarksId)
        {
            Remarks remarksObj = new Remarks();
            string mSQL = "SELECT REMA_ID,REMA_PROD_ID,REMA_DTYPE,REMA_NAME,REMA_NO,REMA_FOR,REFO_NAME,REMA_STATUS FROM t_pluss_remarks LEFT JOIN t_pluss_remarksfor ON REMA_FOR=REFO_ID WHERE REMA_ID=" + remarksId;
             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    remarksObj.Id = Convert.ToInt32(xRs["REMA_ID"]);
                    remarksObj.ProductId = Convert.ToInt32("0" + xRs["REMA_PROD_ID"]);
                    remarksObj.Dtype = Convert.ToInt32("0" + xRs["REMA_DTYPE"]);
                    remarksObj.Name = Convert.ToString(xRs["REMA_NAME"]);
                    remarksObj.RemarksNo = Convert.ToInt32("0" + xRs["REMA_NO"]);
                    remarksObj.RemarksForName.Id = Convert.ToInt32("0" + xRs["REMA_FOR"]);
                    remarksObj.RemarksForName.Name = Convert.ToString(xRs["REFO_NAME"]);
                    remarksObj.RemarksFor = Convert.ToInt32("0" + xRs["REMA_FOR"]);
                    remarksObj.Status = Convert.ToInt32(xRs["REMA_STATUS"]);
                }
                xRs.Close();
            }
            return remarksObj;
        }
        /// <summary>
        /// This method provide the operation of insertion or update for remarks.
        /// </summary>
        /// <param name="remarksObj">Gets a Remars object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendRemarksInToDB(Remarks remarksObj)
        {
            Remarks existingRemarksObj = SelectRemarks(remarksObj.Id);
            if (existingRemarksObj.Id > 0)
            {
                return UpdateRemarks(remarksObj);
            }
            else
            {
                return InsertRemarks(remarksObj);
            }
        }
        /// <summary>
        /// This method provide the insert operation for Remarks.
        /// </summary>
        /// <param name="remarksObj">Gets a Remarks object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertRemarks(Remarks remarksObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_remarks (REMA_PROD_ID,REMA_DTYPE,REMA_NAME,REMA_NO,REMA_FOR,REMA_STATUS) VALUES(" +
                              remarksObj.ProductId + "," +
                              remarksObj.Dtype + ",'" +
                              AddSlash(remarksObj.Name) + "'," +
                              remarksObj.RemarksNo + "," +
                              remarksObj.RemarksFor + "," +
                              remarksObj.Status + ")";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide the update operation for Remarks.
        /// </summary>
        /// <param name="remarksObj">Gets a Remarks object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateRemarks(Remarks remarksObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_remarks SET " +
                              "REMA_PROD_ID=" + remarksObj.ProductId + "," +
                              "REMA_DTYPE=" + remarksObj.Dtype + "," +
                              "REMA_NAME='" + AddSlash(remarksObj.Name) + "'," +
                              "REMA_NO=" + remarksObj.RemarksNo + "," +
                              "REMA_FOR=" + remarksObj.RemarksFor + "," +
                              "REMA_STATUS=" + remarksObj.Status + " WHERE REMA_ID=" + remarksObj.Id;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method select the remarks max id.
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public Int32 SelectRemarksMaxId()
        {
            string mSQL = "SELECT MAX(REMA_ID) FROM t_pluss_remarks";
            Int32 returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue + 1;
        }
        /// <summary>
        /// This method select remarks for list.
        /// </summary>
        /// <returns>Returns a  RemarksFor list object.</returns>
        public List<RemarksFor> SelectRemarksForList()
        {
            List<RemarksFor> remarksForObjList = new List<RemarksFor>();
            RemarksFor remarksForObj = null;
            string mSQL = "SELECT * FROM t_pluss_remarksfor";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    remarksForObj = new RemarksFor();
                    remarksForObj.Id = Convert.ToInt32(xRs["REFO_ID"]);
                    remarksForObj.Name = Convert.ToString(xRs["REFO_NAME"]);
                    remarksForObjList.Add(remarksForObj);
                }
                xRs.Close();
            }
            return remarksForObjList;
        }

        /// <summary>
        /// This method covert the special character.
        /// </summary>
        /// <param name="sMessage">Gets a string.</param>
        /// <returns>Returns a converted string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                sMessage = sMessage.Replace(@"<", @"<<");
                sMessage = sMessage.Replace(@">", @">>");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
