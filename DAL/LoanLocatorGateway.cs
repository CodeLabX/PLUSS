﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data.Common;
using Bat.Common;
namespace DAL
{
    public class LoanLocatorGateway
    {
        enum ActionFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            DELETE = 3,
            INSERTERROR = -1,
            UPDATEERROR = -2,
            DELETEERROR = -3
        }
        public LoanLocatorGateway()
        {
            Bat.Common.Connection.ConnectionStringName = "conStringLoanLocator";
        }
        public int UpdateLoanLocatorStatus(Int32 llid,LoanApplicationDetails loanApplicationDetailsObj, List<Remarks> remarks)
        {
            switch()
            {
                case "1":
                break;
                case "2":
                break;
                case "3":
                break;
                case "4":
                break;
                case "5":
                break;
                case "6":
                break;
                case "7":
                break;
                case "8":
                break;
                case "9":
                break;
                case "10":
                break;
                case "11":
                break;
                case "12":
                break;
                case "13":
                break;
                case "14":
                break;
                case "15":
                break;
                case "16":
                break;
                case "17":
                break;
                case "18":
                break;
                case "19":
                break;
                default:
                break;

            }
            return (int)ActionFlag.INSERT;
        }
        public int SetForwardStatus(Int32 llid, Int32 statusId)
        {
            int updateRow = -1;
            int updateRow2 = -1;
            //Bat.Common.Connection.ConnectionStringName = "conStringLoanLocator";
            string queryString = String.Format("UPDATE loan_app_info SET LOAN_STATU_DT = NOW() , LOAN_STATU_ID = {1} " +
                                               "WHERE LOAN_APPLI_ID = {0}", llid, statusId);
            string queryString2 = String.Format("UPDATE loan_app_info SET LOAN_STATUS_ID = {1} " +
                                               "WHERE LOAN_APPL_ID = {0}", llid, statusId);
            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                updateRow2 = DbQueryManager.ExecuteNonQuery(queryString2, "conStringLoanLocator");
                if (updateRow > -1 && updateRow2 > -1)
                {
                    return (int)ActionFlag.UPDATE;
                }
                else
                {
                    return (int)ActionFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)ActionFlag.UPDATEERROR;
            }
        }

        public int SetDeferStatus(Int32 llid)
        {
            int updateRow = -1;
            string queryString = String.Format("UPDATE loan_app_info SET LOAN_STATU_DT = NOW() , LOAN_STATU_ID = 4 " +
                                               "WHERE LOAN_APPLI_ID = {0}", llid);

            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (updateRow > 0)
                {
                    return (int)ActionFlag.UPDATE;
                }
                else
                {
                    return (int)ActionFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)ActionFlag.UPDATEERROR;
            }
        }

        public int DeleteDeferReason(Int32 llid)
        {
            int deleteRow = -1;
            string queryString = String.Format("DELETE FROM loan_dec_def_reason_info WHERE LOAN_APPL_ID = {0}", llid);

            try
            {
                deleteRow = DbQueryManager.ExecuteNonQuery(queryString, "conStringLoanLocator");
                if (deleteRow > 0)
                {
                    return (int)ActionFlag.DELETE;
                }
                else
                {
                    return (int)ActionFlag.DELETEERROR;
                }
            }
            catch
            {
                return (int)ActionFlag.DELETEERROR;
            }
        }

        public int InsertDeferReason(Int32 llid, List<Int32> reasonId)
        {
            int insertRow = -1;
            string queryString = "INSERT INTO loan_dec_def_reason_info (LOAN_APPL_ID, REAS_ID) VALUES ";
            for (int i = 0; i < reasonId.Count; i++)
            {
                queryString += "(" + llid + "," + reasonId[i] + "),";
            }
            queryString = queryString.Substring(0, queryString.Length - 1);
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(queryString, "conStringLoanLocator");
                if (insertRow > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }

        public int InsertDeferReasonDetails(LoanApplicationDetails loanApplicationDetailsObj, List<Remarks> remarks)
        {
            int insertRow = -1;
            string sRemarks = string.Empty;
            int iRemarksId = 0;
            string mSQL = "SELECT ID_LEARNER FROM learner_details_table WHERE USERNAME='" + loanApplicationDetailsObj.PersonId.ToString() + "'";
            string lonaLocatorUserId = (string) DbQueryManager.ExecuteScalar(mSQL, "conStringLoanLocator");
            string queryString = "INSERT INTO loan_app_details (LOAN_APPL_ID, PERS_ID, DEPT_ID, DATE_TIME, PROCESS_STATUS_ID, REMARKS, DEFE_DECL_REAS_ID) VALUES  ";
            if (lonaLocatorUserId.Length == 0 || lonaLocatorUserId == null || lonaLocatorUserId=="")
            {
                lonaLocatorUserId = "0";
            }
            if (remarks.Count == 0)
            {
                queryString += "(" + loanApplicationDetailsObj.LAId + ", '" + lonaLocatorUserId + "'," + loanApplicationDetailsObj.DeptId + ", Now()," + loanApplicationDetailsObj.ProcessStatusId + ",'',0),";
            }
            else
            {
                for (int i = 0; i < remarks.Count; i++)
                {
                    //queryString += "(" + loanApplicationDetailsObj.LAId + ", '" + lonaLocatorUserId + "'," + loanApplicationDetailsObj.DeptId + ", Now()," + loanApplicationDetailsObj.ProcessStatusId + ",'" + remarks[i].Name + "'," + remarks[i].Id + "),";
                    sRemarks += remarks[i].Name + ",";
                }
                queryString += "(" + loanApplicationDetailsObj.LAId + ", '" + lonaLocatorUserId + "'," + loanApplicationDetailsObj.DeptId + ", Now()," + loanApplicationDetailsObj.ProcessStatusId + ",'" + sRemarks.Substring(0, sRemarks.Length - 1) + "'," + iRemarksId + "),";
            }
            queryString = queryString.Substring(0, queryString.Length - 1);
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(queryString, "conStringLoanLocator");
                if (insertRow > 0)
                {
                    return (int)ActionFlag.INSERT;
                }
                else
                {
                    return (int)ActionFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)ActionFlag.INSERTERROR;
            }
        }


    }
}
