﻿using AzUtilities;
using AzUtilities.Common;
using Bat.Common;
using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DAL
{
    public class CustomerBulkDataGateway
    {
        public Operation Save(DataTable table, Dictionary<string, string> columnMap)
        {
            try
            {
                string conString = Connection.GetConnection().ConnectionString;

                string con = Connection.GetConnection().ConnectionString;
                SqlConnection connection = new SqlConnection(con);
                connection.Open();


                using (SqlBulkCopy copier = new SqlBulkCopy(connection))
                {
                    copier.DestinationTableName = "CustomerBulkData_temp";

                    foreach (var item in columnMap)
                    {
                        copier.ColumnMappings.Add(item.Key, item.Value);
                    }

                    copier.BatchSize = 500;
                    copier.WriteToServer(table);
                }

                return Operation.Success;
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("CustomerBulkDataGateway :: Save");
                LogFile.WriteLog(ex);

                return Operation.Error;
            }
        }

        public CustomersBulkData GetCustomerDataWithAccountNo(string accNo, string withoutDashes)
        {
            string sql = @"SELECT * FROM CustomerBulkData a
                            WHERE (a.RefrenceID = '" + accNo + "' OR a.RefrenceID = '" + withoutDashes + "') AND a.RefType = 'ACCOUNT'";
            try
            {
                return Data<CustomersBulkData>.DataSource(sql).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("CustomerBulkDataGateway :: GetCustomerDataWithAccountNo");
                LogFile.WriteLog(ex);

                return new CustomersBulkData();
            }
        }

        public List<CustomersBulkData> GetAllTemp()
        {
            string sql = "SELECT * FROM CustomerBulkData_temp order by ID";

            var list = Data<CustomersBulkData>.DataSource(sql);

            return list;
        }


        public string ApproveCustomerBulkDataTemp(User actionUser)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@CheckID", actionUser.UserId));

                int count = DbQueryManager.ExecuteSP("spApproveCustomerBulkDataList", parameters);
                if (count > 0)
                    return Operation.Success.ToString();
                return Operation.Failed.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("ApproveNegativeListTemp : CustomerBulkDataGateway");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "ApproveCustomerBulkDataTemp");

                return Operation.Error.ToString();
            }
        }
    }
}
