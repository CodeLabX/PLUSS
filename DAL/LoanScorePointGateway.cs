﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    /// <summary>
    /// LoanScorePointGateway Class.
    /// </summary>
    public class LoanScorePointGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor.
        /// </summary>
        public LoanScorePointGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select loan score point info.
        /// </summary>
        /// <param name="lLId">Gets  a llid.</param>
        /// <returns>Returns a LoanScorePoint object.</returns>
        public LoanScorePoint SelectLoanScorePoint(Int64 lLId)
        {
            string mSQL = "SELECT * FROM t_pluss_loanscorepoint WHERE SCPI_LLID=" + lLId;
           
            LoanScorePoint loanScorePointObj = null;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();

                while (xRs.Read())
                {
                    loanScorePointObj = new LoanScorePoint();
                    loanScorePointObj.Id = Convert.ToInt64(xRs["SCPI_ID"]);
                    loanScorePointObj.LlId = Convert.ToInt64(xRs["SCPI_LLID"]);
                    loanScorePointObj.CutOfScore = Convert.ToDouble(xRs["SCPI_CUTOFSCORE"]);
                    loanScorePointObj.IntreceptValue = Convert.ToDouble(xRs["SCPI_INTRECEPTVALUE"]);
                    loanScorePointObj.WorkExperiencePoint = Convert.ToDouble(xRs["SCPI_WORKEXPERIENCE"]);
                    loanScorePointObj.IncomeRatioPoint = Convert.ToDouble(xRs["SCPI_INCOMERATIO"]);
                    loanScorePointObj.ResidentypePoint = Convert.ToDouble(xRs["SCPI_RESIDENCETYPE"]);
                    loanScorePointObj.MaritalStatusPoint = Convert.ToDouble(xRs["SCPI_MARITALSTATUS"]);
                    loanScorePointObj.QualificationPoint = Convert.ToDouble(xRs["SCPI_QUALIFICATION"]);
                    loanScorePointObj.GrossMonthlyIncomePoint =
                        Convert.ToDouble(xRs["SCPI_GROSSMONTHLYINCOME"]);
                    loanScorePointObj.SpouseOccupationPoint =
                        Convert.ToDouble(xRs["SCPI_SPOUSESOCCUPATION"]);
                    loanScorePointObj.VehiclPoint = Convert.ToDouble(xRs["SCPI_VEHICLE"]);
                    loanScorePointObj.TotalPoint = Convert.ToDouble(xRs["SCPI_TOTALPOINT"]);
                    loanScorePointObj.Result = Convert.ToString(xRs["SCPI_RESULT"]);
                }
                xRs.Close();
            }
            return loanScorePointObj;
        }

        /// <summary>
        /// This method select loan score point info.
        /// </summary>
        /// <returns>Returns a LoanScorePoint list object.</returns>
        public List<LoanScorePoint> GetLoanScorePointList()
        {
            List<LoanScorePoint> loanScorePointObjList = new List<LoanScorePoint>();
            string mSQL = "SELECT * FROM t_pluss_loanscorepoint";
            ADODB.Recordset xRs = new ADODB.Recordset();
            LoanScorePoint loanScorePointObj = null;
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                loanScorePointObj = new LoanScorePoint();
                loanScorePointObj.Id = Convert.ToInt64(xRs.Fields["SCPI_ID"].Value);
                loanScorePointObj.LlId = Convert.ToInt64(xRs.Fields["SCPI_LLID"].Value);
                loanScorePointObj.CutOfScore = Convert.ToDouble(xRs.Fields["SCPI_CUTOFSCORE"].Value);
                loanScorePointObj.IntreceptValue = Convert.ToDouble(xRs.Fields["SCPI_INTRECEPTVALUE"].Value);
                loanScorePointObj.WorkExperiencePoint = Convert.ToDouble(xRs.Fields["SCPI_WORKEXPERIENCE"].Value);
                loanScorePointObj.IncomeRatioPoint = Convert.ToDouble(xRs.Fields["SCPI_INCOMERATIO"].Value);
                loanScorePointObj.ResidentypePoint = Convert.ToDouble(xRs.Fields["SCPI_RESIDENCETYPE"].Value);
                loanScorePointObj.MaritalStatusPoint = Convert.ToDouble(xRs.Fields["SCPI_MARITALSTATUS"].Value);
                loanScorePointObj.QualificationPoint = Convert.ToDouble(xRs.Fields["SCPI_QUALIFICATION"].Value);
                loanScorePointObj.GrossMonthlyIncomePoint = Convert.ToDouble(xRs.Fields["SCPI_GROSSMONTHLYINCOME"].Value);
                loanScorePointObj.SpouseOccupationPoint = Convert.ToDouble(xRs.Fields["SCPI_SPOUSESOCCUPATION"].Value);
                loanScorePointObj.VehiclPoint = Convert.ToDouble(xRs.Fields["SCPI_VEHICLE"].Value);
                loanScorePointObj.TotalPoint = Convert.ToDouble(xRs.Fields["SCPI_TOTALPOINT"].Value);
                loanScorePointObj.Result = Convert.ToString(xRs.Fields["SCPI_RESULT"].Value);
                loanScorePointObjList.Add(loanScorePointObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return loanScorePointObjList;
        }

        /// <summary>
        /// This method provide insertion or update loan score point info.
        /// </summary>
        /// <param name="loanScorePointObj">Gets a LoanScorePoint object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendLoanScorePointInToDB(LoanScorePoint loanScorePointObj)
        {
            LoanScorePoint existingLoanScorePointObj = SelectLoanScorePoint(loanScorePointObj.LlId);
            if (existingLoanScorePointObj!=null)
            {
                loanScorePointObj.Id = existingLoanScorePointObj.Id;
                return UpdateLoanScorePoint(loanScorePointObj);
            }
            else
            {
                return InsertLoanScorePoint(loanScorePointObj);
            }
        }

        /// <summary>
        /// This method provide insertion operation of loan score point info.
        /// </summary>
        /// <param name="loanScorePointObj">Gets a LoanScorePoint object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertLoanScorePoint(LoanScorePoint loanScorePointObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_loanscorepoint (SCPI_LLID, SCPI_CUTOFSCORE,"+ 
                              "SCPI_INTRECEPTVALUE, SCPI_WORKEXPERIENCE, "+
                              "SCPI_INCOMERATIO, SCPI_RESIDENCETYPE, "+
                              "SCPI_MARITALSTATUS, SCPI_QUALIFICATION, "+
                              "SCPI_GROSSMONTHLYINCOME, SCPI_SPOUSESOCCUPATION,"+ 
                              "SCPI_VEHICLE, SCPI_TOTALPOINT, "+
                              "SCPI_RESULT) VALUES(" +
                              loanScorePointObj.LlId + "," +
                              loanScorePointObj.CutOfScore + "," +
                              loanScorePointObj.IntreceptValue + "," +
                              loanScorePointObj.WorkExperiencePoint + "," +
                              loanScorePointObj.IncomeRatioPoint + "," +
                              loanScorePointObj.ResidentypePoint + "," +
                              loanScorePointObj.MaritalStatusPoint + "," +
                              loanScorePointObj.QualificationPoint + "," +
                              loanScorePointObj.GrossMonthlyIncomePoint + "," +
                              loanScorePointObj.SpouseOccupationPoint + "," +
                              loanScorePointObj.VehiclPoint + "," +
                              loanScorePointObj.TotalPoint + ",'" +
                              loanScorePointObj.Result + "')";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1) 
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method provide update operation of loan score point info.
        /// </summary>
        /// <param name="loanScorePointObj">Gets a LoanScorePoint object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateLoanScorePoint(LoanScorePoint loanScorePointObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_loanscorepoint SET " +
                              "SCPI_LLID="+loanScorePointObj.LlId + "," +
                              "SCPI_CUTOFSCORE="+loanScorePointObj.CutOfScore + "," +
                              "SCPI_INTRECEPTVALUE="+loanScorePointObj.IntreceptValue + "," +
                              "SCPI_WORKEXPERIENCE="+loanScorePointObj.WorkExperiencePoint + "," +
                              "SCPI_INCOMERATIO="+loanScorePointObj.IncomeRatioPoint + "," +
                              "SCPI_RESIDENCETYPE="+loanScorePointObj.ResidentypePoint + "," +
                              "SCPI_MARITALSTATUS="+loanScorePointObj.MaritalStatusPoint + "," +
                              "SCPI_QUALIFICATION="+loanScorePointObj.QualificationPoint + "," +
                              "SCPI_GROSSMONTHLYINCOME="+loanScorePointObj.GrossMonthlyIncomePoint + "," +
                              "SCPI_SPOUSESOCCUPATION="+loanScorePointObj.SpouseOccupationPoint + "," +
                              "SCPI_VEHICLE="+loanScorePointObj.VehiclPoint + "," +
                              "SCPI_TOTALPOINT="+loanScorePointObj.TotalPoint + "," +
                              "SCPI_RESULT='" + loanScorePointObj.Result + "' WHERE SCPI_ID=" + loanScorePointObj.Id;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1) 
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

    }
}
