﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// BILMUE Gateway Class.
    /// </summary>
    public class BILMUEGateway
    {
        #region BILMUEGateway()
        /// <summary>
        /// Constructor.
        /// </summary>
        public BILMUEGateway()
        {
            //Constructor logic here.
        }
        #endregion

        #region GetCustomerDetailData(string llId)
        /// <summary>
        /// This method provide customer detail data.
        /// </summary>
        /// <param name="llId">Gets llid</param>
        /// <returns>Returns Bilmue object.</returns>
        public BILMUE GetCustomerDetailData(string llId)
        {
            BILMUE customerDetailObj = null;            

            string queryString = "SELECT DISTINCT CUDE_CUSTOMER_ID, CUDE_CUSTOMER_LLID,  CUST_MOB12 " +
                                  "FROM t_pluss_customerdetails AS CD " +
                                  "INNER JOIN t_pluss_customer AS C ON CD.CUDE_CUSTOMER_LLID = C.CUST_LLID " +
                                  "WHERE CUDE_CUSTOMER_LLID = '" + llId + "'";

            string queryString2 = "SELECT CUDE_FACILITY, SUM(CUDE_OUTSTANDING) AS CUDE_OUTSTANDING " +
                                   "FROM t_pluss_customerdetails " +
                                   "WHERE CUDE_CUSTOMER_LLID = '" + llId + "' " +
                                   "AND CUDE_FACILITY = 'Credit Card ($)' "+
                                   "GROUP BY CUDE_FACILITY";
            DataTable customerDetailDataTableObj = DbQueryManager.GetTable(queryString);
            DataTable customerDetailDataTableObj2 = DbQueryManager.GetTable(queryString2);
            if (customerDetailDataTableObj != null)
            {
                DataTableReader customerDetailDataTableReaderObj = customerDetailDataTableObj.CreateDataReader();
                if (customerDetailDataTableReaderObj != null)
                {
                    while (customerDetailDataTableReaderObj.Read())
                    {
                        customerDetailObj = new BILMUE();
                        customerDetailObj.Id = Convert.ToInt32("0" + customerDetailDataTableReaderObj["CUDE_CUSTOMER_ID"]);
                        customerDetailObj.LLId = Convert.ToString(customerDetailDataTableReaderObj["CUDE_CUSTOMER_LLID"]);
                        customerDetailObj.Mob12 = Convert.ToInt32("0" + customerDetailDataTableReaderObj["CUST_MOB12"]);
                        if (customerDetailDataTableObj2 != null)
                        {
                            DataTableReader customerDetailDataTableReaderObj2 = customerDetailDataTableObj2.CreateDataReader();
                            if (customerDetailDataTableReaderObj2 != null)
                            {
                                while (customerDetailDataTableReaderObj2.Read())
                                {
                                    string facility = Convert.ToString(customerDetailDataTableReaderObj2["CUDE_FACILITY"]);
                                    double outStanding = Convert.ToDouble("0" + customerDetailDataTableReaderObj2["CUDE_OUTSTANDING"]);
                                    customerDetailObj.Facility.Add(facility);
                                    customerDetailObj.OutStanding.Add(outStanding);
                                }
                                customerDetailDataTableReaderObj2.Close();
                            }
                            customerDetailDataTableObj2.Clear();
                        }
                    }
                    customerDetailDataTableReaderObj.Close();
                }
                customerDetailDataTableObj.Clear();
            }
            return customerDetailObj;
        }
        #endregion

        #region GetCustomerExposureData(string llId)
        /// <summary>
        /// This method provide customer exposure data.
        /// </summary>
        /// <param name="llId">Gets llid</param>
        /// <returns>Returns bilmue list object.</returns>
        public List<BILMUE> GetCustomerExposureData(string llId)
        {
            //BILMUE customerDetailListObj = new BILMUE();
            List<BILMUE> exposureListObj = new List<BILMUE>();
            string queryString = "SELECT 	CUDE_FACILITY, CUDE_OUTSTANDING, CUDE_CASHSECURITY " +
                                 "FROM 	" +
                                            "t_pluss_customerdetails " +
                                 "WHERE	CUDE_CUSTOMER_LLID = '" + llId + "'";
            DataTable exposureDataTableObj = DbQueryManager.GetTable(queryString);
            if (exposureDataTableObj != null)
            {
                DataTableReader exposureDataTableReaderObj = exposureDataTableObj.CreateDataReader();
                if (exposureDataTableReaderObj != null)
                {
                    while (exposureDataTableReaderObj.Read())
                    {
                        BILMUE exposureObj = new BILMUE();
                        string facility = Convert.ToString(exposureDataTableReaderObj["CUDE_FACILITY"]);
                        double outStanding = Convert.ToDouble("0" + exposureDataTableReaderObj["CUDE_OUTSTANDING"]);
                        double cashSecurity = Convert.ToDouble("0" + exposureDataTableReaderObj["CUDE_CASHSECURITY"]);
                        exposureObj.Facility.Add(facility);
                        exposureObj.OutStanding.Add(outStanding);
                        exposureObj.CashSecurity.Add(cashSecurity);
                        exposureListObj.Add(exposureObj);
                    }
                    exposureDataTableReaderObj.Close();
                }
                exposureDataTableObj.Clear();
            }
            return exposureListObj;
        }
        #endregion

        #region GetBILMUEData(string llId)
        /// <summary>
        /// This method provide BilMue data.
        /// </summary>
        /// <param name="llId">Gets a llId</param>
        /// <returns>Returns a bilmue object.</returns>
        public BILMUE GetBILMUEData(string llId)
        {
            BILMUE bilMueObj = null;
            string queryString = "SELECT 	BIMU_ID, BIMU_LLID, BIMU_MOB12, BIMU_BILGUARDEDMUE, BIMU_CREDITCARD, " +
                                            "BIMU_NETALLOWED, BIMU_EXISTINGBILOS, BIMU_FDVALUE, " +
                                            "BIMU_UNSECUREDBIL, BIMU_UNSECUREDOTHERS, BIMU_CREDITCARD2, " +
                                            "BIMU_NETUNSECURED, BIMU_AVAILABLE, BIMU_NETEXPOSURE,  " +
                                            "BIMU_NETSECURITY, BIMU_NAR, " +
                                            "BIMU_PLEXP, BIMU_PLSEC, BIMU_PLNAR, BIMU_CCEXP, BIMU_CCSEC, BIMU_CCNAR, BIMU_OTHBILEXP, " +
                                            "BIMU_OTHBILSEC, BIMU_OTHBILNAR, BIMU_OTHPLIPFFLEXP, BIMU_OTHPLIPFFLSEC, BIMU_OTHPLIPFFLNAR, " +
                                            "BIMU_OTHCCEXP, BIMU_OTHCCSEC, BIMU_OTHCCNAR, " +
                                            "BIMU_FAC1, BIMU_EXP1, BIMU_SEC1, BIMU_NAR1, BIMU_FAC2, BIMU_EXP2, BIMU_SEC2, BIMU_NAR2, " +
                                            "BIMU_FAC3, BIMU_EXP3, BIMU_SEC3, BIMU_NAR3, BIMU_FAC4, BIMU_EXP4, BIMU_SEC4, BIMU_NAR4, " +
                                            "BIMU_FAC5, BIMU_EXP5, BIMU_SEC5, BIMU_NAR5, BIMU_FAC6, BIMU_EXP6, BIMU_SEC6, BIMU_NAR6 " +
                                 "FROM " +
                                            "t_pluss_bilmue " +
                                 "WHERE     BIMU_LLID='" + llId + "'";
            DataTable bilMueDataTableObj = DbQueryManager.GetTable(queryString);
            if (bilMueDataTableObj != null)
            {
                DataTableReader bilMueDataTableReaderObj = bilMueDataTableObj.CreateDataReader();
                if (bilMueDataTableReaderObj != null)
                {
                    while (bilMueDataTableReaderObj.Read())
                    {
                        bilMueObj = new BILMUE();
                        bilMueObj.Id = Convert.ToInt32("0" + bilMueDataTableReaderObj["BIMU_ID"]);
                        bilMueObj.LLId = Convert.ToString(bilMueDataTableReaderObj["BIMU_LLID"]);
                        bilMueObj.Mob12 = Convert.ToInt32("0" + bilMueDataTableReaderObj["BIMU_MOB12"]);
                        bilMueObj.BilGuardedMue = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_BILGUARDEDMUE"]);
                        bilMueObj.CreditCard = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_CREDITCARD"]);
                        bilMueObj.NetAllowed = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NETALLOWED"]);
                        bilMueObj.ExistingBilOs = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_EXISTINGBILOS"]);
                        bilMueObj.FdValue = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_FDVALUE"]);
                        bilMueObj.UnsecuredBil = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_UNSECUREDBIL"]);
                        bilMueObj.UnsecuredOther = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_UNSECUREDOTHERS"]);
                        bilMueObj.CreditCard2 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_CREDITCARD2"]);
                        bilMueObj.NetUnsecured = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NETUNSECURED"]);
                        bilMueObj.Available = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_AVAILABLE"]);
                        bilMueObj.NetExposure = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NETEXPOSURE"]);
                        bilMueObj.NetSecurity = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NETSECURITY"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR"]) < 0)
                        {
                            bilMueObj.NetNar = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR"]);
                        }
                        else
                        {
                            bilMueObj.NetNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR"]);
                        }
                        //bilMueObj.NetNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR"]);
                        bilMueObj.PLExposure = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_PLEXP"]);
                        bilMueObj.PLSecurity = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_PLSEC"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_PLNAR"]) < 0)
                        {
                            bilMueObj.PLNar = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_PLNAR"]);
                        }
                        else
                        {
                            bilMueObj.PLNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_PLNAR"]);
                        }
                        //bilMueObj.PLNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_PLNAR"]);
                        bilMueObj.CCExposure = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_CCEXP"]);
                        bilMueObj.CCSecurity = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_CCSEC"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_CCNAR"]) < 0)
                        {
                            bilMueObj.CCNar = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_CCNAR"]);
                        }
                        else
                        {
                            bilMueObj.CCNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_CCNAR"]);
                        }
                        //bilMueObj.CCNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_CCNAR"]);

                        bilMueObj.OtherBilExposure = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHBILEXP"]);
                        bilMueObj.OtherBilSecurity = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHBILSEC"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_OTHBILNAR"]) < 0)
                        {
                            bilMueObj.OtherBilNar = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_OTHBILNAR"]);
                        }
                        else
                        {
                            bilMueObj.OtherBilNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHBILNAR"]);
                        }
                        //bilMueObj.OtherBilNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHBILNAR"]);
                        bilMueObj.OtherPLIPEFLexiExposure = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHPLIPFFLEXP"]);
                        bilMueObj.OtherPLIPEFLexiSecurity = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHPLIPFFLSEC"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_OTHPLIPFFLNAR"]) < 0)
                        {
                            bilMueObj.OtherPLIPEFLexiNar = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_OTHPLIPFFLNAR"]);
                        }
                        else
                        {
                            bilMueObj.OtherPLIPEFLexiNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHPLIPFFLNAR"]);
                        }
                        //bilMueObj.OtherPLIPEFLexiNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHPLIPFFLNAR"]);
                        bilMueObj.OtherCCExposure = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHCCEXP"]);
                        bilMueObj.OtherCCSecurity = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHCCSEC"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_OTHCCNAR"]) < 0)
                        {
                            bilMueObj.OtherCCNar = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_OTHCCNAR"]);
                        }
                        else
                        {
                            bilMueObj.OtherCCNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHCCNAR"]);
                        }
                        //bilMueObj.OtherCCNar = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_OTHCCNAR"]);

                        bilMueObj.Facility1 = Convert.ToString(bilMueDataTableReaderObj["BIMU_FAC1"]);
                        bilMueObj.Exposure1 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_EXP1"]);
                        bilMueObj.Security1 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_SEC1"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR1"]) < 0)
                        {
                            bilMueObj.Nar1 = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR1"]);
                        }
                        else
                        {
                            bilMueObj.Nar1 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR1"]);
                        }
                        
                        bilMueObj.Facility2 = Convert.ToString(bilMueDataTableReaderObj["BIMU_FAC2"]);
                        bilMueObj.Exposure2 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_EXP2"]);
                        bilMueObj.Security2 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_SEC2"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR2"]) < 0)
                        {
                            bilMueObj.Nar2 = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR2"]);
                        }
                        else
                        {
                            bilMueObj.Nar2 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR2"]);
                        }
                        //bilMueObj.Nar2 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR2"]);
                        bilMueObj.Facility3 = Convert.ToString(bilMueDataTableReaderObj["BIMU_FAC3"]);
                        bilMueObj.Exposure3 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_EXP3"]);
                        bilMueObj.Security3 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_SEC3"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR3"]) < 0)
                        {
                            bilMueObj.Nar3 = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR3"]);
                        }
                        else
                        {
                            bilMueObj.Nar3 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR3"]);
                        }
                        //bilMueObj.Nar3 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR3"]);
                        bilMueObj.Facility4 = Convert.ToString(bilMueDataTableReaderObj["BIMU_FAC4"]);
                        bilMueObj.Exposure4 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_EXP4"]);
                        bilMueObj.Security4 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_SEC4"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR4"]) < 0)
                        {
                            bilMueObj.Nar4 = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR4"]);
                        }
                        else
                        {
                            bilMueObj.Nar4 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR4"]);
                        }
                        //bilMueObj.Nar4 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR4"]);
                        bilMueObj.Facility5 = Convert.ToString(bilMueDataTableReaderObj["BIMU_FAC5"]);
                        bilMueObj.Exposure5 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_EXP5"]);
                        bilMueObj.Security5 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_SEC5"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR5"]) < 0)
                        {
                            bilMueObj.Nar5 = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR5"]);
                        }
                        else
                        {
                            bilMueObj.Nar5 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR5"]);
                        }
                        //bilMueObj.Nar5 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR5"]);
                        bilMueObj.Facility6 = Convert.ToString(bilMueDataTableReaderObj["BIMU_FAC6"]);
                        bilMueObj.Exposure6 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_EXP6"]);
                        bilMueObj.Security6 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_SEC6"]);
                        if (Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR6"]) < 0)
                        {
                            bilMueObj.Nar6 = Convert.ToDouble(bilMueDataTableReaderObj["BIMU_NAR6"]);
                        }
                        else
                        {
                            bilMueObj.Nar6 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR6"]);
                        }
                        //bilMueObj.Nar6 = Convert.ToDouble("0" + bilMueDataTableReaderObj["BIMU_NAR6"]);
                    }
                    bilMueDataTableReaderObj.Close();
                }
                bilMueDataTableObj.Clear();
            }
            return bilMueObj;
        }
        #endregion

        #region InsertORUpdateBilMueInfo(BILMUE bilMueObj)
        /// <summary>
        /// This method provide insert/update bilmue information.
        /// </summary>
        /// <param name="bilMueObj">Gets bil mue object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateBilMueInfo(BILMUE bilMueObj)
        {
            int insertORUpdateValue = -1;
            if (!IsExistsBilMueInfo(Convert.ToInt32(bilMueObj.Id)))
            {
                insertORUpdateValue = InsertBilMueInformation(bilMueObj);
            }
            else
            {
                insertORUpdateValue = UpdateBilMueInformation(bilMueObj);
            }
            return insertORUpdateValue;
        }

        /// <summary>
        /// This method provide insertion of bilmue information.
        /// </summary>
        /// <param name="bilMueObj">Gets bilmue object.</param>
        /// <returns>Returns a positive value if inserted.</returns>
        private int InsertBilMueInformation(BILMUE bilMueObj)
        {
            int insertRow = -1;
            string queryString = String.Format("INSERT  INTO t_pluss_bilmue " +
                                                        "(BIMU_LLID, BIMU_MOB12, BIMU_BILGUARDEDMUE, " +
                                                        "BIMU_CREDITCARD, BIMU_NETALLOWED, BIMU_EXISTINGBILOS, " +
                                                        "BIMU_FDVALUE, BIMU_UNSECUREDBIL, BIMU_UNSECUREDOTHERS, " +
                                                        "BIMU_CREDITCARD2, BIMU_NETUNSECURED, " +
                                                        "BIMU_AVAILABLE, BIMU_NETEXPOSURE, BIMU_NETSECURITY, " +
                                                        "BIMU_NAR, " +
                                                        "BIMU_PLEXP, BIMU_PLSEC, BIMU_PLNAR, BIMU_CCEXP, BIMU_CCSEC, BIMU_CCNAR, BIMU_OTHBILEXP, " +
                                                        "BIMU_OTHBILSEC, BIMU_OTHBILNAR, BIMU_OTHPLIPFFLEXP, BIMU_OTHPLIPFFLSEC, BIMU_OTHPLIPFFLNAR, " +
                                                        "BIMU_OTHCCEXP, BIMU_OTHCCSEC, BIMU_OTHCCNAR, " +
                                                        "BIMU_FAC1,BIMU_EXP1, BIMU_SEC1, BIMU_NAR1, BIMU_FAC2, BIMU_EXP2, BIMU_SEC2, BIMU_NAR2, " +
                                                        "BIMU_FAC3, BIMU_EXP3, BIMU_SEC3, BIMU_NAR3, BIMU_FAC4, BIMU_EXP4, BIMU_SEC4, BIMU_NAR4, " +
                                                        "BIMU_FAC5, BIMU_EXP5, BIMU_SEC5, BIMU_NAR5, BIMU_FAC6, BIMU_EXP6, BIMU_SEC6, BIMU_NAR6 " +
                                                        ") " +
                                          "VALUES " +
                                                        "('{0}', {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, " +
                                                        "{10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20},{21},{22},{23},{24},{25},{26},{27},{28},{29},'{30}', " +
                                                        "{31},{32},{33}, '{34}',{35},{36},{37},'{38}',{39},{40},{41},'{42}',{43},{44},{45},'{46}',{47},{48},{49},'{50}',{51},{52},{53} " +
                                                        ") ",
                                                        EscapeCharacter(bilMueObj.LLId), bilMueObj.Mob12, bilMueObj.BilGuardedMue, bilMueObj.CreditCard,
                                                        bilMueObj.NetAllowed, bilMueObj.ExistingBilOs, bilMueObj.FdValue, bilMueObj.UnsecuredBil,
                                                        bilMueObj.UnsecuredOther, bilMueObj.CreditCard2, bilMueObj.NetUnsecured, bilMueObj.Available,
                                                        bilMueObj.NetExposure, bilMueObj.NetSecurity, bilMueObj.NetNar,
                                                        bilMueObj.PLExposure, bilMueObj.PLSecurity, bilMueObj.PLNar, bilMueObj.CCExposure, bilMueObj.CCSecurity, bilMueObj.CCNar,
                                                        bilMueObj.OtherBilExposure, bilMueObj.OtherBilSecurity, bilMueObj.OtherBilNar,
                                                        bilMueObj.OtherPLIPEFLexiExposure, bilMueObj.OtherPLIPEFLexiSecurity, bilMueObj.OtherPLIPEFLexiNar,
                                                        bilMueObj.OtherCCExposure, bilMueObj.OtherCCSecurity, bilMueObj.OtherCCNar,
                                                        EscapeCharacter(bilMueObj.Facility1), bilMueObj.Exposure1, bilMueObj.Security1, bilMueObj.Nar1,
                                                        EscapeCharacter(bilMueObj.Facility2), bilMueObj.Exposure2, bilMueObj.Security2, bilMueObj.Nar2,
                                                        EscapeCharacter(bilMueObj.Facility3), bilMueObj.Exposure3, bilMueObj.Security3, bilMueObj.Nar3,
                                                        EscapeCharacter(bilMueObj.Facility4), bilMueObj.Exposure4, bilMueObj.Security4, bilMueObj.Nar4,
                                                        EscapeCharacter(bilMueObj.Facility5), bilMueObj.Exposure5, bilMueObj.Security5, bilMueObj.Nar5,
                                                        EscapeCharacter(bilMueObj.Facility6), bilMueObj.Exposure6, bilMueObj.Security6, bilMueObj.Nar6
                                                        );

            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (insertRow > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }

        /// <summary>
        /// This method provide update bilmue information.
        /// </summary>
        /// <param name="bilMueObj">Gets bilmue object.</param>
        /// <returns>Returns a positive value if updated.</returns>
        private int UpdateBilMueInformation(BILMUE bilMueObj)
        {
            int updateRow = -1;
            string queryString = String.Format("UPDATE  t_pluss_bilmue SET " +
                                                        "BIMU_LLID = '{0}', BIMU_MOB12 = {1}, BIMU_BILGUARDEDMUE = {2}, " +
                                                        "BIMU_CREDITCARD = {3}, BIMU_NETALLOWED = {4}, BIMU_EXISTINGBILOS = {5}, " +
                                                        "BIMU_FDVALUE = {6}, BIMU_UNSECUREDBIL = {7}, BIMU_UNSECUREDOTHERS = {8}," +
                                                        "BIMU_CREDITCARD2 = {9}, BIMU_NETUNSECURED = {10}, " +
                                                        "BIMU_AVAILABLE = {11}, BIMU_NETEXPOSURE = {12}, BIMU_NETSECURITY = {13}, BIMU_NAR = {14},  " +
                                                        "BIMU_PLEXP = {15}, BIMU_PLSEC = {16}, BIMU_PLNAR = {17}, BIMU_CCEXP = {18}, BIMU_CCSEC = {19}, BIMU_CCNAR = {20}, BIMU_OTHBILEXP = {21}, " +
                                                        "BIMU_OTHBILSEC = {22}, BIMU_OTHBILNAR = {23}, BIMU_OTHPLIPFFLEXP = {24}, BIMU_OTHPLIPFFLSEC = {25}, BIMU_OTHPLIPFFLNAR = {26}, " +
                                                        "BIMU_OTHCCEXP = {27}, BIMU_OTHCCSEC = {28}, BIMU_OTHCCNAR = {29}, " +
                                                        "BIMU_FAC1 = '{30}', BIMU_EXP1 = {31}, BIMU_SEC1 = {32}, BIMU_NAR1 = {33}, BIMU_FAC2 = '{34}', BIMU_EXP2 = {35}, BIMU_SEC2 = {36}, BIMU_NAR2 = {37}, " +
                                                        "BIMU_FAC3 = '{38}', BIMU_EXP3 = {39}, BIMU_SEC3 = {40}, BIMU_NAR3 = {41}, BIMU_FAC4 = '{42}', BIMU_EXP4 = {43}, BIMU_SEC4 = {44}, BIMU_NAR4 = {45}, " +
                                                        "BIMU_FAC5 = '{46}', BIMU_EXP5 = {47}, BIMU_SEC5 = {48}, BIMU_NAR5 = {49}, BIMU_FAC6 = '{50}', BIMU_EXP6 = {51}, BIMU_SEC6 = {52}, BIMU_NAR6 = {53} " +
                                               "WHERE   BIMU_ID = {54} ",
                                                        EscapeCharacter(bilMueObj.LLId), bilMueObj.Mob12, bilMueObj.BilGuardedMue, bilMueObj.CreditCard,
                                                        bilMueObj.NetAllowed, bilMueObj.ExistingBilOs, bilMueObj.FdValue, bilMueObj.UnsecuredBil,
                                                        bilMueObj.UnsecuredOther, bilMueObj.CreditCard2, bilMueObj.NetUnsecured, bilMueObj.Available,
                                                        bilMueObj.NetExposure, bilMueObj.NetSecurity, bilMueObj.NetNar,
                                                        bilMueObj.PLExposure, bilMueObj.PLSecurity, bilMueObj.PLNar, bilMueObj.CCExposure, bilMueObj.CCSecurity, bilMueObj.CCNar,
                                                        bilMueObj.OtherBilExposure, bilMueObj.OtherBilSecurity, bilMueObj.OtherBilNar, bilMueObj.OtherPLIPEFLexiExposure,
                                                        bilMueObj.OtherPLIPEFLexiSecurity, bilMueObj.OtherPLIPEFLexiNar, bilMueObj.OtherCCExposure, bilMueObj.OtherCCSecurity,
                                                        bilMueObj.OtherCCNar,
                                                        EscapeCharacter(bilMueObj.Facility1), bilMueObj.Exposure1, bilMueObj.Security1, bilMueObj.Nar1,
                                                        EscapeCharacter(bilMueObj.Facility2), bilMueObj.Exposure2, bilMueObj.Security2, bilMueObj.Nar2,
                                                        EscapeCharacter(bilMueObj.Facility3), bilMueObj.Exposure3, bilMueObj.Security3, bilMueObj.Nar3,
                                                        EscapeCharacter(bilMueObj.Facility4), bilMueObj.Exposure4, bilMueObj.Security4, bilMueObj.Nar4,
                                                        EscapeCharacter(bilMueObj.Facility5), bilMueObj.Exposure5, bilMueObj.Security5, bilMueObj.Nar5,
                                                        EscapeCharacter(bilMueObj.Facility6), bilMueObj.Exposure6, bilMueObj.Security6, bilMueObj.Nar6,
                                                        bilMueObj.Id);

            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (updateRow > 0)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
        #endregion

        #region IsExistsBilMueInfo(Int32 bilMueId)
        /// <summary>
        /// This method provide bilmue exists or not.
        /// </summary>
        /// <param name="llid">Get bilmue Id</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsBilMueInfo(Int32 bilMueId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_bilmue WHERE BIMU_ID = {0}", bilMueId);
            object bilMueObj;
            try
            {
                bilMueObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(bilMueObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region EscapeCharacter
        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                returnMessage = fieldString;
            }
            return returnMessage;
        }
        #endregion
    }
}
