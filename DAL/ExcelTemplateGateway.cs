﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    public class ExcelTemplateGateway
    {
        private DatabaseConnection dbMySQL = null;
        public ExcelTemplateGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public List<ExcelTemplate> GetAllExcelTemplate()
        {
            string mSQL = "SELECT * FROM t_pluss_template";
            return GetListData(mSQL);
        }
        public List<ExcelTemplate> GetAllExcelTemplate(int Id)
        {
            string mSQL = "SELECT * FROM t_pluss_template WHERE ID=" + Id;
            return GetListData(mSQL);
        }
        private List<ExcelTemplate> GetListData(string mSQL)
        {
            List<ExcelTemplate> excelTemplateObjList = new List<ExcelTemplate>();
             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    ExcelTemplate excelTemplateObj = new ExcelTemplate();
                    excelTemplateObj.Id = Convert.ToInt32(xRs["ID"]);
                    excelTemplateObj.SheetText = Convert.ToString(xRs["SHEETTEXT"]);
                    excelTemplateObjList.Add(excelTemplateObj);
                }
                xRs.Close();
            }
            return excelTemplateObjList;
        }
        public int SendData(ExcelTemplate excelTemplateObj)
        {
            if (excelTemplateObj.Id > 0)
            {
                return UpdateExcelTemplate(excelTemplateObj);
            }
            else
            {
                return InsertExcelTemplate(excelTemplateObj);
            }
        }
        private int InsertExcelTemplate(ExcelTemplate excelTemplateObj)
        {
            string mSQL = "INSERT INTO t_pluss_template (SHEETTEXT) VALUES('" + excelTemplateObj.SheetText + "')";
            return dbMySQL.DbExecute(mSQL);
        }
        private int UpdateExcelTemplate(ExcelTemplate excelTemplateObj)
        {
            string mSQL = "UPDATE t_pluss_template SET SHEETTEXT='" + excelTemplateObj.SheetText + "' WHERE ID=" + excelTemplateObj.Id;
            return dbMySQL.DbExecute(mSQL);
        }

    }
}
