﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    public class ApproverGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        public ApproverGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public List<Approver> SelectApproverList()
        {
            List<Approver> approverObjList=new List<Approver>();
            Approver approverObj=null;
            string mSQL = "SELECT USER_ID,USER_NAME,USER_DESIGNATION,USER_EMAILADDRESS FROM t_pluss_user AS U LEFT JOIN t_pluss_usertype AS UT ON U.USER_LEVEL=UT.USLE_ID WHERE U.USER_LEVEL=2 OR U.USER_LEVEL=4";
              var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    approverObj = new Approver();
                    approverObj.Id = Convert.ToInt32(xRs["USER_ID"]);
                    approverObj.Name = Convert.ToString(xRs["USER_NAME"]);
                    approverObj.Designation = Convert.ToString(xRs["USER_DESIGNATION"]);
                    approverObj.EmailAddress = Convert.ToString(xRs["USER_EMAILADDRESS"]);
                    approverObjList.Add(approverObj);
                }
                xRs.Close();
            }
            return approverObjList;
        }
        public Int32 GetExistiongApproverId(Int32 approverId, string productTag)
        {
            Int32 returnValue = 0;
            string mSQL = "SELECT USER_APP_ID FROM t_pluss_userapproverlimit WHERE USER_ID=" + approverId + " AND REPLACE((Upper(PROD_TAG)),' ','')='" + productTag.ToUpper().Trim().Replace(" ","") + "'";
            returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }
        public List<Approver> SelectAllApproverList()
        {
            List<Approver> approverObjList = new List<Approver>();
            Approver approverObj = null;
            //string mSQL = "select * from t_pluss_userapproverlimit " +
            //"left join t_pluss_user on t_pluss_user.USER_ID = t_pluss_userapproverlimit.USER_ID " +
            //"where t_pluss_user.USER_LEVEL = 2 or t_pluss_user.USER_LEVEL = 4";

            string mSQL = @"select * from t_pluss_userapproverlimit 
left join t_pluss_user on t_pluss_user.USER_ID = t_pluss_userapproverlimit.USER_ID 
left join t_pluss_product on t_pluss_product.PROD_ID = t_pluss_userapproverlimit.PROD_ID 
where t_pluss_user.USER_LEVEL = 2 or t_pluss_user.USER_LEVEL = 4 or t_pluss_user.USER_LEVEL = 7";
            //"where t_pluss_user.USER_LEVEL = 1 or t_pluss_user.USER_LEVEL = 2";//done by tarek at 9 nov 2010 instructed by hiru over phone

             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    approverObj = new Approver();
                    approverObj.UserAppID = Convert.ToInt32(xRs["USER_APP_ID"]);
                    approverObj.UserID = Convert.ToInt32(xRs["USER_ID"]);
                    approverObj.UserName = Convert.ToString(xRs["USER_NAME"]);
                    if (xRs["PROD_ID"] != DBNull.Value)
                    {
                        approverObj.ProductId = Convert.ToInt32(xRs["PROD_ID"]);
                    }
                    approverObj.ProductName = Convert.ToString(xRs["PROD_NAME"]);
                    approverObj.ProdTag = Convert.ToString(xRs["PROD_TAG"]);
                    approverObj.Level1Amt = Convert.ToInt32(xRs["LEVEL1_AMT"]);
                    approverObj.Level2Amt = Convert.ToInt32(xRs["LEVEL2_AMT"]);
                    approverObj.Level3Amt = Convert.ToInt32(xRs["LEVEL3_AMT"]);
                    approverObj.ChangDt = Convert.ToDateTime(xRs["CHANG_DT"]);
                    approverObj.ChangBy = Convert.ToInt32(xRs["CHANG_BY"]);
                    approverObj.Status = Convert.ToInt32(xRs["STATUS"]);
                    //approverObj.ProdTagId = Convert.ToInt32(xRs.Fields[""].Value);

                    approverObjList.Add(approverObj);
                }
                xRs.Close();
            }
            return approverObjList;
        }


        public List<Approver> SelectAllApproverList(string key)
        {
            List<Approver> approverObjList = new List<Approver>();
            Approver approverObj = null;
            //string mSQL = "select * from t_pluss_userapproverlimit " +
            //"left join t_pluss_user on t_pluss_user.USER_ID = t_pluss_userapproverlimit.USER_ID " +
            //"where (t_pluss_user.USER_LEVEL = 2 or t_pluss_user.USER_LEVEL = 4) and (" +
            //    //"where (t_pluss_user.USER_LEVEL = 1 or t_pluss_user.USER_LEVEL = 2) and (" +
            //"LEVEL1_AMT='" + key.SpecialCharacterRemover() + "' or LEVEL2_AMT='" + key.SpecialCharacterRemover() + "' or LEVEL3_AMT='" + key.SpecialCharacterRemover() + "' or " +
            //"USER_NAME like ('%" + key.SpecialCharacterRemover() + "%') or PROD_TAG like ('%" + key.SpecialCharacterRemover() + "%'))";

            //string mSQL = "SELECT L.USER_APP_ID,U.USER_ID,U.USER_NAME,L.PROD_TAG,L.LEVEL1_AMT,L.LEVEL2_AMT,L.LEVEL3_AMT,L.CHANG_DT,L.CHANG_BY,L.STATUS" +
            //" FROM t_pluss_user as U,t_pluss_userapproverlimit as L WHERE (U.USER_ID=L.USER_ID AND U.USER_LEVEL IN (2,4,7))" +
            //"AND (U.USER_NAME LIKE '%" + key.SpecialCharacterRemover() + "%' OR LEVEL1_AMT LIKE '%" + key.SpecialCharacterRemover() + "%'" +
            //"OR LEVEL2_AMT LIKE '%" + key.SpecialCharacterRemover() + "%' OR LEVEL3_AMT LIKE '%" + key.SpecialCharacterRemover() + "%')";

            string mSQL = @"SELECT
t_pluss_userapproverlimit.USER_APP_ID,
t_pluss_userapproverlimit.USER_ID,
t_pluss_userapproverlimit.PROD_ID,
t_pluss_userapproverlimit.PROD_TAG,
t_pluss_userapproverlimit.LEVEL1_AMT,
t_pluss_userapproverlimit.LEVEL2_AMT,
t_pluss_userapproverlimit.LEVEL3_AMT,
t_pluss_userapproverlimit.CHANG_DT,
t_pluss_userapproverlimit.CHANG_BY,
t_pluss_userapproverlimit.STATUS,
t_pluss_user.USER_NAME,
t_pluss_product.PROD_NAME
from t_pluss_userapproverlimit 
left join t_pluss_user on t_pluss_user.USER_ID = t_pluss_userapproverlimit.USER_ID 
left join t_pluss_product on t_pluss_product.PROD_ID = t_pluss_userapproverlimit.PROD_ID
where (t_pluss_user.USER_LEVEL = 2 or t_pluss_user.USER_LEVEL = 4 or t_pluss_user.USER_LEVEL = 7)" +
           "AND (t_pluss_user.USER_NAME LIKE '%" + key.SpecialCharacterRemover() + "%' OR LEVEL1_AMT LIKE '%" + key.SpecialCharacterRemover() + "%'" +
           "OR LEVEL2_AMT LIKE '%" + key.SpecialCharacterRemover() + "%' OR LEVEL3_AMT LIKE '%" + key.SpecialCharacterRemover() + "%')";


  var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    approverObj = new Approver();
                    approverObj.UserAppID = Convert.ToInt32(xRs["USER_APP_ID"]);
                    approverObj.UserID = Convert.ToInt32(xRs["USER_ID"]);
                    approverObj.UserName = Convert.ToString(xRs["USER_NAME"]);
                    approverObj.ProdTag = Convert.ToString(xRs["PROD_TAG"]);
                    approverObj.Level1Amt = Convert.ToInt32(xRs["LEVEL1_AMT"]);
                    approverObj.Level2Amt = Convert.ToInt32(xRs["LEVEL2_AMT"]);
                    approverObj.Level3Amt = Convert.ToInt32(xRs["LEVEL3_AMT"]);
                    approverObj.ChangDt = Convert.ToDateTime(xRs["CHANG_DT"]);
                    approverObj.ChangBy = Convert.ToInt32(xRs["CHANG_BY"]);
                    approverObj.Status = Convert.ToInt32(xRs["STATUS"]);
                    if (xRs["PROD_NAME"] != DBNull.Value)
                    {
                        approverObj.ProductName = Convert.ToString(xRs["PROD_NAME"]);
                    }
                    if (xRs["PROD_ID"] != DBNull.Value)
                    {
                        approverObj.ProductId = Convert.ToInt32(xRs["PROD_ID"]);
                    }
                    approverObjList.Add(approverObj);
                }
                xRs.Close();
            }
            return approverObjList;
        }

        public int InsertApprover(Approver approverObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_userapproverlimit ( USER_ID, " +
                        "PROD_TAG,PROD_ID, LEVEL1_AMT, LEVEL2_AMT, LEVEL3_AMT, " +
                        "CHANG_DT, CHANG_BY, STATUS ) VALUES ( " +
                        approverObj.UserID + ", '" + approverObj.ProdTag + "', " + approverObj.ProductId + "," +
                        approverObj.Level1Amt + ", " + approverObj.Level2Amt + ", " +
                        approverObj.Level3Amt + ", '" + approverObj.ChangDt.ToString("yyyy-MM-dd") + "', " +
                        approverObj.ChangBy + ", " + approverObj.Status + " )";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }

            }
            catch
            {
                return ERROR;
            }
        }

        public int UpdateApprover(Approver approverObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_userapproverlimit SET USER_ID = " +
                    approverObj.UserID + ", PROD_TAG = '" +
                    approverObj.ProdTag + "', LEVEL1_AMT = " +
                    approverObj.Level1Amt + ", LEVEL2_AMT = " +
                    approverObj.Level2Amt + ", LEVEL3_AMT = " +
                    approverObj.Level3Amt + ", CHANG_DT = '" +
                    approverObj.ChangDt.ToString("yyyy-MM-dd") + "', CHANG_BY = " +
                    approverObj.ChangBy + ", STATUS = " +
                    approverObj.Status + ", PROD_ID = " +
                    approverObj.ProductId + " WHERE USER_APP_ID = " +
                    approverObj.UserAppID;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch
            {
                return ERROR;
            }
        }

        public Approver GetApproverLimit(Int32 UserId,string productTag)
        {
            Approver approverObj = null;
            string mSQL = "SELECT * from t_pluss_userapproverlimit " +
            "left join t_pluss_user on t_pluss_user.USER_ID = t_pluss_userapproverlimit.USER_ID " +
            "where t_pluss_userapproverlimit.USER_ID=" + UserId + " AND t_pluss_userapproverlimit.PROD_TAG='" + productTag + "'";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    approverObj = new Approver();
                    approverObj.UserAppID = Convert.ToInt32(xRs["USER_APP_ID"]);
                    approverObj.UserID = Convert.ToInt32(xRs["USER_ID"]);
                    approverObj.UserName = Convert.ToString(xRs["USER_NAME"]);
                    approverObj.ProdTag = Convert.ToString(xRs["PROD_TAG"]);
                    approverObj.Level1Amt = Convert.ToInt32(xRs["LEVEL1_AMT"]);
                    approverObj.Level2Amt = Convert.ToInt32(xRs["LEVEL2_AMT"]);
                    approverObj.Level3Amt = Convert.ToInt32(xRs["LEVEL3_AMT"]);
                    approverObj.ChangDt = Convert.ToDateTime(xRs["CHANG_DT"]);
                    approverObj.ChangBy = Convert.ToInt32(xRs["CHANG_BY"]);
                    approverObj.Status = Convert.ToInt32(xRs["STATUS"]);
                }
                xRs.Close();
            }
            return approverObj;
        }
    }
}
