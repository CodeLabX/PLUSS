﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// SectorGateway Class.
    /// </summary>
    public class SectorGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor.
        /// </summary>
        public SectorGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select sector list
        /// </summary>
        /// <returns>Returns a list of Sector</returns>
        public List<Sector> SelectSectorList()
        {
            List<Sector> sectorObjList = new List<Sector>();
            Sector sectorObj = null;
            string mSQL = "SELECT * FROM t_pluss_sector";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    sectorObj = new Sector();
                    sectorObj.Id = Convert.ToInt32(xRs["SECT_ID"]);
                    sectorObj.Name = Convert.ToString(xRs["SECT_NAME"]);
                    sectorObj.Status = Convert.ToInt32("0" + xRs["SECT_STATUS"]);
                    sectorObjList.Add(sectorObj);
                }
                xRs.Close();
            }
            return sectorObjList;
        }
        /// <summary>
        /// This method select sector list
        /// </summary>
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns a list of Sector.</returns>
        public List<Sector> SelectSectorList(string searchKey)
        {
            List<Sector> sectorObjList = new List<Sector>();
            Sector sectorObj = null;
            string mSQL = "SELECT * FROM t_pluss_sector WHERE SECT_ID LIKE '%" + searchKey + "%' OR SECT_NAME LIKE '%" + searchKey + "%'";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    sectorObj = new Sector();
                    sectorObj.Id = Convert.ToInt32(xRs["SECT_ID"]);
                    sectorObj.Name = Convert.ToString(xRs["SECT_NAME"]);
                    sectorObj.Status = Convert.ToInt32("0" + xRs["SECT_STATUS"]);
                    sectorObjList.Add(sectorObj);
                }
                xRs.Close();
            }
            return sectorObjList;
        }
        /// <summary>
        /// This method select sector
        /// </summary>
        /// <param name="sectorId">Gets a sector id.</param>
        /// <returns>Returns a Sector object.</returns>
        public Sector SelectSector(Int64 sectorId)
        {
            Sector sectorObj = new Sector();
            string mSQL = "SELECT * FROM t_pluss_sector WHERE SECT_ID="+sectorId;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                if (xRs.Read())
                {
                    sectorObj.Id = Convert.ToInt32(xRs["SECT_ID"]);
                    sectorObj.Name = Convert.ToString(xRs["SECT_NAME"]);
                    sectorObj.Status = Convert.ToInt32("0" + xRs["SECT_STATUS"]);
                }
                xRs.Close();
            }
            return sectorObj;
        }
        /// <summary>
        /// This method select max scetor id
        /// </summary>
        /// <returns>Returns a max sector id.</returns>
        public Int64 SelectMaxScetorId()
        {
            string mSQL = "SELECT MAX(SECT_ID) FROM t_pluss_sector";
            Int64 returnValue = Convert.ToInt64("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue + 1;
        }
        /// <summary>
        /// This method provide the operation of insertion or update sector info
        /// </summary>
        /// <param name="sectorObj">Gets a Sector object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendSectorInfoInToDB(Sector sectorObj)
        {
            Sector existingSectorObj = SelectSector(sectorObj.Id);
            if (existingSectorObj.Id>0)
            {
                return UpdateSector(sectorObj);
            }
            else
            {
                return InsertSector(sectorObj);
            }
        }
        /// <summary>
        /// This method provide the operation of insertion sector
        /// </summary>
        /// <param name="sectorObj">Gets a Sector object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertSector(Sector sectorObj)
        {
            string queryString = "INSERT INTO t_pluss_sector (SECT_NAME,SECT_STATUS) VALUES('" +
                             AddSlash(sectorObj.Name) + "'," +
                             sectorObj.Status + ")";
            try
            {
              
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide the operation of update sector
        /// </summary>
        /// <param name="sectorObj">Gets a Sector object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateSector(Sector sectorObj)
        {
            string queryString = "UPDATE t_pluss_sector SET " +
                   "SECT_NAME='" + AddSlash(sectorObj.Name) + "'," +
                   "SECT_STATUS=" + sectorObj.Status + " WHERE SECT_ID=" + sectorObj.Id;
            try
            { 

                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method convert the special character.
        /// </summary>
        /// <param name="sMessage">Gets a string</param>
        /// <returns>Returns a converted string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
