﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// LoanApplicationQueueGateway Class.
    /// </summary>
    public class LoanApplicationQueueGateway
    {
        public DatabaseConnection dbMySQL = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        public LoanApplicationQueueGateway()
        {
            dbMySQL = new DatabaseConnection();
        }

        /// <summary>
        /// This method select loan application queue info from pl.
        /// </summary>
        /// <returns>Returns a LoanApplicationQueue list object.</returns>
        public List<LoanApplicationQueue> GetLoanApplicationQueueFromPL()
        {
            List<LoanApplicationQueue> loanApplicationQueue4PLObjList=new List<LoanApplicationQueue>();
            string mSQL = "SELECT * FROM loan_app_info WHERE LOAN_STATU_ID=3";
            ADODB.Recordset xRs = new ADODB.Recordset();
            LoanApplicationQueue loanApplicationQueueObj = null;
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                loanApplicationQueueObj = new LoanApplicationQueue();
                loanApplicationQueueObj.LLId = Convert.ToInt64(xRs.Fields["LOAN_APPLI_ID"].Value);
                loanApplicationQueueObj.CustomerName = AddSlash(Convert.ToString(xRs.Fields["CUST_NM"].Value));
                loanApplicationQueueObj.Status = "Return from Analyst";//Convert.ToString(xRs.Fields["LOAN_STATUS_ID"].Value);
                loanApplicationQueueObj.Remarks = "";//Convert.ToString(xRs.Fields["PL_REMARKS"].Value);
                loanApplicationQueue4PLObjList.Add(loanApplicationQueueObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return loanApplicationQueue4PLObjList;
        }

        /// <summary>
        /// This method select loan application queue from pl
        /// </summary>
        /// <returns>Returns a LoanApplicationQueue list object.</returns>
        public List<LoanApplicationQueue> GetLoanApplicationQueue4PL()
        {
            List<LoanApplicationQueue> loanApplicationQueue4PLObjList = new List<LoanApplicationQueue>();
            string mSQL = "SELECT * FROM loan_app_info WHERE LOAN_STATU_ID=4 AND RCV_DT>='2009-09-30'";
            ADODB.Recordset xRs = new ADODB.Recordset();
            LoanApplicationQueue loanApplicationQueueObj = null;
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                loanApplicationQueueObj = new LoanApplicationQueue();
                loanApplicationQueueObj.LLId = Convert.ToInt64(xRs.Fields["LOAN_APPLI_ID"].Value);
                loanApplicationQueueObj.CustomerName = AddSlash(Convert.ToString(xRs.Fields["CUST_NM"].Value));
                if (IsDate(xRs.Fields["RCV_DT"].Value.ToString()))
                {
                    loanApplicationQueueObj.ReceiveDate = Convert.ToDateTime(xRs.Fields["RCV_DT"].Value);
                }
                loanApplicationQueueObj.Remarks = "";//Convert.ToString(xRs.Fields["PL_REMARKS"].Value);
                loanApplicationQueue4PLObjList.Add(loanApplicationQueueObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return loanApplicationQueue4PLObjList;
        }

        /// <summary>
        /// This method check the date is valid or not.
        /// </summary>
        /// <param name="myDateString">Gets a date.</param>
        /// <returns>Returns true if valid else false.</returns>
        public bool IsDate(string myDateString)
        {
            try
            {
                DateTime dt = Convert.ToDateTime(myDateString, new System.Globalization.CultureInfo("ru-RU"));
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// This method loan application queue from loan locator.
        /// </summary>
        /// <returns>Returns a LoanApplicationQueue list object.</returns>
        public List<LoanApplicationQueue> GetLoanApplicationQueueFromLL()
        {
            List<LoanApplicationQueue> loanApplicationQueue4LLObjList = new List<LoanApplicationQueue>();
            string mSQL = "SELECT * FROM loan_app_info WHERE LOAN_STATUS_ID=8 AND RECE_DATE>='2009-09-30'";
            //string mSQL = "SELECT * FROM loan_app_info WHERE LOAN_STATU_ID=4 AND RCV_DT>='2009-08-30'";
            ADODB.Recordset xRs2 = new ADODB.Recordset();
            LoanApplicationQueue loanApplicationQueueObj = null;
            xRs2 = dbMySQL.DbOpenRset(mSQL, "conStringLoanLocator");
            while (!xRs2.EOF)
            {
                loanApplicationQueueObj = new LoanApplicationQueue();
                loanApplicationQueueObj.LLId = Convert.ToInt64(xRs2.Fields["LOAN_APPL_ID"].Value);
                loanApplicationQueueObj.CustomerName = AddSlash(Convert.ToString(xRs2.Fields["CUST_NAME"].Value));
                loanApplicationQueueObj.Status = "From Loan Locator";//Convert.ToString(xRs.Fields["LOAN_STATUS_ID"].Value);
                loanApplicationQueueObj.Remarks = "";//Convert.ToString(xRs.Fields["PROD_NAME"].Value);
                loanApplicationQueue4LLObjList.Add(loanApplicationQueueObj);
                xRs2.MoveNext();
            }
            xRs2.Close();
            return loanApplicationQueue4LLObjList;
        }

        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
