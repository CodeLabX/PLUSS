﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data;
using System.Data.Common;
using Bat.Common;
using AzUtilities;

namespace DAL
{
    enum InsertUpdateFlag : int
    {
        INSERT = 1,
        UPDATE = 2,
        STATUSUPDATE = 3,
        DELETE = 4,
        INSERTERROR = -1,
        UPDATEERROR = -2,
        STATUSUPDATEERROR = -3,
        DELETEERROR = -4
    }
    /// <summary>
    /// Loan application gateway class.
    /// </summary>
    public class LoanApplicationGateway
    {
        private const int SCBACCNO_LENGTH = 11;
        private LoanLocatorLoanApplicant loanLocatorLoanApplicantObj;
        private LoanInformation loanInformationObj;
        private JoinApplicantInformation joinApplicantInformationObj;
        public DatabaseConnection dbMySQL = null;

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LoanApplicationGateway()
        {
            //Constructor logic here.
            dbMySQL = new DatabaseConnection();
        }
        #endregion

        #region Get All Product
        /// <summary>
        /// This method gives all the product.
        /// </summary>
        /// <returns>Returns product list object.</returns>
        public List<Product> GetAllProduct()
        {
            List<Product> productListObj = new List<Product>();
            DataTable productDataTableObj = new DataTable();
            string queryString = "SELECT PROD_ID, PROD_NAME, PROD_LOAN_TYPE FROM t_pluss_product ORDER BY PROD_NAME ASC";

            try
            {
                productDataTableObj = DbQueryManager.GetTable(queryString);

                if (productDataTableObj != null)
                {
                    DataTableReader productDataTableReaderObj = productDataTableObj.CreateDataReader();
                    if (productDataTableReaderObj != null)
                    {
                        while (productDataTableReaderObj.Read())
                        {
                            Product productObj = new Product();
                            productObj.ProductId = Convert.ToInt32(productDataTableReaderObj["PROD_ID"].ToString());
                            productObj.ProductName = productDataTableReaderObj["PROD_NAME"].ToString();
                            productObj.ProdLoanType = productDataTableReaderObj["PROD_LOAN_TYPE"].ToString();
                            productListObj.Add(productObj);
                        }
                        productDataTableReaderObj.Close();
                    }
                    productDataTableObj.Clear();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                string msg2 = ex.InnerException.Message;
            }
            return productListObj;
        }
        #endregion

        #region Get All Product from Loan Locator
        /// <summary>
        /// This method gives all the product.
        /// </summary>
        /// <returns>Returns product list object.</returns>
        public List<Product> GetAllProductFromLoanLocator()
        {
            List<Product> productListObj = new List<Product>();
            DataTable productDataTableObj = new DataTable();
            string queryString = "SELECT Id, PROD_NAME FROM Product_info ORDER BY PROD_NAME ASC";

            try
            {
                productDataTableObj = DbQueryManager.GetTable(queryString);

                if (productDataTableObj != null)
                {
                    DataTableReader productDataTableReaderObj = productDataTableObj.CreateDataReader();
                    if (productDataTableReaderObj != null)
                    {
                        while (productDataTableReaderObj.Read())
                        {
                            Product productObj = new Product();
                            productObj.ProductId = Convert.ToInt32(productDataTableReaderObj["Id"].ToString());
                            productObj.ProductName = productDataTableReaderObj["PROD_NAME"].ToString();
                            productListObj.Add(productObj);
                        }
                        productDataTableReaderObj.Close();
                    }
                    productDataTableObj.Clear();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                string msg2 = ex.InnerException.Message;
            }
            return productListObj;
        }
        #endregion

        #region Get All Product by Learner Name

        public List<Product> GetAllProductByLearner()
        {
            List<Product> productObj = new List<Product>();
            DataTable productTableObj = new DataTable();
            string queryString = "select p.Id,p.PROD_NAME,l.ID_LEARNER,l.NAME_LEARNER,p.SUPERVISOR from Product_Info p left join learner_details_table l on p.SUPERVISOR=l.ID_LEARNER";
            try
            {
                productTableObj = DbQueryManager.GetTable(queryString);
                if (productTableObj != null)
                {
                    DataTableReader productDataTableReaderObj = productTableObj.CreateDataReader();
                    if (productDataTableReaderObj != null)
                    {
                        while (productDataTableReaderObj.Read())
                        {
                            Product product = new Product();
                            product.ProductId = Convert.ToInt32(productDataTableReaderObj["Id"]);
                            product.ProductName = productDataTableReaderObj["PROD_NAME"].ToString();
                            if (!String.IsNullOrEmpty(productDataTableReaderObj["ID_LEARNER"].ToString()) || Convert.IsDBNull(productDataTableReaderObj["ID_LEARNER"].ToString()))
                            {
                                product.IdLearner = Convert.ToInt32(productDataTableReaderObj["ID_LEARNER"]);
                            }
                            if (!String.IsNullOrEmpty(productDataTableReaderObj["NAME_LEARNER"].ToString()) || Convert.IsDBNull(productDataTableReaderObj["NAME_LEARNER"].ToString()))
                            {
                                product.NameLearner = productDataTableReaderObj["NAME_LEARNER"].ToString();
                            }
                            productObj.Add(product);
                        }
                        productDataTableReaderObj.Close();
                    }
                    productTableObj.Clear();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                string msg2 = ex.InnerException.Message;
            }
            return productObj;
        }



        #endregion
        #region Get All Source
        /// <summary>
        /// This method gives all the souce.
        /// </summary>
        /// <returns>Returns souce list object.</returns>
        public List<Source> GetAllSource()
        {
            List<Source> sourceListObj = new List<Source>();
            DataTable sourceDataTableObj = null;
            string queryString = "SELECT SCSO_ID,SCSO_NAME FROM t_pluss_scbsource ORDER BY SCSO_NAME ASC";

            sourceDataTableObj = DbQueryManager.GetTable(queryString);
            if (sourceDataTableObj != null)
            {
                DataTableReader sourceDataTableReaderObj = sourceDataTableObj.CreateDataReader();
                if (sourceDataTableReaderObj != null)
                {
                    while (sourceDataTableReaderObj.Read())
                    {
                        Source sourceObj = new Source();
                        sourceObj.SourceId = Convert.ToInt32(sourceDataTableReaderObj["SCSO_ID"].ToString());
                        sourceObj.SourceName = sourceDataTableReaderObj["SCSO_NAME"].ToString();
                        sourceListObj.Add(sourceObj);
                    }
                    sourceDataTableReaderObj.Close();
                }
                sourceDataTableObj.Clear();
            }
            return sourceListObj;
        }
        #endregion

        public List<Source> LocGetAllSource()
        {
            List<Source> sourceListObj = new List<Source>();
            DataTable sourceDataTableObj = null;
            string queryString = "select * from loc_branch_info where BR_NAME<>'' and BR_NAME is NOT NULL";

            sourceDataTableObj = DbQueryManager.GetTable(queryString);
            if (sourceDataTableObj != null)
            {
                DataTableReader sourceDataTableReaderObj = sourceDataTableObj.CreateDataReader();
                if (sourceDataTableReaderObj != null)
                {
                    while (sourceDataTableReaderObj.Read())
                    {
                        Source sourceObj = new Source();
                        sourceObj.SourceId = Convert.ToInt32(sourceDataTableReaderObj["BR_ID"].ToString());
                        sourceObj.SourceName = sourceDataTableReaderObj["BR_NAME"].ToString();
                        sourceListObj.Add(sourceObj);
                    }
                    sourceDataTableReaderObj.Close();
                }
                sourceDataTableObj.Clear();
            }
            return sourceListObj;
        }



        #region Get ALL Vechile
        /// <summary>
        /// This method gives all vechiles.
        /// </summary>
        /// <returns>Returns vechile list object.</returns>
        public List<Vechile> GetAllVechile()
        {
            List<Vechile> vechileListObj = new List<Vechile>();
            string queryString = "SELECT VEHIC_ID, ifNull(VEHIC_TYPE,'') AS VEHIC_TYPE FROM loan_app_info " +
                                 " WHERE VEHIC_TYPE !='' ORDER BY VEHIC_TYPE ASC";
            queryString.Replace("'", "''");
            DataTable vechileDataTableObj = DbQueryManager.GetTable(queryString);
            if (vechileDataTableObj != null)
            {
                DataTableReader vechileDataTableReaderObj = vechileDataTableObj.CreateDataReader();
                if (vechileDataTableReaderObj != null)
                {
                    while (vechileDataTableReaderObj.Read())
                    {
                        Vechile vechileObj = new Vechile();
                        vechileObj.VechileId = Convert.ToInt32(vechileDataTableReaderObj["VEHIC_ID"].ToString());
                        vechileObj.VechileType = vechileDataTableReaderObj["VEHIC_TYPE"].ToString();
                        vechileListObj.Add(vechileObj);
                    }
                    vechileDataTableReaderObj.Close();
                }
                vechileDataTableObj.Clear();
            }
            return vechileListObj;
        }
        #endregion

        #region Get All Banks
        /// <summary>
        /// This method provide all banks.
        /// </summary>
        /// <returns>Returns bank information list object</returns>
        public List<BankInformation> GetAllBanks()
        {
            List<BankInformation> bankInformationListObj = new List<BankInformation>();
            string queryString = "SELECT BANK_ID, BANK_NM FROM t_pluss_bank WHERE BANK_STATUS<>0 ORDER BY BANK_NM ASC";
            DataTable bankDataTableObj = DbQueryManager.GetTable(queryString);
            //BankInformation bankInformationObj = new BankInformation();
            //bankInformationObj.Name = "Null";
            //bankInformationObj.Id = 0;
            //bankInformationListObj.Insert(0, bankInformationObj);
            if (bankDataTableObj != null)
            {
                DataTableReader bankDataTableReaderObj = bankDataTableObj.CreateDataReader();
                if (bankDataTableReaderObj != null)
                {
                    while (bankDataTableReaderObj.Read())
                    {
                        BankInformation bankInformationObj = new BankInformation();
                        bankInformationObj.Id = Convert.ToInt32(bankDataTableReaderObj["BANK_ID"].ToString());
                        bankInformationObj.Name = bankDataTableReaderObj["BANK_NM"].ToString();
                        bankInformationListObj.Add(bankInformationObj);
                    }
                    bankDataTableReaderObj.Close();
                }
                //bankDataTableObj.Clear();
            }
            return bankInformationListObj;
        }
        #endregion

        #region Get All Branchs
        /// <summary>
        /// This method provides all branches.
        /// </summary>
        /// <returns>Returns branch information list object.</returns>
        public List<BranchInformation> GetAllBranchs()
        {
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            string queryString = "SELECT BRAN_ID, BRAN_NAME FROM t_pluss_branch ORDER BY BRAN_NAME ASC ";
            DataTable branchDataTableObj = DbQueryManager.GetTable(queryString);
            if (branchDataTableObj != null)
            {
                DataTableReader branchDataTableReaderObj = branchDataTableObj.CreateDataReader();
                if (branchDataTableReaderObj != null)
                {
                    while (branchDataTableReaderObj.Read())
                    {
                        BranchInformation branchInformationObj = new BranchInformation();
                        branchInformationObj.Id = Convert.ToInt32(branchDataTableReaderObj["BRAN_ID"].ToString());
                        branchInformationObj.Name = branchDataTableReaderObj["BRAN_NAME"].ToString();
                        branchInformationListObj.Add(branchInformationObj);
                    }
                    branchDataTableReaderObj.Close();
                }
                branchDataTableObj.Clear();
            }
            return branchInformationListObj;
        }
        #endregion

        #region GetAllBranchs(Int32 bankId)
        /// <summary>
        /// This method provides all branches.
        /// </summary>
        /// <param name="bankId">Gets a bank id.</param>
        /// <returns>Returns branch information list object.</returns>
        public List<BranchInformation> GetAllBranchs(Int32 bankId)
        {
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            string queryString = String.Format("SELECT BRAN_ID, BRAN_NAME, BRAN_BANK_ID FROM t_pluss_branch WHERE BRAN_BANK_ID = {0} ORDER BY BRAN_NAME ASC ", bankId);
            DataTable branchDataTableObj = DbQueryManager.GetTable(queryString);
            if (branchDataTableObj != null)
            {
                DataTableReader branchDataTableReaderObj = branchDataTableObj.CreateDataReader();
                if (branchDataTableReaderObj != null)
                {
                    while (branchDataTableReaderObj.Read())
                    {
                        BranchInformation branchInformationObj = new BranchInformation();
                        branchInformationObj.Id = Convert.ToInt32(branchDataTableReaderObj["BRAN_ID"].ToString());
                        branchInformationObj.Name = branchDataTableReaderObj["BRAN_NAME"].ToString();
                        branchInformationObj.BankId = Convert.ToInt32(branchDataTableReaderObj["BRAN_BANK_ID"].ToString());
                        branchInformationListObj.Add(branchInformationObj);
                    }
                    branchDataTableReaderObj.Close();
                }
                branchDataTableObj.Clear();
            }
            return branchInformationListObj;
        }
        #endregion

        #region  Get All Account Type
        /// <summary>
        /// This method provides all account types.
        /// </summary>
        /// <returns>Returns account type list object.</returns>
        public List<AccountType> GetAllAccountType()
        {
            List<AccountType> accountTypeListObj = new List<AccountType>();
            string queryString = "SELECT ACTY_ID, ACTY_NAME FROM t_pluss_accounttype ORDER BY ACTY_ID ASC";
            DataTable accountTypeDataTableObj = DbQueryManager.GetTable(queryString);
            if (accountTypeDataTableObj != null)
            {
                DataTableReader accountTypeDataTableReaderObj = accountTypeDataTableObj.CreateDataReader();
                if (accountTypeDataTableReaderObj != null)
                {
                    while (accountTypeDataTableReaderObj.Read())
                    {
                        AccountType accountTypeObj = new AccountType();
                        accountTypeObj.Id = Convert.ToInt32(accountTypeDataTableReaderObj["ACTY_ID"].ToString());
                        accountTypeObj.Name = accountTypeDataTableReaderObj["ACTY_NAME"].ToString();
                        accountTypeListObj.Add(accountTypeObj);
                    }
                    accountTypeDataTableReaderObj.Close();
                }
                accountTypeDataTableObj.Clear();
            }
            return accountTypeListObj;
        }
        #endregion

        #region Get All Id Types
        /// <summary>
        /// This method provides all id types.
        /// </summary>
        /// <returns>Returns id type list object.</returns>
        public List<IdType> GetAllIdTypes()
        {
            List<IdType> idTypeListObj = new List<IdType>();
            string queryString = "SELECT IDTY_ID, IDTY_NAME FROM t_pluss_idtype ORDER BY IDTY_ID ASC ";
            DataTable idTypeDataTableObj = DbQueryManager.GetTable(queryString);
            if (idTypeDataTableObj != null)
            {
                DataTableReader idTypeDataTableReaderObj = idTypeDataTableObj.CreateDataReader();
                if (idTypeDataTableReaderObj != null)
                {
                    while (idTypeDataTableReaderObj.Read())
                    {
                        IdType idTypeObj = new IdType();
                        idTypeObj.Id = Convert.ToInt32(idTypeDataTableReaderObj["IDTY_ID"].ToString());
                        idTypeObj.Name = idTypeDataTableReaderObj["IDTY_NAME"].ToString();
                        idTypeListObj.Add(idTypeObj);
                    }
                    idTypeDataTableReaderObj.Close();
                }
                idTypeDataTableObj.Clear();
            }
            return idTypeListObj;
        }
        #endregion

        #region Get All Profession
        /// <summary>
        /// This method provides all professions.
        /// </summary>
        /// <returns>Returns profession list object.</returns>
        public List<Profession> GetAllProfession()
        {
            List<Profession> professionListObj = new List<Profession>();
            string queryString = "SELECT PROF_ID, PROF_NAME FROM t_pluss_profession ORDER BY PROF_ID ASC ";
            DataTable professionDataTableObj = DbQueryManager.GetTable(queryString);
            if (professionDataTableObj != null)
            {
                DataTableReader professionDataTableReaderObj = professionDataTableObj.CreateDataReader();
                if (professionDataTableReaderObj != null)
                {
                    while (professionDataTableReaderObj.Read())
                    {
                        Profession professionObj = new Profession();
                        professionObj.Id = Convert.ToInt32(professionDataTableReaderObj["PROF_ID"].ToString());
                        professionObj.Name = professionDataTableReaderObj["PROF_NAME"].ToString();
                        professionListObj.Add(professionObj);
                    }
                    professionDataTableReaderObj.Close();
                }
                professionDataTableObj.Clear();
            }
            return professionListObj;
        }
        #endregion

        #region Get All Education Level
        /// <summary>
        /// This method provides all Education level.
        /// </summary>
        /// <returns>Returns education level list object.</returns>
        public List<EducationLevel> GetAllEducationLevel()
        {
            List<EducationLevel> educationLevelListObj = new List<EducationLevel>();
            string queryString = "SELECT EL_ID, EL_NAME FROM t_pluss_education_level";
            DataTable educationLevelDataTableObj = DbQueryManager.GetTable(queryString);
            if (educationLevelDataTableObj != null)
            {
                DataTableReader educationLevelDataTableReaderObj = educationLevelDataTableObj.CreateDataReader();
                if (educationLevelDataTableReaderObj != null)
                {
                    while (educationLevelDataTableReaderObj.Read())
                    {
                        EducationLevel educationLevelObj = new EducationLevel();
                        educationLevelObj.Id = Convert.ToInt32(educationLevelDataTableReaderObj["EL_ID"].ToString());
                        educationLevelObj.Name = educationLevelDataTableReaderObj["EL_NAME"].ToString();
                        educationLevelListObj.Add(educationLevelObj);
                    }
                    educationLevelDataTableReaderObj.Close();
                }
                educationLevelDataTableObj.Clear();
            }
            return educationLevelListObj;
        }
        #endregion

        #region GetLoanAppLoanLocatorInfo
        /// <summary>
        /// This method provide particular loan locator info.
        /// </summary>
        /// <param name="llid">Gets loan applicant llid.</param>
        /// <returns>Returns loan Locator aoan applicant object.</returns>

        public LoanLocatorLoanApplicant GetLoanAppLoanLocatorInfo(Int32 llid)
        {
            Bat.Common.Connection.ConnectionStringName = "conStringLoanLocator";

            string queryString = String.Format(@"
                        SELECT 
	                        Isnull(a.arm_code, '') AS ARM_CODE,
                            a.loan_appl_id,
	                        Isnull(( CASE a.rece_date
                            WHEN '' THEN ''
		                        WHEN '' THEN ''
                                ELSE Cast(a.rece_date AS CHAR)
                            END ), '')  AS RECE_DATE,
                            a.prod_id,
                            a.cust_name,
                            a.loan_app_amou,
                            a.br_id,
                            Isnull(a.empl_type, 'Null') AS EMPL_TYPE,
                            a.organ,
                            a.acc_numb,
                            c.User_Name AS User_Name,
	                        a.loan_status_id,

                            ISNULL(b.RefType,'') AS RefType,
	                        ISNULL(b.RefrenceID,'') AS RefrenceID, 
	                        ISNULL(b.MasterNo,'') AS MasterNo, 
	                        ISNULL(b.RelationshipNo,'') AS RelationshipNo, 
	                        ISNULL(b.FullName,'') AS FullName, 
	                        ISNULL(b.FatherName,'') AS FatherName, 
	                        ISNULL(b.MotherName,'') AS MotherName, 
	                        ISNULL(b.DOB,'') AS DOB, 
	                        ISNULL(b.HighestEduLevel,'') AS HighestEduLevel, 
	                        ISNULL(b.AddressRes,'') AS AddressRes, 
	                        ISNULL(b.AddressRes_PostalCode,'')AS AddressRes_PostalCode, 
	                        ISNULL(b.AddressRes_CityCode,'') AS AddressRes_CityCode, 
	                        ISNULL(b.AddressRes_CityName,'') AS AddressRes_CityName, 
	                        ISNULL(b.AddressRes_CountryCode,'') AS AddressRes_CountryCode, 
	                        ISNULL(b.AddressOff,'') AS AddressOff, 
	                        ISNULL(b.AddressOff_PostalCode,'') AS AddressOff_PostalCode, 
	                        ISNULL(b.AddressOff_CityCode,'') AS AddressOff_CityCode, 
	                        ISNULL(b.AddressOff_CityName,'') AS AddressOff_CityName, 
	                        ISNULL(b.AddressOff_CountryCode,'') AS AddressOff_CountryCode, 
	                        ISNULL(b.ProfessionCode,'') AS ProfessionCode, 
	                        ISNULL(b.OrganisationName,'') AS OrganisationName, 
	                        ISNULL(b.Department ,'' ) AS Department, 
	                        ISNULL(b.TIN,'') AS TIN, 
	                        ISNULL(b.ContactNo1,'') AS ContactNo1, 
	                        ISNULL(b.ContactNo2,'') AS ContactNo2 ,
	                        ISNULL(b.MaritalStatus,'') AS MaritalStatus, 
	                        ISNULL(b.Gender,'') AS Gender, 
	                        ISNULL(b.SpouseName,'') AS SpouseName,
                            ISNULL(PassportNo,'') AS PassportNo, 
                            ISNULL(PassportExp,'') as PassportExp

                        FROM loc_loan_app_info a
	                        left join CustomerBulkData b on replace(b.RefrenceID,'-','')= replace(a.ACC_NUMB,'-','')
	                        left join t_pluss_user c on c.USER_CODE=a.MakerID

                        WHERE a.LOAN_APPL_ID = {0} 
                        	AND a.LOAN_STATUS_ID IN (4,8,10,11)", llid);
            LogFile.WriteLine(queryString);
            //DbDataReader loanLocatorLoanAppDataReaderObj = DbQueryManager.ExecuteReader(queryString, "conStringLoanLocator");
            DbDataReader loanLocatorLoanAppDataReaderObj = DbQueryManager.ExecuteReader(queryString);

            if (loanLocatorLoanAppDataReaderObj != null)
            {
                while (loanLocatorLoanAppDataReaderObj.Read())
                {
                    loanLocatorLoanApplicantObj = new LoanLocatorLoanApplicant();
                    loanLocatorLoanApplicantObj.LLID = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["LOAN_APPL_ID"].ToString());
                    loanLocatorLoanApplicantObj.ApplicantName = Convert.ToString(loanLocatorLoanAppDataReaderObj["CUST_NAME"]);
                    loanLocatorLoanApplicantObj.ProductId = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["PROD_ID"].ToString());
                    loanLocatorLoanApplicantObj.BranchId = Convert.ToInt32(loanLocatorLoanAppDataReaderObj["BR_ID"].ToString());
                    loanLocatorLoanApplicantObj.EmployeType = Convert.ToString(loanLocatorLoanAppDataReaderObj["EMPL_TYPE"]);
                    loanLocatorLoanApplicantObj.Organization = Convert.ToString(loanLocatorLoanAppDataReaderObj["ORGAN"]);
                    loanLocatorLoanApplicantObj.AccountNo = Convert.ToString(loanLocatorLoanAppDataReaderObj["ACC_NUMB"]);
                    loanLocatorLoanApplicantObj.LoanApplyAmount = Convert.ToDouble(loanLocatorLoanAppDataReaderObj["LOAN_APP_AMOU"].ToString());
                    loanLocatorLoanApplicantObj.RecieveDate = Convert.ToDateTime(loanLocatorLoanAppDataReaderObj["RECE_DATE"]);
                    loanLocatorLoanApplicantObj.SourcePersonName = Convert.ToString(loanLocatorLoanAppDataReaderObj["User_Name"]);
                    loanLocatorLoanApplicantObj.ARMCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["ARM_CODE"]);
                    loanLocatorLoanApplicantObj.LoanStatus = Convert.ToString(loanLocatorLoanAppDataReaderObj["LOAN_STATUS_ID"]);



                    loanLocatorLoanApplicantObj.CustBulkData.RefType = Convert.ToString(loanLocatorLoanAppDataReaderObj["RefType"]);
                    loanLocatorLoanApplicantObj.CustBulkData.RefrenceID = Convert.ToString(loanLocatorLoanAppDataReaderObj["RefrenceID"]);
                    loanLocatorLoanApplicantObj.CustBulkData.MasterNo = Convert.ToString(loanLocatorLoanAppDataReaderObj["MasterNo"]);
                    loanLocatorLoanApplicantObj.CustBulkData.RelationshipNo = Convert.ToString(loanLocatorLoanAppDataReaderObj["RelationshipNo"]);
                    loanLocatorLoanApplicantObj.CustBulkData.FullName = Convert.ToString(loanLocatorLoanAppDataReaderObj["FullName"]);
                    loanLocatorLoanApplicantObj.CustBulkData.FatherName = Convert.ToString(loanLocatorLoanAppDataReaderObj["FatherName"]);
                    loanLocatorLoanApplicantObj.CustBulkData.MotherName = Convert.ToString(loanLocatorLoanAppDataReaderObj["MotherName"]);
                    loanLocatorLoanApplicantObj.CustBulkData.DOB = Convert.ToString(loanLocatorLoanAppDataReaderObj["DOB"]);
                    loanLocatorLoanApplicantObj.CustBulkData.HighestEduLevel = Convert.ToString(loanLocatorLoanAppDataReaderObj["HighestEduLevel"]);
                    // Address Res
                    loanLocatorLoanApplicantObj.CustBulkData.AddressRes = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressRes"]);
                    loanLocatorLoanApplicantObj.CustBulkData.AddressRes_PostalCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressRes_PostalCode"]);
                    loanLocatorLoanApplicantObj.CustBulkData.AddressRes_CityCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressRes_CityCode"]);
                    loanLocatorLoanApplicantObj.CustBulkData.AddressRes_CityName = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressRes_CityName"]);
                    loanLocatorLoanApplicantObj.CustBulkData.AddressRes_CountryCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressRes_CountryCode"]);
                    // Address Off
                    loanLocatorLoanApplicantObj.CustBulkData.AddressOff = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressOff"]);
                    loanLocatorLoanApplicantObj.CustBulkData.AddressOff_PostalCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressOff_PostalCode"]);
                    loanLocatorLoanApplicantObj.CustBulkData.AddressOff_CityCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressOff_CityCode"]);
                    loanLocatorLoanApplicantObj.CustBulkData.AddressOff_CityName = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressOff_CityName"]);
                    loanLocatorLoanApplicantObj.CustBulkData.AddressOff_CountryCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["AddressOff_CountryCode"]);

                    loanLocatorLoanApplicantObj.CustBulkData.ProfessionCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["ProfessionCode"]);
                    loanLocatorLoanApplicantObj.CustBulkData.OrganisationName = Convert.ToString(loanLocatorLoanAppDataReaderObj["OrganisationName"]);
                    loanLocatorLoanApplicantObj.CustBulkData.Department = Convert.ToString(loanLocatorLoanAppDataReaderObj["Department"]);
                    loanLocatorLoanApplicantObj.CustBulkData.TIN = Convert.ToString(loanLocatorLoanAppDataReaderObj["TIN"]);
                    loanLocatorLoanApplicantObj.CustBulkData.ContactNo1 = Convert.ToString(loanLocatorLoanAppDataReaderObj["ContactNo1"]);
                    loanLocatorLoanApplicantObj.CustBulkData.ContactNo2 = Convert.ToString(loanLocatorLoanAppDataReaderObj["ContactNo2"]);
                    loanLocatorLoanApplicantObj.CustBulkData.MaritalStatus = Convert.ToString(loanLocatorLoanAppDataReaderObj["MaritalStatus"]);
                    loanLocatorLoanApplicantObj.CustBulkData.Gender = Convert.ToString(loanLocatorLoanAppDataReaderObj["Gender"]);
                    loanLocatorLoanApplicantObj.CustBulkData.SpouseName = Convert.ToString(loanLocatorLoanAppDataReaderObj["SpouseName"]);
                    loanLocatorLoanApplicantObj.CustBulkData.PassportNo = Convert.ToString(loanLocatorLoanAppDataReaderObj["PassportNo"]);
                    loanLocatorLoanApplicantObj.CustBulkData.PassportExp = Convert.ToString(loanLocatorLoanAppDataReaderObj["PassportExp"]);


                }
                loanLocatorLoanAppDataReaderObj.Close();
                //Bat.Common.Connection.GetConnection("conStringLoanLocator").Close();
            }
            Int32[] productIdArray = { 5, 6, 7, 8, 9, 10, 14, 17, 18 };
            string[] processStatus = { "10", "11" };
            try
            {
                if (loanLocatorLoanAppDataReaderObj != null)
                {
                    if (productIdArray.Contains(loanLocatorLoanApplicantObj.ProductId))
                    {
                        return loanLocatorLoanApplicantObj;
                    }
                    else
                    {
                        if (processStatus.Contains(loanLocatorLoanApplicantObj.LoanStatus))
                        {
                            return loanLocatorLoanApplicantObj;
                        }
                        else
                        {
                            loanLocatorLoanApplicantObj = null;
                            return loanLocatorLoanApplicantObj;
                        }
                    }
                }
                else
                {
                    loanLocatorLoanApplicantObj = null;
                    return loanLocatorLoanApplicantObj;
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLine(ex.Message);
                loanLocatorLoanApplicantObj = null;
                return loanLocatorLoanApplicantObj;
            }
        }
        #endregion

        #region GetLoanInformation
        /// <summary>
        /// This method provides applicant loan information.
        /// </summary>
        /// <param name="llid">Gets llid</param>
        /// <returns>Return loan information object.</returns>
        public LoanInformation GetLoanInformation(int llid)
        {
            DataTable loanInfoDataTableObj = new DataTable();
            string queryString = String.Format(@"SELECT 
	LOAN_APPLI_ID, 
	loan_app_info.PROD_ID, 
	BR_ID, 
	RCV_DT, 
	CUST_NM, 
	LOAN_APP_AMT, 
	ACC_NO, 
	LOAP_ACNOFORSCB2, 
	LOAP_ACNOFORSCB3, 
	LOAP_ACNOFORSCB4, 
	LOAP_ACNOFORSCB5, 
	TIN_NO, 
	ISNULL(CR_CARD_NO_SCB,'') AS CR_CARD_NO_SCB, 
	ISNULL(LOAP_CREDITCARDNO2,'') AS LOAP_CREDITCARDNO2, 
	ISNULL(LOAP_CREDITCARDNO3,'') AS LOAP_CREDITCARDNO3, 
	ISNULL(LOAP_CREDITCARDNO4,'') AS LOAP_CREDITCARDNO4,
	ISNULL(LOAP_CREDITCARDNO5,'')AS LOAP_CREDITCARDNO5, 
	loan_app_info.FATHE_NM, 
	loan_app_info.MOTHE_NM,
	Format(loan_app_info.DOB,'MM-dd-yyyy') as DOB,
    MARIT_STATU, 
	ISNULL((CASE HIGH_EDU_LEVEL WHEN '' THEN '' ELSE HIGH_EDU_LEVEL END),'') AS HIGH_EDU_LEVEL,
	ISNULL((CASE HOME_OWNER WHEN '' THEN 'Others' WHEN 'Own' THEN 'Own' WHEN 'Family Owned' THEN 'Family Owned' WHEN 'Others' THEN 'Others' ELSE 'Others' END), 'Others') AS HOME_OWNER, 
	RES_ADD, 
	MOBIL, 
	RES_PH,
	OFF_PH,
	LOAN_CONTACTNO4,
	LOAN_CONTACTNO5, 
	ISNULL((CASE loan_app_info.PROFE WHEN '' THEN '' ELSE loan_app_info.PROFE END),'') AS PROFE, 
	COMPA_NM, 
	OFF_ADD, 
	MON_CURRE_JOB, 
    ISNULL((CASE SPOUS_PROFE WHEN '' THEN '' ELSE SPOUS_PROFE END),'') AS SPOUS_PROFE, 
	ISNULL(OTHER_ACC_BANK_ID_1, 0) AS OTHER_ACC_BANK_ID_1, 
	ISNULL(OTHER_ACC_BR_ID_1, 0) AS OTHER_ACC_BR_ID_1, 
    OTHER_ACC_NO_1, 
	ISNULL((CASE OTHER_ACC_TYPE_1 WHEN '' THEN '' ELSE OTHER_ACC_TYPE_1 END),'') AS OTHER_ACC_TYPE_1, 
	ISNULL(OTHER_ACC_BANK_ID_2, 0) AS OTHER_ACC_BANK_ID_2, 
	ISNULL(OTHER_ACC_BR_ID_2,0) AS OTHER_ACC_BR_ID_2, OTHER_ACC_NO_2, 
	ISNULL((CASE OTHER_ACC_TYPE_2 WHEN '' THEN '' ELSE OTHER_ACC_TYPE_2 END),'') AS OTHER_ACC_TYPE_2, OTHER_ACC_BANK_ID_3, 
    ISNULL(OTHER_ACC_BR_ID_3,0) AS OTHER_ACC_BR_ID_3, OTHER_ACC_NO_3, 
	ISNULL((CASE OTHER_ACC_TYPE_3 WHEN '' THEN '' ELSE OTHER_ACC_TYPE_3 END),'') AS OTHER_ACC_TYPE_3, 
	ISNULL(LOAP_OTHERACWITHBANK_ID4, 0) AS LOAP_OTHERACWITHBANK_ID4, 
	ISNULL(LOAP_OTHERACWITHBRANCH_ID4, 0) AS LOAP_OTHERACWITHBRANCH_ID4, LOAP_OTHERACWITHACNO4, 
	ISNULL((CASE LOAP_ORHREACTYPE_ID4 WHEN '' THEN '' ELSE LOAP_ORHREACTYPE_ID4 END),'') AS  LOAP_ORHREACTYPE_ID4, 
    ISNULL(LOAP_OTHERACWITHBANK_ID5,0) AS LOAP_OTHERACWITHBANK_ID5, 
	ISNULL(LOAP_OTHERACWITHBRANCH_ID5,0) AS LOAP_OTHERACWITHBRANCH_ID5, 
	LOAP_OTHERACWITHACNO5, 
	ISNULL((CASE LOAP_ORHREACTYPE_ID5 WHEN '' THEN '' ELSE LOAP_ORHREACTYPE_ID5 END),'') AS  LOAP_ORHREACTYPE_ID5, 
	ISNULL((CASE loan_app_info.ID_TYPE WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE loan_app_info.ID_TYPE END),'') AS ID_TYPE, 
	loan_app_info.ID_NO,ID_EXPIR , 
	ISNULL((CASE LOAP_IDTYPE2 WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE LOAP_IDTYPE2 END),'') AS LOAP_IDTYPE2, 
	LOAP_IDNO2, 
	LOAP_EXPIRYDATE2, 
	ISNULL((CASE LOAP_IDTYPE3 WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE LOAP_IDTYPE3 END),'') AS LOAP_IDTYPE3,
    LOAP_IDNO3, 
	LOAP_EXPIRYDATE3, 
	ISNULL((CASE LOAP_IDTYPE4 WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE LOAP_IDTYPE4 END),'') AS LOAP_IDTYPE4, 
	LOAP_IDNO4, 
	LOAP_EXPIRYDATE4, 
    ISNULL((CASE LOAP_IDTYPE5 WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE LOAP_IDTYPE5 END),'') AS LOAP_IDTYPE5, 
	LOAP_IDNO5, 
	LOAP_EXPIRYDATE5, 
    VEHIC_ID, 
	VEHIC_TYPE,  
    LOAP_SOURCE_PERSON_NAME,	
	ISNULL(LOAP_DECLARED_INCOME,0) AS LOAP_DECLARED_INCOME, 
	LOAP_EXTRA_FIELD1, 
	LOAP_EXTRA_FIELD2, 
	LOAP_EXTRA_FIELD3, 
	LOAP_EXTRA_FIELD4, 
	LOAP_EXTRA_FIELD5, 
	LOAP_EXTRA_FIELD6, 
	LOAP_EXTRA_FIELD7, 
	LOAP_EXTRA_FIELD8, 
	LOAP_EXTRA_FIELD9, 
	LOAP_EXTRA_FIELD10, 
	ISNULL((CASE LOAN_STATU_ID WHEN '' THEN 1 ELSE LOAN_STATU_ID END),1) AS LOAN_STATU_ID,
	LOAP_USERID,
	de_dup_info_decline_temp.REMAR,
	de_dup_info_decline_temp.DECLI_REASO,
	de_dup_info_decline_temp.MultiLocatorRemarksID,
	b.USER_LEVELCODE,
    ISNULL(ARM_CODE,'') AS ARM_CODE
                                
	FROM loan_app_info 
		LEFT JOIN t_pluss_user b on loan_app_info.LOAP_USERID= b.USER_ID
		left outer join de_dup_info_decline_temp on de_dup_info_decline_temp.DEDU_LOANACCOUNTNO = loan_app_info.LOAN_APPLI_ID WHERE LOAN_APPLI_ID = {0} and LOAN_STATU_ID NOT IN (8,10,11)", llid);
            //DbDataReader loanInformationDataReaderObj = DbQueryManager.ExecuteReader(queryString, "conString");
            LogFile.WriteLine("GetLoanInformation - " + queryString);
            loanInfoDataTableObj = DbQueryManager.GetTable(queryString);
            if (loanInfoDataTableObj != null)
            {
                DataTableReader loanInformationDataReaderObj = loanInfoDataTableObj.CreateDataReader();
                if (loanInformationDataReaderObj != null)
                {
                    while (loanInformationDataReaderObj.Read())
                    {
                        loanInformationObj = new LoanInformation();


                        loanInformationObj.ARMCode = Convert.ToString(loanInformationDataReaderObj["ARM_CODE"]);
                        loanInformationObj.LLId = Convert.ToInt32(loanInformationDataReaderObj["LOAN_APPLI_ID"].ToString());
                        loanInformationObj.ProductId = Convert.ToInt32(loanInformationDataReaderObj["PROD_ID"].ToString());
                        loanInformationObj.SourceId = Convert.ToInt32(loanInformationDataReaderObj["BR_ID"].ToString());
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["RCV_DT"].ToString()))
                        {
                            loanInformationObj.RecieveDate = Convert.ToDateTime(loanInformationDataReaderObj["RCV_DT"].ToString());
                        }
                        //loanInformationObj.RecieveDate = Convert.ToDateTime(loanInformationDataReaderObj["RCV_DT"].ToString());
                        loanInformationObj.ApplicantName = Convert.ToString(loanInformationDataReaderObj["CUST_NM"]);
                        loanInformationObj.AppliedAmount = Convert.ToDecimal("15000000");
                        loanInformationObj.AppliedAmount = Convert.ToDecimal("15000000");
                        string sstr = loanInformationDataReaderObj["LOAN_APP_AMT"].ToString();
                        loanInformationObj.AppliedAmount = Convert.ToDecimal(sstr);

                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["ACC_NO"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo1 = "";
                            loanInformationObj.AccountNo.MasterAccNo1 = "";
                            loanInformationObj.AccountNo.PostAccNo1 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["ACC_NO"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo1 = Convert.ToString(loanInformationDataReaderObj["ACC_NO"]);
                                string preAccNo1 = accountNo1.Substring(0, 2).ToString();
                                string masterAccNo1 = accountNo1.Substring(2, 7).ToString();
                                string postAccNo1 = accountNo1.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo1 = preAccNo1;
                                loanInformationObj.AccountNo.MasterAccNo1 = masterAccNo1;
                                loanInformationObj.AccountNo.PostAccNo1 = postAccNo1;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo1 = "";
                                loanInformationObj.AccountNo.MasterAccNo1 = loanInformationDataReaderObj["ACC_NO"].ToString();
                                loanInformationObj.AccountNo.PostAccNo1 = "";
                            }
                        }

                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_ACNOFORSCB2"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo2 = "";
                            loanInformationObj.AccountNo.MasterAccNo2 = "";
                            loanInformationObj.AccountNo.PostAccNo2 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["LOAP_ACNOFORSCB2"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo2 = Convert.ToString(loanInformationDataReaderObj["LOAP_ACNOFORSCB2"]);
                                string preAccNo2 = accountNo2.Substring(0, 2).ToString();
                                string masterAccNo2 = accountNo2.Substring(2, 7).ToString();
                                string postAccNo2 = accountNo2.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo2 = preAccNo2;
                                loanInformationObj.AccountNo.MasterAccNo2 = masterAccNo2;
                                loanInformationObj.AccountNo.PostAccNo2 = postAccNo2;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo2 = "";
                                loanInformationObj.AccountNo.MasterAccNo2 = loanInformationDataReaderObj["LOAP_ACNOFORSCB2"].ToString();
                                loanInformationObj.AccountNo.PostAccNo2 = "";
                            }
                        }
                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_ACNOFORSCB3"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo3 = "";
                            loanInformationObj.AccountNo.MasterAccNo3 = "";
                            loanInformationObj.AccountNo.PostAccNo3 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["LOAP_ACNOFORSCB3"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo3 = Convert.ToString(loanInformationDataReaderObj["LOAP_ACNOFORSCB3"]);
                                string preAccNo3 = accountNo3.Substring(0, 2).ToString();
                                string masterAccNo3 = accountNo3.Substring(2, 7).ToString();
                                string postAccNo3 = accountNo3.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo3 = preAccNo3;
                                loanInformationObj.AccountNo.MasterAccNo3 = masterAccNo3;
                                loanInformationObj.AccountNo.PostAccNo3 = postAccNo3;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo3 = "";
                                loanInformationObj.AccountNo.MasterAccNo3 = loanInformationDataReaderObj["LOAP_ACNOFORSCB3"].ToString();
                                loanInformationObj.AccountNo.PostAccNo3 = "";
                            }
                        }
                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_ACNOFORSCB4"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo4 = "";
                            loanInformationObj.AccountNo.MasterAccNo4 = "";
                            loanInformationObj.AccountNo.PostAccNo4 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["LOAP_ACNOFORSCB4"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo4 = Convert.ToString(loanInformationDataReaderObj["LOAP_ACNOFORSCB4"]);
                                string preAccNo4 = accountNo4.Substring(0, 2).ToString();
                                string masterAccNo4 = accountNo4.Substring(2, 7).ToString();
                                string postAccNo4 = accountNo4.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo4 = preAccNo4;
                                loanInformationObj.AccountNo.MasterAccNo4 = masterAccNo4;
                                loanInformationObj.AccountNo.PostAccNo4 = postAccNo4;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo4 = "";
                                loanInformationObj.AccountNo.MasterAccNo4 = loanInformationDataReaderObj["LOAP_ACNOFORSCB4"].ToString();
                                loanInformationObj.AccountNo.PostAccNo4 = "";
                            }
                        }
                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_ACNOFORSCB5"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo5 = "";
                            loanInformationObj.AccountNo.MasterAccNo5 = "";
                            loanInformationObj.AccountNo.PostAccNo5 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["LOAP_ACNOFORSCB5"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo5 = Convert.ToString(loanInformationDataReaderObj["LOAP_ACNOFORSCB5"]);
                                string preAccNo5 = accountNo5.Substring(0, 2).ToString();
                                string masterAccNo5 = accountNo5.Substring(2, 7).ToString();
                                string postAccNo5 = accountNo5.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo5 = preAccNo5;
                                loanInformationObj.AccountNo.MasterAccNo5 = masterAccNo5;
                                loanInformationObj.AccountNo.PostAccNo5 = postAccNo5;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo5 = "";
                                loanInformationObj.AccountNo.MasterAccNo5 = loanInformationDataReaderObj["LOAP_ACNOFORSCB5"].ToString();
                                loanInformationObj.AccountNo.PostAccNo5 = "";
                            }
                        }

                        loanInformationObj.TINNo = Convert.ToString(loanInformationDataReaderObj["TIN_NO"]);
                        loanInformationObj.CreditCardNo1 = Convert.ToString(loanInformationDataReaderObj["CR_CARD_NO_SCB"]);
                        loanInformationObj.CreditCardNo2 = Convert.ToString(loanInformationDataReaderObj["LOAP_CREDITCARDNO2"]);
                        loanInformationObj.CreditCardNo3 = Convert.ToString(loanInformationDataReaderObj["LOAP_CREDITCARDNO3"]);
                        loanInformationObj.CreditCardNo4 = Convert.ToString(loanInformationDataReaderObj["LOAP_CREDITCARDNO4"]);
                        loanInformationObj.CreditCardNo5 = Convert.ToString(loanInformationDataReaderObj["LOAP_CREDITCARDNO5"]);
                        loanInformationObj.FatherName = Convert.ToString(loanInformationDataReaderObj["FATHE_NM"]);
                        loanInformationObj.MotherName = Convert.ToString(loanInformationDataReaderObj["MOTHE_NM"]);
                        loanInformationObj.REMAR = Convert.ToString(loanInformationDataReaderObj["REMAR"]);
                        loanInformationObj.DECLI_REASO = Convert.ToString(loanInformationDataReaderObj["DECLI_REASO"]);
                        loanInformationObj.MultiLocatorRemarksID = Convert.ToString(loanInformationDataReaderObj["MultiLocatorRemarksID"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["DOB"].ToString()))
                        {
                            loanInformationObj.DOB = Convert.ToDateTime(loanInformationDataReaderObj["DOB"]);
                        }
                        loanInformationObj.EducationalLevel = Convert.ToString(loanInformationDataReaderObj["HIGH_EDU_LEVEL"]);
                        loanInformationObj.MaritalStatus = Convert.ToString(loanInformationDataReaderObj["MARIT_STATU"]);
                        loanInformationObj.ResidentialStatus = Convert.ToString(loanInformationDataReaderObj["HOME_OWNER"]);
                        loanInformationObj.ResidentAddress = Convert.ToString(loanInformationDataReaderObj["RES_ADD"]);
                        loanInformationObj.ContactNumber.ContactNo1 = Convert.ToString(loanInformationDataReaderObj["MOBIL"]);
                        loanInformationObj.ContactNumber.ContactNo2 = Convert.ToString(loanInformationDataReaderObj["RES_PH"]);
                        loanInformationObj.ContactNumber.ContactNo3 = Convert.ToString(loanInformationDataReaderObj["OFF_PH"]);
                        loanInformationObj.ContactNumber.ContactNo4 = Convert.ToString(loanInformationDataReaderObj["LOAN_CONTACTNO4"]);
                        loanInformationObj.ContactNumber.ContactNo5 = Convert.ToString(loanInformationDataReaderObj["LOAN_CONTACTNO5"]);
                        loanInformationObj.Profession = Convert.ToString(loanInformationDataReaderObj["PROFE"]);
                        loanInformationObj.Business = Convert.ToString(loanInformationDataReaderObj["COMPA_NM"]);
                        loanInformationObj.OfficeAddress = Convert.ToString(loanInformationDataReaderObj["OFF_ADD"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["MON_CURRE_JOB"].ToString()))
                        {
                            loanInformationObj.JobDuration = Convert.ToInt32(loanInformationDataReaderObj["MON_CURRE_JOB"]);
                        }
                        loanInformationObj.SpousOcupation = Convert.ToString(loanInformationDataReaderObj["SPOUS_PROFE"]);

                        loanInformationObj.OtherBankAccount.OtherBankId1 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_1"].ToString());
                        loanInformationObj.OtherBankAccount.OtherBranchId1 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BR_ID_1"].ToString());
                        loanInformationObj.OtherBankAccount.OtherAccNo1 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_NO_1"]);
                        loanInformationObj.OtherBankAccount.OtherAccType1 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_TYPE_1"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_2"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBankId2 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_2"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["OTHER_ACC_BR_ID_2"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBranchId2 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BR_ID_2"].ToString());
                        }
                        loanInformationObj.OtherBankAccount.OtherAccNo2 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_NO_2"]);
                        loanInformationObj.OtherBankAccount.OtherAccType2 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_TYPE_2"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_3"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBankId3 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_3"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["OTHER_ACC_BR_ID_3"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBranchId3 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BR_ID_3"].ToString());
                        }
                        loanInformationObj.OtherBankAccount.OtherAccNo3 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_NO_3"]);
                        loanInformationObj.OtherBankAccount.OtherAccType3 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_TYPE_3"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_OTHERACWITHBANK_ID4"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBankId4 = Convert.ToInt32(loanInformationDataReaderObj["LOAP_OTHERACWITHBANK_ID4"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_OTHERACWITHBRANCH_ID4"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBranchId4 = Convert.ToInt32(loanInformationDataReaderObj["LOAP_OTHERACWITHBRANCH_ID4"].ToString());
                        }
                        loanInformationObj.OtherBankAccount.OtherAccNo4 = Convert.ToString(loanInformationDataReaderObj["LOAP_OTHERACWITHACNO4"]);
                        loanInformationObj.OtherBankAccount.OtherAccType4 = Convert.ToString(loanInformationDataReaderObj["LOAP_ORHREACTYPE_ID4"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_OTHERACWITHBANK_ID5"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBankId5 = Convert.ToInt32(loanInformationDataReaderObj["LOAP_OTHERACWITHBANK_ID5"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_OTHERACWITHBRANCH_ID5"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBranchId5 = Convert.ToInt32(loanInformationDataReaderObj["LOAP_OTHERACWITHBRANCH_ID5"].ToString());
                        }

                        loanInformationObj.OtherBankAccount.OtherAccNo5 = Convert.ToString(loanInformationDataReaderObj["LOAP_OTHERACWITHACNO5"]);
                        loanInformationObj.OtherBankAccount.OtherAccType5 = Convert.ToString(loanInformationDataReaderObj["LOAP_ORHREACTYPE_ID5"]);

                        loanInformationObj.ApplicantId.Type1 = Convert.ToString(loanInformationDataReaderObj["ID_TYPE"]);
                        loanInformationObj.ApplicantId.Id1 = Convert.ToString(loanInformationDataReaderObj["ID_NO"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["ID_EXPIR"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate1 = Convert.ToDateTime(loanInformationDataReaderObj["ID_EXPIR"].ToString());
                        }

                        loanInformationObj.ApplicantId.Type2 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDTYPE2"]);
                        loanInformationObj.ApplicantId.Id2 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDNO2"]);
                        //loanInformationObj.ApplicantId.ExpiryDate2 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE2"].ToString()); 
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_EXPIRYDATE2"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate2 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE2"].ToString());
                        }

                        loanInformationObj.ApplicantId.Type3 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDTYPE3"]);
                        loanInformationObj.ApplicantId.Id3 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDNO3"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_EXPIRYDATE3"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate3 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE3"].ToString());
                        }

                        loanInformationObj.ApplicantId.Type4 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDTYPE4"]);
                        loanInformationObj.ApplicantId.Id4 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDNO4"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_EXPIRYDATE4"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate4 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE4"].ToString());
                        }

                        loanInformationObj.ApplicantId.Type5 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDTYPE5"]);
                        loanInformationObj.ApplicantId.Id5 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDNO5"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_EXPIRYDATE5"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate5 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE5"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["VEHIC_ID"].ToString()))
                        {
                            loanInformationObj.VechileId = Convert.ToInt32(loanInformationDataReaderObj["VEHIC_ID"].ToString());
                        }
                        loanInformationObj.VechileType = Convert.ToString(loanInformationDataReaderObj["VEHIC_TYPE"]);

                        loanInformationObj.SourcePersonName = Convert.ToString(loanInformationDataReaderObj["LOAP_SOURCE_PERSON_NAME"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_DECLARED_INCOME"].ToString()))
                        {
                            loanInformationObj.DeclaredAmount = 0;
                            if (IsNumeric(loanInformationDataReaderObj["LOAP_DECLARED_INCOME"].ToString()))
                            {
                                loanInformationObj.DeclaredAmount = Convert.ToDecimal(loanInformationDataReaderObj["LOAP_DECLARED_INCOME"].ToString());
                            }


                            //loanInformationObj.DeclaredAmount = 0;
                            //int outputValue = 0;
                            //bool isNumber = false;
                            //isNumber = int.TryParse(loanInformationDataReaderObj["LOAP_DECLARED_INCOME"].ToString(), out outputValue);
                            //if (isNumber)
                            //{
                            //    loanInformationObj.DeclaredAmount = Convert.ToDecimal(loanInformationDataReaderObj["LOAP_DECLARED_INCOME"].ToString());
                            //}
                            //loanInformationObj.DeclaredAmount = Convert.ToDecimal(loanInformationDataReaderObj["LOAP_DECLARED_INCOME"].ToString());
                        }

                        loanInformationObj.ExtraField1 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD1"]);
                        loanInformationObj.ExtraField2 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD2"]);
                        loanInformationObj.ExtraField3 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD3"]);
                        loanInformationObj.ExtraField4 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD4"]);
                        loanInformationObj.ExtraField5 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD5"]);
                        loanInformationObj.ExtraField6 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD6"]);
                        loanInformationObj.ExtraField7 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD7"]);
                        loanInformationObj.ExtraField8 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD8"]);
                        loanInformationObj.ExtraField9 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD9"]);
                        loanInformationObj.ExtraField10 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD10"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAN_STATU_ID"].ToString()))
                        {
                            loanInformationObj.ForwardOrDesferStatus = Convert.ToInt32("0" + loanInformationDataReaderObj["LOAN_STATU_ID"]);
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_USERID"].ToString()))
                        {
                            loanInformationObj.EntryUserId = Convert.ToInt32("0" + loanInformationDataReaderObj["LOAP_USERID"]);
                        }
                    }
                    loanInformationDataReaderObj.Close();
                }
            }
            return loanInformationObj;
        }

        public static bool IsNumeric(string str)
        {
            try
            {
                str = str.Trim();
                int foo = int.Parse(str);
                return (true);
            }
            catch (FormatException)
            {
                // Not a numeric value
                return (false);
            }
        }

        public LoanInformation GetLoanInformationCreate(int llid)
        {
            DataTable loanInfoDataTableObj = new DataTable();
            string queryString = String.Format(@"SELECT LOAN_APPLI_ID, loan_app_info.PROD_ID, BR_ID, RCV_DT, CUST_NM, LOAN_APP_AMT, ACC_NO, LOAP_ACNOFORSCB2, LOAP_ACNOFORSCB3, LOAP_ACNOFORSCB4, LOAP_ACNOFORSCB5, TIN_NO, 
                                isnull(CR_CARD_NO_SCB,'') AS CR_CARD_NO_SCB, isnull(LOAP_CREDITCARDNO2,'') AS LOAP_CREDITCARDNO2, 
                                isnull(LOAP_CREDITCARDNO3,'') AS LOAP_CREDITCARDNO3, isnull(LOAP_CREDITCARDNO4,'') AS LOAP_CREDITCARDNO4,
                                isnull(LOAP_CREDITCARDNO5,'')AS LOAP_CREDITCARDNO5, loan_app_info.FATHE_NM, loan_app_info.MOTHE_NM,Format(loan_app_info.DOB,'MM-dd-yyyy') as DOB,
                                MARIT_STATU, isnull((CASE HIGH_EDU_LEVEL WHEN '' THEN '' ELSE HIGH_EDU_LEVEL END),'') AS HIGH_EDU_LEVEL,
                                isnull((CASE HOME_OWNER WHEN '' THEN 'Others' WHEN 'Own' THEN 'Own' WHEN 'Family Owned' THEN 'Family Owned' WHEN 'Others' THEN 'Others' ELSE 'Others' END), 'Others') AS HOME_OWNER, 
                                RES_ADD, MOBIL, RES_PH,OFF_PH,LOAN_CONTACTNO4,LOAN_CONTACTNO5, isnull((CASE loan_app_info.PROFE WHEN '' THEN '' ELSE loan_app_info.PROFE END),'') AS PROFE, COMPA_NM, OFF_ADD, MON_CURRE_JOB, 
                                isnull((CASE SPOUS_PROFE WHEN '' THEN '' ELSE SPOUS_PROFE END),'') AS SPOUS_PROFE, isnull(OTHER_ACC_BANK_ID_1, 0) AS OTHER_ACC_BANK_ID_1, isnull(OTHER_ACC_BR_ID_1, 0) AS OTHER_ACC_BR_ID_1, 
                                OTHER_ACC_NO_1, isnull((CASE OTHER_ACC_TYPE_1 WHEN '' THEN '' ELSE OTHER_ACC_TYPE_1 END),'') AS OTHER_ACC_TYPE_1, isnull(OTHER_ACC_BANK_ID_2, 0) AS OTHER_ACC_BANK_ID_2, isnull(OTHER_ACC_BR_ID_2,0) AS OTHER_ACC_BR_ID_2, OTHER_ACC_NO_2, isnull((CASE OTHER_ACC_TYPE_2 WHEN '' THEN '' ELSE OTHER_ACC_TYPE_2 END),'') AS OTHER_ACC_TYPE_2, OTHER_ACC_BANK_ID_3, 
                                isnull(OTHER_ACC_BR_ID_3,0) AS OTHER_ACC_BR_ID_3, OTHER_ACC_NO_3, isnull((CASE OTHER_ACC_TYPE_3 WHEN '' THEN '' ELSE OTHER_ACC_TYPE_3 END),'') AS OTHER_ACC_TYPE_3, isnull(LOAP_OTHERACWITHBANK_ID4, 0) AS LOAP_OTHERACWITHBANK_ID4, isnull(LOAP_OTHERACWITHBRANCH_ID4, 0) AS LOAP_OTHERACWITHBRANCH_ID4, LOAP_OTHERACWITHACNO4, isnull((CASE LOAP_ORHREACTYPE_ID4 WHEN '' THEN '' ELSE LOAP_ORHREACTYPE_ID4 END),'') AS  LOAP_ORHREACTYPE_ID4, 
                                isnull(LOAP_OTHERACWITHBANK_ID5,0) AS LOAP_OTHERACWITHBANK_ID5, isnull(LOAP_OTHERACWITHBRANCH_ID5,0) AS LOAP_OTHERACWITHBRANCH_ID5, LOAP_OTHERACWITHACNO5, isnull((CASE LOAP_ORHREACTYPE_ID5 WHEN '' THEN '' ELSE LOAP_ORHREACTYPE_ID5 END),'') AS  LOAP_ORHREACTYPE_ID5, isnull((CASE loan_app_info.ID_TYPE WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE loan_app_info.ID_TYPE END),'') AS ID_TYPE, loan_app_info.ID_NO,ID_EXPIR , isnull((CASE LOAP_IDTYPE2 WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE LOAP_IDTYPE2 END),'') AS LOAP_IDTYPE2, LOAP_IDNO2, LOAP_EXPIRYDATE2, isnull((CASE LOAP_IDTYPE3 WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE LOAP_IDTYPE3 END),'') AS LOAP_IDTYPE3,
                                LOAP_IDNO3, LOAP_EXPIRYDATE3, isnull((CASE LOAP_IDTYPE4 WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE LOAP_IDTYPE4 END),'') AS LOAP_IDTYPE4, LOAP_IDNO4, LOAP_EXPIRYDATE4, 
                                isnull((CASE LOAP_IDTYPE5 WHEN '' THEN '' WHEN 'Voter' THEN 'National Id' WHEN 'Photo' THEN '' WHEN 'Reference' THEN '' ELSE LOAP_IDTYPE5 END),'') AS LOAP_IDTYPE5, LOAP_IDNO5, LOAP_EXPIRYDATE5, 
                                VEHIC_ID, VEHIC_TYPE,  LOAP_SOURCE_PERSON_NAME, isnull(LOAP_DECLARED_INCOME,0) AS LOAP_DECLARED_INCOME, LOAP_EXTRA_FIELD1, LOAP_EXTRA_FIELD2, LOAP_EXTRA_FIELD3, LOAP_EXTRA_FIELD4, LOAP_EXTRA_FIELD5, LOAP_EXTRA_FIELD6, LOAP_EXTRA_FIELD7, LOAP_EXTRA_FIELD8, LOAP_EXTRA_FIELD9, LOAP_EXTRA_FIELD10, isnull((CASE LOAN_STATU_ID WHEN '' THEN 1 ELSE LOAN_STATU_ID END),1) AS LOAN_STATU_ID,LOAP_USERID,REMAR,DECLI_REASO,MultiLocatorRemarksID
                                FROM loan_app_info 
								 WHERE LOAN_APPLI_ID = {0} and LOAN_STATU_ID NOT IN (8,10,11)", llid);
            //DbDataReader loanInformationDataReaderObj = DbQueryManager.ExecuteReader(queryString, "conString");
            loanInfoDataTableObj = DbQueryManager.GetTable(queryString);
            if (loanInfoDataTableObj != null)
            {
                DataTableReader loanInformationDataReaderObj = loanInfoDataTableObj.CreateDataReader();
                if (loanInformationDataReaderObj != null)
                {
                    while (loanInformationDataReaderObj.Read())
                    {
                        loanInformationObj = new LoanInformation();
                        loanInformationObj.LLId = Convert.ToInt32(loanInformationDataReaderObj["LOAN_APPLI_ID"].ToString());
                        loanInformationObj.ProductId = Convert.ToInt32(loanInformationDataReaderObj["PROD_ID"].ToString());
                        loanInformationObj.SourceId = Convert.ToInt32(loanInformationDataReaderObj["BR_ID"].ToString());
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["RCV_DT"].ToString()))
                        {
                            loanInformationObj.RecieveDate = Convert.ToDateTime(loanInformationDataReaderObj["RCV_DT"].ToString());
                        }
                        //loanInformationObj.RecieveDate = Convert.ToDateTime(loanInformationDataReaderObj["RCV_DT"].ToString());
                        loanInformationObj.ApplicantName = Convert.ToString(loanInformationDataReaderObj["CUST_NM"]);
                        loanInformationObj.REMAR = Convert.ToString(loanInformationDataReaderObj["REMAR"]);
                        loanInformationObj.DECLI_REASO = Convert.ToString(loanInformationDataReaderObj["DECLI_REASO"]);
                        loanInformationObj.MultiLocatorRemarksID = Convert.ToString(loanInformationDataReaderObj["MultiLocatorRemarksID"]);
                        loanInformationObj.AppliedAmount = Convert.ToDecimal("15000000");
                        loanInformationObj.AppliedAmount = Convert.ToDecimal("15000000");
                        string sstr = loanInformationDataReaderObj["LOAN_APP_AMT"].ToString();
                        loanInformationObj.AppliedAmount = Convert.ToDecimal(sstr);

                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["ACC_NO"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo1 = "";
                            loanInformationObj.AccountNo.MasterAccNo1 = "";
                            loanInformationObj.AccountNo.PostAccNo1 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["ACC_NO"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo1 = Convert.ToString(loanInformationDataReaderObj["ACC_NO"]);
                                string preAccNo1 = accountNo1.Substring(0, 2).ToString();
                                string masterAccNo1 = accountNo1.Substring(2, 7).ToString();
                                string postAccNo1 = accountNo1.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo1 = preAccNo1;
                                loanInformationObj.AccountNo.MasterAccNo1 = masterAccNo1;
                                loanInformationObj.AccountNo.PostAccNo1 = postAccNo1;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo1 = "";
                                loanInformationObj.AccountNo.MasterAccNo1 = loanInformationDataReaderObj["ACC_NO"].ToString();
                                loanInformationObj.AccountNo.PostAccNo1 = "";
                            }
                        }

                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_ACNOFORSCB2"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo2 = "";
                            loanInformationObj.AccountNo.MasterAccNo2 = "";
                            loanInformationObj.AccountNo.PostAccNo2 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["LOAP_ACNOFORSCB2"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo2 = Convert.ToString(loanInformationDataReaderObj["LOAP_ACNOFORSCB2"]);
                                string preAccNo2 = accountNo2.Substring(0, 2).ToString();
                                string masterAccNo2 = accountNo2.Substring(2, 7).ToString();
                                string postAccNo2 = accountNo2.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo2 = preAccNo2;
                                loanInformationObj.AccountNo.MasterAccNo2 = masterAccNo2;
                                loanInformationObj.AccountNo.PostAccNo2 = postAccNo2;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo2 = "";
                                loanInformationObj.AccountNo.MasterAccNo2 = loanInformationDataReaderObj["LOAP_ACNOFORSCB2"].ToString();
                                loanInformationObj.AccountNo.PostAccNo2 = "";
                            }
                        }
                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_ACNOFORSCB3"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo3 = "";
                            loanInformationObj.AccountNo.MasterAccNo3 = "";
                            loanInformationObj.AccountNo.PostAccNo3 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["LOAP_ACNOFORSCB3"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo3 = Convert.ToString(loanInformationDataReaderObj["LOAP_ACNOFORSCB3"]);
                                string preAccNo3 = accountNo3.Substring(0, 2).ToString();
                                string masterAccNo3 = accountNo3.Substring(2, 7).ToString();
                                string postAccNo3 = accountNo3.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo3 = preAccNo3;
                                loanInformationObj.AccountNo.MasterAccNo3 = masterAccNo3;
                                loanInformationObj.AccountNo.PostAccNo3 = postAccNo3;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo3 = "";
                                loanInformationObj.AccountNo.MasterAccNo3 = loanInformationDataReaderObj["LOAP_ACNOFORSCB3"].ToString();
                                loanInformationObj.AccountNo.PostAccNo3 = "";
                            }
                        }
                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_ACNOFORSCB4"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo4 = "";
                            loanInformationObj.AccountNo.MasterAccNo4 = "";
                            loanInformationObj.AccountNo.PostAccNo4 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["LOAP_ACNOFORSCB4"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo4 = Convert.ToString(loanInformationDataReaderObj["LOAP_ACNOFORSCB4"]);
                                string preAccNo4 = accountNo4.Substring(0, 2).ToString();
                                string masterAccNo4 = accountNo4.Substring(2, 7).ToString();
                                string postAccNo4 = accountNo4.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo4 = preAccNo4;
                                loanInformationObj.AccountNo.MasterAccNo4 = masterAccNo4;
                                loanInformationObj.AccountNo.PostAccNo4 = postAccNo4;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo4 = "";
                                loanInformationObj.AccountNo.MasterAccNo4 = loanInformationDataReaderObj["LOAP_ACNOFORSCB4"].ToString();
                                loanInformationObj.AccountNo.PostAccNo4 = "";
                            }
                        }
                        if (String.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_ACNOFORSCB5"].ToString()))
                        {
                            loanInformationObj.AccountNo.PreAccNo5 = "";
                            loanInformationObj.AccountNo.MasterAccNo5 = "";
                            loanInformationObj.AccountNo.PostAccNo5 = "";
                        }
                        else
                        {
                            if (loanInformationDataReaderObj["LOAP_ACNOFORSCB5"].ToString().Length.Equals(SCBACCNO_LENGTH))
                            {
                                string accountNo5 = Convert.ToString(loanInformationDataReaderObj["LOAP_ACNOFORSCB5"]);
                                string preAccNo5 = accountNo5.Substring(0, 2).ToString();
                                string masterAccNo5 = accountNo5.Substring(2, 7).ToString();
                                string postAccNo5 = accountNo5.Substring(9, 2).ToString();
                                loanInformationObj.AccountNo.PreAccNo5 = preAccNo5;
                                loanInformationObj.AccountNo.MasterAccNo5 = masterAccNo5;
                                loanInformationObj.AccountNo.PostAccNo5 = postAccNo5;
                            }
                            else
                            {
                                loanInformationObj.AccountNo.PreAccNo5 = "";
                                loanInformationObj.AccountNo.MasterAccNo5 = loanInformationDataReaderObj["LOAP_ACNOFORSCB5"].ToString();
                                loanInformationObj.AccountNo.PostAccNo5 = "";
                            }
                        }

                        loanInformationObj.TINNo = Convert.ToString(loanInformationDataReaderObj["TIN_NO"]);
                        loanInformationObj.CreditCardNo1 = Convert.ToString(loanInformationDataReaderObj["CR_CARD_NO_SCB"]);
                        loanInformationObj.CreditCardNo2 = Convert.ToString(loanInformationDataReaderObj["LOAP_CREDITCARDNO2"]);
                        loanInformationObj.CreditCardNo3 = Convert.ToString(loanInformationDataReaderObj["LOAP_CREDITCARDNO3"]);
                        loanInformationObj.CreditCardNo4 = Convert.ToString(loanInformationDataReaderObj["LOAP_CREDITCARDNO4"]);
                        loanInformationObj.CreditCardNo5 = Convert.ToString(loanInformationDataReaderObj["LOAP_CREDITCARDNO5"]);
                        loanInformationObj.FatherName = Convert.ToString(loanInformationDataReaderObj["FATHE_NM"]);
                        loanInformationObj.MotherName = Convert.ToString(loanInformationDataReaderObj["MOTHE_NM"]);
                        loanInformationObj.REMAR = Convert.ToString(loanInformationDataReaderObj["REMAR"]);
                        loanInformationObj.DECLI_REASO = Convert.ToString(loanInformationDataReaderObj["DECLI_REASO"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["DOB"].ToString()))
                        {
                            loanInformationObj.DOB = Convert.ToDateTime(loanInformationDataReaderObj["DOB"]);
                        }
                        loanInformationObj.EducationalLevel = Convert.ToString(loanInformationDataReaderObj["HIGH_EDU_LEVEL"]);
                        loanInformationObj.MaritalStatus = Convert.ToString(loanInformationDataReaderObj["MARIT_STATU"]);
                        loanInformationObj.ResidentialStatus = Convert.ToString(loanInformationDataReaderObj["HOME_OWNER"]);
                        loanInformationObj.ResidentAddress = Convert.ToString(loanInformationDataReaderObj["RES_ADD"]);
                        loanInformationObj.ContactNumber.ContactNo1 = Convert.ToString(loanInformationDataReaderObj["MOBIL"]);
                        loanInformationObj.ContactNumber.ContactNo2 = Convert.ToString(loanInformationDataReaderObj["RES_PH"]);
                        loanInformationObj.ContactNumber.ContactNo3 = Convert.ToString(loanInformationDataReaderObj["OFF_PH"]);
                        loanInformationObj.ContactNumber.ContactNo4 = Convert.ToString(loanInformationDataReaderObj["LOAN_CONTACTNO4"]);
                        loanInformationObj.ContactNumber.ContactNo5 = Convert.ToString(loanInformationDataReaderObj["LOAN_CONTACTNO5"]);
                        loanInformationObj.Profession = Convert.ToString(loanInformationDataReaderObj["PROFE"]);
                        loanInformationObj.Business = Convert.ToString(loanInformationDataReaderObj["COMPA_NM"]);
                        loanInformationObj.OfficeAddress = Convert.ToString(loanInformationDataReaderObj["OFF_ADD"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["MON_CURRE_JOB"].ToString()))
                        {
                            loanInformationObj.JobDuration = Convert.ToInt32(loanInformationDataReaderObj["MON_CURRE_JOB"]);
                        }
                        loanInformationObj.SpousOcupation = Convert.ToString(loanInformationDataReaderObj["SPOUS_PROFE"]);

                        loanInformationObj.OtherBankAccount.OtherBankId1 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_1"].ToString());
                        loanInformationObj.OtherBankAccount.OtherBranchId1 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BR_ID_1"].ToString());
                        loanInformationObj.OtherBankAccount.OtherAccNo1 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_NO_1"]);
                        loanInformationObj.OtherBankAccount.OtherAccType1 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_TYPE_1"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_2"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBankId2 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_2"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["OTHER_ACC_BR_ID_2"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBranchId2 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BR_ID_2"].ToString());
                        }
                        loanInformationObj.OtherBankAccount.OtherAccNo2 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_NO_2"]);
                        loanInformationObj.OtherBankAccount.OtherAccType2 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_TYPE_2"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_3"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBankId3 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BANK_ID_3"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["OTHER_ACC_BR_ID_3"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBranchId3 = Convert.ToInt32(loanInformationDataReaderObj["OTHER_ACC_BR_ID_3"].ToString());
                        }
                        loanInformationObj.OtherBankAccount.OtherAccNo3 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_NO_3"]);
                        loanInformationObj.OtherBankAccount.OtherAccType3 = Convert.ToString(loanInformationDataReaderObj["OTHER_ACC_TYPE_3"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_OTHERACWITHBANK_ID4"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBankId4 = Convert.ToInt32(loanInformationDataReaderObj["LOAP_OTHERACWITHBANK_ID4"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_OTHERACWITHBRANCH_ID4"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBranchId4 = Convert.ToInt32(loanInformationDataReaderObj["LOAP_OTHERACWITHBRANCH_ID4"].ToString());
                        }
                        loanInformationObj.OtherBankAccount.OtherAccNo4 = Convert.ToString(loanInformationDataReaderObj["LOAP_OTHERACWITHACNO4"]);
                        loanInformationObj.OtherBankAccount.OtherAccType4 = Convert.ToString(loanInformationDataReaderObj["LOAP_ORHREACTYPE_ID4"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_OTHERACWITHBANK_ID5"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBankId5 = Convert.ToInt32(loanInformationDataReaderObj["LOAP_OTHERACWITHBANK_ID5"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_OTHERACWITHBRANCH_ID5"].ToString()))
                        {
                            loanInformationObj.OtherBankAccount.OtherBranchId5 = Convert.ToInt32(loanInformationDataReaderObj["LOAP_OTHERACWITHBRANCH_ID5"].ToString());
                        }

                        loanInformationObj.OtherBankAccount.OtherAccNo5 = Convert.ToString(loanInformationDataReaderObj["LOAP_OTHERACWITHACNO5"]);
                        loanInformationObj.OtherBankAccount.OtherAccType5 = Convert.ToString(loanInformationDataReaderObj["LOAP_ORHREACTYPE_ID5"]);

                        loanInformationObj.ApplicantId.Type1 = Convert.ToString(loanInformationDataReaderObj["ID_TYPE"]);
                        loanInformationObj.ApplicantId.Id1 = Convert.ToString(loanInformationDataReaderObj["ID_NO"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["ID_EXPIR"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate1 = Convert.ToDateTime(loanInformationDataReaderObj["ID_EXPIR"].ToString());
                        }

                        loanInformationObj.ApplicantId.Type2 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDTYPE2"]);
                        loanInformationObj.ApplicantId.Id2 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDNO2"]);
                        //loanInformationObj.ApplicantId.ExpiryDate2 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE2"].ToString()); 
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_EXPIRYDATE2"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate2 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE2"].ToString());
                        }

                        loanInformationObj.ApplicantId.Type3 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDTYPE3"]);
                        loanInformationObj.ApplicantId.Id3 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDNO3"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_EXPIRYDATE3"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate3 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE3"].ToString());
                        }

                        loanInformationObj.ApplicantId.Type4 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDTYPE4"]);
                        loanInformationObj.ApplicantId.Id4 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDNO4"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_EXPIRYDATE4"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate4 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE4"].ToString());
                        }

                        loanInformationObj.ApplicantId.Type5 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDTYPE5"]);
                        loanInformationObj.ApplicantId.Id5 = Convert.ToString(loanInformationDataReaderObj["LOAP_IDNO5"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_EXPIRYDATE5"].ToString()))
                        {
                            loanInformationObj.ApplicantId.ExpiryDate5 = Convert.ToDateTime(loanInformationDataReaderObj["LOAP_EXPIRYDATE5"].ToString());
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["VEHIC_ID"].ToString()))
                        {
                            loanInformationObj.VechileId = Convert.ToInt32(loanInformationDataReaderObj["VEHIC_ID"].ToString());
                        }
                        loanInformationObj.VechileType = Convert.ToString(loanInformationDataReaderObj["VEHIC_TYPE"]);

                        loanInformationObj.SourcePersonName = Convert.ToString(loanInformationDataReaderObj["LOAP_SOURCE_PERSON_NAME"]);
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_DECLARED_INCOME"].ToString()))
                        {
                            loanInformationObj.DeclaredAmount = Convert.ToDecimal(loanInformationDataReaderObj["LOAP_DECLARED_INCOME"].ToString());
                        }

                        loanInformationObj.ExtraField1 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD1"]);
                        loanInformationObj.ExtraField2 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD2"]);
                        loanInformationObj.ExtraField3 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD3"]);
                        loanInformationObj.ExtraField4 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD4"]);
                        loanInformationObj.ExtraField5 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD5"]);
                        loanInformationObj.ExtraField6 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD6"]);
                        loanInformationObj.ExtraField7 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD7"]);
                        loanInformationObj.ExtraField8 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD8"]);
                        loanInformationObj.ExtraField9 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD9"]);
                        loanInformationObj.ExtraField10 = Convert.ToString(loanInformationDataReaderObj["LOAP_EXTRA_FIELD10"]);

                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAN_STATU_ID"].ToString()))
                        {
                            loanInformationObj.ForwardOrDesferStatus = Convert.ToInt32("0" + loanInformationDataReaderObj["LOAN_STATU_ID"]);
                        }
                        if (!string.IsNullOrEmpty(loanInformationDataReaderObj["LOAP_USERID"].ToString()))
                        {
                            loanInformationObj.EntryUserId = Convert.ToInt32("0" + loanInformationDataReaderObj["LOAP_USERID"]);
                        }
                    }
                    loanInformationDataReaderObj.Close();
                }
            }
            return loanInformationObj;
        }
        #endregion

        #region GetAllJointApplicantInformation
        /// <summary>
        /// This method provide all joint applicants information.
        /// </summary>
        /// <param name="llid">Gets the applicant LLID</param>
        /// <returns>Returns joint applicant information object</returns>
        public JoinApplicantInformation GetAllJointApplicantInformation(Int32 llid)
        {
            string queryString = String.Format("SELECT LOAN_APPLI_ID, J_APP_NM, J_APP_FATHE_NM, J_APP_MOTHE_NM, IFNULL((CASE J_APP_DOB WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J_APP_DOB AS CHAR) END),'0001-01-01') AS J_APP_DOB, " +
                                                "J_APP_EMPLO, IFNULL((CASE J_APP_EDUCA_LEVEL WHEN '' THEN '' ELSE J_APP_EDUCA_LEVEL END),'') AS J_APP_EDUCA_LEVEL, IFNULL(J_APP_AC_NO,0) AS J_APP_AC_NO, IFNULL(J_APP_AC_NO2,0) AS J_APP_AC_NO2, IFNULL(J_APP_AC_NO3,0) AS J_APP_AC_NO3, IFNULL(J_APP_AC_NO4,0) AS J_APP_AC_NO4, " +
                                                "IFNULL(J_APP_AC_NO5,0) AS J_APP_AC_NO5, J_APP_CC_NO, J_APP_CC_NO2, J_APP_CC_NO3, J_APP_CC_NO4, J_APP_CC_NO5, " +
                                                "J_TIN, J_APP_CONTACT_NO1, J_APP_CONTACT_NO2, J_APP_CONTACT_NO3, J_APP_CONTACT_NO4, J_ID_TYPE1, " +
                                                "J_ID_TYPE2, J_ID_TYPE3, J_ID_TYPE4, J_ID_TYPE5, J_ID_NO1, J_ID_NO2, J_ID_NO3, J_ID_NO4,J_ID_NO5, " +
                                                "IFNULL((CASE J_ID_EXPIR1 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J_ID_EXPIR1 AS CHAR) END),'0001-01-01') AS J_ID_EXPIR1, " +
                                                "IFNULL((CASE J_ID_EXPIR2 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J_ID_EXPIR2 AS CHAR) END),'0001-01-01') AS J_ID_EXPIR2, " +
                                                "IFNULL((CASE J_ID_EXPIR3 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J_ID_EXPIR3 AS CHAR) END),'0001-01-01') AS J_ID_EXPIR3, " +
                                                "IFNULL((CASE J_ID_EXPIR4 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J_ID_EXPIR4 AS CHAR) END),'0001-01-01') AS J_ID_EXPIR4, " +
                                                "IFNULL((CASE J_ID_EXPIR5 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J_ID_EXPIR5 AS CHAR) END),'0001-01-01') AS J_ID_EXPIR5, " +
                                                "J2_APP_NM, IFNULL(J2_APP_AC_NO1,0) AS J2_APP_AC_NO1, IFNULL(J2_APP_AC_NO2,0) AS J2_APP_AC_NO2, IFNULL(J2_APP_AC_NO3,0) AS J2_APP_AC_NO3, " +
                                                "IFNULL(J2_APP_AC_NO4,0) AS J2_APP_AC_NO4, IFNULL(J2_APP_AC_NO5,0) AS J2_APP_AC_NO5, " +
                                                "J2_TIN, J2_APP_CC_NO, J2_APP_CC_NO2, J2_APP_CC_NO3, J2_APP_CC_NO4, J2_APP_CC_NO5, J2_APP_FATHE_NM, J2_APP_MOTHE_NM, IFNULL((CASE J2_APP_DOB WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J2_APP_DOB AS CHAR) END),'0001-01-01') AS J2_APP_DOB, IFNULL((CASE J2_APP_EDUCA_LEVEL WHEN '' THEN '' ELSE J2_APP_EDUCA_LEVEL END),'') AS J2_APP_EDUCA_LEVEL, " +
                                                "J2_APP_CONTACT_NO1, J2_APP_CONTACT_NO2, J2_APP_CONTACT_NO3, J2_APP_CONTACT_NO4, J2_APP_EMPLO, J2_ID_TYPE1, " +
                                                "J2_ID_TYPE2, J2_ID_TYPE3, J2_ID_TYPE4,J2_ID_TYPE5, J2_ID_NO1, J2_ID_NO2, J2_ID_NO3, J2_ID_NO4, J2_ID_NO5, " +
                                                "IFNULL((CASE J2_ID_EXPIR1 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J2_ID_EXPIR1 AS CHAR) END),'0001-01-01') AS J2_ID_EXPIR1, " +
                                                "IFNULL((CASE J2_ID_EXPIR2 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J2_ID_EXPIR2 AS CHAR) END),'0001-01-01') AS J2_ID_EXPIR2, " +
                                                "IFNULL((CASE J2_ID_EXPIR3 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J2_ID_EXPIR3 AS CHAR) END),'0001-01-01') AS J2_ID_EXPIR3, " +
                                                "IFNULL((CASE J2_ID_EXPIR4 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J2_ID_EXPIR4 AS CHAR) END),'0001-01-01') AS J2_ID_EXPIR4, " +
                                                "IFNULL((CASE J2_ID_EXPIR5 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J2_ID_EXPIR5 AS CHAR) END),'0001-01-01') AS J2_ID_EXPIR5, " +
                                                "J3_APP_NM, IFNULL(J3_APP_AC_NO1,0) AS J3_APP_AC_NO1, IFNULL(J3_APP_AC_NO2,0) AS J3_APP_AC_NO2, IFNULL(J3_APP_AC_NO3,0) AS J3_APP_AC_NO3, IFNULL(J3_APP_AC_NO4,0) AS J3_APP_AC_NO4, IFNULL(J3_APP_AC_NO5,0) AS J3_APP_AC_NO5, " +
                                                "J3_TIN, J3_APP_CC_NO, J3_APP_CC_NO2, J3_APP_CC_NO3, J3_APP_CC_NO4, J3_APP_CC_NO5, J3_APP_FATHE_NM, J3_APP_MOTHE_NM, " +
                                                "IFNULL((CASE J3_APP_DOB WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J3_APP_DOB AS CHAR) END),'0001-01-01') AS J3_APP_DOB, IFNULL((CASE J3_APP_EDUCA_LEVEL WHEN '' THEN '' ELSE J3_APP_EDUCA_LEVEL END),'') AS J3_APP_EDUCA_LEVEL, " +
                                                "J3_APP_CONTACT_NO1, J3_APP_CONTACT_NO2, J3_APP_CONTACT_NO3, J3_APP_CONTACT_NO4, J3_APP_EMPLO, J3_ID_TYPE1, " +
                                                "J3_ID_TYPE2, J3_ID_TYPE3, J3_ID_TYPE4, J3_ID_TYPE5, J3_ID_NO1, J3_ID_NO2, J3_ID_NO3, J3_ID_NO4, J3_ID_NO5, " +
                                                "IFNULL((CASE J3_ID_EXPIR1 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J3_ID_EXPIR1 AS CHAR) END),'0001-01-01') AS J3_ID_EXPIR1, " +
                                                "IFNULL((CASE J3_ID_EXPIR2 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J3_ID_EXPIR2 AS CHAR) END),'0001-01-01') AS J3_ID_EXPIR2, " +
                                                "IFNULL((CASE J3_ID_EXPIR3 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J3_ID_EXPIR3 AS CHAR) END),'0001-01-01') AS J3_ID_EXPIR3, " +
                                                "IFNULL((CASE J3_ID_EXPIR4 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J3_ID_EXPIR4 AS CHAR) END),'0001-01-01') AS J3_ID_EXPIR4, " +
                                                "IFNULL((CASE J3_ID_EXPIR5 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J3_ID_EXPIR5 AS CHAR) END),'0001-01-01') AS J3_ID_EXPIR5, " +
                                                "J4_APP_NM, IFNULL(J4_APP_AC_NO1,0) AS J4_APP_AC_NO1, IFNULL(J4_APP_AC_NO2,0) AS J4_APP_AC_NO2, IFNULL(J4_APP_AC_NO3,0) AS J4_APP_AC_NO3, IFNULL(J4_APP_AC_NO4,0) AS J4_APP_AC_NO4, IFNULL(J4_APP_AC_NO5,0) AS J4_APP_AC_NO5, " +
                                                "J4_TIN, J4_APP_CC_NO, J4_APP_CC_NO2, J4_APP_CC_NO3, J4_APP_CC_NO4, J4_APP_CC_NO5, J4_APP_FATHE_NM, J4_APP_MOTHE_NM, " +
                                                "IFNULL((CASE J4_APP_DOB WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J4_APP_DOB AS CHAR) END),'0001-01-01') AS J4_APP_DOB, IFNULL((CASE J4_APP_EDUCA_LEVEL WHEN '' THEN '' ELSE J4_APP_EDUCA_LEVEL END),'') AS J4_APP_EDUCA_LEVEL, " +
                                                "J4_APP_CONTACT_NO1, J4_APP_CONTACT_NO2, J4_APP_CONTACT_NO3, J4_APP_CONTACT_NO4, J4_APP_EMPLO, " +
                                                "J4_ID_TYPE1, J4_ID_TYPE2, J4_ID_TYPE3, J4_ID_TYPE4, J4_ID_TYPE5, J4_ID_NO1, J4_ID_NO2, J4_ID_NO3, J4_ID_NO4, J4_ID_NO5, " +
                                                "IFNULL((CASE J4_ID_EXPIR1 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J4_ID_EXPIR1 AS CHAR) END),'0001-01-01') AS J4_ID_EXPIR1, " +
                                                "IFNULL((CASE J4_ID_EXPIR2 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J4_ID_EXPIR2 AS CHAR) END),'0001-01-01') AS J4_ID_EXPIR2, " +
                                                "IFNULL((CASE J4_ID_EXPIR3 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J4_ID_EXPIR3 AS CHAR) END),'0001-01-01') AS J4_ID_EXPIR3, " +
                                                "IFNULL((CASE J4_ID_EXPIR4 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J4_ID_EXPIR4 AS CHAR) END),'0001-01-01') AS J4_ID_EXPIR4, " +
                                                "IFNULL((CASE J4_ID_EXPIR5 WHEN '0000-00-00' THEN '0001-01-01' WHEN '' THEN '0001-01-01' ELSE CAST(J4_ID_EXPIR5 AS CHAR) END),'0001-01-01') AS J4_ID_EXPIR5  " +
                                                "FROM loan_app_info " +
                                                "WHERE LOAN_APPLI_ID = {0} ", llid);

            DataTable jointApplicantDataTableObj = DbQueryManager.GetTable(queryString);
            if (jointApplicantDataTableObj != null)
            {
                DataTableReader jointApplicantDataTableReaderObj = jointApplicantDataTableObj.CreateDataReader();
                if (jointApplicantDataTableReaderObj != null)
                {
                    while (jointApplicantDataTableReaderObj.Read())
                    {
                        joinApplicantInformationObj = new JoinApplicantInformation();
                        joinApplicantInformationObj.JointApplicantLLId = Convert.ToInt64(jointApplicantDataTableReaderObj["LOAN_APPLI_ID"].ToString());

                        //Joint applicant 1
                        joinApplicantInformationObj.JointApp1Name = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_NM"]);
                        joinApplicantInformationObj.J1FatherName = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_FATHE_NM"]);
                        joinApplicantInformationObj.J1MotherName = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_MOTHE_NM"]);
                        joinApplicantInformationObj.J1DOB = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_APP_DOB"]);
                        joinApplicantInformationObj.J1TinNo = Convert.ToString(jointApplicantDataTableReaderObj["J_TIN"]);

                        joinApplicantInformationObj.J1SCBAccNo1 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO"]);
                        joinApplicantInformationObj.J1SCBAccNo2 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO2"].ToString());
                        joinApplicantInformationObj.J1SCBAccNo3 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO3"].ToString());
                        joinApplicantInformationObj.J1SCBAccNo4 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO4"].ToString());
                        joinApplicantInformationObj.J1SCBAccNo5 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO5"].ToString());

                        joinApplicantInformationObj.J1CrdtCrdNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO"]);
                        joinApplicantInformationObj.J1CrdtCrdNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO2"]);
                        joinApplicantInformationObj.J1CrdtCrdNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO3"]);
                        joinApplicantInformationObj.J1CrdtCrdNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO4"]);
                        joinApplicantInformationObj.J1CrdtCrdNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO5"]);

                        joinApplicantInformationObj.JApp1EducationLevel = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_EDUCA_LEVEL"]);
                        joinApplicantInformationObj.JApp1Company = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_EMPLO"]);

                        joinApplicantInformationObj.JApp1ContactNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO1"]);
                        joinApplicantInformationObj.JApp1ContactNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO2"]);
                        joinApplicantInformationObj.JApp1ContactNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO3"]);
                        joinApplicantInformationObj.JApp1ContactNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO4"]);
                        //joinApplicantInformationObj.JApp1ContactNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO5"]);

                        joinApplicantInformationObj.JApp1IdType1 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE1"]);
                        joinApplicantInformationObj.JApp1Id1 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO1"]);
                        joinApplicantInformationObj.JApp1IdExpire1 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR1"]);
                        joinApplicantInformationObj.JApp1IdType2 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE2"]);
                        joinApplicantInformationObj.JApp1Id2 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO2"]);
                        joinApplicantInformationObj.JApp1IdExpire2 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR2"]);
                        joinApplicantInformationObj.JApp1IdType3 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE3"]);
                        joinApplicantInformationObj.JApp1Id3 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO3"]);
                        joinApplicantInformationObj.JApp1IdExpire3 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR3"]);
                        joinApplicantInformationObj.JApp1IdType4 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE4"]);
                        joinApplicantInformationObj.JApp1Id4 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO4"]);
                        joinApplicantInformationObj.JApp1IdExpire4 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR4"]);
                        joinApplicantInformationObj.JApp1IdType5 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE5"]);
                        joinApplicantInformationObj.JApp1Id5 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO5"]);
                        joinApplicantInformationObj.JApp1IdExpire5 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR5"]);
                        //End joint applicant 1

                        //Joint applicant 2
                        joinApplicantInformationObj.JointApp2Name = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_NM"]);
                        joinApplicantInformationObj.J2FatherName = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_FATHE_NM"]);
                        joinApplicantInformationObj.J2MotherName = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_MOTHE_NM"]);
                        joinApplicantInformationObj.J2DOB = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_APP_DOB"]);
                        joinApplicantInformationObj.J2TinNo = Convert.ToString(jointApplicantDataTableReaderObj["J2_TIN"]);

                        joinApplicantInformationObj.J2SCBAccNo1 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO1"].ToString());
                        joinApplicantInformationObj.J2SCBAccNo2 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO2"].ToString());
                        joinApplicantInformationObj.J2SCBAccNo3 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO3"].ToString());
                        joinApplicantInformationObj.J2SCBAccNo4 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO4"].ToString());
                        joinApplicantInformationObj.J2SCBAccNo5 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO5"].ToString());

                        joinApplicantInformationObj.J2CrdtCrdNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO"]);
                        joinApplicantInformationObj.J2CrdtCrdNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO2"]);
                        joinApplicantInformationObj.J2CrdtCrdNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO3"]);
                        joinApplicantInformationObj.J2CrdtCrdNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO4"]);
                        joinApplicantInformationObj.J2CrdtCrdNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO5"]);

                        joinApplicantInformationObj.JApp2EducationLevel = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_EDUCA_LEVEL"]);
                        joinApplicantInformationObj.JApp2Company = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_EMPLO"]);

                        joinApplicantInformationObj.JApp2ContactNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO1"]);
                        joinApplicantInformationObj.JApp2ContactNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO2"]);
                        joinApplicantInformationObj.JApp2ContactNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO3"]);
                        joinApplicantInformationObj.JApp2ContactNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO4"]);
                        //joinApplicantInformationObj.JApp2ContactNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO5"]);

                        joinApplicantInformationObj.JApp2IdType1 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE1"]);
                        joinApplicantInformationObj.JApp2Id1 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO1"]);
                        joinApplicantInformationObj.JApp2IdExpire1 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR1"]);
                        joinApplicantInformationObj.JApp2IdType2 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE2"]);
                        joinApplicantInformationObj.JApp2Id2 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO2"]);
                        joinApplicantInformationObj.JApp2IdExpire2 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR2"]);
                        joinApplicantInformationObj.JApp2IdType3 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE3"]);
                        joinApplicantInformationObj.JApp2Id3 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO3"]);
                        joinApplicantInformationObj.JApp2IdExpire3 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR3"]);
                        joinApplicantInformationObj.JApp2IdType4 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE4"]);
                        joinApplicantInformationObj.JApp2Id4 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO4"]);
                        joinApplicantInformationObj.JApp2IdExpire4 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR4"]);
                        joinApplicantInformationObj.JApp2IdType5 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE5"]);
                        joinApplicantInformationObj.JApp2Id5 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO5"]);
                        joinApplicantInformationObj.JApp2IdExpire5 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR5"]);
                        //End joint applicant 2

                        //Joint applicant 3
                        joinApplicantInformationObj.JointApp3Name = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_NM"]);
                        joinApplicantInformationObj.J3FatherName = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_FATHE_NM"]);
                        joinApplicantInformationObj.J3MotherName = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_MOTHE_NM"]);
                        joinApplicantInformationObj.J3DOB = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_APP_DOB"]);
                        joinApplicantInformationObj.J3TinNo = Convert.ToString(jointApplicantDataTableReaderObj["J3_TIN"]);

                        joinApplicantInformationObj.J3SCBAccNo1 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO1"].ToString());
                        joinApplicantInformationObj.J3SCBAccNo2 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO2"].ToString());
                        joinApplicantInformationObj.J3SCBAccNo3 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO3"].ToString());
                        joinApplicantInformationObj.J3SCBAccNo4 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO4"].ToString());
                        joinApplicantInformationObj.J3SCBAccNo5 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO5"].ToString());

                        joinApplicantInformationObj.J3CrdtCrdNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO"]);
                        joinApplicantInformationObj.J3CrdtCrdNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO2"]);
                        joinApplicantInformationObj.J3CrdtCrdNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO3"]);
                        joinApplicantInformationObj.J3CrdtCrdNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO4"]);
                        joinApplicantInformationObj.J3CrdtCrdNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO5"]);

                        joinApplicantInformationObj.JApp3EducationLevel = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_EDUCA_LEVEL"]);
                        joinApplicantInformationObj.JApp3Company = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_EMPLO"]);

                        joinApplicantInformationObj.JApp3ContactNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO1"]);
                        joinApplicantInformationObj.JApp3ContactNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO2"]);
                        joinApplicantInformationObj.JApp3ContactNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO3"]);
                        joinApplicantInformationObj.JApp3ContactNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO4"]);
                        //joinApplicantInformationObj.JApp3ContactNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO5"]);

                        joinApplicantInformationObj.JApp3IdType1 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE1"]);
                        joinApplicantInformationObj.JApp3Id1 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO1"]);
                        joinApplicantInformationObj.JApp3IdExpire1 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR1"]);
                        joinApplicantInformationObj.JApp3IdType2 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE2"]);
                        joinApplicantInformationObj.JApp3Id2 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO2"]);
                        joinApplicantInformationObj.JApp3IdExpire2 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR2"]);
                        joinApplicantInformationObj.JApp3IdType3 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE3"]);
                        joinApplicantInformationObj.JApp3Id3 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO3"]);
                        joinApplicantInformationObj.JApp3IdExpire3 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR3"]);
                        joinApplicantInformationObj.JApp3IdType4 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE4"]);
                        joinApplicantInformationObj.JApp3Id4 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO4"]);
                        joinApplicantInformationObj.JApp3IdExpire4 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR4"]);
                        joinApplicantInformationObj.JApp3IdType5 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE5"]);
                        joinApplicantInformationObj.JApp3Id5 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO5"]);
                        joinApplicantInformationObj.JApp3IdExpire5 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR5"]);
                        //End joint applicant 3

                        //Joint applicant 4
                        joinApplicantInformationObj.JointApp4Name = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_NM"]);
                        joinApplicantInformationObj.J4FatherName = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_FATHE_NM"]);
                        joinApplicantInformationObj.J4MotherName = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_MOTHE_NM"]);
                        joinApplicantInformationObj.J4DOB = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_APP_DOB"]);
                        joinApplicantInformationObj.J4TinNo = Convert.ToString(jointApplicantDataTableReaderObj["J4_TIN"]);

                        joinApplicantInformationObj.J4SCBAccNo1 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO1"].ToString());
                        joinApplicantInformationObj.J4SCBAccNo2 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO2"].ToString());
                        joinApplicantInformationObj.J4SCBAccNo3 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO3"].ToString());
                        joinApplicantInformationObj.J4SCBAccNo4 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO4"].ToString());
                        joinApplicantInformationObj.J4SCBAccNo5 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO5"].ToString());

                        joinApplicantInformationObj.J4CrdtCrdNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO"]);
                        joinApplicantInformationObj.J4CrdtCrdNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO2"]);
                        joinApplicantInformationObj.J4CrdtCrdNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO3"]);
                        joinApplicantInformationObj.J4CrdtCrdNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO4"]);
                        joinApplicantInformationObj.J4CrdtCrdNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO5"]);

                        joinApplicantInformationObj.JApp4EducationLevel = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_EDUCA_LEVEL"]);
                        joinApplicantInformationObj.JApp4Company = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_EMPLO"]);

                        joinApplicantInformationObj.JApp4ContactNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO1"]);
                        joinApplicantInformationObj.JApp4ContactNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO2"]);
                        joinApplicantInformationObj.JApp4ContactNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO3"]);
                        joinApplicantInformationObj.JApp4ContactNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO4"]);
                        //joinApplicantInformationObj.JApp4ContactNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO5"]);

                        joinApplicantInformationObj.JApp4IdType1 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE1"]);
                        joinApplicantInformationObj.JApp4Id1 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO1"]);
                        joinApplicantInformationObj.JApp4IdExpire1 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR1"]);
                        joinApplicantInformationObj.JApp4IdType2 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE2"]);
                        joinApplicantInformationObj.JApp4Id2 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO2"]);
                        joinApplicantInformationObj.JApp4IdExpire2 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR2"]);
                        joinApplicantInformationObj.JApp4IdType3 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE3"]);
                        joinApplicantInformationObj.JApp4Id3 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO3"]);
                        joinApplicantInformationObj.JApp4IdExpire3 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR3"]);
                        joinApplicantInformationObj.JApp4IdType4 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE4"]);
                        joinApplicantInformationObj.JApp4Id4 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO4"]);
                        joinApplicantInformationObj.JApp4IdExpire4 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR4"]);
                        joinApplicantInformationObj.JApp4IdType5 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE5"]);
                        joinApplicantInformationObj.JApp4Id5 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO5"]);
                        joinApplicantInformationObj.JApp4IdExpire5 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR5"]);
                        //End joint applicant 4
                    }
                }
            }
            return joinApplicantInformationObj;
        }
        #endregion

        #region InsertORUpdateLoanInfo(LoanInformation loanInformationObj, JoinApplicantInformation joinApplicantInformationObj)
        /// <summary>
        /// This method provide insert or update loan information.
        /// </summary>
        /// <param name="loanInformationObj">Gets loanInformation object.</param>
        /// <param name="joinApplicantInformationObj">Gets joint applicant information object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateLoanInfo(LoanInformation loanInformationObj, User user, DateTime loanStartDateTime)
        {
            int insertORUpdateValue = -1;
            if (!IsExistsLoanInfo(Convert.ToInt32(loanInformationObj.LLId)))
            {
                insertORUpdateValue = InsertLoanAppInformation(loanInformationObj, user, loanStartDateTime);
            }
            else
            {
                insertORUpdateValue = UpdateLoanInfo(loanInformationObj, user, loanStartDateTime);
            }
            return insertORUpdateValue;
        }
        #endregion

        #region InsertLoanAppInformation(LoanInformation loanInformationObj, JoinApplicantInformation joinApplicantInformationObj)
        /// <summary>
        /// This method provide insert operation for loan applicant information.
        /// </summary>
        /// <param name="loanInformationObj">Get loan information object</param>
        /// <param name="joinApplicantInformationObj">Get joint applicant information</param>
        /// <returns>Returns a int value</returns>
        private int InsertLoanAppInformation(LoanInformation loanInformationObj, User user, DateTime loanStartTime)
        {
            int insertRow = -1;
            string insertQueryString = String.Format("INSERT INTO loan_app_info " +
                                                "(LOAN_APPLI_ID, PROD_ID, BR_ID, LOAP_SOURCE_PERSON_NAME, RCV_DT, CUST_NM, LOAN_APP_AMT, " +
                                                "ACC_NO, LOAP_ACNOFORSCB2, LOAP_ACNOFORSCB3, LOAP_ACNOFORSCB4, LOAP_ACNOFORSCB5, LOAP_DECLARED_INCOME, " +
                                                "TIN_NO, CR_CARD_NO_SCB, LOAP_CREDITCARDNO2, LOAP_CREDITCARDNO3, LOAP_CREDITCARDNO4, " +
                                                "LOAP_CREDITCARDNO5, FATHE_NM, MOTHE_NM, DOB, MARIT_STATU, HIGH_EDU_LEVEL, " +
                                                "RES_ADD, MOBIL, RES_PH, OFF_PH, LOAN_CONTACTNO4, LOAN_CONTACTNO5, " +
                                                "PROFE, COMPA_NM, OFF_ADD, MON_CURRE_JOB, SPOUS_PROFE, OTHER_ACC_BANK_ID_1, " + //35
                                                "OTHER_ACC_BR_ID_1, OTHER_ACC_NO_1, OTHER_ACC_TYPE_1, OTHER_ACC_BANK_ID_2, OTHER_ACC_BR_ID_2, " +
                                                "OTHER_ACC_NO_2, OTHER_ACC_TYPE_2, OTHER_ACC_BANK_ID_3, OTHER_ACC_BR_ID_3, OTHER_ACC_NO_3, " +
                                                "OTHER_ACC_TYPE_3, LOAP_OTHERACWITHBANK_ID4, LOAP_OTHERACWITHBRANCH_ID4, LOAP_OTHERACWITHACNO4, " +
                                                "LOAP_ORHREACTYPE_ID4, LOAP_OTHERACWITHBANK_ID5, LOAP_OTHERACWITHBRANCH_ID5, " +
                                                "LOAP_OTHERACWITHACNO5, LOAP_ORHREACTYPE_ID5, ID_TYPE, ID_NO, ID_EXPIR, LOAP_IDTYPE2, " + //58
                                                "LOAP_IDNO2, LOAP_EXPIRYDATE2, LOAP_IDTYPE3, LOAP_IDNO3, LOAP_EXPIRYDATE3, LOAP_IDTYPE4, " +
                                                "LOAP_IDNO4, LOAP_EXPIRYDATE4, LOAP_IDTYPE5, LOAP_IDNO5, LOAP_EXPIRYDATE5, VEHIC_ID, " +
                                                "LOAP_EXTRA_FIELD1, LOAP_EXTRA_FIELD2, LOAP_EXTRA_FIELD3, LOAP_EXTRA_FIELD4, LOAP_EXTRA_FIELD5, " +
                                                "LOAP_EXTRA_FIELD6, LOAP_EXTRA_FIELD7, LOAP_EXTRA_FIELD8, LOAP_EXTRA_FIELD9, LOAP_EXTRA_FIELD10, " + //80
                                                "LOAN_STATU_ID, LOAN_STATU_DT,LOAP_USERID,HOME_OWNER,ARM_Code" +
                                                ") " +
                                                "VALUES( {0}, {1}, {2}, '{3}', '{4}', '{5}', {6}, '{7}', '{8}', '{9}','{10}', '{11}', {12} " +
                                                ", '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}' " +
                                                ", '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', '{31}', '{32}', {33}, '{34}', '{35}' " +
                                                ", '{36}', '{37}', '{38}', '{39}', '{40}', '{41}', '{42}', '{43}', '{44}', '{45}', '{46}', '{47}', '{48}', '{49}', '{50}', '{51}' " +
                                                ", '{52}', '{53}', '{54}', '{55}', '{56}', '{57}', '{58}', '{59}', '{60}', '{61}', '{62}', '{63}', '{64}' " +
                                                ", '{65}', '{66}', '{67}', '{68}', '{69}', {70}, '{71}', '{72}', '{73}', '{74}', '{75}', '{76}', '{77}', '{78}' " +
                                                ", '{79}', '{80}' " +
                                                ", 4 , getdate(),'{81}','{82}','{83}'" +
                                                ") ",
                                                loanInformationObj.LLId, loanInformationObj.ProductId, loanInformationObj.SourceId, loanInformationObj.SourcePersonName.Replace("'", "''")
                                                , loanInformationObj.RecieveDate.ToString("yyyy-MM-dd").Replace("'", "''"), loanInformationObj.ApplicantName.Replace("'", "''")
                                                , loanInformationObj.AppliedAmount, loanInformationObj.AccountNo.MasterAccNo1.Replace("'", "''"), loanInformationObj.AccountNo.MasterAccNo2.Replace("'", "''")
                                                , loanInformationObj.AccountNo.MasterAccNo3.Replace("'", "''"), loanInformationObj.AccountNo.MasterAccNo4.Replace("'", "''"), loanInformationObj.AccountNo.MasterAccNo5.Replace("'", "''")
                                                , loanInformationObj.DeclaredAmount, loanInformationObj.TINNo.Replace("'", "''"), loanInformationObj.CreditCardNo1.Replace("'", "''")
                                                , loanInformationObj.CreditCardNo2.Replace("'", "''"), loanInformationObj.CreditCardNo3.Replace("'", "''"), loanInformationObj.CreditCardNo4.Replace("'", "''"), loanInformationObj.CreditCardNo5.Replace("'", "''")
                                                , loanInformationObj.FatherName.Replace("'", "''"), loanInformationObj.MotherName.Replace("'", "''"), loanInformationObj.DOB.ToString("yyyy-MM-dd").Replace("'", "''")
                                                , loanInformationObj.MaritalStatus.Replace("'", "''"), loanInformationObj.EducationalLevel.Replace("'", "''")
                                                , loanInformationObj.ResidentAddress.Replace("'", "''"), loanInformationObj.ContactNumber.ContactNo1.Replace("'", "''"), loanInformationObj.ContactNumber.ContactNo2.Replace("'", "''")
                                                , loanInformationObj.ContactNumber.ContactNo3.Replace("'", "''"), loanInformationObj.ContactNumber.ContactNo4.Replace("'", "''"), loanInformationObj.ContactNumber.ContactNo5.Replace("'", "''")
                                                , loanInformationObj.Profession.Replace("'", "''"), loanInformationObj.Business.Replace("'", "''"), loanInformationObj.OfficeAddress.Replace("'", "''")
                                                , loanInformationObj.JobDuration, loanInformationObj.SpousOcupation.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId1
                                                , loanInformationObj.OtherBankAccount.OtherBranchId1, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo1)
                                                , loanInformationObj.OtherBankAccount.OtherAccType1.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId2
                                                , loanInformationObj.OtherBankAccount.OtherBranchId2, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo2)
                                                , loanInformationObj.OtherBankAccount.OtherAccType2.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId3
                                                , loanInformationObj.OtherBankAccount.OtherBranchId3, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo3)
                                                , loanInformationObj.OtherBankAccount.OtherAccType3.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId4
                                                , loanInformationObj.OtherBankAccount.OtherBranchId4, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo4)
                                                , loanInformationObj.OtherBankAccount.OtherAccType4.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId5
                                                , loanInformationObj.OtherBankAccount.OtherBranchId5, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo5)
                                                , loanInformationObj.OtherBankAccount.OtherAccType5.Replace("'", "''"), loanInformationObj.ApplicantId.Type1.Replace("'", "''")
                                                , loanInformationObj.ApplicantId.Id1.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate1.ToString("yyyy-MM-dd").Replace("'", "''")
                                                , loanInformationObj.ApplicantId.Type2.Replace("'", "''"), loanInformationObj.ApplicantId.Id2.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate2.ToString("yyyy-MM-dd").Replace("'", "''")
                                                , loanInformationObj.ApplicantId.Type3.Replace("'", "''"), loanInformationObj.ApplicantId.Id3.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate3.ToString("yyyy-MM-dd").Replace("'", "''")
                                                , loanInformationObj.ApplicantId.Type4.Replace("'", "''"), loanInformationObj.ApplicantId.Id4.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate4.ToString("yyyy-MM-dd").Replace("'", "''")
                                                , loanInformationObj.ApplicantId.Type5.Replace("'", "''"), loanInformationObj.ApplicantId.Id5.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate5.ToString("yyyy-MM-dd").Replace("'", "''")
                                                , loanInformationObj.VechileId, loanInformationObj.ExtraField1.Replace("'", "''"), loanInformationObj.ExtraField2.Replace("'", "''")
                                                , loanInformationObj.ExtraField3.Replace("'", "''"), loanInformationObj.ExtraField4.Replace("'", "''"), loanInformationObj.ExtraField5.Replace("'", "''")
                                                , loanInformationObj.ExtraField6.Replace("'", "''"), loanInformationObj.ExtraField7.Replace("'", "''"), loanInformationObj.ExtraField8.Replace("'", "''")
                                                , loanInformationObj.ExtraField9.Replace("'", "''"), loanInformationObj.ExtraField10.Replace("'", "''")
                                                , loanInformationObj.EntryUserId, loanInformationObj.ResidentialStatus, loanInformationObj.ARMCode
                                               );

            string updateLocTable = string.Format(@"UPDATE a SET a.LOAN_STATUS_ID=4 FROM loc_loan_app_info a WHERE a.LOAN_APPL_ID='{0}'", loanInformationObj.LLId);


            DateTime now = DateTime.Now;
            string sql = string.Format(@"
INSERT INTO [dbo].[loan_app_details]
           ([LOAN_APPL_ID]
           ,[PERS_ID]
           ,[DEPT_ID]
           ,[DATE_TIME]
           ,[PROCESS_STATUS_ID]
           ,[REMARKS]
           ,[TAT]
           ,[MakerId]
           ,[RoleId])
     VALUES
           ({0}
           ,'{1}'
           ,'{5}'
           ,'{2}'
           ,4
           ,'Information updated'
           ,'{3}'
           ,'{4}'
           ,'{5}')"
            , loanInformationObj.LLId
            , user.UserId
            , DateTime.Now
            , (now - loanStartTime).Days
            , user.UserId
            , user.UserLevel);
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(insertQueryString);
                if (insertRow > 0)
                {
                    DbQueryManager.ExecuteNonQuery(updateLocTable);
                    DbQueryManager.ExecuteNonQuery(sql);
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }
        #endregion

        #region UpdateLoanInfo(LoanInformation loanInformationObj)
        /// <summary>
        /// This method provide update operation for loan applicant information
        /// </summary>
        /// <param name="loanInformationObj">Gets loan information object</param>
        /// <param name="joinApplicantInformationObj">Gets joint information object</param>
        /// <returns>Returns a positive value if updated.</returns>
        private int UpdateLoanInfo(LoanInformation loanInformationObj, User user, DateTime loanStartTime)
        {
            int updateRow = -1;

            string updateQueryString = String.Format("UPDATE loan_app_info SET " +
                        "PROD_ID = {0} , BR_ID = {1}, LOAP_SOURCE_PERSON_NAME = '{2}' , " +
                        "RCV_DT = '{3}' , CUST_NM = '{4}' , LOAN_APP_AMT = {5} , " +
                        "ACC_NO = '{6}' , LOAP_ACNOFORSCB2 ='{7}' , LOAP_ACNOFORSCB3 = '{8}' , " +
                        "LOAP_ACNOFORSCB4 = '{9}' , LOAP_ACNOFORSCB5 = '{10}' , " +
                        "LOAP_DECLARED_INCOME = {11} , TIN_NO = '{12}' , " +
                        "CR_CARD_NO_SCB = '{13}' , LOAP_CREDITCARDNO2 ='{14}' , LOAP_CREDITCARDNO3 = '{15}' , " +
                        "LOAP_CREDITCARDNO4 = '{16}' , LOAP_CREDITCARDNO5 = '{17}' , " +
                        "FATHE_NM = '{18}' , MOTHE_NM = '{19}' , DOB = '{20}' , MARIT_STATU = '{21}' , " +
                        "HIGH_EDU_LEVEL = '{22}' , RES_ADD = '{23}' , MOBIL = '{24}' , RES_PH = '{25}' , " +
                        "OFF_PH = '{26}' , LOAN_CONTACTNO4 = '{27}' , LOAN_CONTACTNO5 = '{28}' , " +
                        "PROFE = '{29}' , COMPA_NM = '{30}' , OFF_ADD = '{31}' , MON_CURRE_JOB = {32} , " +
                        "SPOUS_PROFE = '{33}' , OTHER_ACC_BANK_ID_1 = {34} , OTHER_ACC_BR_ID_1 = {35} , " +
                        "OTHER_ACC_NO_1 = '{36}' , OTHER_ACC_TYPE_1 = '{37}' , OTHER_ACC_BANK_ID_2 = {38} , " +
                        "OTHER_ACC_BR_ID_2 = {39} , OTHER_ACC_NO_2 = '{40}' , OTHER_ACC_TYPE_2 = '{41}' , " +
                        "OTHER_ACC_BANK_ID_3 = {42} , OTHER_ACC_BR_ID_3 = {43} , OTHER_ACC_NO_3 = '{44}' , OTHER_ACC_TYPE_3 = '{45}' , " +
                        "LOAP_OTHERACWITHBANK_ID4 = {46} , LOAP_OTHERACWITHBRANCH_ID4 = {47} , LOAP_OTHERACWITHACNO4 = '{48}' , " +
                        "LOAP_ORHREACTYPE_ID4 = '{49}' , LOAP_OTHERACWITHBANK_ID5 = {50} , LOAP_OTHERACWITHBRANCH_ID5 = {51} , " +
                        "LOAP_OTHERACWITHACNO5 = '{52}' , LOAP_ORHREACTYPE_ID5 = '{53}' , ID_TYPE = '{54}' , ID_NO = '{55}' , ID_EXPIR  = '{56}' , " +
                        "LOAP_IDTYPE2 = '{57}' , LOAP_IDNO2 = '{58}' ,  LOAP_EXPIRYDATE2 = '{59}' , " +
                        "LOAP_IDTYPE3 = '{60}' , LOAP_IDNO3 = '{61}' ,  LOAP_EXPIRYDATE3 = '{62}' , " +
                        "LOAP_IDTYPE4 = '{63}' , LOAP_IDNO4 = '{64}' ,  LOAP_EXPIRYDATE4 = '{65}' , " +
                        "LOAP_IDTYPE5 = '{66}' , LOAP_IDNO5 = '{67}' ,  LOAP_EXPIRYDATE5 = '{68}' , " +
                        "VEHIC_ID = {69} , LOAP_EXTRA_FIELD1 = '{70}' , LOAP_EXTRA_FIELD2 = '{71}' , LOAP_EXTRA_FIELD3 = '{72}' , " +
                        "LOAP_EXTRA_FIELD4 = '{73}' , LOAP_EXTRA_FIELD5 = '{74}' , LOAP_EXTRA_FIELD6 = '{75}' , LOAP_EXTRA_FIELD7 = '{76}' , " +
                        "LOAP_EXTRA_FIELD8 = '{77}' , LOAP_EXTRA_FIELD9 = '{78}' , LOAP_EXTRA_FIELD10 = '{79}' , " + //End loan applicant info.
                        "HOME_OWNER = '{80}', LOAP_USERID='{81}', ARM_Code='{83}', DELI_DEPT_ID='{84}'" +
                        "WHERE LOAN_APPLI_ID = {82} "
                        , loanInformationObj.ProductId, loanInformationObj.SourceId, loanInformationObj.SourcePersonName.Replace("'", "''")
                        , loanInformationObj.RecieveDate.ToString("yyyy-MM-dd").Replace("'", "''"), loanInformationObj.ApplicantName.Replace("'", "''")
                        , loanInformationObj.AppliedAmount, loanInformationObj.AccountNo.MasterAccNo1.Replace("'", "''")
                        , loanInformationObj.AccountNo.MasterAccNo2.Replace("'", "''"), loanInformationObj.AccountNo.MasterAccNo3.Replace("'", "''")
                        , loanInformationObj.AccountNo.MasterAccNo4.Replace("'", "''"), loanInformationObj.AccountNo.MasterAccNo5.Replace("'", "''")
                        , loanInformationObj.DeclaredAmount, loanInformationObj.TINNo.Replace("'", "''"), loanInformationObj.CreditCardNo1.Replace("'", "''")
                        , loanInformationObj.CreditCardNo2.Replace("'", "''"), loanInformationObj.CreditCardNo3.Replace("'", "''")
                        , loanInformationObj.CreditCardNo4.Replace("'", "''"), loanInformationObj.CreditCardNo5.Replace("'", "''")
                        , loanInformationObj.FatherName.Replace("'", "''"), loanInformationObj.MotherName.Replace("'", "''")
                        , loanInformationObj.DOB.ToString("yyyy-MM-dd").Replace("'", "''"), loanInformationObj.MaritalStatus.Replace("'", "''")
                        , loanInformationObj.EducationalLevel.Replace("'", "''"), loanInformationObj.ResidentAddress.Replace("'", "''")
                        , loanInformationObj.ContactNumber.ContactNo1.Replace("'", "''"), loanInformationObj.ContactNumber.ContactNo2.Replace("'", "''")
                        , loanInformationObj.ContactNumber.ContactNo3.Replace("'", "''"), loanInformationObj.ContactNumber.ContactNo4.Replace("'", "''")
                        , loanInformationObj.ContactNumber.ContactNo5.Replace("'", "''"), loanInformationObj.Profession.Replace("'", "''")
                        , loanInformationObj.Business.Replace("'", "''"), loanInformationObj.OfficeAddress.Replace("'", "''")
                        , loanInformationObj.JobDuration, loanInformationObj.SpousOcupation.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId1
                        , loanInformationObj.OtherBankAccount.OtherBranchId1, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo1)
                        , loanInformationObj.OtherBankAccount.OtherAccType1.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId2
                        , loanInformationObj.OtherBankAccount.OtherBranchId2, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo2)
                        , loanInformationObj.OtherBankAccount.OtherAccType2.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId3
                        , loanInformationObj.OtherBankAccount.OtherBranchId3, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo3)
                        , loanInformationObj.OtherBankAccount.OtherAccType3.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId4
                        , loanInformationObj.OtherBankAccount.OtherBranchId4, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo4)
                        , loanInformationObj.OtherBankAccount.OtherAccType4.Replace("'", "''"), loanInformationObj.OtherBankAccount.OtherBankId5
                        , loanInformationObj.OtherBankAccount.OtherBranchId5, EscapeCharacter(loanInformationObj.OtherBankAccount.OtherAccNo5)
                        , loanInformationObj.OtherBankAccount.OtherAccType5.Replace("'", "''"), loanInformationObj.ApplicantId.Type1.Replace("'", "''")
                        , loanInformationObj.ApplicantId.Id1.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate1.ToString("yyyy-MM-dd").Replace("'", "''")
                        , loanInformationObj.ApplicantId.Type2.Replace("'", "''"), loanInformationObj.ApplicantId.Id2.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate2.ToString("yyyy-MM-dd").Replace("'", "''")
                        , loanInformationObj.ApplicantId.Type3.Replace("'", "''"), loanInformationObj.ApplicantId.Id3.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate3.ToString("yyyy-MM-dd").Replace("'", "''")
                        , loanInformationObj.ApplicantId.Type4.Replace("'", "''"), loanInformationObj.ApplicantId.Id4.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate4.ToString("yyyy-MM-dd").Replace("'", "''")
                        , loanInformationObj.ApplicantId.Type5.Replace("'", "''"), loanInformationObj.ApplicantId.Id5.Replace("'", "''"), loanInformationObj.ApplicantId.ExpiryDate5.ToString("yyyy-MM-dd").Replace("'", "''")
                        , loanInformationObj.VechileId, loanInformationObj.ExtraField1.Replace("'", "''"), loanInformationObj.ExtraField2.Replace("'", "''")
                        , loanInformationObj.ExtraField3.Replace("'", "''"), loanInformationObj.ExtraField4.Replace("'", "''"), loanInformationObj.ExtraField5.Replace("'", "''")
                        , loanInformationObj.ExtraField6.Replace("'", "''"), loanInformationObj.ExtraField7.Replace("'", "''"), loanInformationObj.ExtraField8.Replace("'", "''")
                        , loanInformationObj.ExtraField9.Replace("'", "''"), loanInformationObj.ExtraField10.Replace("'", "''")
                        , loanInformationObj.ResidentialStatus, loanInformationObj.EntryUserId, loanInformationObj.LLId
                        , loanInformationObj.ARMCode, user.Type);

            string updateLocTable = string.Format(@"UPDATE a SET a.LOAN_STATUS_ID=4 FROM loc_loan_app_info a WHERE a.LOAN_APPL_ID='{0}'", loanInformationObj.LLId);

            DateTime now = DateTime.Now;
            string sql = string.Format(@"
INSERT INTO [dbo].[loan_app_details]
           ([LOAN_APPL_ID]
           ,[PERS_ID]
           ,[DEPT_ID]
           ,[DATE_TIME]
           ,[PROCESS_STATUS_ID]
           ,[REMARKS]
           ,[TAT]
           ,[MakerId]
           ,[RoleId])
     VALUES
           ({0}
           ,'{1}'
           ,'{5}'
           ,'{2}'
           ,4
           ,'Information updated'
           ,'{3}'
           ,'{4}'
           ,'{5}')"
            , loanInformationObj.LLId
            , user.UserId
            , DateTime.Now
            , (now - loanStartTime).Days
            , user.UserId
            , user.Type);

            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(updateQueryString);

                if (updateRow > 0)
                {
                    DbQueryManager.ExecuteNonQuery(updateLocTable);
                    DbQueryManager.ExecuteNonQuery(sql);
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
        #endregion

        #region IsExistsLoanInfo(Int32 llid)
        /// <summary>
        /// This method provide loan information exists or not.
        /// </summary>
        /// <param name="llid">Get LLID</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsLoanInfo(Int32 llid)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM loan_app_info WHERE LOAN_APPLI_ID = {0}", llid);
            object locationInfoObj;
            try
            {
                locationInfoObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(locationInfoObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region GetAllRemarks
        /// <summary>
        /// This method select all remarks depends on user.
        /// </summary>
        /// <returns>Returns remarks list object</returns>
        public List<Remarks> GetAllRemarks()
        {
            List<Remarks> remarksListObj = new List<Remarks>();
            string queryString = "SELECT REMA_ID, REMA_PROD_ID, REMA_NAME, REMA_NO, REMA_FOR " +
                                 "FROM t_pluss_remarks WHERE REMA_FOR = 1 AND REMA_STATUS = 1";
            DataTable remarksDataTableObj = DbQueryManager.GetTable(queryString);
            if (remarksDataTableObj != null)
            {
                DataTableReader remarksDataTableReaderObj = remarksDataTableObj.CreateDataReader();
                if (remarksDataTableReaderObj != null)
                {
                    while (remarksDataTableReaderObj.Read())
                    {
                        Remarks remarksObj = new Remarks();
                        remarksObj.Id = Convert.ToInt32(remarksDataTableReaderObj["REMA_ID"].ToString());
                        remarksObj.Name = remarksDataTableReaderObj["REMA_NAME"].ToString();
                        remarksListObj.Add(remarksObj);
                    }
                    remarksDataTableReaderObj.Close();
                }
                remarksDataTableObj.Clear();
            }
            return remarksListObj;
        }
        #endregion

        #region SetForwardStatus
        /// <summary>
        /// This method update the status flag
        /// </summary>
        /// <param name="llid">Gets a LLID.</param>
        /// <param name="statusId">Gets a status id.</param>
        /// <returns>Returns a positive value if updated the flaq.</returns>
        public int SetForwardStatus(Int32 llid, Int32 statusId, string loginUserId)
        {
            int updateRow = -1;
            int updateRow2 = -1;
            string userDeptId = "";
            string mSQL = "";
            Int32 checkResult = 0;
            string checkDepartmentId = "";
            string checkStatusId = "";
            if (statusId == 2)
            {
                checkDepartmentId = "4";
                checkStatusId = "4";
                mSQL = String.Format("SELECT ORIG_DEPT_ID FROM loc_loan_app_info WHERE LOAN_APPL_ID={0}", llid);
                checkResult = check4LoanProcessStatus(llid, checkStatusId, checkDepartmentId);
            }
            else
            {
                checkDepartmentId = "1,4,5";
                checkStatusId = "4,8,10,11";
                //mSQL = String.Format("SELECT LOAN_DEPT_ID FROM learner_details_table_old WHERE USERNAME='{0}'", loginUserId);
                mSQL = String.Format("SELECT USER_LEVEL FROM t_pluss_user a WHERE a.USER_CODE='{0}'", loginUserId);
                checkResult = check4LoanProcessStatus(llid, checkStatusId, checkDepartmentId);
            }
            //userDeptId = dbMySQL.DbGetValue(mSQL, "conStringLoanLocator");
            userDeptId = DbQueryManager.ExecuteScalar(mSQL).ToString();

            string queryString = String.Format("UPDATE loan_app_info SET DELI_DEPT_ID= {2},LOAN_STATU_DT = getdate() , LOAN_STATU_ID = {1} " +
                                               "WHERE LOAN_APPLI_ID = {0}", llid, statusId, userDeptId);
            string queryString2 = String.Format("UPDATE loc_loan_app_info SET DELI_DEPT_ID= {2},LOAN_STATUS_ID = {1} " +
                                               "WHERE LOAN_APPL_ID = {0}", llid, statusId, userDeptId);
            if (checkResult > 0)
            {
                try
                {
                    updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                    //updateRow2 = DbQueryManager.ExecuteNonQuery(queryString2, "conStringLoanLocator");
                    updateRow2 = DbQueryManager.ExecuteNonQuery(queryString2);
                    if (updateRow > -1 && updateRow2 > -1)
                    {
                        return (int)InsertUpdateFlag.STATUSUPDATE;
                    }
                    else
                    {
                        return (int)InsertUpdateFlag.STATUSUPDATEERROR;
                    }
                }
                catch
                {
                    return (int)InsertUpdateFlag.STATUSUPDATEERROR;
                }
            }
            else
            {
                return (int)InsertUpdateFlag.STATUSUPDATEERROR;
            }
        }
        #endregion
        public Int32 check4LoanProcessStatus(Int32 LLId, string loanStatus, string departmentId)
        {
            Int32 returnLLId = 0;
            //string queryString = "SELECT top 1 LOAN_APPL_ID FROM loc_loan_app_info WHERE LOAN_APPL_ID='" + LLId.ToString() + "' AND LOAN_STATUS_ID IN (" + loanStatus.ToString() + ") AND DELI_DEPT_ID IN (" + departmentId.ToString() + ")";
            string queryString = "SELECT top 1 LOAN_APPL_ID FROM loc_loan_app_info WHERE LOAN_APPL_ID='" + LLId.ToString() + "'-- AND DELI_DEPT_ID IN (" + departmentId.ToString() + ")";
            try
            {
                //returnLLId = Convert.ToInt32(dbMySQL.DbGetValue(queryString, "conStringLoanLocator"));
                returnLLId = Convert.ToInt32(DbQueryManager.ExecuteScalar(queryString));
            }
            catch
            {
                returnLLId = 0;
            }
            return returnLLId;
        }
        //public int SetForwardStatus(Int32 llid, Int32 statusId)
        //{
        //    int updateRow = -1;
        //    int updateRow2 = -1;
        //    string queryString = String.Format("UPDATE loan_app_info SET LOAN_STATU_DT = NOW() , LOAN_STATU_ID = {1} " +
        //                                       "WHERE LOAN_APPLI_ID = {0}", llid, statusId);
        //    string queryString2 = String.Format("UPDATE loan_app_info SET LOAN_STATUS_ID = {1} " +
        //                                       "WHERE LOAN_APPL_ID = {0}", llid, statusId);
        //    try
        //    {
        //        updateRow = DbQueryManager.ExecuteNonQuery(queryString);
        //        updateRow2 = DbQueryManager.ExecuteNonQuery(queryString2, "conStringLoanLocator");
        //        if (updateRow > -1 && updateRow2 > -1)
        //        {
        //            return (int)InsertUpdateFlag.STATUSUPDATE;
        //        }
        //        else
        //        {
        //            return (int)InsertUpdateFlag.STATUSUPDATEERROR;
        //        }
        //    }
        //    catch
        //    {
        //        return (int)InsertUpdateFlag.STATUSUPDATEERROR;
        //    }
        //}
        #region SetDeferStatus
        /// <summary>
        /// This method update the status flag
        /// </summary>
        /// <param name="llid">Gets a LLID.</param>
        /// <returns>Returns a positive value if updated defer flaq.</returns>
        public int SetDeferStatus(Int32 llid)
        {
            int updateRow = -1;
            string queryString = String.Format("UPDATE loan_app_info SET LOAN_STATU_DT = NOW() , LOAN_STATU_ID = 4 " +
                                               "WHERE LOAN_APPLI_ID = {0}", llid);

            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (updateRow > 0)
                {
                    return (int)InsertUpdateFlag.STATUSUPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.STATUSUPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.STATUSUPDATEERROR;
            }
        }
        #endregion

        #region DeleteDeferReason
        /// <summary>
        /// This method delete defer reason.
        /// </summary>
        /// <param name="llid">Gets LLID</param>
        /// <returns>Return a positive value if sucess.</returns>
        public int DeleteDeferReason(Int32 llid)
        {
            int deleteRow = -1;
            string queryString = String.Format("DELETE FROM loan_dec_def_reason_info WHERE LOAN_APPL_ID = {0}", llid);

            try
            {
                //deleteRow = DbQueryManager.ExecuteNonQuery(queryString, "conStringLoanLocator");
                deleteRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (deleteRow > 0)
                {
                    return (int)InsertUpdateFlag.DELETE;
                }
                else
                {
                    return (int)InsertUpdateFlag.DELETEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.DELETEERROR;
            }
        }
        #endregion

        #region InsertDeferReason
        /// <summary>
        /// This method insert defer reason.
        /// </summary>
        /// <param name="llid">Gets LLID</param>
        /// <param name="reasonId">Gets list of reason id</param>
        /// <returns>Positive value if sucess.</returns>
        public int InsertDeferReason(Int32 llid, List<Int32> reasonId)
        {
            int insertRow = -1;
            string queryString = "INSERT INTO loan_dec_def_reason_info (LOAN_APPL_ID, REAS_ID) VALUES ";
            for (int i = 0; i < reasonId.Count; i++)
            {
                queryString += "(" + llid + "," + reasonId[i] + "),";
            }
            queryString = queryString.Substring(0, queryString.Length - 1);
            try
            {
                //insertRow = DbQueryManager.ExecuteNonQuery(queryString, "conStringLoanLocator");
                insertRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (insertRow > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }
        #endregion

        #region InsertDeferReasonDetails
        /// <summary>
        /// This method insert defer reason details.
        /// </summary>
        /// <param name="loanApplicationDetailsObj">Gets a loanApplicationDetails object.</param>
        /// <param name="remarks">Gets a remarks list.</param>
        /// <returns>Positive values if sucess.</returns>
        public int InsertDeferReasonDetails(LoanApplicationDetails loanApplicationDetailsObj, List<Remarks> remarks)
        {
            int insertRow = -1;
            string sRemarks = string.Empty;
            int iRemarksId = 0;
            //string mSQL = "SELECT ID_LEARNER FROM learner_details_table WHERE USERNAME='" + loanApplicationDetailsObj.PersonId.ToString() + "'";
            string mSQL = "SELECT USER_CODE FROM t_pluss_user WHERE USER_CODE='" + loanApplicationDetailsObj.PersonId.ToString() + "'";
            //string lonaLocatorUserId = DbQueryManager.ExecuteScalar(mSQL, "conStringLoanLocator").ToString();
            string lonaLocatorUserId = DbQueryManager.ExecuteScalar(mSQL).ToString();
            string queryString = "INSERT INTO loan_app_details (LOAN_APPL_ID, PERS_ID, DEPT_ID, DATE_TIME, PROCESS_STATUS_ID, REMARKS, DEFE_DECL_REAS_ID,TAT, MakerId) VALUES  ";
            if (lonaLocatorUserId.Length == 0 || lonaLocatorUserId == null || lonaLocatorUserId == "")
            {
                lonaLocatorUserId = "0";
            }
            if (remarks.Count == 0)
            {
                queryString += "(" + loanApplicationDetailsObj.LAId + ", '" + lonaLocatorUserId + "'," + loanApplicationDetailsObj.DeptId + ", getdate()," + loanApplicationDetailsObj.ProcessStatusId + ",'',0,datediff(day,('" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:s") + "'),('" + GetMaxDateTime(loanApplicationDetailsObj.LAId).ToString("yyyy-MM-dd HH:mm:s") + "')),'"+lonaLocatorUserId+"')";
            }
            else
            {
                for (int i = 0; i < remarks.Count; i++)
                {
                    sRemarks += remarks[i].Name + ",";
                }
                sRemarks = sRemarks.Substring(0, sRemarks.Length - 1);
                queryString += "(" + loanApplicationDetailsObj.LAId + ", '" + lonaLocatorUserId + "'," + loanApplicationDetailsObj.DeptId + ", getdate()," + loanApplicationDetailsObj.ProcessStatusId + ",'" + sRemarks.Substring(0, sRemarks.Length - 1) + "'," + iRemarksId + ",datediff(day,('" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:s") + "'),('" + GetMaxDateTime(loanApplicationDetailsObj.LAId).ToString("yyyy-MM-dd HH:mm:s") + "')),'" + lonaLocatorUserId + "')";
            }
            try
            {
                //insertRow = DbQueryManager.ExecuteNonQuery(queryString, "conStringLoanLocator");
                insertRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (insertRow > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }
        #endregion
        public DateTime GetMaxDateTime(int LLId)
        {
            DateTime? maxDateTime = new DateTime(2015, 12, 25);
            string queryString = "SELECT top 1 MAX(DATE_TIME) as DATE_TIME FROM loan_app_details WHERE LOAN_APPL_ID='" + LLId.ToString() + "'";
            //DateTime maxDateTime = Convert.ToDateTime(DbQueryManager.GetTable(queryString, "conStringLoanLocator"));
            DataTable table = DbQueryManager.GetTable(queryString);
            if (!Convert.IsDBNull(table.Rows[0][0]))
            {
                maxDateTime = Convert.ToDateTime(table.Rows[0][0]);
            }
            return Convert.ToDateTime(maxDateTime);
        }
        #region GetAllDeferReasonId
        /// <summary>
        /// This method populate all reasons id.
        /// </summary>
        /// <param name="llid">Gets a LLID.</param>
        /// <returns>Reason id list object.</returns>
        public List<Int32> GetAllDeferReasonId(Int32 llid)
        {
            string queryString = String.Format("SELECT LOAN_APPL_ID, REAS_ID FROM loan_dec_def_reason_info WHERE LOAN_APPL_ID = {0}", llid);
            List<Int32> reasonIdListObj = new List<Int32>();
            DataTable reasonIdDataTableObj = DbQueryManager.GetTable(queryString, "conStringLoanLocator");
            if (reasonIdDataTableObj != null)
            {
                DataTableReader reasonIdDataTableReaderObj = reasonIdDataTableObj.CreateDataReader();
                if (reasonIdDataTableReaderObj != null)
                {
                    while (reasonIdDataTableReaderObj.Read())
                    {
                        Int32 reasonIdObj = new Int32();
                        reasonIdObj = Convert.ToInt32(reasonIdDataTableReaderObj["REAS_ID"].ToString());
                        //remarksObj.Name = reasonIdDataTableReaderObj["REMA_NAME"].ToString();
                        reasonIdListObj.Add(reasonIdObj);
                    }
                    reasonIdDataTableReaderObj.Close();
                }
                reasonIdDataTableObj.Clear();
            }
            return reasonIdListObj;
        }
        #endregion

        #region EscapeCharacter
        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                //fieldString = fieldString.Replace(
                returnMessage = fieldString;
            }
            return returnMessage;
        }
        #endregion

        public DataTable GetLoanStates()
        {
            try
            {
                var sql = "select LOAN_STATUS_ID as LoanState,LOAN_STATUS_NAME as StateName from loan_status_info";
                return DbQueryManager.GetTable(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetDepartment()
        {
            try
            {
                var query = "select LOAN_DEPT_ID,LOAN_DEPT_NAME,LOAN_DEPT_TYPE from loan_dept_info";
                return DbQueryManager.GetTable(query);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int InsertORUpdateProductInfo(Product product)
        {

            int insertORUpdateValue = 0;
            //if (!IsExistsProductInfo(Convert.ToInt32(product.IdLearner), (int)product.ProductId))
            //{
            //    insertORUpdateValue = InsertProductInformation(product);
            //}
            //else
            //{
            //    insertORUpdateValue = UpdateProductInformation(product);
            //}
            insertORUpdateValue = UpdateProductInformation(product);
            return insertORUpdateValue;
        }

        private int UpdateProductInformation(Product product)
        {
            int action_id = 2; // action_id=2 is UPDATE action.
            int module_id = 2;  // module_id=2 is PRODUCT module.
            string queryString = String.Format("UPDATE Product_Info SET " +
                                               " SUPERVISOR = {0} " +
                                               "WHERE ID = {1} ",
                                               product.IdLearner, product.ProductId);
            Audit(action_id, module_id, queryString, product);
            try
            {
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)

                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch
            {
                return ERROR;
            }
        }

        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;

        private int InsertProductInformation(Product product)
        {
            int action_id = 1; // action_id=2 is UPDATE action.
            int module_id = 2;  // module_id=2 is PRODUCT module.
            string queryString = String.Format("INSERT INTO Product_Info ( SUPERVISOR) " +
                                               "VALUES( '{0}') ",
                                                product.IdLearner);
            Audit(action_id, module_id, queryString, product);

            try
            {
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch
            {
                return ERROR;
            }
        }

        public bool IsExistsProductInfo(int id, int prodId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*)as Count FROM Product_Info WHERE PROD_ID={0}", prodId);
            try
            {
                var banks = DbQueryManager.GetTable(queryString);

                if (Convert.ToInt32(banks.Rows[0][0]) > 0)
                {
                    returnValue = true;
                }
                else
                {
                    returnValue = false;
                }
            }
            catch
            {
                returnValue = false;
            }
            return returnValue;
        }
        public void Audit(int action_id, int module_id, string str_sql1, Product product)
        {
            string query = "insert into audit_trail(ID_LEARNER,ACTION_ID,MODULE_ID,QRY_DATE,QRY_TIME,QRY_STRING) " +
                          "values (" + product.UserId + "," + action_id + "," + module_id + ", CAST(getdate() as DATE) ,CAST (getdate() as Time),'" + AddSlash(str_sql1) + "')";
            DbQueryManager.ExecuteNonQuery(query);
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        #region Get All CheckList

        public List<CheckList> GetAllCheckLists()
        {
            List<CheckList> checkListsObj = new List<CheckList>();
            DataTable dataTable = new DataTable();
            string query = "select CHECK_LIST_ID,CHECK_LIST_TITLE,p.PROD_ID,PROD_NAME,STATUS,(CASE STATUS WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' END) StatusMsg FROM check_list_info c LEFT JOIN  t_pluss_product p ON p.PROD_ID = c.PROD_ID";
            try
            {
                dataTable = DbQueryManager.GetTable(query);
                if (dataTable != null)
                {
                    DataTableReader dataTableReader = dataTable.CreateDataReader();
                    if (dataTableReader != null)
                    {
                        while (dataTableReader.Read())
                        {
                            CheckList checkList = new CheckList();
                            checkList.Id = (int)dataTableReader["CHECK_LIST_ID"];
                            checkList.Title = dataTableReader["CHECK_LIST_TITLE"].ToString();
                            checkList.ProductId = (int)dataTableReader["PROD_ID"];
                            checkList.ProductName = dataTableReader["PROD_NAME"].ToString();
                            checkList.Status = (int)dataTableReader["STATUS"];
                            checkList.StatusMsg = (string)dataTableReader["StatusMsg"];
                            checkListsObj.Add(checkList);
                        }
                        dataTableReader.Close();
                    }
                    dataTable.Clear();
                }
            }
            catch (Exception ex)
            {

                string msg = ex.Message;
                string msg2 = ex.InnerException.Message;
            }
            return checkListsObj;
        }

        #endregion

        #region InsertORUpdateCheckList
        public int InsertORUpdateCheckList(CheckList checkList)
        {
            int insertORUpdateValue = -1;
            if (!IsExistsCheckList(checkList.Id))
            {
                insertORUpdateValue = InsertCheckList(checkList);
            }
            else
            {
                insertORUpdateValue = UpdateCheckList(checkList);
            }
            return insertORUpdateValue;
        }

        public int InsertCheckList(CheckList checkList)
        {
            int insertRow = -1;
            string query =
                "insert into check_list_info(CHECK_LIST_TITLE ,PROD_ID,STATUS ) values('" + checkList.Title + "'," + checkList.ProductId + "," + checkList.Status + ")";
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(query);
                if (insertRow > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }

        public int UpdateCheckList(CheckList checkList)
        {
            int updateRow = -1;
            string query =
                "update check_list_info set CHECK_LIST_TITLE='" + checkList.Title + "',PROD_ID=" + checkList.ProductId + ",STATUS=" + checkList.Status + " where CHECK_LIST_ID=" + checkList.Id + "";
            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(query);
                if (updateRow > 0)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
        public bool IsExistsCheckList(int id)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM check_list_info WHERE CHECK_LIST_ID = {0}", id);
            object checkObj;
            try
            {
                checkObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(checkObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }

        public int DeleteCheckList(int id)
        {
            int deleteRow = -1;
            string query = "DELETE FROM  check_list_info WHERE CHECK_LIST_ID=" + id;
            deleteRow = DbQueryManager.ExecuteNonQuery(query);
            if (deleteRow > 0)
            {
                return (int)InsertUpdateFlag.DELETE;
            }
            else
            {
                return (int)InsertUpdateFlag.DELETEERROR;
            }
        }


        #endregion
    }
}
