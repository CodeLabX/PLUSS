﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// CIBResultGateway Class.
    /// </summary>
    public class CIBResultGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor.
        /// </summary>
        public CIBResultGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public Int32 GetTolalRecord()
        {
            Int32 returnValue = 0;
            string mSQL = "SELECT COUNT(*) as totalRecord FROM t_pluss_cibresult WHERE CIBR_ENTRYDATE BETWEEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
            return returnValue= Convert.ToInt32("0"+DbQueryManager.ExecuteScalar(mSQL));
        }
        public List<CIBResult> GetCIBResult(Int32 lowerLimit,Int32 upporLimit)
        {
            List<CIBResult> CIBResultObjList = new List<CIBResult>();
            CIBResult CIBResult = null;
            string cmdText = "SELECT * FROM t_pluss_cibresult WHERE CIBR_ENTRYDATE BETWEEN '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND '" + DateTime.Now.ToString("yyyy-MM-dd") + "'order by CIBR_ID OFFSET  " + lowerLimit + " ROWS FETCH NEXT " + upporLimit + " ROWS ONLY ";
            DbDataReader CIBResultReader = DbQueryManager.ExecuteReader(cmdText);
            if (CIBResultReader != null)
            {
                while (CIBResultReader.Read())
                {
                    CIBResult = new CIBResult();
                    CIBResult.ID = Convert.ToInt32(CIBResultReader["CIBR_ID"].ToString());
                    CIBResult.LLID = CIBResultReader["CIBR_LLID"].ToString();
                    CIBResult.CustomerName = CIBResultReader["CIBR_CUSTOMERNAME"].ToString();
                    CIBResult.Status = CIBResultReader["CIBR_STATUS"].ToString();
                    CIBResult.Remarks = CIBResultReader["CIBR_REMARKS"].ToString();
                    CIBResult.FatherName = CIBResultReader["CIBR_FATHERSNAME"].ToString();
                    CIBResult.MotherName = CIBResultReader["CIBR_MOTHERSNAME"].ToString();
                    CIBResult.ReferenceNumber = CIBResultReader["CIBR_REFERENCENUMBER"].ToString();
                    CIBResult.Type = CIBResultReader["CIBR_TYPE"].ToString();
                    CIBResultObjList.Add(CIBResult);
                }
                CIBResultReader.Close();
            }
            return CIBResultObjList;
        }
        /// <summary>
        /// This function select CIBResult List from db
        /// </summary>
        /// <param name="customerName">Gets a customer name</param>
        /// <param name="fatherName"> Gets a father name</param>
        /// <param name="motherName">Gets a mother name</param>
        /// <returns>Returns a CIBResult list object.</returns>
        public List<CIBResult> Select(string customerName, string fatherName, string motherName, string businessName)
        {
            List<CIBResult> CIBResultList = new List<BusinessEntities.CIBResult>();
            CIBResult CIBResult = new CIBResult();

            string cmdText = "SELECT CIBR_ID, CIBR_LLID, CIBR_CUSTOMERNAME, " +
                "CIBR_STATUS, CIBR_REMARKS, CIBR_FATHERSNAME, CIBR_MOTHERSNAME, " +
                "CIBR_REFERENCENUMBER, CIBR_TYPE, CIBR_ENTRYDATE, CIBR_USER_ID " +
                "FROM t_pluss_cibresult WHERE 0=0 ";
            if (!string.IsNullOrEmpty(customerName))
            {
                cmdText += "OR RTRIM(Upper(CIBR_CUSTOMERNAME))  LIKE '%" + customerName.ToUpper().Trim() + "%' ";
            }
            if (!string.IsNullOrEmpty(businessName))
            {
                cmdText += "OR RTRIM(Upper(CIBR_CUSTOMERNAME))  LIKE '%" + businessName.ToUpper().Trim() + "%' ";
            }
            if (!string.IsNullOrEmpty(fatherName))
            {
                cmdText += "OR RTRIM(Upper(CIBR_FATHERSNAME))  LIKE '%" + fatherName.ToUpper().Trim() + "%' ";
            }
            if (!string.IsNullOrEmpty(motherName))
            {
                cmdText += "OR RTRIM(Upper(CIBR_MOTHERSNAME))  LIKE '%" + motherName.ToUpper().Trim() + "%' ";
            }

            DbDataReader CIBResultReader = DbQueryManager.ExecuteReader(cmdText);
            if (CIBResultReader != null)
            {
                while (CIBResultReader.Read())
                {
                    CIBResult = new CIBResult();
                    CIBResult.LLID = CIBResultReader["CIBR_LLID"].ToString();
                    CIBResult.CustomerName = CIBResultReader["CIBR_CUSTOMERNAME"].ToString();
                    CIBResult.Status = CIBResultReader["CIBR_STATUS"].ToString();
                    CIBResult.Remarks = CIBResultReader["CIBR_REMARKS"].ToString();
                    CIBResult.FatherName = CIBResultReader["CIBR_FATHERSNAME"].ToString();
                    CIBResult.MotherName = CIBResultReader["CIBR_MOTHERSNAME"].ToString();
                    CIBResult.ReferenceNumber = CIBResultReader["CIBR_REFERENCENUMBER"].ToString();
                    CIBResult.Type = CIBResultReader["CIBR_TYPE"].ToString();

                    CIBResultList.Add(CIBResult);
                }

                CIBResultReader.Close();
            }
            return CIBResultList;
        }

        /// <summary>
        /// This method select CIB result
        /// </summary>
        /// <param name="lLID">Gets a llid.</param>
        /// <returns>Returns a CIBResult object.</returns>
        public CIBResult SelectCIBResult(string lLID)
        {
            CIBResult CIBResult = new CIBResult();
            string cmdText = "SELECT CIBR_ID, CIBR_LLID, CIBR_CUSTOMERNAME, " +
                "CIBR_STATUS, CIBR_REMARKS, CIBR_FATHERSNAME, CIBR_MOTHERSNAME, " +
                "CIBR_REFERENCENUMBER, CIBR_TYPE, CIBR_ENTRYDATE, CIBR_USER_ID " +
                "FROM t_pluss_cibresult WHERE CIBR_LLID='"+lLID+"'";
            DbDataReader CIBResultReader = DbQueryManager.ExecuteReader(cmdText);
            if (CIBResultReader != null)
            {
                while (CIBResultReader.Read())
                {
                    CIBResult = new CIBResult();
                    CIBResult.ID = Convert.ToInt32(CIBResultReader["CIBR_ID"].ToString());
                    CIBResult.LLID = CIBResultReader["CIBR_LLID"].ToString();
                    CIBResult.CustomerName = CIBResultReader["CIBR_CUSTOMERNAME"].ToString();
                    CIBResult.Status = CIBResultReader["CIBR_STATUS"].ToString();
                    CIBResult.Remarks = CIBResultReader["CIBR_REMARKS"].ToString();
                    CIBResult.FatherName = CIBResultReader["CIBR_FATHERSNAME"].ToString();
                    CIBResult.MotherName = CIBResultReader["CIBR_MOTHERSNAME"].ToString();
                    CIBResult.ReferenceNumber = CIBResultReader["CIBR_REFERENCENUMBER"].ToString();
                    CIBResult.Type = CIBResultReader["CIBR_TYPE"].ToString();
                }
                CIBResultReader.Close();
            }
            return CIBResult;
        }
        /// <summary>
        /// This method insertion or update CIB result.
        /// </summary>
        /// <param name="cIBResultObjList">Gets a CIBResult lsit object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendCIBResultInToDB(List<CIBResult> cIBResultObjList)
        {
            int returnValue = 0;
            List<CIBResult> insertCIBResultList = new List<BusinessEntities.CIBResult>();
            List<CIBResult> updateCIBResultList = new List<BusinessEntities.CIBResult>();
            if (cIBResultObjList.Count > 0)
            {
                foreach (CIBResult cIBResultObj in cIBResultObjList)
                {
                    CIBResult existingcIBResultObj = SelectCIBResult(cIBResultObj.LLID);
                    if (existingcIBResultObj.ID > 0)
                    {
                        cIBResultObj.ID = existingcIBResultObj.ID;
                        returnValue = UpdateCIBResultList(cIBResultObj);
                    }
                    else
                    {
                        returnValue = InsertCIBResultList(cIBResultObj);
                    }
                }
                //if (updateCIBResultList.Count > 0)
                //{
                //    returnValue = UpdateCIBResultList(updateCIBResultList);
                //}
                //if (insertCIBResultList.Count > 0)
                //{
                //    returnValue = InsertCIBResultList(insertCIBResultList);
                //}
            }
            return returnValue;
        }
        /// <summary>
        /// This method provide insert of CIB result info.
        /// </summary>
        /// <param name="cIBResultObjList">Gets a CIBResult list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertCIBResultList(CIBResult cIBResultObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_cibresult (CIBR_LLID, CIBR_CUSTOMERNAME,"+ 
	                          "CIBR_STATUS, CIBR_REMARKS, CIBR_FATHERSNAME,"+
	                          "CIBR_MOTHERSNAME, CIBR_REFERENCENUMBER,"+
	                          "CIBR_TYPE, CIBR_ENTRYDATE, CIBR_USER_ID) VALUES";
                //for (int i = 0; i < cIBResultObjList.Count; i++)
                //{
                    mSQL = mSQL + "('" +
                          cIBResultObj.LLID + "','" +
                          AddSlash(cIBResultObj.CustomerName) + "','" +
                          AddSlash(cIBResultObj.Status) + "','" +
                          AddSlash(cIBResultObj.Remarks) + "','" +
                          AddSlash(cIBResultObj.FatherName) + "','" +
                          AddSlash(cIBResultObj.MotherName) + "','" +
                          AddSlash(cIBResultObj.ReferenceNumber) + "','" +
                          AddSlash(cIBResultObj.Type) + "','" +
                          cIBResultObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                          cIBResultObj.UserId + ")";
                //}
                //mSQL = mSQL.Substring(0, mSQL.Length - 1);
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide update of CIB result info.
        /// </summary>
        /// <param name="cIBResultObjList">Gets a CIBResult list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateCIBResultList(CIBResult cIBResultObj)
        {
            bool flag = false;
            try
            {
                //for (int i = 0; i < cIBResultObjList.Count; i++)
                //{
                    string mSQL = "";
                    mSQL = "UPDATE t_pluss_cibresult SET " +
                           "CIBR_LLID='" + cIBResultObj.LLID + "'," +
                           "CIBR_CUSTOMERNAME='" + AddSlash(cIBResultObj.CustomerName) + "'," +
                           "CIBR_STATUS='" + cIBResultObj.Status + "'," +
                           "CIBR_REMARKS='" + AddSlash(cIBResultObj.Remarks) + "'," +
                           "CIBR_FATHERSNAME='" + AddSlash(cIBResultObj.FatherName) + "'," +
                           "CIBR_MOTHERSNAME='" + AddSlash(cIBResultObj.MotherName) + "'," +
                           "CIBR_REFERENCENUMBER='" + AddSlash(cIBResultObj.ReferenceNumber) + "'," +
                           "CIBR_TYPE='" + AddSlash(cIBResultObj.Type) + "',"+
                           "CIBR_ENTRYDATE ='"+ cIBResultObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                           "CIBR_USER_ID =" + cIBResultObj.UserId + " WHERE CIBR_ID=" + cIBResultObj.ID;
                    if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                    {
                        flag = true;
                    }
                //}
                if (flag == true)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
