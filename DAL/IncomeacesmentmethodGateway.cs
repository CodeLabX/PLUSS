﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;
using System.Data.Common;

namespace DAL
{
    /// <summary>
    /// IncomeacesmentmethodGateway Class.
    /// </summary>
    public class IncomeacesmentmethodGateway
    {
        public DatabaseConnection dbMySQL = null;
        /// <summary>
        /// Constructor.
        /// </summary>
        public IncomeacesmentmethodGateway()
        {
            dbMySQL = new DatabaseConnection(); 
        }

        /// <summary>
        /// This method select income acesment
        /// </summary>
        /// <returns>Returns a Incomeacesmentmethod list object.</returns>
        public List<Incomeacesmentmethod> SelectIncomeAcesmentMethodList()
        {
            List<Incomeacesmentmethod> incomeacesmentmethodObjList = new List<Incomeacesmentmethod>();
            Incomeacesmentmethod incomeacesmentmethodObj=null;
          
            string mSQL="SELECT * FROM t_pluss_incomeacesmentmethod";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
          
            while (xRs.Read())
            {
                incomeacesmentmethodObj = new Incomeacesmentmethod();
                incomeacesmentmethodObj.Id = Convert.ToInt32(xRs["INAM_ID"]);
                incomeacesmentmethodObj.Name = Convert.ToString(xRs["INAM_METHOD"]);
                incomeacesmentmethodObj.Code = Convert.ToString(xRs["INAM_METHODCODE"]);
                incomeacesmentmethodObjList.Add(incomeacesmentmethodObj);
                
            }
            xRs.Close();
            }
            return incomeacesmentmethodObjList;
        }

        /// <summary>
        /// This method provide income acesment info.
        /// </summary>
        /// <param name="incomeMethodListId">Gets a income id.</param>
        /// <returns>Returns a Incomeacesmentmethod object.</returns>
        public Incomeacesmentmethod PopIncomeAcesmentMethodList(int incomeMethodListId)
        {
            Incomeacesmentmethod incomeacesmentMethodObj = new Incomeacesmentmethod();
            string query = "SELECT * FROM t_pluss_incomeacesmentmethod WHERE INAM_ID = " + incomeMethodListId + "";
            DbDataReader incomeReader = DbQueryManager.ExecuteReader(query);
            if(incomeReader != null)
            {
                if(incomeReader.HasRows)
                {
                    incomeReader.Read();
                    incomeacesmentMethodObj.Id = Convert.ToInt32(incomeReader["INAM_ID"]);
                    incomeacesmentMethodObj.Name = Convert.ToString(incomeReader["INAM_METHOD"]);
                    incomeacesmentMethodObj.Code = Convert.ToString(incomeReader["INAM_METHODCODE"]);
                    incomeReader.Close();
                }
            }
            return incomeacesmentMethodObj;
        }
    }
}
