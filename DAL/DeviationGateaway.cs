﻿using System;
using System.Collections.Generic;
using System.Data;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    public class DeviationGateaway
    {
        private Deviation tPlussDeviation;
        private List<Deviation> deviationObjList;

        //public DeviationGateaway()   // empty constructor is redundant. compiler generates this by default
        //{ }    

        /// <summary>
        /// This method provide all deviation information.
        /// </summary>
        /// <returns>Returns a Deviation list object</returns>
        public List<Deviation> GetAllDeviationInfo()
        {
            deviationObjList = new List<Deviation>();
            try
            {
                string queryString = "select * from t_pluss_deviation";

                DataTable deviationDataTableObj = DbQueryManager.GetTable(queryString);

                if (deviationDataTableObj != null)
                {
                    DataTableReader tPlussDeviationReader = deviationDataTableObj.CreateDataReader();

                    while (tPlussDeviationReader.Read())
                    {
                        tPlussDeviation = new Deviation();

                        tPlussDeviation.DeviID = Convert.ToInt32(tPlussDeviationReader["DeviID"]);
                        //tPlussDeviation.DeviPlID = Convert.ToInt32(tPlussDeviationReader["DeviPlID"]);
                        tPlussDeviation.DeviLlid = Convert.ToString(tPlussDeviationReader["DeviLlid"]);
                        tPlussDeviation.DeviLevel1 = Convert.ToInt32(tPlussDeviationReader["DeviLevel1"]);
                        tPlussDeviation.DeviDescription1ID = Convert.ToInt32(tPlussDeviationReader["DeviDescription1ID"]);
                        tPlussDeviation.DeviCode1 = Convert.ToString(tPlussDeviationReader["DeviCode1"]);
                        tPlussDeviation.DeviLevel2 = Convert.ToInt32(tPlussDeviationReader["DeviLevel2"]);
                        tPlussDeviation.DeviDescription2ID = Convert.ToInt32(tPlussDeviationReader["DeviDescription2ID"]);
                        tPlussDeviation.DeviCode2 = Convert.ToString(tPlussDeviationReader["DeviCode2"]);
                        tPlussDeviation.DeviLevel3 = Convert.ToInt32(tPlussDeviationReader["DeviLevel3"]);
                        tPlussDeviation.DeviDescription3ID = Convert.ToInt32(tPlussDeviationReader["DeviDescription3ID"]);
                        tPlussDeviation.DeviCode3 = Convert.ToString(tPlussDeviationReader["DeviCode3"]);
                        tPlussDeviation.DeviLevel4 = Convert.ToInt32(tPlussDeviationReader["DeviLevel4"]);
                        tPlussDeviation.DeviDescription4ID = Convert.ToInt32(tPlussDeviationReader["DeviDescription4ID"]);
                        tPlussDeviation.DeviCode4 = Convert.ToString(tPlussDeviationReader["DeviCode4"]);
                        tPlussDeviation.DeviRemarkscondition1ID = Convert.ToString(tPlussDeviationReader["DeviRemarkscondition1ID"]);
                        tPlussDeviation.DeviRemarkscondition2ID = Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition2ID"]);
                        tPlussDeviation.DeviRemarkscondition3ID = Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition3ID"]);
                        tPlussDeviation.DeviRemarkscondition4ID = Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition4ID"]);
                        tPlussDeviation.DeviRelationshipwith1 = Convert.ToInt32(tPlussDeviationReader["DeviRelationshipwith1"]);
                        tPlussDeviation.DeviRelationshipwith2 = Convert.ToInt32(tPlussDeviationReader["DeviRelationshipwith2"]);
                        tPlussDeviation.DeviSince = Convert.ToDateTime(tPlussDeviationReader["DeviSince"]);
                        tPlussDeviation.DeviResidentialstatus = Convert.ToInt32(tPlussDeviationReader["DeviResidentialstatus"]);
                        tPlussDeviation.DeviCreditcardrepayment = Convert.ToInt32(tPlussDeviationReader["DeviCreditcardrepayment"]);
                        tPlussDeviation.DeviLimit = Convert.ToString(tPlussDeviationReader["DeviLimit"]);
                        tPlussDeviation.DeviAvailedsince = Convert.ToDateTime(tPlussDeviationReader["DeviAvailedsince"]);
                        tPlussDeviation.DeviPreviousrepaymentId1 = Convert.ToInt32(tPlussDeviationReader["DeviPreviousrepaymentId1"]);
                        tPlussDeviation.DeviLoantypeId1 = Convert.ToInt32(tPlussDeviationReader["DeviLoantypeId1"]);
                        tPlussDeviation.DeviAvailedsince1 = Convert.ToDateTime(tPlussDeviationReader["DeviAvailedsince1"]);
                        tPlussDeviation.DeviPreviousrepaymentId2 = Convert.ToInt32(tPlussDeviationReader["DeviPreviousrepaymentId2"]);
                        tPlussDeviation.DeviLoantypeId2 = Convert.ToInt32(tPlussDeviationReader["DeviLoantypeId2"]);
                        tPlussDeviation.DeviAvailedsince2 = Convert.ToDateTime(tPlussDeviationReader["DeviAvailedsince2"]);
                        tPlussDeviation.DeviOthersID = Convert.ToString(tPlussDeviationReader["DeviOthersID"]);
                        tPlussDeviation.DeviRepaymentnotsatisfactory = Convert.ToInt32(tPlussDeviationReader["DeviRepaymentnotsatisfactory"]);
                        tPlussDeviation.DeviLoantypeId3 = Convert.ToInt32(tPlussDeviationReader["DeviLoantypeId3"]);
                        tPlussDeviation.DeviDpdID = Convert.ToInt32(tPlussDeviationReader["DeviDpdID"]);
                        tPlussDeviation.DeviCashsalaried = Convert.ToInt32(tPlussDeviationReader["DeviCashsalaried"]);
                        tPlussDeviation.DeviFrequentchangereturn = Convert.ToInt32(tPlussDeviationReader["DeviFrequentchangereturn"]);
                        tPlussDeviation.DeviOthers = Convert.ToString(tPlussDeviationReader["DeviOthers"]);
                        tPlussDeviation.UserId = Convert.ToInt64(tPlussDeviationReader["DEVI_USER_ID"]);
                        tPlussDeviation.EntryDate = Convert.ToDateTime(tPlussDeviationReader["DEVI_ENTRYDATE"]);

                        deviationObjList.Add(tPlussDeviation);
                    }
                    tPlussDeviationReader.Close();
                }
                return deviationObjList;
            }
            catch
            {
                // TODO : tPlussSalaryReader.Close();
                return deviationObjList;
            }
        }

        /// <summary>
        /// This method provide particular devaition info against llid.
        /// </summary>
        /// <param name="llId">Gets a llid.</param>
        /// <returns>Returns a Deviation list object.</returns>
        public List<Deviation> GetAllDeviationInfoByLlId(string llId)
        {
            deviationObjList = new List<Deviation>();
            try
            {
                string queryString = "select * from t_pluss_deviation where DEVI_LLID='" + llId+"'";

                DataTable deviationDataTableObj = DbQueryManager.GetTable(queryString);

                if (deviationDataTableObj != null)
                {
                    DataTableReader tPlussDeviationReader = deviationDataTableObj.CreateDataReader();

                    while (tPlussDeviationReader.Read())
                    {
                        tPlussDeviation = new Deviation();

                        tPlussDeviation.DeviID = Convert.ToInt32(tPlussDeviationReader["Devi_ID"]);
                        //tPlussDeviation.DeviPlID = Convert.ToInt32(tPlussDeviationReader["Devi_Pl_ID"]);
                        tPlussDeviation.DeviLlid = Convert.ToString(tPlussDeviationReader["Devi_Llid"]);
                        tPlussDeviation.DeviLevel1 = Convert.ToInt32(tPlussDeviationReader["Devi_Level1"]);
                        tPlussDeviation.DeviDescription1ID = Convert.ToInt32(tPlussDeviationReader["Devi_Description1_ID"]);
                        tPlussDeviation.DeviCode1 = Convert.ToString(tPlussDeviationReader["Devi_Code1"]);
                        tPlussDeviation.DeviLevel2 = Convert.ToInt32(tPlussDeviationReader["Devi_Level2"]);
                        tPlussDeviation.DeviDescription2ID = Convert.ToInt32(tPlussDeviationReader["Devi_Description2_ID"]);
                        tPlussDeviation.DeviCode2 = Convert.ToString(tPlussDeviationReader["Devi_Code2"]);
                        tPlussDeviation.DeviLevel3 = Convert.ToInt32(tPlussDeviationReader["Devi_Level3"]);
                        tPlussDeviation.DeviDescription3ID = Convert.ToInt32(tPlussDeviationReader["Devi_Description3_ID"]);
                        tPlussDeviation.DeviCode3 = Convert.ToString(tPlussDeviationReader["Devi_Code3"]);
                        tPlussDeviation.DeviLevel4 = Convert.ToInt32(tPlussDeviationReader["Devi_Level4"]);
                        tPlussDeviation.DeviDescription4ID = Convert.ToInt32(tPlussDeviationReader["Devi_Description4_ID"]);
                        tPlussDeviation.DeviCode4 = Convert.ToString(tPlussDeviationReader["Devi_Code4"]);
                        tPlussDeviation.DeviRemarkscondition1ID =
                            Convert.ToString(tPlussDeviationReader["Devi_Remarkscondition"]);
                        //tPlussDeviation.DeviRemarkscondition2ID =
                        //    Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition2ID"]);
                        //tPlussDeviation.DeviRemarkscondition3ID =
                        //    Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition3ID"]);
                        //tPlussDeviation.DeviRemarkscondition4ID =
                        //    Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition4ID"]);
                        tPlussDeviation.DeviRelationshipwith1 =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Relationshipwith1"]);
                        tPlussDeviation.DeviRelationshipwith2 =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Relationshipwith2"]);
                        tPlussDeviation.DeviSince = Convert.ToDateTime(tPlussDeviationReader["Devi_Since"]);
                        tPlussDeviation.DeviResidentialstatus =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Residentialstatus"]);
                        tPlussDeviation.DeviCreditcardrepayment =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Creditcardrepayment"]);
                        tPlussDeviation.DeviLimit = Convert.ToString(tPlussDeviationReader["Devi_Limit"]);
                        tPlussDeviation.DeviAvailedsince = Convert.ToDateTime(tPlussDeviationReader["Devi_Availedsince"]);
                        tPlussDeviation.DeviPreviousrepaymentId1 =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Previousrepayment_Id1"]);
                        tPlussDeviation.DeviLoantypeId1 = Convert.ToInt32(tPlussDeviationReader["Devi_Loantype_Id1"]);
                        tPlussDeviation.DeviAvailedsince1 = Convert.ToDateTime(tPlussDeviationReader["Devi_Availedsince1"]);
                        tPlussDeviation.DeviPreviousrepaymentId2 =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Previousrepayment_Id2"]);
                        tPlussDeviation.DeviLoantypeId2 = Convert.ToInt32(tPlussDeviationReader["Devi_Loantype_Id2"]);
                        tPlussDeviation.DeviAvailedsince2 = Convert.ToDateTime(tPlussDeviationReader["Devi_Availedsince2"]);
                        tPlussDeviation.DeviOthersID = Convert.ToString(tPlussDeviationReader["Devi_Others_ID"]);
                        tPlussDeviation.DeviRepaymentnotsatisfactory =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Repaymentnotsatisfactory"]);
                        tPlussDeviation.DeviLoantypeId3 = Convert.ToInt32(tPlussDeviationReader["Devi_Loantype_Id3"]);
                        tPlussDeviation.DeviDpdID = Convert.ToInt32(tPlussDeviationReader["Devi_Dpd_ID"]);
                        tPlussDeviation.DeviCashsalaried = Convert.ToInt32(tPlussDeviationReader["Devi_Cashsalaried"]);
                        tPlussDeviation.DeviFrequentchangereturn =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Frequentchangereturn"]);
                        tPlussDeviation.DeviOthers = Convert.ToString(tPlussDeviationReader["Devi_Others"]);
                        try
                        {
                            tPlussDeviation.UserId = Convert.ToInt64(tPlussDeviationReader["DEVI_USER_ID"]);
                            tPlussDeviation.EntryDate = Convert.ToDateTime(tPlussDeviationReader["DEVI_ENTRYDATE"]);
                        }
                        catch (Exception)
                        {

                            tPlussDeviation.UserId = 0;
                            tPlussDeviation.EntryDate = DateTime.Now;
                        }

                        deviationObjList.Add(tPlussDeviation);
                    }
                    tPlussDeviationReader.Close();
                }
                return deviationObjList;
            }
            catch
            {
                // TODO : tPlussDeviationReader.Close();
                return deviationObjList;
            }
        }

        /// <summary>
        /// This method provide particular deviation info against pl id.
        /// </summary>
        /// <param name="plId">Gets a pl id.</param>
        /// <returns>Returns a Deviation list object.</returns>
        public List<Deviation> GetAllDeviationInfoByPlId(int plId)
        {
            deviationObjList = new List<Deviation>();
            try
            {
                string queryString = "select * from t_pluss_deviation where DEVI_PL_ID=" + plId ;

                DataTable deviationDataTableObj = DbQueryManager.GetTable(queryString);

                if (deviationDataTableObj != null)
                {
                    DataTableReader tPlussDeviationReader = deviationDataTableObj.CreateDataReader();

                    while (tPlussDeviationReader.Read())
                    {
                        tPlussDeviation = new Deviation();

                        tPlussDeviation.DeviID = Convert.ToInt32(tPlussDeviationReader["Devi_ID"]);
                        tPlussDeviation.DeviPlID = Convert.ToInt32(tPlussDeviationReader["Devi_Pl_ID"]);
                        tPlussDeviation.DeviLlid = Convert.ToString(tPlussDeviationReader["Devi_Llid"]);
                        tPlussDeviation.DeviLevel1 = Convert.ToInt32(tPlussDeviationReader["Devi_Level1"]);
                        tPlussDeviation.DeviDescription1ID = Convert.ToInt32(tPlussDeviationReader["Devi_Description1_ID"]);
                        tPlussDeviation.DeviCode1 = Convert.ToString(tPlussDeviationReader["Devi_Code1"]);
                        tPlussDeviation.DeviLevel2 = Convert.ToInt32(tPlussDeviationReader["Devi_Level2"]);
                        tPlussDeviation.DeviDescription2ID = Convert.ToInt32(tPlussDeviationReader["Devi_Description2_ID"]);
                        tPlussDeviation.DeviCode2 = Convert.ToString(tPlussDeviationReader["Devi_Code2"]);
                        tPlussDeviation.DeviLevel3 = Convert.ToInt32(tPlussDeviationReader["Devi_Level3"]);
                        tPlussDeviation.DeviDescription3ID = Convert.ToInt32(tPlussDeviationReader["Devi_Description3_ID"]);
                        tPlussDeviation.DeviCode3 = Convert.ToString(tPlussDeviationReader["Devi_Code3"]);
                        tPlussDeviation.DeviLevel4 = Convert.ToInt32(tPlussDeviationReader["Devi_Level4"]);
                        tPlussDeviation.DeviDescription4ID = Convert.ToInt32(tPlussDeviationReader["Devi_Description4_ID"]);
                        tPlussDeviation.DeviCode4 = Convert.ToString(tPlussDeviationReader["Devi_Code4"]);
                        tPlussDeviation.DeviRemarkscondition1ID =
                            Convert.ToString(tPlussDeviationReader["Devi_Remarkscondition"]);
                        //tPlussDeviation.DeviRemarkscondition2ID =
                        //    Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition2ID"]);
                        //tPlussDeviation.DeviRemarkscondition3ID =
                        //    Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition3ID"]);
                        //tPlussDeviation.DeviRemarkscondition4ID =
                        //    Convert.ToInt32(tPlussDeviationReader["DeviRemarkscondition4ID"]);
                        tPlussDeviation.DeviRelationshipwith1 =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Relationshipwith1"]);
                        tPlussDeviation.DeviRelationshipwith2 =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Relationshipwith2"]);
                        tPlussDeviation.DeviSince = Convert.ToDateTime(tPlussDeviationReader["Devi_Since"]);
                        tPlussDeviation.DeviResidentialstatus =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Residentialstatus"]);
                        tPlussDeviation.DeviCreditcardrepayment =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Creditcardrepayment"]);
                        tPlussDeviation.DeviLimit = Convert.ToString(tPlussDeviationReader["Devi_Limit"]);
                        tPlussDeviation.DeviAvailedsince = Convert.ToDateTime(tPlussDeviationReader["Devi_Availedsince"]);
                        tPlussDeviation.DeviPreviousrepaymentId1 =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Previousrepayment_Id1"]);
                        tPlussDeviation.DeviLoantypeId1 = Convert.ToInt32(tPlussDeviationReader["Devi_Loantype_Id1"]);
                        tPlussDeviation.DeviAvailedsince1 = Convert.ToDateTime(tPlussDeviationReader["Devi_Availedsince1"]);
                        tPlussDeviation.DeviPreviousrepaymentId2 =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Previousrepayment_Id2"]);
                        tPlussDeviation.DeviLoantypeId2 = Convert.ToInt32(tPlussDeviationReader["Devi_Loantype_Id2"]);
                        tPlussDeviation.DeviAvailedsince2 = Convert.ToDateTime(tPlussDeviationReader["Devi_Availedsince2"]);
                        tPlussDeviation.DeviOthersID = Convert.ToString(tPlussDeviationReader["Devi_Others_ID"]);
                        tPlussDeviation.DeviRepaymentnotsatisfactory =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Repaymentnotsatisfactory"]);
                        tPlussDeviation.DeviLoantypeId3 = Convert.ToInt32(tPlussDeviationReader["Devi_Loantype_Id3"]);
                        tPlussDeviation.DeviDpdID = Convert.ToInt32(tPlussDeviationReader["Devi_Dpd_ID"]);
                        tPlussDeviation.DeviCashsalaried = Convert.ToInt32(tPlussDeviationReader["Devi_Cashsalaried"]);
                        tPlussDeviation.DeviFrequentchangereturn =
                            Convert.ToInt32(tPlussDeviationReader["Devi_Frequentchangereturn"]);
                        tPlussDeviation.DeviOthers = Convert.ToString(tPlussDeviationReader["Devi_Others"]);
                        try
                        {
                            tPlussDeviation.UserId = Convert.ToInt64(tPlussDeviationReader["DEVI_USER_ID"]);
                            tPlussDeviation.EntryDate = Convert.ToDateTime(tPlussDeviationReader["DEVI_ENTRYDATE"]);
                        }
                        catch (Exception)
                        {

                            tPlussDeviation.UserId = 0;
                            tPlussDeviation.EntryDate = DateTime.Now;
                        }

                        deviationObjList.Add(tPlussDeviation);
                    }
                    tPlussDeviationReader.Close();
                }
                return deviationObjList;
            }
            catch
            {
                // TODO : tPlussSalaryReader.Close();
                return deviationObjList;
            }
        }

        /// <summary>
        /// This method provide the insertion operation of deviation.
        /// </summary>
        /// <param name="deviation">Gets a Deviation object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertDeviation(Deviation deviation)
        {
            try
            {
                string sqlInsert ="INSERT INTO t_pluss_deviation ( DEVI_LLID, DEVI_LEVEL1, DEVI_DESCRIPTION1_ID, DEVI_CODE1, DEVI_LEVEL2, DEVI_DESCRIPTION2_ID, DEVI_CODE2, DEVI_LEVEL3, DEVI_DESCRIPTION3_ID, DEVI_CODE3, DEVI_LEVEL4, DEVI_DESCRIPTION4_ID, DEVI_CODE4, DEVI_REMARKSCONDITION, DEVI_RELATIONSHIPWITH1, DEVI_RELATIONSHIPWITH2, DEVI_SINCE, DEVI_RESIDENTIALSTATUS, DEVI_CREDITCARDREPAYMENT, DEVI_LIMIT, DEVI_AVAILEDSINCE, DEVI_PREVIOUSREPAYMENT_ID1, DEVI_LOANTYPE_ID1, DEVI_AVAILEDSINCE1, DEVI_PREVIOUSREPAYMENT_ID2, DEVI_LOANTYPE_ID2, DEVI_AVAILEDSINCE2, DEVI_OTHERS_ID, DEVI_REPAYMENTNOTSATISFACTORY, DEVI_LOANTYPE_ID3, DEVI_DPD_ID, DEVI_CASHSALARIED, DEVI_FREQUENTCHANGERETURN, DEVI_OTHERS,DEVI_USER_ID,DEVI_ENTRYDATE ) VALUES ( " +
                    /*deviation.DeviPlID + "," + */ deviation.DeviLlid + "," + deviation.DeviLevel1 + "," +
                    deviation.DeviDescription1ID + ",'" + deviation.DeviCode1 + "'," +
                    deviation.DeviLevel2 + "," + deviation.DeviDescription2ID + ",'" +
                    deviation.DeviCode2 + "'," + deviation.DeviLevel3 + "," +
                    deviation.DeviDescription3ID + ",'" + deviation.DeviCode3 + "'," +
                    deviation.DeviLevel4 + "," + deviation.DeviDescription4ID + ",'" +
                    deviation.DeviCode4 + "','" + deviation.DeviRemarkscondition1ID + "'," +
                    deviation.DeviRelationshipwith1 + "," +
                    deviation.DeviRelationshipwith2 + ",'" + deviation.DeviSince.ToString("yyyy-MM-dd") + "'," +
                    deviation.DeviResidentialstatus + "," + deviation.DeviCreditcardrepayment + ",'" +
                    deviation.DeviLimit + "','" + deviation.DeviAvailedsince.ToString("yyyy-MM-dd") + "'," +
                    deviation.DeviPreviousrepaymentId1 + "," + deviation.DeviLoantypeId1 + ",'" +
                    deviation.DeviAvailedsince1.ToString("yyyy-MM-dd") + "'," + deviation.DeviPreviousrepaymentId2 + "," +
                    deviation.DeviLoantypeId2 + ",'" + deviation.DeviAvailedsince2.ToString("yyyy-MM-dd") + "','" +
                    deviation.DeviOthersID + "'," + deviation.DeviRepaymentnotsatisfactory + "," +
                    deviation.DeviLoantypeId3 + "," + deviation.DeviDpdID + "," +
                    deviation.DeviCashsalaried + "," + deviation.DeviFrequentchangereturn + ",'" +
                    deviation.DeviOthers + "'," +
                    deviation.UserId + ",'" +
                    deviation.EntryDate.ToString("yyyy-MM-dd") + "')";


                int insert = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                return insert;
            }
            catch
            {
                return 0;
            }
        }


        /// <summary>
        /// This method provide the update operation of deviation.
        /// </summary>DEVI_USER_ID,DEVI_ENTRYDATE
        /// <param name="deviation">Gets a Deviation object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdateDeviation(Deviation deviation)
        {
            try
            {
                string sqlUpdate = "UPDATE t_pluss_deviation SET DEVI_PL_ID = " +
                                   /*deviation.DeviPlID + ", DEVI_LLID = " +*/
                                   deviation.DeviLlid + ", DEVI_LEVEL1 = " +
                                   deviation.DeviLevel1 + ", DEVI_DESCRIPTION1_ID = " +
                                   deviation.DeviDescription1ID + ", DEVI_CODE1 = '" +
                                   deviation.DeviCode1 + "', DEVI_LEVEL2 = " +
                                   deviation.DeviLevel2 + ", DEVI_DESCRIPTION2_ID = " +
                                   deviation.DeviDescription2ID + ", DEVI_CODE2 = '" +
                                   deviation.DeviCode2 + "', DEVI_LEVEL3 = " +
                                   deviation.DeviLevel3 + ", DEVI_DESCRIPTION3_ID = " +
                                   deviation.DeviDescription3ID + ", DEVI_CODE3 = '" +
                                   deviation.DeviCode3 + "', DEVI_LEVEL4 = " +
                                   deviation.DeviLevel4 + ", DEVI_DESCRIPTION4_ID = " +
                                   deviation.DeviDescription4ID + ", DEVI_CODE4 = '" +
                                   deviation.DeviCode4 + "', DEVI_REMARKSCONDITION = '" +
                                   deviation.DeviRemarkscondition1ID + "', DEVI_RELATIONSHIPWITH1 = " +
                                   deviation.DeviRelationshipwith1 + ", DEVI_RELATIONSHIPWITH2 = " +
                                   deviation.DeviRelationshipwith2 + ", DEVI_SINCE = '" +
                                   deviation.DeviSince.ToString("yyyy-MM-dd") + "', DEVI_RESIDENTIALSTATUS = " +
                                   deviation.DeviResidentialstatus + ", DEVI_CREDITCARDREPAYMENT = " +
                                   deviation.DeviCreditcardrepayment + ", DEVI_LIMIT = '" +
                                   deviation.DeviLimit + "', DEVI_AVAILEDSINCE = '" +
                                   deviation.DeviAvailedsince.ToString("yyyy-MM-dd") +
                                   "', DEVI_PREVIOUSREPAYMENT_ID1 = " +
                                   deviation.DeviPreviousrepaymentId1 + ", DEVI_LOANTYPE_ID1 = " +
                                   deviation.DeviLoantypeId1 + ", DEVI_AVAILEDSINCE1 = '" +
                                   deviation.DeviAvailedsince1.ToString("yyyy-MM-dd") +
                                   "', DEVI_PREVIOUSREPAYMENT_ID2 = " +
                                   deviation.DeviPreviousrepaymentId2 + ", DEVI_LOANTYPE_ID2 = " +
                                   deviation.DeviLoantypeId2 + ", DEVI_AVAILEDSINCE2 = '" +
                                   deviation.DeviAvailedsince2.ToString("yyyy-MM-dd") + "', DEVI_OTHERS_ID = '" +
                                   deviation.DeviOthersID + "', DEVI_REPAYMENTNOTSATISFACTORY = " +
                                   deviation.DeviRepaymentnotsatisfactory + ", DEVI_LOANTYPE_ID3 = " +
                                   deviation.DeviLoantypeId3 + ", DEVI_DPD_ID = " +
                                   deviation.DeviDpdID + ", DEVI_CASHSALARIED = " +
                                   deviation.DeviCashsalaried + ", DEVI_FREQUENTCHANGERETURN = " +
                                   deviation.DeviFrequentchangereturn + ", DEVI_OTHERS = '" +
                                   deviation.DeviOthers + "',DEVI_USER_ID=" +
                                   deviation.UserId + ",DEVI_ENTRYDATE='" +
                                   deviation.EntryDate.ToString("yyyy-MM-dd") + "' WHERE DEVI_ID = " + deviation.DeviID;

                int update = DbQueryManager.ExecuteNonQuery(sqlUpdate);   // if sucessfull then 1
                return update;
            }
            catch
            {
                return 0;
            }
        }
    }
}
