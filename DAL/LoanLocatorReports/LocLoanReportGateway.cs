﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    public class LocLoanReportGateway
    {
        public DataTable ApplicationStatusWiseReprot(string date1, string date2, int productId, int status, string roleCutDate)
        {
            var sql = "";
            var cond = "";
            if (productId > 0)
            {
                cond = string.Format(@"and loan_app_info.PROD_ID={0}", productId);
            }
            if (status==5) 
            {
                sql = String.Format(@"SELECT loan_app_info.LOAN_APPL_ID,loan_app_info.CUST_NAME,loan_app_info.LOAN_APRV_AMOU,loan_app_info.RECE_DATE,
                loan_app_info.APPRV_DATE,branch_info.BR_NAME,learner_details_table.NAME_LEARNER 
                From loc_loan_app_info
                loan_app_info,loan_app_details   loan_app_details,learner_details_table   learner_details_table,loc_branch_info
                branch_info Where loan_app_info.PERS_ID = learner_details_table.ID_LEARNER And loan_app_info.BR_ID = branch_info.BR_ID 
                And loan_app_info.LOAN_APPL_ID = loan_app_details.LOAN_APPL_ID {0} and 
                loan_app_details.PROCESS_STATUS_ID={1} and loan_app_details.DATE_TIME BETWEEN '{2} 00:00' and '{3} 23:59' order
                by  LOAN_APPL_ID;", cond, status, date1, date2);

                sql = String.Format(@"

SELECT loan_app_info.LOAN_APPL_ID
	,loan_app_info.CUST_NAME
	,loan_app_info.LOAN_APRV_AMOU
	,loan_app_info.RECE_DATE
	,loan_app_info.APPRV_DATE
	,branch_info.BR_NAME BRAN_NAME
	,learner_details_table.NAME_LEARNER
FROM loc_loan_app_info loan_app_info
	,loan_app_details loan_app_details
	,old_learner_details_table learner_details_table
	,loc_branch_info branch_info
WHERE loan_app_info.PERS_ID = learner_details_table.ID_LEARNER
	AND loan_app_info.BR_ID = branch_info.BR_ID
	AND loan_app_info.LOAN_APPL_ID = loan_app_details.LOAN_APPL_ID
{0}
	AND loan_app_details.PROCESS_STATUS_ID = {1}
	AND loan_app_details.DATE_TIME > '{2}' AND  loan_app_details.DATE_TIME < '{4}'

UNION 
SELECT loan_app_info.LOAN_APPL_ID
	,loan_app_info.CUST_NAME
	,loan_app_info.LOAN_APRV_AMOU
	,loan_app_info.RECE_DATE
	,loan_app_info.APPRV_DATE
	,branch_info.BR_NAME BRAN_NAME
	,learner_details_table.USER_NAME
FROM loc_loan_app_info loan_app_info
	,loan_app_details loan_app_details
	,t_pluss_user learner_details_table
	,loc_branch_info branch_info
WHERE loan_app_info.MakerID = learner_details_table.USER_CODE
	AND loan_app_info.BR_ID = branch_info.BR_ID
	AND loan_app_info.LOAN_APPL_ID = loan_app_details.LOAN_APPL_ID
{0}
	AND loan_app_details.PROCESS_STATUS_ID = {1}
	AND loan_app_details.DATE_TIME > '{4}' AND  loan_app_details.DATE_TIME < '{3}'

ORDER BY LOAN_APPL_ID;", cond, status, date1, date2, roleCutDate);
            }
            else if (status==7)
            {
                sql = String.Format(@"SELECT loan_app_info.LOAN_APPL_ID,loan_app_info.CUST_NAME,
                loan_app_info.LOAN_APRV_AMOU,loan_app_info.RECE_DATE,
                loan_app_info.LOAN_DISB_DATE,branch_info.BR_NAME,
                learner_details_table.NAME_LEARNER 
                From loc_loan_app_info  
                loan_app_info,loan_app_details loan_app_details,learner_details_table
                learner_details_table,loc_branch_info branch_info 
                Where loan_app_info.PERS_ID = 
                learner_details_table.ID_LEARNER And loan_app_info.BR_ID = branch_info.BR_ID And
                loan_app_info.LOAN_APPL_ID = loan_app_details.LOAN_APPL_ID {0}
                and loan_app_details.PROCESS_STATUS_ID={1} and loan_app_details.DATE_TIME BETWEEN '{2}  00:00'
                and '{3} 23:59' order by  LOAN_APPL_ID;", cond, status, date1, date2);
            }
            else
            {
                sql = String.Format(@"SELECT loan_app_info.LOAN_APPL_ID,loan_app_info.CUST_NAME,
                    loan_app_info.LOAN_APP_AMOU,loan_app_info.RECE_DATE,
                    loan_app_details.DATE_TIME,branch_info.BR_NAME,
                    learner_details_table.NAME_LEARNER 
                    From loc_loan_app_info 
                    loan_app_info,loan_app_details   loan_app_details,learner_details_table 
                    learner_details_table,loc_branch_info branch_info 
                    Where loan_app_info.PERS_ID = 
                    learner_details_table.ID_LEARNER And loan_app_info.BR_ID = branch_info.BR_ID And
                    loan_app_info.LOAN_APPL_ID = loan_app_details.LOAN_APPL_ID {0}
                    and loan_app_details.PROCESS_STATUS_ID={1} and loan_app_details.DATE_TIME BETWEEN '{2} 00:00' and '{3} 23:59'
                    order by  LOAN_APPL_ID;", cond, status, date1, date2);
            }

            return DbQueryManager.GetTable(sql);
        }

        public DataTable ApplicationDetailsReprot(string date1, string date2, int productId, string roleCutDate)
        {
            var cond = "";
            if (productId > 0)
            {
               cond = string.Format(@"and loan_app_info.PROD_ID={0}", productId);
            }
           
            var query = String.Format(@"SELECT product_info.PROD_NAME, loan_app_info.RECE_DATE,
            loan_app_info.LOAN_APPL_ID, branch_info.BR_NAME SOURCE_NAME, 
            loan_dept_info.LOAN_DEPT_NAME DELI_UNIT,loan_app_info.APPL_DATE,
            loan_app_info.ACC_NUMB as Account_Number, 
            loan_app_info.CUST_NAME, 	
	        (CASE WHEN loan_app_info.LOAN_COND_APRV_AMOU<>0 THEN loan_app_info.LOAN_COND_APRV_AMOU ELSE null END ) AS  LOAN_COND_APRV_AMOU, 
        (CASE WHEN loan_app_info.LOAN_APP_AMOU<>0 THEN loan_app_info.LOAN_APP_AMOU ELSE null END ) AS  LOAN_APP_AMOU,  
            learner_details_table.NAME_LEARNER SUBMIT_BY, 
            loan_status_info.LOAN_STATUS_NAME CURR_STATUS, 
            loan_app_info.LOAN_DECI_STATUS_ID DECI_STATUS, 
	        loan_app_info.APPRV_DATE,
             loan_app_info.APPRV_LEVEL, 
            acc_type_info.ACC_TYPE_NAME, loan_app_info.EMPL_TYPE, 
            loan_app_info.ORGAN, loan_app_info.LOAN_APRV_AMOU, 
            loan_app_info.LOAN_DISB_AMOU, loan_app_info.LOAN_DISB_DATE, loan_app_info.ARM_CODE, 
            loan_app_info.LOAN_AC_NO, loan_app_info.OLD_LIMIT, 
            loan_app_info.MOTHER_NAME, loan_app_info.OWNER, 
	        loan_app_info.TENOR, loan_app_info.USER_DEFINE, 
            loan_app_info.USER_DEFINE2, 
            loan_app_info.USER_DEFINE3, 
            loan_app_info.USER_DEFINE4,
	        loan_app_info.LOAN_SOURCE_CODE as 'PW_ID',
	        loan_app_info.PRE_DECLIEND as  'Previously Decliend(APP. ID)'
        FROM loc_loan_app_info loan_app_info LEFT OUTER JOIN loc_branch_info branch_info 
	        ON loan_app_info.BR_ID = branch_info.BR_ID  
	        LEFT OUTER JOIN acc_type_info 
	        ON loan_app_info.ACC_TYPE = acc_type_info.ID 
	        LEFT OUTER JOIN loan_status_info
	        ON loan_app_info.LOAN_STATUS_ID = loan_status_info.LOAN_STATUS_ID 
	        LEFT OUTER JOIN learner_details_table 
	        ON loan_app_info.PERS_ID = learner_details_table.ID_LEARNER
	        LEFT OUTER JOIN loan_dept_info 
	        ON loan_app_info.DELI_DEPT_ID = loan_dept_info.LOAN_DEPT_ID
             LEFT OUTER JOIN t_pluss_product
            product_info ON loan_app_info.PROD_ID = product_info.PROD_ID
	        where loan_app_info.RECE_DATE BETWEEN '{0}' and '{1}' {2} order by RECE_DATE, LOAN_APPL_ID;", date1, date2, cond);



            query = string.Format(@"
SELECT product_info.PROD_NAME
	,loan_app_info.RECE_DATE
	,loan_app_info.LOAN_APPL_ID
	,branch_info.BR_NAME SOURCE_NAME
	,loan_dept_info.LOAN_DEPT_NAME DELI_UNIT
	,loan_app_info.APPL_DATE
	,loan_app_info.ACC_NUMB AS Account_Number
	,loan_app_info.CUST_NAME
	,(
		CASE 
			WHEN loan_app_info.LOAN_COND_APRV_AMOU <> 0
				THEN loan_app_info.LOAN_COND_APRV_AMOU
			ELSE NULL
			END
		) AS LOAN_COND_APRV_AMOU
	,(
		CASE 
			WHEN loan_app_info.LOAN_APP_AMOU <> 0
				THEN loan_app_info.LOAN_APP_AMOU
			ELSE NULL
			END
		) AS LOAN_APP_AMOU
	,old_learner_details_table.NAME_LEARNER SUBMIT_BY
	,loan_status_info.LOAN_STATUS_NAME CURR_STATUS
	,loan_app_info.LOAN_DECI_STATUS_ID DECI_STATUS
	,loan_app_info.APPRV_DATE
	,loan_app_info.APPRV_LEVEL
	,acc_type_info.ACC_TYPE_NAME
	,loan_app_info.EMPL_TYPE
	,loan_app_info.ORGAN
	,loan_app_info.LOAN_APRV_AMOU
	,loan_app_info.LOAN_DISB_AMOU
	,loan_app_info.LOAN_DISB_DATE
	,loan_app_info.ARM_CODE
	,loan_app_info.LOAN_AC_NO
	,loan_app_info.OLD_LIMIT
	,loan_app_info.MOTHER_NAME
	,loan_app_info.OWNER
	,loan_app_info.TENOR
	,loan_app_info.USER_DEFINE
	,loan_app_info.USER_DEFINE2
	,loan_app_info.USER_DEFINE3
	,loan_app_info.USER_DEFINE4
	,loan_app_info.LOAN_SOURCE_CODE AS 'PW_ID'
	,loan_app_info.PRE_DECLIEND AS 'Previously Decliend(APP. ID)'
FROM loc_loan_app_info loan_app_info
LEFT OUTER JOIN loc_branch_info branch_info ON loan_app_info.BR_ID = branch_info.BR_ID
LEFT OUTER JOIN acc_type_info ON loan_app_info.ACC_TYPE = acc_type_info.ID
LEFT OUTER JOIN loan_status_info ON loan_app_info.LOAN_STATUS_ID = loan_status_info.LOAN_STATUS_ID
LEFT OUTER JOIN old_learner_details_table ON loan_app_info.PERS_ID = old_learner_details_table.ID_LEARNER
LEFT OUTER JOIN loan_dept_info ON loan_app_info.DELI_DEPT_ID = loan_dept_info.LOAN_DEPT_ID
LEFT OUTER JOIN t_pluss_product product_info ON loan_app_info.PROD_ID = product_info.PROD_ID
WHERE loan_app_info.RECE_DATE > '{0}'
		AND loan_app_info.RECE_DATE < '{3}'
{2}
UNION 
SELECT product_info.PROD_NAME
	,loan_app_info.RECE_DATE
	,loan_app_info.LOAN_APPL_ID
	,branch_info.BR_NAME SOURCE_NAME
	,loan_dept_info.LOAN_DEPT_NAME DELI_UNIT
	,loan_app_info.APPL_DATE
	,loan_app_info.ACC_NUMB AS Account_Number
	,loan_app_info.CUST_NAME
	,(
		CASE 
			WHEN loan_app_info.LOAN_COND_APRV_AMOU <> 0
				THEN loan_app_info.LOAN_COND_APRV_AMOU
			ELSE NULL
			END
		) AS LOAN_COND_APRV_AMOU
	,(
		CASE 
			WHEN loan_app_info.LOAN_APP_AMOU <> 0
				THEN loan_app_info.LOAN_APP_AMOU
			ELSE NULL
			END
		) AS LOAN_APP_AMOU
	,t_pluss_user.USER_NAME SUBMIT_BY
	,loan_status_info.LOAN_STATUS_NAME CURR_STATUS
	,loan_app_info.LOAN_DECI_STATUS_ID DECI_STATUS
	,loan_app_info.APPRV_DATE
	,loan_app_info.APPRV_LEVEL
	,acc_type_info.ACC_TYPE_NAME
	,loan_app_info.EMPL_TYPE
	,loan_app_info.ORGAN
	,loan_app_info.LOAN_APRV_AMOU
	,loan_app_info.LOAN_DISB_AMOU
	,loan_app_info.LOAN_DISB_DATE
	,loan_app_info.ARM_CODE
	,loan_app_info.LOAN_AC_NO
	,loan_app_info.OLD_LIMIT
	,loan_app_info.MOTHER_NAME
	,loan_app_info.OWNER
	,loan_app_info.TENOR
	,loan_app_info.USER_DEFINE
	,loan_app_info.USER_DEFINE2
	,loan_app_info.USER_DEFINE3
	,loan_app_info.USER_DEFINE4
	,loan_app_info.LOAN_SOURCE_CODE AS 'PW_ID'
	,loan_app_info.PRE_DECLIEND AS 'Previously Decliend(APP. ID)'
FROM loc_loan_app_info loan_app_info
LEFT OUTER JOIN loc_branch_info branch_info ON loan_app_info.BR_ID = branch_info.BR_ID
LEFT OUTER JOIN acc_type_info ON loan_app_info.ACC_TYPE = acc_type_info.ID
LEFT OUTER JOIN loan_status_info ON loan_app_info.LOAN_STATUS_ID = loan_status_info.LOAN_STATUS_ID
LEFT OUTER JOIN t_pluss_user ON loan_app_info.MakerID = t_pluss_user.USER_CODE
LEFT OUTER JOIN loan_dept_info ON loan_app_info.DELI_DEPT_ID = loan_dept_info.LOAN_DEPT_ID
LEFT OUTER JOIN t_pluss_product product_info ON loan_app_info.PROD_ID = product_info.PROD_ID
WHERE loan_app_info.RECE_DATE > '{3}'
		AND loan_app_info.RECE_DATE < '{1}'
{2}
ORDER BY RECE_DATE
	,LOAN_APPL_ID;
", date1, date2, cond, roleCutDate);



            return DbQueryManager.GetTable(query);
        }

        public DataTable LocAdminAuditReport(int year, int month, int action)
        {
            var sql = "";
            if (action != 0)
            {
                sql = string.Format(@"select learner_details_table.USERNAME as USER_NAME,case when audit_trail.MODULE_ID=1 then 'Person Info'
	        when audit_trail.MODULE_ID=2 then 'Product Info'
	        when audit_trail.MODULE_ID=3 then 'Source'
	        when audit_trail.MODULE_ID=4 then 'Loan Status'
	        else 'Need to define'
	        end as MODULE_NAME,case
	        when audit_trail.ACTION_ID=1 then 'ADD'
	        when audit_trail.ACTION_ID=2 then 'EDIT'
	        else 'Need to define' 
	        end as ACTION_NAME, audit_trail.QRY_DATE, audit_trail.QRY_TIME,audit_trail.QRY_STRING
            from audit_trail, learner_details_table 
            where audit_trail.ID_LEARNER= learner_details_table.ID_LEARNER
	        and audit_trail.ACTION_ID=" + action + " and year(audit_trail.QRY_DATE)=" + year + " and month(audit_trail.QRY_DATE)=" + month + "");
            }
            else
            {
                sql = string.Format(@"	select learner_details_table.USERNAME as USER_NAME,    
	            case
	            when audit_trail.MODULE_ID=1 then 'Person Info'
	            when audit_trail.MODULE_ID=2 then 'Product Info'
	            when audit_trail.MODULE_ID=3 then 'Source'
	            when audit_trail.MODULE_ID=4 then 'Loan Status'
	            else 'Need to define'
	            end as MODULE_NAME,
	            case
	            when audit_trail.ACTION_ID=1 then 'ADD'
	            when audit_trail.ACTION_ID=2 then 'EDIT'
	            else 'Need to define' 
	            end as ACTION_NAME, audit_trail.QRY_DATE, audit_trail.QRY_TIME,audit_trail.QRY_STRING
                from audit_trail, learner_details_table 
                where audit_trail.ID_LEARNER= learner_details_table.ID_LEARNER
	            and year(audit_trail.QRY_DATE)=" + year + " and month(audit_trail.QRY_DATE)=" + month + "");
            }

            return DbQueryManager.GetTable(sql);
        }


        public DataTable ApplicationDeferredOrDeclinedReprot(int producId, int status, string date)
        {
            int reasId;
            List<int> idList = new List<int>();
            DataTable results = null;
            var query1 = String.Format(@"select * from deferred_decline_reason where PROD_ID={0} and DTYPE={1}", producId, status);
            results = DbQueryManager.GetTable(query1);


            if (results != null)
            {
                DataTableReader data = results.CreateDataReader();


                while (data.Read())
                {

                    reasId = (int)data["REAS_ID"];
                    idList.Add(reasId);


                }
                data.Close();
            }

            DataTable table = null;
            var sql = "";
            int i = 0;
            foreach (var data in idList)
            {
                sql += String.Format(
                    @"select REAS_NAME, (select  count(*) as ct from loan_app_details Where DEFE_DECL_REAS_ID={0} and DATE_TIME Like '{1}') as ct from deferred_decline_reason where PROD_ID={2} and DTYPE={3}",
                    data, date, producId, status);
                if (i < idList.Count - 1)
                {
                    sql += " union all ";
                }
                i++;
                //var query =
                //    String.Format(
                //        @"select *, (select  count(*) as ct from loan_app_details Where DEFE_DECL_REAS_ID={0} and DATE_TIME Like '{1}') as ct from deferred_decline_reason where PROD_ID={2} and DTYPE={3}",
                //        data, date, producId, status);
                //table = DbQueryManager.GetTable(query);
                //tableList.Add(table);
            }
            if(!string.IsNullOrEmpty(sql))
                table = DbQueryManager.GetTable(sql);
            return table;
        }

        public DataTable CappingReport(string date1, string date2, int buttonId, int capptype)
        {
            var sql = string.Empty;
            string sql1 = string.Empty;
            string sql2 = string.Empty;
            if (buttonId == 1) //Product Wise
            {
                //sql =
                //    String.Format(
                //        @"select sum(LOAN_APRV_AMOU) as tot from loc_loan_app_info, product_info where loc_loan_app_info.PROD_ID=product_info.ID and (APPRV_DATE BETWEEN '{0}' and '{1}') and LOAN_DECI_STATUS_ID=5 and product_info.PROD_CAPP={2}",
                //        date1, date2, capptype);

                sql = String.Format(@"select product_info.PROD_NAME, product_info.PROD_CAPP_M, product_info.PCOD_, 
                sum(LOAN_APRV_AMOU) as tot,(select sum(LOAN_APRV_AMOU) as tot from loc_loan_app_info, product_info 
                where loc_loan_app_info.PROD_ID=product_info.ID and (APPRV_DATE BETWEEN '{0}' and '{1}')
                and LOAN_DECI_STATUS_ID=5 and product_info.PROD_CAPP={2}) as total from loc_loan_app_info, product_info
                where loc_loan_app_info.PROD_ID= product_info.ID and (APPRV_DATE BETWEEN '{0}' and '{1}') 
                and LOAN_DECI_STATUS_ID=5 and product_info.PROD_CAPP={2} group by product_info.PROD_NAME, 
                product_info.PROD_CAPP_M,product_info.PCOD_ ",date1,date2,capptype);
            }
            else if (buttonId == 2) //Tenor Wise
            {

//                 @"select count(*) tot,sum(CASE WHEN tenor<=36 THEN LOAN_APRV_AMOU ELSE 0 END) tn1,
//                        sum(CASE WHEN tenor>36 THEN LOAN_APRV_AMOU ELSE 0 END) tn2 from loc_loan_app_info, 
//                        product_info where loc_loan_app_info.PROD_ID= product_info.ID and (APPRV_DATE BETWEEN 
//                        '{0}' and '{1}') and LOAN_DECI_STATUS_ID=5 and product_info.TENO_CAPP={2}",
//                        date1, date2, capptype
                sql =
                    String.Format(
                       @" select * from
               (select count(*) tot,sum(CASE WHEN tenor<=36 THEN LOAN_APRV_AMOU ELSE 0 END) tn1, sum(CASE WHEN tenor>36 
               THEN LOAN_APRV_AMOU ELSE 0 END) tn2 from loc_loan_app_info, product_info where loc_loan_app_info.PROD_ID= product_info.ID and (APPRV_DATE BETWEEN 
               '{0}' and '{1}') and LOAN_DECI_STATUS_ID=5 and product_info.TENO_CAPP={2}) a cross join ( 
               select TCOD_36,TCOD_60,TENO_CAPP_36,TENO_CAPP_60,PROD_NAME from product_info where TENO_CAPP={2}) b", date1, date2, capptype);

            }
            else if (buttonId == 3) //Level Wise for SME,Level Wise for PL,Level Wise for AL,_Level Wise for Flexi
            {
                sql = String.Format(
                    @"select APPRV_LEVEL, sum(LOAN_APRV_AMOU) as tot from loc_loan_app_info, product_info where 
                loc_loan_app_info.PROD_ID= product_info.ID and (APPRV_DATE BETWEEN '{0}' and '{1}') and 
                LOAN_DECI_STATUS_ID=5 and product_info.LEVE_CAPP={2} GROUP BY APPRV_LEVEL", date1, date2, capptype);
            }
            return DbQueryManager.GetTable(sql);
        }

        public DataTable GetLevelCap(int capptype)
        {
            var sql = String.Format(@"
select LCOD_L1, LCOD_L2, LCOD_L3, LEVE_CAPP_L1, LEVE_CAPP_L2, LEVE_CAPP_L3,PROD_NAME from product_info where LEVE_CAPP={0}", capptype);
            return DbQueryManager.GetTable(sql);
        }

        public DataTable GetTotal(string date1, string date2, int capptype)
        {
            var sql =
                String.Format(@"select sum(LOAN_APRV_AMOU) as total from loc_loan_app_info,
                product_info where loc_loan_app_info.PROD_ID= product_info.ID and (APPRV_DATE BETWEEN '{0}' 
                and '{1}') and LOAN_DECI_STATUS_ID=5 and product_info.LEVE_CAPP={2}", date1, date2, capptype);
            return DbQueryManager.GetTable(sql);
        }
        public DataTable AccessLoginReports(string fromDate, string toDate,string userCode)
        {
            var sql = string.Empty;
            if (!string.IsNullOrEmpty(userCode))
            {
                sql = string.Format(@"select ACLO_USERLOGIID as PERS_ID,USER_CODE as USERNAME, USER_NAME as NAME_LEARNER, 
                ACLO_LOGINDATETIME as LOGED_IN_TIME,ACLO_LOGOUTDATETIME as LOGED_OUT_TIME from t_pluss_accesslog as s,t_pluss_user as l 
                where l.USER_CODE=s.ACLO_USERLOGIID and s.ACLO_LOGINDATETIME BETWEEN '{0}' and '{1}' and s.ACLO_USERLOGIID= '{2}'", fromDate, toDate,userCode);
            }
            else
            {
                sql = string.Format(@"select ACLO_USERLOGIID as PERS_ID,USER_CODE as USERNAME, USER_NAME as NAME_LEARNER,
                ACLO_LOGINDATETIME as LOGED_IN_TIME,ACLO_LOGOUTDATETIME as LOGED_OUT_TIME from t_pluss_accesslog as s,
                t_pluss_user as l  where l.USER_CODE=s.ACLO_USERLOGIID and s.ACLO_LOGINDATETIME BETWEEN '{0}' and '{1}'",fromDate,toDate);
            }
           

            return DbQueryManager.GetTable(sql);


        }

        public DataTable SegmentWiseCappingReport(string m1)
        {
            var query1 = "";
           
            query1 = String.Format(@"SELECT LOAN_APRV_AMOU, FORMAT(APPRV_DATE,'MM') as APPRV_DATE
                 FROM loc_loan_app_info
                 WHERE ((USER_DEFINE = 'X1') OR
                 (USER_DEFINE = 'Y1') OR
                 (USER_DEFINE = 'Z1') OR
                 (USER_DEFINE = 'Z2')) ANd MONTH(APPRV_DATE)='{0}' and LOAN_DECI_STATUS_ID=5", m1);

            return DbQueryManager.GetTable(query1);
        }

        public DataTable SegmentWiseCappingMonth(string m1)
        {
            var query2 = String.Format(@"
                SELECT LOAN_APRV_AMOU
                FROM loc_loan_app_info
                WHERE ((USER_DEFINE = 'Z3') OR
                    (USER_DEFINE = 'Z4') OR
                    (USER_DEFINE = 'Z5') OR
	                (USER_DEFINE = 'Z6') OR
	                (USER_DEFINE = 'Z7') OR
                    (USER_DEFINE = 'Z8')) and Month(APPRV_DATE)='{0}' and LOAN_DECI_STATUS_ID=5", m1);
            return DbQueryManager.GetTable(query2);
        }


        public DataTable GetRowOne(string month, int productId)
        {
            var query = String.Format(@"SELECT branch_info.REGI_NAME,branch_info.BR_NAME,branch_info.BR_ID,
	isNull(sum(case when loan_app_info.LOAN_APPL_ID is null then 0 else 1 end),0) NO_APPL,
	isNull(sum(case when loan_app_info.LOAN_APPL_ID is null then 0 else loan_app_info.LOAN_APP_AMOU end),0) APPL_AMT
	FROM ( Loc_branch_info branch_info LEFT OUTER JOIN loc_loan_app_info
	loan_app_info ON branch_info.BR_ID = loan_app_info.BR_ID and month(loan_app_info.RECE_DATE)='{0}' and loan_app_info.PROD_ID={1})
	GROUP BY branch_info.REGI_NAME,branch_info.BR_NAME,branch_info.BR_ID
	ORDER BY branch_info.REGI_NAME,branch_info.BR_NAME",month,productId);
            return DbQueryManager.GetTable(query);

        }

        public DataTable GetRowTwo(string year, int productId)
        {
            var query = String.Format(@"SELECT branch_info.REGI_NAME,branch_info.BR_NAME, 
	isNull(sum(case when loan_app_info.LOAN_APPL_ID is null then 0 else 1 end),0) NO_APPL,
	isNull(sum(case when loan_app_info.LOAN_APPL_ID is null then 0 else loan_app_info.LOAN_APP_AMOU end),0) APPL_AMT
	FROM ( loc_branch_info branch_info LEFT OUTER JOIN loc_loan_app_info
	loan_app_info ON branch_info.BR_ID = loan_app_info.BR_ID and year(loan_app_info.RECE_DATE)='{0}' and loan_app_info.PROD_ID={1})
	GROUP BY branch_info.REGI_NAME,branch_info.BR_NAME ORDER BY branch_info.REGI_NAME,branch_info.BR_NAME",year,productId);
            return DbQueryManager.GetTable(query);
        }

        public DataTable GetRowThree(string month, int productId)
        {
            var query = String.Format(@"SELECT branch_info.BR_ID, branch_info.REGI_NAME,branch_info.BR_NAME, 
	isNull(sum(case when loan_app_info.LOAN_APPL_ID is null then 0 else 1 end),0) NO_APPL,
	isNull(sum(case when loan_app_info.LOAN_APPL_ID is null then 0 else loan_app_info.LOAN_APRV_AMOU end),0) APRV_AMT
	FROM (loc_branch_info branch_info LEFT OUTER JOIN loc_loan_app_info loan_app_info 
	ON branch_info.BR_ID = loan_app_info.BR_ID 
	and month(loan_app_info.APPRV_DATE)='{0}' and loan_app_info.PROD_ID={1} )
	GROUP BY branch_info.REGI_NAME,branch_info.BR_NAME,branch_info.BR_ID ORDER BY branch_info.REGI_NAME,branch_info.BR_NAME",month,productId);
            return DbQueryManager.GetTable(query);
        }

        public DataTable GetRowFour(string year, int productId)
        {
            var query = String.Format(@"SELECT branch_info.REGI_NAME,branch_info.BR_NAME, 
	isNull(sum(case when loan_app_info.LOAN_APPL_ID is null then 0 else 1 end),0) NO_APPL,
	isNull(sum(case when loan_app_info.LOAN_APPL_ID is null then 0 else loan_app_info.LOAN_APRV_AMOU end),0) APRV_AMT
	FROM ( loc_branch_info branch_info LEFT OUTER JOIN loc_loan_app_info loan_app_info 
	ON branch_info.BR_ID = loan_app_info.BR_ID 
	and year(loan_app_info.APPRV_DATE)='{0}' and loan_app_info.PROD_ID={1})
	GROUP BY branch_info.REGI_NAME,branch_info.BR_NAME 
	ORDER BY branch_info.REGI_NAME,branch_info.BR_NAME",year,productId);
            return DbQueryManager.GetTable(query);
        }

        public DataTable GetOldValue(int productId, int branchId)
        {
            var query = String.Format(@"SELECT isNull(sum(case when a.RECE_QTY is null then 0 else a.RECE_QTY end),0) RECE_QTY,
isNull(sum(case when a.RECE_VAL is null then 0 else a.RECE_VAL end),0) RECE_VAL,
isNull(sum(case when a.APPR_QTY is null then 0 else a.APPR_QTY end),0) APPR_QTY,
isNull(sum(case when a.APPR_VAL is null then 0 else a.APPR_VAL end),0) APPR_VAL
FROM aperfometer_old_data a WHERE PROD_ID={0} AND BR_ID={1}",productId,branchId);
            return DbQueryManager.GetTable(query);
        }

        public DataTable GetResultM(string year, string monthNo, int productId, int branchId)
        {
            var query = String.Format(@"SELECT isNull(sum(case when aperfometer_target.TARG_QTY is null then 0 else aperfometer_target.TARG_QTY end),0) TARG_QTY,
	   isNull(sum(case when aperfometer_target.TARG_AMOU is null then 0 else aperfometer_target.TARG_AMOU end),0) TARG_AMOU
	   FROM aperfometer_target WHERE PROD_ID={0} AND BR_ID={1} AND TARG_MONTH='{2}' AND TARG_YEAR='{3}'",productId,branchId,monthNo,year);
            return DbQueryManager.GetTable(query);
        }

        public DataTable GetResultY(string year, int productId, int branchId)
        {
            var query = String.Format(@"SELECT isNull(sum(case when aperfometer_target.TARG_QTY is null then 0 else aperfometer_target.TARG_QTY end),0) TARG_QTY,
	  isNull( sum(case when aperfometer_target.TARG_AMOU is null then 0 else aperfometer_target.TARG_AMOU end),0) TARG_AMOU
	   FROM aperfometer_target WHERE PROD_ID={0} AND BR_ID={1}  AND TARG_YEAR='{2}'",productId,branchId,year);
            return DbQueryManager.GetTable(query);
        }
    }

}