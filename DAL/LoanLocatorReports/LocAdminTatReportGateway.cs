﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Bat.Common;

namespace DAL.LoanLocatorReports
{
    public class LocAdminTatReportGateway
    {
        public DataTable IndividualTatReport(string date1, string date2, int productId, string roleCutDate)
        {

            var query = String.Format(@"SELECT LOAN_APPL_ID,PROD_NAME,OP_TAT_CREDIT,OP_TAT_ASSET FROM loc_loan_app_info LEFT OUTER JOIN t_pluss_product ON loc_loan_app_info.PROD_ID = t_pluss_product.PROD_ID where loc_loan_app_info.RECE_DATE BETWEEN '{0}' and '{1}' and loc_loan_app_info.PROD_ID={2} order by RECE_DATE, LOAN_APPL_ID", date1, date2, productId);

            return DbQueryManager.GetTable(query);
        }



        public DataTable GetData(int loanId)
        {
            var sql = "";
            sql = String.Format(@"SELECT LOAN_APPL_ID,
                    CASE
                    WHEN b.DEPT_ID<4 AND b.PROCESS_STATUS_ID<>4 THEN '1Source'
                    WHEN b.DEPT_ID=4 AND b.TAT_VARI = 1 THEN '4Varification'
                    WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=4 THEN '2Transition'
                    WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=16 THEN '3Credit_valuation'
                    WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=18 THEN '3Credit_legal'
                    WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=17 THEN '3Credit_onhold'					
                    WHEN b.DEPT_ID=4 AND ( b.PROCESS_STATUS_ID<>4 OR b.PROCESS_STATUS_ID<>16 OR b.PROCESS_STATUS_ID<>17) THEN '3Credit'
                    WHEN (b.DEPT_ID=5 OR b.DEPT_ID=6 OR DEPT_ID=7) AND b.PROCESS_STATUS_ID=4 THEN '5Transition'
                    WHEN (b.DEPT_ID=5 OR b.DEPT_ID=6 OR DEPT_ID=7) AND b.PROCESS_STATUS_ID=17 THEN '6Assetops_onhold'
                    WHEN (b.DEPT_ID=5 OR b.DEPT_ID=6 OR DEPT_ID=7) AND b.PROCESS_STATUS_ID<>4 THEN '6Asset ops'
                    ELSE'7Invalid'
                    END AS TrType,  isnull(b.TAT,0) AS TAT
                    FROM loan_app_details b 
                    WHERE LOAN_APPL_ID ={0}", loanId);

            return DbQueryManager.GetTable(sql);

        }






        public DataTable IndividualTatReportByDate(string date1, string date2, int productId)
        {
            var query = String.Format(@"SELECT LOAN_APPL_ID,FORMAT(APPL_DATE,'yyyy-MM-dd') as  APPL_DATE, PROD_NAME,OP_TAT_CREDIT,OP_TAT_ASSET 
FROM loc_loan_app_info LEFT OUTER JOIN
product_info ON loc_loan_app_info.PROD_ID = product_info.ID
where loc_loan_app_info.RECE_DATE BETWEEN '{0}' and '{1}'
and loc_loan_app_info.PROD_ID={2} order by RECE_DATE, LOAN_APPL_ID", date1, date2, productId);

            return DbQueryManager.GetTable(query);



        }


        public DataTable GetReportData(int loanId)
        {
            var query = "";
            query = String.Format(@"SELECT 
CASE
WHEN b.DEPT_ID<4 AND b.PROCESS_STATUS_ID<>4 THEN '1Source'
WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=4 THEN '2Transition'
WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=16 THEN '3Credit_valuation'
WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=18 THEN '3Credit_legal'
WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=17 THEN '3Credit_onhold'
WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=15 THEN '3Credit_Cond_approved'
WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=5 THEN '3Credit_approved'					
WHEN b.DEPT_ID=4 AND b.PROCESS_STATUS_ID=9 THEN '3Credit_verification'					
WHEN b.DEPT_ID=4 AND ( b.PROCESS_STATUS_ID<>4 OR b.PROCESS_STATUS_ID<>16 OR b.PROCESS_STATUS_ID<>17 OR b.PROCESS_STATUS_ID<>9) THEN '3Credit'
WHEN (b.DEPT_ID=5 OR b.DEPT_ID=6 OR DEPT_ID=7) AND b.PROCESS_STATUS_ID=4 THEN '5Transition'
WHEN (b.DEPT_ID=5 OR b.DEPT_ID=6 OR DEPT_ID=7) AND b.PROCESS_STATUS_ID=17 THEN '6Assetops_onhold'
WHEN  (b.DEPT_ID=5 OR b.DEPT_ID=6 OR DEPT_ID=7) AND b.PROCESS_STATUS_ID=7 THEN '6Disbursed'
WHEN (b.DEPT_ID=5 OR b.DEPT_ID=6 OR DEPT_ID=7) AND b.PROCESS_STATUS_ID<>4 AND b.PROCESS_STATUS_ID<>7 THEN '6Asset ops'				
ELSE '7Invalid' END AS TrType,FORMAT(DATE_TIME,'yyyy-MM-dd') as DATE_TIME,b.ID, b.TAT AS TAT
FROM loan_app_details b 
WHERE LOAN_APPL_ID={0};", loanId);

            return DbQueryManager.GetTable(query);
        }

        public string getDateFromDetails(int loanId, int id, string order)
        {
            var date = ""; 
            var sql = ""; 
            if (order == "descDate")
            {

                sql = String.Format(@"SELECT FORMAT(DATE_TIME,'yyyy-MM-dd') as DATE_TIME,PROCESS_STATUS_ID FROM loan_app_details b WHERE LOAN_APPL_ID = {0}  AND ID >{1}",loanId,id);
            }
            else if (order == "ascDate")
            {
                sql = String.Format(@"SELECT FORMAT(DATE_TIME,'yyyy-MM-dd') as DATE_TIME,PROCESS_STATUS_ID FROM loan_app_details b
WHERE LOAN_APPL_ID = {0} AND ID >{1} order by ID Desc", loanId, id);
            }

            DataTable dt = DbQueryManager.GetTable(sql);

            if (dt !=null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var status = Convert.ToInt32(dt.Rows[i]["PROCESS_STATUS_ID"]);
                    date = dt.Rows[i]["DATE_TIME"].ToString();

                    if (status==4)
                    {
                        return date;
                    }
                    else
                    {
                        date = "N/A";
                    }
                }
            }
            else
            {
                date = "N/A";
            }
            return date;
        }

        public DataTable GetQuickReport(int year, string month, int productId, int buttonId) 
        {
            var query = "";
            if (buttonId==1) //for month
            {
                query = String.Format(@"select COUNT(*) tot, 
isNull(SUM(CASE WHEN PRE_DECLIEND<>0 THEN 1 ELSE 0 END),0) re_receieved, 
isNull(SUM(CASE WHEN LOAN_DECI_STATUS_ID=15 THEN 1 ELSE 0 END),0) cond_appr,
isNull(SUM(CASE WHEN LOAN_DECI_STATUS_ID=5 THEN 1 ELSE 0 END),0) appr, 
isNull(SUM(CASE WHEN LOAN_DECI_STATUS_ID=6 THEN 1 ELSE 0 END),0) defer, 
isNull(SUM(CASE WHEN LOAN_DECI_STATUS_ID=2 THEN 1 ELSE 0 END),0) decl, 
isNull(SUM(CASE WHEN LOAN_STATUS_ID=9 THEN 1 ELSE 0 END),0) verifi,
isNull(SUM(CASE WHEN LOAN_STATUS_ID=17 THEN 1 ELSE 0 END),0) on_hold,  
isNull(SUM(CASE WHEN LOAN_DISB_DATE is not null  THEN 1 ELSE 0 END),0) disb
from loc_loan_app_info 
where  year(RECE_DATE)='{0}'
and month(RECE_DATE)= '{1}'
and PROD_ID={2}",year,month,productId);
            }
            else if (buttonId==2) // for year
            {
                query = String.Format(@"select COUNT(*) tot, 
isNull(SUM(CASE WHEN PRE_DECLIEND<>0 THEN 1 ELSE 0 END),0) re_receieved, 
isNull(SUM(CASE WHEN LOAN_DECI_STATUS_ID=15 THEN 1 ELSE 0 END),0) cond_appr,
isNull(SUM(CASE WHEN LOAN_DECI_STATUS_ID=5 THEN 1 ELSE 0 END),0) appr, 
isNull(SUM(CASE WHEN LOAN_DECI_STATUS_ID=6 THEN 1 ELSE 0 END),0) defer, 
isNull(SUM(CASE WHEN LOAN_DECI_STATUS_ID=2 THEN 1 ELSE 0 END),0) decl, 
isNull(SUM(CASE WHEN LOAN_STATUS_ID=9 THEN 1 ELSE 0 END),0) verifi,
isNull(SUM(CASE WHEN LOAN_STATUS_ID=17 THEN 1 ELSE 0 END),0) on_hold,  
isNull(SUM(CASE WHEN LOAN_DISB_DATE is not null  THEN 1 ELSE 0 END),0) disb
from loc_loan_app_info where  year(RECE_DATE)='{0}' and PROD_ID={1}",year,productId);
            }

            return DbQueryManager.GetTable(query);
        }

        public DataTable GetCalculatingVol(int year, string month, int productId, int buttonId)
        {
            var query = "";
            if (buttonId==1)
            {
                query = String.Format(@"select sum(LOAN_APP_AMOU) tot,
		isNull(sum(CASE WHEN LOAN_DECI_STATUS_ID=15 THEN LOAN_COND_APRV_AMOU ELSE 0 END),0) cond_appr,
		isNull(sum(CASE WHEN LOAN_DECI_STATUS_ID=5 THEN LOAN_APRV_AMOU ELSE 0 END),0) appr,
		isNull(sum(CASE WHEN LOAN_DECI_STATUS_ID=2 THEN LOAN_APP_AMOU ELSE 0 END),0) decl,
		isNull(sum(case WHEN LOAN_DISB_DATE is not null THEN LOAN_APRV_AMOU ELSE 0 END),0) disb,
		isNull(sum(CASE WHEN LOAN_STATUS_ID=4 THEN LOAN_APRV_AMOU ELSE 0 END),0) verifi
		from loc_loan_app_info where year(RECE_DATE)='{0}'
		and month(RECE_DATE)='{1}' and PROD_ID={2}", year,month,productId); 
            }
            else if (buttonId==2)
            {
                query = String.Format(@"select sum(LOAN_APP_AMOU) tot,
		isNull(sum(CASE WHEN LOAN_DECI_STATUS_ID=15 THEN LOAN_COND_APRV_AMOU ELSE 0 END),0) cond_appr,
		isNull(sum(CASE WHEN LOAN_DECI_STATUS_ID=5 THEN LOAN_APRV_AMOU ELSE 0 END),0) appr,
		isNull(sum(CASE WHEN LOAN_DECI_STATUS_ID=2 THEN LOAN_APP_AMOU ELSE 0 END),0) decl,
		isNull(sum(case WHEN LOAN_DISB_DATE is not null THEN LOAN_APRV_AMOU ELSE 0 END),0) disb,
		isNull(sum(CASE WHEN LOAN_STATUS_ID=4 THEN LOAN_APRV_AMOU ELSE 0 END),0) verifi
		from loc_loan_app_info where year(RECE_DATE)='{0}'and PROD_ID={1}", year,productId); 
            }
            return DbQueryManager.GetTable(query);
        }

        public int GetApplications(string year, string monthNo, int productId, int flag)
        {
            var query = "";
            int value;  
            if (flag==1)
            {
                query = String.Format(@"select count(*) as noapp from loc_loan_app_info a 
WHERE a.PROD_ID={0} and year(a.LOAN_DISB_DATE)='{1}'
and month(a.LOAN_DISB_DATE)='{2}' and a.DEFF_TAT=0",productId,year,monthNo);
            }
            else 
            {
                query = String.Format(@"select count(*) as noapp from loc_loan_app_info a 
WHERE a.PROD_ID={0} and year(a.LOAN_DISB_DATE)='{1}'
and month(a.LOAN_DISB_DATE)='{2}' and a.DEFF_TAT>0", productId, year, monthNo);
            }
          
         value = Convert.ToInt32( DbQueryManager.ExecuteScalar(query));
          
            return value; 
        }

        public DataTable GetTatVar(string year, string monthNo, int productId, int flag)  
        {
            var query = "";
          
            if (flag == 1)
            {
                query = String.Format(@"select
			case
			when b.DEPT_ID<4 and b.PROCESS_STATUS_ID<>4 then '1Source'
			when b.DEPT_ID=4 and b.TAT_VARI =1 then '4Varification'
			when b.DEPT_ID=4 and b.PROCESS_STATUS_ID=4 then '2Transition'
			when b.DEPT_ID=4 and b.PROCESS_STATUS_ID<>4 then '3Credit'
			when (b.DEPT_ID=5 or b.DEPT_ID=6 or DEPT_ID=7) and b.PROCESS_STATUS_ID=4 then '5Transition'
			when (b.DEPT_ID=5 or b.DEPT_ID=6 or DEPT_ID=7) and b.PROCESS_STATUS_ID<>4 then '6Asset ops'
			else '7Invalid'
			end as TrType,
			sum(b.TAT) as TAT
		FROM loc_loan_app_info a, loan_app_details b
		WHERE (a.PROD_ID={0} and year(a.LOAN_DISB_DATE)='{1}' and month(a.LOAN_DISB_DATE)='{2}')
			   and (a.LOAN_APPL_ID = b.LOAN_APPL_ID)
			   and a.DEFF_TAT=0
			   group by TAT,DEPT_ID,PROCESS_STATUS_ID,TAT_VARI
			   order by TrType", productId, year, monthNo);
            }
            else
            {
                query = String.Format(@"select
			case
			when b.DEPT_ID<4 and b.PROCESS_STATUS_ID<>4 then '1Source'
			when b.DEPT_ID=4 and b.TAT_VARI =1 then '4Varification'
			when b.DEPT_ID=4 and b.PROCESS_STATUS_ID=4 then '2Transition'
			when b.DEPT_ID=4 and b.PROCESS_STATUS_ID<>4 then '3Credit'
			when (b.DEPT_ID=5 or b.DEPT_ID=6 or DEPT_ID=7) and b.PROCESS_STATUS_ID=4 then '5Transition'
			when (b.DEPT_ID=5 or b.DEPT_ID=6 or DEPT_ID=7) and b.PROCESS_STATUS_ID<>4 then '6Asset ops'
			else '7Invalid'
			end as TrType,
			sum(b.TAT) as TAT
		FROM loc_loan_app_info a, loan_app_details b
		WHERE (a.PROD_ID={0} and year(a.LOAN_DISB_DATE)='{1}' and month(a.LOAN_DISB_DATE)='{2}')
			   and (a.LOAN_APPL_ID = b.LOAN_APPL_ID)
			   and a.DEFF_TAT>0
			   group by TAT,DEPT_ID,PROCESS_STATUS_ID,TAT_VARI
			   order by TrType", productId, year, monthNo);
            }
            return DbQueryManager.GetTable(query); 
        }

        public decimal GetCredit(string date, int productId)
        {
            var query = String.Format(@"select isnull(sum(isnull(OP_TAT_CREDIT,0)),0) as optatcr
			from loc_loan_app_info
			where PROD_ID={0} and LOAN_DISB_DATE Like '{1}'",productId,date);
            decimal value = Convert.ToDecimal(DbQueryManager.ExecuteScalar(query));
            return value;
        }

        public decimal GetAsset(string date, int productId)
        {
            var query = String.Format(@"select isnull(sum(isnull(OP_TAT_ASSET,0)),0) as optatas
			from loc_loan_app_info
			where PROD_ID={0} and LOAN_DISB_DATE Like '{1}'", productId, date);
            decimal value = Convert.ToDecimal(DbQueryManager.ExecuteScalar(query));
            return value;
        }
    }
}
