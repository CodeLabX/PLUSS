﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// WordDocumentGateway Class
    /// </summary>
    public class WordDocumentGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor
        /// </summary>
        public WordDocumentGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select word document list
        /// </summary>
        /// <returns>Returns a WordDocument list object.</returns>
        public List<WordDocument> SelectWordDocumentList()
        {
            List<WordDocument> WordDocumentObjList = new List<WordDocument>();
            WordDocument wordDocumentObj = null;
            string mSQL = "SELECT * FROM t_pluss_worddocument LEFT JOIN t_pluss_subject ON WODO_SUBJECT_ID=SUBJ_ID";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    wordDocumentObj = new WordDocument();
                    wordDocumentObj.Id = Convert.ToInt32(xRs["WODO_ID"]);
                    wordDocumentObj.To = Convert.ToString(xRs["WODO_TO"]);
                    wordDocumentObj.SubjectId = Convert.ToInt32(xRs["WODO_SUBJECT_ID"]);
                    wordDocumentObj.SubjectName = Convert.ToString(xRs["SUBJ_NAME"]);
                    wordDocumentObj.Body = Convert.ToString(xRs["WODO_BODY"]);
                    wordDocumentObj.Body1 = Convert.ToString(xRs["WODO_BODYONE"]);
                    wordDocumentObj.Body2 = Convert.ToString(xRs["WODO_BODYTWO"]);
                    wordDocumentObj.Body3 = Convert.ToString(xRs["WODO_BODYTHREE"]);
                    wordDocumentObj.Body4 = Convert.ToString(xRs["WODO_BODYFOUR"]);
                    wordDocumentObj.Body5 = Convert.ToString(xRs["WODO_BODYFIVE"]);
                    wordDocumentObj.Body6 = Convert.ToString(xRs["WODO_BODYSIX"]);
                    WordDocumentObjList.Add(wordDocumentObj);
                }
                xRs.Close();
            }
            return WordDocumentObjList;
        }
        /// <summary>
        /// This method select word document
        /// </summary>
        /// <param name="subjectId">Gets a id.</param>
        /// <returns>Returns a WordDocument object.</returns>
        public WordDocument SelectWordDocument(int subjectId)
        {
            WordDocument wordDocumentObj = new WordDocument();
            string mSQL = "SELECT * FROM t_pluss_worddocument WHERE WODO_SUBJECT_ID=" + subjectId;
            ADODB.Recordset xRs = new ADODB.Recordset();
            xRs = dbMySQL.DbOpenRset(mSQL);
            if (!xRs.EOF)
            {
                wordDocumentObj.Id = Convert.ToInt32(xRs.Fields["WODO_ID"].Value);
                wordDocumentObj.To = Convert.ToString(xRs.Fields["WODO_TO"].Value);
                wordDocumentObj.SubjectId = Convert.ToInt32(xRs.Fields["WODO_SUBJECT_ID"].Value);
                wordDocumentObj.Body = Convert.ToString(xRs.Fields["WODO_BODY"].Value);
                wordDocumentObj.Body1 = Convert.ToString(xRs.Fields["WODO_BODYONE"].Value);
                wordDocumentObj.Body2 = Convert.ToString(xRs.Fields["WODO_BODYTWO"].Value);
                wordDocumentObj.Body3 = Convert.ToString(xRs.Fields["WODO_BODYTHREE"].Value);
                wordDocumentObj.Body4 = Convert.ToString(xRs.Fields["WODO_BODYFOUR"].Value);
                wordDocumentObj.Body5 = Convert.ToString(xRs.Fields["WODO_BODYFIVE"].Value);
                wordDocumentObj.Body6 = Convert.ToString(xRs.Fields["WODO_BODYSIX"].Value);
            }
            xRs.Close();
            return wordDocumentObj;
        }
        /// <summary>
        /// This method select word document
        /// </summary>
        /// <param name="wordDocId">Gets a word doc id.</param>
        /// <returns>Returns a WordDocument object.</returns>
        public WordDocument GetWordDocument(int wordDocId)
        {
            WordDocument wordDocumentObj = new WordDocument();
            string mSQL = "SELECT * FROM t_pluss_worddocument WHERE WODO_ID=" + wordDocId;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                if (xRs.Read())
                {
                    wordDocumentObj.Id = Convert.ToInt32(xRs["WODO_ID"]);
                    wordDocumentObj.To = Convert.ToString(xRs["WODO_TO"]);
                    wordDocumentObj.SubjectId = Convert.ToInt32(xRs["WODO_SUBJECT_ID"]);
                    wordDocumentObj.Body = Convert.ToString(xRs["WODO_BODY"]);
                    wordDocumentObj.Body1 = Convert.ToString(xRs["WODO_BODYONE"]);
                    wordDocumentObj.Body2 = Convert.ToString(xRs["WODO_BODYTWO"]);
                    wordDocumentObj.Body3 = Convert.ToString(xRs["WODO_BODYTHREE"]);
                    wordDocumentObj.Body4 = Convert.ToString(xRs["WODO_BODYFOUR"]);
                    wordDocumentObj.Body5 = Convert.ToString(xRs["WODO_BODYFIVE"]);
                    wordDocumentObj.Body6 = Convert.ToString(xRs["WODO_BODYSIX"]);
                }
                xRs.Close();
            }
            return wordDocumentObj;
        }
        /// <summary>
        /// This method provide the operation of insert or update word document
        /// </summary>
        /// <param name="wordDocumentObj">Gets a WordDocument object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendWordDocumentInToDB(WordDocument wordDocumentObj)
        {
            WordDocument existingWordDocumentObj = GetWordDocument(wordDocumentObj.Id);
            if (existingWordDocumentObj.Id > 0)
            {
                wordDocumentObj.Id = existingWordDocumentObj.Id;
                return UpdateWordDocument(wordDocumentObj);
            }
            else
            {
                return InsertWordDocument(wordDocumentObj);
            }
        }
        /// <summary>
        /// This method provide the operation of insert word document
        /// </summary>
        /// <param name="wordDocumentObj">Gets a WordDocument object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertWordDocument(WordDocument wordDocumentObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_worddocument (WODO_TO, WODO_SUBJECT_ID,"+ 
	                          "WODO_BODY, WODO_BODYONE, WODO_BODYTWO,"+ 
	                          "WODO_BODYTHREE, WODO_BODYFOUR,"+ 
	                          "WODO_BODYFIVE, WODO_BODYSIX) VALUES('" +
                              AddSlash(wordDocumentObj.To) + "'," +
                              wordDocumentObj.SubjectId + ",'" +
                              AddSlash(wordDocumentObj.Body) + "','" +
                              AddSlash(wordDocumentObj.Body1) + "','" +
                              AddSlash(wordDocumentObj.Body2) + "','" +
                              AddSlash(wordDocumentObj.Body3) + "','" +
                              AddSlash(wordDocumentObj.Body4) + "','" +
                              AddSlash(wordDocumentObj.Body5) + "','" +
                              AddSlash(wordDocumentObj.Body6) + "')";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide the operation of update word document
        /// </summary>
        /// <param name="wordDocumentObj">Gets a WordDocument object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateWordDocument(WordDocument wordDocumentObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_worddocument SET " +
                              "WODO_TO='" + AddSlash(wordDocumentObj.To) + "'," +
                              "WODO_SUBJECT_ID=" + wordDocumentObj.SubjectId + "," +
                              "WODO_BODY='" + AddSlash(wordDocumentObj.Body) + "'," +
                              "WODO_BODYONE='" + AddSlash(wordDocumentObj.Body1) + "'," +
                              "WODO_BODYTWO='" + AddSlash(wordDocumentObj.Body2) + "'," +
                              "WODO_BODYTHREE='" + AddSlash(wordDocumentObj.Body3) + "'," +
                              "WODO_BODYFOUR='" + AddSlash(wordDocumentObj.Body4) + "'," +
                              "WODO_BODYFIVE='" + AddSlash(wordDocumentObj.Body5) + "'," +
                              "WODO_BODYSIX='" + AddSlash(wordDocumentObj.Body6) + "' WHERE WODO_ID=" + wordDocumentObj.Id;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method convert the special character.
        /// </summary>
        /// <param name="sMessage">Gets a string</param>
        /// <returns>Returns a converted string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        /// <summary>
        /// This method select max id
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public int SelectMaxId()
        {
            string mSQL = "SELECT MAX(WODO_ID) FROM t_pluss_worddocument";
            Int32 returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue + 1;
        }
    }
}
