﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// SubSectorGateway Class
    /// </summary>
    public class SubSectorGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor
        /// </summary>
        public SubSectorGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select sub sector list
        /// </summary>
        /// <returns>Returns a SubSector list object.</returns>
        public List<SubSector> SelectSubSectorObjList()
        {
            List<SubSector> subSectorObjList = new List<SubSector>();
            SubSector subSectorObj = null;
            string mSQL = "SELECT SUSE_ID,SUSE_NAME,SUSE_SECTOR_ID,SECT_NAME,SUSE_CODE,SUSE_INTERESTRATE,SUSE_IRCODE,SUSE_COLOR,SUSE_STATUS FROM t_pluss_subsector LEFT JOIN t_pluss_sector ON SUSE_SECTOR_ID=SECT_ID";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    subSectorObj = new SubSector();
                    subSectorObj.Id = Convert.ToInt32(xRs["SUSE_ID"]);
                    subSectorObj.Name = Convert.ToString(xRs["SUSE_NAME"]);
                    subSectorObj.SectorId = Convert.ToInt32(xRs["SUSE_SECTOR_ID"]);
                    subSectorObj.SectorName = Convert.ToString(xRs["SECT_NAME"]);
                    subSectorObj.Code = Convert.ToString(xRs["SUSE_CODE"]);
                    subSectorObj.InterestRate = Convert.ToString(xRs["SUSE_INTERESTRATE"]);
                    subSectorObj.IRCode = Convert.ToString(xRs["SUSE_IRCODE"]);
                    subSectorObj.Color = Convert.ToString(xRs["SUSE_COLOR"]);
                    subSectorObj.Status = Convert.ToInt32(xRs["SUSE_STATUS"]);
                    subSectorObjList.Add(subSectorObj);
                }
                xRs.Close();
            }
            return subSectorObjList;
        }
        /// <summary>
        /// This method select subsector list
        /// </summary>
        /// <param name="searchKey">Gets a search string </param>
        /// <returns>Returns a SubSector list object.</returns>
        public List<SubSector> SelectSubSectorObjList(string searchKey)
        {
            List<SubSector> subSectorObjList = new List<SubSector>();
            SubSector subSectorObj = null;
            string mSQL = "SELECT SUSE_ID,SUSE_NAME,SUSE_SECTOR_ID,SECT_NAME,SUSE_CODE,SUSE_INTERESTRATE," +
                          "SUSE_IRCODE,SUSE_COLOR,SUSE_STATUS FROM t_pluss_subsector LEFT JOIN t_pluss_sector " +
                          "ON SUSE_SECTOR_ID=SECT_ID WHERE SUSE_NAME LIKE '%" + searchKey + "%' OR SUSE_SECTOR_ID LIKE '%" + searchKey + "%' OR " +
                          "SECT_NAME LIKE '%" + searchKey + "%' OR SUSE_CODE LIKE '%" + searchKey + "%' OR SUSE_INTERESTRATE LIKE '%" + searchKey + "%' OR "+
                          "SUSE_IRCODE LIKE '%" + searchKey + "%' OR SUSE_COLOR LIKE '%" + searchKey + "%' OR SUSE_STATUS LIKE '%" + searchKey + "%'";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    subSectorObj = new SubSector();
                    subSectorObj.Id = Convert.ToInt32(xRs["SUSE_ID"]);
                    subSectorObj.Name = Convert.ToString(xRs["SUSE_NAME"]);
                    subSectorObj.SectorId = Convert.ToInt32(xRs["SUSE_SECTOR_ID"]);
                    subSectorObj.SectorName = Convert.ToString(xRs["SECT_NAME"]);
                    subSectorObj.Code = Convert.ToString(xRs["SUSE_CODE"]);
                    subSectorObj.InterestRate = Convert.ToString(xRs["SUSE_INTERESTRATE"]);
                    subSectorObj.IRCode = Convert.ToString(xRs["SUSE_IRCODE"]);
                    subSectorObj.Color = Convert.ToString(xRs["SUSE_COLOR"]);
                    subSectorObj.Status = Convert.ToInt32(xRs["SUSE_STATUS"]);
                    subSectorObjList.Add(subSectorObj);
                }
                xRs.Close();
            }
            return subSectorObjList;
        }
        /// <summary>
        /// This method select subsector sector id wise
        /// </summary>
        /// <param name="sectorId">Gets a id.</param>
        /// <returns>Returns a Subsector list object.</returns>
        public List<SubSector> SelectSubSectorSectorIdWise(Int32 sectorId)
        {
            List<SubSector> subSectorObjList = new List<SubSector>();
            SubSector subSectorObj = null;
            string mSQL = "SELECT SUSE_ID,SUSE_NAME,SUSE_SECTOR_ID,SECT_NAME,SUSE_CODE,SUSE_INTERESTRATE,SUSE_IRCODE,SUSE_COLOR,SUSE_STATUS FROM t_pluss_subsector LEFT JOIN t_pluss_sector ON SUSE_SECTOR_ID=SECT_ID WHERE SUSE_SECTOR_ID=" + sectorId;
             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    subSectorObj = new SubSector();
                    subSectorObj.Id = Convert.ToInt32(xRs["SUSE_ID"]);
                    subSectorObj.Name = Convert.ToString(xRs["SUSE_NAME"]);
                    subSectorObj.SectorId = Convert.ToInt32(xRs["SUSE_SECTOR_ID"]);
                    subSectorObj.SectorName = Convert.ToString(xRs["SECT_NAME"]);
                    subSectorObj.Code = Convert.ToString(xRs["SUSE_CODE"]);
                    subSectorObj.InterestRate = Convert.ToString(xRs["SUSE_INTERESTRATE"]);
                    subSectorObj.IRCode = Convert.ToString(xRs["SUSE_IRCODE"]);
                    subSectorObj.Color = Convert.ToString(xRs["SUSE_COLOR"]);
                    subSectorObj.Status = Convert.ToInt32(xRs["SUSE_STATUS"]);
                    subSectorObjList.Add(subSectorObj);
             
                }
                xRs.Close();
            }
            return subSectorObjList;
        }
        /// <summary>
        /// This method select sub sector
        /// </summary>
        /// <param name="subSectorId">Gets a id.</param>
        /// <returns>Returns a SubSector object.</returns>
        public SubSector SelectSubSector(Int64 subSectorId)
        {
            SubSector subSectorObj = new SubSector();
            string mSQL = "SELECT * FROM t_pluss_subsector WHERE SUSE_ID=" + subSectorId;
             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                    {
                        subSectorObj.Id = Convert.ToInt32(xRs["SUSE_ID"]);
                        subSectorObj.Name = Convert.ToString(xRs["SUSE_NAME"]);
                        subSectorObj.SectorId = Convert.ToInt32(xRs["SUSE_SECTOR_ID"]);
                        subSectorObj.Code = Convert.ToString(xRs["SUSE_CODE"]);
                        subSectorObj.InterestRate = Convert.ToString(xRs["SUSE_INTERESTRATE"]);
                        subSectorObj.IRCode = Convert.ToString(xRs["SUSE_IRCODE"]);
                        subSectorObj.Color = Convert.ToString(xRs["SUSE_COLOR"]);
                        subSectorObj.Status = Convert.ToInt32(xRs["SUSE_STATUS"]);
                    }
                xRs.Close();
            }
            return subSectorObj;
        }
        /// <summary>
        /// This method select sub sector
        /// </summary>
        /// <param name="sectorId">Gets a sector id.</param>
        /// <param name="subSectorName">Gets a sub sector name</param>
        /// <returns>Returns a SubSector object.</returns>
        public SubSector SelectSubSector(Int64 sectorId, string subSectorName)
        {
            SubSector subSectorObj = new SubSector();
            string mSQL = "SELECT * FROM t_pluss_subsector WHERE SUSE_SECTOR_ID=" + sectorId + " AND UCASE(SUSE_NAME)=UCASE('" + subSectorName + "')";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                    if (xRs.Read())
                    {
                        subSectorObj.Id = Convert.ToInt32(xRs["SUSE_ID"]);
                        subSectorObj.Name = Convert.ToString(xRs["SUSE_NAME"]);
                        subSectorObj.SectorId = Convert.ToInt32(xRs["SUSE_SECTOR_ID"]);
                        subSectorObj.Code = Convert.ToString(xRs["SUSE_CODE"]);
                        subSectorObj.InterestRate = Convert.ToString(xRs["SUSE_INTERESTRATE"]);
                        subSectorObj.IRCode = Convert.ToString(xRs["SUSE_IRCODE"]);
                        subSectorObj.Color = Convert.ToString(xRs["SUSE_COLOR"]);
                        subSectorObj.Status = Convert.ToInt32(xRs["SUSE_STATUS"]);
                    }
                xRs.Close();
            }
            return subSectorObj;
        }
        /// <summary>
        /// This mehod select max sub sector id
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public Int64 SelectMaxSubSectorId()
        {
            string mSQL = "SELECT MAX(SUSE_ID) FROM t_pluss_subsector";
            Int64 returnValue = Convert.ToInt64("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue + 1;
        }
        /// <summary>
        /// This method provide the operation of insert or update subsector
        /// </summary>
        /// <param name="subSectorObj">Gets a SubSector object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendSubSectorInfoInToDB(SubSector subSectorObj)
        {
            SubSector existingSubSectorObj = SelectSubSector(subSectorObj.Id);
            if (existingSubSectorObj.Id > 0)
            {
                subSectorObj.Id = existingSubSectorObj.Id;
                return UpdateSubSector(subSectorObj);
            }
            else
            {
                return InsertSubSector(subSectorObj);
            }
        }
        /// <summary>
        /// This method provide the operation of insert subsector.
        /// </summary>
        /// <param name="subSectorObj">Gets a SubSector object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertSubSector(SubSector subSectorObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_subsector (SUSE_NAME,SUSE_SECTOR_ID,SUSE_CODE,SUSE_INTERESTRATE,SUSE_IRCODE,SUSE_COLOR,SUSE_STATUS) VALUES('" +
                              AddSlash(subSectorObj.Name) + "'," +
                              subSectorObj.SectorId + ",'" +
                              subSectorObj.Code + "','" +
                              subSectorObj.InterestRate + "','" +
                              subSectorObj.IRCode + "','" +
                              subSectorObj.Color + "'," +
                              subSectorObj.Status + ")";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide the operation of update subsector.
        /// </summary>
        /// <param name="subSectorObj">Gets a SubSector object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateSubSector(SubSector subSectorObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_subsector SET " +
                              "SUSE_NAME='"+ AddSlash(subSectorObj.Name) + "'," +
                              "SUSE_SECTOR_ID="+ subSectorObj.SectorId + "," +
                              "SUSE_CODE='"+ subSectorObj.Code + "'," +
                              "SUSE_INTERESTRATE='"+ AddSlash(subSectorObj.InterestRate) + "'," +
                              "SUSE_IRCODE='"+ subSectorObj.IRCode + "'," +
                              "SUSE_COLOR='"+ subSectorObj.Color + "'," +
                              "SUSE_STATUS=" + subSectorObj.Status + " WHERE SUSE_ID=" + subSectorObj.Id;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method select sector id
        /// </summary>
        /// <param name="sectorName">Gets a sector name</param>
        /// <returns>Returns a sector id</returns>
        public Int32 SelectSectorId(string sectorName)
        {
            string mSQL = "SELECT SECT_ID FROM t_pluss_sector WHERE REPLACE(Upper(SECT_NAME),' ','')=REPLACE(Upper('" + sectorName + "'),' ','')";
            return Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
        }
        /// <summary>
        /// This method provide the operation of insert or update sub sector list info
        /// </summary>
        /// <param name="subSectorObjList">Gets a SubSector list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendSubSectorListInfoInToDB(List<SubSector> subSectorObjList)
        {
            int returnValue = 0;
            List<SubSector> insertSubSectorList = new List<SubSector>();
            List<SubSector> updateSubSectorList = new List<SubSector>();
            if (subSectorObjList.Count > 0)
            {
                //for (int i = 0; i < subSectorObjList.Count; i++)
                foreach (SubSector subSectorObj in subSectorObjList)
                {
                    SubSector existingSubSectorObj = SelectSubSector(subSectorObj.SectorId, subSectorObj.Name);
                    if (existingSubSectorObj.Id > 0)
                    {
                        subSectorObj.Id = existingSubSectorObj.Id;
                        returnValue = UpdateSubSectorList(subSectorObj);
                    }
                    else
                    {
                        returnValue = InsertSubSectorList(subSectorObj);
                    }                    
                }
                //if (updateSubSectorList.Count > 0)
                //{
                //    returnValue=UpdateSubSectorList(updateSubSectorList);
                //}
                //if (insertSubSectorList.Count > 0)
                //{
                //    returnValue=InsertSubSectorList(insertSubSectorList);
                //}
            }
            return returnValue;
        }
        /// <summary>
        /// This method provide the operation of insert subsector
        /// </summary>
        /// <param name="subSectorObjList">Gets a SubSector list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertSubSectorList(SubSector subSectorObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_subsector (SUSE_NAME,SUSE_SECTOR_ID,SUSE_CODE,SUSE_INTERESTRATE,SUSE_IRCODE,SUSE_COLOR,SUSE_STATUS) VALUES";
                //for (int i = 0; i < subSectorObjList.Count; i++)
                //{
                    mSQL = mSQL + "('" +
                          AddSlash(subSectorObj.Name) + "'," +
                          subSectorObj.SectorId + ",'" +
                          subSectorObj.Code + "','" +
                          subSectorObj.InterestRate + "','" +
                          subSectorObj.IRCode + "','" +
                          subSectorObj.Color + "'," +
                          subSectorObj.Status + ")";
                //}
                //mSQL = mSQL.Substring(0, mSQL.Length - 1);
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide the operation of update subsector
        /// </summary>
        /// <param name="subSectorObjList">Gets a SubSector list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateSubSectorList(SubSector subSectorObj)
        {
            bool flag = false;
            try
            {
                //for (int i = 0; i < subSectorObjList.Count; i++)
                //{
                    string mSQL = "";
                       mSQL = "UPDATE t_pluss_subsector SET " +
                              "SUSE_NAME='" + AddSlash(subSectorObj.Name) + "'," +
                              "SUSE_SECTOR_ID=" + subSectorObj.SectorId + "," +
                              "SUSE_CODE='" + subSectorObj.Code + "'," +
                              "SUSE_INTERESTRATE='" + AddSlash(subSectorObj.InterestRate) + "'," +
                              "SUSE_IRCODE='" + subSectorObj.IRCode + "'," +
                              "SUSE_COLOR='" + subSectorObj.Color + "'," +
                              "SUSE_STATUS=" + subSectorObj.Status + " WHERE SUSE_ID=" + subSectorObj.Id;
                       if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                       {
                           flag = true;
                       }
                //}
                if (flag == true)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method convert the special character.
        /// </summary>
        /// <param name="sMessage">Gets a string</param>
        /// <returns>Returns a converted string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
