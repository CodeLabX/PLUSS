﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities;
using System.Data;
using System.Data.Common;
using System.Globalization;
using Bat.Common;

namespace DAL
{

    public class BankStatementCalculationGateway
    {
        public DatabaseConnection dbMySQL = null;
        private BankStatementCalculation bankStatementCalculationObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;

        public BankStatementCalculationGateway()
        {
            bankStatementCalculationObj = new BankStatementCalculation();
            dbMySQL = new DatabaseConnection();
        }

        public BankStatementCalculation GetBankStatementCalculation(Int64 lLId)
        {
            BankStatementCalculation bankStatementCalculationObj = null;
            string mSQL = "SELECT * FROM t_pluss_bankstatementcalculationmaster WHERE BSCM_LLID=" + lLId;
           
              var data = DbQueryManager.GetTable(mSQL);
                if (data != null)
               {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    bankStatementCalculationObj = new BankStatementCalculation();
                    bankStatementCalculationObj.Id = Convert.ToInt64(xRs["BSCM_ID"]);
                    bankStatementCalculationObj.LlId = Convert.ToInt64(xRs["BSCM_LLID"]);
                    bankStatementCalculationObj.Total12MonthCreditTurnover =
                        Convert.ToDouble(xRs["BSCM_TOTAL12MONTHCREDITTURNOVER"]);
                    bankStatementCalculationObj.Total12MonthAverageBalance =
                        Convert.ToDouble(xRs["BSCM_TOTAL12MONTHAVERAGEBALANCE"]);
                    bankStatementCalculationObj.Average12MonthCreditTurnover =
                        Convert.ToDouble(xRs["BSCM_AVERAGE12MONTHCREDITTURNOVER"]);
                    bankStatementCalculationObj.Average12MonthAverageBalance =
                        Convert.ToDouble(xRs["BSCM_AVERAGE12MONTHAVERAGEBALANCE"]);
                    bankStatementCalculationObj.Month12MaxBalanceDate =
                        Convert.ToInt16(xRs["BSCM_12MONTHMAXBALANCEDATE"]);
                    bankStatementCalculationObj.Month1250PerAverageBalance =
                        Convert.ToDouble(xRs["BSCM_12MONTH5OPERAVERAGEBALANCE"]);
                    bankStatementCalculationObj.Total6MonthCreditTurnover =
                        Convert.ToDouble(xRs["BSCM_TOTAL6MONTHCREDITTURNOVER"]);
                    bankStatementCalculationObj.Total6MonthAverageBalance =
                        Convert.ToDouble(xRs["BSCM_TOTAL6MONTHAVERAGEBALANCE"]);
                    bankStatementCalculationObj.Average6MonthCreditTurnover =
                        Convert.ToDouble(xRs["BSCM_AVERAGE6MONTHCREDITTURNOVER"]);
                    bankStatementCalculationObj.Average6MonthAverageBalance =
                        Convert.ToDouble(xRs["BSCM_AVERAGE6MONTHAVERAGEBALANCE"]);
                    bankStatementCalculationObj.Month6MaxBalanceDate =
                        Convert.ToInt16(xRs["BSCM_6MONTHMAXBALANCEDATE"]);
                    bankStatementCalculationObj.Month650PerAverageBalance =
                        Convert.ToDouble(xRs["BSCM_6MONTH5OPERAVERAGEBALANCE"]);
                    bankStatementCalculationObj.XmlData = Convert.ToString(xRs["BSCM_XMLDATA"]);
                    bankStatementCalculationObj.Remarks = Convert.ToString(xRs["BSCM_REMARKS"]);
                    bankStatementCalculationObj.CreditPart = Convert.ToInt16(xRs["BSCM_CREDITPART"]);
                    bankStatementCalculationObj.AveragePart = Convert.ToInt16(xRs["BSCM_AVERAGEPART"]);
                    bankStatementCalculationObj.EntryDate = Convert.ToDateTime(xRs["BSCM_ENTRYDATE"]);
                    bankStatementCalculationObj.UserId = Convert.ToInt32(xRs["BSCM_USER_ID"]);
                   
                }
                xRs.Close();
            }
            return bankStatementCalculationObj;
        }        

        public int SendBankStatementCalculationInToDB(BankStatementCalculation bankStatementCalculationObj)
        {
            BankStatementCalculation existingBankStatementCalculationObj = GetBankStatementCalculation(bankStatementCalculationObj.LlId);
            if (existingBankStatementCalculationObj == null)
            {
                return InsertBankStatementCalculation(bankStatementCalculationObj);
            }
            else
            {
                bankStatementCalculationObj.Id = existingBankStatementCalculationObj.Id;
                return UpdateBankStatementCalculation(bankStatementCalculationObj);
            }
        }

        
        private int InsertBankStatementCalculation(BankStatementCalculation bankStatementCalculationObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_bankstatementcalculationmaster (" +
                              "BSCM_LLID," +
                              "BSCM_TOTAL12MONTHCREDITTURNOVER, " +
	                          "BSCM_TOTAL12MONTHAVERAGEBALANCE," +
	                          "BSCM_AVERAGE12MONTHCREDITTURNOVER," +
                              "BSCM_AVERAGE12MONTHAVERAGEBALANCE," +
                              "BSCM_12MONTHMAXBALANCEDATE," +
                              "BSCM_12MONTH5OPERAVERAGEBALANCE," +
                              "BSCM_TOTAL6MONTHCREDITTURNOVER," +
                              "BSCM_TOTAL6MONTHAVERAGEBALANCE," +
                              "BSCM_AVERAGE6MONTHCREDITTURNOVER," +
                              "BSCM_AVERAGE6MONTHAVERAGEBALANCE," +
                              "BSCM_6MONTHMAXBALANCEDATE," + 
                              "BSCM_6MONTH5OPERAVERAGEBALANCE," +
                              "BSCM_XMLDATA," +
                              "BSCM_REMARKS,BSCM_CREDITPART,BSCM_AVERAGEPART, BSCM_ENTRYDATE," + 
                              "BSCM_USER_ID) VALUES( " +
                                bankStatementCalculationObj.LlId + "," +
                                bankStatementCalculationObj.Total12MonthCreditTurnover + "," +
                                bankStatementCalculationObj.Total12MonthAverageBalance + "," +
                                bankStatementCalculationObj.Average12MonthCreditTurnover + "," +
                                bankStatementCalculationObj.Average12MonthAverageBalance + "," +
                                bankStatementCalculationObj.Month12MaxBalanceDate + "," +
                                bankStatementCalculationObj.Month1250PerAverageBalance + "," +
                                bankStatementCalculationObj.Total6MonthCreditTurnover + "," +
                                bankStatementCalculationObj.Total6MonthAverageBalance + "," +
                                bankStatementCalculationObj.Average6MonthCreditTurnover + "," +
                                bankStatementCalculationObj.Average6MonthAverageBalance + "," +
                                bankStatementCalculationObj.Month6MaxBalanceDate + "," +
                                bankStatementCalculationObj.Month650PerAverageBalance + ",'" +
                                AddSlash(bankStatementCalculationObj.XmlData) + "','" +
                                AddSlash(bankStatementCalculationObj.Remarks) + "'," +
                                bankStatementCalculationObj.CreditPart + "," +
                                bankStatementCalculationObj.AveragePart + ",'" +
                                bankStatementCalculationObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                                bankStatementCalculationObj.UserId + ")";

                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }

        }

        private int UpdateBankStatementCalculation(BankStatementCalculation bankStatementCalculationObj)
        {
            string mSQL = "UPDATE t_pluss_bankstatementcalculationmaster SET " +
                              "BSCM_LLID= " + bankStatementCalculationObj.LlId + "," +
                              "BSCM_TOTAL12MONTHCREDITTURNOVER= " + bankStatementCalculationObj.Total12MonthCreditTurnover + "," +
                              "BSCM_TOTAL12MONTHAVERAGEBALANCE=" + bankStatementCalculationObj.Total12MonthAverageBalance + "," +
                              "BSCM_AVERAGE12MONTHCREDITTURNOVER=" + bankStatementCalculationObj.Average12MonthCreditTurnover + "," +
                              "BSCM_AVERAGE12MONTHAVERAGEBALANCE=" + bankStatementCalculationObj.Average12MonthAverageBalance + "," +
                              "BSCM_12MONTHMAXBALANCEDATE=" + bankStatementCalculationObj.Month12MaxBalanceDate + "," +
                              "BSCM_12MONTH5OPERAVERAGEBALANCE=" + bankStatementCalculationObj.Month1250PerAverageBalance + "," +
                              "BSCM_TOTAL6MONTHCREDITTURNOVER=" + bankStatementCalculationObj.Total6MonthCreditTurnover + "," +
                              "BSCM_TOTAL6MONTHAVERAGEBALANCE=" + bankStatementCalculationObj.Total6MonthAverageBalance + "," +
                              "BSCM_AVERAGE6MONTHCREDITTURNOVER=" + bankStatementCalculationObj.Average6MonthCreditTurnover + "," +
                              "BSCM_AVERAGE6MONTHAVERAGEBALANCE=" + bankStatementCalculationObj.Average6MonthAverageBalance + "," +
                              "BSCM_6MONTHMAXBALANCEDATE=" + bankStatementCalculationObj.Month6MaxBalanceDate + "," +
                              "BSCM_6MONTH5OPERAVERAGEBALANCE=" + bankStatementCalculationObj.Month650PerAverageBalance + "," +
                              "BSCM_XMLDATA='" + AddSlash(bankStatementCalculationObj.XmlData) + "'," +
                              "BSCM_REMARKS='" + AddSlash(bankStatementCalculationObj.Remarks) + "'," +
                              "BSCM_CREDITPART=" + bankStatementCalculationObj.CreditPart + "," +
                              "BSCM_AVERAGEPART=" + bankStatementCalculationObj.AveragePart + "," +
                              "BSCM_ENTRYDATE='" + bankStatementCalculationObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                              "BSCM_USER_ID=" + bankStatementCalculationObj.UserId + " WHERE BSCM_ID=" + bankStatementCalculationObj.Id;
            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                return UPDATE;
            }
            else
            {
                return ERROR;
            }
        }


        public string AddSlash(string sMessage)
        {
            sMessage = sMessage.Replace("'","''");
            sMessage = sMessage.Replace('"', '\"');
            sMessage = sMessage.Replace(@"\",@"\\");
            return sMessage;
        }

        //public List<ScbMaster> SelectScbMasterInfo(Int32 lLId)
        //{
        //    List<ScbMaster> scbMasterList = new List<ScbMaster>();
        //    ScbMaster scbMasterObj = null;
        //    ADODB.Recordset xRs = new ADODB.Recordset();
        //    string mSQL = "SELECT * FROM t_pluss_scbmaster WHERE SCBM_LLID=" + lLId;
        //        xRs = dbMySQL.DbOpenRset(mSQL);
        //        while (!xRs.EOF)
        //        {
        //            scbMasterObj = new ScbMaster();
        //            scbMasterObj.scbmID = Convert.ToInt32(xRs.Fields["SCBM_ID"].Value);
        //            scbMasterObj.scbmAcno = Convert.ToString(xRs.Fields["SCBM_ACNO"].Value);
        //            scbMasterObj.scbmAcname = Convert.ToString(xRs.Fields["SCBM_ACNAME"].Value);
        //            scbMasterObj.scbmLedgeraverage = Convert.ToDouble("0" + xRs.Fields["SCBM_LEDGERAVERAGE"].Value);
        //            scbMasterObj.scbmLedger6monthaverage = Convert.ToDouble("0" + xRs.Fields["SCBM_LEDGER6MONTHAVERAGE"].Value);
        //            scbMasterObj.scbmAdjustedctoaverage = Convert.ToDouble("0" + xRs.Fields["SCBM_ADJUSTEDCTOAVERAGE"].Value);
        //            scbMasterObj.scbmAdjustedcto6monthaverage = Convert.ToDouble("0" + xRs.Fields["SCBM_ADJUSTEDCTO6MONTHAVERAGE"].Value);
        //            scbMasterList.Add(scbMasterObj);
        //            xRs.MoveNext();
        //        }
        //        xRs.Close();
        //    return scbMasterList;

        //}

        //public List<SCBHistoricalEntryInfo> SelectSCBHistoricalEntryInfo(Int32 lLId,string acNo)
        //{
        //    List<SCBHistoricalEntryInfo> sCBHistoricalEntryInfoList = new List<SCBHistoricalEntryInfo>();
        //    SCBHistoricalEntryInfo sCBHistoricalEntryInfoObj = null;
        //    ADODB.Recordset xRs = new ADODB.Recordset();
        //    string mSQL = "SELECT SCBM_ID FROM t_pluss_scbmaster WHERE SCBM_LLID=" + lLId + " AND SCBM_ACNO ='" + acNo + "'";
        //    Int64 sCBMasterId = Convert.ToInt64(dbMySQL.DbGetValue(mSQL));
        //    if (sCBMasterId > 0)
        //    {
        //        mSQL = "SELECT SCBD_ID,SCBM_ACNO,SCBM_ACNAME,SCBD_PERIOD,SCBD_ADJUSTMENTCTO,SCBD_ADJMONTHCTO,SCBD_AVERAGELEDGERBALANCE,SCBD_ADJMONTAVERAGEBALANCE FROM t_pluss_scbmaster AS M LEFT JOIN t_pluss_scbdetails AS D ON M.SCBM_ID=D.SCBD_SCBMASTER_ID WHERE D.SCBD_SCBMASTER_ID=" + sCBMasterId;
        //        xRs = dbMySQL.DbOpenRset(mSQL);
        //        while (!xRs.EOF)
        //        {
        //            sCBHistoricalEntryInfoObj = new SCBHistoricalEntryInfo();
        //            sCBHistoricalEntryInfoObj.SCBDetailsId = Convert.ToInt64(xRs.Fields["SCBD_ID"].Value);
        //            sCBHistoricalEntryInfoObj.SCBAcNo = Convert.ToString(xRs.Fields["SCBM_ACNO"].Value);
        //            sCBHistoricalEntryInfoObj.SCBAcName = Convert.ToString(xRs.Fields["SCBM_ACNAME"].Value);
        //            sCBHistoricalEntryInfoObj.SCBPeriod = Convert.ToDateTime(xRs.Fields["SCBD_PERIOD"].Value);
        //            sCBHistoricalEntryInfoObj.SCBAdjustmentcto = Convert.ToDouble(xRs.Fields["SCBD_ADJUSTMENTCTO"].Value);
        //            sCBHistoricalEntryInfoObj.SCBAdjmonthcto = Convert.ToInt16(xRs.Fields["SCBD_ADJMONTHCTO"].Value);
        //            sCBHistoricalEntryInfoObj.SCBAverageledgerbalance = Convert.ToDouble(xRs.Fields["SCBD_AVERAGELEDGERBALANCE"].Value);
        //            sCBHistoricalEntryInfoObj.SCBAdjmontaveragebalance = Convert.ToInt16(xRs.Fields["SCBD_ADJMONTAVERAGEBALANCE"].Value);
        //            sCBHistoricalEntryInfoList.Add(sCBHistoricalEntryInfoObj);
        //            xRs.MoveNext();
        //        }
        //        xRs.Close();
        //    }
        //    return sCBHistoricalEntryInfoList;
        //}

        //public List<string> SelectScbMasterMonthList(Int64 scbMasterId)
        //{
        //    List<string> ScbMasterMonthList = new List<string>();
        //    ADODB.Recordset xRs = new ADODB.Recordset();
        //    string mSQL = "SELECT SCBD_PERIOD FROM t_pluss_scbdetails WHERE SCBD_SCBMASTER_ID=" + scbMasterId;
        //        xRs = dbMySQL.DbOpenRset(mSQL);
        //        while (!xRs.EOF)
        //        {
        //            string ScbMasterMonth = null;
        //            ScbMasterMonth = Convert.ToString(xRs.Fields["SCBD_PERIOD"].Value);
        //            ScbMasterMonthList.Add(ScbMasterMonth);
        //            xRs.MoveNext();
        //        }
        //        xRs.Close();
        //    return ScbMasterMonthList;
        //}
    }
}
