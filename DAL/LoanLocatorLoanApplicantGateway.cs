﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data.Common;
using Bat.Common;
namespace DAL
{
    public class LoanLocatorLoanApplicantGateway
    {
        public DatabaseConnection dbMySQL = null;
        /// <summary>
        /// Constructor.
        /// </summary>
        /// 
        public LoanLocatorLoanApplicantGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select loan locator loan application info.
        /// </summary>
        /// <param name="name">Gets a name.</param>
        /// <param name="fatherName">Gets a father name.</param>
        /// <param name="motherName">Gets a mother name.</param>
        /// <param name="tinNo">Gets a tin no.</param>
        /// <returns>Returns a LoanLocatorLoanApplicant list object.</returns>
        public List<LoanLocatorLoanApplicant> Select(string name, string motherName, List<string> accountNo, string organization)
        {
            List<LoanLocatorLoanApplicant> loanLocatorApplicantList = new List<LoanLocatorLoanApplicant>();
            LoanLocatorLoanApplicant loanLocatorApplicant = new LoanLocatorLoanApplicant();
            string cmdText="";

            
            if (!string.IsNullOrEmpty(name))
            {
                if (cmdText != "")
                {
                    cmdText += " OR RTRIM(Upper(CUST_NAME)) LIKE '%" + AddSlash(name.ToUpper().Trim()) + "%'";
                }
                else
                {
                    cmdText += " AND RTRIM(Upper(CUST_NAME)) LIKE '%" + AddSlash(name.ToUpper().Trim()) + "%'";
                }
                
                
            }
            if (!string.IsNullOrEmpty(motherName))
            {
                if (cmdText != "")
                {
                    cmdText += " OR RTRIM(Upper(MOTHER_NAME)) LIKE '%" + AddSlash(motherName.ToUpper().Trim()) + "%'";
                }
                else
                {
                    cmdText += " AND RTRIM(Upper(MOTHER_NAME)) LIKE '%" + AddSlash(motherName.ToUpper().Trim()) + "%'";
                }
                
            }
            if (accountNo.Count > 0)
            {
                foreach (string masterAcNo in accountNo)
                {
                    if (masterAcNo.Length > 0)
                    {
                        if (cmdText != "")
                        {
                            cmdText += " OR ACC_NUMB LIKE '%" + masterAcNo + "%' ";
                        }
                        else
                        {
                            cmdText += " AND ACC_NUMB LIKE '%" + masterAcNo + "%' ";
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(organization))
            {
                #region commented code

                //if (organization.Length > 0)
                //{
                //    int firstIndex = 0;
                //    string nameSecondPart = "";
                //    string[] splitName = organization.Split(' ');
                //    if (splitName.Length > 0)
                //    {
                //        foreach (string nameObj in splitName)
                //        {
                //            if (nameObj.Length > 2)
                //            {
                //                if (firstIndex == 0)
                //                {
                //                    cmdText += " OR TRIM(UCASE(ORGAN)) LIKE '%" + AddSlash(nameObj.ToUpper().Trim()) + "%'";
                //                    cmdText += " OR TRIM(UCASE(CUST_NAME)) LIKE '%" + AddSlash(nameObj.ToUpper().Trim()) + "%'";
                //                    firstIndex++;
                //                }
                //                else
                //                {
                //                    if (splitName.Length > 2)
                //                    {
                //                        if (nameObj.Length > 2)
                //                        {
                //                            nameSecondPart += nameObj + " ";
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //    else
                //    {
                //        cmdText += " OR TRIM(UCASE(ORGAN)) LIKE '%" + AddSlash(organization.ToUpper().Trim()) + "%'";
                //        cmdText += " OR TRIM(UCASE(CUST_NAME)) LIKE '%" + AddSlash(organization.ToUpper().Trim()) + "%'";
                //    }
                //    if (nameSecondPart.Length > 0)
                //    {
                //        cmdText += " OR TRIM(UCASE(ORGAN)) LIKE '%" + AddSlash(nameSecondPart.ToUpper().Trim()) + "%'";
                //        cmdText += " OR TRIM(UCASE(CUST_NAME)) LIKE '%" + AddSlash(nameSecondPart.ToUpper().Trim()) + "%'";
                //    }
                //}

                #endregion

                if (cmdText != "")
                {
                    cmdText += " OR RTRIM(Upper(ORGAN)) LIKE '%" + AddSlash(organization.ToUpper().Trim()) + "%'";
                    cmdText += " OR RTRIM(Upper(CUST_NAME)) LIKE '%" + AddSlash(organization.ToUpper().Trim()) + "%'";
                }
                else
                {
                    cmdText += " AND RTRIM(Upper(ORGAN)) LIKE '%" + AddSlash(organization.ToUpper().Trim()) + "%'";
                    cmdText += " OR RTRIM(Upper(CUST_NAME)) LIKE '%" + AddSlash(organization.ToUpper().Trim()) + "%'";
                }
               
            }
            if (cmdText != "")
            {
                cmdText += " ORDER BY CUST_NAME;";
            }

            var queryString = string.Format("SELECT LOAN_APPL_ID,PROD_NAME, ACC_NUMB, CUST_NAME, LOAN_APP_AMOU, LOAN_STATUS_NAME, ACC_TYPE, ORGAN, LOAN_APRV_AMOU, LOAN_DECI_STATUS_ID, MOTHER_NAME, BR_NAME as BRAN_NAME " +
                                           "FROM loc_loan_app_info lai " +
                                           "LEFT JOIN loan_status_info lsi ON lai.LOAN_STATUS_ID = lsi.LOAN_STATUS_ID " +
                                           "LEFT JOIN t_pluss_product p ON lai.PROD_ID = p.PROD_ID " +
                                           "LEFT JOIN loc_branch_info b ON lai.BR_ID = b.BR_ID  WHERE 0=0 {0}",cmdText);
            //DbDataReader LLapplicantReader = Bat.Common.DbQueryManager.ExecuteReader(cmdText, "conStringLoanLocator");

            var data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    loanLocatorApplicant = new LoanLocatorLoanApplicant();
                    loanLocatorApplicant.LLID = Convert.ToInt32(xRs["LOAN_APPL_ID"]);
                    loanLocatorApplicant.ApplicantName = xRs["CUST_NAME"].ToString();
                    loanLocatorApplicant.AccountNo = xRs["ACC_NUMB"].ToString();
                    loanLocatorApplicant.LoanStatus = xRs["LOAN_STATUS_NAME"].ToString();
                    loanLocatorApplicant.Product = xRs["PROD_NAME"].ToString();
                    loanLocatorApplicant.Branch = xRs["BRAN_NAME"].ToString();
                    loanLocatorApplicantList.Add(loanLocatorApplicant);
                   
                }
                xRs.Close();
            }
            return loanLocatorApplicantList;
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
