﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// SubjectGateway Class.
    /// </summary>
    public class SubjectGateway
    {
        public DatabaseConnection dbMySQL = null;
        /// <summary>
        /// Constructor
        /// </summary>
        public SubjectGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select subject list
        /// </summary>
        /// <returns>Returns a Subject list object.</returns>
        public List<Subject>SelectSubjectList()
        {
            List<Subject> subjectObjList = new List<Subject>();
            string mSQL = "SELECT SUBJ_ID, SUBJ_NAME FROM t_pluss_subject";
            Subject subjectObj = null;
             var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    subjectObj = new Subject();
                    subjectObj.Id = Convert.ToInt32(xRs["SUBJ_ID"]);
                    subjectObj.Name = Convert.ToString(xRs["SUBJ_NAME"]);
                    subjectObjList.Add(subjectObj);
                }
                xRs.Close();
            }
            return subjectObjList;
        }
    }
}
