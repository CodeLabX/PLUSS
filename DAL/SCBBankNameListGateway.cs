﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    public class SCBBankNameListGateway
    {
        public DatabaseConnection dbMySQL = null;

        public SCBBankNameListGateway()
        {
            dbMySQL = new DatabaseConnection();
        }

        public List<BankNameList> GetSCBBankNameList(string LLID)
        {
            string mSQL =
                "SELECT SCBM_ID,'42' as BANK_ID,'SCB' as BANK_NAME,'1265' as BRANCH_ID,'Main' as BRABCH_NAME, SCBM_ACNO,SCBM_ACNAME,MAX(SCBD_PERIOD) as STARTMONTH,MIN(SCBD_PERIOD) as ENDMONTH FROM t_pluss_scbmaster " +
                "LEFT JOIN t_pluss_scbdetails ON SCBM_ID=SCBD_SCBMASTER_ID WHERE SCBM_LLID='" + LLID +
                "' GROUP BY SCBM_ACNO,SCBM_ID,SCBM_ACNAME";
            List<BankNameList> SCBBankNameObjList = new List<BankNameList>();
            BankNameList SCBBankNameObj = null;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    SCBBankNameObj = new BankNameList();
                    SCBBankNameObj.MasterId = Convert.ToInt32(xRs["SCBM_ID"]);
                    SCBBankNameObj.OtherBankId = Convert.ToInt32(xRs["BANK_ID"]);
                    SCBBankNameObj.OtherBank = Convert.ToString(xRs["BANK_NAME"]);
                    SCBBankNameObj.OtherBranchId = Convert.ToInt32(xRs["BRANCH_ID"]);
                    SCBBankNameObj.OtherBranch = Convert.ToString(xRs["BRABCH_NAME"]);
                    SCBBankNameObj.OtherAccNo = Convert.ToString(xRs["SCBM_ACNO"]);
                    SCBBankNameObj.OtherAcName = Convert.ToString(xRs["SCBM_ACNAME"]);
                    SCBBankNameObj.OtherAccType = "SCB Account";
                    SCBBankNameObj.StartMonth = Convert.ToString(xRs["STARTMONTH"]);
                    SCBBankNameObj.EndMonth = Convert.ToString(xRs["ENDMONTH"]);
                    SCBBankNameObjList.Add(SCBBankNameObj);
                   
                }
                xRs.Close();
            }

            return SCBBankNameObjList;
        }





    }
}
