﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.Report;
using System.Data.SqlClient;
using System.Data.Common;
using Bat.Common;

namespace DAL.Report
{
    public class ApprovalInformationGateway
    {
        public ApprovalInformationGateway()
        {
            //Constructor logic here.
        }
        /// <summary>
        /// This method provide approval report information.
        /// </summary>
        /// <param name="llId">Gets a llId</param>
        /// <returns>Returns approval information object.</returns>
        public ApprovalInformation GetApprovalReportInformation(string llId)
        {
            ApprovalInformation approvalInformationObj = null;
            string queryString = "SELECT  CUST_LLID, CUST_NAME, PL_INCOME, PL_DBR, PL_MUE, CONCAT(SEGM_NAME,'-',SECM_DESCRIPTION) AS SEGMENT, " +
                                 "        (case when PL_SAFETYPLUS_ID=1 then IsNULL(PL_BANCALOANAMOUNT,0) else IsNULL(PL_PROPOSEDLOANAMOUNT,0)end) as PL_PROPOSEDLOANAMOUNT, PL_LEVEL, PL_INTERESTRAT, PL_EMI, PL_TENURE, CUST_PDCSLFORM, CUST_PDCACNO, CUST_FIRSTPAYMENT1, CUST_MASTERNO, " +
                                 "        PL_SCBCREDITCARDLIMIT, DEVI_ENTRYDATE, CUST_TOPUPAMOUNT1, CUST_TOPUPAMOUNT2, CUST_TOPUPAMOUNT3, DEVI_REMARKSCONDITION, PL_LOANTYPE_ID " +
                                 "FROM " +
	                             "        t_pluss_customer AS C " +
                                 "INNER JOIN " +
	                             "        t_pluss_pl AS PL " +
                                 "ON   	  C.CUST_LLID = PL.PL_LLID " +
                                 "INNER JOIN  " +
	                             "        t_pluss_segment AS S " +
                                 "ON      PL.PL_SEGMENT_ID = S.SEGM_ID " +
                                 "INNER JOIN " + 
	                             "        t_pluss_deviation AS D " +
                                 "ON   	  C.CUST_LLID = D.DEVI_LLID " +
                                 "WHERE " +
	                             "        CUST_LLID = '"+ llId +"'";

            string exposureQueryString = "SELECT 	CUDE_CUSTOMER_LLID, SUM(CUDE_OUTSTANDING) AS Exposure " +
                                         "FROM  " +
                                         "           t_pluss_customerdetails " +
                                         "WHERE 	CUDE_CUSTOMER_LLID = '"+ llId +"' " +
                                         "GROUP BY  CUDE_CUSTOMER_LLID ";

            DbDataReader approvalInformationDbDataReaderObj = DbQueryManager.ExecuteReader(queryString);
            DbDataReader exposureDbDataReaderObj = DbQueryManager.ExecuteReader(exposureQueryString);
            if (approvalInformationDbDataReaderObj != null)
            {
                while (approvalInformationDbDataReaderObj.Read())
                {
                    approvalInformationObj = new ApprovalInformation();
                    approvalInformationObj.LLId = Convert.ToString(approvalInformationDbDataReaderObj["CUST_LLID"]);
                    approvalInformationObj.Customer = Convert.ToString(approvalInformationDbDataReaderObj["CUST_NAME"]);
                    approvalInformationObj.Income = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_INCOME"]);
                    approvalInformationObj.DBR = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_DBR"]);
                    approvalInformationObj.MUE = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_MUE"]);
                    approvalInformationObj.Segment = Convert.ToString(approvalInformationDbDataReaderObj["SEGMENT"]);
                    approvalInformationObj.ApprovedAmt = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_PROPOSEDLOANAMOUNT"]);
                    approvalInformationObj.Level = Convert.ToString(approvalInformationDbDataReaderObj["PL_LEVEL"]);
                    approvalInformationObj.IR = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_INTERESTRAT"]);
                    approvalInformationObj.EMI = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_EMI"]);
                    approvalInformationObj.Tenure = Convert.ToInt32("0" + approvalInformationDbDataReaderObj["PL_TENURE"]);
                    approvalInformationObj.UDCAmount = Convert.ToDouble((Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_EMI"]) * Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_TENURE"])));
                    string tempBankName = Convert.ToString(approvalInformationDbDataReaderObj["CUST_PDCSLFORM"]);
                    approvalInformationObj.BankName = tempBankName;
                    string scbBankName = "Standard Chartered Bank";
                    if (tempBankName.Contains("SCB") || tempBankName.ToUpper().Contains(scbBankName.ToUpper()))
                    {
                        approvalInformationObj.Mode = "SI";
                    }
                    else
                    {
                        approvalInformationObj.Mode = "PDC";
                    }
                    if (Convert.ToString(approvalInformationDbDataReaderObj["CUST_PDCACNO"]) != "")
                    {
                        approvalInformationObj.AccountNo = Convert.ToString(approvalInformationDbDataReaderObj["CUST_PDCACNO"]);
                    }
                    else
                    {
                        approvalInformationObj.AccountNo = Convert.ToString(approvalInformationDbDataReaderObj["CUST_MASTERNO"]);
                    }
                    approvalInformationObj.PdcDate = Convert.ToInt32("0" + approvalInformationDbDataReaderObj["CUST_FIRSTPAYMENT1"]);
                    double topUpAmnt1 = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["CUST_TOPUPAMOUNT1"]);
                    double topUpAmnt2 = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["CUST_TOPUPAMOUNT2"]);
                    double topUpAmnt3 = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["CUST_TOPUPAMOUNT3"]);
                    double totalTopUpAmnt = topUpAmnt1;// +topUpAmnt2 + topUpAmnt3;
                    double crditCrdLimit = Convert.ToDouble("0" + approvalInformationDbDataReaderObj["PL_SCBCREDITCARDLIMIT"]);
                    approvalInformationObj.CrdtCrdLimit = crditCrdLimit;
                    double scbExposure = 0;
                    while (exposureDbDataReaderObj.Read())
                    {
                        //exposureDbDataReaderObj.Read();
                        scbExposure = Convert.ToDouble("0" + exposureDbDataReaderObj["Exposure"]);
                        
                    }
                    approvalInformationObj.SCBExposure = scbExposure + crditCrdLimit + approvalInformationObj.ApprovedAmt;
                    approvalInformationObj.TopUpAmount = totalTopUpAmnt;
                    try
                    {
                        approvalInformationObj.AppraisalDate = Convert.ToDateTime(approvalInformationDbDataReaderObj["DEVI_ENTRYDATE"]);

                    }
                    catch (Exception)
                    {

                        approvalInformationObj.AppraisalDate = DateTime.Now;
                    }

                    //Pre-condition remarks
                    string tempCndtion = Convert.ToString(approvalInformationDbDataReaderObj["DEVI_REMARKSCONDITION"]);
                    if (tempCndtion.Length > 0)
                    {
                        string[] rowData = tempCndtion.Split('*');
                        //approvalInformationObj.PreCondition[] = "";
                        for (int i = 0; i < rowData.Length; i++) 
                        {
                            string[] remarkData = rowData[i].Split('#');
                            switch (i)
                            {
                                case 0:                                
                                    approvalInformationObj.PreCondition1= remarkData[1].ToString();
                                    break;
                                case 1:
                                    approvalInformationObj.PreCondition2 = remarkData[1].ToString();
                                    break;
                                case 2:
                                    approvalInformationObj.PreCondition3 = remarkData[1].ToString();
                                    break;
                                case 3:
                                    approvalInformationObj.PreCondition4 = remarkData[1].ToString();
                                    break;
                                case 4:
                                    approvalInformationObj.PreCondition5 = remarkData[1].ToString();
                                    break;
                            }
                            
                        }
                    }
                    approvalInformationObj.LoanTypeId = Convert.ToInt32(approvalInformationDbDataReaderObj["PL_LOANTYPE_ID"]);
                }
                approvalInformationDbDataReaderObj.Close();
            }
            return approvalInformationObj;
        }
    }
}
