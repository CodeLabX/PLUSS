﻿using System;
using System.Collections.Generic;
using Bat.Common;
using BusinessEntities.Report;
using System.Data;
using System.Data.Common;

namespace DAL.Report
{
    public class DailyUnderlyingActivityReportGateway
    {
        private DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        public DailyUnderlyingActivityReportGateway()
        {
            dbMySQL = new DatabaseConnection();
        }

        public List<ActivityReportObject> GetActivityReportDataList(DateTime toDay)
        {
            List<ActivityReportObject> activityReportObjList = new List<ActivityReportObject>();
            string mSQL = "SELECT *, FORMAT(DAR_DATE,'yyyy-MM') as DAR_DATE FROM t_pluss_dailyactivityreport WHERE FORMAT(DAR_DATE,'yyyy-MM')='" + toDay.ToString("yyyy-MM") + "'";
            return activityReportObjList = GetDataList(mSQL);
        }

        public ActivityReportObject GetActivityReportData(DateTime toDay)
        {
            ActivityReportObject activityReportObj = null;
            string mSQL = "SELECT * FROM t_pluss_dailyactivityreport WHERE DAR_DATE ='" + toDay.ToString("yyyy-MM-dd") + "'";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    activityReportObj = new ActivityReportObject();
                    activityReportObj.Id = Convert.ToInt64(xRs["DAR_ID"]);
                    activityReportObj.Date = Convert.ToDateTime(xRs["DAR_DATE"]);

                    activityReportObj.RcFL = Convert.ToInt32(xRs["DAR_RCFL"]);
                    activityReportObj.RcPLSAL = Convert.ToInt32(xRs["DAR_RCPLSAL"]);
                    activityReportObj.RcPLBIZ = Convert.ToInt32(xRs["DAR_RCPLBIZ"]);
                    activityReportObj.RcIPF = Convert.ToInt32(xRs["DAR_RCIPF"]);
                    activityReportObj.RcSTFL = Convert.ToInt32(xRs["DAR_RCSTFL"]);
                    activityReportObj.ReTotal = (activityReportObj.RcFL + activityReportObj.RcPLSAL +
                                                 activityReportObj.RcPLBIZ + activityReportObj.RcIPF +
                                                 activityReportObj.RcSTFL);

                    activityReportObj.PeFL = Convert.ToInt32(xRs["DAR_PEFL"]);
                    activityReportObj.PePLSAL = Convert.ToInt32(xRs["DAR_PEPLSAL"]);
                    activityReportObj.PePLBIZ = Convert.ToInt32(xRs["DAR_PEPLBIZ"]);
                    activityReportObj.PeIPF = Convert.ToInt32(xRs["DAR_PEIPF"]);
                    activityReportObj.PeSTFL = Convert.ToInt32(xRs["DAR_PESTFL"]);
                    activityReportObj.PeTotal = (activityReportObj.PeFL + activityReportObj.PePLSAL +
                                                 activityReportObj.PePLBIZ + activityReportObj.PeIPF +
                                                 activityReportObj.PeSTFL);

                    activityReportObj.PoFL = Convert.ToInt32(xRs["DAR_POFL"]);
                    activityReportObj.PoPLSAL = Convert.ToInt32(xRs["DAR_POPLSAL"]);
                    activityReportObj.PoPLBIZ = Convert.ToInt32(xRs["DAR_POPLBIZ"]);
                    activityReportObj.PoIPF = Convert.ToInt32(xRs["DAR_POIPF"]);
                    activityReportObj.PoSTFL = Convert.ToInt32(xRs["DAR_POSTFL"]);
                    activityReportObj.PoTotal = (activityReportObj.PoFL + activityReportObj.PoPLSAL +
                                                 activityReportObj.PoPLBIZ + activityReportObj.PoIPF +
                                                 activityReportObj.PoSTFL);
                }
                xRs.Close();
            }
            return activityReportObj;
        }
        private List<ActivityReportObject> GetDataList(string mSQL)
        {
            List<ActivityReportObject> activityReportObjList = new List<ActivityReportObject>();
            ActivityReportObject activityReportObj = null;
            //DataTable data = DbQueryManager.GetTable(mSQL);
            DbDataReader xRs = DbQueryManager.ExecuteReader(mSQL);
            if (xRs != null)
            {
                //var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    activityReportObj = new ActivityReportObject();

                    activityReportObj.Id = Convert.ToInt64(xRs["DAR_ID"]);
                    activityReportObj.Date = Convert.ToDateTime(xRs["DAR_DATE"]);

                    activityReportObj.RcFL = Convert.ToInt32(xRs["DAR_RCFL"]);
                    activityReportObj.RcPLSAL = Convert.ToInt32(xRs["DAR_RCPLSAL"]);
                    activityReportObj.RcPLBIZ = Convert.ToInt32(xRs["DAR_RCPLBIZ"]);
                    activityReportObj.RcIPF = Convert.ToInt32(xRs["DAR_RCIPF"]);
                    activityReportObj.RcSTFL = Convert.ToInt32(xRs["DAR_RCSTFL"]);
                    activityReportObj.ReTotal = (activityReportObj.RcFL + activityReportObj.RcPLSAL +
                                                 activityReportObj.RcPLBIZ + activityReportObj.RcIPF +
                                                 activityReportObj.RcSTFL);

                    activityReportObj.PeFL = Convert.ToInt32(xRs["DAR_PEFL"]);
                    activityReportObj.PePLSAL = Convert.ToInt32(xRs["DAR_PEPLSAL"]);
                    activityReportObj.PePLBIZ = Convert.ToInt32(xRs["DAR_PEPLBIZ"]);
                    activityReportObj.PeIPF = Convert.ToInt32(xRs["DAR_PEIPF"]);
                    activityReportObj.PeSTFL = Convert.ToInt32(xRs["DAR_PESTFL"]);
                    activityReportObj.PeTotal = (activityReportObj.PeFL + activityReportObj.PePLSAL +
                                                 activityReportObj.PePLBIZ + activityReportObj.PeIPF +
                                                 activityReportObj.PeSTFL);

                    activityReportObj.PoFL = Convert.ToInt32(xRs["DAR_POFL"]);
                    activityReportObj.PoPLSAL = Convert.ToInt32(xRs["DAR_POPLSAL"]);
                    activityReportObj.PoPLBIZ = Convert.ToInt32(xRs["DAR_POPLBIZ"]);
                    activityReportObj.PoIPF = Convert.ToInt32(xRs["DAR_POIPF"]);
                    activityReportObj.PoSTFL = Convert.ToInt32(xRs["DAR_POSTFL"]);
                    activityReportObj.PoTotal = (activityReportObj.PoFL + activityReportObj.PoPLSAL +
                                                 activityReportObj.PoPLBIZ + activityReportObj.PoIPF +
                                                 activityReportObj.PoSTFL);

                    activityReportObjList.Add(activityReportObj);
                }
                xRs.Close();
            }
            return activityReportObjList;

        }
        public void SendReceiveStatus(ActivityReportObject activityReportObj)
        {
            ActivityReportObject ecistActivityReportObj = GetActivityReportData(DateTime.Now);
            if (ecistActivityReportObj == null)
            {
                InsertReceiveStatus(activityReportObj);
            }
            else
            {
                activityReportObj.Id = ecistActivityReportObj.Id;
                UpdateReceiveStatus(activityReportObj);
            }
        }

        private int InsertReceiveStatus(ActivityReportObject activityReportObj)
        {
            try
            {
                DateTime LastWorkingDay = Convert.ToDateTime(dbMySQL.DbGetValue("SELECT MAX(DAR_DATE) AS DAR_DATE FROM t_pluss_dailyactivityreport"));
                ActivityReportObject pendingActivityReportObj = GetActivityReportData(LastWorkingDay);
                if (pendingActivityReportObj != null)
                {
                    activityReportObj.PeFL = activityReportObj.RcFL + pendingActivityReportObj.PeFL;
                    activityReportObj.PePLSAL = activityReportObj.RcPLSAL + pendingActivityReportObj.PePLSAL;
                    activityReportObj.PePLBIZ = activityReportObj.RcPLBIZ + pendingActivityReportObj.PePLBIZ;
                    activityReportObj.PeIPF = activityReportObj.RcIPF + pendingActivityReportObj.PeIPF;
                    activityReportObj.PeSTFL = activityReportObj.RcSTFL + pendingActivityReportObj.PeSTFL;
                }
                string mSQL = "INSERT INTO t_pluss_dailyactivityreport (DAR_DATE,DAR_RCFL,DAR_RCPLSAL,DAR_RCPLBIZ,DAR_RCIPF,DAR_RCSTFL,DAR_PEFL,DAR_PEPLSAL,DAR_PEPLBIZ,DAR_PEIPF,DAR_PESTFL) VALUES( '" +
                    activityReportObj.Date.ToString("yyyy-MM-dd") + "'," +
                    activityReportObj.RcFL + "," +
                    activityReportObj.RcPLSAL + "," +
                    activityReportObj.RcPLBIZ + "," +
                    activityReportObj.RcIPF + "," +
                    activityReportObj.RcSTFL + "," +
                    activityReportObj.PeFL + "," +
                    activityReportObj.PePLSAL + "," +
                    activityReportObj.PePLBIZ + "," +
                    activityReportObj.PeIPF + "," +
                    activityReportObj.PeSTFL + ")";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }

        }

        private int UpdateReceiveStatus(ActivityReportObject activityReportObj)
        {
            string mSQL = "UPDATE t_pluss_dailyactivityreport SET " +
                                      "DAR_RCFL=DAR_RCFL+" + activityReportObj.RcFL + "," +
                                      "DAR_RCPLSAL=DAR_RCPLSAL+" + activityReportObj.RcPLSAL + "," +
                                      "DAR_RCPLBIZ=DAR_RCPLBIZ+" + activityReportObj.RcPLBIZ + "," +
                                      "DAR_RCIPF=DAR_RCIPF+" + activityReportObj.RcIPF + "," +
                                      "DAR_RCSTFL=DAR_RCSTFL+" + activityReportObj.RcSTFL + "," +
                                      "DAR_PEFL=DAR_PEFL+" + activityReportObj.RcFL + "," +
                                      "DAR_PEPLSAL=DAR_PEPLSAL+" + activityReportObj.RcPLSAL + "," +
                                      "DAR_PEPLBIZ=DAR_PEPLBIZ+" + activityReportObj.RcPLBIZ + "," +
                                      "DAR_PEIPF=DAR_PEIPF+" + activityReportObj.RcIPF + "," +
                                      "DAR_PESTFL=DAR_PESTFL+" + activityReportObj.RcSTFL + " WHERE DAR_ID=" + activityReportObj.Id;
            if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
            {
                return UPDATE;
            }
            else
            {
                return ERROR;
            }
        }

        public void SendProcessStatus(ActivityReportObject activityReportObj)
        {
            ActivityReportObject ecistActivityReportObj = GetActivityReportData(DateTime.Now);
            if (ecistActivityReportObj == null)
            {
                InsertProcessStatus(activityReportObj);
            }
            else
            {
                activityReportObj.Id = ecistActivityReportObj.Id;
                UpdateProcessStatus(activityReportObj);
            }
        }
        private int InsertProcessStatus(ActivityReportObject activityReportObj)
        {
            try
            {
                DateTime LastWorkingDay = Convert.ToDateTime(dbMySQL.DbGetValue("SELECT MAX(DAR_DATE) AS DAR_DATE FROM t_pluss_dailyactivityreport"));
                ActivityReportObject pendingActivityReportObj = GetActivityReportData(LastWorkingDay);
                if (pendingActivityReportObj != null)
                {
                    activityReportObj.PeFL = pendingActivityReportObj.PeFL - activityReportObj.PoFL;
                    activityReportObj.PePLSAL = pendingActivityReportObj.PePLSAL - activityReportObj.PoPLSAL;
                    activityReportObj.PePLBIZ = pendingActivityReportObj.PePLBIZ - activityReportObj.PoPLBIZ;
                    activityReportObj.PeIPF = pendingActivityReportObj.PeIPF - activityReportObj.PoIPF;
                    activityReportObj.PeSTFL = pendingActivityReportObj.PeSTFL - activityReportObj.PoSTFL;
                }
                string mSQL = "INSERT INTO t_pluss_dailyactivityreport (DAR_DATE,DAR_POFL,DAR_POPLSAL,DAR_POPLBIZ,DAR_POIPF,DAR_POSTFL,DAR_PEFL,DAR_PEPLSAL,DAR_PEPLBIZ,DAR_PEIPF,DAR_PESTFL) VALUES( '" +
                    activityReportObj.Date.ToString("yyyy-MM-dd") + "'," +
                    activityReportObj.PoFL + "," +
                    activityReportObj.PoPLSAL + "," +
                    activityReportObj.PoPLBIZ + "," +
                    activityReportObj.PoIPF + "," +
                    activityReportObj.PoSTFL + "," +
                    activityReportObj.PeFL + "," +
                    activityReportObj.PePLSAL + "," +
                    activityReportObj.PePLBIZ + "," +
                    activityReportObj.PeIPF + "," +
                    activityReportObj.PeSTFL + ")";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1) 
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        private int UpdateProcessStatus(ActivityReportObject activityReportObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_dailyactivityreport SET " +
                                          "DAR_POFL=DAR_POFL+" + activityReportObj.PoFL + "," +
                                          "DAR_POPLSAL=DAR_POPLSAL+" + activityReportObj.PoPLSAL + "," +
                                          "DAR_POPLBIZ=DAR_POPLBIZ+" + activityReportObj.PoPLBIZ + "," +
                                          "DAR_POIPF=DAR_POIPF+" + activityReportObj.PoIPF + "," +
                                          "DAR_POSTFL=DAR_POSTFL+" + activityReportObj.PoSTFL + "," +
                                          "DAR_PEFL=DAR_PEFL-" + activityReportObj.PoFL + "," +
                                          "DAR_PEPLSAL=DAR_PEPLSAL-" + activityReportObj.PoPLSAL + "," +
                                          "DAR_PEPLBIZ=DAR_PEPLBIZ-" + activityReportObj.PoPLBIZ + "," +
                                          "DAR_PEIPF=DAR_PEIPF-" + activityReportObj.PoIPF + "," +
                                          "DAR_PESTFL=DAR_PESTFL-" + activityReportObj.PoSTFL + " WHERE DAR_ID=" + activityReportObj.Id;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }


        public List<ActivityReportObject> GetActivityReportByUser(DateTime dateTime, DateTime toDate)
        {
            List<ActivityReportObject> activityReportObjList = new List<ActivityReportObject>();

            string queryString = string.Format(@"SELECT PU.USER_NAME, FL,PL_SAL, PL_BIZ, IPF, STFL, FL+PL_SAL+PL_BIZ+IPF+STFL AS TOTAL FROM ( 
                SELECT PL_USER_ID, SUM(FL)AS FL, SUM(PL_SAL)AS PL_SAL, SUM(PL_BIZ)AS PL_BIZ, 
                SUM(IPF)AS IPF, SUM(STFL)AS STFL
                FROM( 
                SELECT 	PL_USER_ID, PL_LLID, PL_PRODUCT_ID,
                IIF(PL_PRODUCT_ID = 4,1,0) AS FL, 
                IIF(PL_PRODUCT_ID = 1 AND PL_SEGMENT_ID < 6, 1, 0) AS PL_SAL, 
                IIF(PL_PRODUCT_ID = 1 AND PL_SEGMENT_ID >5, 1, 0) AS PL_BIZ, 
                IIF(PL_PRODUCT_ID = 12, 1, 0) AS IPF, 
                IIF(PL_PRODUCT_ID > 12, 1, 0) AS STFL 
                FROM T_PLUSS_PL 
                WHERE PL_ENTRYDATE BETWEEN '" + dateTime.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "' " +
                ") AS T1 " +
                "GROUP BY PL_USER_ID " +
                ") AS T2 " +
                "INNER JOIN t_pluss_user AS PU ON PU.USER_ID = T2.PL_USER_ID ");
            ActivityReportObject activityReportObj = null;
            DataTable activityReportObjTableObj = DbQueryManager.GetTable(queryString);
            if (activityReportObjTableObj != null)
            {
                DataTableReader activityReportObjDataReader = activityReportObjTableObj.CreateDataReader();

                while (activityReportObjDataReader.Read())
                {
                    activityReportObj = new ActivityReportObject();
                    activityReportObj.Name = Convert.ToString(activityReportObjDataReader["USER_NAME"]);
                    activityReportObj.PoFL = Convert.ToInt32(activityReportObjDataReader["FL"]);
                    activityReportObj.PoPLSAL = Convert.ToInt32(activityReportObjDataReader["PL_SAL"]);
                    activityReportObj.PoPLBIZ = Convert.ToInt32(activityReportObjDataReader["PL_BIZ"]);
                    activityReportObj.PoIPF = Convert.ToInt32(activityReportObjDataReader["IPF"]);
                    activityReportObj.PoSTFL = Convert.ToInt32(activityReportObjDataReader["STFL"]);
                    activityReportObj.PoTotal = Convert.ToInt32(activityReportObjDataReader["TOTAL"]);
                    activityReportObjList.Add(activityReportObj);
                }
                activityReportObjDataReader.Close();

            }
            return activityReportObjList;
        }

        public List<DailyNoofApprovedVolume> GetDailyNoOfApproveVolume()
        {
            //LIMIT 0,1 after desc
            List<DailyNoofApprovedVolume> dailyNoofApprovedVolumeObjList = new List<DailyNoofApprovedVolume>();
            string queryString = string.Format(@"SELECT LWD.PROD_ID, LWD.PROD_NAME, isnull(LWD.TotalNumber,0) AS LWD_QUANTITY, isnull(LWD.APQU_LOANAMOUNT,0) AS LWD_VOLUME, isnull(MTD.TotalNumber,0) AS MTD_QUANTITY, isnull(MTD.APQU_LOANAMOUNT,0) AS MTD_VOLUME, isnull(YTD.TotalNumber,0) AS YTD_QUANTITY, isnull(YTD.APQU_LOANAMOUNT,0) AS YTD_VOLUME FROM (SELECT T1.PROD_ID,T1.PROD_NAME,isnull(T2.TotalNumber,0) as TotalNumber,isnull(T2.APQU_LOANAMOUNT,0) as APQU_LOANAMOUNT FROM (SELECT PROD_ID,PROD_NAME FROM t_pluss_product WHERE PROD_ID IN (1,4,12)) T1 LEFT JOIN (SELECT LOAN_APPLI_ID,PROD_ID,COUNT(APQU_LOANAMOUNT) as TotalNumber ,SUM(APQU_LOANAMOUNT) as APQU_LOANAMOUNT
		   FROM loan_app_info,t_pluss_approvalqueue 
		   WHERE  APQU_LLID=LOAN_APPLI_ID 
		   AND APQU_ENTRYDATE = (SELECT top 1 APQU_ENTRYDATE 
								 FROM t_pluss_approvalqueue 
								 WHERE APQU_ENTRYDATE<getdate()
								 ORDER BY APQU_ENTRYDATE DESC)
		  GROUP BY LOAN_APPLI_ID,PROD_ID ) AS T2 ON T1.PROD_ID=T2.PROD_ID) as LWD 
LEFT JOIN (SELECT T1.PROD_ID,T1.PROD_NAME,isnull(T2.TotalNumber,0) as TotalNumber,isnull(T2.APQU_LOANAMOUNT,0) as APQU_LOANAMOUNT
		   FROM (SELECT PROD_ID,PROD_NAME 
				 FROM t_pluss_product 
				 WHERE PROD_ID IN (1,4,12)) AS T1 
LEFT JOIN (
SELECT SUM(APQU_LOANAMOUNT) as APQU_LOANAMOUNT,COUNT(APQU_LOANAMOUNT) as TotalNumber ,LOAN_APPLI_ID,PROD_ID FROM loan_app_info,t_pluss_approvalqueue WHERE APQU_LLID=LOAN_APPLI_ID AND YEAR(APQU_ENTRYDATE) = YEAR(getdate()) AND MONTH(APQU_ENTRYDATE) = MONTH(getdate()) GROUP BY PROD_ID, LOAN_APPLI_ID
) AS T2 ON T1.PROD_ID=T2.PROD_ID) as MTD ON LWD.PROD_ID=MTD.PROD_ID LEFT JOIN (SELECT T1.PROD_ID,T1.PROD_NAME,isnull(T2.TotalNumber,0) as TotalNumber,isnull(T2.APQU_LOANAMOUNT,0) as APQU_LOANAMOUNT FROM (SELECT PROD_ID,PROD_NAME FROM t_pluss_product WHERE PROD_ID IN (1,4,12) ) AS T1 
LEFT JOIN (SELECT LOAN_APPLI_ID,PROD_ID,COUNT(APQU_LOANAMOUNT) as TotalNumber ,SUM(APQU_LOANAMOUNT) as APQU_LOANAMOUNT FROM loan_app_info,t_pluss_approvalqueue WHERE APQU_LLID=LOAN_APPLI_ID AND YEAR(APQU_ENTRYDATE) = YEAR(getdate()) GROUP BY PROD_ID,LOAN_APPLI_ID ) AS T2 ON T1.PROD_ID=T2.PROD_ID) AS YTD ON MTD.PROD_ID=YTD.PROD_ID");
            DailyNoofApprovedVolume dailyNoofApprovedVolumeObj = null;
            DataTable dailyNoofApprovedVolumeTableObj = DbQueryManager.GetTable(queryString);
            if (dailyNoofApprovedVolumeTableObj != null)
            {
                DataTableReader dailyNoofApprovedVolumeDataReader = dailyNoofApprovedVolumeTableObj.CreateDataReader();

                while (dailyNoofApprovedVolumeDataReader.Read())
                {
                    dailyNoofApprovedVolumeObj = new DailyNoofApprovedVolume();
                    dailyNoofApprovedVolumeObj.ProductId = Convert.ToInt32(dailyNoofApprovedVolumeDataReader["PROD_ID"]);
                    dailyNoofApprovedVolumeObj.ProductName = Convert.ToString(dailyNoofApprovedVolumeDataReader["PROD_NAME"]);
                    dailyNoofApprovedVolumeObj.LWDQuantity = Convert.ToDouble(dailyNoofApprovedVolumeDataReader["LWD_QUANTITY"]);
                    dailyNoofApprovedVolumeObj.LWDVolume = Convert.ToDouble(dailyNoofApprovedVolumeDataReader["LWD_VOLUME"]);
                    dailyNoofApprovedVolumeObj.MTDQuantity = Convert.ToDouble(dailyNoofApprovedVolumeDataReader["MTD_QUANTITY"]);
                    dailyNoofApprovedVolumeObj.MTDVolume = Convert.ToDouble(dailyNoofApprovedVolumeDataReader["MTD_VOLUME"]);
                    dailyNoofApprovedVolumeObj.YTDQuantity = Convert.ToDouble(dailyNoofApprovedVolumeDataReader["YTD_QUANTITY"]);
                    dailyNoofApprovedVolumeObj.YTDVolume = Convert.ToDouble(dailyNoofApprovedVolumeDataReader["YTD_VOLUME"]);
                    dailyNoofApprovedVolumeObjList.Add(dailyNoofApprovedVolumeObj);
                }
                dailyNoofApprovedVolumeDataReader.Close();

            }
            return dailyNoofApprovedVolumeObjList;
        }
        public List<DailyNoofApprovedVolume> GetDailyActivityReportByChannelWise()
        {
            List<DailyNoofApprovedVolume> DNoOfAVCObjList = new List<DailyNoofApprovedVolume>();
            string mSQL = string.Format(@"SELECT DS.PROD_ID,DS.PROD_NAME,DS.TotalNumber AS DS_TotalNumber,DS.APQU_LOANAMOUNT AS DS_APQU_LOANAMOUNT,SB.TotalNumber as SB_TotalNumber,SB.APQU_LOANAMOUNT AS SB_APQU_LOANAMOUNT FROM 
                        (SELECT T1.PROD_ID,T1.PROD_NAME,isnull(T2.TotalNumber,0) as TotalNumber,isnull(T2.APQU_LOANAMOUNT,0) as APQU_LOANAMOUNT FROM 
                        ( 
                        SELECT PROD_ID,PROD_NAME FROM t_pluss_product WHERE PROD_ID IN (1,4,12) 
                        ) AS T1 LEFT JOIN 
                        ( 
                        SELECT LOAN_APPLI_ID,PROD_ID,BR_ID,SCSO_REGIONNAME,COUNT(APQU_LOANAMOUNT) as TotalNumber ,SUM(APQU_LOANAMOUNT) as APQU_LOANAMOUNT  FROM loan_app_info,t_pluss_scbsource,t_pluss_approvalqueue WHERE BR_ID=SCSO_ID AND APQU_LLID=LOAN_APPLI_ID AND APQU_ENTRYDATE = getdate()  AND SCSO_REGIONNAME ='Direct Sales' GROUP BY PROD_ID,LOAN_APPLI_ID,BR_ID,SCSO_REGIONNAME ) AS T2 ON T1.PROD_ID=T2.PROD_ID) as DS  LEFT JOIN (SELECT T1.PROD_ID,T1.PROD_NAME,isnull(T2.TotalNumber,0) as TotalNumber,isnull(T2.APQU_LOANAMOUNT,0) as APQU_LOANAMOUNT FROM  ( SELECT PROD_ID,PROD_NAME FROM t_pluss_product WHERE PROD_ID IN (1,4,12) 
                        ) AS T1 LEFT JOIN 
                        ( 
                        SELECT LOAN_APPLI_ID,PROD_ID,BR_ID,SCSO_REGIONNAME,COUNT(APQU_LOANAMOUNT) as TotalNumber ,SUM(APQU_LOANAMOUNT) as APQU_LOANAMOUNT 
                        FROM loan_app_info,t_pluss_scbsource,t_pluss_approvalqueue 
                        WHERE BR_ID=SCSO_ID AND APQU_LLID=LOAN_APPLI_ID AND APQU_ENTRYDATE = getdate()  
                        AND SCSO_REGIONNAME !='Direct Sales' GROUP BY LOAN_APPLI_ID,PROD_ID,BR_ID,SCSO_REGIONNAME 
                        ) AS T2 ON T1.PROD_ID=T2.PROD_ID) as SB ON DS.PROD_ID=SB.PROD_ID");
            DailyNoofApprovedVolume DNoOfAVCObj = null;
            DataTable DNoOfAVCTableObj = DbQueryManager.GetTable(mSQL);
            if (DNoOfAVCTableObj != null)
            {
                DataTableReader DNoOfAVCDataReader = DNoOfAVCTableObj.CreateDataReader();

                while (DNoOfAVCDataReader.Read())
                {
                    DNoOfAVCObj = new DailyNoofApprovedVolume();
                    DNoOfAVCObj.ProductId = Convert.ToInt32(DNoOfAVCDataReader["PROD_ID"]);
                    DNoOfAVCObj.ProductName = Convert.ToString(DNoOfAVCDataReader["PROD_NAME"]);
                    DNoOfAVCObj.LWDQuantity = Convert.ToDouble(DNoOfAVCDataReader["DS_TotalNumber"]);
                    DNoOfAVCObj.LWDVolume = Convert.ToDouble(DNoOfAVCDataReader["DS_APQU_LOANAMOUNT"]);
                    DNoOfAVCObj.MTDQuantity = Convert.ToDouble(DNoOfAVCDataReader["SB_TotalNumber"]);
                    DNoOfAVCObj.MTDVolume = Convert.ToDouble(DNoOfAVCDataReader["SB_APQU_LOANAMOUNT"]);
                    DNoOfAVCObjList.Add(DNoOfAVCObj);
                }
                DNoOfAVCDataReader.Close();

            }
            return DNoOfAVCObjList;
        }
    }
}
