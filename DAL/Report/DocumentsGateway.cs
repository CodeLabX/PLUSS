﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.Report;
using System.Data;
using Bat.Common;

namespace DAL.Report
{
    public class DocumentsGateway
    {
        public DocumentsGateway()
        {
            //Constructor logic here.
        }
        /// <summary>
        /// This method provide customer document information.
        /// </summary>
        /// <param name="llId">Gets customer llId.</param>
        /// <returns>Returns document object.</returns>
        public Document GetDocumentsInfo(string llId)
        {
            Document documentObj = null;
            string queryString = "SELECT CUST_LLID, CUST_NAME, CUST_MASTERNO, CUST_OTHERSTATEMENT1B, CUST_OTHERSTATEMENT1A, " + 
	                             "       CUST_OTHERSTATEMENT2B, CUST_OTHERSTATEMENT2A, CUST_OTHERSTATEMENT3B, CUST_OTHERSTATEMENT3A, " + 
	                             "       CUST_OTHERSTATEMENT4B, CUST_OTHERSTATEMENT4A, CUST_OTHERSTATEMENT5B, CUST_OTHERSTATEMENT5A " + 
                                 "FROM " + 
	                             "       t_pluss_customer  " + 
                                 "WHERE  CUST_LLID = '" + llId +"'";
            DataTable documentDataTableObj = DbQueryManager.GetTable(queryString);
            if (documentDataTableObj != null)
            {
                DataTableReader documentDataTableReaderObj = documentDataTableObj.CreateDataReader();
                if (documentDataTableReaderObj != null)
                {
                    while (documentDataTableReaderObj.Read())
                    {
                        documentObj = new Document();
                        documentObj.LLID = Convert.ToString(documentDataTableReaderObj["CUST_LLID"]);
                        documentObj.CustomerName = Convert.ToString(documentDataTableReaderObj["CUST_NAME"]);
                        documentObj.Account = Convert.ToString(documentDataTableReaderObj["CUST_MASTERNO"]);
                        documentObj.Bank1 = Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT1B"]) + " - " + Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT1A"]);
                        documentObj.Bank2 = Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT2B"]) + " - " + Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT2A"]);
                        documentObj.Bank3 = Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT3B"]) + " - " + Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT3A"]);
                        documentObj.Bank4 = Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT4B"]) + " - " + Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT4A"]);
                        documentObj.Bank5 = Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT5B"]) + " - " + Convert.ToString(documentDataTableReaderObj["CUST_OTHERSTATEMENT5A"]);
                    }
                    documentDataTableReaderObj.Close();
                }
                documentDataTableObj.Clear();
            }
            return documentObj;
        }
    }
}
