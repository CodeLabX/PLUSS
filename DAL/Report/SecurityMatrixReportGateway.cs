﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;
using BusinessEntities.Report;

namespace DAL.Report
{
    public class SecurityMatrixReportGateway
    {
        public List<SecurityMatrix> GetSystemGeneratedSecurityMatrix()
        {
            var userListObj = new List<SecurityMatrix>();
            string queryString = @"SELECT * FROM t_pluss_securitymatrix";

            DataTable userDataTableObj = new DataTable();
            userDataTableObj = DbQueryManager.GetTable(queryString);
            if (userDataTableObj != null)
            {
                DataTableReader userDataTableReaderObj = userDataTableObj.CreateDataReader();
                if (userDataTableReaderObj != null)
                {
                    while (userDataTableReaderObj.Read())
                    {
                        var userObj = new SecurityMatrix();
                       
                        userObj.Module = Convert.ToString(userDataTableReaderObj["Module"].ToString());
                        userObj.Analyst = Convert.ToString(userDataTableReaderObj["Analyst"].ToString());
                        userObj.Approver = Convert.ToString(userDataTableReaderObj["Approver"].ToString());
                        userObj.SupportUser = Convert.ToString(userDataTableReaderObj["SupportUser"].ToString());
                        userObj.ScoreAdmin = Convert.ToString(userDataTableReaderObj["ScoreAdmin"]);
                        userObj.BusinessAdmin = Convert.ToString(userDataTableReaderObj["BusinessAdmin"]);
                        userObj.Adminchecker = Convert.ToString(userDataTableReaderObj["Adminchecker"]);
                        userObj.Adminmaker = Convert.ToString(userDataTableReaderObj["Adminmaker"]);
                   
                        userListObj.Add(userObj);
                    }
                    userDataTableReaderObj.Close();
                }
                userDataTableObj.Clear();
            }
            return userListObj;
        }

        public DataTable GetSecurityMatrixReportData()
        {
            string spName = "sp_GetSecurityMatrixData";

            DataTable table = DbQueryManager.GetDataTableWithSp(spName);

            return table;
        }

        public List<SecurityMatrix_Auto> GetSystemGeneratedSecurityMatrix_Auto()
        {
            var userListObj = new List<SecurityMatrix_Auto>();
            string queryString = @"SELECT * FROM t_pluss_securitymatrix_auto";

            DataTable userDataTableObj = new DataTable();
            userDataTableObj = DbQueryManager.GetTable(queryString);
            if (userDataTableObj != null)
            {
                DataTableReader userDataTableReaderObj = userDataTableObj.CreateDataReader();
                if (userDataTableReaderObj != null)
                {
                    while (userDataTableReaderObj.Read())
                    {
                        var userObj = new SecurityMatrix_Auto();

                        userObj.Module = Convert.ToString(userDataTableReaderObj["Module"].ToString());
                        userObj.AutoAdmin = Convert.ToString(userDataTableReaderObj["AutoAdmin"].ToString());
                        userObj.AutoAnalystAndApprover = Convert.ToString(userDataTableReaderObj["AutoAnalystAndApprover"].ToString());
                        userObj.AutoApprover = Convert.ToString(userDataTableReaderObj["AutoApprover"].ToString());
                        userObj.CIAnalyst = Convert.ToString(userDataTableReaderObj["CIAnalyst"]);
                        userObj.CISupport = Convert.ToString(userDataTableReaderObj["CISupport"]);
                        userObj.OpsChecker = Convert.ToString(userDataTableReaderObj["OpsChecker"]);
                        userObj.OpsMaker = Convert.ToString(userDataTableReaderObj["OpsMaker"]);
                        userObj.Sales = Convert.ToString(userDataTableReaderObj["Sales"]);

                        userListObj.Add(userObj);
                    }
                    userDataTableReaderObj.Close();
                }
                userDataTableObj.Clear();
            }
            return userListObj;
        }

        public DataTable GetModuleTable()
        {
            try
            {
                string sql = string.Format(@"SELECT * FROM Modules");

                DataTable table = DbQueryManager.GetTable(sql);
                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
