﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using Bat.Common;
using BusinessEntities.Report;
using System.Data;

namespace DAL.Report
{
    /// <summary>
    /// PL check list report gateway.
    /// </summary>
    public class PLCheckListReportGateway
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public PLCheckListReportGateway()
        {    
            //Constructor logic here.
        }

        /// <summary>
        /// This method provide initail data for the report.
        /// </summary>
        /// <param name="llID">Gets llid.</param>
        /// <returns>Returns initial info object.</returns>
        public InitialInfo GetInitialInfo(int llID)
        {
            string query = " SELECT p.ACC_NO, p.CUST_NM, p.LOAN_APPLI_ID, p.DOB, PL_LOANTYPE_ID " +
                           " FROM loan_app_info AS p, t_pluss_pl AS pl " +
                           " WHERE " +
                           " p.LOAN_APPLI_ID = pl.PL_LLID " +
                           " AND p.LOAN_APPLI_ID = " + llID + " ";

            InitialInfo initialInfo = new InitialInfo();

            DbDataReader initialInfoReader = DbQueryManager.ExecuteReader(query);

            if(initialInfoReader != null)
            {
                while(initialInfoReader.Read())
                {
                    initialInfo.LLID = llID;
                    initialInfo.MasterNo = initialInfoReader["ACC_NO"].ToString();
                    initialInfo.CustomerName = initialInfoReader["CUST_NM"].ToString();
                    if (!string.IsNullOrEmpty(Convert.ToDateTime(initialInfoReader["DOB"]).ToString()))
                    {
                        initialInfo.DOB = Convert.ToDateTime(initialInfoReader["DOB"].ToString());
                    }
                   
                    initialInfo.LoanType = Convert.ToString(initialInfoReader["PL_LOANTYPE_ID"]);
                }
                initialInfoReader.Close();
            }
            return initialInfo;
        }

        /// <summary>
        /// This method provide particular segment information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns a segment info object.</returns>
        public SegmentInfo GetSegmentInfo(int llID)
        {
            string query = " SELECT SEGM_CODE, SECM_DESCRIPTION, " +
                           " IsNULL(SECM_MAXLOANAMOUNT,0) AS SECM_MAXLOANAMOUNT, " +
                           " IsNULL(SECM_MINLOANAMOUNT,0) AS SECM_MINLOANAMOUNT, " +
                           " IsNULL(SECM_MINAGE,0) AS SECM_MINAGE, " +
                           " IsNULL(SECM_MAXAGE,0) AS SECM_MAXAGE, " +
                           " IsNULL(SECM_MAXAGEREMARKS,'N/A') AS SECM_MAXAGEREMARKS, " +
                           " IsNULL(SECM_PHYSICALVERIFICATION, 'N/A') AS SECM_PHYSICALVERIFICATION, " +
                           " IsNULL(SECM_TELEPHONE, 'N/A') AS SECM_TELEPHONE, " +
                           " IsNULL(SECM_EXPERIENCE, '') AS SECM_EXPERIENCE, " +
                           " IsNULL(SECM_ACREL, '') AS SECM_ACREL, " +
                           " IsNULL(SECM_MININCOME,0) AS SECM_MININCOME, " +
                           " IsNULL(PL_DBR,0) AS PL_DBR, " +
                           " IsNULL(SECM_DBR29,0) AS SECM_DBR29, " +
                           " IsNULL(SECM_DBR50,0) AS SECM_DBR50, " +
                           " IsNULL(SECM_DBR99,0) AS SECM_DBR99, " +
                           " IsNULL(SECM_DBR1K,0) AS SECM_DBR1K, " +
                           " IsNULL(SECM_MULTIPLIER,0) AS SECM_MULTIPLIER, " +
                           " IsNULL(PL_TENURE,0) AS PL_TENURE, " +
                           " IsNULL(PL_PROPOSEDLOANAMOUNT,0) AS PL_PROPOSEDLOANAMOUNT, " +
                           " IsNULL(SECM_TENOR,0) AS SECM_TENOR, " +
                           " IsNULL(PL_MUE,0) AS PL_MUE, " +
                           " IsNULL(SECM_MUELOC,0) AS SECM_MUELOC, " +
                           " IsNULL(SECM_GURANTEEREFERENCE,'N/A') AS SECM_GURANTEEREFERENCE " +
                           " FROM t_pluss_segment, t_pluss_pl " +
                           " WHERE PL_LLID = " + llID + " " +
                           " AND PL_SEGMENT_ID=SEGM_ID ";

            SegmentInfo segmentInfo = new SegmentInfo();
            segmentInfo.SegmentCode = "No Data";

            DbDataReader segmentInfoReader = DbQueryManager.ExecuteReader(query);
            if(segmentInfoReader != null)
            {
                if(segmentInfoReader.HasRows)
                {
                    segmentInfoReader.Read();
                    segmentInfo.SegmentCode = segmentInfoReader[0].ToString();
                    segmentInfo.SegmentDescription = segmentInfoReader[1].ToString();
                    segmentInfo.MaxLoan = Convert.ToDouble(segmentInfoReader[2].ToString());
                    segmentInfo.MinLoan = Convert.ToDouble(segmentInfoReader[3].ToString());
                    segmentInfo.MinAge = Convert.ToInt32(segmentInfoReader[4].ToString());
                    segmentInfo.MaxAge = Convert.ToInt32(segmentInfoReader[5].ToString());
                    segmentInfo.MaxAgeRemarks = segmentInfoReader[6].ToString();
                    segmentInfo.PhysicalVerification = segmentInfoReader[7].ToString();
                    segmentInfo.Telephone = segmentInfoReader[8].ToString();
                    segmentInfo.Experience = segmentInfoReader[9].ToString();
                    segmentInfo.ACRelationship = segmentInfoReader[10].ToString();
                    segmentInfo.MinIncome = Convert.ToDouble(segmentInfoReader[11].ToString());
                    segmentInfo.DBRatio = Convert.ToDouble(segmentInfoReader[12].ToString());
                    segmentInfo.DBR29 = Convert.ToDouble(segmentInfoReader[13].ToString());
                    segmentInfo.DBR50 = Convert.ToDouble(segmentInfoReader[14].ToString());
                    segmentInfo.DBR99 = Convert.ToDouble(segmentInfoReader[15].ToString());
                    segmentInfo.DBR1K = Convert.ToDouble(segmentInfoReader[16].ToString());
                    segmentInfo.DBR = Convert.ToDouble(segmentInfoReader[17].ToString());
                    segmentInfo.Tenur = Convert.ToDouble(segmentInfoReader[18].ToString());
                    segmentInfo.ProposedLoanAmount = Convert.ToDouble(segmentInfoReader[19].ToString());
                    segmentInfo.TNR = Convert.ToDouble(segmentInfoReader[20].ToString());
                    segmentInfo.MUE = Convert.ToDouble(segmentInfoReader[21].ToString());
                    segmentInfo.MUELOC = Convert.ToDouble(segmentInfoReader[22].ToString());
                    segmentInfo.GuaranteeRef = segmentInfoReader[23].ToString();
                }
            }
            segmentInfoReader.Close();

            return segmentInfo;
        }

        /// <summary>
        /// This method provide Exposure information.
        /// </summary>
        /// <param name="llID">Gets llid.</param>
        /// <returns>Returns exposure info object.</returns>
        public ExposureInfo GetExposureInfo(int llID)
        {
            ExposureInfo exposureInfo = new ExposureInfo();

            string query = " SELECT " +
                           " (isnull(CUST_TOPUPAMOUNT1,0)) AS OutStanding, " +
                           " isnull(CUST_OUTSTANDING,0) AS OriginalLimit, " +
                           " ((case when PL_SAFETYPLUS_ID=1 then isnull(PL_BANCALOANAMOUNT,0) else isnull(PL_PROPOSEDLOANAMOUNT,0)end) - isnull(CUST_TOPUPAMOUNT1,0)) AS TopUpOutStanding, " +
                           " isnull(PL_EMI,0) AS TopUpRepayment, " +
                           " isnull(PL_EMIFACTOR,0) AS TopUpEMIFactor, " +
                           " (case when PL_SAFETYPLUS_ID=1 then isnull(PL_BANCALOANAMOUNT,0) else isnull(PL_PROPOSEDLOANAMOUNT,0)end) AS ThisPLOutStanding, " +
                           " isnull(PL_EMI,0) AS ThisPLRepayment, " +
                           " isnull(PL_SCBCREDITCARDLIMIT, 0) AS CreditCardOutStanding, " +
                           " isnull(PL_INSTABUYBALANCE,0) AS PL_INSTABUYBALANCE, " +
                           " isnull(PL_INSTABUYINSTALLMENT,0) AS PL_INSTABUYINSTALLMENT " +
                           " FROM t_pluss_pl, t_pluss_customer " +
                           " WHERE PL_LLID = CUST_LLID " +
                           " AND PL_LLID = '" + llID + "'";
            DbDataReader queryReader = DbQueryManager.ExecuteReader(query);
            if(queryReader != null)
            {
                if(queryReader.HasRows)
                {
                    queryReader.Read();

                    exposureInfo.ExistingPLOutstanding = Convert.ToDouble(queryReader["OutStanding"].ToString());
                    exposureInfo.ExistingPLOriginalLimit = Convert.ToDouble(queryReader["OriginalLimit"].ToString());
                    exposureInfo.TopUpAmountOutstanding = Convert.ToDouble(queryReader["TopUpOutStanding"].ToString());
                    exposureInfo.TopUpAmountRepayment = Convert.ToDouble(queryReader["TopUpRepayment"].ToString());
                    exposureInfo.TopUpAmountEMIFactor = Convert.ToDouble(queryReader["TopUpEMIFactor"].ToString());
                    exposureInfo.ThisPLOutstanding = Convert.ToDouble(queryReader["ThisPLOutStanding"].ToString());
                    exposureInfo.ThisPLRepayment = Convert.ToDouble(queryReader["ThisPLRepayment"].ToString());
                    exposureInfo.CreditCardLimitOutstanding = Convert.ToDouble(queryReader["CreditCardOutStanding"].ToString());
                    double temp = Convert.ToDouble(queryReader["PL_INSTABUYBALANCE"].ToString());
                    double temp2 = Convert.ToDouble(queryReader["PL_INSTABUYINSTALLMENT"].ToString());
                    double tempCal = 0;
                    if (exposureInfo.CreditCardLimitOutstanding > 0)
                    {
                        tempCal = ((exposureInfo.CreditCardLimitOutstanding - temp) * 0.03);
                    }
                    if (exposureInfo.CreditCardLimitOutstanding > 0 && tempCal < 499)
                    {
                        exposureInfo.CreditCardLimitRepayment = temp2 + 500;
                    }
                    else
                    {
                        exposureInfo.CreditCardLimitRepayment = tempCal + temp2;
                    }
                }
                queryReader.Close();
            }

            string facalityQuery = " SELECT " +
                                   " IsNULL(CUDE_FACILITY,'N/A') AS CUDE_FACILITY, " +
                                   " IsNULL(CUDE_OUTSTANDING,0) AS CUDE_OUTSTANDING, " +
                                   " IsNULL(CUDE_ORGINALLIMIT,0) AS CUDE_ORGINALLIMIT, " +
                                   " IsNULL(CUDE_REPAYMENT,0) AS CUDE_REPAYMENT, " +
                                   " DATE(IsNULL(CUDE_DISBURSEDATE,'1900-01-01')) AS CUDE_DISBURSEDATE, " +
                                   " DATE(CUDE_EXPOSUREDATE) AS CUDE_EXPOSUREDATE " +
                                   " FROM t_pluss_customerdetails " +
                                   " WHERE CUDE_CUSTOMER_LLID='" + llID + "' LIMIT 0,5";

            DbDataReader facalityReader = DbQueryManager.ExecuteReader(facalityQuery);
            int count = 0;

            if(facalityReader != null)
            {
                while(facalityReader.Read())
                {
                    if(count == 0)
                    {
                        exposureInfo.FacalityName1 = facalityReader[0].ToString();
                        exposureInfo.FacalityOutstanding1 = Convert.ToDouble(facalityReader[1].ToString());
                        exposureInfo.FacalityOriginalLimit1 = Convert.ToDouble(facalityReader[2].ToString());
                        exposureInfo.FacalityRepayment1 = Convert.ToDouble(facalityReader[3].ToString());
                        exposureInfo.FacalityDisburseDate1 = Convert.ToDateTime(facalityReader["CUDE_DISBURSEDATE"].ToString());
                        exposureInfo.FacalityExpiryDate1 = Convert.ToDateTime(facalityReader["CUDE_EXPOSUREDATE"].ToString());
                        count = count + 1;
                    }
                    else if(count == 1)
                    {
                        exposureInfo.FacalityName2 = facalityReader[0].ToString();
                        exposureInfo.FacalityOutstanding2 = Convert.ToDouble(facalityReader[1].ToString());
                        exposureInfo.FacalityOriginalLimit2 = Convert.ToDouble(facalityReader[2].ToString());
                        exposureInfo.FacalityRepayment2 = Convert.ToDouble(facalityReader[3].ToString());
                        exposureInfo.FacalityDisburseDate2 = Convert.ToDateTime(facalityReader[4].ToString());
                        exposureInfo.FacalityExpiryDate2 = Convert.ToDateTime(facalityReader[5].ToString());
                        count = count + 1;
                    }
                    else if(count == 2)
                    {
                        exposureInfo.FacalityName3 = facalityReader[0].ToString();
                        exposureInfo.FacalityOutstanding3 = Convert.ToDouble(facalityReader[1].ToString());
                        exposureInfo.FacalityOriginalLimit3 = Convert.ToDouble(facalityReader[2].ToString());
                        exposureInfo.FacalityRepayment3 = Convert.ToDouble(facalityReader[3].ToString());
                        exposureInfo.FacalityDisburseDate3 = Convert.ToDateTime(facalityReader[4].ToString());
                        exposureInfo.FacalityExpiryDate3 = Convert.ToDateTime(facalityReader[5].ToString());
                        count = count + 1;
                    }
                    else if(count == 3)
                    {
                        exposureInfo.FacalityName4 = facalityReader[0].ToString();
                        exposureInfo.FacalityOutstanding4 = Convert.ToDouble(facalityReader[1].ToString());
                        exposureInfo.FacalityOriginalLimit4 = Convert.ToDouble(facalityReader[2].ToString());
                        exposureInfo.FacalityRepayment4 = Convert.ToDouble(facalityReader[3].ToString());
                        exposureInfo.FacalityDisburseDate4 = Convert.ToDateTime(facalityReader[4].ToString());
                        exposureInfo.FacalityExpiryDate4 = Convert.ToDateTime(facalityReader[5].ToString());
                        count = count + 1;
                    }
                    else if(count == 4)
                    {
                        exposureInfo.FacalityName5 = facalityReader[0].ToString();
                        exposureInfo.FacalityOutstanding5 = Convert.ToDouble(facalityReader[1].ToString());
                        exposureInfo.FacalityOriginalLimit5 = Convert.ToDouble(facalityReader[2].ToString());
                        exposureInfo.FacalityRepayment5 = Convert.ToDouble(facalityReader[3].ToString());
                        exposureInfo.FacalityDisburseDate5 = Convert.ToDateTime(facalityReader[4].ToString());
                        exposureInfo.FacalityExpiryDate5 = Convert.ToDateTime(facalityReader[5].ToString());
                        count = count + 1;
                    }
                }
                facalityReader.Close();
            }

            return exposureInfo;
        }

        /// <summary>
        /// This method provide deviation info.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns deviation info object.</returns>
        public DeviationInfo GetDeviationInfo(int llID)
        {
            DeviationInfo deviationInfo = null;// new DeviationInfo();

            string query = " SELECT " +
                           " isnull(DEVI_LEVEL1,0) AS DEVI_LEVEL1, isnull(DEVI_CODE1,'N/A') AS DEVI_CODE1, isnull(PDEV_DESCRIPTION1,'N/A') AS PDEV_DESCRIPTION1, " +
                           " isnull(DEVI_LEVEL2,0) AS DEVI_LEVEL2, isnull(DEVI_CODE2,'N/A') AS DEVI_CODE2, isnull(PDEV_DESCRIPTION2,'N/A') AS PDEV_DESCRIPTION2, " +
                           " isnull(DEVI_LEVEL3,0) AS DEVI_LEVEL3, isnull(DEVI_CODE3,'N/A') AS DEVI_CODE3, isnull(PDEV_DESCRIPTION3,'N/A') AS PDEV_DESCRIPTION3, " +
                           " isnull(DEVI_LEVEL4,0) AS DEVI_LEVEL4, isnull(DEVI_CODE4,'N/A') AS DEVI_CODE4, isnull(PDEV_DESCRIPTION4,'N/A') AS PDEV_DESCRIPTION4  " +
                           " FROM " +
                           " ( " +
                           " SELECT DEVI_LEVEL1, DEVI_CODE1, PDEV_DESCRIPTION AS PDEV_DESCRIPTION1,DEVI_LLID " +
                           " FROM t_pluss_pdddeviation, t_pluss_deviation " +
                           " WHERE DEVI_LLID = " + llID + " " +
                           " AND DEVI_DESCRIPTION1_ID = PDEV_ID " +
                           " ) AS a1 left join  " +
                           " ( " +
                           " SELECT DEVI_LEVEL2, DEVI_CODE2, PDEV_DESCRIPTION AS PDEV_DESCRIPTION2,DEVI_LLID " +
                           " FROM t_pluss_pdddeviation, t_pluss_deviation " +
                           " WHERE DEVI_LLID = " + llID + " " +
                           " AND DEVI_DESCRIPTION2_ID = PDEV_ID " +
                           " ) AS a2 on  a1.DEVI_LLID=a2.DEVI_LLID left join " +
                           " ( " +
                           " SELECT DEVI_LEVEL3, DEVI_CODE3, PDEV_DESCRIPTION AS PDEV_DESCRIPTION3,DEVI_LLID " +
                           " FROM t_pluss_pdddeviation, t_pluss_deviation " +
                           " WHERE DEVI_LLID = " + llID + " " +
                           " AND DEVI_DESCRIPTION3_ID = PDEV_ID " +
                           " ) AS a3 on  a2.DEVI_LLID=a3.DEVI_LLID left join " +
                           " ( " +
                           " SELECT DEVI_LEVEL4, DEVI_CODE4, PDEV_DESCRIPTION AS PDEV_DESCRIPTION4,DEVI_LLID " +
                           " FROM t_pluss_pdddeviation, t_pluss_deviation " +
                           " WHERE DEVI_LLID = " + llID + " " +
                           " AND DEVI_DESCRIPTION4_ID = PDEV_ID " +
                           " ) AS a4 on  a3.DEVI_LLID=a4.DEVI_LLID";

            DbDataReader deviationReader = DbQueryManager.ExecuteReader(query);
            if(deviationReader != null)
            {
                if(deviationReader.HasRows)
                {
                    deviationReader.Read();
                    deviationInfo = new DeviationInfo();
                    deviationInfo.Level1 = Convert.ToString(deviationReader["DEVI_LEVEL1"]);
                    deviationInfo.Level2 = Convert.ToString(deviationReader["DEVI_LEVEL2"]);
                    deviationInfo.Level3 = Convert.ToString(deviationReader["DEVI_LEVEL3"]);
                    deviationInfo.Level4 = Convert.ToString(deviationReader["DEVI_LEVEL4"]);

                    deviationInfo.Code1 = Convert.ToString(deviationReader["DEVI_CODE1"]);
                    deviationInfo.Code2 = Convert.ToString(deviationReader["DEVI_CODE2"]);
                    deviationInfo.Code3 = Convert.ToString(deviationReader["DEVI_CODE3"]);
                    deviationInfo.Code4 = Convert.ToString(deviationReader["DEVI_CODE4"]);

                    deviationInfo.Description1 = Convert.ToString(deviationReader["PDEV_DESCRIPTION1"]);
                    deviationInfo.Description2 = Convert.ToString(deviationReader["PDEV_DESCRIPTION2"]);
                    deviationInfo.Description3 = Convert.ToString(deviationReader["PDEV_DESCRIPTION3"]);
                    deviationInfo.Description4 = Convert.ToString(deviationReader["PDEV_DESCRIPTION4"]);
                    deviationReader.Close();
                }
            }

            return deviationInfo;
        }

        /// <summary>
        /// This method provide net income information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns net income info object.</returns>
        public NetIncomeInfo GetNetIncomeInfo(int llID)
        {
            NetIncomeInfo netIncomeInfo = new NetIncomeInfo();

            string query = " SELECT IIF(PL_NETINCOME<PL_DECLAREDINCOME,PL_NETINCOME,PL_DECLAREDINCOME) AS INCOME, PL_DBR, PL_MUE, PL_PROPOSEDLOANAMOUNT, PL_EMI, PL_TENURE, PL_INTERESTRAT " +
                           " FROM t_pluss_pl " +
                           " WHERE PL_LLID = "+llID+" ";

            DbDataReader netIncomeReader = DbQueryManager.ExecuteReader(query);

            if(netIncomeReader != null)
            {
                if(netIncomeReader.HasRows)
                {
                    netIncomeReader.Read();
                    netIncomeInfo.NetIncome = Convert.ToDouble(netIncomeReader["INCOME"]);
                    netIncomeInfo.DBR = Convert.ToDouble(netIncomeReader["PL_DBR"]);
                    netIncomeInfo.MUE = Convert.ToDouble(netIncomeReader["PL_MUE"]);
                    netIncomeInfo.LoanAmount = Convert.ToDouble(netIncomeReader["PL_PROPOSEDLOANAMOUNT"]);
                    netIncomeInfo.EMI = Convert.ToDouble(netIncomeReader["PL_EMI"]);
                    netIncomeInfo.Tenor = Convert.ToDouble(netIncomeReader["PL_TENURE"]);
                    netIncomeInfo.IR = Convert.ToDouble(netIncomeReader["PL_INTERESTRAT"]);
                    netIncomeReader.Close();
                }
            }
            return netIncomeInfo;
        }

        /// <summary>
        /// This method provide standing order information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns standing order info object.</returns>
        public StandingOrderInfo GetStandingOrderInfo(int llID)
        {
            StandingOrderInfo standingOrderInfo = new StandingOrderInfo();
            standingOrderInfo.StandingOrder = "No Data";

            string query = " SELECT CUST_PDCSLFORM, CUST_FIRSTPAYMENT1, CUST_PDCACNO " +
                           " FROM t_pluss_customer " +
                           " WHERE CUST_LLID = " + llID + " ";

            DbDataReader standingIncomeReader = DbQueryManager.ExecuteReader(query);

            if(standingIncomeReader != null)
            {
                if(standingIncomeReader.HasRows)
                {
                    standingIncomeReader.Read();
                    standingOrderInfo.StandingOrder = Convert.ToString(standingIncomeReader["CUST_PDCSLFORM"]);
                    standingOrderInfo.RpmnthDate = Convert.ToString(standingIncomeReader["CUST_FIRSTPAYMENT1"]);
                    standingOrderInfo.ACNo = Convert.ToString(standingIncomeReader["CUST_PDCACNO"]);
                    standingIncomeReader.Close();
                }
            }
            return standingOrderInfo;
        }

        /// <summary>
        /// This method provide user information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns user info object.</returns>
        public UserInfo GetUserInfo(int llID)
        {
            UserInfo userinfo = new UserInfo();

            string query = " SELECT USER_NAME, USER_DESIGNATION " +
                           " FROM t_pluss_user, t_pluss_deviation " +
                           " WHERE DEVI_LLID = '" + llID + "' " +
                           " AND DEVI_USER_ID=USER_ID ";

            DbDataReader userReader = DbQueryManager.ExecuteReader(query);

            if(userReader != null)
            {
                if(userReader.HasRows)
                {
                    userReader.Read();
                    userinfo.UserName = Convert.ToString(userReader["USER_NAME"]);
                    userinfo.Designation = Convert.ToString(userReader["USER_DESIGNATION"]);
                    userReader.Close();
                }
            }
            return userinfo;
        }

        /// <summary>
        /// This method provide strength weakness information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns strength weakness info object.</returns>
        public StrengthWeaknessInfo GetStrengthWeaknessInfo(int llID)
        {
            StrengthWeaknessInfo strengthWeaknessInfo = new StrengthWeaknessInfo();

            string query = " SELECT " +
                           " isnull(DEVI_RELATIONSHIPWITH1,0) AS DEVI_RELATIONSHIPWITH1, " +
                           " isnull(DEVI_RESIDENTIALSTATUS,0) AS DEVI_RESIDENTIALSTATUS, " +
                           " isnull(DEVI_CREDITCARDREPAYMENT,0) AS DEVI_CREDITCARDREPAYMENT, " +
                           " isnull(DEVI_LIMIT,0) AS DEVI_LIMIT, " +
                           " isnull(DEVI_AVAILEDSINCE,0) AS DEVI_AVAILEDSINCE, " +
                           " isnull(DEVI_PREVIOUSREPAYMENT_ID1,0) AS DEVI_PREVIOUSREPAYMENT_ID1, " +
                           " isnull(DEVI_LOANTYPE_ID1,0) AS DEVI_LOANTYPE_ID1, " +
                           " isnull(DEVI_AVAILEDSINCE1,0) AS DEVI_AVAILEDSINCE1, " +
                           " isnull(DEVI_PREVIOUSREPAYMENT_ID2,0) AS DEVI_PREVIOUSREPAYMENT_ID2, " +
                           " isnull(DEVI_LOANTYPE_ID2,0) AS DEVI_LOANTYPE_ID2, " +
                           " isnull(DEVI_AVAILEDSINCE2,0) AS DEVI_AVAILEDSINCE2, " +
                           " isnull(DEVI_OTHERS_ID, '0') AS DEVI_OTHERS_ID, " +
                           " isnull(DEVI_REPAYMENTNOTSATISFACTORY,0) AS DEVI_REPAYMENTNOTSATISFACTORY, " +
                           " isnull(DEVI_LOANTYPE_ID3,0) AS DEVI_LOANTYPE_ID3, " +
                           " isnull(DEVI_DPD_ID,0) AS DEVI_DPD_ID, " +
                           " isnull(DEVI_CASHSALARIED,0) AS DEVI_CASHSALARIED, " +
                           " isnull(DEVI_FREQUENTCHANGERETURN,0) AS DEVI_FREQUENTCHANGERETURN, " +
                           " isnull(DEVI_OTHERS, '0') AS DEVI_OTHERS " +
                           " FROM t_pluss_deviation " +
                           " WHERE DEVI_LLID = " + llID + "";

            DbDataReader strengthWeaknessReader = DbQueryManager.ExecuteReader(query);
            if(strengthWeaknessReader != null)
            {
                if(strengthWeaknessReader.HasRows)
                {
                    strengthWeaknessReader.Read();
                    strengthWeaknessInfo.StrengthAcId = Convert.ToInt32(strengthWeaknessReader["DEVI_RELATIONSHIPWITH1"]);
                    strengthWeaknessInfo.StrengthResidentialStatusId = Convert.ToInt32(strengthWeaknessReader["DEVI_RESIDENTIALSTATUS"]);
                    strengthWeaknessInfo.StrengthCreditCardRepaymentId = Convert.ToInt32(strengthWeaknessReader["DEVI_CREDITCARDREPAYMENT"]);
                    strengthWeaknessInfo.StrengthPreviousRepaymentId1 = Convert.ToInt32(strengthWeaknessReader["DEVI_PREVIOUSREPAYMENT_ID1"]);
                    strengthWeaknessInfo.StrengthPreviousRepaymentId2 = Convert.ToInt32(strengthWeaknessReader["DEVI_PREVIOUSREPAYMENT_ID2"]);
                    strengthWeaknessInfo.StrengthOthers = strengthWeaknessReader["DEVI_OTHERS_ID"].ToString();
                    strengthWeaknessInfo.WeaknessRepaymentNotSatisfactoryId = Convert.ToInt32(strengthWeaknessReader["DEVI_REPAYMENTNOTSATISFACTORY"]);
                    strengthWeaknessInfo.WeaknessCashSalariedId = Convert.ToInt32(strengthWeaknessReader["DEVI_CASHSALARIED"]);
                    strengthWeaknessInfo.WeaknessFrequentChequeReturnId = Convert.ToInt32(strengthWeaknessReader["DEVI_FREQUENTCHANGERETURN"]);
                    strengthWeaknessInfo.WeaknessOthers = strengthWeaknessReader["DEVI_OTHERS"].ToString();
                    strengthWeaknessInfo.WeaknessLoanType = Convert.ToInt32(strengthWeaknessReader["DEVI_LOANTYPE_ID3"]);
                    strengthWeaknessInfo.WeaknessDPD = Convert.ToInt32(strengthWeaknessReader["DEVI_DPD_ID"]);

                    strengthWeaknessInfo.CreditCardLimit = Convert.ToString(strengthWeaknessReader["DEVI_LIMIT"]);
                    strengthWeaknessInfo.StrengthCreditCardRepaymentDateTemp = strengthWeaknessReader["DEVI_AVAILEDSINCE"].ToString();
                    strengthWeaknessInfo.StrengthPreviousRepaymentLoneType1Id = Convert.ToInt32(strengthWeaknessReader["DEVI_LOANTYPE_ID1"]);
                    strengthWeaknessInfo.StrengthPreviousRepaymentDate1Temp = strengthWeaknessReader["DEVI_AVAILEDSINCE1"].ToString();
                    strengthWeaknessInfo.StrengthPreviousRepaymentLoneType2Id = Convert.ToInt32(strengthWeaknessReader["DEVI_LOANTYPE_ID2"]);
                    strengthWeaknessInfo.StrengthPreviousRepaymentDate2Temp = strengthWeaknessReader["DEVI_AVAILEDSINCE2"].ToString();
                    strengthWeaknessReader.Close();

                    if(strengthWeaknessInfo.StrengthAcId > 0)
                    {
                        string bankNameQuery = " SELECT BANK_NM FROM t_pluss_bank WHERE BANK_ID = " + strengthWeaknessInfo.StrengthAcId + "";
                        object bankNameObj = DbQueryManager.ExecuteScalar(bankNameQuery);
                        if(bankNameObj != null)
                        {
                            strengthWeaknessInfo.StrengthAcRelationshipWith = "Salary a/c relationship with " + bankNameObj.ToString();
                        }
                    }
                    else
                    {
                        strengthWeaknessInfo.StrengthAcRelationshipWith = "";
                    }
                    if (strengthWeaknessInfo.StrengthCreditCardRepaymentId > 0)
                    {
                        string repaymentType = "";
                        string limit = "";
                        string availedScience = "";
                        if (strengthWeaknessInfo.CreditCardLimit != "Closed" || strengthWeaknessInfo.CreditCardLimit != "")
                        {
                            limit = "(Limit - " + strengthWeaknessInfo.CreditCardLimit.ToString() +" ) ";
                        }
                        //if (strengthWeaknessInfo.StrengthCreditCardRepaymentDateTemp!=null)
                        //{
                        //    availedScience = "( since " + strengthWeaknessInfo.StrengthCreditCardRepaymentDateTemp.ToString() + " ) ";
                        //}

                        if(strengthWeaknessInfo.StrengthCreditCardRepaymentId == 1)
                        {
                            repaymentType = "Excellent";
                        }
                        else if(strengthWeaknessInfo.StrengthCreditCardRepaymentId == 2)
                        {
                            repaymentType = "Satisfactory";
                        }
                        else if(strengthWeaknessInfo.StrengthCreditCardRepaymentId == 3)
                        {
                            repaymentType = "Good";
                        }
                        else
                        {
                            repaymentType = "Moderate";
                        }
                        strengthWeaknessInfo.StrengthCreditCardRepayment = repaymentType + " Repayment in Credit Card   " + limit + " " + availedScience;
                    }
                    else
                    {
                        strengthWeaknessInfo.StrengthCreditCardRepayment = "";
                    }
                    if(strengthWeaknessInfo.StrengthPreviousRepaymentId1 > 0)
                    {
                        string repaymentType = "";
                        string loanType = "";
                        string availedSince = "";
                        if(strengthWeaknessInfo.StrengthPreviousRepaymentLoneType1Id > 0)
                        {
                            string loanTypeQuery = " SELECT PROD_NAME FROM t_pluss_product WHERE PROD_ID = " + strengthWeaknessInfo.StrengthPreviousRepaymentLoneType1Id + "";
                            object loanTypeTemp = DbQueryManager.ExecuteScalar(loanTypeQuery);
                            if(loanTypeQuery != null)
                            {
                                loanType = "in " + loanTypeTemp.ToString();
                            }
                        }
                        //if(strengthWeaknessInfo.StrengthPreviousRepaymentDate1Temp != null)
                        //{
                        //    availedSince = "( since " + strengthWeaknessInfo.StrengthPreviousRepaymentDate1Temp.ToString()+" ) ";
                        //}

                        if(strengthWeaknessInfo.StrengthPreviousRepaymentId1 == 1)
                        {
                            repaymentType = "Excellent";
                        }
                        else if(strengthWeaknessInfo.StrengthPreviousRepaymentId1 == 2)
                        {
                            repaymentType = "Satisfactory";
                        }
                        else if(strengthWeaknessInfo.StrengthPreviousRepaymentId1 == 3)
                        {
                            repaymentType = "Good";
                        }
                        else
                        {
                            repaymentType = "Moderate";
                        }
                        strengthWeaknessInfo.StrengthPreviousRepayment1 = "Previous Repayment " + repaymentType + " " + loanType + " " + availedSince;
                    }
                    else
                    {
                        strengthWeaknessInfo.StrengthPreviousRepayment1 = "";
                    }
                    if(strengthWeaknessInfo.StrengthPreviousRepaymentId2 > 0)
                    {
                        string repaymentType = "";
                        string loanType = "";
                        string availedSince = "";
                        if(strengthWeaknessInfo.StrengthPreviousRepaymentLoneType1Id > 0)
                        {
                            string loanTypeQuery = " SELECT PROD_NAME FROM t_pluss_product WHERE PROD_ID = " + strengthWeaknessInfo.StrengthPreviousRepaymentLoneType1Id + "";
                            object loanTypeTemp = DbQueryManager.ExecuteScalar(loanTypeQuery);
                            if(loanTypeQuery != null)
                            {
                                loanType = "in " + loanTypeTemp.ToString();
                            }
                        }
                        //if(strengthWeaknessInfo.StrengthPreviousRepaymentDate2Temp != null)
                        //{
                        //    availedSince = " ( since " + strengthWeaknessInfo.StrengthPreviousRepaymentDate2Temp.ToString()+" ) ";
                        //}

                        if(strengthWeaknessInfo.StrengthPreviousRepaymentId2 == 1)
                        {
                            repaymentType = "Excellent";
                        }
                        else if(strengthWeaknessInfo.StrengthPreviousRepaymentId2 == 2)
                        {
                            repaymentType = "Satisfactory";
                        }
                        else if(strengthWeaknessInfo.StrengthPreviousRepaymentId2 == 3)
                        {
                            repaymentType = "Good";
                        }
                        else
                        {
                            repaymentType = "Moderate";
                        }
                        strengthWeaknessInfo.StrengthPreviousRepayment2 = "Previous Repayment " + repaymentType + " " + loanType + " " + availedSince;
                    }
                    else
                    {
                        strengthWeaknessInfo.StrengthPreviousRepayment2 = "";
                    }

                    if(strengthWeaknessInfo.StrengthResidentialStatus != "0")
                    {
                        string repaymentType = "";
                        if(strengthWeaknessInfo.StrengthResidentialStatusId == 1)
                        {
                            repaymentType = "Own";
                        }
                        else if(strengthWeaknessInfo.StrengthResidentialStatusId == 2)
                        {
                            repaymentType = "Family Owned";
                        }
                        else
                        {
                            repaymentType = "Others";
                        }
                        strengthWeaknessInfo.StrengthResidentialStatus = "Resides in " + repaymentType + " House";
                    }
                    else
                    {
                        strengthWeaknessInfo.StrengthResidentialStatus = "";
                    }

                    if(strengthWeaknessInfo.StrengthOthers == "0")
                    {
                        strengthWeaknessInfo.StrengthOthers = "";
                    }
                    else
                    {
                    }

                    if(strengthWeaknessInfo.WeaknessCashSalariedId > 0)
                    {
                        if(strengthWeaknessInfo.WeaknessCashSalariedId == 1)
                        {
                            strengthWeaknessInfo.WeaknessCashSalaried = "Applicant is cash salaried";
                        }
                        else
                        {
                            strengthWeaknessInfo.WeaknessCashSalaried = "";
                        }
                    }
                    else
                    {
                        strengthWeaknessInfo.WeaknessCashSalaried = "";
                    }

                    if(strengthWeaknessInfo.WeaknessFrequentChequeReturnId > 0)
                    {
                        if(strengthWeaknessInfo.WeaknessFrequentChequeReturnId == 1)
                        {
                            strengthWeaknessInfo.WeaknessFrequentChequeReturn = "Frequent cheque return";
                        }
                        else
                        {
                            strengthWeaknessInfo.WeaknessFrequentChequeReturn = "";
                        }
                    }
                    else
                    {
                        strengthWeaknessInfo.WeaknessFrequentChequeReturn = "";
                    }

                    if(strengthWeaknessInfo.WeaknessRepaymentNotSatisfactoryId > 0)
                    {
                        if(strengthWeaknessInfo.WeaknessRepaymentNotSatisfactoryId == 1)
                        {
                            string loneType = "";
                            string dpd = "";
                            if(strengthWeaknessInfo.WeaknessLoanType > 0)
                            {
                                string loanTypeNameQuery = "SELECT PROD_NAME FROM t_pluss_product WHERE PROD_ID=" + strengthWeaknessInfo.WeaknessLoanType + "";
                                object loanTypeName = DbQueryManager.ExecuteScalar(loanTypeNameQuery);
                                if(loanTypeName != null)
                                {
                                    loneType = " in " + loanTypeName.ToString();
                                }
                            }
                            if(strengthWeaknessInfo.WeaknessDPD == 1)
                            {
                                dpd = "[30+ DPD]";
                            }
                            else if(strengthWeaknessInfo.WeaknessDPD == 2)
                            {
                                dpd = "[60+ DPD]";
                            }
                            else if(strengthWeaknessInfo.WeaknessDPD == 3)
                            {
                                dpd = "[90+ DPD]";
                            }
                            else if(strengthWeaknessInfo.WeaknessDPD == 4)
                            {
                                dpd = "[120+ DPD]";
                            }
                            else if(strengthWeaknessInfo.WeaknessDPD == 5)
                            {
                                dpd = "[150+ DPD]";
                            }
                            else
                            {
                            }

                            strengthWeaknessInfo.WeaknessRepaymentNotSatisfactory = "Previous Repayment " + loneType + " not satisfactory    " + dpd;
                        }
                        else
                        {
                            strengthWeaknessInfo.WeaknessRepaymentNotSatisfactory = "";
                        }
                    }
                    else
                    {
                        strengthWeaknessInfo.WeaknessRepaymentNotSatisfactory = "";
                    }
                    if(strengthWeaknessInfo.WeaknessOthers == "0")
                    {
                        strengthWeaknessInfo.WeaknessOthers = "";
                    }
                }
            }

            return strengthWeaknessInfo;
        }
    }
}
