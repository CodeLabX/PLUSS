﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using ADODB;
using BusinessEntities;
using Bat.Common;
using System.Data.Common;
using AzUtilities;

namespace DAL
{
    public class CustomException
    {
        public static bool Save(Exception exception, string operation)
        {
            Exception ex = exception;

            ex.Source = "Operation :: " + operation + " from Source :: " + exception.Source;

            LogFile.WriteLog(ex);

            var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
            var log = new ExceptionLog
            {
                ExceptionDescription = exception.Message.Replace("'", ""),
                UserId = userId,
                ExceptionDate = DateTime.Now,
                OperationType = operation
            };
            return SaveException(log);

        }
        private static bool SaveException(ExceptionLog exceptionLog)
        {
            var dbMySql = new DatabaseConnection();
            try
            {
                string sql = string.Format(@"Insert Into t_pluss_exceptionlog(OperationType,ExceptionDescription,ExceptionDate,UserId)
                        Values('{0}','{1}','{2}','{3}')", exceptionLog.OperationType, exceptionLog.ExceptionDescription, exceptionLog.ExceptionDate, exceptionLog.UserId);

                 DbQueryManager.ExecuteNonQuery(sql);
                 return true;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }

        public static List<ExceptionLog> GetAllExceptions(string startDate, string endDate)
        {
            var dbMySql = new DatabaseConnection();
            var exceptionList = new List<ExceptionLog>();
            var sSQL = @"Select * from t_pluss_exceptionlog 
                Where ExceptionDate Between '" + startDate + "' And '" +
                       endDate + "'";
            DbDataReader dr = DbQueryManager.ExecuteReader(sSQL);
            //DataTable dr = new DataTable();
            //dr = DbQueryManager.GetTable(sSQL);

            while (dr.Read())
            {
                ExceptionLog exceptionLog = new ExceptionLog();
                exceptionLog.OperationType = dr["OperationType"].ToString();
                exceptionLog.ExceptionDescription = dr["ExceptionDescription"].ToString();
                exceptionLog.ExceptionDate = Convert.ToDateTime(dr["ExceptionDate"]);
                exceptionLog.UserId = dr["UserId"].ToString();
                exceptionList.Add(exceptionLog);
                
            }
            dr.Close();
          
            return exceptionList;
        }
    }
}