﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Bat.Common;
using UpdateStatus = BusinessEntities.UpdateStatus;

namespace DAL
{

    public class UpdateLoanStatusGateway
    {
        public List<UpdateStatus> GetAllStatus()
        {
            List<UpdateStatus> updateStatusesList = new List<UpdateStatus>();
            DataTable dataTable= new DataTable();
            string query = "select * from loan_status_info";
            try
            {
                dataTable = DbQueryManager.GetTable(query);
                if (dataTable != null)
                {
                    DataTableReader dataTableReader = dataTable.CreateDataReader();
                    if (dataTableReader != null)
                    {
                        while (dataTableReader.Read())
                        {
                            UpdateStatus status=new UpdateStatus();
                            status.LOAN_STATUS_ID = Convert.ToInt32(dataTableReader["LOAN_STATUS_ID"]);
                            status.LOAN_STATUS_NAME = dataTableReader["LOAN_STATUS_NAME"].ToString();
                            if (!Convert.IsDBNull(dataTableReader["LOAN_STATUS_TYPE"]))
                            {
                                status.LOAN_STATUS_TYPE = Convert.ToInt32(dataTableReader["LOAN_STATUS_TYPE"]);
                            }
                            status.JOB_STAT = Convert.ToInt32(dataTableReader["JOB_STAT"]);
                            updateStatusesList.Add(status);
                        }
                        dataTableReader.Close();
                    }
                    dataTable.Clear();
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return updateStatusesList;
        }

        public int UpdateStatus(UpdateStatus status)
        {
            int UpdateValue = -1;
            if (IsExistsUpdateStatus(status.LOAN_STATUS_ID))
            {
                UpdateValue = UpdateStatusInfo(status);
            }
            return UpdateValue;
        }

        public void Audit(int action_id, int module_id, string str_sql1, UpdateStatus status)
        {
            string query = "insert into audit_trail(ID_LEARNER,ACTION_ID,MODULE_ID,QRY_DATE,QRY_TIME,QRY_STRING) " +
                          "values (" + status.UserId + "," + action_id + "," + module_id + ", CAST(getdate() as DATE) ,CAST (getdate() as Time),'" + AddSlash(str_sql1) + "')";
            DbQueryManager.ExecuteNonQuery(query);
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private int UpdateStatusInfo(UpdateStatus status)
        {
            int updateRow = -1;
            int action_id=2; // action_id=2 is UPDATE action.
            int module_id=2;  // module_id=2 is PRODUCT module.
  
            string query = "update loan_status_info  set LOAN_STATUS_NAME='"+status.LOAN_STATUS_NAME+"' where  LOAN_STATUS_ID="+status.LOAN_STATUS_ID+"";
            Audit(action_id, module_id, query, status);
            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(query);
                if (updateRow > 0)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }

        private bool IsExistsUpdateStatus(int loanStatusId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM loan_status_info WHERE LOAN_STATUS_ID = {0}", loanStatusId);
            object statusObj;
            try
            {
                statusObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(statusObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
    }
}
