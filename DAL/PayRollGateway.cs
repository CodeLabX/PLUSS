﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{
    public class PayRollGateway
    {
        public DatabaseConnection dbMySQL = null;

        public PayRollGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        private List<PayRoll> payRollObjList = new List<PayRoll>();
        private PayRoll tPlussCompanyObj = new PayRoll();
        public List<PayRoll> GetAllPayRollInfo()
        {
            try
            {
                string queryString = "select COMP_ID, COMP_NAME, COMP_SEGMENT_ID, " +
                "COMP_STRUCTURE, COMP_DOCATEGORIZED, COMP_CATSTATUS, COMP_MUE, COMP_IRWITHEOSB, " +
                "COMP_IRWITHOUTEOSB, COMP_CCUCONDITION, COMP_CCUREMARKS, " +
                "COMP_PROPOSDBY, COMP_APPROVALDATE, COMP_STATUS,COMP_EXTRABENEFIT,COMP_EXTRAFIELD1,COMP_EXTRAFIELD2,COMP_EXTRAFIELD3,COMP_EXTRAFIELD4 " +
                "from t_pluss_company WHERE COMP_ENTRYSTATUS<>0 ORDER BY COMP_NAME";

                DataTable payRollDataTableObj = DbQueryManager.GetTable(queryString);


                if (payRollDataTableObj != null)
                {
                    DataTableReader payRollDataReaderObj = payRollDataTableObj.CreateDataReader();

                    while (payRollDataReaderObj.Read())
                    {
                        tPlussCompanyObj = new PayRoll();

                        tPlussCompanyObj.CompID = Convert.ToInt32(payRollDataReaderObj["Comp_ID"]);
                        tPlussCompanyObj.CompName = Convert.ToString(payRollDataReaderObj["Comp_Name"]);
                        tPlussCompanyObj.CompSegmentID = Convert.ToString(payRollDataReaderObj["Comp_Segment_ID"]);
                        tPlussCompanyObj.CompStructure = Convert.ToString(payRollDataReaderObj["Comp_Structure"]);
                        tPlussCompanyObj.CompDocategorized = Convert.ToString(payRollDataReaderObj["Comp_Docategorized"]);
                        tPlussCompanyObj.CompCatstatus = Convert.ToString(payRollDataReaderObj["Comp_Catstatus"]);
                        tPlussCompanyObj.CompMue = Convert.ToDouble(payRollDataReaderObj["Comp_Mue"]);
                        tPlussCompanyObj.CompIrwitheosb = Convert.ToString(payRollDataReaderObj["Comp_Irwitheosb"]);
                        tPlussCompanyObj.CompIrwithouteosb = Convert.ToString(payRollDataReaderObj["Comp_Irwithouteosb"]);
                        tPlussCompanyObj.CompCcucondition = Convert.ToString(payRollDataReaderObj["Comp_Ccucondition"]);
                        tPlussCompanyObj.CompCcuremarks = Convert.ToString(payRollDataReaderObj["Comp_Ccuremarks"]);
                        tPlussCompanyObj.CompProposdby = Convert.ToString(payRollDataReaderObj["Comp_Proposdby"]);
                        tPlussCompanyObj.CompApprovaldate = Convert.ToDateTime(Convert.ToDateTime(payRollDataReaderObj["Comp_Approvaldate"]).ToString("dd-MM-yyyy"), new System.Globalization.CultureInfo("ru-RU"));
                        if (!String.IsNullOrEmpty(payRollDataReaderObj["COMP_STATUS"].ToString()))
                        {
                            tPlussCompanyObj.CompStatus = Convert.ToInt32(payRollDataReaderObj["COMP_STATUS"]);
                        }
                       
                        tPlussCompanyObj.Date = Convert.ToString(payRollDataReaderObj["Comp_Approvaldate"]).Substring(0,9);
                        tPlussCompanyObj.ExtraBenefit = Convert.ToString(payRollDataReaderObj["COMP_EXTRABENEFIT"]);
                        tPlussCompanyObj.ExtraField1 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD1"]);
                        tPlussCompanyObj.ExtraField2= Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD2"]);
                        tPlussCompanyObj.ExtraField3 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD3"]);
                        tPlussCompanyObj.ExtraField4 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD4"]);

                        payRollObjList.Add(tPlussCompanyObj);
                    }
                    payRollDataReaderObj.Close();
                }
                return payRollObjList;

            }
            catch { return payRollObjList; }
        }

        public List<PayRoll> GetAllPayRollInfo( string searchingKey)
        {
            string queryString = "select COMP_ID, COMP_NAME, COMP_SEGMENT_ID, " +
            "COMP_STRUCTURE, COMP_DOCATEGORIZED, COMP_CATSTATUS, COMP_MUE, COMP_IRWITHEOSB, " +
            "COMP_IRWITHOUTEOSB, COMP_CCUCONDITION, COMP_CCUREMARKS, " +
            "COMP_PROPOSDBY, COMP_APPROVALDATE, COMP_STATUS,COMP_EXTRABENEFIT,COMP_EXTRAFIELD1,COMP_EXTRAFIELD2,COMP_EXTRAFIELD3,COMP_EXTRAFIELD4 " +
            "from t_pluss_company " +
            "where (COMP_ID like('%" + searchingKey.SpecialCharacterRemover() + "%') or COMP_NAME like('%" + searchingKey.SpecialCharacterRemover() + "%') or " +
            "COMP_SEGMENT_ID like('%" + searchingKey.SpecialCharacterRemover() + "%') or COMP_STRUCTURE like('%" + searchingKey.SpecialCharacterRemover() + "%') or " +
            "COMP_DOCATEGORIZED like('%" + searchingKey.SpecialCharacterRemover() + "%') or COMP_CATSTATUS like('%" + searchingKey.SpecialCharacterRemover() + "%') or " +
            "COMP_MUE like('%" + searchingKey.SpecialCharacterRemover() + "%') or COMP_IRWITHEOSB like('%" + searchingKey.SpecialCharacterRemover() + "%') or " +
            "COMP_IRWITHOUTEOSB like('%" + searchingKey.SpecialCharacterRemover() + "%') or COMP_CCUCONDITION like('%" + searchingKey.SpecialCharacterRemover() + "%') or " +
            "COMP_CCUREMARKS like('%" + searchingKey.SpecialCharacterRemover() + "%') or COMP_PROPOSDBY like('%" + searchingKey.SpecialCharacterRemover() + "%') or " +
            "COMP_APPROVALDATE like('%" + searchingKey.SpecialCharacterRemover() + "%')) AND COMP_ENTRYSTATUS<>0";

            DataTable payRollDataTableObj = DbQueryManager.GetTable(queryString);
            if (payRollDataTableObj != null)
            {
                DataTableReader payRollDataReaderObj = payRollDataTableObj.CreateDataReader();

                while (payRollDataReaderObj.Read())
                {
                    tPlussCompanyObj = new PayRoll();

                    tPlussCompanyObj.CompID = Convert.ToInt32(payRollDataReaderObj["Comp_ID"]);
                    tPlussCompanyObj.CompName = Convert.ToString(payRollDataReaderObj["Comp_Name"]);
                    tPlussCompanyObj.CompSegmentID = Convert.ToString(payRollDataReaderObj["Comp_Segment_ID"]);
                    tPlussCompanyObj.CompStructure = Convert.ToString(payRollDataReaderObj["Comp_Structure"]);
                    tPlussCompanyObj.CompDocategorized = Convert.ToString(payRollDataReaderObj["Comp_Docategorized"]);
                    tPlussCompanyObj.CompCatstatus = Convert.ToString(payRollDataReaderObj["Comp_Catstatus"]);
                    tPlussCompanyObj.CompMue = Convert.ToDouble("0"+payRollDataReaderObj["Comp_Mue"]);
                    tPlussCompanyObj.CompIrwitheosb = Convert.ToString(payRollDataReaderObj["Comp_Irwitheosb"]);
                    tPlussCompanyObj.CompIrwithouteosb = Convert.ToString(payRollDataReaderObj["Comp_Irwithouteosb"]);
                    tPlussCompanyObj.CompCcucondition = Convert.ToString(payRollDataReaderObj["Comp_Ccucondition"]);
                    tPlussCompanyObj.CompCcuremarks = Convert.ToString(payRollDataReaderObj["Comp_Ccuremarks"]);
                    tPlussCompanyObj.CompProposdby = Convert.ToString(payRollDataReaderObj["Comp_Proposdby"]);
                    tPlussCompanyObj.CompApprovaldate = Convert.ToDateTime(payRollDataReaderObj["Comp_Approvaldate"]);
                    if (!String.IsNullOrEmpty(payRollDataReaderObj["COMP_STATUS"].ToString()))
                    {
                         tPlussCompanyObj.CompStatus = Convert.ToInt32(payRollDataReaderObj["COMP_STATUS"]);
                    }
                   
                    tPlussCompanyObj.ExtraBenefit = Convert.ToString(payRollDataReaderObj["COMP_EXTRABENEFIT"]);
                    tPlussCompanyObj.ExtraField1 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD1"]);
                    tPlussCompanyObj.ExtraField2= Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD2"]);
                    tPlussCompanyObj.ExtraField3 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD3"]);
                    tPlussCompanyObj.ExtraField4 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD4"]);

                    payRollObjList.Add(tPlussCompanyObj);
                }
                payRollDataReaderObj.Close();

            }
            return payRollObjList;
        }

        public PayRoll SelectCompanyInfo(Int32 companyId, string companyName)
        {
            string idCondition = string.Empty;
            string nameCondition = string.Empty;
            if (companyId > 0)
            {
                idCondition = " AND COMP_ID=" + companyId;
            }
            else
            {
                idCondition = string.Empty;
            }
            if (!string.IsNullOrEmpty(companyName))
            {
                nameCondition = " AND TRIM(UCASE(COMP_NAME)) LIKE (%'" + companyName.ToUpper().Trim() + "'%)";
            }
            else
            {
                nameCondition = string.Empty;
            }
            string queryString = "SELECT COMP_ID, COMP_NAME, COMP_SEGMENT_ID, " +
            "COMP_STRUCTURE, COMP_DOCATEGORIZED, COMP_CATSTATUS, COMP_MUE, COMP_IRWITHEOSB, " +
            "COMP_IRWITHOUTEOSB, COMP_CCUCONDITION, COMP_CCUREMARKS, " +
            "COMP_PROPOSDBY, COMP_APPROVALDATE, COMP_STATUS,COMP_EXTRABENEFIT,COMP_EXTRAFIELD1,COMP_EXTRAFIELD2,COMP_EXTRAFIELD3,COMP_EXTRAFIELD4 " +
            "FROM t_pluss_company " +
            "WHERE COMP_ENTRYSTATUS=1 " + idCondition + nameCondition + " ORDER BY COMP_NAME";
            PayRoll tPlussCompanyObj = null;
            DataTable payRollDataTableObj = DbQueryManager.GetTable(queryString);
            if (payRollDataTableObj != null)
            {
                DataTableReader payRollDataReaderObj = payRollDataTableObj.CreateDataReader();

                while (payRollDataReaderObj.Read())
                {
                    tPlussCompanyObj = new PayRoll();
                    tPlussCompanyObj.CompID = Convert.ToInt32(payRollDataReaderObj["Comp_ID"]);
                    tPlussCompanyObj.CompName = Convert.ToString(payRollDataReaderObj["Comp_Name"]);
                    tPlussCompanyObj.CompSegmentID = Convert.ToString(payRollDataReaderObj["Comp_Segment_ID"]);
                    tPlussCompanyObj.CompStructure = Convert.ToString(payRollDataReaderObj["Comp_Structure"]);
                    tPlussCompanyObj.CompDocategorized = Convert.ToString(payRollDataReaderObj["Comp_Docategorized"]);
                    tPlussCompanyObj.CompCatstatus = Convert.ToString(payRollDataReaderObj["Comp_Catstatus"]);
                    tPlussCompanyObj.CompMue = Convert.ToDouble("0" + payRollDataReaderObj["Comp_Mue"]);
                    tPlussCompanyObj.CompIrwitheosb = Convert.ToString(payRollDataReaderObj["Comp_Irwitheosb"]);
                    tPlussCompanyObj.CompIrwithouteosb = Convert.ToString(payRollDataReaderObj["Comp_Irwithouteosb"]);
                    tPlussCompanyObj.CompCcucondition = Convert.ToString(payRollDataReaderObj["Comp_Ccucondition"]);
                    tPlussCompanyObj.CompCcuremarks = Convert.ToString(payRollDataReaderObj["Comp_Ccuremarks"]);
                    tPlussCompanyObj.CompProposdby = Convert.ToString(payRollDataReaderObj["Comp_Proposdby"]);
                    tPlussCompanyObj.CompApprovaldate = Convert.ToDateTime(payRollDataReaderObj["Comp_Approvaldate"]);
                    if (!String.IsNullOrEmpty(payRollDataReaderObj["COMP_STATUS"].ToString()))
                    {
                        tPlussCompanyObj.CompStatus = Convert.ToInt32(payRollDataReaderObj["COMP_STATUS"]);
                    }
                   
                    tPlussCompanyObj.ExtraBenefit = Convert.ToString(payRollDataReaderObj["COMP_EXTRABENEFIT"]);
                    tPlussCompanyObj.ExtraField1 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD1"]);
                    tPlussCompanyObj.ExtraField2 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD2"]);
                    tPlussCompanyObj.ExtraField3 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD3"]);
                    tPlussCompanyObj.ExtraField4 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD4"]);
                }
                payRollDataReaderObj.Close();

            }
            return tPlussCompanyObj;
        }

        public int InsertPayroll(PayRoll CompanyObj)
        {
            try
            {
                int id = SelectCompanyId(CompanyObj.CompName);
                if (id == 0)
                {
                    string sqlInsert = "insert into t_pluss_company " +
                "(COMP_NAME, COMP_SEGMENT_ID, " +
                "COMP_STRUCTURE, COMP_DOCATEGORIZED, " +
                "COMP_CATSTATUS, COMP_MUE, COMP_IRWITHEOSB, " +
                "COMP_IRWITHOUTEOSB, COMP_CCUCONDITION, " +
                "COMP_CCUREMARKS, COMP_PROPOSDBY, " +
                "COMP_APPROVALDATE,COMP_ENTRYSTATUS,COMP_EXTRABENEFIT,COMP_EXTRAFIELD1,COMP_EXTRAFIELD2,COMP_EXTRAFIELD3,COMP_EXTRAFIELD4) " +
                "values ('" + CompanyObj.CompName.SpecialCharacterRemover() + "', '" + CompanyObj.CompSegmentID.SpecialCharacterRemover() + "', '" +
                CompanyObj.CompStructure.SpecialCharacterRemover() + "', '" + CompanyObj.CompDocategorized.SpecialCharacterRemover() + "', '" +
                CompanyObj.CompCatstatus.SpecialCharacterRemover() + "', '" + CompanyObj.CompMue + "', '" +
                CompanyObj.CompIrwitheosb.SpecialCharacterRemover() + "', '" + CompanyObj.CompIrwithouteosb.SpecialCharacterRemover() + "', '" +
                CompanyObj.CompCcucondition.SpecialCharacterRemover() + "', '" + CompanyObj.CompCcuremarks.SpecialCharacterRemover() + "', '" +
                CompanyObj.CompProposdby.SpecialCharacterRemover() + "', '" + CompanyObj.CompApprovaldate.ToString("yyyy-MM-dd") + "',1,'" +
                CompanyObj.ExtraBenefit + "','" + CompanyObj.ExtraField1.SpecialCharacterRemover() + "','" + CompanyObj.ExtraField2.SpecialCharacterRemover() + "','" + 
                CompanyObj.ExtraField3.SpecialCharacterRemover() + "','" + CompanyObj.ExtraField3.SpecialCharacterRemover() + "')";

                    int insert = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                    return insert;
                }
                else
                {
                    CompanyObj.CompID = id;
                    int update = UpdatePayroll(CompanyObj);
                    return update;
                }
            }
            catch(Exception ex)
            {
                return 0;
            }
        }


        public int UpdatePayroll(PayRoll CompanyObj)
        {
            try
            {
                string sqlUpdate = "UPDATE t_pluss_company SET " +
                "COMP_NAME = '" + CompanyObj.CompName.SpecialCharacterRemover() + "'," +
                "COMP_SEGMENT_ID = '" + CompanyObj.CompSegmentID.SpecialCharacterRemover() + "'," +
                "COMP_STRUCTURE = '" + CompanyObj.CompStructure.SpecialCharacterRemover() + "'," +
                "COMP_DOCATEGORIZED = '" + CompanyObj.CompDocategorized.SpecialCharacterRemover() + "'," +
                "COMP_CATSTATUS = '" + CompanyObj.CompCatstatus.SpecialCharacterRemover() + "'," +
                "COMP_MUE = " + CompanyObj.CompMue + "," +
                "COMP_IRWITHEOSB = '" + CompanyObj.CompIrwitheosb.SpecialCharacterRemover() + "'," +
                "COMP_IRWITHOUTEOSB = '" + CompanyObj.CompIrwithouteosb.SpecialCharacterRemover() + "'," +
                "COMP_CCUCONDITION = '" + CompanyObj.CompCcucondition.SpecialCharacterRemover() + "'," +
                "COMP_CCUREMARKS = '" + CompanyObj.CompCcuremarks.SpecialCharacterRemover() + "'," +
                "COMP_PROPOSDBY = '" + CompanyObj.CompProposdby.SpecialCharacterRemover() + "'," +
                "COMP_APPROVALDATE = '" + CompanyObj.CompApprovaldate.ToString("yyyy-MM-dd") + "'," +
                "COMP_STATUS = " + CompanyObj.CompStatus + "," +
                "COMP_EXTRABENEFIT='" + CompanyObj.ExtraBenefit + "'," +
                "COMP_EXTRAFIELD1='" + CompanyObj.ExtraField1.SpecialCharacterRemover() + "'," +
                "COMP_EXTRAFIELD2='" + CompanyObj.ExtraField2.SpecialCharacterRemover() + "'," +
                "COMP_EXTRAFIELD3='" + CompanyObj.ExtraField3.SpecialCharacterRemover() + "'," +
                "COMP_EXTRAFIELD4='" + CompanyObj.ExtraField3.SpecialCharacterRemover() + "'," +
                "COMP_ENTRYSTATUS = 1 WHERE COMP_ID=" + CompanyObj.CompID; 
                int status = DbQueryManager.ExecuteNonQuery(sqlUpdate);   // if sucessfull then 1
                return status;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        public int DeletePayroll(int id)
        {
            string queryString = "DELETE FROM t_pluss_company WHERE COMP_ID = " + id + " AND COMP_ENTRYSTATUS = 0";

            try
            {
                int status = DbQueryManager.ExecuteNonQuery(queryString);
                return status;
            }
            catch { return 0; }
        }
        //-------------------Add Hiru----------------------------------
        public List<PayRoll> SarchByLetters(string letters)
        {
            List<PayRoll> payRollObjList = new List<PayRoll>();
            try
            {
                string queryString = "SELECT COMP_ID, COMP_NAME from t_pluss_company WHERE COMP_NAME LIKE '"+letters+"%'";
                DataTable payRollDataTableObj = DbQueryManager.GetTable(queryString);
                if (payRollDataTableObj != null)
                {
                    DataTableReader payRollDataReaderObj = payRollDataTableObj.CreateDataReader();
                    while (payRollDataReaderObj.Read())
                    {
                        tPlussCompanyObj = new PayRoll();
                        tPlussCompanyObj.CompID = Convert.ToInt32(payRollDataReaderObj["Comp_ID"]);
                        tPlussCompanyObj.CompName = Convert.ToString(payRollDataReaderObj["Comp_Name"]);                     
                        payRollObjList.Add(tPlussCompanyObj);
                    }
                    payRollDataReaderObj.Close();
                }
                return payRollObjList;

            }
            catch { return payRollObjList; }
        }

        public int SelectCompanyId(string companyName)
        {
            string mSQL = "SELECT COMP_ID FROM t_pluss_company WHERE REPLACE(Upper(LTRIM(RTRIM(COMP_NAME))),' ','')='" + companyName.ToUpper().Trim().Replace(" ", "") + "'";
            Int32 returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }
        public void InsertCompanyName(string companyName)
        {
            string mSQL = "INSERT INTO t_pluss_company (COMP_NAME,COMP_ENTRYSTATUS) VALUES('" + AddSlash(companyName.Trim()) + "',0)";
            DbQueryManager.ExecuteNonQuery(mSQL);
        }
        public List<PayRoll> GetAllCompany()
        {
            List<PayRoll> payRollObjList = new List<PayRoll>();
            try
            {
                string queryString = "SELECT COMP_ID, COMP_NAME from t_pluss_company WHERE COMP_ENTRYSTATUS=0";
                DataTable payRollDataTableObj = DbQueryManager.GetTable(queryString);
                if (payRollDataTableObj != null)
                {
                    DataTableReader payRollDataReaderObj = payRollDataTableObj.CreateDataReader();
                    while (payRollDataReaderObj.Read())
                    {
                        tPlussCompanyObj = new PayRoll();
                        tPlussCompanyObj.CompID = Convert.ToInt32(payRollDataReaderObj["Comp_ID"]);
                        tPlussCompanyObj.CompName = Convert.ToString(payRollDataReaderObj["Comp_Name"]);
                        payRollObjList.Add(tPlussCompanyObj);
                    }
                    payRollDataReaderObj.Close();
                }
                return payRollObjList;

            }
            catch { return payRollObjList; }

        }
        
    }
}
