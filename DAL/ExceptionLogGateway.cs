﻿using System;
using Bat.Common;

namespace DAL
{
    
    public class ExceptionLogGateway
    {
        public void SaveExceptionLog(ExceptionLog exceptionLog)
        {
            string sql = "";
            try
            {
                int result = 0;
               

                sql =string.Format(@"Insert Into CDMS_ExceptionLog(OperationType,ExceptionDescription,ExceptionDate,UserId)
                        Values('{0}','{1}','{2}','{3}')", exceptionLog.OperationType, exceptionLog.ExceptionDescription,
                        exceptionLog.ExceptionDate,exceptionLog.UserId);
                result = DbQueryManager.ExecuteNonQuery(sql);

                if (result == 0)
                {
                    throw new Exception("Failed");
                }
                else
                {
                    throw new Exception("Save Success");
                }
               


            }
            catch (Exception exception)
            {
                
            }

        }
    }
}
