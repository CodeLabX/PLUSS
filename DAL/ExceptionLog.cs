﻿using System;

namespace DAL
{
    /// <summary>
    /// Summary description for ExceptionLog
    /// </summary>
    public class ExceptionLog
    {
        public string OperationType { get; set; }
        public string ExceptionDescription { get; set; }
        public DateTime ExceptionDate { get; set; }
        public string UserId { get; set; }
    }
}


