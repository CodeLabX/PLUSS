﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// Analyst approve gateway class.
    /// </summary>
    public class AnalystApproveGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor.
        /// </summary>
        public AnalystApproveGateway()
        {
            dbMySQL = new DatabaseConnection();
        }

        /// <summary>
        /// This method provide pl information
        /// </summary>
        /// <param name="UserId">Gets a user id.</param>
        /// <returns>Returns analyst approve list object.</returns>
        public List<AnalystApprove> GetAllPL(Int32 UserId, string searchKey)
        {
            string searchCondition = string.Empty;
            if(searchKey!="" || searchKey!=null)
            {
                searchCondition = " AND (PL_LLID LIKE '%" + searchKey + "%' OR CUST_NM LIKE '%" + searchKey + "%')";
            }
            else
            {
                searchCondition = "";
            }

            string mSQL = "SELECT PL_ID , PL_LLID ,LA.CUST_NM ,LA.LOAN_APP_AMT ,isnull(DEVI_ENTRYDATE,getdate()) as DEVI_ENTRYDATE ,PL_PRODUCT_ID  ,P.PROD_NAME ,P.PROD_TYPE ,PL_SEGMENT_ID ,SEGM_NAME , PL_LOANTYPE_ID ,LOTY_NAME, PL_INCOMEACESSMENTMETHOD_ID , PL_SECTOR_ID , PL_SUBSECTOR_ID, PL_COMPANY_ID , PL_COMPANYCATAGORY_ID, PL_COMPANYTYPE_ID , PL_DOCATAGORIZED_ID , PL_COMPANYSEGMENT_ID , PL_INCOME , PL_OWNERSHIP , PL_PROFITMARGIN, PL_OTHERINCOMESOURCE_ID , PL_OTHERINCOME , PL_NETINCOME , PL_DECLAREDINCOME , PL_SCBCREDITCARDLIMIT , PL_INDUSTRYPROFITMARGINRAT , PL_PLIPLFLOUTSTANDING , PL_MONTHLYINTEREST , PL_RESIEDENTIALSTATUS_ID, PL_FLSECUITYOS , PL_AVERAGEBALANCE, PL_EMIWITHSCBSOURCE_ID , PL_EMIWITHSCB, PL_OTHEREMISOURCE , PL_OTHEREMI, PL_INSTABUYBALANCE, PL_INSTABUYINSTALLMENT , PL_BBPDDMAXLOANAMOUND , PL_DBRMULTIPLIER , PL_DBRLEVELID, PL_DBRMAXLOANAMOUNT , PL_AVGBALANCMAXLOANAMOUNT , PL_MUEMULTIPLIER , PL_MUEMAXLOANAMOUNT , PL_INTERESTRAT, PL_TENURE, PL_FIRSTPAYMENT , PL_SAFETYPLUS_ID , PL_APPLIEDLOANAMOUNT , PL_APPROPRIATEDLOANAMOUNT ,(case when PL_SAFETYPLUS_ID=1 then PL_BANCALOANAMOUNT  else PL_PROPOSEDLOANAMOUNT end)  as PL_PROPOSEDLOANAMOUNT ,PL_EMI , PL_EMIFACTOR , PL_DBR , PL_MUE , PL_REMARKS , PL_USER_ID, PL_ENTRYDATE, PL_APPROVESTATUS,SCPI_INTRECEPTVALUE ,SCPI_TOTALPOINT  ,SCPI_RESULT ,USER_NAME ,DEVI_LEVEL1,DEVI_LEVEL2,DEVI_LEVEL3,DEVI_LEVEL4 FROM t_pluss_user,t_pluss_deviation,t_pluss_pl LEFT JOIN loan_app_info as LA ON PL_LLID=LA.LOAN_APPLI_ID " +
                          "LEFT JOIN t_pluss_segment ON PL_SEGMENT_ID=SEGM_ID LEFT JOIN t_pluss_product as P ON PL_PRODUCT_ID=P.PROD_ID LEFT "+
                          "JOIN t_pluss_loantype ON PL_LOANTYPE_ID=LOTY_ID LEFT JOIN t_pluss_loanscorepoint ON PL_LLID=SCPI_LLID  WHERE PL_LLID=DEVI_LLID AND DEVI_USER_ID=USER_ID AND DEVI_USER_ID<>" + UserId + " AND PL_APPROVESTATUS=0" + searchCondition + "";
            

            var data = DbQueryManager.GetTable(mSQL);
            List<AnalystApprove> analystApproveObjList = new List<AnalystApprove>();
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    var analystApproveObj = new AnalystApprove();
                    analystApproveObj.PL.PlId = Convert.ToInt64(xRs["PL_ID"]);
                    analystApproveObj.PL.PlLlid = Convert.ToInt64(xRs["PL_LLID"]);
                    analystApproveObj.LoanInformation.ApplicantName = Convert.ToString(xRs["CUST_NM"]);
                    analystApproveObj.LoanInformation.AppliedAmount =
                        Convert.ToDecimal("0" + xRs["LOAN_APP_AMT"]);
                    analystApproveObj.EntryDate = Convert.ToDateTime(xRs["DEVI_ENTRYDATE"]);
                    analystApproveObj.PL.PlProductId = Convert.ToInt32(xRs["PL_PRODUCT_ID"]);
                    analystApproveObj.Product.ProdName = Convert.ToString(xRs["PROD_NAME"]);
                    analystApproveObj.Product.ProdType = Convert.ToString(xRs["PROD_TYPE"]);
                    analystApproveObj.PL.PlSegmentId = Convert.ToInt32(xRs["PL_SEGMENT_ID"]);
                    analystApproveObj.Segment.Name = Convert.ToString(xRs["SEGM_NAME"]);
                    analystApproveObj.PL.PlLoantypeId = Convert.ToInt32(xRs["PL_LOANTYPE_ID"]);
                    analystApproveObj.LoanType.Name = Convert.ToString(xRs["LOTY_NAME"]);
                    analystApproveObj.PL.PlIncomeacessmentmethodId =
                        Convert.ToInt32(xRs["PL_INCOMEACESSMENTMETHOD_ID"]);
                    analystApproveObj.PL.PlSectorId = Convert.ToInt32(xRs["PL_SECTOR_ID"]);
                    analystApproveObj.PL.PlSubsectorId = Convert.ToInt32(xRs["PL_SUBSECTOR_ID"]);
                    analystApproveObj.PL.PlCompanyId = Convert.ToInt32(xRs["PL_COMPANY_ID"]);
                    analystApproveObj.PL.PlCompanycatagoryId = Convert.ToInt32(xRs["PL_COMPANYCATAGORY_ID"]);
                    analystApproveObj.PL.PlCompanytypeId = Convert.ToInt32(xRs["PL_COMPANYTYPE_ID"]);
                    analystApproveObj.PL.PlDocatagorizedId = Convert.ToInt32(xRs["PL_DOCATAGORIZED_ID"]);
                    analystApproveObj.PL.PlCompanysegmentId = Convert.ToInt32(xRs["PL_COMPANYSEGMENT_ID"]);
                    analystApproveObj.PL.PlIncome = Convert.ToDouble(xRs["PL_INCOME"]);
                    analystApproveObj.PL.PlOwnership = Convert.ToDouble(xRs["PL_OWNERSHIP"]);
                    analystApproveObj.PL.PlProfitmargin = Convert.ToDouble(xRs["PL_PROFITMARGIN"]);
                    analystApproveObj.PL.PlOtherincomesourceId =
                        Convert.ToInt32(xRs["PL_OTHERINCOMESOURCE_ID"]);
                    analystApproveObj.PL.PlOtherincome = Convert.ToDouble(xRs["PL_OTHERINCOME"]);
                    analystApproveObj.PL.PlNetincome = Convert.ToDouble(xRs["PL_NETINCOME"]);
                    analystApproveObj.PL.PlDeclaredincome = Convert.ToDouble(xRs["PL_DECLAREDINCOME"]);
                    analystApproveObj.PL.PlScbcreditcardlimit =
                        Convert.ToDouble(xRs["PL_SCBCREDITCARDLIMIT"]);
                    analystApproveObj.PL.PlIndustryprofitmarginrat =
                        Convert.ToString(xRs["PL_INDUSTRYPROFITMARGINRAT"]);
                    analystApproveObj.PL.PlPliplfloutstanding =
                        Convert.ToDouble(xRs["PL_PLIPLFLOUTSTANDING"]);
                    analystApproveObj.PL.PlMonthlyinterest = Convert.ToDouble(xRs["PL_MONTHLYINTEREST"]);
                    analystApproveObj.PL.PlResiedentialstatusId =
                        Convert.ToInt32(xRs["PL_RESIEDENTIALSTATUS_ID"]);
                    analystApproveObj.PL.PlFlsecuityos = Convert.ToDouble(xRs["PL_FLSECUITYOS"]);
                    analystApproveObj.PL.PlAveragebalance = Convert.ToDouble(xRs["PL_AVERAGEBALANCE"]);
                    analystApproveObj.PL.PlEmiwithscbsourceId =
                        Convert.ToInt32(xRs["PL_EMIWITHSCBSOURCE_ID"]);
                    analystApproveObj.PL.PlEmiwithscb = Convert.ToDouble(xRs["PL_EMIWITHSCB"]);
                    analystApproveObj.PL.PlOtheremisource = Convert.ToString(xRs["PL_OTHEREMISOURCE"]);
                    analystApproveObj.PL.PlOtheremi = Convert.ToDouble(xRs["PL_OTHEREMI"]);
                    analystApproveObj.PL.PlInstabuybalance = Convert.ToDouble(xRs["PL_INSTABUYBALANCE"]);
                    analystApproveObj.PL.PlInstabuyinstallment =
                        Convert.ToDouble(xRs["PL_INSTABUYINSTALLMENT"]);
                    analystApproveObj.PL.PlBbpddmaxloanamound =
                        Convert.ToDouble(xRs["PL_BBPDDMAXLOANAMOUND"]);
                    analystApproveObj.PL.PlDbrmultiplier = Convert.ToInt32(xRs["PL_DBRMULTIPLIER"]);
                    analystApproveObj.PL.PlDbrlevelId = Convert.ToInt32(xRs["PL_DBRLEVELID"]);
                    analystApproveObj.PL.PlDbrmaxloanamount = Convert.ToDouble(xRs["PL_DBRMAXLOANAMOUNT"]);
                    analystApproveObj.PL.PlAvgbalancmaxloanamount =
                        Convert.ToDouble(xRs["PL_AVGBALANCMAXLOANAMOUNT"]);
                    analystApproveObj.PL.PlMuemultiplier = Convert.ToDouble(xRs["PL_MUEMULTIPLIER"]);
                    analystApproveObj.PL.PlMuemaxloanamount = Convert.ToDouble(xRs["PL_MUEMAXLOANAMOUNT"]);
                    analystApproveObj.PL.PlInterestrat = Convert.ToDouble(xRs["PL_INTERESTRAT"]);
                    analystApproveObj.PL.PlTenure = Convert.ToInt32(xRs["PL_TENURE"]);
                    analystApproveObj.PL.PlFirstpayment = Convert.ToInt32(xRs["PL_FIRSTPAYMENT"]);
                    analystApproveObj.PL.PlSafetyplusId = Convert.ToInt32(xRs["PL_SAFETYPLUS_ID"]);
                    analystApproveObj.PL.PlAppliedloanamount = Convert.ToDouble(xRs["PL_APPLIEDLOANAMOUNT"]);
                    analystApproveObj.PL.PlAppropriatedloanamount =
                        Convert.ToDouble(xRs["PL_APPROPRIATEDLOANAMOUNT"]);
                    analystApproveObj.PL.PlProposedloanamount =
                        Convert.ToDouble(xRs["PL_PROPOSEDLOANAMOUNT"]);
                    analystApproveObj.DEVI_LEVEL1 =(int) (xRs["DEVI_LEVEL1"]);
                    analystApproveObj.DEVI_LEVEL2 =(int) (xRs["DEVI_LEVEL2"]);
                    analystApproveObj.DEVI_LEVEL3 =(int) (xRs["DEVI_LEVEL3"]);
                    analystApproveObj.DEVI_LEVEL4 =(int) (xRs["DEVI_LEVEL4"]);
                    analystApproveObj.PL.PlEmi = Convert.ToDouble(xRs["PL_EMI"]);
                    analystApproveObj.PL.PlEmifactor = Convert.ToDouble(xRs["PL_EMIFACTOR"]);
                    analystApproveObj.PL.PlDbr = Convert.ToDouble(xRs["PL_DBR"]);
                    analystApproveObj.PL.PlMue = Convert.ToDouble(xRs["PL_MUE"]);
                    analystApproveObj.PL.PlRemarks = Convert.ToString(xRs["PL_REMARKS"]);
                    analystApproveObj.User.Name = Convert.ToString(xRs["USER_NAME"]);
                    if (!String.IsNullOrEmpty(xRs["SCPI_INTRECEPTVALUE"].ToString()))
                    {
                        analystApproveObj.LoanScorePoint.IntreceptValue =
                       Convert.ToDouble(xRs["SCPI_INTRECEPTVALUE"]);
                    }
                    if (!String.IsNullOrEmpty(xRs["SCPI_TOTALPOINT"].ToString()))
                    {
                        analystApproveObj.LoanScorePoint.TotalPoint = Convert.ToDouble(xRs["SCPI_TOTALPOINT"]);
                    }
                    if (!String.IsNullOrEmpty(xRs["SCPI_RESULT"].ToString()))
                    {
                        analystApproveObj.LoanScorePoint.Result = Convert.ToString(xRs["SCPI_RESULT"]);
                    }

                    if (analystApproveObj.DEVI_LEVEL1 > 0 || analystApproveObj.DEVI_LEVEL2 > 0 ||
                        analystApproveObj.DEVI_LEVEL3 > 0 || analystApproveObj.DEVI_LEVEL4 > 0)
                    {
                        if (analystApproveObj.DEVI_LEVEL1 == 2 || analystApproveObj.DEVI_LEVEL2 == 2 ||
                            analystApproveObj.DEVI_LEVEL3 == 2 || analystApproveObj.DEVI_LEVEL4 == 2)
                        {
                            analystApproveObj.PL.PlLevel = "L2";
                        }
                        else
                        {
                            analystApproveObj.PL.PlLevel = "L3";
                        }
                    }
                    analystApproveObjList.Add(analystApproveObj);
                }
            }
            return analystApproveObjList;
        }

        /// <summary>
        /// This method provide analyst approve info.
        /// </summary>
        /// <param name="lLId">Gets llid.</param>
        /// <param name="UserId">Gets user id.</param>
        /// <returns>Returns analyst approve object.</returns>
        public AnalystApprove SelectAnalystApprove(Int64 lLId, Int32 UserId)
        {
            string queryString = "SELECT * FROM t_pluss_approvalqueue WHERE APQU_LLID=" + lLId + " AND APQU_USER_ID=" + UserId; 
           
            AnalystApprove analystApproveObj = null;
             var data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while(xRs.Read())
                {
                analystApproveObj = new AnalystApprove();
                    analystApproveObj.Id = Convert.ToInt64(xRs["APQU_ID"]);
                    analystApproveObj.LlId = Convert.ToInt64(xRs["APQU_LLID"]);
                    analystApproveObj.CustomerName = Convert.ToString(xRs["APQU_CUSTOMERNAME"]);
                    analystApproveObj.LoanAmount = Convert.ToDouble(xRs["APQU_LOANAMOUNT"]);
                    analystApproveObj.Level = Convert.ToString(xRs["APQU_LEVEL"]);
                    analystApproveObj.LoanTag = Convert.ToString(xRs["APQU_LOANTAG"]);
                    analystApproveObj.Score = Convert.ToDouble(xRs["APQU_SCORE"]);
                    analystApproveObj.Result = Convert.ToString(xRs["APQU_RESULT"]);
                    analystApproveObj.Decision = Convert.ToString(xRs["APQU_DECESION"]);
                    analystApproveObj.Remarks = Convert.ToString(xRs["APQU_REMARKS"]);
                    analystApproveObj.UserId = Convert.ToInt32("0" + xRs["APQU_USER_ID"]);
            }
            xRs.Close();
            }
            return analystApproveObj;
        }
        public AnalystApprove SelectAnalystApprove(Int64 lLId)
        {
            string mSQL = "SELECT * FROM t_pluss_approvalqueue WHERE APQU_LLID=" + lLId ;
            AnalystApprove analystApproveObj = null;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                DataTableReader xRs = data.CreateDataReader();
                if (xRs.Read())
                {
                    analystApproveObj = new AnalystApprove();
                    analystApproveObj.Id = Convert.ToInt64(xRs["APQU_ID"]);
                    analystApproveObj.LlId = Convert.ToInt64(xRs["APQU_LLID"]);
                    analystApproveObj.CustomerName = Convert.ToString(xRs["APQU_CUSTOMERNAME"]);
                    analystApproveObj.LoanAmount = Convert.ToDouble(xRs["APQU_LOANAMOUNT"]);
                    analystApproveObj.Level = Convert.ToString(xRs["APQU_LEVEL"]);
                    analystApproveObj.LoanTag = Convert.ToString(xRs["APQU_LOANTAG"]);
                    analystApproveObj.Score = Convert.ToDouble(xRs["APQU_SCORE"]);
                    analystApproveObj.Result = Convert.ToString(xRs["APQU_RESULT"]);
                    analystApproveObj.Decision = Convert.ToString(xRs["APQU_DECESION"]);
                    analystApproveObj.Remarks = Convert.ToString(xRs["APQU_REMARKS"]);
                    analystApproveObj.UserId = Convert.ToInt32("0" + xRs["APQU_USER_ID"]);
                    try
                    {
                        analystApproveObj.EntryDate = Convert.ToDateTime(xRs["APQU_ENTRYDATE"]);
                    }
                    catch (Exception)
                    {

                        analystApproveObj.EntryDate = DateTime.Now;
                    }

                }
                xRs.Close();
            }
            return analystApproveObj;
        }

        /// <summary>
        /// This method provide insert or update analyst approve information.
        /// </summary>
        /// <param name="analystApproveObj">Gets a analyst approve object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendAnalystApproveInToDB(AnalystApprove analystApproveObj)
        {
            AnalystApprove existingAnalystApproveObj = SelectAnalystApprove(analystApproveObj.LlId, analystApproveObj.UserId);
            if (existingAnalystApproveObj != null)
            {
                analystApproveObj.Id = existingAnalystApproveObj.Id;
                return UpdateAnalystApprove(analystApproveObj);
            }
            else
            {
                return InsertAnalystApprove(analystApproveObj);
            }
        }
        /// <summary>
        /// This method provide insert analyst approve information.
        /// </summary>
        /// <param name="analystApproveObj">Gets a analyst approve object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertAnalystApprove(AnalystApprove analystApproveObj)
        {
            try
            {
                string queryString = "INSERT INTO t_pluss_approvalqueue (APQU_LLID,APQU_CUSTOMERNAME,APQU_LOANAMOUNT," +
                              "APQU_LEVEL,APQU_LOANTAG,APQU_SCORE," +
                              "APQU_RESULT, APQU_DECESION, " +
                              "APQU_REMARKS, APQU_USER_ID,"+
                              "APQU_ENTRYDATE) VALUES(" +
                              analystApproveObj.LlId + ",'" +
                              analystApproveObj.CustomerName + "'," +
                              analystApproveObj.LoanAmount + ",'" +
                              analystApproveObj.Level + "','" +
                              analystApproveObj.LoanTag + "'," +
                              analystApproveObj.Score + ",'" +
                              analystApproveObj.Result + "','" +
                              analystApproveObj.Decision + "','" +
                              AddSlash(analystApproveObj.Remarks) + "'," +
                              analystApproveObj.UserId + ",'" +
                              analystApproveObj.EntryDate.ToString("yyyy-MM-dd") + "')";
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method provide update analyst approve information.
        /// </summary>
        /// <param name="analystApproveObj">Gets a analyst approve object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateAnalystApprove(AnalystApprove analystApproveObj)
        {
            try
            {
                string queryString = "UPDATE t_pluss_approvalqueue SET " +
                              "APQU_LLID=" + analystApproveObj.LlId + "," +
                              "APQU_CUSTOMERNAME='" + analystApproveObj.CustomerName + "'," +
                              "APQU_LOANAMOUNT=" + analystApproveObj.LoanAmount + "," +
                              "APQU_LEVEL='" + analystApproveObj.Level + "'," +
                              "APQU_LOANTAG='" + analystApproveObj.LoanTag + "'," +
                              "APQU_SCORE=" + analystApproveObj.Score + "," +
                              "APQU_RESULT='" + analystApproveObj.Result + "'," +
                              "APQU_DECESION='" + analystApproveObj.Decision + "'," +
                              "APQU_REMARKS='" + AddSlash(analystApproveObj.Remarks) + "'," +
                              "APQU_USER_ID=" + analystApproveObj.UserId + "," +
                              "APQU_ENTRYDATE='" + analystApproveObj.EntryDate.ToString("yyyy-MM-dd") + "' WHERE APQU_ID=" + analystApproveObj.Id;
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1) 
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide adding the special character.
        /// </summary>
        /// <param name="sMessage">Gets a message.</param>
        /// <returns>Returns a coverted string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        /// <summary>
        /// This method provide the update pluss loan status.
        /// </summary>
        /// <param name="LLId">Gets a llid.</param>
        /// <param name="updateStatus">Gets a update status flaq.</param>
        public void UpdatePlussLoanStatus(Int64 LLId, int updateStatus)
        {
            string mSQL = "UPDATE loan_app_info SET LOAN_STATU_ID=" + updateStatus + " WHERE LOAN_APPLI_ID=" + LLId;
            DbQueryManager.ExecuteNonQuery(mSQL);
        }
        /// <summary>
        /// This method provide the update PL status.
        /// </summary>
        /// <param name="LLId">Gets a llid.</param>
        public void UpdatePLStatus(Int64 LLId)
        {
            string mSQL = "UPDATE t_pluss_pl SET PL_APPROVESTATUS=1 WHERE PL_LLID=" + LLId;
            DbQueryManager.ExecuteNonQuery(mSQL);
        }
        
        /// <summary>
        /// This method provide the update pluss loan  locator loan status.
        /// </summary>
        /// <param name="LLId">Gets a llid.</param>
        /// <param name="updateStatus">Gets a update status flaq.</param>
        public void UpdateLoanLocatorLoanStatus(Int64 LLId, int updateStatus)
        {
            string mSQL = "UPDATE loc_loan_app_info SET LOAN_STATUS_ID=" + updateStatus + ",APPRV_DATE='" + DateTime.Now.ToString("yyyy-MM-dd") + "' WHERE LOAN_APPL_ID=" + LLId;
            DbQueryManager.ExecuteNonQuery(mSQL);
        }

        public void UpdateLoanLocatorLoanStatus(Int64 LLId, int updateStatus,double approveLoanAmount)
        {
            string mSQL = "UPDATE loc_loan_app_info SET LOAN_STATUS_ID=" + updateStatus + ", LOAN_APRV_AMOU=" + approveLoanAmount + " ,APPRV_DATE='" + DateTime.Now.ToString("yyyy-MM-dd") + "' WHERE LOAN_APPL_ID=" + LLId;
            DbQueryManager.ExecuteNonQuery(mSQL);
        }
        /// <summary>
        /// This method provide the limit amount.
        /// </summary>
        /// <param name="UserId">Gets a user id.</param>
        /// <param name="Level">Gets a level.</param>
        /// <param name="LoanTag">Gets a loan tag.</param>
        /// <returns>Returns a limit amount value.</returns>
        public double GetLimitAmount(Int32 UserId, string Level, string LoanTag)
        {
            double returnValue=0;
            string selectFieldName="";
            if (Level.Trim().Length > 0 && LoanTag.Trim().Length>0)
            {
                switch (Level.ToUpper().Trim().ToString())
                {
                    case "L1":
                        selectFieldName = "LEVEL1_AMT";
                        break;
                    case "L2":
                        selectFieldName = "LEVEL2_AMT";
                        break;
                    default:
                        selectFieldName = "LEVEL3_AMT";
                        break;
                }
                string mSQL = "SELECT " + selectFieldName + " FROM t_pluss_userapproverlimit WHERE USER_ID=" + UserId + " ADN PROD_TAG='" + LoanTag.Trim().ToUpper() + "'";
                returnValue = Convert.ToDouble("0" + dbMySQL.DbGetValue(mSQL));
            }
            return returnValue;
        }
        public double GetCustomerTotalExistingLoanAmount(Int64 lLId)
        {
            double returnValue = 0;
            string mSQL = "SELECT SUM(CUDE_OUTSTANDING) as TotalExistingLoan FROM t_pluss_customerdetails WHERE CUDE_CUSTOMER_LLID=" + lLId + " GROUP BY CUDE_CUSTOMER_LLID";
            returnValue = Convert.ToDouble("0" + DbQueryManager.ExecuteScalar(mSQL));            
            return returnValue;
        }
    }
}
