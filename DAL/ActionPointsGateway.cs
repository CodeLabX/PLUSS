﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// Action point gateway class.
    /// </summary>
    public class ActionPointsGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;

        /// <summary>
        /// Consturctor.
        /// </summary>
        public ActionPointsGateway()
        {
            dbMySQL = new DatabaseConnection();
        }

        /// <summary>
        /// This method populate all action points.
        /// </summary>
        /// <returns>Returns action points list object.</returns>
        public List<ActionPoints> SelectActionPointsList()
        {
            List<ActionPoints> actionPointsObjList=new List<ActionPoints>();
            ActionPoints actionPointsObj = null;
            string mSQL = "SELECT * FROM t_pluss_actionpoint";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    actionPointsObj = new ActionPoints();
                    actionPointsObj.Id = Convert.ToInt64(xRs["ACPO_ID"]);
                    actionPointsObj.ColorName = Convert.ToString(xRs["ACPO_COLOR"]);
                    actionPointsObj.NewSourcing = Convert.ToString(xRs["ACPO_NEWSOURCING"]);
                    actionPointsObj.IndustryProfitMargin =
                        Convert.ToString(xRs["ACPO_INDUSTORYPROFITMARGIN"]);
                    actionPointsObj.Deviation = Convert.ToString(xRs["ACPO_DEVIATION"]);
                    actionPointsObj.Topup = Convert.ToString(xRs["ACPO_TOPUP"]);
                    actionPointsObj.BureauPolicy = Convert.ToString(xRs["ACPO_BUREAUPOLICY"]);
                    actionPointsObj.Status = Convert.ToInt32(xRs["ACPO_STATUS"]);
                    actionPointsObjList.Add(actionPointsObj);
                }
                xRs.Close();
            }
            return actionPointsObjList;
        }

        /// <summary>
        /// This method provide action point according ot search crieteria.
        /// </summary>
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns action points list object.</returns>
        public List<ActionPoints> SelectActionPointsList(string searchKey)
        {
            List<ActionPoints> actionPointsObjList = new List<ActionPoints>();
            ActionPoints actionPointsObj = null;
            string mSQL = "SELECT * FROM t_pluss_actionpoint WHERE ACPO_ID LIKE '%" + searchKey + "%' OR ACPO_COLOR LIKE '%" + searchKey + "%' OR ACPO_NEWSOURCING LIKE '%" + searchKey + "%' OR " +
                " ACPO_INDUSTORYPROFITMARGIN LIKE '%"+searchKey+"%' OR ACPO_DEVIATION LIKE '%"+searchKey+"%' OR ACPO_TOPUP LIKE '%"+searchKey+"%' OR ACPO_BUREAUPOLICY LIKE '%"+searchKey+"%'";
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    actionPointsObj = new ActionPoints();
                    actionPointsObj.Id = Convert.ToInt64(xRs["ACPO_ID"]);
                    actionPointsObj.ColorName = Convert.ToString(xRs["ACPO_COLOR"]);
                    actionPointsObj.NewSourcing = Convert.ToString(xRs["ACPO_NEWSOURCING"]);
                    actionPointsObj.IndustryProfitMargin =
                        Convert.ToString(xRs["ACPO_INDUSTORYPROFITMARGIN"]);
                    actionPointsObj.Deviation = Convert.ToString(xRs["ACPO_DEVIATION"]);
                    actionPointsObj.Topup = Convert.ToString(xRs["ACPO_TOPUP"]);
                    actionPointsObj.BureauPolicy = Convert.ToString(xRs["ACPO_BUREAUPOLICY"]);
                    actionPointsObj.Status = Convert.ToInt32(xRs["ACPO_STATUS"]);
                    actionPointsObjList.Add(actionPointsObj);
                }
                xRs.Close();
            }
            return actionPointsObjList;
        }

        /// <summary>
        /// This method provide action points information.
        /// </summary>
        /// <param name="colorId">Gets a color id.</param>
        /// <returns>Returns a action point object.</returns>
        public ActionPoints SelectActionPoints(Int64 colorId)
        {
            string mSQL = "SELECT * FROM t_pluss_actionpoint WHERE ACPO_ID="+colorId;
            ActionPoints actionPointsObj = new ActionPoints();
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                if (xRs.Read())
                {
                    actionPointsObj.Id = Convert.ToInt64(xRs["ACPO_ID"]);
                    actionPointsObj.ColorName = Convert.ToString(xRs["ACPO_COLOR"]);
                    actionPointsObj.NewSourcing = Convert.ToString(xRs["ACPO_NEWSOURCING"]);
                    actionPointsObj.IndustryProfitMargin =
                        Convert.ToString(xRs["ACPO_INDUSTORYPROFITMARGIN"]);
                    actionPointsObj.Deviation = Convert.ToString(xRs["ACPO_DEVIATION"]);
                    actionPointsObj.Topup = Convert.ToString(xRs["ACPO_TOPUP"]);
                    actionPointsObj.BureauPolicy = Convert.ToString(xRs["ACPO_BUREAUPOLICY"]);
                    actionPointsObj.Status = Convert.ToInt32(xRs["ACPO_STATUS"]);
                }
                xRs.Close();
            }
            return actionPointsObj;
        }

        /// <summary>
        /// This method provide action point against color.
        /// </summary>
        /// <param name="color">Gets a color.</param>
        /// <returns>Returns action points object.</returns>
        public ActionPoints SelectActionPoints(string color)
        {
            string mSQL = "SELECT * FROM t_pluss_actionpoint WHERE Upper(ACPO_COLOR)='" + color.ToUpper().Trim()+"'";
           // ADODB.Recordset xRs = new ADODB.Recordset();
            ActionPoints actionPointsObj = null;// new ActionPoints();
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    actionPointsObj = new ActionPoints();
                    actionPointsObj.Id = Convert.ToInt64(xRs["ACPO_ID"]);
                    actionPointsObj.ColorName = Convert.ToString(xRs["ACPO_COLOR"]);
                    actionPointsObj.NewSourcing = Convert.ToString(xRs["ACPO_NEWSOURCING"]);
                    actionPointsObj.IndustryProfitMargin =
                        Convert.ToString(xRs["ACPO_INDUSTORYPROFITMARGIN"]);
                    actionPointsObj.Deviation = Convert.ToString(xRs["ACPO_DEVIATION"]);
                    actionPointsObj.Topup = Convert.ToString(xRs["ACPO_TOPUP"]);
                    actionPointsObj.BureauPolicy = Convert.ToString(xRs["ACPO_BUREAUPOLICY"]);
                    actionPointsObj.Status = Convert.ToInt32(xRs["ACPO_STATUS"]);
                }
                xRs.Close();
            }
            return actionPointsObj;
        }

        /// <summary>
        /// This method provide insertion or update information.
        /// </summary>
        /// <param name="actionPointsObj">Gets action point object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendActionPointsInToDB(ActionPoints actionPointsObj)
        {
            ActionPoints existingActionPointsObj = SelectActionPoints(actionPointsObj.Id);
            if (existingActionPointsObj.Id > 0)
            {
                return UpdateActionPoints(actionPointsObj);
            }
            else
            {
                return InsertActionPoints(actionPointsObj);
            }
        }

        /// <summary>
        /// This method provide insertion of the information.
        /// </summary>
        /// <param name="actionPointsObj">Gets action point object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertActionPoints(ActionPoints actionPointsObj)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_actionpoint (ACPO_COLOR,ACPO_NEWSOURCING,ACPO_INDUSTORYPROFITMARGIN,ACPO_DEVIATION,ACPO_TOPUP,ACPO_BUREAUPOLICY,ACPO_STATUS) VALUES('" +
                              AddSlash(actionPointsObj.ColorName) + "','" +
                              actionPointsObj.NewSourcing + "','" +
                              actionPointsObj.IndustryProfitMargin + "','" +
                              actionPointsObj.Deviation + "','" +
                              actionPointsObj.Topup + "','" +
                              actionPointsObj.BureauPolicy + "'," +
                              actionPointsObj.Status + ")";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        /// <summary>
        /// This method provide update information.
        /// </summary>
        /// <param name="actionPointsObj">Gets a action point object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateActionPoints(ActionPoints actionPointsObj)
        {
            try
            {
                string mSQL = "UPDATE t_pluss_actionpoint SET " +
                              "ACPO_COLOR='" + AddSlash(actionPointsObj.ColorName) + "'," +
                              "ACPO_NEWSOURCING='" + AddSlash(actionPointsObj.NewSourcing) + "'," +
                              "ACPO_INDUSTORYPROFITMARGIN='" + AddSlash(actionPointsObj.IndustryProfitMargin) + "'," +
                              "ACPO_DEVIATION='" + AddSlash(actionPointsObj.Deviation) + "'," +
                              "ACPO_TOPUP='" + AddSlash(actionPointsObj.Topup) + "'," +
                              "ACPO_BUREAUPOLICY='" + AddSlash(actionPointsObj.BureauPolicy) + "'," +
                              "ACPO_STATUS=" + actionPointsObj.Status + " WHERE ACPO_ID=" + actionPointsObj.Id;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method provide max action point id.
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public Int64 SelectMaxActionPointsId()
        {
            string mSQL = "SELECT MAX(ACPO_ID) FROM t_pluss_actionpoint";
            Int64 returnValue = Convert.ToInt64("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue + 1;
        }
        /// <summary>
        /// This method provide adding the special character.
        /// </summary>
        /// <param name="sMessage">Gets a message.</param>
        /// <returns>Returns a coverted string.</returns>
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
