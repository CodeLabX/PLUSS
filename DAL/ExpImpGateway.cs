﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Bat.Common;
using BusinessEntities;

namespace DAL
{
    public class ExpImpGateway
    {
        public string check4LoandProcessStatusData(string loanLocatorId, string loanStatus, string departmentId)
        {
            var sSQL = "select LOAN_APPL_ID from loc_loan_app_info where LOAN_APPL_ID=" + loanLocatorId +
                       " and LOAN_STATUS_ID in (" + loanStatus + ") and DELI_DEPT_ID in (" + departmentId + ")";
            var result = DbQueryManager.ExecuteScalar(sSQL);
            return result.ToString();
        }

        public string check4LoandProcessStatusData2(string loanLocatorId, string loanStatus, string departmentId)
        {
            var sSQL = "select LOAN_APPL_ID from loc_loan_app_info where LOAN_APPL_ID=" + loanLocatorId +
                       " and LOAN_STATUS_ID not in (" + loanStatus + ") and DELI_DEPT_ID in (" + departmentId + ")";
            var result = DbQueryManager.ExecuteScalar(sSQL);
            return result.ToString();
        }


        public string check4CIBProcessStatusData(string loanLocatorId, string loanStatus, string departmentId)
        {
            var sSQL = "select top 1 LOAN_APPL_ID from loan_app_details where LOAN_APPL_ID=" + loanLocatorId +
                       " and PROCESS_STATUS_ID ='19' and REMARKS like'Report Received%' order by ID desc";
            var result = DbQueryManager.ExecuteScalar(sSQL);
            return result.ToString();
        }

        public DateTime start_time(string id)
        {
            var sql_d = "select top 1 MAX(DATE_TIME) as DATE_TIME from loan_app_details where LOAN_APPL_ID=" + id + "";
            var result = Convert.ToDateTime(DbQueryManager.ExecuteScalar(sql_d));
            return result;
        }

        static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }
        public ExportImport update_data(DataTable csvarray, int personId, int depId)
         {
            //var depId=4; //$_POST['hid_deptid'];
            //var personId = 107; //$_POST['hid_personId'];


            int loan_status = 19;
            DataRow[] status;
            int llidIndex = 0;
            int processIndex = 1;
            int statusIndex = 2;
            var dateTimeIndex = 3;
            int remarksIndex = 4;
            int amountIndex = 5;
            int levelIndex = 6;
            int tenureIndex = 7;
            var makerId = 0;
            var loaninfo_departmentId = 0;
            var colSpan = 5;
            DateTime dateTime;
            string loanLocatorId = "";
            var remarks="";
            int ApprovedAmount = 0;
            var Level = 0;
            var Tenure = 0;
            DateTime DATE_TIME;
            var strSQL = "";
            var loanStatus2 = "";
            var ckSQL = "";
            DataTable rowCKDeptId;
            var mSQL="";
            DataTable res;
            int result = 0;
            ExportImport export = new ExportImport();
            try
            {
                var str_sql1 = "select distinct replace(Upper(RTRIM(Ltrim(LOAN_STATUS_NAME))),' ','') as" +
                                   " LOAN_STATUS_NAME,LOAN_STATUS_ID,LOAN_STATUS_TYPE from loan_status_info order " +
                                   "by LOAN_STATUS_ID DESC";
                res = DbQueryManager.GetTable(str_sql1);
                DataTable loanStatusArray = new DataTable();
                loanStatusArray.Columns.Add("LOAN_STATUS_ID");
                loanStatusArray.Columns.Add("LOAN_STATUS_NAME");


                for (int i = 0; i < res.Rows.Count; i++)
                {
                    int id = (int)res.Rows[i]["LOAN_STATUS_ID"];
                    string name = res.Rows[i]["LOAN_STATUS_NAME"].ToString();

                    loanStatusArray.Rows.Add();
                    loanStatusArray.Rows[i]["LOAN_STATUS_ID"] = id;
                    loanStatusArray.Rows[i]["LOAN_STATUS_NAME"] = name;
                }
                for (int rowIndex = 1; rowIndex < csvarray.Rows.Count; rowIndex++)
                {
                    int LLId = Convert.ToInt32(csvarray.Rows[rowIndex][llidIndex]);
                    loanLocatorId = csvarray.Rows[rowIndex][llidIndex].ToString();

                    loanStatusArray.Rows[rowIndex][statusIndex - 1] = csvarray.Rows[rowIndex][statusIndex];
                    status =
                        loanStatusArray.Select("LOAN_STATUS_NAME like '%" + loanStatusArray.Rows[rowIndex][statusIndex - 1] +
                                               "%'");
                    loan_status = Convert.ToInt32(status[rowIndex].ItemArray[rowIndex - 1]);
                    if (!String.IsNullOrEmpty(csvarray.Rows[rowIndex][dateTimeIndex].ToString()))
                    {
                        dateTime = Convert.ToDateTime(csvarray.Rows[rowIndex][dateTimeIndex]);
                    }
                    else
                    {

                        dateTime = DateTime.Now;
                    }
                    remarks = HttpContext.Current.Server.HtmlEncode(csvarray.Rows[rowIndex][remarksIndex].ToString());
                    if (!Convert.IsDBNull(csvarray.Rows[rowIndex][amountIndex]))
                    {
                        ApprovedAmount = Convert.ToInt32(csvarray.Rows[rowIndex][amountIndex]);
                    }
                    else
                    {
                        ApprovedAmount = 0;
                    }
                    if (!String.IsNullOrEmpty(csvarray.Rows[rowIndex][levelIndex].ToString()))
                    {
                        Level = Convert.ToInt32(csvarray.Rows[rowIndex][levelIndex]);
                    }
                    else
                    {
                        Level = 0;
                    }
                    if (!String.IsNullOrEmpty(csvarray.Rows[rowIndex][tenureIndex].ToString()))
                    {
                        Tenure = Convert.ToInt32(csvarray.Rows[rowIndex][tenureIndex]);
                    }
                    else
                    {
                        Tenure = 0;
                    }
                    DATE_TIME = start_time(loanLocatorId);
                    string checkResult = "";
                    switch (loan_status)
                    {
                        case 1: //Initiated
                            loaninfo_departmentId = 1;
                            string loanStatus = "1";
                            string departmentId = "4,5";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 2: //Declined

                            strSQL = "select ORIG_DEPT_ID from loc_loan_app_info where LOAN_APPL_ID='" + loanLocatorId + "'";
                            var rowdeptid = DbQueryManager.GetTable(strSQL);
                            loaninfo_departmentId = (int)rowdeptid.Rows[0]["ORIG_DEPT_ID"];
                            loanStatus = "4";
                            departmentId = "4";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 3: //To source
                            strSQL = "select ORIG_DEPT_ID from loan_app_info where LOAN_APPL_ID='" + loanLocatorId + "'";
                            rowdeptid = DbQueryManager.GetTable(strSQL);
                            //rowdeptid = mysql_fetch_object(resultdeptid);
                            loaninfo_departmentId = (int)rowdeptid.Rows[0]["ORIG_DEPT_ID"];
                            loanStatus = "4";
                            departmentId = "4,5";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 4: //In Process
                            loaninfo_departmentId = depId;
                            loanStatus = "8,9,10,11,12,15,17";
                            departmentId = "4,5";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 5: //Approved
                            loaninfo_departmentId = 4;
                            loanStatus = "4";
                            departmentId = "4";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 6://Deferred
                            loaninfo_departmentId = 4;
                            loanStatus = "4";
                            departmentId = "4";
                            loanStatus2 = "2,5";
                            checkResult = check4LoandProcessStatusData2(loanLocatorId, loanStatus2, departmentId);
                            break;
                        case 7: //Disbursed
                            loaninfo_departmentId = 5;
                            makerId = 1;
                            loanStatus = "4";
                            departmentId = "5";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 8: //To Credit
                            loaninfo_departmentId = 4;
                            makerId = 0;
                            loanStatus = "4";
                            departmentId = "5";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 9: //Forward for Verification
                            loanStatus = "0";
                            departmentId = "0";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 10: //To Loan Ops.
                            loaninfo_departmentId = 5;
                            loanStatus = "4,5";
                            departmentId = "4";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 11: //To Loan Admin-Loan Ops.
                            loaninfo_departmentId = 5;
                            loanStatus = "4";
                            departmentId = "4";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 12: //To Credit
                            loaninfo_departmentId = 4;
                            makerId = 0;
                            loanStatus = "4";
                            departmentId = "5";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 13: //From Asset Opps-Docs  To Loan Ops.
                            loaninfo_departmentId = 1;
                            loanStatus = "4";
                            departmentId = "4,5";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 14: //To Other VC
                            loanStatus = "0";
                            departmentId = "0";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 15: //Conditionally Approved
                            strSQL = "select ORIG_DEPT_ID from loan_app_info where LOAN_APPL_ID='" + loanLocatorId + "'";
                            rowdeptid = DbQueryManager.GetTable(strSQL);
                            //rowdeptid = mysql_fetch_object(resultdeptid);
                            loaninfo_departmentId = (int)rowdeptid.Rows[0]["ORIG_DEPT_ID"];
                            loanStatus = "4";
                            departmentId = "4";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 16: //Forward for Valuation
                            loanStatus = "0";
                            departmentId = "0";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 17: //On Hold
                            loaninfo_departmentId = depId;
                            loanStatus = "4";
                            departmentId = "4,5";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 18: //Forward for Legal
                            departmentId = "0";
                            loanStatus = "0";
                            checkResult = check4LoandProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        case 19: //CIB Status
                            departmentId = "5";
                            loanStatus = "0";
                            checkResult = check4CIBProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;
                        default:
                            departmentId = "5";
                            loanStatus = "0";
                            checkResult = check4CIBProcessStatusData(loanLocatorId, loanStatus, departmentId);
                            break;

                    }
                    ckSQL = "select DELI_DEPT_ID from loc_loan_app_info where LOAN_APPL_ID=" + loanLocatorId + "";
                    rowCKDeptId = DbQueryManager.GetTable(ckSQL);
                    int checkDeptId = (int)rowCKDeptId.Rows[0]["DELI_DEPT_ID"];
                    result = 0;
                    if (checkResult.Length > 0)
                    {
                        var dSQL = String.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT) values
                        (" + LLId + "," + personId + "," + depId + ",'" + dateTime + "'," + loan_status + ",'" + remarks + "',datediff(day,'" + dateTime + "','" + DATE_TIME + "'))");
                        if (loan_status != 19)
                        {
                            if (loan_status == 5 || loan_status == 15)
                            {
                                if (!String.IsNullOrEmpty(ApprovedAmount.ToString()))
                                {
                                    ApprovedAmount = 0;
                                }
                                if (Level != null)
                                {
                                    Level = 0;
                                }
                                if (Tenure != null)
                                {
                                    Tenure = 0;
                                }
                                mSQL = "update loc_loan_app_info set LOAN_STATUS_ID='" + loan_status + "',APPRV_LEVEL=" + Level + ", " +
                                       "DELI_DEPT_ID=" + loaninfo_departmentId + ",LOAN_APRV_AMOU=" + ApprovedAmount + ",LOAN_DECI_STATUS_ID=5," +
                                       "APPRV_DATE='" + dateTime + "', TENOR=" + Tenure + " where LOAN_APPL_ID=" + LLId + "";
                                //colSpan = 8;
                            }
                            else
                            {
                                mSQL =
                                        "update loc_loan_app_info set LOAN_STATUS_ID='" + loan_status + "', DELI_DEPT_ID=" + loaninfo_departmentId + ", MAKER=" + makerId + " where LOAN_APPL_ID=" + LLId + "";
                                //colSpan = 5;
                            }
                            if (checkDeptId != null)
                            {
                                if (checkDeptId == depId)
                                {
                                    if (loan_status != 6)
                                    {
                                        result = DbQueryManager.ExecuteNonQuery(mSQL);
                                    }
                                }
                                //return result;
                            }
                        }
                        if (loan_status != 19)
                        {
                            if (checkDeptId == depId)
                            {
                                result = DbQueryManager.ExecuteNonQuery(dSQL);
                            }
                            else
                            {
                                dSQL = "";
                                result = 0;
                            }
                        }
                        else
                        {
                            result = DbQueryManager.ExecuteNonQuery(dSQL);
                        }
                    }
                }
               
                export.Result = result;
                export.Status = loan_status;
                return export;
            }
            catch (Exception)
            {
                
                return export;
            }
        }
    }

    public class ExportImport
    {
        public int Result { get; set; }
        public int Status { get; set; }
    }
}
