﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{

    public class BranchInformationGateway
    {
        private DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;

        public BranchInformationGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        public List<LocationInformation> GetAllLocationInformation()
        {
            List<LocationInformation> locationInformationListObj = new List<LocationInformation>();
            string queryString = "SELECT LOCA_ID, LOCA_NAME FROM t_pluss_location ORDER BY LOCA_NAME";
            //ADODB.Recordset xRs = new ADODB.Recordset();
            //xRs = dbMySQL.DbOpenRset(queryString);
            var data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    LocationInformation locationInformationObj = new LocationInformation();
                    locationInformationObj.Id = Convert.ToInt32(xRs["LOCA_ID"]);
                    locationInformationObj.Name = Convert.ToString(xRs["LOCA_NAME"]);
                    locationInformationListObj.Add(locationInformationObj);
                }
                xRs.Close();
            }
            return locationInformationListObj;
        }

        public List<BranchInformation> GetAllBranchInformation()
        {
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            string queryString = "SELECT BRAN_ID, BRAN_NAME, isnull(BRAN_BANK_ID, 0) AS BRAN_BANK_ID, BNK.BANK_NM, isnull(BRAN_LOCATION_ID,0) AS BRAN_LOCATION_ID, LOC.LOCA_NAME, BRAN_ADDRESS, isnull(BRAN_STATUS, 0) AS BRAN_STATUS, isnull((CASE BRAN_STATUS WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' END), 'Inactive') AS  BRAN_STATUS_MSG,BRAN_BRABCH_STSTUS, CASE BRAN_BRABCH_STSTUS WHEN 0 THEN 'Offline clear' WHEN 1 THEN 'Online clear' WHEN 2 THEN 'Offline not clear' END AS CLEARING_MSG FROM t_pluss_branch AS BNH LEFT JOIN t_pluss_bank AS BNK ON BNH.BRAN_BANK_ID = BNK.BANK_ID LEFT JOIN t_pluss_location AS LOC ON BNH.BRAN_LOCATION_ID = LOC.LOCA_ID  ORDER BY BANK_NM ASC";

            //ADODB.Recordset xRs = new ADODB.Recordset();
            //xRs = dbMySQL.DbOpenRset(queryString);
             var data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    BranchInformation branchInformationObj = new BranchInformation();
                    branchInformationObj.Id = Convert.ToInt32(xRs["BRAN_ID"]);
                    branchInformationObj.Name = Convert.ToString(xRs["BRAN_NAME"]);
                    branchInformationObj.Bank.Id = Convert.ToInt32(xRs["BRAN_BANK_ID"]);
                    branchInformationObj.Bank.Name = Convert.ToString(xRs["BANK_NM"]);
                    branchInformationObj.Location.Id = Convert.ToInt32(xRs["BRAN_LOCATION_ID"]);
                    branchInformationObj.Location.Name = Convert.ToString(xRs["LOCA_NAME"]);
                    branchInformationObj.Address = Convert.ToString(xRs["BRAN_ADDRESS"]);
                    branchInformationObj.Status = Convert.ToInt32(xRs["BRAN_STATUS"]);
                    branchInformationObj.StatusMsg = Convert.ToString(xRs["BRAN_STATUS_MSG"]);
                    branchInformationObj.ClearingStatus = Convert.ToInt32(xRs["BRAN_BRABCH_STSTUS"]);
                    branchInformationObj.ClearingStatusMsg = Convert.ToString(xRs["CLEARING_MSG"]);
                    branchInformationListObj.Add(branchInformationObj);

                }
                xRs.Close();
            }
            return branchInformationListObj;
        }

        public List<BranchInformation> GetAllBranchInformation(string searchKey)
        {
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            string queryString = "SELECT BRAN_ID, BRAN_NAME, isnull(BRAN_BANK_ID, 0) AS BRAN_BANK_ID, BNK.BANK_NM, isnull(BRAN_LOCATION_ID,0) AS BRAN_LOCATION_ID, LOC.LOCA_NAME, BRAN_ADDRESS, isnull(BRAN_STATUS, 0) AS BRAN_STATUS, isnull((CASE BRAN_STATUS WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' END), 'Inactive') AS  BRAN_STATUS_MSG,BRAN_BRABCH_STSTUS, " +
                                 "CASE BRAN_BRABCH_STSTUS WHEN 0 THEN 'Offline clear' WHEN 1 THEN 'Online clear' WHEN 2 THEN 'Offline not clear' END AS CLEARING_MSG " +
                                 "FROM t_pluss_branch AS BNH " +
                                 "LEFT JOIN t_pluss_bank AS BNK ON BNH.BRAN_BANK_ID = BNK.BANK_ID " +
                                 "LEFT JOIN t_pluss_location AS LOC ON BNH.BRAN_LOCATION_ID = LOC.LOCA_ID " +
                                 "WHERE UPPER(BRAN_ID) LIKE(UPPER('%"+ searchKey +"%'))  OR UPPER(BRAN_NAME) LIKE(UPPER('%"+ searchKey +"%')) OR UPPER(BRAN_BANK_ID) LIKE(UPPER('%"+ searchKey +"%')) OR " +
                                 "UPPER(BNK.BANK_NM) LIKE(UPPER('%" + searchKey + "%')) OR UPPER(BRAN_LOCATION_ID) LIKE(UPPER('%" + searchKey + "%')) OR UPPER(LOC.LOCA_NAME) LIKE(UPPER('%" + searchKey + "%')) OR UPPER(BRAN_ADDRESS) LIKE(UPPER('%" + searchKey + "%')) OR " +
                                 "UPPER(BRAN_STATUS) LIKE(UPPER('%" + searchKey + "%'))  OR UPPER(BRAN_BRABCH_STSTUS) LIKE(UPPER('%" + searchKey + "%')) ORDER BY BANK_NM ASC";

            var data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    BranchInformation branchInformationObj = new BranchInformation();
                    branchInformationObj.Id = Convert.ToInt32(xRs["BRAN_ID"]);
                    branchInformationObj.Name = Convert.ToString(xRs["BRAN_NAME"]);
                    branchInformationObj.Bank.Id = Convert.ToInt32(xRs["BRAN_BANK_ID"]);
                    branchInformationObj.Bank.Name = Convert.ToString(xRs["BANK_NM"]);
                    branchInformationObj.Location.Id = Convert.ToInt32(xRs["BRAN_LOCATION_ID"]);
                    branchInformationObj.Location.Name = Convert.ToString(xRs["LOCA_NAME"]);
                    branchInformationObj.Address = Convert.ToString(xRs["BRAN_ADDRESS"]);
                    branchInformationObj.Status = Convert.ToInt32(xRs["BRAN_STATUS"]);
                    branchInformationObj.StatusMsg = Convert.ToString(xRs["BRAN_STATUS_MSG"]);
                    branchInformationObj.ClearingStatus = Convert.ToInt32(xRs["BRAN_BRABCH_STSTUS"]);
                    branchInformationObj.ClearingStatusMsg = Convert.ToString(xRs["CLEARING_MSG"]);
                    branchInformationListObj.Add(branchInformationObj);

                }
                xRs.Close();
            }
            return branchInformationListObj;
        }
        public BranchInformation GetExistingBranchInformation(int branchId)
        {
            BranchInformation branchInformationObj = null;
            string queryString = "SELECT BRAN_ID, BRAN_NAME, isnull(BRAN_BANK_ID, 0) AS BRAN_BANK_ID, BNK.BANK_NM, isnull(BRAN_LOCATION_ID,0) AS BRAN_LOCATION_ID, LOC.LOCA_NAME, BRAN_ADDRESS, isnull(BRAN_STATUS, 0) AS BRAN_STATUS, isnull((CASE BRAN_STATUS WHEN 0 THEN 'Inactive' WHEN 1 THEN 'Active' END), 'Inactive') AS  BRAN_STATUS_MSG,BRAN_BRABCH_STSTUS, " +
                                 "CASE BRAN_BRABCH_STSTUS WHEN 0 THEN 'Offline clear' WHEN 1 THEN 'Online clear' WHEN 2 THEN 'Offline not clear' END AS CLEARING_MSG " +
                                 "FROM t_pluss_branch AS BNH " +
                                 "LEFT JOIN t_pluss_bank AS BNK ON BNH.BRAN_BANK_ID = BNK.BANK_ID " +
                                 "LEFT JOIN t_pluss_location AS LOC ON BNH.BRAN_LOCATION_ID = LOC.LOCA_ID " +
                                 "WHERE BRAN_ID=" + branchId;

           var data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                if (xRs.Read())
                {
                    branchInformationObj = new BranchInformation();
                    branchInformationObj.Id = Convert.ToInt32(xRs["BRAN_ID"]);
                    branchInformationObj.Name = Convert.ToString(xRs["BRAN_NAME"]);
                    branchInformationObj.Bank.Id = Convert.ToInt32(xRs["BRAN_BANK_ID"]);
                    branchInformationObj.Bank.Name = Convert.ToString(xRs["BANK_NM"]);
                    branchInformationObj.Location.Id = Convert.ToInt32(xRs["BRAN_LOCATION_ID"]);
                    branchInformationObj.Location.Name = Convert.ToString(xRs["LOCA_NAME"]);
                    branchInformationObj.Address = Convert.ToString(xRs["BRAN_ADDRESS"]);
                    branchInformationObj.Status = Convert.ToInt32(xRs["BRAN_STATUS"]);
                    branchInformationObj.StatusMsg = Convert.ToString(xRs["BRAN_STATUS_MSG"]);
                    branchInformationObj.ClearingStatus = Convert.ToInt32(xRs["BRAN_BRABCH_STSTUS"]);
                    branchInformationObj.ClearingStatusMsg = Convert.ToString(xRs["CLEARING_MSG"]);
                }
                xRs.Close();
            }
            return branchInformationObj;
        }  
        public int InsertORUpdateBranchInfo(List<BranchInformation> branchInformationListObj)
        {
            int insertORUpdateValue = -1;
            if (branchInformationListObj.Count > 0)
            {
                foreach (BranchInformation branchInformationObj in branchInformationListObj)
                {
                    if (branchInformationObj.Id > 0)
                    {
                        BranchInformation existiongBranchInformationObj = GetExistingBranchInformation(branchInformationObj.Id);
                        if (existiongBranchInformationObj != null)
                        {
                            branchInformationObj.Id = existiongBranchInformationObj.Id;
                            insertORUpdateValue = UpdateBranchInformation(branchInformationObj);
                        }
                        else
                        {
                            insertORUpdateValue = InsertBranchInformation(branchInformationObj);
                        }
                    }
                    else
                    {
                        Int32 branchId = IsExistsBranchInfo(branchInformationObj.Name.Trim(), Convert.ToInt32(branchInformationObj.Bank.Id), branchInformationObj.Location.Id);
                        if (branchId == 0)
                        {
                            insertORUpdateValue = InsertBranchInformation(branchInformationObj);
                        }
                        else
                        {
                            branchInformationObj.Id = branchId;
                            insertORUpdateValue = UpdateBranchInformation(branchInformationObj);
                        }
                    }

                }
            }

            return insertORUpdateValue;
        }
        private int InsertBranchInformation(BranchInformation branchInfoObj)
        {
            string queryString = "INSERT INTO t_pluss_branch (BRAN_NAME, BRAN_BANK_ID, BRAN_LOCATION_ID, BRAN_ADDRESS, BRAN_STATUS, BRAN_BRABCH_STSTUS ) VALUES ";
            queryString += "('" + EscapeCharacter(branchInfoObj.Name) + "'," + branchInfoObj.Bank.Id + "," + branchInfoObj.Location.Id + ",'" + EscapeCharacter(branchInfoObj.Address) + "'," + branchInfoObj.Status + "," + branchInfoObj.ClearingStatus + ")";
            try
            {
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch
            {
                return ERROR;
            }
        }
        private int UpdateBranchInformation(BranchInformation branchInformationObj)
        {
            string queryString = "";
            queryString = String.Format("UPDATE t_pluss_branch SET " +
            "BRAN_NAME = '{0}' , BRAN_BANK_ID = {1}, BRAN_LOCATION_ID = {2}, BRAN_ADDRESS = '{3}', BRAN_STATUS = {4}, BRAN_BRABCH_STSTUS = {5} " +
            "WHERE BRAN_ID = {6} ",
            EscapeCharacter(branchInformationObj.Name), branchInformationObj.Bank.Id, branchInformationObj.Location.Id,
            EscapeCharacter(branchInformationObj.Address), branchInformationObj.Status, branchInformationObj.ClearingStatus,
            branchInformationObj.Id);
            try
            {
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch
            {
                return ERROR;
            }
        }

        private bool IsExistsBranchInfo(Int32 branchId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_branch WHERE BRAN_ID = {0}", branchId);
            try
            {
                Int32 branchCount = Convert.ToInt32("0" + dbMySQL.DbGetValue(queryString));
                if (branchCount > 0)
                {
                    returnValue = true;
                }
                else
                {
                    returnValue = false;
                }
            }
            catch
            {
                returnValue = false;
            }
            return returnValue;
        }

        private Int32 IsExistsBranchInfo(string branchName, Int32 bankId, int locationId)
        {
            Int32 returnValue = 0;
            string queryString = String.Format("SELECT BRAN_ID FROM t_pluss_branch WHERE TRIM(REPLACE(UPPER(REPLACE(BRAN_NAME,'''','')),' ','')) = TRIM(REPLACE(UPPER('{0}'),' ','')) AND BRAN_BANK_ID = {1} AND BRAN_LOCATION_ID={2} ", branchName.ToUpper().Replace("'", "").Replace(" ", "").Trim(), bankId, locationId);
            Int32 branchId = 0;
            try
            {
                branchId = Convert.ToInt32("0" + dbMySQL.DbGetValue(queryString));
                if (branchId > 0)
                {
                    returnValue = branchId;
                }
            }
            catch
            {
                returnValue = branchId;
            }
            return returnValue;
        }
        public Int32 GetBankId(string bankName)
        {
            Int32 bankId = 0;
            try
            {
                string queryString = String.Format("SELECT BANK_ID, REPLACE(UPPER(TRIM(BANK_NM)), ' ', '') AS BANK_NM  FROM t_pluss_bank WHERE REPLACE(UPPER(TRIM(BANK_NM)), ' ', '') = REPLACE(UPPER(TRIM('{0}')),' ', '') ", bankName);
                bankId = Convert.ToInt32("0" + dbMySQL.DbGetValue(queryString));
            }
            catch
            {
                bankId = 0;
            }
            return bankId;
        }
        public Int32 GetLocationId(string locationName)
        {
            Int32 locationId = 0;
            try
            {
                string queryString = String.Format("SELECT LOCA_ID, REPLACE(UPPER(TRIM(LOCA_NAME)), ' ', '') AS LOCA_NAME  FROM t_pluss_location WHERE REPLACE(UPPER(TRIM(LOCA_NAME)), ' ', '') = REPLACE(UPPER(TRIM('{0}')),' ', '') ", locationName);
                locationId = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(queryString));
            }
            catch
            {
                locationId = 0;
            }
            return locationId;
        }
        public Int32 InsertLocationName(string locationName)
        {
            string queryString = String.Format("INSERT INTO t_pluss_location (LOCA_NAME) VALUES('{0}') "
                                                , EscapeCharacter(locationName));


            try
            {
                if (DbQueryManager.ExecuteNonQuery(queryString) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch
            {
                return ERROR;
            }

        }
        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                returnMessage = fieldString;
            }
            return returnMessage;
        }


        public List<BranchInformation> GetAllBranchInfoByBankId(int bankId)
        {
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            string queryString = "select BRAN_ID, BRAN_NAME, REGI_NM, BRAN_BANK_ID, " +
                "BRAN_LOCATION_ID, BRAN_ADDRESS, BRAN_BRABCH_STSTUS, " +
                "BRAN_STATUS from t_pluss_branch where BRAN_BANK_ID = " + bankId;

            ADODB.Recordset xRs = new ADODB.Recordset();
            xRs = dbMySQL.DbOpenRset(queryString);
            while (!xRs.EOF)
            {
                BranchInformation branchInformationObj = new BranchInformation();
                branchInformationObj.Id = Convert.ToInt32(xRs.Fields["BRAN_ID"].Value);
                branchInformationObj.Name = Convert.ToString(xRs.Fields["BRAN_NAME"].Value);
                branchInformationObj.Bank.Id = Convert.ToInt32(xRs.Fields["BRAN_BANK_ID"].Value);
                branchInformationObj.Address = Convert.ToString(xRs.Fields["BRAN_ADDRESS"].Value);
                branchInformationObj.Status = Convert.ToInt32(xRs.Fields["BRAN_STATUS"].Value);
                branchInformationListObj.Add(branchInformationObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return branchInformationListObj;
        }


        public List<BranchInformation> GetAllBankBranchInfo()
        {
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            string queryString = "select* from t_pluss_bank inner join t_pluss_branch on " +
                        "t_pluss_bank.BANK_ID=t_pluss_branch.BRAN_BANK_ID " +
                        "order by t_pluss_bank.BANK_ID";

            ADODB.Recordset xRs = new ADODB.Recordset();
            xRs = dbMySQL.DbOpenRset(queryString);
            while (!xRs.EOF)
            {
                BranchInformation branchInformationObj = new BranchInformation();
                branchInformationObj.Id = Convert.ToInt32(xRs.Fields["BRAN_ID"].Value);
                branchInformationObj.Name = Convert.ToString(xRs.Fields["BRAN_NAME"].Value);
                branchInformationObj.Bank.Id = Convert.ToInt32(xRs.Fields["BRAN_BANK_ID"].Value);
                branchInformationObj.Bank.Name = Convert.ToString(xRs.Fields["BANK_NM"].Value);
                branchInformationObj.Address = Convert.ToString(xRs.Fields["BRAN_ADDRESS"].Value);
                branchInformationObj.Status = Convert.ToInt32(xRs.Fields["BRAN_STATUS"].Value);
                branchInformationListObj.Add(branchInformationObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return branchInformationListObj;
        }


        public bool IsExistsBranchName(string branchName, int branchId, int bankId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_branch WHERE UPPER(REPLACE(TRIM(BRAN_NAME),' ', '')) = UPPER(REPLACE(TRIM('{0}'),' ','')) AND BRAN_BANK_ID = {1} AND BRAN_ID != {2}", EscapeCharacter(branchName),bankId, branchId);
            try
            {
                Int32 branch = Convert.ToInt32("0"+ DbQueryManager.GetTable(queryString).Rows.Count);
                if (branch > 0)
                {
                    returnValue = true;
                }
                else
                {
                    returnValue = false;
                }
            }
            catch
            {
                returnValue = false;
            }
            return returnValue;
        }

    }
}
