﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// CpvTemplateGateway Class.
    /// </summary>
    public class CpvTemplateGateway
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public CpvTemplateGateway()
        { }

        #region Private Members
        private List<CpvTemplate> tPlussCpvTemplateObjList = new List<CpvTemplate>();
        private CpvTemplate tPlussCpvTemplateObj = new CpvTemplate();
        #endregion


        #region Get All CpvTemplate
        /// <summary>
        /// Get all CpvTemplate from database
        /// </summary>
        /// <returns>Returns a CpvTemplate list object.</returns>
        public List<CpvTemplate> GetAllCpvTemplateInfo()
        {
            Bat.Common.Connection.ConnectionStringName = "conString";

            string queryString = "select CPVT_ID, CPVT_LLID, CPVT_DATE, CPVT_GUARANTORSNAME, " +
            "CPVT_CONTACTADDRESS, CPVT_TELEPHONENO, CPVT_CELLPHONE, " +
            "CPVT_GUARANTOREEAMOUNT, CPVT_RELATIONSHIPWITHAPPLICANT, " +
            "CPVT_EMAILADDRESS, CPVT_ENTRYDATE, CPVT_USER_ID, CPVT_DESIGNATION " +
            "from t_pluss_cpvtemplate";

            DataTable cpvTemplateDataTableObj = DbQueryManager.GetTable(queryString);
            if (cpvTemplateDataTableObj != null)
            {
                DataTableReader tPlussCpvtemplateReader = cpvTemplateDataTableObj.CreateDataReader();

                while (tPlussCpvtemplateReader.Read())
                {
                    tPlussCpvTemplateObj = new CpvTemplate();

                    tPlussCpvTemplateObj.CpvtID = Convert.ToInt32(tPlussCpvtemplateReader["Cpvt_ID"]); 
                    tPlussCpvTemplateObj.CpvtLlid = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Llid"]); 
                    tPlussCpvTemplateObj.CpvtDate = Convert.ToDateTime(tPlussCpvtemplateReader["Cpvt_Date"]);
                    tPlussCpvTemplateObj.CpvtGuarantorsname = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Guarantorsname"]);

                    tPlussCpvTemplateObj.CpvtContactaddress = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Contactaddress"]);
                    tPlussCpvTemplateObj.CpvtTelephoneno = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Telephoneno"]);
                    tPlussCpvTemplateObj.CpvtCellphone = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Cellphone"]);
                    tPlussCpvTemplateObj.CpvtGuarantoreeamount = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Guarantoreeamount"]);
                    tPlussCpvTemplateObj.CpvtRelationshipwithapplicant = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Relationshipwithapplicant"]);
                    tPlussCpvTemplateObj.CpvtEmailaddress = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Emailaddress"]); 
                    tPlussCpvTemplateObj.CpvtEntrydate = Convert.ToDateTime(tPlussCpvtemplateReader["Cpvt_Entrydate"]);
                    tPlussCpvTemplateObj.CpvtUserID = Convert.ToInt32(tPlussCpvtemplateReader["Cpvt_UserID"]); 

                    tPlussCpvTemplateObjList.Add(tPlussCpvTemplateObj);
                }
                tPlussCpvtemplateReader.Close();

            }
            return tPlussCpvTemplateObjList;
        }
        #endregion


        #region Get All CpvTemplate by LlId
        /// <summary>
        /// This method provide all cpv template info.
        /// </summary>
        /// <param name="llId">Gets a llid.</param>
        /// <returns>Returns a CpvTemplate list object.</returns>
        public List<CpvTemplate> GetAllCpvTemplateInfoByLlId(Int64 llId)
        {
            Bat.Common.Connection.ConnectionStringName = "conString";

            string queryString = "select CPVT_ID, CPVT_LLID, CPVT_DATE, CPVT_GUARANTORSNAME, " +
            "CPVT_CONTACTADDRESS, CPVT_TELEPHONENO, CPVT_CELLPHONE, " +
            "CPVT_GUARANTOREEAMOUNT, CPVT_RELATIONSHIPWITHAPPLICANT, " +
            "CPVT_EMAILADDRESS, CPVT_ENTRYDATE, CPVT_USER_ID, CPVT_DESIGNATION " +
            "from t_pluss_cpvtemplate " +
            "where CPVT_LLID = " + llId;

            DataTable cpvTemplateDataTableObj = DbQueryManager.GetTable(queryString);
            if (cpvTemplateDataTableObj != null)
            {
                DataTableReader tPlussCpvtemplateReader = cpvTemplateDataTableObj.CreateDataReader();

                while (tPlussCpvtemplateReader.Read())
                {
                    tPlussCpvTemplateObj = new CpvTemplate();

                    tPlussCpvTemplateObj.CpvtID = Convert.ToInt32(tPlussCpvtemplateReader["Cpvt_ID"]); 
                    tPlussCpvTemplateObj.CpvtLlid = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Llid"]);
                    string s = tPlussCpvtemplateReader["Cpvt_Date"].ToString();
                    tPlussCpvTemplateObj.CpvtDate = Convert.ToDateTime(tPlussCpvtemplateReader["Cpvt_Date"]);
                    tPlussCpvTemplateObj.CpvtGuarantorsname = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Guarantorsname"]);

                    tPlussCpvTemplateObj.CpvtContactaddress = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Contactaddress"]);
                    tPlussCpvTemplateObj.CpvtTelephoneno = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Telephoneno"]);
                    tPlussCpvTemplateObj.CpvtCellphone = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Cellphone"]);
                    tPlussCpvTemplateObj.CpvtGuarantoreeamount = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Guarantoreeamount"]);
                    tPlussCpvTemplateObj.CpvtRelationshipwithapplicant = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Relationshipwithapplicant"]);
                    tPlussCpvTemplateObj.CpvtEmailaddress = Convert.ToString(tPlussCpvtemplateReader["Cpvt_Emailaddress"]); 
                    tPlussCpvTemplateObj.CpvtEntrydate = Convert.ToDateTime(tPlussCpvtemplateReader["Cpvt_Entrydate"]);
                    tPlussCpvTemplateObj.CpvtUserID = Convert.ToInt32(tPlussCpvtemplateReader["Cpvt_User_ID"]);
                    tPlussCpvTemplateObj.CpvtDesignation = Convert.ToString(tPlussCpvtemplateReader["CPVT_DESIGNATION"]); 

                    tPlussCpvTemplateObjList.Add(tPlussCpvTemplateObj);
                }
                tPlussCpvtemplateReader.Close();

            }
            return tPlussCpvTemplateObjList;
        }
        #endregion
        
        /// <summary>
        /// This method insertion of cpv template.
        /// </summary>
        /// <param name="cpvTemplateObj">Gets a CpvTemplate object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertCpvTemplate(CpvTemplate cpvTemplateObj)
        {
            Bat.Common.Connection.ConnectionStringName = "conString";

            try
            {
                string sqlInsert = "INSERT INTO t_pluss_cpvtemplate ( " +
            "CPVT_LLID, CPVT_DATE, CPVT_GUARANTORSNAME, " +
            "CPVT_CONTACTADDRESS, CPVT_TELEPHONENO, CPVT_CELLPHONE, " +
            "CPVT_GUARANTOREEAMOUNT, CPVT_RELATIONSHIPWITHAPPLICANT, " +
            "CPVT_EMAILADDRESS, CPVT_ENTRYDATE, CPVT_USER_ID, CPVT_DESIGNATION ) " +
            "VALUES ( '" + cpvTemplateObj.CpvtLlid + "', '" + cpvTemplateObj.CpvtDate.Date.ToString("yyyy-MM-dd") + "', '" + cpvTemplateObj.CpvtGuarantorsname + "', '" +
            cpvTemplateObj.CpvtContactaddress + "', '" + cpvTemplateObj.CpvtTelephoneno + "', '" + cpvTemplateObj.CpvtCellphone + "', '" +
            cpvTemplateObj.CpvtGuarantoreeamount + "', '" + cpvTemplateObj.CpvtRelationshipwithapplicant + "', '" +
            cpvTemplateObj.CpvtEmailaddress + "', '" + cpvTemplateObj.CpvtEntrydate.Date.ToString("yyyy-MM-dd") + "', '" + cpvTemplateObj.CpvtUserID + "', '" + cpvTemplateObj.CpvtDesignation + "')";

                int insert = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                //queryText = sqlInsert.Replace('\'', '\"');
                //queryType = "Insert";
                return insert;
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// This method update of cpv template.
        /// </summary>
        /// <param name="cpvTemplateObj">Gets a CpvTemplate object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdateCpvTemplate(CpvTemplate cpvTemplateObj)
        {
            try
            {
                string sqlUpdate = "UPDATE t_pluss_cpvtemplate SET CPVT_LLID = " + cpvTemplateObj.CpvtLlid + ", " +
                "CPVT_DATE = '" + cpvTemplateObj.CpvtDate.Date.ToString("yyyy-MM-dd") + "', CPVT_GUARANTORSNAME = '" + cpvTemplateObj.CpvtGuarantorsname + "', " +
                "CPVT_CONTACTADDRESS = '" + cpvTemplateObj.CpvtContactaddress + "', " +
                "CPVT_TELEPHONENO = '" + cpvTemplateObj.CpvtTelephoneno + "', CPVT_CELLPHONE = '" + cpvTemplateObj.CpvtCellphone + "', " +
                "CPVT_GUARANTOREEAMOUNT = " + cpvTemplateObj.CpvtGuarantoreeamount + ", " +
                "CPVT_RELATIONSHIPWITHAPPLICANT = '" + cpvTemplateObj.CpvtRelationshipwithapplicant + "', " +
                "CPVT_EMAILADDRESS = '" + cpvTemplateObj.CpvtEmailaddress + "', CPVT_ENTRYDATE = '" + cpvTemplateObj.CpvtEntrydate.Date.ToString("yyyy-MM-dd") + "', " +
                "CPVT_DESIGNATION =  '" + cpvTemplateObj.CpvtDesignation + "', " +
                "CPVT_USER_ID = " + cpvTemplateObj.CpvtUserID + " WHERE CPVT_LLID = " + cpvTemplateObj.CpvtLlid ;

                int update = DbQueryManager.ExecuteNonQuery(sqlUpdate);   // if sucessfull then 1
                //queryText = sqlInsert.Replace('\'', '\"');
                //queryType = "Update";
                return update;
            }
            catch
            {
                return 0;
            }
            return 0;
        }


        /// <summary>
        /// This method provide deletion of cpv template.
        /// </summary>
        /// <param name="id">Gets a id.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeleteCpvTemplateObject(Int32 id)
        {
            string queryString = "DELETE FROM t_pluss_cpvtemplate WHERE CPVT_LLID = " + id;

            try
            {
                int status = DbQueryManager.ExecuteNonQuery(queryString);
                return status;
            }
            catch { return 0; }
        }

    }
}
