﻿using System;

namespace DAL
{
    /// <summary>
    /// MyExtention Class.
    /// </summary>
    public static class MyExtention
    {
        /// <summary>
        /// This method covert the special character.
        /// </summary>
        /// <param name="sMessage">Gets a string</param>
        /// <returns>Returns a converted string.</returns>
        public static string SpecialCharacterRemover(this String sMessage)
        {
            string returnMessage = "";
            if (!string.IsNullOrEmpty(sMessage))
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

    }
}
