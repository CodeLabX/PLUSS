﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using Bat.Common;
using System.Data;

namespace DAL
{
    /// <summary>
    /// SalaryGateway Class.
    /// </summary>
    public class SalaryGateway
    {
        private List<Salary> salaryObjList;
        private Salary tPlussSalaryObj;

        /// <summary>
        /// Constructor
        /// </summary>
        public SalaryGateway()
        { }

        /// <summary>
        /// This method select all salary info.
        /// </summary>
        /// <returns>Returns a Salary list object.</returns>
        public List<Salary> GetAllSalaryInfo()
        {
            salaryObjList = new List<Salary>();
            try
            {
                string queryString = "select * from t_pluss_salary";

                DataTable salaryDataTableObj = DbQueryManager.GetTable(queryString);

                if (salaryDataTableObj != null)
                {
                    DataTableReader tPlussSalaryReader = salaryDataTableObj.CreateDataReader();

                    while (tPlussSalaryReader.Read())
                    {
                        tPlussSalaryObj = new Salary();

                        tPlussSalaryObj.SalaID = Convert.ToInt32(tPlussSalaryReader["SalaID"]);
                        tPlussSalaryObj.SalaLlid = Convert.ToString(tPlussSalaryReader["SalaLlid"]);
                        tPlussSalaryObj.SalaCustomername = Convert.ToString(tPlussSalaryReader["SalaCustomername"]);
                        tPlussSalaryObj.SalaCompanyType = Convert.ToInt32(tPlussSalaryReader["SalaCompanyType"]);
                        tPlussSalaryObj.SalaAmount1 = Convert.ToDouble(tPlussSalaryReader["SalaAmount1"]);
                        tPlussSalaryObj.SalaAmount2 = Convert.ToDouble(tPlussSalaryReader["SalaAmount2"]);
                        tPlussSalaryObj.SalaNetincome = Convert.ToDouble(tPlussSalaryReader["SalaNetincome"]);
                        tPlussSalaryObj.SalaVariablesalarymonth1 = Convert.ToString(tPlussSalaryReader["SalaVariablesalarymonth1"]);
                        tPlussSalaryObj.SalaAverage1 = Convert.ToDouble(tPlussSalaryReader["SalaAverage1"]);
                        tPlussSalaryObj.Sala5Oot1 = Convert.ToDouble(tPlussSalaryReader["Sala5Oot1"]);
                        tPlussSalaryObj.Sala5Oconv1 = Convert.ToDouble(tPlussSalaryReader["Sala5Oconv1"]);
                        tPlussSalaryObj.Sala2Oot1 = Convert.ToDouble(tPlussSalaryReader["Sala2Oot1"]);
                        tPlussSalaryObj.Sala2Otshift1 = Convert.ToDouble(tPlussSalaryReader["Sala2Otshift1"]);
                        tPlussSalaryObj.Sala2Oothers1 = Convert.ToDouble(tPlussSalaryReader["Sala2Oothers1"]);
                        tPlussSalaryObj.SalaVariablesalarymonth2 = Convert.ToString(tPlussSalaryReader["SalaVariablesalarymonth2"]);
                        tPlussSalaryObj.SalaAverage2 = Convert.ToDouble(tPlussSalaryReader["SalaAverage2"]);
                        tPlussSalaryObj.Sala5Oot2 = Convert.ToDouble(tPlussSalaryReader["Sala5Oot2"]);
                        tPlussSalaryObj.Sala5Oconv2 = Convert.ToDouble(tPlussSalaryReader["Sala5Oconv2"]);
                        tPlussSalaryObj.Sala2Oot2 = Convert.ToDouble(tPlussSalaryReader["Sala2Oot2"]);
                        tPlussSalaryObj.Sala2Otshift2 = Convert.ToDouble(tPlussSalaryReader["Sala2Otshift2"]);
                        tPlussSalaryObj.Sala2Oothers2 = Convert.ToDouble(tPlussSalaryReader["Sala2Oothers2"]);
                        tPlussSalaryObj.SalaVariablesalarymonth3 = Convert.ToString(tPlussSalaryReader["SalaVariablesalarymonth3"]);
                        tPlussSalaryObj.SalaAverage3 = Convert.ToDouble(tPlussSalaryReader["SalaAverage3"]);
                        tPlussSalaryObj.Sala5Oot3 = Convert.ToDouble(tPlussSalaryReader["Sala5Oot3"]);
                        tPlussSalaryObj.Sala5Oconv3 = Convert.ToDouble(tPlussSalaryReader["Sala5Oconv3"]);
                        tPlussSalaryObj.Sala2Oot3 = Convert.ToDouble(tPlussSalaryReader["Sala2Oot3"]);
                        tPlussSalaryObj.Sala2Otshift3 = Convert.ToDouble(tPlussSalaryReader["Sala2Otshift3"]);
                        tPlussSalaryObj.Sala2Oothers3 = Convert.ToDouble(tPlussSalaryReader["Sala2Oothers3"]);
                        tPlussSalaryObj.SalaSumofaverage = Convert.ToDouble(tPlussSalaryReader["SalaSumofaverage"]);
                        tPlussSalaryObj.Sala5Ootaverage = Convert.ToDouble(tPlussSalaryReader["Sala5Ootaverage"]);
                        tPlussSalaryObj.Sala5Oconvaerage = Convert.ToDouble(tPlussSalaryReader["Sala5Oconvaerage"]);
                        tPlussSalaryObj.Sala2Ootaverage = Convert.ToDouble(tPlussSalaryReader["Sala2Ootaverage"]);
                        tPlussSalaryObj.SalaTshiftaverage = Convert.ToDouble(tPlussSalaryReader["SalaTshiftaverage"]);
                        tPlussSalaryObj.SalaOthersaverage = Convert.ToDouble(tPlussSalaryReader["SalaOthersaverage"]);
                        tPlussSalaryObj.SalaAvgotconv = Convert.ToDouble(tPlussSalaryReader["SalaAvgotconv"]);
                        tPlussSalaryObj.SalaAvgothervariable = Convert.ToDouble(tPlussSalaryReader["SalaAvgothervariable"]);
                        tPlussSalaryObj.SalaMealallowancemonth1 = Convert.ToString(tPlussSalaryReader["SalaMealallowancemonth1"]);
                        tPlussSalaryObj.SalaUsdall1 = Convert.ToDouble(tPlussSalaryReader["SalaUsdall1"]);
                        tPlussSalaryObj.SalaBdtapp1 = Convert.ToDouble(tPlussSalaryReader["SalaBdtapp1"]);
                        tPlussSalaryObj.SalaMealallowancemonth2 = Convert.ToString(tPlussSalaryReader["SalaMealallowancemonth2"]);
                        tPlussSalaryObj.SalaUsdall2 = Convert.ToDouble(tPlussSalaryReader["SalaUsdall2"]);
                        tPlussSalaryObj.SalaBdtapp2 = Convert.ToDouble(tPlussSalaryReader["SalaBdtapp2"]);
                        tPlussSalaryObj.SalaMealallowancemonth3 = Convert.ToString(tPlussSalaryReader["SalaMealallowancemonth3"]);
                        tPlussSalaryObj.SalaUsdall3 = Convert.ToDouble(tPlussSalaryReader["SalaUsdall3"]);
                        tPlussSalaryObj.SalaBdtapp3 = Convert.ToDouble(tPlussSalaryReader["SalaBdtapp3"]);
                        tPlussSalaryObj.SalaUsdaverage = Convert.ToDouble(tPlussSalaryReader["SalaUsdaverage"]);
                        tPlussSalaryObj.SalaBdtaverage = Convert.ToDouble(tPlussSalaryReader["SalaBdtaverage"]);
                        tPlussSalaryObj.SalaTkrate = Convert.ToDouble(tPlussSalaryReader["SalaTkrate"]);

                        salaryObjList.Add(tPlussSalaryObj);
                    }
                    tPlussSalaryReader.Close();
                }
                return salaryObjList;
            }
            catch
            {
                // TODO : tPlussSalaryReader.Close();
                return salaryObjList;
            }
        }

        /// <summary>
        /// This method select all salary info by llid
        /// </summary>
        /// <param name="llIdId">Gets a llid.</param>
        /// <returns>Returns a Salary list object.</returns>
        public List<Salary> GetAllSalaryInfoByLlId(string llIdId)
        {
            salaryObjList = new List<Salary>();
            try
            {
                string queryString = "select * from t_pluss_salary where SALA_LLID='" + llIdId + "'";

                DataTable salaryDataTableObj = DbQueryManager.GetTable(queryString);

                if (salaryDataTableObj != null)
                {
                    DataTableReader tPlussSalaryReader = salaryDataTableObj.CreateDataReader();

                    while (tPlussSalaryReader.Read())
                    {
                        tPlussSalaryObj = new Salary();

                        tPlussSalaryObj.SalaID = Convert.ToInt32(tPlussSalaryReader["Sala_ID"]);
                        tPlussSalaryObj.SalaLlid = Convert.ToString(tPlussSalaryReader["Sala_Llid"]);
                        tPlussSalaryObj.SalaCustomername = Convert.ToString(tPlussSalaryReader["Sala_Customername"]);
                        tPlussSalaryObj.SalaCompanyType = Convert.ToInt32(tPlussSalaryReader["Sala_Company_Type"]);
                        tPlussSalaryObj.SalaAmount1 = Convert.ToDouble(tPlussSalaryReader["Sala_Amount1"]);
                        tPlussSalaryObj.SalaAmount2 = Convert.ToDouble(tPlussSalaryReader["Sala_Amount2"]);
                        tPlussSalaryObj.SalaNetincome = Convert.ToDouble(tPlussSalaryReader["Sala_Netincome"]);
                        tPlussSalaryObj.SalaVariablesalarymonth1 = Convert.ToString(tPlussSalaryReader["Sala_Variablesalarymonth1"]);
                        tPlussSalaryObj.SalaAverage1 = Convert.ToDouble(tPlussSalaryReader["Sala_Average1"]);
                        tPlussSalaryObj.Sala5Oot1 = Convert.ToDouble(tPlussSalaryReader["Sala_5Oot1"]);
                        tPlussSalaryObj.Sala5Oconv1 = Convert.ToDouble(tPlussSalaryReader["Sala_5Oconv1"]);
                        tPlussSalaryObj.Sala2Oot1 = Convert.ToDouble(tPlussSalaryReader["Sala_2Oot1"]);
                        tPlussSalaryObj.Sala2Otshift1 = Convert.ToDouble(tPlussSalaryReader["Sala_2Otshift1"]);
                        tPlussSalaryObj.Sala2Oothers1 = Convert.ToDouble(tPlussSalaryReader["Sala_2Oothers1"]);
                        tPlussSalaryObj.SalaVariablesalarymonth2 = Convert.ToString(tPlussSalaryReader["Sala_Variablesalarymonth2"]);
                        tPlussSalaryObj.SalaAverage2 = Convert.ToDouble(tPlussSalaryReader["Sala_Average2"]);
                        tPlussSalaryObj.Sala5Oot2 = Convert.ToDouble(tPlussSalaryReader["Sala_5Oot2"]);
                        tPlussSalaryObj.Sala5Oconv2 = Convert.ToDouble(tPlussSalaryReader["Sala_5Oconv2"]);
                        tPlussSalaryObj.Sala2Oot2 = Convert.ToDouble(tPlussSalaryReader["Sala_2Oot2"]);
                        tPlussSalaryObj.Sala2Otshift2 = Convert.ToDouble(tPlussSalaryReader["Sala_2Otshift2"]);
                        tPlussSalaryObj.Sala2Oothers2 = Convert.ToDouble(tPlussSalaryReader["Sala_2Oothers2"]);
                        tPlussSalaryObj.SalaVariablesalarymonth3 = Convert.ToString(tPlussSalaryReader["Sala_Variablesalarymonth3"]);
                        tPlussSalaryObj.SalaAverage3 = Convert.ToDouble(tPlussSalaryReader["Sala_Average3"]);
                        tPlussSalaryObj.Sala5Oot3 = Convert.ToDouble(tPlussSalaryReader["Sala_5Oot3"]);
                        tPlussSalaryObj.Sala5Oconv3 = Convert.ToDouble(tPlussSalaryReader["Sala_5Oconv3"]);
                        tPlussSalaryObj.Sala2Oot3 = Convert.ToDouble(tPlussSalaryReader["Sala_2Oot3"]);
                        tPlussSalaryObj.Sala2Otshift3 = Convert.ToDouble(tPlussSalaryReader["Sala_2Otshift3"]);
                        tPlussSalaryObj.Sala2Oothers3 = Convert.ToDouble(tPlussSalaryReader["Sala_2Oothers3"]);
                        tPlussSalaryObj.SalaSumofaverage = Convert.ToDouble(tPlussSalaryReader["Sala_Sumofaverage"]);
                        tPlussSalaryObj.Sala5Ootaverage = Convert.ToDouble(tPlussSalaryReader["Sala_5Ootaverage"]);
                        tPlussSalaryObj.Sala5Oconvaerage = Convert.ToDouble(tPlussSalaryReader["Sala_5Oconvaerage"]);
                        tPlussSalaryObj.Sala2Ootaverage = Convert.ToDouble(tPlussSalaryReader["Sala_2Ootaverage"]);
                        tPlussSalaryObj.SalaTshiftaverage = Convert.ToDouble(tPlussSalaryReader["Sala_Tshiftaverage"]);
                        tPlussSalaryObj.SalaOthersaverage = Convert.ToDouble(tPlussSalaryReader["Sala_Othersaverage"]);
                        tPlussSalaryObj.SalaAvgotconv = Convert.ToDouble(tPlussSalaryReader["Sala_Avgotconv"]);
                        tPlussSalaryObj.SalaAvgothervariable = Convert.ToDouble(tPlussSalaryReader["Sala_Avgothervariable"]);
                        tPlussSalaryObj.SalaMealallowancemonth1 = Convert.ToString(tPlussSalaryReader["Sala_Mealallowancemonth1"]);
                        tPlussSalaryObj.SalaUsdall1 = Convert.ToDouble(tPlussSalaryReader["Sala_Usdall1"]);
                        tPlussSalaryObj.SalaBdtapp1 = Convert.ToDouble(tPlussSalaryReader["Sala_Bdtapp1"]);
                        tPlussSalaryObj.SalaMealallowancemonth2 = Convert.ToString(tPlussSalaryReader["Sala_Mealallowancemonth2"]);
                        tPlussSalaryObj.SalaUsdall2 = Convert.ToDouble(tPlussSalaryReader["Sala_Usdall2"]);
                        tPlussSalaryObj.SalaBdtapp2 = Convert.ToDouble(tPlussSalaryReader["Sala_Bdtapp2"]);
                        tPlussSalaryObj.SalaMealallowancemonth3 = Convert.ToString(tPlussSalaryReader["Sala_Mealallowancemonth3"]);
                        tPlussSalaryObj.SalaUsdall3 = Convert.ToDouble(tPlussSalaryReader["Sala_Usdall3"]);
                        tPlussSalaryObj.SalaBdtapp3 = Convert.ToDouble(tPlussSalaryReader["Sala_Bdtapp3"]);
                        tPlussSalaryObj.SalaUsdaverage = Convert.ToDouble(tPlussSalaryReader["Sala_Usdaverage"]);
                        tPlussSalaryObj.SalaBdtaverage = Convert.ToDouble(tPlussSalaryReader["Sala_Bdtaverage"]);
                        tPlussSalaryObj.SalaTkrate = Convert.ToDouble(tPlussSalaryReader["Sala_Tkrate"]);

                        salaryObjList.Add(tPlussSalaryObj);
                    }
                    tPlussSalaryReader.Close();
                }
                return salaryObjList;
            }
            catch { return salaryObjList; }
        }

        /// <summary>
        /// This method provide the operation of insertion of salary.
        /// </summary>
        /// <param name="tPlussSalaryObj">Gets a Salary object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertSalary(Salary tPlussSalaryObj)
        {
            try
            {
                string sqlInsert = "INSERT INTO t_pluss_salary ( SALA_LLID, SALA_CUSTOMERNAME, SALA_COMPANY_TYPE, SALA_AMOUNT1, SALA_AMOUNT2, SALA_NETINCOME, SALA_VARIABLESALARYMONTH1, SALA_AVERAGE1, SALA_5OOT1, SALA_5OCONV1, SALA_2OOT1, SALA_2OTSHIFT1, SALA_2OOTHERS1, SALA_VARIABLESALARYMONTH2, SALA_AVERAGE2, SALA_5OOT2, SALA_5OCONV2, SALA_2OOT2, SALA_2OTSHIFT2, SALA_2OOTHERS2, SALA_VARIABLESALARYMONTH3, SALA_AVERAGE3, SALA_5OOT3, SALA_5OCONV3, SALA_2OOT3, SALA_2OTSHIFT3, SALA_2OOTHERS3, SALA_SUMOFAVERAGE, SALA_5OOTAVERAGE, SALA_5OCONVAERAGE, SALA_2OOTAVERAGE, SALA_TSHIFTAVERAGE, SALA_OTHERSAVERAGE, SALA_AVGOTCONV, SALA_AVGOTHERVARIABLE, SALA_MEALALLOWANCEMONTH1, SALA_USDALL1, SALA_BDTAPP1, SALA_MEALALLOWANCEMONTH2, SALA_USDALL2, SALA_BDTAPP2, SALA_MEALALLOWANCEMONTH3, SALA_USDALL3, SALA_BDTAPP3, SALA_USDAVERAGE, SALA_BDTAVERAGE, SALA_TKRATE ) VALUES ( " +
                    tPlussSalaryObj.SalaLlid + ", '" + tPlussSalaryObj.SalaCustomername + "'," +
                    tPlussSalaryObj.SalaCompanyType + ", " + tPlussSalaryObj.SalaAmount1 + ", " +
                    tPlussSalaryObj.SalaAmount2 + ", " + tPlussSalaryObj.SalaNetincome + ", '" +
                    tPlussSalaryObj.SalaVariablesalarymonth1 + "', " +
                    tPlussSalaryObj.SalaAverage1 + ", " + tPlussSalaryObj.Sala5Oot1 + ", " +
                    tPlussSalaryObj.Sala5Oconv1 + ", " + tPlussSalaryObj.Sala2Oot1 + ", " +
                    tPlussSalaryObj.Sala2Otshift1 + ", " + tPlussSalaryObj.Sala2Oothers1 + ", '" +
                    tPlussSalaryObj.SalaVariablesalarymonth2 + "', " +
                    tPlussSalaryObj.SalaAverage2 + ", " + tPlussSalaryObj.Sala5Oot2 + ", " +
                    tPlussSalaryObj.Sala5Oconv2 + ", " + tPlussSalaryObj.Sala2Oot2 + ", " +
                    tPlussSalaryObj.Sala2Otshift2 + ", " + tPlussSalaryObj.Sala2Oothers2 + ", '" +
                    tPlussSalaryObj.SalaVariablesalarymonth3 + "', " +
                    tPlussSalaryObj.SalaAverage3 + ", " + tPlussSalaryObj.Sala5Oot3 + ", " +
                    tPlussSalaryObj.Sala5Oconv3 + ", " + tPlussSalaryObj.Sala2Oot3 + ", " +
                    tPlussSalaryObj.Sala2Otshift3 + ", " + tPlussSalaryObj.Sala2Oothers3 + ", " +
                    tPlussSalaryObj.SalaSumofaverage + ", " + tPlussSalaryObj.Sala5Ootaverage + ", " +
                    tPlussSalaryObj.Sala5Oconvaerage + ", " + tPlussSalaryObj.Sala2Ootaverage + ", " +
                    tPlussSalaryObj.SalaTshiftaverage + ", " + tPlussSalaryObj.SalaOthersaverage + ", " +
                    tPlussSalaryObj.SalaAvgotconv + ", " + tPlussSalaryObj.SalaAvgothervariable + ", '" +
                    tPlussSalaryObj.SalaMealallowancemonth1 + "', " +
                    tPlussSalaryObj.SalaUsdall1 + ", " + tPlussSalaryObj.SalaBdtapp1 + ", '" +
                    tPlussSalaryObj.SalaMealallowancemonth2 + "', " +
                    tPlussSalaryObj.SalaUsdall2 + ", " + tPlussSalaryObj.SalaBdtapp2 + ", '" +
                    tPlussSalaryObj.SalaMealallowancemonth3 + "', " +
                    tPlussSalaryObj.SalaUsdall3 + ", " + tPlussSalaryObj.SalaBdtapp3 + ", " +
                    tPlussSalaryObj.SalaUsdaverage + ", " + tPlussSalaryObj.SalaBdtaverage + ", " +
                    tPlussSalaryObj.SalaTkrate + ")";


                int insert = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                return insert;
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// This method provide the operation of update of salary.
        /// </summary>
        /// <param name="tPlussSalaryObj">Gets a Salary object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdateSalary(Salary salaryObj)
        {
            try
            {
                string sqlInsert = "UPDATE t_pluss_salary SET SALA_LLID = " +
                salaryObj.SalaLlid + ", SALA_CUSTOMERNAME = '" +
                salaryObj.SalaCustomername + "', SALA_COMPANY_TYPE = " +
                salaryObj.SalaCompanyType + ", SALA_AMOUNT1 = " +
                salaryObj.SalaAmount1 + ", SALA_AMOUNT2 = " +
                salaryObj.SalaAmount2 + ", SALA_NETINCOME = " +
                salaryObj.SalaNetincome + ", SALA_VARIABLESALARYMONTH1 = '" +
                salaryObj.SalaVariablesalarymonth1 + "', SALA_AVERAGE1 = " +
                salaryObj.SalaAverage1 + ", SALA_5OOT1 = " +
                salaryObj.Sala5Oot1 + ", SALA_5OCONV1 = " +
                salaryObj.Sala5Oconv1 + ", SALA_2OOT1 = " +
                salaryObj.Sala2Oot1 + ", SALA_2OTSHIFT1 = " +
                salaryObj.Sala2Otshift1 + ", SALA_2OOTHERS1 = " +
                salaryObj.Sala2Oothers1 + ", SALA_VARIABLESALARYMONTH2 = '" +
                salaryObj.SalaVariablesalarymonth2 + "', SALA_AVERAGE2 = " +
                salaryObj.SalaAverage2 + ", SALA_5OOT2 = " +
                salaryObj.Sala5Oot2 + ", SALA_5OCONV2 = " +
                salaryObj.Sala5Oconv2 + ", SALA_2OOT2 = " +
                salaryObj.Sala2Oot2 + ", SALA_2OTSHIFT2 = " +
                salaryObj.Sala2Otshift2 + ", SALA_2OOTHERS2 = " +
                salaryObj.Sala2Oothers2 + ", SALA_VARIABLESALARYMONTH3 = '" +
                salaryObj.SalaVariablesalarymonth3 + "', SALA_AVERAGE3 = " +
                salaryObj.SalaAverage3 + ", SALA_5OOT3 = " +
                salaryObj.Sala5Oot3 + ", SALA_5OCONV3 = " +
                salaryObj.Sala5Oconv3 + ", SALA_2OOT3 = " +
                salaryObj.Sala2Oot3 + ", SALA_2OTSHIFT3 = " +
                salaryObj.Sala2Otshift3 + ", SALA_2OOTHERS3 = " +
                salaryObj.Sala2Oothers3 + ", SALA_SUMOFAVERAGE = " +
                salaryObj.SalaSumofaverage + ", SALA_5OOTAVERAGE = " +
                salaryObj.Sala5Ootaverage + ", SALA_5OCONVAERAGE = " +
                salaryObj.Sala5Oconvaerage + ", SALA_2OOTAVERAGE = " +
                salaryObj.Sala2Ootaverage + ", SALA_TSHIFTAVERAGE = " +
                salaryObj.SalaTshiftaverage + ", SALA_OTHERSAVERAGE = " +
                salaryObj.SalaOthersaverage + ", SALA_AVGOTCONV = " +
                salaryObj.SalaAvgotconv + ", SALA_AVGOTHERVARIABLE = " +
                salaryObj.SalaAvgothervariable + ", SALA_MEALALLOWANCEMONTH1 = '" +
                salaryObj.SalaMealallowancemonth1 + "', SALA_USDALL1 = " +
                salaryObj.SalaUsdall1 + ", SALA_BDTAPP1 = " +
                salaryObj.SalaBdtapp1 + ", SALA_MEALALLOWANCEMONTH2 = '" +
                salaryObj.SalaMealallowancemonth2 + "', SALA_USDALL2 = " +
                salaryObj.SalaUsdall2 + ", SALA_BDTAPP2 = " +
                salaryObj.SalaBdtapp2 + ", SALA_MEALALLOWANCEMONTH3 = '" +
                salaryObj.SalaMealallowancemonth3 + "', SALA_USDALL3 = " +
                salaryObj.SalaUsdall3 + ", SALA_BDTAPP3 = " +
                salaryObj.SalaBdtapp3 + ", SALA_USDAVERAGE = " +
                salaryObj.SalaUsdaverage + ", SALA_BDTAVERAGE = " +
                salaryObj.SalaBdtaverage + ", SALA_TKRATE = " +
                salaryObj.SalaTkrate + " WHERE SALA_ID = " +
                salaryObj.SalaID;

                int update = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                return update;
            }
            catch
            {
                return 0;
            }
            return 0;
        }
    }
}
