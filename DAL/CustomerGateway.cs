﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Data;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// Customer gateway class.
    /// </summary>
    public class CustomerGateway
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public CustomerGateway()
        {
            //Constructor logic here.
        }

        #region GetCustomerLoanAppInfo(string llId)
        /// <summary>
        /// This method provide customer loan Application info.
        /// </summary>
        /// <param name="llId">Gets LLId</param>
        /// <returns>Returns a customer object.</returns>
        public Customer GetCustomerLoanAppInfo(string llId)
        {
            Customer customerObj = null;
            string queryString = "SELECT LOAN_APPLI_ID, CUST_NM, ACC_NO, LOAN_STATU_ID FROM loan_app_info WHERE LOAN_APPLI_ID = '" + llId + "'";
            DataTable customerDataTableObj = DbQueryManager.GetTable(queryString);

            if (customerDataTableObj != null)
            {
                DataTableReader customerDataTableReaderObj = customerDataTableObj.CreateDataReader();
                if (customerDataTableReaderObj != null)
                {
                    while (customerDataTableReaderObj.Read())
                    {
                        customerObj = new Customer();
                        customerObj.LLId = Convert.ToString(customerDataTableReaderObj["LOAN_APPLI_ID"]);
                        customerObj.Name = Convert.ToString(customerDataTableReaderObj["CUST_NM"]);
                        customerObj.MasterNo = Convert.ToString(customerDataTableReaderObj["ACC_NO"]);
                        customerObj.LoanStatusId = Convert.ToInt32(customerDataTableReaderObj["LOAN_STATU_ID"]);
                    }
                    customerDataTableReaderObj.Close();
                }
                customerDataTableObj.Clear();
            }
            return customerObj;
        }
        #endregion

        #region GetCustomerBankStatementCalculation(string llId)
        /// <summary>
        /// This method provide customer bank calculation info.
        /// </summary>
        /// <param name="llId">Gets LLId</param>
        /// <returns>Returns a customer object.</returns>
        public Customer GetCustomerBankStatementCalculation(string llId)
        {
            Customer customerObj = new Customer();
            customerObj.PDC = "";
            string queryString = "SELECT DISTINCT BSCD_LLID, BSCD_BANK_ID, B.BANK_NM, BSCD_BRANCH_ID, BCH.BRAN_NAME, BSCD_ACNO " +
                                 "FROM t_pluss_bankstatementcalculationdetails  AS BSCD " +
                                 "LEFT JOIN t_pluss_bank AS B ON BSCD.BSCD_BANK_ID = B.BANK_ID " +
                                 "LEFT JOIN t_pluss_branch AS BCH ON BSCD.BSCD_BRANCH_ID = BCH.BRAN_ID " +
                                 "WHERE BSCD_LLID = '" + llId + "'";

            DataTable customerDataTableObj = DbQueryManager.GetTable(queryString);

            if (customerDataTableObj != null)
            {
                DataTableReader customerDataTableReaderObj = customerDataTableObj.CreateDataReader();
                if (customerDataTableReaderObj != null)
                {
                    string accNo = "";
                    List<string> tempAccList = new List<string>();
                    string pdcName = "";
                    List<string> bankList = new List<string>();
                    while (customerDataTableReaderObj.Read())
                    {
                        customerObj.LLId = Convert.ToString(customerDataTableReaderObj["BSCD_LLID"]);
                        string pdc = Convert.ToString(customerDataTableReaderObj["BANK_NM"]) + ",";
                        pdcName += pdc;
                        string tempAcc = Convert.ToString(customerDataTableReaderObj["BSCD_ACNO"]);
                        string tempAcc2 = Convert.ToString(customerDataTableReaderObj["BSCD_ACNO"]) + ",";
                        accNo += tempAcc2;
                        bankList.Add(Convert.ToString(customerDataTableReaderObj["BANK_NM"]));
                        tempAccList.Add(tempAcc);
                    }
                    if (pdcName.Length > 1)
                    {
                        customerObj.PDC = pdcName.Substring(0, pdcName.Length - 1);
                    }
                    if (accNo.Length > 1)
                    {
                        customerObj.PDCAccount = accNo.Substring(0, accNo.Length - 1);
                    }
                    for (int i = 0; i < bankList.Count; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                customerObj.OtherStatementBank1 = bankList[i].ToString() + "";
                                break;
                            case 1:
                                customerObj.OtherStatementBank2 = bankList[i].ToString() + "";
                                break;
                            case 2:
                                customerObj.OtherStatementBank3 = bankList[i].ToString() + "";
                                break;

                            case 3:
                                customerObj.OtherStatementBank4 = bankList[i].ToString() + "";
                                break;

                            case 4:
                                customerObj.OtherStatementBank5 = bankList[i].ToString() + "";
                                break;
                        }
                    }
                    for (int i = 0; i < tempAccList.Count; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                customerObj.OtherStatementAcc1 = tempAccList[i].ToString() + "";
                                break;
                            case 1:
                                customerObj.OtherStatementAcc2 = tempAccList[i].ToString() + "";
                                break;
                            case 2:
                                customerObj.OtherStatementAcc3 = tempAccList[i].ToString() + "";
                                break;

                            case 3:
                                customerObj.OtherStatementAcc4 = tempAccList[i].ToString() + "";
                                break;

                            case 4:
                                customerObj.OtherStatementAcc5 = tempAccList[i].ToString() + "";
                                break;
                        }
                    }
                    //customerObj.OtherStatementAcc1 = accNo.Substring(0,accNo.Length-1);
                    customerDataTableReaderObj.Close();
                }
                customerDataTableObj.Clear();
            }
            return customerObj;
        }
        #endregion

        #region GetCustomerInfo(string llId)
        /// <summary>
        /// This method provide customer loan Application info.
        /// </summary>
        /// <param name="llId">Gets LLId</param>
        /// <returns>Returns a customer object.</returns>
        public Customer GetCustomerInfo(string llId)
        {
            Customer customerObj = null;
            string queryString = "SELECT CUST_ID, CUST_LLID, CUST_NAME, CUST_MASTERNO, CUST_PDCSLFORM, CUST_PDCACNO, " +
                                            "CUST_CCPLBUDLEOFFER, CUST_FIRSTPAYMENT1, CUST_OTHERSTATEMENT1A, " +
                                            "CUST_OTHERSTATEMENT1B, CUST_OTHERSTATEMENT2A, " +
                                            "CUST_OTHERSTATEMENT2B, CUST_OTHERSTATEMENT3A, " +
                                            "CUST_OTHERSTATEMENT3B, CUST_OTHERSTATEMENT4A, " +
                                            "CUST_OTHERSTATEMENT4B, CUST_OTHERSTATEMENT5A, " +
                                            "CUST_OTHERSTATEMENT5B, CUST_TOPUPCOUNT ,CUST_TOPUPAMOUNT1, " +
                                            "CUST_OUTSTANDING, CUST_TOPUPDATE1, CUST_TOPUPAMOUNT2, " +
                                            "CUST_TOPUPDATE2, CUST_TOPUPAMOUNT3, CUST_TOPUPDATE3, " +
                                            "CUST_OTHERDIRSLOAN, CUST_MOB12 " +
                                  "FROM " +
                                            "t_pluss_customer " +
                                  "WHERE CUST_LLID = '" + llId + "'";

            DataTable customerDataTableObj = DbQueryManager.GetTable(queryString);

            if (customerDataTableObj != null)
            {
                DataTableReader customerDataTableReaderObj = customerDataTableObj.CreateDataReader();
                if (customerDataTableReaderObj != null)
                {
                    while (customerDataTableReaderObj.Read())
                    {
                        customerObj = new Customer();
                        customerObj.Id = Convert.ToInt32(customerDataTableReaderObj["CUST_ID"]);
                        customerObj.LLId = Convert.ToString(customerDataTableReaderObj["CUST_LLID"]);
                        customerObj.Name = Convert.ToString(customerDataTableReaderObj["CUST_NAME"]);
                        customerObj.MasterNo = Convert.ToString(customerDataTableReaderObj["CUST_MASTERNO"]);
                        customerObj.PDC = Convert.ToString(customerDataTableReaderObj["CUST_PDCSLFORM"]);
                        customerObj.PDCAccount = Convert.ToString(customerDataTableReaderObj["CUST_PDCACNO"]);
                        customerObj.CCPLBundleOffer = Convert.ToInt32(customerDataTableReaderObj["CUST_CCPLBUDLEOFFER"]);
                        customerObj.FirstPayment = Convert.ToInt32(customerDataTableReaderObj["CUST_FIRSTPAYMENT1"]);
                        customerObj.OtherStatementBank1 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT1B"]);
                        customerObj.OtherStatementAcc1 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT1A"]);
                        customerObj.OtherStatementBank2 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT2B"]);
                        customerObj.OtherStatementAcc2 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT2A"]);
                        customerObj.OtherStatementBank3 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT3B"]);
                        customerObj.OtherStatementAcc3 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT3A"]);
                        customerObj.OtherStatementBank4 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT4B"]);
                        customerObj.OtherStatementAcc4 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT4A"]);
                        customerObj.OtherStatementBank5 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT5B"]);
                        customerObj.OtherStatementAcc5 = Convert.ToString(customerDataTableReaderObj["CUST_OTHERSTATEMENT5A"]);
                        customerObj.TopUpCount = Convert.ToInt32(customerDataTableReaderObj["CUST_TOPUPCOUNT"]);
                        customerObj.TopUpAmount1 = Convert.ToDouble(customerDataTableReaderObj["CUST_TOPUPAMOUNT1"]);
                        customerObj.OutStanding = Convert.ToDouble(customerDataTableReaderObj["CUST_OUTSTANDING"]);
                        customerObj.TopUpDate1 = Convert.ToDateTime(customerDataTableReaderObj["CUST_TOPUPDATE1"]);
                        customerObj.TopUpAmount2 = Convert.ToDouble(customerDataTableReaderObj["CUST_TOPUPAMOUNT2"]);
                        customerObj.TopUpDate2 = Convert.ToDateTime(customerDataTableReaderObj["CUST_TOPUPDATE2"]);
                        customerObj.TopUpAmount3 = Convert.ToDouble(customerDataTableReaderObj["CUST_TOPUPAMOUNT3"]);
                        customerObj.TopUpDate3 = Convert.ToDateTime(customerDataTableReaderObj["CUST_TOPUPDATE3"]);
                        customerObj.OtherDirLoan = Convert.ToInt32(customerDataTableReaderObj["CUST_OTHERDIRSLOAN"]);
                        customerObj.Mob12 = Convert.ToInt32(customerDataTableReaderObj["CUST_MOB12"]);
                    }
                    customerDataTableReaderObj.Close();
                }
                customerDataTableObj.Clear();
            }
            return customerObj;
        }
        #endregion

        #region GetCustomerDetailInfo(string llId)
        /// <summary>
        /// This method provide customer detail info.
        /// </summary>
        /// <param name="llId">Gets LLId</param>
        /// <returns>Returns a customer detail list object.</returns>
        public List<CustomerDetail> GetCustomerDetailInfo(string llId)
        {
            List<CustomerDetail> customerDetailListObj = new List<CustomerDetail>();
            string queryString = "SELECT 	CUDE_ID, CUDE_CUSTOMER_ID, CUDE_CUSTOMER_LLID, " +
                                            "CUDE_FACILITY, LTRIM(RTRIM(CUDE_FLAG)) as CUDE_FLAG, CUDE_OUTSTANDING, CUDE_ORGINALLIMIT, " +
                                            "CUDE_REPAYMENT, CUDE_DISBURSEDATE, CUDE_EXPOSUREDATE, " +
                                            "CUDE_CASHSECURITY " +
                                  "FROM " +
                                            "t_pluss_customerdetails " +
                                  "WHERE CUDE_CUSTOMER_LLID = '" + llId + "'";

            DataTable customerDetailDataTableObj = DbQueryManager.GetTable(queryString);

            if (customerDetailDataTableObj != null)
            {
                DataTableReader customerDetailDataTableReaderObj = customerDetailDataTableObj.CreateDataReader();
                if (customerDetailDataTableReaderObj != null)
                {
                    while (customerDetailDataTableReaderObj.Read())
                    {
                        CustomerDetail customerDetailObj = new CustomerDetail();
                        customerDetailObj.Id = Convert.ToInt32("0" + customerDetailDataTableReaderObj["CUDE_ID"]);
                        customerDetailObj.CustomerId = Convert.ToInt32(customerDetailDataTableReaderObj["CUDE_CUSTOMER_ID"]);
                        customerDetailObj.CustomerLLId = Convert.ToString(customerDetailDataTableReaderObj["CUDE_CUSTOMER_LLID"]);
                        customerDetailObj.Facility = Convert.ToString(customerDetailDataTableReaderObj["CUDE_FACILITY"]);

                        customerDetailObj.Flag = Convert.ToChar(customerDetailDataTableReaderObj["CUDE_FLAG"]);
                        customerDetailObj.OutStanding = Convert.ToDouble(customerDetailDataTableReaderObj["CUDE_OUTSTANDING"]);
                        customerDetailObj.OrginalLimit = Convert.ToDouble(customerDetailDataTableReaderObj["CUDE_ORGINALLIMIT"]);
                        customerDetailObj.RePayment = Convert.ToDouble(customerDetailDataTableReaderObj["CUDE_REPAYMENT"]);
                        customerDetailObj.DisbursDate = Convert.ToDateTime(customerDetailDataTableReaderObj["CUDE_DISBURSEDATE"]);
                        customerDetailObj.ExposureDate = Convert.ToDateTime(customerDetailDataTableReaderObj["CUDE_EXPOSUREDATE"]);
                        customerDetailObj.CashSecurity = Convert.ToDouble(customerDetailDataTableReaderObj["CUDE_CASHSECURITY"]);
                        customerDetailListObj.Add(customerDetailObj);
                    }
                    customerDetailDataTableReaderObj.Close();
                }
                customerDetailDataTableObj.Clear();
            }
            return customerDetailListObj;
        }
        #endregion

        #region GetAllProduct()
        /// <summary>
        /// This method provide all products
        /// </summary>
        /// <returns>Returns product list object.</returns>
        public List<Product> GetAllProduct()
        {
            List<Product> productListObj = new List<Product>();
            //string queryString = "SELECT PROD_ID, PROD_NAME, PROD_TYPE FROM t_pluss_product ORDER BY PROD_NAME ASC";
            string queryString = "SElECT FACI_ID, FACI_NAME, FACI_CODE FROM t_pluss_facility ORDER BY FACI_NAME ASC";
            DataTable productDataTableObj = DbQueryManager.GetTable(queryString);
            if (productDataTableObj != null)
            {
                DataTableReader productDataTableReaderObj = productDataTableObj.CreateDataReader();
                if (productDataTableReaderObj != null)
                {
                    while (productDataTableReaderObj.Read())
                    {
                        Product productObj = new Product();
                        //productObj.ProductId = Convert.ToInt32("0" + productDataTableReaderObj["PROD_ID"].ToString());
                        //productObj.ProductName = Convert.ToString(productDataTableReaderObj["PROD_NAME"]);
                        //productObj.ProdType = Convert.ToString(productDataTableReaderObj["PROD_NAME"]).Trim() + "#" + Convert.ToString(productDataTableReaderObj["PROD_TYPE"]);
                        productObj.ProductId = Convert.ToInt32("0" + productDataTableReaderObj["FACI_ID"].ToString());
                        productObj.ProductName = Convert.ToString(productDataTableReaderObj["FACI_NAME"]);
                        productObj.ProdType = Convert.ToString(productDataTableReaderObj["FACI_NAME"]).Trim() + "#" + Convert.ToString(productDataTableReaderObj["FACI_CODE"]);
                        productListObj.Add(productObj);
                    }
                    productDataTableReaderObj.Close();
                }
                productDataTableObj.Clear();
            }
            return productListObj;
        }
        #endregion

        #region InsertORUpdateCustomerInfo(Customer customerObj)
        /// <summary>
        /// This method provide insert/update customer information.
        /// </summary>
        /// <param name="loanInformationObj">Gets customer object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateCustomerInfo(Customer customerObj)
        {
            int insertORUpdateValue = -1;
            if (!IsExistsCustomerInfo(Convert.ToInt32(customerObj.Id)))
            {
                insertORUpdateValue = InsertCustomerInformation(customerObj);
            }
            else
            {
                insertORUpdateValue = UpdateCustomerInformation(customerObj);
            }
            return insertORUpdateValue;
        }

        private int InsertCustomerInformation(Customer customerObj)
        {
            int insertRow = -1;
            string queryString = String.Format("INSERT INTO t_pluss_customer (CUST_LLID, CUST_NAME, CUST_MASTERNO, CUST_PDCSLFORM, CUST_CCPLBUDLEOFFER, " +
                                                "CUST_OTHERDIRSLOAN, CUST_FIRSTPAYMENT1, CUST_OTHERSTATEMENT1B, CUST_OTHERSTATEMENT1A, " +
                                                "CUST_OTHERSTATEMENT2B, CUST_OTHERSTATEMENT2A,CUST_OTHERSTATEMENT3B, CUST_OTHERSTATEMENT3A, " +
                                                "CUST_OTHERSTATEMENT4B, CUST_OTHERSTATEMENT4A, CUST_OTHERSTATEMENT5B, CUST_OTHERSTATEMENT5A, CUST_MOB12, " +
                                                "CUST_TOPUPCOUNT,CUST_TOPUPAMOUNT1, CUST_OUTSTANDING, CUST_TOPUPDATE1, CUST_TOPUPAMOUNT2, CUST_TOPUPDATE2, " +
                                                "CUST_TOPUPAMOUNT3, CUST_TOPUPDATE3, CUST_PDCACNO) VALUES({0}, '{1}', '{2}','{3}', {4}, {5}, {6}, '{7}','{8}','{9}','{10}', " +
                                                "'{11}','{12}','{13}','{14}','{15}','{16}', {17},{18} ,{19},{20},'{21}', {22},'{23}',{24},'{25}','{26}')",
                                                EscapeCharacter(customerObj.LLId), EscapeCharacter(customerObj.Name), EscapeCharacter(customerObj.MasterNo), EscapeCharacter(customerObj.PDC),
                                                customerObj.CCPLBundleOffer, customerObj.OtherDirLoan, customerObj.FirstPayment, EscapeCharacter(customerObj.OtherStatementBank1), EscapeCharacter(customerObj.OtherStatementAcc1),
                                                EscapeCharacter(customerObj.OtherStatementBank2), EscapeCharacter(customerObj.OtherStatementAcc2), EscapeCharacter(customerObj.OtherStatementBank3), EscapeCharacter(customerObj.OtherStatementAcc3), EscapeCharacter(customerObj.OtherStatementBank4),
                                                EscapeCharacter(customerObj.OtherStatementAcc4), EscapeCharacter(customerObj.OtherStatementBank5), EscapeCharacter(customerObj.OtherStatementAcc5), customerObj.Mob12,
                                                customerObj.TopUpCount, customerObj.TopUpAmount1, customerObj.OutStanding, customerObj.TopUpDate1.ToString("yyyy-MM-dd"), customerObj.TopUpAmount2, customerObj.TopUpDate2.ToString("yyyy-MM-dd"),
                                                customerObj.TopUpAmount3, customerObj.TopUpDate3.ToString("yyyy-MM-dd"), EscapeCharacter(customerObj.PDCAccount));
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (insertRow > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }

        private int UpdateCustomerInformation(Customer customerObj)
        {
            int updateRow = -1;
            string queryString = String.Format("UPDATE t_pluss_customer SET " +
                                                "CUST_LLID = '{0}', CUST_NAME = '{1}', CUST_MASTERNO = '{2}', CUST_PDCSLFORM = '{3}', CUST_CCPLBUDLEOFFER = {4}, " +
                                                "CUST_OTHERDIRSLOAN = {5}, CUST_FIRSTPAYMENT1 = {6}, CUST_OTHERSTATEMENT1B = '{7}', CUST_OTHERSTATEMENT1A = '{8}', " +
                                                "CUST_OTHERSTATEMENT2B = '{9}', CUST_OTHERSTATEMENT2A = '{10}',CUST_OTHERSTATEMENT3B = '{25}', CUST_OTHERSTATEMENT3A = '{11}', " +
                                                "CUST_OTHERSTATEMENT4B = '{12}', CUST_OTHERSTATEMENT4A = '{13}', CUST_OTHERSTATEMENT5B = '{14}', CUST_OTHERSTATEMENT5A = '{15}', CUST_MOB12 = {16}, " +
                                                "CUST_TOPUPAMOUNT1 = {17}, CUST_OUTSTANDING = {18}, CUST_TOPUPDATE1 = '{19}', CUST_TOPUPAMOUNT2 = {20}, CUST_TOPUPDATE2 = '{21}', " +
                                                "CUST_TOPUPAMOUNT3 = {22}, CUST_TOPUPDATE3 = '{23}', CUST_PDCACNO = '{26}',CUST_TOPUPCOUNT = {27} " +
                                                "WHERE CUST_ID = {24} ",
                                                EscapeCharacter(customerObj.LLId), EscapeCharacter(customerObj.Name), EscapeCharacter(customerObj.MasterNo), EscapeCharacter(customerObj.PDC),
                                                customerObj.CCPLBundleOffer, customerObj.OtherDirLoan, customerObj.FirstPayment, EscapeCharacter(customerObj.OtherStatementBank1), EscapeCharacter(customerObj.OtherStatementAcc1),
                                                EscapeCharacter(customerObj.OtherStatementBank2), EscapeCharacter(customerObj.OtherStatementAcc2), EscapeCharacter(customerObj.OtherStatementAcc3), EscapeCharacter(customerObj.OtherStatementBank4),
                                                EscapeCharacter(customerObj.OtherStatementAcc4), EscapeCharacter(customerObj.OtherStatementBank5), EscapeCharacter(customerObj.OtherStatementAcc5), customerObj.Mob12,
                                                customerObj.TopUpAmount1, customerObj.OutStanding, customerObj.TopUpDate1.ToString("yyyy-MM-dd"), customerObj.TopUpAmount2, customerObj.TopUpDate2.ToString("yyyy-MM-dd"),
                                                customerObj.TopUpAmount3, customerObj.TopUpDate3.ToString("yyyy-MM-dd"), customerObj.Id, EscapeCharacter(customerObj.OtherStatementBank3), EscapeCharacter(customerObj.PDCAccount), customerObj.TopUpCount);
            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (updateRow > 0)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
        #endregion

        #region InsertORUpdateCustomerDetailsInfo(List<CustomerDetail> CustomerDetailListObj)
        /// <summary>
        /// This method provide insert update customer detail information.
        /// </summary>
        /// <param name="CustomerDetailListObj">Gets customer detail list object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateCustomerDetailsInfo(List<CustomerDetail> CustomerDetailListObj)
        {
            int insertORUpdateValue = -1;
            List<CustomerDetail> insertCustomerDetailInfoListObj = new List<CustomerDetail>();
            List<CustomerDetail> updateCustomerDetailInfoListObj = new List<CustomerDetail>();
            if (CustomerDetailListObj.Count > 0)
            {
                foreach (CustomerDetail customerDetailObj in CustomerDetailListObj)
                {
                    //Int32 branchId = IsExistsBranchInfo(branchInformationObj.Name, Convert.ToInt32(branchInformationObj.Bank.Id));
                    if (!IsExistsCustomerDetailInfo(Convert.ToInt32(customerDetailObj.Id)))
                    {
                        insertCustomerDetailInfoListObj.Add(customerDetailObj);
                    }
                    else
                    {
                        updateCustomerDetailInfoListObj.Add(customerDetailObj);
                    }
                }
                if (insertCustomerDetailInfoListObj.Count > 0)
                {
                    insertORUpdateValue = InsertCustomerDetailInformation(insertCustomerDetailInfoListObj);
                }
                if (updateCustomerDetailInfoListObj.Count > 0)
                {
                    insertORUpdateValue = UpdateCustomerDetailInformation(updateCustomerDetailInfoListObj);
                }
            }

            return insertORUpdateValue;
        }

        #region InsertCustomerDetailInformation(List<CustomerDetail> customerDetailListObj)
        /// <summary>
        /// This method provide insert customer detail information.
        /// </summary>
        /// <param name="customerDetailListObj">Gets customer detail list object.</param>
        /// <returns>Returns positive value if insert.</returns>
        private int InsertCustomerDetailInformation(List<CustomerDetail> customerDetailListObj)
        {
            int insertRow = -1;
            string queryString = "INSERT INTO t_pluss_customerdetails (CUDE_CUSTOMER_ID, CUDE_CUSTOMER_LLID, " +
                                            "CUDE_FACILITY, CUDE_FLAG, CUDE_OUTSTANDING, " +
                                            "CUDE_ORGINALLIMIT, CUDE_REPAYMENT, " +
                                            "CUDE_DISBURSEDATE, CUDE_EXPOSUREDATE, " +
                                            "CUDE_CASHSECURITY ) VALUES ";

            foreach (CustomerDetail customerDetailInfoObj in customerDetailListObj)
            {
                queryString += "(" + customerDetailInfoObj.CustomerId + ",'" + EscapeCharacter(customerDetailInfoObj.CustomerLLId) + "','" + EscapeCharacter(customerDetailInfoObj.Facility) + "','" +
                                customerDetailInfoObj.Flag + "'," + customerDetailInfoObj.OutStanding + "," + customerDetailInfoObj.OrginalLimit + "," +
                                customerDetailInfoObj.RePayment + ",'" + customerDetailInfoObj.DisbursDate.ToString("yyyy-MM-dd") + "','" + customerDetailInfoObj.ExposureDate.ToString("yyyy-MM-dd") + "'," +
                                customerDetailInfoObj.CashSecurity + "),";
            }
            queryString = queryString.Substring(0, queryString.Length - 1);
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(queryString);
                if (insertRow > 0)
                {
                    return (int)InsertUpdateFlag.INSERT;
                }
                else
                {
                    return (int)InsertUpdateFlag.INSERTERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }
        #endregion

        #region UpdateCustomerDetailInformation(List<CustomerDetail> customerDetailInformationListObj)
        /// <summary>
        /// This method provide update customer detail information.
        /// </summary>
        /// <param name="customerDetailInformationListObj">Gets customer detail list object.</param>
        /// <returns>Returns positive value if update.</returns>
        private int UpdateCustomerDetailInformation(List<CustomerDetail> customerDetailInformationListObj)
        {
            int updateRow = -1;
            bool IsUpdated = false;
            string queryString = "";
            try
            {
                foreach (CustomerDetail cutomerDetailInformationObj in customerDetailInformationListObj)
                {
                    queryString = String.Format("UPDATE t_pluss_customerdetails SET " +
                                                    "CUDE_FACILITY = '{0}' , CUDE_FLAG = '{1}', CUDE_OUTSTANDING = {2}, CUDE_ORGINALLIMIT = {3}, CUDE_REPAYMENT = {4} , " +
                                                    "CUDE_DISBURSEDATE = '{5}' , CUDE_EXPOSUREDATE = '{6}' , CUDE_CASHSECURITY = {7} " +
                                                    "WHERE CUDE_ID = {8} ",
                                                    EscapeCharacter(cutomerDetailInformationObj.Facility), cutomerDetailInformationObj.Flag, cutomerDetailInformationObj.OutStanding,
                                                    cutomerDetailInformationObj.OrginalLimit, cutomerDetailInformationObj.RePayment, cutomerDetailInformationObj.DisbursDate.ToString("yyyy-MM-dd"),
                                                    cutomerDetailInformationObj.ExposureDate.ToString("yyyy-MM-dd"), cutomerDetailInformationObj.CashSecurity,
                                                    cutomerDetailInformationObj.Id);
                    updateRow = DbQueryManager.ExecuteNonQuery(queryString);
                    if (updateRow > 0)
                    {
                        IsUpdated = true;
                    }
                }

                if (IsUpdated)
                {
                    return (int)InsertUpdateFlag.UPDATE;
                }
                else
                {
                    return (int)InsertUpdateFlag.UPDATEERROR;
                }
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
        #endregion
        #endregion

        //private int InsertCustomerInformation(Customer customerObj)
        //{
        //    int insertRow = -1;
        //    string queryString = String.Format("INSERT INTO t_pluss_customer (CUST_LLID, CUST_NAME, CUST_MASTERNO, CUST_PDCSLFORM, CUST_CCPLBUDLEOFFER, " +
        //                                        "CUST_OTHERDIRSLOAN, CUST_FIRSTPAYMENT1, CUST_OTHERSTATEMENT1B, CUST_OTHERSTATEMENT1A, " +
        //                                        "CUST_OTHERSTATEMENT2B, CUST_OTHERSTATEMENT2A, CUST_OTHERSTATEMENT3A, " +
        //                                        "CUST_OTHERSTATEMENT4B, CUST_OTHERSTATEMENT4A, CUST_OTHERSTATEMENT5B, CUST_OTHERSTATEMENT5A, CUST_MOB12, " +
        //                                        "CUST_TOPUPAMOUNT1, CUST_OUTSTANDING, CUST_TOPUPDATE1, CUST_TOPUPAMOUNT2, CUST_TOPUPDATE2, " +
        //                                        "CUST_TOPUPAMOUNT3, CUST_TOPUPDATE3) VALUES({0}, '{1}', '{2}','{3}', {4}, {5}, {6}, '{7}','{8}','{9}','{10}', " +
        //                                        "'{11}','{12}','{13}','{14}','{15}','{16}', {17}, {18},{19},'{20}', {21},'{22}',{23})", +
        //                                        EscapeCharacter(customerObj.LLId), EscapeCharacter(customerObj.Name), EscapeCharacter(customerObj.MasterNo), EscapeCharacter(customerObj.PDC),
        //                                        customerObj.CCPLBundleOffer, customerObj.OtherDirLoan, customerObj.FirstPayment, EscapeCharacter(customerObj.OtherStatementBank1), EscapeCharacter(customerObj.OtherStatementAcc1),
        //                                        EscapeCharacter(customerObj.OtherStatementBank2), EscapeCharacter(customerObj.OtherStatementAcc2), EscapeCharacter(customerObj.OtherStatementBank3), EscapeCharacter(customerObj.OtherStatementAcc3), EscapeCharacter(customerObj.OtherStatementBank4),
        //                                        EscapeCharacter(customerObj.OtherStatementAcc4), EscapeCharacter(customerObj.OtherStatementBank5), EscapeCharacter(customerObj.OtherStatementAcc5), customerObj.Mob12,
        //                                        customerObj.TopUpAmount1, customerObj.OutStanding, customerObj.TopUpDate1, customerObj.TopUpAmount2, customerObj.TopUpDate2,
        //                                        customerObj.TopUpAmount3, customerObj.TopUpDate3);
        //    try
        //    {
        //        insertRow = DbQueryManager.ExecuteNonQuery(queryString);
        //        if (insertRow > 0)
        //        {
        //            return (int)InsertUpdateFlag.INSERT;
        //        }
        //        else
        //        {
        //            return (int)InsertUpdateFlag.INSERTERROR;
        //        }
        //    }
        //    catch
        //    {
        //        return (int)InsertUpdateFlag.INSERTERROR;
        //    }
        //}

        //private int UpdateCustomerInformation(Customer customerObj)
        //{
        //    int updateRow = -1;
        //    string queryString = String.Format("UPDATE t_pluss_customer SET " +
        //                                        "CUST_LLID = '{0}', CUST_NAME = '{1}', CUST_MASTERNO = '{2}', CUST_PDCSLFORM = '{3}', CUST_CCPLBUDLEOFFER = {4}, " +
        //                                        "CUST_OTHERDIRSLOAN = {5}, CUST_FIRSTPAYMENT1 = {6}, CUST_OTHERSTATEMENT1B = '{7}', CUST_OTHERSTATEMENT1A = '{8}', " +
        //                                        "CUST_OTHERSTATEMENT2B = '{9}', CUST_OTHERSTATEMENT2A = '{10}', CUST_OTHERSTATEMENT3A = '{11}', " +
        //                                        "CUST_OTHERSTATEMENT4B = '{12}', CUST_OTHERSTATEMENT4A = '{13}', CUST_OTHERSTATEMENT5B = '{14}', CUST_OTHERSTATEMENT5A = '{15}', CUST_MOB12 = {16}, " +
        //                                        "CUST_TOPUPAMOUNT1 = {17}, CUST_OUTSTANDING = {18}, CUST_TOPUPDATE1 = '{19}', CUST_TOPUPAMOUNT2 = {20}, CUST_TOPUPDATE2 = '{21}', " +
        //                                        "CUST_TOPUPAMOUNT3 = {22}, CUST_TOPUPDATE3 = '{23}' " +
        //                                        "WHERE CUST_ID = {24} ",
        //                                        EscapeCharacter(customerObj.LLId), EscapeCharacter(customerObj.Name), EscapeCharacter(customerObj.MasterNo), EscapeCharacter(customerObj.PDC),
        //                                        customerObj.CCPLBundleOffer, customerObj.OtherDirLoan, customerObj.FirstPayment, EscapeCharacter(customerObj.OtherStatementBank1), EscapeCharacter(customerObj.OtherStatementAcc1),
        //                                        EscapeCharacter(customerObj.OtherStatementBank2), EscapeCharacter(customerObj.OtherStatementAcc2), EscapeCharacter(customerObj.OtherStatementBank3), EscapeCharacter(customerObj.OtherStatementAcc3), EscapeCharacter(customerObj.OtherStatementBank4),
        //                                        EscapeCharacter(customerObj.OtherStatementAcc4), EscapeCharacter(customerObj.OtherStatementBank5), EscapeCharacter(customerObj.OtherStatementAcc5), customerObj.Mob12,
        //                                        customerObj.TopUpAmount1, customerObj.OutStanding, customerObj.TopUpDate1, customerObj.TopUpAmount2, customerObj.TopUpDate2,
        //                                        customerObj.TopUpAmount3, customerObj.TopUpDate3, customerObj.Id);
        //    try
        //    {
        //        updateRow = DbQueryManager.ExecuteNonQuery(queryString);
        //        if (updateRow > 0)
        //        {
        //            return (int)InsertUpdateFlag.UPDATE;
        //        }
        //        else
        //        {
        //            return (int)InsertUpdateFlag.UPDATEERROR;
        //        }
        //    }
        //    catch
        //    {
        //        return (int)InsertUpdateFlag.UPDATEERROR;
        //    }
        //}
        //#endregion

        #region IsExistsCustomerInfo(Int32 customerId)
        /// <summary>
        /// This method provide customer exists or not.
        /// </summary>
        /// <param name="llid">Get customer Id</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsCustomerInfo(Int32 customerId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_customer WHERE CUST_ID = {0}", customerId);
            object customerObj;
            try
            {
                customerObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(customerObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region IsExistsCustomerDetailInfo(Int32 customerDetailId)
        /// <summary>
        /// This method provide customer detail information exists or not.
        /// </summary>
        /// <param name="customerDetailId">Get customer detail Id</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsCustomerDetailInfo(Int32 customerDetailId)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM t_pluss_customerdetails WHERE CUDE_ID = {0}", customerDetailId);
            object customerDetailObj;
            try
            {
                customerDetailObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(customerDetailObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region GetCustomerId(string llId)
        /// <summary>
        /// This method provide customer id.
        /// </summary>
        /// <param name="llid">Get llId</param>
        /// <returns>Returns a int value.</returns>
        public Int32 GetCustomerId(string llId)
        {
            Int32 returnValue = 0;
            string queryString = String.Format("SELECT CUST_ID FROM t_pluss_customer WHERE CUST_LLID = {0}", llId);
            Int32 customerId;
            try
            {
                customerId = Convert.ToInt32(DbQueryManager.ExecuteScalar(queryString));
                if (customerId > 0)
                {
                    returnValue = customerId;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region EscapeCharacter
        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                returnMessage = fieldString;
            }
            return returnMessage;
        }
        #endregion


        /// <summary>
        /// This method provide Exposure information.
        /// </summary>
        /// <param name="llid">Gets llid</param>
        /// <returns>Returns exposure list</returns>
        public List<double> GetExposure(string llid)
        {
            DataTable customerDataTableObj = null;
            List<double> exposure = new List<double>();

            string queryString = "select * from " +
                "(SELECT SUM(CUDE_OUTSTANDING) AS SecuredExposure " +
                "FROM t_pluss_customerdetails " +
                "WHERE CUDE_CUSTOMER_LLID = '" + llid + "' and CUDE_FLAG = 'S') as t1, " +
                "(SELECT SUM(CUDE_OUTSTANDING) AS UnSecuredExposure " +
                "FROM t_pluss_customerdetails " +
                "WHERE CUDE_CUSTOMER_LLID = '" + llid + "' and CUDE_FLAG = 'U') as t2 ";

            customerDataTableObj = DbQueryManager.GetTable(queryString);
            if (customerDataTableObj != null)
            {
                DataTableReader customerDataReaderObj = customerDataTableObj.CreateDataReader();

                while (customerDataReaderObj.Read())
                {
                    try
                    {
                        exposure.Add(Convert.ToInt64(customerDataReaderObj["SecuredExposure"]));
                    }
                    catch (Exception)
                    {
                        exposure.Add(Convert.ToInt64("0"));
                    }
                    try
                    {
                        exposure.Add(Convert.ToInt64(customerDataReaderObj["UnSecuredExposure"]));
                    }
                    catch (Exception)
                    {
                        exposure.Add(Convert.ToInt64("0"));
                    }
                }
                customerDataReaderObj.Close();
            }
            return exposure;
        }
        /// <summary>
        /// This method provide topup id.
        /// </summary>
        /// <param name="llId">Gets a llid.</param>
        /// <returns>Returns topup id.</returns>
        public int GetTopUpId(int llId)
        {
            int topUpId = 0;
            string queryString = "SELECT PL_LOANTYPE_ID FROM t_pluss_pl WHERE PL_LLID = " + llId;
            object topUpIdObj = DbQueryManager.ExecuteScalar(queryString);
            if (topUpIdObj != null)
            {
                topUpId = Convert.ToInt32(topUpIdObj.ToString());
            }
            return topUpId;
        }

    }
}
