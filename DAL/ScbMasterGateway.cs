﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using System.Data;
using System.Data.Common;
using Bat.Common;

namespace DAL
{
    /// <summary>
    /// ScbMasterGateway Class.
    /// </summary>
    public class ScbMasterGateway
    {
        private List<ScbMaster> scbMasterObjList = new List<ScbMaster>();
        private ScbMaster scbMasterObj = new ScbMaster();
        /// <summary>
        /// Cosntructor
        /// </summary>
        public ScbMasterGateway()
        {           
        }
         
        /// <summary>
        /// This method select all scb master info
        /// </summary>
        /// <returns>Returns a ScbMaster list object.</returns>
        public List<ScbMaster> GetAllScbMasterInfo()
        {
            DataTable scbMasterDataTableObj = null;

            string queryString = "select SCBM_ID, SCBM_LLID, SCBM_ACNO, SCBM_ACNAME, SCBM_LEDGERTOTAL, " +
	        "SCBM_LEDGERAVERAGE, SCBM_LEDGER5OPER, SCBM_LEDGER6MONTHTOTAL, " +
	        "SCBM_LEDGER6MONTHAVERAGE, SCBM_LEDGER6MONTH5OPER, " +
	        "SCBM_ADJUSTEDCTOTOTAL, SCBM_ADJUSTEDCTOAVERAGE, " +
	        "SCBM_ADJUSTEDCTO6MONTHTOTAL, SCBM_ADJUSTEDCTO6MONTHAVERAGE, " +
	        "SCBM_ENTRYDATE, SCBM_USER_ID " +
	        "from t_pluss_scbmaster ";

            scbMasterDataTableObj = DbQueryManager.GetTable(queryString);
            if (scbMasterDataTableObj != null)
            {
                DataTableReader scbMasterDataReaderObj = scbMasterDataTableObj.CreateDataReader();

                while (scbMasterDataReaderObj.Read())
                {
                    scbMasterObj = new ScbMaster();
                    scbMasterObj.scbmID = Convert.ToInt32(scbMasterDataReaderObj["SCBM_ID"]);
                    scbMasterObj.scbmLlid = Convert.ToString(scbMasterDataReaderObj["SCBM_LLID"]);

                    scbMasterObj.scbmAcno = Convert.ToString(scbMasterDataReaderObj["SCBM_ACNO"]);
                    scbMasterObj.scbmAcname = Convert.ToString(scbMasterDataReaderObj["SCBM_ACNAME"]);
                    scbMasterObj.scbmLedgertotal = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGERTOTAL"]);
                    scbMasterObj.scbmLedgeraverage = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGERAVERAGE"]);
                    scbMasterObj.scbmLedger5oper = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGER5OPER"]);
                    scbMasterObj.scbmLedger6monthtotal = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGER6MONTHTOTAL"]);
                    scbMasterObj.scbmLedger6monthaverage = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGER6MONTHAVERAGE"]);
                    scbMasterObj.scbmLedger6month5oper = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGER6MONTH5OPER"]);
                    scbMasterObj.scbmAdjustedctototal = Convert.ToDouble(scbMasterDataReaderObj["SCBM_ADJUSTEDCTOTOTAL"]);
                    scbMasterObj.scbmAdjustedctoaverage = Convert.ToDouble(scbMasterDataReaderObj["SCBM_ADJUSTEDCTOAVERAGE"]);
                    scbMasterObj.scbmAdjustedcto6monthtotal = Convert.ToDouble(scbMasterDataReaderObj["SCBM_ADJUSTEDCTO6MONTHTOTAL"]);
                    scbMasterObj.scbmAdjustedcto6monthaverage = Convert.ToDouble(scbMasterDataReaderObj["SCBM_ADJUSTEDCTO6MONTHAVERAGE"]);
                    scbMasterObj.scbmEntrydate = Convert.ToDateTime(scbMasterDataReaderObj["SCBM_ENTRYDATE"]);
                    scbMasterObj.scbmUserID = Convert.ToInt32(scbMasterDataReaderObj["SCBM_USER_ID"]);

                    scbMasterObjList.Add(scbMasterObj);
                }
                scbMasterDataReaderObj.Close();

            }
            return scbMasterObjList;
        }


        //public List<ScbMaster> GetAllScbMasterInfoByMasterId(int scbMasterId)
        //{
        //}

        /// <summary>
        /// This method select all scb master info by llid and accountnumber
        /// </summary>
        /// <param name="llId">Gets a llid</param>
        /// <param name="accountNumber">Gets a account no.</param>
        /// <returns>Returns a ScbMaster list object.</returns>
        public List<ScbMaster> GetAllScbMasterInfoByLlidAndAccountNumber(string llId, string accountNumber)
        {
            DataTable scbMasterDataTableObj = null;
            string queryString = String.Format(@"select SCBM_ID, SCBM_LLID, SCBM_ACNO, SCBM_ACNAME, SCBM_LEDGERTOTAL, SCBM_LEDGERAVERAGE, SCBM_LEDGER5OPER, SCBM_LEDGER6MONTHTOTAL, SCBM_LEDGER6MONTHAVERAGE, SCBM_LEDGER6MONTH5OPER, SCBM_ADJUSTEDCTOTOTAL, SCBM_ADJUSTEDCTOAVERAGE, SCBM_ADJUSTEDCTO6MONTHTOTAL, SCBM_ADJUSTEDCTO6MONTHAVERAGE, SCBM_ENTRYDATE, SCBM_USER_ID from t_pluss_scbmaster where SCBM_LLID = '{0}' and SCBM_ACNO = '{1}'", llId, accountNumber);

            //string queryString = "select SCBM_ID, SCBM_LLID, SCBM_ACNO, SCBM_ACNAME, SCBM_LEDGERTOTAL, " +
            //"SCBM_LEDGERAVERAGE, SCBM_LEDGER5OPER, SCBM_LEDGER6MONTHTOTAL, " +
            //"SCBM_LEDGER6MONTHAVERAGE, SCBM_LEDGER6MONTH5OPER, " +
            //"SCBM_ADJUSTEDCTOTOTAL, SCBM_ADJUSTEDCTOAVERAGE, " +
            //"SCBM_ADJUSTEDCTO6MONTHTOTAL, SCBM_ADJUSTEDCTO6MONTHAVERAGE, " +
            //"SCBM_ENTRYDATE, SCBM_USER_ID " +
            //"from t_pluss_scbmaster where SCBM_LLID = '" + llId + "' and SCBM_ACNO = '" + accountNumber;

            scbMasterDataTableObj = DbQueryManager.GetTable(queryString);
            if (scbMasterDataTableObj != null)
            {
                DataTableReader scbMasterDataReaderObj = scbMasterDataTableObj.CreateDataReader();

                while (scbMasterDataReaderObj.Read())
                {
                    scbMasterObj = new ScbMaster();
                    scbMasterObj.scbmID = Convert.ToInt32(scbMasterDataReaderObj["SCBM_ID"]);
                    scbMasterObj.scbmLlid = Convert.ToString(scbMasterDataReaderObj["SCBM_LLID"]);

                    scbMasterObj.scbmAcno = Convert.ToString(scbMasterDataReaderObj["SCBM_ACNO"]);
                    scbMasterObj.scbmAcname = Convert.ToString(scbMasterDataReaderObj["SCBM_ACNAME"]);
                    scbMasterObj.scbmLedgertotal = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGERTOTAL"]);
                    scbMasterObj.scbmLedgeraverage = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGERAVERAGE"]);
                    scbMasterObj.scbmLedger5oper = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGER5OPER"]);
                    scbMasterObj.scbmLedger6monthtotal = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGER6MONTHTOTAL"]);
                    scbMasterObj.scbmLedger6monthaverage = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGER6MONTHAVERAGE"]);
                    scbMasterObj.scbmLedger6month5oper = Convert.ToDouble(scbMasterDataReaderObj["SCBM_LEDGER6MONTH5OPER"]);
                    scbMasterObj.scbmAdjustedctototal = Convert.ToDouble(scbMasterDataReaderObj["SCBM_ADJUSTEDCTOTOTAL"]);
                    scbMasterObj.scbmAdjustedctoaverage = Convert.ToDouble(scbMasterDataReaderObj["SCBM_ADJUSTEDCTOAVERAGE"]);
                    scbMasterObj.scbmAdjustedcto6monthtotal = Convert.ToDouble(scbMasterDataReaderObj["SCBM_ADJUSTEDCTO6MONTHTOTAL"]);
                    scbMasterObj.scbmAdjustedcto6monthaverage = Convert.ToDouble(scbMasterDataReaderObj["SCBM_ADJUSTEDCTO6MONTHAVERAGE"]);
                    scbMasterObj.scbmEntrydate = Convert.ToDateTime(scbMasterDataReaderObj["SCBM_ENTRYDATE"]);
                    scbMasterObj.scbmUserID = Convert.ToInt32(scbMasterDataReaderObj["SCBM_USER_ID"]);

                    scbMasterObjList.Add(scbMasterObj);
                }
                scbMasterDataReaderObj.Close();

            }
            return scbMasterObjList;
        }

        /// <summary>
        /// This method provide the operation of insert scb master
        /// </summary>
        /// <param name="scbMasterObj">Gets a ScbMaster object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertScbMaster(ScbMaster scbMasterObj)
        {
            try
            {
                string sqlInsert = "insert into t_pluss_scbmaster " +
            "(SCBM_LLID, SCBM_ACNO, SCBM_ACNAME, " +
            "SCBM_LEDGERTOTAL, SCBM_LEDGERAVERAGE, SCBM_LEDGER5OPER, " +
            "SCBM_LEDGER6MONTHTOTAL, SCBM_LEDGER6MONTHAVERAGE, " +
            "SCBM_LEDGER6MONTH5OPER, SCBM_ADJUSTEDCTOTOTAL, " +
            "SCBM_ADJUSTEDCTOAVERAGE, SCBM_ADJUSTEDCTO6MONTHTOTAL, " +
            "SCBM_ADJUSTEDCTO6MONTHAVERAGE, SCBM_ENTRYDATE, " +
            "SCBM_USER_ID) " +
            "values " +
            "('" + scbMasterObj.scbmLlid + "', '" + scbMasterObj.scbmAcno + "', '" + scbMasterObj.scbmAcname + "', '" +
            scbMasterObj.scbmLedgertotal + "', '" + scbMasterObj.scbmLedgeraverage + "', '" + scbMasterObj.scbmLedger5oper + "', '" +
            scbMasterObj.scbmLedger6monthtotal + "', '" + scbMasterObj.scbmLedger6monthaverage + "', '" +
            scbMasterObj.scbmLedger6month5oper + "', '" + scbMasterObj.scbmAdjustedctototal + "', '" +
            scbMasterObj.scbmAdjustedctoaverage + "', '" + scbMasterObj.scbmAdjustedcto6monthtotal + "', '" +
            scbMasterObj.scbmAdjustedcto6monthaverage + "', '" + Convert.ToDateTime(scbMasterObj.scbmEntrydate.ToString()).ToString("yyyy-MM-dd") + "', '" +
            scbMasterObj.scbmUserID + "')";

                int insert = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                //queryText = sqlInsert.Replace('\'', '\"');
                //queryType = "Insert";
                return insert;
            }
            catch 
            {
                return 0;
            }
            return 0;
        }
        /// <summary>
        /// This method provide the operation of update scb master
        /// </summary>
        /// <param name="scbMasterObj">Gets a ScbMaster object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdateScbMaster(ScbMaster scbMasterObj)
        {
            try
            {
                string sqlInsert = "UPDATE t_pluss_scbmaster SET SCBM_LLID = "+scbMasterObj.scbmLlid+", " +
                "SCBM_ACNO = "+scbMasterObj.scbmAcno+", SCBM_ACNAME = "+scbMasterObj.scbmAcname+", SCBM_LEDGERTOTAL = "+scbMasterObj.scbmLedgertotal+", " +
                "SCBM_LEDGERAVERAGE = "+scbMasterObj.scbmLedgeraverage+", SCBM_LEDGER5OPER = "+scbMasterObj.scbmLedger5oper+", " +
                "SCBM_LEDGER6MONTHTOTAL = "+scbMasterObj.scbmLedger6monthtotal+", SCBM_LEDGER6MONTHAVERAGE = "+scbMasterObj.scbmLedger6monthaverage+", " +
                "SCBM_LEDGER6MONTH5OPER = "+scbMasterObj.scbmLedger6month5oper+", SCBM_ADJUSTEDCTOTOTAL = "+scbMasterObj.scbmAdjustedctototal+", " +
                "SCBM_ADJUSTEDCTOAVERAGE = "+scbMasterObj.scbmAdjustedctoaverage+", SCBM_ADJUSTEDCTO6MONTHTOTAL = "+scbMasterObj.scbmAdjustedcto6monthtotal+", " +
                "SCBM_ADJUSTEDCTO6MONTHAVERAGE = "+scbMasterObj.scbmAdjustedcto6monthaverage+", SCBM_ENTRYDATE = "+scbMasterObj.scbmEntrydate+", " +
                "SCBM_USER_ID = "+scbMasterObj.scbmUserID+" WHERE SCBM_ID = "+scbMasterObj.scbmID+")";

                int update = DbQueryManager.ExecuteNonQuery(sqlInsert);   // if sucessfull then 1
                //queryText = sqlInsert.Replace('\'', '\"');
                //queryType = "Update";
                return update;
            }
            catch
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// This method provide the operation of delete scb master
        /// </summary>
        ///<param name="masterId">Gets a master id.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeleteScbMasterObject(Int64 masterId)
        {
            string queryString = "DELETE FROM t_pluss_scbmaster WHERE SCBM_ID = " + masterId;

            try
            {
                int status = DbQueryManager.ExecuteNonQuery(queryString);
                return status;
            }
            catch { return 0; }
        }

        /// <summary>
        /// This method select all account number by llid
        /// </summary>
        /// <param name="llid">Gets a llid.</param>
        /// <returns>Returns a list of account no.</returns>
        public List<string> GetAllAccountNumberByLlid(string llid)
        {
            List<string> acNumberList = new List<string>();
            DataTable scbMasterDataTableObj = null;

            //string queryString = "select SCBM_ID, SCBM_LLID, SCBM_ACNO, SCBM_ACNAME, SCBM_LEDGERTOTAL, " +
            //"SCBM_LEDGERAVERAGE, SCBM_LEDGER5OPER, SCBM_LEDGER6MONTHTOTAL, " +
            //"SCBM_LEDGER6MONTHAVERAGE, SCBM_LEDGER6MONTH5OPER, " +
            //"SCBM_ADJUSTEDCTOTOTAL, SCBM_ADJUSTEDCTOAVERAGE, " +
            //"SCBM_ADJUSTEDCTO6MONTHTOTAL, SCBM_ADJUSTEDCTO6MONTHAVERAGE, " +
            //"SCBM_ENTRYDATE, SCBM_USER_ID " +
            //"from t_pluss_scbmaster where SCBM_LLID = " + llid;
            string queryString = "select SCBM_ACNO from t_pluss_scbmaster where SCBM_LLID = " + llid;

            scbMasterDataTableObj = DbQueryManager.GetTable(queryString);
            if (scbMasterDataTableObj != null)
            {
                DataTableReader scbMasterDataReaderObj = scbMasterDataTableObj.CreateDataReader();

                while (scbMasterDataReaderObj.Read())
                {
                    scbMasterObj = new ScbMaster();
                    
                    scbMasterObj.scbmAcno = Convert.ToString(scbMasterDataReaderObj["SCBM_ACNO"]);

                    acNumberList.Add(scbMasterObj.scbmAcno);
                }
                scbMasterDataReaderObj.Close();

            }
            return acNumberList;
        }
    }
}
