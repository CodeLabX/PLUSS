
using System;

namespace DAL
{
    /// <summary>
    /// Interface for Audit Trail
    /// </summary>
    public interface IAT
    {
        void AuditAndTraial(string activity,string description);
        string GetLoginUser();
    }
    /// <summary>
    /// Audit Trail extend from IAT
    /// </summary>
    public class AT : IAT
    {
        private readonly System.Web.UI.Page _ui;
        private readonly System.Web.UI.MasterPage _mui;
        public AT(System.Web.UI.Page ui)
        {
            _ui = ui;
        }
        public AT(System.Web.UI.MasterPage ui)
        {
            _mui = ui;
        }

        /// <summary>
        /// Save Audit Trail Information 
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="description"></param>
        public void AuditAndTraial(string activity,string description)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                
                var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                var strRole = System.Web.HttpContext.Current.Session["UserType"].ToString();
                string UserType = "OTHERS";
                if (strRole == "6" || strRole == "7")
                {
                    UserType = "SA";
                }

                var url = (_ui != null ? _ui.Request.RawUrl : _mui.Request.RawUrl);
                //string host ="test";//(_ui != null ? _ui.Request.ServerVariables["REMOTE_ADDR"] : _mui.Request.ServerVariables["REMOTE_ADDR"]);
                string host = System.Net.Dns.GetHostName();//System.Environment.MachineName;
                AuditTrial.SaveAudit(url,host, GetLoginUser(), activity, description + "-(" + userId + ")", UserType);
            }
          
        } 
        /// <summary>
        /// Save Audit Trail Information with old and new values
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="description"></param>
        public void AuditAndTraial(string activity,string description, string oldValue, string newValue)
        {
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                
                var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                var strRole = System.Web.HttpContext.Current.Session["UserType"].ToString();
                string UserType = "OTHERS";
                if (strRole=="6" || strRole=="7")
                {
                    UserType = "SA";
                }

                var url = (_ui != null ? _ui.Request.RawUrl : _mui.Request.RawUrl);
                //string host ="test";//(_ui != null ? _ui.Request.ServerVariables["REMOTE_ADDR"] : _mui.Request.ServerVariables["REMOTE_ADDR"]);
                string host = System.Net.Dns.GetHostName();//System.Environment.MachineName;
                AuditTrial.SaveAudit(url,host, GetLoginUser(), activity, description + "-(" + userId + ")", oldValue, newValue, UserType);
            }
          
        }
        /// <summary>
        /// Get current login user information
        /// </summary>
        /// <returns></returns>
        public string GetLoginUser()
        {
            return Convert.ToString(System.Web.HttpContext.Current.Session["UserId"]);
        }
    }
}