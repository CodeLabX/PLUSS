﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using Bat.Common;
using BusinessEntities.Report;

namespace DAL
{
    /// <summary>
    /// PLGateway Class.
    /// </summary>
    public class PLGateway
    {
        public DatabaseConnection dbMySQL = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        /// <summary>
        /// Constructor.
        /// </summary>
        public PLGateway()
        {
            dbMySQL = new DatabaseConnection();
        }
        /// <summary>
        /// This method select profession id.
        /// </summary>
        /// <param name="professionName">Gets a profession name.</param>
        /// <returns>Returns a profession id.</returns>
        public int SelectProfessionId(string professionName)
        {
            Int32 returnValue = 0;
            string mSQL = "SELECT PROF_ID FROM t_pluss_profession WHERE PROF_NAME LIKE '" + professionName + "%'";
            returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }
        /// <summary>
        /// This method select segment id.
        /// </summary>
        /// <param name="segment">Gets a segment name.</param>
        /// <returns>Returns a segemtn id.</returns>
        public int GetSegmentId(string segment)
        {
            Int32 returnValue = 0;
            string mSQL = "SELECT SEGM_ID FROM t_pluss_segment WHERE UCASE(SEGM_NAME)='"+segment.ToUpper().Trim()+"'";
            returnValue = Convert.ToInt32("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }
        /// <summary>
        /// This method provide pl id.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a pl id.</returns>
        public Int64 GetPLId(Int64 lLId)
        {
            Int64 returnValue = 0;
            string mSQL = "SELECT PL_ID FROM t_pluss_pl WHERE PL_LLID=" + lLId;
            returnValue = Convert.ToInt64("0" + DbQueryManager.ExecuteScalar(mSQL));
            return returnValue;
        }

        /// <summary>
        /// This method select pl info.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a PL object.</returns>
        public PL SelectPLInfo(Int64 lLId)
        {
            PL pLObj = null; 
          
            string queryString = "SELECT * FROM t_pluss_pl WHERE PL_LLID=" + lLId;
            DataTable data = DbQueryManager.GetTable(queryString);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
            while (xRs.Read())
            {
                pLObj =  new PL();
                pLObj.PlId = Convert.ToInt64(xRs["PL_ID"]);
                pLObj.PlLlid = Convert.ToInt64(xRs["PL_LLID"]);
                pLObj.PlProductId = Convert.ToInt32(xRs["PL_PRODUCT_ID"]);
                pLObj.PlSegmentId = Convert.ToInt32(xRs["PL_SEGMENT_ID"]);
                pLObj.PlLoantypeId = Convert.ToInt32(xRs["PL_LOANTYPE_ID"]);
                pLObj.PlIncomeacessmentmethodId = Convert.ToInt32(xRs["PL_INCOMEACESSMENTMETHOD_ID"]);
                pLObj.PlSectorId = Convert.ToInt32(xRs["PL_SECTOR_ID"]);
                pLObj.PlSubsectorId = Convert.ToInt32(xRs["PL_SUBSECTOR_ID"]);
                pLObj.PlCompanyId = Convert.ToInt32(xRs["PL_COMPANY_ID"]);
                pLObj.PlCompanycatagoryId = Convert.ToInt32(xRs["PL_COMPANYCATAGORY_ID"]);
                pLObj.PlCompanytypeId = Convert.ToInt32(xRs["PL_COMPANYTYPE_ID"]);
                pLObj.PlDocatagorizedId = Convert.ToInt32(xRs["PL_DOCATAGORIZED_ID"]);
                pLObj.PlCompanysegmentId = Convert.ToInt32(xRs["PL_COMPANYSEGMENT_ID"]);
                pLObj.PlDoctorCategory = Convert.ToString(xRs["PL_DOCTORCATEGORY"]);
                pLObj.PlIncome = Convert.ToDouble(xRs["PL_INCOME"]);
                pLObj.PlOwnership = Convert.ToDouble(xRs["PL_OWNERSHIP"]);
                pLObj.PlProfitmargin = Convert.ToDouble(xRs["PL_PROFITMARGIN"]);
                pLObj.PlOtherincomesourceId = Convert.ToInt32(xRs["PL_OTHERINCOMESOURCE_ID"]);
                pLObj.PlOtherincome = Convert.ToDouble(xRs["PL_OTHERINCOME"]);
                pLObj.PlNetincome = Convert.ToDouble(xRs["PL_NETINCOME"]);
                pLObj.PlDeclaredincome = Convert.ToDouble(xRs["PL_DECLAREDINCOME"]);
                pLObj.PlScbcreditcardlimit = Convert.ToDouble(xRs["PL_SCBCREDITCARDLIMIT"]);
                pLObj.PlIndustryprofitmarginrat = Convert.ToString(xRs["PL_INDUSTRYPROFITMARGINRAT"]);
                pLObj.PlPliplfloutstanding = Convert.ToDouble(xRs["PL_PLIPLFLOUTSTANDING"]);
                pLObj.PlMonthlyinterest = Convert.ToDouble(xRs["PL_MONTHLYINTEREST"]);
                pLObj.PlResiedentialstatusId = Convert.ToInt32(xRs["PL_RESIEDENTIALSTATUS_ID"]);
                pLObj.PlFlsecuityos = Convert.ToDouble(xRs["PL_FLSECUITYOS"]);
                pLObj.PlAveragebalance = Convert.ToDouble(xRs["PL_AVERAGEBALANCE"]);
                pLObj.PlEmiwithscbsourceId = Convert.ToInt32(xRs["PL_EMIWITHSCBSOURCE_ID"]);
                pLObj.PlEmiwithscb = Convert.ToDouble(xRs["PL_EMIWITHSCB"]);
                pLObj.PlOtheremisource = Convert.ToString(xRs["PL_OTHEREMISOURCE"]);
                pLObj.PlOtheremi = Convert.ToDouble(xRs["PL_OTHEREMI"]);
                pLObj.PlInstabuybalance = Convert.ToDouble(xRs["PL_INSTABUYBALANCE"]);
                pLObj.PlInstabuyinstallment = Convert.ToDouble(xRs["PL_INSTABUYINSTALLMENT"]);
                pLObj.PlBbpddmaxloanamound = Convert.ToDouble(xRs["PL_BBPDDMAXLOANAMOUND"]);
                pLObj.PlDbrmultiplier = Convert.ToInt32(xRs["PL_DBRMULTIPLIER"]);
                pLObj.PlDbrlevelId = Convert.ToInt32(xRs["PL_DBRLEVELID"]);
                pLObj.PlDbrmaxloanamount = Convert.ToDouble(xRs["PL_DBRMAXLOANAMOUNT"]);
                pLObj.PlAvgbalancmaxloanamount = Convert.ToDouble(xRs["PL_AVGBALANCMAXLOANAMOUNT"]);
                pLObj.PlMuemultiplier = Convert.ToDouble(xRs["PL_MUEMULTIPLIER"]);
                pLObj.PlMuemaxloanamount = Convert.ToDouble(xRs["PL_MUEMAXLOANAMOUNT"]);
                pLObj.PlAddOneId = Convert.ToInt32(xRs["PL_ADDONE_ID"]);
                pLObj.PlInterestrat = Convert.ToDouble(xRs["PL_INTERESTRAT"]);
                pLObj.PlTenure = Convert.ToInt32(xRs["PL_TENURE"]);
                pLObj.PlFirstpayment = Convert.ToInt32(xRs["PL_FIRSTPAYMENT"]);
                pLObj.PlSafetyplusId = Convert.ToInt32(xRs["PL_SAFETYPLUS_ID"]);
                pLObj.PlAppliedloanamount = Convert.ToDouble(xRs["PL_APPLIEDLOANAMOUNT"]);
                pLObj.PlAppropriatedloanamount = Convert.ToDouble(xRs["PL_APPROPRIATEDLOANAMOUNT"]);
                pLObj.PlProposedloanamount = Convert.ToDouble(xRs["PL_PROPOSEDLOANAMOUNT"]);
                pLObj.PlLevel = Convert.ToString(xRs["PL_LEVEL"]);
                pLObj.PlEmi = Convert.ToDouble(xRs["PL_EMI"]);
                pLObj.PlEmifactor = Convert.ToDouble(xRs["PL_EMIFACTOR"]);
                pLObj.PlDbr = Convert.ToDouble(xRs["PL_DBR"]);
                pLObj.PlMue = Convert.ToDouble(xRs["PL_MUE"]);
                pLObj.PlBancaLoanAmount = Convert.ToDouble(xRs["PL_BANCALOANAMOUNT"]);
                pLObj.PlBanca = Convert.ToDouble(xRs["PL_BANCA"]);
                pLObj.PlBancaDBR = Convert.ToDouble(xRs["PL_BANCADBR"]);
                pLObj.PlBancaWithoutDBR = Convert.ToDouble(xRs["PL_BANCAWITHOUTDBR"]);
                pLObj.PlBancaMUE = Convert.ToDouble(xRs["PL_BANCAMUE"]);
                pLObj.PlCurrentAge = Convert.ToString(xRs["PL_CURRENTAGE"]);
                pLObj.PlSecurity = Convert.ToString(xRs["PL_SECURITY"]);
                pLObj.PlRemarks = Convert.ToString(xRs["PL_REMARKS"]);
                pLObj.Status = Convert.ToInt32(xRs["PL_APPROVESTATUS"]);
                pLObj.UserId = Convert.ToInt32(xRs["PL_USER_ID"]);
                
            }
            xRs.Close();
            }

            return pLObj;
        }

        /// <summary>
        /// This method select particular pl info.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a PL list object.</returns>
        public List<PL> GetAllPL(Int64 lLId)
        {
            PL pLObj = null;
            List<PL> pLObjList = new List<PL>();
            ADODB.Recordset xRs = new ADODB.Recordset();
            string mSQL = "SELECT * FROM t_pluss_pl WHERE PL_LLID=" + lLId;
            xRs = dbMySQL.DbOpenRset(mSQL);
            while (!xRs.EOF)
            {
                pLObj = new PL();
                pLObj.PlId = Convert.ToInt64(xRs.Fields["PL_ID"].Value);
                pLObj.PlLlid = Convert.ToInt64(xRs.Fields["PL_LLID"].Value);
                pLObj.PlProductId = Convert.ToInt32(xRs.Fields["PL_PRODUCT_ID"].Value);
                pLObj.PlSegmentId = Convert.ToInt32(xRs.Fields["PL_SEGMENT_ID"].Value);
                pLObj.PlLoantypeId = Convert.ToInt32(xRs.Fields["PL_LOANTYPE_ID"].Value);
                pLObj.PlIncomeacessmentmethodId = Convert.ToInt32(xRs.Fields["PL_INCOMEACESSMENTMETHOD_ID"].Value);
                pLObj.PlSectorId = Convert.ToInt32(xRs.Fields["PL_SECTOR_ID"].Value);
                pLObj.PlSubsectorId = Convert.ToInt32(xRs.Fields["PL_SUBSECTOR_ID"].Value);
                pLObj.PlCompanyId = Convert.ToInt32(xRs.Fields["PL_COMPANY_ID"].Value);
                pLObj.PlCompanycatagoryId = Convert.ToInt32(xRs.Fields["PL_COMPANYCATAGORY_ID"].Value);
                pLObj.PlCompanytypeId = Convert.ToInt32(xRs.Fields["PL_COMPANYTYPE_ID"].Value);
                pLObj.PlDocatagorizedId = Convert.ToInt32(xRs.Fields["PL_DOCATAGORIZED_ID"].Value);
                pLObj.PlCompanysegmentId = Convert.ToInt32(xRs.Fields["PL_COMPANYSEGMENT_ID"].Value);
                pLObj.PlDoctorCategory = Convert.ToString(xRs.Fields["PL_DOCTORCATEGORY"].Value);
                pLObj.PlIncome = Convert.ToDouble(xRs.Fields["PL_INCOME"].Value);
                pLObj.PlOwnership = Convert.ToDouble(xRs.Fields["PL_OWNERSHIP"].Value);
                pLObj.PlProfitmargin = Convert.ToDouble(xRs.Fields["PL_PROFITMARGIN"].Value);
                pLObj.PlOtherincomesourceId = Convert.ToInt32(xRs.Fields["PL_OTHERINCOMESOURCE_ID"].Value);
                pLObj.PlOtherincome = Convert.ToDouble(xRs.Fields["PL_OTHERINCOME"].Value);
                pLObj.PlNetincome = Convert.ToDouble(xRs.Fields["PL_NETINCOME"].Value);
                pLObj.PlDeclaredincome = Convert.ToDouble(xRs.Fields["PL_DECLAREDINCOME"].Value);
                pLObj.PlScbcreditcardlimit = Convert.ToDouble(xRs.Fields["PL_SCBCREDITCARDLIMIT"].Value);
                pLObj.PlIndustryprofitmarginrat = Convert.ToString(xRs.Fields["PL_INDUSTRYPROFITMARGINRAT"].Value);
                pLObj.PlPliplfloutstanding = Convert.ToDouble(xRs.Fields["PL_PLIPLFLOUTSTANDING"].Value);
                pLObj.PlMonthlyinterest = Convert.ToDouble(xRs.Fields["PL_MONTHLYINTEREST"].Value);
                pLObj.PlResiedentialstatusId = Convert.ToInt32(xRs.Fields["PL_RESIEDENTIALSTATUS_ID"].Value);
                pLObj.PlFlsecuityos = Convert.ToDouble(xRs.Fields["PL_FLSECUITYOS"].Value);
                pLObj.PlAveragebalance = Convert.ToDouble(xRs.Fields["PL_AVERAGEBALANCE"].Value);
                pLObj.PlEmiwithscbsourceId = Convert.ToInt32(xRs.Fields["PL_EMIWITHSCBSOURCE_ID"].Value);
                pLObj.PlEmiwithscb = Convert.ToDouble(xRs.Fields["PL_EMIWITHSCB"].Value);
                pLObj.PlOtheremisource = Convert.ToString(xRs.Fields["PL_OTHEREMISOURCE"].Value);
                pLObj.PlOtheremi = Convert.ToDouble(xRs.Fields["PL_OTHEREMI"].Value);
                pLObj.PlInstabuybalance = Convert.ToDouble(xRs.Fields["PL_INSTABUYBALANCE"].Value);
                pLObj.PlInstabuyinstallment = Convert.ToDouble(xRs.Fields["PL_INSTABUYINSTALLMENT"].Value);
                pLObj.PlBbpddmaxloanamound = Convert.ToDouble(xRs.Fields["PL_BBPDDMAXLOANAMOUND"].Value);
                pLObj.PlDbrmultiplier = Convert.ToInt32(xRs.Fields["PL_DBRMULTIPLIER"].Value);
                pLObj.PlDbrlevelId = Convert.ToInt32(xRs.Fields["PL_DBRLEVELID"].Value);
                pLObj.PlDbrmaxloanamount = Convert.ToDouble(xRs.Fields["PL_DBRMAXLOANAMOUNT"].Value);
                pLObj.PlAvgbalancmaxloanamount = Convert.ToDouble(xRs.Fields["PL_AVGBALANCMAXLOANAMOUNT"].Value);
                pLObj.PlMuemultiplier = Convert.ToDouble(xRs.Fields["PL_MUEMULTIPLIER"].Value);
                pLObj.PlMuemaxloanamount = Convert.ToDouble(xRs.Fields["PL_MUEMAXLOANAMOUNT"].Value);
                pLObj.PlAddOneId = Convert.ToInt32(xRs.Fields["PL_ADDONE_ID"].Value);
                pLObj.PlInterestrat = Convert.ToDouble(xRs.Fields["PL_INTERESTRAT"].Value);
                pLObj.PlTenure = Convert.ToInt32(xRs.Fields["PL_TENURE"].Value);
                pLObj.PlFirstpayment = Convert.ToInt32(xRs.Fields["PL_FIRSTPAYMENT"].Value);
                pLObj.PlSafetyplusId = Convert.ToInt32(xRs.Fields["PL_SAFETYPLUS_ID"].Value);
                pLObj.PlAppliedloanamount = Convert.ToDouble(xRs.Fields["PL_APPLIEDLOANAMOUNT"].Value);
                pLObj.PlAppropriatedloanamount = Convert.ToDouble(xRs.Fields["PL_APPROPRIATEDLOANAMOUNT"].Value);
                pLObj.PlProposedloanamount = Convert.ToDouble(xRs.Fields["PL_PROPOSEDLOANAMOUNT"].Value);
                pLObj.PlLevel = Convert.ToString(xRs.Fields["PL_LEVEL"].Value);
                pLObj.PlEmi = Convert.ToDouble(xRs.Fields["PL_EMI"].Value);
                pLObj.PlEmifactor = Convert.ToDouble(xRs.Fields["PL_EMIFACTOR"].Value);
                pLObj.PlDbr = Convert.ToDouble(xRs.Fields["PL_DBR"].Value);
                pLObj.PlMue = Convert.ToDouble(xRs.Fields["PL_MUE"].Value);
                pLObj.PlBancaLoanAmount = Convert.ToDouble(xRs.Fields["PL_BANCALOANAMOUNT"].Value);
                pLObj.PlBanca = Convert.ToDouble(xRs.Fields["PL_BANCA"].Value);
                pLObj.PlBancaDBR = Convert.ToDouble(xRs.Fields["PL_BANCADBR"].Value);
                pLObj.PlBancaWithoutDBR = Convert.ToDouble(xRs.Fields["PL_BANCAWITHOUTDBR"].Value);
                pLObj.PlBancaMUE = Convert.ToDouble(xRs.Fields["PL_BANCAMUE"].Value);
                pLObj.PlCurrentAge = Convert.ToString(xRs.Fields["PL_CURRENTAGE"].Value);
                pLObj.PlSecurity = Convert.ToString(xRs.Fields["PL_SECURITY"].Value);
                pLObj.PlRemarks = Convert.ToString(xRs.Fields["PL_REMARKS"].Value);
                pLObj.Status = Convert.ToInt32(xRs.Fields["PL_APPROVESTATUS"].Value);
                pLObjList.Add(pLObj);
                xRs.MoveNext();
            }
            xRs.Close();
            return pLObjList;
        }
        /// <summary>
        /// This method provide insert or update operation for pl info.
        /// </summary>
        /// <param name="pLObj">Gets a PL object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendPLInToDB(PL pLObj)
        {
            PL existingPLObj = SelectPLInfo(pLObj.PlLlid);
            if (existingPLObj!=null)
            {
                pLObj.PlId = existingPLObj.PlId;
                return UpdatePL(pLObj);
            }
            else
            {
                return InsertPL(pLObj);
            }
        }
        /// <summary>
        /// This method provide insertion operation for pl.
        /// </summary>
        /// <param name="pLObj">Gets a PL object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertPL(PL pLObj)
        {
            try
            {
                string mSQL = "";
                mSQL = "INSERT INTO t_pluss_pl (PL_LLID,PL_PRODUCT_ID, PL_SEGMENT_ID, PL_LOANTYPE_ID," +
                       "PL_INCOMEACESSMENTMETHOD_ID, PL_SECTOR_ID," +
                       "PL_SUBSECTOR_ID, PL_COMPANY_ID, PL_COMPANYCATAGORY_ID," +
                       "PL_COMPANYTYPE_ID, PL_DOCATAGORIZED_ID, " +
                       "PL_COMPANYSEGMENT_ID, PL_DOCTORCATEGORY,PL_INCOME, PL_OWNERSHIP, " +
                       "PL_PROFITMARGIN, PL_OTHERINCOMESOURCE_ID, " +
                       "PL_OTHERINCOME, PL_NETINCOME, PL_DECLAREDINCOME, " +
                       "PL_SCBCREDITCARDLIMIT, PL_INDUSTRYPROFITMARGINRAT, " +
                       "PL_PLIPLFLOUTSTANDING, PL_MONTHLYINTEREST, " +
                       "PL_RESIEDENTIALSTATUS_ID, PL_FLSECUITYOS, " +
                       "PL_AVERAGEBALANCE, PL_EMIWITHSCBSOURCE_ID, " +
                       "PL_EMIWITHSCB, PL_OTHEREMISOURCE, PL_OTHEREMI, " +
                       "PL_INSTABUYBALANCE, PL_INSTABUYINSTALLMENT, " +
                       "PL_BBPDDMAXLOANAMOUND, PL_DBRMULTIPLIER, " +
                       "PL_DBRLEVELID, PL_DBRMAXLOANAMOUNT, " +
                       "PL_AVGBALANCMAXLOANAMOUNT, PL_MUEMULTIPLIER, " +
                       "PL_MUEMAXLOANAMOUNT, PL_ADDONE_ID, PL_INTERESTRAT, " +
                       "PL_TENURE, PL_FIRSTPAYMENT, PL_SAFETYPLUS_ID, " +
                       "PL_APPLIEDLOANAMOUNT, PL_APPROPRIATEDLOANAMOUNT," +
                       "PL_PROPOSEDLOANAMOUNT,PL_LEVEL, PL_EMI, PL_EMIFACTOR, " +
                       "PL_DBR, PL_MUE,PL_BANCALOANAMOUNT,PL_BANCA,PL_BANCADBR,PL_BANCAWITHOUTDBR,PL_BANCAMUE, " +
                       "PL_CURRENTAGE,PL_SECURITY,PL_REMARKS,PL_USER_ID,PL_ENTRYDATE,PL_APPROVESTATUS) VALUES (" + pLObj.PlLlid + "," + pLObj.PlProductId + "," + pLObj.PlSegmentId + "," + pLObj.PlLoantypeId + "," +
                       pLObj.PlIncomeacessmentmethodId + "," + pLObj.PlSectorId + "," + pLObj.PlSubsectorId + "," + pLObj.PlCompanyId + "," +
                       pLObj.PlCompanycatagoryId + "," + pLObj.PlCompanytypeId + "," + pLObj.PlDocatagorizedId + "," + pLObj.PlCompanysegmentId + ",'" + pLObj.PlDoctorCategory + "'," +
                       pLObj.PlIncome + "," + pLObj.PlOwnership + "," + pLObj.PlProfitmargin + "," + pLObj.PlOtherincomesourceId + "," + pLObj.PlOtherincome + "," +
                       pLObj.PlNetincome + "," + pLObj.PlDeclaredincome + "," + pLObj.PlScbcreditcardlimit + ",'" + pLObj.PlIndustryprofitmarginrat + "'," +
                       pLObj.PlPliplfloutstanding + "," + pLObj.PlMonthlyinterest + "," + pLObj.PlResiedentialstatusId + "," + pLObj.PlFlsecuityos + "," +
                       pLObj.PlAveragebalance + "," + pLObj.PlEmiwithscbsourceId + "," + pLObj.PlEmiwithscb + ",'" + pLObj.PlOtheremisource + "'," +
                       pLObj.PlOtheremi + "," + pLObj.PlInstabuybalance + "," + pLObj.PlInstabuyinstallment + "," + pLObj.PlBbpddmaxloanamound + "," +
                       pLObj.PlDbrmultiplier + "," + pLObj.PlDbrlevelId + "," + pLObj.PlDbrmaxloanamount + "," + pLObj.PlAvgbalancmaxloanamount + "," +
                       pLObj.PlMuemultiplier + "," + pLObj.PlMuemaxloanamount + "," + pLObj.PlAddOneId + "," + pLObj.PlInterestrat + "," + pLObj.PlTenure + "," + pLObj.PlFirstpayment + "," +
                       pLObj.PlSafetyplusId + "," + pLObj.PlAppliedloanamount + "," + pLObj.PlAppropriatedloanamount + "," + pLObj.PlProposedloanamount + ",'" + pLObj.PlLevel.Trim() + "'," +
                       pLObj.PlEmi + "," + pLObj.PlEmifactor + "," + pLObj.PlDbr + "," + pLObj.PlMue + "," + pLObj.PlBancaLoanAmount + "," + pLObj.PlBanca + "," +
                       pLObj.PlBancaDBR + "," + pLObj.PlBancaWithoutDBR + "," + pLObj.PlBancaMUE + ",'" + pLObj.PlCurrentAge + "','" + pLObj.PlSecurity + "','" + pLObj.PlRemarks + "'," + pLObj.UserId + ",'" + pLObj.EntryDate.ToString("yyyy-MM-dd") + "'," + pLObj.Status + ")";
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1) 
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }

        }
        /// <summary>
        /// This method provide update operation for pl.
        /// </summary>
        /// <param name="pLObj">Gets a PL object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdatePL(PL pLObj)
        {
            try
            {
                string mSQL = "";
                mSQL = "UPDATE t_pluss_pl SET " +
                       "PL_LLID=" + pLObj.PlLlid + "," +
                       "PL_PRODUCT_ID=" + pLObj.PlProductId + "," +
                       "PL_SEGMENT_ID=" + pLObj.PlSegmentId + "," +
                       "PL_LOANTYPE_ID=" + pLObj.PlLoantypeId + "," +
                       "PL_INCOMEACESSMENTMETHOD_ID=" + pLObj.PlIncomeacessmentmethodId + "," +
                       "PL_SECTOR_ID=" + pLObj.PlSectorId + "," +
                       "PL_SUBSECTOR_ID=" + pLObj.PlSubsectorId + "," +
                       "PL_COMPANY_ID=" + pLObj.PlCompanyId + "," +
                       "PL_COMPANYCATAGORY_ID=" + pLObj.PlCompanycatagoryId + "," +
                       "PL_COMPANYTYPE_ID=" + pLObj.PlCompanytypeId + "," +
                       "PL_DOCATAGORIZED_ID=" + pLObj.PlDocatagorizedId + ", " +
                       "PL_COMPANYSEGMENT_ID=" + pLObj.PlCompanysegmentId + "," +
                       "PL_DOCTORCATEGORY='" + pLObj.PlDoctorCategory + "'," +
                       "PL_INCOME=" + pLObj.PlIncome + "," +
                       "PL_OWNERSHIP=" + pLObj.PlOwnership + ", " +
                       "PL_PROFITMARGIN=" + pLObj.PlProfitmargin + "," +
                       "PL_OTHERINCOMESOURCE_ID=" + pLObj.PlOtherincomesourceId + "," +
                       "PL_OTHERINCOME=" + pLObj.PlOtherincome + "," +
                       "PL_NETINCOME=" + pLObj.PlNetincome + "," +
                       "PL_DECLAREDINCOME=" + pLObj.PlDeclaredincome + "," +
                       "PL_SCBCREDITCARDLIMIT=" + pLObj.PlScbcreditcardlimit + "," +
                       "PL_INDUSTRYPROFITMARGINRAT='" + pLObj.PlIndustryprofitmarginrat + "', " +
                       "PL_PLIPLFLOUTSTANDING=" + pLObj.PlPliplfloutstanding + "," +
                       "PL_MONTHLYINTEREST=" + pLObj.PlMonthlyinterest + ", " +
                       "PL_RESIEDENTIALSTATUS_ID=" + pLObj.PlResiedentialstatusId + "," +
                       "PL_FLSECUITYOS=" + pLObj.PlFlsecuityos + ", " +
                       "PL_AVERAGEBALANCE=" + pLObj.PlAveragebalance + "," +
                       "PL_EMIWITHSCBSOURCE_ID=" + pLObj.PlEmiwithscbsourceId + ", " +
                       "PL_EMIWITHSCB=" + pLObj.PlEmiwithscb + "," +
                       "PL_OTHEREMISOURCE='" + pLObj.PlOtheremisource + "'," +
                       "PL_OTHEREMI=" + pLObj.PlOtheremi + ", " +
                       "PL_INSTABUYBALANCE=" + pLObj.PlInstabuybalance + "," +
                       "PL_INSTABUYINSTALLMENT=" + pLObj.PlInstabuyinstallment + ", " +
                       "PL_BBPDDMAXLOANAMOUND=" + pLObj.PlBbpddmaxloanamound + "," +
                       "PL_DBRMULTIPLIER=" + pLObj.PlDbrmultiplier + ", " +
                       "PL_DBRLEVELID=" + pLObj.PlDbrlevelId + "," +
                       "PL_DBRMAXLOANAMOUNT=" + pLObj.PlDbrmaxloanamount + ", " +
                       "PL_AVGBALANCMAXLOANAMOUNT=" + pLObj.PlAvgbalancmaxloanamount + "," +
                       "PL_MUEMULTIPLIER=" + pLObj.PlMuemultiplier + ", " +
                       "PL_MUEMAXLOANAMOUNT=" + pLObj.PlMuemaxloanamount + "," +
                       "PL_ADDONE_ID=" + pLObj.PlAddOneId + "," +
                       "PL_INTERESTRAT=" + pLObj.PlInterestrat + ", " +
                       "PL_TENURE=" + pLObj.PlTenure + "," +
                       "PL_FIRSTPAYMENT=" + pLObj.PlFirstpayment + "," +
                       "PL_SAFETYPLUS_ID=" + pLObj.PlSafetyplusId + ", " +
                       "PL_APPLIEDLOANAMOUNT=" + pLObj.PlAppliedloanamount + "," +
                       "PL_APPROPRIATEDLOANAMOUNT=" + pLObj.PlAppropriatedloanamount + "," +
                       "PL_PROPOSEDLOANAMOUNT=" + pLObj.PlProposedloanamount + "," +
                       "PL_LEVEL='" + pLObj.PlLevel.Trim() + "'," +
                       "PL_EMI=" + pLObj.PlEmi + "," +
                       "PL_EMIFACTOR=" + pLObj.PlEmifactor + ", " +
                       "PL_DBR=" + pLObj.PlDbr + "," +
                       "PL_MUE=" + pLObj.PlMue + "," +
                       "PL_BANCALOANAMOUNT=" + pLObj.PlBancaLoanAmount + "," +
                       "PL_BANCA=" + pLObj.PlBanca + "," +
                       "PL_BANCADBR=" + pLObj.PlBancaDBR + "," +
                       "PL_BANCAWITHOUTDBR=" + pLObj.PlBancaWithoutDBR + "," +
                       "PL_BANCAMUE=" + pLObj.PlBancaMUE + "," +                       
                       "PL_CURRENTAGE='" + pLObj.PlCurrentAge + "'," +
                       "PL_SECURITY='" + pLObj.PlSecurity + "'," +
                       "PL_REMARKS='" + RemoveSpecialChar(pLObj.PlRemarks) + "'," +
                       "PL_USER_ID=" + pLObj.UserId + "," +
                       "PL_ENTRYDATE='" + pLObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                       "PL_APPROVESTATUS=" + pLObj.Status + " WHERE PL_ID=" + pLObj.PlId;
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        public string RemoveSpecialChar(string sMessage)
        {
            string returnMessage = "";
            if (!string.IsNullOrEmpty(sMessage))
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        /// <summary>
        /// This method select over draft info.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a PLOverDraft list object.</returns>
        public List<PLOverDraft> SelectOverDraft(Int64 lLId)
        {
            PLOverDraft pLOverDraftObj = null;
            List<PLOverDraft> pLOverDraftObjList = new List<PLOverDraft>();
            string mSQL = "SELECT * FROM t_pluss_overdraft WHERE OVER_LLID=" + lLId;
            var data = DbQueryManager.GetTable(mSQL);
            if (data != null)
            {
                var xRs = data.CreateDataReader();
                while (xRs.Read())
                {
                    pLOverDraftObj = new PLOverDraft();
                    pLOverDraftObj.Id = Convert.ToInt32(xRs["OVER_ID"]);
                    pLOverDraftObj.PLId = Convert.ToInt64(xRs["OVER_PL_ID"]);
                    pLOverDraftObj.LlId = Convert.ToInt64(xRs["OVER_LLID"]);
                    pLOverDraftObj.Limit = Convert.ToDouble(xRs["OVER_LIMIT"]);
                    pLOverDraftObj.Rate = Convert.ToDouble(xRs["OVER_RATE"]);
                    pLOverDraftObj.Q1 = Convert.ToDouble(xRs["OVER_Q1"]);
                    pLOverDraftObj.Q2 = Convert.ToDouble(xRs["OVER_Q2"]);
                    pLOverDraftObj.Q3 = Convert.ToDouble(xRs["OVER_Q3"]);
                    pLOverDraftObj.Q4 = Convert.ToDouble(xRs["OVER_Q4"]);
                    pLOverDraftObjList.Add(pLOverDraftObj);
                   
                }
                xRs.Close();
            }
    return pLOverDraftObjList;
        }
        /// <summary>
        /// This method provide the operation of insertion or update for over draft info.
        /// </summary>
        /// <param name="pLOverDraftObjList">Gets a PLOverDraft list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendOverDraftInToDB(List<PLOverDraft> pLOverDraftObjList)
        {
            List<PLOverDraft> insertOverDraftObjList = new List<PLOverDraft>();
            List<PLOverDraft> updateOverDraftObjList = new List<PLOverDraft>();
            int returnValue = 0;
            foreach (PLOverDraft tempPLOverDraftObj in pLOverDraftObjList)
            {
                if (tempPLOverDraftObj.OldId > 0)
                {
                    updateOverDraftObjList.Add(tempPLOverDraftObj);
                }
                else
                {
                    insertOverDraftObjList.Add(tempPLOverDraftObj);
                }
            }
            if (insertOverDraftObjList.Count > 0)
            {
                returnValue = InsertOverDraft(insertOverDraftObjList);
            }
            if (updateOverDraftObjList.Count > 0)
            {
                returnValue = UpdateOverDraft(updateOverDraftObjList);
            }
            return returnValue;
        }

        /// <summary>
        /// This method provide the operation of insertion for over draft.
        /// </summary>
        /// <param name="insertOverDraftObjList">Gets a PLOverDraft list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int InsertOverDraft(List<PLOverDraft> insertOverDraftObjList)
        {
            try
            {
                string mSQL = "INSERT INTO t_pluss_overdraft (OVER_PL_ID, OVER_LLID,OVER_LIMIT, OVER_RATE,OVER_Q1, OVER_Q2, OVER_Q3,OVER_Q4) VALUES";
                foreach (PLOverDraft pLOverDraftObj in insertOverDraftObjList)
                {
                    mSQL = mSQL + "(" +
                          pLOverDraftObj.PLId + "," +
                          pLOverDraftObj.LlId + "," +
                          pLOverDraftObj.Limit + "," +
                          pLOverDraftObj.Rate + "," +
                          pLOverDraftObj.Q1 + "," +
                          pLOverDraftObj.Q2 + "," +
                          pLOverDraftObj.Q3 + "," +
                          pLOverDraftObj.Q4 + "),";
                }
                mSQL = mSQL.Substring(0, mSQL.Length - 1);
                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method provide the operation of update for over draft.
        /// </summary>
        /// <param name="insertOverDraftObjList">Gets a PLOverDraft list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        private int UpdateOverDraft(List<PLOverDraft> updateOverDraftObjList)
        {
            bool flag = false;
            try
            {
                foreach (PLOverDraft pLOverDraftObj in updateOverDraftObjList)
                {
                    string mSQL = "";
                    mSQL = "UPDATE t_pluss_overdraft SET " +
                           "OVER_PL_ID=" + pLOverDraftObj.PLId + "," +
                           "OVER_LLID=" + pLOverDraftObj.LlId + "," +
                           "OVER_LIMIT=" + pLOverDraftObj.Limit + "," +
                           "OVER_RATE=" + pLOverDraftObj.Rate + "," +
                           "OVER_Q1=" + pLOverDraftObj.Q1 + "," +
                           "OVER_Q2=" + pLOverDraftObj.Q2 + "," +
                           "OVER_Q3=" + pLOverDraftObj.Q3 + "," +
                           "OVER_Q4=" + pLOverDraftObj.Q4 + " WHERE OVER_ID=" + pLOverDraftObj.OldId;
                    if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                    {
                        flag = true;
                    }
                }
                if (flag == true)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }

        /// <summary>
        /// This method provide operation of deletion for pl info.
        /// </summary>
        /// <param name="pLObj">Gets a PL object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeletePLinToDB(PL pLObj)
        {
            return 0;
        }
    }
}
