﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bat.Common;
using BusinessEntities;
using System.Data;

namespace DAL
{
    public class JointApplicationGateway
    {
        //private JoinApplicantInformation joinApplicantInformationObj;

        public JoinApplicantInformation GetAllJointApplicantInformation(Int32 llid)
        {
            JoinApplicantInformation joinApplicantInformationObj =null; //= new JoinApplicantInformation();
            string queryString = String.Format("SELECT LOAN_APPLI_ID, J_APP_NM, J_APP_FATHE_NM, J_APP_MOTHE_NM, isnull((CASE J_APP_DOB WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J_APP_DOB AS char) END),'') AS J_APP_DOB, J_APP_EMPLO, isnull((CASE J_APP_EDUCA_LEVEL WHEN '' THEN '' ELSE J_APP_EDUCA_LEVEL END),'') AS J_APP_EDUCA_LEVEL, isnull(J_APP_AC_NO,0) AS J_APP_AC_NO, isnull(J_APP_AC_NO2,0) AS J_APP_AC_NO2, isnull(J_APP_AC_NO3,0) AS J_APP_AC_NO3, isnull(J_APP_AC_NO4,0) AS J_APP_AC_NO4, isnull(J_APP_AC_NO5,0) AS J_APP_AC_NO5, J_APP_CC_NO, J_APP_CC_NO2, J_APP_CC_NO3, J_APP_CC_NO4, J_APP_CC_NO5, J_TIN, J_APP_CONTACT_NO1, J_APP_CONTACT_NO2, J_APP_CONTACT_NO3, J_APP_CONTACT_NO4, J_ID_TYPE1, J_ID_TYPE2, J_ID_TYPE3, J_ID_TYPE4, J_ID_TYPE5, J_ID_NO1, J_ID_NO2, J_ID_NO3, J_ID_NO4,J_ID_NO5, isnull((CASE J_ID_EXPIR1 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J_ID_EXPIR1 AS char) END),'') AS J_ID_EXPIR1, isnull((CASE J_ID_EXPIR2 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J_ID_EXPIR2 AS CHAR) END),'') AS J_ID_EXPIR2, isnull((CASE J_ID_EXPIR3 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J_ID_EXPIR3 AS CHAR) END),'') AS J_ID_EXPIR3, isnull((CASE J_ID_EXPIR4 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J_ID_EXPIR4 AS CHAR) END),'') AS J_ID_EXPIR4, isnull((CASE J_ID_EXPIR5 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J_ID_EXPIR5 AS CHAR) END),'') AS J_ID_EXPIR5, J2_APP_NM, isnull(J2_APP_AC_NO1,0) AS J2_APP_AC_NO1, isnull(J2_APP_AC_NO2,0) AS J2_APP_AC_NO2, isnull(J2_APP_AC_NO3,0) AS J2_APP_AC_NO3, isnull(J2_APP_AC_NO4,0) AS J2_APP_AC_NO4, isnull(J2_APP_AC_NO5,0) AS J2_APP_AC_NO5, J2_TIN, J2_APP_CC_NO, J2_APP_CC_NO2, J2_APP_CC_NO3, J2_APP_CC_NO4, J2_APP_CC_NO5, J2_APP_FATHE_NM, J2_APP_MOTHE_NM, isnull((CASE J2_APP_DOB WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J2_APP_DOB AS CHAR) END),'') AS J2_APP_DOB, isnull((CASE J2_APP_EDUCA_LEVEL WHEN '' THEN '' ELSE J2_APP_EDUCA_LEVEL END),'') AS J2_APP_EDUCA_LEVEL, J2_APP_CONTACT_NO1, J2_APP_CONTACT_NO2, J2_APP_CONTACT_NO3, J2_APP_CONTACT_NO4, J2_APP_EMPLO, J2_ID_TYPE1, J2_ID_TYPE2, J2_ID_TYPE3, J2_ID_TYPE4,J2_ID_TYPE5, J2_ID_NO1, J2_ID_NO2, J2_ID_NO3, J2_ID_NO4, J2_ID_NO5, isnull((CASE J2_ID_EXPIR1 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J2_ID_EXPIR1 AS CHAR) END),'') AS J2_ID_EXPIR1, isnull((CASE J2_ID_EXPIR2 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J2_ID_EXPIR2 AS CHAR) END),'') AS J2_ID_EXPIR2, isnull((CASE J2_ID_EXPIR3 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J2_ID_EXPIR3 AS CHAR) END),'') AS J2_ID_EXPIR3, isnull((CASE J2_ID_EXPIR4 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J2_ID_EXPIR4 AS CHAR) END),'') AS J2_ID_EXPIR4, isnull((CASE J2_ID_EXPIR5 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J2_ID_EXPIR5 AS CHAR) END),'') AS J2_ID_EXPIR5, J3_APP_NM, isnull(J3_APP_AC_NO1,0) AS J3_APP_AC_NO1, isnull(J3_APP_AC_NO2,0) AS J3_APP_AC_NO2, isnull(J3_APP_AC_NO3,0) AS J3_APP_AC_NO3, isnull(J3_APP_AC_NO4,0) AS J3_APP_AC_NO4, isnull(J3_APP_AC_NO5,0) AS J3_APP_AC_NO5, J3_TIN, J3_APP_CC_NO, J3_APP_CC_NO2, J3_APP_CC_NO3, J3_APP_CC_NO4, J3_APP_CC_NO5, J3_APP_FATHE_NM, J3_APP_MOTHE_NM, isnull((CASE J3_APP_DOB WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J3_APP_DOB AS CHAR) END),'') AS J3_APP_DOB, isnull((CASE J3_APP_EDUCA_LEVEL WHEN '' THEN '' ELSE J3_APP_EDUCA_LEVEL END),'') AS J3_APP_EDUCA_LEVEL, J3_APP_CONTACT_NO1, J3_APP_CONTACT_NO2, J3_APP_CONTACT_NO3, J3_APP_CONTACT_NO4, J3_APP_EMPLO, J3_ID_TYPE1, J3_ID_TYPE2, J3_ID_TYPE3, J3_ID_TYPE4, J3_ID_TYPE5, J3_ID_NO1, J3_ID_NO2, J3_ID_NO3, J3_ID_NO4, J3_ID_NO5, isnull((CASE J3_ID_EXPIR1 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J3_ID_EXPIR1 AS CHAR) END),'') AS J3_ID_EXPIR1, isnull((CASE J3_ID_EXPIR2 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J3_ID_EXPIR2 AS CHAR) END),'') AS J3_ID_EXPIR2, isnull((CASE J3_ID_EXPIR3 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J3_ID_EXPIR3 AS CHAR) END),'') AS J3_ID_EXPIR3, isnull((CASE J3_ID_EXPIR4 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J3_ID_EXPIR4 AS CHAR) END),'') AS J3_ID_EXPIR4, isnull((CASE J3_ID_EXPIR5 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J3_ID_EXPIR5 AS CHAR) END),'') AS J3_ID_EXPIR5, J4_APP_NM, isnull(J4_APP_AC_NO1,0) AS J4_APP_AC_NO1, isnull(J4_APP_AC_NO2,0) AS J4_APP_AC_NO2, isnull(J4_APP_AC_NO3,0) AS J4_APP_AC_NO3, isnull(J4_APP_AC_NO4,0) AS J4_APP_AC_NO4, isnull(J4_APP_AC_NO5,0) AS J4_APP_AC_NO5, J4_TIN, J4_APP_CC_NO, J4_APP_CC_NO2, J4_APP_CC_NO3, J4_APP_CC_NO4, J4_APP_CC_NO5, J4_APP_FATHE_NM, J4_APP_MOTHE_NM, isnull((CASE J4_APP_DOB WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J4_APP_DOB AS CHAR) END),'') AS J4_APP_DOB, isnull((CASE J4_APP_EDUCA_LEVEL WHEN '' THEN '' ELSE J4_APP_EDUCA_LEVEL END),'') AS J4_APP_EDUCA_LEVEL, J4_APP_CONTACT_NO1, J4_APP_CONTACT_NO2, J4_APP_CONTACT_NO3, J4_APP_CONTACT_NO4, J4_APP_EMPLO, J4_ID_TYPE1, J4_ID_TYPE2, J4_ID_TYPE3, J4_ID_TYPE4, J4_ID_TYPE5, J4_ID_NO1, J4_ID_NO2, J4_ID_NO3, J4_ID_NO4, J4_ID_NO5, isnull((CASE J4_ID_EXPIR1 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J4_ID_EXPIR1 AS CHAR) END),'') AS J4_ID_EXPIR1, isnull((CASE J4_ID_EXPIR2 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J4_ID_EXPIR2 AS CHAR) END),'') AS J4_ID_EXPIR2, isnull((CASE J4_ID_EXPIR3 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J4_ID_EXPIR3 AS CHAR) END),'') AS J4_ID_EXPIR3, isnull((CASE J4_ID_EXPIR4 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J4_ID_EXPIR4 AS CHAR) END),'') AS J4_ID_EXPIR4, isnull((CASE J4_ID_EXPIR5 WHEN '' THEN '' WHEN '' THEN '' ELSE CAST(J4_ID_EXPIR5 AS CHAR) END),'') AS J4_ID_EXPIR5  FROM loan_app_info " +
                                                "WHERE LOAN_APPLI_ID = {0} ", llid);

            DataTable jointApplicantDataTableObj = DbQueryManager.GetTable(queryString);
            if (jointApplicantDataTableObj != null)
            {
                DataTableReader jointApplicantDataTableReaderObj = jointApplicantDataTableObj.CreateDataReader();
                if (jointApplicantDataTableReaderObj != null)
                {
                    while (jointApplicantDataTableReaderObj.Read())
                    {
                        joinApplicantInformationObj = new JoinApplicantInformation();
                        joinApplicantInformationObj.JointApplicantLLId = Convert.ToInt64(jointApplicantDataTableReaderObj["LOAN_APPLI_ID"].ToString());

                        //Joint applicant 1
                        joinApplicantInformationObj.JointApp1Name = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_NM"]);
                        joinApplicantInformationObj.J1FatherName = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_FATHE_NM"]);
                        joinApplicantInformationObj.J1MotherName = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_MOTHE_NM"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J_APP_DOB"].ToString()))
                        {
                            joinApplicantInformationObj.J1DOB = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_APP_DOB"]);
                        }
               
                        joinApplicantInformationObj.J1TinNo = Convert.ToString(jointApplicantDataTableReaderObj["J_TIN"]);

                        joinApplicantInformationObj.J1SCBAccNo1 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO"]);
                        joinApplicantInformationObj.J1SCBAccNo2 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO2"].ToString());
                        joinApplicantInformationObj.J1SCBAccNo3 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO3"].ToString());
                        joinApplicantInformationObj.J1SCBAccNo4 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO4"].ToString());
                        joinApplicantInformationObj.J1SCBAccNo5 = Convert.ToInt64(jointApplicantDataTableReaderObj["J_APP_AC_NO5"].ToString());

                        joinApplicantInformationObj.J1CrdtCrdNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO"]);
                        joinApplicantInformationObj.J1CrdtCrdNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO2"]);
                        joinApplicantInformationObj.J1CrdtCrdNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO3"]);
                        joinApplicantInformationObj.J1CrdtCrdNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO4"]);
                        joinApplicantInformationObj.J1CrdtCrdNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CC_NO5"]);

                        joinApplicantInformationObj.JApp1EducationLevel = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_EDUCA_LEVEL"]);
                        joinApplicantInformationObj.JApp1Company = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_EMPLO"]);

                        joinApplicantInformationObj.JApp1ContactNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO1"]);
                        joinApplicantInformationObj.JApp1ContactNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO2"]);
                        joinApplicantInformationObj.JApp1ContactNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO3"]);
                        joinApplicantInformationObj.JApp1ContactNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO4"]);
                        //joinApplicantInformationObj.JApp1ContactNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J_APP_CONTACT_NO5"]);

                        joinApplicantInformationObj.JApp1IdType1 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE1"]);
                        joinApplicantInformationObj.JApp1Id1 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO1"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J_ID_EXPIR1"].ToString()))
                        {
                            joinApplicantInformationObj.JApp1IdExpire1 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR1"]);
                        }
                        joinApplicantInformationObj.JApp1IdType2 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE2"]);
                        joinApplicantInformationObj.JApp1Id2 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO2"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J_ID_EXPIR2"].ToString()))
                        {
                            joinApplicantInformationObj.JApp1IdExpire2 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR2"]);
                        }
                        joinApplicantInformationObj.JApp1IdType3 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE3"]);
                        joinApplicantInformationObj.JApp1Id3 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO3"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J_ID_EXPIR3"].ToString()))
                        {
                            joinApplicantInformationObj.JApp1IdExpire3 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR3"]);
                        }
                        joinApplicantInformationObj.JApp1IdType4 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE4"]);
                        joinApplicantInformationObj.JApp1Id4 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO4"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J_ID_EXPIR4"].ToString()))
                        {
                            joinApplicantInformationObj.JApp1IdExpire4 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR4"]);
                        }
                        joinApplicantInformationObj.JApp1IdType5 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_TYPE5"]);
                        joinApplicantInformationObj.JApp1Id5 = Convert.ToString(jointApplicantDataTableReaderObj["J_ID_NO5"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J_ID_EXPIR5"].ToString()))
                        {
                            joinApplicantInformationObj.JApp1IdExpire5 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J_ID_EXPIR5"]);
                        }
                        //End joint applicant 1

                        //Joint applicant 2
                        joinApplicantInformationObj.JointApp2Name = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_NM"]);
                        joinApplicantInformationObj.J2FatherName = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_FATHE_NM"]);
                        joinApplicantInformationObj.J2MotherName = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_MOTHE_NM"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J2_APP_DOB"].ToString()))
                        {
                            joinApplicantInformationObj.J2DOB = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_APP_DOB"]);
                        }
                        joinApplicantInformationObj.J2TinNo = Convert.ToString(jointApplicantDataTableReaderObj["J2_TIN"]);

                        joinApplicantInformationObj.J2SCBAccNo1 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO1"].ToString());
                        joinApplicantInformationObj.J2SCBAccNo2 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO2"].ToString());
                        joinApplicantInformationObj.J2SCBAccNo3 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO3"].ToString());
                        joinApplicantInformationObj.J2SCBAccNo4 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO4"].ToString());
                        joinApplicantInformationObj.J2SCBAccNo5 = Convert.ToInt64(jointApplicantDataTableReaderObj["J2_APP_AC_NO5"].ToString());

                        joinApplicantInformationObj.J2CrdtCrdNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO"]);
                        joinApplicantInformationObj.J2CrdtCrdNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO2"]);
                        joinApplicantInformationObj.J2CrdtCrdNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO3"]);
                        joinApplicantInformationObj.J2CrdtCrdNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO4"]);
                        joinApplicantInformationObj.J2CrdtCrdNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CC_NO5"]);

                        joinApplicantInformationObj.JApp2EducationLevel = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_EDUCA_LEVEL"]);
                        joinApplicantInformationObj.JApp2Company = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_EMPLO"]);

                        joinApplicantInformationObj.JApp2ContactNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO1"]);
                        joinApplicantInformationObj.JApp2ContactNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO2"]);
                        joinApplicantInformationObj.JApp2ContactNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO3"]);
                        joinApplicantInformationObj.JApp2ContactNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO4"]);
                        //joinApplicantInformationObj.JApp2ContactNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J2_APP_CONTACT_NO5"]);

                        joinApplicantInformationObj.JApp2IdType1 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE1"]);
                        joinApplicantInformationObj.JApp2Id1 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO1"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J2_ID_EXPIR1"].ToString()))
                        {
                            joinApplicantInformationObj.JApp2IdExpire1 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR1"]);
                        }
                        joinApplicantInformationObj.JApp2IdType2 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE2"]);
                        joinApplicantInformationObj.JApp2Id2 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO2"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J2_ID_EXPIR2"].ToString()))
                        {
                            joinApplicantInformationObj.JApp2IdExpire2 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR2"]);
                        }
                        joinApplicantInformationObj.JApp2IdType3 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE3"]);
                        joinApplicantInformationObj.JApp2Id3 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO3"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J2_ID_EXPIR3"].ToString()))
                        {
                            joinApplicantInformationObj.JApp2IdExpire3 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR3"]);
                        }
                        joinApplicantInformationObj.JApp2IdType4 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE4"]);
                        joinApplicantInformationObj.JApp2Id4 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO4"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J2_ID_EXPIR4"].ToString()))
                        {
                            joinApplicantInformationObj.JApp2IdExpire4 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR4"]);
                        }
                        joinApplicantInformationObj.JApp2IdType5 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_TYPE5"]);
                        joinApplicantInformationObj.JApp2Id5 = Convert.ToString(jointApplicantDataTableReaderObj["J2_ID_NO5"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J2_ID_EXPIR5"].ToString()))
                        {
                            joinApplicantInformationObj.JApp2IdExpire5 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J2_ID_EXPIR5"]);
                        }
                        //End joint applicant 2

                        //Joint applicant 3
                        joinApplicantInformationObj.JointApp3Name = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_NM"]);
                        joinApplicantInformationObj.J3FatherName = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_FATHE_NM"]);
                        joinApplicantInformationObj.J3MotherName = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_MOTHE_NM"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J3_APP_DOB"].ToString()))
                        {
                            joinApplicantInformationObj.J3DOB = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_APP_DOB"]);
                        }

                        joinApplicantInformationObj.J3TinNo = Convert.ToString(jointApplicantDataTableReaderObj["J3_TIN"]);

                        joinApplicantInformationObj.J3SCBAccNo1 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO1"].ToString());
                        joinApplicantInformationObj.J3SCBAccNo2 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO2"].ToString());
                        joinApplicantInformationObj.J3SCBAccNo3 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO3"].ToString());
                        joinApplicantInformationObj.J3SCBAccNo4 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO4"].ToString());
                        joinApplicantInformationObj.J3SCBAccNo5 = Convert.ToInt64(jointApplicantDataTableReaderObj["J3_APP_AC_NO5"].ToString());

                        joinApplicantInformationObj.J3CrdtCrdNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO"]);
                        joinApplicantInformationObj.J3CrdtCrdNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO2"]);
                        joinApplicantInformationObj.J3CrdtCrdNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO3"]);
                        joinApplicantInformationObj.J3CrdtCrdNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO4"]);
                        joinApplicantInformationObj.J3CrdtCrdNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CC_NO5"]);

                        joinApplicantInformationObj.JApp3EducationLevel = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_EDUCA_LEVEL"]);
                        joinApplicantInformationObj.JApp3Company = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_EMPLO"]);

                        joinApplicantInformationObj.JApp3ContactNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO1"]);
                        joinApplicantInformationObj.JApp3ContactNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO2"]);
                        joinApplicantInformationObj.JApp3ContactNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO3"]);
                        joinApplicantInformationObj.JApp3ContactNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO4"]);
                        //joinApplicantInformationObj.JApp3ContactNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J3_APP_CONTACT_NO5"]);

                        joinApplicantInformationObj.JApp3IdType1 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE1"]);
                        joinApplicantInformationObj.JApp3Id1 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO1"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J3_ID_EXPIR1"].ToString()))
                        {
                            joinApplicantInformationObj.JApp3IdExpire1 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR1"]);
                        }
                        joinApplicantInformationObj.JApp3IdType2 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE2"]);
                        joinApplicantInformationObj.JApp3Id2 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO2"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J3_ID_EXPIR2"].ToString()))
                        {
                            joinApplicantInformationObj.JApp3IdExpire2 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR2"]);
                        }
                        joinApplicantInformationObj.JApp3IdType3 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE3"]);
                        joinApplicantInformationObj.JApp3Id3 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO3"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J3_ID_EXPIR3"].ToString()))
                        {
                            joinApplicantInformationObj.JApp3IdExpire3 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR3"]);
                        }
                        joinApplicantInformationObj.JApp3IdType4 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE4"]);
                        joinApplicantInformationObj.JApp3Id4 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO4"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J3_ID_EXPIR4"].ToString()))
                        {
                            joinApplicantInformationObj.JApp3IdExpire4 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR4"]);
                        }
                        joinApplicantInformationObj.JApp3IdType5 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_TYPE5"]);
                        joinApplicantInformationObj.JApp3Id5 = Convert.ToString(jointApplicantDataTableReaderObj["J3_ID_NO5"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J3_ID_EXPIR5"].ToString()))
                        {
                            joinApplicantInformationObj.JApp3IdExpire5 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J3_ID_EXPIR5"]);
                        }
                        //End joint applicant 3

                        //Joint applicant 4
                        joinApplicantInformationObj.JointApp4Name = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_NM"]);
                        joinApplicantInformationObj.J4FatherName = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_FATHE_NM"]);
                        joinApplicantInformationObj.J4MotherName = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_MOTHE_NM"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J4_APP_DOB"].ToString()))
                        {
                            joinApplicantInformationObj.J4DOB = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_APP_DOB"]);
                        }
                        joinApplicantInformationObj.J4TinNo = Convert.ToString(jointApplicantDataTableReaderObj["J4_TIN"]);

                        joinApplicantInformationObj.J4SCBAccNo1 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO1"].ToString());
                        joinApplicantInformationObj.J4SCBAccNo2 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO2"].ToString());
                        joinApplicantInformationObj.J4SCBAccNo3 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO3"].ToString());
                        joinApplicantInformationObj.J4SCBAccNo4 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO4"].ToString());
                        joinApplicantInformationObj.J4SCBAccNo5 = Convert.ToInt64(jointApplicantDataTableReaderObj["J4_APP_AC_NO5"].ToString());

                        joinApplicantInformationObj.J4CrdtCrdNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO"]);
                        joinApplicantInformationObj.J4CrdtCrdNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO2"]);
                        joinApplicantInformationObj.J4CrdtCrdNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO3"]);
                        joinApplicantInformationObj.J4CrdtCrdNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO4"]);
                        joinApplicantInformationObj.J4CrdtCrdNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CC_NO5"]);

                        joinApplicantInformationObj.JApp4EducationLevel = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_EDUCA_LEVEL"]);
                        joinApplicantInformationObj.JApp4Company = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_EMPLO"]);

                        joinApplicantInformationObj.JApp4ContactNo1 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO1"]);
                        joinApplicantInformationObj.JApp4ContactNo2 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO2"]);
                        joinApplicantInformationObj.JApp4ContactNo3 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO3"]);
                        joinApplicantInformationObj.JApp4ContactNo4 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO4"]);
                        //joinApplicantInformationObj.JApp4ContactNo5 = Convert.ToString(jointApplicantDataTableReaderObj["J4_APP_CONTACT_NO5"]);

                        joinApplicantInformationObj.JApp4IdType1 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE1"]);
                        joinApplicantInformationObj.JApp4Id1 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO1"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J4_ID_EXPIR1"].ToString()))
                        {
                            joinApplicantInformationObj.JApp4IdExpire1 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR1"]);
                        }
                        joinApplicantInformationObj.JApp4IdType2 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE2"]);
                        joinApplicantInformationObj.JApp4Id2 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO2"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J4_ID_EXPIR2"].ToString()))
                        {
                            joinApplicantInformationObj.JApp4IdExpire2 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR2"]);
                        }
                        joinApplicantInformationObj.JApp4IdType3 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE3"]);
                        joinApplicantInformationObj.JApp4Id3 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO3"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J4_ID_EXPIR3"].ToString()))
                        {
                            joinApplicantInformationObj.JApp4IdExpire3 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR3"]);
                        }
                        joinApplicantInformationObj.JApp4IdType4 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE4"]);
                        joinApplicantInformationObj.JApp4Id4 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO4"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J4_ID_EXPIR4"].ToString()))
                        {
                            joinApplicantInformationObj.JApp4IdExpire4 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR4"]);
                        }
                        joinApplicantInformationObj.JApp4IdType5 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_TYPE5"]);
                        joinApplicantInformationObj.JApp4Id5 = Convert.ToString(jointApplicantDataTableReaderObj["J4_ID_NO5"]);
                        if (!string.IsNullOrEmpty(jointApplicantDataTableReaderObj["J4_ID_EXPIR5"].ToString()))
                        {
                            joinApplicantInformationObj.JApp4IdExpire5 = Convert.ToDateTime(jointApplicantDataTableReaderObj["J4_ID_EXPIR5"]);
                        }
                        //End joint applicant 4
                    }
                }
            }
            return joinApplicantInformationObj;
        }

        public int InsertORUpdateJointApplicationInfo(JoinApplicantInformation joinApplicantInformationObj)
        {
            int insertORUpdateValue = -1;
            if (IsExistsLoanInfo(Convert.ToInt32(joinApplicantInformationObj.LlId)))
            {
                insertORUpdateValue = UpdateJointApplicattionInformation(joinApplicantInformationObj);
            }
            return insertORUpdateValue;
        }

        private int InsertJointApplicattionInformation( JoinApplicantInformation joinApplicantInformation)
        {
            int insertRow = -1;
            string insertQueryString = "INSERT INTO loan_app_info " +
                                                "(LOAN_APPLI_ID, " +
                                                "J_APP_NM, J_APP_FATHE_NM, J_APP_MOTHE_NM, J_APP_DOB, " +
                                                "J_APP_EMPLO, J_APP_EDUCA_LEVEL, J_APP_AC_NO, J_APP_AC_NO2, J_APP_AC_NO3, " +
                                                "J_APP_AC_NO4, J_APP_AC_NO5, J_APP_CC_NO, J_APP_CC_NO2, J_APP_CC_NO3, J_APP_CC_NO4, " +
                                                "J_APP_CC_NO5, J_TIN, J_APP_CONTACT_NO1, J_APP_CONTACT_NO2, J_APP_CONTACT_NO3, " + //100
                                                "J_APP_CONTACT_NO4, J_ID_TYPE1, J_ID_TYPE2, J_ID_TYPE3, J_ID_TYPE4, J_ID_TYPE5, " +
                                                "J_ID_NO1, J_ID_NO2, J_ID_NO3, J_ID_NO4, J_ID_NO5, J_ID_EXPIR1, J_ID_EXPIR2, " +
                                                "J_ID_EXPIR3, J_ID_EXPIR4, J_ID_EXPIR5, J2_APP_NM, " +
                                                "J2_APP_AC_NO1, J2_APP_AC_NO2, J2_APP_AC_NO3, J2_APP_AC_NO4, J2_APP_AC_NO5, J2_TIN, " +
                                                "J2_APP_CC_NO, J2_APP_CC_NO2, J2_APP_CC_NO3, J2_APP_CC_NO4, J2_APP_CC_NO5, " +
                                                "J2_APP_FATHE_NM, J2_APP_MOTHE_NM, J2_APP_DOB, J2_APP_EDUCA_LEVEL, J2_APP_CONTACT_NO1, " +
                                                "J2_APP_CONTACT_NO2, J2_APP_CONTACT_NO3, J2_APP_CONTACT_NO4, J2_APP_EMPLO, J2_ID_TYPE1, " +
                                                "J2_ID_TYPE2, J2_ID_TYPE3, J2_ID_TYPE4, J2_ID_TYPE5, J2_ID_NO1, J2_ID_NO2, " +
                                                "J2_ID_NO3, J2_ID_NO4, J2_ID_NO5, J2_ID_EXPIR1, J2_ID_EXPIR2, J2_ID_EXPIR3, " +
                                                "J2_ID_EXPIR4, J2_ID_EXPIR5, " + //joint applicant2 end
                                                "J3_APP_NM, " +
                                                "J3_APP_AC_NO1, J3_APP_AC_NO2, J3_APP_AC_NO3, J3_APP_AC_NO4, J3_APP_AC_NO5, J3_TIN, " +
                                                "J3_APP_CC_NO, J3_APP_CC_NO2, J3_APP_CC_NO3, J3_APP_CC_NO4, J3_APP_CC_NO5, " +
                                                "J3_APP_FATHE_NM, J3_APP_MOTHE_NM, J3_APP_DOB, J3_APP_EDUCA_LEVEL, J3_APP_CONTACT_NO1, " +
                                                "J3_APP_CONTACT_NO2, J3_APP_CONTACT_NO3, J3_APP_CONTACT_NO4, J3_APP_EMPLO, J3_ID_TYPE1, " +
                                                "J3_ID_TYPE2, J3_ID_TYPE3, J3_ID_TYPE4, J3_ID_TYPE5, J3_ID_NO1, J3_ID_NO2, " +
                                                "J3_ID_NO3, J3_ID_NO4, J3_ID_NO5, J3_ID_EXPIR1, J3_ID_EXPIR2, J3_ID_EXPIR3, " +
                                                "J3_ID_EXPIR4, J3_ID_EXPIR5, " + //join applicant3 end
                                                "J4_APP_NM, " +
                                                "J4_APP_AC_NO1, J4_APP_AC_NO2, J4_APP_AC_NO3, J4_APP_AC_NO4, J4_APP_AC_NO5, J4_TIN, " +
                                                "J4_APP_CC_NO, J4_APP_CC_NO2, J4_APP_CC_NO3, J4_APP_CC_NO4, J4_APP_CC_NO5, " +
                                                "J4_APP_FATHE_NM, J4_APP_MOTHE_NM, J4_APP_DOB, J4_APP_EDUCA_LEVEL, J4_APP_CONTACT_NO1, " +
                                                "J4_APP_CONTACT_NO2, J4_APP_CONTACT_NO3, J4_APP_CONTACT_NO4, J4_APP_EMPLO, J4_ID_TYPE1, " +
                                                "J4_ID_TYPE2, J4_ID_TYPE3, J4_ID_TYPE4, J4_ID_TYPE5, J4_ID_NO1, J4_ID_NO2, " +
                                                "J4_ID_NO3, J4_ID_NO4, J4_ID_NO5, J4_ID_EXPIR1, J4_ID_EXPIR2, J4_ID_EXPIR3, " +
                                                "J4_ID_EXPIR4, J4_ID_EXPIR5 " +
                                                ") " +
                                                "VALUES( '" + joinApplicantInformation.LlId +
                                                "','" + joinApplicantInformation.JointApp1Name.Replace("'", "''") + "','" + joinApplicantInformation.J1FatherName.Replace("'", "''") + "','" + joinApplicantInformation.J1MotherName.Replace("'", "''") + 
                                                "','" + joinApplicantInformation.J1DOB.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1Company.Replace("'", "''") +
                                                "','" + joinApplicantInformation.JApp1EducationLevel.Replace("'", "''")+ "','" +  joinApplicantInformation.J1SCBAccNo1 +
                                                "','" + joinApplicantInformation.J1SCBAccNo2+ "','" +  joinApplicantInformation.J1SCBAccNo3+ "','" +  joinApplicantInformation.J1SCBAccNo4+ "','" +  joinApplicantInformation.J1SCBAccNo5 +
                                                "','" +  joinApplicantInformation.J1CrdtCrdNo1.Replace("'", "''")+ "','" +  joinApplicantInformation.J1CrdtCrdNo2.Replace("'", "''")+ "','" +  joinApplicantInformation.J1CrdtCrdNo3.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J1CrdtCrdNo4.Replace("'", "''")+ "','" +  joinApplicantInformation.J1CrdtCrdNo5.Replace("'", "''")+ "','" +  joinApplicantInformation.J1TinNo.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp1ContactNo1.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1ContactNo2.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1ContactNo3.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp1ContactNo4.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1IdType1.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp1IdType2.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1IdType3.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1IdType4.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp1IdType5.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1Id1.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1Id2.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp1Id3.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1Id4.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1Id5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp1IdExpire1.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1IdExpire2.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp1IdExpire3.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp1IdExpire4.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp1IdExpire5.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JointApp2Name.Replace("'", "''")+ "','" +  joinApplicantInformation.J2SCBAccNo1+ "','" +  joinApplicantInformation.J2SCBAccNo2+ "','" +  joinApplicantInformation.J2SCBAccNo3 +
                                                "','" +  joinApplicantInformation.J2SCBAccNo4+ "','" +  joinApplicantInformation.J2SCBAccNo5+ "','" +  joinApplicantInformation.J2TinNo.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J2CrdtCrdNo1.Replace("'", "''")+ "','" +  joinApplicantInformation.J2CrdtCrdNo2.Replace("'", "''")+ "','" +  joinApplicantInformation.J2CrdtCrdNo3.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J2CrdtCrdNo4.Replace("'", "''")+ "','" +  joinApplicantInformation.J2CrdtCrdNo5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J2FatherName.Replace("'", "''")+ "','" +  joinApplicantInformation.J2MotherName.Replace("'", "''")+ "','" +  joinApplicantInformation.J2DOB.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2EducationLevel.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2ContactNo1.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2ContactNo2.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2ContactNo3.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2ContactNo4.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2Company.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2IdType1.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2IdType2.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2IdType3.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2IdType4.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2IdType5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2Id1.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2Id2.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2Id3.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2Id4.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2Id5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2IdExpire1.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2IdExpire2.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2IdExpire3.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp2IdExpire4.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp2IdExpire5.ToString("yyyy-MM-dd").Replace("'", "''") +
                
                                                "','" +  joinApplicantInformation.JointApp3Name.Replace("'", "''")+ "','" +  joinApplicantInformation.J3SCBAccNo1+ "','" +  joinApplicantInformation.J3SCBAccNo2+ "','" +  joinApplicantInformation.J3SCBAccNo3 +
                                                "','" +  joinApplicantInformation.J3SCBAccNo4+ "','" +  joinApplicantInformation.J3SCBAccNo5+ "','" +  joinApplicantInformation.J3TinNo.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J3CrdtCrdNo1.Replace("'", "''")+ "','" +  joinApplicantInformation.J3CrdtCrdNo2.Replace("'", "''")+ "','" +  joinApplicantInformation.J3CrdtCrdNo3.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J3CrdtCrdNo4.Replace("'", "''")+ "','" +  joinApplicantInformation.J3CrdtCrdNo5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J3FatherName.Replace("'", "''")+ "','" +  joinApplicantInformation.J3MotherName.Replace("'", "''")+ "','" +  joinApplicantInformation.J3DOB.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3EducationLevel.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3ContactNo1.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3ContactNo2.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3ContactNo3.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3ContactNo4.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3Company.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3IdType1.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3IdType2.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3IdType3.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3IdType4.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3IdType5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3Id1.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3Id2.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3Id3.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3Id4.Replace("'", "''")+ "','" + joinApplicantInformation.JApp3Id5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3IdExpire1.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3IdExpire2.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3IdExpire3.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp3IdExpire4.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp3IdExpire5.ToString("yyyy-MM-dd").Replace("'", "''") +
                
                                                "','" +  joinApplicantInformation.JointApp4Name.Replace("'", "''")+ "','" +  joinApplicantInformation.J4SCBAccNo1+ "','" +  joinApplicantInformation.J4SCBAccNo2+ "','" +  joinApplicantInformation.J4SCBAccNo3 +
                                                "','" +  joinApplicantInformation.J4SCBAccNo4+ "','" +  joinApplicantInformation.J4SCBAccNo5+ "','" +  joinApplicantInformation.J4TinNo.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J4CrdtCrdNo1.Replace("'", "''")+ "','" +  joinApplicantInformation.J4CrdtCrdNo2.Replace("'", "''")+ "','" +  joinApplicantInformation.J4CrdtCrdNo3.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J4CrdtCrdNo4.Replace("'", "''")+ "','" +  joinApplicantInformation.J4CrdtCrdNo5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.J4FatherName.Replace("'", "''")+ "','" +  joinApplicantInformation.J4MotherName.Replace("'", "''")+ "','" +  joinApplicantInformation.J4DOB.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4EducationLevel.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4ContactNo1.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4ContactNo2.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4ContactNo3.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4ContactNo4.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4Company.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4IdType1.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4IdType2.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4IdType3.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4IdType4.Replace("'", "''")+ "','" + joinApplicantInformation.JApp4IdType5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4Id1.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4Id2.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4Id3.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4Id4.Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4Id5.Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4IdExpire1.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4IdExpire2.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4IdExpire3.ToString("yyyy-MM-dd").Replace("'", "''")+ "','" +  joinApplicantInformation.JApp4IdExpire4.ToString("yyyy-MM-dd").Replace("'", "''") +
                                                "','" +  joinApplicantInformation.JApp4IdExpire5.ToString("yyyy-MM-dd").Replace("'", "''")+ "'" +
                                                "') ";
            try
            {
                insertRow = DbQueryManager.ExecuteNonQuery(insertQueryString);
                return insertRow > 0 ? (int) InsertUpdateFlag.INSERT : (int) InsertUpdateFlag.INSERTERROR;
            }
            catch
            {
                return (int)InsertUpdateFlag.INSERTERROR;
            }
        }

 
        private int UpdateJointApplicattionInformation(JoinApplicantInformation joinApplicantInformationObj)
        {
            int updateRow = -1;

            string updateQueryString =  "UPDATE loan_app_info SET " +
                         "J_APP_NM = '" +
                         joinApplicantInformationObj.JointApp1Name.Replace("'", "''") + "'," +
                         "J_APP_FATHE_NM = '" +
                         joinApplicantInformationObj.J1FatherName.Replace("'", "''") + "'," +
                         " J_APP_MOTHE_NM = '" +
                         joinApplicantInformationObj.J1MotherName.Replace("'", "''") +
                         "' , J_APP_DOB = '" +
                         joinApplicantInformationObj.J1DOB.ToString("yyyy-MM-dd").Replace(
                             "'", "''") + "' , " +
                         "J_APP_EMPLO = '" + joinApplicantInformationObj.JApp1Company +
                         "', J_APP_EDUCA_LEVEL ='" +
                         joinApplicantInformationObj.JApp1EducationLevel +
                         "' , J_APP_AC_NO = '" + joinApplicantInformationObj.J1SCBAccNo1 +
                         "' , J_APP_AC_NO2 =  '" + joinApplicantInformationObj.J1SCBAccNo2 +
                         "' , J_APP_AC_NO3 = '" + joinApplicantInformationObj.J1SCBAccNo3 +
                         "' , " +
                         "J_APP_AC_NO4 = '" + joinApplicantInformationObj.J1SCBAccNo4 +
                         "' , J_APP_AC_NO5 = '" + joinApplicantInformationObj.J1SCBAccNo5 +
                         "' , J_APP_CC_NO = '" +
                         joinApplicantInformationObj.J1CrdtCrdNo1.Replace("'", "''") +
                         "' , J_APP_CC_NO2 = '" +
                         joinApplicantInformationObj.J1CrdtCrdNo2.Replace("'", "''") +
                         "' , J_APP_CC_NO3 = '" +
                         joinApplicantInformationObj.J1CrdtCrdNo3.Replace("'", "''") +
                         "' , J_APP_CC_NO4 = '" +
                         joinApplicantInformationObj.J1CrdtCrdNo4.Replace("'", "''") +
                         "' , " +
                         "J_APP_CC_NO5 = '" +
                         joinApplicantInformationObj.J1CrdtCrdNo5.Replace("'", "''") +
                         "' , J_TIN =  '" +
                         joinApplicantInformationObj.J1TinNo.Replace("'", "''") +
                         "' , J_APP_CONTACT_NO1 =  '" +
                         joinApplicantInformationObj.JApp1ContactNo1.Replace("'", "''") +
                         "' , J_APP_CONTACT_NO2 =  '" +
                         joinApplicantInformationObj.JApp1ContactNo2.Replace("'", "''") +
                         "' , J_APP_CONTACT_NO3 =  '" +
                         joinApplicantInformationObj.JApp1ContactNo3.Replace("'", "''") +
                         "' , " + //100
                         "J_APP_CONTACT_NO4 =  '" +
                         joinApplicantInformationObj.JApp1ContactNo4.Replace("'", "''") +
                         "' , J_ID_TYPE1 =  '" +
                         joinApplicantInformationObj.JApp1IdType1.Replace("'", "''") +
                         "' , J_ID_TYPE2 =  '" +
                         joinApplicantInformationObj.JApp1IdType2.Replace("'", "''") +
                         "' , J_ID_TYPE3 =  '" +
                         joinApplicantInformationObj.JApp1IdType3.Replace("'", "''") +
                         "' , J_ID_TYPE4 =  '" +
                         joinApplicantInformationObj.JApp1IdType4.Replace("'", "''") +
                         "' , J_ID_TYPE5 =  '" +
                         joinApplicantInformationObj.JApp1IdType5.Replace("'", "''") +
                         "' , " +
                         "J_ID_NO1 =  '" +
                         joinApplicantInformationObj.JApp1Id1.Replace("'", "''") +
                         "' , J_ID_NO2 =  '" +
                         joinApplicantInformationObj.JApp1Id2.Replace("'", "''") +
                         "' , J_ID_NO3 =  '" +
                         joinApplicantInformationObj.JApp1Id3.Replace("'", "''") +
                         "' , J_ID_NO4 =  '" +
                         joinApplicantInformationObj.JApp1Id4.Replace("'", "''") +
                         "' , J_ID_NO5 =  '" +
                         joinApplicantInformationObj.JApp1Id5.Replace("'", "''") +
                         "' , J_ID_EXPIR1 =  '" +
                         joinApplicantInformationObj.JApp1IdExpire1.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' , J_ID_EXPIR2 =  '" +
                         joinApplicantInformationObj.JApp1IdExpire2.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' , " +
                         "J_ID_EXPIR3 =  '" +
                         joinApplicantInformationObj.JApp1IdExpire3.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' , J_ID_EXPIR4 =  '" +
                         joinApplicantInformationObj.JApp1IdExpire4.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' , J_ID_EXPIR5 =  '" +
                         joinApplicantInformationObj.JApp1IdExpire5.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' ," +
                         "J2_APP_NM =  '" +
                         joinApplicantInformationObj.JointApp2Name.Replace("'", "''") +
                         "' , " +
                         "J2_APP_AC_NO1 =  '" + joinApplicantInformationObj.J2SCBAccNo1 +
                         "' , J2_APP_AC_NO2 =  '" + joinApplicantInformationObj.J2SCBAccNo2 +
                         "' , J2_APP_AC_NO3 =  '" + joinApplicantInformationObj.J2SCBAccNo3 +
                         "' , J2_APP_AC_NO4 =  '" + joinApplicantInformationObj.J2SCBAccNo4 +
                         "' , J2_APP_AC_NO5 =  '" + joinApplicantInformationObj.J2SCBAccNo5 +
                         "' , " +
                         "J2_TIN =  '" +
                         joinApplicantInformationObj.J2TinNo.Replace("'", "''") +
                         "' , J2_APP_CC_NO =  '" +
                         joinApplicantInformationObj.J2CrdtCrdNo1.Replace("'", "''") +
                         "' , J2_APP_CC_NO2 =  '" +
                         joinApplicantInformationObj.J2CrdtCrdNo2.Replace("'", "''") +
                         "' , J2_APP_CC_NO3 =  '" +
                         joinApplicantInformationObj.J2CrdtCrdNo3.Replace("'", "''") +
                         "' , J2_APP_CC_NO4 =  '" +
                         joinApplicantInformationObj.J2CrdtCrdNo4.Replace("'", "''") +
                         "' , J2_APP_CC_NO5 =  '" +
                         joinApplicantInformationObj.J2CrdtCrdNo4.Replace("'", "''") +
                         "' , " +
                         "J2_APP_FATHE_NM =  '" +
                         joinApplicantInformationObj.J2FatherName.Replace("'", "''") +
                         "' , J2_APP_MOTHE_NM =  '" +
                         joinApplicantInformationObj.J2MotherName.Replace("'", "''") +
                         "' , J2_APP_DOB =  '" +
                         joinApplicantInformationObj.J2DOB.ToString("yyyy-MM-dd").Replace(
                             "'", "''") + "' , J2_APP_EDUCA_LEVEL =  '" +
                         joinApplicantInformationObj.JApp2EducationLevel.Replace("'", "''") +
                         "' , J2_APP_CONTACT_NO1 =  '" +
                         joinApplicantInformationObj.JApp2ContactNo1.Replace("'", "''") +
                         "' , " +
                         "J2_APP_CONTACT_NO2 =  '" +
                         joinApplicantInformationObj.JApp2ContactNo2.Replace("'", "''") +
                         "' , J2_APP_CONTACT_NO3 =  '" +
                         joinApplicantInformationObj.JApp2ContactNo3.Replace("'", "''") +
                         "' , J2_APP_CONTACT_NO4 =  '" +
                         joinApplicantInformationObj.JApp2ContactNo4.Replace("'", "''") +
                         "' , J2_APP_EMPLO =  '" +
                         joinApplicantInformationObj.JApp2Company.Replace("'", "''") +
                         "' , " +
                         "J2_ID_TYPE1 =  '" +
                         joinApplicantInformationObj.JApp2IdType1.Replace("'", "''") +
                         "' , J2_ID_TYPE2 =  '" +
                         joinApplicantInformationObj.JApp2IdType2.Replace("'", "''") +
                         "' , J2_ID_TYPE3 =  '" +
                         joinApplicantInformationObj.JApp2IdType3.Replace("'", "''") +
                         "' , J2_ID_TYPE4 =  '" +
                         joinApplicantInformationObj.JApp2IdType4.Replace("'", "''") +
                         "' , J2_ID_TYPE5 =  '" +
                         joinApplicantInformationObj.JApp2IdType5.Replace("'", "''") +
                         "' , J2_ID_NO1 =  '" +
                         joinApplicantInformationObj.JApp2Id1.Replace("'", "''") +
                         "' , J2_ID_NO2 =  '" +
                         joinApplicantInformationObj.JApp2Id2.Replace("'", "''") + "' , " +
                         "J2_ID_NO3 =  '" +
                         joinApplicantInformationObj.JApp2Id3.Replace("'", "''") +
                         "' , J2_ID_NO4 =  '" +
                         joinApplicantInformationObj.JApp2Id4.Replace("'", "''") +
                         "' , J2_ID_NO5 =  '" +
                         joinApplicantInformationObj.JApp2Id5.Replace("'", "''") +
                         "' , J2_ID_EXPIR1 =  '" +
                         joinApplicantInformationObj.JApp2IdExpire1.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' , J2_ID_EXPIR2 =  '" +
                         joinApplicantInformationObj.JApp2IdExpire2.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' , J2_ID_EXPIR3 =  '" +
                         joinApplicantInformationObj.JApp2IdExpire3.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' , " +
                         "J2_ID_EXPIR4 =  '" +
                         joinApplicantInformationObj.JApp2IdExpire4.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' , J2_ID_EXPIR5 =  '" +
                         joinApplicantInformationObj.JApp2IdExpire5.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "',  " + //joint aplicant2 end

                         "J3_APP_NM =  '" +
                         joinApplicantInformationObj.JointApp3Name.Replace("'", "''") +
                         "', " +
                         "J3_APP_AC_NO1 =  '" + joinApplicantInformationObj.J3SCBAccNo1 +
                         "', J3_APP_AC_NO2 =  '" + joinApplicantInformationObj.J3SCBAccNo2 +
                         "', J3_APP_AC_NO3 =  '" + joinApplicantInformationObj.J3SCBAccNo3 +
                         "', J3_APP_AC_NO4 =  '" + joinApplicantInformationObj.J3SCBAccNo4 +
                         "', J3_APP_AC_NO5 =  '" + joinApplicantInformationObj.J3SCBAccNo5 +
                         "', J3_TIN =  '" +
                         joinApplicantInformationObj.J3TinNo.Replace("'", "''") + "', " +
                         "J3_APP_CC_NO =  '" +
                         joinApplicantInformationObj.J3CrdtCrdNo1.Replace("'", "''") +
                         "', J3_APP_CC_NO2 =  '" +
                         joinApplicantInformationObj.J3CrdtCrdNo2.Replace("'", "''") +
                         "', J3_APP_CC_NO3 =  '" +
                         joinApplicantInformationObj.J3CrdtCrdNo3.Replace("'", "''") +
                         "', J3_APP_CC_NO4 =  '" +
                         joinApplicantInformationObj.J3CrdtCrdNo4.Replace("'", "''") +
                         "', J3_APP_CC_NO5 =  '" +
                         joinApplicantInformationObj.J3CrdtCrdNo5.Replace("'", "''") + "', " +
                         "J3_APP_FATHE_NM =  '" +
                         joinApplicantInformationObj.J3FatherName.Replace("'", "''") +
                         "', J3_APP_MOTHE_NM =  '" +
                         joinApplicantInformationObj.J3MotherName.Replace("'", "''") +
                         "', J3_APP_DOB =  '" +
                         joinApplicantInformationObj.J3DOB.ToString("yyyy-MM-dd").Replace(
                             "'", "''") + "', J3_APP_EDUCA_LEVEL =  '" +
                         joinApplicantInformationObj.JApp3EducationLevel.Replace("'", "''") +
                         "', J3_APP_CONTACT_NO1 =  '" +
                         joinApplicantInformationObj.JApp3ContactNo1.Replace("'", "''") +
                         "', " +
                         "J3_APP_CONTACT_NO2 =  '" +
                         joinApplicantInformationObj.JApp3ContactNo2.Replace("'", "''") +
                         "', J3_APP_CONTACT_NO3 =  '" +
                         joinApplicantInformationObj.JApp3ContactNo3.Replace("'", "''") +
                         "', J3_APP_CONTACT_NO4 =  '" +
                         joinApplicantInformationObj.JApp3ContactNo4.Replace("'", "''") +
                         "', J3_APP_EMPLO =  '" +
                         joinApplicantInformationObj.JApp3Company.Replace("'", "''") +
                         "', J3_ID_TYPE1 =  '" +
                         joinApplicantInformationObj.JApp3IdType1.Replace("'", "''") + "', " +
                         "J3_ID_TYPE2 =  '" +
                         joinApplicantInformationObj.JApp3IdType2.Replace("'", "''") +
                         "', J3_ID_TYPE3 =  '" +
                         joinApplicantInformationObj.JApp3IdType3.Replace("'", "''") +
                         "', J3_ID_TYPE4 =  '" +
                         joinApplicantInformationObj.JApp3IdType4.Replace("'", "''") +
                         "', J3_ID_TYPE5 =  '" +
                         joinApplicantInformationObj.JApp3IdType5.Replace("'", "''") +
                         "', J3_ID_NO1 =  '" +
                         joinApplicantInformationObj.JApp3Id1.Replace("'", "''") +
                         "', J3_ID_NO2 =  '" +
                         joinApplicantInformationObj.JApp3Id2.Replace("'", "''") + "', " +
                         "J3_ID_NO3 =  '" +
                         joinApplicantInformationObj.JApp3Id3.Replace("'", "''") +
                         "', J3_ID_NO4 =  '" +
                         joinApplicantInformationObj.JApp3Id4.Replace("'", "''") +
                         "', J3_ID_NO5 =  '" +
                         joinApplicantInformationObj.JApp3Id5.Replace("'", "''") +
                         "', J3_ID_EXPIR1 =  '" +
                         joinApplicantInformationObj.JApp3IdExpire1.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', J3_ID_EXPIR2 =  '" +
                         joinApplicantInformationObj.JApp3IdExpire2.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', J3_ID_EXPIR3 =  '" +
                         joinApplicantInformationObj.JApp3IdExpire3.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', " +
                         "J3_ID_EXPIR4 =  '" +
                         joinApplicantInformationObj.JApp3IdExpire4.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', J3_ID_EXPIR5 =  '" +
                         joinApplicantInformationObj.JApp3IdExpire5.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', " + //join applicant3 end
                         "J4_APP_NM =  '" +
                         joinApplicantInformationObj.JointApp4Name.Replace("'", "''") +
                         "', " +
                         "J4_APP_AC_NO1 =  '" + joinApplicantInformationObj.J4SCBAccNo1 +
                         "', J4_APP_AC_NO2 =  '" + joinApplicantInformationObj.J4SCBAccNo2 +
                         "', J4_APP_AC_NO3 =  '" + joinApplicantInformationObj.J4SCBAccNo3 +
                         "', J4_APP_AC_NO4 =  '" + joinApplicantInformationObj.J4SCBAccNo4 +
                         "', J4_APP_AC_NO5 =  '" + joinApplicantInformationObj.J4SCBAccNo5 +
                         "', J4_TIN =  '" +
                         joinApplicantInformationObj.J4TinNo.Replace("'", "''") + "', " +
                         "J4_APP_CC_NO =  '" +
                         joinApplicantInformationObj.J4CrdtCrdNo1.Replace("'", "''") +
                         "', J4_APP_CC_NO2 =  '" +
                         joinApplicantInformationObj.J4CrdtCrdNo2.Replace("'", "''") +
                         "', J4_APP_CC_NO3 =  '" +
                         joinApplicantInformationObj.J4CrdtCrdNo3.Replace("'", "''") +
                         "', J4_APP_CC_NO4 =  '" +
                         joinApplicantInformationObj.J4CrdtCrdNo4.Replace("'", "''") +
                         "', J4_APP_CC_NO5 =  '" +
                         joinApplicantInformationObj.J4CrdtCrdNo5.Replace("'", "''") + "', " +
                         "J4_APP_FATHE_NM =  '" +
                         joinApplicantInformationObj.J4FatherName.Replace("'", "''") +
                         "', J4_APP_MOTHE_NM =  '" +
                         joinApplicantInformationObj.J4MotherName.Replace("'", "''") +
                         "', J4_APP_DOB =  '" +
                         joinApplicantInformationObj.J4DOB.ToString("yyyy-MM-dd").Replace(
                             "'", "''") + "', J4_APP_EDUCA_LEVEL =  '" +
                         joinApplicantInformationObj.JApp4EducationLevel.Replace("'", "''") +
                         "', J4_APP_CONTACT_NO1 =  '" +
                         joinApplicantInformationObj.JApp4ContactNo1.Replace("'", "''") +
                         "', " +
                         "J4_APP_CONTACT_NO2 =  '" +
                         joinApplicantInformationObj.JApp4ContactNo2.Replace("'", "''") +
                         "', J4_APP_CONTACT_NO3 =  '" +
                         joinApplicantInformationObj.JApp4ContactNo3.Replace("'", "''") +
                         "', J4_APP_CONTACT_NO4 =  '" +
                         joinApplicantInformationObj.JApp4ContactNo4.Replace("'", "''") +
                         "', J4_APP_EMPLO =  '" +
                         joinApplicantInformationObj.JApp4Company.Replace("'", "''") +
                         "', J4_ID_TYPE1 =  '" +
                         joinApplicantInformationObj.JApp4IdType1.Replace("'", "''") + "', " +
                         "J4_ID_TYPE2 =  '" +
                         joinApplicantInformationObj.JApp4IdType2.Replace("'", "''") +
                         "', J4_ID_TYPE3 =  '" +
                         joinApplicantInformationObj.JApp4IdType3.Replace("'", "''") +
                         "', J4_ID_TYPE4 =  '" +
                         joinApplicantInformationObj.JApp4IdType4.Replace("'", "''") +
                         "', J4_ID_TYPE5 =  '" +
                         joinApplicantInformationObj.JApp4IdType5.Replace("'", "''") +
                         "', J4_ID_NO1 =  '" +
                         joinApplicantInformationObj.JApp4Id1.Replace("'", "''") +
                         "', J4_ID_NO2 =  '" +
                         joinApplicantInformationObj.JApp4Id2.Replace("'", "''") + "', " +
                         "J4_ID_NO3 =  '" +
                         joinApplicantInformationObj.JApp4Id3.Replace("'", "''") +
                         "', J4_ID_NO4 =  '" +
                         joinApplicantInformationObj.JApp4Id4.Replace("'", "''") +
                         "', J4_ID_NO5 =  '" +
                         joinApplicantInformationObj.JApp4Id5.Replace("'", "''") +
                         "', J4_ID_EXPIR1 =  '" +
                         joinApplicantInformationObj.JApp4IdExpire1.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', J4_ID_EXPIR2 =  '" +
                         joinApplicantInformationObj.JApp4IdExpire2.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', J4_ID_EXPIR3 =  '" +
                         joinApplicantInformationObj.JApp4IdExpire3.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', " +
                         "J4_ID_EXPIR4 =  '" +
                         joinApplicantInformationObj.JApp4IdExpire4.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "', J4_ID_EXPIR5 =  '" +
                         joinApplicantInformationObj.JApp4IdExpire5.ToString("yyyy-MM-dd").
                             Replace("'", "''") + "' " +
                         "WHERE LOAN_APPLI_ID =  '" + joinApplicantInformationObj.LlId + "' ";
                                                  
            try
            {
                updateRow = DbQueryManager.ExecuteNonQuery(updateQueryString);
                return updateRow > 0 ? (int) InsertUpdateFlag.UPDATE : (int) InsertUpdateFlag.UPDATEERROR;
            }
            catch
            {
                return (int)InsertUpdateFlag.UPDATEERROR;
            }
        }
     

        #region IsExistsLoanInfo(Int32 llid)
        /// <summary>
        /// This method provide loan information exists or not.
        /// </summary>
        /// <param name="llid">Get LLID</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsLoanInfo(Int32 llid)
        {
            bool returnValue = false;
            string queryString = String.Format("SELECT COUNT(*) FROM loan_app_info WHERE LOAN_APPLI_ID = {0}", llid);
            object locationInfoObj;
            try
            {
                locationInfoObj = DbQueryManager.ExecuteScalar(queryString);
                if (Convert.ToInt32(locationInfoObj.ToString()) > 0)
                {
                    returnValue = true;
                }
            }
            catch
            {
                //
            }
            return returnValue;
        }
        #endregion

        #region EscapeCharacter
        /// <summary>
        /// This method convert special character.
        /// </summary>
        /// <param name="fieldString">Gets field string.</param>
        /// <returns>Returns string.</returns>
        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                //fieldString = fieldString.Replace(
                returnMessage = fieldString;
            }
            return returnMessage;
        }
        #endregion
    }
}
