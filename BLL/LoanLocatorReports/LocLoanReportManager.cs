﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL.LoanLocatorReports
{
  public  class LocLoanReportManager
    {
      private readonly LocLoanReportGateway _locLoanReportGateway = new LocLoanReportGateway();
      public DataTable ApplicationStatusWiseReprot(string date1, string date2, int productId, int status, string roleCutDate)
      {
          return _locLoanReportGateway.ApplicationStatusWiseReprot(date1, date2, productId, status, roleCutDate); 
      }

      public DataTable LocAdminAuditReport(int year, int month, int action)
      {
          return _locLoanReportGateway.LocAdminAuditReport(year, month, action);
      }

      public DataTable ApplicationDeferredOrDeclinedReprot(int producId, int status, string date)
      {
         return _locLoanReportGateway.ApplicationDeferredOrDeclinedReprot(producId, status, date); 
      }

      public DataTable CappingReport(string date1, string date2, int buttonId, int capptype)
      {
          return _locLoanReportGateway.CappingReport(date1, date2, buttonId, capptype);
      }

      public DataTable GetTotal(string date1, string date2, int capptype)
      {
          return _locLoanReportGateway.GetTotal(date1, date2, capptype);
      }

      public DataTable SegmentWiseCappingReport(string m1)
      {
          return _locLoanReportGateway.SegmentWiseCappingReport(m1);
      }

      public DataTable SegmentWiseCappingMonth(string m1)
      {
          return _locLoanReportGateway.SegmentWiseCappingMonth(m1);
      }

      public DataTable AccessLoginReports(string fromDate, string toDate,string userCode)
      {
          return _locLoanReportGateway.AccessLoginReports(fromDate, toDate, userCode);
      }

      public DataTable ApplicationDetailsReprot(string date1, string date2, int productId, string roleCutDate)
      {
          return _locLoanReportGateway.ApplicationDetailsReprot(date1, date2, productId, roleCutDate);
      }

      public DataTable GetLevelCap(int capptype)
      {
          return _locLoanReportGateway.GetLevelCap(capptype);
      }

      public DataTable GetRowOne(string month, int productId)
      {
          return _locLoanReportGateway.GetRowOne(month, productId);
      }

      public DataTable GetRowTwo(string year, int productId)
      {
          return _locLoanReportGateway.GetRowTwo(year, productId); 
      }

      public DataTable GetRowThree(string month, int productId)
      {
          return _locLoanReportGateway.GetRowThree(month, productId); 
      }

      public DataTable GetRowFour(string year, int productId)
      {
          return _locLoanReportGateway.GetRowFour(year, productId); 
      }

      public DataTable GetOldValue(int productId, int branchId)
      {
          return _locLoanReportGateway.GetOldValue(productId, branchId);
      }

      public DataTable GetResultM(string year, string monthNo, int productId, int branchId)
      {
          return _locLoanReportGateway.GetResultM(year, monthNo, productId, branchId); 
      }

      public DataTable GetResultY(string year, int productId, int branchId)
      {
          return _locLoanReportGateway.GetResultY(year, productId, branchId); 
      }
    }
}
