﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL.LoanLocatorReports;

namespace BLL.LoanLocatorReports
{
  public  class LocAdminTatReportManager
    {
      private readonly LocAdminTatReportGateway _locAdminTatGateway=new LocAdminTatReportGateway();

      public DataTable IndividualTatReport(string date1, string date2, int productId, string roleCutDate) 
      {
          return _locAdminTatGateway.IndividualTatReport(date1, date2, productId, roleCutDate);
      }

      public DataTable IndividualTatReportByDate(string date1, string date2, int productId)
      {
          return _locAdminTatGateway.IndividualTatReportByDate(date1, date2, productId);
      }

      public DataTable GetData(int loanId)
      {
          return _locAdminTatGateway.GetData(loanId); 
      }

      public DataTable GetReportData(int loanId)
      {
          return _locAdminTatGateway.GetReportData(loanId); 
      }

      public string getDateFromDetails(int loanId, int id, string order) 
      {
          return _locAdminTatGateway.getDateFromDetails(loanId, id, order);
      }

      public DataTable GetQuickReport(int year, string month, int productId, int buttonId)  
      {
          return _locAdminTatGateway.GetQuickReport(year, month, productId, buttonId);  
      }

      public DataTable GetCalculatingVol(int year, string month, int productId, int buttonId)
      {
          return _locAdminTatGateway.GetCalculatingVol(year, month, productId, buttonId); 
      }

      public int GetApplications(string year, string monthNo, int productId, int flag)
      {
          return _locAdminTatGateway.GetApplications(year, monthNo, productId, flag); 
      }

      public DataTable GetTatVar(string year, string monthNo, int productId, int flag)
      {
          return _locAdminTatGateway.GetTatVar(year, monthNo, productId, flag);  
      }

      public decimal GetCredit(string date, int productId)
      {
          return _locAdminTatGateway.GetCredit(date, productId); 
      }

      public decimal GetAsset(string date, int productId)
      {
          return _locAdminTatGateway.GetAsset(date, productId);  
      }
    }
}
