﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    public class ApproverManager
    {
        public ApproverGateway ApproverGatewayObj=null;
        public ApproverManager()
        {
            ApproverGatewayObj = new ApproverGateway();
        }
        public List<Approver> SelectApproverList()
        {
            return ApproverGatewayObj.SelectApproverList();
        }

        public List<Approver> SelectAllApproverList()
        {
            return ApproverGatewayObj.SelectAllApproverList();
        }

        public List<Approver> SelectAllApproverList(string key)
        {
            return ApproverGatewayObj.SelectAllApproverList(key);
        }

        public int InsertApprover(Approver approverObj)
        {
            return ApproverGatewayObj.InsertApprover(approverObj);
        }

        public int UpdateApprover(Approver approverObj)
        {
            return ApproverGatewayObj.UpdateApprover(approverObj);
        }
        public Approver GetApproverLimit(Int32 UserId,string productTag)
        {
            return ApproverGatewayObj.GetApproverLimit(UserId,productTag);
        }
        public Int32 GetExistiongApproverId(Int32 approverId, string productTag)
        {
            return ApproverGatewayObj.GetExistiongApproverId(approverId,productTag);
        }
    }
}
