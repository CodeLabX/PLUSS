﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// SegmentManager Class.
    /// </summary>
    public class SegmentManager
    {
        public SegmentGateway segmentGatewayObj = null;
        /// <summary>
        /// Constructor
        /// </summary>
        public SegmentManager()
        {
            segmentGatewayObj = new SegmentGateway();
        }
        /// <summary>
        /// This method select segment list
        /// </summary>
        /// <returns>Returns a Segment list object.</returns>
        public List<Segment> SelectSegmentList()
        {
            return segmentGatewayObj.SelectSegmentList();
        }
        /// <summary>
        /// This method select segment list
        /// </summary>
        /// <param name="professionId">Gets a profession id.</param>
        /// <returns>Returns a Segment list object.</returns>
        public List<Segment> SelectSegmentList(int professionId)
        {
            return segmentGatewayObj.SelectSegmentList(professionId);
        }
        /// <summary>
        /// This method select segment
        /// </summary>
        /// <param name="segmentId">Gets a segment id.</param>
        /// <returns>Returns a Segment  object.</returns>
        public Segment SelectSegment(int segmentId)
        {
            return segmentGatewayObj.SelectSegment(segmentId);
        }
        /// <summary>
        /// This method provide the operation of insert segment info
        /// </summary>
        /// <param name="segmentObj">Gets a Segment object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendSegmentInfoInToDB(Segment segmentObj)
        {
            return segmentGatewayObj.SendSegmentInfoInToDB(segmentObj);
        }
        /// <summary>
        /// This method select segment code
        /// </summary>
        /// <returns>Returns list of segment code.</returns>
        public List<string> SelectSegmentCode()
        {
            return segmentGatewayObj.SelectSegmentCode();
        }
        /// <summary>
        /// This method select profession list
        /// </summary>
        /// <returns>Returns a Profession list object.</returns>
        public List<Profession> SelectProfessionList()
        {
            return segmentGatewayObj.SelectProfessionList();
        }
    }
}
