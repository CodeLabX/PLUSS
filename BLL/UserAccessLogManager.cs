﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    public class UserAccessLogManager
    {
        public UserAccessLogGateway userAccessLogGateway = null;
        public UserAccessLogManager()
        {
            userAccessLogGateway = new UserAccessLogGateway();
        }
        public List<UserAccessLog> GetAccessLog()
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            return userAccessLogObjList = userAccessLogGateway.GetAccessLog();
        }
        public List<UserAccessLog> GetAccessLog(string loginId)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            return userAccessLogObjList = userAccessLogGateway.GetAccessLog(loginId);
        }
        public List<UserAccessLog> GetLoginHistory(string loginId)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            return userAccessLogObjList = userAccessLogGateway.GetLoginHistory(loginId);
        }
        public List<UserAccessLog> GetAccessLog(string loginId, int logInStatus)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            return userAccessLogObjList = userAccessLogGateway.GetAccessLog(loginId,logInStatus);
        }
        public List<UserAccessLog> GetAccessLog(DateTime startDate, DateTime endDate)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            return userAccessLogObjList = userAccessLogGateway.GetAccessLog(startDate, endDate);
        }
        public List<UserAccessLog> GetAccessLog(string loginId, DateTime startDate, DateTime endDate)
        {
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            return userAccessLogObjList = userAccessLogGateway.GetAccessLog(loginId,startDate,endDate);
        }
        public UserAccessLog GetAccessLog(string loginId, DateTime logInDate)
        {
            UserAccessLog userAccessLogObj = new UserAccessLog();
            return userAccessLogObj=userAccessLogGateway.GetAccessLog(loginId,logInDate);
        }

        public int SendDataInToDB(UserAccessLog userAccessLogObj)
        {
            int returnValue = 0;
            return returnValue = userAccessLogGateway.SendDataInToDB(userAccessLogObj);
        }
        public int UpdateLoginStatus(UserAccessLog userAccessLogObj)
        {
            int returnValue = 0;
            return returnValue = userAccessLogGateway.UpdateLoginStatus(userAccessLogObj);
        }

        public DataTable GetUnSuccessfullLoginHistory(string loginId)
        {
            return userAccessLogGateway.GetUnSuccessfullLoginHistory(loginId);
        }

        public void DisableUserIf90DaysNotLogin()
        {
            userAccessLogGateway.DisableUserIf90DaysNotLogin();
        }
    }
}
