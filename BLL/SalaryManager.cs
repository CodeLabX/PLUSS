﻿using System;
using System.Collections.Generic;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// SalaryManager Class.
    /// </summary>
    public class SalaryManager
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public SalaryManager()
        {
        }

        private SalaryGateway salaryGatewayObj;

        /// <summary>
        /// This method select all salary info by llid
        /// </summary>
        /// <param name="llIdId">Gets a llid.</param>
        /// <returns>Returns a Salary list object.</returns>
        public List<Salary> GetAllSalaryInfoByLlId(string llId)
        {
            salaryGatewayObj = new SalaryGateway();
            return salaryGatewayObj.GetAllSalaryInfoByLlId(llId);
        }

        /// <summary>
        /// This method provide the operation of insertion of salary.
        /// </summary>
        /// <param name="tPlussSalaryObj">Gets a Salary object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertSalary(Salary salaryObj)
        {
            salaryGatewayObj = new SalaryGateway();
            return salaryGatewayObj.InsertSalary(salaryObj);
        }
        /// <summary>
        /// This method provide the operation of update of salary.
        /// </summary>
        /// <param name="tPlussSalaryObj">Gets a Salary object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdateSalary(Salary tPlussSalaryObj)
        {
            salaryGatewayObj = new SalaryGateway();
            return salaryGatewayObj.UpdateSalary(tPlussSalaryObj);
        }
    }
}
