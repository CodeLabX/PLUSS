﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    public class ExcelTemplateManager
    {
        private ExcelTemplateGateway excelTemplateGatewayObj =null;
        public ExcelTemplateManager()
        {
            excelTemplateGatewayObj = new ExcelTemplateGateway();
        }
        public List<ExcelTemplate> GetAllExcelTemplate()
        {
            return excelTemplateGatewayObj.GetAllExcelTemplate();
        }
        public List<ExcelTemplate> GetAllExcelTemplate(int Id)
        {
            return excelTemplateGatewayObj.GetAllExcelTemplate(Id);
        }
        public int SendData(ExcelTemplate excelTemplateObj)
        {
            return excelTemplateGatewayObj.SendData(excelTemplateObj);
        }
    }
}
