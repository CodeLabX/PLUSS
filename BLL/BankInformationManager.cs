﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    public class BankInformationManager
    {
        BankInformationGateway bankInformationGatewayObj=null;

        public BankInformationManager()
        {
            bankInformationGatewayObj = new BankInformationGateway();
        }

        public List<BankInformation> GetAllBankInfo()
        {
            return bankInformationGatewayObj.GetAllBankInfo();
        }

        public int InsertORUpdateBankInfo(BankInformation bankInformationObj)
        {
            return bankInformationGatewayObj.InsertORUpdateBankInfo(bankInformationObj);
        }

        public bool IsExistsBankCode(string bankCode, int bankId)
        {
            return bankInformationGatewayObj.IsExistsBankCode(bankCode, bankId);
        }

        public bool IsExistsBankName(string bankName, int bankId)
        {
            return bankInformationGatewayObj.IsExistsBankName(bankName, bankId);
        }
        public Int32 GetBankId(string bankName)
        {
            return bankInformationGatewayObj.GetBankId(bankName);
        }
    }
}
