﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;
namespace BLL
{
    public class BankStatementManager
    {
        private BankStatement bankStatementObj = null;
        private BankStatementGateway bankStatementGatewayObj = null;

        public BankStatementManager()
        {
            bankStatementObj = new BankStatement();
            bankStatementGatewayObj = new BankStatementGateway();
        }
        public string GetLLIdEntryUserId(Int64 LLId)
        {
            return bankStatementGatewayObj.GetLLIdEntryUserId(LLId);
        }
        public string SelectACTypeId(string acTypeName)
        {
            return bankStatementGatewayObj.SelectACTypeId(acTypeName);
        }

        public ApplicantOtherBankAccount SelectLoanApplicationOthreBankInfo(Int64 lLID)
        {
            return bankStatementGatewayObj.SelectLoanApplicationOthreBankInfo(lLID);
        }
        //public List<BankStatementEntryInfo> SelectBankStatementEntryInfoList(Int64 lLID)
        //{
        //    return bankStatementGatewayObj.SelectBankStatementEntryList(lLID);
        //}
        public BankStatement GetBankStatement(Int64 LLID, Int16 bankId, Int16 branchId, string acNo)
        {
            return bankStatementGatewayObj.GetBankStatement(LLID, bankId, branchId, acNo);
        }
        public List<BankStatement> GetBankStatementList(Int64 LLID)
        {
            return bankStatementGatewayObj.GetBankStatementList(LLID);
        }
        public int SendBankStatementInToDB(BankStatement bankStatementObj)
        {
            return bankStatementGatewayObj.SendBankStatementInToDB(bankStatementObj);
        }
    }
}
