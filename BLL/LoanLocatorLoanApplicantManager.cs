﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// LoanLocatorLoanApplicantManager Class.
    /// </summary>
    public class LoanLocatorLoanApplicantManager
    {
        LoanLocatorLoanApplicantGateway llApplicantGateway;
        /// <summary>
        /// Constructor
        /// </summary>
        public LoanLocatorLoanApplicantManager()
        {
            llApplicantGateway = new LoanLocatorLoanApplicantGateway();
        }

        /// <summary>
        /// This method select loan locator loan application info.
        /// </summary>
        /// <param name="name">Gets a name.</param>
        /// <param name="fatherName">Gets a father name.</param>
        /// <param name="motherName">Gets a mother name.</param>
        /// <param name="tinNo">Gets a tin no.</param>
        /// <returns>Returns a LoanLocatorLoanApplicant list object.</returns>
        public List<LoanLocatorLoanApplicant> GetLoanLocatorLoanApplicantList(string name, string motherName, List<string> accountNo, string organization)
         {
             //if (string.IsNullOrEmpty(name) && string.IsNullOrEmpty(motherName) && string.IsNullOrEmpty(accountNo) && string.IsNullOrEmpty(organization))
             //{
             //    return new List<LoanLocatorLoanApplicant>();
             //}
             //else
             //{
                return llApplicantGateway.Select(name, motherName, accountNo, organization);
             //}
        }
    }
}
