﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// LoanApplicationQueueManager Class.
    /// </summary>
    public class LoanApplicationQueueManager
    {
        public LoanApplicationQueueGateway loanApplicationQueueGatewayObj = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        public LoanApplicationQueueManager()
        {
            loanApplicationQueueGatewayObj = new LoanApplicationQueueGateway();
        }

        /// <summary>
        /// This method select loan application queue info from pl.
        /// </summary>
        /// <returns>Returns a LoanApplicationQueue list object.</returns>
        public List<LoanApplicationQueue> GetLoanApplicationQueueFromPL()
        {
            return loanApplicationQueueGatewayObj.GetLoanApplicationQueueFromPL();
        }
        /// <summary>
        /// This method select loan application queue from pl
        /// </summary>
        /// <returns>Returns a LoanApplicationQueue list object.</returns>
        public List<LoanApplicationQueue> GetLoanApplicationQueue4PL()
        {
            return loanApplicationQueueGatewayObj.GetLoanApplicationQueue4PL();
        }

        /// <summary>
        /// This method loan application queue from loan locator.
        /// </summary>
        /// <returns>Returns a LoanApplicationQueue list object.</returns>
        public List<LoanApplicationQueue> GetLoanApplicationQueueFromLL()
        {
            return loanApplicationQueueGatewayObj.GetLoanApplicationQueueFromLL();
        }
    }
}
