﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Report;
using BusinessEntities.Report;

namespace BLL.Report
{
    public class DocumentManager
    {
        private DocumentsGateway documentsGatewayObj;
        public DocumentManager()
        {
            //Constructor logic here.
            documentsGatewayObj = new DocumentsGateway();
        }

        /// <summary>
        /// This method provide customer document information.
        /// </summary>
        /// <param name="llId">Gets customer llId.</param>
        /// <returns>Returns document object.</returns>
        public Document GetDocumentsInfo(string llId)
        {
            return documentsGatewayObj.GetDocumentsInfo(llId);
        }
    }
}
