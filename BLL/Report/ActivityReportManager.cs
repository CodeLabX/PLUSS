﻿using System;
using System.Collections.Generic;
using BusinessEntities.Report;
using DAL.Report;

namespace BLL.Report
{
    public class ActivityReportManager
    {
        private DailyUnderlyingActivityReportGateway dailyUnderlyingActivityReportGatewayObj = null;

        public ActivityReportManager()
        {
            dailyUnderlyingActivityReportGatewayObj = new DailyUnderlyingActivityReportGateway();
        }

        public List<ActivityReportObject> GetActivityReportDataList(DateTime toDay)
        {
            return dailyUnderlyingActivityReportGatewayObj.GetActivityReportDataList(toDay);
        }
        public List<DailyNoofApprovedVolume> GetDailyNoOfApproveVolume()
        {
            return dailyUnderlyingActivityReportGatewayObj.GetDailyNoOfApproveVolume();
        }
        public List<DailyNoofApprovedVolume> DailyNoOfApprovedVolumeChannelWise()
        {
            return dailyUnderlyingActivityReportGatewayObj.GetDailyActivityReportByChannelWise();
        }
        public ActivityReportObject GetActivityReportData(DateTime toDay)
        {
            return dailyUnderlyingActivityReportGatewayObj.GetActivityReportData(toDay);
        }
        public List<ActivityReportObject> GetActivityReportByUser(DateTime dateTime, DateTime toDate)
        {
            return dailyUnderlyingActivityReportGatewayObj.GetActivityReportByUser(dateTime, toDate);
        }
        public void SendReceiveStatus(ActivityReportObject activityReportObj)
        {
            dailyUnderlyingActivityReportGatewayObj.SendReceiveStatus(activityReportObj);
        }
        public void SendProcessStatus(ActivityReportObject activityReportObj)
        {
            dailyUnderlyingActivityReportGatewayObj.SendProcessStatus(activityReportObj);
        }        
    }
}
