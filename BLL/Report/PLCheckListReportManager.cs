﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Report;

namespace BLL.Report
{
    /// <summary>
    /// PL Check List Report Manager Class.
    /// </summary>
    public class PLCheckListReportManager
    {
        public PLCheckListReportGateway plCheckListReportGateway;
        /// <summary>
        /// Constructor
        /// </summary>
        public PLCheckListReportManager()
        {

        }
        /// <summary>
        /// This method provide initail data for the report.
        /// </summary>
        /// <param name="llID">Gets llid.</param>
        /// <returns>Returns initial info object.</returns>
        public BusinessEntities.Report.InitialInfo GetInitialInfo(int llID)
        {
            plCheckListReportGateway = new PLCheckListReportGateway();
            return plCheckListReportGateway.GetInitialInfo(llID);
        }
        /// <summary>
        /// This method provide particular segment information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns a segment info object.</returns>
        public BusinessEntities.Report.SegmentInfo GetSegmentInfo(int llID)
        {
            plCheckListReportGateway = new PLCheckListReportGateway();
            return plCheckListReportGateway.GetSegmentInfo(llID);
        }
        /// <summary>
        /// This method provide Exposure information.
        /// </summary>
        /// <param name="llID">Gets llid.</param>
        /// <returns>Returns exposure info object.</returns>
        public BusinessEntities.Report.ExposureInfo GetExposureInfo(int llID)
        {
            plCheckListReportGateway = new PLCheckListReportGateway();
            return plCheckListReportGateway.GetExposureInfo(llID);
        }
        /// <summary>
        /// This method provide deviation info.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns deviation info object.</returns>
        public BusinessEntities.Report.DeviationInfo GetDeviationInfo(int llID)
        {
            plCheckListReportGateway = new PLCheckListReportGateway();
            return plCheckListReportGateway.GetDeviationInfo(llID);
        }
        /// <summary>
        /// This method provide net income information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns net income info object.</returns>
        public BusinessEntities.Report.NetIncomeInfo GetNetIncomeInfo(int llID)
        {
            plCheckListReportGateway = new PLCheckListReportGateway();
            return plCheckListReportGateway.GetNetIncomeInfo(llID);
        }
        /// <summary>
        /// This method provide standing order information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns standing order info object.</returns>
        public BusinessEntities.Report.StandingOrderInfo GetStandingOrderInfo(int llID)
        {
            plCheckListReportGateway = new PLCheckListReportGateway();
            return plCheckListReportGateway.GetStandingOrderInfo(llID);
        }
        /// <summary>
        /// This method provide user information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns user info object.</returns>
        public BusinessEntities.Report.UserInfo GetUserInfo(int llID)
        {
            plCheckListReportGateway = new PLCheckListReportGateway();
            return plCheckListReportGateway.GetUserInfo(llID);
        }
        /// <summary>
        /// This method provide strength weakness information.
        /// </summary>
        /// <param name="llID">Gets a llid.</param>
        /// <returns>Returns strength weakness info object.</returns>
        public BusinessEntities.Report.StrengthWeaknessInfo GetStrengthWeaknessInfo(int llID)
        {
            plCheckListReportGateway = new PLCheckListReportGateway();
            return plCheckListReportGateway.GetStrengthWeaknessInfo(llID);
        }
    }
}
