﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Report;
using BusinessEntities.Report;

namespace BLL.Report
{
    public class ApprovalInformationManager
    {
        private ApprovalInformationGateway approvalInformationGatewayObj;
        public ApprovalInformationManager()
        {
            approvalInformationGatewayObj = new ApprovalInformationGateway();
        }
        /// <summary>
        /// This method provide approval report information.
        /// </summary>
        /// <param name="llId">Gets a llId</param>
        /// <returns>Returns approval information object.</returns>
        public ApprovalInformation GetApprovalReportInformation(string llId)
        {
            return approvalInformationGatewayObj.GetApprovalReportInformation(llId);
        }
    }
}
