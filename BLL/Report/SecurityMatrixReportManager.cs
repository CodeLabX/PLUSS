﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using BusinessEntities.Report;
using DAL.Report;
using System.Data;
using AzUtilities;

namespace BLL.Report
{
    public class SecurityMatrixReportManager
    {
        private SecurityMatrixReportGateway _matrixReportGateway;
        public SecurityMatrixReportManager()
        {
            _matrixReportGateway = new SecurityMatrixReportGateway();
        }



        public List<SecurityMatrix> GetSystemGeneratedSecurityMatrix()
        {
            var datalist = _matrixReportGateway.GetSystemGeneratedSecurityMatrix();
         
            return datalist;
        }
        public DataTable GetSecurityMatrixReportData()
        {
            DataTable table = _matrixReportGateway.GetSecurityMatrixReportData();

            DataTable transposed = ConvertionUtilities.TransposeDataTable(table);

            DataTable modules = _matrixReportGateway.GetModuleTable();

            transposed.Columns[0].ColumnName = "ModuleName";

            for (int colIndex = 0; colIndex < transposed.Rows.Count; colIndex++)
            {
                string moduleCode = transposed.Rows[colIndex]["ModuleName"].ToString();

                for (int i = 0; i < modules.Rows.Count; i++)
                {
                    string mCode = modules.Rows[i]["Code"].ToString();

                    if(moduleCode == mCode)
                    {
                        transposed.Rows[colIndex]["ModuleName"] = modules.Rows[i]["Name"];
                    }
                }
            }

            return transposed;
        }


        public List<SecurityMatrix_Auto> GetSystemGeneratedSecurityMatrix_Auto()
        {
            var datalist = _matrixReportGateway.GetSystemGeneratedSecurityMatrix_Auto();
            return datalist;
        }
    }
}
