﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// Customer manager class.
    /// </summary>
    public class CustomerManager
    {
        private CustomerGateway customerGatewayObj;
        public CustomerManager()
        {
            //Constructor logic here.
            customerGatewayObj = new CustomerGateway();
        }
        #region GetCustomerLoanAppInfo(string llId)
        /// <summary>
        /// This method provide customer loan Application info.
        /// </summary>
        /// <param name="llId">Gets LLId</param>
        /// <returns>Returns a customer object.</returns>
        public Customer GetCustomerLoanAppInfo(string llId)
        {
            return customerGatewayObj.GetCustomerLoanAppInfo(llId);
        }
        #endregion

        #region GetCustomerBankStatementCalculation(string llId)
        /// <summary>
        /// This method provide customer bank calculation info.
        /// </summary>
        /// <param name="llId">Gets LLId</param>
        /// <returns>Returns a customer object.</returns>
        public Customer GetCustomerBankStatementCalculation(string llId)
        {
            return customerGatewayObj.GetCustomerBankStatementCalculation(llId);
        }
        #endregion

        #region GetCustomerInfo(string llId)
        /// <summary>
        /// This method provide customer loan Application info.
        /// </summary>
        /// <param name="llId">Gets LLId</param>
        /// <returns>Returns a customer object.</returns>
        public Customer GetCustomerInfo(string llId)
        {
            return customerGatewayObj.GetCustomerInfo(llId);
        }
        #endregion

        #region GetCustomerId(string llId)
        /// <summary>
        /// This method provide customer id.
        /// </summary>
        /// <param name="llid">Get llId</param>
        /// <returns>Returns a int value.</returns>
        public Int32 GetCustomerId(string llId)
        {
            return customerGatewayObj.GetCustomerId(llId);
        }
        #endregion

        #region GetCustomerDetailInfo(string llId)
        /// <summary>
        /// This method provide customer loan Application info.
        /// </summary>
        /// <param name="llId">Gets a LLId</param>
        /// <returns>Returns a customer list object.</returns>
        public List<CustomerDetail> GetCustomerDetailInfo(string llId)
        {
            return customerGatewayObj.GetCustomerDetailInfo(llId);
        }
        #endregion

        #region GetAllProduct()
        /// <summary>
        /// This method provide all products
        /// </summary>
        /// <returns></returns>
        public List<Product> GetAllProduct()
        {
            return customerGatewayObj.GetAllProduct();
        }
        #endregion

        #region InsertORUpdateCustomerInfo(Customer customerObj)
        /// <summary>
        /// This method provide insert or update customer information.
        /// </summary>
        /// <param name="loanInformationObj">Gets customer object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateCustomerInfo(Customer customerObj)
        {
            return customerGatewayObj.InsertORUpdateCustomerInfo(customerObj);
        }
        #endregion

        #region InsertORUpdateCustomerDetailsInfo(List<CustomerDetail> CustomerDetailListObj)
        /// <summary>
        /// This method provide insert or update customer detail information.
        /// </summary>
        /// <param name="loanInformationObj">Gets customer detail list object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateCustomerDetailsInfo(List<CustomerDetail> CustomerDetailListObj)
        {
            return customerGatewayObj.InsertORUpdateCustomerDetailsInfo(CustomerDetailListObj);
        }
        #endregion

        /// <summary>
        /// This method provide customer exposure information.
        /// </summary>
        /// <param name="llid">Gets a llid.</param>
        /// <returns>Returns exposure amount list.</returns>
        public List<double> GetExposure(string llid)
        {
            return customerGatewayObj.GetExposure(llid);
        }
         /// <summary>
        /// This method provide topup id.
        /// </summary>
        /// <param name="llId">Gets a llid.</param>
        /// <returns>Returns topup id.</returns>
        public int GetTopUpId(int llId)
        {
            return customerGatewayObj.GetTopUpId(llId);
        }
    }
}
