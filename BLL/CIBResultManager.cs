﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// CIBResultManager Class.
    /// </summary>
    public class CIBResultManager
    {
        CIBResultGateway crGateway;// = new CIBResultGateway();
        /// <summary>
        /// Constructor.
        /// </summary>
        public CIBResultManager()
        {
            crGateway = new CIBResultGateway();
        }

        /// <summary>
        /// This function select CIBResult List from DB.
        /// </summary>
        /// <param name="customerName">Gets a customer name</param>
        /// <param name="fatherName"> Gets a father name</param>
        /// <param name="motherName">Gets a mother name</param>
        /// <returns>Returns a CIBResult list object.</returns>
        public List<CIBResult> GetCIBResultList(string customerName, string fatherName, string motherName, string businessName)
        {
            return crGateway.Select(customerName, fatherName, motherName, businessName);
        }
        /// <summary>
        /// This method insertion or update CIB result.
        /// </summary>
        /// <param name="cIBResultObjList">Gets a CIBResult lsit object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendCIBResultInToDB(List<CIBResult> cIBResultObjList)
        {
            return crGateway.SendCIBResultInToDB(cIBResultObjList);
        }

        public List<CIBResult> GetCIBResult(Int32 lowerLimit, Int32 upporLimit)
        {
            return crGateway.GetCIBResult(lowerLimit, upporLimit);
        }
        public Int32 GetTolalRecord()
        {
            return crGateway.GetTolalRecord();
        }
    }
}
