﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    public class BranchInformationManager
    {
        BranchInformationGateway branchInformationGatewayObj;
        public BranchInformationManager()
        {
            //Constructor logic here.
            branchInformationGatewayObj = new BranchInformationGateway();
        }
        #region GetAllLocationInformation
        /// <summary>
        /// This method provide all location information.
        /// </summary>
        /// <returns>Returns location information list object</returns>
        public List<LocationInformation> GetAllLocationInformation()
        {
            return branchInformationGatewayObj.GetAllLocationInformation();
        }
        #endregion

        #region GetAllBranchInformation
        /// <summary>
        /// This method provide all branch information.
        /// </summary>
        /// <returns>Branch information list object.</returns>
        public List<BranchInformation> GetAllBranchInformation()
        {
            return branchInformationGatewayObj.GetAllBranchInformation();
        }
        #endregion

        //#region ImportDataFromExcel
        ///// <summary>
        ///// This method provide import excel data.
        ///// </summary>
        ///// <param name="fileName">Gets a excel file name.</param>
        ///// <param name="sheetName">Gets a sheet name.</param>
        ///// <returns></returns>
        //public List<BranchInformation> ImportDataFromExcel(string fileName)
        //{
        //    return branchInformationGatewayObj.ImportDataFromExcel(fileName);
        //}
        //#endregion

        #region InsertORUpdateBranchInfo
        /// <summary>
        /// This method provide insert or update branch information.
        /// </summary>
        /// <param name="branchInformationListObj">Gets branch information list object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateBranchInfo(List<BranchInformation> branchInformationListObj)
        {
            return branchInformationGatewayObj.InsertORUpdateBranchInfo(branchInformationListObj);
        }
        #endregion
        
        #region GetAllBranchInformation
        /// <summary>
        /// This method provide all branch information.
        /// </summary>
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Branch information list object.</returns>        
        public List<BranchInformation> GetAllBranchInformation(string searchKey)
        {
            return branchInformationGatewayObj.GetAllBranchInformation(searchKey);
        }
        #endregion

        /// <summary>
        /// This method provide all branch info against bank id.
        /// </summary>
        /// <param name="bankId">Gets a bank id.</param>
        /// <returns>Returns a branch information list object.</returns>                
        public List<BranchInformation> GetAllBranchInfoByBankId(int bankId)
        {
            return branchInformationGatewayObj.GetAllBranchInfoByBankId(bankId);
        }

        /// <summary>
        /// This method provide all bank branch information.
        /// </summary>
        /// <returns>Returns a branch information list object.</returns>
        public List<BranchInformation> GetAllBankBranchInfo()
        {
            return branchInformationGatewayObj.GetAllBankBranchInfo();
        }
                
        #region IsExistsBranchName(string branchName, int bankId)
        /// <summary>
        /// This method provide branch name exists or not.
        /// </summary>
        /// <param name="branchName">Get branch name</param>
        /// <param name="branchId">Get branch id.</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsBranchName(string branchName, int branchId, int bankId)
        {
            return branchInformationGatewayObj.IsExistsBranchName(branchName, branchId, bankId);
        }
        #endregion

        public Int32 InsertLocationName(string locationName)
        {
            return branchInformationGatewayObj.InsertLocationName(locationName);
        }

        public Int32 GetLocationId(string locationName)
        {
            return branchInformationGatewayObj.GetLocationId(locationName);
        }
    }
}
