﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    public class JointApplicationManager
    {
        private JointApplicationGateway jointApplicationGateway;
        public JointApplicationManager()
        {
            jointApplicationGateway = new JointApplicationGateway();
        }

        #region GetAllJointApplicantInformation
        /// <summary>
        /// This method provide all joint applicants information.
        /// </summary>
        /// <param name="llid">Gets the applicant LLID</param>
        /// <returns>Returns joint applicant information object</returns>
        public JoinApplicantInformation GetAllJointApplicantInformation(Int32 llid)
        {
            return jointApplicationGateway.GetAllJointApplicantInformation(llid);
        }
        #endregion

        public int InsertORUpdateJointApplicationInfo(JoinApplicantInformation joinApplicantInformationObj)
        {
            return jointApplicationGateway.InsertORUpdateJointApplicationInfo(joinApplicantInformationObj);
        }

        public bool IsExistsLoanInfo(Int32 llid)
        {
            return jointApplicationGateway.IsExistsLoanInfo(llid);
        }
    }
}
