﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// ScbManager Class.
    /// </summary>
    public class ScbManager
    {
        private ScbDetailsGateway scbDetailsGatewayObj = new ScbDetailsGateway();
        private ScbMasterGateway scbMasterGatewayObj = new ScbMasterGateway();
        
        /// <summary>
        /// Constructor
        /// </summary>
        public ScbManager()
        {
        }

        public List<ScbDetails> GetScbDetailsData(Int32 masterId,DateTime startManth,DateTime endMonth)
        {
            return scbDetailsGatewayObj.GetScbDetailsData(masterId, startManth, endMonth);
        } 
        /// <summary>
        /// This method select the all scb details info
        /// </summary>
        /// <returns>Returns a ScbDetails list object.</returns>
        public List<ScbDetails> GetAllScbDetailsInfo()
        {
            return scbDetailsGatewayObj.GetAllScbDetailsInfo();
        }
        /// <summary>
        /// This method select all scb details info by master id
        /// </summary>
        /// <param name="scbMasterId">Gets a scb master id.</param>
        /// <returns>Returns a ScbDetails list object.</returns>
        public List<ScbDetails> GetAllScbDetailsInfo(int masterId)
        {
            return scbDetailsGatewayObj.GetAllScbDetailsInfoByMasterId(masterId);
        }
        /// <summary>
        /// This method provide the operation of insertion of scb details
        /// </summary>
        /// <param name="scbDetailsObjList">Gets a ScbDetails list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertScbDetailsInfo(List<ScbDetails> scbDetailsObjList)
        {
            return scbDetailsGatewayObj.InsertScbDetails(scbDetailsObjList);
        }
        /// <summary>
        /// This method provide the operation of delete of scb details
        /// </summary>
        /// <param name="scbDetailsObjList">Gets a scb master id.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeleteScbDetailsInfo(Int64 scbMasterId)
        {
            return scbDetailsGatewayObj.DeleteScbDetailsObject(scbMasterId);
        }


        /*------------ for scb master gateaway --------------------*/
        /// <summary>
        /// This method select all scb master info
        /// </summary>
        /// <returns>Returns a ScbMaster list object.</returns>
        public List<ScbMaster> GetAllScbMasterInfo()
        {
            return scbMasterGatewayObj.GetAllScbMasterInfo();
        }
        /// <summary>
        /// This method select all scb master info by llid and accountnumber
        /// </summary>
        /// <param name="llId">Gets a llid</param>
        /// <param name="accountNumber">Gets a account no.</param>
        /// <returns>Returns a ScbMaster list object.</returns>
        public List<ScbMaster> GetAllScbMasterInfoByLlidAndAccountNumber(string llId, string accountNumber)
        {
            return scbMasterGatewayObj.GetAllScbMasterInfoByLlidAndAccountNumber(llId, accountNumber);
        }
        /// <summary>
        /// This method provide the operation of insert scb master
        /// </summary>
        /// <param name="scbMasterObj">Gets a ScbMaster object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertScbMasterInfo(ScbMaster scbMasterObj)
        {
            return scbMasterGatewayObj.InsertScbMaster(scbMasterObj);
        }
        /// <summary>
        /// This method provide the operation of delete scb master
        /// </summary>
        ///<param name="masterId">Gets a master id.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeleteScbMasterInfo(Int64 scbMasterId)
        {
            return scbMasterGatewayObj.DeleteScbMasterObject(scbMasterId);
        }
        /// <summary>
        /// This method select all account number by llid
        /// </summary>
        /// <param name="llid">Gets a llid.</param>
        /// <returns>Returns a list of account no.</returns>
        public List<string> GetAllAccountNumberByLlid(string llid)
        {
            scbMasterGatewayObj = new ScbMasterGateway();
            return scbMasterGatewayObj.GetAllAccountNumberByLlid(llid);
        }
    }
}

