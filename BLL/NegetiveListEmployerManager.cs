﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AzUtilities;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// NegetiveListEmployerManager Class.
    /// </summary>
    public class NegetiveListEmployerManager
    {
        private NegetiveListEmployerGateway _negativeList;
        /// <summary>
        /// Constructor
        /// </summary>
        public NegetiveListEmployerManager()
        {
            //Constructor logic here.
            _negativeList = new NegetiveListEmployerGateway();
        }
        public List<NegetiveListEmployer> GetUploadResult(DateTime startDate, DateTime endDate, Int32 offset, Int32 rowsPerPage)
        {
            return _negativeList.GetUploadResult(startDate, endDate, offset, rowsPerPage);
        }
        public List<NegetiveListEmployer> GetUploadedWatchListTempData()
        {
            return _negativeList.GetUploadedWatchListTempData();
        }

        public string GetWatchListTempDataCSV()
        {
            try
            {
                DataTable table = _negativeList.GetTempWatchListDataTable();

                return ConvertionUtilities.ConvertDataTableToCsvString(table);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("NegetiveListEmployerManager :GetTempDataCSV");
                LogFile.WriteLog(ex);
            }
            return null;
        }

        public string ApproveNegativeList(User actionUser)
        {
            return _negativeList.ApproveWatchList(actionUser);
        }

        public Int32 GetTolalRecord(DateTime startDate, DateTime endDate)
        {
            return _negativeList.GetTolalRecord(startDate, endDate);
        }
        /// <summary>
        /// This method select the negetive list employer info.
        /// </summary>
        /// <param name="business">Gets a business name.</param>
        /// <param name="bank">Gets a bank list.</param>
        /// <param name="branch">Gets a branch list.</param>
        /// <returns>Returns a NegetiveListEmployer list object.</returns>
        public List<NegetiveListEmployer> GetNegetiveListEmployer(string business, List<string> bank, List<string> branch)
        {
            return _negativeList.GetNegetiveListEmployer(business, bank, branch);
        }
        /// <summary>
        /// This method select negetive list employer info.
        /// </summary>
        /// <param name="bankCompanyName">Gets a bank company name.</param>
        /// <param name="branch">Gets a branch name.</param>
        /// <returns>Returns a NegetiveListEmployer object.</returns>
        public NegetiveListEmployer SelectNegetiveListEmployer(string bankCompanyName, string branch)
        {
            return _negativeList.SelectNegetiveListEmployer(bankCompanyName,branch);
        }

        /// <summary>
        /// This method provide the insertion or update operation of watch list.
        /// </summary>
        /// <param name="negetiveListEmployerObjList">Gets a NegetiveListEmployer list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendWatchListInToDB(List<NegetiveListEmployer> negetiveListEmployerObjList)
        {
            return _negativeList.SendWatchListInToDB(negetiveListEmployerObjList);
        }

        public string FlushTemp()
        {
            return _negativeList.FlushTempData();
        }
    }
}
