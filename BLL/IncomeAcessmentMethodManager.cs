﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// IncomeAcessmentMethodManager Class.
    /// </summary>
    public class IncomeAcessmentMethodManager
    {
        public IncomeacesmentmethodGateway incomeacesmentmethodGatewayObj=null;
        /// <summary>
        /// Constructor.
        /// </summary>
        public IncomeAcessmentMethodManager()
        {
            incomeacesmentmethodGatewayObj=new IncomeacesmentmethodGateway();
        }
        /// <summary>
        /// This method select income acesment
        /// </summary>
        /// <returns>Returns a Incomeacesmentmethod list object.</returns>
        public List<Incomeacesmentmethod> SelectIncomeAcesmentMethodList()
        {
            return incomeacesmentmethodGatewayObj.SelectIncomeAcesmentMethodList();
        }
        /// <summary>
        /// This method provide income acesment info.
        /// </summary>
        /// <param name="incomeMethodListId">Gets a income id.</param>
        /// <returns>Returns a Incomeacesmentmethod object.</returns>
        public Incomeacesmentmethod PopIncomeAcesmentMethod(int incomeMethodListId)
        {
            return incomeacesmentmethodGatewayObj.PopIncomeAcesmentMethodList(incomeMethodListId);
        }
    }
}
