﻿using AzUtilities;
using BusinessEntities;
using BusinessEntities.SecurityMatrixEntities;
using DAL.SecurityMatrixGateways;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.SecurityMatrixBLL
{

    public class PageAccessPermissionManager
    {
        PageAccessPermission _permission = null;
        public PageAccessPermissionManager()
        {
            _permission = new PageAccessPermission();
        }

        public List<MenuModel> GetMenuListForUser(User user)
        {
            return _permission.GetMenuListForUser(user);
        }
    }
}
