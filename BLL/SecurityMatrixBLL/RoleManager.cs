﻿using BusinessEntities;
using BusinessEntities.SecurityMatrixEntities;
using DAL.SecurityMatrixGateways;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.SecurityMatrixBLL
{
    public class RoleManager
    {
        RoleGateway _roles = null;
        public RoleManager()
        {
            _roles = new RoleGateway();
        }

        public List<RoleModel> Get()
        {
            return _roles.Get();
        }
    }
}
