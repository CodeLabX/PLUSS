﻿using BusinessEntities.SecurityMatrixEntities;
using DAL.SecurityMatrixGateways;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.SecurityMatrixBLL
{
    public class UserStatusManager
    {
        UserStatusGateway _gateway = new UserStatusGateway();

        public List<UserStatusModel> Get()
        {
            return _gateway.Get();
        } 
        public UserStatusModel Get(int value)
        {
            return _gateway.Get(value);
        }
    }
}
