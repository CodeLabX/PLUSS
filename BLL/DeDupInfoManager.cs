﻿using System.Collections.Generic;
using BusinessEntities;
using DAL;
using System;
using System.Data;
using System.Text;
using System.Linq;
using AzUtilities;
using AzUtilities.Common;

namespace BLL
{
    public class DeDupInfoManager
    {
        private DeDupInfoGateway _gateway = null;
        public DeDupInfoManager()
        {
            _gateway = new DeDupInfoGateway();
        }
        public static long GetCurrentUploadedDataNumber()
        {
            return DeDupInfoGateway.uploadDataCount;
        }

        public DataTable GetDeDupDeclinedData()
        {
            return _gateway.GetDeDupDeclinedData();
        }

        //public List<DeDupInfo> GetAllDeDupInfo(string name, string fatherName, string motherName, string businessEmployeeName, DateTime dob)
        //{
        //    DeDupInfoGateway _gateway = new DeDupInfoGateway();
        //    return _gateway.GetAllDeDupInfo(name, fatherName, motherName, businessEmployeeName, dob);
        //}

        public List<DeDupInfo> GetAllDeDupInfo(string brokenNameList, string fatherName, string motherName, string businessName, string dob, List<string> contact, List<string> id, List<string> ac, /*List<string> bankBranch,*/ string tin, int type)
        {
            try
            {
                return _gateway.GetAllDeDupInfo(brokenNameList, fatherName, motherName, businessName, dob, contact, id, ac, /*bankBranch,*/ tin, type);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //--------------------------------
        public int SendDedupeInToDB(List<DeDupInfo> deDupInfoObjList)
        {
           
            //return _gateway.SendDedupeInToDB(deDupInfoObjList);
            return _gateway.SendDedupeInToFinalTable(deDupInfoObjList);
           
        }
        public void DeleteTempData(string status)
        {
            _gateway.DeleteTempData(status);
        }
        //----------------------------------
        public int SendDedupInfoInToDB(List<DeDupInfo> deDupInfoObjList)
        {
            return _gateway.SendDedupInfoInToDB(deDupInfoObjList);
        }  
        
        public int SendDedupDeclineInfoInToDB(List<DeDupInfo> deDupInfoObjList, User user)
        {
            return _gateway.SendDedupDeclineInfoInToDB(deDupInfoObjList, user);
        } 
        public int InsertIntoDeDupNegativeTemp(List<DeDupInfo> deDupInfoObjList, User actionUser)
        {
            try
            {
                DataTable table = ListDataTableManager.GetListToDataTable<DeDupInfo>(deDupInfoObjList);

                table.Columns.Add("MAKER_PS_ID");
                table.Columns.Add("MAKE_DATE");

                for (int index = 0; index < table.Rows.Count; index++)
                {
                    table.Rows[index]["MAKER_PS_ID"] = actionUser.UserId;
                    table.Rows[index]["MAKE_DATE"] = DateTime.Now.ToString();
                }

                return _gateway.InsertIntoDeDupNegativeTemp(table);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("InsertIntoDeDupNegativeTemp : Dedupinfomanager");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "InsertIntoDeDupNegativeTemp");
            }
            return 0;
        }

        public string GetDedupNegativeListTempDataCSV()
        {
            try
            {
                DataTable table = _gateway.GetDedupNegativeListTempDataTable();

                string csv = ConvertionUtilities.ConvertDataTableToCsvString(table);

                return csv;
            }
            catch (Exception ex)
            {
                return Operation.Error.ToString();
            }
        }

        public int GetProductId(string productName)
        {
            return _gateway.GetProductId(productName);
        }
        public List<DeDupInfo> GetUploadResult(DateTime startDate, DateTime endDate, string type, Int32 offset, Int32 rowsPerPage)
        {
            return _gateway.GetUploadResult(startDate, endDate, type, offset, rowsPerPage);
        } 
        public Int32 GetTolalRecord(DateTime startDate, DateTime endDate, string type)
        {
            return _gateway.GetTolalRecord(startDate, endDate, type);
        }
        public string GetLoanLocatorUserId(string userId)
        {
            return _gateway.GetLoanLocatorUserId(userId);
        }

        public string DeleteDeclineTemp(string DE_DUP_ID)
        {
            return _gateway.DeleteDeclineTemp(DE_DUP_ID);
        }

        public string SendUploadDataToDB(DataTable uploadDat, string status)
        {
            return _gateway.SendUploadDataToDB(uploadDat, status);
        }
        
        public void SaveMFU_UploadResult(string fileName, DateTime uploadDate, string userId, int rowNom, string activity, string fileType)
        {
             _gateway.SaveMFU_UploadResult(fileName, uploadDate, userId, rowNom, activity, fileType);
        }

        public int SendDedupeInToFinalTable(List<DeDupInfo> deDupInfoObjList) 
        {
            return _gateway.SendDedupeInToFinalTable(deDupInfoObjList);
        }

        public int SendDedupeInToFinalTable(DataTable table, string fileType, User user, DateTime uploadDate)
        {
            table.Columns.Add("MAKER_PS_ID");
            table.Columns.Add("MAKE_DATE");

            for (int index = 0; index < table.Rows.Count; index++)
            {
                table.Rows[index]["MAKER_PS_ID"] = user.UserId;
                table.Rows[index]["MAKE_DATE"] = uploadDate; //  DateTime.Now.ToString();
            }
            return _gateway.SendDedupeInToFinalTable(table, fileType, user); 
        }

        public DataTable GetMFU_FileUploadStatus()
        {
            return _gateway.GetMFU_FileUploadStatus();
        }

        #region Clear Data

        public bool ClearTempTableForCC_CHARGEOFF()
        {
            return _gateway.ClearTempTableForCC_CHARGEOFF();
        }
        public bool ClearTempTableForCC_DISBURSED()
        {
            return _gateway.ClearTempTableForCC_DISBURSED();
        }
        public bool ClearTempTableForLOAN_CHARGEOFF()
        {
            return _gateway.ClearTempTableForLOAN_CHARGEOFF();
        }
        public bool ClearTempTableForLOAN_DISBURSED()
        {
            return _gateway.ClearTempTableForLOAN_DISBURSED();
        }
        public bool ClearTempTableForLOAN_EBBS_CHARGEOFF()
        {
            return _gateway.ClearTempTableForLOAN_EBBS_CHARGEOFF();
        }
        public bool ClearTempTableForLOAN_EBBS_DISBURSED()
        {
            return _gateway.ClearTempTableForLOAN_EBBS_DISBURSED();
        }
        #endregion


        #region  Data Checking / process
        public bool CheckDataForCC_CHARGEOFF()
        {
            return _gateway.CheckDataForCC_CHARGEOFF();
        }

        public bool CheckDataForCC_DISBURSED()
        {
            return _gateway.CheckDataForCC_DISBURSED();
        }

        public bool CheckDataForLOAN_CHARGEOFF()
        {
            return _gateway.CheckDataForLOAN_CHARGEOFF();
        }

        public bool CheckDataForLOAN_DISBURSED()
        {
            return _gateway.CheckDataForLOAN_DISBURSED();
        }

        public bool CheckDataForLOAN_EBBS_CHARGEOFF()
        {
            return _gateway.CheckDataForLOAN_EBBS_CHARGEOFF();
        }

        public bool CheckDataForLOAN_EBBS_DISBURSED()
        {
            return _gateway.CheckDataForLOAN_EBBS_DISBURSED();
        }
        #endregion


        #region Move To main or Approve.. 

        public bool MoveToMainForCC_CHARGEOFF(string userid)
        {
            return _gateway.MoveToMainForCC_CHARGEOFF(userid);
        }
        public bool MoveToMainForCC_DISBURSED(string userid)
        {
            return _gateway.MoveToMainForCC_DISBURSED(userid);
        }
        public bool MoveToMainForLOAN_CHARGEOFF(string userid)
        {
            return _gateway.MoveToMainForLOAN_CHARGEOFF(userid);
        }
        public bool MoveToMainForLOAN_DISBURSED(string userid)
        {
            return _gateway.MoveToMainForLOAN_DISBURSED(userid);
        }
        public bool MoveToMainForLOAN_EBBS_CHARGEOFF(string userid)
        {
            return _gateway.MoveToMainForLOAN_EBBS_CHARGEOFF(userid);
        }
        public bool MoveToMainForLOAN_EBBS_DISBURSED(string userid)
        {
            return _gateway.MoveToMainForLOAN_EBBS_DISBURSED(userid);
        }

        #endregion


        #region Error File Download 
        public string GetCSVStringWithError()
        {
            try
            {
                DataTable table = _gateway.GetErrorData();

                StringBuilder sb = ConvertDataTableToCsvFile(table);

                #region Old
                //string[] columnNames = table.Columns.Cast<DataColumn>().
                //                                  Select(column => column.ColumnName).
                //                                  ToArray();
                //sb.AppendLine("\"" + string.Join("\",\"", columnNames) + "\"");

                //string str = sb.ToString();

                //foreach (DataRow row in table.Rows)
                //{
                //    string[] fields = row.ItemArray.Select(field => field.ToString()).ToArray();
                //    //sb.AppendLine("\"" + string.Join(",", fields));

                //    foreach (string data in fields)
                //    {
                //        //if (data.Contains("\""))
                //        //{
                //        //    sb.Append(data.Replace("\"", "'"));
                //        //    sb.Append(String.Format("\"{0}\"", data));
                //        //}
                //        //else if (data.Contains(",") || data.Contains(System.Environment.NewLine))
                //        //{
                //        //    sb.Append(String.Format("\"{0}\"", data));
                //        //}
                //        //else
                //        //{
                //        //    sb.Append(data);
                //        //}

                //        string words = String.Empty;
                //        if(data == "MRS. SHAMSUNNAHAR")
                //            words = String.Empty;

                //        if (data.Contains("\""))
                //            words = data.Replace("\"", "'");

                //        words = string.Format("\"{0}\"", data);

                //        sb.Append(words);
                //        sb.Append(",");


                //    }
                //    sb.Remove(sb.Length - 1, 1);
                //    sb.Append(Environment.NewLine);
                //}

                //string str2 = sb.ToString(); 
                #endregion
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetCSVStringWithErrorForCC_CHARGEOFF()
        {
            try
            {
                DataTable table = _gateway.GetErrorDataForCC_CHARGEOFF();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetCSVStringWithErrorForCC_DISBURSED()
        {
            try
            {
                DataTable table = _gateway.GetErrorDataForCC_DISBURSED();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetCSVStringWithErrorForLOAN_CHARGEOFF()
        {
            try
            {
                DataTable table = _gateway.GetErrorDataForLOAN_CHARGEOFF();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetCSVStringWithErrorForLOAN_DISBURSED()
        {
            try
            {
                DataTable table = _gateway.GetErrorDataForLOAN_DISBURSED();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetCSVStringWithErrorForLOAN_EBBS_CHARGEOFF()
        {
            try
            {
                DataTable table = _gateway.GetErrorDataForLOAN_EBBS_CHARGEOFF();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetCSVStringWithErrorForLOAN_EBBS_DISBURSED()
        {
            try
            {
                DataTable table = _gateway.GetErrorDataForLOAN_EBBS_DISBURSED();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Temp data doanload

        
        public string GetTempDataCSVForCC_CHARGEOFF()
        {
            try
            {
                DataTable table = _gateway.GetTempDataCSVForCC_CHARGEOFF();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public string GetTempDataCSVForCC_DISBURSED()
        {
            try
            {
                DataTable table = _gateway.GetTempDataCSVForCC_DISBURSED();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public string GetTempDataCSVForLOAN_CHARGEOFF()
        {
            try
            {
                DataTable table = _gateway.GetTempDataCSVForLOAN_CHARGEOFF();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public string GetTempDataCSVForLOAN_DISBURSED()
        {
            try
            {
                DataTable table = _gateway.GetTempDataCSVForLOAN_DISBURSED();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public string GetTempDataCSVForLOAN_EBBS_CHARGEOFF()
        {
            try
            {
                DataTable table = _gateway.GetTempDataCSVForLOAN_EBBS_CHARGEOFF();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public string GetTempDataCSVForLOAN_EBBS_DISBURSED()
        {
            try
            {
                DataTable table = _gateway.GetTempDataCSVForLOAN_EBBS_DISBURSED();
                StringBuilder sb = ConvertDataTableToCsvFile(table);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion


        //This method convertrs the DataTable to Csv (in the form of StringBuilder instance).
        public StringBuilder ConvertDataTableToCsvFile(DataTable dtData)
        {
            StringBuilder data = new StringBuilder();

            //Taking the column names.
            for (int column = 0; column < dtData.Columns.Count; column++)
            {
                //Making sure that end of the line, shoould not have comma delimiter.
                if (column == dtData.Columns.Count - 1)
                    data.Append(dtData.Columns[column].ColumnName.ToString().Replace(",", ";"));
                else
                    data.Append(dtData.Columns[column].ColumnName.ToString().Replace(",", ";") + ',');
            }

            data.Append(Environment.NewLine);//New line after appending columns.

            for (int row = 0; row < dtData.Rows.Count; row++)
            {
                for (int column = 0; column < dtData.Columns.Count; column++)
                {
                    ////Making sure that end of the line, shoould not have comma delimiter.
                    if (column == dtData.Columns.Count - 1)
                        data.Append(dtData.Rows[row][column].ToString().Replace(",", ";"));
                    else
                        data.Append(dtData.Rows[row][column].ToString().Replace(",", ";") + ',');
                }

                //Making sure that end of the file, should not have a new line.
                if (row != dtData.Rows.Count - 1)
                    data.Append(Environment.NewLine);
            }
            return data;
        }

        public bool DeleteErrorsFromtemp()
        {
            return _gateway.DeleteErrorsFromtemp();
        }


        public List<DeDupInfo> GetTempDedupNegativeList()
        {
            return _gateway.GetTempDedupNegativeList();
        }

        public string FlushDeDupNegativListTemp()
        {
            return _gateway.FlushDeDupNegativListTemp();
        } 
        public string ApproveDeDupNegativListTemp(User actionUser)
        {
            return _gateway.ApproveDeDupNegativListTemp(actionUser);
        }

        public bool ApproveDeDupDecline(int deDupId, User actionUser)
        {
            return _gateway.ApproveDeDupDecline(deDupId, actionUser);
        }





        public string FlushCustomerBulkDataTemp()
        {
            return _gateway.FlushCustomerBulkDataTemp();
        }


        public string GetDCustomerBulkDataTempDataCSV()
        {
            try
            {
                DataTable table = _gateway.GetCustomerBulkDataTempListTable();

                string csv = ConvertionUtilities.ConvertDataTableToCsvString(table);

                return csv;
            }
            catch (Exception ex)
            {
                return Operation.Error.ToString();
            }
        }
    }
}
