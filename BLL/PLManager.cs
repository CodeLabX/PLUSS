﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// PLManager Class.
    /// </summary>
    public class PLManager
    {
        public PLGateway pLGatewayObj = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        public PLManager()
        {
            pLGatewayObj = new PLGateway();
        }
        /// <summary>
        /// This method select segment id.
        /// </summary>
        /// <param name="segment">Gets a segment name.</param>
        /// <returns>Returns a segemtn id.</returns>
        public int GetSegmentId(string segment)
        {
            return pLGatewayObj.GetSegmentId(segment);
        }
        /// <summary>
        /// This method select profession id.
        /// </summary>
        /// <param name="professionName">Gets a profession name.</param>
        /// <returns>Returns a profession id.</returns>
        public int SelectProfessionId(string professionName)
        {
            return pLGatewayObj.SelectProfessionId(professionName);
        }
        /// <summary>
        /// This method select pl info.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a PL object.</returns>
        public PL SelectPLInfo(Int64 lLId)
        {
            return pLGatewayObj.SelectPLInfo(lLId);
        }
        /// <summary>
        /// This method provide pl id.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a pl id.</returns>
        public Int64 GetPLId(Int64 lLId)
        {
            return pLGatewayObj.GetPLId(lLId);
        }
        /// <summary>
        /// This method provide insert or update operation for pl info.
        /// </summary>
        /// <param name="pLObj">Gets a PL object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendPLInToDB(PL pLObj)
        {
            return pLGatewayObj.SendPLInToDB(pLObj);
        }
        /// <summary>
        /// This method provide operation of deletion for pl info.
        /// </summary>
        /// <param name="pLObj">Gets a PL object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeletePLinToDB(PL pLObj)
        {
            return pLGatewayObj.DeletePLinToDB(pLObj);
        }
        /// <summary>
        /// This method select over draft info.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a PLOverDraft list object.</returns>
        public List<PLOverDraft> SelectOverDraft(Int64 lLId)
        {
            return pLGatewayObj.SelectOverDraft(lLId);
        }
        /// <summary>
        /// This method provide the operation of insertion or update for over draft info.
        /// </summary>
        /// <param name="pLOverDraftObjList">Gets a PLOverDraft list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendOverDraftInToDB(List<PLOverDraft> pLOverDraftObjList)
        {
            return pLGatewayObj.SendOverDraftInToDB(pLOverDraftObjList);
        }
    }
}
