﻿using System.Collections.Generic;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// DeviationManager Class.
    /// </summary>
    public class DeviationManager
    {
        private DeviationGateaway deviationGateawayObj = new DeviationGateaway();

        /// <summary>
        /// This method provide all deviation information.
        /// </summary>
        /// <returns>Returns a Deviation list object</returns>
        public List<Deviation> GetAllDeviationInfo()
        {
            return deviationGateawayObj.GetAllDeviationInfo();
        }
        /// <summary>
        /// This method provide particular devaition info against llid.
        /// </summary>
        /// <param name="llId">Gets a llid.</param>
        /// <returns>Returns a Deviation list object.</returns>
        public List<Deviation> GetAllDeviationInfoByLlId(string llid)
        {
            return deviationGateawayObj.GetAllDeviationInfoByLlId(llid);
        }
        /// <summary>
        /// This method provide particular deviation info against pl id.
        /// </summary>
        /// <param name="plId">Gets a pl id.</param>
        /// <returns>Returns a Deviation list object.</returns>
        public List<Deviation> GetAllDeviationInfoByPlId(int plid)
        {
            return deviationGateawayObj.GetAllDeviationInfoByPlId(plid);
        }
        /// <summary>
        /// This method provide the insertion operation of deviation.
        /// </summary>
        /// <param name="deviation">Gets a Deviation object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertDeviationInfo(Deviation deviation)
        {
            return deviationGateawayObj.InsertDeviation(deviation);
        }
        /// <summary>
        /// This method provide the update operation of deviation.
        /// </summary>
        /// <param name="deviation">Gets a Deviation object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdateDeviationInfo(Deviation deviation)
        {
            return deviationGateawayObj.UpdateDeviation(deviation);
        }
    }
}
