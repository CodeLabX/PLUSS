﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    public class SourceManager
    {
        private SourceGateway sourceGatewayObj = null;
        public SourceManager()
        {
            sourceGatewayObj = new SourceGateway();
        }
        public Int32 GetSourceMaxId()
        {
            return sourceGatewayObj.GetSourceMaxId();
        }
        public List<Source> GetSourceList()
        {
            return sourceGatewayObj.GetSourceList();
        }
        public List<Source> GetSourceList(string searchKey)
        {
            return sourceGatewayObj.GetSourceList(searchKey);
        }
        public Source GetSource(Int32 sourceId)
        {
            return sourceGatewayObj.GetSource(sourceId);
        }
        public int SendSourceToDB(Source sourceObj)
        {
            return sourceGatewayObj.SendSourceToDB(sourceObj);
        }
    }
}
