﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// RemarksManager Class.
    /// </summary>
    public class RemarksManager
    {
        public RemarksGateway remarksGatewayObj = null;
        /// <summary>
        /// Constructor.
        /// </summary>
        public RemarksManager()
        {
            remarksGatewayObj = new RemarksGateway();
        }
        /// <summary>
        /// This method select remarks list.
        /// </summary>
        /// <returns>Returns a Remarks list object.</returns>
        public List<Remarks> SelectRemarksList()
        {
            return remarksGatewayObj.SelectRemarksList();
        }
        /// <summary>
        /// This method select remarks list against search string.
        /// </summary>
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns a Remarks list object.</returns>
        public List<Remarks> SelectRemarksList(string searchKey)
        {
            return remarksGatewayObj.SelectRemarksList(searchKey);
        }
        /// <summary>
        /// This method select remarks list.
        /// </summary>
        /// <param name="remarksFor">Gets a remarks for.</param>
        /// <returns>Returns a Remarks list object.</returns>
        public List<Remarks> SelectRemarksList(int remarksFor)
        {
            return remarksGatewayObj.SelectRemarksList(remarksFor);
        }
        /// <summary>
        /// This method select remarks against id.
        /// </summary>
        /// <param name="remarksId">Gets a remarks id.</param>
        /// <returns>Returns a Remarks object.</returns>
        public Remarks SelectRemarks(Int32 remarksId)
        {
            return remarksGatewayObj.SelectRemarks(remarksId);
        }
        /// <summary>
        /// This method provide the operation of insertion or update for remarks.
        /// </summary>
        /// <param name="remarksObj">Gets a Remars object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendRemarksInToDB(Remarks remarksObj)
        {
            return remarksGatewayObj.SendRemarksInToDB(remarksObj);
        }
        /// <summary>
        /// This method select the remarks max id.
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public Int32 SelectRemarksMaxId()
        {
            return remarksGatewayObj.SelectRemarksMaxId();
        }
        /// <summary>
        /// This method select remarks for list.
        /// </summary>
        /// <returns>Returns a  RemarksFor list object.</returns>
        public List<RemarksFor> SelectRemarksForList()
        {
            return remarksGatewayObj.SelectRemarksForList();
        }
    }
}
