﻿using System;
using System.Web;
using DAL;

namespace BLL
{
    public  class ExceptionLogManager
    {
        public ExceptionLogGateway _exceptionLogGateway;
        private System.Web.UI.Page _ui;

        public ExceptionLogManager(System.Web.UI.Page ui)
        {
            _exceptionLogGateway = new ExceptionLogGateway();
            _ui = ui;
        }

        public void SaveExceptionLog(string operationType,Exception exception)
        {
            var exLog = new ExceptionLog();
            var userId = HttpContext.Current.Session["User"].ToString();
            exLog.OperationType = operationType;
            exLog.ExceptionDescription = exception.Message.Replace("'", " ");
            exLog.UserId = userId;
            exLog.ExceptionDate = DateTime.Now;
            if (_exceptionLogGateway != null) _exceptionLogGateway.SaveExceptionLog(exLog);
        }


      
    }
}
