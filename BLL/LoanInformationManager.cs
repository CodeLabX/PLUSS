﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// Loan information manager class.
    /// </summary>
    public class LoanInformationManager
    {
        private LoanInformation loanInformationObj;
        private LoanInformationGateway loanInformationGatewayObj;
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LoanInformationManager()
        {
            //Constructor logic here.
            loanInformationObj = new LoanInformation();
            loanInformationGatewayObj = new LoanInformationGateway();            
        }
        #endregion

        #region Particular Applicant Loan information
        /// <summary>
        /// This method give particular applicant loan information.
        /// </summary>
        /// <param name="llId">Gets applicant LLID</param>
        /// <returns>Returns loan information object</returns>
        public LoanInformation GetParticularApplicantLoanInfo(Int32 llId)
        {
            return loanInformationGatewayObj.GetApplicantLoanInfo(llId);
        }
        #endregion
    }
}
