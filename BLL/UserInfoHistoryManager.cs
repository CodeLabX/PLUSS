﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using DAL;

namespace BLL
{
    public class UserInfoHistoryManager
    {
        public UserInfoHistoryGateway userInfoHistoryGatewayObj = null;
        public UserInfoHistoryManager()
        {
            userInfoHistoryGatewayObj = new UserInfoHistoryGateway();
        }
        public List<UserInfoHistory> GetUserInfoHistory()
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            return userInfoHistoryObjList = userInfoHistoryGatewayObj.GetUserInfoHistory();
        }
        public List<UserInfoHistory> GetUserInfoHistory(string loginId)
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            return userInfoHistoryObjList = userInfoHistoryGatewayObj.GetUserInfoHistory(loginId);
        }
        public List<UserInfoHistory> GetUserInfoHistory(string loginId, DateTime changeDate)
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            return userInfoHistoryObjList = userInfoHistoryGatewayObj.GetUserInfoHistory(loginId,changeDate);
        }
        public List<UserInfoHistory> GetUserInfoHistory(string loginId, DateTime startDate, DateTime endDate)
        {
            List<UserInfoHistory> userInfoHistoryObjList = new List<UserInfoHistory>();
            return userInfoHistoryObjList = userInfoHistoryGatewayObj.GetUserInfoHistory(loginId,startDate,endDate);
        }

        public bool GetLastChangeUserInfoHistory(string loginId,string password, int numberOfChange)
        {
            List<UserInfoHistory> userInfoHistoryObjResultCheckList = new List<UserInfoHistory>();
            List<UserInfoHistory> userInfoHistoryObjList = userInfoHistoryGatewayObj.GetLastChangeUserInfoHistory(loginId, numberOfChange);
            userInfoHistoryObjResultCheckList = userInfoHistoryObjList.FindAll(findMatchHistory => findMatchHistory.PassWord == password);
            if (userInfoHistoryObjResultCheckList.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public UserInfoHistory GetLastPsswordChangedDate(string loginId)
        {
           return userInfoHistoryGatewayObj.GetLastPsswordChangedDate(loginId);
        }

        public int SendDataInToDB(UserInfoHistory userInfoHistoryObj)
        {
            int returnValue = 0;
            return returnValue = userInfoHistoryGatewayObj.SendDataInToDB(userInfoHistoryObj);
        }

    }
}
