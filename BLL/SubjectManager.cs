﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{   
    /// <summary>
    /// SubjectManager Class.
    /// </summary>
    public class SubjectManager
    {
        public SubjectGateway subjectGatewayObj = null;
        /// <summary>
        /// Cosntructor
        /// </summary>
        public SubjectManager()
        {
            subjectGatewayObj=new SubjectGateway();
        }
        /// <summary>
        /// This method select subject list
        /// </summary>
        /// <returns>Returns a Subject list object.</returns>
        public List<Subject> SelectSubjectList()
        {
            return subjectGatewayObj.SelectSubjectList();
        }
    }
}
