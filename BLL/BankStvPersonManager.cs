﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    public class BankStvPersonManager
    {
        public BankStvPersonGateway bankStvPersonGatewayObj = null;
        public BankStvPersonManager()
        {
            bankStvPersonGatewayObj = new BankStvPersonGateway();
        }
        public BankStvPerson SelectBankStvPersonInfo(Int16 bankId)
        {
            return bankStvPersonGatewayObj.SelectBankStvPersonInfo(bankId);
        }
        public List<BankStvPerson> SelectBankStvPersonList(Int16 bankId, Int16 branchId)
        {
            return bankStvPersonGatewayObj.SelectBankStvPersonList(bankId,branchId);
        }

        public int InsertBankStvPerson(BankStvPerson bankStvPersonObj)
        {
            return bankStvPersonGatewayObj.InsertBankStvPerson(bankStvPersonObj);
        }

        public int UpdateBankStvPerson(BankStvPerson bankStvPersonObj)
        {
            return bankStvPersonGatewayObj.UpdateBankStvPerson(bankStvPersonObj);
        }

        public int DeleteBankStvPerson(int bankStvPersonId)
        {
            return bankStvPersonGatewayObj.DeleteBankStvPerson(bankStvPersonId);
        }

        public List<BankStvPerson> SelectBankStvPersonList()
        {
            return bankStvPersonGatewayObj.SelectBankStvPersonList();
        }

        public List<BankStvPerson> SelectBankStvPersonList(string searchingKey)
        {
            return bankStvPersonGatewayObj.SelectBankStvPersonList(searchingKey);
        }

        public List<string> GetBankBrnchNameById(int bankId, int branchId)
        {
            return bankStvPersonGatewayObj.GetBankBrnchNameById(bankId, branchId);
        }
        public Int32 GetBSVPersonId(Int32 BankId)
        {
            return bankStvPersonGatewayObj.GetBSVPersonId(BankId);
        }
    }
}
