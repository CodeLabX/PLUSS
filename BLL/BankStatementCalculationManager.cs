﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;
namespace BLL
{
    public class BankStatementCalculationManager
    {
        private BankStatementManager bankStatementManagerObj = null;
        private BankStatementCalculationGateway bankStatementCalculationGatewayObj = null;

        public BankStatementCalculationManager()
        {
            bankStatementManagerObj = new BankStatementManager();
            bankStatementCalculationGatewayObj = new BankStatementCalculationGateway();
        }

        public BankStatementCalculation GetBankStatementCalculation(Int64 lLId)
        {
            return bankStatementCalculationGatewayObj.GetBankStatementCalculation(lLId);
        }

        public int SendBankStatementCalculationInToDB(BankStatementCalculation bankStatementCalculationObj)
        {
            return bankStatementCalculationGatewayObj.SendBankStatementCalculationInToDB(bankStatementCalculationObj);
        }

        //public List<ScbMaster> SelectScbMasterInfo(Int32 lLId)
        //{
        //    return bankStatementCalculationGatewayObj.SelectScbMasterInfo(lLId);
        //}

        //public List<SCBHistoricalEntryInfo> SelectSCBHistoricalEntryInfo(Int32 lLId, string acNo)
        //{
        //    return bankStatementCalculationGatewayObj.SelectSCBHistoricalEntryInfo(lLId,acNo);
        //}
        //public List<string> SelectScbMasterMonthList(Int64 scbMasterId)
        //{
        //    return bankStatementCalculationGatewayObj.SelectScbMasterMonthList(scbMasterId);
        //}
    }
}
