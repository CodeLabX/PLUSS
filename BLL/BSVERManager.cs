﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// BSVERManager Class.
    /// </summary>
    public class BSVERManager
    {
        public BsvarGateway bsvarGatewayObj = null;
        /// <summary>
        /// Constructor.
        /// </summary>
        public BSVERManager()
        {
            bsvarGatewayObj = new BsvarGateway();
        }
        /// <summary>
        /// This method select BSVer master id.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <param name="statementTypeId">Gets a statement type id</param>
        /// <param name="bankId">Gets a bank id.</param>
        /// <param name="branchId">Gets a branch id.</param>
        /// <param name="subjectId">Gets a subject id.</param>
        /// <param name="acNo">Gets a account no.</param>
        /// <returns>Returns a BSVER master id.</returns>
        public Int64 SelectBSVerMasterId(Int64 lLId, Int16 statementTypeId, Int16 bankId, Int16 branchId, Int16 subjectId, string acNo)
        {
            return bsvarGatewayObj.SelectBSVerMasterId(lLId,statementTypeId,bankId,branchId,subjectId,acNo);
        }
        /// <summary>
        /// This method select account name.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <param name="bankId">Gets a bank id.</param>
        /// <param name="branchId">Gets a branch id.</param>
        /// <param name="acNo">Gets a account no.</param>
        /// <returns>Returns a account name.</returns>
        public string SelectAccountName(Int64 lLId, Int16 bankId, Int16 branchId, string acNo)
        {
            return bsvarGatewayObj.SelectAccountName(lLId,bankId,branchId,acNo);
        }
        /// <summary>
        /// This method select customer name address.
        /// </summary>
        /// <param name="lLId">Gets a llid.</param>
        /// <returns>Returns a customer name address.</returns>
        public string SelectCustomerNameAddress(Int64 lLId)
        {
            return bsvarGatewayObj.SelectCustomerNameAddress(lLId);
        }
        /// <summary>
        /// This method provide insertion or update BSVER master info.
        /// </summary>
        /// <param name="bsverMasterObj">Gets a BSVERMaster object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendBSVERMasterInfoInToDB(BSVERMaster bsverMasterObj)
        {
            return bsvarGatewayObj.SendBSVERMasterInfoInToDB(bsverMasterObj);
        }
        /// <summary>
        /// This method provide insertion or update BSVER details info.
        /// </summary>
        /// <param name="bsverDetailsObjList">Gets a BSVERDetails list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendBSVERDetailsInfoInToDB(List<BSVERDetails> bsverDetailsObjList)
        {
            return bsvarGatewayObj.SendBSVERDetailsInfoInToDB(bsverDetailsObjList);
        }
        /// <summary>
        /// This method select BASVER report info list.
        /// </summary>
        /// <param name="bsVerMasterId">Gets a bsver master id.</param>
        /// <returns>Returns a BASVERReportInfo list object.</returns>
        public List<BASVERReportInfo> SelectBASVERReportInfoList(Int64 bsVerMasterId)
        {
            return bsvarGatewayObj.SelectBASVERReportInfoList(bsVerMasterId);
        }
        public BASVERReportInfo SelectBASVERReportInfo(Int64 lLId, Int16 bankId, Int16 branchId, string acNo)
        {
            return bsvarGatewayObj.SelectBASVERReportInfo(lLId, bankId, branchId, acNo);
        }
    }
}
