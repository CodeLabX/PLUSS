﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// Analyst approve manager class.
    /// </summary>
    public class AnalystApproveManager
    {
        public AnalystApproveGateway AnalystApproveGatewayObj = null;
        /// <summary>
        /// Constructor.
        /// </summary>
        public AnalystApproveManager()
        {
            AnalystApproveGatewayObj = new AnalystApproveGateway();
        }

        /// <summary>
        /// This method provide pl information
        /// </summary>
        /// <param name="UserId">Gets a user id.</param>
        /// <returns>Returns analyst approve list object.</returns>
        public List<AnalystApprove> GetAllPL(Int32 UserId, string searchKey)
        {
            return AnalystApproveGatewayObj.GetAllPL(UserId, searchKey);
        }
        /// <summary>
        /// This method provide analyst approve info.
        /// </summary>
        /// <param name="lLId">Gets llid.</param>
        /// <param name="UserId">Gets user id.</param>
        /// <returns>Returns analyst approve object.</returns>
        public AnalystApprove SelectAnalystApprove(Int64 lLId,Int32 UserId)
        {
            return AnalystApproveGatewayObj.SelectAnalystApprove(lLId,UserId);
        }
        /// <summary>
        /// This method provide analyst approve info.
        /// </summary>
        /// <param name="lLId">Gets llid.</param>
        /// <param name="UserId">Gets user id.</param>
        /// <returns>Returns analyst approve object.</returns>
        public AnalystApprove SelectAnalystApprove(Int64 lLId)
        {
            return AnalystApproveGatewayObj.SelectAnalystApprove(lLId);
        }
        /// <summary>
        /// This method provide insert or update analyst approve information.
        /// </summary>
        /// <param name="analystApproveObj">Gets a analyst approve object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendAnalystApproveInToDB(AnalystApprove analystApproveObj)
        {
            return AnalystApproveGatewayObj.SendAnalystApproveInToDB(analystApproveObj);
        }

        /// <summary>
        /// This method provide the update pluss loan status.
        /// </summary>
        /// <param name="LLId">Gets a llid.</param>
        /// <param name="updateStatus">Gets a update status flaq.</param>
        public void UpdatePlussLoanStatus(Int64 lLId,int updateStatus)
        {
            AnalystApproveGatewayObj.UpdatePlussLoanStatus(lLId,updateStatus);
        }

        /// <summary>
        /// This method provide the update PL status.
        /// </summary>
        /// <param name="LLId">Gets a llid.</param>
        public void UpdatePLStatus(Int64 LLId)
        {
            AnalystApproveGatewayObj.UpdatePLStatus(LLId);
        }

        /// <summary>
        /// This method provide the update pluss loan  locator loan status.
        /// </summary>
        /// <param name="LLId">Gets a llid.</param>
        /// <param name="updateStatus">Gets a update status flaq.</param>
        public void UpdateLoanLocatorLoanStatus(Int64 lLId, int updateStatus)
        {
            AnalystApproveGatewayObj.UpdateLoanLocatorLoanStatus(lLId,updateStatus);
        }

        public void UpdateLoanLocatorLoanStatus(Int64 lLId, int updateStatus, double approveLoanAmount)
        {
            AnalystApproveGatewayObj.UpdateLoanLocatorLoanStatus(lLId,updateStatus,approveLoanAmount);
        }
        
        /// <summary>
        /// This method provide the limit amount.
        /// </summary>
        /// <param name="UserId">Gets a user id.</param>
        /// <param name="Level">Gets a level.</param>
        /// <param name="LoanTag">Gets a loan tag.</param>
        /// <returns>Returns a limit amount value.</returns>
        public double GetLimitAmount(Int32 UserId, string Level, string LoanTag)
        {
            return AnalystApproveGatewayObj.GetLimitAmount(UserId,Level,LoanTag);
        }
        public double GetCustomerTotalExistingLoanAmount(Int64 lLId)
        {
            return AnalystApproveGatewayObj.GetCustomerTotalExistingLoanAmount(lLId);
        }
    }
}
