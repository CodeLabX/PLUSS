﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    public class SCBBankNameListManager
    {
        private SCBBankNameListGateway SCBBNListGatewayObj = null;
        public SCBBankNameListManager()
        {
            SCBBNListGatewayObj = new SCBBankNameListGateway();
        }
        public List<BankNameList> GetSCBBankNameList(string LLID)
        {
            return SCBBNListGatewayObj.GetSCBBankNameList(LLID);
        }
    }
}
