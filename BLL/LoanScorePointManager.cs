﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// LoanScorePointManager Class.
    /// </summary>
    public class LoanScorePointManager
    {
        public LoanScorePointGateway loanScorePointGatewayObj = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public LoanScorePointManager()
        {
            loanScorePointGatewayObj = new LoanScorePointGateway();
        }
        /// <summary>
        /// This method select loan score point info.
        /// </summary>
        /// <param name="lLId">Gets  a llid.</param>
        /// <returns>Returns a LoanScorePoint object.</returns>
        public LoanScorePoint SelectLoanScorePoint(Int64 lLId)
        {
            return loanScorePointGatewayObj.SelectLoanScorePoint(lLId);
        }

        /// <summary>
        /// This method select loan score point info.
        /// </summary>
        /// <returns>Returns a LoanScorePoint list object.</returns>
        public List<LoanScorePoint> GetLoanScorePointList()
        {
            return loanScorePointGatewayObj.GetLoanScorePointList();
        }
        /// <summary>
        /// This method provide insertion or update loan score point info.
        /// </summary>
        /// <param name="loanScorePointObj">Gets a LoanScorePoint object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendLoanScorePointInToDB(LoanScorePoint loanScorePointObj)
        {
            return loanScorePointGatewayObj.SendLoanScorePointInToDB(loanScorePointObj);
        }
    }
}
