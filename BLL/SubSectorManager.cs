﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// SubSectorManager Class.
    /// </summary>
    public class SubSectorManager
    {
        public SubSectorGateway subSectorGatewayObj = null;
        /// <summary>
        /// Constructor
        /// </summary>
        public SubSectorManager()
        {
            subSectorGatewayObj = new SubSectorGateway();
        }
        /// <summary>
        /// This method select sub sector list
        /// </summary>
        /// <returns>Returns a SubSector list object.</returns>
        public List<SubSector> SelectSubSectorObjList()
        {
            return subSectorGatewayObj.SelectSubSectorObjList();
        }
        /// <summary>
        /// This method select subsector list
        /// </summary>
        /// <param name="searchKey">Gets a search string </param>
        /// <returns>Returns a SubSector list object.</returns>
        public List<SubSector> SelectSubSectorObjList(string searchKey)
        {
            return subSectorGatewayObj.SelectSubSectorObjList(searchKey);
        }
        /// <summary>
        /// This method select subsector sector id wise
        /// </summary>
        /// <param name="sectorId">Gets a id.</param>
        /// <returns>Returns a Subsector list object.</returns>
        public List<SubSector> SelectSubSectorSectorIdWise(Int32 sectorId)
        {
            return subSectorGatewayObj.SelectSubSectorSectorIdWise(sectorId);
        }
        /// <summary>
        /// This method select sub sector
        /// </summary>
        /// <param name="subSectorId">Gets a id.</param>
        /// <returns>Returns a SubSector object.</returns>
        public SubSector SelectSubSector(Int64 subSectorId)
        {
            return subSectorGatewayObj.SelectSubSector(subSectorId);
        }
        /// <summary>
        /// This mehod select max sub sector id
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public Int64 SelectMaxSubSectorId()
        {
            return subSectorGatewayObj.SelectMaxSubSectorId();
        }
        /// <summary>
        /// This method provide the operation of insert or update subsector
        /// </summary>
        /// <param name="subSectorObj">Gets a SubSector object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendSubSectorInfoInToDB(SubSector subSectorObj)
        {
            return subSectorGatewayObj.SendSubSectorInfoInToDB(subSectorObj);
        }
        /// <summary>
        /// This method provide the operation of insert or update sub sector list info
        /// </summary>
        /// <param name="subSectorObjList">Gets a SubSector list object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendSubSectorListInfoInToDB(List<SubSector> subSectorObjList)
        {
            return subSectorGatewayObj.SendSubSectorListInfoInToDB(subSectorObjList);
        }
        /// <summary>
        /// This method select sector id
        /// </summary>
        /// <param name="sectorName">Gets a sector name</param>
        /// <returns>Returns a sector id</returns>
        public Int32 SelectSectorId(string sectorName)
        {
            return subSectorGatewayObj.SelectSectorId(sectorName);
        }
    }
}
