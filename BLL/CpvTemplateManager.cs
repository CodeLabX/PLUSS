﻿using System;
using System.Collections.Generic;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// CpvTemplateManager Class.
    /// </summary>
    public class CpvTemplateManager
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public CpvTemplateManager() { }

        private CpvTemplateGateway cpvTemplateGatewayObj;

        /// <summary>
        /// This method provide all cpv template info.
        /// </summary>
        /// <param name="llId">Gets a llid.</param>
        /// <returns>Returns a CpvTemplate list object.</returns>
        public List<CpvTemplate> SelectCpvTemplateList(Int64 llId)
        {
            cpvTemplateGatewayObj = new CpvTemplateGateway();
            return cpvTemplateGatewayObj.GetAllCpvTemplateInfoByLlId(llId);
        }

        /// <summary>
        /// This method insertion of cpv template.
        /// </summary>
        /// <param name="cpvTemplateObj">Gets a CpvTemplate object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertCpvTemplate(CpvTemplate cpvTemplateObj)
        {
            cpvTemplateGatewayObj = new CpvTemplateGateway();
            return cpvTemplateGatewayObj.InsertCpvTemplate(cpvTemplateObj);
        }

        /// <summary>
        /// This method update of cpv template.
        /// </summary>
        /// <param name="cpvTemplateObj">Gets a CpvTemplate object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdateCpvTemplate(CpvTemplate cpvTemplateObj)
        {
            cpvTemplateGatewayObj = new CpvTemplateGateway();
            return cpvTemplateGatewayObj.UpdateCpvTemplate(cpvTemplateObj);
        }

        /// <summary>
        /// This method provide deletion of cpv template.
        /// </summary>
        /// <param name="id">Gets a id.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeleteCpvTemplate(int id)
        {
            cpvTemplateGatewayObj = new CpvTemplateGateway();
            return cpvTemplateGatewayObj.DeleteCpvTemplateObject(id);
        }
    }
}
