﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    public class UpdateLoanStatusManager
    {
        private UpdateLoanStatusGateway updateLoanStatus = new UpdateLoanStatusGateway();
        public List<UpdateStatus> GetAllStatus()
        {
            return updateLoanStatus.GetAllStatus();
        }

        public int UpdateStatus(UpdateStatus updateStatus)
        {
            return updateLoanStatus.UpdateStatus(updateStatus);
        }
    }
}
