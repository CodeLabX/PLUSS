﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// WordDocumentManager Class 
    /// </summary>
    public class WordDocumentManager
    {
        public WordDocumentGateway wordDocumentGatewayObj = null;
        /// <summary>
        /// Constructor
        /// </summary>
        public WordDocumentManager()
        {
            wordDocumentGatewayObj = new WordDocumentGateway();
        }
        /// <summary>
        /// This method select word document list
        /// </summary>
        /// <returns>Returns a WordDocument list object.</returns>
        public List<WordDocument> SelectWordDocumentList()
        {
            return wordDocumentGatewayObj.SelectWordDocumentList();
        }
        /// <summary>
        /// This method select word document
        /// </summary>
        /// <param name="subjectId">Gets a id.</param>
        /// <returns>Returns a WordDocument object.</returns>
        public WordDocument SelectWordDocument(int subjectId)
        {
            return wordDocumentGatewayObj.SelectWordDocument(subjectId);
        }
        /// <summary>
        /// This method provide the operation of insert or update word document
        /// </summary>
        /// <param name="wordDocumentObj">Gets a WordDocument object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendWordDocumentInToDB(WordDocument wordDocumentObj)
        {
            return wordDocumentGatewayObj.SendWordDocumentInToDB(wordDocumentObj);           
        }
        /// <summary>
        /// This method select max id
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public int SelectMaxId()
        {
            return wordDocumentGatewayObj.SelectMaxId();
        }
    }
}
