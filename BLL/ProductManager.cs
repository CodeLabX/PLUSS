﻿using System;
using System.Collections.Generic;
using DAL;
using BusinessEntities;

namespace BLL
{
    public class ProductManager
    {
        #region Default Constructor
        public ProductManager()
        { }
        #endregion
        
        #region private members
        private ProductGateway productGatewayObj;
        #endregion

        #region select Methods
        public List<Product> GetAllProductTypes()
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.GetAllProductTypes();
        }

        public List<Product> GetAllProducts()
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.GetAllProductInfo();
        }
        public Int32 GetProductId(string productName,string productType)
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.GetProductId(productName,productType);
        }
        public List<string> GetAllDistinctProductInfo()
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.GetAllDistinctProductInfo();
        }

        public List<Product> GetAllProductsById(int productId)
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.GetAllProductInfoById(productId);
        }
        #endregion


        public int InsertProduct(Product productObj)
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.InsertProduct(productObj);
        }


        public int DeleteProduct(int productCode)
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.DeleteProduct(productCode);
        }

        public List<Product> GetAllProductsBySearchingKey(string key)
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.GetAllProductsBySearchingKey(key);
        }

        public int UpdateProduct(Product productObj)
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.UpdateProduct(productObj);
        }
        public string GetProductName(int productId)
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.GetProductName(productId);
        }

        public List<Product> GetAllDistinctProductInfoById(int productId)
        {
            productGatewayObj = new ProductGateway();
            return productGatewayObj.GetAllDistinctProductInfoById(productId);
        }
    }
}
