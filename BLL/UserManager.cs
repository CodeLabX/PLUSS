﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;
using AutoLoanService.Entity;
using AzUtilities;

namespace BLL
{
    public class UserManager
    {
        UserGateway _gateway;
        /// <summary>
        /// Constructor.
        /// </summary>
        public UserManager()
        {
            //Constructor logic here.
            _gateway = new UserGateway();
        }
        public User GetUserInfo(Int32 userId)
        {
            return _gateway.GetUserInfo(userId);
        }

        public List<UserTModel> GetAllPendingUsers()
        {
            try
            {
                return _gateway.GetAllPendingUsers();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #region GetUserInfo

        /// <summary>
        /// This method populate user info.
        /// </summary>
        /// <param name="loginId">Gets login id.</param>
        /// <param name="password">Gets user password.</param>
        /// <param name="passsword"></param>
        /// <returns>Returns a user object.</returns>
        //public User GetUserInfo(string loginId, string password)
        //{
        //    return userGatewayObj.GetUserInfo(loginId, password);
        //}
        public User GetUserLoginInfo(string loginId, string passsword = null)
        {
            return _gateway.GetUserLoginInfo(loginId, passsword);
        }

        #endregion

        public void UpdateLoginStatus(string userId)
        {
            _gateway.UpdateLoginStatus(userId);
        }

        #region IsValidUserId(string loginId)
        /// <summary>
        /// This method check user id is valid or not.
        /// </summary>
        /// <param name="loginId">Gets login id.</param>
        /// <returns>Returns true if match else false.</returns>
        public bool IsValidUserId(string loginId)
        {
            return _gateway.IsValidUserId(loginId);
        }
        #endregion

        #region IsValidPassword(string loginId, string password)
        /// <summary>
        /// This method check password is valid or not.
        /// </summary>
        /// <param name="loginId">Gets login id.</param>
        /// <param name="password">Gets password</param>
        /// <returns>Returns true if match else false.</returns>
        public bool IsValidPassword(string loginId, string password)
        {
            return _gateway.IsValidPassword(loginId, password);
        }
        #endregion

        #region GetUserInfo()
        /// <summary>
        /// This method populate all user info.
        /// </summary>       
        /// <param name="searchKey">Gets search string.</param>
        /// <returns>Returns a user list object.</returns>
        public List<User> GetUserInfo(string searchKey)
        {
            return _gateway.GetUserInfo(searchKey);
        }

        public List<User> GetUnApproveUserInfo(string searchKey)
        {
            return _gateway.GetUnApproveUserInfo(searchKey);
        }
        #endregion

        #region GetAllUserType()
        /// <summary>
        /// This method populate all user type.
        /// </summary>
        /// <returns>Returns a user type list object.</returns>
        public List<UserType> GetAllUserType()
        {
            return _gateway.GetAllUserType();
        }

        public string ApproveUser(UserTModel user, User loggedInUser)
        {
            return _gateway.ApproveUser(user, loggedInUser);
        }
        #endregion

        public List<Department> GetAllDeptInfo()
        {
            return _gateway.GetAllDeptInfo();
        }

        #region GetAllAutoUserType()
        /// <summary>
        /// This method populate all user type.
        /// </summary>
        /// <returns>Returns a user type list object.</returns>
        public List<autoUserType> GetAllAutoUserType()
        {
            return _gateway.GetAllAutoUserType();
        }
        #endregion


        #region InsertORUpdateCreateUserInfo(User userObj)
        /// <summary>
        /// This method provide insert or update create user information.
        /// </summary>
        /// <param name="userObj">Gets user object.</param>
        /// <returns>Returns a int value.</returns>
        //public int InsertORUpdateCreateUserInfo(User userObj, List<GroupMember> groupMemberList)
        //{
        //    return userGatewayObj.InsertORUpdateCreateUserInfo(userObj, groupMemberList);
        //}
        public int InsertORUpdateCreateUserInfo(User userObj)
        {
            return _gateway.InsertORUpdateCreateUserInfo(userObj);
        }

        //public int InsertORUpdateCreateAutoUserInfo(User userObj)
        //{
        //    return userGatewayObj.InsertCreateAutoUserInformation(userObj);
        //}
        #endregion

        #region IsExistsUserId(string userId)
        /// <summary>
        /// This method provide user id exists or not.
        /// </summary>
        /// <param name="llid">Get user Id</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsExistsUserId(string userId)
        {
            return _gateway.IsExistsUserId(userId);
        }
        #endregion





        //code by arif start 19-12-2011

        #region GetGroupID(int userId)
        /// <summary>
        /// This method provide user id exists or not.
        /// </summary>
        /// <param name="llid">Get user Id</param>
        /// <returns>Returns true if exists else false.</returns>
        public List<GroupMember> GetGroupID(int userId)
        {
            return _gateway.GetGroupID(userId);
        }
        #endregion

        //code by arif end 19-12-2011





        #region ResetUserPassword(Int32 userId, string password)
        /// <summary>
        /// This method reset user password.
        /// </summary>
        /// <param name="userId">Gets user id.</param>
        /// <param name="password">Gets default password.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int ResetUserPassword(Int32 userId, string password)
        {
            return _gateway.ResetUserPassword(userId, password);
        }
        #endregion

        #region ChangePassword(string loginId, string password)
        /// <summary>
        /// This method update user password.
        /// </summary>
        /// <param name="loginId">Gets login id.</param>
        /// <param name="password">Gets default password.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int ChangePassword(string loginId, string password)
        {
            return _gateway.ChangePassword(loginId, password);
        }
        #endregion

        #region IsOldPasswordMatch(string loginId, string oldPassword)
        /// <summary>
        /// This method provide old password is match or not.
        /// </summary>
        /// <param name="loginId">Gets login Id</param>
        /// <param name="oldPassword">Gets old password.</param>
        /// <returns>Returns true if exists else false.</returns>
        public bool IsOldPasswordMatch(string loginId, string oldPassword)
        {
            return _gateway.IsOldPasswordMatch(loginId, oldPassword);
        }
        #endregion

        /// <summary>
        /// This method provide all user information.
        /// </summary>
        /// <returns>Returns user list object.</returns>
        public List<User> GetAllUserInfo()
        {
            return _gateway.GetAllUserInfo();
        }


        //Added by ARif

        public List<AutoUserTypeVsWorkflow> getAutoLoanStatePermission(short userType)
        {
            return _gateway.getAutoLoanStatePermission(userType);
        }

        public List<User> GetAllUsers()
        {
            return _gateway.GetAllUsers();
        } 
        public DataTable GetUserListReportData(User user)
        {
            return _gateway.GetUserListReportData(user);
        }

        public List<Audit> GetAuditTrailData(string startDate, string endDate)
        {

            return _gateway.GetAuditTrailData(startDate, endDate);

        }

        public DataTable GetAuditTrailDataTable(string startDate, string endDate)
        {

            return _gateway.GetAuditTrailDataTable(startDate, endDate);

        }

        public void UserApproval(string userCode, string loginUser)
        {
            _gateway.UserApproval(userCode, loginUser);
        }

        public void UserDelete(string userCode, string loginUser)
        {
            _gateway.UserDelete(userCode, loginUser);
        }

        public LoanLocatorUser GetLoanLocatorUser(string userName)
        {
            return _gateway.GetLoanLocatorUser(userName);
        }

        public DataTable GetLoginAccess(DateTime fromDateTime, DateTime toDateTime)
        {
            return _gateway.GetLoginAccess(fromDateTime, toDateTime);
        }


        /*
         * NA
         */
        public string CreateUser(UserTModel userT, User user)
        {
            //LogFile.WriteLine("Checking for already existed user");
           
            return _gateway.CreateUser(userT, user);
        }
        public string UpdateExistingUser(UserTModel user)
        {
            //user.Status = 0;
            return _gateway.UpdateExistingUser(user);
        }

        public UserTModel GetUserByLoginId(string loginId)
        {
            var tempUser = _gateway.GetUserFromTempByLoginId(loginId);
            if (tempUser != null)
            {
                return tempUser;
            }
            var rUser = _gateway.GetUserByLoginIdFromMain(loginId);

            return rUser;
        }

        public UserTModel GetUserByLoginIdFromMain(string loginId)
        {
            return _gateway.GetUserByLoginIdFromMain(loginId);
        }

        public string RejectUserCreation(UserTModel user, User userT)
        {
            return _gateway.RejectUserCreation(user, userT);
        }

        public UserTModel GetUserByTempLoginId(string loginId)
        {
            return _gateway.GetUserByTempLoginId(loginId);
        }
        public UserTModel GetUserByTempId(string loginId)
        {
            return _gateway.GetUserByTempId(loginId);
        }
        public void UserDeleteFromTemp(string UserCodeID)
        {
             _gateway.UserDeleteFromTemp(UserCodeID);
        }
        public List<UserTModel> GetAllInActiveUsers()
        {
            try
            {
                return _gateway.GetAllInActiveUsers();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UserTModel> GetAllInActiveUsers(string sPSID)
        {
            try
            {
                return _gateway.GetAllInActiveUsers(sPSID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ActivateUser(string id)
        {
            try
            {
                return _gateway.ActivateUser(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
