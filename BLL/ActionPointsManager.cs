﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// Action point manager class.
    /// </summary>
    public class ActionPointsManager
    {
        public ActionPointsGateway actionPointsGatewayObj = null;

        /// <summary>
        /// Constructor 
        /// </summary>
        public ActionPointsManager()
        {
            actionPointsGatewayObj = new ActionPointsGateway();
        }
        /// <summary>
        /// This method populate all action points.
        /// </summary>
        /// <returns>Returns action points list object.</returns>
        public List<ActionPoints> SelectActionPointsList()
        {
            return actionPointsGatewayObj.SelectActionPointsList();
        }
        /// <summary>
        /// This method provide action point according ot search crieteria.
        /// </summary>
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns action points list object.</returns>
        public List<ActionPoints> SelectActionPointsList(string searchKey)
        {
            return actionPointsGatewayObj.SelectActionPointsList(searchKey);
        }

        /// <summary>
        /// This method provide action points information.
        /// </summary>
        /// <param name="colorId">Gets a color id.</param>
        /// <returns>Returns a action point object.</returns>
        public ActionPoints SelectActionPoints(int colorId)
        {
            return actionPointsGatewayObj.SelectActionPoints(colorId);
        }

        /// <summary>
        /// This method provide action point against color.
        /// </summary>
        /// <param name="color">Gets a color.</param>
        /// <returns>Returns action points object.</returns>
        public ActionPoints SelectActionPoints(string color)
        {
            return actionPointsGatewayObj.SelectActionPoints(color);
        }

        /// <summary>
        /// This method provide insertion or update information.
        /// </summary>
        /// <param name="actionPointsObj">Gets action point object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendActionPointsInToDB(ActionPoints actionPointsObj)
        {
            return actionPointsGatewayObj.SendActionPointsInToDB(actionPointsObj);
        }

        /// <summary>
        /// This method provide max action point id.
        /// </summary>
        /// <returns>Returns a max id.</returns>
        public Int64 SelectMaxActionPointsId()
        {
            return actionPointsGatewayObj.SelectMaxActionPointsId();
        }
    }
}
