﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// BILMUE Manager Class.
    /// </summary>
    public class BILMUEManager
    {
        private BILMUEGateway bilMUEGatewayObj;
        /// <summary>
        /// Constructor.
        /// </summary>
        public BILMUEManager()
        {
            //Constructor logic here.
            bilMUEGatewayObj = new BILMUEGateway();
        }
        #region GetCustomerDetailData(string llId)
        /// <summary>
        /// This method provide customer detail data.
        /// </summary>
        /// <param name="llId">Gets llid</param>
        /// <returns>Returns bilmue object.</returns>
        public BILMUE GetCustomerDetailData(string llId)
        {
            return bilMUEGatewayObj.GetCustomerDetailData(llId);
        }
        #endregion

        #region GetCustomerExposureData(string llId)
        /// <summary>
        /// This method provide customer exposure data.
        /// </summary>
        /// <param name="llId">Gets llid</param>
        /// <returns>Returns bilmue exposure list object.</returns>
        public List<BILMUE> GetCustomerExposureData(string llId)
        {
            return bilMUEGatewayObj.GetCustomerExposureData(llId);
        }
        #endregion

        #region GetBILMUEData(string llId)
        /// <summary>
        /// This method provide BilMue data.
        /// </summary>
        /// <param name="llId">Gets a llId</param>
        /// <returns>Returns a bilmue object.</returns>
        public BILMUE GetBILMUEData(string llId)
        {
            return bilMUEGatewayObj.GetBILMUEData(llId);
        }
        #endregion

        #region InsertORUpdateBilMueInfo(BILMUE bilMueObj)
        /// <summary>
        /// This method provide insert/update bil mue information.
        /// </summary>
        /// <param name="loanInformationObj">Gets bil mue object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateBilMueInfo(BILMUE bilMueObj)
        {
            return bilMUEGatewayObj.InsertORUpdateBilMueInfo(bilMueObj);
        }
        #endregion
    }
}
