﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// PayRollManager Class.
    /// </summary>
    public class PayRollManager
    {
        private PayRollGateway payRollGatewayObj = new PayRollGateway();

        /// <summary>
        /// Constructor.
        /// </summary>
        public PayRollManager()
        {
        }
        /// <summary>
        /// Get all PayRoll from database by searching key
        /// </summary>
        /// <param name="searchingKey">Gets a search string.</param>
        /// <returns>Returns a PayRoll list object.</returns>
        public List<PayRoll> SearchPayRoll(string searchingKey)
        {
            return payRollGatewayObj.GetAllPayRollInfo(searchingKey);
        }

        /// <summary>
        /// Get all PayRoll from database
        /// </summary>
        /// <returns>Returns a PayRoll list object.</returns>
        public List<PayRoll> SearchAllPayRoll()
        {
            return payRollGatewayObj.GetAllPayRollInfo();
        }

        /// <summary>
        /// This method provide insertion operation of payroll information.
        /// </summary>
        /// <param name="CompanyObj">Gets a payroll object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int InsertPayroll(PayRoll tPlussCompanyObj)
        {
            return payRollGatewayObj.InsertPayroll(tPlussCompanyObj);
        }

        /// <summary>
        /// This method provide update operation of payroll information.
        /// </summary>
        /// <param name="CompanyObj">Gets a payroll object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int UpdatePayroll(PayRoll tPlussCompanyObj)
        {
            return payRollGatewayObj.UpdatePayroll(tPlussCompanyObj);
        }

        /// <summary>
        /// This method provide the delete operation of payroll.
        /// </summary>
        /// <param name="id">Gets a payroll id.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int DeletePayroll(int id)
        {
            return payRollGatewayObj.DeletePayroll(id);
        }
        //--------------------Add Hiru----------------------
        /// <summary>
        /// This method select the all company.
        /// </summary>
        /// <returns>Returns a PayRoll list object. </returns>
        public List<PayRoll> GetAllCompany()
        {
            return payRollGatewayObj.GetAllCompany();
        }
        /// <summary>
        /// This method provide the search operation against letter.
        /// </summary>
        /// <param name="letters">Gets a  letter</param>
        /// <returns>Returns a PayRoll list object.</returns>
        public List<PayRoll> SarchByLetters(string letters)
        {
            return payRollGatewayObj.SarchByLetters(letters);
        }
        /// <summary>
        /// This method select company id
        /// </summary>
        /// <param name="companyName">Gets a company name.</param>
        /// <returns>Returns a company id.</returns>
        public int SelectCompanyId(string companyName)
        {
            return payRollGatewayObj.SelectCompanyId(companyName);
        }
        /// <summary>
        /// This method provide the operation of company Name
        /// </summary>
        /// <param name="companyName">Gets a company name.</param>
        public void InsertCompanyName(string companyName)
        {
            payRollGatewayObj.InsertCompanyName(companyName);
        }

        public PayRoll SelectCompanyInfo(Int32 companyId, string companyName)
        {
            return payRollGatewayObj.SelectCompanyInfo(companyId,companyName);
        }
    }
}
