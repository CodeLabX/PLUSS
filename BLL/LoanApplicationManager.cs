﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// Loan application manager class.
    /// </summary>
    public class LoanApplicationManager
    {
        private LoanApplicationGateway loanApplicationGatewayObj;

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LoanApplicationManager()
        {
            //Constructor logic here. 
            loanApplicationGatewayObj = new LoanApplicationGateway();
        }
        #endregion

        #region Get All Product
        /// <summary>
        /// This method gives the all product.
        /// </summary>
        /// <returns>Returns product list object.</returns>
        public List<Product> GetAllProduct()
        {
            return loanApplicationGatewayObj.GetAllProduct();
        }
        #endregion

        public List<Product> GetAllProductFromLoanLocator()
        {
            return loanApplicationGatewayObj.GetAllProductFromLoanLocator();
        }

        public List<Product> GetAllProductByLearner()
        {
            return loanApplicationGatewayObj.GetAllProductByLearner();
        }

        #region Get All Source
        /// <summary>
        /// This method gives all the souce.
        /// </summary>
        /// <returns>Returns source list object.</returns>
        public List<Source> GetAllSource()
        {
            return loanApplicationGatewayObj.GetAllSource();
        }
        #endregion


        public List<Source> LocGetAllSource()
        {
            return loanApplicationGatewayObj.LocGetAllSource();
        }


        #region Get All Vechile
        /// <summary>
        /// This method gives all vechiles.
        /// </summary>
        /// <returns>Returns vechile list object.</returns>
        public List<Vechile> GetAllVechile()
        {
            return loanApplicationGatewayObj.GetAllVechile();
        }
        #endregion

        #region Get All Banks
        /// <summary>
        /// This method provide all banks.
        /// </summary>
        /// <returns>Returns bank information list object</returns>
        public List<BankInformation> GetAllBanks()
        {
            return loanApplicationGatewayObj.GetAllBanks();
        }
        #endregion

        #region Get All Branchs
        /// <summary>
        /// This method provides all branches.
        /// </summary>
        /// <returns>Returns branch information list object.</returns>
        public List<BranchInformation> GetAllBranchs()
        {
            return loanApplicationGatewayObj.GetAllBranchs();
        }
        #endregion

        #region Get All Branchs
        /// <summary>
        /// This method provides all branches.
        /// </summary>
        /// <param name="bankId">Gets a bank id.</param>
        /// <returns>Returns branch information list object.</returns>
        public List<BranchInformation> GetAllBranchs(Int32 bankId)
        {
            return loanApplicationGatewayObj.GetAllBranchs(bankId);
        }
        #endregion

        #region  Get All Account Type
        /// <summary>
        /// This method provides all account types.
        /// </summary>
        /// <returns>Returns account type list object.</returns>
        public List<AccountType> GetAllAccountType()
        {
            return loanApplicationGatewayObj.GetAllAccountType();
        }
        #endregion

        #region Get All Id Types
        /// <summary>
        /// This method provides all id types.
        /// </summary>
        /// <returns>Returns id type list object.</returns>
        public List<IdType> GetAllIdTypes()
        {
            return loanApplicationGatewayObj.GetAllIdTypes();
        }
        #endregion

        #region Get All Profession
        /// <summary>
        /// This method provides all professions.
        /// </summary>
        /// <returns>Returns profession list object.</returns>
        public List<Profession> GetAllProfession()
        {
            return loanApplicationGatewayObj.GetAllProfession();
        }
        #endregion

        #region Get All Education Level
        /// <summary>
        /// This method provides all Education level.
        /// </summary>
        /// <returns>Returns education level list object.</returns>
        public List<EducationLevel> GetAllEducationLevel()
        {
            return loanApplicationGatewayObj.GetAllEducationLevel();
        }
        #endregion

        #region GetLoanAppLoanLocatorInfo
        /// <summary>
        /// This method provide particular loan locator info.
        /// </summary>
        /// <param name="llid">Gets loan applicant llid.</param>
        /// <returns>Returns loan Locator aoan applicant object.</returns>
        public LoanLocatorLoanApplicant GetLoanAppLoanLocatorInfo(Int32 llid)
        {
            return loanApplicationGatewayObj.GetLoanAppLoanLocatorInfo(llid);
        }
        #endregion

        #region GetLoanInformation
        /// <summary>
        /// This method provides applicant loan information.
        /// </summary>
        /// <param name="llid">Gets a llid</param>
        /// <returns>Returns loan information object.</returns>
        public LoanInformation GetLoanInformation(int llid)
        {
            return loanApplicationGatewayObj.GetLoanInformation(llid);
        }
        public LoanInformation GetLoanInformationCreate(int llid)
        {
            return loanApplicationGatewayObj.GetLoanInformationCreate(llid);
        }
        
        #endregion

        #region GetAllRemarks
        /// <summary>
        /// This method select all remarks depends on user.
        /// </summary>
        /// <returns>Remarks list object</returns>
        public List<Remarks> GetAllRemarks()
        {
            return loanApplicationGatewayObj.GetAllRemarks();
        }
        #endregion

        #region GetAllJointApplicantInformation
        /// <summary>
        /// This method provide all joint applicants information.
        /// </summary>
        /// <param name="llid">Gets the applicant LLID</param>
        /// <returns>Returns joint applicant information object</returns>
        public JoinApplicantInformation GetAllJointApplicantInformation(Int32 llid)
        {
            return loanApplicationGatewayObj.GetAllJointApplicantInformation(llid);
        }
        #endregion

        #region InsertORUpdateLoanInfo
        /// <summary>
        /// This method provide insert or update loan information.
        /// </summary>
        /// <param name="loanInformationObj">Gets loanInformation object.</param>
        /// <param name="joinApplicantInformationObj">Gets joint applicant information object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateLoanInfo(LoanInformation loanInformationObj, User user, DateTime loanStartTime)
        {
            return loanApplicationGatewayObj.InsertORUpdateLoanInfo(loanInformationObj, user, loanStartTime);
        }
        #endregion

        #region SetForwardStatus
        /// <summary>
        /// This method update the status flag for loan applicant.
        /// </summary>
        /// <param name="llid">Gets a LLID.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SetForwardStatus(Int32 llid, Int32 statusId, string loginUserId)
        {
            return loanApplicationGatewayObj.SetForwardStatus(llid, statusId, loginUserId);
        }
        #endregion

        #region DeleteDeferReason
        /// <summary>
        /// This method delete defer reason.
        /// </summary>
        /// <param name="llid">Gets LLID</param>
        /// <returns>Return a positive value if sucess.</returns>
        public int DeleteDeferReason(Int32 llid)
        {
            return loanApplicationGatewayObj.DeleteDeferReason(llid);
        }
        #endregion

        #region InsertDeferReason
        /// <summary>
        /// This method insert defer reason.
        /// </summary>
        /// <param name="llid">Gets LLID</param>
        /// <param name="reasonId">Gets list of reason id</param>
        /// <returns>Positive value if sucess.</returns>
        public int InsertDeferReason(Int32 llid, List<Int32> reasonId)
        {
            return loanApplicationGatewayObj.InsertDeferReason(llid, reasonId);
        }
        #endregion

        #region InsertDeferReasonDetails
        /// <summary>
        /// This method insert defer reason details.
        /// </summary>
        /// <param name="loanApplicationDetailsObj">Gets a loanApplicationDetails object.</param>
        /// <param name="remarks">Gets a remarks list.</param>
        /// <returns>Positive values if sucess.</returns>
        public int InsertDeferReasonDetails(LoanApplicationDetails loanApplicationDetailsObj, List<Remarks> remarks)
        {
            return loanApplicationGatewayObj.InsertDeferReasonDetails(loanApplicationDetailsObj, remarks);
        }
        #endregion

        #region GetAllDeferReasonId
        /// <summary>
        /// This method populate all reasons id.
        /// </summary>
        /// <param name="llid">Gets a LLID.</param>
        /// <returns>Reason id list object.</returns>
        public List<Int32> GetAllDeferReasonId(Int32 llid)
        {
            return loanApplicationGatewayObj.GetAllDeferReasonId(llid);
        }
        #endregion

        public bool IsExistsLoanInfo(Int32 llid)
        {
            return loanApplicationGatewayObj.IsExistsLoanInfo(llid);
        }

        public DataTable GetLoanStates()
        {
            return loanApplicationGatewayObj.GetLoanStates();
        }

        public DataTable GetDepartment()
        {
            return loanApplicationGatewayObj.GetDepartment();
        }

        public int InsertORUpdateProductInfo(Product product)
        {
            return loanApplicationGatewayObj.InsertORUpdateProductInfo(product);

        }

        public List<CheckList> GetAllCheckLists()
        {
            return loanApplicationGatewayObj.GetAllCheckLists();
        }

        public int InsertORUpdateCheckList(CheckList checkList)
        {
            return loanApplicationGatewayObj.InsertORUpdateCheckList(checkList);
        }

        public int DeleteCheckList(int id)
        {
            return loanApplicationGatewayObj.DeleteCheckList(id);
        }


    }
}
