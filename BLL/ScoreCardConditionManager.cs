﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// Score cared condition manager class.
    /// </summary>
    public class ScoreCardConditionManager
    {
        private ScoreCardConditionGateway scoreCardConditionGatewayObj;
        /// <summary>
        /// Constructor
        /// </summary>
        public ScoreCardConditionManager()
        {
            //Constructor logic here.
            scoreCardConditionGatewayObj = new ScoreCardConditionGateway();
        }

        #region GetScoreCardCondition(string searchKey)
        /// <summary>
        /// This method populate score card condition.
        /// </summary>   
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns a score card condition list object.</returns>
        public List<ScoreCardCondition> GetScoreCardCondition(string searchKey)
        {
            return scoreCardConditionGatewayObj.GetScoreCardCondition(searchKey);
        }
        public List<ScoreCardCondition> GetScoreCardCondition()
        {
            return scoreCardConditionGatewayObj.GetScoreCardCondition();
        }
        #endregion

        #region GetAllVariableName()
        /// <summary>
        /// This method populate all variable name.
        /// </summary>
        /// <returns>Returns a variable name list object.</returns>
        public List<VariableName> GetAllVariableName()
        {
            return scoreCardConditionGatewayObj.GetAllVariableName();
        }
        #endregion

        #region InsertORUpdateScoreCardConditionInfo(ScoreCardCondition scoreCardConditionObj)
        /// <summary>
        /// This method provide insert or update score card condition information.
        /// </summary>
        /// <param name="scoreCardConditionObj">Gets score card condition object.</param>
        /// <returns>Returns a int value.</returns>
        public int InsertORUpdateScoreCardConditionInfo(ScoreCardCondition scoreCardConditionObj)
        {
            return scoreCardConditionGatewayObj.InsertORUpdateScoreCardConditionInfo(scoreCardConditionObj);
        }
        #endregion
    }
}
