﻿using System;
using System.Collections.Generic;
using DAL;
using BusinessEntities;

namespace BLL
{
    /// <summary>
    /// PddDeviationManager Class.
    /// </summary>
    public class PddDeviationManager
    {
        PddDeviationGateaway pddDeviationGateawayObj;

        /// <summary>
        /// Constructor.
        /// </summary>
        public PddDeviationManager()
        {
            pddDeviationGateawayObj = new PddDeviationGateaway();
        }

        /// <summary>
        /// This method select all pdd deviaton.
        /// </summary>
        /// <returns>Returns  a PddDeviation list object.</returns>
        public List<PddDeviation> GetAllPddDeviation()
        {
            return pddDeviationGateawayObj.GetAllPddDeviation();
        }


        /// <summary>
        /// This method select pdd deviation info against level.
        /// </summary>
        /// <param name="level">Gets a level id</param>
        /// <returns>Returns a PddDeviation list object.</returns>
        public List<PddDeviation> GetPddDeviationOnLevel(int level)
        {
            return pddDeviationGateawayObj.GetPddDeviationOnLevel(level);
        }
        /// <summary>
        /// This method select pdd deviation code
        /// </summary>
        /// <param name="descriptionId">Gets a description id.</param>
        /// <returns>Returns a pdd deviation code.</returns>
        public string GetPddDeviationCode(string descriptionId)
        {
            return pddDeviationGateawayObj.GetPddDeviationCode(descriptionId);    
        }
    }
}
