﻿using System;
using DAL;

namespace BLL
{
    public class LoanCounterManager
    {
        public DateTime GetLastCounterObjectDate()
        {
            return new LoanCounterGateway().GetLastCounterObject().DATE;
        }

        #region Received
        public bool IsLastReceivedDateToday()
        {
            return GetLastCounterObjectDate() == DateTime.Now;
        }

        public int SaveReceivedLoan()
        {
            return IsLastReceivedDateToday() ? UpdateReceivedLoan() : InsertReceivedLoan();
        }

        public int InsertReceivedLoan()
        {
            return new LoanCounterGateway().InsertReceivedLoan();
        }

        public int UpdateReceivedLoan()
        {
            return new LoanCounterGateway().UpdateReceivedLoan();
        }
        #endregion


        #region Pending
        public bool IsLastPendingDateToday()
        {
            return GetLastCounterObjectDate() == DateTime.Now;
        }

        public int SavePendingLoan()
        {
            return IsLastPendingDateToday() ? UpdatePendingLoan() : InsertPendingLoan();
        }

        public int InsertPendingLoan()
        {
            return new LoanCounterGateway().InsertPendingLoan();
        }

        public int UpdatePendingLoan()
        {
            return new LoanCounterGateway().UpdatePendingLoan();
        }
        #endregion


        #region Processing
        public bool IsLastProcessingDateToday()
        {
            return GetLastCounterObjectDate() == DateTime.Now;
        }

        public int SaveProcessingLoan()
        {
            return IsLastProcessingDateToday() ? UpdateProcessingLoan() : InsertProcessingLoan();
        }

        public int InsertProcessingLoan()
        {
            return new LoanCounterGateway().InsertProcessingLoan();
        }

        public int UpdateProcessingLoan()
        {
            return new LoanCounterGateway().UpdateProcessingLoan();
        }
        #endregion
    }
}
