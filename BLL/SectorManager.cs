﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using DAL;

namespace BLL
{
    /// <summary>
    /// SectorManager Class.
    /// </summary>
    public class SectorManager
    {
        public SectorGateway sectorGatewayObj = null;
        /// <summary>
        /// Constructor.
        /// </summary>
        public SectorManager()
        {
            sectorGatewayObj = new SectorGateway();
        }
        /// <summary>
        /// This method select sector list
        /// </summary>
        /// <returns>Returns a list of Sector</returns>
        public List<Sector> SelectSectorList()
        {
            return sectorGatewayObj.SelectSectorList();
        }
        /// <summary>
        /// This method select sector list
        /// </summary>
        /// <param name="searchKey">Gets a search string.</param>
        /// <returns>Returns a list of Sector.</returns>
        public List<Sector> SelectSectorList(string searchKey)
        {
            return sectorGatewayObj.SelectSectorList(searchKey);
        }
        /// <summary>
        /// This method select sector
        /// </summary>
        /// <param name="sectorId">Gets a sector id.</param>
        /// <returns>Returns a Sector object.</returns>
        public Sector SelectSector(Int64 sectorId)
        {
            return sectorGatewayObj.SelectSector(sectorId);
        }
        /// <summary>
        /// This method select max scetor id
        /// </summary>
        /// <returns>Returns a max sector id.</returns>
        public Int64 SelectMaxScetorId()
        {
            return sectorGatewayObj.SelectMaxScetorId();
        }
        /// <summary>
        /// This method provide the operation of insertion or update sector info
        /// </summary>
        /// <param name="sectorObj">Gets a Sector object.</param>
        /// <returns>Returns a positive value if success.</returns>
        public int SendSectorInfoInToDB(Sector sectorObj)
        {
            return sectorGatewayObj.SendSectorInfoInToDB(sectorObj);
        }
    }
}
