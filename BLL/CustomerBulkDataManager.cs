﻿using AzUtilities;
using AzUtilities.Common;
using BusinessEntities;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CustomerBulkDataManager
    {
        CustomerBulkDataGateway _gateway;

        public CustomerBulkDataManager()
        {
            _gateway = new CustomerBulkDataGateway();
        }

        private Dictionary<string, string> CreateColumnMaps()
        {
            int i = 0;

            List<string> fileColumnNames = new List<string>() { "REFTYPE", "REFERENCEID", "MASTERNO", "RELATIONSHIPNO", "FULLNAME", "FATHERNAME", "MOTHERNAME", "DATEOFBIRTH", "QUALIFICATIONCODE", "ADDRESS_RES", "ADDRESS_RES_POSTALCODE", "ADDRESS_RES_CITYCODE", "ADDRESS_RES_CITYNAME", "ADDRESS_RES_COUNTRYCODE", "ADDRESS_OFF", "ADDRESS_OFF_POSTALCODE", "ADDRESS_OFF_CITYCODE", "ADDRESS_OFF_CITYNAME", "ADDRESS_OFF_COUNTRYCODE", "PROFESSIONCODE", "ORGANISATIONNAME", "DEPARTMENT",
                "PASSPORT_UID1", "PASSPORT_EXPDT", "UNIQUEID2" ,  "UNIQUEID2EXPDT", "TIN_UID3","NID_O_UID4" , "NID_O_EXPDT" , "NID_N_UID4" ,  "NID_N_ESPDT", "MOBILE", "MOBILE1", "MARITALST", "GENDER", "SPOUSENAME", "MakerId", "MakeDate" };
            List<string> dbTableColumnNames = new List<string>() { "RefType", "RefrenceID", "MasterNo", "RelationshipNo", "FullName", "FatherName", "MotherName", "DOB", "HighestEduLevel", "AddressRes", "AddressRes_PostalCode", "AddressRes_CityCode", "AddressRes_CityName", "AddressRes_CountryCode", "AddressOff", "AddressOff_PostalCode", "AddressOff_CityCode", "AddressOff_CityName", "AddressOff_CountryCode", "ProfessionCode", "OrganisationName", "Department",
                "PassportNo", "PassportExp", "UniqueID2", "UniqueID2Exp", "TIN", "NID_O", "NID_OExp", "NID_N", "NID_NExp", "ContactNo1", "ContactNo2", "MaritalStatus", "Gender", "SpouseName", "MakerId", "MakeDate" };
            Dictionary<string, string> map = new Dictionary<string, string>();
            try
            {
                for (i = 0; i < dbTableColumnNames.Count; i++)
                {
                    map.Add(fileColumnNames[i], dbTableColumnNames[i]);
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("CustomerBulkDataManager :: Dictionary");
                LogFile.WriteLog(ex);
            }
            return map;
        }



        public Operation Save(DataTable table, User user)
        {
            try
            {
                Dictionary<string, string> columnMap = CreateColumnMaps();
                foreach (DataRow item in table.Rows)
                {
                    item["MakerId"] = user.UserId;
                    item["MakeDate"] = DateTime.Now;
                }

                return _gateway.Save(table, columnMap);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("CustomerBulkDataManager :: Save");
                LogFile.WriteLog(ex);

                return Operation.Error;
            }
        }

        public List<CustomersBulkData> GetAllTemp()
        {
            try
            {
                return _gateway.GetAllTemp();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("CustomerBulkDataManager :: GetAllTemp");
                LogFile.WriteLog(ex);

                return new List<CustomersBulkData>();
            }
        }

        public string ApproveCustomerBulkDataTemp(User actionUser)
        {
            try
            {
                return _gateway.ApproveCustomerBulkDataTemp(actionUser);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("CustomerBulkDataManager :: ApproveCustomerBulkDataTemp");
                LogFile.WriteLog(ex);

                return "Error";
            }
        }

        public void InsertAudit(System.Web.UI.Page page, string activity, string description)
        {
            new AT(page).AuditAndTraial(activity, description);
        }

        public CustomersBulkData GetCustomerDataWithAccountNo(string accNo)
        {
            string withoutDashes = accNo.Replace("-", "");
            try
            {
                return _gateway.GetCustomerDataWithAccountNo(accNo, withoutDashes);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("CustomerBulkDataManager :: GetCustomerDataWithAccountNo");
                LogFile.WriteLog(ex);

                return new CustomersBulkData();
            }
        }
    }
}
