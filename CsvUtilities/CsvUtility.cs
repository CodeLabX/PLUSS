﻿using AzUtilities;
using BLL;
using BusinessEntities;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvUtilities
{
    public class CsvUtilities
    {
        public int InsertIntoDatabase(string filePath)
        {
            List<DeDupInfo> list = null;
            int inserted = 0;
            int rowIndex = 0;
            try
            {
                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    DeDupInfoManager DeDupInfoManagerObj = new DeDupInfoManager();

                    List<DeDupInfo> records = new List<DeDupInfo>();

                    list = new List<DeDupInfo>();

                    csv.Read();
                    csv.ReadHeader();
                    rowIndex++;

                    while (csv.Read())
                    {
                        rowIndex++;
                        DeDupInfo record = GetDeDupInfo(csv, rowIndex, DeDupInfoManagerObj);
                        list.Add(record);

                        if (list.Count == 5000)
                        {
                            inserted += DeDupInfoManagerObj.SendDedupeInToFinalTable(list);
                            list = new List<DeDupInfo>();
                        }
                    }
                    if (list != null)
                    {
                        inserted += DeDupInfoManagerObj.SendDedupeInToFinalTable(list);
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                throw new Exception("MFU Upload Failed at row index  :: " + rowIndex);
            }
            return inserted;
        }

        private DeDupInfo GetDeDupInfo(CsvReader csv, int rowIndex, DeDupInfoManager DeDupInfoManagerObj)
        {
            if (rowIndex == 20000)
                LogFile.Write(".");
            string error = "";
            DeDupInfo deDupInfo = new DeDupInfo();

            try
            {
                deDupInfo.Name = csv.GetField<string>(0);
            }
            catch (Exception)
            {
                error = "Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.FatherName = csv.GetField<string>(1);
            }
            catch (Exception)
            {
                error = "Father Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                deDupInfo.MotherName = csv.GetField<string>(2);
            }
            catch (Exception)
            {
                error = "Mother Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                deDupInfo.ScbMaterNo = csv.GetField<string>(3);
            }
            catch (Exception)
            {
                error = "Scb Mater No is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }
            try
            {
                string dob = csv.GetField<string>(4);
                if (!String.IsNullOrEmpty(dob))
                {
                    if (!dob.StartsWith("######"))
                        deDupInfo.DateOfBirth = Convert.ToDateTime(dob);
                    else
                        deDupInfo.DateOfBirth = (DateTime)SqlDateTime.MinValue;
                }
                else
                {
                    deDupInfo.DateOfBirth = (DateTime)SqlDateTime.MinValue;
                }
            }
            catch (Exception)
            {
                error = "Date Of Birth is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.Profession = csv.GetField<string>(5);

            }
            catch (Exception)
            {
                error = "Customer profession not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                string pi = csv.GetField<string>(6);
                if (!string.IsNullOrEmpty(pi))
                    deDupInfo.ProductId = DeDupInfoManagerObj.GetProductId(pi);
                else
                    deDupInfo.ProductId = 0;
            }
            catch (Exception)
            {
                error = "Product is not in correct in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.DeclineReason = csv.GetField<string>(7);
            }
            catch (Exception)
            {
                error = "Decline Reason is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                string declineDate = csv.GetField<string>(8);
                //deDupInfo.DeclineDate = !String.IsNullOrEmpty(declineDate) && !declineDate.StartsWith("######")
                //    ? Convert.ToDateTime(csv.GetField<string>(8))
                //    : DateTime.Now;
                if (!String.IsNullOrEmpty(declineDate) && !declineDate.StartsWith("######"))
                    deDupInfo.DeclineDate = Convert.ToDateTime(declineDate);
                else
                    deDupInfo.DeclineDate = (DateTime)SqlDateTime.MinValue;
            }
            catch (Exception)
            {
                error = "Decline Date is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                deDupInfo.BusinessEmployeeName = csv.GetField<string>(9);
            }
            catch (Exception)
            {
                error = "Business Employee Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.Address1 = csv.GetField<string>(10);
            }
            catch (Exception)
            {
                error = "Address1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                deDupInfo.Address2 = csv.GetField<string>(11);
            }
            catch (Exception)
            {
                error = "Address2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                deDupInfo.Phone1 = csv.GetField<string>(12);
            }
            catch (Exception)
            {
                error = "Phone1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                deDupInfo.Phone2 = csv.GetField<string>(13);
            }
            catch (Exception)
            {
                error = "Phone2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                deDupInfo.Mobile1 = csv.GetField<string>(14);
            }
            catch (Exception)
            {
                error = "Mobile1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.Mobile2 = csv.GetField<string>(15);
            }
            catch (Exception)
            {
                error = "Mobile2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                deDupInfo.BankBranch = csv.GetField<string>(16);
            }
            catch (Exception)
            {
                error = "Branch is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                deDupInfo.Source = csv.GetField<string>(17);
            }
            catch (Exception)
            {
                error = "Source is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }


            try
            {
                deDupInfo.Status = csv.GetField<string>(18);
            }
            catch (Exception)
            {
                error = "Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }


            try
            {
                deDupInfo.Remarks = csv.GetField<string>(19);
            }
            catch (Exception)
            {
                error = "Remarks is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.MfuTin = csv.GetField<string>(20);
            }
            catch (Exception)
            {
                error = "Tin is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                throw new Exception(error);
            }

            try
            {
                deDupInfo.IdType = csv.GetField<string>(21);
            }
            catch (Exception)
            {
                error = "IdType is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                throw new Exception(error);
            }

            try
            {
                deDupInfo.IdType1 = csv.GetField<string>(22);
            }
            catch (Exception)
            {
                error = "IdType1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                throw new Exception(error);
            }

            try
            {
                deDupInfo.IdType2 = csv.GetField<string>(23);
            }
            catch (Exception)
            {
                error = "IdType3 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                throw new Exception(error);
            }

            try
            {
                deDupInfo.IdType3 = csv.GetField<string>(24);
            }
            catch (Exception)
            {
                error = "IdType4 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.IdType4 = csv.GetField<string>(25);
            }
            catch (Exception)
            {
                error = "IdType5 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.IdNo = csv.GetField<string>(26);
            }
            catch (Exception)
            {
                error = "IdNo1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.IdNo1 = csv.GetField<string>(27);
            }
            catch (Exception)
            {
                error = "IdNo2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }


            try
            {
                deDupInfo.IdType2 = csv.GetField<string>(28);
            }
            catch (Exception)
            {
                error = "IdType3 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                deDupInfo.IdNo3 = csv.GetField<string>(29);
            }
            catch (Exception)
            {
                error = "IdNo4 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception();
            }
            try
            {
                deDupInfo.IdType4 = csv.GetField<string>(30);
            }
            catch (Exception)
            {
                error = "IdType5 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                deDupInfo.ResidentaileStatus = csv.GetField<string>(31);
            }
            catch (Exception)
            {
                error = "Residentail Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                deDupInfo.EducationalLevel = csv.GetField<string>(32);
            }
            catch (Exception)
            {
                error = "Educational Level is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                deDupInfo.MaritalStatus = csv.GetField<string>(33);
            }
            catch (Exception)
            {
                error = "Marital Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                deDupInfo.CriteriaType = csv.GetField<string>(34);
            }
            catch (Exception)
            {
                error = "Criteria Type is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                string entryDate = csv.GetField<string>(35);
                if (!entryDate.StartsWith("######"))
                    deDupInfo.EntryDate = Convert.ToDateTime(entryDate);
                else
                    deDupInfo.EntryDate = (DateTime)SqlDateTime.MinValue;
            }
            catch (Exception)
            {
                error = "Entry date is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                deDupInfo.Active = csv.GetField<string>(36);
            }
            catch (Exception)
            {
                error = "Active is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                deDupInfo.LoanAccountNo = csv.GetField<string>(37).Replace(",", "");
            }
            catch (Exception)
            {
                error = "Loan Account No is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            return deDupInfo;
        }
    }
}
