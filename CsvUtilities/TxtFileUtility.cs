﻿using AzUtilities;
using BLL;
using BusinessEntities;
using CsvHelper;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvUtilities
{
    public class TxtFileUtility
    {
        //public int InsertIntoDatabase(string filePath)
        //{
        //    DataTable table = new DataTable();
        //    int inserted = 0;
        //    int rowIndex = 0;
        //    try
        //    {

        //        using (TextFieldParser parser = new TextFieldParser(filePath))
        //        {
        //            parser.TextFieldType = FieldType.Delimited;
        //            parser.Delimiters = new string[]{ "|"};

        //            DeDupInfoManager DeDupInfoManagerObj = new DeDupInfoManager();

        //            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(DeDupInfo));

        //            foreach (PropertyDescriptor prop in properties)
        //            {
        //                table.Columns.Add(prop.Name, typeof(String));
        //            }

        //            string[] headers = parser.ReadFields();

        //            DataTable sample = table.Clone();

        //            string[] row = null;
        //            while (true)
        //            {
        //                row = parser.ReadFields();

        //                if (row == null)
        //                    break;

        //                GetDeDupInfo(row, rowIndex, DeDupInfoManagerObj, ref table);
        //                rowIndex++;
        //            }

        //            LogFile.WriteLine(DateTime.Now.ToString() + " Sending for Database insertion");
        //            inserted = DeDupInfoManagerObj.SendDedupeInToFinalTable(table);
        //            LogFile.WriteLine(DateTime.Now.ToString() + " Database insertion completed");

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        LogFile.WriteLog(ex);
        //        throw new Exception("MFU Upload Failed at row index  :: " + rowIndex);
        //    }
        //    return inserted;
        //}

        private void GetDeDupInfo(string[] line, int rowIndex, DeDupInfoManager DeDupInfoManagerObj, ref DataTable table)
        {
            if (rowIndex % 1000 == 0)
                LogFile.WriteLine(DateTime.Now.ToString() + " Line processed upto :: " + rowIndex);
            string error = "";
            DataRow row = table.NewRow();

            try
            {
                row["Name"] = line[0];
            }
            catch (Exception)
            {
                error = "Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["FatherName"] = line[1];
            }
            catch (Exception ex)
            {
                error = "Father Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                row["MotherName"] = line[2];
            }
            catch (Exception)
            {
                error = "Mother Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                row["ScbMaterNo"] = line[3];
            }
            catch (Exception)
            {
                error = "Scb Mater No is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }
            try
            {
                string dob = line[4];
                if (!String.IsNullOrEmpty(dob))
                {
                    if (!dob.StartsWith("######"))
                        row["DateOfBirth"] = Convert.ToDateTime(dob);
                    else
                        row["DateOfBirth"] = (DateTime)SqlDateTime.MinValue;
                }
                else
                {
                    row["DateOfBirth"] = (DateTime)SqlDateTime.MinValue;
                }
            }
            catch (Exception)
            {
                error = "Date Of Birth is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["Profession"] = line[5];

            }
            catch (Exception)
            {
                error = "Customer profession not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                string pi = line[6];
                if (!string.IsNullOrEmpty(pi))
                    row["ProductId"] = DeDupInfoManagerObj.GetProductId(pi);
                else
                    row["ProductId"] = 0;
            }
            catch (Exception)
            {
                error = "Product is not in correct in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["DeclineReason"] = line[7];
            }
            catch (Exception)
            {
                error = "Decline Reason is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                string declineDate = line[8];
                //deDupInfo.DeclineDate = !String.IsNullOrEmpty(declineDate) && !declineDate.StartsWith("######")
                //    ? Convert.ToDateTime(csv.GetField<string>(8))
                //    : DateTime.Now;
                if (!String.IsNullOrEmpty(declineDate) && !declineDate.StartsWith("######"))
                    row["DeclineDate"] = Convert.ToDateTime(declineDate);
                else
                    row["DeclineDate"] = (DateTime)SqlDateTime.MinValue;
            }
            catch (Exception)
            {
                error = "Decline Date is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                row["BusinessEmployeeName"] = line[9];
            }
            catch (Exception)
            {
                error = "Business Employee Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["Address1"] = line[10];
            }
            catch (Exception)
            {
                error = "Address1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                row["Address2"] = line[11];
            }
            catch (Exception)
            {
                error = "Address2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                row["Phone1"] = line[12];
            }
            catch (Exception)
            {
                error = "Phone1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                row["Phone2"] = line[13];
            }
            catch (Exception)
            {
                error = "Phone2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                row["Mobile1"] = line[14];
            }
            catch (Exception)
            {
                error = "Mobile1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["Mobile2"] = line[15];
            }
            catch (Exception)
            {
                error = "Mobile2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
            }

            try
            {
                row["BankBranch"] = line[16];
            }
            catch (Exception)
            {
                error = "Branch is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")"; throw new Exception(error);
                throw new Exception(error);
            }

            try
            {
                row["Source"] = line[17];
            }
            catch (Exception)
            {
                error = "Source is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }


            try
            {
                row["Status"] = line[18];
            }
            catch (Exception)
            {
                error = "Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }


            try
            {
                row["Remarks"] = line[19];
            }
            catch (Exception)
            {
                error = "Remarks is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["MfuTin"] = line[20];
            }
            catch (Exception)
            {
                error = "Tin is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                throw new Exception(error);
            }

            try
            {
                row["IdType"] = line[21];
            }
            catch (Exception)
            {
                error = "IdType is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                throw new Exception(error);
            }

            try
            {
                row["IdType1"] = line[22];
            }
            catch (Exception)
            {
                error = "IdType1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                throw new Exception(error);
            }

            try
            {
                row["IdType2"] = line[23];
            }
            catch (Exception)
            {
                error = "IdType3 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                throw new Exception(error);
            }

            try
            {
                row["IdType3"] = line[24];
            }
            catch (Exception)
            {
                error = "IdType4 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["IdType4"] = line[25];
            }
            catch (Exception)
            {
                error = "IdType5 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["IdNo"] = line[26];
            }
            catch (Exception)
            {
                error = "IdNo1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["IdNo1"] = line[27];
            }
            catch (Exception)
            {
                error = "IdNo2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }


            try
            {
                row["IdType2"] = line[28];
            }
            catch (Exception)
            {
                error = "IdType3 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            try
            {
                row["IdNo3"] = line[29];
            }
            catch (Exception)
            {
                error = "IdNo4 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception();
            }
            try
            {
                row["IdType4"] = line[30];
            }
            catch (Exception)
            {
                error = "IdType5 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                row["ResidentaileStatus"] = line[31];
            }
            catch (Exception)
            {
                error = "Residentail Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                row["EducationalLevel"] = line[32];
            }
            catch (Exception)
            {
                error = "Educational Level is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                row["MaritalStatus"] = line[33];
            }
            catch (Exception)
            {
                error = "Marital Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                row["CriteriaType"] = line[34];
            }
            catch (Exception)
            {
                error = "Criteria Type is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                string entryDate = line[35];
                if (!entryDate.StartsWith("######") && !string.IsNullOrEmpty(entryDate))
                    row["EntryDate"] = Convert.ToDateTime(entryDate);
                else
                    row["EntryDate"] = (DateTime)SqlDateTime.MinValue;
            }
            catch (Exception)
            {
                error = "Entry date is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                row["Active"] = line[36];
            }
            catch (Exception)
            {
                error = "Active is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }
            try
            {
                row["LoanAccountNo"] = line[37].Replace(",", "");
            }
            catch (Exception)
            {
                error = "Loan Account No is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                //CustomException.Save(e, error);
                throw new Exception(error);
            }

            if (rowIndex % 1000 == 0)
                LogFile.WriteLine(DateTime.Now.ToString() + " Single process completed");

            try
            {
                table.Rows.Add(row);
            }
            catch (Exception ex)
            {
                throw new Exception("Table row Adding exception :: " + ex.Message);
            }
        }

        public int InsertIntoDataBaseFromTable(DataTable table, string fileType, User user, DateTime uploadDate)
        {
            int inserted = 0;
            try
            {
                DeDupInfoManager _manager = new DeDupInfoManager();

                LogFile.WriteLine(DateTime.Now.ToString() + " Sending for Database insertion");
                inserted = _manager.SendDedupeInToFinalTable(table, fileType, user , uploadDate);
                LogFile.WriteLine(DateTime.Now.ToString() + " Database insertion completed");
            }
            catch (Exception)
            {
                throw;
            }
            return inserted;
        }


        private DataTable GetDataTable(string fileName)
        {
            using (var reader = new StreamReader("path\\to\\file.csv"))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                // Do any configuration to `CsvReader` before creating CsvDataReader.
                using (var dr = new CsvDataReader(csv))
                {
                    var dt = new DataTable();
                    dt.Columns.Add("Id", typeof(int));
                    dt.Columns.Add("Name", typeof(string));

                    dt.Load(dr);
                }
            }
            return null;
        }
    }
}
