using System;
using System.Data;

namespace Bat.Common
{
    public class UserManager
    {
        public static string m_lastErrorMessage = "";

        public class ReturnValue
        {
            public enum login
            {
                Successful,
                InvalidUser,
                InvalidPassword,                
                InactiveUser,
                PasswordExpired,
                DatabaseFail,
                EmptyUser
            }
            
            public enum Reply
            {
                Successful,
                Unsuccessful
            }            
        }        
        
        #region constructor
        //
        // Summary:
        //     Private Constructor for UserManager Table 
        //     Can not call for Create Instance
        //        
        private UserManager()
        {
            
        }
        //
        // Summary:
        //     Constructor for UserManager Table        
        //
        // Parameters:   
        //   tableName:
        //     An System.String The User Table Name. 
        //   fieldPrefix:
        //     An System.String The User Table Field Prefix Name. 
        //        
        public UserManager(String tableName, String fieldPrefix)
        {
            userTableName = tableName;
            fldPrefix = fieldPrefix;
        }

        #endregion 

        #region Private Proprties

        private string userTableName = "";
        private string fldPrefix = "";
        private string Password = "";
        
        private string _userLoginID;
        private string _userPassword;
        private int _userType;
        private string _userAccessRight;
        private int _userStatus;
        private string _passwordValidUntil;
        private string _passwordChangeDate = DateTime.Now.ToString(Connection.dateFormat);
        //private string _passwordValidUntil = DateTime.Now.AddMonths(3).ToString();
        //private string _passwordChangeDate;  
        #endregion 

        #region User Public Properties
        
        public string UserLoginId
        {
            get { return _userLoginID; }
            set { _userLoginID = value; }
        }
        public string UserPassword
        {
            get { return _userPassword; }
            set { _userPassword = value; }
        }
        public int UserType
        {
            get { return _userType; }
            set {_userType=value; }
        }
        public string UserAccessRight
        {
            get { return _userAccessRight; }
            set { _userAccessRight = value; }
        }
        public int UserStatus
        {
            get { return _userStatus; }
            set { _userStatus = value; }
        }
        public string PasswordValidUntil
        {
            get { return _passwordValidUntil; }
            set { _passwordValidUntil = value; }
        }
        /*public string PasswordChangeDate
        {
            get { return _passwordChangeDate; }
            //set { _passwordChangeDate = value; }
        }*/

        #endregion

        #region User Management Methods      

        //
        // Summary:
        //     Cheaking the User Login Id is Duplicate or not. User Login Id can not be duplicate.
        //
        // Parameters:        
        //   pUserLoginId:
        //     An System.String Value to compare        
        //
        // Returns:
        //     A Boolean value. Value True, The user login id is duplicate.False, The user login id is not duplicate.
        //
        // Exceptions:
        //   System.DbException:
        //
        public bool IsDuplicateUserLoginId(String pUserLoginId)
        {
            string sqlText = "Select " + fldPrefix + "_USER_LOGI_ID From " + userTableName + " Where " + fldPrefix + "_USER_LOGI_ID='"
                + pUserLoginId + "'";
            try
            {
                object objLoginID = DbQueryManager.ExecuteScalar(sqlText);
                if (objLoginID == null) return false;
                else
                {
                    m_lastErrorMessage = "The User Login ID is not available.";
                    return true;
                }
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                throw;
            }
        }

        //
        // Summary:
        //     Update User Information at User Table.
        //
        // Parameters:        
        //   pUserLoginId:
        //     An System.String User unique identity.
        //   pUserType:
        //     An System.Int32 What type of user it is.
        //   pUserAccessRight:
        //     An System.String The level of user power to access this application.        
        //
        // Returns:
        //     enum:ReturnValue.Reply. ReturnValue.Reply.Successful The user table update succefully. 
        //     ReturnValue.Reply.Unsuccessful The user table fail to update.       
        //
        // Exceptions:
        //   System.DbException:
        //
        public ReturnValue.Reply UpdateUserInfo(String pUserLoginId, Int16 pUserType, String pUserAccessRight)
        {            
            _userLoginID = pUserLoginId; _userType = pUserType; _userAccessRight = pUserAccessRight;
            string sqlText = String.Format("Update " + userTableName + " Set " + fldPrefix + "_USER_TYPE={0}" +
                    "," + fldPrefix + "_USER_ACCE_RIGH='{1}' Where " + fldPrefix + "_USER_LOGI_ID='{2}'", pUserType,
                    pUserAccessRight, pUserLoginId);
            try
            {
                if (DbQueryManager.ExecuteNonQuery(sqlText) > 0) return ReturnValue.Reply.Successful;
                else return ReturnValue.Reply.Unsuccessful;
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                throw;
            }
           
        }
 
        //
        // Summary:
        //     Set password for user and Update the password related field and insert these information for user history table.
        //
        // Parameters:        
        //   pUserLoginId:
        //     An System.String Indetify which user's password is setting.
        //   pUserPassword:
        //     An System.String The password.
        //   pPasswordValidUntil:
        //     An System.String Password validation time.
        //   pComplexity:
        //     enum: GenaratePassword.Complexity The complexity level of password.
        //
        // Returns:
        //     enum:ReturnValue.Reply. ReturnValue.Reply.Successful Password information update succeful. 
        //     ReturnValue.Reply.Unsuccessful Password information fail to update.       
        //
        // Exceptions:
        //   System.DbException:
        //
        public ReturnValue.Reply SetPassword(String pUserLoginId, String pUserPassword, String pPasswordValidUntil, GenaratePassword.Complexity pComplexity)
        {
            if (pComplexity.Equals(GenaratePassword.Complexity.Easy))
            {
                if (!this.ValidPasswordLength(pUserPassword))
                    return ReturnValue.Reply.Unsuccessful;
            }
            else if (pComplexity.Equals(GenaratePassword.Complexity.Numeric))
            {
                if (!this.ValidPasswordLength(pUserPassword))
                {
                    return ReturnValue.Reply.Unsuccessful;
                }
                else
                {
                    if (this.CompareLastPassword(pUserLoginId, pUserPassword)==0)
                    {
                        return ReturnValue.Reply.Unsuccessful;
                    }
                    else
                    {
                        foreach (char chr in pUserPassword)
                        {
                            if (!char.IsNumber(chr))
                            {
                                m_lastErrorMessage = "Password must be numeric";
                                return ReturnValue.Reply.Unsuccessful;
                            }
                        }
                    }
                }
            }
            else if (pComplexity.Equals(GenaratePassword.Complexity.Standard))
            {
                if (!this.ValidPasswordLength(pUserPassword))
                {
                    return ReturnValue.Reply.Unsuccessful;
                }
                else
                {
                    if (this.CompareLastPassword(pUserLoginId,pUserPassword) == 0)
                    {
                        return ReturnValue.Reply.Unsuccessful;
                    }
                    else
                    {
                        foreach (char chr in pUserPassword)
                        {
                            if (!char.IsLetterOrDigit(chr))
                            {
                                m_lastErrorMessage = "Password must be alphanumeric";
                                return ReturnValue.Reply.Unsuccessful;
                            }
                        }
                    }
                }
            }            
            _userLoginID = pUserLoginId; _userPassword = pUserPassword; _passwordValidUntil = pPasswordValidUntil;
            Password = EncryptDecrypt.Encrypt(pUserLoginId + pUserPassword);
            string sqlText="";
            if (pPasswordValidUntil!="")
            {
                sqlText = String.Format("Update " + userTableName + " Set " + fldPrefix + "_USER_PASS='{0}'," + fldPrefix + "_USER_PASS_VALL='{1}'" +
                    " Where " + fldPrefix + "_USER_LOGI_ID='{2}'", Password, pPasswordValidUntil, pUserLoginId);
            }
            else
            {
                sqlText = String.Format("Update " + userTableName + " Set " + fldPrefix + "_USER_PASS='{0}' " +
                    " Where " + fldPrefix + "_USER_LOGI_ID='{1}'", Password, pUserLoginId);
            }
            try
            {
                if (DbQueryManager.ExecuteNonQuery(sqlText) > 0)
                {
                    if (InsertUserHistory(pUserLoginId, Password) > 0) return ReturnValue.Reply.Successful;
                    else return ReturnValue.Reply.Unsuccessful;
                }
                else
                    return ReturnValue.Reply.Unsuccessful;
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                return ReturnValue.Reply.Unsuccessful;
            }
        }

        

        //
        // Summary:
        //     Cheaking the password length. It must be less than 15 and more than 4
        //
        // Parameters:   
        //   pUserPassword:
        //     An System.String The password.
        //
        // Returns:
        //     A Boolean value. If True Valid Password. 
        //     False Unvalid Password. 
        //
        private bool ValidPasswordLength(String pUserPassword)
        {
            if (pUserPassword.Length < 4)
            {
                m_lastErrorMessage = "Password minimum 4 charecter";
                return false;
            }
            else if (pUserPassword.Length > 15)
            {
                m_lastErrorMessage = "Password maximum 15 charecter";
                return false;
            }
            else return true;
        }

        //
        // Summary:
        //     Compare new password with current use password. It must be less than 15 and more than 4
        //
        // Parameters:   
        //   pUserPassword:
        //     An System.String The password.
        //
        // Returns:
        //     A System.Int32 value. If Zeor Password is mached with current password. 
        //     One Password is not mached with current password. 
        //
        private int CompareLastPassword(String pUserLoginId, String pUserPassword)
        {            
            if (EncryptDecrypt.Encrypt(pUserLoginId + pUserPassword).CompareTo(this.GetCurrentPassword(pUserLoginId))==0)
            {
                m_lastErrorMessage = "You can not use your last password";
                return 0;
            }
            else return 1;
        }        

        //
        // Summary:
        //     Get the current Password from Database.
        //
        // Parameters:   
        //   pUserLoginId:
        //     An System.String Which User's Password is to retrive.
        //
        // Returns:
        //     A System.String value. The current Password.
        //
        // Exceptions:
        //   System.DbException:
        //
        private string GetCurrentPassword(String pUserLoginId)
        {
            string sqlText = "Select " + fldPrefix + "_USER_PASS From " + userTableName + " Where " + fldPrefix + "_USER_LOGI_ID='" + pUserLoginId + "'";
            try
            {
                object objUserPass = DbQueryManager.ExecuteScalar(sqlText);
                if (objUserPass != null) return Convert.ToString(objUserPass);
                else return "";
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                return "";
            }
        }

        //
        // Summary:
        //     Insert Password related informatin in User History Table.
        //     Tracking the information how many time,When a User Password changed and Passwords those used. 
        //
        // Parameters:   
        //   pUserLoginId:
        //     An System.String Which User's Password.
        //   pUserPassword:
        //     An System.String The Password.
        //
        // Returns:
        //     A System.Int32 value. Value is grater than One Successfully inserted in Password History.
        //
        // Exceptions:
        //   System.DbException:
        //
        private int InsertUserHistory(string pUserLoginId, string pUserPassword)
        {
            string HistoryTable = userTableName + "_hist";
            string sqlText = String.Format("Insert into " + HistoryTable + " (" + fldPrefix + "_USER_LOGI_ID," + fldPrefix + "_USER_PASS," + fldPrefix + "_CHAN_DATE)" +
              " values('{0}','{1}','{2}')", pUserLoginId, pUserPassword, this._passwordChangeDate);
            try
            {
                return DbQueryManager.ExecuteNonQuery(sqlText);
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                return -1;
            }
            
        }                

        //
        // Summary:
        //     Automatic generate password for user and set that password for user.        
        //
        // Parameters:   
        //   pUserLoginId:
        //     An System.String For whome the password is generated.
        //   complexity:
        //     enum:GenaratePassword.Complexity The complexity level of password.
        //
        // Returns:
        //     A System.String The auto generated password.
        //
        public string GenerateAutoPassword(String pUserLoginID,GenaratePassword.Complexity complexity)
        {
            string autoPass = GenaratePassword.GeneratePassword(complexity);
            _passwordValidUntil="";
            if (this.SetPassword(pUserLoginID, autoPass, this._passwordValidUntil, complexity) == ReturnValue.Reply.Successful)
            {
                this._userPassword = autoPass;
                return autoPass;
            }
            else
            {
                m_lastErrorMessage = "Fail to reset password";
                return "";
            }
        }       
        
        //
        // Summary:
        //     Get the type of user        
        //
        // Parameters:   
        //   pUserLoginId:
        //     An System.String The corresponding User.     
        //
        // Returns:
        //     A System.Int32 The corresponding Type value.
        //
        // Exceptions:
        //   System.DbException:
        //
        public int GetUserType(string userLoginId)
        {
            string sqlText = "Select " + fldPrefix + "_USER_TYPE From " + userTableName + " Where " + fldPrefix + "_USER_LOGI_ID='" + userLoginId + "'";
            try
            {
                object objUserType = DbQueryManager.ExecuteScalar(sqlText);
                if (objUserType != null)
                {
                    this._userType = Convert.ToInt32(objUserType);
                    return Convert.ToInt32(objUserType);
                }
                else return -100;
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                return -101;
            }
        }

        //
        // Summary:
        //     Get the access right of user        
        //
        // Parameters:   
        //   pUserLoginId:
        //     An System.String The corresponding User.     
        //
        // Returns:
        //     A System.String The corresponding Access right value.
        //
        // Exceptions:
        //   System.DbException:
        //
        public string GetAccessRight(string userLoginId)
        {
            string sqlText = "Select " + fldPrefix + "_USER_ACCE_RIGH From " + userTableName + " Where " + fldPrefix + "_USER_LOGI_ID='" + userLoginId + "'";
            try
            {
                object objUserAccessRight = DbQueryManager.ExecuteScalar(sqlText);
                if (objUserAccessRight != null)
                {
                    this._userAccessRight = objUserAccessRight.ToString();
                    return objUserAccessRight.ToString();
                }
                else return "";
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                return m_lastErrorMessage;
            }
        }        

        //
        // Summary:
        //     Active the user.        
        //
        // Parameters:   
        //   pUserLoginId:
        //     An System.String The corresponding User.     
        //
        // Returns:
        //     enum:ReturnValue.Reply
        //
        // Exceptions:
        //   System.DbException:
        //
        public ReturnValue.Reply SetActivation(string pUserLoginID)
        {
            string sqlText = String.Format("Update " + userTableName + " Set " + fldPrefix + "_USER_STAT=1 Where " + fldPrefix + "_USER_LOGI_ID='{0}'"
                , pUserLoginID);
            try
            {
                if (DbQueryManager.ExecuteNonQuery(sqlText) > 0) return ReturnValue.Reply.Successful;
                else return ReturnValue.Reply.Unsuccessful;
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                return ReturnValue.Reply.Unsuccessful;
            }
        }

        //
        // Summary:
        //     InActive the user.        
        //
        // Parameters:   
        //   pUserLoginId:
        //     An System.String The corresponding User.     
        //
        // Returns:
        //     enum:ReturnValue.Reply
        //
        // Exceptions:
        //   System.DbException:
        //
        public ReturnValue.Reply SetInActivation(string pUserLoginID)
        {
            string sqlText = String.Format("Update " + userTableName + " Set " + fldPrefix + "_USER_STAT=0 Where " + fldPrefix + "_USER_LOGI_ID='{0}'"
                , pUserLoginID);
            try
            {
                if (DbQueryManager.ExecuteNonQuery(sqlText) > 0) return ReturnValue.Reply.Successful;
                else return ReturnValue.Reply.Unsuccessful;
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                return ReturnValue.Reply.Unsuccessful;
            }
        }

        //
        // Summary:
        //     Login the user and cheak user validation.        
        //
        // Parameters:   
        //   pUserLoginId:
        //     An System.String The corresponding User Login Id. 
        //   pUserPassword:
        //     An System.String The corresponding User Password. 
        //
        // Returns:
        //     enum:ReturnValue.login
        //
        // Exceptions:
        //   System.DbException:
        //
        public ReturnValue.login Login(string pUserLoginId, string pUserPassword)
        {
            string encryptPassword = EncryptDecrypt.Encrypt(pUserLoginId + pUserPassword);
            string sqlText = "Select " + fldPrefix + "_USER_LOGI_ID," + fldPrefix + "_USER_PASS," + fldPrefix + "_USER_TYPE," + fldPrefix + "_USER_STAT," + fldPrefix + "_USER_ACCE_RIGH," + fldPrefix + "_USER_PASS_VALL From " + userTableName + " Where " + fldPrefix + "_USER_LOGI_ID='" + pUserLoginId + "' AND " + fldPrefix + "_USER_STAT>-1";
            DataTable dt = new DataTable();
            try
            {
                dt = DbQueryManager.GetTable(sqlText);
            }
            catch
            {
                m_lastErrorMessage = DbQueryManager.ErrorMessage;
                return ReturnValue.login.DatabaseFail;
            }
            if (dt != null)
            {
                DataTableReader dtReader = dt.CreateDataReader();
                while (dtReader.Read())
                {
                    if (dtReader[fldPrefix + "_USER_PASS"].ToString() == encryptPassword)
                    {
                        if (Convert.ToInt32(dtReader[fldPrefix + "_USER_STAT"]) > 0)
                        {
                            if (dtReader[fldPrefix + "_USER_PASS_VALL"].ToString() != "")
                            {
                                if (Convert.ToDateTime(dtReader[fldPrefix + "_USER_PASS_VALL"]).Date < DateTime.Now.Date)
                                {
                                    m_lastErrorMessage = "Password Expired";
                                    return ReturnValue.login.PasswordExpired;
                                }
                            }

                            _userLoginID = dtReader[fldPrefix + "_USER_LOGI_ID"].ToString();
                            _userType = Convert.ToInt32(dtReader[fldPrefix + "_USER_TYPE"]); _userAccessRight = dtReader[fldPrefix + "_USER_ACCE_RIGH"].ToString();
                            _userStatus = Convert.ToInt32(dtReader[fldPrefix + "_USER_STAT"]); _userPassword = dtReader[fldPrefix + "_USER_PASS"].ToString();
                            _passwordValidUntil = dtReader[fldPrefix + "_USER_PASS_VALL"].ToString();
                            return ReturnValue.login.Successful;

                        }
                        else
                        {
                            m_lastErrorMessage = "Inactive User";
                            return ReturnValue.login.InactiveUser;
                        }
                    }
                    else
                    {
                        m_lastErrorMessage = "Invalid Password";
                        return ReturnValue.login.InvalidPassword;
                    }
                }
            }
            else
            {
                if (IsEmptyUser())
                {
                    m_lastErrorMessage = "Empty User";
                    return ReturnValue.login.EmptyUser;
                }
            }
            m_lastErrorMessage = "Invalid User";
            return ReturnValue.login.InvalidUser;
        }
        /// <summary>
        /// This function is used to check the user table for empty
        /// </summary>
        /// <returns></returns>
        private bool IsEmptyUser()
        {
            string cmdText = "SELECT COUNT(*) FROM " + userTableName + "";
            object count;
            try
            {
                count = DbQueryManager.ExecuteScalar(cmdText);
                if (count.ToString() == "0")
                    return true;
            }
            catch (Exception e)
            {

            }
            return false;
        }
        //
        // Summary:
        //     Get the last error message.        
        //        
        // Returns:
        //     A System.String The Error Message.
        //
        public static string GetLastErrorMessage()
        {
            return m_lastErrorMessage;
        }

        #endregion
    }
}
