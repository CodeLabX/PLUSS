using System;
using System.Configuration;
using System.Web.Configuration;

namespace Bat.Common
{
    public class ConstringConfiguration
    {
        private string _conString = "";

        /*public string EConString(string con, string uid, string pwd)
        {
            //string O_conString = con;
            _conString = con;
            UserID = "X " + this.Encrypt(uid);
            Password = "X " + this.Encrypt(pwd);
            con = _conString;
            //DConString = O_conString;
            return con;
        }*/
        public string EConString(string con)
        {
            _conString = con;
            string uid = UserID;
            string pwd = Password;
            if (!uid.Contains("X ") && !pwd.Contains("X "))
            {
                UserID = "X " + EncryptDecrypt.Encrypt(uid);
                Password = "X " + EncryptDecrypt.Encrypt(pwd);
            }
            con = _conString;
            return con;
        }

        public string DConString
        {
            get
            {
                string uid = UserID;
                string pwd = Password;
                if (uid.Substring(0, 2) == "X ")
                {
                    UserID = EncryptDecrypt.Decrypt(uid.Substring(2));
                }
                if (pwd.Substring(0, 2) == "X ")
                {
                    Password = EncryptDecrypt.Decrypt(pwd.Substring(2));
                }
                return _conString;
            }
            set { _conString = value; }
        }

        public string getValue(string id)
        {
            int pos = _conString.IndexOf(id);
            if (pos != -1)
            {
                int pos2 = _conString.IndexOf("=", pos);
                int len = _conString.IndexOf(";", pos2);
                if (pos2 != -1 && len != -1) return _conString.Substring(pos2 + 1, len - (pos2 + 1));

            }
            return "";
        }

        public void setValue(string id, string value)
        {
            int pos = _conString.IndexOf(id);
            if (pos != -1)
            {
                int pos2 = _conString.IndexOf("=", pos);
                int len = _conString.IndexOf(";", pos2);
                if (pos2 != -1 && len != -1)
                    _conString = _conString.Substring(0, pos2 + 1) + value + _conString.Substring(len);
            }
        }

        public string UserID
        {
            get { return getValue("user id"); }
            set { setValue("user id", value); }
        }

        public string Password
        {
            get { return getValue("password"); }
            set { setValue("password", value); }
        }
        
        
    }
}
