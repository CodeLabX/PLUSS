using Bat.Common;
using System;
using System.Collections.Generic;
using System.Data;

namespace Utilities
{
	public class Data<T>
	{
		public static List<T> DataSource(string query)
		{
			try
			{
				DataTable dataTable = DbQueryManager.GetTable(query);
				return (List<T>)ListConversion.ConvertTo<T>(dataTable);
			}
			catch (Exception ex)
			{
			}
			finally
			{
			}
			return null;
		}

		//public static List<T> GenericDataSource(string query)
		//{
		//	CommonConnection commonConnection = new CommonConnection();
		//	try
		//	{
		//		DataTable dataTable = commonConnection.GetDataTable(query);
		//		return (List<T>)GenericListGenerator.GetList<T>(dataTable);
		//	}
		//	catch (Exception ex)
		//	{
		//		throw ex;
		//	}
		//	finally
		//	{
		//		commonConnection.Close();
		//	}
		//}
	}
}
