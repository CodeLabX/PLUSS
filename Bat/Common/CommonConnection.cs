﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;

using System.Text;
using Bat.Common;


namespace Utilities
{
    public class CommonConnection
    {

        public bool IsConfigureDb { get; set; }
        private string ConnectionString = "";
        private IDataReader reader = null;
        private bool isDbConnection { get; set; }
        public SqlConnection SqlConnection = null;
        public DatabaseType DatabaseType { get; set; }

        private DbCommand dbCommand = null;
        private DbConnection dbConnection = null;
        private IsolationLevel rIsolationLevel;
        private bool isIsolation { get; set; }
        private DbTransaction dbTransaction = null;

        public CommonConnection() { }
        public CommonConnection(IsolationLevel readCommitted)
        {
            rIsolationLevel = readCommitted;
            isIsolation = true;

            ConnectionStringSettingsCollection connections = ConfigurationManager.ConnectionStrings;
            ConnectionString = "conString";
            this.DatabaseType = DatabaseType.SQL;
            ConstringManager constringManager = new ConstringManager();
            ConnectionString = constringManager.DecryptConstring("conString");

            //dbConnection = new SqlConnection(connections[ConnectionString].ConnectionString);
            dbConnection = new SqlConnection(ConnectionString);
            dbConnection.Open();
            IsConfigureDb = true;
        }

        public string GetConnectionString()
        {
            ConnectionStringSettingsCollection connections = ConfigurationManager.ConnectionStrings;
            ConnectionString = "conString";
            this.DatabaseType = DatabaseType.SQL;
            ConstringManager constringManager = new ConstringManager();
            ConnectionString = constringManager.DecryptConstring("conString");
            return ConnectionString;
        }

        public int ExecuteNonQuery(string sql)
        {
            if (isIsolation)
            {
                return ExecuteDbCommand(sql);
            }
            return 0;
        }

        private int ExecuteDbCommand(string sqlTxt)
        {
            dbCommand = new SqlCommand(sqlTxt, (SqlConnection)dbConnection);
            dbCommand.CommandText = sqlTxt;
            dbCommand.Connection = dbConnection;
            if (dbConnection.State == ConnectionState.Closed)
                dbConnection.Open();
            return dbCommand.ExecuteNonQuery();
        }

        public void RollBack()
        {
            if (isIsolation)
            {
                dbTransaction.Rollback();
            }
        }

        public void Close()
        {
            if (isIsolation)
            {
                dbCommand.Dispose();
                dbTransaction.Dispose();
                dbConnection.Close();
            }
        }



        public void BeginTransaction()
        {
            if (isIsolation)
            {
                if (dbConnection.State == ConnectionState.Closed)
                {
                    dbConnection.Open();
                }

                dbTransaction = dbConnection.BeginTransaction(rIsolationLevel);
                dbCommand = dbConnection.CreateCommand();
                dbCommand.Transaction = dbTransaction;
            }

        }


        public void CommitTransaction()
        {
            if (isIsolation)
            {
                dbTransaction.Commit();
            }
        }


        public DataTable GetDataTable(string sql, int minute)
        {
            DataTable returnDT = new DataTable();

            SqlConnection com = new SqlConnection(dbConnection.ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, com);
            cmd.CommandTimeout = minute * 60 * 1000;
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            adapter.Fill(returnDT);


            return returnDT;
        }

        public DataTable GetXLDataTable()
        {
            if (dbConnection.State == ConnectionState.Closed)
            {
                dbConnection.Open();
            }
            dbCommand = new OleDbCommand("SELECT * FROM [Sheet1$]");
            dbCommand.Connection = dbConnection;
            DataTable dt = new DataTable();
            dt.Load(dbCommand.ExecuteReader());
            dbConnection.Close();
            return dt;
        }

        /// <summary>
        /// Get XL DATA TABLE OF SPECIFIC SHEET NAME
        /// </summary>
        /// <param name="sheetName">SHEET NAME</param>
        /// <returns></returns>
        public DataTable GetXLDataTable(string sheetName)
        {
            if (dbConnection.State == ConnectionState.Closed)
            {
                dbConnection.Open();
            }
            dbCommand = new OleDbCommand("SELECT * FROM [" + sheetName + "$]");
            dbCommand.Connection = dbConnection;
            DataTable dt = new DataTable();
            dt.Load(dbCommand.ExecuteReader());
            dbConnection.Close();
            return dt;
        }

        public DataTable GetXLDataTable(string sheetName, int userId)
        {
            if (dbConnection.State == ConnectionState.Closed)
            {
                dbConnection.Open();
            }
            dbCommand = new OleDbCommand(string.Format("SELECT *,{0} as UserId FROM [{1}$]", userId, sheetName));
            dbCommand.Connection = dbConnection;
            DataTable dt = new DataTable();
            dt.Load(dbCommand.ExecuteReader());
            dbConnection.Close();
            return dt;
        }


    }
}