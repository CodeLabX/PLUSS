﻿using System.Configuration;

namespace Bat.Common
{
    public static class WebSequirty
    {
        private static string provider = "DataProtectionConfigurationProvider";
        private static string section = "connectionStrings";
        public static bool EncryptWebConfig(Configuration config)
        {
            ConfigurationSection configSection = config.GetSection("connectionStrings");
            if (!configSection.SectionInformation.IsProtected)
            {
                configSection.SectionInformation.ProtectSection(provider);
                config.Save();
            }
            return true;

        }

        public static bool DecryptWebConfig(Configuration config)
        {
            ConfigurationSection configSection = config.GetSection("connectionStrings");
            if (configSection.SectionInformation.IsProtected)
            {
                configSection.SectionInformation.UnprotectSection();
                config.Save();

            }
            return true;
        }
    }
}
