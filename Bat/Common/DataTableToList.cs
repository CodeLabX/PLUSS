﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;

namespace Bat.Common
{
    public static class DataTableToList
    {
        /// <summary>
        /// Convert Data Table to a List of Object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<T> GetList<T>(DataTable dt)
        {
            if (dt == null) return null;
            var data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                var item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            var temp = typeof(T);
            var obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        if (dr[column.ColumnName] != System.DBNull.Value) 
                        {
                            pro.SetValue(obj, dr[column.ColumnName], null);
                        }
                    }

                    else
                    {
                        continue;
                        
                    }
                      
                }
            }
            return obj;
        }
    }
}
