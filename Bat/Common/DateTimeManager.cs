﻿using System;


namespace Bat.Common
{
    public class DateTimeManager
    {
        public static String ToDBDateFormat(DateTime dateTime)
        {            
            return dateTime.ToString(Connection.dateFormat);            
        }

        public static DateTime ToUIDateFormat(String dateTime)
        {
            string day = "";  
            string month = "";
            string year = "";
            string hour = "";
            string minute = "";
            string second = "";            
            // Month configure
            int mi=Connection.dateFormat.IndexOf("M");
            int mj=Connection.dateFormat.LastIndexOf("M");
            string nMonth = dateTime.Substring(mi, ((mj - mi) + 1));
            if (mj == mi + 1)
            {
                switch (nMonth)
                {
                    case "01":
                        month = "Jan";
                        break;
                    case "02":
                        month = "Feb";
                        break;
                    case "03":
                        month = "Mar";
                        break;
                    case "04":
                        month = "Apr";
                        break;
                    case "05":
                        month = "May";
                        break;
                    case "06":
                        month = "Jun";
                        break;
                    case "07":
                        month = "Jul";
                        break;
                    case "08":
                        month = "Aug";
                        break;
                    case "09":
                        month = "Sep";
                        break;
                    case "10":
                        month = "Oct";
                        break;
                    case "11":
                        month = "Nov";
                        break;
                    case "12":
                        month = "Dec";
                        break;
                    default:
                        month = nMonth;
                        break;
                }                
            }           
            else
            {
                month = nMonth;
            }

            // Day configure
            int di = Connection.dateFormat.IndexOf("d");
            int dj = Connection.dateFormat.LastIndexOf("d");
            day = dateTime.Substring(di, ((dj - di) + 1));

            // Year configure
            int yi = Connection.dateFormat.IndexOf("y");
            int yj = Connection.dateFormat.LastIndexOf("y");
            year = dateTime.Substring(yi, ((yj - yi) + 1)); 
       
            // Hour configure
            int hi = Connection.dateFormat.IndexOf("H");
            int hj = Connection.dateFormat.LastIndexOf("H");            
            hour = dateTime.Substring(hi, ((hj - hi) + 1));

            // Minute configure
            int mni = Connection.dateFormat.IndexOf("m");
            int mnj = Connection.dateFormat.LastIndexOf("m");
            minute = dateTime.Substring(mni, ((mnj - mni) + 1));

            // Second configure
            int si = Connection.dateFormat.IndexOf("s");
            int sj = Connection.dateFormat.LastIndexOf("s");
            second = dateTime.Substring(si, ((sj - si) + 1));

            string cDate = month + "-" + day + "-" + year+" "+hour+":"+minute+":"+second;
            return Convert.ToDateTime(cDate);
        }

        
    }
}
