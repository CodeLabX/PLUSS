using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace Bat.Common
{
    public class Connection
    {
        public static string sTemp;//System.Configuration.ConfigurationManager.ConnectionStrings["SPMSConnectionString"].ConnectionString;
        private static string[] sTempList;// = sTemp.Split(";".ToCharArray());

        public static string ConnectionStringName = "";
        public static string ConnectionString = "";
        public static string ConnectionString2 = "";
        public static string ProviderName = "";
        public static string dateFormat = "";
        private static ConstringManager constringManager = new ConstringManager();
        //private static string conStringName = ConfigurationManager.ConnectionStrings[0].Name;
        //string conString = conED.DecryptConstring(conStringName);

        /// <summary>
        /// Cannot Instantiate this class
        /// </summary>
        private Connection()
        {

        }


        /// <summary>
        /// Returns a closed DbConnection object. User has to open/close it whenever required.
        /// </summary>
        /// <returns></returns>
        /// 
        public static DbConnection GetConnection()
        {
            if (ConnectionString.Length == 0)
            {
                if (ConnectionStringName.Length != 0)
                {
                    ConnectionString = constringManager.DecryptConstring(ConnectionStringName);
                    //ProviderName = ConfigurationManager.ConnectionStrings[ConnectionStringName].ProviderName;
                }
                else
                    UserManager.m_lastErrorMessage = "Please specify Connectionstring name.";
            }
            return CreateConnection(ConnectionString);
        }

        /// <summary>
        /// Returns a closed DbConnection object. User has to open/close it whenever required.
        /// </summary>
        /// <param name="connectionStringName"></param>
        /// <returns></returns>
        public static DbConnection GetConnection(string connectionStringName)
        {
            if (ConnectionString2.Length == 0)
            {
                if (connectionStringName.Length > 0)
                {
                    ConnectionString2 = constringManager.DecryptConstring(connectionStringName);
                }
                else
                    UserManager.m_lastErrorMessage = "Please specify Connectionstring name for web.";
            }
            return CreateConnection(ConnectionString2);
        }

        private static DbConnection CreateConnection(string connectionString)
        {
            try
            {
                /*if (ConnectionString.Length == 0)
                {
                    ConnectionString = constringManager.DecryptConstring(ConnectionStringName);
                    ProviderName = ConfigurationManager.ConnectionStrings[ConnectionStringName].ProviderName;
                }*/
                //string ConnectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["conString"].ConnectionString);
                DbProviderFactory Dbfactory = GetDbFactory();
                DbConnection conn = Dbfactory.CreateConnection();
                conn.ConnectionString = connectionString;
                return conn;
            }
            catch (DbException)
            {
                throw new Exception("An exception has occured while creating the connection. Please check Connection String settings in the web.config file.");
            }
        }

        /// <summary>
        /// Provides with a DbProviderFactory object with the provider name from the config file.
        /// </summary>
        /// <returns>DbProviderFactory object</returns>
        internal static DbProviderFactory GetDbFactory()
        {
            try
            {
                if (ProviderName.Length == 0)
                {
                    ConnectionStringName = "conString";
                    ProviderName = ConfigurationManager.ConnectionStrings[ConnectionStringName].ProviderName;
                }
                DbProviderFactory Dbfactory = DbProviderFactories.GetFactory(ProviderName);
                return Dbfactory;
            }
            catch (DbException)
            {
                throw new Exception("An exception has occured while creating the database provider factory. Please check the ProviderName specified in the web.config file.");
            }
        }


        /// <summary>
        /// Provides with a DbProviderFactory object with the supplied Provider name.
        /// </summary>
        /// <param name="ProviderName">Data Provider Name (e.g.) Oledb, Odbc, SqlClient</param> 
        /// <returns>DbProviderFactory object</returns>
        internal static DbProviderFactory GetDbFactory(string ProviderName)
        {
            DataTable dtProviders = DbProviderFactories.GetFactoryClasses();

            if (dtProviders.Rows.Count == 0)
            {
                throw new Exception("No Data Providers are installed in the .Net FrameWork that implement the abstract DbProviderFactory Classes. ");
            }

            bool errorFlag = false;
            foreach (DataRow dr in dtProviders.Rows)
            {
                if (dr[2] != null)
                {
                    string ExistingProviderName = dr[2].ToString();
                    if (ProviderName.ToLower() == ExistingProviderName.Trim().ToLower())
                    {
                        errorFlag = false;
                        break;
                    }
                    else
                    {
                        errorFlag = true;
                    }

                }
            }

            if (errorFlag)
            {
                throw new Exception("The ProviderName string supplied is not a valid Provider Name<BR>or it does not implement the abstract DbProviderFactory Classes. <BR>The string ProviderName is case-sensitive. Also please check it for proper spelling. ");
            }
            DbProviderFactory Dbfactory = DbProviderFactories.GetFactory(ProviderName);
            return Dbfactory;
        }


        public static ConnectionPropertie GetConnectionProp(string connectionName)
        {
            ConnectionPropertie connection = new ConnectionPropertie();
            sTemp = System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            sTempList = sTemp.Split(";".ToCharArray());
            connection.ServerName = sTempList[0].Replace(ConnectionConstProp.ServerName, "");
            connection.DatabaseName = sTempList[1].Replace(ConnectionConstProp.DatabaseName, "");
            connection.UserId = sTempList[2].Replace(ConnectionConstProp.UserId, "");
            connection.Password = sTempList[3].Replace(ConnectionConstProp.Password, "");

            return connection;
        }
        public static string GetConnectionString(string server, string database, string userId, string password, string timeout)
        {

            string connectionString = string.Format(@"Data Source={0};Initial Catalog={1}; user id={2};password={3};Connect Timeout={4};Persist Security Info=True;", server, database, userId, password, timeout);
            return connectionString;
        }
    }


    public class ConnectionPropertie
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
    }
    public static class ConnectionConstProp
    {
        public const string ServerName = "Data Source=";
        public const string DatabaseName = "Initial Catalog=";
        public const string UserId = "user id=";
        public const string Password = "password=";


    }

}
