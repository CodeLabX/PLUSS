using System;
using System.Collections;

namespace Bat.Common
{
    public class CryptographyManager
    {
        private const string cryoptoPass = "TDSKEY";
        #region Constructor
        public CryptographyManager()
        {
        }
        #endregion Constructor

        public IList GetEncryptedDataList(IList ilRealDataList)
        {
            try
            {
                Cryptography oCryptography = new Cryptography();

                IList idRetVal = oCryptography.GetEncryptedDataList(ilRealDataList);

                return idRetVal;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public IList GetDecryptedDataList(IList ilRealDataList)
        {
            try
            {
                Cryptography oCryptography = new Cryptography();

                IList idRetVal = oCryptography.GetDecryptedDataList(ilRealDataList);

                return idRetVal;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public string GetDecryptedString(string sPassword)
        {
            try
            {
                Cryptography oCryptography = new Cryptography();

                string sDecrptedString = oCryptography.Decrypt(sPassword, cryoptoPass);
                return sDecrptedString;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public string GetEncryptedString(string sClearText)
        {

            return GetEncryptedString(sClearText, cryoptoPass);
        }

        public string GetEncryptedString(string sClearText, string sPassword)
        {
            try
            {
                Cryptography oCryptography = new Cryptography();

                string sEncrptedString = oCryptography.Encrypt(sClearText, sPassword);
                return sEncrptedString;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public string GetDecryptedString(string sClearText, string sPassword)
        {
            try
            {
                Cryptography oCryptography = new Cryptography();

                string sDecrptedString = oCryptography.Decrypt(sClearText, sPassword);
                return sDecrptedString;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}