using System;
using System.Text;
using System.Security.Cryptography;

namespace Bat.Common
{
    public class EncryptDecrypt
    {
        public static string Encrypt(string stringValue)
        {
            try
            {
                byte[] keyArray;
                byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(stringValue);

                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes("BatEnDeSys"));
                hashmd5.Clear();

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();

                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch
            {
                throw;
            }
        }

        public static string Decrypt(string stringValue)
        {
            try
            {
                byte[] keyArray;
                byte[] toEncryptArray = Convert.FromBase64String(stringValue);
                MD5CryptoServiceProvider objMD5 = new MD5CryptoServiceProvider();
                keyArray = objMD5.ComputeHash(UTF8Encoding.UTF8.GetBytes("BatEnDeSys"));
                objMD5.Clear();

                TripleDESCryptoServiceProvider objTriple = new TripleDESCryptoServiceProvider();
                objTriple.Key = keyArray;
                objTriple.Mode = CipherMode.ECB;
                objTriple.Padding = PaddingMode.PKCS7;

                ICryptoTransform objTransform = objTriple.CreateDecryptor();
                byte[] resultArray = objTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                objTriple.Clear();

                return UTF8Encoding.UTF8.GetString(resultArray);
            }
            catch
            {
                throw;
            }
        }
    }
}
