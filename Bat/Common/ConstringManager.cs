using System;
using System.Configuration;
using System.Web.Configuration;

namespace Bat.Common
{
    public class ConstringManager
    {
        private ConstringConfiguration consConfig = new ConstringConfiguration();
        private Configuration config;
        private string Constringname { get; set; }
        public ConstringManager()
        {
          
        }
        public ConstringManager(string appPath)
        {
            config = WebConfigurationManager.OpenWebConfiguration(appPath);
            //conStringName = config.ConnectionStrings.ConnectionStrings[0].Name;
        }
        public ConstringManager(string appPath, string ConStrName)
        {
            Constringname = ConStrName;
            if (string.IsNullOrEmpty(appPath))
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            else
            {
                config = WebConfigurationManager.OpenWebConfiguration(appPath);
            }
        }
        public int EncryptConstring()
        {
            for (int i = 0; i < config.ConnectionStrings.ConnectionStrings.Count; i++)
            {
                try
                {
                    string conStringName = config.ConnectionStrings.ConnectionStrings[i].Name;
                    string conString = config.ConnectionStrings.ConnectionStrings[conStringName].ConnectionString;
                    config.ConnectionStrings.ConnectionStrings[conStringName].ConnectionString = consConfig.EConString(conString);
                    config.Save();
                }
                catch
                {
                    return -1;
                }

            }
            return 1;
        }
        public string DecryptConstring(string name)
        {
            string dconString = "";            
            try
            {                
                consConfig.DConString = ConfigurationManager.ConnectionStrings[name].ConnectionString;
                dconString = consConfig.DConString;
            }
            catch
            {
                return "";
            }

            //}
            return dconString;
        }

        public int SetConnectionString(string server, string database, string userId, string password, string timeout)
        {
            CryptographyManager oCrypMg = new CryptographyManager();
            //if (!password.StartsWith("X"))
            //{
            //    password = "X" + oCrypMg.GetEncryptedString(password);
            //}

            string connectionString = Connection.GetConnectionString(server, database, userId, password, timeout);

            try
            {
                config.ConnectionStrings.ConnectionStrings[Constringname].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch
            {
                return -1;
            }
            return 1;

        }
    }
}
