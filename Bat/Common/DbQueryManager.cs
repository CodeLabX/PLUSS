using AzUtilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Utilities;
using System.Web;
using System.Web.UI;
//using System.Text.RegularExpressions;

namespace Bat.Common
{
    public class DbQueryManager
    {
        public static string ErrorMessage;
        //ADODB.Connection conSQL = new ADODB.Connection();
        //ADODB.Connection conSQL2 = new ADODB.Connection();
        /// <summary>
        /// Cannot Instantiate this class.
        /// </summary>
        public DbQueryManager()
        {

        }

        //private static Regex regEx = new Regex(@"([\\\n\r\b\t'""\x1a\x00])", RegexOptions.Compiled);

        //public override string ToString()
        //{
        //    return regEx.Replace(base.ToString(), new MatchEvaluator(FromToMatchEvaluator));
        //}

        //private static string FromToMatchEvaluator(Match match)
        //{
        //    switch (match.Value)
        //    {
        //        case "\\": return "\\\\";
        //        case "\n": return "\\n";
        //        case "\r": return "\\r";
        //        case "\b": return "\\b";
        //        case "\t": return "\\t";
        //        case "\x1a": return "\\Z";
        //        case "\x00": return "\\0";
        //        case "'": return "\\'";
        //        case "\"": return "\\\"";
        //        default: return match.Value;
        //    }
        //}


        #region Utility Methods



        /// <summary>
        /// Returns a DbCommand object.
        /// </summary>
        /// <returns>Returns a DbCommand object.</returns>
        public static DbCommand GetCommand()
        {
            DbCommand comm = Connection.GetDbFactory().CreateCommand();
            return comm;
        }



        /// <summary>
        /// Returns an array of DbParameter objects.
        /// </summary>
        /// <param name="count">Denotes the size of the required parameter array. Should be greater than 0.</param>
        /// <returns>Returns an array of DbParameter objects.</returns>
        public static DbParameter[] GetParameters(ushort count)
        {
            DbProviderFactory Dbfactory = Connection.GetDbFactory();
            DbParameter[] parameters = new DbParameter[count];

            try
            {
                for (int i = 0; i < count; i++)
                {
                    parameters[i] = Dbfactory.CreateParameter();
                }
            }
            catch (System.IndexOutOfRangeException)
            {
                throw new Exception("Count has to be greater than zero and less than 100", new IndexOutOfRangeException());
            }

            return parameters;
        }



        /// <summary>
        /// Returns a single DbParameter object 
        /// </summary>
        /// <returns>Returns a single DbParameter object </returns>
        public static DbParameter GetParameter()
        {
            DbProviderFactory Dbfactory = Connection.GetDbFactory();
            DbParameter parameter = Dbfactory.CreateParameter();
            return parameter;
        }
        /// <summary>
        /// Returns a Adapter object 
        /// </summary>
        /// <returns>Returns a single DbParameter object </returns>
        public static DbDataAdapter GetDataAdapter()
        {
            DbProviderFactory Dbfactory = Connection.GetDbFactory();
            DbDataAdapter adapter = Dbfactory.CreateDataAdapter();
            return adapter;
        }
        /// <summary>
        /// Returns a CommandBuilder object 
        /// </summary>
        /// <returns>Returns a single CommandBuilder object </returns>
        public static DbCommandBuilder GetDbCommandBuilder()
        {
            DbProviderFactory Dbfactory = Connection.GetDbFactory();
            DbCommandBuilder dbCommandBuilder = Dbfactory.CreateCommandBuilder();
            return dbCommandBuilder;
        }


        /// <summary>
        /// Returns a DataSet filled with data from the execution of the given Command.
        /// </summary>
        /// <param name="command">Command Object filled with necessary parameters.</param>        
        /// <returns>Returns a DataSet filled with data from the execution of the given Command.</returns>
        public static DataSet GetDataSet(string commandText)
        {
            ErrorMessage = String.Empty;
            DataSet ds = new DataSet();
            DbDataAdapter dbDap = null;
            DbCommand command = GetCommand();
            DbConnection conn = null;

            if (commandText == null)
            {
                ErrorMessage = "Please initilise the sql command text.";
                return null;
            }

            try
            {
                DbProviderFactory Dbfactory = Connection.GetDbFactory();
                dbDap = Dbfactory.CreateDataAdapter();
                conn = Connection.GetConnection();
                command.CommandText = commandText;
                command.Connection = conn;
                command.CommandTimeout = 0;
                dbDap.SelectCommand = command;
                dbDap.Fill(ds);
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                //ErrorMessage = ErrorMessage + exp.Message + exp.ErrorCode;
            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (dbDap != null)
                {
                    dbDap.Dispose();
                }
            }

            return ds;

        }


        /// <summary>
        /// Returns a DataSet filled with data from the execution of the given Command.
        /// </summary>
        /// <param name="commandText">Command Object filled with necessary parameters.</param>
        /// <param name="connectionStringName">To Create a new connection</param>
        /// <returns></returns>
        public static DataSet GetDataSet(string commandText, string connectionStringName)
        {
            ErrorMessage = String.Empty;
            DataSet ds = new DataSet();
            DbDataAdapter dbDap = null;
            DbCommand command = GetCommand();
            DbConnection conn = null;

            if (commandText == null)
            {
                ErrorMessage = "Please initilise the sql command text.";
                return null;
            }

            try
            {
                DbProviderFactory Dbfactory = Connection.GetDbFactory();
                dbDap = Dbfactory.CreateDataAdapter();
                conn = Connection.GetConnection(connectionStringName);
                command.CommandText = commandText;
                command.Connection = conn;
                command.CommandTimeout = 0;
                dbDap.SelectCommand = command;
                dbDap.Fill(ds);
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                //ErrorMessage = ErrorMessage + exp.Message + exp.ErrorCode;
            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (dbDap != null)
                {
                    dbDap.Dispose();
                }
            }

            return ds;

        }


        /// <summary>
        /// Executes the command object returning an int value.
        /// </summary>
        /// <param name="command">Command object filled with the necessary parameters.</param>        
        /// <returns>Int32 value indicating the error.</returns>
        public static int ExecuteNonQuery(string commandText)
        {
            ErrorMessage = String.Empty;
            //DbTransaction tran = null;
            DbConnection conn = null;
            int result = 0;
            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the sql command text.";
                return result = -1;
            }

            try
            {
                conn = Connection.GetConnection();
                command.CommandText = commandText;
                command.Connection = conn;
                conn.Open();
                command.CommandTimeout = 0;
                //tran = conn.BeginTransaction();
                //command.Transaction = tran;
                result = command.ExecuteNonQuery();
                //result = Convert.ToInt32(command.ExecuteScalar());
                //tran.Commit();
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                //ErrorMessage = ErrorMessage + exp.Message;
                //if (exp.ErrorCode == -2147467259) 
                //{
                //    result=22;
                //}  
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                /*if (tran != null)
                {
                    tran.Rollback();
                }*/

                ///DumpSQL
                LogFile.WriteLine("DbQueryManager ExecuteNonQuery :: " + commandText);
                LogFile.WriteLog(exp);
                throw exp;

            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
                throw ex;
            }

            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                /*if (tran != null)
                {
                    tran.Dispose();
                }*/
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }
            }
            //  var x = DbQueryManager.ExecuteScalar("select SCOPE_IDENTITY() as x");
            return result;
        }

        /// <summary>
        /// Return Identity key after Insert row into table
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public static int ExecuteNonQueryAndReturnPrimaryKey(string commandText)
        {
            ErrorMessage = String.Empty;
            DbConnection conn = null;
            int result = 0;
            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the sql command text.";
                return result = -1;
            }

            try
            {
                conn = Connection.GetConnection();
                command.CommandText = commandText + "SELECT SCOPE_IDENTITY()";
                command.Connection = conn;
                conn.Open();
                command.CommandTimeout = 0;
                result = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (DbException exp)
            {
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
            }

            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// Executes the command object returning an int value.
        /// </summary>
        /// <param name="commandText"></param>        
        /// <param name="connectionStringName"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string commandText, string connectionStringName)
        {
            ErrorMessage = String.Empty;
            //DbTransaction tran = null;
            DbConnection conn = null;
            int result = 0;
            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the sql command text.";
                return result = -1;
            }

            try
            {
                conn = Connection.GetConnection(connectionStringName);
                command.CommandText = commandText;
                command.Connection = conn;
                conn.Open();
                command.CommandTimeout = 0;
                //tran = conn.BeginTransaction();
                //command.Transaction = tran;
                result = command.ExecuteNonQuery();
                //tran.Commit();
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                //ErrorMessage = ErrorMessage + exp.Message;
                //if (exp.ErrorCode == -2147467259) 
                //{
                //    result=22;
                //}  
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                /*if (tran != null)
                 {
                     tran.Rollback();
                 }*/

            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
            }

            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                /*if (tran != null)
                {
                    tran.Dispose();
                }*/
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }
            }

            return result;
        }



        /// <summary>
        /// Executes the command object returning the first column of the first row.
        /// </summary>
        /// <param name="command">Command object filled with the necessary parameters.</param>        
        /// <returns>An object containing the first column of the first row.</returns>
        public static object ExecuteScalar(string commandText)
        {
            ErrorMessage = String.Empty;
            //DbTransaction tran = null;
            DbConnection conn = null;
            object result = null;
            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the command object.";
                return null;
            }

            try
            {
                conn = Connection.GetConnection();
                command.CommandText = commandText;
                command.Connection = conn;
                conn.Open();
                command.CommandTimeout = 0;
                //tran = conn.BeginTransaction();
                //command.Transaction = tran;
                result = command.ExecuteScalar();
                /*if (tran != null)
                {
                    tran.Commit();
                }*/
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                //ErrorMessage = ErrorMessage + exp.Message;
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                /*if (tran != null)
                {
                    tran.Rollback();
                }*/
            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
            }

            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
                /*if (tran != null)
                {
                    tran.Dispose();
                }*/

            }

            return result;
        }

        /// <summary>
        /// Executes the command object returning the first column of the first row.
        /// </summary>
        /// <param name="CommandText"></param>
        /// <param name="connectionStringName"></param>
        /// <returns></returns>
        public static object ExecuteScalar(string commandText, string connectionStringName)
        {
            ErrorMessage = String.Empty;
            //DbTransaction tran = null;
            DbConnection conn = null;
            object result = null;
            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the command object.";
                return null;
            }

            try
            {
                conn = Connection.GetConnection(connectionStringName);
                command.CommandText = commandText;
                command.Connection = conn;
                conn.Open();
                command.CommandTimeout = 0;
                //tran = conn.BeginTransaction();
                //command.Transaction = tran;
                result = command.ExecuteScalar();
                /*if (tran != null)
                {
                    tran.Commit();
                }*/
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                //ErrorMessage = ErrorMessage + exp.Message;
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                /*if (tran != null)
                {
                    tran.Rollback();
                }*/
            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
            }

            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
                /*if (tran != null)
                {
                    tran.Dispose();
                }*/

            }

            return result;
        }

        /// <summary>
        /// Uses a fast datareader to load and return a datatable.
        /// </summary>
        /// <param name="command">Command object filled with necessary parameters.</param>        
        /// <returns>Returns a loaded DataTable from the execution of the given Command.</returns>
        public static DataTable GetTable(string commandText)
        {
            ErrorMessage = String.Empty;
            //DbTransaction tran = null;
            DbConnection conn = null;
            DbDataReader reader = null;
            DataTable dtable = new DataTable();
            conn = Connection.GetConnection();

            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the command object.";
                return dtable;
            }

            try
            {
                command.CommandText = commandText;
                command.Connection = conn;
                conn.Open();
                //command.CommandTimeout = 0;
                //tran = conn.BeginTransaction();
                //command.Transaction = tran;
                reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    dtable.Load(reader);
                }
                else
                {
                    ErrorMessage = "No records found.  <BR>";
                    return null;
                }
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                //ErrorMessage = ErrorMessage + exp.Message;
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                var q = commandText.Replace("'", "''");
                var savesql = string.Format(@"INSERT INTO SqlTable (Query,Remarks) VALUES ('{0}','{1}')", q, ErrorMessage.Replace("'", "''"));
                DbQueryManager.ExecuteNonQuery(savesql);
            }

            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                var q = commandText.Replace("'", "''");
                var savesql = string.Format(@"INSERT INTO SqlTable (Query,Remarks) VALUES ('{0}','{1}')", q, err.Replace("'", "''"));
                DbQueryManager.ExecuteNonQuery(savesql);
            }

            finally
            {
                if (reader != null)
                    reader.Close();
                if (command != null)
                    command.Dispose();
                /*if (tran != null)
                {
                    tran.Dispose();
                }*/
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }
            }

            return dtable;
        }

        /// <summary>
        /// Uses a fast datareader to load and return a datatable.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="connectionStringName"></param>
        /// <returns></returns>
        public static DataTable GetTable(string commandText, string connectionStringName)
        {
            ErrorMessage = String.Empty;
            //DbTransaction tran = null;
            DbConnection conn = null;
            DbDataReader reader = null;
            DataTable dtable = new DataTable();
            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the command object.";
                return dtable;
            }

            try
            {
                conn = Connection.GetConnection(connectionStringName);
                command.CommandText = commandText;
                command.Connection = conn;
                conn.Open();
                command.CommandTimeout = 0;
                //tran = conn.BeginTransaction();
                //command.Transaction = tran;
                reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    dtable.Load(reader);
                }
                else
                {
                    ErrorMessage = "No records found.  <BR>";
                    return null;
                }
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                //ErrorMessage = ErrorMessage + exp.Message;
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                /*if (tran != null)
                {
                    tran.Rollback();
                }*/
            }

            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
            }

            finally
            {
                if (reader != null)
                    reader.Close();
                if (command != null)
                    command.Dispose();
                /*if (tran != null)
                {
                    tran.Dispose();
                }*/
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }
            }

            return dtable;

        }

        /// <summary>
        /// Executes the command object returning an DataReader.
        /// </summary>
        /// <param name="command">Command object filled with the necessary parameters.</param>
        /// <param name="ErrorMessage">Error Message if any.</param>
        /// <returns>DataReader</returns>
        public static DbDataReader ExecuteReader(string commandText)
        {
            ErrorMessage = String.Empty;
            //DbTransaction tran = null;
            DbConnection conn = null;
            DbDataReader reader = null;
            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the command object.";
                return reader;
            }

            try
            {
                conn = Connection.GetConnection();
                command.CommandText = commandText;
                command.Connection = conn;
                conn.Open();
                command.CommandTimeout = 0;
                //tran = conn.BeginTransaction();
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                //ErrorMessage = ErrorMessage + exp.Message;
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                /*if (tran != null)
                {
                    tran.Rollback();
                }*/
            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
            }

            finally
            {
                command.Dispose();
                /*if (tran != null)
                {
                    tran.Dispose();
                }
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }*/
            }
            return reader;

        }

        /// <summary>
        /// Executes the command object returning an DataReader.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="connectionStringName"></param>
        /// <returns></returns>
        public static DbDataReader ExecuteReader(string commandText, string connectionStringName)
        {
            ErrorMessage = String.Empty;
            //DbTransaction tran = null;
            DbConnection conn = null;
            DbDataReader reader = null;
            DbCommand command = GetCommand();
            if (commandText == null)
            {
                ErrorMessage = "Please initilise the command object.";
                return reader;
            }

            try
            {
                conn = Connection.GetConnection(connectionStringName);
                command.CommandText = commandText;
                command.Connection = conn;
                conn.Open();
                command.CommandTimeout = 0;
                //tran = conn.BeginTransaction();
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (DbException exp)
            {
                //ErrorMessage = "An exception has occured while executing the database transactions.  <BR>";
                //ErrorMessage = ErrorMessage + exp.Message;
                ErrorMessage = "DbException:" + exp.ErrorCode + "-" + exp.Message + "\r\n---- " + commandText + " ----\r\n";
                //WriteErrorLog(ErrorMessage);
                /*if (tran != null)
                {
                    tran.Rollback();
                }*/
            }
            catch (Exception ex)
            {
                string err = "Exception:" + ex.Message;
                //WriteErrorLog(err);
            }

            finally
            {
                command.Dispose();
                /*if (tran != null)
                {
                    tran.Dispose();
                }
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                if (conn != null)
                {
                    conn.Dispose();
                }*/
            }
            return reader;

        }
        ///// <summary>
        ///// Write Database Error Log
        ///// </summary>
        ///// <param name="errorMessage"></param>
        //private static void WriteErrorLog(string errorMessage)
        //{
        //    LogManager logManager = new LogManager();
        //    try
        //    {
        //        logManager.FilePath = @".\DatabaseTransaction\ErrorLog";
        //        logManager.PrefixName = "logDate-";
        //        logManager.WriteDaily(errorMessage);
        //    }
        //    catch
        //    {
        //        try
        //        {                    
        //            logManager.FilePath = @"DatabaseTransaction\ErrorLog";
        //            logManager.PrefixName = "logDate-";
        //            logManager.WriteDaily(errorMessage);
        //        }
        //        catch
        //        { }
        //    }
        //}        

        public static int ExecuteSP(string spName)
        {
            int rowAffected = 0;
            ErrorMessage = String.Empty;
            try
            {
                using (DbConnection connection = Connection.GetConnection())
                {
                    using (DbCommand command = GetCommand())
                    {
                        command.CommandText = spName;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Connection = connection;

                        command.Connection.Open();

                        rowAffected = command.ExecuteNonQuery();
                    }
                }

                return rowAffected;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rowAffected;
        }

        public static int ExecuteSP(string spName, List<SqlParameter> parameters)
        {
            int rowAffected = 0;
            ErrorMessage = String.Empty;
            try
            {
                using (DbConnection connection = Connection.GetConnection())
                {
                    using (DbCommand command = GetCommand())
                    {
                        command.CommandText = spName;
                        command.CommandType = CommandType.StoredProcedure;

                        foreach (SqlParameter item in parameters)
                        {
                            command.Parameters.Add(item);
                        }

                        command.Connection = connection;

                        command.Connection.Open();

                        rowAffected = command.ExecuteNonQuery();
                    }
                }

                return rowAffected;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rowAffected;
        }
        public static DataTable GetDataTableWithSp(string spName)
        {
            DataTable table = new DataTable();
            ErrorMessage = String.Empty;
            try
            {
                using (DbConnection connection = Connection.GetConnection())
                {
                    using (DbCommand command = GetCommand())
                    {
                        command.CommandText = spName;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Connection = connection;

                        command.Connection.Open();

                        using (DbDataAdapter adapter = Connection.GetDbFactory().CreateDataAdapter())
                        {
                            adapter.SelectCommand = command;

                            adapter.Fill(table);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }

            return table;
        }
        public static DataTable GetDataTable(string sql)
        {
            DataTable table = new DataTable();
            ErrorMessage = String.Empty;
            try
            {
                using (DbConnection connection = Connection.GetConnection())
                {
                    using (DbCommand command = GetCommand())
                    {
                        command.CommandText = sql;
                        command.CommandType = CommandType.Text;

                        command.Connection = connection;

                        command.Connection.Open();

                        using (DbDataAdapter adapter = Connection.GetDbFactory().CreateDataAdapter())
                        {
                            adapter.SelectCommand = command;

                            adapter.Fill(table);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }

            return table;
        }
        public static DataTable GetDataTableWithSpAndParam(string spName, List<SqlParameter> param)
        {
            DataTable table = new DataTable();
            ErrorMessage = String.Empty;
            try
            {
                using (DbConnection connection = Connection.GetConnection())
                {
                    using (DbCommand command = GetCommand())
                    {
                        command.CommandText = spName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection = connection;

                        foreach (var item in param)
                        {
                            command.Parameters.Add(item);
                        }
                        
                        command.Connection.Open();

                        using (DbDataAdapter adapter = Connection.GetDbFactory().CreateDataAdapter())
                        {
                            adapter.SelectCommand = command;

                            adapter.Fill(table);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }

            return table;
        }
        #endregion

        //------------------------Add By Hiru---------------------------

        //string sql_user = "root";
        //string sql_host = "localhost";
        ////string sql_host = "192.168.152.163";
        //string sql_pass = "";
        //string sql_db = "pluss";

        //public int DbOpen()
        //{
        //    DbConnection conn = null;
        //    try
        //    {
        //        if (conSQL.State != 1)
        //        {
        //            conn = Connection.GetConnection();
        //            //string conString = @"driver={MySQL ODBC 3.51 Driver};server =" + sql_host + "; database =" + sql_db + ";user id =" + sql_user + "; password =" + sql_pass + ";";
        //            string conString = @"driver={MySQL ODBC 3.51 Driver};" + conn.ConnectionString;
        //            conSQL.ConnectionString = conString;
        //            conSQL.Open(conSQL.ConnectionString, "", "", 0);
        //        }
        //        return conSQL.State;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("Connection Error " + ex.Message.ToString());
        //    }
        //}

        //public int DbOpen(string connectionStringName)
        //{
        //    DbConnection conn = null;
        //    try
        //    {
        //        if (conSQL2.State != 1)
        //        {
        //            conn = Connection.GetConnection(connectionStringName);
        //            //string conString = @"driver={MySQL ODBC 3.51 Driver};server =" + sql_host + "; database =" + sql_db + ";user id =" + sql_user + "; password =" + sql_pass + ";";
        //            string conString = @"driver={MySQL ODBC 3.51 Driver};" + conn.ConnectionString;
        //            conSQL2.ConnectionString = conString;
        //            conSQL2.Open(conSQL2.ConnectionString, "", "", 0);
        //        }
        //        return conSQL2.State;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("Connection Error " + ex.Message.ToString());
        //    }

        //}
        //public void dbClose()
        //{
        //    if (conSQL.State == 1)
        //    {
        //        conSQL.Close();
        //    }
        //    //return conSQL.State;
        //}
        //public void dbClose(string connectionStringName)
        //{
        //    if (conSQL2.State == 1)
        //    {
        //        conSQL2.Close();
        //    }
        //}

        //public int DbExecute(string sSQL)
        //{
        //    try
        //    {
        //        object objAffected;
        //        DbOpen();
        //        conSQL.Execute(sSQL, out objAffected, 0);
        //        return 0;
        //        dbClose();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("SQL Error " + ex.Message.ToString());
        //    }

        //}
        //public int DbExecute(string sSQL, string connectionStringName)
        //{
        //    try
        //    {
        //        object objAffected;
        //        DbOpen(connectionStringName);
        //        conSQL2.Execute(sSQL, out objAffected, 0);
        //        return 0;
        //        dbClose(connectionStringName);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("SQL Error " + ex.Message.ToString());
        //    }

        //}

        //public ADODB.Recordset DbOpenRset(string sSQL)
        //{
        //    ADODB.Recordset rs = new Recordset();
        //    try
        //    {
        //        object objAffected;
        //        DbOpen();
        //        return conSQL.Execute(sSQL, out objAffected, 0);
        //        dbClose();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("SQL Error " + ex.Message.ToString());
        //    }
        //}

        //public ADODB.Recordset DbOpenRset(string sSQL, string connectionStringName)
        //{
        //    ADODB.Recordset rs = new Recordset();
        //    try
        //    {
        //        object objAffected;
        //        DbOpen(connectionStringName);
        //        return conSQL2.Execute(sSQL, out objAffected, 0);
        //        dbClose(connectionStringName);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("SQL Error " + ex.Message.ToString());
        //    }
        //}

        //public string DbGetValue(string sSQL)
        //{
        //    string sStr=null;
        //    try
        //    {
        //        ADODB.Recordset rs = new ADODB.Recordset();
        //        rs = DbOpenRset(sSQL);
        //        if (!rs.EOF)
        //        {
        //            sStr = "" + rs.Fields[0].Value.ToString();
        //        }
        //        rs.Close();
        //        return sStr;
        //        dbClose();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("SQL Error " + ex.Message.ToString());
        //    }
        //}
        //public string DbGetValue(string sSQL, string connectionStringName)
        //{
        //    string sStr=null;
        //    try
        //    {
        //        ADODB.Recordset rs = new ADODB.Recordset();
        //        rs = DbOpenRset(sSQL,connectionStringName);
        //        if (!rs.EOF)
        //        {
        //            sStr = "" + rs.Fields[0].Value.ToString();
        //        }
        //        rs.Close();
        //        return sStr;
        //        dbClose(connectionStringName);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("SQL Error " + ex.Message.ToString());
        //    }
        //}

        public static DataTable GetDataTable(string sql, bool withHeaderIfEmpty)
        {
            try
            {
                CommonConnection commonConnection = new CommonConnection();
                SqlConnection connection = new SqlConnection(commonConnection.GetConnectionString());
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                var reader = cmd.ExecuteReader();
                var dataTable = new DataTable();
                if (withHeaderIfEmpty)
                {
                    dataTable.Load(reader);
                }
                else
                {
                    if (reader.HasRows)
                    {
                        dataTable.Load(reader);
                    }
                }
                connection.Close();
                return dataTable;
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("File: DbQueryManager; Method: GetDataTable(string sql, bool withHeaderIfEmpty)");
                LogFile.WriteLog(ex);
                throw ex;
            }
        }
    }
}

