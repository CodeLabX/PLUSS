﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities
{
    public enum DatabaseType
    {
        SQL,
        Oracle,
        MySql,
        Odbc,
        OleDb
    }

    public enum DatabaseProvider
    {
        ODT
    }
}