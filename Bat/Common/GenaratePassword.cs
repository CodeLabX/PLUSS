using System;

namespace Bat.Common
{
    public class GenaratePassword
    {        
        public enum Complexity
        {
            Standard, Easy, Numeric
        }        

        public static string GeneratePassword(Complexity complex)
        {
            string password = "";
            Int32 value = 0;
            Int32 second = (Convert.ToInt32(DateTime.Now.Day.ToString()) * (24 * 60 * 60));
            second += (Convert.ToInt32(DateTime.Now.Hour.ToString()) * (60 * 60));
            second += (Convert.ToInt32(DateTime.Now.Minute.ToString()) * 60);
            second += Convert.ToInt32(DateTime.Now.Second.ToString());
            Random rd = new Random(second);
            if (complex.Equals(Complexity.Numeric))
            {
                for (int i = 4; i > 0; i--)
                {
                    value = rd.Next(0, 9);
                    password += value;
                }
            }
            else if (complex.Equals(Complexity.Easy))
            {
                //do
                //{
                    value = rd.Next(65, 90);
                    password += (char)value;
                    //if (password.Length == length) break;
                    value = rd.Next(97, 122);
                    password += (char)value;
                    //if (password.Length == length) break;
                    value = rd.Next(0, 9);
                    password += value;
                    //if (password.Length == length) break;
                    value = rd.Next(0, 9);
                    password += value;
                    //if (password.Length == length) break;
                    value = rd.Next(97, 122);
                    password += (char)value;
                    //if (password.Length == length) break;
                    value = rd.Next(65, 90);
                    password += (char)value;
                    //if (password.Length == length) break;

                //} while (password.Length < length);
            }
            else if (complex.Equals(Complexity.Standard))
            {
                //do
                //{
                    value = rd.Next(65, 90);
                    password += (char)value;
                    //if (password.Length == length) break;
                    value = rd.Next(97, 122);
                    password += (char)value;
                   // if (password.Length == length) break;
                    value = rd.Next(0, 9);
                    password += value;                   
                   // if (password.Length == length) break;
                    /*if (value == 1) password += "@";
                    else if (value == 2) password += "%";
                    else if (value == 3) password += "$";
                    else password += "!";*/
                    //if (password.Length == length) break;                    
                    value = rd.Next(97, 122);
                    password += (char)value;
                    //if (password.Length == length) break;
                    value = rd.Next(65, 90);
                    password += (char)value;
                    value = rd.Next(0, 9);
                    password += value;
                    //if (password.Length == length) break;
                //} while (password.Length < length);
            }
            else password = "";

            return password;
        }

        /*internal static string GeneratePassword()
        {
            return GeneratePassword(Complexity.Standard);            
        }*/


        private static string GeneratePassword(Int32 userId)
        {

            string password = "";

            Random rd = new Random(userId);

            Int32 value = rd.Next(65, 90);
            password += (char)value;
            value = rd.Next(97, 122);
            password += (char)value;

            value = rd.Next(1, 5);
            password += value;
            if (value == 1) password += "@";
            else if (value == 2) password += "%";
            else if (value == 3) password += "$";
            else password += "!";

            value = rd.Next(97, 122);
            password += (char)value;
            value = rd.Next(65, 90);
            password += (char)value;

            //value = rd.Next(33, 38);
            //password += (char)value;

            return password;
        }        
    }
}
