using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace Bat.Common
{
    public class LogManager
    {
        private string m_PrefixName="";
        private string m_FilePath = "";
        public string PrefixName
        {
            get { return m_PrefixName; }
            set { m_PrefixName=value;}
        }
        public string FilePath
        {
            get { return m_FilePath; }
            set { m_FilePath = value; }
        }
        private int Write(string fileName, string logData)
        {
            using (StreamWriter sw = new StreamWriter(fileName, true))
            {
                sw.Write(logData);
            }
            return 0;
        }
        public int WriteMonthly(string logData)
        {
            //string fileName = FilePath+"\\"+PrefixName + DateTime.Now.ToString("yyyyMM") + ".log";
            string fileName = Path.Combine(FilePath,PrefixName + DateTime.Now.ToString("yyyyMM") + ".log");
            return Write(fileName, "###" + DateTime.Now.ToString("dd HH:mm:ss.fff") +"\t"+ logData + "\r\n");
        }
        public int WriteDaily(string logData)
        {
            //string fileName = FilePath + "\\" + PrefixName + DateTime.Now.ToString("yyyyMMdd") + ".log";
            string fileName = Path.Combine(FilePath, PrefixName + DateTime.Now.ToString("yyyyMMdd") + ".log");
            return Write(fileName, "###" + DateTime.Now.ToString("HH:mm:ss.fff") + "\t" + logData + "\r\n");
        }
        //internal int WriteError(string logData)
        //{
        //    string filePath = Path.Combine(FilePath,@"DatabaseTransaction\ErrorLog");
        //    string fileName = Path.Combine(this.GetFileLocation(filePath), PrefixName + DateTime.Now.ToString("yyyyMMdd") + ".log");
        //    return Write(fileName, "###" + DateTime.Now.ToString("HH:mm:ss.fff") + "\t" + logData + "\r\n");
        //}        
        //private string GetFileLocation(string filePath)
        //{
        //    string fileLocation = filePath;            
        //    if (!Directory.Exists(fileLocation))
        //        Directory.CreateDirectory(fileLocation);
        //    return fileLocation;
        //}

        
    }
}
