using System;
using System.Collections;
using System.IO;
using System.Security.Cryptography;

namespace Bat.Common
{
    public class Cryptography
    {
        #region Constructor
        public Cryptography()
        {
        }
        #endregion Constructor

        public IList GetEncryptedDataList(IList ilRealDataList)
        {
            try
            {
                int iIndex = 0;
                int iCharIndex = 0;
                string sNewValue = string.Empty;

                ArrayList arRetVal = new ArrayList();
                
                foreach (string sValue in ilRealDataList)
                {
                    char[] cArray = sValue.ToCharArray();
                    char[] cNewArray = new char[cArray.Length];
                    iCharIndex = 0;
                    sNewValue = string.Empty;

                    foreach (char ch in cArray)
                    {
                        decimal decValue = (decimal)ch;
                        decValue = decValue + 1;
                        cNewArray[iCharIndex] = (char)decValue;
                        sNewValue = sNewValue + ((char)decValue).ToString();
                    }
                    sNewValue="12O91" + sNewValue + "O412";
                    arRetVal.Add(sNewValue);
                    iIndex++;
                }

                return arRetVal;

            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public IList GetDecryptedDataList(IList ilRealDataList)
        {
            try
            {
                int iIndex = 0;
                int iCharIndex = 0;
                string sNewValue = string.Empty;

                ArrayList arRetVal = new ArrayList();

                foreach (string sValue in ilRealDataList)
                {
                    string EncryptedString = sValue.Remove(sValue.Length-4, 4);
                    EncryptedString = sValue.Remove(0, 5);

                    char[] cArray = EncryptedString.ToCharArray();
                    char[] cNewArray = new char[cArray.Length];
                    iCharIndex = 0;
                    sNewValue = string.Empty;

                    foreach (char ch in cArray)
                    {
                        decimal decValue = (decimal)ch;
                        decValue = decValue - 1;
                        cNewArray[iCharIndex] = (char)decValue;
                        //decValue = decValue - 1;
                        //char chN = (char)decValue;
                        sNewValue = sNewValue + ((char)decValue).ToString();
                    }
                    arRetVal.Add(sNewValue);
                    iIndex++;
                }

                return arRetVal;

            }
            catch (Exception exp)
            {
                return null;
            }
        }
        public string GetDecryptedDataList(string sValue)
        {
            try
            {
                int iIndex = 0;
                int iCharIndex = 0;
                string sNewValue = string.Empty;

                string EncryptedString = sValue.Remove(sValue.Length - 4, 4);
                EncryptedString = sValue.Remove(0, 5);

                char[] cArray = EncryptedString.ToCharArray();
                char[] cNewArray = new char[cArray.Length];
                iCharIndex = 0;
                

                foreach (char ch in cArray)
                {
                    decimal decValue = (decimal)ch;
                    decValue = decValue - 1;
                    cNewArray[iCharIndex] = (char)decValue;
                    sNewValue = sNewValue + ((char)decValue).ToString();
                }

                return sNewValue;

            }
            catch (Exception exp)
            {
                return null;
            }
        }
        #region Encrypt and Decrypt Method By Munir
        // [Author("Munirul Islam", "01-August-2009", "Encrypt value")]
        public string Encrypt(string sNewValue)
        {
            char[] cArray = sNewValue.ToCharArray();
            sNewValue = string.Empty;

            foreach (char ch in cArray)
            {
                decimal decValue = (decimal)ch;
                decValue = decValue + 49;
                sNewValue = sNewValue + ((char)decValue).ToString();
            }

            sNewValue = "1x2yO9qn"+sNewValue+"O4MoxOl1";
            return sNewValue;
        }
        // [Author("Munirul Islam", "01-August-2009", "Decrypt value")]
        public string Decrypt(string sNewValue)
        {
            string EncryptedString = sNewValue.Remove(sNewValue.Length - 8);
            EncryptedString = EncryptedString.Remove(0, 8);

            char[] ciArray = EncryptedString.ToCharArray();

            sNewValue = string.Empty;

            foreach (char ch in ciArray)
            {
                decimal decValue = (decimal)ch;
                decValue = decValue - 49;
                sNewValue = sNewValue + ((char)decValue).ToString();
            }
            return sNewValue;
        }
        #endregion Encrypt and Decrypt
        #region All Encryption
        //[Author("A.K.M Zahidul Quaium", "23-May-2009", "Encrypt a string into a string using a password")]
        public string Encrypt(string clearText, string Password)
        {
            byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(clearText);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            byte[] encryptedData = Encrypt(clearBytes, pdb.GetBytes(32), pdb.GetBytes(16));
            
            string sReturnValue=Convert.ToBase64String(encryptedData);
           
            sReturnValue = sReturnValue.Replace("+", "_c_j_");
            sReturnValue = sReturnValue.Replace("%", "_m_p_");
         
            return sReturnValue;
        }

        //  [Author("A.K.M Zahidul Quaium", "23-May-2009", "Encrypt a byte array into a byte array using a key and an IV")]         
        private byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(clearData, 0, clearData.Length);
            cs.Close();
            byte[] encryptedData = ms.ToArray();
            return encryptedData;
        }

        // [Author("A.K.M Zahidul Quaium", "23-May-2009", "Encrypt bytes into bytes using a password")]
        private byte[] Encrypt(byte[] clearData, string Password)
        {
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            return Encrypt(clearData, pdb.GetBytes(32), pdb.GetBytes(16));
        }
        #endregion  All Encryption

        #region All Decryption
        public string Decrypt(string cipherText, string Password)
        {
            string sActualText = cipherText.Replace("_c_j_", "+");
            sActualText = sActualText.Replace("_m_p_", "%");

            byte[] cipherBytes = Convert.FromBase64String(sActualText);
                     
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            byte[] decryptedData = Decrypt(cipherBytes, pdb.GetBytes(32), pdb.GetBytes(16));
            return System.Text.Encoding.Unicode.GetString(decryptedData);
        }

        private byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(cipherData, 0, cipherData.Length);
            cs.Close();
            byte[] decryptedData = ms.ToArray();
            return decryptedData;
        }

        private byte[] Decrypt(byte[] cipherData, string Password)
        {
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            return Decrypt(cipherData, pdb.GetBytes(32), pdb.GetBytes(16));
        }
        #endregion  All Decryption
    }
}