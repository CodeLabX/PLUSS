using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Mail;

namespace Bat.Net.Mail.Smtp
{
    public class SendMail
    {
        private string fromName;
        private string from;        
        private string to;        
        private string cc;        
        private string bcc;
        //private string replyto;
        private string subject;
        private string body;
        private string attachment;
        private string smtpServerName;
        private string smtpUserName;
        private string smtpUserPassword;
        private int smtpServerPort;
        private bool smtpSSL;


        private SmtpClient smtpClient = null;
        private NetworkCredential credential = null;
        /// <summary>
        /// Constructor
        /// </summary>
        public SendMail()
        {
            fromName = string.Empty;
            from = string.Empty;
            to = string.Empty;
            cc = string.Empty;
            bcc = string.Empty;
            attachment = string.Empty;
            smtpServerName = string.Empty;
            smtpUserName = string.Empty;
            smtpUserPassword = string.Empty;
            smtpServerPort = 25;
            smtpSSL = false;
        }
        /// <summary>
        /// The display name that will appear in the recipient mail client
        /// </summary>
        public string FromName
        {
            set
            {
                fromName = value;
            }
            get
            {
                return fromName;
            }
        }
        /// <summary>
        /// The sender name
        /// </summary>
        public string From
        {
            set
            {
                from = value;
            }
            get
            {
                return from;
            }
        }
        
        /// <summary>
        /// The recipient name
        /// </summary>
        public string To
        {
            set
            {
                to = value;
            }
            get
            {
                return to;
            }
        }        

         /// <summary>
         /// The recipient CC Address
         /// </summary>
         public string Cc
         {
             set
             {
                 cc = value;
             }
             get
             {
                 return cc;
             }
         }        
         /// <summary>
         /// The recipient name
         /// </summary>
         public string Bcc
         {
             set
             {
                 bcc = value;
             }
             get
             {
                 return bcc;
             }
         }        

        /// <summary>
        /// The subject of the mail
        /// </summary>
        public string Subject
        {
            set
            {
                subject = value;
            }
            get
            {
                return subject;
            }
        }

        /// <summary>
        /// The body of the mail
        /// </summary>
        public string Body
        {
            set
            {
                body = value;
            }
            get
            {
                return body;
            }
        }
        /// <summary>
        /// Attachment File
        /// </summary>
        public string Attachment
        {
            set
            {
                attachment = value;
            }
            get
            {
                return attachment;
            }
        }
        /// <summary>
        /// SMTP server (name or IP address)
        /// </summary>
        public string SMTPServerName
        {
            set
            {
                smtpServerName = value;
            }
            get
            {
                return smtpServerName;
            }
        }

        /// <summary>
        /// Username needed for a SMTP server that requires authentication
        /// </summary>
        public string SMTPUserName
        {
            set
            {
                smtpUserName = value;
            }
            get
            {
                return smtpUserName;
            }
        }

        /// <summary>
        /// Password needed for a SMTP server that requires authentication
        /// </summary>
        public string SMTPUserPassword
        {
            set
            {
                smtpUserPassword = value;
            }
            get
            {
                return smtpUserPassword;
            }
        }

        /// <summary>
        /// SMTP server port (default 25)
        /// </summary>
        public int SMTPServerPort
        {
            set
            {
                smtpServerPort = value;
            }
            get
            {
                return smtpServerPort;
            }
        }

        /// <summary>
        /// If SMTP server requires SSL
        /// </summary>
        public bool SMTPSSL
        {
            set
            {
                smtpSSL = value;
            }
            get
            {
                return smtpSSL;
            }
        }
        /// <summary>
        /// Reply message for mail
        /// </summary>
        //public string ReplyTo
        //{
        //    set
        //    {
        //        replyto = value;
        //    }
        //    get
        //    {
        //        return replyto;
        //    }
        //}

        public void Send()
        {

            if (this.smtpServerName.Length == 0)
            {
                throw new SendMailException("SMTP Server not specified");
            }
            else
            {
                smtpClient = new SmtpClient(this.SMTPServerName, this.SMTPServerPort);
                //smtpClient.Host = SMTPServerName;
                //smtpClient.Port = SMTPServerPort;                
            }
            if (this.smtpUserName.Length > 0 && this.smtpUserPassword.Length > 0)
            {
                credential = new NetworkCredential(this.SMTPUserName, this.SMTPUserPassword);
                smtpClient.Credentials = credential;
            }
            else
            {
                throw new SendMailException("Authentication fail.");
            }
            smtpClient.EnableSsl = this.SMTPSSL;

            /*MailAddress address = null;
            address = new MailAddress(this.From, this.FromName);
            message.From = address;
            address = new MailAddress(this.To);
            message.Headers = address;           
            //message.Sender = this.MailTo;
            //if(this.CC.Length >0 )  message.CC = this.MailToCC;
            //if (this.BCC.Length > 0) message.Bcc = this.MailToBCC;*/
            MailAddress SendFrom = new MailAddress(this.From, this.FromName);
            //MailAddress SendTo = new MailAddress(this.To);
            MailMessage message = new MailMessage();
            message.From = SendFrom;
            string[] Tos=this.To.Split(new char[] {';',','});
            foreach (string addTo in Tos)
            {
                message.To.Add(addTo);
            }
            if (this.Cc.Length > 0)
            {
                string[] Ccs = this.Cc.Split(new char[] { ';', ',' });
                foreach (string addCc in Ccs)
                {
                    message.CC.Add(addCc);
                }
            }
            if (this.Bcc.Length > 0)
            {
                string[] Bccs = this.Bcc.Split(new char[] { ';', ',' });
                foreach (string addBcc in Bccs)
                {
                    message.Bcc.Add(addBcc);
                }
            }            
            message.Subject = this.Subject;
            message.Body = this.Body;
            //MailAddress ToReply = new MailAddress(this.replyto);
            //message.ReplyTo = ToReply;
            if (this.Attachment.Length > 0)
            {
                string[] Atts = this.Attachment.Split(new char[] { ',' });
                foreach (string att in Atts)
                {
                    message.Attachments.Add(new Attachment(att));
                }
            }
            //message.CC.Add();
            try
            {
                smtpClient.Send(message);                
            }
            catch
            {
                throw new SendMailException("Sending failure");
                
            }

        }
    }
}
