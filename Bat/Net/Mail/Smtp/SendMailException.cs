using System;
using System.Collections.Generic;
using System.Text;

namespace Bat.Net.Mail.Smtp
{
    public class SendMailException : System.Exception
    {
        private string message;
        public SendMailException(string str)
        {
            message = str;
        }
        public string What()
        {
            return message;
        }
    }
}
