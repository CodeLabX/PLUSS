﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DA.aspx.cs" Inherits="PlussAndLoan.DA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form action="{% url 'result' %}">
            <div style="margin:auto; width: 80%;text-align: center;background-color: rgb(201, 214, 253)">
                <h2 style="background-color: black;color: white">Sentiment Analysis of amazon product review</h2>
                <table style="margin-left: 22%">
                    <tr>
                        <td>Review Comments</td>
                        <td><textarea cols="50" rows="7" required name="review"></textarea></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="text-align: left"><input type="submit" name="" value="Predict Review"></td>
                    </tr>
                </table>
            </div>
    </form>
</body>
</html>
