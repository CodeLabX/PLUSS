/*
 *
 * Copyright (c) 2006/2007 Sam Collett (http://www.texotela.co.uk)
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Version 1.0
 * Demo: http://www.texotela.co.uk/code/jquery/numeric/
 *
 * $LastChangedDate$
 * $Rev$
 */
 
/*
 * Allows only valid characters to be entered into input boxes.
 * Note: does not validate that the final text is a valid number
 * (that could be done by another script, or server-side)
 *
 * @name     numeric
 * @param    decimal      Decimal separator (e.g. '.' or ',' - default is '.')
 * @param    callback     A function that runs if the number is not valid (fires onblur)
 * @author   Sam Collett (http://www.texotela.co.uk)
 * @example  $(".numeric").numeric();
 * @example  $(".numeric").numeric(",");
 * @example  $(".numeric").numeric(null, callback);
 *
 */
jQuery.fn.numeric = function(decimal, callback)
{
	decimal = decimal || ".";
	callback = typeof callback == "function" ? callback : function(){};
	this.keypress(
		function(e)
		{
			var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
			// allow enter/return key (only when in an input box)
			if(key == 13 && this.nodeName.toLowerCase() == "input")
			{
				return true;
			}
			else if(key == 13)
			{
				return false;
			}
			var allow = false;
			// allow Ctrl+A
			if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) return true;
			// allow Ctrl+X (cut)
			if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) return true;
			// allow Ctrl+C (copy)
			if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) return true;
			// allow Ctrl+Z (undo)
			if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) return true;
			// allow or deny Ctrl+V (paste), Shift+Ins
			if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */
			|| (e.shiftKey && key == 45)) return true;
			// if a number was not pressed
			if(key < 48 || key > 57)
			{
				/* '-' only allowed at start */
				if(key == 45 && this.value.length == 0) return true;
				//if(key == 43) return true;
				//if(key == 45) return true;
				/* only one decimal separator allowed */
				if(key == decimal.charCodeAt(0) && this.value.indexOf(decimal) != -1)
				{
					allow = false;
				}
				// check for other keys that have special purposes
				if(
					key != 8 /* backspace */ &&
					key != 9 /* tab */ &&
					key != 13 /* enter */ &&
					key != 35 /* end */ &&
					key != 36 /* home */ &&
					key != 37 /* left */ &&
					key != 39 /* right */ &&
					key != 46 /* del */ 
					//key != 44
				)
				{
					allow = false;
				}
				else
				{
					// for detecting special keys (listed above)
					// IE does not support 'charCode' and ignores them in keypress anyway
					if(typeof e.charCode != "undefined")
					{
						// special keys have 'keyCode' and 'which' the same (e.g. backspace)
						if(e.keyCode == e.which && e.which != 0)
						{
							allow = true;
						}
						// or keyCode != 0 and 'charCode'/'which' = 0
						else if(e.keyCode != 0 && e.charCode == 0 && e.which == 0)
						{
							allow = true;
						}
					}
				}
				// if key pressed is the decimal and it is not already in the field
				if(key == decimal.charCodeAt(0) && this.value.indexOf(decimal) == -1)
				{			
				    allow = true;
				}
				// + , - value
				if(key == decimal.charCodeAt(0) || key == decimal.charCodeAt(1) )
				{
				    if(key == 45)	
					{allow = true;}
					else if (key == 43)
					{allow = true;}
				}					
			}
			else
			{
				allow = true;
			}
			return allow;
		}
	)
	.blur(
		function()
		{
			var val = jQuery(this).val();
			if(val != "")
			{
				var re = new RegExp("^\\d+$|\\d*" + decimal + "\\d+");
				if(!re.exec(val))
				{
					callback.apply(this);
				}
			}
		}
	);
	return this;
}

jQuery.fn.numberOnly = function()
{
    this.keypress(
       function(e){
                   var keyCode = window.event.keyCode;
                   //check to see if key pressed was 0 through 9
                   if ( keyCode > 47 && keyCode < 58 )
                      //numeric value so do nothing
                      return;                      
                   else
                      //they tried non-numeric value,
                      window.event.returnValue = null;
                 })
}
jQuery.fn.decimalNumber = function()
{
    decimal = ".";
    minus = "-";
    this.keypress(
       function(e){
                   var keyCode = window.event.keyCode;
                   //Allowed minus sign
                   if(keyCode == minus.charCodeAt(0) && this.value.indexOf(minus) == -1) return true;
                   //Only one decimal seperator allowed
                   if(keyCode == decimal.charCodeAt(0) && this.value.indexOf(decimal) != -1) return false;
                   if(keyCode == decimal.charCodeAt(0) && this.value.indexOf(decimal) == -1) return true;
                   //check to see if key pressed was 0 through 9
                   if ( keyCode > 47 && keyCode < 58 )
                      //numeric value so do nothing
                      return true;                      
                   else
                      //they tried non-numeric value,
                      window.event.returnValue = null;
                 })
}

jQuery.fn.contactNumber = function()
{
    //decimal = ".";
     plus = '+';
    minus = '-';    
    this.keypress(
       function(e){
                   var keyCode = window.event.keyCode;   
                    //string +=  String.fromCharCode(keyCode);
                   //alert(string);
                       // alert( this.value.indexOf(minus));   
                   if(keyCode == minus.charCodeAt(0) && this.value.indexOf(minus) == -1)   return true;
                   if(keyCode == plus.charCodeAt(0) && this.value.indexOf(plus) == -1)   return true;
                                  // alert(this.value.indexOf(this.value));
                                 // alert(string.indexOf('+',1));
                   //if(keyCode == 43 && string.indexOf(this.value) == 0 ) return true;
                   //if(keyCode == 43 && string.indexOf(this.value) == -1) return false;
                   
                   //Allowed minus sign
                   //if(keyCode == minus.charCodeAt(0) && this.value.indexOf(minus) == -1) window.event.returnValue = null;
                   
                   
                   //if(keyCode == 107 && this.value.indexOf(plus) == -1) return true;
                   
                   //Only one decimal seperator allowed
                   //if(keyCode == decimal.charCodeAt(0) && this.value.indexOf(decimal) != -1) return false;
                   //if(keyCode == decimal.charCodeAt(0) && this.value.indexOf(decimal) == -1) return true;
                   //check to see if key pressed was 0 through 9
                   if ( (keyCode > 47 && keyCode < 58))
                      //numeric value so do nothing
                      return true;                      
                   else
                      //they tried non-numeric value,
                      window.event.returnValue = null;
                 })
}

jQuery.fn.maxLength = function(controlMaxLength)
{
    var valLength;
    this.keypress(
         function(e){
                    valLength = this.value.length;
                    if(valLength == controlMaxLength)
                    return false;
                    else
                    return true;
               })
}

jQuery.fn.ReadOnly = function()
{        
      this.keydown( 
       function(e){                            
                    var keyCode = window.event.keyCode;                                                       
                    if(keyCode > -1)
                    {    
                        window.event.returnValue = null;                                   
                    }                      
                  });
}

jQuery.fn.CursorMoveAvg = function()
{
    this.keydown(
    function(e){
        var keyCode = window.event.keyCode;
        var NameAata=new Array();
        if(keyCode==40 || keyCode==13)
        {
            var objId=this.id;
            NameAata=objId.split('_');
            var num=NameAata[3].substr(3,NameAata[3].length);
            var i=parseInt(num)+1;
            if(i<6)
            {
            var obj=NameAata[0]+"_"+NameAata[1]+"_"+NameAata[2]+"_"+NameAata[3].substr(0,4)+i+"_"+NameAata[4];
            //alert("num="+num +" obj="+ obj +"i="+i);
                document.getElementById(obj).focus();
            }            
        }
        if(keyCode==38)
        {
            var objId=this.id;
            NameAata=objId.split('_');
            var num=NameAata[3].substr(3,NameAata[3].length);
            var i=parseInt(num)-1;
            if(i>1)
            {
            var obj=NameAata[0]+"_"+NameAata[1]+"_"+NameAata[2]+"_"+NameAata[3].substr(0,4)+i+"_"+NameAata[4];
                document.getElementById(obj).focus();
            }            
        }
    }
    );
}
jQuery.fn.CursorMoveCRDLes = function()
{
    this.keydown(
    function(e){
        var keyCode = window.event.keyCode;
        var NameAata=new Array();
        var obj;
        if(keyCode==40 || keyCode==13)
        {
            var objId=this.id;
            NameAata=objId.split('_');
            var num=NameAata[3].substr(3,NameAata[3].length);
            var i=parseInt(num)+1;
            if(num=="08")
            {
                i=9;
            }
            if(num=="09")
            {
                i=10;
            }
            if(i<=9)
            {
            var obj=NameAata[0]+"_"+NameAata[1]+"_"+NameAata[2]+"_"+NameAata[3].substr(0,4)+i+"_"+NameAata[4];
                document.getElementById(obj).focus();
            }
            else
            {
            var obj=NameAata[0]+"_"+NameAata[1]+"_"+NameAata[2]+"_"+NameAata[3].substr(0,3)+i+"_"+NameAata[4];
                document.getElementById(obj).focus();
            }            
        }
        if(keyCode==38)
        {
            var objId=this.id;
            NameAata=objId.split('_');
            var num=NameAata[3].substr(3,NameAata[3].length);
            var i=parseInt(num)-1;
            if(num=="08")
            {
                i=7;
            }
            if(num=="09")
            {
                i=8;
            }
            if(num=="10")
            {
                i=9;
            }
            if(i>1)
            {
                var obj=NameAata[0]+"_"+NameAata[1]+"_"+NameAata[2]+"_"+NameAata[3].substr(0,4)+i+"_"+NameAata[4];
                document.getElementById(obj).focus();
            }            
        }
    }
    );
}
jQuery.fn.CursorMoveCRDGater = function()
{
    this.keydown(
    function(e){
        var keyCode = window.event.keyCode;
        var NameAata=new Array();
        var obj;
        if(keyCode==40 || keyCode==13)
        {
            var objId=this.id;
            NameAata=objId.split('_');
            var num=NameAata[3].substr(3,NameAata[3].length);
            var i=parseInt(num)+1;
            if(i<102)
            {
                var obj=NameAata[0]+"_"+NameAata[1]+"_"+NameAata[2]+"_"+NameAata[3].substr(0,3)+i+"_"+NameAata[4];
                document.getElementById(obj).focus();
            }
        }
        if(keyCode==38)
        {
            var objId=this.id;
            NameAata=objId.split('_');
            var num=NameAata[3].substr(3,NameAata[3].length);
            var i=parseInt(num)-1;
            if(num=="10")
            {
                i=9;
            }
            if(i>9)
            {
                var obj=NameAata[0]+"_"+NameAata[1]+"_"+NameAata[2]+"_"+NameAata[3].substr(0,3)+i+"_"+NameAata[4];
                document.getElementById(obj).focus();
            }
            else
            {
                var obj=NameAata[0]+"_"+NameAata[1]+"_"+NameAata[2]+"_"+NameAata[3].substr(0,4)+i+"_"+NameAata[4];
                document.getElementById(obj).focus();
            }           
        }
    }
    );
}


jQuery.fn.decimalFormat = function()
{
    var fieldValue;
    var  objectv;
             
    this.blur(
         function(e){
           
             var myRegExp = /\./;                 
             fieldValue = this.value; 
             var fvalue = new Array();
             if(fieldValue!="")
             {   
                if(fieldValue.search(myRegExp) > -1)
                {
                    fvalue = fieldValue.split("."); 
                    var tempValue1 = fvalue[1];
                    var temp;
                    if(tempValue1.length==1)
                    {
                    temp = tempValue1+'00';
                    }
                    if(tempValue1.length==2)
                    {
                    temp = tempValue1+'0';
                    } 
                    if(tempValue1.length>2)
                    {
                    temp = tempValue1;
                    }                      
                   var result = fvalue[0]+'.'+temp;
                   var decimalv = result.substr(result.indexOf('.')+1,3);
                   this.value = fvalue[0]+'.'+decimalv;
                }
                else
                this.value = fieldValue+'.000';
             }
                   
                   
     });
}
