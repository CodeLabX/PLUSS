﻿function CalculateNetAllowed()
        {
            var value1 = document.getElementById('ctl00_ContentPlaceHolder_bilGrdMueTextBox').value;
            var value2 = document.getElementById('ctl00_ContentPlaceHolder_creditCrdTextBox').value;
            var value3;            
                value3 = parseFloat('0' + value1) + parseFloat('0' + value2);
                document.getElementById('ctl00_ContentPlaceHolder_netAllowedTextBox').value = value3;
           CalculateNetUnsecured();
        }
        
function CalculateNetUnsecured()
        {
            var value1 = document.getElementById('ctl00_ContentPlaceHolder_bilGrdMueTextBox').value;
            var value2 = document.getElementById('ctl00_ContentPlaceHolder_creditCrdTextBox').value;   
            var value3 = document.getElementById('ctl00_ContentPlaceHolder_netAllowedTextBox').value;
            var value6 = document.getElementById('ctl00_ContentPlaceHolder_unsecuredBillTextBox').value;
            var value7 = document.getElementById('ctl00_ContentPlaceHolder_unsecuredOtherTextBox').value;
            var value8 = document.getElementById('ctl00_ContentPlaceHolder_creditCrd2TextBox').value;
            var value9;
            var value10;
                value9 = parseFloat('0' + value6) + parseFloat('0' + value7) + parseFloat('0' + value8);
                document.getElementById('ctl00_ContentPlaceHolder_netUnsecuredTextBox').value = value9;
        
            if(parseFloat('0' + value2) < parseFloat('0' + value8))
            {
                value10 = parseFloat('0' + value3) - parseFloat('0' + value9);
            }
            else
            {
                value10 = parseFloat('0' + value1) - (parseFloat('0' + value6) + parseFloat('0' +value7));
            }   
             document.getElementById('ctl00_ContentPlaceHolder_availableTextBox').value = value10;        
        }
        
       
$(function(){
            $('#ctl00_ContentPlaceHolder_llIdTextBox').numberOnly();  
            $('#ctl00_ContentPlaceHolder_bilGrdMueTextBox,#ctl00_ContentPlaceHolder_creditCrdTextBox,#ctl00_ContentPlaceHolder_netAllowedTextBox,#ctl00_ContentPlaceHolder_existingBilOSTextBox,#ctl00_ContentPlaceHolder_fdValueTextBox').decimalNumber();
            $('#ctl00_ContentPlaceHolder_unsecuredBillTextBox,#ctl00_ContentPlaceHolder_unsecuredOtherTextBox,#ctl00_ContentPlaceHolder_creditCrd2TextBox,#ctl00_ContentPlaceHolder_netUnsecuredTextBox,#ctl00_ContentPlaceHolder_availableTextBox').decimalNumber(); 
                     
         });
        
$(function(){
            for(var i = 1; i<=6 ; i++)
            {
                $('#ctl00_ContentPlaceHolder_facExpsure'+i+'TextBox,#ctl00_ContentPlaceHolder_facSecrty'+i+'TextBox,#ctl00_ContentPlaceHolder_facNar'+i+'TextBox').decimalNumber();
            }
            $('#ctl00_ContentPlaceHolder_plExprTextBox,#ctl00_ContentPlaceHolder_plSecrtyTextBox,#ctl00_ContentPlaceHolder_plNarTextBox,#ctl00_ContentPlaceHolder_crdtCrdExpTextBox,#ctl00_ContentPlaceHolder_crdtCrdSecrtyTextBox,#ctl00_ContentPlaceHolder_crdtCrdNarTextBox').decimalNumber();
            $('#ctl00_ContentPlaceHolder_bilExpTextBox,#ctl00_ContentPlaceHolder_bilSecrtyTextBox,#ctl00_ContentPlaceHolder_bilNarTextBox,#ctl00_ContentPlaceHolder_flexiExpTextBox,#ctl00_ContentPlaceHolder_flexiSecrtyTextBox,#ctl00_ContentPlaceHolder_flexiNarTextBox').decimalNumber();
            $('#ctl00_ContentPlaceHolder_otherDirCrditcrdExpTextBox,#ctl00_ContentPlaceHolder_otherDirCrditcrdSecrtyTextBox,#ctl00_ContentPlaceHolder_otherDirCrditcrdNarTextBox,#ctl00_ContentPlaceHolder_netExpTextBox,#ctl00_ContentPlaceHolder_netSecrtyTextBox,#ctl00_ContentPlaceHolder_netNarTextBox').decimalNumber();
         });

function CalculateNar(controlId1, controlId2, controlId3)
            {
                var ctrlValue1 = document.getElementById(controlId1).value;
                var ctrlValue2 = document.getElementById(controlId2).value;
                var result;
                if(ctrlValue1 < 0)
                {
                    ctrlValue1 = ctrlValue1;
                }
                else
                {
                    ctrlValue1 = parseFloat('0' + ctrlValue1);
                }
                if(ctrlValue2 < 0)
                {
                    ctrlValue2 = ctrlValue2;
                }
                else
                {
                    ctrlValue2 = parseFloat('0' + ctrlValue2);
                }
                    result = parseFloat(ctrlValue1) - parseFloat(ctrlValue2);
                    document.getElementById(controlId3).value = result;
                    CalculateNetExposure();
                    CalculateNetSecurity();
                    CalculateNetNar();
            }

function CalculateNetExposure()
            {
                var result = 0;
                var plExValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_plExprTextBox').value);
                var cCExValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_crdtCrdExpTextBox').value);
                var bilExValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_bilExpTextBox').value);
                var flexiExValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_flexiExpTextBox').value);
                var otherCCExValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_otherDirCrditcrdExpTextBox').value);
                var result2 = plExValue + cCExValue + bilExValue + flexiExValue + otherCCExValue;  
                for(var i = 1; i<=6 ; i++)
                    {
                        var value;                                       
                        var control;
                        control = 'ctl00_ContentPlaceHolder_facExpsure' + i + 'TextBox';
                        value = 'value' + i;
                        value = parseFloat('0' + document.getElementById(control).value);
                        result = parseFloat('0' + result) + value;
                    } 
                 //----------------------------------------------------------
                 var BilOS = document.getElementById('ctl00_ContentPlaceHolder_bilOSHiddenField').value;
                 if(bilExValue>0)
                 {
                    document.getElementById('ctl00_ContentPlaceHolder_existingBilOSTextBox').value = parseFloat('0'+BilOS) + parseFloat(bilExValue);
                 }
                 else
                 {
                    document.getElementById('ctl00_ContentPlaceHolder_existingBilOSTextBox').value=BilOS;
                 }
                 document.getElementById('ctl00_ContentPlaceHolder_unsecuredBillTextBox').value = (parseFloat(document.getElementById('ctl00_ContentPlaceHolder_existingBilOSTextBox').value) - parseFloat(document.getElementById('ctl00_ContentPlaceHolder_fdValueTextBox').value));
                 var otherSecurty = document.getElementById('ctl00_ContentPlaceHolder_unsecureOtherHiddenField').value; 
                 if(flexiExValue>0)
                 {
                    document.getElementById('ctl00_ContentPlaceHolder_unsecuredOtherTextBox').value = parseFloat('0'+otherSecurty) + parseFloat('0'+ flexiExValue);
                 }
                 else
                 {
                    document.getElementById('ctl00_ContentPlaceHolder_unsecuredOtherTextBox').value =otherSecurty;
                 }
                 var creditcard = document.getElementById('ctl00_ContentPlaceHolder_creditCardHiddenField').value;  
                 if(otherCCExValue>0)
                 {
                    document.getElementById('ctl00_ContentPlaceHolder_creditCrd2TextBox').value=parseFloat('0'+creditcard) + parseFloat('0'+ otherCCExValue);
                    document.getElementById('ctl00_ContentPlaceHolder_crdtCrdExpTextBox').value=parseFloat('0'+creditcard) + parseFloat('0'+ otherCCExValue);  
                    document.getElementById('ctl00_ContentPlaceHolder_crdtCrdNarTextBox').value=parseFloat('0'+creditcard) + parseFloat('0'+ otherCCExValue);  
                    
                 }
                 else
                 {
                    document.getElementById('ctl00_ContentPlaceHolder_creditCrd2TextBox').value=creditcard;
                    document.getElementById('ctl00_ContentPlaceHolder_crdtCrdExpTextBox').value=creditcard;
                    document.getElementById('ctl00_ContentPlaceHolder_crdtCrdNarTextBox').value=creditcard;
                 }
                 CalculateNetUnsecured();
                 CalculateNetAllowed();
                 //---------------------------------------------------
                result = parseFloat('0' + result) + parseFloat('0' + result2);
                document.getElementById('ctl00_ContentPlaceHolder_netExpTextBox').value = result;
            }
            
function CalculateNetSecurity()
            {
                var result = 0;
                var plSrtyValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_plSecrtyTextBox').value);
                var cCSrtyValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_crdtCrdSecrtyTextBox').value);
                var bilSrtyValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_bilSecrtyTextBox').value);
                var flexiSrtyValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_flexiSecrtyTextBox').value);
                var otherCCSrtyValue = parseFloat('0' + document.getElementById('ctl00_ContentPlaceHolder_otherDirCrditcrdSecrtyTextBox').value);
                var result2 = plSrtyValue + cCSrtyValue + bilSrtyValue + flexiSrtyValue + otherCCSrtyValue;  
                for(var i = 1; i<=6 ; i++)
                    {
                        var value;                                       
                        var control;
                        control = 'ctl00_ContentPlaceHolder_facSecrty' + i + 'TextBox';
                        value = 'value' + i;                        
                        value = parseFloat('0' + document.getElementById(control).value);
                        result = parseFloat('0' + result) + value;
                    } 
                result = parseFloat('0' + result) + parseFloat('0' + result2);
                document.getElementById('ctl00_ContentPlaceHolder_netSecrtyTextBox').value = result;
            }
 
 function CalculateNetNar()
            {
                var netExposure = document.getElementById('ctl00_ContentPlaceHolder_netExpTextBox').value;
                var netSec = document.getElementById('ctl00_ContentPlaceHolder_netSecrtyTextBox').value;
                var result = parseFloat('0' + netExposure) - parseFloat('0' + netSec)
                document.getElementById('ctl00_ContentPlaceHolder_netNarTextBox').value = result;
            }
 
 function CalculateNarLoad()
             {
                var fEx1 = document.getElementById('ctl00_ContentPlaceHolder_facExpsure1TextBox').value;
                var fSec1 = document.getElementById('ctl00_ContentPlaceHolder_facSecrty1TextBox').value;
                //var fNar1 = document.getElementById('ctl00_ContentPlaceHolder_facNar1TextBox').value;
                var rNar1 = parseFloat('0'+fEx1) - parseFloat('0'+fSec1);
                document.getElementById('ctl00_ContentPlaceHolder_facNar1TextBox').value = rNar1;
                
                var fEx2 = document.getElementById('ctl00_ContentPlaceHolder_facExpsure2TextBox').value;
                var fSec2 = document.getElementById('ctl00_ContentPlaceHolder_facSecrty2TextBox').value;
               //var fNar2 = document.getElementById('ctl00_ContentPlaceHolder_facNar2TextBox').value;
                var rNar2 = parseFloat('0'+fEx2) - parseFloat('0'+fSec2);
                document.getElementById('ctl00_ContentPlaceHolder_facNar2TextBox').value = rNar2;
                
                var fEx3 = document.getElementById('ctl00_ContentPlaceHolder_facExpsure3TextBox').value;
                var fSec3 = document.getElementById('ctl00_ContentPlaceHolder_facSecrty3TextBox').value;
                 //var fNar3 = document.getElementById('ctl00_ContentPlaceHolder_facNar3TextBox').value;
                 var rNar3 = parseFloat('0'+fEx3) - parseFloat('0'+fSec3);
                document.getElementById('ctl00_ContentPlaceHolder_facNar3TextBox').value =rNar3;
                
                var fEx4 = document.getElementById('ctl00_ContentPlaceHolder_facExpsure4TextBox').value;
                var fSec4 = document.getElementById('ctl00_ContentPlaceHolder_facSecrty4TextBox').value;
                 //var fNar4= document.getElementById('ctl00_ContentPlaceHolder_facNar4TextBox').value;
                 var rNar4 = parseFloat('0'+fEx4) - parseFloat('0'+fSec4);
                document.getElementById('ctl00_ContentPlaceHolder_facNar4TextBox').value =rNar4;
                
                var fEx5 = document.getElementById('ctl00_ContentPlaceHolder_facExpsure5TextBox').value;
                var fSec5 = document.getElementById('ctl00_ContentPlaceHolder_facSecrty5TextBox').value;
                 //var fNar5 = document.getElementById('ctl00_ContentPlaceHolder_facNar5TextBox').value;
                 var rNar5 = parseFloat('0'+fEx5) - parseFloat('0'+fSec5);
                document.getElementById('ctl00_ContentPlaceHolder_facNar5TextBox').value =rNar5;
                
                var fEx6 = document.getElementById('ctl00_ContentPlaceHolder_facExpsure6TextBox').value;
                var fSec6 = document.getElementById('ctl00_ContentPlaceHolder_facSecrty6TextBox').value;
                 //var fNar6 = document.getElementById('ctl00_ContentPlaceHolder_facNar6TextBox').value;
                 var rNar6 = parseFloat('0'+fEx6) - parseFloat('0'+fSec6);
                 document.getElementById('ctl00_ContentPlaceHolder_facNar6TextBox').value = rNar6;    
                 
                 var plEx =  document.getElementById('ctl00_ContentPlaceHolder_plExprTextBox').value;            
                 var plSec =  document.getElementById('ctl00_ContentPlaceHolder_plSecrtyTextBox').value; 
                 var rPl = parseFloat('0'+plEx) - parseFloat('0'+plSec); 
                 document.getElementById('ctl00_ContentPlaceHolder_plNarTextBox').value = rPl;
                 
                  var crdEx =  document.getElementById('ctl00_ContentPlaceHolder_crdtCrdExpTextBox').value;            
                 var crdSec =  document.getElementById('ctl00_ContentPlaceHolder_crdtCrdSecrtyTextBox').value; 
                 var rCrd = parseFloat('0'+crdEx) - parseFloat('0'+crdSec); 
                 document.getElementById('ctl00_ContentPlaceHolder_crdtCrdNarTextBox').value = rCrd;
                 
                  var bilEx =  document.getElementById('ctl00_ContentPlaceHolder_bilExpTextBox').value;            
                 var bilSec =  document.getElementById('ctl00_ContentPlaceHolder_bilSecrtyTextBox').value; 
                 var rbil = parseFloat('0'+bilEx) - parseFloat('0'+bilSec); 
                 document.getElementById('ctl00_ContentPlaceHolder_bilNarTextBox').value = rbil;
                 var flEx =  document.getElementById('ctl00_ContentPlaceHolder_flexiExpTextBox').value;            
                 var flSec =  document.getElementById('ctl00_ContentPlaceHolder_flexiSecrtyTextBox').value; 
                 var rFl = parseFloat('0'+flEx) - parseFloat('0'+flSec); 
                 document.getElementById('ctl00_ContentPlaceHolder_flexiNarTextBox').value = rFl;
                 
                  var otEx =  document.getElementById('ctl00_ContentPlaceHolder_otherDirCrditcrdExpTextBox').value;            
                 var otSec =  document.getElementById('ctl00_ContentPlaceHolder_otherDirCrditcrdSecrtyTextBox').value; 
                 var rot = parseFloat('0'+otEx) - parseFloat('0'+otSec); 
                 document.getElementById('ctl00_ContentPlaceHolder_otherDirCrditcrdNarTextBox').value = rot;               
                                                   
             }
             
             function MobDropDownSelect()
             {
                var mobValue = document.getElementById('ctl00_ContentPlaceHolder_mob12DropDownList');                
                if(mobValue.selectedIndex == 0)
                {
                document.getElementById('ctl00_ContentPlaceHolder_bilGrdMueTextBox').value = '4900000';
                }
                else 
                {
                document.getElementById('ctl00_ContentPlaceHolder_bilGrdMueTextBox').value = '3500000';
                }
                CalculateNetAllowed();
             }