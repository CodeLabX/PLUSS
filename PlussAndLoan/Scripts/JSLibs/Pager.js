var Pager = Class.create({
    initialize: function(pageSize, onChange, ctrls) {
        this.pageSize = pageSize || 1;
        this.onChange = onChange || Prototype.emptyFunction;
        this.totalPage = 1;
        this.pageNo = 0;

        if (ctrls) {
            this.txtPageNo = ctrls.txtPageNo;
            this.nextPage = ctrls.nextPage;
            this.prevPage = ctrls.prevPage;
            this.spnTotalPage = ctrls.spnTotalPage;
            this.pager = ctrls.pager;
        } else {
            this.txtPageNo = $D.txtPageNo;
            this.nextPage = $D.nextPage;
            this.prevPage = $D.prevPage;
            this.spnTotalPage = $D.spnTotalPage;
            this.pager = $D.pager;
        }

        var go = function(event) {
            var v = $F(this.txtPageNo).toInt();
            if (v < 1 || isNaN(v))
                this.pageNo = 0;
            else if (v >= this.totalPage)
                this.pageNo = this.totalPage - 1;
            else
                this.pageNo = v - 1;

            this._setValues(event);

        } .bind(this);

        this.txtPageNo.insert({ after:
			$($a({ href: 'javascript:;',
			    className: 'go',
			    title: 'Go'
			}))
			.observe('click', go)
        });

        this.txtPageNo.observe('keypress', util.isNumericKey);
        this.txtPageNo.observe('keypress', function(event) {
            var key = event.keyCode || event.charCode;
            if (key == Event.KEY_RETURN)
                go(event);
        });



        this.nextPage.observe('click', function(event) {
            if (this.nextPage.disabled) return;
            this.pageNo++;
            this._setValues(event);
        } .bind(this));

        this.prevPage.observe('click', function(event) {
            if (this.prevPage.disabled) return;
            this.pageNo--;
            this._setValues(event);
        } .bind(this));

        //this.update(888||this.pageSize);
        this.hide();

    },

    _setValues: function(event) {
        this.txtPageNo.value = this.pageNo + 1;
        this.onChange(this.pageNo);
        this.update(this.rowCount);
        Event.stop(event);
    },

    update: function(rowCount) {

        this.rowCount = rowCount || 0;

        this.totalPage = Math.ceil(this.rowCount / this.pageSize);

        this.pageNo = Math.min(this.totalPage - 1, this.pageNo);
        if ((this.pageNo <= 0) && (this.totalPage <= 1)) {
            this.pager.hide();
            return;
        }

        this.pager.show();
        util.toggleLink(this.nextPage, false);
        util.toggleLink(this.prevPage, false);

        this.spnTotalPage.innerHTML = 'of ' + this.totalPage;
        this.txtPageNo.value = this.pageNo + 1;

        if ((this.pageNo + 1) == this.totalPage) {
            util.toggleLink(this.nextPage, true);
            return;
        }
        if (this.pageNo == 0 && this.totalPage > this.pageNo)
            util.toggleLink(this.prevPage, true);

    },

    reset: function(rowCount, pageNo) {
        this.pageNo = pageNo;
        this.update(rowCount);
    },

    hide: function() {
        this.pager.hide();
    },

    show: function() {
        this.pager.show();
    }
});