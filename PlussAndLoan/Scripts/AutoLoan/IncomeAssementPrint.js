﻿var AccountType = { 1: 'Company', 2: 'Personal' };
var askingrepayment = { 1: 'PDC', 2: 'SI' };
var dbrLevel = { 1: '50', 2: '55', 3: '60' };
var reasonOfJoint = { 'Income Consideration': '1', 'Joint Registration': '2', 'Business is in Joint Name': '3', 'Customer is Mariner': '4', 'AC is in Joint Name': '5', 'Others': '6' };
var vehicleType = { 'Sedan': 'S1', 'Station wagon': 'S2', 'SUV': 'S3', 'Pick-Ups': 'S4', 'Microbus': 'S5', 'MPV': 'S6' };
var Page = {
    //Start Global Veriable
    vehicleStatusArray: [],
    manufactureArray: [],
    vendoreArray: [],
    bankLinkOption: "",
    facilityArray: [],
    sectorLinkOption: [],
    cibInfo: [],
    segmentArray: [],
    artaPreference: [],
    ownDamageArray: [],
    userArray: [],
    sourceForGeneralArray: [],
    incomeAssementArray: [],
    incomeAssesmentLinkOption: "",
    modelArray: [],
    vehicleId: 0,
    bankIdforotherBranch: -1,
    vehicleDetailsArray: null,

    age: 0,
    jointage: 0,
    tenor: 0,
    otherBankAccountForrepayment: [],
    deviationArray: [],
    tabArray: [],
    employerForSalariedArray: [],
    subsectorArray: [],
    bussinessManTabArray: [],
    selfEmployedTabArray: [],
    facilityListArray: [],
    jtCount: 0,
    prCount: 0,
    bankStatementArray: [],
    isAverageBalancePrimaryArray: [],
    isAverageBalanceJointArray: [],
    newIncomeArrayforAll: [],
    incomeArrayforlc: [],

    primaryProfessionId: 0,
    otherProfessionId: 0,
    branchNameForSource: -1,
    jointPrimaryProfessionId: 0,
    subsectorForLcCashCoveredArray: [],
    isjoint: false,
    //End Global Veriable


    init: function() {
        util.prepareDom();
        
        
//        incomeAssmentManager.getManufacturer();
//        incomeAssmentManager.getVendor();
//        incomeAssmentManager.GetAllAutoBranch();
        incomeAssmentManager.getBank();
        incomeAssmentManager.getFacility();
        incomeAssmentManager.getSector();
//        incomeAssmentManager.getCibInfo();
//        //analyzeLoanApplicationManager.getIncomeAssement();

        incomeAssmentManager.GetEmployeSegment();
//        incomeAssmentManager.getGeneralInsurance();
//        incomeAssmentManager.renderOtherDamage();
//        incomeAssmentManager.GetUserData();
//        incomeAssmentManager.GetDeviationLevel();
//        incomeAssmentManager.GetRemarksStrengthCondition();

//        //End Page Load Function
        incomeAssmentManager.GetDataByLLID();
    }

};
var incomeAssmentManager = {

    getSector: function() {

        var res = PlussAuto.IncomeAssementPrint.GetSector();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateSectorCombo(res);
    },
    
    GetEmployeSegment: function() {
        var res = PlussAuto.IncomeAssementPrint.GetEmployeSegment();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateSegmentCombo(res);
    },
    //Get All Data By LLID
    GetDataByLLID: function() {
        
        var res = PlussAuto.IncomeAssementPrint.GetDataByLLID();
        if (res.error) {
            alert(res.error.Message);
            return;
        }

        if (res.value == "No Data found.") {
            alert(res.value);
            var url = "../../UI/AutoLoan/AnalystSummary.aspx";
            location.href = url;
        }
        else {

            
            incomeAssmentHelper.populateDataByLLId(res);
        }
    },
    
    GetEmployerForSalaried: function(accountType) {
        var res = PlussAuto.IncomeAssementPrint.GetEmployerForSalaried();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateEmployerForSalaried(res, accountType);
    },
    
    getIncomeAssement: function() {
        var res = PlussAuto.IncomeAssementPrint.GetIncomeAssement();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateIncomeAssementCombo(res);
    },
    
    GetSubSectorForLandLord: function(sectorId, accountType) {
        var res = PlussAuto.IncomeAssementPrint.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateSubSectorComboForLandLord(res, accountType);
    },
    
    changeSectorForLcCashCovered: function(sectorId) {
        var res = PlussAuto.IncomeAssementPrint.GetSubSector(sectorId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateSubSectorComboLcCashCovered(res);
    },
    
    getFacility: function() {

        var res = PlussAuto.IncomeAssementPrint.getFacility();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateFacilityCombo(res);
    },
    
    getBank: function() {

        var res = PlussAuto.IncomeAssementPrint.GetBank(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateBankCombo(res);
    },
    
    GetBranchNameForIncomeGenerator: function(bankIdElement, elementId) {

        var bankId = $F(bankIdElement);
        if (bankId == 0) {
        }
        else {
            var res = PlussAuto.IncomeAssementPrint.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            incomeAssmentHelper.populateBranchForIncomeGeneratorCombo(res, elementId);
        }

    },
    
    changeSectorForSelfEmployed: function(bankNo, accountType) {
        var sectorId = 0;

        if (accountType == "Pr") {
            sectorId = $F("cmbIndustryForSEPrBank" + bankNo);
        }
        else if (accountType == "Jt") {
            sectorId = $F("cmbIndustryForSEJtBank" + bankNo);
        }

        var res = PlussAuto.IncomeAssementPrint.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateSubSectorComboForSelfEmployed(res, bankNo, accountType);
        incomeAssmentHelper.changePrivateInCome(bankNo, accountType, "Se");
        incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'SE');
    },
    
    changeSector: function(bankNo, accountType) {
        var sectorId = 0;
        if (accountType == "Pr") {
            sectorId = $F("cmbIndustryForPrimaryBank" + bankNo);
        }
        else if (accountType == "Jt") {
            sectorId = $F("cmbIndustryForJointBank" + bankNo);
        }

        var res = PlussAuto.IncomeAssementPrint.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        incomeAssmentHelper.populateSubSectorCombo(res, bankNo, accountType);
        incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'BM');
    },
    
    GetBranchNameForIncomeGeneratorJoint: function(bankId, elementId) {

        //var bankId = $F(bankIdElement);
        if (bankId == 0) {
        }
        else {
            var res = PlussAuto.IncomeAssementPrint.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            incomeAssmentHelper.populateBranchForIncomeGeneratorCombo(res, elementId);
        }

    },
    
    
    
    
};
var incomeAssmentHelper = {
    populateDataByLLId: function(res) {

        incomeAssmentHelper.populatePrimaryApplicantData(res);
        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        if (isJoint == true) {
            $('divForJointApplicant').style.display = 'block';
            //Populate Joint Applicant Data
            $("lblTotaljointIncome").innerHTML = "TOTAL JOINT INCOME:";
            Page.isjoint = true;
            Page.jointPrimaryProfessionId = res.value.objAutoJTProfession.PrimaryProfession;

            incomeAssmentHelper.populateJointApplicantData(res);
        }
        else {
            $("lblTotaljointIncome").innerHTML = "TOTAL INCOME:";
            $('divForJointApplicant').style.display = 'none';
        }

        //Age Validation
        var currentAgeForPrimary = parseInt($F("txtCurrenAge"));
        Page.age = currentAgeForPrimary;
        var currentAgeForJoin = parseInt($F("txtJtCurrntAge"));
        Page.jointage = currentAgeForJoin;
        $("lblFieldrequiredforPrAge").innerHTML = "";
        if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && currentAgeForJoin < 65 && isJoint == true) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
        }
        if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && isJoint == false) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
        }
        if (currentAgeForJoin > 65 && currentAgeForJoin < 69 && currentAgeForPrimary < 65 && isJoint == true) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
        }
        if ((currentAgeForPrimary > 68 || currentAgeForJoin > 68) && isJoint == true) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 3 required";
        }
        if (currentAgeForPrimary > 68 && isJoint == false) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 3 required";
        }
        Page.jtCount = res.value.AutoJTBankStatementList.length;
        Page.prCount = res.value.AutoPRBankStatementList.length;

        var primaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var otherProfession = res.value.objAutoPRProfession.OtherProfession;

        Page.primaryProfessionId = primaryProfession;
        Page.otherProfessionId = otherProfession;

        incomeAssmentHelper.CreateTabbyType(res);

        incomeAssmentHelper.populateSalaried(res);

        incomeAssmentHelper.populateLandLord(res);

        incomeAssmentHelper.populateFacilityOnScb(res);

        incomeAssmentHelper.populateSequrityOnScb(res);

        incomeAssmentHelper.populateFacilityOffScb(res);

        incomeAssmentHelper.createExistingrepaymentGrid();

        Page.bankStatementArray = res.value.AutoBankStatementAvgTotalList;

        incomeAssmentHelper.populateBankStatementData(res);

        incomeAssmentHelper.populateIncomBankStatementSum(res);

        incomeAssmentHelper.populateSegmentApplicantIncomelist(res);

        incomeAssmentHelper.populateSegmentIncome(res);

        incomeAssmentHelper.populateTotalLoanAmount(res);

        incomeAssmentHelper.populateInsurenceTotal(res);

        incomeAssmentHelper.populateAnalystLoanAmountTotal(res);

        incomeAssmentHelper.populateAnalystLoanInterestRate(res);

        incomeAssmentHelper.populateAnalystLoanTenorRate(res);

        incomeAssmentHelper.populateAnalystRepaymentCapability(res);

        incomeAssmentHelper.changeCashSecured();

        incomeAssmentHelper.disableAllField();


    },

    disableAllField: function() {
        var texts = document.getElementsByTagName('input');
        for (var i_tem = 0; i_tem < texts.length; i_tem++) {
            if (texts[i_tem].type == 'text') {
                $(texts[i_tem].id).disabled = 'false';
            }
        }

        var opts = document.getElementsByTagName('Select');
        for (var i_tem = 0; i_tem < opts.length; i_tem++) {
            //if (texts[i_tem].type == 'text') {

            $(opts[i_tem].id).disabled = 'false';
            //}
        }
        var arae = document.getElementsByTagName('textarea');
        for (var i_tem = 0; i_tem < arae.length; i_tem++) {
            //if (texts[i_tem].type == 'text') {

            $(arae[i_tem].id).disabled = 'false';
            //}
        }
    },

    populatePrimaryApplicantData: function(res) {

        if (res.value.AutoLoanAnalystApplication.CashSecured100Per == false) {
            $('cmbCashSecured').setValue('2');
        }
        else {
            $('cmbCashSecured').setValue('1');
        }
        
        $('txtPrimaryApplicant').setValue(res.value.objAutoPRApplicant.PrName);
        $('txtDateOfBirth').setValue(res.value.objAutoPRApplicant.DateofBirth.format());
        $('txtCurrenAge').setValue(res.value.objAutoPRApplicant.Age);
        $('txtPrimaryProfession').setValue(res.value.objAutoPRProfession.PrimaryProfessionName);
        $('txtOtherProfession').setValue(res.value.objAutoPRProfession.OtherProfessionName);
        $('txtDeclaredIncome').setValue(res.value.objAutoPRFinance.DPIAmount);
        $('txtDeclaredIncomeForLcAll').setValue(res.value.objAutoPRFinance.DPIAmount);
        $('txtVerifiedAddress').setValue(res.value.objAutoPRApplicant.ResidenceAddress);
    },

    populateJointApplicantData: function(res) {

        $('txtJointApplicant').setValue(res.value.objAutoJTApplicant.JtName);
        $('txtJtDateOfBirth').setValue(res.value.objAutoJTApplicant.DateofBirth.format());
        $('txtJtCurrntAge').setValue(res.value.objAutoJTApplicant.Age);
        $('txtJtProfession').setValue(res.value.objAutoJTProfession.JtPrimaryProfessionName);
        $('txtJtDeclaredIncome').setValue(res.value.objAutoJTFinance.DPIAmount);
        $('txtDeclaredIncomeForLcJointAll').setValue(res.value.objAutoJTFinance.DPIAmount);
        $('txtJtProfessionId').setValue(res.value.objAutoJTProfession.PrimaryProfession);
        $('analystMasterIdHiddenField').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);
    },

    CreateTabbyType: function(res) {

        var primaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var otherProfession = res.value.objAutoPRProfession.OtherProfession;
        var jointProfession = res.value.objAutoJTProfession.PrimaryProfession;
        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        $('cmbReasonofJointApplicant').setValue(reasonOfJoint[res.value.AutoLoanAnalystMaster.ReasonOfJoint]);
        var primaryProfessionLink = incomeAssmentHelper.CreateTabbyProfession(primaryProfession, "Pr", res, 1);
        var otherProfessionLink = incomeAssmentHelper.CreateTabbyProfession(otherProfession, "Pr", res, 2);
        var primaryProfessionLinkForJoint = "";
        var jtTab = "";
        var jtDiv = "";
        var jtActive = "";
        Page.tabArray = [];

        if (isJoint == true) {
            primaryProfessionLinkForJoint = incomeAssmentHelper.CreateTabbyProfession(jointProfession, "Jt", res, 3);
            var jtLink = primaryProfessionLinkForJoint.split('^');
            jtTab = jtLink[0];
            jtDiv = jtLink[1];
            jtActive = jtLink[2];
            var obj = new Object();
            obj.PrType = 2;
            obj.PrTab = jtLink[2];
            obj.TabLink = jtLink[3]
            Page.tabArray.push(obj);

        }

        var prLink = primaryProfessionLink.split('^');
        var othLink = otherProfessionLink.split('^');
        var objPr = new Object();
        objPr.PrType = 1;
        objPr.PrTab = prLink[2];
        objPr.TabLink = prLink[3]
        if (objPr.PrTab != "") {
            Page.tabArray.push(objPr);
        }

        var objOth = new Object();
        objOth.PrType = 1;
        objOth.PrTab = othLink[2];
        objOth.TabLink = othLink[3]
        if (objOth.PrTab) {
            Page.tabArray.push(objOth);
        }

        var mainTabForIncomeGenerator = "";
        if (jtTab == "") {
            if (othLink[2] == "") {
                //mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold;\"></label><ol class =\"toc\">" + prLink[0] + othLink[0] + "</ol>" + prLink[1] + othLink[1];
                //                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                //                activatables('profs', [prLink[2]]);

                //change Date 07/11/2012 by Rimpo
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + prLink[0] + "</ol>" + prLink[1];
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
            }
            else {
                //mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + prLink[0] + "</ol>" + prLink[1];
                //                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                //                activatables('profs', [prLink[2], othLink[2]]);

                //change Date 07/11/2012 by Rimpo
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold;\"></label><ol class =\"toc\">" + prLink[0] + othLink[0] + "</ol>" + prLink[1] + othLink[1];
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
            }
        }
        else {
            if (prLink[0] == "") {
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + othLink[0] + jtTab + "</ol>" + othLink[1] + jtDiv;
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
            }
            if (othLink[0] == "") {
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + prLink[0] + jtTab + "</ol>" + prLink[1] + jtDiv;
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
            }
            else if (prLink[0] != "" && othLink[0] != "") {
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + prLink[0] + othLink[0] + jtTab + "</ol>" + prLink[1] + othLink[1] + jtDiv;
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
            }

        }

        //Primary

        //Salaried
        if (primaryProfession == 2 || otherProfession == 2) {
            incomeAssmentManager.GetEmployerForSalaried('Pr');
        }

        //Land Lord

        if (primaryProfession == 5 || otherProfession == 5) {
            $("cmbEmployeeForLandLordPr").update(Page.sectorLinkOption);
            if (Page.incomeAssementArray.length == 0) {
                incomeAssmentManager.getIncomeAssement();
            }
            $("cmbIncomeAssementForLandLordPr").update(Page.incomeAssesmentLinkOption);

        }

        //Bussiness Man
        if (primaryProfession == 1 || otherProfession == 1) {
            var htmlForPrimaryBussinessMan = bussManHtmlHelper.prHtmlForBussinessMan(res);
            var htmlPrBussinessMan = htmlForPrimaryBussinessMan.split('^');
            $("divPrBmBankListShow").update(htmlPrBussinessMan[0]);
            var tabName = [];
            for (var i = 0; i < Page.bussinessManTabArray.length; i++) {
                if (Page.bussinessManTabArray[i].AccountType == "Pr") {
                    tabName.push(Page.bussinessManTabArray[i].TabName);
                }
            }
            if (Page.incomeAssementArray.length == 0) {
                incomeAssmentManager.getIncomeAssement();
            }
        }

        //Self Employed
        if (primaryProfession == 3 || otherProfession == 3 || primaryProfession == 4 || otherProfession == 4 || primaryProfession == 6 || otherProfession == 6) {
            var htmlForPrimarySelfemployed = selfEmployedHtmlHelper.prHtmlForSelfEmployed(res);
            var htmlPrSelfEmployed = htmlForPrimarySelfemployed.split('^');
            $("divPrSEBankListShow").update(htmlPrSelfEmployed[0]);
            var tabNamePrSe = [];
            for (var i = 0; i < Page.selfEmployedTabArray.length; i++) {
                if (Page.selfEmployedTabArray[i].AccountType == "Pr") {
                    tabNamePrSe.push(Page.selfEmployedTabArray[i].TabName);
                }
            }
            if (Page.incomeAssementArray.length == 0) {
                incomeAssmentManager.getIncomeAssement();
            }
        }


        //Joint
        if (isJoint == true) {
            if (jointProfession == 1) {
                var htmlForJointBussinessMan = bussManHtmlHelper.jtHtmlForBussinessManTab(res);
                var htmlJtBussinessMan = htmlForJointBussinessMan.split('^');
                $("divJtBmBankListShow").update(htmlJtBussinessMan[0]);
                var tabNameJt = [];
                for (var i = 0; i < Page.bussinessManTabArray.length; i++) {
                    if (Page.bussinessManTabArray[i].AccountType == "Jt") {
                        tabNameJt.push(Page.bussinessManTabArray[i].TabName);
                    }
                }
                if (Page.incomeAssementArray.length == 0) {
                    incomeAssmentManager.getIncomeAssement();
                }
            }
            if (jointProfession == 3 || jointProfession == 4 || jointProfession == 6) {
                var htmlForJointSelfEmployed = selfEmployedHtmlHelper.jtHtmlForSelfEmployed(res);
                var htmlJtSelfEmployed = htmlForJointSelfEmployed.split('^');
                $("divJtSEBankListShow").update(htmlJtSelfEmployed[0]);
                var tabNameJtSe = [];
                for (var i = 0; i < Page.selfEmployedTabArray.length; i++) {
                    if (Page.selfEmployedTabArray[i].AccountType == "Jt") {
                        tabNameJtSe.push(Page.selfEmployedTabArray[i].TabName);
                    }
                }

                if (Page.incomeAssementArray.length == 0) {
                    incomeAssmentManager.getIncomeAssement();
                }
            }
            if (jointProfession == 5) {
                $("cmbEmployeeForLandLordJt").update(Page.sectorLinkOption);
                if (Page.incomeAssementArray.length == 0) {
                    incomeAssmentManager.getIncomeAssement();
                }
                $("cmbIncomeAssementForLandLordJt").update(Page.incomeAssesmentLinkOption);
            }
            if (jointProfession == 2) {
                incomeAssmentManager.GetEmployerForSalaried('Jt');
            }
        }
    },

    CreateTabbyProfession: function(profession, accountType, res, accountNo) {

        var primaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var otherProfession = res.value.objAutoPRProfession.OtherProfession;
        var jointProfession = res.value.objAutoJTProfession.PrimaryProfession;

        var mainTabForIncomeGenerator = "";
        var tabContentForIncomegeneratorStart = "";
        var tabContentFormiddle = "";
        var tabContentForIncomegeneratorEnd = "<div class=\"clear\"></div></div><div class=\"clear\"></div>";
        var tabName = '';
        var tablink = "";

        switch (profession) {
            case 1:

                mainTabForIncomeGenerator = "<li id=\"liTabBussiness\" style=\"*display:inline;\" ><a id=\"lnkbussinessMan\" href=\"#tabBusinessman\"><span>Businessman</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabBusinessman\" style=\"*border:0px;\">";
                tabName = 'tabBusinessman';
                tablink = "lnkbussinessMan";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentForIncomegeneratorStart += "<div id=\"divPApplicantIncomeGenerator\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 98%;padding-top: 15px;background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrBmBankListShow\"></div></div>";
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";
                    }

                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = "<div id=\"divjtApplicantIncomeGeneratorBussinessMan\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;width: 98%;padding-top: 15px;background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT APPLICANT</span></div><br>" +
                                "<div id=\"divJtBmBankListShow\"></div></div>";
                    }
                    else {
                        tabContentForIncomegeneratorStart += "<div id=\"divPApplicantIncomeGenerator\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 98%;padding-top: 15px;background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrBmBankListShow\"></div></div>";
                        tabContentFormiddle = "<div class=\"clear\"></div><div id=\"divjtApplicantIncomeGeneratorBussinessMan\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 98%;padding-top: 15px;background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT APPLICANT</span></div>" +
                                "<div id=\"divJtBmBankListShow\"></div></div>";
                    }
                }
                break;
            case 2:
                mainTabForIncomeGenerator = "<li id=\"liTabSalaried\" style=\"*display:inline;\"><a id=\"lnkSalaried\" href=\"#tabSalaried\"><span>Salaried</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabSalaried\" style=\"*border:0px;\">";
                tabName = 'tabSalaried';
                tablink = "lnkSalaried";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentForIncomegeneratorStart += "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 98%; border-radius: 15px; margin: 0px;\"><span>PRIMARY APPLICANT</span></div>";
                        tabContentFormiddle = salariedHtmlHelper.prhtmlForSalaried();
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }

                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentForIncomegeneratorStart += "<div id=\"divjtApplicantIncomeGeneratorSalaried\"><div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 100%;*width: 100%;_width: 100%; border-radius: 15px; margin: 0px;\"><span>JOINT APPLICANT</span></div>";
                        tabContentFormiddle = salariedHtmlHelper.jthtmlForSalaried() + "</div>";
                    }
                    else {
                        tabContentForIncomegeneratorStart += "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 100%;*width: 100%;_width: 100%; border-radius: 15px; margin: 0px;\"><span>PRIMARY APPLICANT</span></div>";
                        tabContentFormiddle = salariedHtmlHelper.prhtmlForSalaried();
                        tabContentFormiddle += "<div class=\"clear\"></div><div id=\"divjtApplicantIncomeGeneratorSalaried\"><div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 100%;*width: 100%;_width: 100%; border-radius: 15px; margin: 0px;\"><span>JOINT APPLICANT</span></div>";
                        tabContentFormiddle += salariedHtmlHelper.jthtmlForSalaried() + "</div>";
                    }
                }
                break;
            case 3:
                mainTabForIncomeGenerator = "<li id=\"liTabSelfEmployed\" style=\"*display:inline; float:left;\"><a id=\"lnkSelfEmployed\" href=\"#tabSelf-Employed\" ><span>Self-Employed</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabSelf-Employed\" style=\"*border:0px;\">";
                tabName = 'tabSelf-Employed';
                tablink = "lnkSelfEmployed";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 98%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div><br>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }
                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = "<div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                    else {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                        tabContentFormiddle += "<div class=\"clear\"></div><div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                }
                break;
            case 4:
                mainTabForIncomeGenerator = "<li id=\"liTabSelfEmployed\" style=\"*display:inline;\"><a id=\"lnkSelfEmployed\" href=\"#tabSelf-Employed\" ><span>Self-Employed</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabSelf-Employed\" style=\"*border:0px;\">";
                tabName = 'tabSelf-Employed';
                tablink = "lnkSelfEmployed";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }
                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = "<div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                    else {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                        tabContentFormiddle += "<div class=\"clear\"></div><div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";

                    }
                }
                break;
            case 5:
                mainTabForIncomeGenerator = "<li id=\"liTabLandlord\" style=\"*display:inline;\"><a id=\"lnkLandLord\" href=\"#tabLandlord\" ><span>Landlord</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabLandlord\" style=\"*border:0px;\">";
                tabName = 'tabLandlord';
                tablink = "lnkLandLord";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentFormiddle = landLordHtmlHelper.prHtmlForLandLord();
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }
                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = landLordHtmlHelper.jtHtmlForLandLord();
                    }
                    else {
                        tabContentFormiddle = landLordHtmlHelper.prHtmlForLandLord();
                        tabContentFormiddle += "<div class=\"clear\"></div>" + landLordHtmlHelper.jtHtmlForLandLord();
                    }
                }
                break;
            case 6:
                mainTabForIncomeGenerator = "<li id=\"liTabSelfEmployed\" style=\"*display:inline;\"><a id=\"lnkSelfEmployed\" href=\"#tabSelf-Employed\" ><span>Self-Employed</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabSelf-Employed\" style=\"*border:0px;\">";
                tabName = 'tabSelf-Employed';
                tablink = "lnkSelfEmployed";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :98%;padding-top: 15px;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }
                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = "<div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :98%;padding-top: 15px;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                    else {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :98%;padding-top: 15px;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                        tabContentFormiddle += "<div class=\"clear\"></div><div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :98%;padding-top: 15px;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                }
                break;
        }
        var tabContentForIncomegenerator = tabContentForIncomegeneratorStart + tabContentFormiddle + tabContentForIncomegeneratorEnd;
        var result = mainTabForIncomeGenerator + "^" + tabContentForIncomegenerator + "^" + tabName + "^" + tablink;
        return result;

    },

    populateEmployerForSalaried: function(res, accountType) {
        var link = "<option value=\"-1\">Select an income Assement</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].AutoCompanyId + "\">" + res.value[i].CompanyName + "</option>";
        }
        if (accountType == "Pr") {
            $("cmbEmployerForSalariedForPr").update(link);
        }
        else if (accountType == "Jt") {
            $("cmbEmployerForSalariedForJt").update(link);
        }

        Page.employerForSalariedArray = res.value;
    },

    populateIncomeAssementCombo: function(res) {
        var link = "<option value=\"-1\">Select an income Assement</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].INAM_ID + "\">" + res.value[i].INAM_METHOD + "</option>";
        }
        Page.incomeAssesmentLinkOption = link;
        Page.incomeAssementArray = res.value;

    },

    populateSalaried: function(res) {

        if (res.value.objAutoPRProfession.PrimaryProfession == 2 || res.value.objAutoPRProfession.OtherProfession == 2) {
            $('cmbEmployerForSalariedForPr').setValue(res.value.objAutoPRProfession.NameofCompany);
            var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoPRProfession.NameofCompany);
            if (emp) {
                $('txtEmployerCodeForPr').setValue(emp.CompanyCode);
                $('cmbEmployerCategoryForPrSalaried').setValue(emp.CategoryName);
            }
        }
        if (res.value.objAutoJTProfession.PrimaryProfession == 2) {
            $('cmbEmployerForSalariedForJt').setValue(res.value.objAutoJTProfession.NameofCompany);
            var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoJTProfession.NameofCompany);
            if (emp) {
                $('txtEmployerCodeForJt').setValue(emp.CompanyCode);
                $('cmbEmployerCategoryForJtSalaried').setValue(emp.CategoryName);
            }
        }

        for (var i = 0; i < res.value.objAutoIncomeSalariedList.length; i++) {
            //Primary Salaried Applicant
            if (res.value.objAutoIncomeSalariedList[i].AccountType == 1) {
                $('cmbSalaryModeForPr').setValue(res.value.objAutoIncomeSalariedList[i].SalaryMode);
                $('cmbEmployerForSalariedForPr').setValue(res.value.objAutoIncomeSalariedList[i].Employer);
                $('txtRemarksForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].Remarks);

                var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoIncomeSalariedList[i].Employer);
                if (emp) {
                    $('txtEmployerCodeForPr').setValue(emp.CompanyCode);
                    $('cmbEmployerCategoryForPrSalaried').setValue(emp.CategoryName);
                }
                $('txtGrossSalaryForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].GrosSalary);
                $('txtNetSalaryForPr').setValue(res.value.objAutoIncomeSalariedList[i].NetSalary);
                $('cmbVeriableSalaryForPr').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalary);
                if (res.value.objAutoIncomeSalariedList[i].VariableSalary == 1) {
                    $('divVeriableSalaryForPr').style.display = 'block';
                    $('divVariableSalaryCalculationForPr').style.display = 'block';
                    $('txtVeriableSalaryPerForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent);
                    var totalvrAmountForPr = 0;
                    var identity = 0;
                    if (res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist != null) {
                        for (var j = 0; j < res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist.length; j++) {
                            identity = j + 1;
                            $('cmbMonth' + identity + 'ForPrSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Month);
                            $('txtVaiableAmountForMonth' + identity + 'ForPrSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                            totalvrAmountForPr += parseFloat(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                        }
                    }
                    var variableSalaryAmount = parseFloat(parseFloat(totalvrAmountForPr) * parseFloat(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent)) / 100;

                    $('txtTotalVeriableSalaryForPrSalaried').setValue(variableSalaryAmount.toFixed(2));

                }
                else {
                    $('divVeriableSalaryForPr').style.display = 'none';
                    $('divVariableSalaryCalculationForPr').style.display = 'none';
                }
                $('cmbCarAllowence').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownce);
                $('txtCarAllowenceAmount').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount);
                if (res.value.objAutoIncomeSalariedList[i].CarAllownce == 1) {
                    $('divCarAllowenceForPrSalaried').style.display = 'block';
                    $('txtCarAllowenceForPr').setValue(res.value.objAutoIncomeSalariedList[i].CarAllowncePer);
                    var carAllowanceAmntTotal = parseFloat(parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllowncePer) * parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount)) / 100;
                    $('txtCarAllowenceTotalAmountPr').setValue(carAllowanceAmntTotal.toFixed(2));

                }
                else {
                    $('divCarAllowenceForPrSalaried').style.display = 'none';
                }

                if (res.value.objAutoIncomeSalariedList[i].SalaryMode == 2) {
                    $('changeOwnHouseAmount').setValue(res.value.objAutoIncomeSalariedList[i].OwnHouseBenifit);
                    $('cmbInComeAssementForSalaryForPr').setValue('2');
                    $('cmbInComeAssementForPr').setValue('SL');
                    $('OwnHouseBenifitDivForPrSalaried').style.display = 'block';
                }
                else {
                    $('cmbInComeAssementForSalaryForPr').setValue('1');
                    $('cmbInComeAssementForPr').setValue('SC');
                    $('OwnHouseBenifitDivForPrSalaried').style.display = 'none';
                }
                salariedHtmlHelper.calculateTotalIncome('Pr');
            }
            //Joint Salaried Applicant
            else if (res.value.objAutoIncomeSalariedList[i].AccountType == 2) {
                $('cmbSalaryModeForJt').setValue(res.value.objAutoIncomeSalariedList[i].SalaryMode);
                $('cmbEmployerForSalariedForJt').setValue(res.value.objAutoIncomeSalariedList[i].Employer);
                $('txtRemarksForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].Remarks);
                var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoIncomeSalariedList[i].Employer);
                if (emp) {
                    $('txtEmployerCodeForJt').setValue(emp.CompanyCode);
                    $('cmbEmployerCategoryForJtSalaried').setValue(emp.CategoryName);
                }
                $('txtGrossSalaryForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].GrosSalary);
                $('txtNetSalaryForJt').setValue(res.value.objAutoIncomeSalariedList[i].NetSalary);
                $('cmbVeriableSalaryForJt').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalary);
                if (res.value.objAutoIncomeSalariedList[i].VariableSalary == 1) {
                    $('divVeriableSalaryForJt').style.display = 'block';
                    $('divVariableSalaryCalculationForJt').style.display = 'block';
                    $('txtVeriableSalaryPerForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent);
                    var totalvrAmountForPr = 0;
                    var identity = 0;
                    for (var j = 0; j < res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist.length; j++) {
                        identity = j + 1;
                        $('cmbMonth' + identity + 'ForJtSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Month);
                        $('txtVaiableAmountForMonth' + identity + 'ForJtSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                        totalvrAmountForPr += parseFloat(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                    }
                    var variableSalaryAmount = parseFloat(parseFloat(totalvrAmountForPr) * parseFloat(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent)) / 100;

                    $('txtTotalVeriableSalaryForJtSalaried').setValue(variableSalaryAmount.toFixed(2));

                }
                else {
                    $('divVeriableSalaryForJt').style.display = 'none';
                    $('divVariableSalaryCalculationForJt').style.display = 'none';
                }
                $('cmbCarAllowenceJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownce);
                $('txtCarAllowenceAmountJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount);
                if (res.value.objAutoIncomeSalariedList[i].CarAllownce == 1) {
                    $('divCarAllowenceForJtSalaried').style.display = 'block';
                    $('txtCarAllowenceForJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllowncePer);
                    var carAllowanceAmntTotal = parseFloat(parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllowncePer) * parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount)) / 100;
                    $('txtCarAllowenceTotalAmountJt').setValue(carAllowanceAmntTotal.toFixed(2));

                }
                else {
                    $('divCarAllowenceForJtSalaried').style.display = 'none';
                }

                if (res.value.objAutoIncomeSalariedList[i].SalaryMode == 2) {
                    $('changeOwnHouseAmountJt').setValue(res.value.objAutoIncomeSalariedList[i].OwnHouseBenifit);
                    $('cmbInComeAssementForSalaryForJt').setValue('2');
                    $('cmbInComeAssementForJt').setValue('SL');
                    $('OwnHouseBenifitDivForJtSalaried').style.display = 'block';
                }
                else {
                    $('cmbInComeAssementForSalaryForJt').setValue('1');
                    $('cmbInComeAssementForJt').setValue('SC');
                    $('OwnHouseBenifitDivForJtSalaried').style.display = 'none';
                }
                salariedHtmlHelper.calculateTotalIncome('Jt');
            }
        }
    },

    populateLandLord: function(res) {

        if (res.value.objAutoPRProfession.PrimaryProfession == 5 || res.value.objAutoPRProfession.OtherProfession == 5) {
            $('cmbEmployeeForLandLordPr').setValue('12');
            incomeAssmentManager.GetSubSectorForLandLord(12, "Pr");
            $('cmbSubsectorForLandLordPr').setValue('145');
            var sub = Page.subsectorArray.findByProp('SUSE_ID', 145);
            $('txtEmployeeCategoryCodeForLandLordPr').setValue(sub.SUSE_IRCODE);
            $('cmbIncomeAssementForLandLordPr').setValue('5');
            var ass = Page.incomeAssementArray.findByProp('INAM_ID', 5);
            if (ass) {
                $('txtInComeAssementCodeForLandLordPr').setValue(ass.INAM_METHODCODE);
            }
        }
        if (res.value.objAutoJTProfession.PrimaryProfession == 5) {
            $('cmbEmployeeForLandLordJt').setValue('12');
            incomeAssmentManager.GetSubSectorForLandLord(12, "Jt");
            $('cmbSubsectorForLandLordJt').setValue('145');
            var sub = Page.subsectorArray.findByProp('SUSE_ID', 145);
            $('txtEmployeeCategoryCodeForLandLordJt').setValue(sub.SUSE_IRCODE);
            $('cmbIncomeAssementForLandLordJt').setValue('5');
            var ass = Page.incomeAssementArray.findByProp('INAM_ID', 5);
            if (ass) {
                $('txtInComeAssementCodeForLandLordJt').setValue(ass.INAM_METHODCODE);
            }
        }

        for (var i = 0; i < res.value.objAutoLandLord.length; i++) {
            //Primary Landlord
            if (res.value.objAutoLandLord[i].AccountType == 1) {
                $('txtAreaRemarksForLandlordPr').setValue(res.value.objAutoLandLord[i].Remarks);
                $('txtReflactionPerForLandlordPr').setValue(res.value.objAutoLandLord[i].Percentage);
                $('txtReflactionrequiredForLandLordPr').setValue(res.value.objAutoLandLord[i].ReflectionRequired);
                $('txttotalrentalIncomeForLandLordPr').setValue(res.value.objAutoLandLord[i].TotalRentalIncome);
                var idFieldForLandlord = 0;
                for (var j = 0; j < res.value.objAutoLandLord[i].Property.length; j++) {
                    idFieldForLandlord = j + 1;
                    $('txtVerifiedrent' + idFieldForLandlord + 'ForLandLordPr').setValue(res.value.objAutoLandLord[i].Property[j].Property);
                    $('txtShare' + idFieldForLandlord + 'ForLandLordPr').setValue(res.value.objAutoLandLord[i].Property[j].Share);
                    var rentalIncomeForPrlandLord = parseFloat(parseFloat(res.value.objAutoLandLord[i].Property[j].Property) * parseFloat(res.value.objAutoLandLord[i].Property[j].Share)) / 100;
                    $('txtRentalIncome' + idFieldForLandlord + 'ForLandLordPr').setValue(rentalIncomeForPrlandLord);
                }
                incomeAssmentHelper.calculaterentalIncome('txtVerifiedrent1ForLandLordPr', 'Pr');

            }
            //Joint Landlord
            else if (res.value.objAutoLandLord[i].AccountType == 2) {
                $('txtAreaRemarksForLandlordJt').setValue(res.value.objAutoLandLord[i].Remarks);
                $('txtReflactionPerForLandlordJt').setValue(res.value.objAutoLandLord[i].Percentage);
                $('txtReflactionrequiredForLandLordJt').setValue(res.value.objAutoLandLord[i].ReflectionRequired);
                $('txttotalrentalIncomeForLandLordJt').setValue(res.value.objAutoLandLord[i].TotalRentalIncome);
                var idFieldForLandlord = 0;
                for (var j = 0; j < res.value.objAutoLandLord[i].Property.length; j++) {
                    idFieldForLandlord = j + 1;
                    $('txtVerifiedrent' + idFieldForLandlord + 'ForLandLordJt').setValue(res.value.objAutoLandLord[i].Property[j].Property);
                    $('txtShare' + idFieldForLandlord + 'ForLandLordJt').setValue(res.value.objAutoLandLord[i].Property[j].Share);
                    var rentalIncomeForPrlandLord = parseFloat(parseFloat(res.value.objAutoLandLord[i].Property[j].Property) * parseFloat(res.value.objAutoLandLord[i].Property[j].Share)) / 100;
                    $('txtRentalIncome' + idFieldForLandlord + 'ForLandLordJt').setValue(rentalIncomeForPrlandLord);
                }
                incomeAssmentHelper.calculaterentalIncome('txtVerifiedrent1ForLandLordJt', 'Jt');
            }
        }
    },

    populateSubSectorComboForLandLord: function(res, accountType) {
        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }
        if (accountType == "Pr") {
            $("cmbSubsectorForLandLordPr").update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubsectorForLandLordJt").update(link);
        }

    },

    calculaterentalIncome: function(identity, accounttype) {
        var verifiedrent1Total = 0;
        var verifiedrent2Total = 0;
        var verifiedrent3Total = 0;

        var rental1 = 0;
        var rental2 = 0;
        var rental3 = 0;

        var share1 = 0;
        var share2 = 0;
        var share3 = 0;
        var totalrentalIncome = 0;
        var per = 0;
        var reflectionrequired = 0;

        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            //Primary Applicant
            if (accounttype == "Pr") {
                if (util.isFloat($F('txtVerifiedrent1ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent1ForLandLordPr'))) {
                    rental1 = parseFloat($F('txtVerifiedrent1ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare1ForLandLordPr')) && !util.isEmpty($F('txtShare1ForLandLordPr'))) {
                    share1 = parseFloat($F('txtShare1ForLandLordPr'));
                }

                verifiedrent1Total = parseFloat((parseFloat(rental1) * parseFloat(share1)) / 100);

                if (util.isFloat($F('txtVerifiedrent2ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent2ForLandLordPr'))) {
                    rental2 = parseFloat($F('txtVerifiedrent2ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare2ForLandLordPr')) && !util.isEmpty($F('txtShare2ForLandLordPr'))) {
                    share2 = parseFloat($F('txtShare2ForLandLordPr'));
                }
                verifiedrent2Total = parseFloat((parseFloat(rental2) * parseFloat(share2)) / 100);
                if (util.isFloat($F('txtVerifiedrent3ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent3ForLandLordPr'))) {
                    rental3 = parseFloat($F('txtVerifiedrent3ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare3ForLandLordPr')) && !util.isEmpty($F('txtShare3ForLandLordPr'))) {
                    share3 = parseFloat($F('txtShare3ForLandLordPr'));
                }
                verifiedrent3Total = parseFloat((parseFloat(rental3) * parseFloat(share3)) / 100);
                totalrentalIncome = parseFloat(verifiedrent1Total) + parseFloat(verifiedrent2Total) + parseFloat(verifiedrent3Total); //DE39

                if (totalrentalIncome >= 32500 && totalrentalIncome < 49999) {
                    per = 40;
                }
                if (totalrentalIncome >= 50000 && totalrentalIncome < 75000) {
                    per = 50;
                }
                if (totalrentalIncome >= 75000 && totalrentalIncome < 150000) {
                    per = 55;
                }
                if (totalrentalIncome >= 150000) {
                    per = 60;
                }


                reflectionrequired = parseFloat((totalrentalIncome * per) / 100);
                $('txtRentalIncome1ForLandLordPr').setValue(verifiedrent1Total.toFixed(2));
                $('txtRentalIncome2ForLandLordPr').setValue(verifiedrent2Total.toFixed(2));
                $('txtRentalIncome3ForLandLordPr').setValue(verifiedrent3Total.toFixed(2));
                $('txttotalrentalIncomeForLandLordPr').setValue(totalrentalIncome.toFixed(2));
                $('txtReflactionPerForLandlordPr').setValue(per);
                $('txtReflactionrequiredForLandLordPr').setValue(reflectionrequired.toFixed(2));

            }
            //Joint Applicant
            else {
                if (util.isFloat($F('txtVerifiedrent1ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent1ForLandLordJt'))) {
                    rental1 = parseFloat($F('txtVerifiedrent1ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare1ForLandLordJt')) && !util.isEmpty($F('txtShare1ForLandLordJt'))) {
                    share1 = parseFloat($F('txtShare1ForLandLordJt'));
                }

                verifiedrent1Total = parseFloat((parseFloat(rental1) * parseFloat(share1)) / 100);

                if (util.isFloat($F('txtVerifiedrent2ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent2ForLandLordJt'))) {
                    rental2 = parseFloat($F('txtVerifiedrent2ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare2ForLandLordJt')) && !util.isEmpty($F('txtShare2ForLandLordJt'))) {
                    share2 = parseFloat($F('txtShare2ForLandLordJt'));
                }
                verifiedrent2Total = parseFloat((parseFloat(rental2) * parseFloat(share2)) / 100);
                if (util.isFloat($F('txtVerifiedrent3ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent3ForLandLordJt'))) {
                    rental3 = parseFloat($F('txtVerifiedrent3ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare3ForLandLordJt')) && !util.isEmpty($F('txtShare3ForLandLordJt'))) {
                    share3 = parseFloat($F('txtShare3ForLandLordJt'));
                }
                verifiedrent3Total = parseFloat((parseFloat(rental3) * parseFloat(share3)) / 100);
                totalrentalIncome = parseFloat(verifiedrent1Total) + parseFloat(verifiedrent2Total) + parseFloat(verifiedrent3Total);

                if (totalrentalIncome >= 32500 && totalrentalIncome < 49999) {
                    per = 40;
                }
                if (totalrentalIncome >= 50000 && totalrentalIncome < 75000) {
                    per = 50;
                }
                if (totalrentalIncome >= 75000 && totalrentalIncome < 150000) {
                    per = 55;
                }
                if (totalrentalIncome >= 150000) {
                    per = 60;
                }


                reflectionrequired = parseFloat((totalrentalIncome * per) / 100);
                $('txtRentalIncome1ForLandLordJt').setValue(verifiedrent1Total.toFixed(2));
                $('txtRentalIncome2ForLandLordJt').setValue(verifiedrent2Total.toFixed(2));
                $('txtRentalIncome3ForLandLordJt').setValue(verifiedrent3Total.toFixed(2));
                $('txttotalrentalIncomeForLandLordJt').setValue(totalrentalIncome.toFixed(2));
                $('txtReflactionPerForLandlordJt').setValue(per);
                $('txtReflactionrequiredForLandLordJt').setValue(reflectionrequired.toFixed(2));
            }
            incomeAssmentHelper.setLandLordCalculation(accounttype);
        }
        else {
            alert("Required Number Field");
        }
    },

    setLandLordCalculation: function(accountType) {

        var income = 0;
        var assessmentName = "";
        var assesmentCode = "";
        var employerName = "";
        var employerSubsector = "";
        var employerCode = "";
        var assMethodId = 0;
        var assCatId = 0;

        if (accountType == "Pr") {

            income = $F("txttotalrentalIncomeForLandLordPr");
            $('txtIncomeBForLCPrRent').setValue(income);

            assessmentName = $('cmbIncomeAssementForLandLordPr').options[$('cmbIncomeAssementForLandLordPr').selectedIndex].text;
            $('txtInComeAssMethLCPrRent').setValue(assessmentName);

            assesmentCode = $F('txtInComeAssementCodeForLandLordPr');
            $('txtAssCodeForLCPrRent').setValue(assesmentCode);

            employerName = $('cmbEmployeeForLandLordPr').options[$('cmbEmployeeForLandLordPr').selectedIndex].text;
            employerSubsector = $('cmbSubsectorForLandLordPr').options[$('cmbSubsectorForLandLordPr').selectedIndex].text;
            $('txtEmployerCategoryForLCPrRent').setValue(employerName + "-" + employerSubsector);

            employerCode = $F("txtEmployeeCategoryCodeForLandLordPr");
            $('txtEmployerCatCodeForLCPrRent').setValue(employerCode);

            assMethodId = $F('cmbIncomeAssementForLandLordPr');
            $('txtAssMethodIdForLcPrrent').setValue(assMethodId);

            assCatId = $F('cmbEmployeeForLandLordPr');
            $('txtAssCatIdForLcPrRent').setValue(assCatId);

            $('cmbConsiderForLCPrRent').setValue("0");
            $('trLandlordForLoanCalculatorPr').style.display = "";
        }
        else {
            income = $F("txttotalrentalIncomeForLandLordJt");
            $('txtIncomeBForLCJtRent').setValue(income);

            assessmentName = $('cmbIncomeAssementForLandLordJt').options[$('cmbIncomeAssementForLandLordJt').selectedIndex].text;
            $('txtInComeAssMethLCJtRent').setValue(assessmentName);

            assesmentCode = $F('txtInComeAssementCodeForLandLordJt');
            $('txtAssCodeForLCJtRent').setValue(assesmentCode);

            employerName = $('cmbEmployeeForLandLordJt').options[$('cmbEmployeeForLandLordJt').selectedIndex].text;
            employerSubsector = $('cmbSubsectorForLandLordJt').options[$('cmbSubsectorForLandLordJt').selectedIndex].text;
            $('txtEmployerCategoryForLCJtRent').setValue(employerName + "-" + employerSubsector);

            employerCode = $F("txtEmployeeCategoryCodeForLandLordJt");
            $('txtEmployerCatCodeForLCJtRent').setValue(employerCode);

            assMethodId = $F('cmbIncomeAssementForLandLordJt');
            $('txtAssMethodIdForLcJtrent').setValue(assMethodId);

            assCatId = $F('cmbEmployeeForLandLordJt');
            $('txtAssCatIdForLcJtRent').setValue(assCatId);

            $('cmbConsiderForLCJtRent').setValue("0");
            $('trLandlordForLoanCalculatorJt').style.display = "";
        }


    },

    populateSectorCombo: function(res) {
        var link = "<option value=\"-1\">Select a Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SECT_ID + "\">" + res.value[i].SECT_NAME + "</option>";
        }
        $("cmbCategoryForLcCash").update(link);
        Page.sectorLinkOption = link;
    },

    populateSegmentApplicantIncomelist: function(res) {
        for (var i = 0; i < res.value.Auto_An_IncomeSegmentApplicantIncomeList.length; i++) {

            if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].ApplicantType == 1) {
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource > 0) {
                    var identityFieldForLcPr = 0;
                    for (var j = 0; j < Page.prCount; j++) {
                        identityFieldForLcPr = j + 1;
                        var bankId = parseInt($F("txtbankIdForLcPrBank" + identityFieldForLcPr));
                        if (bankId == res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource) {
                            $('cmbConsiderForLCPrBank' + identityFieldForLcPr).setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                            break;
                        }
                    }
                }

                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -7) {
                    $('cmbConsiderForLCPrSalary').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -8) {
                    $('cmbConsiderForLCPrRent').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -9) {
                    $('cmbConsiderForLCPrDoctor').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -10) {
                    $('cmbConsiderForLCPrTeacher').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                incomeAssmentHelper.CalculateLoanCalculator('Pr');

            }
            else if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].ApplicantType == 2) {
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource > 0) {
                    var identityFieldForLcJt = 0;
                    for (var j = 0; j < Page.jtCount; j++) {
                        identityFieldForLcJt = j + 1;
                        var bankId = parseInt($F("txtbankIdForLcJtBank" + identityFieldForLcJt));
                        if (bankId == res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource) {
                            $('cmbConsiderForLCJtBank' + identityFieldForLcJt).setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                            break;
                        }
                    }
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -7) {
                    $('cmbConsiderForLCJtSalary').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -8) {
                    $('cmbConsiderForLCJtRent').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -9) {
                    $('cmbConsiderForLCJtDoctor').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -10) {
                    $('cmbConsiderForLCJtTeacher').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                incomeAssmentHelper.CalculateLoanCalculator('Jt');

            }
        }
    },

    populateSegmentIncome: function(res) {
        if (res.value.objAuto_An_IncomeSegmentIncome != null) {
            $('cmbEmployeeSegmentLc').setValue(res.value.objAuto_An_IncomeSegmentIncome.EmployeeSegment);
            $('txtSegmentCodeLc').setValue(res.value.objAuto_An_IncomeSegmentIncome.EmployeeSegmentCode);
            if (res.value.AutoLoanAnalystApplication.CashSecured100Per == 1) {
                $('cmbAssessmentMethodForLcCash').setValue(res.value.objAuto_An_IncomeSegmentIncome.AssesmentMethod);
                incomeAssmentHelper.changeIncomeAssementMethodForCashSequre();

                $('cmbCategoryForLcCash').setValue(res.value.objAuto_An_IncomeSegmentIncome.Category);
                incomeAssmentHelper.changeCategoryForCashSequre();
            }

        }
    },

    changeIncomeAssementMethodForCashSequre: function() {
        var incomeAssId = $F("cmbAssessmentMethodForLcCash");
        var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssId);
        if (incomeAssBuss) {
            $("txtAssessmentCodeLc").setValue(incomeAssBuss.INAM_METHODCODE);
            $("txtAssessmentMethodIdLc").setValue(incomeAssId);
            $("txtAssessmentMethodLc").setValue(incomeAssBuss.INAM_METHOD);
        }
        else {
            $("txtAssessmentCodeLc").setValue('FF');
            $("txtAssessmentMethodIdLc").setValue('-1');
            $("txtAssessmentMethodLc").setValue('Income Assessment Not Required');
        }
    },

    changeCategoryForCashSequre: function() {
        var categoryId = $F("cmbCategoryForLcCash");
        if (categoryId != "-1") {

            var categoryName = $('cmbCategoryForLcCash').options[$('cmbCategoryForLcCash').selectedIndex].text;
            $("txtEmployeeCategoryIdForLc").setValue(categoryId);
            $("txtEmployeeCategoryForLc").setValue(categoryName);
            $("txtEmployeeCategoryCodeForLc").setValue('N/A');

            if (Page.primaryProfessionId != 2) {
                incomeAssmentManager.changeSectorForLcCashCovered(categoryId);
            }
        }
    },

    populateSubSectorComboLcCashCovered: function(res) {
        Page.subsectorForLcCashCoveredArray = [];
        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            Page.subsectorForLcCashCoveredArray.push(res[i]);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }
        $("cmbSubCategoryForLcCash").update(link);

    },

    populateSegmentCombo: function(res) {
        var link = "<option value=\"-1\">Select a Segment</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SegmentID + "\">" + res.value[i].SegmentName + "</option>";
        }
        $("cmbEmployeeSegmentLc").update(link);
        Page.segmentArray = res.value;
    },

    populateTotalLoanAmount: function(res) {
        if (res.value.objAuto_An_LoanCalculationTotal != null) {
            $('txtloanAmountForLcLoanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.TotalLoanAmount);
            $('txtLtvExclusdingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.LTVExcludingInsurance);
            $('txtDBRExcludingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.FBRExcludingInsurance);
            $('txtltvIncludingLtvForLcLoanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.LTVIncludingInsurance);
            $('txtDBRIncludingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.FBRIncludingInsurance);
            $('txtEmrForlcloanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.EMR);
            $('txtExtraLtvForLcloanSumary').setValue(res.value.objAuto_An_LoanCalculationTotal.ExtraLTV);
            $('txtExtraDBRForLcloanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.ExtraDBR);
        }
    },

    populateInsurenceTotal: function(res) {
        if (res.value.objAuto_An_LoanCalculationInsurance != null) {
            $('txtgeneralInsurenceForLcIns').setValue(res.value.objAuto_An_LoanCalculationInsurance.GeneralInsurance);
            $('txtArtaForLCInsurenceAmount').setValue(res.value.objAuto_An_LoanCalculationInsurance.ARTA);
            $('txtTotalInsurenceForLcInsurence').setValue(res.value.objAuto_An_LoanCalculationInsurance.TotalInsurance);

        }
    },

    populateAnalystLoanAmountTotal: function(res) {
        if (res.value.objAuto_An_LoanCalculationAmount != null) {
            $('txtLtvAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.LTVAllowed);
            $('txtIncoomeAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.IncomeAllowed);
            $('txtbdCadAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.BB_CADAllowed);
            $('txtAskingLoanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.AskingLoan);
            $('txtExistingFacilitiesAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.ExistingFacilityAllowed);
            $('txtAppropriteloanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.AppropriateLoan);
            $('txtApprovedLoanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.ApprovedLoan);

        }
    },

    populateAnalystLoanInterestRate: function(res) {
        if (res.value.objAuto_An_LoanCalculationInterest != null) {
            $('txtInterestLCVendoreAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.VendorAllowed);
            $('txtInterestLCArtaAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.ARTAAllowed);
            $('txtInterestLCSegmentAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.SegmentAllowed);
            $('txtInterestLCAskingRate').setValue(res.value.objAuto_An_LoanCalculationInterest.AskingRate);
            $('txtAppropriteRate').setValue(res.value.objAuto_An_LoanCalculationInterest.AppropriateRate);
            $('txtInterestLCCosideredRate').setValue(res.value.objAuto_An_LoanCalculationInterest.ConsideredRate);

        }
    },

    populateAnalystLoanTenorRate: function(res) {
        if (res.value.objAuto_An_LoanCalculationTenor != null) {
            $('txtArtaAllowedFoLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ARTAAllowed);
            $('txtvehicleAllowedForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.VehicleAllowed);
            $('cmbLabelForTonerLc').setValue(res.value.objAuto_An_LoanCalculationTenor.Level);
            $('txtAgeAllowedForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AgeAllowed);
            $('txtAskingTenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AskingTenor);
            $('txtAppropriteTenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AppropriateTenor);
            $('txtCosideredtenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ConsideredTenor);
            $('txtConsideredMarginForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ConsideredInMonth);

        }
    },

    populateAnalystRepaymentCapability: function(res) {
        if (res.value.objAuto_An_RepaymentCapability != null) {
            $('cmbDBRLevel').setValue(res.value.objAuto_An_RepaymentCapability.DBRLevel);
            $('txtAppropriateDbr').setValue(res.value.objAuto_An_RepaymentCapability.AppropriateDBR);
            $('txtConsideredDbr').setValue(res.value.objAuto_An_RepaymentCapability.ConsideredDBR);
            $('txtMaximumEmiCapability').setValue(res.value.objAuto_An_RepaymentCapability.MaxEMICapability);
            $('txtExistingEmi').setValue(res.value.objAuto_An_RepaymentCapability.ExistingEMI);
            $('txtCurrentEMICapability').setValue(res.value.objAuto_An_RepaymentCapability.CurrentEMICapability);
            if (res.value.objAuto_An_RepaymentCapability.BalanceSupported == 0) {
                $('txtBalanceSupported').setValue('NOT APPLICABLE');
            }
            else {
                $('txtBalanceSupported').setValue(res.value.objAuto_An_RepaymentCapability.BalanceSupported);
            }
            $('txtMaximumEmi').setValue(res.value.objAuto_An_RepaymentCapability.MaxEMI);
        }
    },

    CalculateLoanCalculator: function(accountType) {
        var totalIncome = 0;
        var income = 0;
        var isConsider = 0;
        var declaredPrIncome = 0;
        var declaredJtIncome = 0;
        var totalJointIncome = 0;
        Page.incomeArrayforlc = [];
        Page.newIncomeArrayforAll = [];

        if (accountType == "Pr") {
            var tablePrAccount = $("tblPrLoanCalculator");
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                income = 0;
                var obj = new Object();
                if (i < 7) {
                    isConsider = $F("cmbConsiderForLCPrBank" + i);
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrBank" + i);
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrBank" + i);
                        obj.AssessmentCode = $F("txtAssCodeForLCPrBank" + i);
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrBank" + i);
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrBank" + i);
                        obj.AssmentMethodId = $F("txtAssMethodIdForLcPrBank" + i);
                        obj.AssCatId = $F("txtAssCatIdForLcPrBank" + i);
                        obj.AverageBalance = $F("txtAverageBalanceIdForLcPrBank" + i);
                        var prProfession = $F("txtPrProfessionId");
                        var otherProfession = $F("txtPrOtherProfessionId");
                        if (prProfession == "1" || otherProfession == "1") {
                            obj.Profession = "1";
                        }
                        else {
                            obj.Profession = "3";
                        }

                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);

                    }
                    else {
                        //;
                    }
                }
                else if (i == 7) {
                    isConsider = $F("cmbConsiderForLCPrSalary");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrSalary");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrSalary");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrSalary");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrSalary");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrSalary");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "2"; //Profession
                        obj.Profession = "2";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                    else {
                        //;
                    }
                }
                else if (i == 8) {
                    isConsider = $F("cmbConsiderForLCPrRent");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrRent");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrRent");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrRent");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrRent");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrRent");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "5"; //Profession
                        obj.Profession = "5";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);

                    }
                    else {
                        //;
                    }
                }
                else if (i == 9) {
                    isConsider = $F("cmbConsiderForLCPrDoctor");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrDoctor");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrDoctor");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrDoctor");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrDoctor");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrDoctor");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "4"; //Profession
                        obj.Profession = "4";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                }
                else if (i == 10) {
                    isConsider = $F("cmbConsiderForLCPrTeacher");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrTeacher");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrTeacher");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrTeacher");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrTeacher");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrTeacher");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "150"; //Subsector
                        obj.Profession = 3;
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                }

                if (util.isFloat(income) && !util.isEmpty(income)) {
                    totalIncome += parseFloat(income);
                }
            }




            $('txtTotalInComeForLcAll').setValue(totalIncome);
            declaredPrIncome = $F("txtDeclaredIncomeForLcAll");
            if (util.isFloat(declaredPrIncome) && !util.isEmpty(declaredPrIncome)) {
                if (totalIncome > parseFloat(declaredPrIncome)) {
                    $('txtAppropriteIncomeForLcAll').setValue(declaredPrIncome);
                }
                else {
                    $('txtAppropriteIncomeForLcAll').setValue(totalIncome);
                }
            }
            else {
                $('txtAppropriteIncomeForLcAll').setValue('0');
            }

        }
        else {
            var tableJtAccount = $("tblJtLoanCalculator");
            var rowCountJt = tableJtAccount.rows.length;
            for (var i = 1; i <= rowCountJt - 1; i++) {
                income = 0;
                if (i < 7) {
                    isConsider = $F("cmbConsiderForLCJtBank" + i);
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtBank" + i);
                    }
                }
                else if (i == 7) {
                    isConsider = $F("cmbConsiderForLCJtSalary");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtSalary");
                    }
                }
                else if (i == 8) {
                    isConsider = $F("cmbConsiderForLCJtRent");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtRent");
                    }
                }
                else if (i == 9) {
                    isConsider = $F("cmbConsiderForLCJtDoctor");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtDoctor");
                    }
                }
                else if (i == 10) {
                    isConsider = $F("cmbConsiderForLCJtTeacher");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtTeacher");
                    }
                }



                if (util.isFloat(income) && !util.isEmpty(income)) {
                    totalIncome += parseFloat(income);
                }
            }
            $('txtTotalIncomeForLcJointAll').setValue(totalIncome);

            declaredJtIncome = $F("txtDeclaredIncomeForLcJointAll");
            if (util.isFloat(declaredJtIncome) && !util.isEmpty(declaredJtIncome)) {
                if (totalIncome > parseFloat(declaredJtIncome)) {
                    $('txtAppropriteIncomeForLcJointAll').setValue(declaredJtIncome);
                }
                else {
                    $('txtAppropriteIncomeForLcJointAll').setValue(totalIncome);
                }
            }
            else {
                $('txtAppropriteIncomeForLcJointAll').setValue('0');
            }
        }

        var reasonofJointApplicant = $F("cmbReasonofJointApplicant");
        if (reasonofJointApplicant == "1") {
            var appropripteIncomePr = $F("txtAppropriteIncomeForLcAll");
            var appropriteIncomeJt = $F("txtAppropriteIncomeForLcJointAll");

            if (util.isFloat(appropripteIncomePr) && !util.isEmpty(appropripteIncomePr)) {
                totalJointIncome += parseFloat(appropripteIncomePr);
            }
            if (util.isFloat(appropriteIncomeJt) && !util.isEmpty(appropriteIncomeJt)) {
                totalJointIncome += parseFloat(appropriteIncomeJt);
            }
            $('txtTotalJointInComeForLcWithCal').setValue(totalJointIncome);

        }
        else {
            var appropripteIncome = $F("txtAppropriteIncomeForLcAll");
            if (util.isFloat(appropripteIncome) && !util.isEmpty(appropripteIncome)) {
                totalJointIncome += parseFloat(appropripteIncome);
            }
            $('txtTotalJointInComeForLcWithCal').setValue(totalJointIncome);
        }


        var dbrLevel = $F("cmbDBRLevel");
        if (dbrLevel == "" || dbrLevel == "1") {
            $("txtAppropriateDbr").setValue('50');
        }
    },

    populateFacilityCombo: function(res) {
        var link = "<option value=\"-1\">Select a Type</option>";
        var link1 = "<option value=\"-1\">Select a Type</option>";


        for (var i = 0; i < res.value.length; i++) {
            if (res.value[i].FACI_NAME == "Credit Card ($)".trim() || res.value[i].FACI_NAME == "Credit Card (BDT)".trim()) {
                link1 += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
            }
            else {
                if (res.value[i].FACI_NAME != "Over Draft".trim()) {
                    link += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
                }
            }
        }
        $("cmbTypeForTermLoans1").update(link);
        $("cmbTypeForTermLoans2").update(link);
        $("cmbTypeForTermLoans3").update(link);
        $("cmbTypeForTermLoans4").update(link);

        $("cmbTypeForCreditCard1").update(link1);
        $("cmbTypeForCreditCard2").update(link1);

        Page.facilityArray = res.value;

    },

    populateBankCombo: function(res) {

        var link = "<option value=\"-1\">Select a bank</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BankID + "\">" + res.value[i].BankName + "</option>";
        }
        //Exposer Off Scb Finential Institution

        $("cmbFinancialInstitutionForOffSCB1").update(link);
        $("cmbFinancialInstitutionForOffSCB2").update(link);
        $("cmbFinancialInstitutionForOffSCB3").update(link);
        $("cmbFinancialInstitutionForOffSCB4").update(link);
        $("cmbFinancialInstitutionForCreditCardOffSCB1").update(link);
        $("cmbFinancialInstitutionForCreditCardOffSCB2").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB1").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB2").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB3").update(link);
        Page.bankLinkOption = link;
    },

    populateFacilityOnScb: function(res) {

        var optlinkForColl = "<option value='-1'>Select From list</option>";
        var prPrimaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var jtPrimaryProfession = res.value.objAutoJTProfession.PrimaryProfession;
        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        Page.facilityListArray = res.value.objAutoLoanFacilityList;

        var j = 0;
        var k = 0;
        var l = 0;
        for (var i = 0; i < res.value.objAutoLoanFacilityList.length; i++) {
            if (res.value.objAutoLoanFacilityList[i].FacilityType == 22 || res.value.objAutoLoanFacilityList[i].FacilityType == 18) {
                var idfieldforExposerForCreditCard = j + 1;
                if (idfieldforExposerForCreditCard < 3) {
                    $('cmbTypeForCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);
                    var typeforCreditCardobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    $('txtFlagForcreditCard' + idfieldforExposerForCreditCard).setValue(typeforCreditCardobject.FACI_CODE);
                    $('txtOriginalLimitForCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    var credInterest = res.value.objAutoLoanFacilityList[i].PresentLimit * (res.value.objAutoLoanFacilityList[i].InterestRate / 100);
                    $('txtInterestForCreditCard' + idfieldforExposerForCreditCard).setValue(Math.round(credInterest.toFixed(2)));
                    j = j + 1;
                }
            }
            else if (res.value.objAutoLoanFacilityList[i].FacilityType == 23) {
                var idfieldforExposerForOverdraft = k + 1;
                if (idfieldforExposerForOverdraft < 4) {
                    var typeforOverDraftobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    $('txtOverdraftFlag' + idfieldforExposerForOverdraft).setValue(typeforOverDraftobject.FACI_CODE);
                    $('txtOverdraftInterest' + idfieldforExposerForOverdraft).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                    $('txtOverdraftlimit' + idfieldforExposerForOverdraft).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    var interst = parseFloat(parseFloat((res.value.objAutoLoanFacilityList[i].PresentLimit * res.value.objAutoLoanFacilityList[i].InterestRate) / 12) / 100);
                    $('txtOverdraftInteres' + idfieldforExposerForOverdraft).setValue(Math.round(interst.toFixed(2)));


                    //Income Generator----------------------------------
                    //OVER DRAFT FACILITIES WITH SCB For bussiness Man Tab--------------------

                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        incomeAssmentHelper.bussinessManAndSelfEmployedTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Pr", "BM");

                    }
                    if (jtPrimaryProfession == 1 && isJoint == true) {
                        incomeAssmentHelper.bussinessManAndSelfEmployedTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Jt", "BM");
                    }

                    //OVER DRAFT FACILITIES WITH SCB For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        incomeAssmentHelper.bussinessManAndSelfEmployedTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Pr", "SE");

                    }
                    if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
                        Page.jointPrimaryProfessionId = jtPrimaryProfession;
                        Page.isjoint = true;
                        incomeAssmentHelper.bussinessManAndSelfEmployedTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Jt", "SE");
                    }
                }

                k = k + 1;
            }
            else {
                var idfieldforExposerForTermsloans = l + 1;
                if (idfieldforExposerForTermsloans < 5) {
                    $('cmbTypeForTermLoans' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);


                    var typeforOtherobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    if (typeforOtherobject != undefined) {
                        $('txtflag' + idfieldforExposerForTermsloans).setValue(typeforOtherobject.FACI_CODE);
                    }
                    $('txtInterestRate' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                    $('txtLimit' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    $('txtoutStanding' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentBalance);
                    $('txtEMI' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentEMI);
                    $('txtEMIPer' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].EMIPercent);
                    $('cmbStatusForSecurities' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].Status);

                    var emiShare = parseFloat(parseFloat(res.value.objAutoLoanFacilityList[i].PresentEMI) * parseFloat(res.value.objAutoLoanFacilityList[i].EMIPercent / 100));
                    $('txtEMIShare' + idfieldforExposerForTermsloans).setValue(Math.round(emiShare.toFixed(2)));
                    //Preveios repayment On Scb For bussiness Man Tab
                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        if (res.value.objAutoLoanFacilityList[i].Status == 2 || res.value.objAutoLoanFacilityList[i].Status == 3) {
                            incomeAssmentHelper.prevRepaymentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Pr", "BM");
                        }
                    }
                    if (jtPrimaryProfession == 1 && isJoint == true) {
                        if (res.value.objAutoLoanFacilityList[i].Status == 2 || res.value.objAutoLoanFacilityList[i].Status == 3) {
                            incomeAssmentHelper.prevRepaymentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Jt", "BM");
                        }
                    }

                    //Preveios repayment On Scb For bussiness Man Tab For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        if (res.value.objAutoLoanFacilityList[i].Status == 2 || res.value.objAutoLoanFacilityList[i].Status == 3) {
                            incomeAssmentHelper.prevRepaymentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Pr", "SE");
                        }

                    }
                    if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
                        if (res.value.objAutoLoanFacilityList[i].Status == 2 || res.value.objAutoLoanFacilityList[i].Status == 3) {
                            incomeAssmentHelper.prevRepaymentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Jt", "SE");
                        }
                    }
                    if (typeforOtherobject != undefined) {
                        optlinkForColl += "<option value=" + res.value.objAutoLoanFacilityList[i].FacilityType + ">" + typeforOtherobject.FACI_NAME + "</option>";
                    }
                }
                l = l + 1;
            }

        }
    },

    bussinessManAndSelfEmployedTabOverDraftwithSCB: function(objAutoLoanFacilityList, idField, accountType, tabName) {
        var status = objAutoLoanFacilityList.Status;
        if (status == 1) {
            var interst = parseFloat(parseFloat(parseFloat((objAutoLoanFacilityList.PresentLimit * objAutoLoanFacilityList.InterestRate)) / 12) / 100);
            var bankno = 0;
            if (accountType == "Pr" && tabName == "BM") {
                for (var i = 0; i < Page.prCount; i++) {
                    bankno = i + 1;
                    if (bankno < 7) {
                        $('txtLimt' + idField + 'WithScbForBank' + bankno).setValue(objAutoLoanFacilityList.PresentLimit);
                        $('txtInterst' + idField + 'WithScbForBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                    }
                }
            }
            else if (accountType == "Jt" && tabName == "BM") {
                for (var i = 0; i < Page.jtCount; i++) {
                    bankno = i + 1;
                    if (bankno < 7) {
                        $('txtLimit' + idField + 'ForJointBank' + bankno).setValue(objAutoLoanFacilityList.PresentLimit);
                        $('txtInterest' + idField + 'ForJointBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                    }
                }
            }
            else if (accountType == "Pr" && tabName == "SE") {
                for (var i = 0; i < Page.prCount; i++) {
                    bankno = i + 1;
                    if (bankno < 7) {
                        $('txtLimt' + idField + 'WithScbForSEBank' + bankno).setValue(objAutoLoanFacilityList.PresentLimit);
                        $('txtInterst' + idField + 'WithScbForSEBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                    }
                }

            }
            else if (accountType == "Jt" && tabName == "SE") {
                for (var i = 0; i < Page.jtCount; i++) {
                    bankno = i + 1;
                    if (bankno < 7) {
                        $('txtLimt' + idField + 'WithScbForSEJtBank' + bankno).setValue(objAutoLoanFacilityList.PresentLimit);
                        $('txtInterst' + idField + 'WithScbForSEJtBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                    }
                }
            }
        }
    },

    prevRepaymentOnSCB: function(objAutoLoanonScbFacilityList, idfieldforExposerForTermsloans, accountType, tabName) {
        var bankno = 0;
        if (accountType == "Pr" && tabName == "BM") {
            for (var i = 0; i < Page.prCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForPrimaryBank' + bankno).setValue(objAutoLoanonScbFacilityList.EMIShare);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForPrimaryBank' + bankno).setValue('0');
                }

            }
        }
        else if (accountType == "Jt" && tabName == "BM") {
            for (var i = 0; i < Page.jtCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtEmiOnScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue(objAutoLoanonScbFacilityList.EMIShare);
                    $('cmbConsiderOnScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue('0');
                }

            }
        }
        else if (accountType == "Pr" && tabName == "SE") {
            for (var i = 0; i < Page.prCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEPrBank' + bankno).setValue(objAutoLoanonScbFacilityList.EMIShare);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEPrBank' + bankno).setValue('0');
                }
            }
        }
        else if (accountType == "Jt" && tabName == "SE") {
            for (var i = 0; i < Page.jtCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEJtBank' + bankno).setValue(objAutoLoanonScbFacilityList.EMIShare);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEJtBank' + bankno).setValue('0');
                }

            }
        }
    },

    populateSequrityOnScb: function(res) {
        var m = 0;
        var n = 0;
        var p = 0;

        for (var i = 0; i < res.value.objAutoLoanSecurityList.length; i++) {
            if (res.value.objAutoLoanSecurityList[i].FacilityType == 22 || res.value.objAutoLoanSecurityList[i].FacilityType == 18) {
                var idfieldforExposerForCreditCardSequrity = p + 1;
                if (idfieldforExposerForCreditCardSequrity < 3) {
                    var typeforCreditCardobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforCreditCardobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        var facalityObjectForCredtcard = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForCredtcard != undefined) {
                            var outStandingForcreditCard = facalityObjectForCredtcard.PresentLimit;
                            var ltvForCreditturnOver = parseFloat(outStandingForcreditCard / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;
                            if (util.isFloat(ltvForCreditturnOver)) {
                                $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(Math.round(ltvForCreditturnOver.toFixed(2)));
                            }
                            else {
                                $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                            }
                        }
                        else {
                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                        }

                    }
                }

                p = p + 1;
            }
            else if (res.value.objAutoLoanSecurityList[i].FacilityType == 23) {
                var idfieldforExposerForOverDraftSequrity = n + 1;
                if (idfieldforExposerForOverDraftSequrity < 4) {
                    var typeforOverDraftobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforOverDraftobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        $('txtOverDraftInterestRate' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);
                        var facalityObjectForOverDraft = Page.facilityListArray.findByProp('FacilityId', res.value.objAutoLoanSecurityList[i].FacilityId);
                        if (facalityObjectForOverDraft != undefined) {
                            var originalLimit = facalityObjectForOverDraft.PresentLimit;
                            var ltvForOverDraft = parseFloat(originalLimit / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;
                            if (util.isFloat(ltvForOverDraft)) {
                                $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(Math.round(ltvForOverDraft.toFixed(2)));
                            }
                            else {
                                $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                            }

                        }
                        else {
                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                        }

                    }
                }
                n = n + 1;
            }
            else {
                var idfieldforExposerForTermLoanSequrity = m + 1;
                if (idfieldforExposerForTermLoanSequrity < 5) {
                    var typeforTermLoanobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    $('cmbTypeForSequrities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                    $('txtSecurityFV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                    $('txtSecurityPV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                    $('txttermloanInterestRate' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);
                    var facalityObjectFortermLoan = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (facalityObjectFortermLoan != undefined) {
                        var outStandingFortermloan = facalityObjectFortermLoan.PresentBalance;
                        var ltvFortermLoan = parseFloat(outStandingFortermloan / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;
                        if (util.isFloat(ltvFortermLoan)) {
                            $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue(Math.round(ltvFortermLoan.toFixed(2)));
                        }
                        else {
                            $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                        }
                    }
                    else {
                        $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                    }

                    //}
                }
                m = m + 1;
            }

        }
    },

    populateFacilityOffScb: function(res) {
        var prPrimaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var jtPrimaryProfession = res.value.objAutoJTProfession.PrimaryProfession;
        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        var s = 0;
        var d = 0;
        var f = 0;
        var idfieldforOffSCBForCreditCard = 0;
        for (var i = 0; i < res.value.AutoLoanFacilityOffSCBList.length; i++) {
            if (res.value.AutoLoanFacilityOffSCBList[i].Type == 2) {
                idfieldforOffSCBForCreditCard = s + 1;
                if (idfieldforOffSCBForCreditCard < 3) {
                    $('cmbFinancialInstitutionForCreditCardOffSCB' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    $('txtOriginalLimitForcreditCardOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    var interest = (res.value.AutoLoanFacilityOffSCBList[i].InterestRate / 100) * res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit;
                    $('txtInterestForCreditCardForOffScb' + idfieldforOffSCBForCreditCard).setValue(interest);
                    $('cmbStatusForCreditCardOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);
                }
                s = s + 1;
            }
            else if (res.value.AutoLoanFacilityOffSCBList[i].Type == 3) {

                idfieldforOffSCBForOverdraft = d + 1;
                if (idfieldforOffSCBForOverdraft < 4) {
                    $('cmbFinancialInstitutionForOverDraftOffSCB' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    $('txtOriginalLimitForOverDraftOffScb' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    $('txtinterestRateForOverDraftOffSCB' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].InterestRate);
                    incomeAssmentHelper.changeOverDraftInterstForOffSCB(idfieldforOffSCBForOverdraft);
                    $('cmbStatusForOverDraftOffScb' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);
                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        incomeAssmentHelper.bussinessManAndSelfEmployedTabOverDraftwithOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Pr", "BM");

                    }
                    if (jtPrimaryProfession == 1 && isJoint == true) {
                        incomeAssmentHelper.bussinessManAndSelfEmployedTabOverDraftwithOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Jt", "BM");
                    }

                    //OVER DRAFT FACILITIES WITH SCB For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        incomeAssmentHelper.bussinessManAndSelfEmployedTabOverDraftwithOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Pr", "SE");

                    }
                    if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
                        incomeAssmentHelper.bussinessManAndSelfEmployedTabOverDraftwithOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Jt", "SE");
                    }

                    //Need to do For BankStatement Tab

                }

                d = d + 1;
            }
            else {
                idfieldforOffSCBForTermsloans = f + 1;
                if (idfieldforOffSCBForTermsloans < 5) {
                    $('cmbFinancialInstitutionForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    $('txtOriginalLimitForOffScb' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    $('txtEMIforOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMI);
                    $('txtEMIPerForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMIPercent);
                    $('txtEMIShareForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMIShare);
                    $('cmbStatusForOffScb' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);
                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        if (res.value.AutoLoanFacilityOffSCBList[i].Status == 2 || res.value.AutoLoanFacilityOffSCBList[i].Status == 3) {
                            incomeAssmentHelper.prevRepaymentHistoryOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Pr", "BM");
                        }
                    }
                    if (jtPrimaryProfession == 1 && isJoint == true) {
                        if (res.value.AutoLoanFacilityOffSCBList[i].Status == 2 || res.value.AutoLoanFacilityOffSCBList[i].Status == 3) {
                            incomeAssmentHelper.prevRepaymentHistoryOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Jt", "BM");
                        }
                    }

                    //Preveios repayment Of Scb For bussiness Man Tab For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        if (res.value.AutoLoanFacilityOffSCBList[i].Status == 2 || res.value.AutoLoanFacilityOffSCBList[i].Status == 3) {
                            incomeAssmentHelper.prevRepaymentHistoryOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Pr", "SE");
                        }
                    }
                    if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
                        if (res.value.AutoLoanFacilityOffSCBList[i].Status == 2 || res.value.AutoLoanFacilityOffSCBList[i].Status == 3) {
                            incomeAssmentHelper.prevRepaymentHistoryOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Jt", "SE");
                        }
                    }
                }

                f = f + 1;
            }

        }

    },

    changeOverDraftInterstForOffSCB: function(rowId) {
        var originalLimit = $F("txtOriginalLimitForOverDraftOffScb" + rowId);
        var interestRate = $F("txtinterestRateForOverDraftOffSCB" + rowId);
        if (originalLimit == "") {
            originalLimit = 0;
        }
        if (interestRate == "") {
            interestRate = 0;
        }
        if (!util.isFloat(originalLimit)) {
            alert("Number field is required");
            $('txtOriginalLimitForOverDraftOffScb' + rowId).setValue('0');
            $("txtInterestForOverDraftInterest" + rowId).setValue('0');
            //$("txtOriginalLimitForOverDraftOffScb" + rowId).focus();
            return false;
        }
        if (!util.isFloat(interestRate)) {
            alert("Number field is required");
            $('txtOriginalLimitForOverDraftOffScb' + rowId).setValue('0');
            $("txtInterestForOverDraftInterest" + rowId).setValue('0');
            $("txtinterestRateForOverDraftOffSCB" + rowId).setValue('0');
            //$("txtOriginalLimitForOverDraftOffScb" + rowId).focus();

            return false;
        }
        var overDraftInterest = parseFloat(parseFloat(originalLimit * parseFloat(interestRate / 12)) / 100);

        $("txtInterestForOverDraftInterest" + rowId).setValue(Math.round(overDraftInterest.toFixed(2)));

        incomeAssmentHelper.createOverDraftStatemntForBank('2', rowId);

    },

    createOverDraftStatemntForBank: function(accountType, identity) {
        //if account type =1 , then onscb, accountTye = 2,  then offscb

        if (accountType == 1) {
            var overDraftStatus = $F("cmbStatusForSecuritiesForoverDraft" + identity);
            var originalLimit = $F("txtOverdraftlimit" + identity);
            var interestRate = $F("txtOverdraftInteres" + identity);
            if (Page.primaryProfessionId == 1 || Page.otherProfessionId == 1) {
                if (overDraftStatus == "1") {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('txtLimt' + identity + 'WithScbForBank' + b).setValue(originalLimit);
                        $('txtInterst' + identity + 'WithScbForBank' + b).setValue(interestRate);
                        $('cmbConsider' + identity + 'ForBank' + b).setValue('0');
                    }
                }
                else {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('txtLimt' + identity + 'WithScbForBank' + b).setValue('');
                        $('txtInterst' + identity + 'WithScbForBank' + b).setValue('');
                        $('cmbConsider' + identity + 'ForBank' + b).setValue('0');
                    }
                }
            }
            if (Page.primaryProfessionId == 3 || Page.otherProfessionId == 3 || Page.primaryProfessionId == 4 || Page.otherProfessionId == 4 || Page.primaryProfessionId == 6 || Page.otherProfessionId == 6) {
                if (overDraftStatus == "1") {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('txtLimt' + identity + 'WithScbForSEBank' + b).setValue(originalLimit);
                        $('txtInterst' + identity + 'WithScbForSEBank' + b).setValue(interestRate);
                        $('cmbConsider' + identity + 'WithScbForSEBank' + b).setValue('0');
                    }
                }
                else {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('txtLimt' + identity + 'WithScbForSEBank' + b).setValue('');
                        $('txtInterst' + identity + 'WithScbForSEBank' + b).setValue('');
                        $('cmbConsider' + identity + 'WithScbForSEBank' + b).setValue('0');
                    }
                }
            }
            if (Page.isjoint == true) {
                if (Page.jointPrimaryProfessionId == 1) {
                    if (overDraftStatus == "1") {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('txtLimit' + identity + 'ForJointBank' + b).setValue(originalLimit);
                            $('txtInterest' + identity + 'ForJointBank' + b).setValue(interestRate);
                            $('cmbInterest' + identity + 'ForJointBank' + b).setValue('0');
                        }
                    }
                    else {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('txtLimit' + identity + 'ForJointBank' + b).setValue('');
                            $('txtInterest' + identity + 'ForJointBank' + b).setValue('');
                            $('cmbInterest' + identity + 'ForJointBank' + b).setValue('0');
                        }
                    }
                }
                if (Page.jointPrimaryProfessionId == 3 || Page.jointPrimaryProfessionId == 4 || Page.jointPrimaryProfessionId == 6) {
                    if (overDraftStatus == "1") {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('txtLimt' + identity + 'WithScbForSEJtBank' + b).setValue(originalLimit);
                            $('txtInterst' + identity + 'WithScbForSEJtBank' + b).setValue(interestRate);
                            $('cmbConsider' + identity + 'WithScbForSEJtBank' + b).setValue('0');
                        }
                    }
                    else {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('txtLimt' + identity + 'WithScbForSEJtBank' + b).setValue('');
                            $('txtInterst' + identity + 'WithScbForSEJtBank' + b).setValue('');
                            $('cmbConsider' + identity + 'WithScbForSEJtBank' + b).setValue('0');
                        }
                    }
                }
            }

        }
        if (accountType == 2) {
            var overDraftStatus = $F("cmbStatusForOverDraftOffScb" + identity);
            var originalLimit = $F("txtOriginalLimitForOverDraftOffScb" + identity);
            var interestRate = $F("txtInterestForOverDraftInterest" + identity);
            var bankId = $F("cmbFinancialInstitutionForOverDraftOffSCB" + identity);

            //


            if (Page.primaryProfessionId == 1 || Page.otherProfessionId == 1) {
                if (overDraftStatus == "1") {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('cmbBankName' + identity + 'ForBussinessTabPrBank' + b).update(Page.bankLinkOption);
                        $('cmbBankName' + identity + 'ForBussinessTabPrBank' + b).setValue(bankId);
                        $('txtLimit' + identity + 'ForOffSCBBank' + b).setValue(originalLimit);
                        $('txtInterest' + identity + 'ForOffSCBBank' + b).setValue(interestRate);
                        $('cmbConsider' + identity + 'ForOffScbBank' + b).setValue('0');
                    }
                }
                else {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('cmbBankName' + identity + 'ForBussinessTabPrBank' + b).update('');
                        $('txtLimit' + identity + 'ForOffSCBBank' + b).setValue('');
                        $('txtInterest' + identity + 'ForOffSCBBank' + b).setValue('');
                        $('cmbConsider' + identity + 'ForOffScbBank' + b).setValue('0');
                    }
                }
            }
            if (Page.primaryProfessionId == 3 || Page.otherProfessionId == 3 || Page.primaryProfessionId == 4 || Page.otherProfessionId == 4 || Page.primaryProfessionId == 6 || Page.otherProfessionId == 6) {
                if (overDraftStatus == "1") {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('cmbBankName' + identity + 'ForSEPrBank' + b).update(Page.bankLinkOption);
                        $('cmbBankName' + identity + 'ForSEPrBank' + b).setValue(bankId);
                        $('txtLimit' + identity + 'ForSEOffSCBBank' + b).setValue(originalLimit);
                        $('txtInterest' + identity + 'ForSEOffSCBBank' + b).setValue(interestRate);
                        $('cmbConsider' + identity + 'ForSEOffScbBank' + b).setValue('0');
                    }
                }
                else {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('cmbBankName' + identity + 'ForSEPrBank' + b).update('');
                        $('txtLimit' + identity + 'ForSEOffSCBBank' + b).setValue('');
                        $('txtInterest' + identity + 'ForSEOffSCBBank' + b).setValue('');
                        $('cmbConsider' + identity + 'ForSEOffScbBank' + b).setValue('0');
                    }
                }
            }
            if (Page.isjoint == true) {
                if (Page.jointPrimaryProfessionId == 1) {
                    if (overDraftStatus == "1") {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('cmbBankName' + identity + 'ForBussinessTabJtBank' + b).update(Page.bankLinkOption);
                            $('cmbBankName' + identity + 'ForBussinessTabJtBank' + b).setValue(bankId);
                            $('txtLimitOffScb' + identity + 'ForJointBank' + b).setValue(originalLimit);
                            $('txtInterestOffSCB' + identity + 'ForJointBank' + b).setValue(interestRate);
                            $('cmbConsiderOffScbBM' + identity + 'ForJointBank' + b).setValue('0');
                        }
                    }
                    else {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('cmbBankName' + identity + 'ForBussinessTabJtBank' + b).update('');
                            $('txtLimitOffScb' + identity + 'ForJointBank' + b).setValue('');
                            $('txtInterestOffSCB' + identity + 'ForJointBank' + b).setValue('');
                            $('cmbConsiderOffScbBM' + identity + 'ForJointBank' + b).setValue('0');
                        }
                    }
                }
                if (Page.jointPrimaryProfessionId == 3 || Page.jointPrimaryProfessionId == 4 || Page.jointPrimaryProfessionId == 6) {
                    if (overDraftStatus == "1") {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('cmbBankName' + identity + 'ForSEJtBank' + b).update(Page.bankLinkOption);
                            $('cmbBankName' + identity + 'ForSEJtBank' + b).setValue(bankId);
                            $('txtLimit' + identity + 'ForSEJtOffSCBBank' + b).setValue(originalLimit);
                            $('txtInterest' + identity + 'ForSEJtOffSCBBank' + b).setValue(interestRate);
                            $('cmbConsider' + identity + 'ForSEJtOffScbBank' + b).setValue('0');
                        }
                    }
                    else {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('cmbBankName' + identity + 'ForSEJtBank' + b).update('');
                            $('txtLimit' + identity + 'ForSEJtOffSCBBank' + b).setValue('');
                            $('txtInterest' + identity + 'ForSEJtOffSCBBank' + b).setValue('');
                            $('cmbConsider' + identity + 'ForSEJtOffScbBank' + b).setValue('0');
                        }
                    }
                }
            }
        }
    },

    bussinessManAndSelfEmployedTabOverDraftwithOffSCB: function(objAutoLoanFacilityList, idField, accountType, tabName) {
        var status = objAutoLoanFacilityList.Status;
        if (status == 1) {
            var interst = parseFloat(parseFloat(parseFloat((objAutoLoanFacilityList.OriginalLimit * objAutoLoanFacilityList.InterestRate)) / 12) / 100);
            var bankno = 0;
            if (accountType == "Pr" && tabName == "BM") {
                for (var i = 0; i < Page.prCount; i++) {
                    bankno = i + 1;
                    if (bankno < 5) {
                        $('cmbBankName' + idField + 'ForBussinessTabPrBank' + bankno).update(Page.bankLinkOption);
                        $('cmbBankName' + idField + 'ForBussinessTabPrBank' + bankno).setValue(objAutoLoanFacilityList.BankID);
                        $('txtLimit' + idField + 'ForOffSCBBank' + bankno).setValue(objAutoLoanFacilityList.OriginalLimit);
                        $('txtInterest' + idField + 'ForOffSCBBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                        $('cmbConsider' + idField + 'ForOffScbBank' + bankno).setValue('0');
                    }
                }
            }
            else if (accountType == "Jt" && tabName == "BM") {

                for (var i = 0; i < Page.jtCount; i++) {
                    bankno = i + 1;
                    if (bankno < 5) {
                        $('cmbBankName' + idField + 'ForBussinessTabJtBank' + bankno).update(Page.bankLinkOption);
                        $('cmbBankName' + idField + 'ForBussinessTabJtBank' + bankno).setValue(objAutoLoanFacilityList.BankID);
                        $('txtLimitOffScb' + idField + 'ForJointBank' + bankno).setValue(objAutoLoanFacilityList.OriginalLimit);
                        $('txtInterestOffSCB' + idField + 'ForJointBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                        $('cmbConsiderOffScbBM' + idField + 'ForJointBank' + bankno).setValue('0');
                    }
                }

            }
            else if (accountType == "Pr" && tabName == "SE") {
                for (var i = 0; i < Page.prCount; i++) {
                    bankno = i + 1;
                    if (bankno < 5) {
                        $('cmbBankName' + idField + 'ForSEPrBank' + bankno).update(Page.bankLinkOption);
                        $('cmbBankName' + idField + 'ForSEPrBank' + bankno).setValue(objAutoLoanFacilityList.BankID);
                        $('txtLimit' + idField + 'ForSEOffSCBBank' + bankno).setValue(objAutoLoanFacilityList.OriginalLimit);
                        $('txtInterest' + idField + 'ForSEOffSCBBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                        $('cmbConsider' + idField + 'ForSEOffScbBank' + bankno).setValue('0');
                    }
                }
            }
            else if (accountType == "Jt" && tabName == "SE") {
                for (var i = 0; i < Page.jtCount; i++) {
                    bankno = i + 1;
                    if (bankno < 5) {
                        $('cmbBankName' + idField + 'ForSEJtBank' + bankno).update(Page.bankLinkOption);
                        $('cmbBankName' + idField + 'ForSEJtBank' + bankno).setValue(objAutoLoanFacilityList.BankID);
                        $('txtLimit' + idField + 'ForSEJtOffSCBBank' + bankno).setValue(objAutoLoanFacilityList.OriginalLimit);
                        $('txtInterest' + idField + 'ForSEJtOffSCBBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                        $('cmbConsider' + idField + 'ForSEJtOffScbBank' + bankno).setValue('0');
                    }
                }
            }
        }
    },

    prevRepaymentHistoryOffSCB: function(objAutoLoanOffScbFacilityList, idfieldforExposerForTermsloans, accountType, tabName) {
        var instituteName = $('cmbFinancialInstitutionForOffSCB' + idfieldforExposerForTermsloans).options[$('cmbFinancialInstitutionForOffSCB' + idfieldforExposerForTermsloans).selectedIndex].text;
        var bankno = 0;
        if (accountType == "Pr" && tabName == "BM") {
            for (var i = 0; i < Page.prCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank' + bankno).setValue(instituteName);
                    $('txtBank' + idfieldforExposerForTermsloans + 'BMPr' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank' + bankno).setValue('0');
                }
            }
        }
        else if (accountType == "Jt" && tabName == "BM") {
            for (var i = 0; i < Page.jtCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue(instituteName); //txtInstituteOffScb
                    $('txtBank' + idfieldforExposerForTermsloans + 'BMJt' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue('0');
                }
            }
        }
        else if (accountType == "Pr" && tabName == "SE") {
            for (var i = 0; i < Page.prCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank' + bankno).setValue(instituteName);
                    $('txtBank' + idfieldforExposerForTermsloans + 'SEPr' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank' + bankno).setValue('0');
                }
            }
        }
        else if (accountType == "Jt" && tabName == "SE") {
            for (var i = 0; i < Page.jtCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue(instituteName);
                    $('txtBank' + idfieldforExposerForTermsloans + 'SEJt' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank' + bankno).setValue('0');
                }
            }
        }
    },

    populateBankStatementData: function(res) {

        var prPrimaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var jtPrimaryProfession = res.value.objAutoJTProfession.PrimaryProfession;
        var isJoint = res.value.objAutoLoanMaster.JointApplication;

        if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
            incomeAssmentHelper.populatePrBussinessManBankStatementData(res);
        }
        if (jtPrimaryProfession == 1 && isJoint == true) {
            incomeAssmentHelper.populateJtBussinessManBankStatementData(res);
        }
        if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6) {
            incomeAssmentHelper.populatePrSelfEmployedBankStatementData(res);
        }
        if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
            incomeAssmentHelper.populateJtSelfEmployedBankStatementData(res);
        }
    },

    populatePrBussinessManBankStatementData: function(res) {

        var idFieldForBankStatementForIncomegenerator = 0;
        for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {
            idFieldForBankStatementForIncomegenerator = i + 1;
            if (idFieldForBankStatementForIncomegenerator < 7) {
                $('txtAccountNameBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].AccountName);
                $('txtBankStatementIdBMPr' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].PrBankStatementId);
                $('txtAccountNumberBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].AccountNumber);
                var accounttypeName = AccountType[res.value.AutoPRBankStatementList[i].AccountCategory];
                $('txtAccountTypeBank' + idFieldForBankStatementForIncomegenerator).setValue(accounttypeName);
                $('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').update(Page.bankLinkOption);
                $('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').setValue(res.value.AutoPRBankStatementList[i].BankId);

                var bankName = $('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').options[$('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').selectedIndex].text;
                $("bmprtabBank" + idFieldForBankStatementForIncomegenerator).innerHTML = bankName;

                $('txtbmprStatementBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].Statement);




                incomeAssmentManager.GetBranchNameForIncomeGenerator('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator', 'cmbBranchNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator');
                $('cmbBranchNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').setValue(res.value.AutoPRBankStatementList[i].BranchId);

                if (res.value.AutoPRBankStatementList[i].AccountCategory == 2) {
                    $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                }
                else if (res.value.AutoPRBankStatementList[i].AccountCategory != 2 && res.value.objAutoPRProfession.EquityShare < 100) {
                    if (res.value.objAutoPRProfession.EquityShare < 10) {
                        //$('cmbLabelForDlaLimit').setValue('3');
                    }
                    $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                    var objPrBm = new Object();
                    objPrBm.BankNo = idFieldForBankStatementForIncomegenerator;
                    objPrBm.isAverageBalance = false;
                    objPrBm.Accounttype = "Pr";
                    objPrBm.TabName = "Bm";
                    Page.isAverageBalancePrimaryArray.push(objPrBm);
                }
                else {
                    $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                    $("lblShareForBMPr" + idFieldForBankStatementForIncomegenerator).innerHTML = "";
                    $("lblForAvgBalBMPr" + idFieldForBankStatementForIncomegenerator).innerHTML = "";
                }

                incomeAssmentHelper.populateAverageTotalForIncomeGeneratorBussinessManbankStatement(res.value.AutoPRBankStatementList[i].BankId, res.value.AutoPRBankStatementList[i].AccountNumber, "Primary", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementForIncomegenerator);
            }
        }
    },

    populateAverageTotalForIncomeGeneratorBussinessManbankStatement: function(bankId, accountNumber, typeName, autoBankStatementAvgTotalList, bankNoCount) {

        var j = 0;
        var k = 0;
        var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var averageBalanceFor12Month = 0;
        var averageBalanceFor6Month = 0;
        var sixMonthArrayForCto = [];
        var sixMonthArrayForAverageBalance = [];

        if (typeName == "Primary") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 1) {
                    j += 1;
                    var d = new Date(autoBankStatementAvgTotalList[i].Month);
                    var newDate = monthNames[d.getMonth()] + "-" + d.getFullYear();
                    $("hdn" + j + "ForBMPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtMonth" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(newDate);
                    $("txtcreditTurnOver" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAvarageBalance" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                }
            }
            $("txtTotalCTOFor12MonthBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForCto.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                        totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                    }
                }

            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForAverageBalance.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForAverageBalance[(sixMonthArrayForCto.length - 1) - s] != "") {
                        averageBalanceFor6Month += sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s];
                    }
                }

            }
            $("txtTotalCTOFor6MonthBank" + bankNoCount).setValue(Math.round(totalCtofor6Month));
            var avergTotalCto12Month = Math.round((totalCtofor12Month / 12));
            var avergTotalCto6Month = Math.round((totalCtofor6Month / 6));
            $("txtAverageCTO12MonthBank" + bankNoCount).setValue(avergTotalCto12Month);
            $("txtAverageCTO6MonthBank" + bankNoCount).setValue(avergTotalCto6Month);

            var avgBalanceFor12 = Math.round(averageBalanceFor12Month / 12);
            var avgBalanceFor6 = Math.round(averageBalanceFor6Month / 6);

            $("txtAverageBalanceFor12MonthBank" + bankNoCount).setValue(avgBalanceFor12);
            $("txtAverageBalanceFor6MonthBank" + bankNoCount).setValue(avgBalanceFor6);

            //var linkCto = "<option value='" + Math.round(totalCtofor12Month.toFixed(2)) + "'>" + Math.round(totalCtofor12Month.toFixed(2)) + "</option><option value='" + Math.round(totalCtofor6Month.toFixed(2)) + "'>" + Math.round(totalCtofor6Month.toFixed(2)) + "</option>";
            var linkCto = "<option value='" + Math.round(avergTotalCto12Month.toFixed(2)) + "'>" + Math.round(avergTotalCto12Month.toFixed(2)) + "</option><option value='" + Math.round(avergTotalCto6Month.toFixed(2)) + "'>" + Math.round(avergTotalCto6Month.toFixed(2)) + "</option>";
            //var linkAvg = "<option value='" + avergTotalCto12Month + "'>" + avergTotalCto12Month + "</option><option value='" + avergTotalCto6Month + "'>" + avergTotalCto6Month + "</option>";
            var linkAvg = "<option value='" + avgBalanceFor12 + "'>" + avgBalanceFor12 + "</option><option value='" + avgBalanceFor6 + "'>" + avgBalanceFor6 + "</option>";

            $("cmbConsideredCtoForPrBank" + bankNoCount).update(linkCto);
            $("cmbConsideredBalanceForPrBank" + bankNoCount).update(linkAvg);

            $("cmbConsideredCtoForPrBank" + bankNoCount).setValue(Math.round(totalCtofor12Month.toFixed(2)));
            $("cmbConsideredBalanceForPrBank" + bankNoCount).setValue(Math.round(avergTotalCto12Month));
            $("txtConsiderMarginForPrimaryBank" + bankNoCount).setValue('3');
            var grossincome = parseFloat((totalCtofor12Month * 3) / 100);
            $("txtgrossincomeForPrimaryBank" + bankNoCount).setValue(Math.round(grossincome.toFixed(2)));
            $("txtNetAfterPaymentForPrimaryBank" + bankNoCount).setValue(Math.round(grossincome.toFixed(2)));
            var share = $F("txtShareForPrimaryBank" + bankNoCount);

            var netIncomeTotal = parseFloat((grossincome * parseFloat(share)) / 100);

            $("txtNetincomeForPrimaryBank" + bankNoCount).setValue(Math.round(netIncomeTotal.toFixed(2)));
            $("cmbIncomeAssessmentForPrimaryBank" + bankNoCount).update(Page.incomeAssesmentLinkOption);
            $("cmbIndustryForPrimaryBank" + bankNoCount).update(Page.sectorLinkOption);

            incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNoCount, 'Pr', 'BM');



        }
        else if (typeName == "Joint") {

            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 2) {
                    k += 1;
                    if (k < 13) {
                        $("hdn" + k + "ForBMJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                        $("txtmonthName" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Month);
                        $("txtcreditturnOver" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                        $("txtAverageBalance" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                        $("txtAdjustment" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                        totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                        averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                        sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                        sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                    }
                    else {
                        k = 0;
                    }
                }
            }
            $("txtTotalCTOFor12MonthJointBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));

            //Code Comment by Washik 19/11/2012

            //            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            //            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });
            //            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            //                if (sixMonthArrayForCto[s] != "") {
            //                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            //                }
            //            }
            //            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
            //                if (sixMonthArrayForAverageBalance[s] != "") {
            //                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
            //                }
            //            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForCto.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                        totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                    }
                }

            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForAverageBalance.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s] != "") {
                        averageBalanceFor6Month += sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s];
                    }
                }

            }


            $("txtTotalCTOFor6MonthJointBank" + bankNoCount).setValue(Math.round(totalCtofor6Month));
            var avergTotalCto12Monthjoint = Math.round((totalCtofor12Month / 12));
            var avergTotalCto6Monthjoint = Math.round((totalCtofor6Month / 6));
            $("txtAverageCTOFor12MonthForJointBank" + bankNoCount).setValue(avergTotalCto12Monthjoint);
            $("txtAverageCTOFor6MonthForJointBank" + bankNoCount).setValue(avergTotalCto6Monthjoint);



            var avgBalanceFor12 = Math.round(averageBalanceFor12Month / 12);
            var avgBalanceFor6 = Math.round(averageBalanceFor6Month / 6);

            $("txtAverageBalance12MonthForJointBank" + bankNoCount).setValue(avgBalanceFor12);
            $("txtAverageBalance6MonthForJointBank" + bankNoCount).setValue(avgBalanceFor6);

            //var linkCtoJoint = "<option value='" + Math.round(totalCtofor12Month) + "'>" + Math.round(totalCtofor12Month) + "</option><option value='" + Math.round(totalCtofor6Month) + "'>" + Math.round(totalCtofor6Month) + "</option>";
            var linkCtoJoint = "<option value='" + Math.round(avergTotalCto12Monthjoint) + "'>" + Math.round(avergTotalCto12Monthjoint) + "</option><option value='" + Math.round(avergTotalCto6Monthjoint) + "'>" + Math.round(avergTotalCto6Monthjoint) + "</option>";
            //var linkAvgJoint = "<option value='" + avergTotalCto12Monthjoint + "'>" + avergTotalCto12Monthjoint + "</option><option value='" + avergTotalCto6Monthjoint + "'>" + avergTotalCto6Monthjoint + "</option>";

            var linkAvgJoint = "<option value='" + avgBalanceFor12 + "'>" + avgBalanceFor12 + "</option><option value='" + avgBalanceFor6 + "'>" + avgBalanceFor6 + "</option>";

            $("cmbConsideredCtoForJtBank" + bankNoCount).update(linkCtoJoint);
            $("cmbConsideredBalanceForJtBank" + bankNoCount).update(linkAvgJoint);
            $("cmbConsideredCtoForJtBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));
            $("cmbConsideredBalanceForJtBank" + bankNoCount).setValue(Math.round(avergTotalCto12Monthjoint));
            $("txtConsideredMarginBMForJointBank" + bankNoCount).setValue('3');
            var grossJtincome = parseFloat((totalCtofor12Month * 3) / 100);
            $("txtgrossIncomeForJointBank" + bankNoCount).setValue(Math.round(grossJtincome.toFixed(2)));
            $("txtNetAfterPaymentForJointBank" + bankNoCount).setValue(Math.round(grossJtincome.toFixed(2)));
            var shareJt = $F("txtShareForJointBank" + bankNoCount);

            var netIncomeTotalJt = parseFloat((grossJtincome * parseFloat(shareJt)) / 100);

            $("txtNetIncomeForJointBank" + bankNoCount).setValue(Math.round(netIncomeTotalJt.toFixed(2)));

            $("cmbIncomeAssementForJointBank" + bankNoCount).update(Page.incomeAssesmentLinkOption);
            $("cmbIndustryForJointBank" + bankNoCount).update(Page.sectorLinkOption);
            incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNoCount, 'Jt', 'BM');
        }

    },

    setBankForBussinessManAndSelfEmployedCalculation: function(bankno, accountype, tabName) {

        var income = 0;
        var assessmentName = "";
        var assesmentCode = "";
        var employerName = "";
        var employerSubsector = "";
        var employerCode = "";
        var consider = 0;
        var bankName = "";
        var bankId = "0";
        var assMethodId = "0";
        var assCatId = "0";
        var averageBalance = 0;

        if (accountype == "Pr" && tabName == "BM") {
            income = $F("txtNetincomeForPrimaryBank" + bankno);
            $('txtIncomeBForLCPrBank' + bankno).setValue(income);

            averageBalance = $F("txtAverageBalanceFor12MonthBank" + bankno);
            $('txtAverageBalanceIdForLcPrBank' + bankno).setValue(averageBalance);

            if ($F('cmbIncomeAssessmentForPrimaryBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForPrimaryBank' + bankno).options[$('cmbIncomeAssessmentForPrimaryBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCPrBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForPrimaryBank' + bankno);
            $('txtAssCodeForLCPrBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForPrimaryBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForPrimaryBank' + bankno).options[$('cmbIndustryForPrimaryBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForPrimaryBank' + bankno).options[$('cmbSubsectorForPrimaryBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForPrimaryBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForPrimaryBank" + bankno);
            $('txtEmployerCatCodeForLCPrBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCPrBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForBank' + bankno + 'IncomeGenerator').options[$('cmbBankNameForBank' + bankno + 'IncomeGenerator').selectedIndex].text;
            $('tdBank' + bankno + 'ForLCPr').innerHTML = bankName;

            bankId = $F('cmbBankNameForBank' + bankno + 'IncomeGenerator');
            $('txtbankIdForLcPrBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForPrimaryBank' + bankno);
            $('txtAssMethodIdForLcPrBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForPrimaryBank' + bankno);
            $('txtAssCatIdForLcPrBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorPr').style.display = "";

        }
        else if (accountype == "Jt" && tabName == "BM") {
            income = $F("txtNetIncomeForJointBank" + bankno);
            $('txtIncomeBForLCJtBank' + bankno).setValue(income);
            if ($F('cmbIncomeAssementForJointBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssementForJointBank' + bankno).options[$('cmbIncomeAssementForJointBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCJtBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssementCodeForJointBank' + bankno);
            $('txtAssCodeForLCJtBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForJointBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForJointBank' + bankno).options[$('cmbIndustryForJointBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubSectorForJointBank' + bankno).options[$('cmbSubSectorForJointBank' + bankno).selectedIndex].text;
                if ($F('cmbSubSectorForJointBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtSubSectorCode1ForJointBank" + bankno);
            $('txtEmployerCatCodeForLCJtBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCJtBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankName1ForJointBank' + bankno).options[$('cmbBankName1ForJointBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCJt').innerHTML = bankName;
            $("bmjttabBank" + bankno).innerHTML = bankName;

            bankId = $F('cmbBankName1ForJointBank' + bankno);
            $('txtbankIdForLcJtBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssementForJointBank' + bankno);
            $('txtAssMethodIdForLcJtBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForJointBank' + bankno);
            $('txtAssCatIdForLcJtBank' + bankno).setValue(assCatId);
            $('trBank' + bankno + 'ForLoanCalculatorJt').style.display = "";
        }
        else if (accountype == "Pr" && tabName == "SE") {
            income = $F("txtNetincomeForSEPrBank" + bankno);
            $('txtIncomeBForLCPrBank' + bankno).setValue(income);

            averageBalance = $F("txtAverageBalanceForSEPr12MonthBank" + bankno);
            $('txtAverageBalanceIdForLcPrBank' + bankno).setValue(averageBalance);

            if ($F('cmbIncomeAssessmentForSEPrBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForSEPrBank' + bankno).options[$('cmbIncomeAssessmentForSEPrBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCPrBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForSEPrBank' + bankno);
            $('txtAssCodeForLCPrBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForSEPrBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForSEPrBank' + bankno).options[$('cmbIndustryForSEPrBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForSEPrBank' + bankno).options[$('cmbSubsectorForSEPrBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForSEPrBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForSEPrBank" + bankno);
            $('txtEmployerCatCodeForLCPrBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCPrBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForSEBank' + bankno).options[$('cmbBankNameForSEBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCPr').innerHTML = bankName;

            bankId = $F('cmbBankNameForSEBank' + bankno);
            $('txtbankIdForLcPrBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForSEPrBank' + bankno);
            $('txtAssMethodIdForLcPrBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForSEPrBank' + bankno);
            $('txtAssCatIdForLcPrBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorPr').style.display = "";
        }
        else if (accountype == "Jt" && tabName == "SE") {
            income = $F("txtNetincomeForSEJtBank" + bankno);
            $('txtIncomeBForLCJtBank' + bankno).setValue(income);
            if ($F('cmbIncomeAssessmentForSEJtBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForSEJtBank' + bankno).options[$('cmbIncomeAssessmentForSEJtBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCJtBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForSEJtBank' + bankno);
            $('txtAssCodeForLCJtBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForSEJtBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForSEJtBank' + bankno).options[$('cmbIndustryForSEJtBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForSEJtBank' + bankno).options[$('cmbSubsectorForSEJtBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForSEJtBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForSEJtBank" + bankno);
            $('txtEmployerCatCodeForLCJtBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCJtBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCJtBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForSEJtBank' + bankno).options[$('cmbBankNameForSEJtBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCJt').innerHTML = bankName;

            bankId = $F('cmbBankNameForSEJtBank' + bankno);
            $('txtbankIdForLcJtBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForSEJtBank' + bankno);
            $('txtAssMethodIdForLcJtBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForSEJtBank' + bankno);
            $('txtAssCatIdForLcJtBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorJt').style.display = "";
        }

        //IncomeAssementPrintCalculationHelper.CalculateLoanCalculator(accountype);


    },

    populateBranchForIncomeGeneratorCombo: function(res, elementId) {
        var link = "<option value=\"-1\">Select a Branch</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BRAN_ID + "\">" + res.value[i].BRAN_NAME + "</option>";
        }
        $(elementId).update(link);
    },

    populateJtBussinessManBankStatementData: function(res) {
        var idFieldForBankStatementjtForIncomegenerator = 0;

        for (var i = 0; i < res.value.AutoJTBankStatementList.length; i++) {
            idFieldForBankStatementjtForIncomegenerator = i + 1;
            if (idFieldForBankStatementjtForIncomegenerator < 7) {
                $('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).update(Page.bankLinkOption);
                $('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].BankId);
                $('txtAccountName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].AccountName);
                $('txtAccountNumber1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].AccountNumber);
                $('txtBankStatementIdBMJt' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].JtBankStatementId);
                var accounttypeNamejt = AccountType[res.value.AutoJTBankStatementList[i].AccountCategory];
                $('txtCompanyName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(accounttypeNamejt);


                var bankName = $('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).options[res.value.AutoJTBankStatementList[i].BankId].text;
                $("bmjttabBank" + idFieldForBankStatementjtForIncomegenerator).innerHTML = bankName;

                $('txtbmjtStatementBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].Statement);

                //IncomeAssementPrintManager.GetBranchNameForIncomeGenerator('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator, 'cmbBranchName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator);
                incomeAssmentManager.GetBranchNameForIncomeGeneratorJoint(res.value.AutoJTBankStatementList[i].BankId, 'cmbBranchName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator);
                $('cmbBranchName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].BranchId);

                if (res.value.AutoJTBankStatementList[i].AccountCategory == 2) {
                    $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                }
                else if (res.value.AutoJTBankStatementList[i].AccountCategory != 2 && res.value.objAutoJTProfession.EquityShare < 100) {
                    if (res.value.objAutoJTProfession.EquityShare < 10) {
                        //$('cmbLabelForDlaLimit').setValue('3');
                    }
                    $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                    var objJtBm = new Object();
                    objJtBm.BankNo = idFieldForBankStatementjtForIncomegenerator;
                    objJtBm.isAverageBalance = false;
                    objJtBm.Accounttype = "Jt";
                    objJtBm.TabName = "Bm";
                    Page.isAverageBalanceJointArray.push(objJtBm);
                }
                else {
                    $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                    $("lblShareForBMJt" + idFieldForBankStatementjtForIncomegenerator).innerHTML = "";
                    $("lblForAvgBalBMJt" + idFieldForBankStatementjtForIncomegenerator).innerHTML = "";
                }

                incomeAssmentHelper.populateAverageTotalForIncomeGeneratorBussinessManbankStatement(res.value.AutoJTBankStatementList[i].BankId, res.value.AutoJTBankStatementList[i].AccountNumber, "Joint", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementjtForIncomegenerator);
            }
        }
    },

    populatePrSelfEmployedBankStatementData: function(res) {
        var idFieldForBankStatementForSePr = 0;
        var prPrimaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {

            idFieldForBankStatementForSePr = i + 1;
            if (idFieldForBankStatementForSePr < 7) {
                $('txtAccountNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].AccountName);
                $('txtAccountNumberForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].AccountNumber);
                $('txtBankStatementIdSEPr' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].PrBankStatementId);
                var accounttypeSePrName = AccountType[res.value.AutoPRBankStatementList[i].AccountCategory];
                $('txtAccountTypeForSEBank' + idFieldForBankStatementForSePr).setValue(accounttypeSePrName);
                $('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).update(Page.bankLinkOption);
                $('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].BankId);
                incomeAssmentManager.GetBranchNameForIncomeGenerator('cmbBankNameForSEBank' + idFieldForBankStatementForSePr, 'cmbbranchNameForSEBank' + idFieldForBankStatementForSePr);
                $('cmbbranchNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].BranchId);

                var bankName = $('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).options[$('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).selectedIndex].text;
                $("seprtabBank" + idFieldForBankStatementForSePr).innerHTML = bankName;

                $('txtseprStatementBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].Statement);

                if (prPrimaryProfession == 4) {

                    $('cmbIndustryForSEPrBank' + idFieldForBankStatementForSePr).setValue('12');
                    incomeAssmentManager.changeSectorForSelfEmployed(idFieldForBankStatementForSePr, "Pr");
                    $('cmbSubsectorForSEPrBank' + idFieldForBankStatementForSePr).setValue('143');
                    var sub = Page.subsectorArray.findByProp('SUSE_ID', 143);
                    $('txtProfitMarginForSEPrBank' + idFieldForBankStatementForSePr).setValue(sub.SUSE_INTERESTRATE);
                    $('txtProfitMarginCodeForSEPrBank' + idFieldForBankStatementForSePr).setValue(sub.SUSE_IRCODE);

                }

                if (res.value.AutoPRBankStatementList[i].AccountCategory == 2) {
                    $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue(res.value.objAutoPRProfession.EquityShare);
                }
                else if (res.value.AutoPRBankStatementList[i].AccountCategory != 2 && res.value.objAutoPRProfession.EquityShare < 100) {
                    if (res.value.objAutoPRProfession.EquityShare < 10) {
                        //$('cmbLabelForDlaLimit').setValue('3');
                    }
                    $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue(res.value.objAutoPRProfession.EquityShare);
                    var objPrSe = new Object();
                    objPrSe.BankNo = idFieldForBankStatementForSePr;
                    objPrSe.isAverageBalance = false;
                    objPrSe.Accounttype = "Pr";
                    objPrSe.TabName = "Se";
                    Page.isAverageBalancePrimaryArray.push(objPrSe);
                }
                else {
                    $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue(res.value.objAutoPRProfession.EquityShare);
                    $("lblShareForSEPr" + idFieldForBankStatementForSePr).innerHTML = "";
                    $("lblForAvgBalSEPr" + idFieldForBankStatementForSePr).innerHTML = "";
                }

                incomeAssmentHelper.populateAverageTotalForSelfEmployedBankStatement(res.value.AutoPRBankStatementList[i].BankId, res.value.AutoPRBankStatementList[i].AccountNumber, "Primary", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementForSePr);
            }
        }
    },

    populateSubSectorComboForSelfEmployed: function(res, bankNo, accountType) {

        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }
        if (accountType == "Pr") {
            $("cmbSubsectorForSEPrBank" + bankNo).update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubsectorForSEJtBank" + bankNo).update(link);
        }
    },

    changePrivateInCome: function(bankNo, accountType, tabName) {
        if (accountType == "Pr" && tabName == "Se") {
            var industry = $F("cmbIndustryForSEPrBank" + bankNo);
            var subsector = $F("cmbSubsectorForSEPrBank" + bankNo);
            var privateIncome = $F("cmbPrivateIncomeForSEPrBank" + bankNo);
            if (industry == "12" && subsector == "143" && privateIncome == "1") {
                $("divTutionForSEPrBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'block';
                $("txtTotalInComeTutionForSEPrBank" + bankNo).setValue('0');
            }
            else if (industry == "12" && subsector == "150" && privateIncome == "1") {
                $("divTutionForSEPrBank" + bankNo).style.display = 'block';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEPrBank" + bankNo).setValue('0');
            }
            else {
                $("divTutionForSEPrBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEPrBank" + bankNo).setValue('0');
                $("txtTotalInComeTutionForSEPrBank" + bankNo).setValue('0');
            }
        }
        else if (accountType == "Jt" && tabName == "Se") {
            var industryForSeJt = $F("cmbIndustryForSEJtBank" + bankNo);
            var subsectorSeJt = $F("cmbSubsectorForSEJtBank" + bankNo);
            var privateIncomeSeJt = $F("cmbPrivateIncomeForSEJtBank" + bankNo);
            if (industryForSeJt == "12" && subsectorSeJt == "143" && privateIncomeSeJt == "1") {
                $("divTutionJtBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'block';
                $("txtTotalInComeTutionForSEJtBank" + bankNo).setValue('0');
            }
            else if (industryForSeJt == "12" && subsectorSeJt == "150" && privateIncomeSeJt == "1") {
                $("divTutionJtBank" + bankNo).style.display = 'block';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEJtBank" + bankNo).setValue('0');
            }
            else {
                $("divTutionJtBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'none';
                $("txtTotalInComeTutionForSEJtBank" + bankNo).setValue('0');
                $("txtTotalIncomeForSEJtBank" + bankNo).setValue('0');
            }
        }
        incomeAssmentHelper.checkDoctorChambers(accountType, 'DC');
        incomeAssmentHelper.checkDoctorChambers(accountType, 'TC');
    },

    checkDoctorChambers: function(accountType, tabName) {
        var isDoctor = false;
        var isTeacher = false;
        if (accountType == "Pr" && tabName == "DC") {
            for (var i = 1; i < Page.prCount; i++) {
                if ($F("cmbIndustryForSEPrBank" + i) == "12" && $F("cmbSubsectorForSEPrBank" + i) == "143" && $F("cmbPrivateIncomeForSEPrBank" + i) == "1") {
                    isDoctor = true;
                    break;
                }
            }
            if (isDoctor) {
                incomeAssmentHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trDoctorForLoanCalculatorPr').style.display = "none";
            }

        }
        else if (accountType == "Jt" && tabName == "DC") {
            for (var i = 1; i < Page.jtCount; i++) {
                if ($F("cmbIndustryForSEJtBank" + i) == "12" && $F("cmbSubsectorForSEJtBank" + i) == "143" && $F("cmbPrivateIncomeForSEJtBank" + i) == "1") {
                    isDoctor = true;
                    break;
                }
            }
            if (isDoctor) {
                incomeAssmentHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trDoctorForLoanCalculatorJt').style.display = "none";
            }

        }
        else if (accountType == "Pr" && tabName == "TC") {
            for (var i = 1; i < Page.prCount; i++) {
                if ($F("cmbIndustryForSEPrBank" + i) == "12" && $F("cmbSubsectorForSEPrBank" + i) == "150" && $F("cmbPrivateIncomeForSEPrBank" + i) == "1") {
                    isTeacher = true;
                    break;
                }
            }
            if (isTeacher) {
                incomeAssmentHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trTeacherForLoanCalculatorPr').style.display = "none";
            }

        }
        else if (accountType == "Jt" && tabName == "TC") {
            for (var i = 1; i < Page.jtCount; i++) {
                if ($F("cmbIndustryForSEJtBank" + i) == "12" && $F("cmbSubsectorForSEJtBank" + i) == "150" && $F("cmbPrivateIncomeForSEJtBank" + i) == "1") {
                    isTeacher = true;
                    break;
                }
            }
            if (isTeacher) {
                incomeAssmentHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trTeacherForLoanCalculatorJt').style.display = "none";
            }

        }
    },

    setBussinessManCalculationForDoctorAndTeachers: function(accountType, tabName) {
        var income = 0;
        var consider = 0;
        var identity = 0;

        if (accountType == "Pr" && tabName == "DC") {
            for (var i = 0; i < Page.prCount; i++) {
                identity = i + 1;
                if (util.isFloat($F('txtTotalIncomeForSEPrBank' + identity)) && !util.isEmpty($F('txtTotalIncomeForSEPrBank' + identity))) {
                    income += parseFloat($F('txtTotalIncomeForSEPrBank' + identity));
                }
            }
            $('txtIncomeBForLCPrDoctor').setValue(income);
            $('txtInComeAssMethLCPrDoctor').setValue("Doctor");
            $('txtAssCodeForLCPrDoctor').setValue('FD');
            $('txtAssMethodIdForLcPrDoc').setValue('4');
            $('txtAssCatIdForLcPrDoc').setValue('12');
            $('txtEmployerCategoryForLCPrDoctor').setValue('Professional - Doctors');
            $('txtEmployerCatCodeForLCPrDoctor').setValue('YA1');
            consider = $F("cmbConsiderForLCPrDoctor");
            if (consider != "1") {
                $('cmbConsiderForLCPrDoctor').setValue("0");
            }
            $('trDoctorForLoanCalculatorPr').style.display = "";
        }
        else if (accountType == "Jt" && tabName == "DC") {
            for (var i = 0; i < Page.jtCount; i++) {
                identity = i + 1;
                if (util.isFloat($F('txtTotalIncomeForSEJtBank' + identity)) && !util.isEmpty($F('txtTotalIncomeForSEJtBank' + identity))) {
                    income += parseFloat($F('txtTotalIncomeForSEJtBank' + identity));
                }
            }
            $('txtIncomeBForLCJtDoctor').setValue(income);
            $('txtInComeAssMethLCJtDoctor').setValue("Doctor");
            $('txtAssCodeForLCJtDoctor').setValue('FD');
            $('txtEmployerCategoryForLCJtDoctor').setValue('Professional - Doctors');
            $('txtEmployerCatCodeForLCJtDoctor').setValue('YA1');
            $('txtAssMethodIdForLcJtDoc').setValue('4');
            $('txtAssCatIdForLcJtDoc').setValue('12');
            consider = $F("cmbConsiderForLCJtDoctor");
            if (consider != "1") {
                $('cmbConsiderForLCJtDoctor').setValue("0");
            }
            $('trDoctorForLoanCalculatorJt').style.display = "";
        }
        else if (accountType == "Pr" && tabName == "TC") {
            for (var i = 0; i < Page.prCount; i++) {
                identity = i + 1;
                if (util.isFloat($F('txtTotalInComeTutionForSEPrBank' + identity)) && !util.isEmpty($F('txtTotalInComeTutionForSEPrBank' + identity))) {
                    income += parseFloat($F('txtTotalInComeTutionForSEPrBank' + identity));
                }
            }
            $('txtIncomeBForLCPrTeacher').setValue(income);
            $('txtInComeAssMethLCPrTeacher').setValue("Teachers");
            $('txtAssCodeForLCPrTeacher').setValue('TS');
            $('txtEmployerCategoryForLCPrTeacher').setValue('Professional - Teachers');
            $('txtEmployerCatCodeForLCPrTeacher').setValue('YT1');
            $('txtAssMethodIdForLcPrTec').setValue('150');
            $('txtAssCatIdForLcPrTec').setValue('12');
            consider = $F("cmbConsiderForLCPrTeacher");
            if (consider != "1") {
                $('cmbConsiderForLCPrTeacher').setValue("0");
            }
            $('trTeacherForLoanCalculatorPr').style.display = "";
        }
        else if (accountType == "Jt" && tabName == "TC") {
            for (var i = 0; i < Page.jtCount; i++) {
                identity = i + 1;
                if (util.isFloat($F('txtTotalInComeTutionForSEJtBank' + identity)) && !util.isEmpty($F('txtTotalInComeTutionForSEJtBank' + identity))) {
                    income += parseFloat($F('txtTotalInComeTutionForSEJtBank' + identity));
                }
            }
            $('txtIncomeBForLCJtTeacher').setValue(income);
            $('txtInComeAssMethLCJtTeacher').setValue("Teachers");
            $('txtAssCodeForLCJtTeacher').setValue('TS');
            $('txtAssMethodIdForLcJtTec').setValue('150');
            $('txtAssCatIdForLcJtTec').setValue('12');
            $('txtEmployerCategoryForLCJtTeacher').setValue('Professional - Teachers');
            $('txtEmployerCatCodeForLCJtTeacher').setValue('YT1');
            consider = $F("cmbConsiderForLCJtTeacher");
            if (consider != "1") {
                $('cmbConsiderForLCJtTeacher').setValue("0");
            }
            $('trTeacherForLoanCalculatorJt').style.display = "";
        }
        //IncomeAssementPrintCalculationHelper.CalculateLoanCalculator(accountType);
    },

    populateAverageTotalForSelfEmployedBankStatement: function(bankId, accountNumber, typeName, autoBankStatementAvgTotalList, bankNoCount) {

        var j = 0;
        var k = 0;
        var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var averageBalanceFor12Month = 0;
        var averageBalanceFor6Month = 0;
        var sixMonthArrayForCto = [];
        var sixMonthArrayForAverageBalance = [];
        if (typeName == "Primary") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 1) {
                    j += 1;
                    var d = new Date(autoBankStatementAvgTotalList[i].Month);
                    var newDate = monthNames[d.getMonth()] + "-" + d.getFullYear();
                    $("hdn" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtMonth" + j + "ForSEPrBank" + bankNoCount).setValue(newDate);
                    $("txtCreditTurnOver" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAverageBalance" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                }
            }
            $("txtTotalCTOForSEPr12MonthBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));

            //Code comment by Washik 19/11/2012

            //            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            //            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });

            //            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            //                if (sixMonthArrayForCto[s] != "") {
            //                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            //                }
            //            }
            //            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
            //                if (sixMonthArrayForAverageBalance[s] != "") {
            //                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
            //                }
            //            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForCto.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                        totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                    }
                }

            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForAverageBalance.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s] != "") {
                        averageBalanceFor6Month += sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s];
                    }
                }

            }

            $("txtTotalCTOForSEPr6MonthBank" + bankNoCount).setValue(Math.round(totalCtofor6Month));
            var avergTotalCto12Month = Math.round((totalCtofor12Month / 12));
            var avergTotalCto6Month = Math.round((totalCtofor6Month / 6));
            $("txtAverageCTOForSEPr12MonthBank" + bankNoCount).setValue(avergTotalCto12Month);
            $("txtAverageCTOForSEPr6MonthBank" + bankNoCount).setValue(avergTotalCto6Month);

            var avgBalanceFor12 = Math.round(averageBalanceFor12Month / 12);
            var avgBalanceFor6 = Math.round(averageBalanceFor6Month / 6);

            $("txtAverageBalanceForSEPr12MonthBank" + bankNoCount).setValue(avgBalanceFor12);
            $("txtAverageBalanceForSEPr6MonthBank" + bankNoCount).setValue(avgBalanceFor6);

            //var linkCto = "<option value='" + Math.round(totalCtofor12Month.toFixed(2)) + "'>" + Math.round(totalCtofor12Month.toFixed(2)) + "</option><option value='" + Math.round(totalCtofor6Month.toFixed(2)) + "'>" + Math.round(totalCtofor6Month.toFixed(2)) + "</option>";
            var linkCto = "<option value='" + Math.round(avergTotalCto12Month.toFixed(2)) + "'>" + Math.round(avergTotalCto12Month.toFixed(2)) + "</option><option value='" + Math.round(avergTotalCto6Month.toFixed(2)) + "'>" + Math.round(avergTotalCto6Month.toFixed(2)) + "</option>";
            //var linkAvg = "<option value='" + avergTotalCto12Month + "'>" + avergTotalCto12Month + "</option><option value='" + avergTotalCto6Month + "'>" + avergTotalCto6Month + "</option>";
            var linkAvg = "<option value='" + avgBalanceFor12 + "'>" + avgBalanceFor12 + "</option><option value='" + avgBalanceFor6 + "'>" + avgBalanceFor6 + "</option>";

            $("cmbConsideredCtoForSEPrBank" + bankNoCount).update(linkCto);
            $("cmbConsideredBalanceForSEPrBank" + bankNoCount).update(linkAvg);

            $("cmbConsideredCtoForSEPrBank" + bankNoCount).setValue(Math.round(totalCtofor12Month.toFixed(2))); //E35
            $("cmbConsideredBalanceForSEPrBank" + bankNoCount).setValue(avergTotalCto12Month);
            $("txtConsiderMarginForSEPrBank" + bankNoCount).setValue('3'); //E37

            //=IF(P31="FT",P37,E35*E37)
            var grossincome = parseFloat((totalCtofor12Month * 3) / 100);
            $("txtgrossincomeForSEPrBank" + bankNoCount).setValue(Math.round(grossincome.toFixed(2)));
            $("txtNetAfterPaymentForSEPrBank" + bankNoCount).setValue(Math.round(grossincome.toFixed(2)));
            var share = $F("txtShareForSEPrBank" + bankNoCount);

            var netIncomeTotal = parseFloat((grossincome * parseFloat(share)) / 100);

            $("txtNetincomeForSEPrBank" + bankNoCount).setValue(Math.round(netIncomeTotal.toFixed(2)));
            $("cmbIncomeAssessmentForSEPrBank" + bankNoCount).update(Page.incomeAssesmentLinkOption);
            $("cmbIndustryForSEPrBank" + bankNoCount).update(Page.sectorLinkOption);

            incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNoCount, 'Pr', 'SE');

        }
        else if (typeName == "Joint") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 2) {
                    k += 1;
                    $("txtMonth" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Month);
                    $("hdn" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtCreditTurnOver" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAverageBalance" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                }
            }

            $("txtTotalCTOForSEJt12MonthBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));

            //Code Comment by Washik 19/11/2012
            //            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            //            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });
            //            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            //                if (sixMonthArrayForCto[s] != "") {
            //                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            //                }
            //            }
            //            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
            //                if (sixMonthArrayForAverageBalance[s] != "") {
            //                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
            //                }
            //            }
            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForCto.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                        totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                    }
                }

            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForAverageBalance.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s] != "") {
                        averageBalanceFor6Month += sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s];
                    }
                }

            }


            $("txtTotalCTOForSEJt6MonthBank" + bankNoCount).setValue(Math.round(totalCtofor6Month));
            var avergTotalCto12Monthjoint = Math.round((totalCtofor12Month / 12));
            var avergTotalCto6Monthjoint = Math.round((totalCtofor6Month / 6));
            $("txtAverageCTOForSEJt12MonthBank" + bankNoCount).setValue(avergTotalCto12Monthjoint);
            $("txtAverageCTOForSEJt6MonthBank" + bankNoCount).setValue(avergTotalCto6Monthjoint);

            var avgBalanceFor12 = Math.round(averageBalanceFor12Month / 12);
            var avgBalanceFor6 = Math.round(averageBalanceFor6Month / 6);

            $("txtAverageBalanceForSEJt12MonthBank" + bankNoCount).setValue(avgBalanceFor12);
            $("txtAverageBalanceForSEJt6MonthBank" + bankNoCount).setValue(avgBalanceFor6);

            //var linkCtoJoint = "<option value='" + totalCtofor12Month.toFixed(2) + "'>" + totalCtofor12Month.toFixed(2) + "</option><option value='" + totalCtofor6Month.toFixed(2) + "'>" + totalCtofor6Month.toFixed(2) + "</option>";
            var linkCtoJoint = "<option value='" + avergTotalCto12Monthjoint.toFixed(2) + "'>" + avergTotalCto12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalCto6Monthjoint.toFixed(2) + "'>" + avergTotalCto6Monthjoint.toFixed(2) + "</option>";
            //var linkAvgJoint = "<option value='" + avergTotalCto12Monthjoint + "'>" + avergTotalCto12Monthjoint + "</option><option value='" + avergTotalCto6Monthjoint + "'>" + avergTotalCto6Monthjoint + "</option>";
            var linkAvgJoint = "<option value='" + avgBalanceFor12 + "'>" + avgBalanceFor12 + "</option><option value='" + avgBalanceFor6 + "'>" + avgBalanceFor6 + "</option>";
            $("cmbConsideredCtoForSEJtBank" + bankNoCount).update(linkCtoJoint);
            $("cmbConsideredBalanceForSEJtBank" + bankNoCount).update(linkAvgJoint);

            $("cmbConsideredCtoForSEJtBank" + bankNoCount).setValue(Math.round(totalCtofor12Month.toFixed(2)));
            $("cmbConsideredBalanceForSEJtBank" + bankNoCount).setValue(Math.round(avergTotalCto12Monthjoint));
            $("txtConsiderMarginForSEJtBank" + bankNoCount).setValue("3");
            var grossJtincome = parseFloat((totalCtofor12Month * 3) / 100);
            $("txtgrossincomeForSEJtBank" + bankNoCount).setValue(Math.round(grossJtincome.toFixed(2)));
            $("txtNetAfterPaymentForSEJtBank" + bankNoCount).setValue(Math.round(grossJtincome.toFixed(2)));
            var shareJt = $F("txtShareForSEJtBank" + bankNoCount);
            var netIncomeTotalJt = parseFloat((grossJtincome * parseFloat(shareJt)) / 100);
            $("txtNetincomeForSEJtBank" + bankNoCount).setValue(Math.round(netIncomeTotalJt.toFixed(2)));
            $("cmbIncomeAssessmentForSEJtBank" + bankNoCount).update(Page.incomeAssesmentLinkOption);
            $("cmbIndustryForSEJtBank" + bankNoCount).update(Page.sectorLinkOption);

            incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNoCount, 'Jt', 'SE');
        }

    },

    populateJtSelfEmployedBankStatementData: function(res) {
        var idFieldForBankStatementjtForSe = 0;
        var jtPrimaryProfession = res.value.objAutoJTProfession.PrimaryProfession;
        for (var i = 0; i < res.value.AutoJTBankStatementList.length; i++) {
            idFieldForBankStatementjtForSe = i + 1;
            if (idFieldForBankStatementjtForSe < 7) {
                $('txtAccountNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].AccountName);
                $('txtAccountNumberForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].AccountNumber);
                $('txtBankStatementIdSEJt' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].JtBankStatementId);
                var accounttypeNamejtForSe = AccountType[res.value.AutoJTBankStatementList[i].AccountCategory];
                $('txtAccountTypeForSEJtBank' + idFieldForBankStatementjtForSe).setValue(accounttypeNamejtForSe);
                $('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).update(Page.bankLinkOption);
                $('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].BankId);
                incomeAssmentManager.GetBranchNameForIncomeGenerator('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe, 'cmbbranchNameForSEJtBank' + idFieldForBankStatementjtForSe);
                $('cmbbranchNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].BranchId);


                var bankName = $('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).options[$('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).selectedIndex].text;
                $("sejtBank" + idFieldForBankStatementjtForSe).innerHTML = bankName;

                $('txtsejtStatementBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoPRBankStatementList[i].Statement);
                if (jtPrimaryProfession == 4) {

                    $('cmbIndustryForSEJtBank' + idFieldForBankStatementjtForSe).setValue('12');
                    incomeAssmentManager.changeSectorForSelfEmployed(idFieldForBankStatementjtForSe, 'Jt');
                    $('cmbSubsectorForSEJtBank' + idFieldForBankStatementjtForSe).setValue('143');
                    var sub = Page.subsectorArray.findByProp('SUSE_ID', 143);
                    $('txtProfitMarginForSEJtBank' + idFieldForBankStatementjtForSe).setValue(sub.SUSE_INTERESTRATE);
                    $('txtProfitMarginCodeForSEJtBank' + idFieldForBankStatementjtForSe).setValue(sub.SUSE_IRCODE);

                }


                if (res.value.AutoJTBankStatementList[i].AccountCategory == 2) {
                    $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.objAutoJTProfession.EquityShare);
                }
                else if (res.value.AutoJTBankStatementList[i].AccountCategory != 2 && res.value.objAutoJTProfession.EquityShare < 100) {
                    if (res.value.objAutoJTProfession.EquityShare < 10) {
                        //$('cmbLabelForDlaLimit').setValue('3');
                    }
                    $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.objAutoJTProfession.EquityShare);
                    var objJtSe = new Object();
                    objJtSe.BankNo = idFieldForBankStatementjtForSe;
                    objJtSe.isAverageBalance = false;
                    objJtSe.Accounttype = "Jt";
                    objJtSe.TabName = "Se";
                    Page.isAverageBalanceJointArray.push(objJtSe);
                }
                else {
                    $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.objAutoJTProfession.EquityShare);
                    $("lblShareForBMJt" + idFieldForBankStatementjtForSe).innerHTML = "";
                    $("lblForAvgBalSEJtBank" + idFieldForBankStatementjtForSe).innerHTML = "";
                }

                incomeAssmentHelper.populateAverageTotalForSelfEmployedBankStatement(res.value.AutoJTBankStatementList[i].BankId, res.value.AutoJTBankStatementList[i].AccountNumber, "Joint", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementjtForSe);
            }

        }
    },

    populateIncomBankStatementSum: function(res) {

        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        for (var i = 0; i < res.value.objAutoAnalystIncomBankStatementSum.length; i++) {
            //Populate Bussiness Man Tab
            if (res.value.objAutoAnalystIncomBankStatementSum[i].TabType == 1) {
                //Populate Bussiness Man Tab For Primary
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 1) {
                    incomeAssmentHelper.populateIncomBankStatementSumForPrBussinessMan(res, i);
                }
                //Populate Bussiness Man Tab For Joint
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 2 && isJoint == true) {
                    incomeAssmentHelper.populateIncomBankStatementSumForJtBussinessMan(res, i);
                }
            }
            //Populate Self Employed Tab
            if (res.value.objAutoAnalystIncomBankStatementSum[i].TabType == 2) {
                //Populate Self Employed Tab for Primary Tab
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 1) {
                    incomeAssmentHelper.populateIncomBankStatementSumForPrSelfEmployed(res, i);
                }
                //Populate Self Employed Tab For Joint Tab
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 2 && isJoint == true) {
                    incomeAssmentHelper.populateIncomBankStatementSumForJtSelfEmployed(res, i);
                }
            }
        }
    },

    populateIncomBankStatementSumForPrBussinessMan: function(res, i) {
        var identityFieldForBmPr = 0;

        for (var j = 0; j < Page.prCount; j++) {
            identityFieldForBmPr = j + 1;
            var bankStatementId = parseInt($F("txtBankStatementIdBMPr" + identityFieldForBmPr));
            if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                $('tarearemarksForIncomeGeneratorBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                $('txtTotalCTOFor12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                $('txtAverageCTO12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                $('txtAverageBalanceFor12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                $('txtTotalCTOFor6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                $('txtAverageCTO6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                $('txtAverageBalanceFor6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                $('cmbConsideredCtoForPrBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                $('cmbConsideredBalanceForPrBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                $('txtConsiderMarginForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                $('txtgrossincomeForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                $('txtNetAfterPaymentForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                $('txtShareForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                $('txtNetincomeForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                $('cmbIncomeAssessmentForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                var incomeAssBussPr = Page.incomeAssementArray.findByProp('INAM_ID', res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                if (incomeAssBussPr) {
                    $('txtIncomeAssessmentCodeForPrimaryBank' + identityFieldForBmPr).setValue(incomeAssBussPr.INAM_METHODCODE);
                }
                $('cmbIndustryForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                incomeAssmentManager.changeSector(identityFieldForBmPr, 'Pr');
                $('cmbSubsectorForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                incomeAssmentHelper.changeSubSector(identityFieldForBmPr, 'Pr');
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                    $('divAverageBalancePr' + identityFieldForBmPr).style.display = 'block';
                    $('txt1st9monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                    $('txt1st6MonthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                    $('txtLast6monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                    $('txtLast3monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                    $('txtConsiderednarginForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                }

                //Previous Repayment History Bussiness Man Tab IncomBankStatementSum 
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                    $('divPrevRepHis' + identityFieldForBmPr).style.display = 'block';
                    incomeAssmentHelper.PopulatePrevRePyHisPrBmIncomBankStatementSum(res, i, identityFieldForBmPr);
                }

                //populate Over Draft Bussiness Man Tab IncomBankStatementSum
                incomeAssmentHelper.PopulateOverDraftPrBmIncomBankStatementSum(res, i);

                incomeAssmentHelper.changeIncomeAssementForBusssinessMan(identityFieldForBmPr, 'Pr');
                break;
            }
        }
    },


    populateSubSectorCombo: function(res, bankNo, accountType) {

        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }

        if (accountType == "Pr") {
            $("cmbSubsectorForPrimaryBank" + bankNo).update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubSectorForJointBank" + bankNo).update(link);
        }
    },

    changeSubSector: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var subsectorPrimaryId = $F("cmbSubsectorForPrimaryBank" + bankNo);
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', subsectorPrimaryId);
            if (subsectorPrimary) {
                $('txtProfitMarginForPrimaryBank' + bankNo).setValue(subsectorPrimary.SUSE_INTERESTRATE);
                $('txtProfitMarginCodeForPrimaryBank' + bankNo).setValue(subsectorPrimary.SUSE_IRCODE);
            }
        }
        else if (accountType == "Jt") {
            var subsectorId = $F("cmbSubSectorForJointBank" + bankNo);
            var subsector = Page.subsectorArray.findByProp('SUSE_ID', subsectorId);
            if (subsector) {
                $('txtProfitMarginForJointBank' + bankNo).setValue(subsector.SUSE_INTERESTRATE);
                $('txtSubSectorCode1ForJointBank' + bankNo).setValue(subsector.SUSE_IRCODE);
            }
        }
        incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'BM');
    },

    PopulatePrevRePyHisPrBmIncomBankStatementSum: function(res, i, identityFieldForBmPr) {

        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {

            $('txttotalForOffSCBForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
            $('txtTotal1OnSCBRepHisForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
            $('txtTotalEMIClosingConsideredForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
            $('txtremarks1ForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                    var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                    if (parseInt(bankId) < 0) {
                        var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var emi = $F("txtEmi" + hs + "ForOnScbForPrimaryBank" + identityFieldForBmPr);
                            if (parseFloat(emi) == emidb) {
                                $('cmbConsider' + hs + "ForOnSCBReplHisForPrimaryBank" + identityFieldForBmPr).setValue('1');
                                break;
                            }
                        }

                    }
                    else {
                        var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var bankIdtxt = $F("txtBank" + hs + "BMPr" + identityFieldForBmPr);
                            if (parseInt(bankIdtxt) == bankId) {
                                $('cmbConsider' + hs + "ForPrimaryOffScbBank" + identityFieldForBmPr).setValue('1');
                                break;
                            }
                        }
                    }
                }
            }

        }
    },

    PopulateOverDraftPrBmIncomBankStatementSum: function(res, i) {
        var identityFieldForBmPr = 0;
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
            for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                identityFieldForBmPr = ov + 1;
                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                    var tablePrAccount = $("tblOverDraftFacilitiesForPrBMWithScb" + identityFieldForBmPr);
                    var rowCount = tablePrAccount.rows.length;
                    for (var t = 1; t <= rowCount - 1; t++) {
                        var interestovd = parseFloat($F("txtInterst" + t + "WithScbForBank" + identityFieldForBmPr)).toFixed(2);
                        var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForBank" + identityFieldForBmPr)).toFixed(2);
                        var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                        var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                        if (interestovd == interestDb && LimitAmount == limitdb) {
                            $('cmbConsider' + t + "ForBank" + identityFieldForBmPr).setValue('1');
                            break;
                        }
                    }
                }
                else {
                    var tablePrAccountOffScb = $("tblOverDraftFacilitiesForPrBMOffScb" + identityFieldForBmPr);
                    var rowCountOffScb = tablePrAccountOffScb.rows.length;
                    for (var t = 1; t <= rowCountOffScb - 1; t++) {
                        var bankId = parseFloat($F("cmbBankName" + t + "ForBussinessTabPrBank" + identityFieldForBmPr));
                        var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                        if (bankId == bankIdDb) {
                            $('cmbConsider' + t + "ForOffScbBank" + identityFieldForBmPr).setValue('1');
                            break;
                        }
                    }
                }
            }
        }
    },

    changeIncomeAssementForBusssinessMan: function(bankNo, accountType) {
        if (accountType == "Pr") {
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
            if (incomeAssBuss) {
                var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);

                $("txtIncomeAssessmentCodeForPrimaryBank" + bankNo).setValue(incomeAssBuss.INAM_METHODCODE);
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    //Code Comment by Washik 26/12/2012
                    //                    var averageBalancePr = false;

                    //                    for (var i = 0; i < Page.isAverageBalancePrimaryArray.length; i++) {
                    //                        if (Page.isAverageBalancePrimaryArray[i].BankNo == bankNo && Page.isAverageBalancePrimaryArray[i].Accounttype == accountType && Page.isAverageBalancePrimaryArray[i].TabName == "Bm") {
                    //                            averageBalancePr = true;
                    //                            break;
                    //                        }
                    //                    }
                    //                    if (averageBalancePr == false) {
                    var share = $F("txtShareForPrimaryBank");
                    if (parseInt(share) == 100) {
                        $('divPrevRepHis' + bankNo).style.display = 'none';
                        $('divAverageBalancePr' + bankNo).style.display = 'block';
                        incomeAssmentHelper.calculateAverageBalance(bankNo, accountType);
                    }
                    else {
                        alert("Average Balance is not applicable");
                        $('divPrevRepHis' + bankNo).style.display = 'block';
                        $('divAverageBalancePr' + bankNo).style.display = 'none';
                    }

                }
                else if (incomeAssBuss.INAM_METHODCODE == "FF") {
                    $('divAverageBalancePr' + bankNo).style.display = 'none';
                    $('divPrevRepHis' + bankNo).style.display = 'block';
                }
                else {
                    $('divAverageBalancePr' + bankNo).style.display = 'none';
                    $('divPrevRepHis' + bankNo).style.display = 'none';
                }
            }

        }
        else if (accountType == "Jt") {

            var incomeAssIdForBussJoint = $F("cmbIncomeAssementForJointBank" + bankNo);
            var incomeAssBussJoint = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBussJoint);
            if (incomeAssIdForBussJoint && incomeAssIdForBussJoint != undefined && incomeAssIdForBussJoint != "-1") {
                $("txtIncomeAssementCodeForJointBank" + bankNo).setValue(incomeAssBussJoint.INAM_METHODCODE);
                if (incomeAssBussJoint.INAM_METHODCODE == "FT") {
                    //Code Comment by Washik 26/12/2012
                    //                    var averageBalanceJt = false;

                    //                    for (var i = 0; i < Page.isAverageBalanceJointArray.length; i++) {
                    //                        if (Page.isAverageBalanceJointArray[i].BankNo == bankNo && Page.isAverageBalanceJointArray[i].Accounttype == accountType && Page.isAverageBalanceJointArray[i].TabName == "Bm") {
                    //                            averageBalanceJt = true;
                    //                            break;
                    //                        }
                    //                    }

                    //                    if (averageBalanceJt == false) {
                    var share = $F("txtShareForJointBank")
                    if (parseInt(share) == 100) {
                        $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                        $('divAverageBalanceJoint' + bankNo).style.display = 'block';
                        incomeAssmentHelper.calculateAverageBalance(bankNo, accountType);
                    }
                    else {
                        alert("Average Balance is not applicable");
                        $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                        $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                    }
                }
                else if (incomeAssBussJoint.INAM_METHODCODE == "FF") {
                    $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                    $('divPrevRepHisJoint' + bankNo).style.display = 'block';
                }
                else {
                    $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                    $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                }
            }
        }
        incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'BM');
    },

    calculateAverageBalance: function(bankNo, accountType) {
        var firstst9MonthTotal = 0;
        var first6MonthTotal = 0;
        var last6Month = 0;
        var last3month = 0;
        var j = 0;
        var monthArray = [];
        if (accountType == "Pr") {
            var tablePrAccount = $("tblPrBankStatementForIncomeGenerator" + bankNo);
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                if (util.isFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txt1st9monthForPrimaryBank" + bankNo).setValue(Math.round(firstst9MonthTotal / 9));
            $("txt1st6MonthForPrimaryBank" + bankNo).setValue(Math.round(first6MonthTotal / 6));
            $("txtLast6monthForPrimaryBank" + bankNo).setValue(Math.round(last6Month / 6));
            $("txtLast3monthForPrimaryBank" + bankNo).setValue(Math.round(last3month / 3));

            //Consider Margin

            var considerMargin1 = 0;
            var considerMargin2 = 0;
            var last3monthAvg = Math.round(last3month / 3);
            var first9monthAvg = Math.round(firstst9MonthTotal / 9);
            var first6monthAvg = Math.round(first6MonthTotal / 6);
            var last6monthAvg = Math.round(last6Month / 6);

            if (last3monthAvg >= first9monthAvg) {
                considerMargin1 = "25";
            }
            else {
                considerMargin1 = "15";
            }

            if (last6monthAvg >= first6monthAvg) {
                considerMargin2 = "33";
            }
            else {
                considerMargin2 = "20";
            }


            var considerMarginFinal = 0;
            if (considerMargin1 <= considerMargin2) {
                considerMarginFinal = considerMargin1;
            }
            else {
                considerMarginFinal = considerMargin2;
            }


            $("txtConsiderednarginForPrimaryBank" + bankNo).setValue(considerMarginFinal);
            $("txtConsiderMarginForPrimaryBank" + bankNo).setValue(considerMarginFinal);
            incomeAssmentHelper.changeConsiderForOverDraftWithScb(bankNo, "Pr", "BM");

        }
        else if (accountType == "Jt") {
            var tableJtAccount = $("tblJtBankStatementForIncomeGenerator" + bankNo);
            var rowCountJt = tableJtAccount.rows.length;
            for (var i = 1; i <= rowCountJt - 1; i++) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForJointBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txtAVGBalanceFor1st9MontForJointBank" + bankNo).setValue(Math.round(firstst9MonthTotal / 9));
            $("txtAVGBalanceFor1st6MontForJointBank" + bankNo).setValue(Math.round(first6MonthTotal / 6));
            $("txtAVGBalanceForLast6MontForJointBank" + bankNo).setValue(Math.round(last6Month / 6));
            $("txtAVGBalanceForLast3MontForJointBank" + bankNo).setValue(Math.round(last3month / 3));

            //Consider Margin
            var considerMarginForJoint1 = 0;
            var considerMarginForJoint2 = 0;

            var last3monthAvg = Math.round(last3month / 3);
            var first9monthAvg = Math.round(firstst9MonthTotal / 9);
            var first6monthAvg = Math.round(first6MonthTotal / 6);
            var last6monthAvg = Math.round(last6Month / 6);

            if (last3monthAvg >= first9monthAvg) {
                considerMarginForJoint1 = "25";
            }
            else {
                considerMarginForJoint1 = "15";
            }

            if (last6monthAvg >= first6monthAvg) {
                considerMarginForJoint2 = "33";
            }
            else {
                considerMarginForJoint2 = "20";
            }

            var considerMarginFinalForJoint = 0;
            if (considerMarginForJoint1 <= considerMarginForJoint2) {
                considerMarginFinalForJoint = considerMarginForJoint1;
            }
            else {
                considerMarginFinalForJoint = considerMarginForJoint2;
            }
            $("txtConsideredMarginForJointBank" + bankNo).setValue(considerMarginFinalForJoint);
            $("txtConsideredMarginBMForJointBank" + bankNo).setValue(considerMarginFinalForJoint);
            incomeAssmentHelper.changeConsiderForOverDraftWithScb(bankNo, "Jt", "BM");
        }
    },

    changeConsiderForOverDraftWithScb: function(bankNo, accountType, tabName) {
        var totalLimit = 0;
        var totalInterest = 0;

        if (accountType == "Pr" && tabName == "BM") {

            var tableBmPrBankDraftWithScb = $("tblOverDraftFacilitiesForPrBMWithScb" + bankNo);
            var rowCountPrBmWithScb = tableBmPrBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountPrBmWithScb - 1; i++) {
                var considerwithScbBmPr = $F("cmbConsider" + i + "ForBank" + bankNo);
                if (considerwithScbBmPr == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForBank' + bankNo));
                    }
                }
            }

            var tableBmPrBankDraftOffScb = $("tblOverDraftFacilitiesForPrBMWithScb" + bankNo);
            var rowCountPrBmOffScb = tableBmPrBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountPrBmOffScb - 1; i++) {
                var considerOffScbBmPr = $F("cmbConsider" + i + "ForOffScbBank" + bankNo);
                if (considerOffScbBmPr == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForOffSCBBank' + bankNo));
                    }
                }
            }
            $("txtTotallimitForOoverDraftBMPrBank" + bankNo).setValue(Math.round(totalLimit));
            $("txtTotalInterestForOverDraftBMPrBank" + bankNo).setValue(Math.round(totalInterest));



            var consideredCto = $F("cmbConsideredCtoForPrBank" + bankNo);
            var considerMargin = $F("txtConsiderMarginForPrimaryBank" + bankNo);
            var grossIncome = 0;
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            var share = $F("txtShareForPrimaryBank" + bankNo);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    if (parseInt(share) == 100) {
                        grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalanceFor12MonthBank" + bankNo)) * considerMargin) / 100));
                    }
                    else {
                        alert("Average balance is not applicable");
                        $('divAverageBalancePr' + bankNo).style.display = 'block';
                        $('txtConsiderMarginForPrimaryBank' + bankNo).disabled = "false";
                    }
                }
                else {
                    grossIncome = Math.round(parseFloat((parseFloat(consideredCto) * parseFloat(considerMargin)) / 100));
                }
            }
            else {
                grossIncome = Math.round(parseFloat((parseFloat(consideredCto) * parseFloat(considerMargin)) / 100));
            }
            $("txtgrossincomeForPrimaryBank" + bankNo).setValue(grossIncome);
            var netIncome = grossIncome - totalInterest;
            $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(netIncome);

            var netIncomeTotal = Math.round(parseFloat((netIncome * parseFloat(share)) / 100));
            $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal);

        }
        else if (accountType == "Jt" && tabName == "BM") {
            var tableBmJtBankDraftWithScb = $("tblOverDraftFacilitiesForJtBMWithScb" + bankNo);
            var rowCountJtBmWithScb = tableBmJtBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountJtBmWithScb - 1; i++) {
                var considerwithScbBmJt = $F("cmbInterest" + i + "ForJointBank" + bankNo);
                if (considerwithScbBmJt == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForJointBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForJointBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForJointBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForJointBank' + bankNo));
                    }
                }
            }

            var tableBmJtBankDraftOffScb = $("tblOverDraftFacilitiesForJtBMOffScb" + bankNo);
            var rowCountJtBmOffScb = tableBmJtBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountJtBmOffScb - 1; i++) {
                var considerOffScbBmJt = $F("cmbConsiderOffScbBM" + i + "ForJointBank" + bankNo);
                if (considerOffScbBmJt == "1") {
                    if (util.isFloat($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo));
                    }
                }
            }



            var considerMarginFinalForJoint = $F("txtConsideredMarginBMForJointBank" + bankNo);
            var shareJoint = $F("txtShareForJointBank" + bankNo);
            var incomeAssIdForBuss = $F("cmbIncomeAssementForJointBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    if (parseInt(shareJoint) == 100) {
                        grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalance12MonthForJointBank" + bankNo)) * considerMarginFinalForJoint) / 100));
                    }
                    else {
                        alert("Average balance is not applicable");
                        $('divAverageBalanceJoint' + bankNo).style.display = 'block';
                        $('txtConsideredMarginForJointBank' + bankNo).disabled = "false";
                    }

                }
                else {
                    grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100));
                }
            }
            else {
                grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100));
            }
            $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncome);
            var netIncomeJt = grossIncome - totalInterest;
            $("txtNetAfterPaymentForJointBank" + bankNo).setValue(netIncomeJt);



            var netIncomeTotalJoint = Math.round(parseFloat((netIncomeJt * parseFloat(shareJoint)) / 100));

            $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint);
        }
        else if (accountType == "Pr" && tabName == "SE") {
            var tableSePrBankDraftWithScb = $("tblOverDraftFacilitiesForPrSEWithScb" + bankNo);
            var rowCountPrSeWithScb = tableSePrBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountPrSeWithScb - 1; i++) {
                var considerwithScbSePr = $F("cmbConsider" + i + "WithScbForSEBank" + bankNo);
                if (considerwithScbSePr == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForSEBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForSEBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForSEBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForSEBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForSEBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForSEBank' + bankNo));
                    }
                }
            }

            var tableSePrBankDraftOffScb = $("tblOverDraftFacilitiesForPrSEOffScb" + bankNo);
            var rowCountPrSeOffScb = tableSePrBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountPrSeOffScb - 1; i++) {
                var considerOffScbSePr = $F("cmbConsider" + i + "ForSEOffScbBank" + bankNo);
                if (considerOffScbSePr == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo));
                    }
                }
            }
            var considerMarginFinalForSePr = $F("txtConsiderMarginForSEPrBank" + bankNo);
            var shareSepr = $F("txtShareForSEPrBank" + bankNo);
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEPrBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    if (parseInt(shareSepr) == 100) {
                        grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalanceForSEPr12MonthBank" + bankNo)) * considerMarginFinalForSePr) / 100));
                    }
                    else {
                        alert("Average balance is not applicable");
                        $('divAverageBalanceForSEPr' + bankNo).style.display = 'block';
                        $('txtConsiderMarginForSEPrBank' + bankNo).disabled = "false";
                    }
                }
                else {
                    grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinalForSePr) / 100));
                }
            }
            else {
                grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinalForSePr) / 100));
            }
            //var grossIncomeForSePr = parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinalForSePr) / 100);
            $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncome);
            var netIncomeSePr = grossIncome - totalInterest;
            $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(netIncomeSePr);



            var netIncomeTotalSePr = Math.round(parseFloat((netIncomeSePr * parseFloat(shareSepr)) / 100));

            $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotalSePr);
        }
        else if (accountType == "Jt" && tabName == "SE") {
            var tableSeJtBankDraftWithScb = $("tblOverDraftFacilitiesForJtSEWithScb" + bankNo);
            var rowCountJtSeWithScb = tableSeJtBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountJtSeWithScb - 1; i++) {
                var considerwithScbSeJt = $F("cmbConsider" + i + "WithScbForSEJtBank" + bankNo);
                if (considerwithScbSeJt == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo));
                    }
                }
            }

            var tableSeJtBankDraftOffScb = $("tblOverDraftFacilitiesForJtSEOffScbBank" + bankNo);
            var rowCountJtSeOffScb = tableSeJtBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountJtSeOffScb - 1; i++) {
                var considerOffScbSeJt = $F("cmbConsider" + i + "ForSEJtOffScbBank" + bankNo);
                if (considerOffScbSeJt == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo));
                    }
                }
            }
            var considerMarginFinalForSeJt = $F("txtConsiderMarginForSEJtBank" + bankNo);
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEJtBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    if (parseInt(shareSepr) == 100) {
                        grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalanceForSEJt12MonthBank" + bankNo)) * considerMarginFinalForSePr) / 100));
                    }
                    else {
                        alert("Average balance is not applicable");
                        $('divAverageBalanceForSEJt' + bankNo).style.display = 'block';
                        $('txtgrossincomeForSEJtBank' + bankNo).disabled = "false";
                    }

                }
                else {
                    grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForSePr) / 100));
                }
            }
            else {
                grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForSePr) / 100));
            }
            //var grossIncomeForSeJt = parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForSeJt) / 100);
            $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncome);
            var netIncomeSeJt = grossIncome - totalInterest;
            $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(netIncomeSeJt);

            var shareSeJt = $F("txtShareForSEJtBank" + bankNo);

            var netIncomeTotalSeJt = Math.round(parseFloat((netIncomeSeJt * parseFloat(shareSeJt)) / 100));

            $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalSeJt);
        }
    },

    populateIncomBankStatementSumForJtBussinessMan: function(res, i) {

        var identityFieldForBmJt = 0;
        for (var j = 0; j < Page.jtCount; j++) {
            identityFieldForBmJt = j + 1;
            var bankStatementId = parseInt($F("txtBankStatementIdBMJt" + identityFieldForBmJt));
            if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                //Main Object
                $('txtRemark1ForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                $('txtTotalCTOFor12MonthJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                $('txtAverageCTOFor12MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                $('txtAverageBalance12MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                $('txtTotalCTOFor6MonthJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                $('txtAverageCTOFor6MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                $('txtAverageBalance6MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                $('cmbConsideredCtoForJtBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                $('cmbConsideredBalanceForJtBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                $('txtConsideredMarginBMForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                $('txtgrossIncomeForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                $('txtNetAfterPaymentForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                $('txtShareForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                $('txtNetIncomeForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                $('cmbIncomeAssementForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                var incomeAssBussJt = Page.incomeAssementArray.findByProp('INAM_ID', res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                if (incomeAssBussJt) {
                    $('txtIncomeAssementCodeForJointBank' + identityFieldForBmJt).setValue(incomeAssBussJt.INAM_METHODCODE);
                }

                $('cmbIndustryForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                incomeAssmentManager.changeSector(identityFieldForBmJt, 'Jt');
                $('cmbSubSectorForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                incomeAssmentHelper.changeSubSector(identityFieldForBmJt, 'Jt');
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                    $('divAverageBalanceJoint' + identityFieldForBmJt).style.display = 'block';
                    $('txtAVGBalanceFor1st9MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                    $('txtAVGBalanceFor1st6MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                    $('txtAVGBalanceForLast6MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                    $('txtAVGBalanceForLast3MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                    $('txtConsideredMarginForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                }

                //Previous Repayment History
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                    $('divPrevRepHisJoint' + identityFieldForBmJt).style.display = 'block';
                    incomeAssmentHelper.PopulatePrevRePyHisJtBmIncomBankStatementSum(res, i, identityFieldForBmJt);
                }

                //Over Draft Facility

                incomeAssmentHelper.PopulateOverDraftJtBmIncomBankStatementSum(res, i);


                incomeAssmentHelper.changeIncomeAssementForBusssinessMan(identityFieldForBmJt, 'Jt');
                break;
            }
        }
    },

    PopulatePrevRePyHisJtBmIncomBankStatementSum: function(res, i, identityFieldForBmJt) {

        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
            $('txtTotalOffSCBJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
            $('txtTotalOnScbForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
            $('txtTotalEMIClosingConsideredPrevRepHisForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
            $('txtRemarksPrevRepHisForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                    var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                    if (parseInt(bankId) < 0) {
                        var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var emi = $F("txtEmiOnScb" + hs + "ForJointBank" + identityFieldForBmJt);
                            if (parseFloat(emi) == emidb) {
                                $('cmbConsiderOnScb' + hs + "ForJointBank" + identityFieldForBmJt).setValue('1');
                                break;
                            }
                        }

                    }
                    else {
                        var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var bankIdtxt = $F("txtBank" + hs + "BMJt" + identityFieldForBmJt);
                            if (parseInt(bankIdtxt) == bankId) {
                                $('cmbConsiderOffScb' + hs + "ForJointBank" + identityFieldForBmJt).setValue('1');
                                break;
                            }
                        }
                    }
                }
            }
        }
    },

    PopulateOverDraftJtBmIncomBankStatementSum: function(res, i) {

        var identityFieldForBmJt = 0;
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
            for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                identityFieldForBmJt = ov + 1;
                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                    var tablePrAccount = $("tblOverDraftFacilitiesForJtBMWithScb" + identityFieldForBmJt);
                    var rowCount = tablePrAccount.rows.length;
                    for (var t = 1; t <= rowCount - 1; t++) {
                        var interestovd = parseFloat($F("txtInterest" + t + "ForJointBank" + identityFieldForBmJt)).toFixed(2);
                        var LimitAmount = parseFloat($F("txtLimit" + t + "ForJointBank" + identityFieldForBmJt)).toFixed(2);
                        var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                        var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                        if (interestovd == interestDb && LimitAmount == limitdb) {
                            $('cmbInterest' + t + "ForJointBank" + identityFieldForBmJt).setValue('1');
                            break;
                        }
                    }
                }
                else {
                    var tablePrAccountOffScb = $("tblOverDraftFacilitiesForJtBMOffScb" + identityFieldForBmJt);
                    var rowCountOffScb = tablePrAccountOffScb.rows.length;
                    for (var t = 1; t <= rowCountOffScb - 1; t++) {
                        var bankId = parseFloat($F("cmbBankName" + t + "ForBussinessTabJtBank" + identityFieldForBmJt));
                        var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                        if (bankId == bankIdDb) {
                            $('cmbConsiderOffScbBM' + t + "ForJointBank" + identityFieldForBmJt).setValue('1');
                            break;
                        }
                    }
                }
            }
        }
    },

    populateIncomBankStatementSumForPrSelfEmployed: function(res, i) {

        var identityFieldForSePr = 0;
        for (var j = 0; j < Page.prCount; j++) {
            identityFieldForSePr = j + 1;
            var bankStatementId = parseInt($F("txtBankStatementIdSEPr" + identityFieldForSePr));
            if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                $('txtremarksForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                $('txtTotalCTOForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                $('txtAverageCTOForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                $('txtAverageBalanceForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                $('txtTotalCTOForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                $('txtAverageCTOForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                $('txtAverageBalanceForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                $('cmbConsideredCtoForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                $('cmbConsideredBalanceForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                $('txtConsiderMarginForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                $('txtgrossincomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                $('txtNetAfterPaymentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                $('txtShareForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                $('txtNetincomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                $('cmbIncomeAssessmentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                var incomeAssSePr = Page.incomeAssementArray.findByProp('INAM_ID', res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                if (incomeAssSePr) {
                    $('txtIncomeAssessmentCodeForSEPrBank' + identityFieldForSePr).setValue(incomeAssSePr.INAM_METHODCODE);
                }
                $('cmbIndustryForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                incomeAssmentManager.changeSectorForSelfEmployed(identityFieldForSePr, 'Pr');
                $('cmbSubsectorForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                incomeAssmentHelper.changeSubSectorForSelfEmployed(identityFieldForSePr, 'Pr');
                $('cmbPrivateIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome);
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                    $('divAverageBalanceForSEPr' + identityFieldForSePr).style.display = 'block';
                    $('txt1st9monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                    $('txt1st6MonthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                    $('txtLast6monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                    $('txtLast3monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                    $('txtConsiderednarginForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                }

                //Previous Repayment History
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                    $('divPrevRepHisForSEPrBank' + identityFieldForSePr).style.display = 'block';
                    incomeAssmentHelper.PopulatePrevRePyHisPrSeIncomBankStatementSum(res, i, identityFieldForSePr);
                }

                //OverDeraft facility


                incomeAssmentHelper.PopulateOverDraftPrSeIncomBankStatementSum(res, i);

                incomeAssmentHelper.changeIncomeAssementForSelfEmployed(identityFieldForSePr, 'Pr');
                //Auto_PracticingDoctor
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 143 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                    $('divChamberIncomeForSePrBank' + identityFieldForSePr).style.display = 'block';
                    incomeAssmentHelper.populateAutoPrectricingDoctorForPrimary(res, i, identityFieldForSePr);
                    incomeAssmentHelper.changePrivateInCome(identityFieldForSePr, 'Pr', 'Se');
                }
                //Auto_TutioningTeacher

                if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 150 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                    $('divTutionForSEPrBank' + identityFieldForSePr).style.display = 'block';
                    incomeAssmentHelper.PopulateTutioningTeacher(res, i, identityFieldForSePr);
                    incomeAssmentHelper.changePrivateInCome(identityFieldForSePr, 'Pr', 'Se');
                }

                break;
            }
        }
    },

    changeSubSectorForSelfEmployed: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var subsectorPrimaryId = $F("cmbSubsectorForSEPrBank" + bankNo);
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', subsectorPrimaryId);
            if (subsectorPrimary) {
                $('txtProfitMarginForSEPrBank' + bankNo).setValue(subsectorPrimary.SUSE_INTERESTRATE);
                $('txtProfitMarginCodeForSEPrBank' + bankNo).setValue(subsectorPrimary.SUSE_IRCODE);
            }
        }
        else if (accountType == "Jt") {
            var subsectorId = $F("cmbSubsectorForSEJtBank" + bankNo);
            var subsector = Page.subsectorArray.findByProp('SUSE_ID', subsectorId);
            if (subsectorPrimary) {
                $('txtProfitMarginForSEJtBank' + bankNo).setValue(subsector.SUSE_INTERESTRATE);
                $('txtProfitMarginCodeForSEJtBank' + bankNo).setValue(subsector.SUSE_IRCODE);
            }
        }
        incomeAssmentHelper.changePrivateInCome(bankNo, accountType, "Se");
        incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'SE');
    },

    PopulatePrevRePyHisPrSeIncomBankStatementSum: function(res, i, identityFieldForSePr) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
            $('txttotalForOffSCBForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
            $('txtTotal1OnSCBRepHisForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
            $('txtTotalEMIClosingConsideredForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
            $('txtremarks1ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                    var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                    if (parseInt(bankId) < 0) {
                        var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var emi = $F("txtEMI" + hs + "ForOffSCBSEPrBank" + identityFieldForSePr);
                            if (parseFloat(emi) == emidb) {
                                $('cmbConsider' + hs + "ForOnSCBReplHisForSEPrBank" + identityFieldForSePr).setValue('1');
                                break;
                            }
                        }

                    }
                    else {
                        var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var bankIdtxt = $F("txtBank" + hs + "SEPr" + identityFieldForSePr);
                            if (parseInt(bankIdtxt) == bankId) {
                                $('cmbConsider' + hs + "ForSEPrOffScbBank" + identityFieldForSePr).setValue('1');
                                break;
                            }
                        }
                    }
                }
            }

        }
    },

    PopulateOverDraftPrSeIncomBankStatementSum: function(res, i) {
        var identityFieldForSePr = 0;

        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
            for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                identityFieldForSePr = ov + 1;
                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                    var tablePrAccount = $("tblOverDraftFacilitiesForPrSEWithScb" + identityFieldForSePr);
                    var rowCount = tablePrAccount.rows.length;
                    for (var t = 1; t <= rowCount - 1; t++) {
                        var interestOvd = parseFloat($F("txtInterst" + t + "WithScbForSEBank" + identityFieldForSePr)).toFixed(2);
                        var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForSEBank" + identityFieldForSePr)).toFixed(2);
                        var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                        var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                        if (interestOvd == interestDb && LimitAmount == limitdb) {
                            $('cmbConsider' + t + "WithScbForSEBank" + identityFieldForSePr).setValue('1');
                            break;
                        }
                    }
                }
                else {
                    var tablePrAccountOffScb = $("tblOverDraftFacilitiesForPrSEOffScb" + identityFieldForSePr);
                    var rowCountOffScb = tablePrAccountOffScb.rows.length;
                    for (var t = 1; t <= rowCountOffScb - 1; t++) {
                        var bankId = parseFloat($F("cmbBankName" + t + "ForSEPrBank" + identityFieldForSePr));
                        var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                        if (bankId == bankIdDb) {
                            $('cmbConsider' + t + "ForSEOffScbBank" + identityFieldForSePr).setValue('1');
                            break;
                        }
                    }
                }
            }
        }
    },

    changeIncomeAssementForSelfEmployed: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEPrBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                $("txtIncomeAssessmentCodeForSEPrBank" + bankNo).setValue(incomeAssBuss.INAM_METHODCODE);
                $('txtConsiderMarginForSEPrBank' + bankNo).disabled = "";
                if (incomeAssBuss.INAM_METHODCODE == "FT") {

                    var share = $F("txtShareForSEPrBank" + bankNo);
                    if (parseInt(share) == 100) {
                        $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'none';
                        $('divAverageBalanceForSEPr' + bankNo).style.display = 'block';
                        $('txtConsiderMarginForSEPrBank' + bankNo).disabled = "false";
                        incomeAssmentHelper.calculateAverageBalanceForSelfEmployed(bankNo, accountType);
                    }
                    else {
                        alert("Average Balance is not applicable");
                        $("cmbIncomeAssessmentForSEPrBank" + bankNo).setValue('-1');
                        $("txtIncomeAssessmentCodeForSEPrBank" + bankNo).setValue('');
                    }

                }
                else if (incomeAssBuss.INAM_METHODCODE == "FF") {
                    $('divAverageBalanceForSEPr' + bankNo).style.display = 'none';
                    $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'block';

                    var avergTotalCto12MonthPr = $F("cmbConsideredCtoForSEPrBank" + bankNo);
                    var considerMarginFinal = $F("txtConsiderMarginForSEPrBank" + bankNo);

                    var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                    $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncome);
                    $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncome);
                    var share = $F("txtShareForSEPrBank" + bankNo);
                    var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
                    $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotal);
                }
                else {
                    $('divAverageBalanceForSEPr' + bankNo).style.display = 'none';
                    $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'none';

                    var avergTotalCto12MonthPr = $F("cmbConsideredCtoForSEPrBank" + bankNo);
                    var considerMarginFinal = $F("txtConsiderMarginForSEPrBank" + bankNo);

                    var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                    $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncome);
                    $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncome);
                    var share = $F("txtShareForSEPrBank" + bankNo);
                    var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
                    $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotal);
                }

            }

        }
        else if (accountType == "Jt") {

            var incomeAssIdForBussJoint = $F("cmbIncomeAssessmentForSEJtBank" + bankNo);
            var incomeAssBussJoint = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBussJoint);
            if (incomeAssBussJoint) {
                $("txtIncomeAssessmentCodeForSEJtBank" + bankNo).setValue(incomeAssBussJoint.INAM_METHODCODE);
                $('txtConsiderMarginForSEJtBank' + bankNo).disabled = "";
                if (incomeAssBussJoint.INAM_METHODCODE == "FT") {
                    //Code Comment by Washik 26/12/2012
                    //                var averageBalance = false;

                    //                for (var i = 0; i < Page.isAverageBalanceJointArray.length; i++) {
                    //                    if (Page.isAverageBalanceJointArray[i].BankNo == bankNo && Page.isAverageBalanceJointArray[i].Accounttype == accountType && Page.isAverageBalanceJointArray[i].TabName == "Se") {
                    //                        averageBalance = true;
                    //                        break;
                    //                    }
                    //                }

                    //if (averageBalance == false) {
                    var share = $F("txtShareForSEJtBank" + bankNo);
                    if (parseInt(share) == 100) {
                        $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'none';
                        $('divAverageBalanceForSEJt' + bankNo).style.display = 'block';
                        $('txtConsiderMarginForSEJtBank' + bankNo).disabled = "false";
                        incomeAssmentHelper.calculateAverageBalanceForSelfEmployed(bankNo, accountType);
                    }
                    else {
                        alert("Average Balance is not applicable");
                        $("cmbIncomeAssessmentForSEJtBank" + bankNo).setValue('-1');
                        $("txtIncomeAssessmentCodeForSEJtBank" + bankNo).setValue('');
                    }
                }
                else if (incomeAssBussJoint.INAM_METHODCODE == "FF") {
                    $('divAverageBalanceForSEJt' + bankNo).style.display = 'none';
                    $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'block';

                    var avergTotalCto12MonthPr = $F("cmbConsideredCtoForSEJtBank" + bankNo);
                    var considerMarginFinal = $F("txtConsiderMarginForSEJtBank" + bankNo);

                    var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                    $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncome);
                    $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncome);
                    var share = $F("txtShareForSEJtBank" + bankNo);
                    var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
                    $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotal);
                }
                else {
                    $('divAverageBalanceForSEJt' + bankNo).style.display = 'none';
                    $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'none';

                    var avergTotalCto12MonthPr = $F("cmbConsideredCtoForSEJtBank" + bankNo);
                    var considerMarginFinal = $F("txtConsiderMarginForSEJtBank" + bankNo);

                    var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                    $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncome);
                    $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncome);
                    var share = $F("txtShareForSEJtBank" + bankNo);
                    var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
                    $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotal);
                }
            }
        }
        incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'SE');
    },


    calculateAverageBalanceForSelfEmployed: function(bankNo, accountType) {
        var firstst9MonthTotal = 0;
        var first6MonthTotal = 0;
        var last6Month = 0;
        var last3month = 0;
        var j = 0;
        var monthArray = [];
        if (accountType == "Pr") {
            var tablePrAccount = $("tblPrBankStatementForSEBank" + bankNo);
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txt1st9monthForSEPrBank" + bankNo).setValue(Math.round(firstst9MonthTotal / 9));
            $("txt1st6MonthForSEPrBank" + bankNo).setValue(Math.round(first6MonthTotal / 6));
            $("txtLast6monthForSEPrBank" + bankNo).setValue(Math.round(last6Month / 6));
            $("txtLast3monthForSEPrBank" + bankNo).setValue(Math.round(last3month / 3));



            //Consider Margin
            var considerMargin1 = 0;
            var considerMargin2 = 0;



            var last3monthAvg = Math.round(last3month / 3);
            var first9monthAvg = Math.round(firstst9MonthTotal / 9);
            var first6monthAvg = Math.round(first6MonthTotal / 6);
            var last6monthAvg = Math.round(last6Month / 6);

            if (last3monthAvg >= first9monthAvg) {
                considerMargin1 = "25";
            }
            else {
                considerMargin1 = "15";
            }

            if (last6monthAvg >= first6monthAvg) {
                considerMargin2 = "33";
            }
            else {
                considerMargin2 = "20";
            }

            var considerMarginFinal = 0;
            if (considerMargin1 <= considerMargin2) {
                considerMarginFinal = considerMargin1;
            }
            else {
                considerMarginFinal = considerMargin2;
            }


            $("txtConsiderednarginForSEPrBank" + bankNo).setValue(considerMarginFinal);
            $("txtConsiderMarginForSEPrBank" + bankNo).setValue(considerMarginFinal);


            incomeAssmentHelper.changeConsiderForOverDraftWithScb(bankNo, "Pr", "SE");

        }
        else if (accountType == "Jt") {
            var tableJtAccount = $("tblJtBankStatementForSEBank" + bankNo);
            var rowCountJt = tableJtAccount.rows.length;
            for (var i = 1; i <= rowCountJt - 1; i++) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txt1st9monthForSEJtBank" + bankNo).setValue(Math.round(firstst9MonthTotal / 9));
            $("txt1st6MonthForSEJtBank" + bankNo).setValue(Math.round(first6MonthTotal / 6));
            $("txtLast6monthForSEJtBank" + bankNo).setValue(Math.round(last6Month / 6));
            $("txtLast3monthForSEJtBank" + bankNo).setValue(Math.round(last3month / 3));

            //Consider Margin
            var considerMarginForJoint1 = 0;
            var considerMarginForJoint2 = 0;


            //            if (last3month >= firstst9MonthTotal) {
            //                considerMarginForJoint1 = "25";
            //            }
            //            else {
            //                considerMarginForJoint1 = "15";
            //            }

            //            if (last6Month >= first6MonthTotal) {
            //                considerMarginForJoint2 = "33";
            //            }
            //            else {
            //                considerMarginForJoint2 = "20";
            //            }
            var last3monthAvg = Math.round(last3month / 3);
            var first9monthAvg = Math.round(firstst9MonthTotal / 9);
            var first6monthAvg = Math.round(first6MonthTotal / 6);
            var last6monthAvg = Math.round(last6Month / 6);

            if (last3monthAvg >= first9monthAvg) {
                considerMarginForJoint1 = "25";
            }
            else {
                considerMarginForJoint1 = "15";
            }

            if (last6monthAvg >= first6monthAvg) {
                considerMarginForJoint2 = "33";
            }
            else {
                considerMarginForJoint2 = "20";
            }

            var considerMarginFinalForJoint = 0;
            if (considerMarginForJoint1 <= considerMarginForJoint2) {
                considerMarginFinalForJoint = considerMarginForJoint1;
            }
            else {
                considerMarginFinalForJoint = considerMarginForJoint2;
            }
            $("txtConsiderednarginForSEJtBank" + bankNo).setValue(considerMarginFinalForJoint);
            $("txtConsiderMarginForSEJtBank" + bankNo).setValue(considerMarginFinalForJoint);

            //var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);
            incomeAssmentHelper.changeConsiderForOverDraftWithScb(bankNo, "Jt", "SE");
            //            var grossIncomeForJoint = Math.round(parseFloat((parseFloat($F("txtAverageBalanceForSEJt12MonthBank" + bankNo)) * considerMarginFinalForJoint) / 100));
            //            $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncomeForJoint);
            //            $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncomeForJoint);
            //            var shareJoint = $F("txtShareForSEJtBank" + bankNo);
            //            var netIncomeTotalJoint = Math.round(parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100));
            //            $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalJoint);

        }
        incomeAssmentHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'SE');
    },

    populateAutoPrectricingDoctorForPrimary: function(res, i, identityFieldForSePr) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor != null) {
            $('txtTotalChamberIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalChamberIncome);
            $('txtTotalOtherIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.OtherOTIncome);
            $('txtTotalIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalIncome);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome != null) {
                for (var ch = 0; ch < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome.length; ch++) {
                    var chamberId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].ChamberId;
                    $('txtPatientsOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldPatents);
                    $('txtFeeOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldFee);
                    $('txtTotalOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldTotal);
                    $('txtPatientsNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewPatents);
                    $('txtFeeNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewFee);
                    $('txtTotalNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewTotal);
                    $('txtTotalOldNew' + chamberId + 'ForSEPr1Bank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Total);
                    $('txtDayMonth' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].DayMonth);
                    $('txtIncome' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Income);
                }
            }
        }
    },

    PopulateTutioningTeacher: function(res, i, identityFieldForSePr) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome != null) {
            $('txtNoOfStudentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfStudent);
            $('txtNoOfDaysForTutionForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfDaysForTution);
            $('txtTutionFeeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.Fees);
            $('txtTotalInComeTutionForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.TotalIncome);
        }
    },

    populateIncomBankStatementSumForJtSelfEmployed: function(res, i) {

        var identityFieldForSeJt = 0;
        for (var j = 0; j < Page.jtCount; j++) {
            identityFieldForSeJt = j + 1;
            var bankStatementId = parseInt($F("txtBankStatementIdSEJt" + identityFieldForSeJt));
            if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                //Main Object
                $('txtremarksForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                $('txtTotalCTOForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                $('txtAverageCTOForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                $('txtAverageBalanceForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                $('txtTotalCTOForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                $('txtAverageCTOForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                $('txtAverageBalanceForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                $('cmbConsideredCtoForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                $('cmbConsideredBalanceForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                $('txtConsiderMarginForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                $('txtgrossincomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                $('txtNetAfterPaymentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                $('txtShareForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                $('txtNetincomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                $('cmbIncomeAssessmentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                var incomeAssSeJt = Page.incomeAssementArray.findByProp('INAM_ID', res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                if (incomeAssSeJt) {
                    $('txtIncomeAssessmentCodeForSEJtBank' + identityFieldForSeJt).setValue(incomeAssSeJt.INAM_METHODCODE);
                }
                $('cmbIndustryForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                incomeAssmentManager.changeSectorForSelfEmployed(identityFieldForSeJt, 'Jt');
                $('cmbSubsectorForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                incomeAssmentHelper.changeSubSectorForSelfEmployed(identityFieldForSeJt, 'Jt');
                $('cmbPrivateIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome);
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                    $('divAverageBalanceForSEJt' + identityFieldForSeJt).style.display = 'block';
                    $('txt1st9monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                    $('txt1st6MonthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                    $('txtLast6monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                    $('txtLast3monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                    $('txtConsiderednarginForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                }

                //Preveous Repayment History
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                    $('divPrevRepHisForSEJtBank' + identityFieldForSeJt).style.display = 'block';
                    incomeAssmentHelper.PopulatePrevRePyHisJtSeIncomBankStatementSum(res, i, identityFieldForSeJt);
                }

                //Over Draft Facility
                incomeAssmentHelper.PopulateOverDraftJtSeIncomBankStatementSum(res, i);
                incomeAssmentHelper.changeIncomeAssementForSelfEmployed(identityFieldForSeJt, 'Jt');
                //Auto_PracticingDoctor

                if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 143 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                    $('divChamberIncomeForSeJtBank' + identityFieldForSeJt).style.display = 'block';
                    incomeAssmentHelper.populateAutoPrectricingDoctorForJoint(res, i, identityFieldForSeJt);
                    incomeAssmentHelper.changePrivateInCome(identityFieldForSeJt, 'Jt', 'Se');
                }
                //Auto_TutioningTeacher

                if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 150 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                    $('divTutionJtBank' + identityFieldForSeJt).style.display = 'block';
                    incomeAssmentHelper.PopulateTutioningTeacherJoint(res, i, identityFieldForSeJt);
                    incomeAssmentHelper.changePrivateInCome(identityFieldForSeJt, 'Jt', 'Se');
                }
                break;
            }
        }
    },

    PopulatePrevRePyHisJtSeIncomBankStatementSum: function(res, i, identityFieldForSeJt) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
            $('txttotalForOffSCBForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
            $('txtTotal1OnSCBRepHisForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
            $('txtTotalEMIClosingConsideredForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
            $('txtremarks1ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                    var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                    if (parseInt(bankId) < 0) {
                        var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var emi = $F("txtEmi" + hs + "ForOnScbForSEJtBank" + identityFieldForSeJt);
                            if (parseFloat(emi) == emidb) {
                                $('cmbConsider' + hs + "ForOnSCBReplHisForSEJtBank" + identityFieldForSeJt).setValue('1');
                                break;
                            }
                        }

                    }
                    else {
                        var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var bankIdtxt = $F("txtBank" + hs + "SEJt" + identityFieldForSeJt);
                            if (parseInt(bankIdtxt) == bankId) {
                                $('cmbConsider' + hs + "ForSEJtRepHisOffScbBank" + identityFieldForSeJt).setValue('1');
                                break;
                            }
                        }
                    }
                }
            }

        }
    },

    PopulateOverDraftJtSeIncomBankStatementSum: function(res, i) {
        var identityFieldForSeJt = 0;
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
            for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                identityFieldForSeJt = ov + 1;
                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                    var tablePrAccount = $("tblOverDraftFacilitiesForJtSEWithScb" + identityFieldForSeJt);
                    var rowCount = tablePrAccount.rows.length;
                    for (var t = 1; t <= rowCount - 1; t++) {
                        var interestovd = parseFloat($F("txtInterst" + t + "WithScbForSEJtBank" + identityFieldForSeJt)).toFixed(2);
                        var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForSEJtBank" + identityFieldForSeJt)).toFixed(2);
                        var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                        var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                        if (interestovd == interestDb && LimitAmount == limitdb) {
                            $('cmbConsider' + t + "WithScbForSEJtBank" + identityFieldForSeJt).setValue('1');
                            break;
                        }
                    }
                }
                else {
                    var tablePrAccountOffScb = $("tblOverDraftFacilitiesForJtSEOffScbBank" + identityFieldForSeJt);
                    var rowCountOffScb = tablePrAccountOffScb.rows.length;
                    for (var t = 1; t <= rowCountOffScb - t; i++) {
                        var bankId = parseFloat($F("cmbBankName" + t + "ForSEJtBank" + identityFieldForSeJt));
                        var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                        if (bankId == bankIdDb) {
                            $('cmbConsider' + t + "ForSEJtOffScbBank" + identityFieldForSeJt).setValue('1');
                            break;
                        }
                    }
                }
            }
        }
    },

    populateAutoPrectricingDoctorForJoint: function(res, i, identityFieldForSeJt) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor != null) {
            $('txtTotalChamberIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalChamberIncome);
            $('txtTotalOtherIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.OtherOTIncome);
            $('txtTotalIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalIncome);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome != null) {
                for (var ch = 0; ch < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome.length; ch++) {
                    var chamberId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].ChamberId;
                    $('txtPatientsOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldPatents);
                    $('txtFeeOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldFee);
                    $('txtTotalOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldTotal);
                    $('txtPatientsNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewPatents);
                    $('txtFeeNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewFee);
                    $('txtTotalNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewTotal);
                    $('txtTotalOldNew' + chamberId + 'ForSEJt1Bank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Total);
                    $('txtDayMonth' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].DayMonth);
                    $('txtIncome' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Income);
                }
            }
        }
    },

    //Populate Tutioning teacher For Joint
    PopulateTutioningTeacherJoint: function(res, i, identityFieldForSeJt) {

        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome != null) {
            $('txtNoOfStudentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfStudent);
            $('txtNoOfDaysForTutionForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfDaysForTution);
            $('txtTutionFeeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.Fees);
            $('txtTotalInComeTutionForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.TotalIncome);
        }
    },

    createExistingrepaymentGrid: function() {
        var limit = 0;
        var interest = 0;
        var typeName = "";
        var bankName = "";
        var emiShare = 0;
        var totalInterestOnScbCreditCard = 0;
        var totalInterestOffScbCreditcard = 0;
        var totalEmiShareOnScb = 0;
        var totalEmiShareOffScb = 0;
        //credit card----------------
        for (var j = 1; j < 3; j++) {
            var sequrityForCreditCard = $F("cmbStatusForSecuritiesForCreditCard" + j);
            if (sequrityForCreditCard == "1") {
                limit = $F("txtOriginalLimitForCreditCard" + j);
                interest = $F("txtInterestForCreditCard" + j);
                if (util.isFloat(interest) && !util.isEmpty(interest)) {
                    $("txtLimitCarditcardForLcExistingPaymentForScb" + j).setValue(limit);
                    $("txtInterestcreditcardForLcExistingPaymentForScb" + j).setValue(interest);
                    totalInterestOnScbCreditCard += parseFloat(interest);
                }
                limit = 0;
                interest = 0;
            }
            else {
                $("txtLimitCarditcardForLcExistingPaymentForScb" + j).setValue('');
                $("txtInterestcreditcardForLcExistingPaymentForScb" + j).setValue('');
            }
            var sequrityCardOffScb = $F("cmbStatusForCreditCardOffScb" + j);
            if (sequrityCardOffScb == "1") {
                limit = $F("txtOriginalLimitForcreditCardOffScb" + j);
                interest = $F("txtInterestForCreditCardForOffScb" + j);
                if (util.isFloat(interest) && !util.isEmpty(interest)) {
                    $("txtLimitCarditcardForLcExistingPaymentForOffScb" + j).setValue(limit);
                    $("txtInterestcreditcardForLcExistingPaymentOnScb" + j).setValue(interest);
                    totalInterestOffScbCreditcard += parseFloat(interest);
                }
                limit = 0;
                interest = 0;
            }
            else {
                $("txtLimitCarditcardForLcExistingPaymentForOffScb" + j).setValue('');
                $("txtInterestcreditcardForLcExistingPaymentOnScb" + j).setValue('');
            }
        }
        $("txtTotalinterestForOnScbLcExistingrepayment").setValue(totalInterestOnScbCreditCard);
        $("txtTotalinterestForOffScbLcExistingrepayment").setValue(totalInterestOffScbCreditcard);
        //Term loans
        var sequrityForTerLoanOnScb = "";
        var sequrityForTermLoanOffScb = "";
        for (var i = 1; i < 5; i++) {
            sequrityForTerLoanOnScb = $F("cmbStatusForSecurities" + i);
            if (sequrityForTerLoanOnScb == "1") {
                typeName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                emiShare = $F("txtEMIShare" + i);
                if (util.isFloat(emiShare) && !util.isEmpty(emiShare)) {
                    $("txtTypeForLcExistingPayment" + i).setValue(typeName);
                    $("txtEmiForLcExistingPayment" + i).setValue(emiShare);
                    totalEmiShareOnScb += parseFloat(emiShare);
                }

            }
            else {
                $("txtTypeForLcExistingPayment" + i).setValue('');
                $("txtEmiForLcExistingPayment" + i).setValue('');
            }
            sequrityForTermLoanOffScb = $F("cmbStatusForOffScb" + i);
            var bankId = $F("cmbFinancialInstitutionForOffSCB" + i);
            if (sequrityForTermLoanOffScb == "1") {
                bankName = $('cmbFinancialInstitutionForOffSCB' + i).options[$('cmbFinancialInstitutionForOffSCB' + i).selectedIndex].text;
                emiShare = $F("txtEMIShareForOffSCB" + i);
                if (util.isFloat(emiShare) && !util.isEmpty(emiShare)) {
                    $("txtBankForLcExistingPaymentOffScb" + i).setValue(bankName);
                    $("txtEmiForLcExistingPaymentOffScb" + i).setValue(emiShare);
                    totalEmiShareOffScb += parseFloat(emiShare);
                }

            }
            else {
                $("txtBankForLcExistingPaymentOffScb" + i).setValue('');
                $("txtEmiForLcExistingPaymentOffScb" + i).setValue('');
            }

        }
        $("txtTotalEmiForLcExistingPaymentOnScb").setValue(totalEmiShareOnScb);
        $("txtTotalEmiForLcExistingPaymentOffScb").setValue(totalEmiShareOffScb);

        //IncomeAssementPrintHelper.changeConsideredDbr();

    },

    changeCashSecured: function() {
        
        var cashSecured = $F("cmbCashSecured");
        var primaryProfession = Page.primaryProfessionId;
        var otherProfession = Page.otherProfessionId;

        var emiClosing = 0;
        var totalEmiClosing = 0;
        var lccalculate = Page.incomeArrayforlc;
        if (cashSecured == "1") {
            $("lblincomeGeneratorAlertMasg").innerHTML = "Income Assessment Not Required";
            $('txtAssessmentMethodLc').setValue('Income Assessment Not Required');
            $('txtAssessmentCodeLc').setValue('FF');
            $('txtBalanceSupported').setValue('NOT APPLICABLE');
//            $('divSecurityValue').style.display = "block";
//            $('divSequrityvaluetextbox').style.display = "block";
            $('divCashForLc').style.display = "block";

            if (Page.primaryProfessionId != 2) {
                $("cmbAssessmentMethodForLcCash").update(Page.incomeAssesmentLinkOption);
                $('divSubCategoryForLcCashCovered').style.display = "block";
            }
            else {
                var link = "<option value=\"-1\">Select an income Assement</option>";
                for (var i = 0; i < Page.employerForSalariedArray.length; i++) {
                    link += "<option value=\"" + Page.employerForSalariedArray[i].AutoCompanyId + "\">" + Page.employerForSalariedArray[i].CompanyName + "</option>";
                }
                $("cmbCategoryForLcCash").update(link);
                $('divSubCategoryForLcCashCovered').style.display = "none";
            }

            var declaredIncome = $F("txtDeclaredIncomeForLcAll");
            $('txtTotalJointInComeForLcWithCal').setValue(declaredIncome);



            for (var i = 1; i <= Page.prCount; i++) {
                if (i < 7) {
                    if (primaryProfession == 3 || otherProfession == 3 || primaryProfession == 4 || otherProfession == 4 || primaryProfession == 6 || otherProfession == 6) {
                        emiClosing = $F("txtTotalEMIClosingConsideredForSEPrBank" + i);
                        if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                            totalEmiClosing += parseFloat(emiClosing);
                        }
                    }
                    if (primaryProfession == 1 || otherProfession == 1) {
                        emiClosing = $F("txtTotalEMIClosingConsideredForPrimaryBank" + i);
                        if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                            totalEmiClosing += parseFloat(emiClosing);
                        }
                    }
                }
            }
            if (totalEmiClosing != 0) {
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (incomeAssmentHelper.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtBalanceSupported').setValue('NOT APPLICABLE');
                        }
                    }
                }
            }
            else {
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {
                        if (incomeAssmentHelper.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            if (lccalculate[j].AssessmentCode == "FT" || lccalculate[j].AssessmentCode == "FC") {
                                $('txtBalanceSupported').setValue(lccalculate[j].AverageBalance);
                            }
                        }
                    }
                }

            }

        }
        else {
//            $('divSecurityValue').style.display = "none";
//            $('divSequrityvaluetextbox').style.display = "none";
            $('divCashForLc').style.display = "none";
            totalEmiClosing = 0;
            emiClosing = 0;
            $("lblincomeGeneratorAlertMasg").innerHTML = "";

            var totalIncome = 0;
            var appPrIncome = $F("txtAppropriteIncomeForLcAll");
            var appJtIncome = $F("txtAppropriteIncomeForLcJointAll");
            if (Page.isjoint == true) {
                if (!util.isFloat(appJtIncome)) {
                    appJtIncome = 0;
                }
                if (!util.isFloat(appPrIncome)) {
                    appPrIncome = 0;
                }
                totalIncome = parseInt(appPrIncome) + parseInt(appJtIncome);
            }
            else {
                if (!util.isFloat(appPrIncome)) {
                    appPrIncome = 0;
                }
                totalIncome = parseInt(appPrIncome);
            }
            $('txtTotalJointInComeForLcWithCal').setValue(totalIncome);





            for (var i = 1; i <= Page.prCount; i++) {
                if (i < 7) {
                    if (primaryProfession == 3 || otherProfession == 3 || primaryProfession == 4 || otherProfession == 4 || primaryProfession == 6 || otherProfession == 6) {
                        emiClosing = $F("txtTotalEMIClosingConsideredForSEPrBank" + i);
                        if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                            totalEmiClosing += parseFloat(emiClosing);
                        }
                    }
                    if (primaryProfession == 1 || otherProfession == 1) {
                        emiClosing = $F("txtTotalEMIClosingConsideredForPrimaryBank" + i);
                        if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                            totalEmiClosing += parseFloat(emiClosing);
                        }
                    }
                }
            }
            if (totalEmiClosing != 0) {
                $('txtAssessmentMethodLc').setValue('PREVIOUS REPAYMENT HISTORY');
                $('txtAssessmentCodeLc').setValue('FF');
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (incomeAssmentHelper.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtAssessmentMethodIdLc').setValue('7');
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtBalanceSupported').setValue('NOT APPLICABLE');
                        }
                    }
                }
            }
            else {
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (incomeAssmentHelper.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtAssessmentMethodLc').setValue(lccalculate[j].Assessmentmethod);
                            $('txtAssessmentCodeLc').setValue(lccalculate[j].AssessmentCode);
                            $('txtAssessmentMethodIdLc').setValue(lccalculate[j].AssmentMethodId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            if (lccalculate[j].AssessmentCode == "FT" || lccalculate[j].AssessmentCode == "FC") {
                                $('txtBalanceSupported').setValue(lccalculate[j].AverageBalance);
                            }
                        }
                    }


                }

            }
        }
    },

    isMax: function(listofArray, value) {
        var ismax = true;
        for (var i = 0; i < listofArray.length; i++) {
            if (parseFloat(value) < parseFloat(listofArray[i])) {
                ismax = false;
                break;
            }
        }
        return ismax;
    }

};
Event.onReady(Page.init);