﻿/// <reference path="AnalystFinalizationTab.js" />

/// <reference path="../activatables.js" />
var AccountType = { 1: 'Company', 2: 'Personal' };
var dbrLevel = { 1: '50', 2: '55', 3: '60' };
var askingrepayment = { 1: 'PDC', 2: 'SI' };
var reasonOfJoint = { 'Income Consideration': '1', 'Joint Registration': '2', 'Business is in Joint Name':'3', 'Customer is Mariner':'4','AC is in Joint Name':'5','Others':'6' };
var Page = {
    pageSize: 15,
    pageNo: 0,
    pager: null,
    LoanSummeryArray: [],
    sourceForGeneralArray: [],
    facilityArray: [],
    facilityListArray: [],
    bankStatementArray: [],
    subsectorArray: [],
    incomeAssementArray: [],
    isAverageBalancePrimaryArray: [],
    isAverageBalanceJointArray: [],
    employerForSalariedArray: [],
    segmentArray: [],
    jtCount: 0,
    prCount: 0,
    incomeArrayforlc: [],
    newIncomeArrayforAll: [],
    artaPreference: [],
    age: 0,
    jointage:0,
    tenor: 0,
    ownDamageArray: [],
    otherBankAccountForrepayment: [],
    userArray:[],
    stateId: 0,
    vendoreArray: [],
    manufactureArray: [],
    modelArray: [],
    vehicleStatusArray: [],
    approverLimit: [],
    vehicleId: 0,
    cibInfo:[],

    init: function() {
        util.prepareDom();
        Event.observe('lnkClose', 'click', analystSaveManager.close);
        Event.observe('lnkBookLet', 'click', analystSaveManager.printBooklet);
        Event.observe('lnkOfferLetter', 'click', analystSaveManager.printOfferLetter);
        Event.observe('lnkLLI', 'click', analystSaveManager.printLLI);
        Event.observe('lnkBasel', 'click', analystSaveManager.printBasel);
        Event.observe('lnkLoanChecklist', 'click', analystSaveManager.printLoanCheckList);
        Event.observe('lnkDocumentChecklist', 'click', analystSaveManager.printDocumentList);
        Event.observe('lnkDecline', 'click', analystSaveManager.Decline);
        Event.observe('lnkReject', 'click', analystSaveManager.Reject);
        Event.observe('lnkOnHold', 'click', analystSaveManager.OnHold);
        Event.observe('lnkBackward', 'click', analystSaveManager.Backward);
        Event.observe('lnkForward', 'click', analystSaveManager.Forward);
        
        Event.observe('cmbReasonofJointApplicant', 'change', analyzeLoanApplicationHelper.showJointApplicant);
        Event.observe('cmbCashSecured', 'change', analyzeLoanApplicationHelper.changeCashSecured);
        Event.observe('cmbARTA', 'change', analyzeLoanApplicationHelper.changeARTA);
        Event.observe('cmbCCBundle', 'change', analyzeLoanApplicationHelper.changeCCBundle);
        Event.observe('cmsSeatingCapacity', 'change', analyzeLoanApplicationHelper.changeSeatingCapacity);
        Event.observe('cmbCarVerification', 'change', analyzeLoanApplicationHelper.changeCarVerification);
        Event.observe('cmbSource1', 'change', analyzeLoanApplicationManager.changeBranchNameForSourceOnApplicantDetailsForGeneral);
        Event.observe('cmbBrowingRelationship', 'change', analyzeLoanApplicationHelper.changeBrowingRelationship);
        Event.observe('cmbEmployeeSegmentLc', 'change', analyzeLoanApplicationHelper.changeSegment);
        Event.observe('cmbBranch', 'change', analyzeLoanApplicationHelper.changeSourceNameForSourceCodeOnApplicantDetailsForGeneral);
        Event.observe('cmbDBRLevel', 'change', analyzeLoanApplicationHelper.changeDbrLevel);
        Event.observe('txtConsideredDbr', 'change', analyzeLoanApplicationHelper.changeConsideredDbr);
        Event.observe('txtAskingInterest', 'change', loanCalculatorHelPer.changeAskingInterest);
        Event.observe('txtAskingtenor', 'change', loanCalculatorHelPer.changeAskingTenor);
        Event.observe('cmbAskingRepaymentmethod', 'change', loanCalculatorHelPer.changeAskingRepaymentMethod);
        Event.observe('cmbARTAReference', 'change', loanCalculatorHelPer.changeArtareference);
        Event.observe('cmbGeneralInsurence', 'change', loanCalculatorHelPer.changeGeneralInsurence);
        Event.observe('txtQuotedPrice', 'change', loanCalculatorHelPer.changeQuotedPrice);
        Event.observe('txtConsideredprice', 'change', loanCalculatorHelPer.changeConsideredPrice);
        Event.observe('cmbAssessmentMethodForLcCash', 'change', loanCalculatorHelPer.changeIncomeAssementMethodForCashSequre);
        Event.observe('cmbCategoryForLcCash', 'change', loanCalculatorHelPer.changeCategoryForCashSequre);
        Event.observe('cmbBureauHistory', 'change', loanCalculatorHelPer.changeCibInfo);
        
        
       

        Event.observe('cmbVendor', 'change', analyzeLoanApplicationHelper.changeVendore);
        Event.observe('cmbManufacturer', 'change', analyzeLoanApplicationHelper.changeManufacture);
        Event.observe('txtregistrationYear', 'change', loanCalculatorHelPer.changeregistationyear);
        Event.observe('txtManufacturaryear', 'change', analyzeLoanApplicationHelper.changeManufactureryear);
        Event.observe('cmdVehicleStatus', 'change', analyzeLoanApplicationHelper.changeVehicleStatus);
        
        
        Event.observe('btnClosePopup', 'click', util.hideModal.curry('bankStatementPopupDiv'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNo'),
            spnTotalPage: $('spntotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });
        
        analyzeLoanApplicationManager.getLoanSummary();



        analyzeLoanApplicationManager.getVehicleStatus();
        analyzeLoanApplicationManager.getManufacturer();
        analyzeLoanApplicationManager.getVendor();
        analyzeLoanApplicationManager.GetAllAutoBranch();
        analyzeLoanApplicationManager.getBank();
        analyzeLoanApplicationManager.getFacility();
        analyzeLoanApplicationManager.getSector();
        analyzeLoanApplicationManager.getCibInfo();
        analyzeLoanApplicationManager.getIncomeAssement();

//        //Salaried

//        analyzeLoanApplicationManager.GetEmployerForSalaried();

//        //Segment Settings
//        analyzeLoanApplicationManager.GetEmployeSegment();
//        analyzeLoanApplicationManager.getGeneralInsurance();
//        analyzeLoanApplicationManager.renderOtherDamage();
//        analyzeLoanApplicationManager.GetUserData();
//        FinalizationTabManager.FinalizationTabOnLoad();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        analyzeLoanApplicationManager.getLoanSummary(Page.pageNo, Page.pageSize);
    }


};

var analyzeLoanApplicationManager = {
    //Loan Summary
    getLoanSummary: function() {

        var searchKey = $F('txtLLIDMain');
        if (searchKey == "") {
            searchKey = 0;
        }
        if (util.isDigit(searchKey)) {
            var res = PlussAuto.AnalyzeLoanApplication.GetLoanSummary(Page.pageNo, Page.pageSize, searchKey);
            //debugger;
            if (res.error) {
                alert(res.error.Message);
                return;
            }
            Page.LoanSummeryArray = res.value;

            analyzeLoanApplicationHelper.populateLoanSummeryGrid(res);
        } else {
            alert("LLID Must to be in Degit");
            return false;
        }
    },

    getLoanSummarySearch: function() {
        Page.pageNo = 0;
        analyzeLoanApplicationManager.getLoanSummary();
    },
    searchKeypress: function(e) {
        if (e.keyCode == 13) {
            analyzeLoanApplicationManager.getLoanSummary();
        }
    },

    //render Own Damage and Other Damage

    renderOtherDamage: function() {

        var res = PlussAuto.AnalyzeLoanApplication.GetOtherDamage();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            $('txtaccidentOrTheft').setValue(res.value.AccidentOrTheft.toFixed(2));
            $('txtCommision').setValue(res.value.Commission.toFixed(2));
            $('txtVat').setValue(res.value.Vat.toFixed(2));
            $('txtPassenger').setValue(res.value.PassengerPerSeatingCap.toFixed(2));
            $('txtAmountofDriver').setValue(res.value.AmountofDriverSeat.toFixed(2));
            $('txtStampCharge').setValue(res.value.StampCharge.toFixed(2));


            analyzeLoanApplicationManager.renderOwnDamage();
        }

    },
    renderOwnDamage: function() {

        var res = PlussAuto.AnalyzeLoanApplication.GetOwnDamage();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            Page.ownDamageArray = res.value;
        }

    },

    //Get Data
    GetUserData: function() {
        var res = PlussAuto.AnalyzeLoanApplication.gett_pluss_userAll();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            analyzeLoanApplicationHelper.populateUserCombo(res.value);
        }
    },

    GetAllAutoBranch: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AnalyzeLoanApplication.GetAllAutoBranch();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbSource1').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Source';
        ExtraOpt.value = '-1';
        document.getElementById('cmbSource1').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].AutoBranchName;
            opt.value = res.value[i].AutoBranchId;
            document.getElementById('cmbSource1').options.add(opt);
        }
    },

    getGeneralInsurance: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AnalyzeLoanApplication.getGeneralInsurance();
        //var insType = res.value[i].InsType;

        Page.artaPreference = res.value;

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbARTAReference').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select a Status';
        ExtraOpt.value = '-1';
        document.getElementById('cmbARTAReference').options.add(ExtraOpt);


        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");

            opt.text = res.value[i].CompanyName;
            opt.value = res.value[i].InsCompanyId;

            document.getElementById('cmbARTAReference').options.add(opt);
        }
    },



    getBank: function() {

        var res = PlussAuto.AnalyzeLoanApplication.GetBank(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateBankCombo(res);
    },

    getFacility: function() {

        var res = PlussAuto.AnalyzeLoanApplication.getFacility();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateFacilityCombo(res);
    },

    getSector: function() {

        var res = PlussAuto.AnalyzeLoanApplication.GetSector();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateSectorCombo(res);
    },
    getCibInfo: function() {

    var res = PlussAuto.AnalyzeLoanApplication.getCibInfo();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateCibInfoCombo(res);
    },

    getIncomeAssement: function() {
        var res = PlussAuto.AnalyzeLoanApplication.GetIncomeAssement();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateIncomeAssementCombo(res);
    },

    GetEmployeSegment: function() {
        var res = PlussAuto.AnalyzeLoanApplication.GetEmployeSegment();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateSegmentCombo(res);
    },
    GetEmployerForSalaried: function() {
        var res = PlussAuto.AnalyzeLoanApplication.GetEmployerForSalaried();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateEmployerForSalaried(res);
    },

    changeSector: function(bankNo, accountType) {
        var sectorId = 0;
        if (accountType == "Pr") {
            sectorId = $F("cmbIndustryForPrimaryBank" + bankNo);
        }
        else if (accountType == "Jt") {
            sectorId = $F("cmbIndustryForJointBank" + bankNo);
        }

        var res = PlussAuto.AnalyzeLoanApplication.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateSubSectorCombo(res, bankNo, accountType);
        loanCalculatorHelPer.setBussinessManCalculation(bankNo, accountType, 'BM');
    },

    changeSectorForSelfEmployed: function(bankNo, accountType) {
        var sectorId = 0;

        if (accountType == "Pr") {
            sectorId = $F("cmbIndustryForSEPrBank" + bankNo);
        }
        else if (accountType == "Jt") {
            sectorId = $F("cmbIndustryForSEJtBank" + bankNo);
        }

        var res = PlussAuto.AnalyzeLoanApplication.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateSubSectorComboForSelfEmployed(res, bankNo, accountType);
        analyzeLoanApplicationHelper.changePrivateInCome(bankNo, accountType, "Se");
        loanCalculatorHelPer.setBussinessManCalculation(bankNo, accountType, 'SE');
    },
    GetSubSectorForLandLord: function(sectorId, accountType) {
        var res = PlussAuto.AnalyzeLoanApplication.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateSubSectorComboForLandLord(res, accountType);
    },

    GetBranch: function(elementId) {
        // alert(ElementID);
        //        debugger;

        var bankId = $F(elementId);
        if (bankId == 0) {
        }
        else {
            var res = PlussAuto.AnalyzeLoanApplication.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            analyzeLoanApplicationHelper.populateBranchCombo(res, elementId);
        }

    },
    GetBranchNameForIncomeGenerator: function(bankIdElement, elementId) {
        // alert(ElementID);
        //        debugger;
        var bankId = $F(bankIdElement);
        if (bankId == 0) {
        }
        else {
            var res = PlussAuto.AnalyzeLoanApplication.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            analyzeLoanApplicationHelper.populateBranchForIncomeGeneratorCombo(res, elementId);
        }

    },
    changeBranchNameForSourceOnApplicantDetailsForGeneral: function() {
        var branchId = $F("cmbSource1");
        var res = PlussAuto.AnalyzeLoanApplication.GetAllAutoSourceByBranchId(branchId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbBranch').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Source';
        ExtraOpt.value = '-1';
        document.getElementById('cmbBranch').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].SourceName;
            opt.value = res.value[i].AutoSourceId;
            document.getElementById('cmbBranch').options.add(opt);
        }
        Page.sourceForGeneralArray = res.value;
    },

    getVehicleStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AnalyzeLoanApplication.getVehicleStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmdVehicleStatus').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vehicle Status';
        ExtraOpt.value = '-1';
        document.getElementById('cmdVehicleStatus').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmdVehicleStatus').options.add(opt);
        }
    },

    getManufacturer: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AnalyzeLoanApplication.GetAutoManufacturer();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.manufactureArray = res.value;
        //
        document.getElementById('cmbManufacturer').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Manufacturer';
        ExtraOpt.value = '-1';
        document.getElementById('cmbManufacturer').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ManufacturerName;
            opt.value = res.value[i].ManufacturerId;
            document.getElementById('cmbManufacturer').options.add(opt);
        }

    },

    getModelByManufactureId: function() {
        var manufacturerID = $F('cmbManufacturer');

        //var res = PlussAuto.LoanApplication.GetAutoModelByManufacturerID(manufacturerID);
        var res = PlussAuto.AnalyzeLoanApplication.getModelbyManufacturerID(manufacturerID);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.modelArray = res.value;

        document.getElementById('cmbModel').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Model';
        ExtraOpt.value = '-1';
        document.getElementById('cmbModel').options.add(ExtraOpt);
        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            var ch = res.value[i].Model;
            if (res.value[i].TrimLevel != "") {
                ch += "-" + res.value[i].TrimLevel;

            }
            opt.text = ch;
            opt.value = res.value[i].VehicleId;
            document.getElementById('cmbModel').options.add(opt);
        }

        if (Page.vehicleId != 0) {
            $('cmbModel').setValue(Page.vehicleId);

            analyzeLoanApplicationHelper.changeModel();
        }
        //analyzeLoanApplicationManager.getvaluepackallowed();
        analyzeLoanApplicationManager.Getvaluepackallowed();
    },

    Getvaluepackallowed: function() {
        var manufacturerId = $F('cmbManufacturer');
        var vehicleId = $F("cmbModel");
        var carStatus = $F("cmdVehicleStatus");
        if (manufacturerId == "-1" || vehicleId == "-1" || carStatus == "-1") {
            $('cmbvaluePackAllowed').setValue('0');
            return;
        }

        var res = PlussAuto.AnalyzeLoanApplication.getValuePackAllowed(manufacturerId, vehicleId, carStatus);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value != null) {
            $('cmbvaluePackAllowed').setValue(res.value.ValuePackAllowed);
        }
        else {
            $('cmbvaluePackAllowed').setValue('0');
        }
        loanCalculatorHelPer.calculateTenorSettings();

    },

    GetDataByLLID: function() {
        var llId = $F("txtLLID");
        var res = PlussAuto.AnalyzeLoanApplication.GetDataByLLID(llId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        $("spnWait").innerHTML = "";
        if (res.value == "No Data found.") {
            alert(res.value);
            $('maindivContent').style.display = 'none';
            $('divAutoloanSummary').style.display = 'block';
            return;
        }
        else {
            //Primary Applicant
            $('cmbLabelForDlaLimit').setValue('1');
            $('txtPrProfessionId').setValue(res.value.objAutoPRProfession.PrimaryProfession);
            $('txtPrOtherProfessionId').setValue(res.value.objAutoPRProfession.OtherProfession);


            $('txtPrimaryApplicant').setValue(res.value.objAutoPRApplicant.PrName);
            $('txtDateOfBirth').setValue(res.value.objAutoPRApplicant.DateofBirth.format());
            $('txtCurrenAge').setValue(res.value.objAutoPRApplicant.Age);
            $('txtPrimaryProfession').setValue(res.value.objAutoPRProfession.PrimaryProfessionName);
            $('txtOtherProfession').setValue(res.value.objAutoPRProfession.OtherProfessionName);
            $('txtDeclaredIncome').setValue(res.value.objAutoPRFinance.DPIAmount);
            $('txtDeclaredIncomeForLcAll').setValue(res.value.objAutoPRFinance.DPIAmount);
            $('txtVerifiedAddress').setValue(res.value.objAutoPRApplicant.ResidenceAddress);

            //Joint Applicant
            $('txtJointApplicant').setValue(res.value.objAutoJTApplicant.JtName);
            $('txtJtDateOfBirth').setValue(res.value.objAutoJTApplicant.DateofBirth.format());
            $('txtJtCurrntAge').setValue(res.value.objAutoJTApplicant.Age);
            $('txtJtProfession').setValue(res.value.objAutoJTProfession.JtPrimaryProfessionName);
            $('txtJtDeclaredIncome').setValue(res.value.objAutoJTFinance.DPIAmount);
            $('txtDeclaredIncomeForLcJointAll').setValue(res.value.objAutoJTFinance.DPIAmount);

            $('txtJtProfessionId').setValue(res.value.objAutoJTProfession.PrimaryProfession);
            $('analystMasterIdHiddenField').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);

            if (res.value.objAuto_App_Step_ExecutionByUser != null) {

                $('cmbAppraisedby').setValue(res.value.objAuto_App_Step_ExecutionByUser.AppRaiseBy);
                var usr = Page.userArray.findByProp('USER_ID', res.value.objAuto_App_Step_ExecutionByUser.AppRaiseBy);
                if (usr) {
                    $('txtAppraiserCode').setValue(usr.USER_CODE);
                }
                $('cmbSupportedby').setValue(res.value.objAuto_App_Step_ExecutionByUser.AppSupportBy);
                usr = Page.userArray.findByProp('USER_ID', res.value.objAuto_App_Step_ExecutionByUser.AppSupportBy);
                if (usr) {
                    $('txtSupportedbyCode').setValue(usr.USER_CODE);
                }

            }

            var isJoint = res.value.objAutoLoanMaster.JointApplication;
            if (isJoint == true) {
                $('divForJointApplicant').style.display = 'block';
            }
            else {
                $('divForJointApplicant').style.display = 'none';
            }


            var currentAgeForPrimary = parseInt($F("txtCurrenAge"));
            Page.age = currentAgeForPrimary;
            var currentAgeForJoin = parseInt($F("txtJtCurrntAge"));
            Page.jointage = currentAgeForJoin;
            $("lblFieldrequiredforPrAge").innerHTML = "";
            if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && currentAgeForJoin < 65 && isJoint == true) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
                $('cmbLabelForDlaLimit').setValue('2');

            }
            if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && isJoint == false) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
                $('cmbLabelForDlaLimit').setValue('2');

            }
            if (currentAgeForJoin > 65 && currentAgeForJoin < 69 && currentAgeForPrimary < 65 && isJoint == true) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
                $('cmbLabelForDlaLimit').setValue('2');

            }
            if ((currentAgeForPrimary > 68 || currentAgeForJoin > 68) && isJoint == true) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 3 required";
                $('cmbLabelForDlaLimit').setValue('3');
            }
            if (currentAgeForPrimary > 68 && isJoint == false) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 3 required";
                $('cmbLabelForDlaLimit').setValue('3');
            }

            var jtPrimaryProfession = parseInt($F("txtJtProfessionId"));
            var prPrimaryProfession = parseInt($F("txtPrProfessionId"));


            //Application Details---------------------------------------------------------------------------------------------------------------------
            //$('cmbSource1').setValue(res.value.objAutoLoanMaster.Source);

            if (res.value.objAutoLoanVehicle.ValuePackAllowed == false) {
                $('cmbValuePack').setValue('0');
            }
            else {
                $('cmbValuePack').setValue('1');
            }

            var bodyText = "";
            var pattern = "";
            var replaceText = "";
            var replacementtext = "";

            $('cmbProductName').setValue(res.value.objAutoLoanMaster.ProductId);
            $('txtAskingtenor').setValue(res.value.objAutoLoanMaster.AskingTenor);
            $('txtAskingTenorForLc').setValue(res.value.objAutoLoanMaster.AskingTenor);
            $('txtAskingInterest').setValue(res.value.objAutoLoanMaster.AskingRate);
            $('txtInterestLCAskingRate').setValue(res.value.objAutoLoanMaster.AskingRate);

            if (res.value.objAutoLoanMaster.ProductId == 3) {
                labelChangeHelper.showFinanceLabelForAnalyst();
            }
            else {
                labelChangeHelper.showLoanLabelForAnalyst();

                if (res.value.objAutoLoanMaster.ProductId == 1) {
                    $('cmbARTA').setValue('2');
                    $('cmbGeneralInsurence').setValue('2');
                    $('txtAppropriteRate').setValue('4.43');
                    $('cmbARTA').disabled = 'false';
                    $('cmbGeneralInsurence').disabled = 'false';
                    $('cmbARTAReference').disabled = 'false';
                }
                else {
                    $('cmbARTA').disabled = '';
                    $('cmbGeneralInsurence').disabled = '';
                    $('cmbARTAReference').disabled = '';
                }

            }

            //Vehicle Status---------------------------------------------------------------------------------------------------------------------------
            $('cmdVehicleStatus').setValue(res.value.objAutoLoanVehicle.VehicleStatus);
            if (res.value.objAutoLoanVehicle.VehicleStatus == 3) {
                $('cmbCarVerification').setValue('2');
            }
            else {
                $('cmbCarVerification').setValue(res.value.objAutoLoanVehicle.CarVerification);
                if (res.value.objAutoLoanVehicle.CarVerification == 1) {
                    $('divLblDeliveryStatus').style.display = 'block';
                    $('divDeliveryStatus').style.display = 'block';
                }
                else {
                    $('divLblDeliveryStatus').style.display = 'none';
                    $('divDeliveryStatus').style.display = 'none';
                }
            }

            //From Arif Code---------------------------------------------------------------------------------------
            if (res.value.AutoLoanAnalystApplication != null) {

                $('txtAutoLoanMasterId').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);
                $('tAnalystRemark').setValue(res.value.AutoLoanAnalystMaster.AnalystRemark);
                $('txtAnalystMasterID').setValue(res.value.AutoLoanAnalystMaster.AnalystMasterId);
                $('txtloanAnalystApplicationID').setValue(res.value.AutoLoanAnalystApplication.Id);
                $('txtVehicleDetailID').setValue(res.value.AutoLoanAnalystVehicleDetail.Id);
                if (res.value.objAutoLoanMaster.JointApplication) {
                    $('txtIsJoint').setValue('1');
                } else {
                    $('txtIsJoint').setValue('0');
                }
                $('cmbSource1').setValue(res.value.AutoLoanAnalystApplication.Source);
                analyzeLoanApplicationManager.changeBranchNameForSourceOnApplicantDetailsForGeneral();
                $('cmbBranch').setValue(res.value.AutoLoanAnalystApplication.BranchId);
                $('txtBranchCode').setValue(res.value.AutoLoanAnalystApplication.BranchCode);
                $('cmbValuePack').setValue(res.value.AutoLoanAnalystApplication.ValuePack);
                $('cmbBrowingRelationship').setValue(res.value.AutoLoanAnalystApplication.BorrowingRelationShip);
                $('txtRelationshipCode').setValue(res.value.AutoLoanAnalystApplication.BorrowingRelationShipCode);
                $('cmbCustomerStatus').setValue(res.value.AutoLoanAnalystApplication.CustomerStatus);

                $('cmbAskingRepaymentmethod').setValue(res.value.AutoLoanAnalystApplication.AskingRepaymentMethod);
                $('cmbAskingRepaymentmethodForFinalize').setValue(askingrepayment[res.value.AutoLoanAnalystApplication.AskingRepaymentMethod]);
                $('txtInstrumentForFinalizetab').setValue(res.value.AutoLoanAnalystApplication.AskingRepaymentMethod);
                $('cmbARTA').setValue(res.value.AutoLoanAnalystApplication.ARTA);
                $('cmbARTAReference').setValue(res.value.AutoLoanAnalystApplication.ARTAStatus);
                $('cmbGeneralInsurence').setValue(res.value.AutoLoanAnalystApplication.GeneralInsurance);
                $('cmbCashSecured').setValue(res.value.AutoLoanAnalystApplication.CashSecured100Per);
                $('cmbCCBundle').setValue(res.value.AutoLoanAnalystApplication.CCBundle);
                if (res.value.AutoLoanAnalystApplication.CashSecured100Per == 1) {
                    $('divSecurityValue').style.display = "block";
                    $('divSequrityvaluetextbox').style.display = "block";
                    $('divCashForLc').style.display = "block";
                    $('txtSecurityValue').setValue(res.value.AutoLoanAnalystApplication.SecurityValue);
                }
                else {
                    $('divSecurityValue').style.display = "none";
                    $('divSequrityvaluetextbox').style.display = "none";
                    $('divCashForLc').style.display = "none";
                    $('txtSecurityValue').setValue('0');
                }

            }

            //End Arif Code------------------------------------------------------------------------------------------

            $('cmbManufacturer').setValue(res.value.objAutoLoanVehicle.Auto_ManufactureId);
            $('txtAutoLoanVehicleId').setValue(res.value.objAutoLoanVehicle.AutoLoanVehicleId);
            Page.vehicleId = res.value.objAutoLoanVehicle.VehicleId;
            analyzeLoanApplicationHelper.changeManufacture();
            $('cmbModel').setValue(res.value.objAutoLoanVehicle.VehicleId);
            analyzeLoanApplicationHelper.changeModel();
            $('hdnModel').setValue(res.value.objAutoLoanVehicle.Model);
            $('txtVehicleType').setValue(res.value.objAutoLoanVehicle.VehicleType);
            $('txtManufacturaryear').setValue(res.value.objAutoLoanVehicle.ManufacturingYear);
            $("lblManufactureYearAlert").innerHTML = "";
            if (res.value.objAutoLoanVehicle.ManufacturingYear < 2007) {
                alert("Car Import Docs Required");

                $("lblManufactureYearAlert").innerHTML = "Car Import Docs Required";
                $('divmanuyearalerttext').style.display = "block";
                $('divmanufacalertlbl').style.display = "block";
            }
            else {
                $('divmanuyearalerttext').style.display = "none";
                $('divmanufacalertlbl').style.display = "none";
            }

            $('txtEngineCapacityCC').setValue(res.value.objAutoLoanVehicle.EngineCC);
            $('cmsSeatingCapacity').setValue(res.value.objAutoLoanVehicle.SeatingCapacity);
            $('cmbVendor').setValue(res.value.objAutoLoanVehicle.VendorId);

            var vendoreId = $F("cmbVendor");
            if (vendoreId == "-1") {
                $('txtVendorName').disabled = '';
            }
            else {
                var vdr = Page.vendoreArray.findByProp('AutoVendorId', vendoreId);
                if (vdr) {
                    $('txtVendoreCode').setValue(vdr.VendorCode);
                    $('txtVendorName').disabled = 'false';
                }
            }

            $('txtQuotedPrice').setValue(res.value.objAutoLoanVehicle.QuotedPrice);
            $('txtConsideredprice').setValue(res.value.objAutoLoanVehicle.VerifiedPrice);
            $('cmbPriceConsideredOn').setValue(res.value.objAutoLoanVehicle.PriceConsideredBasedOn);

            $('txtVehicleStatusCode').setValue(res.value.objAutoLoanVehicle.StatusCode);
            $('txtManufacturarCode').setValue(res.value.objAutoLoanVehicle.ManufacturerCode);
            $('txtVehicleModelCode').setValue(res.value.objAutoLoanVehicle.ModelCode);
            $('txtVehicletypeCode').setValue(res.value.objAutoLoanVehicle.VehicleTypeCode);
            $('txtVendoreCode').setValue(res.value.objAutoLoanVehicle.VendorCode);
            if (res.value.objAutoLoanVehicle.VendoreRelationshipStatus != 1 && res.value.objAutoLoanVehicle.VendoreRelationshipStatus != 0) {
                alert("The Vendore is Negative Listed");
                $('cmbVendoreRelationShip').setValue('2');
            }
            else {
                $('cmbVendoreRelationShip').setValue(res.value.objAutoLoanVehicle.VendoreRelationshipStatus);
            }

            Page.tenor = res.value.objAutoLoanVehicle.Tenor;
            //Analyst Part
            //For Analyst part.

            if (res.value.AutoLoanAnalystVehicleDetail != null) {

                $('cmbvaluePackAllowed').setValue(res.value.AutoLoanAnalystVehicleDetail.valuePackAllowed);
                $('cmsSeatingCapacity').setValue(res.value.AutoLoanAnalystVehicleDetail.SeatingCapacity);
                $('txtConsideredprice').setValue(res.value.AutoLoanAnalystVehicleDetail.ConsideredPrice);
                $('cmbPriceConsideredOn').setValue(res.value.AutoLoanAnalystVehicleDetail.PriceConsideredOn);
                $('cmbCarVerification').setValue(res.value.AutoLoanAnalystVehicleDetail.CarVarification);
                analyzeLoanApplicationHelper.changeCarVerification();
                $('cmbDeliveryStatus').setValue(res.value.AutoLoanAnalystVehicleDetail.DeliveryStatus);
                $('txtregistrationYear').setValue(res.value.AutoLoanAnalystVehicleDetail.RegistrationYear);
            }


            analyzeLoanApplicationHelper.showTabbyType(res.value.objAutoPRProfession.PrimaryProfession, res.value.objAutoPRProfession.OtherProfession);

            //General Tab
            var scbAccount = "<option value=\"-1\">Account Number</option>";
            if (res.value.AutoApplicantSCBAccountList.length > 0) {
                for (var i = 0; i < res.value.AutoApplicantSCBAccountList.length; i++) {
                    var idfieldscbaccount = (i + 1);
                    $('txtAccountNumberForSCB' + idfieldscbaccount).setValue(res.value.AutoApplicantSCBAccountList[i].AccountNumberSCB);
                    $('cmbAccountStatusForSCB' + idfieldscbaccount).setValue(res.value.AutoApplicantSCBAccountList[i].AccountStatus);
                    $('cmbPriorityAcHolderForSCB' + idfieldscbaccount).setValue(res.value.AutoApplicantSCBAccountList[i].PriorityHolder);
                    scbAccount += "<option value=\"" + res.value.AutoApplicantSCBAccountList[i].AccountNumberSCB + "\">" + res.value.AutoApplicantSCBAccountList[i].AccountNumberSCB + "</option>"
                }
                $("cmbAccountNumberForSCBForFinalizetab").update(scbAccount);
            }
            else {
                scbAccount += "<option value=\"" + res.value.objAutoPRAccount.AccountNumberForSCB + "\">" + res.value.objAutoPRAccount.AccountNumberForSCB + "</option>"
                $('txtAccountNumberForSCB1').setValue(res.value.objAutoPRAccount.AccountNumberForSCB);
                $("cmbAccountNumberForSCBForFinalizetab").update(scbAccount);
            }

            for (var i = 0; i < res.value.objAutoPRAccountDetailList.length; i++) {
                var idfield = (i + 1);
                $('cmbBankNameForOther' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].BankId);
                analyzeLoanApplicationManager.GetBranch('cmbBankNameForOther' + idfield);
                $('cmbBranchNameOther' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].BranchId);
                $('txtAccountNumberother' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountNumber);
                $('cmbAccountStatus' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountCategory);
                $('cmbAccountType' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountType);
                var bankName = $('cmbBankNameForOther' + idfield).options[res.value.objAutoPRAccountDetailList[i].BankId].text;
                res.value.objAutoPRAccountDetailList[i].BankName = bankName;
            }

            analystfinalizationHelper.populaterepaymentbankName(res.value.objAutoPRAccountDetailList);

            //Exposures on SCB For Term loans
            var optlinkForColl = "<option value='-1'>Select From list</option>";
            Page.facilityListArray = res.value.objAutoLoanFacilityList;
            var j = 0;
            var k = 0;
            var l = 0;
            for (var i = 0; i < res.value.objAutoLoanFacilityList.length; i++) {
                if (res.value.objAutoLoanFacilityList[i].FacilityType == 22 || res.value.objAutoLoanFacilityList[i].FacilityType == 18) {
                    var idfieldforExposerForCreditCard = j + 1;
                    $('cmbTypeForCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);
                    var typeforCreditCardobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    $('txtFlagForcreditCard' + idfieldforExposerForCreditCard).setValue(typeforCreditCardobject.FACI_CODE);
                    $('txtOriginalLimitForCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    $('txtInterestRateCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                    var credInterest = res.value.objAutoLoanFacilityList[i].PresentLimit * (res.value.objAutoLoanFacilityList[i].InterestRate / 100);
                    $('txtInterestForCreditCard' + idfieldforExposerForCreditCard).setValue(credInterest);
                    j = j + 1;
                }
                else if (res.value.objAutoLoanFacilityList[i].FacilityType == 23) {
                    var idfieldforExposerForOverdraft = k + 1;
                    var typeforOverDraftobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    $('txtOverdraftFlag' + idfieldforExposerForOverdraft).setValue(typeforOverDraftobject.FACI_CODE);
                    $('txtOverdraftInterest' + idfieldforExposerForOverdraft).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                    $('txtOverdraftlimit' + idfieldforExposerForOverdraft).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    var interst = parseFloat(parseFloat((res.value.objAutoLoanFacilityList[i].PresentLimit * res.value.objAutoLoanFacilityList[i].InterestRate) / 12) / 100);
                    $('txtOverdraftInteres' + idfieldforExposerForOverdraft).setValue(interst);


                    //Income Generator----------------------------------
                    //OVER DRAFT FACILITIES WITH SCB For bussiness Man Tab--------------------

                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        analyzeLoanApplicationHelper.bussinessManTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Pr", "BM");

                    }
                    if (jtPrimaryProfession == 1) {
                        analyzeLoanApplicationHelper.bussinessManTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Jt", "BM");
                    }

                    //OVER DRAFT FACILITIES WITH SCB For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        analyzeLoanApplicationHelper.bussinessManTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Pr", "SE");

                    }
                    if (jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) {
                        analyzeLoanApplicationHelper.bussinessManTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Jt", "SE");
                    }

                    k = k + 1;
                }
                else {
                    var idfieldforExposerForTermsloans = l + 1;
                    $('cmbTypeForTermLoans' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);

                    var typeforOtherobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    $('txtflag' + idfieldforExposerForTermsloans).setValue(typeforOtherobject.FACI_CODE);
                    $('txtInterestRate' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                    $('txtLimit' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    $('txtoutStanding' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentBalance);
                    $('txtEMI' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentEMI);
                    $('txtEMIPer' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].EMIPercent);
                    var emiShare = parseFloat(parseFloat(res.value.objAutoLoanFacilityList[i].PresentEMI) * parseFloat(res.value.objAutoLoanFacilityList[i].EMIPercent / 100));
                    $('txtEMIShare' + idfieldforExposerForTermsloans).setValue(emiShare.toFixed(2));
                    //Preveios repayment On Scb For bussiness Man Tab
                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        analyzeLoanApplicationHelper.prevreplaceMentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Pr", "BM");
                    }
                    if (jtPrimaryProfession == 1) {
                        analyzeLoanApplicationHelper.prevreplaceMentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Jt", "BM");
                    }

                    //Preveios repayment On Scb For bussiness Man Tab For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        analyzeLoanApplicationHelper.prevreplaceMentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Pr", "SE");

                    }
                    if (jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) {
                        analyzeLoanApplicationHelper.prevreplaceMentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Jt", "SE");
                    }
                    optlinkForColl += "<option value=" + res.value.objAutoLoanFacilityList[i].FacilityType + ">" + typeforOtherobject.FACI_NAME + "</option>";

                    l = l + 1;
                }

            }
            $("cmbColleterization").update(optlinkForColl);
            $("cmbColleterizationWith").update(optlinkForColl);

            if (res.value.AutoLoanAnalystMaster != null) {
                $("cmbColleterization").setValue(res.value.AutoLoanAnalystMaster.Colleterization);
                $("cmbColleterizationWith").setValue(res.value.AutoLoanAnalystMaster.ColleterizationWith);
                $("cmbBureauHistory").setValue(res.value.AutoLoanAnalystMaster.CibInfoId);
                var cib = Page.cibInfo.findByProp('CibInfoId', res.value.AutoLoanAnalystMaster.CibInfoId);
                if (cib) {
                    $("txtCibInfoDesc").setValue(cib.CibInfoDescription);
                    $("txtCibInfoCode").setValue(cib.CibInfoCode);

                }
                else {
                    $("txtCibInfoDesc").setValue('');
                    $("txtCibInfoCode").setValue('');
                }
            }

            var m = 0;
            var n = 0;
            var p = 0;

            for (var i = 0; i < res.value.objAutoLoanSecurityList.length; i++) {
                if (res.value.objAutoLoanSecurityList[i].FacilityType == 22 || res.value.objAutoLoanSecurityList[i].FacilityType == 18) {
                    var idfieldforExposerForCreditCardSequrity = p + 1;
                    var typeforCreditCardobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforCreditCardobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);

                        $('txtCreditCardIssuingOffice' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                        $('txtCreditCardIssuingDate' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());
                        $('txtCreditCardInterestRate' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);

                        var facalityObjectForCredtcard = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForCredtcard != undefined) {
                            var outStandingForcreditCard = facalityObjectForCredtcard.PresentLimit;

                            var ltvForCreditturnOver = parseFloat(outStandingForcreditCard / res.value.objAutoLoanSecurityList[i].XTVRate);

                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(ltvForCreditturnOver.toFixed(2));
                        }
                        else {
                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                        }

                    }

                    p = p + 1;
                }
                else if (res.value.objAutoLoanSecurityList[i].FacilityType == 23) {
                    var idfieldforExposerForOverDraftSequrity = n + 1;
                    var typeforOverDraftobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforOverDraftobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);

                        $('cmbStatusForSecuritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);

                        $('txtOverDraftIssuingOffice' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                        $('txtOverDraftIssuingDate' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());
                        $('txtOverDraftInterestRate' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);


                        var facalityObjectForOverDraft = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForOverDraft != undefined) {
                            var originalLimit = facalityObjectForOverDraft.PresentLimit;

                            var ltvForOverDraft = parseFloat(originalLimit / res.value.objAutoLoanSecurityList[i].XTVRate);

                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(ltvForOverDraft.toFixed(2));
                        }
                        else {
                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                        }

                    }

                    n = n + 1;
                }
                else {
                    var idfieldforExposerForTermLoanSequrity = m + 1;
                    var typeforTermLoanobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforTermLoanobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequrities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecurities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);

                        $('txttermloanIssuingOffice' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                        $('txttermloanIssuingDate' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());
                        $('txttermloanInterestRate' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);

                        var facalityObjectFortermLoan = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectFortermLoan != undefined) {
                            var outStandingFortermloan = facalityObjectFortermLoan.PresentBalance;

                            var ltvFortermLoan = parseFloat(outStandingFortermloan / res.value.objAutoLoanSecurityList[i].XTVRate);

                            $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue(ltvFortermLoan.toFixed(2));
                        }
                        else {
                            $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                        }

                    }

                    m = m + 1;
                }

            }


            var s = 0;
            var d = 0;
            var f = 0;
            for (var i = 0; i < res.value.AutoLoanFacilityOffSCBList.length; i++) {
                if (res.value.AutoLoanFacilityOffSCBList[i].Type == 2) {
                    var idfieldforOffSCBForCreditCard = s + 1;
                    $('cmbFinancialInstitutionForCreditCardOffSCB' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    $('txtOriginalLimitForcreditCardOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    $('txtInterestRateForCreditCardForOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].InterestRate);
                    var interest = (res.value.AutoLoanFacilityOffSCBList[i].InterestRate / 100) * res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit;
                    $('txtInterestForCreditCardForOffScb' + idfieldforOffSCBForCreditCard).setValue(interest);
                    $('cmbStatusForCreditCardOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);

                    s = s + 1;
                }
                else if (res.value.AutoLoanFacilityOffSCBList[i].Type == 3) {

                    var idfieldforOffSCBForOverdraft = d + 1;
                    $('cmbFinancialInstitutionForOverDraftOffSCB' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    $('txtOriginalLimitForOverDraftOffScb' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    $('txtinterestRateForOverDraftOffSCB' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].InterestRate);
                    //$('txtInterestForOverDraftInterest' + idfieldforOffSCBForCreditCard).setValue('0');
                    analyzeLoanApplicationHelper.changeOverDraftInterstForOffSCB(idfieldforOffSCBForOverdraft);
                    $('cmbStatusForOverDraftOffScb' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);

                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        analyzeLoanApplicationHelper.bussinessManTabOverDraftOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Pr", "BM");

                    }
                    if (jtPrimaryProfession == 1) {
                        analyzeLoanApplicationHelper.bussinessManTabOverDraftOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Jt", "BM");
                    }

                    //OVER DRAFT FACILITIES WITH SCB For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        analyzeLoanApplicationHelper.bussinessManTabOverDraftOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Pr", "SE");

                    }
                    if (jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) {
                        analyzeLoanApplicationHelper.bussinessManTabOverDraftOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Jt", "SE");
                    }

                    //Need to do For BankStatement Tab



                    d = d + 1;
                }
                else {
                    var idfieldforOffSCBForTermsloans = f + 1;
                    $('cmbFinancialInstitutionForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    $('txtOriginalLimitForOffScb' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    $('txtEMIforOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMI);
                    //analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB(f);

                    $('txtEMIPerForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMIPercent);
                    //analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB(f);
                    $('txtEMIShareForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMIShare);
                    $('cmbStatusForOffScb' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);

                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        analyzeLoanApplicationHelper.prevreplaceMentOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Pr", "BM");
                    }
                    if (jtPrimaryProfession == 1) {
                        analyzeLoanApplicationHelper.prevreplaceMentOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Jt", "BM");
                    }

                    //Preveios repayment On Scb For bussiness Man Tab For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        analyzeLoanApplicationHelper.prevreplaceMentOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Pr", "SE");

                    }
                    if (jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) {
                        analyzeLoanApplicationHelper.prevreplaceMentOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Jt", "SE");
                    }


                    f = f + 1;
                }

            }

            //OFF SCB Facility List end





            var m = 0;
            var n = 0;
            var p = 0;

            for (var i = 0; i < res.value.objAutoLoanSecurityList.length; i++) {
                if (res.value.objAutoLoanSecurityList[i].FacilityType == 22 || res.value.objAutoLoanSecurityList[i].FacilityType == 18) {
                    var idfieldforExposerForCreditCardSequrity = p + 1;
                    var typeforCreditCardobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforCreditCardobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        var facalityObjectForCredtcard = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForCredtcard != undefined) {
                            var outStandingForcreditCard = facalityObjectForCredtcard.PresentLimit;

                            var ltvForCreditturnOver = parseFloat(outStandingForcreditCard / res.value.objAutoLoanSecurityList[i].XTVRate);

                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(ltvForCreditturnOver.toFixed(2));
                        }
                        else {
                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                        }

                    }

                    p = p + 1;
                }
                else if (res.value.objAutoLoanSecurityList[i].FacilityType == 23) {
                    var idfieldforExposerForOverDraftSequrity = n + 1;
                    var typeforOverDraftobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforOverDraftobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);



                        var facalityObjectForOverDraft = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForOverDraft != undefined) {
                            var originalLimit = facalityObjectForOverDraft.PresentLimit;

                            var ltvForOverDraft = parseFloat(originalLimit / res.value.objAutoLoanSecurityList[i].XTVRate);

                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(ltvForOverDraft.toFixed(2));
                        }
                        else {
                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                        }

                    }

                    n = n + 1;
                }
                else {
                    var idfieldforExposerForTermLoanSequrity = m + 1;
                    var typeforTermLoanobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    //if (typeforTermLoanobjectForSequrity.FACI_CODE == "U") {
                    $('cmbTypeForSequrities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                    $('txtSecurityFV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                    $('txtSecurityPV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                    $('cmbStatusForSecurities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                    var facalityObjectFortermLoan = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (facalityObjectFortermLoan != undefined) {
                        var outStandingFortermloan = facalityObjectFortermLoan.PresentBalance;

                        var ltvFortermLoan = parseFloat(outStandingFortermloan / res.value.objAutoLoanSecurityList[i].XTVRate);

                        $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue(ltvFortermLoan.toFixed(2));
                    }
                    else {
                        $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                    }

                    //}

                    m = m + 1;
                }

            }


            loanCalculatorHelPer.createExistingrepaymentGrid();
            //Bussiness Man Tab
            // Primary bank Statement-----------------------------------------------------

            Page.bankStatementArray = res.value.AutoBankStatementAvgTotalList;
            if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                var idFieldForBankStatementForIncomegenerator = 0;
                for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {
                    idFieldForBankStatementForIncomegenerator = i + 1;
                    $('txtAccountNameBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].AccountName);
                    $('txtBankStatementIdBMPr' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].PrBankStatementId);
                    $('txtAccountNumberBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].AccountNumber);
                    var accounttypeName = AccountType[res.value.AutoPRBankStatementList[i].AccountCategory];
                    $('txtAccountTypeBank' + idFieldForBankStatementForIncomegenerator).setValue(accounttypeName);
                    $('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').setValue(res.value.AutoPRBankStatementList[i].BankId);

                    var bankName = $('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').options[$('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').selectedIndex].text;
                    $("bmprtabBank" + idFieldForBankStatementForIncomegenerator).innerHTML = bankName;

                    $('txtbmprStatementBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].Statement);




                    analyzeLoanApplicationManager.GetBranchNameForIncomeGenerator('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator', 'cmbBranchNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator');
                    $('cmbBranchNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').setValue(res.value.AutoPRBankStatementList[i].BranchId);

                    if (res.value.AutoPRBankStatementList[i].AccountCategory == 2) {
                        $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue('100');
                    }
                    else if (res.value.AutoPRBankStatementList[i].AccountCategory != 2 && res.value.objAutoPRProfession.EquityShare < 100) {
                        alert("Average Balance is not applicable For Primary Bank Statement" + idFieldForBankStatementForIncomegenerator);
                        if (res.value.objAutoPRProfession.EquityShare < 10) {
                            alert("Label 3 required");
                            $('cmbLabelForDlaLimit').setValue('3');
                            $("lblShareForBMPr" + idFieldForBankStatementForIncomegenerator).innerHTML = "Label 3 required";
                        }
                        $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                        var objPrBm = new Object();
                        objPrBm.BankNo = idFieldForBankStatementForIncomegenerator;
                        objPrBm.isAverageBalance = false;
                        objPrBm.Accounttype = "Pr";
                        objPrBm.TabName = "Bm";
                        Page.isAverageBalancePrimaryArray.push(objPrBm);
                        $("lblForAvgBalBMPr" + idFieldForBankStatementForIncomegenerator).innerHTML = "Average Balance is not applicable";
                    }
                    else {
                        $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                        $("lblShareForBMPr" + idFieldForBankStatementForIncomegenerator).innerHTML = "";
                        $("lblForAvgBalBMPr" + idFieldForBankStatementForIncomegenerator).innerHTML = "";
                    }

                    analyzeLoanApplicationHelper.populateAverageTotalForIncomeGeneratorbankStatement(res.value.AutoPRBankStatementList[i].BankId, res.value.AutoPRBankStatementList[i].AccountNumber, "Primary", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementForIncomegenerator);

                }
            }
            //Joint Bank Statement
            if (jtPrimaryProfession == 1) {
                var idFieldForBankStatementjtForIncomegenerator = 0;
                for (var i = 0; i < res.value.AutoJTBankStatementList.length; i++) {
                    idFieldForBankStatementjtForIncomegenerator = i + 1;
                    $('txtAccountName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].AccountName);
                    $('txtAccountNumber1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].AccountNumber);
                    $('txtBankStatementIdBMJt' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].JtBankStatementId);
                    var accounttypeNamejt = AccountType[res.value.AutoJTBankStatementList[i].AccountCategory];
                    $('txtCompanyName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(accounttypeNamejt);
                    $('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].BankId);

                    var bankName = $('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).options[res.value.AutoJTBankStatementList[i].BankId].text;
                    $("bmjttabBank" + idFieldForBankStatementjtForIncomegenerator).innerHTML = bankName;

                    $('txtbmjtStatementBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].Statement);

                    analyzeLoanApplicationManager.GetBranchNameForIncomeGenerator('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator, 'cmbBranchName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator);
                    $('cmbBranchName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].BranchId);

                    if (res.value.AutoJTBankStatementList[i].AccountCategory == 2) {
                        $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue('100');
                    }
                    else if (res.value.AutoJTBankStatementList[i].AccountCategory != 2 && res.value.objAutoJTProfession.EquityShare < 100) {
                        //alert("Average Balance is not applicable For Joint Applicant" + idFieldForBankStatementjtForIncomegenerator);
                        if (res.value.objAutoJTProfession.EquityShare < 10) {
                            //alert("Label 3 required");
                            $('cmbLabelForDlaLimit').setValue('3');
                            $("lblShareForBMJt" + idFieldForBankStatementjtForIncomegenerator).innerHTML = "Label 3 required";
                        }
                        $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                        var objJtBm = new Object();
                        objJtBm.BankNo = idFieldForBankStatementjtForIncomegenerator;
                        objJtBm.isAverageBalance = false;
                        objJtBm.Accounttype = "Jt";
                        objJtBm.TabName = "Bm";
                        Page.isAverageBalanceJointArray.push(objJtBm);
                        $("lblForAvgBalBMJt" + idFieldForBankStatementjtForIncomegenerator).innerHTML = "Average Balance is not applicable";
                    }
                    else {
                        $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                        $("lblShareForBMJt" + idFieldForBankStatementjtForIncomegenerator).innerHTML = "";
                        $("lblForAvgBalBMJt" + idFieldForBankStatementjtForIncomegenerator).innerHTML = "";
                    }

                    analyzeLoanApplicationHelper.populateAverageTotalForIncomeGeneratorbankStatement(res.value.AutoJTBankStatementList[i].BankId, res.value.AutoJTBankStatementList[i].AccountNumber, "Joint", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementjtForIncomegenerator);


                }
            }
            //Show Hide Bank Statement Tab for Income generator bussiness man tab

            if (prPrimaryProfession == 1 || jtPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                Page.jtCount = res.value.AutoJTBankStatementList.length;
                Page.prCount = res.value.AutoPRBankStatementList.length;
                analyzeLoanApplicationHelper.shobankStatementTabForBussinessMan(res.value.AutoPRBankStatementList.length, res.value.AutoJTBankStatementList.length);
            }


            $('cmbReasonofJointApplicant').setValue(reasonOfJoint[res.value.AutoLoanAnalystMaster.ReasonOfJoint]);
            if (reasonOfJoint[res.value.AutoLoanAnalystMaster.ReasonOfJoint] == "1") {
                analyzeLoanApplicationHelper.showJointApplicant();
            }


            //Self Employed Tab-------------------------------------------------------------------------------------------------
            //Primary bank Statement
            if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6) {
                var idFieldForBankStatementForSePr = 0;
                for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {

                    idFieldForBankStatementForSePr = i + 1;
                    $('txtAccountNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].AccountName);
                    $('txtAccountNumberForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].AccountNumber);
                    $('txtBankStatementIdSEPr' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].PrBankStatementId);
                    var accounttypeSePrName = AccountType[res.value.AutoPRBankStatementList[i].AccountCategory];
                    $('txtAccountTypeBank' + idFieldForBankStatementForSePr).setValue(accounttypeSePrName);
                    $('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].BankId);
                    analyzeLoanApplicationManager.GetBranchNameForIncomeGenerator('cmbBankNameForSEBank' + idFieldForBankStatementForSePr, 'cmbbranchNameForSEBank' + idFieldForBankStatementForSePr);
                    $('cmbbranchNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].BranchId);

                    var bankName = $('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).options[$('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).selectedIndex].text;
                    $("seprtabBank" + idFieldForBankStatementForSePr).innerHTML = bankName;

                    $('txtseprStatementBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].Statement);

                    if (prPrimaryProfession == 4) {

                        $('cmbIndustryForSEPrBank' + idFieldForBankStatementForSePr).setValue('12');
                        analyzeLoanApplicationManager.changeSectorForSelfEmployed(idFieldForBankStatementForSePr, "Pr");
                        $('cmbSubsectorForSEPrBank' + idFieldForBankStatementForSePr).setValue('143');
                        var sub = Page.subsectorArray.findByProp('SUSE_ID', 143);
                        $('txtProfitMarginForSEPrBank' + idFieldForBankStatementForSePr).setValue(sub.SUSE_INTERESTRATE);
                        $('txtProfitMarginCodeForSEPrBank' + idFieldForBankStatementForSePr).setValue(sub.SUSE_IRCODE);

                    }

                    if (res.value.AutoPRBankStatementList[i].AccountCategory == 2) {
                        $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue('100');
                    }
                    else if (res.value.AutoPRBankStatementList[i].AccountCategory != 2 && res.value.objAutoPRProfession.EquityShare < 100) {
                        alert("Average Balance is not applicable For Primary Bank Statement" + idFieldForBankStatementForSePr);
                        if (res.value.objAutoPRProfession.EquityShare < 10) {
                            alert("Label 3 required");
                            $('cmbLabelForDlaLimit').setValue('3');
                            $("lblShareForSEPr" + idFieldForBankStatementForSePr).innerHTML = "Label 3 required";
                        }
                        $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue(res.value.objAutoPRProfession.EquityShare);
                        var objPrSe = new Object();
                        objPrSe.BankNo = idFieldForBankStatementForSePr;
                        objPrSe.isAverageBalance = false;
                        objPrSe.Accounttype = "Pr";
                        objPrSe.TabName = "Se";
                        Page.isAverageBalancePrimaryArray.push(objPrSe);

                        $("lblForAvgBalSEPr" + idFieldForBankStatementForSePr).innerHTML = "Average Balance is not applicable";
                    }
                    else {
                        $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue(res.value.objAutoPRProfession.EquityShare);
                        $("lblShareForSEPr" + idFieldForBankStatementForSePr).innerHTML = "";
                        $("lblForAvgBalSEPr" + idFieldForBankStatementForSePr).innerHTML = "";
                    }

                    analyzeLoanApplicationHelper.populateAverageTotalForSelfEmployedBankStatement(res.value.AutoPRBankStatementList[i].BankId, res.value.AutoPRBankStatementList[i].AccountNumber, "Primary", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementForSePr);


                }

            }
            //Joint Bank Statement For Self Employed

            if (jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) {
                var idFieldForBankStatementjtForSe = 0;
                for (var i = 0; i < res.value.AutoJTBankStatementList.length; i++) {
                    idFieldForBankStatementjtForSe = i + 1;
                    $('txtAccountNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].AccountName);
                    $('txtAccountNumberForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].AccountNumber);
                    $('txtBankStatementIdSEJt' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].JtBankStatementId);
                    var accounttypeNamejtForSe = AccountType[res.value.AutoJTBankStatementList[i].AccountCategory];
                    $('txtAccountTypeForSEJtBank' + idFieldForBankStatementjtForSe).setValue(accounttypeNamejtForSe);
                    $('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].BankId);
                    analyzeLoanApplicationManager.GetBranchNameForIncomeGenerator('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe, 'cmbbranchNameForSEJtBank' + idFieldForBankStatementjtForSe);
                    $('cmbbranchNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].BranchId);


                    var bankName = $('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).options[$('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).selectedIndex].text;
                    $("sejtBank" + idFieldForBankStatementjtForSe).innerHTML = bankName;

                    $('txtsejtStatementBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoPRBankStatementList[i].Statement);
                    if (jtPrimaryProfession == 4) {

                        $('cmbIndustryForSEJtBank' + idFieldForBankStatementjtForSe).setValue('12');
                        analyzeLoanApplicationManager.changeSectorForSelfEmployed(idFieldForBankStatementjtForSe, 'Jt');
                        $('cmbSubsectorForSEJtBank' + idFieldForBankStatementjtForSe).setValue('143');
                        var sub = Page.subsectorArray.findByProp('SUSE_ID', 143);
                        $('txtProfitMarginForSEJtBank' + idFieldForBankStatementjtForSe).setValue(sub.SUSE_INTERESTRATE);
                        $('txtProfitMarginCodeForSEJtBank' + idFieldForBankStatementjtForSe).setValue(sub.SUSE_IRCODE);

                    }


                    if (res.value.AutoJTBankStatementList[i].AccountCategory == 2) {
                        $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue('100');
                    }
                    else if (res.value.AutoJTBankStatementList[i].AccountCategory != 2 && res.value.objAutoJTProfession.EquityShare < 100) {
                        //alert("Average Balance is not applicable For Joint Applicant" + idFieldForBankStatementjtForSe);
                        if (res.value.objAutoJTProfession.EquityShare < 10) {
                            //alert("Label 3 required");
                            $('cmbLabelForDlaLimit').setValue('3');
                            $("lblShareForSEJtBank" + idFieldForBankStatementjtForSe).innerHTML = "Label 3 required";
                        }
                        $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.objAutoJTProfession.EquityShare);
                        var objJtSe = new Object();
                        objJtSe.BankNo = idFieldForBankStatementjtForSe;
                        objJtSe.isAverageBalance = false;
                        objJtSe.Accounttype = "Jt";
                        objJtSe.TabName = "Se";
                        Page.isAverageBalanceJointArray.push(objJtSe);

                        $("lblForAvgBalSEJtBank" + idFieldForBankStatementjtForSe).innerHTML = "Average Balance is not applicable";
                    }
                    else {
                        $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.objAutoJTProfession.EquityShare);
                        $("lblShareForBMJt" + idFieldForBankStatementjtForSe).innerHTML = "";
                        $("lblForAvgBalSEJtBank" + idFieldForBankStatementjtForSe).innerHTML = "";
                    }

                    analyzeLoanApplicationHelper.populateAverageTotalForSelfEmployedBankStatement(res.value.AutoJTBankStatementList[i].BankId, res.value.AutoJTBankStatementList[i].AccountNumber, "Joint", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementjtForSe);


                }
            }
            //Show Hide Bank Statement Tab for Self Employed Tab
            if (prPrimaryProfession == 3 || jtPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6) {
                Page.jtCount = res.value.AutoJTBankStatementList.length;
                Page.prCount = res.value.AutoPRBankStatementList.length;
                analyzeLoanApplicationHelper.shobankStatementTabForSelfEmployed(res.value.AutoPRBankStatementList.length, res.value.AutoJTBankStatementList.length);
            }
            //Land Lord Tab Start-----------------------------------------------------------------------------------------------------
            if (prPrimaryProfession == 5 || res.value.objAutoPRProfession.OtherProfession == 5) {
                $('cmbEmployeeForLandLordPr').setValue('12');
                analyzeLoanApplicationManager.GetSubSectorForLandLord(12, "Pr");
                $('cmbSubsectorForLandLordPr').setValue('145');
                var sub = Page.subsectorArray.findByProp('SUSE_ID', 145);
                $('txtEmployeeCategoryCodeForLandLordPr').setValue(sub.SUSE_IRCODE);
                $('cmbIncomeAssementForLandLordPr').setValue('5');
                var ass = Page.incomeAssementArray.findByProp('INAM_ID', 5);
                $('txtInComeAssementCodeForLandLordPr').setValue(ass.INAM_METHODCODE);


            }
            if (jtPrimaryProfession == 5) {
                $('cmbEmployeeForLandLordJt').setValue('12');
                analyzeLoanApplicationManager.GetSubSectorForLandLord(12, "Jt");
                $('cmbSubsectorForLandLordJt').setValue('145');
                var sub = Page.subsectorArray.findByProp('SUSE_ID', 145);
                $('txtEmployeeCategoryCodeForLandLordJt').setValue(sub.SUSE_IRCODE);
                $('cmbIncomeAssementForLandLordJt').setValue('5');
                var ass = Page.incomeAssementArray.findByProp('INAM_ID', 5);
                $('txtInComeAssementCodeForLandLordJt').setValue(ass.INAM_METHODCODE);
            }
            if (prPrimaryProfession == 2 || res.value.objAutoPRProfession.OtherProfession == 2) {
                $('cmbEmployerForSalariedForPr').setValue(res.value.objAutoPRProfession.NameofCompany);
                var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoPRProfession.NameofCompany);
                if (emp) {
                    $('txtEmployerCodeForPr').setValue(emp.CompanyCode);
                    $('cmbEmployerCategoryForPrSalaried').setValue(emp.CategoryName);
                }
            }
            if (jtPrimaryProfession == 5) {
                $('cmbEmployerForSalariedForJt').setValue(res.value.objAutoJtProfession.NameofCompany);
                var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', employer);
                if (emp) {
                    $('txtEmployerCodeForJt').setValue(emp.CompanyCode);
                    $('cmbEmployerCategoryForJtSalaried').setValue(emp.CategoryName);
                }
            }

            //Land Lord Tab End-------------------------------------------------------------------------------------------------------

            //Edit For Salary------------------------------------------------------------------------------------------------------
            for (var i = 0; i < res.value.objAutoIncomeSalariedList.length; i++) {
                //Primary Salaried Applicant
                if (res.value.objAutoIncomeSalariedList[i].AccountType == 1) {
                    $('cmbSalaryModeForPr').setValue(res.value.objAutoIncomeSalariedList[i].SalaryMode);
                    $('cmbEmployerForSalariedForPr').setValue(res.value.objAutoIncomeSalariedList[i].Employer);
                    $('txtRemarksForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].Remarks);

                    var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoIncomeSalariedList[i].Employer);
                    if (emp) {
                        $('txtEmployerCodeForPr').setValue(emp.CompanyCode);
                        $('cmbEmployerCategoryForPrSalaried').setValue(emp.CategoryName);
                    }
                    $('txtGrossSalaryForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].GrosSalary);
                    $('txtNetSalaryForPr').setValue(res.value.objAutoIncomeSalariedList[i].NetSalary);
                    $('cmbVeriableSalaryForPr').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalary);
                    if (res.value.objAutoIncomeSalariedList[i].VariableSalary == 1) {
                        $('divVeriableSalaryForPr').style.display = 'block';
                        $('divVariableSalaryCalculationForPr').style.display = 'block';
                        $('txtVeriableSalaryPerForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent);
                        var totalvrAmountForPr = 0;
                        var identity = 0;
                        if (res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist != null) {
                            for (var j = 0; j < res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist.length; j++) {
                                identity = j + 1;
                                $('cmbMonth' + identity + 'ForPrSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Month);
                                $('txtVaiableAmountForMonth' + identity + 'ForPrSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                                totalvrAmountForPr += parseFloat(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                            }
                        }
                        var variableSalaryAmount = parseFloat(parseFloat(totalvrAmountForPr) * parseFloat(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent)) / 100;

                        $('txtTotalVeriableSalaryForPrSalaried').setValue(variableSalaryAmount.toFixed(2));

                    }
                    else {
                        $('divVeriableSalaryForPr').style.display = 'none';
                        $('divVariableSalaryCalculationForPr').style.display = 'none';
                    }
                    $('cmbCarAllowence').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownce);
                    $('txtCarAllowenceAmount').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount);
                    if (res.value.objAutoIncomeSalariedList[i].CarAllownce == 1) {
                        $('divCarAllowenceForPrSalaried').style.display = 'block';
                        $('txtCarAllowenceForPr').setValue(res.value.objAutoIncomeSalariedList[i].CarAllowncePer);
                        var carAllowanceAmntTotal = parseFloat(parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllowncePer) * parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount)) / 100;
                        $('txtCarAllowenceTotalAmountPr').setValue(carAllowanceAmntTotal.toFixed(2));

                    }
                    else {
                        $('divCarAllowenceForPrSalaried').style.display = 'none';
                    }

                    if (res.value.objAutoIncomeSalariedList[i].SalaryMode == 2) {
                        $('changeOwnHouseAmount').setValue(res.value.objAutoIncomeSalariedList[i].OwnHouseBenifit);
                        $('cmbInComeAssementForSalaryForPr').setValue('2');
                        $('cmbInComeAssementForPr').setValue('SL');
                        $('OwnHouseBenifitDivForPrSalaried').style.display = 'block';
                    }
                    else {
                        $('cmbInComeAssementForSalaryForPr').setValue('1');
                        $('cmbInComeAssementForPr').setValue('SC');
                        $('OwnHouseBenifitDivForPrSalaried').style.display = 'none';
                    }
                    salaredHelper.calculateTotalIncome('Pr');
                }
                //Joint Salaried Applicant
                else if (res.value.objAutoIncomeSalariedList[i].AccountType == 2) {
                    $('cmbSalaryModeForJt').setValue(res.value.objAutoIncomeSalariedList[i].SalaryMode);
                    $('cmbEmployerForSalariedForJt').setValue(res.value.objAutoIncomeSalariedList[i].Employer);
                    $('txtRemarksForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].Remarks);
                    var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoIncomeSalariedList[i].Employer);
                    if (emp) {
                        $('txtEmployerCodeForJt').setValue(emp.CompanyCode);
                        $('cmbEmployerCategoryForJtSalaried').setValue(emp.CategoryName);
                    }
                    $('txtGrossSalaryForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].GrosSalary);
                    $('txtNetSalaryForJt').setValue(res.value.objAutoIncomeSalariedList[i].NetSalary);
                    $('cmbVeriableSalaryForJt').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalary);
                    if (res.value.objAutoIncomeSalariedList[i].VariableSalary == 1) {
                        $('divVeriableSalaryForJt').style.display = 'block';
                        $('divVariableSalaryCalculationForJt').style.display = 'block';
                        $('txtVeriableSalaryPerForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent);
                        var totalvrAmountForPr = 0;
                        var identity = 0;
                        for (var j = 0; j < res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist.length; j++) {
                            identity = j + 1;
                            $('cmbMonth' + identity + 'ForJtSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Month);
                            $('txtVaiableAmountForMonth' + identity + 'ForJtSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                            totalvrAmountForPr += parseFloat(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                        }
                        var variableSalaryAmount = parseFloat(parseFloat(totalvrAmountForPr) * parseFloat(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent)) / 100;

                        $('txtTotalVeriableSalaryForJtSalaried').setValue(variableSalaryAmount.toFixed(2));

                    }
                    else {
                        $('divVeriableSalaryForJt').style.display = 'none';
                        $('divVariableSalaryCalculationForJt').style.display = 'none';
                    }
                    $('cmbCarAllowenceJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownce);
                    $('txtCarAllowenceAmountJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount);
                    if (res.value.objAutoIncomeSalariedList[i].CarAllownce == 1) {
                        $('divCarAllowenceForJtSalaried').style.display = 'block';
                        $('txtCarAllowenceForJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllowncePer);
                        var carAllowanceAmntTotal = parseFloat(parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllowncePer) * parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount)) / 100;
                        $('txtCarAllowenceTotalAmountJt').setValue(carAllowanceAmntTotal.toFixed(2));

                    }
                    else {
                        $('divCarAllowenceForJtSalaried').style.display = 'none';
                    }

                    if (res.value.objAutoIncomeSalariedList[i].SalaryMode == 2) {
                        $('changeOwnHouseAmountJt').setValue(res.value.objAutoIncomeSalariedList[i].OwnHouseBenifit);
                        $('cmbInComeAssementForSalaryForJt').setValue('2');
                        $('cmbInComeAssementForJt').setValue('SL');
                        $('OwnHouseBenifitDivForJtSalaried').style.display = 'block';
                    }
                    else {
                        $('cmbInComeAssementForSalaryForJt').setValue('1');
                        $('cmbInComeAssementForJt').setValue('SC');
                        $('OwnHouseBenifitDivForJtSalaried').style.display = 'none';
                    }
                    salaredHelper.calculateTotalIncome('Jt');
                }
            }
        }

        //Edit For Land Lord-----------------------------------------------------------------------------------------------------
        for (var i = 0; i < res.value.objAutoLandLord.length; i++) {
            //Primary Landlord
            if (res.value.objAutoLandLord[i].AccountType == 1) {
                $('txtAreaRemarksForLandlordPr').setValue(res.value.objAutoLandLord[i].Remarks);
                $('txtReflactionPerForLandlordPr').setValue(res.value.objAutoLandLord[i].Percentage);
                $('txtReflactionrequiredForLandLordPr').setValue(res.value.objAutoLandLord[i].ReflectionRequired);
                $('txttotalrentalIncomeForLandLordPr').setValue(res.value.objAutoLandLord[i].TotalRentalIncome);
                var idFieldForLandlord = 0;
                for (var j = 0; j < res.value.objAutoLandLord[i].Property.length; j++) {
                    idFieldForLandlord = j + 1;
                    $('txtVerifiedrent' + idFieldForLandlord + 'ForLandLordPr').setValue(res.value.objAutoLandLord[i].Property[j].Property);
                    $('txtShare' + idFieldForLandlord + 'ForLandLordPr').setValue(res.value.objAutoLandLord[i].Property[j].Share);
                    var rentalIncomeForPrlandLord = parseFloat(parseFloat(res.value.objAutoLandLord[i].Property[j].Property) * parseFloat(res.value.objAutoLandLord[i].Property[j].Share)) / 100;
                    $('txtRentalIncome' + idFieldForLandlord + 'ForLandLordPr').setValue(rentalIncomeForPrlandLord);
                }
                landLordHelper.calculaterentalIncome('txtVerifiedrent1ForLandLordPr', 'Pr');

            }
            //Joint Landlord
            else if (res.value.objAutoLandLord[i].AccountType == 2) {
                $('txtAreaRemarksForLandlordJt').setValue(res.value.objAutoLandLord[i].Remarks);
                $('txtReflactionPerForLandlordJt').setValue(res.value.objAutoLandLord[i].Percentage);
                $('txtReflactionrequiredForLandLordJt').setValue(res.value.objAutoLandLord[i].ReflectionRequired);
                $('txttotalrentalIncomeForLandLordJt').setValue(res.value.objAutoLandLord[i].TotalRentalIncome);
                var idFieldForLandlord = 0;
                for (var j = 0; j < res.value.objAutoLandLord[i].Property.length; j++) {
                    idFieldForLandlord = j + 1;
                    $('txtVerifiedrent' + idFieldForLandlord + 'ForLandLordJt').setValue(res.value.objAutoLandLord[i].Property[j].Property);
                    $('txtShare' + idFieldForLandlord + 'ForLandLordJt').setValue(res.value.objAutoLandLord[i].Property[j].Share);
                    var rentalIncomeForPrlandLord = parseFloat(parseFloat(res.value.objAutoLandLord[i].Property[j].Property) * parseFloat(res.value.objAutoLandLord[i].Property[j].Share)) / 100;
                    $('txtRentalIncome' + idFieldForLandlord + 'ForLandLordJt').setValue(rentalIncomeForPrlandLord);
                }
                landLordHelper.calculaterentalIncome('txtVerifiedrent1ForLandLordJt', 'Jt');
            }
        }

        //Bank Statement Tab for Bussinessman

        for (var i = 0; i < res.value.objAutoAnalystIncomBankStatementSum.length; i++) {
            //Bussiness man Tab
            if (res.value.objAutoAnalystIncomBankStatementSum[i].TabType == 1) {
                //Primary Bank Statement

                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 1) {
                    var identityFieldForBmPr = 0;
                    for (var j = 0; j < Page.prCount; j++) {
                        identityFieldForBmPr = j + 1;
                        var bankStatementId = parseInt($F("txtBankStatementIdBMPr" + identityFieldForBmPr));
                        if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {

                            //Main Object
                            $('tarearemarksForIncomeGeneratorBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                            $('txtTotalCTOFor12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                            $('txtAverageCTO12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                            $('txtAverageBalanceFor12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                            $('txtTotalCTOFor6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                            $('txtAverageCTO6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                            $('txtAverageBalanceFor6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                            $('cmbConsideredCtoForPrBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                            $('cmbConsideredBalanceForPrBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                            $('txtConsiderMarginForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                            $('txtgrossincomeForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                            $('txtNetAfterPaymentForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                            $('txtShareForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                            $('txtNetincomeForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                            $('cmbIncomeAssessmentForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                            $('cmbIndustryForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                            analyzeLoanApplicationManager.changeSector(identityFieldForBmPr, 'Pr');
                            $('cmbSubsectorForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                            analyzeLoanApplicationHelper.changeSubSector(identityFieldForBmPr, 'Pr');
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                                $('divAverageBalancePr' + identityFieldForBmPr).style.display = 'block';
                                $('txt1st9monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                                $('txt1st6MonthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                                $('txtLast6monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                                $('txtLast3monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                                $('txtConsiderednarginForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                            }

                            //Previous Repayment History
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                                $('divPrevRepHis' + identityFieldForBmPr).style.display = 'block';
                                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
                                    $('txttotalForOffSCBForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
                                    $('txtTotal1OnSCBRepHisForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
                                    $('txtTotalEMIClosingConsideredForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
                                    $('txtremarks1ForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                                        for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                                            var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                                            if (parseInt(bankId) < 0) {
                                                var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                                                for (var hs = 1; hs < 5; hs++) {
                                                    var emi = $F("txtEmi" + hs + "ForOnScbForPrimaryBank" + identityFieldForBmPr);
                                                    if (parseFloat(emi) == emidb) {
                                                        $('cmbConsider' + hs + "ForOnSCBReplHisForPrimaryBank" + identityFieldForBmPr).setValue('1');
                                                        break;
                                                    }
                                                }

                                            }
                                            else {
                                                var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                                                for (var hs = 1; hs < 5; hs++) {
                                                    var bankIdtxt = $F("txtBank" + hs + "BMPr" + identityFieldForBmPr);
                                                    if (parseInt(bankIdtxt) == bankId) {
                                                        $('cmbConsider' + hs + "ForPrimaryOffScbBank" + identityFieldForBmPr).setValue('1');
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }



                            }
                            //Over Draft Facility
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
                                for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                                        var tablePrAccount = $("tblOverDraftFacilitiesForPrBMWithScb" + identityFieldForBmPr);
                                        var rowCount = tablePrAccount.rows.length;
                                        for (var t = 1; t <= rowCount - 1; t++) {
                                            var interestovd = parseFloat($F("txtInterst" + t + "WithScbForBank" + identityFieldForBmPr)).toFixed(2);
                                            var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForBank" + identityFieldForBmPr)).toFixed(2);
                                            var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                                            var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                                            if (interestovd == interestDb && LimitAmount == limitdb) {
                                                $('cmbConsider' + t + "ForBank" + identityFieldForBmPr).setValue('1');
                                                break;
                                            }
                                        }
                                    }
                                    else {
                                        var tablePrAccountOffScb = $("tblOverDraftFacilitiesForPrBMOffScb" + identityFieldForBmPr);
                                        var rowCountOffScb = tablePrAccountOffScb.rows.length;
                                        for (var t = 1; t <= rowCountOffScb - 1; t++) {
                                            var bankId = parseFloat($F("cmbBankName" + t + "ForBussinessTabPrBank" + identityFieldForBmPr));
                                            var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                                            if (bankId == bankIdDb) {
                                                $('cmbConsider' + t + "ForOffScbBank" + identityFieldForBmPr).setValue('1');
                                                break;
                                            }
                                        }
                                    }
                                }
                            }



                            analyzeLoanApplicationHelper.changeIncomeAssement(identityFieldForBmPr, 'Pr');

                            break;
                        }
                    }


                }
                //Joint Bank Statement
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 2) {
                    var identityFieldForBmJt = 0;
                    for (var j = 0; j < Page.jtCount; j++) {
                        identityFieldForBmJt = j + 1;
                        var bankStatementId = parseInt($F("txtBankStatementIdBMJt" + identityFieldForBmJt));
                        if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                            //Main Object
                            $('txtRemark1ForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                            $('txtTotalCTOFor12MonthJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                            $('txtAverageCTOFor12MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                            $('txtAverageBalance12MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                            $('txtTotalCTOFor6MonthJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                            $('txtAverageCTOFor6MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                            $('txtAverageBalance6MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                            $('cmbConsideredCtoForJtBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                            $('cmbConsideredBalanceForJtBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                            $('txtConsideredMarginBMForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                            $('txtgrossIncomeForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                            $('txtNetAfterPaymentForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                            $('txtShareForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                            $('txtNetIncomeForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                            $('cmbIncomeAssementForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                            $('cmbIndustryForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                            analyzeLoanApplicationManager.changeSector(identityFieldForBmJt, 'Jt');
                            $('cmbSubSectorForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                            analyzeLoanApplicationHelper.changeSubSector(identityFieldForBmJt, 'Jt');
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                                $('divAverageBalanceJoint' + identityFieldForBmJt).style.display = 'block';
                                $('txtAVGBalanceFor1st9MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                                $('txtAVGBalanceFor1st6MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                                $('txtAVGBalanceForLast6MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                                $('txtAVGBalanceForLast3MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                                $('txtConsideredMarginForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                            }

                            //Previous Repayment History
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                                $('divPrevRepHisJoint' + identityFieldForBmJt).style.display = 'block';
                                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
                                    $('txtTotalOffSCBJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
                                    $('txtTotalOnScbForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
                                    $('txtTotalEMIClosingConsideredPrevRepHisForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
                                    $('txtRemarksPrevRepHisForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                                        for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                                            var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                                            if (parseInt(bankId) < 0) {
                                                var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                                                for (var hs = 1; hs < 5; hs++) {
                                                    var emi = $F("txtEmiOnScb" + hs + "ForJointBank" + identityFieldForBmJt);
                                                    if (parseFloat(emi) == emidb) {
                                                        $('cmbConsiderOnScb' + hs + "ForJointBank" + identityFieldForBmJt).setValue('1');
                                                        break;
                                                    }
                                                }

                                            }
                                            else {
                                                var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                                                for (var hs = 1; hs < 5; hs++) {
                                                    var bankIdtxt = $F("txtBank" + hs + "BMJt" + identityFieldForBmJt);
                                                    if (parseInt(bankIdtxt) == bankId) {
                                                        $('cmbConsiderOffScb' + hs + "ForJointBank" + identityFieldForBmJt).setValue('1');
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            //Over Draft Facility
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
                                for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                                        var tablePrAccount = $("tblOverDraftFacilitiesForJtBMWithScb" + identityFieldForBmJt);
                                        var rowCount = tablePrAccount.rows.length;
                                        for (var t = 1; t <= rowCount - 1; t++) {
                                            var interestovd = parseFloat($F("txtInterest" + t + "ForJointBank" + identityFieldForBmJt)).toFixed(2);
                                            var LimitAmount = parseFloat($F("txtLimit" + t + "ForJointBank" + identityFieldForBmJt)).toFixed(2);
                                            var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                                            var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                                            if (interestovd == interestDb && LimitAmount == limitdb) {
                                                $('cmbInterest' + t + "ForJointBank" + identityFieldForBmJt).setValue('1');
                                                break;
                                            }
                                        }
                                    }
                                    else {
                                        var tablePrAccountOffScb = $("tblOverDraftFacilitiesForJtBMOffScb" + identityFieldForBmJt);
                                        var rowCountOffScb = tablePrAccountOffScb.rows.length;
                                        for (var t = 1; t <= rowCountOffScb - 1; t++) {
                                            var bankId = parseFloat($F("cmbBankName" + t + "ForBussinessTabJtBank" + identityFieldForBmJt));
                                            var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                                            if (bankId == bankIdDb) {
                                                $('cmbConsiderOffScbBM' + t + "ForJointBank" + identityFieldForBmJt).setValue('1');
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            analyzeLoanApplicationHelper.changeIncomeAssement(identityFieldForBmJt, 'Jt');
                            break;
                        }
                    }
                }
            }
            //Self Employed Tab
            if (res.value.objAutoAnalystIncomBankStatementSum[i].TabType == 2) {
                //Primary Bank Statement
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 1) {
                    var identityFieldForSePr = 0;
                    for (var j = 0; j < Page.prCount; j++) {
                        identityFieldForSePr = j + 1;
                        var bankStatementId = parseInt($F("txtBankStatementIdSEPr" + identityFieldForSePr));
                        if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                            //Main Object
                            $('txtremarksForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                            $('txtTotalCTOForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                            $('txtAverageCTOForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                            $('txtAverageBalanceForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                            $('txtTotalCTOForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                            $('txtAverageCTOForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                            $('txtAverageBalanceForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                            $('cmbConsideredCtoForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                            $('cmbConsideredBalanceForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                            $('txtConsiderMarginForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                            $('txtgrossincomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                            $('txtNetAfterPaymentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                            $('txtShareForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                            $('txtNetincomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                            $('cmbIncomeAssessmentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                            $('cmbIndustryForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                            analyzeLoanApplicationManager.changeSectorForSelfEmployed(identityFieldForSePr, 'Pr');
                            $('cmbSubsectorForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                            analyzeLoanApplicationHelper.changeSubSectorForSelfEmployed(identityFieldForSePr, 'Pr');
                            $('cmbPrivateIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome);
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                                $('divAverageBalanceForSEPr' + identityFieldForSePr).style.display = 'block';
                                $('txt1st9monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                                $('txt1st6MonthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                                $('txtLast6monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                                $('txtLast3monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                                $('txtConsiderednarginForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                            }

                            //Previous Repayment History
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                                $('divPrevRepHisForSEPrBank' + identityFieldForSePr).style.display = 'block';
                                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
                                    $('txttotalForOffSCBForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
                                    $('txtTotal1OnSCBRepHisForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
                                    $('txtTotalEMIClosingConsideredForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
                                    $('txtremarks1ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                                        for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                                            var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                                            if (parseInt(bankId) < 0) {
                                                var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                                                for (var hs = 1; hs < 5; hs++) {
                                                    var emi = $F("txtEMI" + hs + "ForOffSCBSEPrBank" + identityFieldForSePr);
                                                    if (parseFloat(emi) == emidb) {
                                                        $('cmbConsider' + hs + "ForOnSCBReplHisForSEPrBank" + identityFieldForSePr).setValue('1');
                                                        break;
                                                    }
                                                }

                                            }
                                            else {
                                                var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                                                for (var hs = 1; hs < 5; hs++) {
                                                    var bankIdtxt = $F("txtBank" + hs + "SEPr" + identityFieldForSePr);
                                                    if (parseInt(bankIdtxt) == bankId) {
                                                        $('cmbConsider' + hs + "ForSEPrOffScbBank" + identityFieldForSePr).setValue('1');
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            //OverDeraft facility
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
                                for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                                        var tablePrAccount = $("tblOverDraftFacilitiesForPrSEWithScb" + identityFieldForSePr);
                                        var rowCount = tablePrAccount.rows.length;
                                        for (var t = 1; t <= rowCount - 1; t++) {
                                            var interestOvd = parseFloat($F("txtInterst" + t + "WithScbForSEBank" + identityFieldForSePr)).toFixed(2);
                                            var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForSEBank" + identityFieldForSePr)).toFixed(2);
                                            var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                                            var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                                            if (interestOvd == interestDb && LimitAmount == limitdb) {
                                                $('cmbConsider' + t + "WithScbForSEBank" + identityFieldForSePr).setValue('1');
                                                break;
                                            }
                                        }
                                    }
                                    else {
                                        var tablePrAccountOffScb = $("tblOverDraftFacilitiesForPrSEOffScb" + identityFieldForSePr);
                                        var rowCountOffScb = tablePrAccountOffScb.rows.length;
                                        for (var t = 1; t <= rowCountOffScb - 1; t++) {
                                            var bankId = parseFloat($F("cmbBankName" + t + "ForSEPrBank" + identityFieldForSePr));
                                            var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                                            if (bankId == bankIdDb) {
                                                $('cmbConsider' + t + "ForSEOffScbBank" + identityFieldForSePr).setValue('1');
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            analyzeLoanApplicationHelper.changeIncomeAssementForSelfEmployed(identityFieldForSePr, 'Pr');
                            //Auto_PracticingDoctor

                            if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 143 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                                $('divChamberIncomeForSePrBank' + identityFieldForSePr).style.display = 'block';
                                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor != null) {
                                    $('txtTotalChamberIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalChamberIncome);
                                    $('txtTotalOtherIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.OtherOTIncome);
                                    $('txtTotalIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalIncome);
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome != null) {
                                        for (var ch = 0; ch < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome.length; ch++) {
                                            var chamberId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].ChamberId;
                                            $('txtPatientsOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldPatents);
                                            $('txtFeeOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldFee);
                                            $('txtTotalOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldTotal);
                                            $('txtPatientsNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewPatents);
                                            $('txtFeeNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewFee);
                                            $('txtTotalNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewTotal);
                                            $('txtTotalOldNew' + chamberId + 'ForSEPr1Bank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Total);
                                            $('txtDayMonth' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].DayMonth);
                                            $('txtIncome' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Income);
                                        }
                                    }
                                }
                                analyzeLoanApplicationHelper.changePrivateInCome(identityFieldForSePr, 'Pr', 'Se');
                            }
                            //Auto_TutioningTeacher

                            if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 150 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                                $('divTutionForSEPrBank' + identityFieldForSePr).style.display = 'block';
                                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome != null) {
                                    $('txtNoOfStudentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfStudent);
                                    $('txtNoOfDaysForTutionForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfDaysForTution);
                                    $('txtTutionFeeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.Fees);
                                    $('txtTotalInComeTutionForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.TotalIncome);
                                }
                                analyzeLoanApplicationHelper.changePrivateInCome(identityFieldForSePr, 'Pr', 'Se');
                            }

                            break;
                        }
                    }
                }
                //Joint Bank Statement
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 2) {
                    var identityFieldForSeJt = 0;
                    for (var j = 0; j < Page.jtCount; j++) {
                        identityFieldForSeJt = j + 1;
                        var bankStatementId = parseInt($F("txtBankStatementIdSEJt" + identityFieldForSeJt));
                        if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                            //Main Object
                            $('txtremarksForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                            $('txtTotalCTOForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                            $('txtAverageCTOForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                            $('txtAverageBalanceForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                            $('txtTotalCTOForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                            $('txtAverageCTOForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                            $('txtAverageBalanceForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                            $('cmbConsideredCtoForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                            $('cmbConsideredBalanceForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                            $('txtConsiderMarginForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                            $('txtgrossincomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                            $('txtNetAfterPaymentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                            $('txtShareForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                            $('txtNetincomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                            $('cmbIncomeAssessmentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                            $('cmbIndustryForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                            analyzeLoanApplicationManager.changeSectorForSelfEmployed(identityFieldForSeJt, 'Jt');
                            $('cmbSubsectorForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                            analyzeLoanApplicationHelper.changeSubSectorForSelfEmployed(identityFieldForSeJt, 'Jt');
                            $('cmbPrivateIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome);
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                                $('divAverageBalanceForSEJt' + identityFieldForSeJt).style.display = 'block';
                                $('txt1st9monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                                $('txt1st6MonthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                                $('txtLast6monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                                $('txtLast3monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                                $('txtConsiderednarginForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                            }

                            //Preveous Repayment History
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                                $('divPrevRepHisForSEJtBank' + identityFieldForSeJt).style.display = 'block';
                                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
                                    $('txttotalForOffSCBForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
                                    $('txtTotal1OnSCBRepHisForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
                                    $('txtTotalEMIClosingConsideredForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
                                    $('txtremarks1ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                                        for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                                            var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                                            if (parseInt(bankId) < 0) {
                                                var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                                                for (var hs = 1; hs < 5; hs++) {
                                                    var emi = $F("txtEmi" + hs + "ForOnScbForSEJtBank" + identityFieldForSeJt);
                                                    if (parseFloat(emi) == emidb) {
                                                        $('cmbConsider' + hs + "ForOnSCBReplHisForSEJtBank" + identityFieldForSeJt).setValue('1');
                                                        break;
                                                    }
                                                }

                                            }
                                            else {
                                                var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                                                for (var hs = 1; hs < 5; hs++) {
                                                    var bankIdtxt = $F("txtBank" + hs + "SEJt" + identityFieldForSeJt);
                                                    if (parseInt(bankIdtxt) == bankId) {
                                                        $('cmbConsider' + hs + "ForSEJtRepHisOffScbBank" + identityFieldForSeJt).setValue('1');
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            //Over Draft Facility
                            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
                                for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                                        var tablePrAccount = $("tblOverDraftFacilitiesForJtSEWithScb" + identityFieldForSeJt);
                                        var rowCount = tablePrAccount.rows.length;
                                        for (var t = 1; t <= rowCount - 1; t++) {
                                            var interestovd = parseFloat($F("txtInterst" + t + "WithScbForSEJtBank" + identityFieldForSeJt)).toFixed(2);
                                            var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForSEJtBank" + identityFieldForSeJt)).toFixed(2);
                                            var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                                            var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                                            if (interestovd == interestDb && LimitAmount == limitdb) {
                                                $('cmbConsider' + t + "WithScbForSEJtBank" + identityFieldForSeJt).setValue('1');
                                                break;
                                            }
                                        }
                                    }
                                    else {
                                        var tablePrAccountOffScb = $("tblOverDraftFacilitiesForJtSEOffScbBank" + identityFieldForSeJt);
                                        var rowCountOffScb = tablePrAccountOffScb.rows.length;
                                        for (var t = 1; t <= rowCountOffScb - t; i++) {
                                            var bankId = parseFloat($F("cmbBankName" + t + "ForSEJtBank" + identityFieldForSeJt));
                                            var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                                            if (bankId == bankIdDb) {
                                                $('cmbConsider' + t + "ForSEJtOffScbBank" + identityFieldForSeJt).setValue('1');
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            analyzeLoanApplicationHelper.changeIncomeAssementForSelfEmployed(identityFieldForSeJt, 'Jt');
                            //Auto_PracticingDoctor

                            if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 143 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                                $('divChamberIncomeForSeJtBank' + identityFieldForSeJt).style.display = 'block';
                                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor != null) {
                                    $('txtTotalChamberIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalChamberIncome);
                                    $('txtTotalOtherIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.OtherOTIncome);
                                    $('txtTotalIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalIncome);
                                    if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome != null) {
                                        for (var ch = 0; ch < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome.length; ch++) {
                                            var chamberId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].ChamberId;
                                            $('txtPatientsOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldPatents);
                                            $('txtFeeOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldFee);
                                            $('txtTotalOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldTotal);
                                            $('txtPatientsNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewPatents);
                                            $('txtFeeNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewFee);
                                            $('txtTotalNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewTotal);
                                            $('txtTotalOldNew' + chamberId + 'ForSEJt1Bank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Total);
                                            $('txtDayMonth' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].DayMonth);
                                            $('txtIncome' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Income);
                                        }
                                    }
                                }
                                analyzeLoanApplicationHelper.changePrivateInCome(identityFieldForSeJt, 'Jt', 'Se');
                            }
                            //Auto_TutioningTeacher

                            if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 150 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                                $('divTutionJtBank' + identityFieldForSeJt).style.display = 'block';
                                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome != null) {
                                    $('txtNoOfStudentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfStudent);
                                    $('txtNoOfDaysForTutionForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfDaysForTution);
                                    $('txtTutionFeeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.Fees);
                                    $('txtTotalInComeTutionForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.TotalIncome);
                                }
                                analyzeLoanApplicationHelper.changePrivateInCome(identityFieldForSeJt, 'Jt', 'Se');
                            }
                            break;
                        }
                    }
                }
            }
        }



        //Start Loan Calculator Tab------------------------------------------------------------------------------------------------

        //Auto An Income Segment Applicant Income List---------------------------------------------------------------------------
        for (var i = 0; i < res.value.Auto_An_IncomeSegmentApplicantIncomeList.length; i++) {

            if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].ApplicantType == 1) {
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource > 0) {
                    var identityFieldForLcPr = 0;
                    for (var j = 0; j < Page.prCount; j++) {
                        identityFieldForLcPr = j + 1;
                        var bankId = parseInt($F("txtbankIdForLcPrBank" + identityFieldForLcPr));
                        if (bankId == res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource) {
                            $('cmbConsiderForLCPrBank' + identityFieldForLcPr).setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                            break;
                        }
                    }
                }

                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -7) {
                    $('cmbConsiderForLCPrSalary').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -8) {
                    $('cmbConsiderForLCPrRent').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -9) {
                    $('cmbConsiderForLCPrDoctor').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -10) {
                    $('cmbConsiderForLCPrTeacher').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                loanCalculatorHelPer.CalculateLoanCalculator('Pr');

            }
            else if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].ApplicantType == 2) {
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource > 0) {
                    var identityFieldForLcJt = 0;
                    for (var j = 0; j < Page.jtCount; j++) {
                        identityFieldForLcJt = j + 1;
                        var bankId = parseInt($F("txtbankIdForLcJtBank" + identityFieldForLcJt));
                        if (bankId == res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource) {
                            $('cmbConsiderForLCJtBank' + identityFieldForLcJt).setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                            break;
                        }
                    }
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -7) {
                    $('cmbConsiderForLCJtSalary').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -8) {
                    $('cmbConsiderForLCJtRent').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -9) {
                    $('cmbConsiderForLCJtDoctor').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -10) {
                    $('cmbConsiderForLCJtTeacher').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                loanCalculatorHelPer.CalculateLoanCalculator('Jt');

            }
        }
        //End Auto An Income Segment Applicant Income List----------------------------------------------------------------------

        //Auto_An_IncomeSegmentIncome Start

        if (res.value.objAuto_An_IncomeSegmentIncome != null) {
            $('cmbEmployeeSegmentLc').setValue(res.value.objAuto_An_IncomeSegmentIncome.EmployeeSegment);
            $('txtSegmentCodeLc').setValue(res.value.objAuto_An_IncomeSegmentIncome.EmployeeSegmentCode);
            if (res.value.AutoLoanAnalystApplication.CashSecured100Per == 1) {
                $('cmbAssessmentMethodForLcCash').setValue(res.value.objAuto_An_IncomeSegmentIncome.AssesmentMethod);
                $('cmbCategoryForLcCash').setValue(res.value.objAuto_An_IncomeSegmentIncome.Category);
                loanCalculatorHelPer.changeIncomeAssementMethodForCashSequre();
                loanCalculatorHelPer.changeCategoryForCashSequre();
            }

        }

        //End Auto_An_IncomeSegmentIncome
        //objAuto_An_RepaymentCapability Start--------------------------------------------------------------------------------

        if (res.value.objAuto_An_RepaymentCapability != null) {
            $('cmbDBRLevel').setValue(res.value.objAuto_An_RepaymentCapability.DBRLevel);
            var label = parseInt($F("cmbLabelForDlaLimit")); //$('cmbLabelForDlaLimit').setValue('1');
            if (res.value.objAuto_An_RepaymentCapability.DBRLevel > label) {
                $('cmbLabelForDlaLimit').setValue(res.value.objAuto_An_RepaymentCapability.DBRLevel);
            }
            $('txtAppropriateDbr').setValue(res.value.objAuto_An_RepaymentCapability.AppropriateDBR);
            $('txtConsideredDbr').setValue(res.value.objAuto_An_RepaymentCapability.ConsideredDBR);
            $('txtMaximumEmiCapability').setValue(res.value.objAuto_An_RepaymentCapability.MaxEMICapability);
            $('txtExistingEmi').setValue(res.value.objAuto_An_RepaymentCapability.ExistingEMI);
            $('txtCurrentEMICapability').setValue(res.value.objAuto_An_RepaymentCapability.CurrentEMICapability);
            if (res.value.objAuto_An_RepaymentCapability.BalanceSupported == 0) {
                $('txtBalanceSupported').setValue('NOT APPLICABLE');
            }
            else {
                $('txtBalanceSupported').setValue(res.value.objAuto_An_RepaymentCapability.BalanceSupported);
            }
            $('txtMaximumEmi').setValue(res.value.objAuto_An_RepaymentCapability.MaxEMI);
        }

        //objAuto_An_RepaymentCapability End ---------------------------------------------------------------------------------
        //objAuto_An_LoanCalculationTenor Start-------------------------------------------------------------------------------
        if (res.value.objAuto_An_LoanCalculationTenor != null) {
            $('txtArtaAllowedFoLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ARTAAllowed);
            $('txtvehicleAllowedForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.VehicleAllowed);
            $('cmbLabelForTonerLc').setValue(res.value.objAuto_An_LoanCalculationTenor.Level);
            var label = parseInt($F("cmbLabelForDlaLimit")); //$('cmbLabelForDlaLimit').setValue('1');
            if (res.value.objAuto_An_LoanCalculationTenor.Level > label) {
                $('cmbLabelForDlaLimit').setValue(res.value.objAuto_An_LoanCalculationTenor.Level);
            }
            $('txtAgeAllowedForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AgeAllowed);
            $('txtAskingTenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AskingTenor);
            $('txtAppropriteTenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AppropriateTenor);
            $('txtCosideredtenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ConsideredTenor);
            $('txtConsideredMarginForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ConsideredInMonth);

        }
        //objAuto_An_LoanCalculationTenor End------------------------------------------------------------------------------------
        //objAuto_An_LoanCalculationInterest Start-------------------------------------------------------------------------------
        if (res.value.objAuto_An_LoanCalculationInterest != null) {
            $('txtInterestLCVendoreAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.VendorAllowed);
            $('txtInterestLCArtaAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.ARTAAllowed);
            $('txtInterestLCSegmentAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.SegmentAllowed);
            $('txtInterestLCAskingRate').setValue(res.value.objAuto_An_LoanCalculationInterest.AskingRate);
            $('txtAppropriteRate').setValue(res.value.objAuto_An_LoanCalculationInterest.AppropriateRate);
            $('txtInterestLCCosideredRate').setValue(res.value.objAuto_An_LoanCalculationInterest.ConsideredRate);

        }
        //objAuto_An_LoanCalculationInterest End---------------------------------------------------------------------------------

        //objAuto_An_LoanCalculationAmount Start-------------------------------------------------------------------------------
        if (res.value.objAuto_An_LoanCalculationAmount != null) {
            $('txtLtvAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.LTVAllowed);
            $('txtIncoomeAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.IncomeAllowed);
            $('txtbdCadAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.BB_CADAllowed);
            $('txtAskingLoanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.AskingLoan);
            $('txtExistingFacilitiesAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.ExistingFacilityAllowed);
            $('txtAppropriteloanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.AppropriateLoan);
            $('txtApprovedLoanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.ApprovedLoan);

        }
        //objAuto_An_LoanCalculationAmount End---------------------------------------------------------------------------------
        //objAuto_An_LoanCalculationInsurance Start-------------------------------------------------------------------------------
        if (res.value.objAuto_An_LoanCalculationInsurance != null) {
            $('txtgeneralInsurenceForLcIns').setValue(res.value.objAuto_An_LoanCalculationInsurance.GeneralInsurance);
            $('txtArtaForLCInsurenceAmount').setValue(res.value.objAuto_An_LoanCalculationInsurance.ARTA);
            $('txtTotalInsurenceForLcInsurence').setValue(res.value.objAuto_An_LoanCalculationInsurance.TotalInsurance);

        }
        //objAuto_An_LoanCalculationInsurance End---------------------------------------------------------------------------------
        //objAuto_An_LoanCalculationTotal Start-------------------------------------------------------------------------------
        if (res.value.objAuto_An_LoanCalculationTotal != null) {
            $('txtloanAmountForLcLoanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.TotalLoanAmount);
            $('txtLtvExclusdingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.LTVExcludingInsurance);
            $('txtDBRExcludingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.FBRExcludingInsurance);
            $('txtltvIncludingLtvForLcLoanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.LTVIncludingInsurance);
            $('txtDBRIncludingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.FBRIncludingInsurance);
            $('txtEmrForlcloanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.EMR);
            $('txtExtraLtvForLcloanSumary').setValue(res.value.objAuto_An_LoanCalculationTotal.ExtraLTV);
            $('txtExtraDBRForLcloanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.ExtraDBR);

        }


        //End loan Calculator Tab----------------------------------------------

        analystfinalizationHelper.calculateRepaymentForFinalize();
        //objAuto_An_LoanCalculationTotal End---------------------------------------------------------------------------------
        //Start Finalization Tab-----------------------------------------------
        //Auto_An_FinalizationDeviationList Start-------------------------------------------
        for (var i = 0; i < res.value.Auto_An_FinalizationDeviationList.length; i++) {
            var identityFieldFordev = i + 1;
            $('cmdDeviationLevel' + identityFieldFordev).setValue(res.value.Auto_An_FinalizationDeviationList[i].Level);
            FinalizationTabManager.getDeviationDescriptionByLevel('cmdDeviationLevel' + identityFieldFordev);
            $('cmdDeviationDescription' + identityFieldFordev).setValue(res.value.Auto_An_FinalizationDeviationList[i].Description);
            $('txtDeviationCode' + identityFieldFordev).setValue(res.value.Auto_An_FinalizationDeviationList[i].Code);
        }
        //Auto_An_FinalizationDeviationList End-------------------------------------------
        //objAuto_An_FinalizationDevFound Start-------------------------------------------------------------------------------
        if (res.value.objAuto_An_FinalizationDevFound != null) {
            $('txtDeviationAgeForfinalizeTab').setValue(res.value.objAuto_An_FinalizationDevFound.Age);
            $('txtDeviationDbrForfinalizeTab').setValue(res.value.objAuto_An_FinalizationDevFound.DBR);
            $('txtSeacitingCapacityDeviationForfinalizeTab').setValue(res.value.objAuto_An_FinalizationDevFound.SeatingCapacilty);
            $('txtSharePortionForFinalizeTab').setValue(res.value.objAuto_An_FinalizationDevFound.SharePortion);
        }
        analystfinalizationHelper.calculateRepaymentForFinalize();
        //objAuto_An_LoanCalculationTotal End---------------------------------------------------------------------------------

        //Auto_An_FinalizationSterengthList Start-------------------------------------------
        for (var i = 0; i < res.value.Auto_An_FinalizationSterengthList.length; i++) {
            var identityFieldFordev = i + 1;
            var textToFind = res.value.Auto_An_FinalizationSterengthList[i].Strength;
            var dd = document.getElementById('cmbStrength' + identityFieldFordev);
            for (var j = 0; j < dd.options.length; j++) {
                if (dd.options[j].text === textToFind) {
                    dd.selectedIndex = j;
                    break;
                }
            }

        }
        //Auto_An_FinalizationSterengthList End-------------------------------------------
        //Auto_An_FinalizationWeaknessList Start-------------------------------------------
        for (var i = 0; i < res.value.Auto_An_FinalizationWeaknessList.length; i++) {
            var identityFieldForWk = i + 1;
            var textToFind = res.value.Auto_An_FinalizationWeaknessList[i].Weakness;
            var dd = document.getElementById('cmbWeakness' + identityFieldForWk);
            for (var j = 0; j < dd.options.length; j++) {
                if (dd.options[j].text === textToFind) {
                    dd.selectedIndex = j;
                    break;
                }
            }

        }
        //Auto_An_FinalizationWeaknessList End-------------------------------------------
        //Auto_An_FinalizationConditionList Start-------------------------------------------
        for (var i = 0; i < res.value.Auto_An_FinalizationConditionList.length; i++) {
            var identityFieldForWk = i + 1;
            var textToFind = res.value.Auto_An_FinalizationConditionList[i].Condition;
            var dd = document.getElementById('cmbCondition' + identityFieldForWk);
            for (var j = 0; j < dd.options.length; j++) {
                if (dd.options[j].text === textToFind) {
                    dd.selectedIndex = j;
                    break;
                }
            }

        }
        //Auto_An_FinalizationConditionList End-------------------------------------------
        //Auto_An_FinalizationRemarkList Start-------------------------------------------
        var remarkOtherCount = 4;
        for (var i = 0; i < res.value.Auto_An_FinalizationRemarkList.length; i++) {
            var identityFieldForRk = i + 1;
            var remarkAdded = false;
            var textToFind = res.value.Auto_An_FinalizationRemarkList[i].Remarks;
            if (i < 3) {
                var dd = document.getElementById('cmbRemark' + identityFieldForRk);
                for (var j = 0; j < dd.options.length; j++) {
                    if (dd.options[j].text === textToFind) {
                        dd.selectedIndex = j;
                        remarkAdded = true;
                        break;
                    }
                }
            }
            if (remarkAdded == false) {
                $('txtRemark' + remarkOtherCount).setValue(res.value.Auto_An_FinalizationRemarkList[i].Remarks);
                remarkOtherCount += 1;
            }

        }
        //Auto_An_FinalizationRemarkList End-------------------------------------------
        //objAuto_An_FinalizationRepaymentMethod
        if (res.value.objAuto_An_FinalizationRepaymentMethod != null) {
            $('cmbAskingRepaymentmethodForFinalize').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.Asking);
            $('txtInstrumentForFinalizetab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.Instrument);
            $('cmbAccountNumberForSCBForFinalizetab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.SCBAccount);
            $('cmbSequrityPdcForFinalizetab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.SecurityPDC);
            $('txtNoOfPdcForFinalizeTab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.NoOfPDC);
            $('txtPdcAmountForFinalizeTab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.PDCAmount);
            $('cmbBankNameForFinalizeTabofrepaymentMethod').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.BankID);
            $('txtAccountnumberForaBankFinalizeTab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.AccountNumber);

        }

        //objAuto_An_FinalizationRepaymentMethod
        if (res.value.objAuto_An_FinalizationReqDoc != null) {
            $('cmbLetterOfIntroduction').setValue(res.value.objAuto_An_FinalizationReqDoc.LetterOfIntroduction);
            $('cmbPayslip').setValue(res.value.objAuto_An_FinalizationReqDoc.Payslips);
            $('cmbtradelicence').setValue(res.value.objAuto_An_FinalizationReqDoc.TradeLicence);
            $('cmbMoaAoa').setValue(res.value.objAuto_An_FinalizationReqDoc.MOA_AOA);
            $('cmbPartnershipDeed').setValue(res.value.objAuto_An_FinalizationReqDoc.PartnershipDeed);
            $('cmbFrom').setValue(res.value.objAuto_An_FinalizationReqDoc.FormX);
            $('cmbBoardresolution').setValue(res.value.objAuto_An_FinalizationReqDoc.BoardResolution);
            $('cmbDiplomacertificate').setValue(res.value.objAuto_An_FinalizationReqDoc.DiplomaCertificate);
            $('cmbProfessinoalCertificate').setValue(res.value.objAuto_An_FinalizationReqDoc.ProfessionalCertificate);
            $('cmbunicipalTax').setValue(res.value.objAuto_An_FinalizationReqDoc.MunicipalTaxRecipt);
            $('cmbTitleDeed').setValue(res.value.objAuto_An_FinalizationReqDoc.TitleDeed);
            $('cmbentalDeed').setValue(res.value.objAuto_An_FinalizationReqDoc.RentalDeed);
            $('cmbtilityBills').setValue(res.value.objAuto_An_FinalizationReqDoc.UtilityBill);
            $('cmbLoanOfferLetter').setValue(res.value.objAuto_An_FinalizationReqDoc.LoanOfferLatter);

        }

        //objAuto_An_FinalizationVarificationReport
        if (res.value.objAuto_An_FinalizationVarificationReport != null) {
            $('cmbCpvreport').setValue(res.value.objAuto_An_FinalizationVarificationReport.CPVReport);
            $('cmbCarPriceVerificationReport').setValue(res.value.objAuto_An_FinalizationVarificationReport.CarPriceVarificationReport);
            $('cmbStatementAutheticationreport').setValue(res.value.objAuto_An_FinalizationVarificationReport.StatementAuthentication);
            $('cmbcreditReport').setValue(res.value.objAuto_An_FinalizationVarificationReport.CreditReport);
            $('cmbCarVerificationreport').setValue(res.value.objAuto_An_FinalizationVarificationReport.CarVerificationReport);
            $('cmbUsedCarVerificationreport').setValue(res.value.objAuto_An_FinalizationVarificationReport.UsedCarValuationReport);
            $('cmbUsedcarDocs').setValue(res.value.objAuto_An_FinalizationVarificationReport.UsedCarDocs);
            $('cmbCardrepaymentHistory').setValue(res.value.objAuto_An_FinalizationVarificationReport.CardRepaymentHistory);
        }
        //objAuto_An_FinalizationAppraiserAndApprover
        if (res.value.objAuto_An_FinalizationAppraiserAndApprover != null) {
            //            $('cmbAppraisedby').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.AppraisedByUserID);
            //            $('txtAppraiserCode').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.AppraisedByUserCode);
            //            $('cmbSupportedby').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.SupportedByUserID);
            //            $('txtSupportedbyCode').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.SupportedByUserCode);
            $('cmbApprovedby').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.ApprovedByUserID);
            $('txtApprovedbyCode').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.ApprovedByUserCode);
            $('txtAutoDLALimit').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.AutoDLALimit);
            $('txtTotalSecuredlimit').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.TotalSecuredLimit);
        }

        //objAuto_An_AlertVerificationReport
        labelChangeHelper.Auto_An_AlertVerificationReport(res.value.objAuto_An_AlertVerificationReport);
        //objAuto_An_FinalizationRework
        if (res.value.objAuto_An_FinalizationRework != null) {
            $('cmbReworkDone').setValue(res.value.objAuto_An_FinalizationRework.ReworkDone);
            $('txtreworkReason').setValue(res.value.objAuto_An_FinalizationRework.ReworkReason);
            $('txtLastApprovalDate').setValue(res.value.objAuto_An_FinalizationRework.LastApprovalDate);
            $('txtReworkDate').setValue(res.value.objAuto_An_FinalizationRework.ReworkDate);
            $('cmbReworkCount').setValue(res.value.objAuto_An_FinalizationRework.ReworkCount);
            $('txtLastApprovalAmount').setValue(res.value.objAuto_An_FinalizationRework.LastApprovalAmount);
        }


        //
        for (var i = 0; i < res.value.objAuto_An_CibRequestLoan.length; i++) {
            var identityFieldForCib = i + 1;
            $('cmbFacilityForCibRequest' + identityFieldForCib).setValue(res.value.objAuto_An_CibRequestLoan[i].CibFacilityId);
            $('txtLoanAmountCibrequest' + identityFieldForCib).setValue(res.value.objAuto_An_CibRequestLoan[i].CibAmount);
            $('txtTenorForCibRequest' + identityFieldForCib).setValue(res.value.objAuto_An_CibRequestLoan[i].CibTenor);
        }

        //End Finalization Tab------------------------------------------------


    },
    getVendor: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AnalyzeLoanApplication.GetVendor();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.vendoreArray = res.value;

        document.getElementById('cmbVendor').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vendor';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVendor').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].VendorName;
            opt.value = res.value[i].AutoVendorId;
            document.getElementById('cmbVendor').options.add(opt);
        }

    },
    CheckMouByVendoreId: function() {
        var vendoreId = $F("cmbVendor");
        var res = PlussAuto.AnalyzeLoanApplication.CheckMouByVendoreId(vendoreId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value.VendoreRelationshipStatus != 1 && res.value.VendoreRelationshipStatus != 0) {
            alert("The Vendore is Negative Listed");
            $('cmbVendoreRelationShip').setValue('2');
        }
        else {
            $('cmbVendoreRelationShip').setValue(res.value.VendoreRelationshipStatus);
        }
        loanCalculatorHelPer.calculateTenorSettings();
    }


};

var analyzeLoanApplicationHelper = {
    populateLoanSummeryGrid: function(rss) {
        var html = [];
        for (var i = 0; i < rss.value.length; i++) {
            // var serialNo = i + 1;
            //            <a title="Appeal" href="javascript:;" onclick="loanSummeryHelper.replayPopupShow(#{Auto_LoanMasterId})" class="icon iconReplay"></a>
            var btnWillBe = "";
            if (Page.LoanSummeryArray[i].StatusID == 17 || Page.LoanSummeryArray[i].StatusID == 19 || Page.LoanSummeryArray[i].StatusID == 21 || Page.LoanSummeryArray[i].StatusID == 24) {
                btnWillBe = '<input title="Process" type="button" id="btnAppeal" class="searchButton   widthSize100_per" value="Process" onclick=" analystSaveManager.inprocessOfllId(' + Page.LoanSummeryArray[i].Auto_LoanMasterId + ')"/>';
            }
            else if (Page.LoanSummeryArray[i].StatusID == 17 || Page.LoanSummeryArray[i].StatusID == 18 || Page.LoanSummeryArray[i].StatusID == 20 || Page.LoanSummeryArray[i].StatusID == 22 || Page.LoanSummeryArray[i].StatusID == 23 || Page.LoanSummeryArray[i].StatusID == 25 || Page.LoanSummeryArray[i].StatusID == 26 || Page.LoanSummeryArray[i].StatusID == 28 || Page.LoanSummeryArray[i].StatusID == 33 || Page.LoanSummeryArray[i].StatusID == 34 || Page.LoanSummeryArray[i].StatusID == 35) {
                btnWillBe = '<input title="Open" type="button" id="btnPrintBA" class="searchButton   widthSize100_per" value="Open" onclick=" analystSaveManager.SetLLIDAndSearch(' + Page.LoanSummeryArray[i].LLID + ')"/>';
            }
            var className = i % 2 ? '' : 'odd';
            Page.LoanSummeryArray[i].btnWillBe = btnWillBe;
            html.push('<tr class="' + className + '"><td>#{LLID}</td><td>#{PrName}</td><td>#{ProductName}</td><td>#{ManufacturerName}</td><td>#{Model}</td><td>#{AppliedAmount}</td><td>#{AskingTenor}</td><td>#{StatusName}</td><td>#{btnWillBe}</td></tr>'
                        .format2((i % 2 ? '' : 'odd'), true)
                        .format(Page.LoanSummeryArray[i])
                );
        }
        $('GridLoanSummery').update(html.join(''));
        var totalCount = 0;

        if (rss.value.length != 0) {
            totalCount = Page.LoanSummeryArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);

    },
    showTabbyType: function(prPrimaryProfession, prOtherProfession) {

        $('mainTabForAnalyse').style.display = 'block';

        //Income Generator Hide All Tab
        $('liTabBussiness').style.display = 'none';
        $('liTabSalaried').style.display = 'none';
        $('liTabLandlord').style.display = 'none';
        $('liTabSelfEmployed').style.display = 'none';

        analyzeLoanApplicationHelper.showIncomeGeneratortab(prPrimaryProfession, 'block');
        analyzeLoanApplicationHelper.showIncomeGeneratortab(prOtherProfession, 'block');
    },
    showJointApplicant: function() {
        var reasonofJointApplicant = $F("cmbReasonofJointApplicant");
        var jtPrimaryProfession = parseInt($F("txtJtProfessionId"));
        var prPrimaryProfession = parseInt($F("txtPrProfessionId"));
        var prOtherProfession = parseInt($F("txtPrOtherProfessionId"));
        if (reasonofJointApplicant == "1") {
            switch (jtPrimaryProfession) {
                case 1:
                    $('divjtApplicantIncomeGeneratorBussinessMan').style.display = 'inline-block';
                    if (prPrimaryProfession != 1 && prOtherProfession != 1) {
                        $('divPApplicantIncomeGenerator').style.display = 'none';
                    }

                    break;
                case 2:
                    $('divjtApplicantIncomeGeneratorSalaried').style.display = 'inline-block';
                    if (prPrimaryProfession != 2 && prOtherProfession != 2) {
                        $('divPrApplicantIncomeGeneratorSalaried').style.display = 'none';
                    }
                    break;
                case 3:

                    $('divjtApplicantSelf-Employed').style.display = 'inline-block';
                    if ((prPrimaryProfession != 4 && prOtherProfession != 4) && (prPrimaryProfession != 3 && prOtherProfession != 3) && (prPrimaryProfession != 6 && prOtherProfession != 6)) {
                        $('divPApplicantSelf-Employed').style.display = 'none';
                    }
                    break;
                case 4:
                    $('divjtApplicantSelf-Employed').style.display = 'inline-block';
                    if ((prPrimaryProfession != 4 && prOtherProfession != 4) && (prPrimaryProfession != 3 && prOtherProfession != 3) && (prPrimaryProfession != 6 && prOtherProfession != 6)) {
                        $('divPApplicantSelf-Employed').style.display = 'none';
                    }
                    break;
                case 5:
                    $('divjtApplicantIncomeGeneratorLandlord').style.display = 'inline-block';
                    if (prPrimaryProfession != 5 && prOtherProfession != 5) {
                        $('divPrApplicantIncomeGeneratorLandlord').style.display = 'none';
                    }
                    break;
                case 6:
                    $('divjtApplicantSelf-Employed').style.display = 'inline-block';
                    if ((prPrimaryProfession != 6 && prOtherProfession != 6) && (prPrimaryProfession != 3 && prOtherProfession != 3) && (prPrimaryProfession != 4 && prOtherProfession != 4)) {
                        $('divPApplicantSelf-Employed').style.display = 'none';
                    }
                    break;
            }
            $('divLoanCalculatorForJointApplicant').style.display = 'block';
            analyzeLoanApplicationHelper.showIncomeGeneratortabJoint(jtPrimaryProfession, 'block');

        }
        else {
            $('divjtApplicantIncomeGeneratorBussinessMan').style.display = 'none';
            $('divjtApplicantIncomeGeneratorSalaried').style.display = 'none';
            $('divjtApplicantIncomeGeneratorLandlord').style.display = 'none';
            $('divjtApplicantSelf-Employed').style.display = 'none';
            $('divLoanCalculatorForJointApplicant').style.display = 'none';
            analyzeLoanApplicationHelper.showIncomeGeneratortabJoint(jtPrimaryProfession, 'none');

        }
    },
    showIncomeGeneratortab: function(profession, displaytype) {

        switch (profession) {
            case 1:
                $('liTabBussiness').style.display = displaytype;

                break;
            case 2:
                $('liTabSalaried').style.display = displaytype;
                if (displaytype == "block") {
                    $('trSalariedForLoanCalculatorPr').style.display = "";
                }
                else {
                    $('trSalariedForLoanCalculatorPr').style.display = "none";
                }
                break;
            case 3:
                $('liTabSelfEmployed').style.display = displaytype;

                break;
            case 4:
                $('liTabSelfEmployed').style.display = displaytype;
                break;
            case 5:
                $('liTabLandlord').style.display = displaytype;
                if (displaytype == "block") {
                    $('trLandlordForLoanCalculatorPr').style.display = "";
                }
                else {
                    $('trLandlordForLoanCalculatorPr').style.display = "none";
                }
                break;
            case 6:
                $('liTabSelfEmployed').style.display = displaytype;
                break;
        }
    },
    showIncomeGeneratortabJoint: function(profession, displaytype) {

        switch (profession) {
            case 1:
                $('liTabBussiness').style.display = displaytype;
                for (var j = 1; j <= Page.jtCount; j++) {
                    if (displaytype == "block") {
                        $('trBank' + j + 'ForLoanCalculatorJt').style.display = '';
                    }
                    else {
                        $('trBank' + j + 'ForLoanCalculatorJt').style.display = 'none';
                    }
                }
                break;
            case 2:
                $('liTabSalaried').style.display = displaytype;
                if (displaytype == "block") {
                    $('trSalariedForLoanCalculatorJt').style.display = "";
                }
                else {
                    $('trSalariedForLoanCalculatorJt').style.display = "none";
                }
                break;
            case 3:
                $('liTabSelfEmployed').style.display = displaytype;
                for (var j = 1; j <= Page.jtCount; j++) {
                    if (displaytype == "block") {
                        $('trBank' + j + 'ForLoanCalculatorJt').style.display = '';
                    }
                    else {
                        $('trBank' + j + 'ForLoanCalculatorJt').style.display = 'none';
                    }
                }
                break;
            case 4:
                $('liTabSelfEmployed').style.display = displaytype;
                break;
            case 5:
                $('liTabLandlord').style.display = displaytype;
                if (displaytype == "block") {
                    $('trLandlordForLoanCalculatorJt').style.display = "";
                }
                else {
                    $('trLandlordForLoanCalculatorJt').style.display = "none";
                }
                break;
            case 6:
                $('liTabSelfEmployed').style.display = displaytype;
                break;
        }
    },
    shobankStatementTabForBussinessMan: function(prCount, jtCount) {
        for (var i = 1; i <= 6 - prCount; i++) {
            var identityRow = prCount + i;
            $('prBankStatementIncomeGenerator' + identityRow).style.display = 'none';
            $('trBank' + identityRow + 'ForLoanCalculatorPr').style.display = 'none';
        }
        for (var j = 1; j <= 6 - jtCount; j++) {
            var identityRowForjt = jtCount + j;
            $('jtBankStatementIncomeGenerator' + identityRowForjt).style.display = 'none';
            $('trBank' + identityRow + 'ForLoanCalculatorJt').style.display = 'none';
        }
    },

    changeCashSecured: function() {
        var cashSecured = $F("cmbCashSecured");
        if (cashSecured == "1") {
            $("lblincomeGeneratorAlertMasg").innerHTML = "Income Assessment Not Required";
            $('txtAssessmentMethodLc').setValue('Income Assessment Not Required');
            $('txtAssessmentCodeLc').setValue('FF');
            $('txtBalanceSupported').setValue('NOT APPLICABLE');
            $('divSecurityValue').style.display = "block";
            $('divSequrityvaluetextbox').style.display = "block";
            $('divCashForLc').style.display = "block";

            for (var i = 1; i <= Page.prCount; i++) {
                emiClosing = $F("txtTotalEMIClosingConsideredForPrimaryBank" + i);
                if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                    totalEmiClosing += parseFloat(emiClosing);
                }
            }
            if (totalEmiClosing != 0) {

                var lccalculate = Page.incomeArrayforlc;
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (loanCalculatorHelPer.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtBalanceSupported').setValue('NOT APPLICABLE');
                        }
                    }
                }
            }
            else {
                var lccalculate = Page.incomeArrayforlc;
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (loanCalculatorHelPer.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            if (lccalculate[j].AssessmentCode == "FT" || lccalculate[j].AssessmentCode == "FC") {
                                $('txtBalanceSupported').setValue(lccalculate[j].AverageBalance);
                            }
                        }
                    }


                }

            }

        }
        else {
            $('divSecurityValue').style.display = "none";
            $('divSequrityvaluetextbox').style.display = "none";
            $('divCashForLc').style.display = "none";
            var totalEmiClosing = 0;
            var emiClosing = 0;
            $("lblincomeGeneratorAlertMasg").innerHTML = "";
            for (var i = 1; i <= Page.prCount; i++) {
                emiClosing = $F("txtTotalEMIClosingConsideredForPrimaryBank" + i);
                if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                    totalEmiClosing += parseFloat(emiClosing);
                }
            }
            if (totalEmiClosing != 0) {
                $('txtAssessmentMethodLc').setValue('PREVIOUS REPAYMENT HISTORY');
                $('txtAssessmentCodeLc').setValue('FF');
                var lccalculate = Page.incomeArrayforlc;
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (loanCalculatorHelPer.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtAssessmentMethodIdLc').setValue('7');
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtBalanceSupported').setValue('NOT APPLICABLE');
                        }
                    }
                }
            }
            else {
                var lccalculate = Page.incomeArrayforlc;
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (loanCalculatorHelPer.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtAssessmentMethodLc').setValue(lccalculate[j].Assessmentmethod);
                            $('txtAssessmentCodeLc').setValue(lccalculate[j].AssessmentCode);
                            $('txtAssessmentMethodIdLc').setValue(lccalculate[j].AssmentMethodId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            if (lccalculate[j].AssessmentCode == "FT" || lccalculate[j].AssessmentCode == "FC") {
                                $('txtBalanceSupported').setValue(lccalculate[j].AverageBalance);
                            }
                        }
                    }


                }

            }
        }
    },

    changeCarVerification: function() {
        var carVerification = $F("cmbCarVerification");
        if (carVerification == 1) {
            $('divLblDeliveryStatus').style.display = 'block';
            $('divDeliveryStatus').style.display = 'block';

        }
        else {
            $('divLblDeliveryStatus').style.display = 'none';
            $('divDeliveryStatus').style.display = 'none';
        }
    },
    changeSourceNameForSourceCodeOnApplicantDetailsForGeneral: function() {
        var sourceId = parseInt($F("cmbBranch"));
        var source = Page.sourceForGeneralArray.findByProp('AutoSourceId', sourceId);
        if (source) {
            $('txtBranchCode').setValue(source.SourceCode);
        }

    },
    changeSegment: function() {
        var segmentId = $F("cmbEmployeeSegmentLc");
        if (segmentId == "-1") {
            $('txtSegmentCodeLc').setValue('');
            return false;
        }
        var segment = Page.segmentArray.findByProp('SegmentID', segmentId);
        if (segment) {
            $('txtSegmentCodeLc').setValue(segment.SegmentCode);
            loanCalculatorHelPer.calculateTenorSettings();
        }
    },
    changeSubSector: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var subsectorPrimaryId = $F("cmbSubsectorForPrimaryBank" + bankNo);
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', subsectorPrimaryId);
            $('txtProfitMarginForPrimaryBank' + bankNo).setValue(subsectorPrimary.SUSE_INTERESTRATE);
            $('txtProfitMarginCodeForPrimaryBank' + bankNo).setValue(subsectorPrimary.SUSE_IRCODE);
        }
        else if (accountType == "Jt") {
            var subsectorId = $F("cmbSubSectorForJointBank" + bankNo);
            var subsector = Page.subsectorArray.findByProp('SUSE_ID', subsectorId);
            $('txtProfitMarginForJointBank' + bankNo).setValue(subsector.SUSE_INTERESTRATE);
            $('txtSubSectorCode1ForJointBank' + bankNo).setValue(subsector.SUSE_IRCODE);
        }
        loanCalculatorHelPer.setBussinessManCalculation(bankNo, accountType, 'BM');
    },
    changeSubSectorForSelfEmployed: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var subsectorPrimaryId = $F("cmbSubsectorForSEPrBank" + bankNo);
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', subsectorPrimaryId);
            $('txtProfitMarginForSEPrBank' + bankNo).setValue(subsectorPrimary.SUSE_INTERESTRATE);
            $('txtProfitMarginCodeForSEPrBank' + bankNo).setValue(subsectorPrimary.SUSE_IRCODE);
        }
        else if (accountType == "Jt") {
            var subsectorId = $F("cmbSubsectorForSEJtBank" + bankNo);
            var subsector = Page.subsectorArray.findByProp('SUSE_ID', subsectorId);
            $('txtProfitMarginForSEJtBank' + bankNo).setValue(subsector.SUSE_INTERESTRATE);
            $('txtProfitMarginCodeForSEJtBank' + bankNo).setValue(subsector.SUSE_IRCODE);
        }
        analyzeLoanApplicationHelper.changePrivateInCome(bankNo, accountType, "Se");
        loanCalculatorHelPer.setBussinessManCalculation(bankNo, accountType, 'SE');
    },
    changeBrowingRelationship: function() {
        var browingrelationship = $F("cmbBrowingRelationship");
        $("txtRelationshipCode").setValue(browingrelationship);
    },
    populateBankCombo: function(res) {
        var link = "<option value=\"-1\">Select a bank</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BankID + "\">" + res.value[i].BankName + "</option>";
        }
        $("cmbBankNameForOther1").update(link);
        $("cmbBankNameForOther2").update(link);
        $("cmbBankNameForOther3").update(link);
        $("cmbBankNameForOther4").update(link);
        $("cmbBankNameForOther5").update(link);

        //Exposer Off Scb Finential Institution

        $("cmbFinancialInstitutionForOffSCB1").update(link);
        $("cmbFinancialInstitutionForOffSCB2").update(link);
        $("cmbFinancialInstitutionForOffSCB3").update(link);
        $("cmbFinancialInstitutionForOffSCB4").update(link);
        $("cmbFinancialInstitutionForCreditCardOffSCB1").update(link);
        $("cmbFinancialInstitutionForCreditCardOffSCB2").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB1").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB2").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB3").update(link);


        //InCome Generator Tab
        //Primary Applicant
        $("cmbBankNameForBank1IncomeGenerator").update(link);
        $("cmbBankNameForBank2IncomeGenerator").update(link);
        $("cmbBankNameForBank3IncomeGenerator").update(link);
        $("cmbBankNameForBank4IncomeGenerator").update(link);
        $("cmbBankNameForBank5IncomeGenerator").update(link);
        $("cmbBankNameForBank6IncomeGenerator").update(link);

        //Joint Applicant
        $("cmbBankName1ForJointBank1").update(link);
        $("cmbBankName1ForJointBank2").update(link);
        $("cmbBankName1ForJointBank3").update(link);
        $("cmbBankName1ForJointBank4").update(link);
        $("cmbBankName1ForJointBank5").update(link);
        $("cmbBankName1ForJointBank6").update(link);

        //Off SCB For Primary Bank Name For Bussinessman
        $("cmbBankName1ForBussinessTabPrBank1").update(link);
        $("cmbBankName2ForBussinessTabPrBank1").update(link);
        $("cmbBankName3ForBussinessTabPrBank1").update(link);
        $("cmbBankName4ForBussinessTabPrBank1").update(link);

        $("cmbBankName1ForBussinessTabPrBank2").update(link);
        $("cmbBankName2ForBussinessTabPrBank2").update(link);
        $("cmbBankName3ForBussinessTabPrBank2").update(link);
        $("cmbBankName4ForBussinessTabPrBank2").update(link);

        $("cmbBankName1ForBussinessTabPrBank3").update(link);
        $("cmbBankName2ForBussinessTabPrBank3").update(link);
        $("cmbBankName3ForBussinessTabPrBank3").update(link);
        $("cmbBankName4ForBussinessTabPrBank3").update(link);

        $("cmbBankName1ForBussinessTabPrBank4").update(link);
        $("cmbBankName2ForBussinessTabPrBank4").update(link);
        $("cmbBankName3ForBussinessTabPrBank4").update(link);
        $("cmbBankName4ForBussinessTabPrBank4").update(link);

        $("cmbBankName1ForBussinessTabPrBank5").update(link);
        $("cmbBankName2ForBussinessTabPrBank5").update(link);
        $("cmbBankName3ForBussinessTabPrBank5").update(link);
        $("cmbBankName4ForBussinessTabPrBank5").update(link);

        $("cmbBankName1ForBussinessTabPrBank6").update(link);
        $("cmbBankName2ForBussinessTabPrBank6").update(link);
        $("cmbBankName3ForBussinessTabPrBank6").update(link);
        $("cmbBankName4ForBussinessTabPrBank6").update(link);






        //Off SCB for Joint Bank Name For Bussiness man
        $("cmbBankName1ForBussinessTabJtBank1").update(link);
        $("cmbBankName2ForBussinessTabJtBank1").update(link);
        $("cmbBankName3ForBussinessTabJtBank1").update(link);
        $("cmbBankName4ForBussinessTabJtBank1").update(link);

        $("cmbBankName1ForBussinessTabJtBank2").update(link);
        $("cmbBankName2ForBussinessTabJtBank2").update(link);
        $("cmbBankName3ForBussinessTabJtBank2").update(link);
        $("cmbBankName4ForBussinessTabJtBank2").update(link);

        $("cmbBankName1ForBussinessTabJtBank3").update(link);
        $("cmbBankName2ForBussinessTabJtBank3").update(link);
        $("cmbBankName3ForBussinessTabJtBank3").update(link);
        $("cmbBankName4ForBussinessTabJtBank3").update(link);

        $("cmbBankName1ForBussinessTabJtBank4").update(link);
        $("cmbBankName2ForBussinessTabJtBank4").update(link);
        $("cmbBankName3ForBussinessTabJtBank4").update(link);
        $("cmbBankName4ForBussinessTabJtBank4").update(link);

        $("cmbBankName1ForBussinessTabJtBank5").update(link);
        $("cmbBankName2ForBussinessTabJtBank5").update(link);
        $("cmbBankName3ForBussinessTabJtBank5").update(link);
        $("cmbBankName4ForBussinessTabJtBank5").update(link);

        $("cmbBankName1ForBussinessTabJtBank6").update(link);
        $("cmbBankName2ForBussinessTabJtBank6").update(link);
        $("cmbBankName3ForBussinessTabJtBank6").update(link);
        $("cmbBankName4ForBussinessTabJtBank6").update(link);


        //Self Employed tab 
        //Primary bank Statement
        $("cmbBankNameForSEBank1").update(link);
        $("cmbBankNameForSEBank2").update(link);
        $("cmbBankNameForSEBank3").update(link);
        $("cmbBankNameForSEBank4").update(link);
        $("cmbBankNameForSEBank5").update(link);
        $("cmbBankNameForSEBank6").update(link);

        //Self Employed Tab
        //joint Bank Statement

        $("cmbBankNameForSEJtBank1").update(link);
        $("cmbBankNameForSEJtBank2").update(link);
        $("cmbBankNameForSEJtBank3").update(link);
        $("cmbBankNameForSEJtBank4").update(link);
        $("cmbBankNameForSEJtBank5").update(link);
        $("cmbBankNameForSEJtBank6").update(link);



        //Off SCB For Primary Bank Name For Self Employed
        $("cmbBankName1ForSEPrBank1").update(link);
        $("cmbBankName2ForSEPrBank1").update(link);
        $("cmbBankName3ForSEPrBank1").update(link);
        $("cmbBankName4ForSEPrBank1").update(link);

        $("cmbBankName1ForSEPrBank2").update(link);
        $("cmbBankName2ForSEPrBank2").update(link);
        $("cmbBankName3ForSEPrBank2").update(link);
        $("cmbBankName4ForSEPrBank2").update(link);

        $("cmbBankName1ForSEPrBank3").update(link);
        $("cmbBankName2ForSEPrBank3").update(link);
        $("cmbBankName3ForSEPrBank3").update(link);
        $("cmbBankName4ForSEPrBank3").update(link);

        $("cmbBankName1ForSEPrBank4").update(link);
        $("cmbBankName2ForSEPrBank4").update(link);
        $("cmbBankName3ForSEPrBank4").update(link);
        $("cmbBankName4ForSEPrBank4").update(link);

        $("cmbBankName1ForSEPrBank5").update(link);
        $("cmbBankName2ForSEPrBank5").update(link);
        $("cmbBankName3ForSEPrBank5").update(link);
        $("cmbBankName4ForSEPrBank5").update(link);

        $("cmbBankName1ForSEPrBank6").update(link);
        $("cmbBankName2ForSEPrBank6").update(link);
        $("cmbBankName3ForSEPrBank6").update(link);
        $("cmbBankName4ForSEPrBank6").update(link);






        //Off SCB for Joint Bank Name For Self Employed
        $("cmbBankName1ForSEJtBank1").update(link);
        $("cmbBankName2ForSEJtBank1").update(link);
        $("cmbBankName3ForSEJtBank1").update(link);
        $("cmbBankName4ForSEJtBank1").update(link);

        $("cmbBankName1ForSEJtBank2").update(link);
        $("cmbBankName2ForSEJtBank2").update(link);
        $("cmbBankName3ForSEJtBank2").update(link);
        $("cmbBankName4ForSEJtBank2").update(link);

        $("cmbBankName1ForSEJtBank3").update(link);
        $("cmbBankName2ForSEJtBank3").update(link);
        $("cmbBankName3ForSEJtBank3").update(link);
        $("cmbBankName4ForSEJtBank3").update(link);

        $("cmbBankName1ForSEJtBank4").update(link);
        $("cmbBankName2ForSEJtBank4").update(link);
        $("cmbBankName3ForSEJtBank4").update(link);
        $("cmbBankName4ForSEJtBank4").update(link);

        $("cmbBankName1ForSEJtBank5").update(link);
        $("cmbBankName2ForSEJtBank5").update(link);
        $("cmbBankName3ForSEJtBank5").update(link);
        $("cmbBankName4ForSEJtBank5").update(link);

        $("cmbBankName1ForSEJtBank6").update(link);
        $("cmbBankName2ForSEJtBank6").update(link);
        $("cmbBankName3ForSEJtBank6").update(link);
        $("cmbBankName4ForSEJtBank6").update(link);

    },
    populateUserCombo: function(objUserlist) {
        var link = "<option value=\"-1\">Select a User</option>";
        var linkForApp = "<option value=\"-1\">Select a User</option>";
        for (var i = 0; i < objUserlist.length; i++) {
            link += "<option value=\"" + objUserlist[i].USER_ID + "\">" + objUserlist[i].USER_NAME + "</option>";
            if (objUserlist[i].USER_LEVEL == 7) {
                linkForApp += "<option value=\"" + objUserlist[i].USER_ID + "\">" + objUserlist[i].USER_NAME + "</option>";
            }
        }
        $("cmbAppraisedby").update(link);
        $("cmbSupportedby").update(link);
        $("cmbApprovedby").update(linkForApp);
        Page.userArray = objUserlist;
    },
    populateFacilityCombo: function(res) {
        var link = "<option value=\"-1\">Select a Type</option>";
        var link1 = "<option value=\"-1\">Select a Type</option>";


        for (var i = 0; i < res.value.length; i++) {
            if (res.value[i].FACI_NAME == "Credit Card ($)".trim() || res.value[i].FACI_NAME == "Credit Card (BDT)".trim()) {
                link1 += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
            }
            else {
                if (res.value[i].FACI_NAME != "Over Draft".trim()) {
                    link += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
                }
            }
        }
        $("cmbTypeForTermLoans1").update(link);
        $("cmbTypeForTermLoans2").update(link);
        $("cmbTypeForTermLoans3").update(link);
        $("cmbTypeForTermLoans4").update(link);

        $("cmbTypeForCreditCard1").update(link1);
        $("cmbTypeForCreditCard2").update(link1);

        Page.facilityArray = res.value;

    },
    populateSectorCombo: function(res) {
        var link = "<option value=\"-1\">Select a Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SECT_ID + "\">" + res.value[i].SECT_NAME + "</option>";
        }

        //Bussiness Man
        $("cmbIndustryForPrimaryBank1").update(link);
        $("cmbIndustryForPrimaryBank2").update(link);
        $("cmbIndustryForPrimaryBank3").update(link);
        $("cmbIndustryForPrimaryBank4").update(link);
        $("cmbIndustryForPrimaryBank5").update(link);
        $("cmbIndustryForPrimaryBank6").update(link);

        $("cmbIndustryForJointBank1").update(link);
        $("cmbIndustryForJointBank2").update(link);
        $("cmbIndustryForJointBank3").update(link);
        $("cmbIndustryForJointBank4").update(link);
        $("cmbIndustryForJointBank5").update(link);
        $("cmbIndustryForJointBank6").update(link);

        //Self Employed tab
        $("cmbIndustryForSEPrBank1").update(link);
        $("cmbIndustryForSEPrBank2").update(link);
        $("cmbIndustryForSEPrBank3").update(link);
        $("cmbIndustryForSEPrBank4").update(link);
        $("cmbIndustryForSEPrBank5").update(link);
        $("cmbIndustryForSEPrBank6").update(link);

        $("cmbIndustryForSEJtBank1").update(link);
        $("cmbIndustryForSEJtBank2").update(link);
        $("cmbIndustryForSEJtBank3").update(link);
        $("cmbIndustryForSEJtBank4").update(link);
        $("cmbIndustryForSEJtBank5").update(link);
        $("cmbIndustryForSEJtBank6").update(link);

        //Land Lord Tab
        $("cmbEmployeeForLandLordPr").update(link);
        $("cmbEmployeeForLandLordJt").update(link);

        //Loan Calculator For Cash Sequred
        $("cmbCategoryForLcCash").update(link);


    },
    populateCibInfoCombo: function(res) {
        var link = "<option value=\"-1\">Select from List</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].CibInfoId + "\">" + res.value[i].CibInfoName + "</option>";
        }
        $("cmbBureauHistory").update(link);
        Page.cibInfo = res.value;
    },
    populateIncomeAssementCombo: function(res) {
        var link = "<option value=\"-1\">Select an income Assement</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].INAM_ID + "\">" + res.value[i].INAM_METHOD + "</option>";
        }

        //Bussiness man Tab----------------------------------------------------------------
        $("cmbIncomeAssessmentForPrimaryBank1").update(link);
        $("cmbIncomeAssessmentForPrimaryBank2").update(link);
        $("cmbIncomeAssessmentForPrimaryBank3").update(link);
        $("cmbIncomeAssessmentForPrimaryBank4").update(link);
        $("cmbIncomeAssessmentForPrimaryBank5").update(link);
        $("cmbIncomeAssessmentForPrimaryBank6").update(link);

        $("cmbIncomeAssementForJointBank1").update(link);
        $("cmbIncomeAssementForJointBank2").update(link);
        $("cmbIncomeAssementForJointBank3").update(link);
        $("cmbIncomeAssementForJointBank4").update(link);
        $("cmbIncomeAssementForJointBank5").update(link);
        $("cmbIncomeAssementForJointBank6").update(link);

        //Self Employed Tab--------------------------------------------------------------------

        $("cmbIncomeAssessmentForSEPrBank1").update(link);
        $("cmbIncomeAssessmentForSEPrBank2").update(link);
        $("cmbIncomeAssessmentForSEPrBank3").update(link);
        $("cmbIncomeAssessmentForSEPrBank4").update(link);
        $("cmbIncomeAssessmentForSEPrBank5").update(link);
        $("cmbIncomeAssessmentForSEPrBank6").update(link);

        $("cmbIncomeAssessmentForSEJtBank1").update(link);
        $("cmbIncomeAssessmentForSEJtBank2").update(link);
        $("cmbIncomeAssessmentForSEJtBank3").update(link);
        $("cmbIncomeAssessmentForSEJtBank4").update(link);
        $("cmbIncomeAssessmentForSEJtBank5").update(link);
        $("cmbIncomeAssessmentForSEJtBank6").update(link);

        //Land lord Combo
        $("cmbIncomeAssementForLandLordPr").update(link);
        $("cmbIncomeAssementForLandLordJt").update(link);

        //Loan Calculator For Cash Sequred
        $("cmbAssessmentMethodForLcCash").update(link);



        Page.incomeAssementArray = res.value;
    },
    populateEmployerForSalaried: function(res) {
        var link = "<option value=\"-1\">Select an income Assement</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].AutoCompanyId + "\">" + res.value[i].CompanyName + "</option>";
        }

        //Salried Tab----------------------------------------------------------------
        $("cmbEmployerForSalariedForJt").update(link);
        $("cmbEmployerForSalariedForPr").update(link);

        Page.employerForSalariedArray = res.value;
    },
    populateSegmentCombo: function(res) {
        var link = "<option value=\"-1\">Select a Segment</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SegmentID + "\">" + res.value[i].SegmentName + "</option>";
        }

        //Loan Calculator----------------------------------------------------------------
        $("cmbEmployeeSegmentLc").update(link);

        Page.segmentArray = res.value;
    },
    changeIncomeAssement: function(bankNo, accountType) {
        if (accountType == "Pr") {
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            $("txtIncomeAssessmentCodeForPrimaryBank" + bankNo).setValue(incomeAssBuss.INAM_METHODCODE);
            if (incomeAssBuss.INAM_METHODCODE == "FT") {

                var averageBalancePr = false;

                for (var i = 0; i < Page.isAverageBalancePrimaryArray.length; i++) {
                    if (Page.isAverageBalancePrimaryArray[i].BankNo == bankNo && Page.isAverageBalancePrimaryArray[i].Accounttype == accountType && Page.isAverageBalancePrimaryArray[i].TabName == "Bm") {
                        averageBalancePr = true;
                        break;
                    }
                }
                if (averageBalancePr == false) {
                    $('divPrevRepHis' + bankNo).style.display = 'none';
                    $('divAverageBalancePr' + bankNo).style.display = 'block';
                    analyzeLoanApplicationHelper.calculateAverageBalance(bankNo, accountType);
                }
                else {
                    alert("Average Balance is not applicable");
                }

            }
            else if (incomeAssBuss.INAM_METHODCODE == "FF") {
                $('divAverageBalancePr' + bankNo).style.display = 'none';
                $('divPrevRepHis' + bankNo).style.display = 'block';
            }
            else {
                $('divAverageBalancePr' + bankNo).style.display = 'none';
                $('divPrevRepHis' + bankNo).style.display = 'none';
            }

        }
        else if (accountType == "Jt") {

            var incomeAssIdForBussJoint = $F("cmbIncomeAssementForJointBank" + bankNo);
            var incomeAssBussJoint = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBussJoint);
            $("txtIncomeAssementCodeForJointBank" + bankNo).setValue(incomeAssBussJoint.INAM_METHODCODE);
            if (incomeAssBussJoint.INAM_METHODCODE == "FT") {
                var averageBalanceJt = false;

                for (var i = 0; i < Page.isAverageBalanceJointArray.length; i++) {
                    if (Page.isAverageBalanceJointArray[i].BankNo == bankNo && Page.isAverageBalanceJointArray[i].Accounttype == accountType && Page.isAverageBalanceJointArray[i].TabName == "Bm") {
                        averageBalanceJt = true;
                        break;
                    }
                }

                if (averageBalanceJt == false) {
                    $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                    $('divAverageBalanceJoint' + bankNo).style.display = 'block';
                    analyzeLoanApplicationHelper.calculateAverageBalance(bankNo, accountType);
                }
                else {
                    alert("Average Balance is not applicable");
                }
            }
            else if (incomeAssBussJoint.INAM_METHODCODE == "FF") {
                $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                $('divPrevRepHisJoint' + bankNo).style.display = 'block';
            }
            else {
                $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                $('divPrevRepHisJoint' + bankNo).style.display = 'none';
            }
        }
        loanCalculatorHelPer.setBussinessManCalculation(bankNo, accountType, 'BM');
    },
    populateSubSectorCombo: function(res, bankNo, accountType) {

        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }

        if (accountType == "Pr") {
            $("cmbSubsectorForPrimaryBank" + bankNo).update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubSectorForJointBank" + bankNo).update(link);
        }
        //debugger;
        //Page.subsectorArray = res.value;
        //Page.subsectorArray.push(res.value);
    },
    populateSubSectorComboForSelfEmployed: function(res, bankNo, accountType) {

        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }
        if (accountType == "Pr") {
            $("cmbSubsectorForSEPrBank" + bankNo).update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubsectorForSEJtBank" + bankNo).update(link);
        }
        //debugger;
        //Page.subsectorArray = res.value;
        //Page.subsectorArray.push(res.value);
    },
    populateSubSectorComboForLandLord: function(res, accountType) {

        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }
        if (accountType == "Pr") {
            $("cmbSubsectorForLandLordPr").update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubsectorForLandLordJt").update(link);
        }

    },
    changePrivateInCome: function(bankNo, accountType, tabName) {
        if (accountType == "Pr" && tabName == "Se") {
            var industry = $F("cmbIndustryForSEPrBank" + bankNo);
            var subsector = $F("cmbSubsectorForSEPrBank" + bankNo);
            var privateIncome = $F("cmbPrivateIncomeForSEPrBank" + bankNo);
            if (industry == "12" && subsector == "143" && privateIncome == "1") {
                $("divTutionForSEPrBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'block';
                $("txtTotalInComeTutionForSEPrBank" + bankNo).setValue('0');
            }
            else if (industry == "12" && subsector == "150" && privateIncome == "1") {
                $("divTutionForSEPrBank" + bankNo).style.display = 'block';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEPrBank" + bankNo).setValue('0');
            }
            else {
                $("divTutionForSEPrBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEPrBank" + bankNo).setValue('0');
                $("txtTotalInComeTutionForSEPrBank" + bankNo).setValue('0');
            }
        }
        else if (accountType == "Jt" && tabName == "Se") {
            var industryForSeJt = $F("cmbIndustryForSEJtBank" + bankNo);
            var subsectorSeJt = $F("cmbSubsectorForSEJtBank" + bankNo);
            var privateIncomeSeJt = $F("cmbPrivateIncomeForSEJtBank" + bankNo);
            if (industryForSeJt == "12" && subsectorSeJt == "143" && privateIncomeSeJt == "1") {
                $("divTutionJtBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'block';
                $("txtTotalInComeTutionForSEJtBank" + bankNo).setValue('0');
            }
            else if (industryForSeJt == "12" && subsectorSeJt == "150" && privateIncomeSeJt == "1") {
                $("divTutionJtBank" + bankNo).style.display = 'block';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEJtBank" + bankNo).setValue('0');
            }
            else {
                $("divTutionJtBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'none';
                $("txtTotalInComeTutionForSEJtBank" + bankNo).setValue('0');
                $("txtTotalIncomeForSEJtBank" + bankNo).setValue('0');
            }
        }
        loanCalculatorHelPer.checkDoctorChambers(accountType, 'DC');
        loanCalculatorHelPer.checkDoctorChambers(accountType, 'TC');
    },
    changeTypeForExposer: function(elementId) {
        var type = $F(elementId);
        var typeobject = Page.facilityArray.findByProp('FACI_ID', type);
        if (typeobject) {
            switch (elementId) {
                case "cmbTypeForTermLoans1":
                    $("txtflag1").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans2":
                    $("txtflag2").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans3":
                    $("txtflag3").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans4":
                    $("txtflag4").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForCreditCard1":
                    $("txtFlagForcreditCard1").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForCreditCard2":
                    $("txtFlagForcreditCard2").setValue(typeobject.FACI_CODE);
                    break;
            }
        }
        else {
            switch (elementId) {
                case "cmbTypeForTermLoans1":
                    $("txtflag1").setValue('');
                    break;
                case "cmbTypeForTermLoans2":
                    $("txtflag2").setValue('');
                    break;
                case "cmbTypeForTermLoans3":
                    $("txtflag3").setValue('');
                    break;
                case "cmbTypeForTermLoans4":
                    $("txtflag4").setValue('');
                    break;
                case "cmbTypeForCreditCard1":
                    $("txtFlagForcreditCard1").setValue('');
                    break;
                case "cmbTypeForCreditCard2":
                    $("txtFlagForcreditCard2").setValue('');
                    break;
            }
        }
        analyzeLoanApplicationHelper.populateColleterization();
    },
    populateBranchCombo: function(res, elementId) {
        var link = "<option value=\"-1\">Select a Branch</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BRAN_ID + "\">" + res.value[i].BRAN_NAME + "</option>";
        }

        if (elementId == "cmbBankNameForOther1") {
            $("cmbBranchNameOther1").update(link);
        }
        else if (elementId == "cmbBankNameForOther2") {
            $("cmbBranchNameOther2").update(link);
        }
        else if (elementId == "cmbBankNameForOther3") {
            $("cmbBranchNameOther3").update(link);
        }
        else if (elementId == "cmbBankNameForOther4") {
            $("cmbBranchNameOther4").update(link);
        }
        else if (elementId == "cmbBankNameForOther5") {
            $("cmbBranchNameOther5").update(link);
        }
    },
    populateBranchForIncomeGeneratorCombo: function(res, elementId) {
        var link = "<option value=\"-1\">Select a Branch</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BRAN_ID + "\">" + res.value[i].BRAN_NAME + "</option>";
        }
        $(elementId).update(link);
    },
    calculateEMIShare: function(rowId) {
        var emi = $F("txtEMI" + rowId);
        var emiPercesnt = $F("txtEMIPer" + rowId);
        if (emi == "") {
            emi = 0;
        }
        if (emiPercesnt == "") {
            emiPercesnt = 0;
        }
        if (!util.isFloat(emi)) {
            alert("Number field is required");
            $('txtEMI' + rowId).setValue('0');
            $("txtEMIShare" + rowId).setValue('0');
            //$("txtEMI" + rowId).focus();
            return false;
        }
        if (!util.isFloat(emiPercesnt)) {
            alert("Number field is required");
            $('txtEMIPer' + rowId).setValue('0');
            $("txtEMIShare" + rowId).setValue('0');
            //$("txtEMIPer" + rowId).focus();
            return false;
        }
        var emiShare = parseFloat(emi * parseFloat(emiPercesnt / 100));

        $("txtEMIShare" + rowId).setValue(emiShare.toFixed(2));
    },
    changeoverDraftInterest: function(rowId) {
        var originalLimit = $F("txtOverdraftlimit" + rowId);
        var interestRate = $F("txtOverdraftInterest" + rowId);
        var sequrityPv = $F("txtSecurityPVForoverDraft" + rowId);
        if (originalLimit == "") {
            originalLimit = 0;
        }
        if (interestRate == "") {
            interestRate = 0;
        }


        if (!util.isFloat(originalLimit)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtOverdraftInteres" + rowId).setValue('0');
            //$("txtOverdraftlimit" + rowId).focus();
            return false;
        }
        if (!util.isFloat(interestRate)) {
            alert("Number field is required");
            $('txtOverdraftInterest' + rowId).setValue('0');
            $("txtOverdraftInteres" + rowId).setValue('0');
            //$("txtOverdraftInterest" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtSecurityPVForoverDraft" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var overDraftInterest = parseFloat(originalLimit * parseFloat(interestRate / 12));

        $("txtOverdraftInteres" + rowId).setValue(overDraftInterest.toFixed(2));

        if (sequrityPv == "") {
            sequrityPv = 0;
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            return false;
        }

        var ltv = parseFloat(originalLimit / sequrityPv);

        $("txtLTVForSecurityForoverDraft" + rowId).setValue(ltv.toFixed(2));

    },
    changeTurnOverShareForOffSCB: function(rowId) {
        var emi = $F("txtEMIforOffSCB" + rowId);
        var emiper = $F("txtEMIPerForOffSCB" + rowId);
        if (emi == "") {
            emi = 0;
        }
        if (emiper == "") {
            emiper = 0;
        }
        if (!util.isFloat(emi)) {
            alert("Number field is required");
            $('txtEMIforOffSCB' + rowId).setValue('0');
            $("txtEMIShareForOffSCB" + rowId).setValue('0');
            //$("txtEMIforOffSCB" + rowId).focus();
            return false;
        }
        if (!util.isFloat(emiper)) {
            alert("Number field is required");
            $('txtEMIPerForOffSCB' + rowId).setValue('0');
            $("txtEMIShareForOffSCB" + rowId).setValue('0');
            //$("txtEMIPerForOffSCB" + rowId).focus();
            return false;
        }
        var emiShare = parseFloat(emi * parseFloat(emiper / 100));

        $("txtEMIShareForOffSCB" + rowId).setValue(emiShare.toFixed(2));
    },
    changeOverDraftInterstForOffSCB: function(rowId) {
        var originalLimit = $F("txtOriginalLimitForOverDraftOffScb" + rowId);
        var interestRate = $F("txtinterestRateForOverDraftOffSCB" + rowId);
        if (originalLimit == "") {
            originalLimit = 0;
        }
        if (interestRate == "") {
            interestRate = 0;
        }
        if (!util.isFloat(originalLimit)) {
            alert("Number field is required");
            $('txtOriginalLimitForOverDraftOffScb' + rowId).setValue('0');
            $("txtInterestForOverDraftInterest" + rowId).setValue('0');
            //$("txtOriginalLimitForOverDraftOffScb" + rowId).focus();
            return false;
        }
        if (!util.isFloat(interestRate)) {
            alert("Number field is required");
            $('txtOriginalLimitForOverDraftOffScb' + rowId).setValue('0');
            $("txtInterestForOverDraftInterest" + rowId).setValue('0');
            //$("txtOriginalLimitForOverDraftOffScb" + rowId).focus();

            return false;
        }
        var overDraftInterest = parseFloat(parseFloat(originalLimit * parseFloat(interestRate / 12)) / 100);

        $("txtInterestForOverDraftInterest" + rowId).setValue(overDraftInterest.toFixed(2));
    },
    calculateLTVForSequrity: function(rowId) {
        var outStandingLimit = $F("txtoutStanding" + rowId);
        var sequrityPv = $F("txtSecurityPV" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $("txtSecurityPV" + rowId).setValue('0');
            $("txtLTVForSecurity" + rowId).setValue('0');
            return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtoutStanding' + rowId).setValue('0');
            $("txtLTVForSecurity" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurity' + rowId).setValue('0');
            $("txtSecurityPV" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv);

        $("txtLTVForSecurity" + rowId).setValue(ltv.toFixed(2));
    },
    calculateLTVForCreditCardSequrity: function(rowId) {
        var outStandingLimit = $F("txtOriginalLimitForCreditCard" + rowId);
        var sequrityPv = $F("txtSecurityPVForCreditCard" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            $("txtOriginalLimitForCreditCard" + rowId).setValue('0');
            //$("txtOriginalLimitForCreditCard" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            $("txtSecurityPVForCreditCard" + rowId).setValue('0');
            //$("txtSecurityPVForCreditCard" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv);

        $("txtLTVForSecurityForCreditCard" + rowId).setValue(ltv.toFixed(2));
    },
    calculateLTVForOverDraftSequrity: function(rowId) {
        var outStandingLimit = $F("txtOverdraftlimit" + rowId);
        var sequrityPv = $F("txtSecurityPVForoverDraft" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            //$("txtOverdraftlimit" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurityForoverDraft' + rowId).setValue('0');
            $("txtSecurityPVForoverDraft" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv);

        $("txtLTVForSecurityForoverDraft" + rowId).setValue(ltv.toFixed(2));
    },

    //Bussiness Man tab
    populateAverageTotalForIncomeGeneratorbankStatement: function(bankId, accountNumber, typeName, autoBankStatementAvgTotalList, bankNoCount) {

        var j = 0;
        var k = 0;
        var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var averageBalanceFor12Month = 0;
        var averageBalanceFor6Month = 0;
        var sixMonthArrayForCto = [];
        var sixMonthArrayForAverageBalance = [];
        if (typeName == "Primary") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 1) {
                    j += 1;
                    var d = new Date(autoBankStatementAvgTotalList[i].Month);
                    var newDate = monthNames[d.getMonth()] + "-" + d.getFullYear();
                    $("hdn" + j + "ForBMPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtMonth" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(newDate);
                    $("txtcreditTurnOver" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAvarageBalance" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                }
            }
            $("txtTotalCTOFor12MonthBank" + bankNoCount).setValue(totalCtofor12Month);

            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });

            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
                if (sixMonthArrayForCto[s] != "") {
                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
                }
            }
            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
                if (sixMonthArrayForAverageBalance[s] != "") {
                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
                }
            }

            $("txtTotalCTOFor6MonthBank" + bankNoCount).setValue(totalCtofor6Month);
            var avergTotalCto12Month = (totalCtofor12Month / 12);
            var avergTotalCto6Month = (totalCtofor6Month / 6);
            $("txtAverageCTO12MonthBank" + bankNoCount).setValue(avergTotalCto12Month.toFixed(2));
            $("txtAverageCTO6MonthBank" + bankNoCount).setValue(avergTotalCto6Month.toFixed(2));

            $("txtAverageBalanceFor12MonthBank" + bankNoCount).setValue(averageBalanceFor12Month);
            $("txtAverageBalanceFor6MonthBank" + bankNoCount).setValue(averageBalanceFor6Month);

            var linkCto = "<option value='" + avergTotalCto12Month.toFixed(2) + "'>" + avergTotalCto12Month.toFixed(2) + "</option><option value='" + avergTotalCto6Month.toFixed(2) + "'>" + avergTotalCto6Month.toFixed(2) + "</option>";
            var linkAvg = "<option value='" + averageBalanceFor12Month + "'>" + averageBalanceFor12Month + "</option><option value='" + averageBalanceFor6Month + "'>" + averageBalanceFor6Month + "</option>";

            $("cmbConsideredCtoForPrBank" + bankNoCount).update(linkCto);
            $("cmbConsideredBalanceForPrBank" + bankNoCount).update(linkAvg);

            $("cmbConsideredCtoForPrBank" + bankNoCount).setValue(avergTotalCto12Month.toFixed(2)); //E35
            $("cmbConsideredBalanceForPrBank" + bankNoCount).setValue(averageBalanceFor12Month);
            $("txtConsiderMarginForPrimaryBank" + bankNoCount).setValue('3'); //E37

            //=IF(P31="FT",P37,E35*E37)
            var grossincome = parseFloat((avergTotalCto12Month * 3) / 100);
            $("txtgrossincomeForPrimaryBank" + bankNoCount).setValue(grossincome.toFixed(2));
            $("txtNetAfterPaymentForPrimaryBank" + bankNoCount).setValue(grossincome.toFixed(2));
            var share = $F("txtShareForPrimaryBank" + bankNoCount);

            var netIncomeTotal = parseFloat((grossincome * parseFloat(share)) / 100);

            $("txtNetincomeForPrimaryBank" + bankNoCount).setValue(netIncomeTotal.toFixed(2));

            loanCalculatorHelPer.setBussinessManCalculation(bankNoCount, 'Pr', 'BM');



        }
        else if (typeName == "Joint") {

            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 2) {
                    k += 1;

                    $("hdn" + k + "ForBMJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtmonthName" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Month);
                    $("txtcreditturnOver" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAverageBalance" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                    //                    if (j < 7) {
                    //                        totalCtofor6Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    //                        averageBalanceFor6Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    //                    }
                }
            }
            $("txtTotalCTOFor12MonthJointBank" + bankNoCount).setValue(totalCtofor12Month);


            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });
            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
                if (sixMonthArrayForCto[s] != "") {
                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
                }
            }
            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
                if (sixMonthArrayForAverageBalance[s] != "") {
                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
                }
            }


            $("txtTotalCTOFor6MonthJointBank" + bankNoCount).setValue(totalCtofor6Month);
            var avergTotalCto12Monthjoint = (totalCtofor12Month / 12);
            var avergTotalCto6Monthjoint = (totalCtofor6Month / 6);
            $("txtAverageCTOFor12MonthForJointBank" + bankNoCount).setValue(avergTotalCto12Monthjoint.toFixed(2));
            $("txtAverageCTOFor6MonthForJointBank" + bankNoCount).setValue(avergTotalCto6Monthjoint.toFixed(2));

            $("txtAverageBalance12MonthForJointBank" + bankNoCount).setValue(averageBalanceFor12Month);
            $("txtAverageBalance6MonthForJointBank" + bankNoCount).setValue(averageBalanceFor6Month);


            var linkCtoJoint = "<option value='" + avergTotalCto12Monthjoint.toFixed(2) + "'>" + avergTotalCto12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalCto6Monthjoint.toFixed(2) + "'>" + avergTotalCto6Monthjoint.toFixed(2) + "</option>";
            var linkAvgJoint = "<option value='" + averageBalanceFor12Month + "'>" + averageBalanceFor12Month + "</option><option value='" + averageBalanceFor6Month + "'>" + averageBalanceFor6Month + "</option>";

            $("cmbConsideredCtoForJtBank" + bankNoCount).update(linkCtoJoint);
            $("cmbConsideredBalanceForJtBank" + bankNoCount).update(linkAvgJoint);
            $("cmbConsideredCtoForJtBank" + bankNoCount).setValue(avergTotalCto12Monthjoint.toFixed(2));
            $("cmbConsideredBalanceForJtBank" + bankNoCount).setValue(averageBalanceFor12Month);
            $("txtConsideredMarginBMForJointBank" + bankNoCount).setValue('3'); //E37


            //=IF(P31="FT",P37,E35*E37)
            var grossJtincome = parseFloat((avergTotalCto12Monthjoint * 3) / 100);
            $("txtgrossIncomeForJointBank" + bankNoCount).setValue(grossJtincome.toFixed(2));
            $("txtNetAfterPaymentForJointBank" + bankNoCount).setValue(grossJtincome.toFixed(2));
            var shareJt = $F("txtShareForJointBank" + bankNoCount);

            var netIncomeTotalJt = parseFloat((grossJtincome * parseFloat(shareJt)) / 100);

            $("txtNetIncomeForJointBank" + bankNoCount).setValue(netIncomeTotalJt.toFixed(2));
            loanCalculatorHelPer.setBussinessManCalculation(bankNoCount, 'Jt', 'BM');
        }

    },
    changeCreditTurnoverForPrApplicant: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField)) && !util.isNegativeNumber($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var sixMonthArrayForCto = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                totalCtofor12Month += parseFloat($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo));
            }
            if (util.isFloat($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo));
            }
            else if (util.isNegativeNumber($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo));
            }
            sixMonthArrayForCto.push(totalCtofor12Month);
        }
        $("txtTotalCTOFor12MonthBank" + bankNo).setValue(totalCtofor12Month);
        sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            if (sixMonthArrayForCto[s] != "") {
                totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            }
        }
        $("txtTotalCTOFor6MonthBank" + bankNo).setValue(totalCtofor6Month);
        var avergTotalCto12MonthPr = (totalCtofor12Month / 12);
        var avergTotalCto6MonthPr = (totalCtofor6Month / 6);
        $("txtAverageCTO12MonthBank" + bankNo).setValue(avergTotalCto12MonthPr.toFixed(2));
        $("txtAverageCTO6MonthBank" + bankNo).setValue(avergTotalCto6MonthPr.toFixed(2));

        var linkCto = "<option value='" + avergTotalCto12MonthPr.toFixed(2) + "'>" + avergTotalCto12MonthPr.toFixed(2) + "</option><option value='" + avergTotalCto6MonthPr.toFixed(2) + "'>" + avergTotalCto6MonthPr.toFixed(2) + "</option>";

        $("cmbConsideredCtoForPrBank" + bankNo).update(linkCto);
        $("cmbConsideredCtoForPrBank" + bankNo).setValue(avergTotalCto12MonthPr.toFixed(2));

        var considerMarginFinal = $F("txtConsiderMarginForPrimaryBank" + bankNo);
        var grossIncome = parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100);
        $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncome);
        $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(grossIncome);

        var share = $F("txtShareForPrimaryBank" + bankNo);

        var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

        $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal.toFixed(2));


    },
    changeAverageBalanceForPrApplicant: function(bankNo, tableName, identityField) {
        if (!util.isFloat($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalAvgfor12Month = 0;
        var totalAvgfor6Month = 0;
        var sixMonthArrayForAvg = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                totalAvgfor12Month += parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo));
            }
            sixMonthArrayForAvg.push(totalAvgfor12Month);
        }
        sixMonthArrayForAvg = sixMonthArrayForAvg.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForAvg.length - 6; s++) {
            if (sixMonthArrayForAvg[s] != "") {
                totalAvgfor6Month += parseFloat(sixMonthArrayForAvg[s]);
            }
        }
        var avergTotalAvg12Monthjoint = (totalAvgfor12Month / 12);
        var avergTotalAvg6Monthjoint = (totalAvgfor6Month / 6);
        $("txtAverageBalanceFor12MonthBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));
        $("txtAverageBalanceFor6MonthBank" + bankNo).setValue(avergTotalAvg6Monthjoint.toFixed(2));

        var incomeAssement = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
        if (incomeAssement == "1") {

            analyzeLoanApplicationHelper.calculateAverageBalance(bankNo, "Pr");

        }
        var linkAvg = "<option value='" + avergTotalAvg12Monthjoint.toFixed(2) + "'>" + avergTotalAvg12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalAvg6Monthjoint.toFixed(2) + "'>" + avergTotalAvg6Monthjoint.toFixed(2) + "</option>";
        $("cmbConsideredBalanceForPrBank" + bankNo).update(linkAvg);
        $("cmbConsideredBalanceForPrBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));

    },
    changeCreditTurnoverForJtApplicant: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField)) && !util.isNegativeNumber($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var sixMonthArrayForCto = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo))) {
                totalCtofor12Month += parseFloat($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo));
            }
            if (util.isFloat($F('txtAdjustment' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtAdjustment' + i + 'ForJointBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForJointBank' + bankNo));
            }
            else if (util.isNegativeNumber($F('txtAdjustment' + i + 'ForJointBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForJointBank' + bankNo));
            }
            sixMonthArrayForCto.push(totalCtofor12Month);
        }
        $("txtTotalCTOFor12MonthJointBank" + bankNo).setValue(totalCtofor12Month);
        sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            if (sixMonthArrayForCto[s] != "") {
                totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            }
        }
        $("txtTotalCTOFor6MonthJointBank" + bankNo).setValue(totalCtofor6Month);
        var avergTotalCto12Monthjoint = (totalCtofor12Month / 12);
        var avergTotalCto6Monthjoint = (totalCtofor6Month / 6);
        $("txtAverageBalance12MonthForJointBank" + bankNo).setValue(avergTotalCto12Monthjoint.toFixed(2));
        $("txtAverageBalance6MonthForJointBank" + bankNo).setValue(avergTotalCto6Monthjoint.toFixed(2));


        var linkCto = "<option value='" + avergTotalCto12Monthjoint.toFixed(2) + "'>" + avergTotalCto12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalCto6Monthjoint.toFixed(2) + "'>" + avergTotalCto6Monthjoint.toFixed(2) + "</option>";
        $("cmbConsideredCtoForJtBank" + bankNo).update(linkCto);
        $("cmbConsideredCtoForJtBank" + bankNo).setValue(avergTotalCto12Monthjoint.toFixed(2));

        var considerMarginFinalForJoint = $F("txtConsideredMarginBMForJointBank" + bankNo);

        var grossIncomeForJoint = parseFloat(parseFloat(avergTotalCto12Monthjoint * considerMarginFinalForJoint) / 100);
        $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncomeForJoint);
        $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncomeForJoint);

        var shareJoint = $F("txtShareForJointBank" + bankNo);

        var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);

        $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));



    },

    changeAverageBalanceForJtApplicant: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalAvgfor12Month = 0;
        var totalAvgfor6Month = 0;
        var sixMonthArrayForAvg = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForJointBank' + bankNo))) {
                totalAvgfor12Month += parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo));
            }
            sixMonthArrayForAvg.push(totalAvgfor12Month);
        }
        sixMonthArrayForAvg = sixMonthArrayForAvg.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForAvg.length - 6; s++) {
            if (sixMonthArrayForAvg[s] != "") {
                totalAvgfor6Month += parseFloat(sixMonthArrayForAvg[s]);
            }
        }
        var avergTotalAvg12Monthjoint = (totalAvgfor12Month / 12);
        var avergTotalAvg6Monthjoint = (totalAvgfor6Month / 6);
        $("txtAverageBalance12MonthForJointBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));
        $("txtAverageBalance6MonthForJointBank" + bankNo).setValue(avergTotalAvg6Monthjoint.toFixed(2));

        var incomeAssement = $F("cmbIncomeAssementForJointBank" + bankNo);
        if (incomeAssement == "1") {
            analyzeLoanApplicationHelper.calculateAverageBalance(bankNo, "Jt");
        }



        var linkAvg = "<option value='" + avergTotalAvg12Monthjoint + "'>" + avergTotalAvg12Monthjoint + "</option><option value='" + avergTotalAvg6Monthjoint + "'>" + avergTotalAvg6Monthjoint + "</option>";

        $("cmbConsideredBalanceForJtBank" + bankNo).update(linkAvg);
        $("cmbConsideredBalanceForJtBank" + bankNo).setValue(avergTotalAvg12Monthjoint);

    },
    calculateAverageBalance: function(bankNo, accountType) {
        var firstst9MonthTotal = 0;
        var first6MonthTotal = 0;
        var last6Month = 0;
        var last3month = 0;
        var j = 0;
        var monthArray = [];




        if (accountType == "Pr") {
            //var accountnumber = $F("txtAccountNumberBank" + bankNo);
            //var bankId = $F("cmbBankNameForBank" + bankNo + "IncomeGenerator");


            var tablePrAccount = $("tblPrBankStatementForIncomeGenerator" + bankNo);
            var rowCount = tablePrAccount.rows.length;


            //for (var i = 0; i < Page.bankStatementArray.length; i++) {
            for (var i = 1; i <= rowCount - 1; i++) {
                //if (bankId == Page.bankStatementArray[i].BankId && accountnumber == Page.bankStatementArray[i].AccountNumber && Page.bankStatementArray[i].Type == 1) {
                if (util.isFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    //last3month += monthArray[k].AVGTotal;
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    //last6Month += monthArray[k].AVGTotal;
                    last6Month += monthArray[k];
                }
            }
            //=IF(M40>=M37,"25%",IF(M40<=M37,"15%","-"))
            //=IF(M39>=M38,"33%",IF(M39<=M38,"20%","-"))
            $("txt1st9monthForPrimaryBank" + bankNo).setValue(firstst9MonthTotal); //m37
            $("txt1st6MonthForPrimaryBank" + bankNo).setValue(first6MonthTotal); //m38
            $("txtLast6monthForPrimaryBank" + bankNo).setValue(last6Month); //m39
            $("txtLast3monthForPrimaryBank" + bankNo).setValue(last3month); //m40

            //Consider Margin
            var considerMargin1 = 0;
            var considerMargin2 = 0;
            if (last3month >= firstst9MonthTotal) {
                considerMargin1 = "25";
            }
            else {
                considerMargin1 = "15";
            }

            if (last6Month >= first6MonthTotal) {
                considerMargin2 = "33";
            }
            else {
                considerMargin2 = "20";
            }

            var considerMarginFinal = 0;
            if (considerMargin1 <= considerMargin2) {
                considerMarginFinal = considerMargin1;
            }
            else {
                considerMarginFinal = considerMargin2;
            }


            $("txtConsiderednarginForPrimaryBank" + bankNo).setValue(considerMarginFinal);
            $("txtConsiderMarginForPrimaryBank" + bankNo).setValue(considerMarginFinal);

            var grossIncome = parseFloat((parseFloat($F("cmbConsideredCtoForPrBank" + bankNo)) * considerMarginFinal) / 100);
            $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncome);
            $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(grossIncome);

            var share = $F("txtShareForPrimaryBank" + bankNo);

            var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

            $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal.toFixed(2));

        }
        else if (accountType == "Jt") {


            //            var accountnumberForJoint = $F("txtAccountNumber1ForJointBank" + bankNo);
            //            var bankIdForJoint = $F("cmbBankName1ForJointBank" + bankNo);


            var tableJtAccount = $("tblJtBankStatementForIncomeGenerator" + bankNo);
            var rowCountJt = tableJtAccount.rows.length;
            //for (var i = 0; i < Page.bankStatementArray.length; i++) {
            for (var i = 1; i <= rowCountJt - 1; i++) {
                //if (bankIdForJoint == Page.bankStatementArray[i].BankId && accountnumberForJoint == Page.bankStatementArray[i].AccountNumber && Page.bankStatementArray[i].Type == 2) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForJointBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txtAVGBalanceFor1st9MontForJointBank" + bankNo).setValue(firstst9MonthTotal);
            $("txtAVGBalanceFor1st6MontForJointBank" + bankNo).setValue(first6MonthTotal);
            $("txtAVGBalanceForLast6MontForJointBank" + bankNo).setValue(last6Month);
            $("txtAVGBalanceForLast3MontForJointBank" + bankNo).setValue(last3month);

            //Consider Margin
            var considerMarginForJoint1 = 0;
            var considerMarginForJoint2 = 0;
            if (last3month >= firstst9MonthTotal) {
                considerMarginForJoint1 = "25";
            }
            else {
                considerMarginForJoint1 = "15";
            }

            if (last6Month >= first6MonthTotal) {
                considerMarginForJoint2 = "33";
            }
            else {
                considerMarginForJoint2 = "20";
            }

            var considerMarginFinalForJoint = 0;
            if (considerMarginForJoint1 <= considerMarginForJoint2) {
                considerMarginFinalForJoint = considerMarginForJoint1;
            }
            else {
                considerMarginFinalForJoint = considerMarginForJoint2;
            }
            $("txtConsideredMarginForJointBank" + bankNo).setValue(considerMarginFinalForJoint);
            $("txtConsideredMarginBMForJointBank" + bankNo).setValue(considerMarginFinalForJoint);

            var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);
            $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncomeForJoint);
            $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncomeForJoint);

            var shareJoint = $F("txtShareForJointBank" + bankNo);

            var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);

            $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));

        }
    },
    changeConsiderCto: function(bankNo, accounttype, tabName) {
        if (accounttype == "Pr" && tabName == "BM") {
            var consideredCto = $F("cmbConsideredCtoForPrBank" + bankNo);
            var considerMargin = $F("txtConsiderMarginForPrimaryBank" + bankNo);
            var grossIncome = parseFloat((parseFloat(consideredCto) * parseFloat(considerMargin)) / 100);
            $("txtgrossincomeForPrimaryBank" + bankNo).setValue(grossIncome);
            $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(grossIncome);

            var share = $F("txtShareForPrimaryBank" + bankNo);

            var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

            $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal.toFixed(2));

        }
        else if (accounttype == "Jt" && tabName == "BM") {
            var considerMarginFinalForJoint = $F("txtConsiderMarginForPrimaryBank" + bankNo);
            var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);
            $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncomeForJoint);
            $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncomeForJoint);

            var shareJoint = $F("txtShareForJointBank" + bankNo);

            var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);

            $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));
        }
        else if (accounttype == "Pr" && tabName == "SE") {
            var consideredCtoForSePr = $F("cmbConsideredCtoForSEPrBank" + bankNo);
            var considerMarginForEsPr = $F("txtConsiderMarginForSEPrBank" + bankNo);
            var grossIncomeForSePr = parseFloat((parseFloat(consideredCtoForSePr) * parseFloat(considerMarginForEsPr)) / 100);
            $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncomeForSePr);
            $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncomeForSePr);

            var shareForSePr = $F("txtShareForSEPrBank" + bankNo);

            var netIncomeTotalForSePr = parseFloat((grossIncomeForSePr * parseFloat(shareForSePr)) / 100);

            $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotalForSePr.toFixed(2));
        }
        else if (accounttype == "Jt" && tabName == "SE") {
            var consideredCtoForSeJt = $F("cmbConsideredCtoForSEJtBank" + bankNo);
            var considerMarginForSeJt = $F("txtConsiderMarginForSEJtBank" + bankNo);
            var grossIncomeForSeJt = parseFloat((parseFloat(consideredCtoForSeJt) * parseFloat(considerMarginForSeJt)) / 100);
            $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncomeForSeJt);
            $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncomeForSeJt);

            var shareForSeJt = $F("txtShareForSEJtBank" + bankNo);

            var netIncomeTotalForSeJt = parseFloat((grossIncomeForSeJt * parseFloat(shareForSeJt)) / 100);

            $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalForSeJt.toFixed(2));
        }
        loanCalculatorHelPer.setBussinessManCalculation(bankNo, accounttype, tabName);
    },
    changeConsiderForOverDraftWithScb: function(bankNo, accountType, tabName) {
        var totalLimit = 0;
        var totalInterest = 0;
        if (accountType == "Pr" && tabName == "BM") {

            var tableBmPrBankDraftWithScb = $("tblOverDraftFacilitiesForPrBMWithScb" + bankNo);
            var rowCountPrBmWithScb = tableBmPrBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountPrBmWithScb - 1; i++) {
                var considerwithScbBmPr = $F("cmbConsider" + i + "ForBank" + bankNo);
                if (considerwithScbBmPr == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForBank' + bankNo));
                    }
                }
            }

            var tableBmPrBankDraftOffScb = $("tblOverDraftFacilitiesForPrBMWithScb" + bankNo);
            var rowCountPrBmOffScb = tableBmPrBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountPrBmOffScb - 1; i++) {
                var considerOffScbBmPr = $F("cmbConsider" + i + "ForOffScbBank" + bankNo);
                if (considerOffScbBmPr == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForOffSCBBank' + bankNo));
                    }
                }
            }
            $("txtTotallimitForOoverDraftBMPrBank" + bankNo).setValue(totalLimit);
            $("txtTotalInterestForOverDraftBMJtBank" + bankNo).setValue(totalInterest);

            var consideredCto = $F("cmbConsideredCtoForPrBank" + bankNo);
            var considerMargin = $F("txtConsiderMarginForPrimaryBank" + bankNo);
            var grossIncome = parseFloat((parseFloat(consideredCto) * parseFloat(considerMargin)) / 100);
            $("txtgrossincomeForPrimaryBank" + bankNo).setValue(grossIncome);
            var netIncome = grossIncome - totalInterest;
            $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(netIncome);

            var share = $F("txtShareForPrimaryBank" + bankNo);

            var netIncomeTotal = parseFloat((netIncome * parseFloat(share)) / 100);

            $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal.toFixed(2));

        }
        else if (accountType == "Jt" && tabName == "BM") {
            var tableBmJtBankDraftWithScb = $("tblOverDraftFacilitiesForPrBMWithScb" + bankNo);
            var rowCountJtBmWithScb = tableBmJtBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountJtBmWithScb - 1; i++) {
                var considerwithScbBmJt = $F("cmbInterest" + i + "ForJointBank" + bankNo);
                if (considerwithScbBmJt == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForJointBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForJointBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForJointBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForJointBank' + bankNo));
                    }
                }
            }

            var tableBmJtBankDraftOffScb = $("tblOverDraftFacilitiesForPrBMWithScb" + bankNo);
            var rowCountJtBmOffScb = tableBmJtBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountJtBmOffScb - 1; i++) {
                var considerOffScbBmJt = $F("cmbConsiderOffScbBM" + i + "ForJointBank" + bankNo);
                if (considerOffScbBmJt == "1") {
                    if (util.isFloat($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo));
                    }
                }
            }
            var considerMarginFinalForJoint = $F("txtConsiderMarginForPrimaryBank" + bankNo);
            var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);
            $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncomeForJoint);
            var netIncomeJt = grossIncomeForJoint - totalInterest;
            $("txtNetAfterPaymentForJointBank" + bankNo).setValue(netIncomeJt);

            var shareJoint = $F("txtShareForJointBank" + bankNo);

            var netIncomeTotalJoint = parseFloat((netIncomeJt * parseFloat(shareJoint)) / 100);

            $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));
        }
        else if (accountType == "Pr" && tabName == "SE") {
            var tableSePrBankDraftWithScb = $("tblOverDraftFacilitiesForPrSEWithScb" + bankNo);
            var rowCountPrSeWithScb = tableSePrBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountPrSeWithScb - 1; i++) {
                var considerwithScbSePr = $F("cmbConsider" + i + "WithScbForSEBank" + bankNo);
                if (considerwithScbSePr == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForSEBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForSEBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForSEBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForSEBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForSEBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForSEBank' + bankNo));
                    }
                }
            }

            var tableSePrBankDraftOffScb = $("tblOverDraftFacilitiesForPrSEOffScb" + bankNo);
            var rowCountPrSeOffScb = tableSePrBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountPrSeOffScb - 1; i++) {
                var considerOffScbSePr = $F("cmbConsider" + i + "ForSEOffScbBank" + bankNo);
                if (considerOffScbSePr == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo));
                    }
                }
            }
            var considerMarginFinalForSePr = $F("txtConsiderMarginForSEPrBank" + bankNo);
            var grossIncomeForSePr = parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinalForSePr) / 100);
            $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncomeForSePr);
            var netIncomeSePr = grossIncomeForSePr - totalInterest;
            $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(netIncomeSePr);

            var shareSepr = $F("txtShareForSEPrBank" + bankNo);

            var netIncomeTotalSePr = parseFloat((netIncomeSePr * parseFloat(shareSepr)) / 100);

            $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotalSePr.toFixed(2));
        }
        else if (accountType == "Jt" && tabName == "SE") {
            var tableSeJtBankDraftWithScb = $("tblOverDraftFacilitiesForJtSEWithScb" + bankNo);
            var rowCountJtSeWithScb = tableSeJtBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountJtSeWithScb - 1; i++) {
                var considerwithScbSeJt = $F("cmbConsider" + i + "WithScbForSEJtBank" + bankNo);
                if (considerwithScbSeJt == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo));
                    }
                }
            }

            var tableSeJtBankDraftOffScb = $("tblOverDraftFacilitiesForJtSEOffScbBank" + bankNo);
            var rowCountJtSeOffScb = tableSeJtBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountJtSeOffScb - 1; i++) {
                var considerOffScbSeJt = $F("cmbConsider" + i + "ForSEJtOffScbBank" + bankNo);
                if (considerOffScbSeJt == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo));
                    }
                }
            }
            var considerMarginFinalForSeJt = $F("txtConsiderMarginForSEJtBank" + bankNo);
            var grossIncomeForSeJt = parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForSeJt) / 100);
            $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncomeForSeJt);
            var netIncomeSeJt = grossIncomeForSeJt - totalInterest;
            $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(netIncomeSeJt);

            var shareSeJt = $F("txtShareForSEJtBank" + bankNo);

            var netIncomeTotalSeJt = parseFloat((netIncomeSeJt * parseFloat(shareSeJt)) / 100);

            $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalSeJt.toFixed(2));
        }
    },

    bussinessManTabOverDraftwithSCB: function(objAutoLoanFacilityList, idField, accountType, tabName) {
        var interst = parseFloat(parseFloat((objAutoLoanFacilityList.PresentLimit * objAutoLoanFacilityList.InterestRate)) / 12);
        if (accountType == "Pr" && tabName == "BM") {

            $('txtLimt' + idField + 'WithScbForBank1').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForBank1').setValue(interst);

            $('txtLimt' + idField + 'WithScbForBank2').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForBank2').setValue(interst);

            $('txtLimt' + idField + 'WithScbForBank3').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForBank3').setValue(interst);

            $('txtLimt' + idField + 'WithScbForBank4').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForBank4').setValue(interst);

            $('txtLimt' + idField + 'WithScbForBank5').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForBank5').setValue(interst);

            $('txtLimt' + idField + 'WithScbForBank6').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForBank6').setValue(interst);
        }
        else if (accountType == "Jt" && tabName == "BM") {
            $('txtLimit' + idField + 'ForJointBank1').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterest' + idField + 'ForJointBank1').setValue(interst);

            $('txtLimit' + idField + 'ForJointBank2').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterest' + idField + 'ForJointBank2').setValue(interst);

            $('txtLimit' + idField + 'ForJointBank3').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterest' + idField + 'ForJointBank3').setValue(interst);

            $('txtLimit' + idField + 'ForJointBank4').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterest' + idField + 'ForJointBank4').setValue(interst);

            $('txtLimit' + idField + 'ForJointBank5').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterest' + idField + 'ForJointBank5').setValue(interst);

            $('txtLimit' + idField + 'ForJointBank6').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterest' + idField + 'ForJointBank6').setValue(interst);
        }
        else if (accountType == "Pr" && tabName == "SE") {
            $('txtLimt' + idField + 'WithScbForSEBank1').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEBank1').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEBank2').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEBank2').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEBank3').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEBank3').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEBank4').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEBank4').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEBank5').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEBank5').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEBank6').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEBank6').setValue(interst);
        }
        else if (accountType == "Jt" && tabName == "SE") {
            $('txtLimt' + idField + 'WithScbForSEJtBank1').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEJtBank1').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEJtBank2').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEJtBank2').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEJtBank3').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEJtBank3').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEJtBank4').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEJtBank4').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEJtBank5').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEJtBank5').setValue(interst);

            $('txtLimt' + idField + 'WithScbForSEJtBank6').setValue(objAutoLoanFacilityList.PresentLimit);
            $('txtInterst' + idField + 'WithScbForSEJtBank6').setValue(interst);
        }
    },
    bussinessManTabOverDraftOffSCB: function(objAutoLoanFacilityList, idField, accountType, tabName) {

        var interst = parseFloat(parseFloat((objAutoLoanFacilityList.OriginalLimit * objAutoLoanFacilityList.InterestRate)) / 12);
        if (accountType == "Pr" && tabName == "BM") {

            $('cmbBankName' + idField + 'ForBussinessTabPrBank1').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForOffSCBBank1').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForOffSCBBank1').setValue(interst);
            $('cmbConsider' + idField + 'ForOffScbBank1').setValue('0');

            $('cmbBankName' + idField + 'ForBussinessTabPrBank2').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForOffSCBBank2').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForOffSCBBank2').setValue(interst);
            $('cmbConsider' + idField + 'ForOffScbBank2').setValue('0');

            $('cmbBankName' + idField + 'ForBussinessTabPrBank3').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForOffSCBBank3').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForOffSCBBank3').setValue(interst);
            $('cmbConsider' + idField + 'ForOffScbBank3').setValue('0');

            $('cmbBankName' + idField + 'ForBussinessTabPrBank4').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForOffSCBBank4').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForOffSCBBank4').setValue(interst);
            $('cmbConsider' + idField + 'ForOffScbBank4').setValue('0');

            $('cmbBankName' + idField + 'ForBussinessTabPrBank5').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForOffSCBBank5').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForOffSCBBank5').setValue(interst);
            $('cmbConsider' + idField + 'ForOffScbBank5').setValue('0');

            $('cmbBankName' + idField + 'ForBussinessTabPrBank6').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForOffSCBBank6').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForOffSCBBank6').setValue(interst);
            $('cmbConsider' + idField + 'ForOffScbBank6').setValue('0');
        }
        else if (accountType == "Jt" && tabName == "BM") {
            $('cmbBankName' + idField + 'ForBussinessTabJtBank1').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimitOffScb' + idField + 'ForJointBank1').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterestOffSCB' + idField + 'ForJointBank1').setValue(interst);
            $('cmbConsiderOffScbBM' + idField + 'ForJointBank1').setValue('0');

            $('cmbBankName' + idField + 'ForBussinessTabJtBank2').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimitOffScb' + idField + 'ForJointBank2').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterestOffSCB' + idField + 'ForJointBank2').setValue(interst);
            $('cmbConsiderOffScbBM' + idField + 'ForJointBank2').setValue('0');


            $('cmbBankName' + idField + 'ForBussinessTabJtBank3').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimitOffScb' + idField + 'ForJointBank3').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterestOffSCB' + idField + 'ForJointBank3').setValue(interst);
            $('cmbConsiderOffScbBM' + idField + 'ForJointBank3').setValue('0');


            $('cmbBankName' + idField + 'ForBussinessTabJtBank4').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimitOffScb' + idField + 'ForJointBank4').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterestOffSCB' + idField + 'ForJointBank4').setValue(interst);
            $('cmbConsiderOffScbBM' + idField + 'ForJointBank4').setValue('0');


            $('cmbBankName' + idField + 'ForBussinessTabJtBank5').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimitOffScb' + idField + 'ForJointBank5').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterestOffSCB' + idField + 'ForJointBank5').setValue(interst);
            $('cmbConsiderOffScbBM' + idField + 'ForJointBank5').setValue('0');


            $('cmbBankName' + idField + 'ForBussinessTabJtBank6').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimitOffScb' + idField + 'ForJointBank6').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterestOffSCB' + idField + 'ForJointBank6').setValue(interst);
            $('cmbConsiderOffScbBM' + idField + 'ForJointBank6').setValue('0');

        }
        else if (accountType == "Pr" && tabName == "SE") {
            $('cmbBankName' + idField + 'ForSEPrBank1').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEOffSCBBank1').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEOffSCBBank1').setValue(interst);
            $('cmbConsider' + idField + 'ForSEOffScbBank1').setValue('0');


            $('cmbBankName' + idField + 'ForSEPrBank2').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEOffSCBBank2').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEOffSCBBank2').setValue(interst);
            $('cmbConsider' + idField + 'ForSEOffScbBank2').setValue('0');

            $('cmbBankName' + idField + 'ForSEPrBank3').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEOffSCBBank3').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEOffSCBBank3').setValue(interst);
            $('cmbConsider' + idField + 'ForSEOffScbBank3').setValue('0');

            $('cmbBankName' + idField + 'ForSEPrBank4').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEOffSCBBank4').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEOffSCBBank4').setValue(interst);
            $('cmbConsider' + idField + 'ForSEOffScbBank4').setValue('0');

            $('cmbBankName' + idField + 'ForSEPrBank5').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEOffSCBBank5').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEOffSCBBank5').setValue(interst);
            $('cmbConsider' + idField + 'ForSEOffScbBank5').setValue('0');

            $('cmbBankName' + idField + 'ForSEPrBank6').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEOffSCBBank6').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEOffSCBBank6').setValue(interst);
            $('cmbConsider' + idField + 'ForSEOffScbBank6').setValue('0');
        }
        else if (accountType == "Jt" && tabName == "SE") {
            $('cmbBankName' + idField + 'ForSEJtBank1').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEJtOffSCBBank1').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEJtOffSCBBank1').setValue(interst);
            $('cmbConsider' + idField + 'ForSEJtOffScbBank1').setValue('0');

            $('cmbBankName' + idField + 'ForSEJtBank2').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEJtOffSCBBank2').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEJtOffSCBBank2').setValue(interst);
            $('cmbConsider' + idField + 'ForSEJtOffScbBank2').setValue('0');

            $('cmbBankName' + idField + 'ForSEJtBank3').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEJtOffSCBBank3').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEJtOffSCBBank3').setValue(interst);
            $('cmbConsider' + idField + 'ForSEJtOffScbBank3').setValue('0');

            $('cmbBankName' + idField + 'ForSEJtBank4').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEJtOffSCBBank4').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEJtOffSCBBank4').setValue(interst);
            $('cmbConsider' + idField + 'ForSEJtOffScbBank4').setValue('0');

            $('cmbBankName' + idField + 'ForSEJtBank5').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEJtOffSCBBank5').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEJtOffSCBBank5').setValue(interst);
            $('cmbConsider' + idField + 'ForSEJtOffScbBank5').setValue('0');

            $('cmbBankName' + idField + 'ForSEJtBank6').setValue(objAutoLoanFacilityList.BankID);
            $('txtLimit' + idField + 'ForSEJtOffSCBBank6').setValue(objAutoLoanFacilityList.OriginalLimit);
            $('txtInterest' + idField + 'ForSEJtOffSCBBank6').setValue(interst);
            $('cmbConsider' + idField + 'ForSEJtOffScbBank6').setValue('0');
        }
    },

    prevreplaceMentOnSCB: function(objAutoLoanFacilityList, idfieldforExposerForTermsloans, accountType, tabName) {
        if (accountType == "Pr" && tabName == "BM") {
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForPrimaryBank1').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForPrimaryBank1').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForPrimaryBank2').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForPrimaryBank2').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForPrimaryBank3').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForPrimaryBank3').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForPrimaryBank4').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForPrimaryBank4').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForPrimaryBank5').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForPrimaryBank5').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForPrimaryBank6').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForPrimaryBank6').setValue('0');
        }
        else if (accountType == "Jt" && tabName == "BM") {
            $('txtEmiOnScb' + idfieldforExposerForTermsloans + 'ForJointBank1').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsiderOnScb' + idfieldforExposerForTermsloans + 'ForJointBank1').setValue('0');
            $('txtEmiOnScb' + idfieldforExposerForTermsloans + 'ForJointBank2').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsiderOnScb' + idfieldforExposerForTermsloans + 'ForJointBank2').setValue('0');
            $('txtEmiOnScb' + idfieldforExposerForTermsloans + 'ForJointBank3').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsiderOnScb' + idfieldforExposerForTermsloans + 'ForJointBank3').setValue('0');
            $('txtEmiOnScb' + idfieldforExposerForTermsloans + 'ForJointBank4').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsiderOnScb' + idfieldforExposerForTermsloans + 'ForJointBank4').setValue('0');
            $('txtEmiOnScb' + idfieldforExposerForTermsloans + 'ForJointBank5').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsiderOnScb' + idfieldforExposerForTermsloans + 'ForJointBank5').setValue('0');
            $('txtEmiOnScb' + idfieldforExposerForTermsloans + 'ForJointBank6').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsiderOnScb' + idfieldforExposerForTermsloans + 'ForJointBank6').setValue('0');
        }
        else if (accountType == "Pr" && tabName == "SE") {
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEPrBank1').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEPrBank1').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEPrBank2').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEPrBank2').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEPrBank3').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEPrBank3').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEPrBank4').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEPrBank4').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEPrBank5').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEPrBank5').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEPrBank6').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEPrBank6').setValue('0');
        }
        else if (accountType == "Jt" && tabName == "SE") {

            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEJtBank1').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEJtBank1').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEJtBank2').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEJtBank2').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEJtBank3').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEJtBank3').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEJtBank4').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEJtBank4').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEJtBank5').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEJtBank5').setValue('0');
            $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEJtBank6').setValue(objAutoLoanFacilityList.PresentEMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEJtBank6').setValue('0');
        }
    },
    prevreplaceMentOffSCB: function(objAutoLoanOffScbFacilityList, idfieldforExposerForTermsloans, accountType, tabName) {
        var instituteName = $('cmbFinancialInstitutionForOffSCB' + idfieldforExposerForTermsloans).options[$('cmbFinancialInstitutionForOffSCB' + idfieldforExposerForTermsloans).selectedIndex].text;
        if (accountType == "Pr" && tabName == "BM") {
            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank1').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMPr1').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank1').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank1').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank2').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMPr2').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank2').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank2').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank3').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMPr3').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank3').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank3').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank4').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMPr4').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank4').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank4').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank5').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMPr5').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank5').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank5').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank6').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMPr6').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank6').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank6').setValue('0');
        }
        else if (accountType == "Jt" && tabName == "BM") {
            $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank1').setValue(instituteName); //txtInstituteOffScb
            $('txtBank' + idfieldforExposerForTermsloans + 'BMJt1').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank1').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank1').setValue('0');

            $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank2').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMJt2').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank2').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank2').setValue('0');

            $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank3').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMJt3').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank3').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank3').setValue('0');

            $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank4').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMJt4').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank4').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank4').setValue('0');

            $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank5').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMJt5').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank5').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank5').setValue('0');

            $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank6').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'BMJt6').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank6').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank6').setValue('0');

        }
        else if (accountType == "Pr" && tabName == "SE") {
            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank1').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEPr1').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank1').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank1').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank2').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEPr2').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank2').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank2').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank3').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEPr3').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank3').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank3').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank4').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEPr4').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank4').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank4').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank5').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEPr5').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank5').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank5').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank6').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEPr6').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank6').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank6').setValue('0');
        }
        else if (accountType == "Jt" && tabName == "SE") {

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank1').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEJt1').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank1').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank1').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank2').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEJt2').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank2').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank2').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank3').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEJt3').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank3').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank3').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank4').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEJt4').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank4').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank4').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank5').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEJt5').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank5').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank5').setValue('0');

            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank6').setValue(instituteName);
            $('txtBank' + idfieldforExposerForTermsloans + 'SEJt6').setValue(objAutoLoanOffScbFacilityList.BankID);
            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank6').setValue(objAutoLoanOffScbFacilityList.EMI);
            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank6').setValue('0');
        }
    },

    //Self Employed Tab
    populateAverageTotalForSelfEmployedBankStatement: function(bankId, accountNumber, typeName, autoBankStatementAvgTotalList, bankNoCount) {

        var j = 0;
        var k = 0;
        var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var averageBalanceFor12Month = 0;
        var averageBalanceFor6Month = 0;
        var sixMonthArrayForCto = [];
        var sixMonthArrayForAverageBalance = [];
        if (typeName == "Primary") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 1) {
                    j += 1;
                    var d = new Date(autoBankStatementAvgTotalList[i].Month);
                    var newDate = monthNames[d.getMonth()] + "-" + d.getFullYear();
                    $("hdn" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtMonth" + j + "ForSEPrBank" + bankNoCount).setValue(newDate);
                    $("txtCreditTurnOver" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAverageBalance" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                }
            }
            $("txtTotalCTOForSEPr12MonthBank" + bankNoCount).setValue(totalCtofor12Month);

            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });

            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
                if (sixMonthArrayForCto[s] != "") {
                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
                }
            }
            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
                if (sixMonthArrayForAverageBalance[s] != "") {
                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
                }
            }

            $("txtTotalCTOForSEPr6MonthBank" + bankNoCount).setValue(totalCtofor6Month);
            var avergTotalCto12Month = (totalCtofor12Month / 12);
            var avergTotalCto6Month = (totalCtofor6Month / 6);
            $("txtAverageCTOForSEPr12MonthBank" + bankNoCount).setValue(avergTotalCto12Month.toFixed(2));
            $("txtAverageCTOForSEPr6MonthBank" + bankNoCount).setValue(avergTotalCto6Month.toFixed(2));

            $("txtAverageBalanceForSEPr12MonthBank" + bankNoCount).setValue(averageBalanceFor12Month);
            $("txtAverageBalanceForSEPr6MonthBank" + bankNoCount).setValue(averageBalanceFor6Month);

            var linkCto = "<option value='" + avergTotalCto12Month.toFixed(2) + "'>" + avergTotalCto12Month.toFixed(2) + "</option><option value='" + avergTotalCto6Month.toFixed(2) + "'>" + avergTotalCto6Month.toFixed(2) + "</option>";
            var linkAvg = "<option value='" + averageBalanceFor12Month + "'>" + averageBalanceFor12Month + "</option><option value='" + averageBalanceFor6Month + "'>" + averageBalanceFor6Month + "</option>";

            $("cmbConsideredCtoForSEPrBank" + bankNoCount).update(linkCto);
            $("cmbConsideredBalanceForSEPrBank" + bankNoCount).update(linkAvg);

            $("cmbConsideredCtoForSEPrBank" + bankNoCount).setValue(avergTotalCto12Month.toFixed(2)); //E35
            $("cmbConsideredBalanceForSEPrBank" + bankNoCount).setValue(averageBalanceFor12Month);
            $("txtConsiderMarginForSEPrBank" + bankNoCount).setValue('3'); //E37

            //=IF(P31="FT",P37,E35*E37)
            var grossincome = parseFloat((avergTotalCto12Month * 3) / 100);
            $("txtgrossincomeForSEPrBank" + bankNoCount).setValue(grossincome.toFixed(2));
            $("txtNetAfterPaymentForSEPrBank" + bankNoCount).setValue(grossincome.toFixed(2));
            var share = $F("txtShareForSEPrBank" + bankNoCount);

            var netIncomeTotal = parseFloat((grossincome * parseFloat(share)) / 100);

            $("txtNetincomeForSEPrBank" + bankNoCount).setValue(netIncomeTotal.toFixed(2));

            loanCalculatorHelPer.setBussinessManCalculation(bankNoCount, 'Pr', 'SE');

        }
        else if (typeName == "Joint") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 2) {
                    k += 1;
                    $("txtMonth" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Month);
                    $("hdn" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtCreditTurnOver" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAverageBalance" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                    //                    if (j < 7) {
                    //                        totalCtofor6Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    //                        averageBalanceFor6Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    //                    }
                }
            }

            $("txtTotalCTOForSEJt12MonthBank" + bankNoCount).setValue(totalCtofor12Month);


            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });
            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
                if (sixMonthArrayForCto[s] != "") {
                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
                }
            }
            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
                if (sixMonthArrayForAverageBalance[s] != "") {
                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
                }
            }


            $("txtTotalCTOForSEJt6MonthBank" + bankNoCount).setValue(totalCtofor6Month);
            var avergTotalCto12Monthjoint = (totalCtofor12Month / 12);
            var avergTotalCto6Monthjoint = (totalCtofor6Month / 6);
            $("txtAverageCTOForSEJt12MonthBank" + bankNoCount).setValue(avergTotalCto12Monthjoint.toFixed(2));
            $("txtAverageCTOForSEJt6MonthBank" + bankNoCount).setValue(avergTotalCto6Monthjoint.toFixed(2));

            $("txtAverageBalanceForSEJt12MonthBank" + bankNoCount).setValue(averageBalanceFor12Month);
            $("txtAverageBalanceForSEJt6MonthBank" + bankNoCount).setValue(averageBalanceFor6Month);


            var linkCtoJoint = "<option value='" + avergTotalCto12Monthjoint.toFixed(2) + "'>" + avergTotalCto12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalCto6Monthjoint.toFixed(2) + "'>" + avergTotalCto6Monthjoint.toFixed(2) + "</option>";
            var linkAvgJoint = "<option value='" + averageBalanceFor12Month + "'>" + averageBalanceFor12Month + "</option><option value='" + averageBalanceFor6Month + "'>" + averageBalanceFor6Month + "</option>";

            $("cmbConsideredCtoForSEJtBank" + bankNoCount).update(linkCtoJoint);
            $("cmbConsideredBalanceForSEJtBank" + bankNoCount).update(linkAvgJoint);
            $("cmbConsideredCtoForSEJtBank" + bankNoCount).setValue(avergTotalCto12Monthjoint.toFixed(2));
            $("cmbConsideredBalanceForSEJtBank" + bankNoCount).setValue(averageBalanceFor12Month);
            $("txtConsiderMarginForSEJtBank" + bankNoCount).setValue("3");



            //=IF(P31="FT",P37,E35*E37)
            var grossJtincome = parseFloat((avergTotalCto12Monthjoint * 3) / 100);
            $("txtgrossincomeForSEJtBank" + bankNoCount).setValue(grossJtincome.toFixed(2));
            $("txtNetAfterPaymentForSEJtBank" + bankNoCount).setValue(grossJtincome.toFixed(2));
            var shareJt = $F("txtShareForSEJtBank" + bankNoCount);

            var netIncomeTotalJt = parseFloat((grossJtincome * parseFloat(shareJt)) / 100);

            $("txtNetincomeForSEJtBank" + bankNoCount).setValue(netIncomeTotalJt.toFixed(2));
            loanCalculatorHelPer.setBussinessManCalculation(bankNoCount, 'Jt', 'SE');
        }

    },
    shobankStatementTabForSelfEmployed: function(prCount, jtCount) {

        for (var i = 1; i <= 6 - prCount; i++) {
            var identityRow = prCount + i;
            $('prBankStatementSelfEmployed' + identityRow).style.display = 'none';

        }
        for (var j = 1; j <= 6 - jtCount; j++) {
            var identityRowForjt = jtCount + j;
            $('jtBankStatementSelfEmployed' + identityRowForjt).style.display = 'none';
        }
    },
    changeCreditTurnoverForPrApplicantSelfEmployed: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField)) && !util.isNegativeNumber($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var sixMonthArrayForCto = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo))) {
                totalCtofor12Month += parseFloat($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo));
            }
            if (util.isFloat($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo));
            }
            else if (util.isNegativeNumber($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo));
            }
            sixMonthArrayForCto.push(totalCtofor12Month);
        }
        $("txtTotalCTOForSEPr12MonthBank" + bankNo).setValue(totalCtofor12Month);
        sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            if (sixMonthArrayForCto[s] != "") {
                totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            }
        }
        $("txtTotalCTOForSEPr6MonthBank" + bankNo).setValue(totalCtofor6Month);
        var avergTotalCto12MonthPr = (totalCtofor12Month / 12);
        var avergTotalCto6MonthPr = (totalCtofor6Month / 6);
        $("txtAverageCTOForSEPr12MonthBank" + bankNo).setValue(avergTotalCto12MonthPr.toFixed(2));
        $("txtAverageCTOForSEPr6MonthBank" + bankNo).setValue(avergTotalCto6MonthPr.toFixed(2));

        var linkCto = "<option value='" + avergTotalCto12MonthPr.toFixed(2) + "'>" + avergTotalCto12MonthPr.toFixed(2) + "</option><option value='" + avergTotalCto6MonthPr.toFixed(2) + "'>" + avergTotalCto6MonthPr.toFixed(2) + "</option>";

        $("cmbConsideredCtoForSEPrBank" + bankNo).update(linkCto);
        $("cmbConsideredCtoForSEPrBank" + bankNo).setValue(avergTotalCto12MonthPr.toFixed(2));

        var considerMarginFinal = $F("txtConsiderMarginForSEPrBank" + bankNo);
        var grossIncome = parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100);
        $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncome);
        $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncome);

        var share = $F("txtShareForSEPrBank" + bankNo);

        var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

        $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotal.toFixed(2));


    },
    changeAverageBalanceForPrApplicantSelfEmployed: function(bankNo, tableName, identityField) {
        if (!util.isFloat($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalAvgfor12Month = 0;
        var totalAvgfor6Month = 0;
        var sixMonthArrayForAvg = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo))) {
                totalAvgfor12Month += parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo));
            }
            sixMonthArrayForAvg.push(totalAvgfor12Month);
        }
        sixMonthArrayForAvg = sixMonthArrayForAvg.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForAvg.length - 6; s++) {
            if (sixMonthArrayForAvg[s] != "") {
                totalAvgfor6Month += parseFloat(sixMonthArrayForAvg[s]);
            }
        }
        var avergTotalAvg12Monthjoint = (totalAvgfor12Month / 12);
        var avergTotalAvg6Monthjoint = (totalAvgfor6Month / 6);
        $("txtAverageCTOForSEPr12MonthBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));
        $("txtAverageCTOForSEPr6MonthBank" + bankNo).setValue(avergTotalAvg6Monthjoint.toFixed(2));

        var incomeAssement = $F("cmbIncomeAssessmentForSEPrBank" + bankNo);
        if (incomeAssement == "1") {

            analyzeLoanApplicationHelper.calculateAverageBalanceForSelfEmployed(bankNo, "Pr");

        }
        var linkAvg = "<option value='" + avergTotalAvg12Monthjoint.toFixed(2) + "'>" + avergTotalAvg12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalAvg6Monthjoint.toFixed(2) + "'>" + avergTotalAvg6Monthjoint.toFixed(2) + "</option>";
        $("cmbConsideredBalanceForSEPrBank" + bankNo).update(linkAvg);
        $("cmbConsideredBalanceForSEPrBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));

    },
    changeCreditTurnoverForJtApplicantSelfEmployed: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField)) && !util.isNegativeNumber($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var sixMonthArrayForCto = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo))) {
                totalCtofor12Month += parseFloat($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo));
            }
            if (util.isFloat($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo));
            }
            else if (util.isNegativeNumber($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo));
            }
            sixMonthArrayForCto.push(totalCtofor12Month);
        }
        $("txtTotalCTOForSEJt12MonthBank" + bankNo).setValue(totalCtofor12Month);
        sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            if (sixMonthArrayForCto[s] != "") {
                totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            }
        }
        $("txtTotalCTOForSEJt6MonthBank" + bankNo).setValue(totalCtofor6Month);
        var avergTotalCto12Monthjoint = (totalCtofor12Month / 12);
        var avergTotalCto6Monthjoint = (totalCtofor6Month / 6);
        $("txtAverageCTOForSEJt12MonthBank" + bankNo).setValue(avergTotalCto12Monthjoint.toFixed(2));
        $("txtAverageCTOForSEJt6MonthBank" + bankNo).setValue(avergTotalCto6Monthjoint.toFixed(2));


        var linkCto = "<option value='" + avergTotalCto12Monthjoint.toFixed(2) + "'>" + avergTotalCto12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalCto6Monthjoint.toFixed(2) + "'>" + avergTotalCto6Monthjoint.toFixed(2) + "</option>";
        $("cmbConsideredCtoForSEJtBank" + bankNo).update(linkCto);
        $("cmbConsideredCtoForSEJtBank" + bankNo).setValue(avergTotalCto12Monthjoint.toFixed(2));

        var considerMarginFinalForJoint = $F("txtConsiderMarginForSEJtBank" + bankNo);

        var grossIncomeForJoint = parseFloat(parseFloat(avergTotalCto12Monthjoint * considerMarginFinalForJoint) / 100);
        $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncomeForJoint);
        $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncomeForJoint);

        var shareJoint = $F("txtShareForSEJtBank" + bankNo);

        var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);

        $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));



    },

    changeAverageBalanceForJtApplicantSelfEmployed: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalAvgfor12Month = 0;
        var totalAvgfor6Month = 0;
        var sixMonthArrayForAvg = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo))) {
                totalAvgfor12Month += parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo));
            }
            sixMonthArrayForAvg.push(totalAvgfor12Month);
        }
        sixMonthArrayForAvg = sixMonthArrayForAvg.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForAvg.length - 6; s++) {
            if (sixMonthArrayForAvg[s] != "") {
                totalAvgfor6Month += parseFloat(sixMonthArrayForAvg[s]);
            }
        }
        var avergTotalAvg12Monthjoint = (totalAvgfor12Month / 12);
        var avergTotalAvg6Monthjoint = (totalAvgfor6Month / 6);
        $("txtAverageBalanceForSEJt12MonthBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));
        $("txtAverageBalanceForSEJt6MonthBank" + bankNo).setValue(avergTotalAvg6Monthjoint.toFixed(2));

        var incomeAssement = $F("cmbIncomeAssessmentForSEJtBank" + bankNo);
        if (incomeAssement == "1") {
            analyzeLoanApplicationHelper.calculateAverageBalanceForSelfEmployed(bankNo, "Jt");
        }



        var linkAvg = "<option value='" + avergTotalAvg12Monthjoint + "'>" + avergTotalAvg12Monthjoint + "</option><option value='" + avergTotalAvg6Monthjoint + "'>" + avergTotalAvg6Monthjoint + "</option>";

        $("cmbConsideredBalanceForSEJtBank" + bankNo).update(linkAvg);
        $("cmbConsideredBalanceForSEJtBank" + bankNo).setValue(avergTotalAvg12Monthjoint);

    },
    calculateAverageBalanceForSelfEmployed: function(bankNo, accountType) {
        var firstst9MonthTotal = 0;
        var first6MonthTotal = 0;
        var last6Month = 0;
        var last3month = 0;
        var j = 0;
        var monthArray = [];




        if (accountType == "Pr") {
            //var accountnumber = $F("txtAccountNumberBank" + bankNo);
            //var bankId = $F("cmbBankNameForBank" + bankNo + "IncomeGenerator");


            var tablePrAccount = $("tblPrBankStatementForSEBank" + bankNo);
            var rowCount = tablePrAccount.rows.length;


            //for (var i = 0; i < Page.bankStatementArray.length; i++) {
            for (var i = 1; i <= rowCount - 1; i++) {
                //if (bankId == Page.bankStatementArray[i].BankId && accountnumber == Page.bankStatementArray[i].AccountNumber && Page.bankStatementArray[i].Type == 1) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    //last3month += monthArray[k].AVGTotal;
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    //last6Month += monthArray[k].AVGTotal;
                    last6Month += monthArray[k];
                }
            }
            //=IF(M40>=M37,"25%",IF(M40<=M37,"15%","-"))
            //=IF(M39>=M38,"33%",IF(M39<=M38,"20%","-"))
            $("txt1st9monthForSEPrBank" + bankNo).setValue(firstst9MonthTotal); //m37
            $("txt1st6MonthForSEPrBank" + bankNo).setValue(first6MonthTotal); //m38
            $("txtLast6monthForSEPrBank" + bankNo).setValue(last6Month); //m39
            $("txtLast3monthForSEPrBank" + bankNo).setValue(last3month); //m40

            //Consider Margin
            var considerMargin1 = 0;
            var considerMargin2 = 0;
            if (last3month >= firstst9MonthTotal) {
                considerMargin1 = "25";
            }
            else {
                considerMargin1 = "15";
            }

            if (last6Month >= first6MonthTotal) {
                considerMargin2 = "33";
            }
            else {
                considerMargin2 = "20";
            }

            var considerMarginFinal = 0;
            if (considerMargin1 <= considerMargin2) {
                considerMarginFinal = considerMargin1;
            }
            else {
                considerMarginFinal = considerMargin2;
            }


            $("txtConsiderednarginForSEPrBank" + bankNo).setValue(considerMarginFinal);
            $("txtConsiderMarginForSEPrBank" + bankNo).setValue(considerMarginFinal);

            var grossIncome = parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinal) / 100);
            $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncome);
            $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncome);

            var share = $F("txtShareForSEPrBank" + bankNo);

            var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

            $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotal.toFixed(2));

        }
        else if (accountType == "Jt") {


            //            var accountnumberForJoint = $F("txtAccountNumber1ForJointBank" + bankNo);
            //            var bankIdForJoint = $F("cmbBankName1ForJointBank" + bankNo);


            var tableJtAccount = $("tblJtBankStatementForSEBank" + bankNo);
            var rowCountJt = tableJtAccount.rows.length;
            //for (var i = 0; i < Page.bankStatementArray.length; i++) {
            for (var i = 1; i <= rowCountJt - 1; i++) {
                //if (bankIdForJoint == Page.bankStatementArray[i].BankId && accountnumberForJoint == Page.bankStatementArray[i].AccountNumber && Page.bankStatementArray[i].Type == 2) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txt1st9monthForSEJtBank" + bankNo).setValue(firstst9MonthTotal);
            $("txt1st6MonthForSEJtBank" + bankNo).setValue(first6MonthTotal);
            $("txtLast6monthForSEJtBank" + bankNo).setValue(last6Month);
            $("txtLast3monthForSEJtBank" + bankNo).setValue(last3month);

            //Consider Margin
            var considerMarginForJoint1 = 0;
            var considerMarginForJoint2 = 0;
            if (last3month >= firstst9MonthTotal) {
                considerMarginForJoint1 = "25";
            }
            else {
                considerMarginForJoint1 = "15";
            }

            if (last6Month >= first6MonthTotal) {
                considerMarginForJoint2 = "33";
            }
            else {
                considerMarginForJoint2 = "20";
            }

            var considerMarginFinalForJoint = 0;
            if (considerMarginForJoint1 <= considerMarginForJoint2) {
                considerMarginFinalForJoint = considerMarginForJoint1;
            }
            else {
                considerMarginFinalForJoint = considerMarginForJoint2;
            }
            $("txtConsiderednarginForSEJtBank" + bankNo).setValue(considerMarginFinalForJoint);
            $("txtConsiderMarginForSEJtBank" + bankNo).setValue(considerMarginFinalForJoint);

            var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);
            $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncomeForJoint);
            $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncomeForJoint);

            var shareJoint = $F("txtShareForSEJtBank" + bankNo);

            var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);

            $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));

        }
        loanCalculatorHelPer.setBussinessManCalculation(bankNo, accountType, 'SE');
    },
    changeIncomeAssementForSelfEmployed: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEPrBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            $("txtIncomeAssessmentCodeForSEPrBank" + bankNo).setValue(incomeAssBuss.INAM_METHODCODE);
            if (incomeAssBuss.INAM_METHODCODE == "FT") {
                var averageBalancePr = false;

                for (var i = 0; i < Page.isAverageBalancePrimaryArray.length; i++) {
                    if (Page.isAverageBalancePrimaryArray[i].BankNo == bankNo && Page.isAverageBalancePrimaryArray[i].Accounttype == accountType && Page.isAverageBalancePrimaryArray[i].TabName == "Se") {
                        averageBalancePr = true;
                        break;
                    }
                }
                if (averageBalancePr == false) {
                    $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'none';
                    $('divAverageBalanceForSEPr' + bankNo).style.display = 'block';
                    analyzeLoanApplicationHelper.calculateAverageBalanceForSelfEmployed(bankNo, accountType);
                }
                else {
                    alert("Average Balance is not applicable");
                }

            }
            else if (incomeAssBuss.INAM_METHODCODE == "FF") {
                $('divAverageBalanceForSEPr' + bankNo).style.display = 'none';
                $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'block';
            }
            else {
                $('divAverageBalanceForSEPr' + bankNo).style.display = 'none';
                $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'none';
            }



        }
        else if (accountType == "Jt") {

            var incomeAssIdForBussJoint = $F("cmbIncomeAssessmentForSEJtBank" + bankNo);
            var incomeAssBussJoint = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBussJoint);
            $("txtIncomeAssessmentCodeForSEJtBank" + bankNo).setValue(incomeAssBussJoint.INAM_METHODCODE);
            if (incomeAssBussJoint.INAM_METHODCODE == "FT") {
                var averageBalance = false;

                for (var i = 0; i < Page.isAverageBalanceJointArray.length; i++) {
                    if (Page.isAverageBalanceJointArray[i].BankNo == bankNo && Page.isAverageBalanceJointArray[i].Accounttype == accountType && Page.isAverageBalanceJointArray[i].TabName == "Se") {
                        averageBalance = true;
                        break;
                    }
                }

                if (averageBalance == false) {
                    $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'none';
                    $('divAverageBalanceForSEJt' + bankNo).style.display = 'block';
                    analyzeLoanApplicationHelper.calculateAverageBalanceForSelfEmployed(bankNo, accountType);
                }
                else {
                    alert("Average Balance is not applicable");
                }
            }
            else if (incomeAssBussJoint.INAM_METHODCODE == "FF") {
                $('divAverageBalanceForSEJt' + bankNo).style.display = 'none';
                $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'block';
            }
            else {
                $('divAverageBalanceForSEJt' + bankNo).style.display = 'none';
                $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'none';
            }
        }
        loanCalculatorHelPer.setBussinessManCalculation(bankNo, accountType, 'SE');
    },
    changeDbrLevel: function() {
        var dbrLevelid = $F("cmbDBRLevel");
        var appropriateDbr = dbrLevel[dbrLevelid];
        $("txtAppropriateDbr").setValue(appropriateDbr);

        var lbl = parseInt($F("cmbLabelForDlaLimit"));
        if (parseInt(dbrLevelid) > lbl) {
            $("cmbLabelForDlaLimit").setValue(dbrLevelid);
            var approverId = $F("cmbApprovedby");
            if (approverId != "-1") {
                analystfinalizationHelper.GetApproverLimitByUserId(approverId);
            }
        }
        else {
            analyzeLoanApplicationHelper.setagewiseLebel();
        }

    },
    changeConsideredDbr: function() {

        var consideredDbr = $F("txtConsideredDbr");
        var totalEmiOnScb = 0;
        var totalEmiOffScb = 0;
        var totalInteresOnScb = 0;
        var totalInterestOffScb = 0;
        var balanceSupport = 0;
        var maxEmiCap = 0;

        if ((util.isFloat(consideredDbr) && !util.isEmpty(consideredDbr))) {
            var totalJointIncome = $F("txtTotalJointInComeForLcWithCal");
            if ((util.isFloat(totalJointIncome) && !util.isEmpty(totalJointIncome))) {
                maxEmiCap = parseFloat((parseFloat(totalJointIncome) * parseFloat(consideredDbr)) / 100);
            }
            else {
                maxEmiCap = 0;
            }
            if (consideredDbr > 65) {
                $('tdDbr4').style.display = 'block';
                //alert("Level 3 Required");
            }
            else {
                $('tdDbr4').style.display = 'none';
            }
        }
        else {
            maxEmiCap = 0;
        }

        $("txtMaximumEmiCapability").setValue(maxEmiCap);
        if ((util.isFloat($F("txtTotalEmiForLcExistingPaymentOnScb")) && !util.isEmpty($F("txtTotalEmiForLcExistingPaymentOnScb")))) {
            totalEmiOnScb = parseFloat($F("txtTotalEmiForLcExistingPaymentOnScb"));
        }
        if ((util.isFloat($F("txtTotalEmiForLcExistingPaymentOffScb")) && !util.isEmpty($F("txtTotalEmiForLcExistingPaymentOffScb")))) {
            totalEmiOffScb = parseFloat($F("txtTotalEmiForLcExistingPaymentOffScb"));
        }
        if ((util.isFloat($F("txtTotalinterestForOnScbLcExistingrepayment")) && !util.isEmpty($F("txtTotalinterestForOnScbLcExistingrepayment")))) {
            totalInteresOnScb = parseFloat($F("txtTotalinterestForOnScbLcExistingrepayment"));
        }
        if ((util.isFloat($F("txtTotalinterestForOffScbLcExistingrepayment")) && !util.isEmpty($F("txtTotalinterestForOffScbLcExistingrepayment")))) {
            totalInterestOffScb = parseFloat($F("txtTotalinterestForOffScbLcExistingrepayment"));
        }
        var existingEmi = totalEmiOnScb + totalEmiOffScb + totalInteresOnScb + totalInterestOffScb;
        $("txtExistingEmi").setValue(existingEmi);
        var currentPrimary = maxEmiCap - existingEmi;
        $("txtCurrentEMICapability").setValue(currentPrimary);

        if ((util.isFloat($F("txtBalanceSupported")) && !util.isEmpty($F("txtBalanceSupported")))) {
            balanceSupport = parseFloat($F("txtBalanceSupported"));
        }
        if (parseFloat(balanceSupport) > parseFloat(currentPrimary) || $F("txtBalanceSupported") == "NOT APPLICABLE") {
            $("txtMaximumEmi").setValue(currentPrimary);
            if (currentPrimary < 0) {
                $('tdDbrEx5').style.display = 'block';
                //alert("Level 3 Required");
            }
            else {
                $('tdDbrEx5').style.display = 'none';
            }
        }
        else {
            $("txtMaximumEmi").setValue(balanceSupport);
            if (balanceSupport < 0) {
                $('tdDbrEx5').style.display = 'block';
                //alert("Level 3 Required");
            }
            else {
                $('tdDbrEx5').style.display = 'none';
            }
        }

        loanCalculatorHelPer.calculateTenorSettings();



    },
    considerPrevrepHis: function(bankId, tabName, accountType) {
        var consider = "";
        var offscbEmi = 0;
        var onscbEmi = 0;
        var totalEmi = 0;
        if (tabName == "BM" && accountType == "Pr") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForPrimaryOffScbBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEMI" + i + "ForOffSCBPrimaryBank" + bankId)) && !util.isEmpty($F("txtEMI" + i + "ForOffSCBPrimaryBank" + bankId))) {
                        offscbEmi += parseFloat($F("txtEMI" + i + "ForOffSCBPrimaryBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotal1OnSCBRepHisForPrimaryBank" + bankId)) && !util.isEmpty($F("txtTotal1OnSCBRepHisForPrimaryBank" + bankId))) {
                onscbEmi = $F("txtTotal1OnSCBRepHisForPrimaryBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txttotalForOffSCBForPrimaryBank" + bankId).setValue(offscbEmi);
            $("txtTotalEMIClosingConsideredForPrimaryBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "BM" && accountType == "Jt") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsiderOffScb" + i + "ForJointBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmiOffScb" + i + "ForJointBank" + bankId)) && !util.isEmpty($F("txtEmiOffScb" + i + "ForJointBank" + bankId))) {
                        offscbEmi += parseFloat($F("txtEmiOffScb" + i + "ForJointBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotalOnScbForJointBank" + bankId)) && !util.isEmpty($F("txtTotalOnScbForJointBank" + bankId))) {
                onscbEmi = $F("txtTotalOnScbForJointBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotalOffSCBJointBank" + bankId).setValue(offscbEmi);
            $("txtTotalEMIClosingConsideredPrevRepHisForJointBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "SE" && accountType == "Pr") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForSEPrOffScbBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEMI" + i + "ForOffSCBSEPrBank" + bankId)) && !util.isEmpty($F("txtEMI" + i + "ForOffSCBSEPrBank" + bankId))) {
                        offscbEmi += parseFloat($F("txtEMI" + i + "ForOffSCBSEPrBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotal1OnSCBRepHisForSEPrBank" + bankId)) && !util.isEmpty($F("txtTotal1OnSCBRepHisForSEPrBank" + bankId))) {
                onscbEmi = $F("txtTotal1OnSCBRepHisForSEPrBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txttotalForOffSCBForSEPrBank" + bankId).setValue(offscbEmi);
            $("txtTotalEMIClosingConsideredForSEPrBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "SE" && accountType == "Jt") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForSEJtRepHisOffScbBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEMI" + i + "ForOffSCBSEJtBank" + bankId)) && !util.isEmpty($F("txtEMI" + i + "ForOffSCBSEJtBank" + bankId))) {
                        offscbEmi += parseFloat($F("txtEMI" + i + "ForOffSCBSEJtBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotal1OnSCBRepHisForSEJtBank" + bankId)) && !util.isEmpty($F("txtTotal1OnSCBRepHisForSEJtBank" + bankId))) {
                onscbEmi = $F("txtTotal1OnSCBRepHisForSEJtBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txttotalForOffSCBForSEJtBank" + bankId).setValue(offscbEmi);
            $("txtTotalEMIClosingConsideredForSEJtBank" + bankId).setValue(totalEmi);

        }
    },
    considerPrevrepHisOnScb: function(bankId, tabName, accountType) {
        var consider = "";
        var offscbEmi = 0;
        var onscbEmi = 0;
        var totalEmi = 0;
        if (tabName == "BM" && accountType == "Pr") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForOnSCBReplHisForPrimaryBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmi" + i + "ForOnScbForPrimaryBank" + bankId)) && !util.isEmpty($F("txtEmi" + i + "ForOnScbForPrimaryBank" + bankId))) {
                        onscbEmi += parseFloat($F("txtEmi" + i + "ForOnScbForPrimaryBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txttotalForOffSCBForPrimaryBank" + bankId)) && !util.isEmpty($F("txttotalForOffSCBForPrimaryBank" + bankId))) {
                offscbEmi = $F("txttotalForOffSCBForPrimaryBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotal1OnSCBRepHisForPrimaryBank" + bankId).setValue(onscbEmi);
            $("txtTotalEMIClosingConsideredForPrimaryBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "BM" && accountType == "Jt") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsiderOnScb" + i + "ForJointBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmiOnScb" + i + "ForJointBank" + bankId)) && !util.isEmpty($F("txtEmiOnScb" + i + "ForJointBank" + bankId))) {
                        onscbEmi += parseFloat($F("txtEmiOnScb" + i + "ForJointBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotalOffSCBJointBank" + bankId)) && !util.isEmpty($F("txtTotalOffSCBJointBank" + bankId))) {
                offscbEmi = $F("txtTotalOffSCBJointBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotalOnScbForJointBank" + bankId).setValue(onscbEmi);
            $("txtTotalEMIClosingConsideredPrevRepHisForJointBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "SE" && accountType == "Pr") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForSEPrOffScbBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmi" + i + "ForOnScbForSEPrBank" + bankId)) && !util.isEmpty($F("txtEmi" + i + "ForOnScbForSEPrBank" + bankId))) {
                        onscbEmi += parseFloat($F("txtEmi" + i + "ForOnScbForSEPrBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txttotalForOffSCBForSEPrBank" + bankId)) && !util.isEmpty($F("txttotalForOffSCBForSEPrBank" + bankId))) {
                offscbEmi = $F("txttotalForOffSCBForSEPrBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotal1OnSCBRepHisForSEPrBank" + bankId).setValue(onscbEmi);
            $("txtTotalEMIClosingConsideredForSEPrBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "SE" && accountType == "Jt") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForOnSCBReplHisForSEJtBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmi" + i + "ForOnScbForSEJtBank" + bankId)) && !util.isEmpty($F("txtEmi" + i + "ForOnScbForSEJtBank" + bankId))) {
                        onscbEmi += parseFloat($F("txtEmi" + i + "ForOnScbForSEJtBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txttotalForOffSCBForSEJtBank" + bankId)) && !util.isEmpty($F("txttotalForOffSCBForSEJtBank" + bankId))) {
                offscbEmi = $F("txttotalForOffSCBForSEJtBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotal1OnSCBRepHisForSEJtBank" + bankId).setValue(onscbEmi);
            $("txtTotalEMIClosingConsideredForSEJtBank" + bankId).setValue(totalEmi);

        }
    },
    changeVendore: function() {
        var vendoreId = $F("cmbVendor");
        if (vendoreId == "-1") {
            $('txtVendorName').disabled = '';
            $('txtVendoreCode').setValue('');
        }
        else {
            var vdr = Page.vendoreArray.findByProp('AutoVendorId', vendoreId);
            if (vdr) {
                $('txtVendoreCode').setValue(vdr.VendorCode);
                $('txtVendorName').disabled = 'false';
                analyzeLoanApplicationManager.CheckMouByVendoreId();
            }
        }
    },
    changeManufacture: function() {
        var manufactureId = $F("cmbManufacturer");
        if (manufactureId == "-1") {
            $('txtManufacturarCode').setValue('');
        }
        else {
            var mnf = Page.manufactureArray.findByProp('ManufacturerId', manufactureId);
            if (mnf) {
                $('txtManufacturarCode').setValue(mnf.ManufacturerCode);
                $('txtVehicleModelCode').setValue('');
                analyzeLoanApplicationManager.getModelByManufactureId();

            }
        }
    },
    changeModel: function() {
        var vehicleId = $F('cmbModel');
        var model = Page.modelArray.findByProp('VehicleId', vehicleId);
        if (model) {
            $('txtTrimLevel').setValue(model.TrimLevel);
            $('txtEngineCapacityCC').setValue(model.CC);
            $('hdnModel').setValue(model.Model);
            $('txtVehicleType').setValue(model.Type);
            $('txtManufacturaryear').setValue(model.ManufacturingYear);
            $('txtVehicleModelCode').setValue(model.ModelCode);
        }
        analyzeLoanApplicationManager.Getvaluepackallowed();

    },
    changeVehicleStatus: function() {
        var vehicleStatus = $F('cmdVehicleStatus');
        var vs = Page.vehicleStatusArray.findByProp('StatusId', vehicleStatus);
        if (vs) {
            $('txtVehicleStatusCode').setValue(vs.StatusCode);
        }
        else {
            $('txtVehicleStatusCode').setValue('');
        }
        analyzeLoanApplicationManager.Getvaluepackallowed();

    },
    changeARTA: function() {
        var arta = $F("cmbARTA");
        if (arta == "1") {
            var age = parseInt($F("txtCurrenAge"));
            if (age > 60 && age <= 65) {
                $('cmbARTAReference').setValue('1');
                $('cmbARTAReference').disabled = 'false';
            }
        }
        else {
            $('cmbARTAReference').setValue('-1');
            $('cmbARTAReference').disabled = '';
        }
        loanCalculatorHelPer.calculateTenorSettings();
    },
    changeCCBundle: function() {
        loanCalculatorHelPer.calculateTenorSettings();

    },
    changeSeatingCapacity: function() {
        var seatingCapacity = parseInt($F("cmsSeatingCapacity"));
        if (seatingCapacity > 9) {
            alert("Cannot be more than 9 without Level 3");
            $('cmbLabelForDlaLimit').setValue('3');

        }
        else {
            analyzeLoanApplicationHelper.setagewiseLebel();
        }
        loanCalculatorHelPer.calculateTenorSettings();
        var approverId = $F("cmbApprovedby");
        if (approverId != "-1") {
            analystfinalizationHelper.GetApproverLimitByUserId(approverId);
        }
    },
    populateColleterization: function() {
        var optlinkForColl = "<option value='-1'>Select From list</option>";
        var tablePrAccount = $('tblFacilityTermLoan');
        var rowCount = tablePrAccount.rows.length;

        for (var i = 1; i <= rowCount - 1; i++) {
            if ($F('cmbTypeForTermLoans' + i) == '-1') {
            }
            else {
                var facilityId = $F('cmbTypeForTermLoans' + i);
                var facilityName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                optlinkForColl += "<option value=" + facilityId + ">" + facilityName + "</option>";
            }
        }
        var prevcoll = $F("cmbColleterization");
        var prevCollWith = $F("cmbColleterizationWith");

        $("cmbColleterization").update(optlinkForColl);
        $("cmbColleterizationWith").update(optlinkForColl);

        $("cmbColleterization").setValue(prevcoll);
        $("cmbColleterizationWith").setValue(prevCollWith);

    },
    setagewiseLebel: function() {
        $('cmbLabelForDlaLimit').setValue('1');
        var currentAgeForPrimary = parseInt($F("txtCurrenAge"));
        var currentAgeForJoin = parseInt($F("txtJtCurrntAge"));
        var isJoint = $F('txtIsJoint');
        if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && currentAgeForJoin < 65 && isJoint == 1) {
            $('cmbLabelForDlaLimit').setValue('2');

        }
        if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && isJoint == 0) {
            $('cmbLabelForDlaLimit').setValue('2');

        }
        if (currentAgeForJoin > 65 && currentAgeForJoin < 69 && currentAgeForPrimary < 65 && isJoint == 1) {
            $('cmbLabelForDlaLimit').setValue('2');

        }
        if ((currentAgeForPrimary > 68 || currentAgeForJoin > 68) && isJoint == 1) {
            $('cmbLabelForDlaLimit').setValue('3');
        }
        if (currentAgeForPrimary > 68 && isJoint == 0) {
            $('cmbLabelForDlaLimit').setValue('3');
        }

        var lbl = parseInt($F("cmbLabelForDlaLimit"));
        var dbrLabel = parseInt($F("cmbDBRLevel"));
        if (dbrLabel > lbl) {
            $('cmbLabelForDlaLimit').setValue(dbrLabel);
        }
        var lavelFortenor = parseInt("cmbLabelForTonerLc");
        if (lavelFortenor > lbl) {
            $('cmbLabelForDlaLimit').setValue(lavelFortenor);
        }
    },
    changeManufactureryear: function() {
        var manuyear = $F("txtManufacturaryear");
        if (!util.isDigit(manuyear)) {
            alert("Manufacture Year must me a number Field");
            $('txtManufacturaryear').setValue('0');
            //$('txtManufacturaryear').focus();
            return false;
        }
        $("lblManufactureYearAlert").innerHTML = "";
        if (parseInt(manuyear) < 2007) {
            alert("Car Import Docs Required");
            $("lblManufactureYearAlert").innerHTML = "Car Import Docs Required";
        }
        loanCalculatorHelPer.calculateTenorSettings();
    }


};
Event.onReady(Page.init);