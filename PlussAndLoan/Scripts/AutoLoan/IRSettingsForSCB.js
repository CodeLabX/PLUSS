﻿
var InsurenceType = { 1: 'General', 2: 'Life', 3: 'General & Life' };
var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    insurence: null,
    IRSettingArray: [],

    init: function() {
        util.prepareDom();
        Event.observe('lnkAddNewIRSettingsForVehicle', 'click', IRSCBSettingsHelper.update);
        Event.observe('lnkSave', 'click', IRSCBSettingsManager.save);
        //    Event.observe('btnSearch', 'click', IRVehicleSettingsManager.render);
        Event.observe('lnkClose', 'click', util.hideModal.curry('IRSettingsPopupDiv'));

        //    Event.observe('lnkUploadSave', 'click', Page.upload);

        //    Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });

        IRSCBSettingsManager.render();
        //IRSCBSettingsManager.getStatus();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        IRSCBSettingsManager.render();
        //IRSCBSettingsManager.getStatus();
    }

};

var IRSCBSettingsManager = {

    render: function() {
        //var comapanyName = $F('txtSearch');

        var res = PlussAuto.IRSettingsForSCBStatus.GetAutoSCBStatusIRSummary(Page.pageNo, Page.pageSize);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        IRSCBSettingsHelper.renderCollection(res);
    },
    getStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.IRSettingsForSCBStatus.GetStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        IRSCBSettingsHelper.populateStatusCombo(res);
    },


    save: function() {
        if (!util.validateEmpty('txtIRWithARTA')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        var IRWithARTA = $F('txtIRWithARTA');
        if (!util.isFloat(IRWithARTA)) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        if (!util.validateEmpty('cmbVehicleStatus')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }


        var a = $F('txtIRWithoutARTA');
        if (!util.isFloat(a)) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        var AutoSCBStatusIR = Page.insurence;
        AutoSCBStatusIR.IrWithArta = $F('txtIRWithARTA');
        AutoSCBStatusIR.SCBStatus = $F('cmbVehicleStatus');
        AutoSCBStatusIR.IrWithoutArta = $F('txtIRWithoutARTA');

        var res = PlussAuto.IRSettingsForSCBStatus.SaveAuto_IRSCBStatus(AutoSCBStatusIR);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("IR Settings for SCB Save successfully");
        }

        IRSCBSettingsManager.render();
        util.hideModal('IRSettingsPopupDiv');
    },
    SCBStatusIRDeleteById: function(SCBStatusIRId) {
    var res = PlussAuto.IRSettingsForSCBStatus.IRSCBStatusDeleteById(SCBStatusIRId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("IR Settings for SCB Delete successfully");
        }

        IRSCBSettingsManager.render();
    }
};
var IRSCBSettingsHelper = {
    renderCollection: function(res) {
        Page.IRSettingArray = res.value;
        var html = [];
        for (var i = 0; i < Page.IRSettingArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            Page.IRSettingArray[i].InsTypeName = InsurenceType[Page.IRSettingArray[i].InsType];
            Page.IRSettingArray[i]._LastupdateDate = Page.IRSettingArray[i].LastUpdateDate.format();
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{StatusName}</td><td>#{IrWithArta}</td><td>#{IrWithoutArta}</td><td>#{_LastupdateDate}</td><td><a title="Edit" href="javascript:;" onclick="IRSCBSettingsHelper.update(#{SCBStatusIRId})" class="icon iconEdit"></a> \
                            <a title="Delete" href="javascript:;" onclick="IRSCBSettingsHelper.remove(#{SCBStatusIRId})" class="icon iconDelete"></a></td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.IRSettingArray[i])
			     );
        }
        $('IRSettingsList').update(html.join(''));
        if (res.value.length != 0) {
            Page.pager.reset(Page.IRSettingArray[0].TotalCount, Page.pageNo);
        }


    },
    update: function(scbStatusIRId) {
        $$('#IRSettingsPopupDiv .txt').invoke('clear');
        IRSCBSettingsHelper.clearForm();

        var insurence = Page.IRSettingArray.findByProp('SCBStatusIRId', scbStatusIRId);
        Page.insurence = insurence || { SCBStatusIRId: util.uniqId() };
        if (insurence) {
            $('txtIRWithARTA').setValue(insurence.IrWithArta);
            $('cmbVehicleStatus').setValue(insurence.SCBStatus);
            $('txtIRWithoutARTA').setValue(insurence.IrWithoutArta);
            //            $('txtVehicleCC').setValue(insurence.LastUpdateDate);
            ////            $('txtInsurancePer').setValue(insurence.InsurancePercent);
        }
        else {
            $('cmbVehicleStatus').setValue('-1');
        }

        util.showModal('IRSettingsPopupDiv');
    },
    remove: function(SCBStatusIRId) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            IRSCBSettingsManager.SCBStatusIRDeleteById(SCBStatusIRId);
        }

    },
    clearForm: function() {
        $('txtIRWithARTA').setValue('');
        $('cmbVehicleStatus').setValue('1');
        $('txtIRWithoutARTA').setValue('');
        //        $('txtVehicleCC').setValue('');
        //        $('txtInsurancePer').setValue('');
    },
    //    searchKeypress: function(e) {
    //        if (event.keyCode == 13) {
    //            IRVehicleSettingsManager.render();
    //        }
    //    },
    populateStatusCombo: function(res) {
        document.getElementById('cmbVehicleStatus').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmbVehicleStatus').options.add(opt);
        }


        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVehicleStatus').options.add(ExtraOpt);


    }
};

Event.onReady(Page.init);
