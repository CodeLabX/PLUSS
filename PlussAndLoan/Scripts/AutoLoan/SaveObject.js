﻿

var saveObjectHelper = {
    //Analyst Master Object Start
    anlystMasterObject: function() {
        var objAutoAnalistMaster = new Object();
        objAutoAnalistMaster.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoAnalistMaster.LlId = $F('ctl00_ContentPlaceHolder_txtLLID');
        objAutoAnalistMaster.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        objAutoAnalistMaster.IsJoint = $F('txtIsJoint') == '0' ? false : true; ;
        objAutoAnalistMaster.ReasonOfJoint = $('cmbReasonofJointApplicant').options[$('cmbReasonofJointApplicant').selectedIndex].text;
        objAutoAnalistMaster.AnalystRemark = $F('tAnalystRemark');
        objAutoAnalistMaster.Colleterization = $F("cmbColleterization");
        objAutoAnalistMaster.ColleterizationWith = $F("cmbColleterizationWith");
        objAutoAnalistMaster.CibInfoId = $F("cmbBureauHistory");

        return objAutoAnalistMaster;
    },
    //Analyst Master object End
    //---------------------------------------------------------

    //Auto Loan Analyst Application Start
    autoLoanAnalystApplication: function() {
        var objAutoLoanAnalystApplication = new Object();
        objAutoLoanAnalystApplication.Id = $F('txtloanAnalystApplicationID');
        objAutoLoanAnalystApplication.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoLoanAnalystApplication.Source = $F('cmbSource1');
        objAutoLoanAnalystApplication.BranchId = $F('cmbBranch');
        objAutoLoanAnalystApplication.BranchCode = $F('txtBranchCode');
        objAutoLoanAnalystApplication.ValuePack = $F('cmbValuePack');
        objAutoLoanAnalystApplication.BorrowingRelationShip = $F('cmbBrowingRelationship');
        objAutoLoanAnalystApplication.BorrowingRelationShipCode = $F('txtRelationshipCode');
        objAutoLoanAnalystApplication.CustomerStatus = $F('cmbCustomerStatus');
        objAutoLoanAnalystApplication.AskingRepaymentMethod = $F('cmbAskingRepaymentmethod');
        objAutoLoanAnalystApplication.ARTA = $F('cmbARTA');
        objAutoLoanAnalystApplication.ARTAStatus = $F('cmbARTAReference');
        objAutoLoanAnalystApplication.GeneralInsurance = $F('cmbGeneralInsurence');
        objAutoLoanAnalystApplication.GeneralInsurancePreference = $F('cmbGeneralInsurencePreference');
        objAutoLoanAnalystApplication.CashSecured100Per = $F('cmbCashSecured') == '1' ? true : false;
        objAutoLoanAnalystApplication.CCBundle = $F('cmbCCBundle') == '1' ? true : false;
        objAutoLoanAnalystApplication.SecurityValue = $F('txtSecurityValue');
        if (objAutoLoanAnalystApplication.SecurityValue == "") {
            objAutoLoanAnalystApplication.SecurityValue = 0;
        }
        return objAutoLoanAnalystApplication;
    },

    //Auto Loan Vehicle Object
    CreateAutoLoanVehicleObject: function() {
        var objAutoLoanVehicle = new Object();
        objAutoLoanVehicle.AutoLoanVehicleId = $F('txtAutoLoanVehicleId');
        objAutoLoanVehicle.VendorId = $F('cmbVendor');
        objAutoLoanVehicle.Auto_ManufactureId = $F('cmbManufacturer');
        var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;

        if (manufacturName != "Other") {
            objAutoLoanVehicle.VehicleId = $F('cmbModel');
            var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
            if (vehicleName != "Other") {
                objAutoLoanVehicle.Model = $F('hdnModel');
                objAutoLoanVehicle.VehicleType = $F('txtVehicleType');
            }
            else {
                objAutoLoanVehicle.Model = vehicleName;
                objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
            }
        }
        else {
            objAutoLoanVehicle.VehicleId = "-7"; //Vehicle -7= Others
            objAutoLoanVehicle.Model = $F('txtModelName');
            objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
        }
        objAutoLoanVehicle.TrimLevel = $F('txtTrimLevel');
        objAutoLoanVehicle.EngineCC = $F('txtEngineCapacityCC');
        objAutoLoanVehicle.VehicleStatus = $F('cmdVehicleStatus');

        objAutoLoanVehicle.ManufacturingYear = $F('txtManufacturaryear') == '' ? '0' : $F('txtManufacturaryear');
        objAutoLoanVehicle.QuotedPrice = $F('txtQuotedPrice') == '' ? '0' : $F('txtQuotedPrice');
        objAutoLoanVehicle.VerifiedPrice = $F('txtConsideredprice') == '' ? '0' : $F('txtConsideredprice');

        objAutoLoanVehicle.PriceConsideredBasedOn = $F('cmbPriceConsideredOn');
        objAutoLoanVehicle.SeatingCapacity = $F('cmsSeatingCapacity');
        objAutoLoanVehicle.CarVerification = $F('cmbCarVerification');

        return objAutoLoanVehicle;
    },
    //End Auto Loan Vehicle Object

    //Auto Loan Vendore Object Start

    CreateAutoLoanVendoreObject: function() {
        var objAutoVendor = new Object();
        objAutoVendor.VendorName = $F('txtVendorName');
        objAutoVendor.DealerType = '1';
        return objAutoVendor;
    },

    //Auto Loan Vendore Object End

    //Auto Loan Analyst Application End
    //----------------------------------------------------------

    //Auto Loan Analyst  Vehicle Start
    autoLoanAnalystVehicleDetail: function() {
        var objAutoLoanAnalystVehicleDetail = new Object();
        objAutoLoanAnalystVehicleDetail.Id = $F('txtVehicleDetailID');
        objAutoLoanAnalystVehicleDetail.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoLoanAnalystVehicleDetail.valuePackAllowed = $F('cmbvaluePackAllowed') == '0' ? false : true;
        objAutoLoanAnalystVehicleDetail.SeatingCapacity = $F('cmsSeatingCapacity');
        objAutoLoanAnalystVehicleDetail.ConsideredPrice = $F('txtConsideredprice');
        objAutoLoanAnalystVehicleDetail.PriceConsideredOn = $F('cmbPriceConsideredOn');
        objAutoLoanAnalystVehicleDetail.CarVarification = $F('cmbCarVerification');
        objAutoLoanAnalystVehicleDetail.DeliveryStatus = $F('cmbDeliveryStatus');
        return objAutoLoanAnalystVehicleDetail;

    },
    //Auto Loan Analyst  Vehicle End
    //--------------------------------------------------------------
    //Auto Applicant Account Scb List Start
    autoApplicantAccountSCBList: function() {
        var objAutoApplicantAccountScbList = [];

        var tablePrAccount = $('tblApplicantAccountSCB');
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoApplicantAccountScb = new Object();
            if ($F('txtAccountNumberForSCB' + i) == '') {
            } else {
                if ($F('cmbAccountStatusForSCB' + i) == '0') {
                    alert('please select Account Status');
                    //$('cmbAccountStatusForSCB' + i).focus();
                    return false;
                }

                else {
                    objAutoApplicantAccountScb.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objAutoApplicantAccountScb.LLID = $F('ctl00_ContentPlaceHolder_txtLLID');
                    objAutoApplicantAccountScb.AccountNumberSCB = $F('txtAccountNumberForSCB' + i);
                    objAutoApplicantAccountScb.AccountStatus = $F('cmbAccountStatusForSCB' + i);
                    objAutoApplicantAccountScb.PriorityHolder = $F('cmbPriorityAcHolderForSCB' + i);

                    objAutoApplicantAccountScbList.push(objAutoApplicantAccountScb);

                }

            }
        }
        return objAutoApplicantAccountScbList;

    },
    //Auto Applicant Account Scb List End
    //------------------------------------------------------
    //Auto Applicant Account Other List Start

    autoApplicantAccountOtherList: function() {
        var objAutoApplicantAccountOtherList = [];

        var tableOtherBank = $('tblApplicantAccountOther');
        var rowCount = tableOtherBank.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoApplicantAccountOther = new Object();
            if ($F('cmbBankNameForOther' + i) == '-1') {
            } else {
                if ($F('cmbBranchNameOther' + i) == '-1') {
                    alert('please select Branch');
                    //$('cmbBranchNameOther' + i).focus();
                    return false;
                }

                else {
                    if ($F('txtAccountNumberother' + i) == '') {
                        alert('please Input Account Number');
                        //$('txtAccountNumberother' + i).focus();
                        return false;
                    }

                    else {

                        if ($F('cmbAccountStatus' + i) == '0') {
                            alert('please Select Account Status');
                            //$('cmbAccountStatus' + i).focus();
                            return false;
                        }

                        else {

                            if ($F('cmbAccountType' + i) == '0') {
                                alert('please Select Account Type');
                                // $('cmbAccountType' + i).focus();
                                return false;
                            }

                            else {


                                objAutoApplicantAccountOther.AutoLoanMasterId = $F('txtAutoLoanMasterId');

                                objAutoApplicantAccountOther.BankId = $F('cmbBankNameForOther' + i);
                                objAutoApplicantAccountOther.BranchId = $F('cmbBranchNameOther' + i);
                                objAutoApplicantAccountOther.AccountNumber = $F('txtAccountNumberother' + i);
                                objAutoApplicantAccountOther.AccountCategory = $F('cmbAccountStatus' + i);
                                objAutoApplicantAccountOther.AccountType = $F('cmbAccountType' + i);

                                objAutoApplicantAccountOtherList.push(objAutoApplicantAccountOther);
                            }
                        }
                    }
                }

            }
        }
        return objAutoApplicantAccountOtherList;

    },
    //Auto Applicant Account Other List End
    //--------------------------------------------------------

    // autoFacilityListForScb Start
    autoFacilityListForScb: function() {
        var objAutoFacilityList = [];

        //Term Loan
        var tablePrAccount = $('tblFacilityTermLoan');
        var rowCount = tablePrAccount.rows.length;

        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoFacility = new Object();
            if ($F('cmbTypeForTermLoans' + i) == '-1') {
            } else {
                if ($F('txtInterestRate' + i) == '') {
                    //alert('please input Interest rate.');
                    //$('txtInterestRate' + i).focus();
                    //return false;
                    $('txtLimit' + i).setValue('0');
                }
                if ($F('txtLimit' + i) == '') {
                    //alert('please input Limit.');
                    //$('txtLimit' + i).focus();
                    //return false;
                    $('txtLimit' + i).setValue('0');
                }
                if ($F('txtoutStanding' + i) == '') {
                    //alert('please input Outstanding.');
                    //$('txtoutStanding' + i).focus();
                    //return false;
                    $('txtoutStanding' + i).setValue('0');
                }
                if ($F('txtEMI' + i) == '') {
                    //alert('please input EMI.');
                    // $('txtEMI' + i).focus();
                    //return false;
                    $('txtEMI' + i).setValue('0');
                }
                if ($F('txtEMIPer' + i) == '') {
                    //alert('please input EMI Percent.');
                    //$('txtEMIPer' + i).focus();
                    //  return false;
                    $('txtEMIPer' + i).setValue('0');
                }
                //else {


                objAutoFacility.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacility.FacilityTypeName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                objAutoFacility.FacilityType = $F('cmbTypeForTermLoans' + i);
                objAutoFacility.InterestRate = $F('txtInterestRate' + i) == '' ? '0' : $F('txtInterestRate' + i);
                objAutoFacility.PresentBalance = $F('txtoutStanding' + i) == '' ? '0' : $F('txtoutStanding' + i);
                objAutoFacility.PresentEMI = $F('txtEMI' + i) == '' ? '0' : $F('txtEMI' + i);
                objAutoFacility.PresentLimit = $F('txtLimit' + i) == '' ? '0' : $F('txtLimit' + i);
                objAutoFacility.EMIPercent = $F('txtEMIPer' + i) == '' ? '0' : $F('txtEMIPer' + i);
                objAutoFacility.EMIShare = $F('txtEMIShare' + i) == '' ? '0' : $F('txtEMIShare' + i);
                objAutoFacility.Status = $F('cmbStatusForSecurities' + i);

                objAutoFacilityList.push(objAutoFacility);

                //}

            }
        }
        // trem Loan end

        //CreditCard Start

        var tablePrAccountCreditCard = $('tblFacilityCreditCard');
        var rowCountCreditCard = tablePrAccountCreditCard.rows.length;

        for (var i = 1; i <= rowCountCreditCard - 1; i++) {
            var objAutoFacilityCreditCard = new Object();
            if ($F('cmbTypeForCreditCard' + i) == '-1') {
            } else {
                if ($F('txtOriginalLimitForCreditCard' + i) == '') {
                    //alert('please input Credit Card Limit.');
                    //$('txtOriginalLimitForCreditCard' + i).focus();
                    //return false;
                    $('txtOriginalLimitForCreditCard' + i).setValue('0');
                }
                //                else if ($F('txtInterestForCreditCard' + i) == '') {
                //                    alert('please input Credit Card Interest.');
                //                    // $('txtInterestForCreditCard' + i).focus();
                //                    return false;
                //                }
                //                else {

                objAutoFacilityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacilityCreditCard.FacilityTypeName = $('cmbTypeForCreditCard' + i).options[$('cmbTypeForCreditCard' + i).selectedIndex].text;
                objAutoFacilityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
                objAutoFacilityCreditCard.InterestRate = 5; //$F('txtInterestRateCreditCard' + i) == '' ? '0' : $F('txtInterestRateCreditCard' + i);
                objAutoFacilityCreditCard.PresentLimit = $F('txtOriginalLimitForCreditCard' + i) == '' ? '0' : $F('txtOriginalLimitForCreditCard' + i);
                objAutoFacilityCreditCard.Status = $F('cmbStatusForSecuritiesForCreditCard' + i);
                objAutoFacilityList.push(objAutoFacilityCreditCard);

                //}

            }
        }


        //Credit card end


        //OverDraft Start

        var tablePrAccountoverDraft = $('tblFacilityOverDraft');
        var rowCountoverDraft = tablePrAccountoverDraft.rows.length;

        for (var i = 1; i <= rowCountoverDraft - 1; i++) {
            var objAutoFacilityOverDraft = new Object();
            if ($F('txtOverdraftlimit' + i) == '') {
            } else {
                //if ($F('txtOverdraftInterest' + i) == '') {
                // alert('please input Interest Rate for Over Draft.');
                //$('txtOverdraftInterest' + i).focus();
                //return false;
                //  $('txtOverdraftInterest' + i).setValue('0');
                //}
                //else {

                objAutoFacilityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacilityOverDraft.FacilityTypeName = 'Over Draft';
                objAutoFacilityOverDraft.FacilityType = '23';
                objAutoFacilityOverDraft.PresentLimit = $F('txtOverdraftlimit' + i) == '' ? '0' : $F('txtOverdraftlimit' + i);
                objAutoFacilityOverDraft.InterestRate = $F('txtOverdraftInterest' + i) == '' ? '0' : $F('txtOverdraftInterest' + i);
                objAutoFacilityOverDraft.Status = $F('cmbStatusForSecuritiesForoverDraft' + i);

                objAutoFacilityList.push(objAutoFacilityOverDraft);

                //}

            }
        }
        return objAutoFacilityList;

    },
    // autoFacilityListForScb End
    //------------------------------------------------
    //AutoSecurityListForScb Start
    AutoSecurityListForScb: function() {

        var objAutoSecurityList = [];

        // start TERM LOANS
        //        var tableOnSCBSecurityTermLoan = $('tblOnSCBSecurityTermLoan');
        //        var rowCountTremLoan = tableOnSCBSecurityTermLoan.rows.length;

        //        for (var i = 1; i <= rowCountTremLoan - 1; i++) {
        //            var objAutoSecurityTermLoan = new Object();
        //            if ($F('cmbTypeForTermLoans' + i) == '-1') {
        //            } else {
        //                if ($F('cmbStatusForSecurities' + i) == '-1') {
        //                    alert('please Select Status for Security (Term Loan).');
        //                    //$('cmbStatusForSecurities' + i).focus();
        //                    return false;
        //                }

        //                else if ($F('txtSecurityFV' + i) == '-1') {
        //                    alert('please input FV for Security (Term Loan).');
        //                    // $('txtSecurityFV' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txttermloanIssuingDate' + i) == '-1') {
        //                    alert('please input Issue Date for Security (Term Loan).');
        //                    //$('txttermloanIssuingDate' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('cmbTypeForSequrities' + i) == '-1') {
        //                    alert('please select Security Type (Term Loan).');
        //                    //  $('cmbTypeForSequrities' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityPV' + i) == '-1') {
        //                    alert('please input PV for Security (Term Loan).');
        //                    // $('txtSecurityPV' + i).focus();
        //                    return false;
        //                }

        //                else {

        //                    objAutoSecurityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        //                    objAutoSecurityTermLoan.FacilityType = $F('cmbTypeForTermLoans' + i);
        //                    objAutoSecurityTermLoan.NatureOfSecurity = $F('cmbTypeForSequrities' + i);
        //                    objAutoSecurityTermLoan.Status = $F('cmbStatusForSecurities' + i);
        //                    objAutoSecurityTermLoan.IssuingOffice = $F('txttermloanIssuingOffice' + i);
        //                    objAutoSecurityTermLoan.FaceValue = $F('txtSecurityFV' + i) == '' ? '0' : $F('txtSecurityFV' + i);
        //                    objAutoSecurityTermLoan.XTVRate = $F('txtSecurityPV' + i) == '' ? '0' : $F('txtSecurityPV' + i);
        //                    objAutoSecurityTermLoan.IssueDate = $F('txttermloanIssuingDate' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txttermloanIssuingDate' + i));

        //                    objAutoSecurityList.push(objAutoSecurityTermLoan);

        //                }

        //            }
        //        }

        var tableOnSCBSecurityTermLoan = $('tblFacilityTermLoan');
        var rowCountTremLoan = tableOnSCBSecurityTermLoan.rows.length;

        for (var i = 1; i <= rowCountTremLoan - 1; i++) {
            var objAutoSecurityTermLoan = new Object();
            if ($F('cmbTypeForTermLoans' + i) == '-1') {
            }
            else {
                //                if ($F('cmbStatusForSecurities' + i) == '-1') {
                //                    alert('please Select Status for Security (Term Loan).');
                //                    //$('cmbStatusForSecurities' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtSecurityFV' + i) == '-1') {
                //                    alert('please input FV for Security (Term Loan).');
                //                    // $('txtSecurityFV' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txttermloanIssuingDate' + i) == '-1') {
                //                    alert('please input Issue Date for Security (Term Loan).');
                //                    //$('txttermloanIssuingDate' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('cmbTypeForSequrities' + i) == '-1') {
                //                    alert('please select Security Type (Term Loan).');
                //                    //  $('cmbTypeForSequrities' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtSecurityPV' + i) == '-1') {
                //                    alert('please input PV for Security (Term Loan).');
                //                    // $('txtSecurityPV' + i).focus();
                //                    return false;
                //                }

                //                else {

                objAutoSecurityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityTermLoan.FacilityType = $F('cmbTypeForTermLoans' + i);
                objAutoSecurityTermLoan.NatureOfSecurity = $F('cmbTypeForSequrities' + i);
                objAutoSecurityTermLoan.Status = $F('cmbStatusForSecurities' + i);
                //objAutoSecurityTermLoan.IssuingOffice = $F('txttermloanIssuingOffice' + i);
                objAutoSecurityTermLoan.FaceValue = $F('txtSecurityFV' + i) == '' ? '0' : $F('txtSecurityFV' + i);
                objAutoSecurityTermLoan.XTVRate = $F('txtSecurityPV' + i) == '' ? '0' : $F('txtSecurityPV' + i);
                //objAutoSecurityTermLoan.IssueDate = $F('txttermloanIssuingDate' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txttermloanIssuingDate' + i));

                objAutoSecurityList.push(objAutoSecurityTermLoan);

                //}

            }
        }
        //TERM LOANS end



        // start CreditCard
        var tableOnSCBSecurityCreditCard = $('tblFacilityCreditCard');
        var rowCountCreditCard = tableOnSCBSecurityCreditCard.rows.length;

        for (var i = 1; i <= rowCountCreditCard - 1; i++) {
            var objAutoSecurityCreditCard = new Object();
            if ($F('cmbTypeForCreditCard' + i) == '-1') {
            }
            else {
                //                if ($F('cmbStatusForSecuritiesForCreditCard' + i) == '-1') {
                //                    alert('please Select Status for Security (Credit Card).');
                //                    // $('cmbStatusForSecuritiesForCreditCard' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtSecurityFVForcreditCard' + i) == '-1') {
                //                    alert('please input FV for Security (Credit Card).');
                //                    //   $('txtSecurityFVForcreditCard' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtCreditCardIssuingDate' + i) == '-1') {
                //                    alert('please input Issue Date for Security (Credit Card).');
                //                    //    $('txtSecurityIssuingDateCreditCard' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('cmbTypeForSequritiesForcreditCard' + i) == '-1') {
                //                    alert('please select Security Type (Credit Card).');
                //                    //  $('cmbTypeForSequritiesForcreditCard' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtSecurityPVForCreditCard' + i) == '-1') {
                //                    alert('please input PV for Security (Credit Card).');
                //                    //   $('txtSecurityPVForCreditCard' + i).focus();
                //                    return false;
                //                }

                //                else {

                objAutoSecurityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
                objAutoSecurityCreditCard.NatureOfSecurity = $F('cmbTypeForSequritiesForcreditCard' + i);
                objAutoSecurityCreditCard.Status = $F('cmbStatusForSecuritiesForCreditCard' + i);
                //objAutoSecurityCreditCard.IssuingOffice = $F('txtCreditCardIssuingOffice' + i);
                objAutoSecurityCreditCard.FaceValue = $F('txtSecurityFVForcreditCard' + i) == '' ? '0' : $F('txtSecurityFVForcreditCard' + i);
                objAutoSecurityCreditCard.XTVRate = $F('txtSecurityPVForCreditCard' + i) == '' ? '0' : $F('txtSecurityPVForCreditCard' + i);
                //objAutoSecurityCreditCard.IssueDate = $F('txtCreditCardIssuingDate' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtCreditCardIssuingDate' + i));

                objAutoSecurityList.push(objAutoSecurityCreditCard);

                //}

            }
        }
        //CreditCard end


        // start OverDraft
        var tableOnSCBSecurityOverDraft = $('tblFacilityOverDraft');
        var rowCountOverDraft = tableOnSCBSecurityOverDraft.rows.length;

        for (var i = 1; i <= rowCountOverDraft - 1; i++) {
            var objAutoSecurityOverDraft = new Object();
            if ($F('txtOverdraftlimit' + i) == '') {
            }
            else {
                //                if ($F('cmbStatusForSecuritiesForoverDraft' + i) == '-1') {
                //                    alert('please Select Status for Security (Over Draft).');
                //                    //   $('cmbStatusForSecuritiesForoverDraft' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtSecurityFVForoverDraft' + i) == '-1') {
                //                    alert('please input FV for Security (Over Draft).');
                //                    //   $('txtSecurityFVForoverDraft' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtOverDraftIssuingDate' + i) == '-1') {
                //                    alert('please input Issue Date for Security (Over Draft).');
                //                    //     $('txtSecurityIssuingDateOverDraft' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('cmbTypeForSequritiesForoverDraft' + i) == '-1') {
                //                    alert('please select Security Type (Over Draft).');
                //                    //    $('cmbTypeForSequritiesForoverDraft' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtSecurityPVForoverDraft' + i) == '-1') {
                //                    alert('please input PV for Security (Over Draft).');
                //                    //   $('txtSecurityPVForoverDraft' + i).focus();
                //                    return false;
                //                }

                //                else {

                objAutoSecurityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityOverDraft.FacilityType = '23';
                objAutoSecurityOverDraft.NatureOfSecurity = $F('cmbTypeForSequritiesForoverDraft' + i);
                objAutoSecurityOverDraft.Status = $F('cmbStatusForSecuritiesForoverDraft' + i);
                //objAutoSecurityOverDraft.IssuingOffice = $F('txtOverDraftIssuingOffice' + i);
                objAutoSecurityOverDraft.FaceValue = $F('txtSecurityFVForoverDraft' + i) == '' ? '0' : $F('txtSecurityFVForoverDraft' + i);
                objAutoSecurityOverDraft.XTVRate = $F('txtSecurityPVForoverDraft' + i) == '' ? '0' : $F('txtSecurityPVForoverDraft' + i);
                //  objAutoSecurityOverDraft.IssueDate = $F('txtOverDraftIssuingDate' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtOverDraftIssuingDate' + i));

                objAutoSecurityList.push(objAutoSecurityOverDraft);

                //}

            }
        }
        return objAutoSecurityList;
    },
    //AutoSecurityListForScb End
    //-----------------------------------------------

    //OFFSCBFacilityList Start

    OFFSCBFacilityList: function() {
        var objOFFSCBFacilityList = [];

        // start TERM LOANS
        var tableOFFSCBSecurityTermLoan = $('tblOFFSCBFacilityTermLoan');
        var rowCountOFFSCBTremLoan = tableOFFSCBSecurityTermLoan.rows.length;

        for (var i = 1; i <= rowCountOFFSCBTremLoan - 1; i++) {
            var objOFFSCBFacilityTermLoan = new Object();
            if ($F('cmbFinancialInstitutionForOffSCB' + i) == '-1') {
            }
            else {
                //                if ($F('txtOriginalLimitForOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Term Loan');
                //                    //   $('txtOriginalLimitForOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtEMIforOffSCB' + i) == '') {
                //                    alert('please input EMI (OFF SCB Term Loan).');
                //                    //    $('txtEMIforOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtEMIPerForOffSCB' + i) == '-1') {
                //                    alert('please input EMI Percent (OFF SCB Term Loan).');
                //                    //   $('txtEMIPerForOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtEMIShareForOffSCB' + i) == '-1') {
                //                    alert('EMI Share is not calculated(OFF SCB Term Loan).');
                //                    //     $('txtEMIShareForOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('cmbStatusForOffScb' + i) == '0') {
                //                    alert('please Select Status (OFF SCB Term Loan).');
                //                    //      $('cmbStatusForOffScb' + i).focus();
                //                    return false;
                //                }

                //                else {

                objOFFSCBFacilityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objOFFSCBFacilityTermLoan.BankID = $F('cmbFinancialInstitutionForOffSCB' + i);
                objOFFSCBFacilityTermLoan.OriginalLimit = $F('txtOriginalLimitForOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForOffScb' + i);
                objOFFSCBFacilityTermLoan.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
                objOFFSCBFacilityTermLoan.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                objOFFSCBFacilityTermLoan.EMIShare = $F('txtEMIShareForOffSCB' + i) == '' ? '0' : $F('txtEMIShareForOffSCB' + i);
                objOFFSCBFacilityTermLoan.Status = $F('cmbStatusForOffScb' + i);
                objOFFSCBFacilityTermLoan.Type = '1';

                objOFFSCBFacilityList.push(objOFFSCBFacilityTermLoan);

                //}

            }
        }
        //TERM LOANS end

        // start CreditCard
        var tableOFFSCBSecurityCreditCard = $('tblOFFSCBFacilityCreditCard');
        var rowCountOFFSCBCreditCard = tableOFFSCBSecurityCreditCard.rows.length;

        for (var i = 1; i <= rowCountOFFSCBCreditCard - 1; i++) {
            var objOFFSCBFacilityCreditCard = new Object();
            if ($F('cmbFinancialInstitutionForCreditCardOffSCB' + i) == '-1') {
            }
            else {
                //                if ($F('txtOriginalLimitForcreditCardOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Credit Card');
                //                    //    $('txtOriginalLimitForcreditCardOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtEMIforOffSCB' + i) == '') {
                //                    alert('please input EMI (OFF SCB Credit Card).');
                //                    //      $('txtEMIforOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtInterestRateForCreditCardForOffScb' + i) == '-1') {
                //                    alert('please input Interest Rate (OFF SCB Credit Card).');
                //                    //      $('txtInterestRateForCreditCardForOffScb' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('cmbStatusForCreditCardOffScb' + i) == '0') {
                //                    alert('please Select Status (OFF SCB Credit Card).');
                //                    //      $('cmbStatusForCreditCardOffScb' + i).focus();
                //                    return false;
                //                }

                //                else {

                objOFFSCBFacilityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objOFFSCBFacilityCreditCard.BankID = $F('cmbFinancialInstitutionForCreditCardOffSCB' + i);
                objOFFSCBFacilityCreditCard.OriginalLimit = $F('txtOriginalLimitForcreditCardOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForcreditCardOffScb' + i);
                objOFFSCBFacilityCreditCard.InterestRate = 5; //$F('txtInterestRateForCreditCardForOffScb' + i) == '' ? '0' : $F('txtInterestRateForCreditCardForOffScb' + i);
                objOFFSCBFacilityCreditCard.Status = $F('cmbStatusForCreditCardOffScb' + i);
                objOFFSCBFacilityCreditCard.Type = '2';


                objOFFSCBFacilityList.push(objOFFSCBFacilityCreditCard);

                //}

            }
        }
        //CreditCard end

        // start OverDraft
        var tableOFFSCBSecurityOverDraft = $('tblOFFSCBFacilityOverDraft');
        var rowCountOFFSCBOverDraft = tableOFFSCBSecurityOverDraft.rows.length;

        for (var i = 1; i <= rowCountOFFSCBOverDraft - 1; i++) {
            var objOFFSCBFacilityOverDraft = new Object();
            if ($F('cmbFinancialInstitutionForOverDraftOffSCB' + i) == '-1') {
            }
            else {
                //                if ($F('txtOriginalLimitForOverDraftOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Over Draft');
                //                    //     $('txtOriginalLimitForOverDraftOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtinterestRateForOverDraftOffSCB' + i) == '') {
                //                    alert('please input Interest Rate (OFF SCB Over Draft).');
                //                    //     $('txtinterestRateForOverDraftOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtInterestForOverDraftInterest' + i) == '-1') {
                //                    alert('Interest did not calculated (OFF SCB Over Draft).');
                //                    //    $('txtInterestForOverDraftInterest' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('cmbStatusForOverDraftOffScb' + i) == '0') {
                //                    alert('please Select Status (OFF SCB Over Draft).');
                //                    //$('cmbStatusForOverDraftOffScb' + i).focus();
                //                    return false;
                //                }

                //                else {

                objOFFSCBFacilityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objOFFSCBFacilityOverDraft.BankID = $F('cmbFinancialInstitutionForOverDraftOffSCB' + i);
                objOFFSCBFacilityOverDraft.OriginalLimit = $F('txtOriginalLimitForOverDraftOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForOverDraftOffScb' + i);
                objOFFSCBFacilityOverDraft.InterestRate = $F('txtinterestRateForOverDraftOffSCB' + i) == '' ? '0' : $F('txtinterestRateForOverDraftOffSCB' + i);
                objOFFSCBFacilityOverDraft.Status = $F('cmbStatusForOverDraftOffScb' + i);
                objOFFSCBFacilityOverDraft.Type = '3';


                objOFFSCBFacilityList.push(objOFFSCBFacilityOverDraft);

                //}

            }
        }
        return objOFFSCBFacilityList;
    },

    //OFFSCBFacilityList End
    //--------------------------------------------------------

    //SalariedObject Start
    SalariedObject: function() {
        var salariedArray = [];
        var jtPrimaryProfession = $F("txtJtProfessionId");
        var prPrimaryProfession = $F("txtPrProfessionId");
        var prOtherProfession = $F("txtPrOtherProfessionId");
        var reasonOfJointIncome = $F("cmbReasonofJointApplicant");

        //Primary Salary Object
        if (prPrimaryProfession == "2" || prOtherProfession == "2") {
            var objSalariedPr = new Object();
            objSalariedPr.AnalystMasterId = $F('txtAnalystMasterID');
            objSalariedPr.SalaryMode = $F('cmbSalaryModeForPr');
            objSalariedPr.Employer = $F("cmbEmployerForSalariedForPr");
            objSalariedPr.EmployerCategory = $F("cmbEmployerCategoryForPrSalaried");
            objSalariedPr.IncomeAssId = $F("cmbInComeAssementForSalaryForPr");
            objSalariedPr.IncomeAssessmentCode = $F("cmbInComeAssementForPr");
            objSalariedPr.GrosSalary = util.isEmptyReturn0($F("txtGrossSalaryForPrSalaried"));
            objSalariedPr.NetSalary = util.isEmptyReturn0($F("txtNetSalaryForPr"));
            objSalariedPr.VariableSalary = $F("cmbVeriableSalaryForPr") == '0' ? false : true;
            objSalariedPr.VariableSalaryPercent = util.isEmptyReturn0($F("txtVeriableSalaryPerForPrSalaried"));
            objSalariedPr.VariableSalaryAmount = util.isEmptyReturn0($F("txtTotalVeriableSalaryForPrSalaried"));
            objSalariedPr.CarAllownce = $F("cmbCarAllowence") == '0' ? false : true;
            objSalariedPr.CarAllownceAmount = util.isEmptyReturn0($F("txtCarAllowenceAmount"));
            objSalariedPr.CarAllowncePer = util.isEmptyReturn0($F("txtCarAllowenceForPr"));
            objSalariedPr.CarAllowanceCalculatedAmount = util.isEmptyReturn0($F("txtCarAllowenceTotalAmountPr"));
            objSalariedPr.OwnHouseBenifit = $F("changeOwnHouseAmount") == '0' ? false : true;
            objSalariedPr.TotalIncome = util.isEmptyReturn0($F("txtTotalInComeForPrSalaried"));
            objSalariedPr.AccountType = "1";
            objSalariedPr.Remarks = $F("txtRemarksForPrSalaried");

            objSalariedPr.ObjSalariedVariableSalaryMonthIncomelist = saveObjectHelper.getPropertyListForSalaried('tblVariableSalaryCalculationForPrSalaried', 'Pr');
            salariedArray.push(objSalariedPr);

        }
        //Joint Salary Object
        if (jtPrimaryProfession == "2" && reasonOfJointIncome == "1") {

            var objSalariedJt = new Object();
            objSalariedJt.AnalystMasterId = $F('txtAnalystMasterID');
            objSalariedJt.SalaryMode = $F('cmbSalaryModeForJt');
            objSalariedJt.Employer = $F("cmbEmployerForSalariedForJt");
            objSalariedJt.EmployerCategory = $F("cmbEmployerCategoryForJtSalaried");
            objSalariedJt.IncomeAssId = $F("cmbInComeAssementForSalaryForJt");
            objSalariedJt.IncomeAssessmentCode = $F("cmbInComeAssementForJt");
            objSalariedJt.GrosSalary = util.isEmptyReturn0($F("txtGrossSalaryForJtSalaried"));
            objSalariedJt.NetSalary = util.isEmptyReturn0($F("txtNetSalaryForJt"));
            objSalariedJt.VariableSalary = $F("cmbVeriableSalaryForJt") ? false : true;
            objSalariedJt.VariableSalaryPercent = util.isEmptyReturn0($F("txtVeriableSalaryPerForJtSalaried"));
            objSalariedJt.VariableSalaryAmount = util.isEmptyReturn0($F("txtTotalVeriableSalaryForJtSalaried"));
            objSalariedJt.CarAllownce = $F("cmbCarAllowenceJt") ? false : true;
            objSalariedJt.CarAllownceAmount = util.isEmptyReturn0($F("txtCarAllowenceAmountJt"));
            objSalariedJt.CarAllowncePer = util.isEmptyReturn0($F("txtCarAllowenceForJt"));
            objSalariedJt.CarAllowanceCalculatedAmount = util.isEmptyReturn0($F("txtCarAllowenceTotalAmountJt"));
            objSalariedJt.OwnHouseBenifit = $F("changeOwnHouseAmountJt") ? false : true;
            objSalariedJt.TotalIncome = util.isEmptyReturn0($F("txtTotalInComeForJtSalaried"));
            objSalariedJt.AccountType = "2";
            objSalariedJt.Remarks = $F("txtRemarksForJtSalaried");

            objSalariedJt.ObjSalariedVariableSalaryMonthIncomelist = saveObjectHelper.getPropertyListForSalaried('tblVariableSalaryCalculationForJtSalaried', 'Jt');


            salariedArray.push(objSalariedJt);
        }
        return salariedArray;

    },
    //SalariedObject End

    //Variable Salary
    getPropertyListForSalaried: function(tableName, accountType) {


        var propertyArray = [];
        if (tableName != "") {

            var tableProperty = $(tableName);
            var rowCount = tableProperty.rows.length;
            var amount = 0;
            for (var i = 1; i < rowCount; i++) {
                var objProperty = new Object();
                if (util.isFloat($F('txtVaiableAmountForMonth' + i + 'For' + accountType + 'Salary')) && !util.isEmpty($F('txtVaiableAmountForMonth' + i + 'For' + accountType + 'Salary'))) {
                    amount = $F('txtVaiableAmountForMonth' + i + 'For' + accountType + 'Salary');
                }


                objProperty.Month = $F("cmbMonth" + i + "For" + accountType + "Salary");
                objProperty.Amount = amount;
                objProperty.AnalystMasterId = $F('txtAnalystMasterID');
                if (amount != 0) {
                    propertyArray.push(objProperty);
                }
                amount = 0;

            }

        }
        return propertyArray;
    },
    //Land lord object
    LandlordObject: function() {
        var landlordArray = [];
        var jtPrimaryProfession = $F("txtJtProfessionId");
        var prPrimaryProfession = $F("txtPrProfessionId");
        var prOtherProfession = $F("txtPrOtherProfessionId");
        var reasonOfJointIncome = $F("cmbReasonofJointApplicant");

        //Primary Land lord Object
        if (prPrimaryProfession == "5" || prOtherProfession == "5") {

            var txttotalrentalIncomeForLandLordPr = 0.0, txtReflactionrequiredForLandLordPr = 0.0, txtReflactionPerForLandlordPr = 0.0;
            if ($F('txttotalrentalIncomeForLandLordPr') != "") {
                txttotalrentalIncomeForLandLordPr = $F('txttotalrentalIncomeForLandLordPr');
            }
            if ($F('txtReflactionrequiredForLandLordPr') != "") {
                txtReflactionrequiredForLandLordPr = $F('txtReflactionrequiredForLandLordPr');
            }
            if ($F('txtReflactionPerForLandlordPr') != "") {
                txtReflactionPerForLandlordPr = $F('txtReflactionPerForLandlordPr');
            }
            var objLandLordPr = new Object();
            objLandLordPr.AutoLoanMasterId = $F('txtAnalystMasterID');
            objLandLordPr.EmployerCatagoryId = $F('cmbEmployeeForLandLordPr');
            objLandLordPr.SectorId = $F('cmbSubsectorForLandLordPr');
            objLandLordPr.IncomeAssId = $F('cmbIncomeAssementForLandLordPr');
            objLandLordPr.TotalRentalIncome = txttotalrentalIncomeForLandLordPr;
            objLandLordPr.ReflectionRequired = txtReflactionrequiredForLandLordPr;
            objLandLordPr.Percentage = txtReflactionPerForLandlordPr;
            objLandLordPr.Remarks = $F('txtAreaRemarksForLandlordPr');
            objLandLordPr.AccountType = 1;
            objLandLordPr.Property = saveObjectHelper.getPropertyList('tblPropertyPr', 'Pr');
            landlordArray.push(objLandLordPr);
        }
        //Joint Landlord Object
        if (jtPrimaryProfession == "5" && reasonOfJointIncome == "1") {

            var txttotalrentalIncomeForLandLordJt = 0.0, txtReflactionrequiredForLandLordJt = 0.0, txtReflactionPerForLandlordJt = 0.0;
            if ($F('txttotalrentalIncomeForLandLordJt') != "") {
                txttotalrentalIncomeForLandLordJt = $F('txttotalrentalIncomeForLandLordJt');
            }
            if ($F('txtReflactionrequiredForLandLordJt') != "") {
                txtReflactionrequiredForLandLordJt = $F('txtReflactionrequiredForLandLordJt');
            }
            if ($F('txtReflactionPerForLandlordJt') != "") {
                txtReflactionPerForLandlordJt = $F('txtReflactionPerForLandlordJt');
            }
            var objLandLordJt = new Object();
            objLandLordJt.AutoLoanMasterId = $F('txtAnalystMasterID');
            objLandLordJt.EmployerCatagoryId = $F('cmbEmployeeForLandLordJt');
            objLandLordJt.SectorId = $F('cmbSubsectorForLandLordJt');
            objLandLordJt.IncomeAssId = $F('cmbIncomeAssementForLandLordJt');
            objLandLordJt.TotalRentalIncome = txttotalrentalIncomeForLandLordJt;
            objLandLordJt.ReflectionRequired = txtReflactionrequiredForLandLordJt;
            objLandLordJt.Percentage = txtReflactionPerForLandlordJt;
            objLandLordJt.Remarks = $F('txtAreaRemarksForLandlordJt');
            objLandLordJt.AccountType = 2;
            objLandLordJt.Property = saveObjectHelper.getPropertyList('tblPropertyJt', 'Jt');
            landlordArray.push(objLandLordJt);
        }

        return landlordArray;

    },
    //create Verified rent For Land lord object
    getPropertyList: function(tableName, accountType) {
        var propertyArray = [];
        if (tableName != "") {

            var tableProperty = $(tableName);
            var rowCount = tableProperty.rows.length;
            var verifiedRent = 0, share = 0;
            // debugger;
            for (var i = 1; i <= rowCount - 2; i++) {
                var objProperty = new Object();
                if (util.isFloat($F('txtVerifiedrent' + i + 'ForLandLord' + accountType)) && !util.isEmpty($F('txtVerifiedrent' + i + 'ForLandLord' + accountType))) {
                    verifiedRent = $F('txtVerifiedrent' + i + 'ForLandLord' + accountType);
                }
                if (util.isFloat($F('txtShare' + i + 'ForLandLord' + accountType)) && !util.isEmpty($F('txtShare' + i + 'ForLandLord' + accountType))) {
                    share = $F('txtShare' + i + 'ForLandLord' + accountType);
                }

                objProperty.Property = verifiedRent;
                objProperty.Share = share;
                propertyArray.push(objProperty);
                verifiedRent = 0;
                share = 0;

            }

        }
        return propertyArray;
    },

    //Income For Bank Statement
    IncomeForBankStatement: function() {
        var incomeForBankStatementList = [];
        var jtPrimaryProfession = $F("txtJtProfessionId");
        var prPrimaryProfession = $F("txtPrProfessionId");
        var prOtherProfession = $F("txtPrOtherProfessionId");
        var reasonOfJointIncome = $F("cmbReasonofJointApplicant");
        //create bussiness man Tab object for Primary Applicant
        if (prPrimaryProfession == "1" || prOtherProfession == "1") {
            for (var i = 0; i < Page.prCount; i++) {
                var identityFieldForBmPr = i + 1;
                if (identityFieldForBmPr < 7) {
                    var objincomeForBankStatement = new Object();
                    objincomeForBankStatement.AnalystMasterId = $F('txtAnalystMasterID');
                    if (objincomeForBankStatement.AnalystMasterId == "") {
                        objincomeForBankStatement.AnalystMasterId = $F('txtAnalystMasterID');
                    }
                    objincomeForBankStatement.BankStatementId = $F('txtBankStatementIdBMPr' + identityFieldForBmPr);
                    objincomeForBankStatement.TotalCTO12 = $F('txtTotalCTOFor12MonthBank' + identityFieldForBmPr);
                    objincomeForBankStatement.AvgCTO12 = $F('txtAverageCTO12MonthBank' + identityFieldForBmPr);
                    objincomeForBankStatement.AvgBalance12 = $F('txtAverageBalanceFor12MonthBank' + identityFieldForBmPr);
                    objincomeForBankStatement.TotalCTO6 = $F('txtTotalCTOFor6MonthBank' + identityFieldForBmPr);
                    objincomeForBankStatement.AvgCTO6 = $F('txtAverageCTO6MonthBank' + identityFieldForBmPr);
                    objincomeForBankStatement.AvgBalance6 = $F('txtAverageBalanceFor6MonthBank' + identityFieldForBmPr);
                    objincomeForBankStatement.ConsideredCTO = $F('cmbConsideredCtoForPrBank' + identityFieldForBmPr);
                    objincomeForBankStatement.ConsideredBalance = $F('cmbConsideredBalanceForPrBank' + identityFieldForBmPr);
                    objincomeForBankStatement.ConsideredMargin = $F('txtConsiderMarginForPrimaryBank' + identityFieldForBmPr);
                    objincomeForBankStatement.GrossIncome = $F('txtgrossincomeForPrimaryBank' + identityFieldForBmPr);
                    objincomeForBankStatement.NetAfterPayment = $F('txtNetAfterPaymentForPrimaryBank' + identityFieldForBmPr);
                    objincomeForBankStatement.Share = $F('txtShareForPrimaryBank' + identityFieldForBmPr);
                    objincomeForBankStatement.NetIncome = $F('txtNetincomeForPrimaryBank' + identityFieldForBmPr);
                    objincomeForBankStatement.IncomeAssessmentMethod = $F('cmbIncomeAssessmentForPrimaryBank' + identityFieldForBmPr);
                    objincomeForBankStatement.Industry = $F('cmbIndustryForPrimaryBank' + identityFieldForBmPr);
                    objincomeForBankStatement.SubSector = $F('cmbSubsectorForPrimaryBank' + identityFieldForBmPr);
                    objincomeForBankStatement.Remarks = $F('tarearemarksForIncomeGeneratorBank' + identityFieldForBmPr);

                    if (objincomeForBankStatement.IncomeAssessmentMethod == "1") {
                        objincomeForBankStatement.AvgBalance1st9m = $F('txt1st9monthForPrimaryBank' + identityFieldForBmPr);
                        objincomeForBankStatement.AvgBalance1st6m = $F('txt1st6MonthForPrimaryBank' + identityFieldForBmPr);
                        objincomeForBankStatement.AvgBalanceLast6m = $F('txtLast6monthForPrimaryBank' + identityFieldForBmPr);
                        objincomeForBankStatement.AvgBalanceLast3m = $F('txtLast3monthForPrimaryBank' + identityFieldForBmPr);
                    }

                    objincomeForBankStatement.Type = 1;
                    objincomeForBankStatement.TabType = 1;
                    //Create OverDraft objec for Off Scb and On scb
                    objincomeForBankStatement.AutoAnOverDraftFacility = saveObjectHelper.CreateOverdraftObjectList(identityFieldForBmPr, 'Pr', 'BM');
                    objincomeForBankStatement.AutoBankStatementAvgTotalList = saveObjectHelper.CreateAverageTotalObjectList(identityFieldForBmPr, 'Pr', 'BM');
                    objincomeForBankStatement.AutoPreviousRePaymentHistory = saveObjectHelper.CreateOverDraftFacilityListForRepaymentHistory(identityFieldForBmPr, 'Pr', 'BM');
                    incomeForBankStatementList.push(objincomeForBankStatement);

                }
            }

        }

        //Create bussiness man Tab object For Joint Applicant
        if (jtPrimaryProfession == "1" && reasonOfJointIncome == "1") {
            for (var i = 0; i < Page.jtCount; i++) {
                var identityFieldForBmJt = i + 1;
                if (identityFieldForBmJt < 7) {
                    var objincomeForBankStatementBmJt = new Object();
                    objincomeForBankStatementBmJt.AnalystMasterId = $F('txtAnalystMasterID');
                    objincomeForBankStatementBmJt.BankStatementId = $F('txtBankStatementIdBMJt' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.TotalCTO12 = $F('txtTotalCTOFor12MonthJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.AvgCTO12 = $F('txtAverageCTOFor12MonthForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.AvgBalance12 = $F('txtAverageBalance12MonthForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.TotalCTO6 = $F('txtTotalCTOFor6MonthJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.AvgCTO6 = $F('txtAverageCTOFor6MonthForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.AvgBalance6 = $F('txtAverageBalance6MonthForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.ConsideredCTO = $F('cmbConsideredCtoForJtBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.ConsideredBalance = $F('cmbConsideredBalanceForJtBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.ConsideredMargin = $F('txtConsideredMarginBMForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.GrossIncome = $F('txtgrossIncomeForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.NetAfterPayment = $F('txtNetAfterPaymentForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.Share = $F('txtShareForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.NetIncome = $F('txtNetIncomeForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.IncomeAssessmentMethod = $F('cmbIncomeAssementForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.Industry = $F('cmbIndustryForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.SubSector = $F('cmbSubSectorForJointBank' + identityFieldForBmJt);
                    objincomeForBankStatementBmJt.Remarks = $F('txtRemark1ForJointBank' + identityFieldForBmJt);
                    if (objincomeForBankStatementBmJt.IncomeAssessmentMethod == "1") {
                        objincomeForBankStatementBmJt.AvgBalance1st9m = $F('txtAVGBalanceFor1st9MontForJointBank' + identityFieldForBmJt);
                        objincomeForBankStatementBmJt.AvgBalance1st6m = $F('txtAVGBalanceFor1st6MontForJointBank' + identityFieldForBmJt);
                        objincomeForBankStatementBmJt.AvgBalanceLast6m = $F('txtAVGBalanceForLast6MontForJointBank' + identityFieldForBmJt);
                        objincomeForBankStatementBmJt.AvgBalanceLast3m = $F('txtConsideredMarginForJointBank' + identityFieldForBmJt);
                    }

                    objincomeForBankStatementBmJt.Type = 2;
                    objincomeForBankStatementBmJt.TabType = 1;
                    //Create OverDraft objec for Off Scb and On scb
                    objincomeForBankStatementBmJt.AutoAnOverDraftFacility = saveObjectHelper.CreateOverdraftObjectList(identityFieldForBmJt, 'Jt', 'BM');
                    objincomeForBankStatementBmJt.AutoBankStatementAvgTotalList = saveObjectHelper.CreateAverageTotalObjectList(identityFieldForBmJt, 'Jt', 'BM');
                    objincomeForBankStatementBmJt.AutoPreviousRePaymentHistory = saveObjectHelper.CreateOverDraftFacilityListForRepaymentHistory(identityFieldForBmJt, 'Jt', 'BM');
                    incomeForBankStatementList.push(objincomeForBankStatementBmJt);
                }
            }
        }

        //Create SelfEmployed Tab Object for Primary Applicant
        if (prPrimaryProfession == "3" || prOtherProfession == "3" || prPrimaryProfession == "4" || prOtherProfession == "4" || prPrimaryProfession == "6" || prOtherProfession == "6") {
            for (var i = 0; i < Page.prCount; i++) {
                var identityFieldForSePr = i + 1;
                if (identityFieldForSePr < 7) {
                    var objincomeForBankStatementSePr = new Object();
                    objincomeForBankStatementSePr.AnalystMasterId = $F('txtAnalystMasterID');
                    if (objincomeForBankStatementSePr.AnalystMasterId == "") {
                        objincomeForBankStatementSePr.AnalystMasterId = $F('txtAnalystMasterID');
                    }
                    objincomeForBankStatementSePr.BankStatementId = $F('txtBankStatementIdSEPr' + identityFieldForSePr);
                    objincomeForBankStatementSePr.TotalCTO12 = $F('txtTotalCTOForSEPr12MonthBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.AvgCTO12 = $F('txtAverageCTOForSEPr12MonthBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.AvgBalance12 = $F('txtAverageBalanceForSEPr12MonthBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.TotalCTO6 = $F('txtTotalCTOForSEPr6MonthBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.AvgCTO6 = $F('txtAverageCTOForSEPr6MonthBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.AvgBalance6 = $F('txtAverageBalanceForSEPr6MonthBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.ConsideredCTO = $F('cmbConsideredCtoForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.ConsideredBalance = $F('cmbConsideredBalanceForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.ConsideredMargin = $F('txtConsiderMarginForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.GrossIncome = $F('txtgrossincomeForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.NetAfterPayment = $F('txtNetAfterPaymentForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.Share = $F('txtShareForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.NetIncome = $F('txtNetincomeForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.IncomeAssessmentMethod = $F('cmbIncomeAssessmentForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.Industry = $F('cmbIndustryForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.SubSector = $F('cmbSubsectorForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.Remarks = $F('txtremarksForSEPrBank' + identityFieldForSePr);
                    objincomeForBankStatementSePr.PrivateIncome = $F('cmbPrivateIncomeForSEPrBank' + identityFieldForSePr);
                    if (objincomeForBankStatementSePr.IncomeAssessmentMethod == "1") {
                        objincomeForBankStatementSePr.AvgBalance1st9m = $F('txt1st9monthForSEPrBank' + identityFieldForSePr);
                        objincomeForBankStatementSePr.AvgBalance1st6m = $F('txt1st6MonthForSEPrBank' + identityFieldForSePr);
                        objincomeForBankStatementSePr.AvgBalanceLast6m = $F('txtLast6monthForSEPrBank' + identityFieldForSePr);
                        objincomeForBankStatementSePr.AvgBalanceLast3m = $F('txtLast3monthForSEPrBank' + identityFieldForSePr);
                    }

                    objincomeForBankStatementSePr.Type = 1;
                    objincomeForBankStatementSePr.TabType = 2;
                    //Create OverDraft objec for Off Scb and On scb
                    objincomeForBankStatementSePr.AutoAnOverDraftFacility = saveObjectHelper.CreateOverdraftObjectList(identityFieldForSePr, 'Pr', 'SE');
                    objincomeForBankStatementSePr.AutoBankStatementAvgTotalList = saveObjectHelper.CreateAverageTotalObjectList(identityFieldForSePr, 'Pr', 'SE');
                    objincomeForBankStatementSePr.AutoPreviousRePaymentHistory = saveObjectHelper.CreateOverDraftFacilityListForRepaymentHistory(identityFieldForSePr, 'Pr', 'SE');
                    if (objincomeForBankStatementSePr.Industry == "12" && objincomeForBankStatementSePr.SubSector == "143" && objincomeForBankStatementSePr.PrivateIncome == "1") {
                        objincomeForBankStatementSePr.AutoPracticingDoctor = saveObjectHelper.CreateAutoPracticingDoctor(identityFieldForSePr, 'Pr', 'SE');
                    }
                    if (objincomeForBankStatementSePr.Industry == "12" && objincomeForBankStatementSePr.SubSector == "150" && objincomeForBankStatementSePr.PrivateIncome == "1") {
                        objincomeForBankStatementSePr.AutoTeacherIncome = saveObjectHelper.CreateTeacherIncome(identityFieldForSePr, 'Pr', 'SE');
                    }


                    incomeForBankStatementList.push(objincomeForBankStatementSePr);
                }
            }
        }

        //Create Self Employed Tab Object For Joint Applicant
        if ((jtPrimaryProfession == "3" || jtPrimaryProfession == "4" || jtPrimaryProfession == "6") && reasonOfJointIncome == "1") {
            for (var i = 0; i < Page.jtCount; i++) {
                var identityFieldForSeJt = i + 1;
                if (identityFieldForSeJt < 7) {
                    var objincomeForBankStatementSeJt = new Object();
                    objincomeForBankStatementSeJt.AnalystMasterId = $F('txtAnalystMasterID');
                    if (objincomeForBankStatementSeJt.AnalystMasterId == "") {
                        objincomeForBankStatementSeJt.AnalystMasterId = $F('txtAnalystMasterID');
                    }
                    objincomeForBankStatementSeJt.BankStatementId = $F('txtBankStatementIdSEJt' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.TotalCTO12 = $F('txtTotalCTOForSEJt12MonthBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.AvgCTO12 = $F('txtAverageCTOForSEJt12MonthBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.AvgBalance12 = $F('txtAverageBalanceForSEJt12MonthBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.TotalCTO6 = $F('txtTotalCTOForSEJt6MonthBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.AvgCTO6 = $F('txtAverageCTOForSEJt6MonthBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.AvgBalance6 = $F('txtAverageBalanceForSEJt6MonthBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.ConsideredCTO = $F('cmbConsideredCtoForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.ConsideredBalance = $F('cmbConsideredBalanceForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.ConsideredMargin = $F('txtConsiderMarginForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.GrossIncome = $F('txtgrossincomeForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.NetAfterPayment = $F('txtNetAfterPaymentForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.Share = $F('txtShareForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.NetIncome = $F('txtNetincomeForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.IncomeAssessmentMethod = $F('cmbIncomeAssessmentForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.Industry = $F('cmbIndustryForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.SubSector = $F('cmbSubsectorForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.PrivateIncome = $F('cmbPrivateIncomeForSEJtBank' + identityFieldForSeJt);
                    objincomeForBankStatementSeJt.Remarks = $F('txtremarksForSEJtBank' + identityFieldForSeJt);
                    if (objincomeForBankStatementSeJt.IncomeAssessmentMethod == "1") {
                        objincomeForBankStatementSeJt.AvgBalance1st9m = $F('txt1st9monthForSEJtBank' + identityFieldForSeJt);
                        objincomeForBankStatementSeJt.AvgBalance1st6m = $F('txt1st6MonthForSEJtBank' + identityFieldForSeJt);
                        objincomeForBankStatementSeJt.AvgBalanceLast6m = $F('txtLast6monthForSEJtBank' + identityFieldForSeJt);
                        objincomeForBankStatementSeJt.AvgBalanceLast3m = $F('txtLast3monthForSEJtBank' + identityFieldForSeJt);
                    }

                    objincomeForBankStatementSeJt.Type = 2;
                    objincomeForBankStatementSeJt.TabType = 2;
                    //Create OverDraft objec for Off Scb and On scb
                    objincomeForBankStatementSeJt.AutoAnOverDraftFacility = saveObjectHelper.CreateOverdraftObjectList(identityFieldForSeJt, 'Jt', 'SE');
                    objincomeForBankStatementSeJt.AutoBankStatementAvgTotalList = saveObjectHelper.CreateAverageTotalObjectList(identityFieldForSeJt, 'Jt', 'SE');
                    objincomeForBankStatementSeJt.AutoPreviousRePaymentHistory = saveObjectHelper.CreateOverDraftFacilityListForRepaymentHistory(identityFieldForSeJt, 'Jt', 'SE');
                    if (objincomeForBankStatementSeJt.Industry == "12" && objincomeForBankStatementSeJt.SubSector == "143" && objincomeForBankStatementSeJt.PrivateIncome == "1") {
                        objincomeForBankStatementSeJt.AutoPracticingDoctor = saveObjectHelper.CreateAutoPracticingDoctor(identityFieldForSeJt, 'Jt', 'SE');
                    }
                    if (objincomeForBankStatementSeJt.Industry == "12" && objincomeForBankStatementSeJt.SubSector == "150" && objincomeForBankStatementSeJt.PrivateIncome == "1") {
                        objincomeForBankStatementSeJt.AutoTeacherIncome = saveObjectHelper.CreateTeacherIncome(identityFieldForSeJt, 'Jt', 'SE');
                    }


                    incomeForBankStatementList.push(objincomeForBankStatementSeJt);
                }

            }
        }
        return incomeForBankStatementList;
    },
    //Create OverDraft Object
    CreateOverdraftObjectList: function(identityField, accountType, tabName) {
        var overdraftObjectList = [];
        var consider = "";
        //Primary Applicant For Bussiness Man
        if (accountType == "Pr" && tabName == "BM") {
            //On Scb
            var tablePrAccount = $("tblOverDraftFacilitiesForPrBMWithScb" + identityField);
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                var objOverDraftBmPrScb = new Object();
                consider = $F("cmbConsider" + i + "ForBank" + identityField);
                if (consider == "1") {
                    objOverDraftBmPrScb.Consider = consider;
                    objOverDraftBmPrScb.Interest = $F("txtInterst" + i + "WithScbForBank" + identityField);
                    objOverDraftBmPrScb.LimitAmount = $F("txtLimt" + i + "WithScbForBank" + identityField);
                    objOverDraftBmPrScb.BankId = -1;
                    objOverDraftBmPrScb.BankStatementId = $F("txtBankStatementIdBMPr" + identityField);
                    objOverDraftBmPrScb.Type = "1";
                    overdraftObjectList.push(objOverDraftBmPrScb);
                }
            }
            //Off Scb
            var tablePrAccountOffScb = $("tblOverDraftFacilitiesForPrBMOffScb" + identityField);
            var rowCountOffScb = tablePrAccountOffScb.rows.length;
            for (var i = 1; i <= rowCountOffScb - 1; i++) {
                var objOverDraftBmPrOffScb = new Object();
                consider = $F("cmbConsider" + i + "ForOffScbBank" + identityField);
                if (consider == "1") {
                    objOverDraftBmPrOffScb.Consider = consider;
                    objOverDraftBmPrOffScb.Interest = $F("txtInterest" + i + "ForOffSCBBank" + identityField);
                    objOverDraftBmPrOffScb.LimitAmount = $F("txtLimit" + i + "ForOffSCBBank" + identityField);
                    objOverDraftBmPrOffScb.BankId = $F("cmbBankName" + i + "ForBussinessTabPrBank" + identityField);
                    objOverDraftBmPrOffScb.BankStatementId = $F("txtBankStatementIdBMPr" + identityField);
                    objOverDraftBmPrOffScb.Type = "2";
                    overdraftObjectList.push(objOverDraftBmPrOffScb);
                }
            }

        }
        //Joint Applicant For Bussiness Man
        else if (accountType == "Jt" && tabName == "BM") {
            //On Scb
            var tableJtAccount = $("tblOverDraftFacilitiesForJtBMWithScb" + identityField);
            var rowJtCount = tableJtAccount.rows.length;
            for (var i = 1; i <= rowJtCount - 1; i++) {
                var objOverDraftBmJtScb = new Object();
                consider = $F("cmbInterest" + i + "ForJointBank" + identityField);
                if (consider == "1") {
                    objOverDraftBmJtScb.Consider = consider;
                    objOverDraftBmJtScb.Interest = $F("txtInterest" + i + "ForJointBank" + identityField);
                    objOverDraftBmJtScb.LimitAmount = $F("txtLimit" + i + "ForJointBank" + identityField);
                    objOverDraftBmJtScb.BankId = -1;
                    objOverDraftBmJtScb.BankStatementId = $F("txtBankStatementIdBMJt" + identityField);
                    objOverDraftBmJtScb.Type = "1";
                    overdraftObjectList.push(objOverDraftBmJtScb);
                }
            }
            //Off Scb
            var tableJtAccountOffScb = $("tblOverDraftFacilitiesForJtBMOffScb" + identityField);
            var rowCountJtOffScb = tableJtAccountOffScb.rows.length;
            for (var i = 1; i <= rowCountJtOffScb - 1; i++) {
                var objOverDraftBmJtOffScb = new Object();
                consider = $F("cmbConsiderOffScbBM" + i + "ForJointBank" + identityField);
                if (consider == "1") {
                    objOverDraftBmJtOffScb.Consider = consider;
                    objOverDraftBmJtOffScb.Interest = $F("txtInterestOffSCB" + i + "ForJointBank" + identityField);
                    objOverDraftBmJtOffScb.LimitAmount = $F("txtLimitOffScb" + i + "ForJointBank" + identityField);
                    objOverDraftBmJtOffScb.BankId = $F("cmbBankName" + i + "ForBussinessTabJtBank" + identityField);
                    objOverDraftBmJtOffScb.BankStatementId = $F("txtBankStatementIdBMJt" + identityField);
                    objOverDraftBmJtOffScb.Type = "2";
                    overdraftObjectList.push(objOverDraftBmJtOffScb);
                }
            }

        }
        // Primary Applicant for Self employed
        if (accountType == "Pr" && tabName == "SE") {
            //On Scb
            var tablePrAccountSe = $("tblOverDraftFacilitiesForPrSEWithScb" + identityField);
            var rowCountSe = tablePrAccountSe.rows.length;
            for (var i = 1; i <= rowCountSe - 1; i++) {
                var objOverDraftSePrScb = new Object();
                consider = $F("cmbConsider" + i + "WithScbForSEBank" + identityField);
                if (consider == "1") {
                    objOverDraftSePrScb.Consider = consider;
                    objOverDraftSePrScb.Interest = $F("txtInterst" + i + "WithScbForSEBank" + identityField);
                    objOverDraftSePrScb.LimitAmount = $F("txtLimt" + i + "WithScbForSEBank" + identityField);
                    objOverDraftSePrScb.BankId = -1;
                    objOverDraftSePrScb.BankStatementId = $F("txtBankStatementIdSEPr" + identityField);
                    objOverDraftSePrScb.Type = "1";
                    overdraftObjectList.push(objOverDraftSePrScb);
                }
            }
            //Off Scb
            var tableSePrAccountOffScb = $("tblOverDraftFacilitiesForPrSEOffScb" + identityField);
            var rowCountSeOffScb = tableSePrAccountOffScb.rows.length;
            for (var i = 1; i <= rowCountSeOffScb - 1; i++) {
                var objOverDraftSePrOffScb = new Object();
                consider = $F("cmbConsider" + i + "ForSEOffScbBank" + identityField);
                if (consider == "1") {
                    objOverDraftSePrOffScb.Consider = consider;
                    objOverDraftSePrOffScb.Interest = $F("txtInterest" + i + "ForSEOffSCBBank" + identityField);
                    objOverDraftSePrOffScb.LimitAmount = $F("txtLimit" + i + "ForSEOffSCBBank" + identityField);
                    objOverDraftSePrOffScb.BankId = $F("cmbBankName" + i + "ForSEPrBank" + identityField);
                    objOverDraftSePrOffScb.BankStatementId = $F("txtBankStatementIdSEPr" + identityField);
                    objOverDraftSePrOffScb.Type = "2";
                    overdraftObjectList.push(objOverDraftSePrOffScb);
                }
            }

        }

        // Joint Applicant for Self employed
        if (accountType == "Jt" && tabName == "SE") {
            //On Scb
            var tableJtAccountSe = $("tblOverDraftFacilitiesForJtSEWithScb" + identityField);
            var rowCountSeJt = tableJtAccountSe.rows.length;
            for (var i = 1; i <= rowCountSeJt - 1; i++) {
                var objOverDraftSeJtScb = new Object();
                consider = $F("cmbConsider" + i + "WithScbForSEJtBank" + identityField);
                if (consider == "1") {
                    objOverDraftSeJtScb.Consider = consider;
                    objOverDraftSeJtScb.Interest = $F("txtInterst" + i + "WithScbForSEJtBank" + identityField);
                    objOverDraftSeJtScb.LimitAmount = $F("txtLimt" + i + "WithScbForSEJtBank" + identityField);
                    objOverDraftSeJtScb.BankId = -1;
                    objOverDraftSeJtScb.BankStatementId = $F("txtBankStatementIdSEJt" + identityField);
                    objOverDraftSeJtScb.Type = "1";
                    overdraftObjectList.push(objOverDraftSeJtScb);
                }
            }
            //Off Scb
            var tableSeJtAccountOffScb = $("tblOverDraftFacilitiesForJtSEOffScbBank" + identityField);
            var rowCountSeJtOffScb = tableSeJtAccountOffScb.rows.length;
            for (var i = 1; i <= rowCountSeJtOffScb - 1; i++) {
                var objOverDraftSeJtOffScb = new Object();
                consider = $F("cmbConsider" + i + "ForSEJtOffScbBank" + identityField);
                if (consider == "1") {
                    objOverDraftSeJtOffScb.Consider = consider;
                    objOverDraftSeJtOffScb.Interest = $F("txtInterest" + i + "ForSEJtOffSCBBank" + identityField);
                    objOverDraftSeJtOffScb.LimitAmount = $F("txtLimit" + i + "ForSEJtOffSCBBank" + identityField);
                    objOverDraftSeJtOffScb.BankId = $F("cmbBankName" + i + "ForSEJtBank" + identityField);
                    objOverDraftSeJtOffScb.BankStatementId = $F("txtBankStatementIdSEJt" + identityField);
                    objOverDraftSeJtOffScb.Type = "2";
                    overdraftObjectList.push(objOverDraftSeJtOffScb);
                }
            }

        }
        return overdraftObjectList;

    },

    //Average Bank Statement Object--------------------------------------------------
    CreateAverageTotalObjectList: function(identity, accountType, tabName) {
        //Primary Avg Total for Bussiness man
        var averageTotalObjectListArray = [];
        if (accountType == "Pr" && tabName == "BM") {
            var tableAvgBmPr = $("tblPrBankStatementForIncomeGenerator" + identity);
            var rowCountBmPr = tableAvgBmPr.rows.length;
            for (var i = 1; i <= rowCountBmPr - 1; i++) {
                var avgTotal = $F("txtAdjustment" + i + "ForIncomeGeneratorBank" + identity);
                if (avgTotal != "0" && avgTotal != "") {
                    var objAverageTotal = new Object();
                    objAverageTotal.Adjustment = avgTotal;
                    objAverageTotal.BankStatementAvgTotalId = $F("hdn" + i + "ForBMPrBank" + identity);
                    averageTotalObjectListArray.push(objAverageTotal);
                }
            }

        }
        //Joint Avg Total for bussinessman Tab
        if (accountType == "Jt" && tabName == "BM") {
            var tableAvgBmJt = $("tblJtBankStatementForIncomeGenerator" + identity);
            var rowCountBmJt = tableAvgBmJt.rows.length;
            for (var i = 1; i <= rowCountBmJt - 1; i++) {
                var avgTotalBmJt = $F("txtAdjustment" + i + "ForJointBank" + identity);
                if (avgTotalBmJt != "0" && avgTotalBmJt != "") {
                    var objAverageTotalBmJt = new Object();
                    objAverageTotalBmJt.AVGTotal = avgTotalBmJt;
                    objAverageTotalBmJt.BankStatementAvgTotalId = $F("hdn" + i + "ForBMJtBank" + identity);
                    averageTotalObjectListArray.push(objAverageTotalBmJt);
                }
            }
        }
        //Primary Avg Total for SelfEmployed
        if (accountType == "Pr" && tabName == "SE") {
            var tableAvgSePr = $("tblPrBankStatementForSEBank" + identity);
            var rowCountSePr = tableAvgSePr.rows.length;
            for (var i = 1; i <= rowCountSePr - 1; i++) {
                var avgTotalSePr = $F("txtAdjustment" + i + "ForSEPrBank" + identity);
                if (avgTotalSePr != "0" && avgTotalSePr != "") {
                    var objAverageTotalSePr = new Object();
                    objAverageTotalSePr.AVGTotal = avgTotalSePr;
                    objAverageTotalSePr.BankStatementAvgTotalId = $F("hdn" + i + "ForSEPrBank" + identity);
                    averageTotalObjectListArray.push(objAverageTotalSePr);
                }
            }
        }
        //joint Avg Total for Self employed
        if (accountType == "Jt" && tabName == "SE") {
            var tableAvgSeJt = $("tblJtBankStatementForSEBank" + identity);
            var rowCountSeJt = tableAvgSeJt.rows.length;
            for (var i = 1; i <= rowCountSeJt - 1; i++) {
                var avgTotalSeJt = $F("txtAdjustment" + i + "ForSEJtBank" + identity);
                if (avgTotalSeJt != "0" && avgTotalSeJt != "") {
                    var objAverageTotalSeJt = new Object();
                    objAverageTotalSeJt.AVGTotal = avgTotalSeJt;
                    objAverageTotalSeJt.BankStatementAvgTotalId = $F("hdn" + i + "ForSEJtBank" + identity);
                    averageTotalObjectListArray.push(objAverageTotalSeJt);
                }
            }
        }
        return averageTotalObjectListArray;
    },

    //Create Over Draft Object List For Emi Previous repayment History
    CreateOverDraftFacilityListForRepaymentHistory: function(identity, accountType, tabName) {
        //var previouserepaymentHistory = [];
        var objPreviouserepaymentHistory = new Object();
        var totalEmiClosingConsidered = 0;
        if (accountType == "Pr" && tabName == "BM") {
            totalEmiClosingConsidered = $F("txtTotalEMIClosingConsideredForPrimaryBank" + identity);
            if (totalEmiClosingConsidered != "0" && totalEmiClosingConsidered != "") {
                objPreviouserepaymentHistory.AutoLoanMasterId = $F('txtAnalystMasterID');
                if (objPreviouserepaymentHistory.AutoLoanMasterId == "") {
                    objPreviouserepaymentHistory.AutoLoanMasterId == 0;
                }
                objPreviouserepaymentHistory.BankStatementId = $F('txtBankStatementIdBMPr' + identity);
                objPreviouserepaymentHistory.BankId = $F('cmbBankNameForBank' + identity + 'IncomeGenerator');
                objPreviouserepaymentHistory.OffScb_Total = $F('txttotalForOffSCBForPrimaryBank' + identity);
                objPreviouserepaymentHistory.OnScb_Total = $F('txtTotal1OnSCBRepHisForPrimaryBank' + identity);
                if (objPreviouserepaymentHistory.OffScb_Total == "") {
                    objPreviouserepaymentHistory.OffScb_Total = "0";
                }
                if (objPreviouserepaymentHistory.OnScb_Total == "") {
                    objPreviouserepaymentHistory.OnScb_Total = "0";
                }
                objPreviouserepaymentHistory.TotalEMIClosingConsidered = totalEmiClosingConsidered;
                objPreviouserepaymentHistory.Remarks = $F('txtremarks1ForPrimaryBank' + identity);
                objPreviouserepaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb = saveObjectHelper.CreateAutoSubPreviousRepaymentHistoryOffScbOnScb(identity, accountType, tabName);

            }
        }
        if (accountType == "Jt" && tabName == "BM") {
            totalEmiClosingConsidered = $F("txtTotalEMIClosingConsideredPrevRepHisForJointBank" + identity);
            if (totalEmiClosingConsidered != "0" && totalEmiClosingConsidered != "") {
                objPreviouserepaymentHistory.AutoLoanMasterId = $F('txtAnalystMasterID');
                if (objPreviouserepaymentHistory.AutoLoanMasterId == "") {
                    objPreviouserepaymentHistory.AutoLoanMasterId = 0;
                }
                objPreviouserepaymentHistory.BankStatementId = $F('txtBankStatementIdBMJt' + identity);
                objPreviouserepaymentHistory.BankId = $F('cmbBankName1ForJointBank' + identity);
                objPreviouserepaymentHistory.OffScb_Total = $F('txtTotalOffSCBJointBank' + identity);
                objPreviouserepaymentHistory.OnScb_Total = $F('txtTotalOnScbForJointBank' + identity);
                if (objPreviouserepaymentHistory.OffScb_Total == "") {
                    objPreviouserepaymentHistory.OffScb_Total = "0";
                }
                if (objPreviouserepaymentHistory.OnScb_Total == "") {
                    objPreviouserepaymentHistory.OnScb_Total = "0";
                }
                objPreviouserepaymentHistory.TotalEMIClosingConsidered = totalEmiClosingConsidered;
                objPreviouserepaymentHistory.Remarks = $F('txtRemarksPrevRepHisForJointBank' + identity);
                objPreviouserepaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb = saveObjectHelper.CreateAutoSubPreviousRepaymentHistoryOffScbOnScb(identity, accountType, tabName);
            }

        }
        if (accountType == "Pr" && tabName == "SE") {
            totalEmiClosingConsidered = $F("txtTotalEMIClosingConsideredForSEPrBank" + identity);
            if (totalEmiClosingConsidered != "0" && totalEmiClosingConsidered != "") {
                objPreviouserepaymentHistory.AutoLoanMasterId = $F('txtAnalystMasterID');
                if (objPreviouserepaymentHistory.AutoLoanMasterId == "") {
                    objPreviouserepaymentHistory.AutoLoanMasterId = 0;
                }
                objPreviouserepaymentHistory.BankStatementId = $F('txtBankStatementIdSEPr' + identity);
                objPreviouserepaymentHistory.BankId = $F('cmbBankNameForSEBank' + identity);
                objPreviouserepaymentHistory.OffScb_Total = $F('txttotalForOffSCBForSEPrBank' + identity);
                objPreviouserepaymentHistory.OnScb_Total = $F('txtTotal1OnSCBRepHisForSEPrBank' + identity);
                if (objPreviouserepaymentHistory.OffScb_Total == "") {
                    objPreviouserepaymentHistory.OffScb_Total = "0";
                }
                if (objPreviouserepaymentHistory.OnScb_Total == "") {
                    objPreviouserepaymentHistory.OnScb_Total = "0";
                }
                objPreviouserepaymentHistory.TotalEMIClosingConsidered = totalEmiClosingConsidered;
                objPreviouserepaymentHistory.Remarks = $F('txtremarks1ForSEPrBank' + identity);
                objPreviouserepaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb = saveObjectHelper.CreateAutoSubPreviousRepaymentHistoryOffScbOnScb(identity, accountType, tabName);
            }
        }
        if (accountType == "Jt" && tabName == "SE") {
            totalEmiClosingConsidered = $F("txtTotalEMIClosingConsideredForSEJtBank" + identity);
            if (totalEmiClosingConsidered != "0" && totalEmiClosingConsidered != "") {
                objPreviouserepaymentHistory.AutoLoanMasterId = $F('txtAnalystMasterID');
                if (objPreviouserepaymentHistory.AutoLoanMasterId == "") {
                    objPreviouserepaymentHistory.AutoLoanMasterId = 0;
                }
                objPreviouserepaymentHistory.BankStatementId = $F('txtBankStatementIdSEJt' + identity);
                objPreviouserepaymentHistory.BankId = $F('cmbBankNameForSEJtBank' + identity);
                objPreviouserepaymentHistory.OffScb_Total = $F('txttotalForOffSCBForSEJtBank' + identity);
                objPreviouserepaymentHistory.OnScb_Total = $F('txtTotal1OnSCBRepHisForSEJtBank' + identity);
                if (objPreviouserepaymentHistory.OffScb_Total == "") {
                    objPreviouserepaymentHistory.OffScb_Total = "0";
                }
                if (objPreviouserepaymentHistory.OnScb_Total == "") {
                    objPreviouserepaymentHistory.OnScb_Total = "0";
                }
                objPreviouserepaymentHistory.TotalEMIClosingConsidered = totalEmiClosingConsidered;
                objPreviouserepaymentHistory.Remarks = $F('txtremarks1ForSEJtBank' + identity);
                objPreviouserepaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb = saveObjectHelper.CreateAutoSubPreviousRepaymentHistoryOffScbOnScb(identity, accountType, tabName);
            }
        }
        return objPreviouserepaymentHistory;
    },

    //Repayment History Details.........................

    CreateAutoSubPreviousRepaymentHistoryOffScbOnScb: function(identity, accountType, tabName) {
        var previousRepaymentHistoryDetailsArray = [];
        //Bussiness Man Primary Start
        if (accountType == "Pr" && tabName == "BM") {
            for (var i = 1; i < 5; i++) {
                var offScbConsider = $F("cmbConsider" + i + "ForPrimaryOffScbBank" + identity);
                var offScbEmi = $F("txtEMI" + i + "ForOffSCBPrimaryBank" + identity);
                if (offScbConsider == "1" && offScbEmi != "" && offScbEmi != "0") {
                    var objRepaymentHistory = new Object();
                    objRepaymentHistory.BankId = $F("txtBank" + i + "BMPr" + identity);
                    objRepaymentHistory.Type = "2";
                    objRepaymentHistory.EMI = offScbEmi;
                    objRepaymentHistory.Consider = true;
                    previousRepaymentHistoryDetailsArray.push(objRepaymentHistory);
                }
                var onscbConsiderForBmPr = $F("cmbConsider" + i + "ForOnSCBReplHisForPrimaryBank" + identity);
                var onscbEmi = $F("txtEmi" + i + "ForOnScbForPrimaryBank" + identity);
                if (onscbConsiderForBmPr == "1" && onscbEmi != "" && onscbEmi != "0") {
                    var objRepaymentHistoryOnScbBm = new Object();
                    objRepaymentHistoryOnScbBm.BankId = "-1";
                    objRepaymentHistoryOnScbBm.Type = "1";
                    objRepaymentHistoryOnScbBm.EMI = onscbEmi;
                    objRepaymentHistoryOnScbBm.Consider = true;
                    previousRepaymentHistoryDetailsArray.push(objRepaymentHistoryOnScbBm);
                }

            }
        }
        //Bussiness Man Joint Start
        if (accountType == "Jt" && tabName == "BM") {
            for (var i = 1; i < 5; i++) {
                var offScbConsiderBmJt = $F("cmbConsiderOffScb" + i + "ForJointBank" + identity);
                var offScbEmiBmJt = $F("txtEmiOffScb" + i + "ForJointBank" + identity);
                if (offScbConsiderBmJt == "1" && offScbEmiBmJt != "" && offScbEmiBmJt != "0") {
                    var objRepaymentHistoryBmJtOffScb = new Object();
                    objRepaymentHistoryBmJtOffScb.BankId = $F("txtBank" + i + "BMJt" + identity);
                    objRepaymentHistoryBmJtOffScb.Type = "2";
                    objRepaymentHistoryBmJtOffScb.EMI = offScbEmiBmJt;
                    objRepaymentHistoryBmJtOffScb.Consider = true;
                    previousRepaymentHistoryDetailsArray.push(objRepaymentHistoryBmJtOffScb);
                }
                var onscbConsiderForBmJt = $F("cmbConsiderOnScb" + i + "ForJointBank" + identity);
                var onscbEmiForBmJt = $F("txtEmiOnScb" + i + "ForJointBank" + identity);
                if (onscbConsiderForBmJt == "1" && onscbEmiForBmJt != "" && onscbEmiForBmJt != "0") {
                    var objRepaymentHistoryOnScbBmJt = new Object();
                    objRepaymentHistoryOnScbBmJt.BankId = "-1";
                    objRepaymentHistoryOnScbBmJt.Type = "1";
                    objRepaymentHistoryOnScbBmJt.EMI = onscbEmiForBmJt;
                    objRepaymentHistoryOnScbBmJt.Consider = true;
                    previousRepaymentHistoryDetailsArray.push(objRepaymentHistoryOnScbBmJt);
                }

            }
        }
        //Self Employed Primary Start
        if (accountType == "Pr" && tabName == "SE") {
            for (var i = 1; i < 5; i++) {
                var offScbConsiderSePr = $F("cmbConsider" + i + "ForSEPrOffScbBank" + identity);
                var offScbEmiSePr = $F("txtEMI" + i + "ForOffSCBSEPrBank" + identity);
                if (offScbConsiderSePr == "1" && offScbEmiSePr != "" && offScbEmiSePr != "0") {
                    var objRepaymentHistorySePrOffScb = new Object();
                    objRepaymentHistorySePrOffScb.BankId = $F("txtBank" + i + "SEPr" + identity);
                    objRepaymentHistorySePrOffScb.Type = "2";
                    objRepaymentHistorySePrOffScb.EMI = offScbEmiSePr;
                    objRepaymentHistorySePrOffScb.Consider = true;
                    previousRepaymentHistoryDetailsArray.push(objRepaymentHistorySePrOffScb);
                }
                var onscbConsiderForSePr = $F("cmbConsider" + i + "ForOnSCBReplHisForSEPrBank" + identity);
                var onscbEmiForSePr = $F("txtEMI" + i + "ForOffSCBSEPrBank" + identity);
                if (onscbConsiderForSePr == "1" && onscbEmiForSePr != "" && onscbEmiForSePr != "0") {
                    var objRepaymentHistoryOnScbSePr = new Object();
                    objRepaymentHistoryOnScbSePr.BankId = "-1";
                    objRepaymentHistoryOnScbSePr.Type = "1";
                    objRepaymentHistoryOnScbSePr.EMI = onscbEmiForSePr;
                    objRepaymentHistoryOnScbSePr.Consider = true;
                    previousRepaymentHistoryDetailsArray.push(objRepaymentHistoryOnScbSePr);
                }

            }
        }
        // Self Employed joint Start
        if (accountType == "Jt" && tabName == "SE") {
            for (var i = 1; i < 5; i++) {
                var offScbConsiderSeJt = $F("cmbConsider" + i + "ForSEJtRepHisOffScbBank" + identity);
                var offScbEmiSeJt = $F("txtEMI" + i + "ForOffSCBSEJtBank" + identity);
                if (offScbConsiderSeJt == "1" && offScbEmiSeJt != "" && offScbEmiSeJt != "0") {
                    var objRepaymentHistorySeJtOffScb = new Object();
                    objRepaymentHistorySeJtOffScb.BankId = $F("txtBank" + i + "SEJt" + identity);
                    objRepaymentHistorySeJtOffScb.Type = "2";
                    objRepaymentHistorySeJtOffScb.EMI = offScbEmiSeJt;
                    objRepaymentHistorySeJtOffScb.Consider = true;
                    previousRepaymentHistoryDetailsArray.push(objRepaymentHistorySeJtOffScb);
                }
                var onscbConsiderForSeJt = $F("cmbConsider" + i + "ForOnSCBReplHisForSEJtBank" + identity);
                var onscbEmiForSeJt = $F("txtEmi" + i + "ForOnScbForSEJtBank" + identity);
                if (onscbConsiderForSeJt == "1" && onscbEmiForSeJt != "" && onscbEmiForSeJt != "0") {
                    var objRepaymentHistoryOnScbSeJt = new Object();
                    objRepaymentHistoryOnScbSeJt.BankId = "-1";
                    objRepaymentHistoryOnScbSeJt.Type = "1";
                    objRepaymentHistoryOnScbSeJt.EMI = onscbEmiForSeJt;
                    objRepaymentHistoryOnScbSeJt.Consider = true;
                    previousRepaymentHistoryDetailsArray.push(objRepaymentHistoryOnScbSeJt);
                }

            }
        }
        return previousRepaymentHistoryDetailsArray;
    },

    //CreateAutoPracticingDoctor
    CreateAutoPracticingDoctor: function(identity, accountType, tabName) {
        var objAutoPracticingDoctor = new Object();
        if (accountType == "Pr" && tabName == "SE") {
            var totalIncomeSePr = $F("txtTotalIncomeForSEPrBank" + identity);
            if (totalIncomeSePr != "" && totalIncomeSePr != "0") {
                objAutoPracticingDoctor.TotalIncome = $F("txtTotalIncomeForSEPrBank" + identity);
                objAutoPracticingDoctor.OtherOTIncome = $F("txtTotalOtherIncomeForSEPrBank" + identity);
                objAutoPracticingDoctor.TotalChamberIncome = $F("txtTotalChamberIncomeForSEPrBank" + identity);
                objAutoPracticingDoctor.Type = "1";
                objAutoPracticingDoctor.BankStatementId = $F("txtBankStatementIdSEPr" + identity);
                objAutoPracticingDoctor.AutoLoanMasterId = $F("txtAnalystMasterID");
                if (objAutoPracticingDoctor.AutoLoanMasterId == "") {
                    objAutoPracticingDoctor.AutoLoanMasterId = 0;
                }
                objAutoPracticingDoctor.AutoChamberIncome = saveObjectHelper.CreateChamberIncome(identity, accountType, tabName);
            }
        }
        else if (accountType == "Jt" && tabName == "SE") {
            objAutoPracticingDoctor.TotalIncome = $F("txtTotalIncomeForSEJtBank" + identity);
            objAutoPracticingDoctor.OtherOTIncome = $F("txtTotalOtherIncomeForSEJtBank" + identity);
            objAutoPracticingDoctor.TotalChamberIncome = $F("txtTotalChamberIncomeForSEJtBank" + identity);
            objAutoPracticingDoctor.Type = "2";
            objAutoPracticingDoctor.BankStatementId = $F("txtBankStatementIdSEJt" + identity);
            objAutoPracticingDoctor.AutoLoanMasterId = $F("txtAnalystMasterID");
            if (objAutoPracticingDoctor.AutoLoanMasterId == "") {
                objAutoPracticingDoctor.AutoLoanMasterId = 0;
            }
            objAutoPracticingDoctor.AutoChamberIncome = saveObjectHelper.CreateChamberIncome(identity, accountType, tabName);
        }
        return objAutoPracticingDoctor;
    },
    //Create Chamber Details
    CreateChamberIncome: function(bankNo, accountType, tabName) {
        var resultArray = [];
        prChanberIncomeArray = [];
        var rowCount = 0;
        if (accountType == "Pr" && tabName == "SE") {
            var tablePrAccount = $("tblChamberIncomeForSEPrBank" + bankNo);
            rowCount = tablePrAccount.rows.length;
        } else if (accountType == "Jt" && tabName == "SE") {
            var tableJtAccount = $("tblChamberIncomeForSEJtBank" + bankNo);
            rowCount = tableJtAccount.rows.length;
        }

        if (rowCount > 0) {
            for (var i = 1; i <= rowCount - 1; i++) {

                var patientsOld = 0.0, feeOld = 0.0, totalOfPatientsOld = 0.0;
                var patientsNew = 0.0, feeNew = 0.0, totalOfPatientsNew = 0.0;
                var total = 0.0, dayMonth = 0, totalIncome = 0.0, totalChamberIncome = 0.0;

                var objAutoChamberIncome = new Object();


                if (util.isFloat($F('txtPatientsOld' + i + 'ForSE' + accountType + 'Bank' + bankNo)) && !util.isEmpty($F('txtPatientsOld' + i + 'ForSE' + accountType + 'Bank' + bankNo))) {
                    patientsOld = parseFloat($F('txtPatientsOld' + i + 'ForSE' + accountType + 'Bank' + bankNo));
                }
                if (util.isFloat($F('txtFeeOld' + i + 'ForSE' + accountType + 'Bank' + bankNo)) && !util.isEmpty($F('txtFeeOld' + i + 'ForSE' + accountType + 'Bank' + bankNo))) {
                    feeOld = parseFloat($F('txtFeeOld' + i + 'ForSE' + accountType + 'Bank' + bankNo));
                }
                if (util.isFloat($F('txtPatientsNew' + i + 'ForSE' + accountType + 'Bank' + bankNo)) && !util.isEmpty($F('txtPatientsNew' + i + 'ForSE' + accountType + 'Bank' + bankNo))) {
                    patientsNew = parseFloat($F('txtPatientsNew' + i + 'ForSE' + accountType + 'Bank' + bankNo));
                }
                if (util.isFloat($F('txtFeeNew' + i + 'ForSE' + accountType + 'Bank' + bankNo)) && !util.isEmpty($F('txtFeeNew' + i + 'ForSE' + accountType + 'Bank' + bankNo))) {
                    feeNew = parseFloat($F('txtFeeNew' + i + 'ForSE' + accountType + 'Bank' + bankNo));
                }
                if (util.isFloat($F('txtDayMonth' + i + 'ForSE' + accountType + 'Bank' + bankNo)) && !util.isEmpty($F('txtDayMonth' + i + 'ForSE' + accountType + 'Bank' + bankNo))) {
                    dayMonth = parseInt($F('txtDayMonth' + i + 'ForSE' + accountType + 'Bank' + bankNo));
                }


                totalOfPatientsOld = (patientsOld * feeOld);
                totalOfPatientsNew = (patientsNew * feeNew);
                total = totalOfPatientsOld + totalOfPatientsNew;
                totalIncome = total * dayMonth;
                totalChamberIncome += totalIncome;

                objAutoChamberIncome.ChamberId = i;
                objAutoChamberIncome.OldPatents = patientsOld;
                objAutoChamberIncome.OldFee = feeOld;
                objAutoChamberIncome.OldTotal = totalOfPatientsOld;
                objAutoChamberIncome.NewPatents = patientsNew;
                objAutoChamberIncome.NewFee = feeNew;
                objAutoChamberIncome.NewTotal = totalOfPatientsNew;
                objAutoChamberIncome.Total = total;
                objAutoChamberIncome.DayMonth = dayMonth;
                objAutoChamberIncome.Income = totalIncome;
                prChanberIncomeArray.push(objAutoChamberIncome);


            }
            resultArray = prChanberIncomeArray;

        }
        return resultArray;
    },

    //Create Teacher Income
    CreateTeacherIncome: function(identity, accountType, tabName) {

        if (accountType == "Pr" && tabName == "SE") {
            var totalIncome = $F('txtTotalInComeTutionForSEPrBank' + identity);
            if (totalIncome != "0" && totalIncome != "") {
                var objTutioningTeacher = new Object();
                objTutioningTeacher.AutoLoanMasterId = $F('txtAnalystMasterID');
                if (objTutioningTeacher.AutoLoanMasterId == "") {
                    objTutioningTeacher.AutoLoanMasterId = 0;
                }
                objTutioningTeacher.BankStatementId = $F('txtBankStatementIdSEPr' + identity);
                objTutioningTeacher.Type = "1";
                objTutioningTeacher.NoOfStudent = parseFloat(util.isEmptyReturn0($F('txtNoOfStudentForSEPrBank' + identity)));
                objTutioningTeacher.NoOfDaysForTution = parseFloat(util.isEmptyReturn0($F('txtNoOfDaysForTutionForSEPrBank' + identity)));
                objTutioningTeacher.Fees = parseFloat(util.isEmptyReturn0($F('txtTutionFeeForSEPrBank' + identity)));
                objTutioningTeacher.TotalIncome = parseFloat(util.isEmptyReturn0($F('txtTotalInComeTutionForSEPrBank' + identity)));
                return objTutioningTeacher;
            }
        }
        if (accountType == "Jt" && tabName == "SE") {
            var totalIncomeJt = $F('txtTotalInComeTutionForSEJtBank' + identity);
            if (totalIncomeJt != "0" && totalIncomeJt != "") {
                var objTutioningTeacherJt = new Object();
                objTutioningTeacherJt.AutoLoanMasterId = $F('txtAnalystMasterID');
                if (objTutioningTeacherJt.AutoLoanMasterId == "") {
                    objTutioningTeacherJt.AutoLoanMasterId = 0;
                }

                objTutioningTeacherJt.BankStatementId = $F('txtBankStatementIdSEJt' + identity);
                objTutioningTeacherJt.Type = "2";
                objTutioningTeacherJt.NoOfStudent = parseFloat(util.isEmptyReturn0($F('txtNoOfStudentForSEJtBank' + identity)));
                objTutioningTeacherJt.NoOfDaysForTution = parseFloat(util.isEmptyReturn0($F('txtNoOfDaysForTutionForSEJtBank' + identity)));
                objTutioningTeacherJt.Fees = parseFloat(util.isEmptyReturn0($F('txtTutionFeeForSEJtBank' + identity)));
                objTutioningTeacherJt.TotalIncome = parseFloat(util.isEmptyReturn0($F('txtTotalInComeTutionForSEJtBank' + identity)));
                return objTutioningTeacherJt;
            }
        }
    }
};