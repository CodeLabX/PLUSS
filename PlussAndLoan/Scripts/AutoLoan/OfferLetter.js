﻿var isPopupOpened = false;
var Page = {
    init: function() {
        util.prepareDom();
        Event.observe('btnClose', 'click', util.hideModal.curry('requiredInfoPopupDiv'));
        OfferLeterHelper.requiredInfoPopup();
    }
};

var OfferLetterManager = {
    PopupOpened: function(isOpened) {
        if (isOpened == 1) {
            isPopupOpened = true;
        } else {
            isPopupOpened = false;
        }
        docCheckListHelper.requiredInfoPopup();
    }
};


var OfferLeterHelper = {
    requiredInfoPopup: function() {

        if (isPopupOpened == true) {
            util.hideModal.curry('requiredInfoPopupDiv');
        } else {
            util.showModal('requiredInfoPopupDiv');
        }

    }
};
Event.onReady(Page.init);