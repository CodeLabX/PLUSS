﻿/// <reference path="AnalystDetailsCalculation.js" />

var AccountType = { 1: 'Company', 2: 'Personal' };
var askingrepayment = { 1: 'PDC', 2: 'SI' };
var dbrLevel = { 1: '50', 2: '55', 3: '60' };
var reasonOfJoint = { 'Income Consideration': '1', 'Joint Registration': '2', 'Business is in Joint Name': '3', 'Customer is Mariner': '4', 'AC is in Joint Name': '5', 'Others': '6' };
var vehicleType = { 'Sedan': 'S1', 'Station wagon': 'S2', 'SUV': 'S3', 'Pick-Ups': 'S4', 'Microbus': 'S5', 'MPV': 'S6' };
var Page = {
        //Start Global Veriable
        vehicleStatusArray: [],
        manufactureArray: [],
        vendoreArray:[],
        bankLinkOption:"",
        facilityArray:[],
        sectorLinkOption:[],
        cibInfo:[],
        segmentArray:[],
        artaPreference:[],
        ownDamageArray:[],
        userArray:[],
        sourceForGeneralArray:[],
        incomeAssementArray:[],
        incomeAssesmentLinkOption:"",
        modelArray:[],
        vehicleId: 0,
        bankIdforotherBranch: -1,
        vehicleDetailsArray: null,

        age: 0,
        jointage: 0,
        tenor: 0,
        otherBankAccountForrepayment: [],
        deviationArray: [],
        tabArray: [],
        employerForSalariedArray: [],
        subsectorArray:[],
        bussinessManTabArray:[],
        selfEmployedTabArray:[],
        facilityListArray:[],
        jtCount: 0,
        prCount: 0,
        bankStatementArray:[],
        isAverageBalancePrimaryArray:[],
        isAverageBalanceJointArray:[],
        newIncomeArrayforAll:[],
        incomeArrayforlc:[],
    
        primaryProfessionId:0,
        otherProfessionId: 0,
        branchNameForSource: -1,
        jointPrimaryProfessionId: 0,
        subsectorForLcCashCoveredArray:[],
        isjoint: false,
        //End Global Veriable
        
        
        init: function() {
        util.prepareDom();
        Event.observe('lnkClose', 'click', analystSaveManager.close);
        Event.observe('lnkBookLet', 'click', analystSaveManager.printBooklet);
        Event.observe('lnkOfferLetter', 'click', analystSaveManager.printOfferLetter);
        Event.observe('lnkLLI', 'click', analystSaveManager.printLLI);
        Event.observe('lnkBasel', 'click', analystSaveManager.printBasel);
        Event.observe('lnkLoanChecklist', 'click', analystSaveManager.printLoanCheckList);
        Event.observe('lnkDocumentChecklist', 'click', analystSaveManager.printDocumentList);
        Event.observe('lnkIncomeAssment', 'click', analystSaveManager.printIncomeAssmentList);
        Event.observe('lnkDecline', 'click', analystSaveManager.Decline);
        Event.observe('lnkReject', 'click', analystSaveManager.Reject);
        Event.observe('lnkOnHold', 'click', analystSaveManager.OnHold);
        Event.observe('lnkBackward', 'click', analystSaveManager.Backward);
        Event.observe('lnkForward', 'click', analystSaveManager.Forward);
        Event.observe('lnkSave', 'click', analystSaveManager.SaveData);
            
        //Start Change Event
        Event.observe('cmdVehicleStatus', 'change', analystDetailsHelper.changeVehicleStatus);
        Event.observe('cmbReasonofJointApplicant', 'change', analystDetailsHelper.showJointApplicant);
        Event.observe('cmbCashSecured', 'change', analystDetailsHelper.changeCashSecured);
        Event.observe('cmbARTA', 'change', analystDetailsHelper.changeARTA);
        Event.observe('cmbCCBundle', 'change', analystDetailsHelper.changeCCBundle);
        Event.observe('cmsSeatingCapacity', 'change', analystDetailsHelper.changeSeatingCapacity);
        Event.observe('cmbEmployeeSegmentLc', 'change', analystDetailsHelper.changeSegment);
        Event.observe('cmbDBRLevel', 'change', analystDetailsHelper.changeDbrLevel);
        Event.observe('txtConsideredDbr', 'change', analystDetailsHelper.changeConsideredDbr);
        Event.observe('txtAskingInterest', 'change', analystDetailsHelper.changeAskingInterest);
        Event.observe('txtAskingtenor', 'change', analystDetailsHelper.changeAskingTenor);
        Event.observe('cmbARTAReference', 'change', analystDetailsHelper.changeArtareference);
        Event.observe('cmbGeneralInsurence', 'change', analystDetailsHelper.changeGeneralInsurence);
        Event.observe('txtQuotedPrice', 'change', analystDetailsHelper.changeQuotedPrice);
        Event.observe('txtConsideredprice', 'change', analystDetailsHelper.changeConsideredPrice);
        Event.observe('txtInstrumentForFinalizetab', 'change', analystDetailsHelper.changeInstrumentForFinalizeTab);
        
            
        Event.observe('btnClosePopup', 'click', util.hideModal.curry('bankStatementPopupDiv'));
        
        
        
        Event.observe('cmbCarVerification', 'change', analystDetailsHelper.changeCarVerification);
        Event.observe('cmbSource1', 'change', analystDetailsManager.changeBranchNameForSourceOnApplicantDetailsForGeneral);
        Event.observe('cmbBrowingRelationship', 'change', analystDetailsHelper.changeBrowingRelationship);
        Event.observe('cmbBranch', 'change', analystDetailsHelper.changeSourceNameForSourceCodeOnApplicantDetailsForGeneral);
        Event.observe('cmbAskingRepaymentmethod', 'change', analystDetailsHelper.changeAskingRepaymentMethod);
        Event.observe('cmbAssessmentMethodForLcCash', 'change', analystDetailsHelper.changeIncomeAssementMethodForCashSequre);
        Event.observe('cmbCategoryForLcCash', 'change', analystDetailsHelper.changeCategoryForCashSequre);
        Event.observe('cmbBureauHistory', 'change', analystDetailsHelper.changeCibInfo);
        Event.observe('cmbVendor', 'change', analystDetailsHelper.changeVendore);
        Event.observe('cmbManufacturer', 'change', analystDetailsHelper.changeManufacture);
        Event.observe('cmbModel', 'change', analystDetailsHelper.changeModel);
        Event.observe('txtregistrationYear', 'change', analystDetailsHelper.changeregistationyear);
        Event.observe('txtManufacturaryear', 'change', analystDetailsHelper.changeManufactureryear);


        Event.observe('cmbGeneralInsurencePreference', 'change', analystDetailsHelper.changeGeneralInsurence);
        Event.observe('cmbARTAReference', 'change', analystDetailsHelper.changeLifeInsurence);
        Event.observe('cmbReworkDone', 'change', analystDetailsHelper.changeRework);
        Event.observe('cmbProductName', 'change', analystDetailsHelper.changeProduct);
        
        //End Change Event

        //Start Page Load Function
            
        analystDetailsManager.getVehicleStatus();
        analystDetailsManager.getManufacturer();
        analystDetailsManager.getVendor();
        analystDetailsManager.GetAllAutoBranch();
        analystDetailsManager.getBank();
        analystDetailsManager.getFacility();
        analystDetailsManager.getSector();
        analystDetailsManager.getCibInfo();
        //analyzeLoanApplicationManager.getIncomeAssement();

        analystDetailsManager.GetEmployeSegment();
        analystDetailsManager.getGeneralInsurance();
        analystDetailsManager.renderOtherDamage();
        analystDetailsManager.GetUserData();
        analystDetailsManager.GetDeviationLevel();
        analystDetailsManager.GetRemarksStrengthCondition();
        
        //End Page Load Function
        analystDetailsManager.GetDataByLLID();
    }
    
};
var analystDetailsManager = {
    //Vehicle Status Combo
    getVehicleStatus: function() {

        var res = PlussAuto.AnalystDetails.getVehicleStatus(1);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateVehicleStatus(res);
    },

    //Manufacturar Combo
    getManufacturer: function() {
        var res = PlussAuto.AnalystDetails.GetAutoManufacturer();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateManufacturer(res);
    },

    //Vendore Combo
    getVendor: function() {
        var res = PlussAuto.AnalystDetails.GetVendor();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateVendore(res);
    },

    //Auto Branch Combo
    GetAllAutoBranch: function() {
        var res = PlussAuto.AnalystDetails.GetAllAutoBranch();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateAutoBranch(res);
    },

    //Bank Combo
    getBank: function() {

        var res = PlussAuto.AnalystDetails.GetBank(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateBankCombo(res);
    },

    //Facility Combo
    getFacility: function() {

        var res = PlussAuto.AnalystDetails.getFacility();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateFacilityCombo(res);
    },

    //Sector Combo
    getSector: function() {

        var res = PlussAuto.AnalystDetails.GetSector();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateSectorCombo(res);
    },

    //CIB Info Combo
    getCibInfo: function() {
        var res = PlussAuto.AnalystDetails.getCibInfo();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateCibInfoCombo(res);
    },

    //Employee Segment
    GetEmployeSegment: function() {
        var res = PlussAuto.AnalystDetails.GetEmployeSegment();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateSegmentCombo(res);
    },

    //General Insurence Combo
    getGeneralInsurance: function() {
        var res = PlussAuto.AnalystDetails.getGeneralInsurance();
        Page.artaPreference = res.value;

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateGeneralInsurenceCombo(res);

    },

    //Render Other Damage
    renderOtherDamage: function() {

        var res = PlussAuto.AnalystDetails.GetOtherDamage();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            $('txtaccidentOrTheft').setValue(res.value.AccidentOrTheft.toFixed(2));
            $('txtCommision').setValue(res.value.Commission.toFixed(2));
            $('txtVat').setValue(res.value.Vat.toFixed(2));
            $('txtPassenger').setValue(res.value.PassengerPerSeatingCap.toFixed(2));
            $('txtAmountofDriver').setValue(res.value.AmountofDriverSeat.toFixed(2));
            $('txtStampCharge').setValue(res.value.StampCharge.toFixed(2));


            analystDetailsManager.renderOwnDamage();
        }

    },

    //Render Own Damage
    renderOwnDamage: function() {

        var res = PlussAuto.AnalystDetails.GetOwnDamage();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            Page.ownDamageArray = res.value;
        }

    },

    //User Combo
    GetUserData: function() {
        var res = PlussAuto.AnalystDetails.gett_pluss_userAll();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            analystDetailsHelper.populateUserCombo(res.value);
        }
    },

    //Populate Deviation Level
    GetDeviationLevel: function() {
        var res = PlussAuto.AnalystDetails.getDeviationLevel();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            analystDetailsHelper.PopulateDeviationLevel(res);
        }
    },

    //Populate Remarks, Strength and Condition
    GetRemarksStrengthCondition: function() {
        var res = PlussAuto.AnalystDetails.getRemarkStrangthConditionWeaknessAll();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            analystDetailsHelper.PopulateRemarksConditionStrengthWwakness(res);
        }
    },

    //Populate Branch Name For Source OnApplicantDetails For General Tab
    changeBranchNameForSourceOnApplicantDetailsForGeneral: function() {
        var branchId = $F("cmbSource1");
        var res = PlussAuto.AnalystDetails.GetAllAutoSourceByBranchId(branchId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateBranchNameForSourceForGeneralTab(res);
    },

    //Populate Model Combo by Manufactara
    getModelByManufactureId: function() {
        var manufacturerID = $F('cmbManufacturer');
        var res = PlussAuto.AnalystDetails.getModelbyManufacturerID(manufacturerID);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateModelCombo(res);
    },

    //Get Value Pack Allowed
    Getvaluepackallowed: function() {
        var manufacturerId = $F('cmbManufacturer');
        var vehicleId = $F("cmbModel");
        var carStatus = $F("cmdVehicleStatus");
        if (manufacturerId == "-1" || vehicleId == "-1" || carStatus == "-1") {
            $('cmbvaluePackAllowed').setValue('0');
            return;
        }

        var res = PlussAuto.AnalystDetails.getValuePackAllowed(manufacturerId, vehicleId, carStatus);
        
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value != null) {
            $('cmbvaluePackAllowed').setValue(res.value.ValuePackAllowed);
        }
        else {
            $('cmbvaluePackAllowed').setValue('0');
        }
        analystDetailsHelper.calculateTenorSettings();

    },

    //Check Mou by VendoreId
    CheckMouByVendoreId: function() {
        var vendoreId = $F("cmbVendor");
        var res = PlussAuto.AnalystDetails.CheckMouByVendoreId(vendoreId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value.VendoreRelationshipStatus != 1 && res.value.VendoreRelationshipStatus != 0) {
            alert("The Vendore is Negative Listed");
            $('cmbVendoreRelationShip').setValue('2');
        }
        else {
            $('cmbVendoreRelationShip').setValue(res.value.VendoreRelationshipStatus);
        }
        analystDetailsHelper.calculateTenorSettings();
    },

    //Get Branch By Bank Id
    GetBranch: function(elementId) {
        // alert(ElementID);
        //        debugger;

        var bankId = $F(elementId);
        if (bankId == 0) {
        }
        else {
            if (bankId == "-1") {
                bankId = Page.bankIdforotherBranch;
                Page.bankIdforotherBranch = -1;
            }
            var res = PlussAuto.AnalystDetails.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            analystDetailsHelper.populateBranchCombo(res, elementId);
        }

    },

    //Get All Data By LLID
    GetDataByLLID: function() {
        //$("spnWait").innerHTML = "Please Wait some moment .........";
        var res = PlussAuto.AnalystDetails.GetDataByLLID();
        if (res.error) {
            alert(res.error.Message);
            return;
        }

        if (res.value == "No Data found.") {
            alert(res.value);
            var url = "../../UI/AutoLoan/AnalystSummary.aspx";
            location.href = url;
        }
        else {
            $("ctl00_ContentPlaceHolder_spnWait").innerHTML = "";

            //$('<%= spnWait.ClientID %>').innerHTML = "";
            AnalystDetailsCalculationHelper.populateDataByLLId(res);
        }
    },

    //Get Deviation Description By Level
    getDeviationDescriptionByLevel: function(controlId) {
        var level = $F(controlId);
        var controlNumber = 0;
        if (controlId == 'cmdDeviationLevel1') {
            controlNumber = 1;
        }
        else if (controlId == 'cmdDeviationLevel2') {
            controlNumber = 2;
        }
        else if (controlId == 'cmdDeviationLevel3') {
            controlNumber = 3;
        }
        else if (controlId == 'cmdDeviationLevel4') {
            controlNumber = 4;
        }

        var res = PlussAuto.AnalystDetails.getDeviationByLevel(level);

        analystDetailsHelper.PolulateDeviationDescription(res, controlNumber);
    },

    //Get Subsector For Land Lord
    GetSubSectorForLandLord: function(sectorId, accountType) {
        var res = PlussAuto.AnalystDetails.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateSubSectorComboForLandLord(res, accountType);
    },

    //Get Employer For Salaried
    GetEmployerForSalaried: function(accountType) {
        var res = PlussAuto.AnalystDetails.GetEmployerForSalaried();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateEmployerForSalaried(res, accountType);
    },

    //Get Income Assessment
    getIncomeAssement: function() {
        var res = PlussAuto.AnalystDetails.GetIncomeAssement();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateIncomeAssementCombo(res);
    },

    //Get Branch Name For Income Generator
    GetBranchNameForIncomeGenerator: function(bankIdElement, elementId) {

        var bankId = $F(bankIdElement);
        if (bankId == 0) {
        }
        else {
            var res = PlussAuto.AnalystDetails.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            analystDetailsHelper.populateBranchForIncomeGeneratorCombo(res, elementId);
        }

    },
    GetBranchNameForIncomeGeneratorJoint: function(bankId, elementId) {

        //var bankId = $F(bankIdElement);
        if (bankId == 0) {
        }
        else {
            var res = PlussAuto.AnalystDetails.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            analystDetailsHelper.populateBranchForIncomeGeneratorCombo(res, elementId);
        }

    },

    //Get Approver Limit
    GetApproverLimitByUserId: function(userId) {
        var res = PlussAuto.AnalystDetails.GetApproverLimitByUserId(userId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.approverLimit = res.value;
        if (res.value != null) {
            var label = parseInt($F("cmbLabelForDlaLimit"));
            var totalLoanAmount = 0;
            var totaldlalimitAmount = 0;
            switch (label) {
                case 1:
                    totalLoanAmount = parseFloat($F("txtloanAmountForLcLoanSummary"));
                    totaldlalimitAmount = res.value.TotalDLAAmountL1;
                    if (totalLoanAmount > totaldlalimitAmount) {
                        alert("Loan Amount Cross Total DLA Amount. Please reseat another approver");
                        $("txtAutoDLALimit").setValue('0');
                        $("txtTotalSecuredlimit").setValue('0');
                        $("txtApprovedbyCode").setValue('');
                        $("cmbApprovedby").setValue('-1');
                    }
                    else {
                        $("txtAutoDLALimit").setValue(totaldlalimitAmount);
                        $("txtTotalSecuredlimit").setValue(res.value.LEVEL1_AMT);
                        $("txtApproversDlaAllowed").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFound").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFlag").setValue('YES');
                    }
                    break;
                case 2:
                    totalLoanAmount = parseFloat($F("txtloanAmountForLcLoanSummary"));
                    totaldlalimitAmount = res.value.TotalDLAAmountL2;
                    if (totalLoanAmount > totaldlalimitAmount) {
                        alert("Loan Amount Cross Total DLA Amount. Please reseat another approver");
                        $("txtAutoDLALimit").setValue('0');
                        $("txtTotalSecuredlimit").setValue('0');
                        $("txtApprovedbyCode").setValue('');
                        $("cmbApprovedby").setValue('-1');
                    }
                    else {
                        $("txtAutoDLALimit").setValue(totaldlalimitAmount);
                        $("txtTotalSecuredlimit").setValue(res.value.LEVEL2_AMT);
                        $("txtApproversDlaAllowed").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFound").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFlag").setValue('YES');
                    }
                    break;
                case 3:
                    totalLoanAmount = parseFloat($F("txtloanAmountForLcLoanSummary"));
                    totaldlalimitAmount = res.value.TotalDLAAmountL3;
                    if (totalLoanAmount > totaldlalimitAmount) {
                        alert("Loan Amount Cross Total DLA Amount. Please reseat another approver");
                        $("txtAutoDLALimit").setValue('0');
                        $("txtTotalSecuredlimit").setValue('0');
                        $("txtApprovedbyCode").setValue('');
                        $("cmbApprovedby").setValue('-1');
                    }
                    else {
                        $("txtAutoDLALimit").setValue(totaldlalimitAmount);
                        $("txtTotalSecuredlimit").setValue(res.value.LEVEL3_AMT);
                        $("txtApproversDlaAllowed").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFound").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFlag").setValue('YES');
                    }
                    break;
            }

        }
        else {
            alert("This approver don't have any limit");
            $("txtAutoDLALimit").setValue('0');
            $("txtTotalSecuredlimit").setValue('0');
            $("cmbApprovedby").setValue('-1');
            $("txtApprovedbyCode").setValue('');
            $("txtApproversDlaFlag").setValue('NO');

        }

    },

    //Change Sector For Self Employed
    changeSectorForSelfEmployed: function(bankNo, accountType) {
        var sectorId = 0;

        if (accountType == "Pr") {
            sectorId = $F("cmbIndustryForSEPrBank" + bankNo);
        }
        else if (accountType == "Jt") {
            sectorId = $F("cmbIndustryForSEJtBank" + bankNo);
        }

        var res = PlussAuto.AnalystDetails.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateSubSectorComboForSelfEmployed(res, bankNo, accountType);
        analystDetailsHelper.changePrivateInCome(bankNo, accountType, "Se");
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'SE');
    },

    //Change Sector For Bussiness Man
    changeSector: function(bankNo, accountType) {
        var sectorId = 0;
        if (accountType == "Pr") {
            sectorId = $F("cmbIndustryForPrimaryBank" + bankNo);
        }
        else if (accountType == "Jt") {
            sectorId = $F("cmbIndustryForJointBank" + bankNo);
        }

        var res = PlussAuto.AnalystDetails.GetSubSector(sectorId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateSubSectorCombo(res, bankNo, accountType);
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'BM');
    },
    changeSectorForLcCashCovered: function(sectorId) {
        var res = PlussAuto.AnalystDetails.GetSubSector(sectorId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystDetailsHelper.populateSubSectorComboLcCashCovered(res);
    }


};

var analystDetailsHelper = {
    //Populate Vehicle Status
    populateVehicleStatus: function(res) {
        //        var link = "<option value=\"-1\">Select Vehicle Status</option>";

        //        for (var i = 0; i < res.value.length; i++) {
        //            link += "<option value=\"" + res.value[i].StatusId + "\">" + res.value[i].StatusName + "</option>";
        //        }
        //        $("cmdVehicleStatus").update(link);

        document.getElementById('cmdVehicleStatus').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vehicle Status';
        ExtraOpt.value = '-1';
        document.getElementById('cmdVehicleStatus').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmdVehicleStatus').options.add(opt);
        }


        Page.vehicleStatusArray = res.value;
    },

    //Populate Manufactarar
    populateManufacturer: function(res) {
        //        var link = "<option value=\"-1\">Select Manufacturer</option>";

        //        for (var i = 0; i < res.value.length; i++) {
        //            link += "<option value=\"" + res.value[i].ManufacturerId + "\">" + res.value[i].ManufacturerName + "</option>";
        //        }
        //        $("cmbManufacturer").update(link);
        document.getElementById('cmbManufacturer').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Manufacturer';
        ExtraOpt.value = '-1';
        document.getElementById('cmbManufacturer').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ManufacturerName;
            opt.value = res.value[i].ManufacturerId;
            document.getElementById('cmbManufacturer').options.add(opt);
        }

        Page.manufactureArray = res.value;
    },

    //Populate Vendore
    populateVendore: function(res) {
        //        var link = "<option value=\"-1\">Select Vendor</option>";

        //        for (var i = 0; i < res.value.length; i++) {
        //            link += "<option value=\"" + res.value[i].AutoVendorId + "\">" + res.value[i].VendorName + "</option>";
        //        }
        //        $("cmbVendor").update(link);
        document.getElementById('cmbVendor').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vendor';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVendor').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].VendorName;
            opt.value = res.value[i].AutoVendorId;
            document.getElementById('cmbVendor').options.add(opt);
        }

        Page.vendoreArray = res.value;
    },

    //Populate Auto Source
    populateAutoBranch: function(res) {
        //        var link = "<option value=\"-1\">Select Source</option>";

        //        for (var i = 0; i < res.value.length; i++) {
        //            link += "<option value=\"" + res.value[i].AutoBranchId + "\">" + res.value[i].AutoBranchName + "</option>";
        //        }
        //        $("cmbSource1").update(link);
        //$("cmbSource1").innerHTML(link);
        document.getElementById('cmbSource1').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Source';
        ExtraOpt.value = '-1';
        document.getElementById('cmbSource1').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].AutoBranchName;
            opt.value = res.value[i].AutoBranchId;
            document.getElementById('cmbSource1').options.add(opt);
        }
    },


    testmethod: function(opt, ele) {
        ele.options.add(opt);
    },


    //Populate Bank
    populateBankCombo: function(res) {

        var link = "<option value=\"-1\">Select a bank</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BankID + "\">" + res.value[i].BankName + "</option>";
        }
        $("cmbBankNameForOther1").update(link);
        $("cmbBankNameForOther2").update(link);
        $("cmbBankNameForOther3").update(link);
        $("cmbBankNameForOther4").update(link);
        $("cmbBankNameForOther5").update(link);

        //Exposer Off Scb Finential Institution

        $("cmbFinancialInstitutionForOffSCB1").update(link);
        $("cmbFinancialInstitutionForOffSCB2").update(link);
        $("cmbFinancialInstitutionForOffSCB3").update(link);
        $("cmbFinancialInstitutionForOffSCB4").update(link);
        $("cmbFinancialInstitutionForCreditCardOffSCB1").update(link);
        $("cmbFinancialInstitutionForCreditCardOffSCB2").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB1").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB2").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB3").update(link);
        Page.bankLinkOption = link;
    },

    //Populate Facility Cobmo
    populateFacilityCombo: function(res) {
        var link = "<option value=\"-1\">Select a Type</option>";
        var link1 = "<option value=\"-1\">Select a Type</option>";


        for (var i = 0; i < res.value.length; i++) {
            if (res.value[i].FACI_NAME == "Credit Card ($)".trim() || res.value[i].FACI_NAME == "Credit Card (BDT)".trim()) {
                link1 += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
            }
            else {
                if (res.value[i].FACI_NAME != "Over Draft".trim()) {
                    link += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
                }
            }
        }
        $("cmbTypeForTermLoans1").update(link);
        $("cmbTypeForTermLoans2").update(link);
        $("cmbTypeForTermLoans3").update(link);
        $("cmbTypeForTermLoans4").update(link);

        $("cmbTypeForCreditCard1").update(link1);
        $("cmbTypeForCreditCard2").update(link1);

        Page.facilityArray = res.value;

    },

    //Populate Sector Combo
    populateSectorCombo: function(res) {
        var link = "<option value=\"-1\">Select a Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SECT_ID + "\">" + res.value[i].SECT_NAME + "</option>";
        }
        $("cmbCategoryForLcCash").update(link);
        Page.sectorLinkOption = link;
    },

    //Populate CIBINfo Combo
    populateCibInfoCombo: function(res) {
        var link = "<option value=\"-1\">Select from List</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].CibInfoId + "\">" + res.value[i].CibInfoName + "</option>";
        }
        $("cmbBureauHistory").update(link);
        Page.cibInfo = res.value;
    },

    //Populate Segment Combo
    populateSegmentCombo: function(res) {
        var link = "<option value=\"-1\">Select a Segment</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SegmentID + "\">" + res.value[i].SegmentName + "</option>";
        }
        $("cmbEmployeeSegmentLc").update(link);
        Page.segmentArray = res.value;
    },

    //Populate General Insurence Combo
    populateGeneralInsurenceCombo: function(res) {
        var link = "<option value=\"-1\">No Insurence</option>";
        var link1 = "<option value=\"-1\">No Insurence</option>";
        for (var i = 0; i < res.value.length; i++) {
            var insType = res.value[i].InsType;
            if (insType == '1' || insType == '3') {
                link += "<option value=\"" + res.value[i].InsCompanyId + "\">" + res.value[i].CompanyName + "</option>";
            }
            if (insType == '2' || insType == '3') {
                link1 += "<option value=\"" + res.value[i].InsCompanyId + "\">" + res.value[i].CompanyName + "</option>";
            }
        }
        $("cmbARTAReference").update(link1);
        $("cmbGeneralInsurencePreference").update(link);
        Page.artaPreference = res.value;
    },

    //Populate User Combo
    populateUserCombo: function(objUserlist) {

        var link = "<option value=\"-1\">Select a User</option>";
        var linkForApp = "<option value=\"-1\">Select a User</option>";
        for (var i = 0; i < objUserlist.length; i++) {
            link += "<option value=\"" + objUserlist[i].USER_ID + "\">" + objUserlist[i].USER_NAME + "</option>";
            if (objUserlist[i].AUTO_USER_LEVEL == 7 || objUserlist[i].AUTO_USER_LEVEL == 13) {
                linkForApp += "<option value=\"" + objUserlist[i].USER_ID + "\">" + objUserlist[i].USER_NAME + "</option>";
            }
        }
        $("cmbAppraisedby").update(link);
        $("cmbSupportedby").update(link);
        $("cmbApprovedby").update(linkForApp);
        Page.userArray = objUserlist;
    },

    //Populate Deviation Level
    PopulateDeviationLevel: function(res) {
        var link = "<option value=\"-1\">Select a Level</option>";
        for (var i = 0; i < res.value.length; i++) {
            if (res.value[i].DeviationFor != 1) {
                link += "<option value=\"" + res.value[i].DeviationFor + "\">" + "Level " + res.value[i].DeviationFor + "</option>";
            }
            else {
                link += "<option value=\"" + res.value[i].DeviationFor + "\">Income Assessment</option>";
            }
        }
        for (var j = 1; j <= 4; j++) {
            $("cmdDeviationLevel" + j).update(link);
        }
    },

    //Populate Remarks, Condition, Strength
    PopulateRemarksConditionStrengthWwakness: function(res) {
        var linkStrength = "<option value=\"-1\">Select a Strength</option>";
        var linkWeakness = "<option value=\"-1\">Select a Weakness</option>";
        var linkCondition = "<option value=\"-1\">Select a Condition</option><option value=\"Board Resoulation Required For PDC/SI\">Board Resoulation Required For PDC/SI</option><option value=\"Car Import Docs is Required\">Car Import Docs is Required</option><option value=\"To be Paid in Cash amount\">To be Paid in Cash amount</option><option value=\"ARTA waiver required\">ARTA waiver required</option><option value=\"Registration to be verified\">Registration to be verified</option>";
        var linkRemark = "<option value=\"-1\">Select a Remark</option>";

        for (var i = 0; i < res.value.length; i++) {

            if (res.value[i].REMA_DTYPE == '1') {
                linkRemark += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
            else if (res.value[i].REMA_DTYPE == '2') {
                linkCondition += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
            else if (res.value[i].REMA_DTYPE == '3') {
                linkStrength += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
            else if (res.value[i].REMA_DTYPE == '4') {
                linkWeakness += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
        }

        for (var j = 1; j <= 5; j++) {
            $("cmbStrength" + j).update(linkStrength);
            $("cmbWeakness" + j).update(linkWeakness);
            $("cmbCondition" + j).update(linkCondition);
            if (j < 4) {
                $("cmbRemark" + j).update(linkRemark);
            }

        }
    },

    //populate Branch Name For Source For General Tab
    populateBranchNameForSourceForGeneralTab: function(res) {

        var link = "<option value=\"-1\">Select Source</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].AutoSourceId + "\">" + res.value[i].SourceName + "</option>";
        }
        $("cmbBranch").update(link);
        Page.sourceForGeneralArray = res.value;
        if (Page.branchNameForSource != -1) {
            $('cmbBranch').setValue(Page.branchNameForSource);
        }
    },

    //Populate Model Combo
    populateModelCombo: function(res) {
        var link = "<option value=\"-1\">Select Model</option>";

        for (var i = 0; i < res.value.length; i++) {
            var ch = res.value[i].Model;
            if (res.value[i].TrimLevel != "") {
                ch += "-" + res.value[i].TrimLevel;
            }

            link += "<option value=\"" + res.value[i].VehicleId + "\">" + ch + "</option>";
        }
        $("cmbModel").update(link);
        Page.modelArray = res.value;
        if (Page.vehicleId != 0) {
            $('cmbModel').setValue(Page.vehicleId);
            //var cmbModel111 = document.getElementById('cmbModel');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbModel111, Page.vehicleId);

            analystDetailsHelper.changeModel();
        }

        analystDetailsManager.Getvaluepackallowed();
    },

    //Populate Branch Combo
    populateBranchCombo: function(res, elementId) {
        var link = "<option value=\"-1\">Select a Branch</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BRAN_ID + "\">" + res.value[i].BRAN_NAME + "</option>";
        }

        if (elementId == "cmbBankNameForOther1") {
            $("cmbBranchNameOther1").update(link);
        }
        else if (elementId == "cmbBankNameForOther2") {
            $("cmbBranchNameOther2").update(link);
        }
        else if (elementId == "cmbBankNameForOther3") {
            $("cmbBranchNameOther3").update(link);
        }
        else if (elementId == "cmbBankNameForOther4") {
            $("cmbBranchNameOther4").update(link);
        }
        else if (elementId == "cmbBankNameForOther5") {
            $("cmbBranchNameOther5").update(link);
        }
    },

    //Populate Repayment Bank Name For Finalization Tab
    populaterepaymentbankName: function(objOtherBanklist) {
        var link = "<option value=\"-1\">Select a bank</option>";
        for (var i = 0; i < objOtherBanklist.length; i++) {
            link += "<option value=\"" + objOtherBanklist[i].BankId + "\">" + objOtherBanklist[i].BankName + "</option>";
        }
        $("cmbBankNameForFinalizeTabofrepaymentMethod").update(link);
        Page.otherBankAccountForrepayment = objOtherBanklist;

    },

    //Populate Deviation Description
    PolulateDeviationDescription: function(res, controlNumber) {
        var link = "<option value=\"-1\">Select Description</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].DeviationId + "\">" + res.value[i].Description + "</option>";
        }
        $("cmdDeviationDescription" + controlNumber).update(link);
        Page.deviationArray = res.value;
    },

    //Populate Employer For Salaried
    populateEmployerForSalaried: function(res, accountType) {
        var link = "<option value=\"-1\">Select an income Assement</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].AutoCompanyId + "\">" + res.value[i].CompanyName + "</option>";
        }
        if (accountType == "Pr") {
            $("cmbEmployerForSalariedForPr").update(link);
        }
        else if (accountType == "Jt") {
            $("cmbEmployerForSalariedForJt").update(link);
        }

        Page.employerForSalariedArray = res.value;
    },

    //Populate Subsector Combo for LandLord
    populateSubSectorComboForLandLord: function(res, accountType) {

        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }
        if (accountType == "Pr") {
            $("cmbSubsectorForLandLordPr").update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubsectorForLandLordJt").update(link);
        }

    },

    populateSubSectorComboLcCashCovered: function(res) {
        Page.subsectorForLcCashCoveredArray = [];
        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            Page.subsectorForLcCashCoveredArray.push(res[i]);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }
        $("cmbSubCategoryForLcCash").update(link);

    },

    //populate Income Assement Combo
    populateIncomeAssementCombo: function(res) {
        var link = "<option value=\"-1\">Select an income Assement</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].INAM_ID + "\">" + res.value[i].INAM_METHOD + "</option>";
        }
        Page.incomeAssesmentLinkOption = link;
        Page.incomeAssementArray = res.value;

    },

    //Populate barnch For Income Generator
    populateBranchForIncomeGeneratorCombo: function(res, elementId) {
        var link = "<option value=\"-1\">Select a Branch</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BRAN_ID + "\">" + res.value[i].BRAN_NAME + "</option>";
        }
        $(elementId).update(link);
    },

    //Populate SubSector For Self Employed
    populateSubSectorComboForSelfEmployed: function(res, bankNo, accountType) {

        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }
        if (accountType == "Pr") {
            $("cmbSubsectorForSEPrBank" + bankNo).update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubsectorForSEJtBank" + bankNo).update(link);
        }
    },

    //Populate SubSector For Bussiness Man
    populateSubSectorCombo: function(res, bankNo, accountType) {

        var link = "<option value=\"-1\">Select a Sub-Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SUSE_ID + "\">" + res.value[i].SUSE_NAME + "</option>";
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', res.value[i].SUSE_ID);
            if (!subsectorPrimary) {
                Page.subsectorArray.push(res.value[i]);
            }
        }

        if (accountType == "Pr") {
            $("cmbSubsectorForPrimaryBank" + bankNo).update(link);
        }
        else if (accountType == "Jt") {
            $("cmbSubSectorForJointBank" + bankNo).update(link);
        }
    },





    //Change Vehicle Status
    changeVehicleStatus: function() {
        var vehicleStatus = $F('cmdVehicleStatus');
        var vs = Page.vehicleStatusArray.findByProp('StatusId', vehicleStatus);
        if (vs) {
            $('txtVehicleStatusCode').setValue(vs.StatusCode);
        }
        else {
            $('txtVehicleStatusCode').setValue('');
        }

        if (vehicleStatus == "3") {
            $('txtregistrationYear').disabled = '';
        }
        else {
            $('txtregistrationYear').setValue('0');
            $('txtregistrationYear').disabled = 'false';
        }
        analystDetailsManager.Getvaluepackallowed();
    },

    //Change Joint Applicant
    showJointApplicant: function() {

        var reasonofJointApplicant = $F("cmbReasonofJointApplicant");
        var jtPrimaryProfession = parseInt($F("txtJtProfessionId"));
        var prPrimaryProfession = parseInt($F("txtPrProfessionId"));
        var prOtherProfession = parseInt($F("txtPrOtherProfessionId"));
        var blockType = 'none';
        var parentblockType = 'none';
        if (reasonofJointApplicant == "1") {
            blockType = 'inline-block';
        }
        switch (jtPrimaryProfession) {
            case 1:
                $('divjtApplicantIncomeGeneratorBussinessMan').style.display = blockType;
                if (prPrimaryProfession != 1 && prOtherProfession != 1 && reasonofJointApplicant != 1) {
                    $('liTabBussiness').style.display = 'none';
                    $('tabBusinessman').style.display = 'none';
                }
                if (prPrimaryProfession != 1 && prOtherProfession != 1 && reasonofJointApplicant == 1) {
                    $('liTabBussiness').style.display = 'inline-block';
                    $('tabBusinessman').style.display = 'inline-block';
                }
                else if (jtPrimaryProfession == 1 && reasonofJointApplicant == 1) {
                    $('liTabBussiness').style.display = 'inline-block';
                    $('tabBusinessman').style.display = 'inline-block';
                    $('divjtApplicantIncomeGeneratorBussinessMan').style.display = 'inline-block';
                }
                break;
            case 2:
                $('divjtApplicantIncomeGeneratorSalaried').style.display = blockType;
                if (prPrimaryProfession != 2 && prOtherProfession != 2 && reasonofJointApplicant != 1) {
                    $('liTabSalaried').style.display = 'none';
                    $('tabSalaried').style.display = 'none';
                }
                if (prPrimaryProfession != 2 && prOtherProfession != 2 && reasonofJointApplicant == 1) {
                    $('liTabSalaried').style.display = 'inline-block';
                    $('tabSalaried').style.display = 'inline-block';
                }
                else if (jtPrimaryProfession == 2 && reasonofJointApplicant == 1) {
                    $('liTabSalaried').style.display = 'inline-block';
                    $('tabSalaried').style.display = 'inline-block';
                    $('divjtApplicantIncomeGeneratorSalaried').style.display = 'inline-block';
                }
                break;
            case 3:

                $('divjtApplicantSelf-Employed').style.display = blockType;
                if (((prPrimaryProfession != 3 && prPrimaryProfession != 4 && prPrimaryProfession != 6) && (prOtherProfession != 3 && prOtherProfession != 4 && prOtherProfession != 6)) && reasonofJointApplicant != 1) {
                    $('liTabSelfEmployed').style.display = 'none';
                    $('tabSelf-Employed').style.display = 'none';
                }
                else if (((prPrimaryProfession != 3 || prPrimaryProfession != 4 || prPrimaryProfession != 6) || (prOtherProfession != 3 || prOtherProfession != 4 || prOtherProfession != 6)) && reasonofJointApplicant != 1) {
                    $('liTabSelfEmployed').style.display = 'inline-block';
                    $('tabSelf-Employed').style.display = 'inline-block';
                }
                else if (jtPrimaryProfession == 3 && reasonofJointApplicant == 1) {
                    $('liTabSelfEmployed').style.display = 'inline-block';
                    $('tabSelf-Employed').style.display = 'inline-block';
                    $('divjtApplicantSelf-Employed').style.display = 'inline-block';
                }
                break;
            case 4:
                $('divjtApplicantSelf-Employed').style.display = blockType;
                if ((prPrimaryProfession != 3 || prPrimaryProfession != 4 || prPrimaryProfession != 6) && (prOtherProfession != 3 || prOtherProfession != 4 || prOtherProfession != 6) && reasonofJointApplicant != 1) {
                    $('liTabSelfEmployed').style.display = 'none';
                    $('tabSelf-Employed').style.display = 'none';
                }
                else if ((prPrimaryProfession != 3 || prPrimaryProfession != 4 || prPrimaryProfession != 6) && (prOtherProfession != 3 || prOtherProfession != 4 || prOtherProfession != 6) && reasonofJointApplicant != 1) {
                    $('liTabSelfEmployed').style.display = 'inline-block';
                    $('tabSelf-Employed').style.display = 'inline-block';
                }
                else if (jtPrimaryProfession == 3 && reasonofJointApplicant == 1) {
                    $('liTabSelfEmployed').style.display = 'inline-block';
                    $('tabSelf-Employed').style.display = 'inline-block';
                    $('divjtApplicantSelf-Employed').style.display = 'inline-block';
                }
                break;
            case 5:
                $('divjtApplicantIncomeGeneratorLandlord').style.display = blockType;
                if ((prPrimaryProfession != 5 && prOtherProfession != 5) && reasonOfJoint != 1) {
                    $('liTabLandlord').style.display = 'none';
                    $('liTabLandlord').style.display = 'none';
                }
                else if ((prPrimaryProfession != 5 && prOtherProfession != 5) && reasonofJointApplicant == 1) {
                    $('liTabLandlord').style.display = 'inline-block';
                    $('liTabLandlord').style.display = 'inline-block';
                }
                else if (jtPrimaryProfession == 5 && reasonofJointApplicant == 1) {
                    $('liTabSelfEmployed').style.display = 'inline-block';
                    $('tabSelf-Employed').style.display = 'inline-block';
                    $('divjtApplicantIncomeGeneratorLandlord').style.display = 'inline-block';
                }
                break;
            case 6:
                $('divjtApplicantSelf-Employed').style.display = blockType;
                if ((prPrimaryProfession != 3 || prPrimaryProfession != 4 || prPrimaryProfession != 6) && (prOtherProfession != 3 || prOtherProfession != 4 || prOtherProfession != 6) && reasonofJointApplicant != 1) {
                    $('liTabSelfEmployed').style.display = 'none';
                    $('tabSelf-Employed').style.display = 'none';
                }
                else if ((prPrimaryProfession != 3 || prPrimaryProfession != 4 || prPrimaryProfession != 6) && (prOtherProfession != 3 || prOtherProfession != 4 || prOtherProfession != 6) && reasonofJointApplicant != 1) {
                    $('liTabSelfEmployed').style.display = 'inline-block';
                    $('tabSelf-Employed').style.display = 'inline-block';
                }
                else if (jtPrimaryProfession == 3 && reasonofJointApplicant == 1) {
                    $('liTabSelfEmployed').style.display = 'inline-block';
                    $('tabSelf-Employed').style.display = 'inline-block';
                    $('divjtApplicantSelf-Employed').style.display = 'inline-block';
                }
                break;
        }
        $('divLoanCalculatorForJointApplicant').style.display = blockType;

    },

    //Change Cash Sequred
    changeCashSecured: function() {
        var cashSecured = $F("cmbCashSecured");
        var primaryProfession = Page.primaryProfessionId;
        var otherProfession = Page.otherProfessionId;

        var emiClosing = 0;
        var totalEmiClosing = 0;
        var lccalculate = Page.incomeArrayforlc;
        if (cashSecured == "1") {
            $("lblincomeGeneratorAlertMasg").innerHTML = "Income Assessment Not Required";
            $('txtAssessmentMethodLc').setValue('Income Assessment Not Required');
            $('txtAssessmentCodeLc').setValue('FF');
            $('txtBalanceSupported').setValue('NOT APPLICABLE');
            $('divSecurityValue').style.display = "block";
            $('divSequrityvaluetextbox').style.display = "block";
            $('divCashForLc').style.display = "block";

            if (Page.primaryProfessionId != 2) {
                $("cmbAssessmentMethodForLcCash").update(Page.incomeAssesmentLinkOption);
                $('divSubCategoryForLcCashCovered').style.display = "block";
            }
            else {
                var link = "<option value=\"-1\">Select an income Assement</option>";
                for (var i = 0; i < Page.employerForSalariedArray.length; i++) {
                    link += "<option value=\"" + Page.employerForSalariedArray[i].AutoCompanyId + "\">" + Page.employerForSalariedArray[i].CompanyName + "</option>";
                }
                $("cmbCategoryForLcCash").update(link);
                $('divSubCategoryForLcCashCovered').style.display = "none";
            }

            var declaredIncome = $F("txtDeclaredIncomeForLcAll");
            $('txtTotalJointInComeForLcWithCal').setValue(declaredIncome);



            for (var i = 1; i <= Page.prCount; i++) {
                if (i < 7) {
                    if (primaryProfession == 3 || otherProfession == 3 || primaryProfession == 4 || otherProfession == 4 || primaryProfession == 6 || otherProfession == 6) {
                        emiClosing = $F("txtTotalEMIClosingConsideredForSEPrBank" + i);
                        if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                            totalEmiClosing += parseFloat(emiClosing);
                        }
                    }
                    if (primaryProfession == 1 || otherProfession == 1) {
                        emiClosing = $F("txtTotalEMIClosingConsideredForPrimaryBank" + i);
                        if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                            totalEmiClosing += parseFloat(emiClosing);
                        }
                    }
                }
            }
            if (totalEmiClosing != 0) {
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (analystDetailsHelper.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtBalanceSupported').setValue('NOT APPLICABLE');
                        }
                    }
                }
            }
            else {
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {
                        if (analystDetailsHelper.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            if (lccalculate[j].AssessmentCode == "FT" || lccalculate[j].AssessmentCode == "FC") {
                                $('txtBalanceSupported').setValue(lccalculate[j].AverageBalance);
                            }
                        }
                    }
                }

            }

        }
        else {
            $('divSecurityValue').style.display = "none";
            $('divSequrityvaluetextbox').style.display = "none";
            $('divCashForLc').style.display = "none";
            totalEmiClosing = 0;
            emiClosing = 0;
            $("lblincomeGeneratorAlertMasg").innerHTML = "";

            var totalIncome = 0;
            var appPrIncome = $F("txtAppropriteIncomeForLcAll");
            var appJtIncome = $F("txtAppropriteIncomeForLcJointAll");
            if (Page.isjoint == true) {
                if (!util.isFloat(appJtIncome)) {
                    appJtIncome = 0;
                }
                if (!util.isFloat(appPrIncome)) {
                    appPrIncome = 0;
                }
                totalIncome = parseInt(appPrIncome) + parseInt(appJtIncome);
            }
            else {
                if (!util.isFloat(appPrIncome)) {
                    appPrIncome = 0;
                }
                totalIncome = parseInt(appPrIncome);
            }
            $('txtTotalJointInComeForLcWithCal').setValue(totalIncome);





            for (var i = 1; i <= Page.prCount; i++) {
                if (i < 7) {
                    if (primaryProfession == 3 || otherProfession == 3 || primaryProfession == 4 || otherProfession == 4 || primaryProfession == 6 || otherProfession == 6) {
                        emiClosing = $F("txtTotalEMIClosingConsideredForSEPrBank" + i);
                        if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                            totalEmiClosing += parseFloat(emiClosing);
                        }
                    }
                    if (primaryProfession == 1 || otherProfession == 1) {
                        emiClosing = $F("txtTotalEMIClosingConsideredForPrimaryBank" + i);
                        if (util.isFloat(emiClosing) && !util.isEmpty(emiClosing)) {
                            totalEmiClosing += parseFloat(emiClosing);
                        }
                    }
                }
            }
            if (totalEmiClosing != 0) {
                $('txtAssessmentMethodLc').setValue('PREVIOUS REPAYMENT HISTORY');
                $('txtAssessmentCodeLc').setValue('FF');
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (analystDetailsHelper.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtAssessmentMethodIdLc').setValue('7');
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtBalanceSupported').setValue('NOT APPLICABLE');
                        }
                    }
                }
            }
            else {
                if (lccalculate.length > 0) {
                    for (var j = 0; j < lccalculate.length; j++) {

                        if (analystDetailsHelper.isMax(Page.newIncomeArrayforAll, lccalculate[j].Income)) {
                            $('txtAssessmentMethodLc').setValue(lccalculate[j].Assessmentmethod);
                            $('txtAssessmentCodeLc').setValue(lccalculate[j].AssessmentCode);
                            $('txtAssessmentMethodIdLc').setValue(lccalculate[j].AssmentMethodId);
                            $('txtProfessionIdForLcIncomeAndSegment').setValue(lccalculate[j].Profession);
                            $('txtEmployeeCategoryForLc').setValue(lccalculate[j].EmployeeCategory);
                            $('txtEmployeeCategoryCodeForLc').setValue(lccalculate[j].EmployeeCategoryCode);
                            $('txtEmployeeCategoryIdForLc').setValue(lccalculate[j].AssCatId);
                            if (lccalculate[j].AssessmentCode == "FT" || lccalculate[j].AssessmentCode == "FC") {
                                $('txtBalanceSupported').setValue(lccalculate[j].AverageBalance);
                            }
                        }
                    }


                }

            }
        }
    },

    //Change ARTA
    changeARTA: function() {
        var arta = $F("cmbARTA");
        //        if (arta == "1") {
        //            var age = parseInt($F("txtCurrenAge"));
        //            if (age > 60 && age <= 65) {
        //                $('cmbARTAReference').setValue('1');
        //                $('cmbARTAReference').disabled = 'false';
        //            }
        //        }
        //        else {
        //            $('cmbARTAReference').setValue('-1');
        //            $('cmbARTAReference').disabled = '';
        //        }
        analystDetailsHelper.calculateTenorSettings();
    },

    //Change CC Bundle
    changeCCBundle: function() {
        analystDetailsHelper.calculateTenorSettings();
    },

    //Change Seating Capacity
    changeSeatingCapacity: function() {
        var seatingCapacity = parseInt($F("cmsSeatingCapacity"));
        if (seatingCapacity > 9) {
            alert("Cannot be more than 9 without Level 3");
            $('cmbLabelForDlaLimit').setValue('3');
        }
        else {
            analystDetailsHelper.setagewiseLebel();
        }
        analystDetailsHelper.calculateTenorSettings();
        var approverId = $F("cmbApprovedby");
        if (approverId != "-1") {
            analystDetailsManager.GetApproverLimitByUserId(approverId);
        }
    },

    //Change Car Verification
    changeCarVerification: function() {
        var carVerification = $F("cmbCarVerification");
        if (carVerification == 1) {
            $('divLblDeliveryStatus').style.display = 'block';
            $('divDeliveryStatus').style.display = 'block';

        }
        else {
            $('divLblDeliveryStatus').style.display = 'none';
            $('divDeliveryStatus').style.display = 'none';
        }
    },

    //Change BrowingRelationship
    changeBrowingRelationship: function() {
        var browingrelationship = $F("cmbBrowingRelationship");
        $("txtRelationshipCode").setValue(browingrelationship);
    },

    //Change Segment
    changeSegment: function() {
        var segmentId = $F("cmbEmployeeSegmentLc");
        if (segmentId == "-1") {
            $('txtSegmentCodeLc').setValue('');
            return false;
        }
        var segment = Page.segmentArray.findByProp('SegmentID', segmentId);
        if (segment) {
            $('txtSegmentCodeLc').setValue(segment.SegmentCode);
            analystDetailsHelper.calculateTenorSettings();
        }
    },

    //change Source Name For Source Code On Applicant Details For General Tab
    changeSourceNameForSourceCodeOnApplicantDetailsForGeneral: function() {
        var sourceId = parseInt($F("cmbBranch"));
        var source = Page.sourceForGeneralArray.findByProp('AutoSourceId', sourceId);
        if (source) {
            $('txtBranchCode').setValue(source.SourceCode);
        }

    },

    //Change DBR Level
    changeDbrLevel: function() {
        var dbrLevelid = $F("cmbDBRLevel");
        var appropriateDbr = 0;


        //New Code by Washik 08/01/2013
        var tojIn = 0;
        if ((util.isFloat($F("txtTotalJointInComeForLcWithCal")) && !util.isEmpty($F("txtTotalJointInComeForLcWithCal")))) {
            tojIn = Math.round($F("txtTotalJointInComeForLcWithCal"));
        }
        if (parseInt(tojIn) <= 32500) {
            appropriateDbr = 35;
        }
        if (parseInt(tojIn) > 32500 && parseInt(tojIn) < 49999) {
            appropriateDbr = 40;
        }
        if (parseInt(tojIn) > 50000 && parseInt(tojIn) < 75000) {
            appropriateDbr = 40;
        }
        if (parseInt(tojIn) > 75000 && parseInt(tojIn) < 150000) {
            appropriateDbr = 55;
        }
        if (parseInt(tojIn) > 150000) {
            appropriateDbr = 60;
        }

        //End New Code By Washik


        //var appropriateDbr = dbrLevel[dbrLevelid];

        if (dbrLevelid == "1") {
            appropriateDbr = appropriateDbr;
        }
        if (dbrLevelid == "2") {
            appropriateDbr = appropriateDbr + 5;
        }
        if (dbrLevelid == "3") {
            appropriateDbr = appropriateDbr + 10;
        }
        if (appropriateDbr > 65) {
            appropriateDbr = 65;
        }



        $("txtAppropriateDbr").setValue(appropriateDbr);
        var lbl = parseInt($F("cmbLabelForDlaLimit"));
        if (parseInt(dbrLevelid) > lbl) {
            $("cmbLabelForDlaLimit").setValue(dbrLevelid);
            var approverId = $F("cmbApprovedby");
            if (approverId != "-1") {
                analystDetailsManager.GetApproverLimitByUserId(approverId);
            }
        }
        else {
            analystDetailsHelper.setagewiseLebel();
        }

        analystDetailsHelper.changeConsideredDbr();
    },

    //Change Consider DBR
    changeConsideredDbr: function() {

        var dbrLevelid = $F("cmbDBRLevel");
        var appropriateDbr = 0;
        //New Code by Washik 08/01/2013
        var tojIn = 0;
        if ((util.isFloat($F("txtTotalJointInComeForLcWithCal")) && !util.isEmpty($F("txtTotalJointInComeForLcWithCal")))) {
            tojIn = Math.round($F("txtTotalJointInComeForLcWithCal"));
        }
        if (parseInt(tojIn) <= 32500) {
            appropriateDbr = 35;
        }
        if (parseInt(tojIn) > 32500 && parseInt(tojIn) < 49999) {
            appropriateDbr = 40;
        }
        if (parseInt(tojIn) > 50000 && parseInt(tojIn) < 75000) {
            appropriateDbr = 50;
        }
        if (parseInt(tojIn) > 75000 && parseInt(tojIn) < 150000) {
            appropriateDbr = 55;
        }
        if (parseInt(tojIn) > 150000) {
            appropriateDbr = 60;
        }

        //End New Code By Washik


        //var appropriateDbr = dbrLevel[dbrLevelid];

        if (dbrLevelid == "1") {
            appropriateDbr = appropriateDbr;
        }
        if (dbrLevelid == "2") {
            appropriateDbr = appropriateDbr + 5;
        }
        if (dbrLevelid == "3") {
            appropriateDbr = appropriateDbr + 10;
        }
        if (appropriateDbr > 65) {
            appropriateDbr = 65;
        }



        $("txtAppropriateDbr").setValue(appropriateDbr);






        var consideredDbr = $F("txtConsideredDbr");
        var totalEmiOnScb = 0;
        var totalEmiOffScb = 0;
        var totalInteresOnScb = 0;
        var totalInterestOffScb = 0;
        var balanceSupport = 0;
        var maxEmiCap = 0;

        if ((util.isFloat(consideredDbr) && !util.isEmpty(consideredDbr))) {
            var totalJointIncome = $F("txtTotalJointInComeForLcWithCal");
            if ((util.isFloat(totalJointIncome) && !util.isEmpty(totalJointIncome))) {
                maxEmiCap = parseFloat((parseFloat(totalJointIncome) * parseFloat(consideredDbr)) / 100);
            }
            else {
                maxEmiCap = 0;
            }
            if (consideredDbr > 65) {
                $('tdDbr4').style.display = 'block';
                $("tdDbr4").innerHTML = "*Level-3 DBR Required";
            }
            else {
                $('tdDbr4').style.display = 'none';
            }

            var appropriatedbr = 0;
            if ((util.isFloat($F("txtAppropriateDbr")) && !util.isEmpty($F("txtAppropriateDbr")))) {
                appropriatedbr = parseFloat($F("txtAppropriateDbr"));
            }
            var dbrdiff = parseFloat(consideredDbr) - parseFloat(appropriatedbr);
            if (parseFloat(dbrdiff) > 10) {
                $("tdDbr4").innerHTML = "*Level-3 DBR Required";
                $('tdDbr4').style.display = 'block';

            }
            else if (parseFloat(dbrdiff) > 0 && parseFloat(dbrdiff) <= 10) {
                if (consideredDbr > 65) {
                    $('tdDbr4').style.display = 'block';
                    $("tdDbr4").innerHTML = "*Level-3 DBR Required";
                }
                else {
                    $("tdDbr4").innerHTML = "TRY AT LEVEL-2";
                    $('tdDbr4').style.display = 'block';
                }
            }
            else {
                if (consideredDbr > 65) {
                    $('tdDbr4').style.display = 'block';
                    $("tdDbr4").innerHTML = "*Level-3 DBR Required";
                }
                else {
                    $('tdDbr4').style.display = 'none';
                }
            }



        }
        else {
            maxEmiCap = 0;
        }

        $("txtMaximumEmiCapability").setValue(maxEmiCap);
        if ((util.isFloat($F("txtTotalEmiForLcExistingPaymentOnScb")) && !util.isEmpty($F("txtTotalEmiForLcExistingPaymentOnScb")))) {
            totalEmiOnScb = parseFloat($F("txtTotalEmiForLcExistingPaymentOnScb"));
        }
        if ((util.isFloat($F("txtTotalEmiForLcExistingPaymentOffScb")) && !util.isEmpty($F("txtTotalEmiForLcExistingPaymentOffScb")))) {
            totalEmiOffScb = parseFloat($F("txtTotalEmiForLcExistingPaymentOffScb"));
        }
        if ((util.isFloat($F("txtTotalinterestForOnScbLcExistingrepayment")) && !util.isEmpty($F("txtTotalinterestForOnScbLcExistingrepayment")))) {
            totalInteresOnScb = parseFloat($F("txtTotalinterestForOnScbLcExistingrepayment"));
        }
        if ((util.isFloat($F("txtTotalinterestForOffScbLcExistingrepayment")) && !util.isEmpty($F("txtTotalinterestForOffScbLcExistingrepayment")))) {
            totalInterestOffScb = parseFloat($F("txtTotalinterestForOffScbLcExistingrepayment"));
        }
        var existingEmi = totalEmiOnScb + totalEmiOffScb + totalInteresOnScb + totalInterestOffScb;
        $("txtExistingEmi").setValue(existingEmi);
        var currentPrimary = maxEmiCap - existingEmi;
        $("txtCurrentEMICapability").setValue(currentPrimary);

        if ((util.isFloat($F("txtBalanceSupported")) && !util.isEmpty($F("txtBalanceSupported")))) {
            balanceSupport = parseFloat($F("txtBalanceSupported"));
        }
        if (parseFloat(balanceSupport) > parseFloat(currentPrimary) || $F("txtBalanceSupported") == "NOT APPLICABLE") {
            $("txtMaximumEmi").setValue(currentPrimary);
            if (currentPrimary < 0) {
                $('tdDbrEx5').style.display = 'block';
            }
            else {
                $('tdDbrEx5').style.display = 'none';
            }
        }
        else {
            $("txtMaximumEmi").setValue(balanceSupport);
            if (balanceSupport < 0) {
                $('tdDbrEx5').style.display = 'block';
            }
            else {
                $('tdDbrEx5').style.display = 'none';
            }
        }

        analystDetailsHelper.calculateTenorSettings();
    },

    //Change Asking Interest
    changeAskingInterest: function() {
        var askingIntersetRate = $F("txtAskingInterest");
        if (!util.isFloat(askingIntersetRate)) {
            alert("Asking rate must be a number field");
            $("txtAskingInterest").setValue('0');
            //$("txtAskingInterest").focus();
            return false;
        }

        $("txtInterestLCAskingRate").setValue(askingIntersetRate);
        analystDetailsHelper.calculateTenorSettings();
    },

    //Change Asking Tenor
    changeAskingTenor: function() {
        var askingTenor = $F("txtAskingtenor");
        if (!util.isFloat(askingTenor)) {
            alert("Asking rate must be a number field");
            $("txtAskingInterest").setValue('0');
            //$("txtAskingInterest").focus();
            return false;
        }

        $("txtAskingTenorForLc").setValue(askingTenor);
        analystDetailsHelper.calculateTenorSettings();
    },

    //change Asking Repayment Method
    changeAskingRepaymentMethod: function() {
        var askingRepaymentMethod = $F("cmbAskingRepaymentmethod");
        if (askingRepaymentMethod != "-1") {
            var repaymentMethodName = askingrepayment[parseInt(askingRepaymentMethod)];
            $('cmbAskingRepaymentmethodForFinalize').setValue(repaymentMethodName);
            $('txtInstrumentForFinalizetab').setValue(askingRepaymentMethod);
            analystDetailsHelper.changeInstrumentForFinalizeTab();
        }
        AnalystDetailsCalculationHelper.calculateRepaymentForFinalize();

    },

    // Change arta Reference
    changeArtareference: function() {
        analystDetailsHelper.calculateTenorSettings();
    },

    //Change General Insurence
    //    changeGeneralInsurence: function() {
    //        debugger;
    //        analystDetailsHelper.calculateTenorSettings();
    //    },

    //Change Quoted Price
    changeQuotedPrice: function() {
        var qutoedPrice = $F("txtQuotedPrice");
        if (!util.isFloat(qutoedPrice)) {
            alert("Price must be a number field");
            $("txtQuotedPrice").setValue('0');
            //$("txtQuotedPrice").focus();
            return false;
        }
        var consideredPrice = $F("txtConsideredprice");
        if (consideredPrice != "") {
            if (parseFloat(consideredPrice) > parseFloat(qutoedPrice)) {
                alert("Considered price cannot grater then quoted price");
                $('txtConsideredprice').setValue('0');
            }
        }


        analystDetailsHelper.calculateTenorSettings();
    },

    //Change Considered Price
    changeConsideredPrice: function() {
        var consideredPrice = $F("txtConsideredprice");
        if (!util.isFloat(consideredPrice)) {
            alert("Price must be a number field");
            $("txtConsideredprice").setValue('0');
            //$("txtConsideredprice").focus();
            return false;
        }

        var quotedPrice = $F("txtQuotedPrice");
        if (quotedPrice == "") {
            alert("Please insert quoted price before give considered price");
            $('txtConsideredprice').setValue('0');
        }
        else {
            if (parseFloat(consideredPrice) > parseFloat(quotedPrice)) {
                alert("Considered Price cannot grater then quoted price");
                $('txtConsideredprice').setValue('0');
            }
        }
        analystDetailsHelper.calculateTenorSettings();
    },

    //Change Income Assement Method For Lc Cash Sequred
    changeIncomeAssementMethodForCashSequre: function() {
        var incomeAssId = $F("cmbAssessmentMethodForLcCash");
        var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssId);
        if (incomeAssBuss) {
            $("txtAssessmentCodeLc").setValue(incomeAssBuss.INAM_METHODCODE);
            $("txtAssessmentMethodIdLc").setValue(incomeAssId);
            $("txtAssessmentMethodLc").setValue(incomeAssBuss.INAM_METHOD);
        }
        else {
            $("txtAssessmentCodeLc").setValue('FF');
            $("txtAssessmentMethodIdLc").setValue('-1');
            $("txtAssessmentMethodLc").setValue('Income Assessment Not Required');
        }
    },

    //Change Income Assement Method For Bussiness Man
    changeIncomeAssementForBusssinessMan: function(bankNo, accountType) {
        if (accountType == "Pr") {
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
            if (incomeAssBuss) {
                var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);

                $("txtIncomeAssessmentCodeForPrimaryBank" + bankNo).setValue(incomeAssBuss.INAM_METHODCODE);
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    //Code Comment by Washik 26/12/2012
                    //                    var averageBalancePr = false;

                    //                    for (var i = 0; i < Page.isAverageBalancePrimaryArray.length; i++) {
                    //                        if (Page.isAverageBalancePrimaryArray[i].BankNo == bankNo && Page.isAverageBalancePrimaryArray[i].Accounttype == accountType && Page.isAverageBalancePrimaryArray[i].TabName == "Bm") {
                    //                            averageBalancePr = true;
                    //                            break;
                    //                        }
                    //                    }
                    //                    if (averageBalancePr == false) {
                    var share = $F("txtShareForPrimaryBank");
                    if (parseInt(share) == 100) {
                        $('divPrevRepHis' + bankNo).style.display = 'none';
                        $('divAverageBalancePr' + bankNo).style.display = 'block';
                        analystDetailsHelper.calculateAverageBalance(bankNo, accountType);
                    }
                    else {
                        alert("Average Balance is not applicable");
                        $('divPrevRepHis' + bankNo).style.display = 'block';
                        $('divAverageBalancePr' + bankNo).style.display = 'none';
                    }

                }
                else if (incomeAssBuss.INAM_METHODCODE == "FF") {
                    $('divAverageBalancePr' + bankNo).style.display = 'none';
                    $('divPrevRepHis' + bankNo).style.display = 'block';
                }
                else {
                    $('divAverageBalancePr' + bankNo).style.display = 'none';
                    $('divPrevRepHis' + bankNo).style.display = 'none';
                }
            }

        }
        else if (accountType == "Jt") {

            var incomeAssIdForBussJoint = $F("cmbIncomeAssementForJointBank" + bankNo);
            var incomeAssBussJoint = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBussJoint);
            if (incomeAssIdForBussJoint && incomeAssIdForBussJoint != undefined && incomeAssIdForBussJoint != "-1") {
                $("txtIncomeAssementCodeForJointBank" + bankNo).setValue(incomeAssBussJoint.INAM_METHODCODE);
                if (incomeAssBussJoint.INAM_METHODCODE == "FT") {
                    //Code Comment by Washik 26/12/2012
                    //                    var averageBalanceJt = false;

                    //                    for (var i = 0; i < Page.isAverageBalanceJointArray.length; i++) {
                    //                        if (Page.isAverageBalanceJointArray[i].BankNo == bankNo && Page.isAverageBalanceJointArray[i].Accounttype == accountType && Page.isAverageBalanceJointArray[i].TabName == "Bm") {
                    //                            averageBalanceJt = true;
                    //                            break;
                    //                        }
                    //                    }

                    //                    if (averageBalanceJt == false) {
                    var share = $F("txtShareForJointBank")
                    if (parseInt(share) == 100) {
                        $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                        $('divAverageBalanceJoint' + bankNo).style.display = 'block';
                        analystDetailsHelper.calculateAverageBalance(bankNo, accountType);
                    }
                    else {
                        alert("Average Balance is not applicable");
                        $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                        $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                    }
                }
                else if (incomeAssBussJoint.INAM_METHODCODE == "FF") {
                    $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                    $('divPrevRepHisJoint' + bankNo).style.display = 'block';
                }
                else {
                    $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                    $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                }
            }
        }
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'BM');
    },

    //Change Income Assement Method For SelfEmployed
    changeIncomeAssementForSelfEmployed: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEPrBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                $("txtIncomeAssessmentCodeForSEPrBank" + bankNo).setValue(incomeAssBuss.INAM_METHODCODE);
                $('txtConsiderMarginForSEPrBank' + bankNo).disabled = "";
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    //Code Comment By Washik 26/12/2012
                    //                var averageBalancePr = false;

                    //                for (var i = 0; i < Page.isAverageBalancePrimaryArray.length; i++) {
                    //                    if (Page.isAverageBalancePrimaryArray[i].BankNo == bankNo && Page.isAverageBalancePrimaryArray[i].Accounttype == accountType && Page.isAverageBalancePrimaryArray[i].TabName == "Se") {
                    //                        averageBalancePr = true;
                    //                        break;
                    //                    }
                    //                }
                    var share = $F("txtShareForSEPrBank" + bankNo);
                    if (parseInt(share) == 100) {
                        $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'none';
                        $('divAverageBalanceForSEPr' + bankNo).style.display = 'block';
                        $('txtConsiderMarginForSEPrBank' + bankNo).disabled = "false";
                        analystDetailsHelper.calculateAverageBalanceForSelfEmployed(bankNo, accountType);
                    }
                    else {
                        alert("Average Balance is not applicable");
                        $("cmbIncomeAssessmentForSEPrBank" + bankNo).setValue('-1');
                        $("txtIncomeAssessmentCodeForSEPrBank" + bankNo).setValue('');
                    }

                }
                else if (incomeAssBuss.INAM_METHODCODE == "FF") {
                    $('divAverageBalanceForSEPr' + bankNo).style.display = 'none';
                    $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'block';

                    var avergTotalCto12MonthPr = $F("cmbConsideredCtoForSEPrBank" + bankNo);
                    var considerMarginFinal = $F("txtConsiderMarginForSEPrBank" + bankNo);

                    var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                    $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncome);
                    $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncome);
                    var share = $F("txtShareForSEPrBank" + bankNo);
                    var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
                    $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotal);
                }
                else {
                    $('divAverageBalanceForSEPr' + bankNo).style.display = 'none';
                    $('divPrevRepHisForSEPrBank' + bankNo).style.display = 'none';

                    var avergTotalCto12MonthPr = $F("cmbConsideredCtoForSEPrBank" + bankNo);
                    var considerMarginFinal = $F("txtConsiderMarginForSEPrBank" + bankNo);

                    var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                    $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncome);
                    $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncome);
                    var share = $F("txtShareForSEPrBank" + bankNo);
                    var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
                    $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotal);
                }

            }

        }
        else if (accountType == "Jt") {

            var incomeAssIdForBussJoint = $F("cmbIncomeAssessmentForSEJtBank" + bankNo);
            var incomeAssBussJoint = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBussJoint);
            if (incomeAssBussJoint) {
                $("txtIncomeAssessmentCodeForSEJtBank" + bankNo).setValue(incomeAssBussJoint.INAM_METHODCODE);
                $('txtConsiderMarginForSEJtBank' + bankNo).disabled = "";
                if (incomeAssBussJoint.INAM_METHODCODE == "FT") {
                    //Code Comment by Washik 26/12/2012
                    //                var averageBalance = false;

                    //                for (var i = 0; i < Page.isAverageBalanceJointArray.length; i++) {
                    //                    if (Page.isAverageBalanceJointArray[i].BankNo == bankNo && Page.isAverageBalanceJointArray[i].Accounttype == accountType && Page.isAverageBalanceJointArray[i].TabName == "Se") {
                    //                        averageBalance = true;
                    //                        break;
                    //                    }
                    //                }

                    //if (averageBalance == false) {
                    var share = $F("txtShareForSEJtBank" + bankNo);
                    if (parseInt(share) == 100) {
                        $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'none';
                        $('divAverageBalanceForSEJt' + bankNo).style.display = 'block';
                        $('txtConsiderMarginForSEJtBank' + bankNo).disabled = "false";
                        analystDetailsHelper.calculateAverageBalanceForSelfEmployed(bankNo, accountType);
                    }
                    else {
                        alert("Average Balance is not applicable");
                        $("cmbIncomeAssessmentForSEJtBank" + bankNo).setValue('-1');
                        $("txtIncomeAssessmentCodeForSEJtBank" + bankNo).setValue('');
                    }
                }
                else if (incomeAssBussJoint.INAM_METHODCODE == "FF") {
                    $('divAverageBalanceForSEJt' + bankNo).style.display = 'none';
                    $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'block';

                    var avergTotalCto12MonthPr = $F("cmbConsideredCtoForSEJtBank" + bankNo);
                    var considerMarginFinal = $F("txtConsiderMarginForSEJtBank" + bankNo);

                    var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                    $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncome);
                    $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncome);
                    var share = $F("txtShareForSEJtBank" + bankNo);
                    var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
                    $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotal);
                }
                else {
                    $('divAverageBalanceForSEJt' + bankNo).style.display = 'none';
                    $('divPrevRepHisForSEJtBank' + bankNo).style.display = 'none';

                    var avergTotalCto12MonthPr = $F("cmbConsideredCtoForSEJtBank" + bankNo);
                    var considerMarginFinal = $F("txtConsiderMarginForSEJtBank" + bankNo);

                    var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                    $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncome);
                    $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncome);
                    var share = $F("txtShareForSEJtBank" + bankNo);
                    var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
                    $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotal);
                }
            }
        }
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'SE');
    },

    //Change Category For 100% Cash Sequred
    changeCategoryForCashSequre: function() {
        var categoryId = $F("cmbCategoryForLcCash");
        if (categoryId != "-1") {

            var categoryName = $('cmbCategoryForLcCash').options[$('cmbCategoryForLcCash').selectedIndex].text;
            $("txtEmployeeCategoryIdForLc").setValue(categoryId);
            $("txtEmployeeCategoryForLc").setValue(categoryName);
            $("txtEmployeeCategoryCodeForLc").setValue('N/A');

            if (Page.primaryProfessionId != 2) {
                analystDetailsManager.changeSectorForLcCashCovered(categoryId);
            }
        }
    },

    //Change CIB Info
    changeCibInfo: function() {
        var cibInfoId = $F("cmbBureauHistory");
        var cib = Page.cibInfo.findByProp('CibInfoId', cibInfoId);
        if (cib) {
            $("txtCibInfoDesc").setValue(cib.CibInfoDescription);
            $("txtCibInfoCode").setValue(cib.CibInfoCode);

        }
        else {
            $("txtCibInfoDesc").setValue('');
            $("txtCibInfoCode").setValue('');
        }
    },

    //Change Vendore
    changeVendore: function() {
        var vendoreId = $F("cmbVendor");
        if (vendoreId == "-1") {
            $('txtVendorName').disabled = '';
            $('txtVendoreCode').setValue('');
        }
        else {
            var vdr = Page.vendoreArray.findByProp('AutoVendorId', vendoreId);
            if (vdr) {
                $('txtVendoreCode').setValue(vdr.VendorCode);
                $('txtVendorName').disabled = 'false';
                analystDetailsManager.CheckMouByVendoreId();
            }
        }
    },

    //Change Manufacturar
    changeManufacture: function() {

        var manufactureId = $F("cmbManufacturer");
        if (manufactureId == "-1") {
            $('txtManufacturarCode').setValue('');
        }
        else {
            var mnf = Page.manufactureArray.findByProp('ManufacturerId', manufactureId);
            if (mnf) {
                $('txtManufacturarCode').setValue(mnf.ManufacturerCode);
                var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;
                if (manufacturName != "Other") {
                    $('cmbModel').style.display = 'block';
                    $('txtVehicleType').style.display = 'block';
                    $('txtModelName').style.display = 'none';
                    $('cmbVehicleType').style.display = 'none';

                    $('txtTrimLevel').disabled = 'false';
                    $('txtEngineCapacityCC').disabled = 'false';
                    $('txtVehicleType').disabled = 'false';

                    $('txtVehicleModelCode').setValue('');
                    analystDetailsManager.getModelByManufactureId();
                }
                else {
                    $('cmbModel').style.display = 'none';
                    $('txtVehicleType').style.display = 'none';
                    $('txtModelName').style.display = 'block';
                    $('cmbVehicleType').style.display = 'block';
                    $('txtTrimLevel').disabled = '';
                    $('txtEngineCapacityCC').disabled = '';
                    $('txtVehicleType').disabled = '';

                    if (Page.vehicleDetailsArray != null && Page.vehicleDetailsArray.AutoLoanMasterId != 0) {
                        $('txtModelName').setValue(Page.vehicleDetailsArray.Model);
                        $('txtTrimLevel').setValue(Page.vehicleDetailsArray.TrimLevel);
                        $('txtEngineCapacityCC').setValue(Page.vehicleDetailsArray.EngineCC);
                        //
                        if (Page.vehicleDetailsArray.VehicleType != "") {
                            $('cmbVehicleType').setValue(vehicleType[Page.vehicleDetailsArray.VehicleType]);
                        }
                    }
                }

            }
        }
    },

    //Change Model
    changeModel: function() {
        var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;
        if (manufacturName != "Other") {
            var vehicleId = $F('cmbModel');
            var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
            if (vehicleName != "Other") {
                $('txtVehicleType').style.display = 'block';
                $('cmbVehicleType').style.display = 'none';
                $('txtTrimLevel').disabled = 'false';
                $('txtEngineCapacityCC').disabled = 'false';
                $('txtVehicleType').disabled = 'false';

                var model = Page.modelArray.findByProp('VehicleId', vehicleId);
                if (model) {
                    $('txtTrimLevel').setValue(model.TrimLevel);
                    $('txtEngineCapacityCC').setValue(model.CC);
                    $('hdnModel').setValue(model.Model);
                    $('txtVehicleType').setValue(model.Type);
                    $('txtManufacturaryear').setValue(model.ManufacturingYear);
                    $('txtVehicleModelCode').setValue(model.ModelCode);
                }
                analystDetailsManager.Getvaluepackallowed();
            }
            else {
                $('txtVehicleType').style.display = 'none';
                $('cmbVehicleType').style.display = 'block';
                $('txtTrimLevel').disabled = '';
                $('txtEngineCapacityCC').disabled = '';
                $('txtVehicleType').disabled = '';
                if (Page.vehicleDetailsArray != null && Page.vehicleDetailsArray.AutoLoanMasterId != 0) {
                    //$('txtModelName').setValue(Page.vehicleDetailsArray.Model);
                    $('txtTrimLevel').setValue(Page.vehicleDetailsArray.TrimLevel);
                    $('txtEngineCapacityCC').setValue(Page.vehicleDetailsArray.EngineCC);
                    //
                    if (Page.vehicleDetailsArray.VehicleType != "") {
                        $('cmbVehicleType').setValue(vehicleType[Page.vehicleDetailsArray.VehicleType]);
                    }
                }
            }
        }

    },

    //Change Registration Year
    changeregistationyear: function() {
        var regiyear = $F("txtregistrationYear");
        if (!util.isDigit(regiyear)) {
            alert("Year must be a number field");
            $("txtregistrationYear").setValue('0');
            //$("txtregistrationYear").focus();
            return false;
        }
        analystDetailsHelper.calculateTenorSettings();
    },

    //Change Manufacturar Year Functionality
    changeManufactureryear: function() {
        var manuyear = $F("txtManufacturaryear");
        if (!util.isDigit(manuyear)) {
            alert("Manufacture Year must me a number Field");
            $('txtManufacturaryear').setValue('0');
            //$('txtManufacturaryear').focus();
            return false;
        }
        $("lblManufactureYearAlert").innerHTML = "";
        if (parseInt(manuyear) < 2007) {
            //alert("Car Import Docs Required");
            $("lblManufactureYearAlert").innerHTML = "Car Import Docs Required";
            $('divmanuyearalerttext').style.display = "block";
            $('divmanufacalertlbl').style.display = "block";
        }
        else {
            $('divmanuyearalerttext').style.display = "none";
            $('divmanufacalertlbl').style.display = "none";
        }
        analystDetailsHelper.calculateTenorSettings();
    },

    //Change type For Exposer
    changeTypeForExposer: function(elementId) {
        var type = $F(elementId);
        var typeobject = Page.facilityArray.findByProp('FACI_ID', type);
        if (typeobject) {
            switch (elementId) {
                case "cmbTypeForTermLoans1":
                    $("txtflag1").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans2":
                    $("txtflag2").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans3":
                    $("txtflag3").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans4":
                    $("txtflag4").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForCreditCard1":
                    $("txtFlagForcreditCard1").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForCreditCard2":
                    $("txtFlagForcreditCard2").setValue(typeobject.FACI_CODE);
                    break;
            }
        }
        else {
            switch (elementId) {
                case "cmbTypeForTermLoans1":
                    $("txtflag1").setValue('');
                    break;
                case "cmbTypeForTermLoans2":
                    $("txtflag2").setValue('');
                    break;
                case "cmbTypeForTermLoans3":
                    $("txtflag3").setValue('');
                    break;
                case "cmbTypeForTermLoans4":
                    $("txtflag4").setValue('');
                    break;
                case "cmbTypeForCreditCard1":
                    $("txtFlagForcreditCard1").setValue('');
                    break;
                case "cmbTypeForCreditCard2":
                    $("txtFlagForcreditCard2").setValue('');
                    break;
            }
        }

        analystDetailsHelper.populateColleterization();
        analystDetailsHelper.calculateInterestRate();
    },

    //populate Colleterization Combo
    populateColleterization: function() {
        var optlinkForColl = "<option value='-1'>Select From list</option>";
        var tablePrAccount = $('tblFacilityTermLoan');
        var rowCount = tablePrAccount.rows.length;

        for (var i = 1; i <= rowCount - 1; i++) {
            if ($F('cmbTypeForTermLoans' + i) == '-1') {
            }
            else {
                var facilityId = $F('cmbTypeForTermLoans' + i);
                var facilityName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                optlinkForColl += "<option value=" + facilityId + ">" + facilityName + "</option>";
            }
        }
        var prevcoll = $F("cmbColleterization");
        var prevCollWith = $F("cmbColleterizationWith");

        $("cmbColleterization").update(optlinkForColl);
        $("cmbColleterizationWith").update(optlinkForColl);

        $("cmbColleterization").setValue(prevcoll);
        $("cmbColleterizationWith").setValue(prevCollWith);

    },

    //Calculate LTV For Sequrity For Term Loans
    calculateLTVForSequrity: function(rowId) {
        var outStandingLimit = $F("txtoutStanding" + rowId);
        var originalLimit = $F("txtOriginalLimitForCreditCard" + rowId);
        if (originalLimit == "") {
            originalLimit = 0;
        }
        var sequrityPv = $F("txtSecurityPV" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $("txtSecurityPV" + rowId).setValue('0');
            $("txtLTVForSecurity" + rowId).setValue('0');
            return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtoutStanding' + rowId).setValue('0');
            $("txtLTVForSecurity" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurity' + rowId).setValue('0');
            $("txtSecurityPV" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv) * 100;

        var intr = parseFloat(originalLimit) + parseFloat(originalLimit * 0.05);

        $("txtInterestForCreditCard" + rowId).setValue(intr.toFixed(2));
        $("txtLTVForSecurity" + rowId).setValue(Math.round(ltv.toFixed(2)));
    },

    //Calculate EMI Share
    calculateEMIShare: function(rowId) {
        var emi = $F("txtEMI" + rowId);
        var emiPercesnt = $F("txtEMIPer" + rowId);
        if (emi == "") {
            emi = 0;
        }
        if (emiPercesnt == "") {
            emiPercesnt = 0;
        }
        if (!util.isFloat(emi)) {
            alert("Number field is required");
            $('txtEMI' + rowId).setValue('0');
            $("txtEMIShare" + rowId).setValue('0');
            //$("txtEMI" + rowId).focus();
            return false;
        }
        if (!util.isFloat(emiPercesnt)) {
            alert("Number field is required");
            $('txtEMIPer' + rowId).setValue('0');
            $("txtEMIShare" + rowId).setValue('0');
            //$("txtEMIPer" + rowId).focus();
            return false;
        }
        var emiShare = parseFloat(emi * parseFloat(emiPercesnt / 100));

        $("txtEMIShare" + rowId).setValue(Math.round(emiShare.toFixed(2)));

        analystDetailsHelper.createExistingrepaymentGrid();
    },

    //Calculate LTV For Credit Card Sequroty
    calculateLTVForCreditCardSequrity: function(rowId) {

        var outStandingLimit = $F("txtOriginalLimitForCreditCard" + rowId);
        var sequrityPv = $F("txtSecurityPVForCreditCard" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
            $('txtOriginalLimitForCreditCard' + rowId).setValue('0');
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            //return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            $("txtOriginalLimitForCreditCard" + rowId).setValue('0');
            //$("txtOriginalLimitForCreditCard" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            $("txtSecurityPVForCreditCard" + rowId).setValue('0');
            //$("txtSecurityPVForCreditCard" + rowId).focus();

            return false;
        }
        var ltv = Math.round(parseFloat(outStandingLimit / sequrityPv));
        if (ltv != Infinity) {
            $("txtLTVForSecurityForCreditCard" + rowId).setValue(Math.round(ltv.toFixed(2)));
        }
        else {
            $("txtLTVForSecurityForCreditCard" + rowId).setValue('0');
        }

        var origLimit = $F("txtOriginalLimitForCreditCard" + rowId);
        var inteRate = 5; //$F("txtInterestRateCreditCard" + rowId);

        var interest = Math.round((inteRate / 100) * origLimit);
        if (interest != Infinity) {
            $('txtInterestForCreditCard' + rowId).setValue(interest);
        }
        else {
            $('txtInterestForCreditCard' + rowId).setValue('0');
        }
        analystDetailsHelper.createExistingrepaymentGrid();

    },

    //Change Over Draft Interest
    changeoverDraftInterest: function(rowId) {

        var originalLimit = $F("txtOverdraftlimit" + rowId);
        var interestRate = $F("txtOverdraftInterest" + rowId);
        var sequrityPv = $F("txtSecurityPVForoverDraft" + rowId);
        if (originalLimit == "") {
            originalLimit = 0;
        }
        if (interestRate == "") {
            interestRate = 0;
        }


        if (!util.isFloat(originalLimit)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtOverdraftInteres" + rowId).setValue('0');
            //$("txtOverdraftlimit" + rowId).focus();
            return false;
        }
        if (!util.isFloat(interestRate)) {
            alert("Number field is required");
            $('txtOverdraftInterest' + rowId).setValue('0');
            $("txtOverdraftInteres" + rowId).setValue('0');
            //$("txtOverdraftInterest" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtSecurityPVForoverDraft" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var overDraftInterest = parseFloat(originalLimit) * parseFloat(parseFloat(interestRate / 100)) / 12;

        //$("txtOverdraftInteres" + rowId).setValue(Math.round(overDraftInterest.toFixed(2)));
        $("txtOverdraftInteres" + rowId).setValue(Math.round(overDraftInterest.toFixed(2)));

        if (sequrityPv == "") {
            sequrityPv = 0;
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            return false;
        }

        var ltv = parseFloat(originalLimit / sequrityPv) * 100;

        //$("txtLTVForSecurityForoverDraft" + rowId).setValue(Math.round(ltv.toFixed(2)));
        $("txtLTVForSecurityForoverDraft" + rowId).setValue(Math.round(ltv.toFixed(2)));
        analystDetailsHelper.createOverDraftStatemntForBank('1', rowId);

    },

    //Calculate LTV For Sequrity For Term Loan
    calculateLTVForSequrity: function(rowId) {
        var outStandingLimit = $F("txtoutStanding" + rowId);
        var sequrityPv = $F("txtSecurityPV" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $("txtSecurityPV" + rowId).setValue('0');
            $("txtLTVForSecurity" + rowId).setValue('0');
            return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtoutStanding' + rowId).setValue('0');
            $("txtLTVForSecurity" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurity' + rowId).setValue('0');
            $("txtSecurityPV" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv) * 100;

        $("txtLTVForSecurity" + rowId).setValue(Math.round(ltv.toFixed(2)));
    },

    //Calculate LTV For Sequrity For Credit Card
    //    calculateLTVForCreditCardSequrity: function(rowId) {
    //        var outStandingLimit = $F("txtOriginalLimitForCreditCard" + rowId);
    //        var sequrityPv = $F("txtSecurityPVForCreditCard" + rowId);
    //        if (outStandingLimit == "") {
    //            outStandingLimit = 0;
    //        }
    //        if (sequrityPv == "" || sequrityPv == "0") {
    //            sequrityPv = 0;
    //            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
    //            return false;
    //        }
    //        if (!util.isFloat(outStandingLimit)) {
    //            alert("Number field is required");
    //            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
    //            $("txtOriginalLimitForCreditCard" + rowId).setValue('0');
    //            //$("txtOriginalLimitForCreditCard" + rowId).focus();
    //            return false;
    //        }
    //        if (!util.isFloat(sequrityPv)) {
    //            alert("Number field is required");
    //            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
    //            $("txtSecurityPVForCreditCard" + rowId).setValue('0');
    //            //$("txtSecurityPVForCreditCard" + rowId).focus();

    //            return false;
    //        }
    //        var ltv = parseFloat(outStandingLimit / sequrityPv);

    //        $("txtLTVForSecurityForCreditCard" + rowId).setValue(ltv.toFixed(2));
    //    },

    // Calculate LTV For Sequrity For OverDrat
    calculateLTVForOverDraftSequrity: function(rowId) {
        var outStandingLimit = $F("txtOverdraftlimit" + rowId);
        var sequrityPv = $F("txtSecurityPVForoverDraft" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            //$("txtOverdraftlimit" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurityForoverDraft' + rowId).setValue('0');
            $("txtSecurityPVForoverDraft" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv) * 100;

        //$("txtLTVForSecurityForoverDraft" + rowId).setValue(Math.round(ltv.toFixed(2)));
        $("txtLTVForSecurityForoverDraft" + rowId).setValue(Math.round(ltv.toFixed(2)));
    },

    //Change credit TurnOverShare For OffSCB
    changeTurnOverShareForOffSCB: function(rowId) {
        var emi = $F("txtEMIforOffSCB" + rowId);
        var emiper = $F("txtEMIPerForOffSCB" + rowId);
        if (emi == "") {
            emi = 0;
        }
        if (emiper == "") {
            emiper = 0;
        }
        if (!util.isFloat(emi)) {
            alert("Number field is required");
            $('txtEMIforOffSCB' + rowId).setValue('0');
            $("txtEMIShareForOffSCB" + rowId).setValue('0');
            //$("txtEMIforOffSCB" + rowId).focus();
            return false;
        }
        if (!util.isFloat(emiper)) {
            alert("Number field is required");
            $('txtEMIPerForOffSCB' + rowId).setValue('0');
            $("txtEMIShareForOffSCB" + rowId).setValue('0');
            //$("txtEMIPerForOffSCB" + rowId).focus();
            return false;
        }
        var emiShare = Math.round(parseFloat(emi * parseFloat(emiper / 100)));

        $("txtEMIShareForOffSCB" + rowId).setValue(emiShare.toFixed(2));
        analystDetailsHelper.createExistingrepaymentGrid();
    },

    //Change credit Card For Interest Rate
    changeCreditCardInterestForOffSCB: function(rowId) {
        var originalLimit = $F("txtOriginalLimitForcreditCardOffScb" + rowId);
        var interestRate = 5;
        if (originalLimit == "") {
            originalLimit = 0;
        }
        if (interestRate == "") {
            interestRate = 0;
        }
        if (!util.isFloat(originalLimit)) {
            alert("Number field is required");
            $('txtOriginalLimitForcreditCardOffScb' + rowId).setValue('0');
            $("txtOriginalLimitForcreditCardOffScb" + rowId).setValue('0');
            //$("txtOriginalLimitForcreditCardOffScb" + rowId).focus();
            return false;
        }
        //        if (!util.isFloat(interestRate)) {
        //            alert("Number field is required");
        //            $('txtInterestRateForCreditCardForOffScb' + rowId).setValue('0');
        //            $("txtInterestRateForCreditCardForOffScb" + rowId).setValue('0');
        //            //$("txtInterestRateForCreditCardForOffScb" + rowId).focus();
        //            return false;
        //        }
        var totalInterest = parseFloat(originalLimit * parseFloat(5 / 100));

        $("txtInterestForCreditCardForOffScb" + rowId).setValue(totalInterest.toFixed(2));

        analystDetailsHelper.createExistingrepaymentGrid();
    },

    //Change OverDraft Interest For Off Scb
    changeOverDraftInterstForOffSCB: function(rowId) {
        var originalLimit = $F("txtOriginalLimitForOverDraftOffScb" + rowId);
        var interestRate = $F("txtinterestRateForOverDraftOffSCB" + rowId);
        if (originalLimit == "") {
            originalLimit = 0;
        }
        if (interestRate == "") {
            interestRate = 0;
        }
        if (!util.isFloat(originalLimit)) {
            alert("Number field is required");
            $('txtOriginalLimitForOverDraftOffScb' + rowId).setValue('0');
            $("txtInterestForOverDraftInterest" + rowId).setValue('0');
            //$("txtOriginalLimitForOverDraftOffScb" + rowId).focus();
            return false;
        }
        if (!util.isFloat(interestRate)) {
            alert("Number field is required");
            $('txtOriginalLimitForOverDraftOffScb' + rowId).setValue('0');
            $("txtInterestForOverDraftInterest" + rowId).setValue('0');
            $("txtinterestRateForOverDraftOffSCB" + rowId).setValue('0');
            //$("txtOriginalLimitForOverDraftOffScb" + rowId).focus();

            return false;
        }
        var overDraftInterest = parseFloat(parseFloat(originalLimit * parseFloat(interestRate / 12)) / 100);

        $("txtInterestForOverDraftInterest" + rowId).setValue(Math.round(overDraftInterest.toFixed(2)));

        analystDetailsHelper.createOverDraftStatemntForBank('2', rowId);

    },

    //Create Existing Repayment Grid

    changeStatusForTermLoans: function(loanType, rowid) {
        //LoanType 1=OnScb, 2=OffScb
        
        analystDetailsHelper.createExistingrepaymentGrid();
        //On SCB
        if (loanType == 1) {
            var emishare = $F("txtEMIShare" + rowid);
            var emiStatus = $F("cmbStatusForSecurities" + rowid);
            //Bussiness Man Tab

            //Primary
            if (Page.primaryProfessionId == 1 || Page.otherProfessionId == 1) {
                if (emiStatus == "2" || emiStatus == "3") {
                    for (var i = 0; i < Page.prCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $("txtEmi" + rowid + "ForOnScbForPrimaryBank" + bankno).setValue(emishare);
                            $("cmbConsider" + rowid + "ForOnSCBReplHisForPrimaryBank" + bankno).setValue('0');
                        }
                    }
                }
                else {
                    for (var i = 0; i < Page.prCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $("txtEmi" + rowid + "ForOnScbForPrimaryBank" + bankno).setValue('');
                            $("cmbConsider" + rowid + "ForOnSCBReplHisForPrimaryBank" + bankno).setValue('0');
                        }
                    }
                }
                analystDetailsHelper.considerPrevrepHisOnScb(rowid, 'BM', 'Pr');
            }
            //Joint
            if (Page.isjoint == true && Page.jointPrimaryProfessionId == 1) {
                if (emiStatus == "2" || emiStatus == "3") {
                    for (var i = 0; i < Page.jtCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtEmiOnScb' + rowid + 'ForJointBank' + bankno).setValue(emishare);
                            $('cmbConsiderOnScb' + rowid + 'ForJointBank' + bankno).setValue('0');
                        }
                    }
                }
                else {
                    for (var i = 0; i < Page.jtCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtEmiOnScb' + rowid + 'ForJointBank' + bankno).setValue('');
                            $('cmbConsiderOnScb' + rowid + 'ForJointBank' + bankno).setValue('0');
                        }
                    }
                }
                analystDetailsHelper.considerPrevrepHisOnScb(rowid, 'BM', 'Jt');
            }

            //Self Employed Tab
            if (Page.primaryProfessionId == 3 || Page.otherProfessionId == 3) {
                if (emiStatus == "2" || emiStatus == "3") {
                    for (var i = 0; i < Page.prCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $("txtEmi" + rowid + "ForOnScbForSEPrBank" + bankno).setValue(emishare);
                            $("cmbConsider" + rowid + "ForOnSCBReplHisForSEPrBank" + bankno).setValue('0');
                        }
                    }
                }
                else {
                    for (var i = 0; i < Page.prCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $("txtEmi" + rowid + "ForOnScbForSEPrBank" + bankno).setValue('');
                            $("cmbConsider" + rowid + "ForOnSCBReplHisForSEPrBank" + bankno).setValue('0');
                        }
                    }
                }
                analystDetailsHelper.considerPrevrepHisOnScb(rowid, 'SE', 'Pr');
            }
            //
            if (Page.isjoint == true && Page.jointPrimaryProfessionId == 3) {
                if (emiStatus == "2" || emiStatus == "3") {
                    for (var i = 0; i < Page.jtCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtEmiOnScb' + rowid + 'ForJointBank' + bankno).setValue(emishare);
                            $('cmbConsiderOnScb' + rowid + 'ForJointBank' + bankno).setValue('0');
                        }
                    }
                }
                else {
                    for (var i = 0; i < Page.jtCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtEmi' + rowid + 'ForOnScbForSEJtBank' + bankno).setValue('');
                            $('cmbConsider' + rowid + 'ForOnSCBReplHisForSEJtBank' + bankno).setValue('0');
                        }
                    }
                }
                analystDetailsHelper.considerPrevrepHisOnScb(rowid, 'SE', 'Jt');
            }
        }

        //Off SCB
        if (loanType == 2) {
            var emishare = $F("txtEMIShareForOffSCB" + rowid);
            var emiStatus = $F("cmbStatusForOffScb" + rowid);
            var instituteName = $('cmbFinancialInstitutionForOffSCB' + rowid).options[$('cmbFinancialInstitutionForOffSCB' + rowid).selectedIndex].text;
            var bankId = $F('cmbFinancialInstitutionForOffSCB' + rowid);
            //Bussiness Man Tab

            //Primary Bussiness Man Tab
            if (Page.primaryProfessionId == 1 || Page.otherProfessionId == 1) {
                if (emiStatus == "2" || emiStatus == "3") {
                    for (var i = 0; i < Page.prCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtInstitute' + rowid + 'ForOffSCBPrimaryBank' + bankno).setValue(instituteName);
                            $('txtBank' + rowid + 'BMPr' + bankno).setValue(bankId);
                            $('txtEMI' + rowid + 'ForOffSCBPrimaryBank' + bankno).setValue(emishare);
                            $('cmbConsider' + rowid + 'ForPrimaryOffScbBank' + bankno).setValue('0');
                        }
                    }
                }
                else {
                    for (var i = 0; i < Page.prCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtInstitute' + rowid + 'ForOffSCBPrimaryBank' + bankno).setValue('');
                            $('txtBank' + rowid + 'BMPr' + bankno).setValue('-1');
                            $('txtEMI' + rowid + 'ForOffSCBPrimaryBank' + bankno).setValue('');
                            $('cmbConsider' + rowid + 'ForPrimaryOffScbBank' + bankno).setValue('0');
                        }
                    }
                }
                analystDetailsHelper.considerPrevrepHis(rowid, 'BM', 'Pr');
            }

            // Joint Bussiness Man Tab
            if (Page.isjoint == true && Page.jointPrimaryProfessionId == 1) {
                if (emiStatus == "2" || emiStatus == "3") {
                    for (var i = 0; i < Page.jtCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtInstituteOffScb' + rowid + 'ForJointBank' + bankno).setValue(instituteName); //txtInstituteOffScb
                            $('txtBank' + rowid + 'BMJt' + bankno).setValue(bankId);
                            $('txtEmiOffScb' + rowid + 'ForJointBank' + bankno).setValue(emishare);
                            $('cmbConsiderOffScb' + rowid + 'ForJointBank' + bankno).setValue('0');
                        }
                    }
                }
                else {
                    for (var i = 0; i < Page.jtCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtInstituteOffScb' + rowid + 'ForJointBank' + bankno).setValue(''); //txtInstituteOffScb
                            $('txtBank' + rowid + 'BMJt' + bankno).setValue('-1');
                            $('txtEmiOffScb' + rowid + 'ForJointBank' + bankno).setValue('');
                            $('cmbConsiderOffScb' + rowid + 'ForJointBank' + bankno).setValue('0');
                        }
                    }
                }
                analystDetailsHelper.considerPrevrepHis(rowid, 'BM', 'Jt');
            }

            //Self Employed Tab

            if (Page.primaryProfessionId == 3 || Page.otherProfessionId == 3) {
                if (emiStatus == "2" || emiStatus == "3") {
                    for (var i = 0; i < Page.prCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtInstitute' + rowid + 'ForOffSCBSEPrBank' + bankno).setValue(instituteName);
                            $('txtBank' + rowid + 'SEPr' + bankno).setValue(bankId);
                            $('txtEMI' + rowid + 'ForOffSCBSEPrBank' + bankno).setValue(emishare);
                            $('cmbConsider' + rowid + 'ForSEPrOffScbBank' + bankno).setValue('0');
                        }
                    }
                }
                else {
                    for (var i = 0; i < Page.prCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtInstitute' + rowid + 'ForOffSCBSEPrBank' + bankno).setValue('');
                            $('txtBank' + rowid + 'SEPr' + bankno).setValue('-1');
                            $('txtEMI' + rowid + 'ForOffSCBSEPrBank' + bankno).setValue('');
                            $('cmbConsider' + rowid + 'ForSEPrOffScbBank' + bankno).setValue('0');
                        }
                    }
                }
                analystDetailsHelper.considerPrevrepHis(rowid, 'SE', 'Pr');
            }
            //Joint
            if (Page.isjoint == true && Page.jointPrimaryProfessionId == 3) {
                if (emiStatus == "2" || emiStatus == "3") {
                    for (var i = 0; i < Page.jtCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue(instituteName);
                            $('txtBank' + idfieldforExposerForTermsloans + 'SEJt' + bankno).setValue(bankId);
                            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue(emishare);
                            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank' + bankno).setValue('0');
                        }
                    }
                }
                else {
                    for (var i = 0; i < Page.jtCount; i++) {
                        bankno = i + 1;
                        if (bankno < 7) {
                            $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue('');
                            $('txtBank' + idfieldforExposerForTermsloans + 'SEJt' + bankno).setValue('-1');
                            $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue('');
                            $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank' + bankno).setValue('0');
                        }
                    }
                }
                analystDetailsHelper.considerPrevrepHis(rowid, 'SE', 'Jt');
            }
        }
    },

    createExistingrepaymentGrid: function() {
        var limit = 0;
        var interest = 0;
        var typeName = "";
        var bankName = "";
        var emiShare = 0;
        var totalInterestOnScbCreditCard = 0;
        var totalInterestOffScbCreditcard = 0;
        var totalEmiShareOnScb = 0;
        var totalEmiShareOffScb = 0;
        //credit card----------------
        for (var j = 1; j < 3; j++) {
            var sequrityForCreditCard = $F("cmbStatusForSecuritiesForCreditCard" + j);
            if (sequrityForCreditCard == "1") {
                limit = $F("txtOriginalLimitForCreditCard" + j);
                interest = $F("txtInterestForCreditCard" + j);
                if (util.isFloat(interest) && !util.isEmpty(interest)) {
                    $("txtLimitCarditcardForLcExistingPaymentForScb" + j).setValue(limit);
                    $("txtInterestcreditcardForLcExistingPaymentForScb" + j).setValue(interest);
                    totalInterestOnScbCreditCard += parseFloat(interest);
                }
                limit = 0;
                interest = 0;
            }
            else {
                $("txtLimitCarditcardForLcExistingPaymentForScb" + j).setValue('');
                $("txtInterestcreditcardForLcExistingPaymentForScb" + j).setValue('');
            }
            var sequrityCardOffScb = $F("cmbStatusForCreditCardOffScb" + j);
            if (sequrityCardOffScb == "1") {
                limit = $F("txtOriginalLimitForcreditCardOffScb" + j);
                interest = $F("txtInterestForCreditCardForOffScb" + j);
                if (util.isFloat(interest) && !util.isEmpty(interest)) {
                    $("txtLimitCarditcardForLcExistingPaymentForOffScb" + j).setValue(limit);
                    $("txtInterestcreditcardForLcExistingPaymentOnScb" + j).setValue(interest);
                    totalInterestOffScbCreditcard += parseFloat(interest);
                }
                limit = 0;
                interest = 0;
            }
            else {
                $("txtLimitCarditcardForLcExistingPaymentForOffScb" + j).setValue('');
                $("txtInterestcreditcardForLcExistingPaymentOnScb" + j).setValue('');
            }
        }
        $("txtTotalinterestForOnScbLcExistingrepayment").setValue(totalInterestOnScbCreditCard);
        $("txtTotalinterestForOffScbLcExistingrepayment").setValue(totalInterestOffScbCreditcard);
        //Term loans
        var sequrityForTerLoanOnScb = "";
        var sequrityForTermLoanOffScb = "";
        for (var i = 1; i < 5; i++) {
            sequrityForTerLoanOnScb = $F("cmbStatusForSecurities" + i);
            if (sequrityForTerLoanOnScb == "1") {
                typeName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                emiShare = $F("txtEMIShare" + i);
                if (util.isFloat(emiShare) && !util.isEmpty(emiShare)) {
                    $("txtTypeForLcExistingPayment" + i).setValue(typeName);
                    $("txtEmiForLcExistingPayment" + i).setValue(emiShare);
                    totalEmiShareOnScb += parseFloat(emiShare);
                }

            }
            else {
                $("txtTypeForLcExistingPayment" + i).setValue('');
                $("txtEmiForLcExistingPayment" + i).setValue('');
            }
            sequrityForTermLoanOffScb = $F("cmbStatusForOffScb" + i);
            var bankId = $F("cmbFinancialInstitutionForOffSCB" + i);
            if (sequrityForTermLoanOffScb == "1") {
                bankName = $('cmbFinancialInstitutionForOffSCB' + i).options[$('cmbFinancialInstitutionForOffSCB' + i).selectedIndex].text;
                emiShare = $F("txtEMIShareForOffSCB" + i);
                if (util.isFloat(emiShare) && !util.isEmpty(emiShare)) {
                    $("txtBankForLcExistingPaymentOffScb" + i).setValue(bankName);
                    $("txtEmiForLcExistingPaymentOffScb" + i).setValue(emiShare);
                    totalEmiShareOffScb += parseFloat(emiShare);
                }

            }
            else {
                $("txtBankForLcExistingPaymentOffScb" + i).setValue('');
                $("txtEmiForLcExistingPaymentOffScb" + i).setValue('');
            }

        }
        $("txtTotalEmiForLcExistingPaymentOnScb").setValue(totalEmiShareOnScb);
        $("txtTotalEmiForLcExistingPaymentOffScb").setValue(totalEmiShareOffScb);

        analystDetailsHelper.changeConsideredDbr();

    },

    //Get Deviation Code by Id
    getDeviationCodeByID: function(controlId) {
        var devCode = "";
        var devid = $F(controlId);
        var dev = Page.deviationArray.findByProp('DeviationId', devid);
        if (dev) {
            devCode = dev.DeviationCode;
        }
        if (controlId == 'cmdDeviationDescription1') {
            $('txtDeviationCode1').setValue(devCode);
        }
        else if (controlId == 'cmdDeviationDescription2') {
            $('txtDeviationCode2').setValue(devCode);
        }
        else if (controlId == 'cmdDeviationDescription3') {
            $('txtDeviationCode3').setValue(devCode);
        }
        else if (controlId == 'cmdDeviationDescription4') {
            $('txtDeviationCode4').setValue(devCode);
        }
    },

    //Is Max function
    isMax: function(listofArray, value) {
        var ismax = true;
        for (var i = 0; i < listofArray.length; i++) {
            if (parseFloat(value) < parseFloat(listofArray[i])) {
                ismax = false;
                break;
            }
        }
        return ismax;
    },

    //Calculate tenor Settings
    calculateTenorSettings: function() {

        var artaPreferenceId = $F("cmbARTAReference");
        var artaAllowed = 6;
        var arta = $F("cmbARTA");
        if (arta == "1" || arta == "3") {
            var artaPreference = Page.artaPreference.findByProp("InsCompanyId", artaPreferenceId);
            if (artaPreference) {
                var artaAge = artaPreference.InsAgeLimit;
                var currentAge = Page.age;
                artaAllowed = parseInt(artaAge) - parseInt(currentAge);
                if (artaAllowed > 6) {
                    artaAllowed = 6;
                }
                if (artaAllowed < 0) {
                    artaAllowed = 0;
                }
            }

            $("txtArtaAllowedFoLc").setValue(artaAllowed);
        }
        else {
            $("txtArtaAllowedFoLc").setValue(artaAllowed);
        }


        //$("txtvehicleAllowedForLc").setValue(Page.tenor);

        var vehicleId = $F("cmbModel");
        var tenor = 0;
        if (vehicleId != "-1") {
            var model = Page.modelArray.findByProp('VehicleId', vehicleId);
            if (model) {
                var vehicleTenor = model.Tenor;
                var manufacuturingYear = 0;

                if (util.isFloat($F("txtManufacturaryear")) && $F("txtManufacturaryear") != "") {
                    manufacuturingYear = $F("txtManufacturaryear");
                }
                var currentAge = new Date().getFullYear()
                var ageDif = currentAge - parseInt(manufacuturingYear);
                tenor = vehicleTenor - ageDif;
                if (tenor > 6) {
                    tenor = 6;
                }
                if (tenor < 0) {
                    tenor = 0;
                }

                Page.tenor = tenor;
            }
        }

        $("txtvehicleAllowedForLc").setValue(tenor);

        var label = $F("cmbLabelForTonerLc");
        var ageAllowed = 0;
        //        var ageAllowed = 0;
        //        if (label == "1") {
        //            var maxAge = 65;
        //            var currentAge = $F("txtCurrenAge");
        //            var agedif = maxAge - parseInt(currentAge);
        //            if (ageDif > 6) {
        //                ageAllowed = 6;
        //            }
        //            if (ageDif < 0) {
        //                ageAllowed = 0;
        //            }
        //        }
        //        else {
        //            var maxAge = 68;
        //            var currentAge = $F("txtCurrenAge");
        //            var agedif = maxAge - parseInt(currentAge);
        //            if (ageDif > 6) {
        //                ageAllowed = 6;
        //            }
        //            if (ageDif < 0) {
        //                ageAllowed = 0;
        //            }
        //        }
        //        $("txtAgeAllowedForLc").setValue(ageAllowed);

        var segment = $F("cmbEmployeeSegmentLc");

        if (segment == "-1") {
            $("txtAgeAllowedForLc").setValue('0');

        }
        else {
            var sg = Page.segmentArray.findByProp("SegmentID", segment);
            var maxAgeDif = sg.MaxAgeL2;
            var minAgeDif = sg.MinAgeL2;
            if (label == "2") {
                var msAgeSeg = sg.MaxAgeL1 + maxAgeDif;
                var currentAge = $F("txtCurrenAge");
                var agedif = msAgeSeg - parseInt(currentAge);
                if (agedif > 6) {
                    ageAllowed = 6;
                }
                else if (agedif < 0) {
                    ageAllowed = 0;
                }
                else {
                    ageAllowed = agedif;
                }

                //                var minageForlbl2 = sg.MinAgeL1;
                //                var maxageForlbl2 = sg.MaxAgeL1;
                //                if (parseFloat(Page.age) >= parseFloat(minageForlbl2) && parseFloat(Page.age) <= parseFloat(maxageForlbl2)) {
                //                    $("txtAgeAllowedForLc").setValue(sg.MaxTanor);
                //                }
                //                else {
                //                    $("txtAgeAllowedForLc").setValue(sg.MinTanor);
                //                }
            }
            else {
                var msAgeSeg = sg.MaxAgeL1;
                var currentAge = $F("txtCurrenAge");
                var agedif = msAgeSeg - parseInt(currentAge);
                if (agedif > 6) {
                    ageAllowed = 6;
                }
                else if (agedif < 0) {
                    ageAllowed = 0;
                }
                else {
                    ageAllowed = agedif;
                }
                //                var minageForlbl1 = sg.MinAgeL1;
                //                var maxageForlbl1 = sg.MaxAgeL1;

                //                if (parseFloat(Page.age) >= parseFloat(minageForlbl1) && parseFloat(Page.age) <= parseFloat(maxageForlbl1)) {
                //                    $("txtAgeAllowedForLc").setValue(sg.MaxTanor);
                //                }
                //                else {
                //                    $("txtAgeAllowedForLc").setValue(sg.MinTanor);
                //                }
            }
        }
        $("txtAgeAllowedForLc").setValue(ageAllowed);
        var askingTenor = parseFloat($F("txtAskingtenor")) / 12;
        $("txtAskingTenorForLc").setValue(askingTenor);
        if (parseFloat(artaAllowed) > parseFloat(askingTenor)) {
            $("txtAppropriteTenorForLc").setValue(askingTenor);
        }
        else {
            $("txtAppropriteTenorForLc").setValue(artaAllowed);
        }
        if (util.isFloat($F("txtCosideredtenorForLc")) && !util.isEmpty($F("txtCosideredtenorForLc"))) {
            var consideredTenor = parseFloat($F("txtCosideredtenorForLc")) * 12;
            $("txtConsideredMarginForLc").setValue(consideredTenor);
        }
        else {
            $("txtCosideredtenorForLc").setValue('0');
            var consideredTenor = parseFloat($F("txtCosideredtenorForLc")) * 12;
            $("txtConsideredMarginForLc").setValue(consideredTenor);
        }
        analystDetailsHelper.calculateInterestRate();

        var lbl = parseInt($F("cmbLabelForDlaLimit"));
        if (parseInt(label) > lbl) {
            $("cmbLabelForDlaLimit").setValue(label);
            var approverId = $F("cmbApprovedby");
            if (approverId != "-1") {
                analystDetailsManager.GetApproverLimitByUserId(approverId);
            }
        }
        else {
            analystDetailsHelper.setagewiseLebel();
        }
    },

    //Calculate Interest Rate
    calculateInterestRate: function() {

        var artaPreferenceId = $F("cmbARTAReference");

        var vendoreId = $F("cmbVendor");

        var artaAllowed = 0;
        var vendoreArtaAllowed = 0;
        var segmentAllowed = 0;
        var appropriateAllowed = 0;
        var bdcatAllowed = 0;
        var interestAllowedArray = [];



        var artaPreference = Page.artaPreference.findByProp("InsCompanyId", artaPreferenceId);
        if (artaPreference) {
            artaAllowed = artaPreference.ArtaRate;
        }

        var arta = $F("cmbARTA");
        var generalIns = $F("cmbGeneralInsurence");
        if (arta == "1" || arta == "3") {
            var vendor = Page.vendoreArray.findByProp("AutoVendorId", vendoreId);
            if (vendor) {
                vendoreArtaAllowed = vendor.IrWithArta;
            }
            $("txtVendorwithArta").setValue(vendoreArtaAllowed);

            $("txtInterestLCVendoreAllowed").setValue($F("txtVendorwithArta"));

            if (arta == "1" && generalIns == "1") {
                artaAllowed = vendoreArtaAllowed
            }
            else {
                artaAllowed = vendoreArtaAllowed + artaAllowed;
            }

            $("txtInterestLCArtaAllowed").setValue(artaAllowed);
            vendoreArtaAllowed = $F("txtVendorwithArta");
            interestAllowedArray.push(artaAllowed);
            interestAllowedArray.push(vendoreArtaAllowed);
        }
        else {
            var vendor = Page.vendoreArray.findByProp("AutoVendorId", vendoreId);
            if (vendor) {
                vendoreArtaAllowed = vendor.IrWithoutArta;
            }
            $("txtVendorwithoutArta").setValue(vendoreArtaAllowed);

            $("txtInterestLCVendoreAllowed").setValue($F("txtVendorwithoutArta"));
            artaAllowed = vendoreArtaAllowed + artaAllowed;
            $("txtInterestLCArtaAllowed").setValue(artaAllowed);
            vendoreArtaAllowed = $F("txtVendorwithoutArta");
            interestAllowedArray.push(vendoreArtaAllowed);
            interestAllowedArray.push(artaAllowed);
        }
        var segment = $F("cmbEmployeeSegmentLc");

        if (segment == "-1") {
            $("txtAgeAllowedForLc").setValue('0');

        }
        else {
            var sg = Page.segmentArray.findByProp("SegmentID", segment);
            if (sg) {
                if (arta == "1") {
                    $("txtInterestLCSegmentAllowed").setValue(sg.IrWithARTA);
                    segmentAllowed = sg.IrWithARTA;
                    interestAllowedArray.push(segmentAllowed);
                }
                else {
                    $("txtInterestLCSegmentAllowed").setValue(sg.IrWithoutARTA);
                    segmentAllowed = sg.IrWithoutARTA;
                    interestAllowedArray.push(segmentAllowed);
                }
                bdcatAllowed = sg.MaxLoanAmt;
            }
            else {
                $("txtInterestLCSegmentAllowed").setValue('0');
            }
        }
        var askingInterestRate = $F("txtInterestLCAskingRate");
        if (!util.isFloat(askingInterestRate)) {
            askingInterestRate = 0;
        }

        interestAllowedArray.push(parseFloat(askingInterestRate));
        var productId = $F("cmbProductName");
        var artamin = interestAllowedArray.sort(function(a, b) { return a - b });

        if (productId == "1") {
            $("txtAppropriteRate").setValue('4.43');
        }
        else {
            $("txtAppropriteRate").setValue(artamin[0]);
        }
        $("txtInterestLCAskingRate").setValue($F("txtAskingInterest"));

        //General Insurence



        //        var gins = $F("cmbGeneralInsurence");
        //        if (gins == "1") {
        //            var ownDamage = 0;
        //            var actliability = 0;
        //            var ingineCc = $F("txtEngineCapacityCC");
        //            for (var i = 0; i < Page.ownDamageArray.length; i++) {
        //                var ccfrom = Page.ownDamageArray[i].CCFrom;
        //                var ccTo = Page.ownDamageArray[i].CCTo;
        //                if (parseInt(ingineCc) > parseInt(ccfrom) && parseInt(ingineCc) < parseInt(ccTo)) {
        //                    ownDamage = Page.ownDamageArray[i].OwnDamageCharge;
        //                    actliability = Page.ownDamageArray[i].ActLiability;
        //                    break;
        //                }
        //            }


        //            var seatingCapacity = $F("cmsSeatingCapacity");
        //            if (seatingCapacity == "-1") {
        //                seatingCapacity = 0;
        //            }
        //            var passengerPerseatingCapacity = $F("txtPassenger");
        //            var passengerAmount = parseFloat(seatingCapacity) * parseFloat(passengerPerseatingCapacity);
        //            var driverSeatAmount = $F("txtAmountofDriver");

        //            var quotedPrice = $F("txtQuotedPrice");
        //            var acidentoftheft = (parseFloat(quotedPrice) * parseFloat($F("txtaccidentOrTheft"))) / 100;
        //            var totalbeforeVat = parseFloat(driverSeatAmount) + parseFloat(passengerAmount) + parseFloat(acidentoftheft) + parseFloat(actliability) + parseFloat(ownDamage);

        //            var vat = ((parseFloat(totalbeforeVat) * parseFloat($F("txtVat"))) / 100);
        //            var stampcharge = $F("txtStampCharge");
        //            var commision = ((parseFloat(totalbeforeVat) * parseFloat($F("txtCommision"))) / 100);
        //            var grossAmount = parseFloat(totalbeforeVat) + parseFloat(vat) + parseFloat(stampcharge);
        //            var generalInsurenceAmount = parseFloat(grossAmount) - parseFloat(commision);
        //            var finalAmount = Math.round(generalInsurenceAmount);
        //            $("txtgeneralInsurenceForLcIns").setValue(finalAmount);
        //            $("txtArtaForLCInsurenceAmount").setValue('0');
        //            $("txtTotalInsurenceForLcInsurence").setValue(finalAmount);

        //        }
        //        else {
        //            $("txtgeneralInsurenceForLcIns").setValue('0');
        //            var approvedamount = 0;
        //            var loantenor = 0;
        //            //var age = Page.age;
        //            if (util.isFloat($F("txtApprovedLoanAmountForLcLoanAmount")) && !util.isEmpty($F("txtApprovedLoanAmountForLcLoanAmount"))) {
        //                approvedamount = $F("txtApprovedLoanAmountForLcLoanAmount");
        //            }
        //            if (util.isFloat($F("txtCosideredtenorForLc")) && !util.isEmpty($F("txtCosideredtenorForLc"))) {
        //                loantenor = $F("txtCosideredtenorForLc");
        //            }



        //            var insurenceAmount = parseFloat((parseFloat(approvedamount) * parseFloat(artaAllowed)) / 100) * parseFloat(loantenor);
        //            $("txtArtaForLCInsurenceAmount").setValue(insurenceAmount);
        //            $("txtTotalInsurenceForLcInsurence").setValue(insurenceAmount);

        //        }


        if (arta == "1" || arta == "3") {
            var artaPreference = Page.artaPreference.findByProp("InsCompanyId", artaPreferenceId);
            if (artaPreference) {
                artaAllowed = artaPreference.InsurancePercent;
            }
            var approvedamount = 0;
            var loantenor = 0;
            if (util.isFloat($F("txtApprovedLoanAmountForLcLoanAmount")) && !util.isEmpty($F("txtApprovedLoanAmountForLcLoanAmount"))) {
                approvedamount = $F("txtApprovedLoanAmountForLcLoanAmount");
            }
            if (util.isFloat($F("txtCosideredtenorForLc")) && !util.isEmpty($F("txtCosideredtenorForLc"))) {
                loantenor = $F("txtCosideredtenorForLc");
            }
            var insurenceAmount = parseFloat((parseFloat(approvedamount) * parseFloat(artaAllowed)) / 100) * parseFloat(loantenor);
            $("txtArtaForLCInsurenceAmount").setValue(Math.round(insurenceAmount));

            var gnrIns = 0;
            if (util.isFloat($F("txtgeneralInsurenceForLcIns")) && !util.isEmpty($F("txtgeneralInsurenceForLcIns"))) {
                gnrIns = $F("txtgeneralInsurenceForLcIns");
            }

            $("txtTotalInsurenceForLcInsurence").setValue(parseInt(insurenceAmount) + parseInt(gnrIns));
        }
        else {
            $("txtArtaForLCInsurenceAmount").setValue('0');
            var gnrIns = 0;
            if (util.isFloat($F("txtgeneralInsurenceForLcIns")) && !util.isEmpty($F("txtgeneralInsurenceForLcIns"))) {
                gnrIns = $F("txtgeneralInsurenceForLcIns");
            }
            $("txtTotalInsurenceForLcInsurence").setValue(gnrIns);
        }

        var gins = $F("cmbGeneralInsurence");
        var genAllowed = 0;
        if (gins == "1" || gins == "3") {
            var generalInsPrefor = $F("cmbGeneralInsurencePreference");
            var genPreference = Page.artaPreference.findByProp("InsCompanyId", generalInsPrefor);
            if (genPreference) {
                genAllowed = genPreference.InsurancePercent;
            }
            var ownDamage = 0;
            var actliability = 0;
            var ingineCc = $F("txtEngineCapacityCC");
            for (var i = 0; i < Page.ownDamageArray.length; i++) {
                var ccfrom = Page.ownDamageArray[i].CCFrom;
                var ccTo = Page.ownDamageArray[i].CCTo;
                if (parseInt(ingineCc) > parseInt(ccfrom) && parseInt(ingineCc) < parseInt(ccTo)) {
                    ownDamage = Page.ownDamageArray[i].OwnDamageCharge;
                    actliability = Page.ownDamageArray[i].ActLiability;
                    break;
                }
            }


            var seatingCapacity = $F("cmsSeatingCapacity");
            if (seatingCapacity == "-1") {
                seatingCapacity = 0;
            }
            var passengerPerseatingCapacity = $F("txtPassenger");
            var passengerAmount = parseFloat(seatingCapacity) * parseFloat(passengerPerseatingCapacity);
            var driverSeatAmount = $F("txtAmountofDriver");

            var quotedPrice = $F("txtQuotedPrice");
            var acidentoftheft = (parseFloat(quotedPrice) * parseFloat($F("txtaccidentOrTheft"))) / 100;
            var totalbeforeVat = parseFloat(driverSeatAmount) + parseFloat(passengerAmount) + parseFloat(acidentoftheft) + parseFloat(actliability) + parseFloat(ownDamage);

            var vat = ((parseFloat(totalbeforeVat) * parseFloat($F("txtVat"))) / 100);
            var stampcharge = $F("txtStampCharge");
            var commision = ((parseFloat(totalbeforeVat) * parseFloat($F("txtCommision"))) / 100);
            var grossAmount = parseFloat(totalbeforeVat) + parseFloat(vat) + parseFloat(stampcharge);
            var generalInsurenceAmount = parseFloat(grossAmount) - parseFloat(commision);
            var finalAmount = Math.ceil(generalInsurenceAmount);
            $("txtgeneralInsurenceForLcIns").setValue(finalAmount);
            //$("txtArtaForLCInsurenceAmount").setValue('0');
            var lfIns = 0;
            if (util.isFloat($F("txtArtaForLCInsurenceAmount")) && !util.isEmpty($F("txtArtaForLCInsurenceAmount"))) {
                lfIns = $F("txtArtaForLCInsurenceAmount");
            }
            $("txtTotalInsurenceForLcInsurence").setValue(finalAmount + parseFloat(lfIns));

        }
        else {
            $("txtgeneralInsurenceForLcIns").setValue('0');
            var lfIns = 0;
            if (util.isFloat($F("txtArtaForLCInsurenceAmount")) && !util.isEmpty($F("txtArtaForLCInsurenceAmount"))) {
                lfIns = $F("txtArtaForLCInsurenceAmount");
            }
            $("txtTotalInsurenceForLcInsurence").setValue(lfIns);
        }

        //End General Insurence


        //Loan Amount--------------------------

        var vehicleId = $F("cmbModel");
        var vehicleLtv = 0;
        var model = Page.modelArray.findByProp('VehicleId', vehicleId);
        if (model) {
            vehicleLtv = model.Ltv / 100;
        }


        var consideredPrice = (parseFloat($F("txtConsideredprice")) * vehicleLtv);
        var generalInsurence = $F("txtgeneralInsurenceForLcIns");
        var ltvAllowed = 0;
        if (util.isFloat(generalInsurence) && !util.isEmpty(generalInsurence)) {
            ltvAllowed = consideredPrice - generalInsurence;
        }
        else {
            ltvAllowed = consideredPrice;
        }
        $("txtLtvAllowedForLcLoanAmount").setValue(ltvAllowed);

        var maxEmi = 0;
        if (util.isFloat($F("txtMaximumEmi")) && !util.isEmpty($F("txtMaximumEmi"))) {
            maxEmi = parseFloat($F("txtMaximumEmi"));
        }
        var consideredInterest = 0;
        if (util.isFloat($F("txtInterestLCCosideredRate")) && !util.isEmpty($F("txtInterestLCCosideredRate"))) {
            consideredInterest = parseFloat($F("txtInterestLCCosideredRate"));
        }

        $("txtAskingLoanAmountForLcLoanAmount").setValue($F("txtQuotedPrice"));

        var tenor = 0;
        if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
            tenor = parseFloat($F("txtConsideredMarginForLc"));
        }


        var interestperlack = Math.round(analystDetailsHelper.PMT(parseFloat(parseFloat(consideredInterest) / 100) / 12, tenor, -100000));
        if (!util.isFloat(interestperlack)) {
            interestperlack = 0;
        }
        var incomeAllowed = (parseFloat(maxEmi) / interestperlack) * 100000;

        if (!util.isFloat(incomeAllowed)) {
            incomeAllowed = 0;
        }


        var existingFacilityAllowed = 0;
        var appincomeArray = [];

        var totalPrevEmi = analystDetailsHelper.calculatePrevHistoryEmi();

        var incomeass = Math.round(incomeAllowed);
        var isRepayment = false;
        var totalRep = 0;


        if (Page.prCount > 0) {

            for (var i = 0; i < Page.prCount; i++) {
                var identity = i + 1;
                if (Page.primaryProfessionId == 1 || Page.otherProfessionId == 1) {
                    var rep = $F("cmbIncomeAssessmentForPrimaryBank" + identity);
                    if (rep == "7") {
                        var totalEMIclosing = $F("txtTotalEMIClosingConsideredForPrimaryBank" + identity);
                        if (util.isFloat(totalEMIclosing)) {
                            totalRep += parseFloat(totalEMIclosing);
                        }
                        isRepayment = true;
                    }
                }
                if (Page.primaryProfessionId == 3 || Page.otherProfessionId == 3 || Page.primaryProfessionId == 4 || Page.otherProfessionId == 4 || Page.primaryProfessionId == 6 || Page.otherProfessionId == 6) {

                    var rep = $F("cmbIncomeAssessmentForSEPrBank" + identity);
                    if (rep == "7") {
                        var totalEMIclosing = $F("txtTotalEMIClosingConsideredForSEPrBank" + identity);
                        if (util.isFloat(totalEMIclosing)) {
                            totalRep += parseFloat(totalEMIclosing);
                        }
                        isRepayment = true;
                    }

                }
            }
        }

        if (Page.jtCount > 0) {
            for (var i = 0; i < Page.jtCount; i++) {
                var identity = i + 1;
                if (Page.jointPrimaryProfessionId == 1) {
                    var rep = $F("cmbIncomeAssementForJointBank" + identity);
                    if (rep == "7") {
                        var totalEMIclosing = $F("txtTotalEMIClosingConsideredPrevRepHisForJointBank" + identity);
                        if (util.isFloat(totalEMIclosing)) {
                            totalRep += parseFloat(totalEMIclosing);
                        }
                        isRepayment = true;
                    }
                }
                if (Page.jointPrimaryProfessionId == 3 || Page.jointPrimaryProfessionId == 4 || Page.jointPrimaryProfessionId == 6) {

                    var rep = $F("cmbIncomeAssessmentForSEJtBank" + identity);
                    if (rep == "7") {
                        var totalEMIclosing = $F("txtTotalEMIClosingConsideredForSEJtBank" + identity);
                        if (util.isFloat(totalEMIclosing)) {
                            totalRep += parseFloat(totalEMIclosing);
                        }
                        isRepayment = true;
                    }

                }
            }
        }
        if (isRepayment == true && totalRep != 0 && util.isFloat(totalRep)) {
            var emiPerlack = analystDetailsHelper.PMT((consideredInterest / 100) / 12, tenor, -100000);
            if (!util.isFloat(emiPerlack)) {
                emiPerlack = 0;
            }
            var repaymentBassedAmount = (totalRep / emiPerlack) * 100000;
            var innassAmount = Math.round(incomeAllowed) * 2;
            if (innassAmount >= repaymentBassedAmount) {
                incomeAllowed = repaymentBassedAmount;
            }
            else {
                incomeAllowed = incomeAllowed;
            }

        }

        $("txtIncoomeAllowedForLcLoanAmount").setValue(Math.round(incomeAllowed)); //Need to be calculate
        appincomeArray.push(Math.round(incomeAllowed));


        $("txtbdCadAllowedForLcLoanAmount").setValue(bdcatAllowed);
        appincomeArray.push(bdcatAllowed);

        var outStandingAmount = analystDetailsHelper.calculateOutStandingAmountFortermloan();
        existingFacilityAllowed = parseFloat(bdcatAllowed) - parseFloat(outStandingAmount);

        $("txtExistingFacilitiesAllowedForLcLoanAmount").setValue(existingFacilityAllowed);
        appincomeArray.push(existingFacilityAllowed);
        appincomeArray.push(ltvAllowed);
        appincomeArray.push(parseFloat($F("txtQuotedPrice")));
        if (totalPrevEmi == 0) {
            for (var ii = 0; ii < appincomeArray.length; ii++) {
                if (!util.isFloat(appincomeArray[ii])) {
                    appincomeArray[ii] = 0;
                }
            }
            var apploanamountArray = appincomeArray.sort(function(a, b) { return a - b });
            $("txtAppropriteloanAmountForLcLoanAmount").setValue(apploanamountArray[0]);
        }
        else {
            if (ltvAllowed > existingFacilityAllowed) {
                $("txtAppropriteloanAmountForLcLoanAmount").setValue(existingFacilityAllowed);
            }
            else {
                $("txtAppropriteloanAmountForLcLoanAmount").setValue(ltvAllowed);
            }
        }
        analystDetailsHelper.calculateTotalLoanAmount();
    },

    //Set Age wise Label
    setagewiseLebel: function() {
        $('cmbLabelForDlaLimit').setValue('1');
        var currentAgeForPrimary = parseInt($F("txtCurrenAge"));
        var currentAgeForJoin = parseInt($F("txtJtCurrntAge"));
        var isJoint = $F('txtIsJoint');
        if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && currentAgeForJoin < 65 && isJoint == 1) {
            $('cmbLabelForDlaLimit').setValue('2');

        }
        if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && isJoint == 0) {
            $('cmbLabelForDlaLimit').setValue('2');

        }
        if (currentAgeForJoin > 65 && currentAgeForJoin < 69 && currentAgeForPrimary < 65 && isJoint == 1) {
            $('cmbLabelForDlaLimit').setValue('2');

        }
        if ((currentAgeForPrimary > 68 || currentAgeForJoin > 68) && isJoint == 1) {
            $('cmbLabelForDlaLimit').setValue('3');
        }
        if (currentAgeForPrimary > 68 && isJoint == 0) {
            $('cmbLabelForDlaLimit').setValue('3');
        }

        var lbl = parseInt($F("cmbLabelForDlaLimit"));
        var dbrLabel = parseInt($F("cmbDBRLevel"));
        if (dbrLabel > lbl) {
            $('cmbLabelForDlaLimit').setValue(dbrLabel);
        }
        var lavelFortenor = parseInt("cmbLabelForTonerLc");
        if (lavelFortenor > lbl) {
            $('cmbLabelForDlaLimit').setValue(lavelFortenor);
        }
    },

    //PMT
    PMT: function(i, n, p) {
        return i * p * Math.pow((1 + i), n) / (1 - Math.pow((1 + i), n));
    },

    //Calculate Prev History EMI
    calculatePrevHistoryEmi: function() {
        var totalAmount = 0;
        var primaryProfession = Page.primaryProfessionId;
        var otherProfession = Page.otherProfessionId;
        for (var i = 1; i < Page.prCount; i++) {

            if (i < 7) {
                if (primaryProfession == 3 || otherProfession == 3 || primaryProfession == 4 || otherProfession == 4 || primaryProfession == 6 || otherProfession == 6) {
                    if (util.isFloat($F("txtTotalEMIClosingConsideredForSEPrBank" + i)) && !util.isEmpty($F("txtTotalEMIClosingConsideredForSEPrBank" + i))) {
                        totalAmount += parseFloat($F("txtTotalEMIClosingConsideredForSEPrBank" + i));
                    }
                }
                if (primaryProfession == 1 || otherProfession == 1) {
                    if (util.isFloat($F("txttotalForOffSCBForPrimaryBank" + i)) && !util.isEmpty($F("txttotalForOffSCBForPrimaryBank" + i))) {
                        totalAmount += parseFloat($F("txttotalForOffSCBForPrimaryBank" + i));
                    }
                }
            }
        }
        return totalAmount;
    },


    //Calculate Out Standing Amount For Term Loan
    calculateOutStandingAmountFortermloan: function() {
        var outStandingAmount = 0;

        for (var i = 1; i < 5; i++) {
            if (util.isFloat($F("txtoutStanding" + i)) && !util.isEmpty($F("txtoutStanding" + i))) {
                var type = $F("cmbTypeForTermLoans" + i);
                if (type == "10" || type == "11") {
                    outStandingAmount += parseFloat($F("txtoutStanding" + i));
                }
            }
        }
        return outStandingAmount;
    },
    //Calculate General Insurance Amount
    calculateGeneralInsuranceAmount: function() {

        var genAllowed = 0;

        var generalInsPrefor = $F("cmbGeneralInsurencePreference");
        var genPreference = Page.artaPreference.findByProp("InsCompanyId", generalInsPrefor);
        if (genPreference) {
            genAllowed = genPreference.InsurancePercent;
        }
        var ownDamage = 0;
        var actliability = 0;
        var ingineCc = $F("txtEngineCapacityCC");
        for (var i = 0; i < Page.ownDamageArray.length; i++) {
            var ccfrom = Page.ownDamageArray[i].CCFrom;
            var ccTo = Page.ownDamageArray[i].CCTo;
            if (parseInt(ingineCc) > parseInt(ccfrom) && parseInt(ingineCc) < parseInt(ccTo)) {
                ownDamage = Page.ownDamageArray[i].OwnDamageCharge;
                actliability = Page.ownDamageArray[i].ActLiability;
                break;
            }
        }


        var seatingCapacity = $F("cmsSeatingCapacity");
        if (seatingCapacity == "-1") {
            seatingCapacity = 0;
        }
        var passengerPerseatingCapacity = $F("txtPassenger");
        var passengerAmount = parseFloat(seatingCapacity) * parseFloat(passengerPerseatingCapacity);
        var driverSeatAmount = $F("txtAmountofDriver");

        var quotedPrice = $F("txtQuotedPrice");
        var acidentoftheft = (parseFloat(quotedPrice) * parseFloat($F("txtaccidentOrTheft"))) / 100;
        var totalbeforeVat = parseFloat(driverSeatAmount) + parseFloat(passengerAmount) + parseFloat(acidentoftheft) + parseFloat(actliability) + parseFloat(ownDamage);

        var vat = ((parseFloat(totalbeforeVat) * parseFloat($F("txtVat"))) / 100);
        var stampcharge = $F("txtStampCharge");
        var commision = ((parseFloat(totalbeforeVat) * parseFloat($F("txtCommision"))) / 100);
        var grossAmount = parseFloat(totalbeforeVat) + parseFloat(vat) + parseFloat(stampcharge);
        var generalInsurenceAmount = parseFloat(grossAmount) - parseFloat(commision);
        var finalAmount = Math.round(generalInsurenceAmount);
        //$("txtgeneralInsurenceForLcIns").setValue(finalAmount);
        return finalAmount;

    },

    //Calculate Total Loan Amount
    calculateTotalLoanAmount: function() {
        var approvedAmount = 0;
        var totalInsurenceAmount = 0;
        var artaAllowed = 0;
        var genAllowed = 0;
        var generalinsurence = 0;
        
        var artaPreferenceId = $F("cmbARTAReference");
        var artaPreference = Page.artaPreference.findByProp("InsCompanyId", artaPreferenceId);
        if (artaPreference) {
            artaAllowed = artaPreference.InsurancePercent;
        }


        if (util.isFloat($F("txtApprovedLoanAmountForLcLoanAmount")) && !util.isEmpty($F("txtApprovedLoanAmountForLcLoanAmount"))) {
            approvedAmount = parseFloat($F("txtApprovedLoanAmountForLcLoanAmount"));
        }
        if (util.isFloat($F("txtTotalInsurenceForLcInsurence")) && !util.isEmpty($F("txtTotalInsurenceForLcInsurence"))) {
            totalInsurenceAmount = parseFloat($F("txtTotalInsurenceForLcInsurence"));
        }

        var arta = $F("cmbARTA");
        var gen = $F("cmbGeneralInsurence");
        var totalLoanAmount = 0;
        if (arta == "1" || gen == "1") {
            totalLoanAmount = parseFloat(approvedAmount) + parseFloat(totalInsurenceAmount);
        }
        else {
            totalLoanAmount = parseFloat(approvedAmount);
        }
        $("txtloanAmountForLcLoanSummary").setValue(totalLoanAmount);

        var productId = $F("cmbProductName");
        var emr = 0;
        var interestrate = 0;
        var tenor = 0;

        if (util.isFloat($F("txtInterestLCCosideredRate")) && !util.isEmpty($F("txtInterestLCCosideredRate"))) {
            interestrate = (parseFloat(parseFloat($F("txtInterestLCCosideredRate")) / 100) / 12);
        }
        if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
            tenor = parseFloat($F("txtConsideredMarginForLc"));
        }
        //        if (util.isFloat($F("txtgeneralInsurenceForLcIns")) && !util.isEmpty($F("txtgeneralInsurenceForLcIns"))) {
        //            generalinsurence = parseFloat($F("txtgeneralInsurenceForLcIns"));
        //        }




        var emiPerlack = analystDetailsHelper.PMT(interestrate, tenor, -100000);
        if (!util.isFloat(emiPerlack)) {
            emiPerlack = 0;
        }
        var emionPo = Math.ceil(parseFloat(approvedAmount) * parseFloat(emiPerlack) / 100000); //Math.round(parseFloat(approvedAmount) * parseFloat(emiPerlack) / 100000);
        var emionPoForSadiq = parseFloat(approvedAmount) * parseFloat(emiPerlack) / 100000;


        generalinsurence = analystDetailsHelper.calculateGeneralInsuranceAmount();

        var generalInsurencePerlack = 0;
        if (gen == "1") {
            generalInsurencePerlack = parseFloat(parseFloat(emiPerlack) * parseFloat(generalinsurence)) / 100000;
        }
        var lifeInsurence = approvedAmount * parseFloat(parseFloat(artaAllowed) / 100) * parseFloat(tenor / 12);
        var lifeinsurencePerlack = (parseFloat(lifeInsurence) * parseFloat(emiPerlack)) / 100000;


        if (productId == "3") {
            emr = parseFloat(emionPoForSadiq) + parseFloat(generalInsurencePerlack) + parseFloat(lifeinsurencePerlack);
            $("txtEmrForlcloanSummary").setValue(emr.toFixed(2));
        }
        else {
            emr = parseFloat(emionPo) + parseFloat(generalInsurencePerlack) + parseFloat(lifeinsurencePerlack);
            $("txtEmrForlcloanSummary").setValue(Math.round(emr));
        }

        //
        var ltvExcludingInsurence = 0;
        var ltbIncludingInsurence = 0;
        var consideredPrice = 0;
        if (util.isFloat($F("txtConsideredprice")) && !util.isEmpty($F("txtConsideredprice"))) {
            consideredPrice = parseFloat($F("txtConsideredprice"));
        }
        if (consideredPrice != 0) {
            ltvExcludingInsurence = parseFloat(parseFloat(approvedAmount / consideredPrice) * 100);
            $("txtLtvExclusdingInsurence").setValue(ltvExcludingInsurence.toFixed(2));

            ltbIncludingInsurence = parseFloat(parseFloat(totalLoanAmount / consideredPrice) * 100);
            $("txtltvIncludingLtvForLcLoanSummary").setValue(ltbIncludingInsurence.toFixed(2));
        }
        var extraltv = ltbIncludingInsurence - ltvExcludingInsurence;
        $("txtExtraLtvForLcloanSumary").setValue(extraltv.toFixed(2));
        var dbrExcludingInsurence = 0;
        var existingEmi = 0;
        var totalJointIncome = 0;
        if (util.isFloat($F("txtExistingEmi")) && !util.isEmpty($F("txtExistingEmi"))) {
            existingEmi = parseFloat($F("txtExistingEmi"));
        }
        if (util.isFloat($F("txtTotalJointInComeForLcWithCal")) && !util.isEmpty($F("txtTotalJointInComeForLcWithCal"))) {
            totalJointIncome = parseFloat($F("txtTotalJointInComeForLcWithCal"));
        }

        dbrExcludingInsurence = parseFloat(parseFloat((emionPo + existingEmi) / totalJointIncome) * 100);

        if (!util.isFloat(dbrExcludingInsurence)) {
            dbrExcludingInsurence = 0;
        }

        $("txtDBRExcludingInsurence").setValue(dbrExcludingInsurence.toFixed(2));

        var dbrIncludingInsurence = 0;
        dbrIncludingInsurence = parseFloat(parseFloat((emr + existingEmi) / totalJointIncome) * 100);
        if (!util.isFloat(dbrIncludingInsurence)) {
            dbrIncludingInsurence = 0;
        }
        $("txtDBRIncludingInsurence").setValue(dbrIncludingInsurence.toFixed(2));

        var extraDbr = dbrIncludingInsurence - dbrExcludingInsurence;
        $("txtExtraDBRForLcloanSummary").setValue(extraDbr.toFixed(2));

        if (ltbIncludingInsurence > 30) {
            document.getElementById('txtltvIncludingLtvForLcLoanSummary').style.background = 'red';
            document.getElementById('txtltvIncludingLtvForLcLoanSummary').style.color = 'white';
        }
        else {
            document.getElementById('txtltvIncludingLtvForLcLoanSummary').style.background = '';
            document.getElementById('txtltvIncludingLtvForLcLoanSummary').style.color = '';
        }




        AnalystDetailsCalculationHelper.calculateRepaymentForFinalize();

    },

    // Calculate Average Balance For Bussiness Man
    calculateAverageBalance: function(bankNo, accountType) {
        var firstst9MonthTotal = 0;
        var first6MonthTotal = 0;
        var last6Month = 0;
        var last3month = 0;
        var j = 0;
        var monthArray = [];
        if (accountType == "Pr") {
            var tablePrAccount = $("tblPrBankStatementForIncomeGenerator" + bankNo);
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                if (util.isFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txt1st9monthForPrimaryBank" + bankNo).setValue(Math.round(firstst9MonthTotal / 9));
            $("txt1st6MonthForPrimaryBank" + bankNo).setValue(Math.round(first6MonthTotal / 6));
            $("txtLast6monthForPrimaryBank" + bankNo).setValue(Math.round(last6Month / 6));
            $("txtLast3monthForPrimaryBank" + bankNo).setValue(Math.round(last3month / 3));

            //Consider Margin

            var considerMargin1 = 0;
            var considerMargin2 = 0;
            var last3monthAvg = Math.round(last3month / 3);
            var first9monthAvg = Math.round(firstst9MonthTotal / 9);
            var first6monthAvg = Math.round(first6MonthTotal / 6);
            var last6monthAvg = Math.round(last6Month / 6);

            if (last3monthAvg >= first9monthAvg) {
                considerMargin1 = "25";
            }
            else {
                considerMargin1 = "15";
            }

            if (last6monthAvg >= first6monthAvg) {
                considerMargin2 = "33";
            }
            else {
                considerMargin2 = "20";
            }
            //            if (last3month >= firstst9MonthTotal) {
            //                considerMargin1 = "25";
            //            }
            //            else {
            //                considerMargin1 = "15";
            //            }

            //            if (last6Month >= first6MonthTotal) {
            //                considerMargin2 = "33";
            //            }
            //            else {
            //                considerMargin2 = "20";
            //            }

            var considerMarginFinal = 0;
            if (considerMargin1 <= considerMargin2) {
                considerMarginFinal = considerMargin1;
            }
            else {
                considerMarginFinal = considerMargin2;
            }


            $("txtConsiderednarginForPrimaryBank" + bankNo).setValue(considerMarginFinal);
            $("txtConsiderMarginForPrimaryBank" + bankNo).setValue(considerMarginFinal);

            //var grossIncome = parseFloat((parseFloat($F("cmbConsideredCtoForPrBank" + bankNo)) * considerMarginFinal) / 100);
            analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Pr", "BM");
            //            var grossIncome = parseFloat((parseFloat($F("txtAverageBalanceFor12MonthBank" + bankNo)) * considerMarginFinal) / 100);
            //            $("txtgrossincomeForPrimaryBank" + bankNo).setValue(Math.round(grossIncome));
            //            $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(Math.round(grossIncome));

            //            var share = $F("txtShareForPrimaryBank" + bankNo);

            //            var netIncomeTotal = parseFloat((Math.round(grossIncome) * parseFloat(share)) / 100);

            //            $("txtNetincomeForPrimaryBank" + bankNo).setValue(Math.round(netIncomeTotal));

        }
        else if (accountType == "Jt") {
            var tableJtAccount = $("tblJtBankStatementForIncomeGenerator" + bankNo);
            var rowCountJt = tableJtAccount.rows.length;
            for (var i = 1; i <= rowCountJt - 1; i++) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForJointBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txtAVGBalanceFor1st9MontForJointBank" + bankNo).setValue(Math.round(firstst9MonthTotal / 9));
            $("txtAVGBalanceFor1st6MontForJointBank" + bankNo).setValue(Math.round(first6MonthTotal / 6));
            $("txtAVGBalanceForLast6MontForJointBank" + bankNo).setValue(Math.round(last6Month / 6));
            $("txtAVGBalanceForLast3MontForJointBank" + bankNo).setValue(Math.round(last3month / 3));

            //Consider Margin
            var considerMarginForJoint1 = 0;
            var considerMarginForJoint2 = 0;

            var last3monthAvg = Math.round(last3month / 3);
            var first9monthAvg = Math.round(firstst9MonthTotal / 9);
            var first6monthAvg = Math.round(first6MonthTotal / 6);
            var last6monthAvg = Math.round(last6Month / 6);

            if (last3monthAvg >= first9monthAvg) {
                considerMarginForJoint1 = "25";
            }
            else {
                considerMarginForJoint1 = "15";
            }

            if (last6monthAvg >= first6monthAvg) {
                considerMarginForJoint2 = "33";
            }
            else {
                considerMarginForJoint2 = "20";
            }

            var considerMarginFinalForJoint = 0;
            if (considerMarginForJoint1 <= considerMarginForJoint2) {
                considerMarginFinalForJoint = considerMarginForJoint1;
            }
            else {
                considerMarginFinalForJoint = considerMarginForJoint2;
            }
            $("txtConsideredMarginForJointBank" + bankNo).setValue(considerMarginFinalForJoint);
            $("txtConsideredMarginBMForJointBank" + bankNo).setValue(considerMarginFinalForJoint);

            //var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);

            analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Jt", "BM");
            //            var grossIncome = parseFloat((parseFloat($F("txtAverageBalance12MonthForJointBank" + bankNo)) * considerMarginFinal) / 100);
            //            $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncomeForJoint);
            //            $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncomeForJoint);

            //            var shareJoint = $F("txtShareForJointBank" + bankNo);

            //            var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);

            //            $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));

        }
    },

    //Calculate Average Balance For Self Employed
    calculateAverageBalanceForSelfEmployed: function(bankNo, accountType) {
        var firstst9MonthTotal = 0;
        var first6MonthTotal = 0;
        var last6Month = 0;
        var last3month = 0;
        var j = 0;
        var monthArray = [];
        if (accountType == "Pr") {
            var tablePrAccount = $("tblPrBankStatementForSEBank" + bankNo);
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txt1st9monthForSEPrBank" + bankNo).setValue(Math.round(firstst9MonthTotal / 9));
            $("txt1st6MonthForSEPrBank" + bankNo).setValue(Math.round(first6MonthTotal / 6));
            $("txtLast6monthForSEPrBank" + bankNo).setValue(Math.round(last6Month / 6));
            $("txtLast3monthForSEPrBank" + bankNo).setValue(Math.round(last3month / 3));



            //Consider Margin
            var considerMargin1 = 0;
            var considerMargin2 = 0;



            //            if (last3month >= firstst9MonthTotal) {
            //                considerMargin1 = "25";
            //            }
            //            else {
            //                considerMargin1 = "15";
            //            }

            //            if (last6Month >= first6MonthTotal) {
            //                considerMargin2 = "33";
            //            }
            //            else {
            //                considerMargin2 = "20";
            //            }

            var last3monthAvg = Math.round(last3month / 3);
            var first9monthAvg = Math.round(firstst9MonthTotal / 9);
            var first6monthAvg = Math.round(first6MonthTotal / 6);
            var last6monthAvg = Math.round(last6Month / 6);

            if (last3monthAvg >= first9monthAvg) {
                considerMargin1 = "25";
            }
            else {
                considerMargin1 = "15";
            }

            if (last6monthAvg >= first6monthAvg) {
                considerMargin2 = "33";
            }
            else {
                considerMargin2 = "20";
            }

            var considerMarginFinal = 0;
            if (considerMargin1 <= considerMargin2) {
                considerMarginFinal = considerMargin1;
            }
            else {
                considerMarginFinal = considerMargin2;
            }


            $("txtConsiderednarginForSEPrBank" + bankNo).setValue(considerMarginFinal);
            $("txtConsiderMarginForSEPrBank" + bankNo).setValue(considerMarginFinal);

            //var grossIncome = parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinal) / 100);
            analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Pr", "SE");
            //            var grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalanceForSEPr12MonthBank" + bankNo)) * considerMarginFinal) / 100));
            //            $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncome);
            //            $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncome);
            //            var share = $F("txtShareForSEPrBank" + bankNo);
            //            var netIncomeTotal = Math.round(parseFloat((grossIncome * parseFloat(share)) / 100));
            //            $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotal);

        }
        else if (accountType == "Jt") {
            var tableJtAccount = $("tblJtBankStatementForSEBank" + bankNo);
            var rowCountJt = tableJtAccount.rows.length;
            for (var i = 1; i <= rowCountJt - 1; i++) {
                if (util.isFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo))) {
                    j += 1;
                    if (j < 7) {
                        first6MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo));
                    }
                    if (j < 10) {
                        firstst9MonthTotal += parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo));
                    }
                    monthArray.push(parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo)));
                }
            }
            j = 0;
            for (var k = monthArray.length - 1; k >= 0; k--) {
                j += 1;
                if (j < 4) {
                    last3month += monthArray[k];
                }
                if (j < 7) {
                    last6Month += monthArray[k];
                }
            }
            $("txt1st9monthForSEJtBank" + bankNo).setValue(Math.round(firstst9MonthTotal / 9));
            $("txt1st6MonthForSEJtBank" + bankNo).setValue(Math.round(first6MonthTotal / 6));
            $("txtLast6monthForSEJtBank" + bankNo).setValue(Math.round(last6Month / 6));
            $("txtLast3monthForSEJtBank" + bankNo).setValue(Math.round(last3month / 3));

            //Consider Margin
            var considerMarginForJoint1 = 0;
            var considerMarginForJoint2 = 0;


            //            if (last3month >= firstst9MonthTotal) {
            //                considerMarginForJoint1 = "25";
            //            }
            //            else {
            //                considerMarginForJoint1 = "15";
            //            }

            //            if (last6Month >= first6MonthTotal) {
            //                considerMarginForJoint2 = "33";
            //            }
            //            else {
            //                considerMarginForJoint2 = "20";
            //            }
            var last3monthAvg = Math.round(last3month / 3);
            var first9monthAvg = Math.round(firstst9MonthTotal / 9);
            var first6monthAvg = Math.round(first6MonthTotal / 6);
            var last6monthAvg = Math.round(last6Month / 6);

            if (last3monthAvg >= first9monthAvg) {
                considerMarginForJoint1 = "25";
            }
            else {
                considerMarginForJoint1 = "15";
            }

            if (last6monthAvg >= first6monthAvg) {
                considerMarginForJoint2 = "33";
            }
            else {
                considerMarginForJoint2 = "20";
            }

            var considerMarginFinalForJoint = 0;
            if (considerMarginForJoint1 <= considerMarginForJoint2) {
                considerMarginFinalForJoint = considerMarginForJoint1;
            }
            else {
                considerMarginFinalForJoint = considerMarginForJoint2;
            }
            $("txtConsiderednarginForSEJtBank" + bankNo).setValue(considerMarginFinalForJoint);
            $("txtConsiderMarginForSEJtBank" + bankNo).setValue(considerMarginFinalForJoint);

            //var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);
            analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Jt", "SE");
            //            var grossIncomeForJoint = Math.round(parseFloat((parseFloat($F("txtAverageBalanceForSEJt12MonthBank" + bankNo)) * considerMarginFinalForJoint) / 100));
            //            $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncomeForJoint);
            //            $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncomeForJoint);
            //            var shareJoint = $F("txtShareForSEJtBank" + bankNo);
            //            var netIncomeTotalJoint = Math.round(parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100));
            //            $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalJoint);

        }
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'SE');
    },

    //Change Private InCome
    changePrivateInCome: function(bankNo, accountType, tabName) {
        if (accountType == "Pr" && tabName == "Se") {
            var industry = $F("cmbIndustryForSEPrBank" + bankNo);
            var subsector = $F("cmbSubsectorForSEPrBank" + bankNo);
            var privateIncome = $F("cmbPrivateIncomeForSEPrBank" + bankNo);
            if (industry == "12" && subsector == "143" && privateIncome == "1") {
                $("divTutionForSEPrBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'block';
                $("txtTotalInComeTutionForSEPrBank" + bankNo).setValue('0');
            }
            else if (industry == "12" && subsector == "150" && privateIncome == "1") {
                $("divTutionForSEPrBank" + bankNo).style.display = 'block';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEPrBank" + bankNo).setValue('0');
            }
            else {
                $("divTutionForSEPrBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSePrBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEPrBank" + bankNo).setValue('0');
                $("txtTotalInComeTutionForSEPrBank" + bankNo).setValue('0');
            }
        }
        else if (accountType == "Jt" && tabName == "Se") {
            var industryForSeJt = $F("cmbIndustryForSEJtBank" + bankNo);
            var subsectorSeJt = $F("cmbSubsectorForSEJtBank" + bankNo);
            var privateIncomeSeJt = $F("cmbPrivateIncomeForSEJtBank" + bankNo);
            if (industryForSeJt == "12" && subsectorSeJt == "143" && privateIncomeSeJt == "1") {
                $("divTutionJtBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'block';
                $("txtTotalInComeTutionForSEJtBank" + bankNo).setValue('0');
            }
            else if (industryForSeJt == "12" && subsectorSeJt == "150" && privateIncomeSeJt == "1") {
                $("divTutionJtBank" + bankNo).style.display = 'block';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'none';
                $("txtTotalIncomeForSEJtBank" + bankNo).setValue('0');
            }
            else {
                $("divTutionJtBank" + bankNo).style.display = 'none';
                $("divChamberIncomeForSeJtBank" + bankNo).style.display = 'none';
                $("txtTotalInComeTutionForSEJtBank" + bankNo).setValue('0');
                $("txtTotalIncomeForSEJtBank" + bankNo).setValue('0');
            }
        }
        analystDetailsHelper.checkDoctorChambers(accountType, 'DC');
        analystDetailsHelper.checkDoctorChambers(accountType, 'TC');
    },

    //check Doctor Chambers
    checkDoctorChambers: function(accountType, tabName) {
        var isDoctor = false;
        var isTeacher = false;
        if (accountType == "Pr" && tabName == "DC") {
            for (var i = 1; i < Page.prCount; i++) {
                if ($F("cmbIndustryForSEPrBank" + i) == "12" && $F("cmbSubsectorForSEPrBank" + i) == "143" && $F("cmbPrivateIncomeForSEPrBank" + i) == "1") {
                    isDoctor = true;
                    break;
                }
            }
            if (isDoctor) {
                analystDetailsHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trDoctorForLoanCalculatorPr').style.display = "none";
            }

        }
        else if (accountType == "Jt" && tabName == "DC") {
            for (var i = 1; i < Page.jtCount; i++) {
                if ($F("cmbIndustryForSEJtBank" + i) == "12" && $F("cmbSubsectorForSEJtBank" + i) == "143" && $F("cmbPrivateIncomeForSEJtBank" + i) == "1") {
                    isDoctor = true;
                    break;
                }
            }
            if (isDoctor) {
                analystDetailsHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trDoctorForLoanCalculatorJt').style.display = "none";
            }

        }
        else if (accountType == "Pr" && tabName == "TC") {
            for (var i = 1; i < Page.prCount; i++) {
                if ($F("cmbIndustryForSEPrBank" + i) == "12" && $F("cmbSubsectorForSEPrBank" + i) == "150" && $F("cmbPrivateIncomeForSEPrBank" + i) == "1") {
                    isTeacher = true;
                    break;
                }
            }
            if (isTeacher) {
                analystDetailsHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trTeacherForLoanCalculatorPr').style.display = "none";
            }

        }
        else if (accountType == "Jt" && tabName == "TC") {
            for (var i = 1; i < Page.jtCount; i++) {
                if ($F("cmbIndustryForSEJtBank" + i) == "12" && $F("cmbSubsectorForSEJtBank" + i) == "150" && $F("cmbPrivateIncomeForSEJtBank" + i) == "1") {
                    isTeacher = true;
                    break;
                }
            }
            if (isTeacher) {
                analystDetailsHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trTeacherForLoanCalculatorJt').style.display = "none";
            }

        }
    },

    //setBussinessManCalculationForDoctorAndTeachers
    setBussinessManCalculationForDoctorAndTeachers: function(accountType, tabName) {
        var income = 0;
        var consider = 0;
        var identity = 0;

        if (accountType == "Pr" && tabName == "DC") {
            for (var i = 0; i < Page.prCount; i++) {
                identity = i + 1;
                if (util.isFloat($F('txtTotalIncomeForSEPrBank' + identity)) && !util.isEmpty($F('txtTotalIncomeForSEPrBank' + identity))) {
                    income += parseFloat($F('txtTotalIncomeForSEPrBank' + identity));
                }
            }
            $('txtIncomeBForLCPrDoctor').setValue(income);
            $('txtInComeAssMethLCPrDoctor').setValue("Doctor");
            $('txtAssCodeForLCPrDoctor').setValue('FD');
            $('txtAssMethodIdForLcPrDoc').setValue('4');
            $('txtAssCatIdForLcPrDoc').setValue('12');
            $('txtEmployerCategoryForLCPrDoctor').setValue('Professional - Doctors');
            $('txtEmployerCatCodeForLCPrDoctor').setValue('YA1');
            consider = $F("cmbConsiderForLCPrDoctor");
            if (consider != "1") {
                $('cmbConsiderForLCPrDoctor').setValue("0");
            }
            $('trDoctorForLoanCalculatorPr').style.display = "";
        }
        else if (accountType == "Jt" && tabName == "DC") {
            for (var i = 0; i < Page.jtCount; i++) {
                identity = i + 1;
                if (util.isFloat($F('txtTotalIncomeForSEJtBank' + identity)) && !util.isEmpty($F('txtTotalIncomeForSEJtBank' + identity))) {
                    income += parseFloat($F('txtTotalIncomeForSEJtBank' + identity));
                }
            }
            $('txtIncomeBForLCJtDoctor').setValue(income);
            $('txtInComeAssMethLCJtDoctor').setValue("Doctor");
            $('txtAssCodeForLCJtDoctor').setValue('FD');
            $('txtEmployerCategoryForLCJtDoctor').setValue('Professional - Doctors');
            $('txtEmployerCatCodeForLCJtDoctor').setValue('YA1');
            $('txtAssMethodIdForLcJtDoc').setValue('4');
            $('txtAssCatIdForLcJtDoc').setValue('12');
            consider = $F("cmbConsiderForLCJtDoctor");
            if (consider != "1") {
                $('cmbConsiderForLCJtDoctor').setValue("0");
            }
            $('trDoctorForLoanCalculatorJt').style.display = "";
        }
        else if (accountType == "Pr" && tabName == "TC") {
            for (var i = 0; i < Page.prCount; i++) {
                identity = i + 1;
                if (util.isFloat($F('txtTotalInComeTutionForSEPrBank' + identity)) && !util.isEmpty($F('txtTotalInComeTutionForSEPrBank' + identity))) {
                    income += parseFloat($F('txtTotalInComeTutionForSEPrBank' + identity));
                }
            }
            $('txtIncomeBForLCPrTeacher').setValue(income);
            $('txtInComeAssMethLCPrTeacher').setValue("Teachers");
            $('txtAssCodeForLCPrTeacher').setValue('TS');
            $('txtEmployerCategoryForLCPrTeacher').setValue('Professional - Teachers');
            $('txtEmployerCatCodeForLCPrTeacher').setValue('YT1');
            $('txtAssMethodIdForLcPrTec').setValue('150');
            $('txtAssCatIdForLcPrTec').setValue('12');
            consider = $F("cmbConsiderForLCPrTeacher");
            if (consider != "1") {
                $('cmbConsiderForLCPrTeacher').setValue("0");
            }
            $('trTeacherForLoanCalculatorPr').style.display = "";
        }
        else if (accountType == "Jt" && tabName == "TC") {
            for (var i = 0; i < Page.jtCount; i++) {
                identity = i + 1;
                if (util.isFloat($F('txtTotalInComeTutionForSEJtBank' + identity)) && !util.isEmpty($F('txtTotalInComeTutionForSEJtBank' + identity))) {
                    income += parseFloat($F('txtTotalInComeTutionForSEJtBank' + identity));
                }
            }
            $('txtIncomeBForLCJtTeacher').setValue(income);
            $('txtInComeAssMethLCJtTeacher').setValue("Teachers");
            $('txtAssCodeForLCJtTeacher').setValue('TS');
            $('txtAssMethodIdForLcJtTec').setValue('150');
            $('txtAssCatIdForLcJtTec').setValue('12');
            $('txtEmployerCategoryForLCJtTeacher').setValue('Professional - Teachers');
            $('txtEmployerCatCodeForLCJtTeacher').setValue('YT1');
            consider = $F("cmbConsiderForLCJtTeacher");
            if (consider != "1") {
                $('cmbConsiderForLCJtTeacher').setValue("0");
            }
            $('trTeacherForLoanCalculatorJt').style.display = "";
        }
        AnalystDetailsCalculationHelper.CalculateLoanCalculator(accountType);
    },

    //Change SubSector For Self Employed
    changeSubSectorForSelfEmployed: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var subsectorPrimaryId = $F("cmbSubsectorForSEPrBank" + bankNo);
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', subsectorPrimaryId);
            if (subsectorPrimary) {
                $('txtProfitMarginForSEPrBank' + bankNo).setValue(subsectorPrimary.SUSE_INTERESTRATE);
                $('txtProfitMarginCodeForSEPrBank' + bankNo).setValue(subsectorPrimary.SUSE_IRCODE);
            }
        }
        else if (accountType == "Jt") {
            var subsectorId = $F("cmbSubsectorForSEJtBank" + bankNo);
            var subsector = Page.subsectorArray.findByProp('SUSE_ID', subsectorId);
            if (subsectorPrimary) {
                $('txtProfitMarginForSEJtBank' + bankNo).setValue(subsector.SUSE_INTERESTRATE);
                $('txtProfitMarginCodeForSEJtBank' + bankNo).setValue(subsector.SUSE_IRCODE);
            }
        }
        analystDetailsHelper.changePrivateInCome(bankNo, accountType, "Se");
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'SE');
    },

    //Change Sub Sector
    changeSubSector: function(bankNo, accountType) {

        if (accountType == "Pr") {
            var subsectorPrimaryId = $F("cmbSubsectorForPrimaryBank" + bankNo);
            var subsectorPrimary = Page.subsectorArray.findByProp('SUSE_ID', subsectorPrimaryId);
            if (subsectorPrimary) {
                $('txtProfitMarginForPrimaryBank' + bankNo).setValue(subsectorPrimary.SUSE_INTERESTRATE);
                $('txtProfitMarginCodeForPrimaryBank' + bankNo).setValue(subsectorPrimary.SUSE_IRCODE);
            }
        }
        else if (accountType == "Jt") {
            var subsectorId = $F("cmbSubSectorForJointBank" + bankNo);
            var subsector = Page.subsectorArray.findByProp('SUSE_ID', subsectorId);
            if (subsector) {
                $('txtProfitMarginForJointBank' + bankNo).setValue(subsector.SUSE_INTERESTRATE);
                $('txtSubSectorCode1ForJointBank' + bankNo).setValue(subsector.SUSE_IRCODE);
            }
        }
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'BM');
    },

    //Change Credit turn Over For Bussiness Man Primary
    changeCreditTurnoverForPrApplicant: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField)) && !util.isNegativeNumber($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var sixMonthArrayForCto = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        var sixmonthVariable = 0;

        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                totalCtofor12Month += parseFloat($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo));
            }
            if (util.isFloat($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo)) - parseFloat($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo));
            }
            else if (util.isNegativeNumber($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtcreditTurnOver' + i + 'ForIncomeGeneratorBank' + bankNo)) - parseFloat($F('txtAdjustment' + i + 'ForIncomeGeneratorBank' + bankNo));
            }
            sixMonthArrayForCto.push(sixmonthVariable);
        }
        $("txtTotalCTOFor12MonthBank" + bankNo).setValue(Math.round(totalCtofor12Month));
        //Code Comment by Washik 26/12/2012
        //        sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
        //        for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
        //            if (sixMonthArrayForCto[s] != "") {
        //                totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
        //            }
        //        }
        //New Code Entered By Washik 26/12/2012
        for (var s = 0; s <= 5; s++) {
            if ((sixMonthArrayForCto.length - 1) - s < 0) {
            }
            else {
                if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                    totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                }
            }

        }
        //New Code Ended by Washik
        $("txtTotalCTOFor6MonthBank" + bankNo).setValue(Math.round(totalCtofor6Month));
        var avergTotalCto12MonthPr = (totalCtofor12Month / 12);
        var avergTotalCto6MonthPr = (totalCtofor6Month / 6);
        $("txtAverageCTO12MonthBank" + bankNo).setValue(Math.round(avergTotalCto12MonthPr.toFixed(2)));
        $("txtAverageCTO6MonthBank" + bankNo).setValue(Math.round(avergTotalCto6MonthPr.toFixed(2)));

        //var linkCto = "<option value='" + Math.round(avergTotalCto12MonthPr.toFixed(2)) + "'>" + Math.round(avergTotalCto12MonthPr.toFixed(2)) + "</option><option value='" + Math.round(avergTotalCto6MonthPr.toFixed(2)) + "'>" + Math.round(avergTotalCto6MonthPr.toFixed(2)) + "</option>";

        var linkCto = "<option value='" + Math.round(totalCtofor12Month.toFixed(2)) + "'>" + Math.round(totalCtofor12Month.toFixed(2)) + "</option><option value='" + Math.round(totalCtofor6Month.toFixed(2)) + "'>" + Math.round(totalCtofor6Month.toFixed(2)) + "</option>";
        $("cmbConsideredCtoForPrBank" + bankNo).update(linkCto);
        $("cmbConsideredCtoForPrBank" + bankNo).setValue(Math.round(avergTotalCto12MonthPr.toFixed(2)));

        var considerMarginFinal = $F("txtConsiderMarginForPrimaryBank" + bankNo);
        var grossIncome = parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100);
        $("txtgrossincomeForPrimaryBank" + bankNo).setValue(Math.round(grossIncome));
        $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(Math.round(grossIncome));

        var share = $F("txtShareForPrimaryBank" + bankNo);

        var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

        $("txtNetincomeForPrimaryBank" + bankNo).setValue(Math.round(Math.round(netIncomeTotal.toFixed(2))));
    },

    //Change Average Balance For bussiness Man Primary
    changeAverageBalanceForPrApplicant: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalAvgfor12Month = 0;
        var totalAvgfor6Month = 0;
        var sixMonthArrayForAvg = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo)) && !util.isEmpty($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo))) {
                totalAvgfor12Month += parseFloat($F('txtAvarageBalance' + i + 'ForIncomeGeneratorBank' + bankNo));
            }
            sixMonthArrayForAvg.push(totalAvgfor12Month);
        }
        sixMonthArrayForAvg = sixMonthArrayForAvg.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForAvg.length - 6; s++) {
            if (sixMonthArrayForAvg[s] != "") {
                totalAvgfor6Month += parseFloat(sixMonthArrayForAvg[s]);
            }
        }
        var avergTotalAvg12Monthjoint = (totalAvgfor12Month / 12);
        var avergTotalAvg6Monthjoint = (totalAvgfor6Month / 6);
        $("txtAverageBalanceFor12MonthBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));
        $("txtAverageBalanceFor6MonthBank" + bankNo).setValue(avergTotalAvg6Monthjoint.toFixed(2));

        var incomeAssement = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
        if (incomeAssement == "1") {
            analystDetailsHelper.calculateAverageBalance(bankNo, "Pr");
        }

        var linkAvg = "<option value='" + avergTotalAvg12Monthjoint.toFixed(2) + "'>" + avergTotalAvg12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalAvg6Monthjoint.toFixed(2) + "'>" + avergTotalAvg6Monthjoint.toFixed(2) + "</option>";
        $("cmbConsideredBalanceForPrBank" + bankNo).update(linkAvg);
        $("cmbConsideredBalanceForPrBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));

    },

    //Change Credit turn Over For Bussiness Man Joint 
    changeCreditTurnoverForJtApplicant: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField)) && !util.isNegativeNumber($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var sixMonthArrayForCto = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        var sixmonthVariable = 0;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo))) {
                totalCtofor12Month += parseFloat($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo));
            }
            if (util.isFloat($F('txtAdjustment' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtAdjustment' + i + 'ForJointBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForJointBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo)) - parseFloat($F('txtAdjustment' + i + 'ForJointBank' + bankNo));
            }
            else if (util.isNegativeNumber($F('txtAdjustment' + i + 'ForJointBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForJointBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtcreditturnOver' + i + 'ForJointBank' + bankNo)) - parseFloat($F('txtAdjustment' + i + 'ForJointBank' + bankNo));
            }
            sixMonthArrayForCto.push(sixmonthVariable);
        }
        $("txtTotalCTOFor12MonthJointBank" + bankNo).setValue(Math.round(totalCtofor12Month));

        //Code Comment by Washik 26/12/2012
        //        sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
        //        for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
        //            if (sixMonthArrayForCto[s] != "") {
        //                totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
        //            }
        //        }
        //New Code Entered By Washik
        for (var s = 0; s <= 5; s++) {
            if ((sixMonthArrayForCto.length - 1) - s < 0) {
            }
            else {
                if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                    totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                }
            }

        }
        //New Code Ended by Washik

        $("txtTotalCTOFor6MonthJointBank" + bankNo).setValue(Math.round(totalCtofor6Month));
        var avergTotalCto12Monthjoint = (totalCtofor12Month / 12);
        var avergTotalCto6Monthjoint = (totalCtofor6Month / 6);
        $("txtAverageBalance12MonthForJointBank" + bankNo).setValue(Math.round(avergTotalCto12Monthjoint.toFixed(2)));
        $("txtAverageBalance6MonthForJointBank" + bankNo).setValue(Math.round(avergTotalCto6Monthjoint.toFixed(2)));


        //var linkCto = "<option value='" + avergTotalCto12Monthjoint.toFixed(2) + "'>" + avergTotalCto12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalCto6Monthjoint.toFixed(2) + "'>" + avergTotalCto6Monthjoint.toFixed(2) + "</option>";
        var linkCto = "<option value='" + Math.round(totalCtofor12Month.toFixed(2)) + "'>" + Math.round(totalCtofor12Month.toFixed(2)) + "</option><option value='" + Math.round(totalCtofor6Month.toFixed(2)) + "'>" + Math.round(totalCtofor6Month.toFixed(2)) + "</option>";
        $("cmbConsideredCtoForJtBank" + bankNo).update(linkCto);
        $("cmbConsideredCtoForJtBank" + bankNo).setValue(Math.round(avergTotalCto12Monthjoint.toFixed(2)));

        var considerMarginFinalForJoint = $F("txtConsideredMarginBMForJointBank" + bankNo);

        var grossIncomeForJoint = parseFloat(parseFloat(avergTotalCto12Monthjoint * considerMarginFinalForJoint) / 100);
        $("txtgrossIncomeForJointBank" + bankNo).setValue(Math.round(grossIncomeForJoint));
        $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncomeForJoint);

        var shareJoint = $F("txtShareForJointBank" + bankNo);

        var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);

        $("txtNetIncomeForJointBank" + bankNo).setValue(Math.round(netIncomeTotalJoint.toFixed(2)));



    },

    //Change Average Balance For bussiness Man Joint 
    changeAverageBalanceForJtApplicant: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalAvgfor12Month = 0;
        var totalAvgfor6Month = 0;
        var sixMonthArrayForAvg = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForJointBank' + bankNo))) {
                totalAvgfor12Month += parseFloat($F('txtAverageBalance' + i + 'ForJointBank' + bankNo));
            }
            sixMonthArrayForAvg.push(totalAvgfor12Month);
        }
        sixMonthArrayForAvg = sixMonthArrayForAvg.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForAvg.length - 6; s++) {
            if (sixMonthArrayForAvg[s] != "") {
                totalAvgfor6Month += parseFloat(sixMonthArrayForAvg[s]);
            }
        }
        var avergTotalAvg12Monthjoint = (totalAvgfor12Month / 12);
        var avergTotalAvg6Monthjoint = (totalAvgfor6Month / 6);
        $("txtAverageBalance12MonthForJointBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));
        $("txtAverageBalance6MonthForJointBank" + bankNo).setValue(avergTotalAvg6Monthjoint.toFixed(2));

        var incomeAssement = $F("cmbIncomeAssementForJointBank" + bankNo);
        if (incomeAssement == "1") {
            analystDetailsHelper.calculateAverageBalance(bankNo, "Jt");
        }



        var linkAvg = "<option value='" + avergTotalAvg12Monthjoint + "'>" + avergTotalAvg12Monthjoint + "</option><option value='" + avergTotalAvg6Monthjoint + "'>" + avergTotalAvg6Monthjoint + "</option>";

        $("cmbConsideredBalanceForJtBank" + bankNo).update(linkAvg);
        $("cmbConsideredBalanceForJtBank" + bankNo).setValue(avergTotalAvg12Monthjoint);

    },


    //Change Consider Over Draft with Scb
    changeConsiderForOverDraftWithScb: function(bankNo, accountType, tabName) {
        var totalLimit = 0;
        var totalInterest = 0;

        if (accountType == "Pr" && tabName == "BM") {

            var tableBmPrBankDraftWithScb = $("tblOverDraftFacilitiesForPrBMWithScb" + bankNo);
            var rowCountPrBmWithScb = tableBmPrBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountPrBmWithScb - 1; i++) {
                var considerwithScbBmPr = $F("cmbConsider" + i + "ForBank" + bankNo);
                if (considerwithScbBmPr == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForBank' + bankNo));
                    }
                }
            }

            var tableBmPrBankDraftOffScb = $("tblOverDraftFacilitiesForPrBMWithScb" + bankNo);
            var rowCountPrBmOffScb = tableBmPrBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountPrBmOffScb - 1; i++) {
                var considerOffScbBmPr = $F("cmbConsider" + i + "ForOffScbBank" + bankNo);
                if (considerOffScbBmPr == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForOffSCBBank' + bankNo));
                    }
                }
            }
            $("txtTotallimitForOoverDraftBMPrBank" + bankNo).setValue(Math.round(totalLimit));
            $("txtTotalInterestForOverDraftBMPrBank" + bankNo).setValue(Math.round(totalInterest));



            var consideredCto = $F("cmbConsideredCtoForPrBank" + bankNo);
            var considerMargin = $F("txtConsiderMarginForPrimaryBank" + bankNo);
            var grossIncome = 0;
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            var share = $F("txtShareForPrimaryBank" + bankNo);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    if (parseInt(share) == 100) {
                        grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalanceFor12MonthBank" + bankNo)) * considerMargin) / 100));
                    }
                    else {
                        alert("Average balance is not applicable");
                        $('divAverageBalancePr' + bankNo).style.display = 'block';
                        $('txtConsiderMarginForPrimaryBank' + bankNo).disabled = "false";
                    }
                }
                else {
                    grossIncome = Math.round(parseFloat((parseFloat(consideredCto) * parseFloat(considerMargin)) / 100));
                }
            }
            else {
                grossIncome = Math.round(parseFloat((parseFloat(consideredCto) * parseFloat(considerMargin)) / 100));
            }
            $("txtgrossincomeForPrimaryBank" + bankNo).setValue(grossIncome);
            var netIncome = grossIncome - totalInterest;
            $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(netIncome);

            var netIncomeTotal = Math.round(parseFloat((netIncome * parseFloat(share)) / 100));
            $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal);

        }
        else if (accountType == "Jt" && tabName == "BM") {
            var tableBmJtBankDraftWithScb = $("tblOverDraftFacilitiesForJtBMWithScb" + bankNo);
            var rowCountJtBmWithScb = tableBmJtBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountJtBmWithScb - 1; i++) {
                var considerwithScbBmJt = $F("cmbInterest" + i + "ForJointBank" + bankNo);
                if (considerwithScbBmJt == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForJointBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForJointBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForJointBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForJointBank' + bankNo));
                    }
                }
            }

            var tableBmJtBankDraftOffScb = $("tblOverDraftFacilitiesForJtBMOffScb" + bankNo);
            var rowCountJtBmOffScb = tableBmJtBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountJtBmOffScb - 1; i++) {
                var considerOffScbBmJt = $F("cmbConsiderOffScbBM" + i + "ForJointBank" + bankNo);
                if (considerOffScbBmJt == "1") {
                    if (util.isFloat($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimitOffScb' + i + 'ForJointBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo)) && !util.isEmpty($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterestOffSCB' + i + 'ForJointBank' + bankNo));
                    }
                }
            }



            var considerMarginFinalForJoint = $F("txtConsideredMarginBMForJointBank" + bankNo);
            var shareJoint = $F("txtShareForJointBank" + bankNo);
            var incomeAssIdForBuss = $F("cmbIncomeAssementForJointBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    if (parseInt(shareJoint) == 100) {
                        grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalance12MonthForJointBank" + bankNo)) * considerMarginFinalForJoint) / 100));
                    }
                    else {
                        alert("Average balance is not applicable");
                        $('divAverageBalanceJoint' + bankNo).style.display = 'block';
                        $('txtConsideredMarginForJointBank' + bankNo).disabled = "false";
                    }

                }
                else {
                    grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100));
                }
            }
            else {
                grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100));
            }
            $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncome);
            var netIncomeJt = grossIncome - totalInterest;
            $("txtNetAfterPaymentForJointBank" + bankNo).setValue(netIncomeJt);



            var netIncomeTotalJoint = Math.round(parseFloat((netIncomeJt * parseFloat(shareJoint)) / 100));

            $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint);
        }
        else if (accountType == "Pr" && tabName == "SE") {
            var tableSePrBankDraftWithScb = $("tblOverDraftFacilitiesForPrSEWithScb" + bankNo);
            var rowCountPrSeWithScb = tableSePrBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountPrSeWithScb - 1; i++) {
                var considerwithScbSePr = $F("cmbConsider" + i + "WithScbForSEBank" + bankNo);
                if (considerwithScbSePr == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForSEBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForSEBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForSEBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForSEBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForSEBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForSEBank' + bankNo));
                    }
                }
            }

            var tableSePrBankDraftOffScb = $("tblOverDraftFacilitiesForPrSEOffScb" + bankNo);
            var rowCountPrSeOffScb = tableSePrBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountPrSeOffScb - 1; i++) {
                var considerOffScbSePr = $F("cmbConsider" + i + "ForSEOffScbBank" + bankNo);
                if (considerOffScbSePr == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForSEOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForSEOffSCBBank' + bankNo));
                    }
                }
            }
            var considerMarginFinalForSePr = $F("txtConsiderMarginForSEPrBank" + bankNo);
            var shareSepr = $F("txtShareForSEPrBank" + bankNo);
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEPrBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    if (parseInt(shareSepr) == 100) {
                        grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalanceForSEPr12MonthBank" + bankNo)) * considerMarginFinalForSePr) / 100));
                    }
                    else {
                        alert("Average balance is not applicable");
                        $('divAverageBalanceForSEPr' + bankNo).style.display = 'block';
                        $('txtConsiderMarginForSEPrBank' + bankNo).disabled = "false";
                    }
                }
                else {
                    grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinalForSePr) / 100));
                }
            }
            else {
                grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinalForSePr) / 100));
            }
            //var grossIncomeForSePr = parseFloat((parseFloat($F("cmbConsideredCtoForSEPrBank" + bankNo)) * considerMarginFinalForSePr) / 100);
            $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncome);
            var netIncomeSePr = grossIncome - totalInterest;
            $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(netIncomeSePr);



            var netIncomeTotalSePr = Math.round(parseFloat((netIncomeSePr * parseFloat(shareSepr)) / 100));

            $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotalSePr);
        }
        else if (accountType == "Jt" && tabName == "SE") {
            var tableSeJtBankDraftWithScb = $("tblOverDraftFacilitiesForJtSEWithScb" + bankNo);
            var rowCountJtSeWithScb = tableSeJtBankDraftWithScb.rows.length;

            for (var i = 1; i <= rowCountJtSeWithScb - 1; i++) {
                var considerwithScbSeJt = $F("cmbConsider" + i + "WithScbForSEJtBank" + bankNo);
                if (considerwithScbSeJt == "1") {
                    if (util.isFloat($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo)) && !util.isEmpty($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimt' + i + 'WithScbForSEJtBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo)) && !util.isEmpty($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterst' + i + 'WithScbForSEJtBank' + bankNo));
                    }
                }
            }

            var tableSeJtBankDraftOffScb = $("tblOverDraftFacilitiesForJtSEOffScbBank" + bankNo);
            var rowCountJtSeOffScb = tableSeJtBankDraftOffScb.rows.length;

            for (var i = 1; i <= rowCountJtSeOffScb - 1; i++) {
                var considerOffScbSeJt = $F("cmbConsider" + i + "ForSEJtOffScbBank" + bankNo);
                if (considerOffScbSeJt == "1") {
                    if (util.isFloat($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo)) && !util.isEmpty($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo))) {
                        totalLimit += parseFloat($F('txtLimit' + i + 'ForSEJtOffSCBBank' + bankNo));
                    }
                    if (util.isFloat($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo)) && !util.isEmpty($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo))) {
                        totalInterest += parseFloat($F('txtInterest' + i + 'ForSEJtOffSCBBank' + bankNo));
                    }
                }
            }
            var considerMarginFinalForSeJt = $F("txtConsiderMarginForSEJtBank" + bankNo);
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEJtBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    if (parseInt(shareSepr) == 100) {
                        grossIncome = Math.round(parseFloat((parseFloat($F("txtAverageBalanceForSEJt12MonthBank" + bankNo)) * considerMarginFinalForSePr) / 100));
                    }
                    else {
                        alert("Average balance is not applicable");
                        $('divAverageBalanceForSEJt' + bankNo).style.display = 'block';
                        $('txtgrossincomeForSEJtBank' + bankNo).disabled = "false";
                    }

                }
                else {
                    grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForSePr) / 100));
                }
            }
            else {
                grossIncome = Math.round(parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForSePr) / 100));
            }
            //var grossIncomeForSeJt = parseFloat((parseFloat($F("cmbConsideredCtoForSEJtBank" + bankNo)) * considerMarginFinalForSeJt) / 100);
            $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncome);
            var netIncomeSeJt = grossIncome - totalInterest;
            $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(netIncomeSeJt);

            var shareSeJt = $F("txtShareForSEJtBank" + bankNo);

            var netIncomeTotalSeJt = Math.round(parseFloat((netIncomeSeJt * parseFloat(shareSeJt)) / 100));

            $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalSeJt);
        }
    },

    //Change Consider Cto
    changeConsiderCto: function(bankNo, accounttype, tabName) {
        if (accounttype == "Pr" && tabName == "BM") {

            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    analystDetailsHelper.calculateAverageBalance(bankNo, "Pr");
                }
                else {
                    analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Pr", "BM");
                    //                    var consideredCto = $F("cmbConsideredCtoForPrBank" + bankNo);
                    //                    var considerMargin = $F("txtConsiderMarginForPrimaryBank" + bankNo);
                    //                    var grossIncome = parseFloat((parseFloat(consideredCto) * parseFloat(considerMargin)) / 100);
                    //                    $("txtgrossincomeForPrimaryBank" + bankNo).setValue(grossIncome);
                    //                    $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(grossIncome);

                    //                    var share = $F("txtShareForPrimaryBank" + bankNo);

                    //                    var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

                    //                    $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal.toFixed(2));
                }
            }
            else {
                //                var consideredCto = $F("cmbConsideredCtoForPrBank" + bankNo);
                //                var considerMargin = $F("txtConsiderMarginForPrimaryBank" + bankNo);
                //                var grossIncome = parseFloat((parseFloat(consideredCto) * parseFloat(considerMargin)) / 100);
                //                $("txtgrossincomeForPrimaryBank" + bankNo).setValue(grossIncome);
                //                $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(grossIncome);

                //                var share = $F("txtShareForPrimaryBank" + bankNo);

                //                var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

                //                $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal.toFixed(2));
                analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Pr", "BM");
            }

        }
        else if (accounttype == "Jt" && tabName == "BM") {

            var incomeAssIdForBuss = $F("cmbIncomeAssementForJointBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    analystDetailsHelper.calculateAverageBalance(bankNo, "Jt");
                }
                else {
                    //                    var considerMarginFinalForJoint = $F("txtConsiderMarginForPrimaryBank" + bankNo);
                    //                    var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);
                    //                    $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncomeForJoint);
                    //                    $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncomeForJoint);
                    //                    var shareJoint = $F("txtShareForJointBank" + bankNo);
                    //                    var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);
                    //                    $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));
                    analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Jt", "BM");
                }
            }
            else {
                //                var considerMarginFinalForJoint = $F("txtConsiderMarginForPrimaryBank" + bankNo);
                //                var grossIncomeForJoint = parseFloat((parseFloat($F("cmbConsideredCtoForJtBank" + bankNo)) * considerMarginFinalForJoint) / 100);
                //                $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncomeForJoint);
                //                $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncomeForJoint);
                //                var shareJoint = $F("txtShareForJointBank" + bankNo);
                //                var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);
                //                $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotalJoint.toFixed(2));
                analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Jt", "BM");
            }
        }
        else if (accounttype == "Pr" && tabName == "SE") {

            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEPrBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    analystDetailsHelper.changeIncomeAssementForSelfEmployed(bankNo, "Pr");
                }
                else {
                    //                    var consideredCtoForSePr = $F("cmbConsideredCtoForSEPrBank" + bankNo);
                    //                    var considerMarginForEsPr = $F("txtConsiderMarginForSEPrBank" + bankNo);
                    //                    var grossIncomeForSePr = Math.round(parseFloat((parseFloat(consideredCtoForSePr) * parseFloat(considerMarginForEsPr)) / 100));
                    //                    $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncomeForSePr);
                    //                    $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncomeForSePr);
                    //                    var shareForSePr = $F("txtShareForSEPrBank" + bankNo);
                    //                    var netIncomeTotalForSePr = Math.round(parseFloat((grossIncomeForSePr * parseFloat(shareForSePr)) / 100));
                    //                    $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotalForSePr);
                    analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Pr", "SE");
                }
            }
            else {
                //                var consideredCtoForSePr = $F("cmbConsideredCtoForSEPrBank" + bankNo);
                //                var considerMarginForEsPr = $F("txtConsiderMarginForSEPrBank" + bankNo);
                //                var grossIncomeForSePr = Math.round(parseFloat((parseFloat(consideredCtoForSePr) * parseFloat(considerMarginForEsPr)) / 100));
                //                $("txtgrossincomeForSEPrBank" + bankNo).setValue(grossIncomeForSePr);
                //                $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(grossIncomeForSePr);
                //                var shareForSePr = $F("txtShareForSEPrBank" + bankNo);
                //                var netIncomeTotalForSePr = Math.round(parseFloat((grossIncomeForSePr * parseFloat(shareForSePr)) / 100));
                //                $("txtNetincomeForSEPrBank" + bankNo).setValue(netIncomeTotalForSePr);
                analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Pr", "SE");
            }
        }
        else if (accounttype == "Jt" && tabName == "SE") {
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForSEJtBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            if (incomeAssBuss) {
                if (incomeAssBuss.INAM_METHODCODE == "FT") {
                    analystDetailsHelper.changeIncomeAssementForSelfEmployed(bankNo, "Jt");
                }
                else {
                    //                    var consideredCtoForSeJt = $F("cmbConsideredCtoForSEJtBank" + bankNo);
                    //                    var considerMarginForSeJt = $F("txtConsiderMarginForSEJtBank" + bankNo);
                    //                    var grossIncomeForSeJt = Math.round(parseFloat((parseFloat(consideredCtoForSeJt) * parseFloat(considerMarginForSeJt)) / 100));
                    //                    $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncomeForSeJt);
                    //                    $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncomeForSeJt);

                    //                    var shareForSeJt = $F("txtShareForSEJtBank" + bankNo);

                    //                    var netIncomeTotalForSeJt = Math.round(parseFloat((grossIncomeForSeJt * parseFloat(shareForSeJt)) / 100));

                    //                    $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalForSeJt);
                    analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Jt", "SE");
                }
            }
            else {
                //                var consideredCtoForSeJt = $F("cmbConsideredCtoForSEJtBank" + bankNo);
                //                var considerMarginForSeJt = $F("txtConsiderMarginForSEJtBank" + bankNo);
                //                var grossIncomeForSeJt = Math.round(parseFloat((parseFloat(consideredCtoForSeJt) * parseFloat(considerMarginForSeJt)) / 100));
                //                $("txtgrossincomeForSEJtBank" + bankNo).setValue(grossIncomeForSeJt);
                //                $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(grossIncomeForSeJt);

                //                var shareForSeJt = $F("txtShareForSEJtBank" + bankNo);

                //                var netIncomeTotalForSeJt = Math.round(parseFloat((grossIncomeForSeJt * parseFloat(shareForSeJt)) / 100));

                //                $("txtNetincomeForSEJtBank" + bankNo).setValue(netIncomeTotalForSeJt);
                analystDetailsHelper.changeConsiderForOverDraftWithScb(bankNo, "Jt", "SE");
            }
        }
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accounttype, tabName);
    },

    //Change Income Assessment
    changeIncomeAssement: function(bankNo, accountType) {
        if (accountType == "Pr") {
            var incomeAssIdForBuss = $F("cmbIncomeAssessmentForPrimaryBank" + bankNo);
            var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBuss);
            $("txtIncomeAssessmentCodeForPrimaryBank" + bankNo).setValue(incomeAssBuss.INAM_METHODCODE);
            $('txtConsiderMarginForPrimaryBank' + bankNo).disabled = "";
            if (incomeAssBuss.INAM_METHODCODE == "FT") {
                // Code Comment by Washik
                //                var averageBalancePr = false;

                //                for (var i = 0; i < Page.isAverageBalancePrimaryArray.length; i++) {
                //                    if (Page.isAverageBalancePrimaryArray[i].BankNo == bankNo && Page.isAverageBalancePrimaryArray[i].Accounttype == accountType && Page.isAverageBalancePrimaryArray[i].TabName == "Bm") {
                //                        averageBalancePr = true;
                //                        break;
                //                    }
                //                }
                //                if (averageBalancePr == false) {
                var share = $F("txtShareForPrimaryBank" + bankNo);
                if (parseInt(share) == 100) {
                    $('divPrevRepHis' + bankNo).style.display = 'none';
                    $('divAverageBalancePr' + bankNo).style.display = 'block';
                    $('txtConsiderMarginForPrimaryBank' + bankNo).disabled = "false";
                    analystDetailsHelper.calculateAverageBalance(bankNo, accountType);
                }
                else {
                    alert("Average Balance is not applicable");
                    $("cmbIncomeAssessmentForPrimaryBank" + bankNo).setValue('-1');
                    $("txtIncomeAssessmentCodeForPrimaryBank" + bankNo).setValue('');
                    $('divPrevRepHis' + bankNo).style.display = 'none';
                    $('divAverageBalancePr' + bankNo).style.display = 'none';
                }

            }
            else if (incomeAssBuss.INAM_METHODCODE == "FF") {
                $('divAverageBalancePr' + bankNo).style.display = 'none';
                $('divPrevRepHis' + bankNo).style.display = 'block';

                var avergTotalCto12MonthPr = $F("cmbConsideredCtoForPrBank" + bankNo);
                var considerMarginFinal = $F("txtConsiderMarginForPrimaryBank" + bankNo);

                var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                $("txtgrossincomeForPrimaryBank" + bankNo).setValue(grossIncome);
                $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(grossIncome);

                var share = $F("txtShareForPrimaryBank" + bankNo);

                var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

                $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal.toFixed(2));


            }
            else {
                $('divAverageBalancePr' + bankNo).style.display = 'none';
                $('divPrevRepHis' + bankNo).style.display = 'none';
                var avergTotalCto12MonthPr = $F("cmbConsideredCtoForPrBank" + bankNo);
                var considerMarginFinal = $F("txtConsiderMarginForPrimaryBank" + bankNo);

                var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                $("txtgrossincomeForPrimaryBank" + bankNo).setValue(grossIncome);
                $("txtNetAfterPaymentForPrimaryBank" + bankNo).setValue(grossIncome);

                var share = $F("txtShareForPrimaryBank" + bankNo);

                var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

                $("txtNetincomeForPrimaryBank" + bankNo).setValue(netIncomeTotal.toFixed(2));
            }

        }
        else if (accountType == "Jt") {

            var incomeAssIdForBussJoint = $F("cmbIncomeAssementForJointBank" + bankNo);
            var incomeAssBussJoint = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssIdForBussJoint);
            $('txtConsideredMarginBMForJointBank' + bankNo).disabled = "";
            $("txtIncomeAssementCodeForJointBank" + bankNo).setValue(incomeAssBussJoint.INAM_METHODCODE);
            if (incomeAssBussJoint.INAM_METHODCODE == "FT") {
                //Code Comment by Washik 26/12/2012
                //                var averageBalanceJt = false;

                //                for (var i = 0; i < Page.isAverageBalanceJointArray.length; i++) {
                //                    if (Page.isAverageBalanceJointArray[i].BankNo == bankNo && Page.isAverageBalanceJointArray[i].Accounttype == accountType && Page.isAverageBalanceJointArray[i].TabName == "Bm") {
                //                        averageBalanceJt = true;
                //                        break;
                //                    }
                //                }

                //                if (averageBalanceJt == false) {
                var share = $F("txtShareForJointBank" + bankNo);
                if (parseInt(share) == 100) {
                    $('txtConsideredMarginBMForJointBank' + bankNo).disabled = "false";
                    $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                    $('divAverageBalanceJoint' + bankNo).style.display = 'block';
                    analystDetailsHelper.calculateAverageBalance(bankNo, accountType);
                }
                else {
                    alert("Average Balance is not applicable");
                    $("cmbIncomeAssementForJointBank" + bankNo).setValue('-1');
                    $("txtIncomeAssementCodeForJointBank" + bankNo).setValue('');
                    $('divPrevRepHisJoint' + bankNo).style.display = 'none';
                    $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                }
            }
            else if (incomeAssBussJoint.INAM_METHODCODE == "FF") {
                $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                $('divPrevRepHisJoint' + bankNo).style.display = 'block';

                var avergTotalCto12MonthPr = $F("cmbConsideredCtoForJtBank" + bankNo);
                var considerMarginFinal = $F("txtConsideredMarginBMForJointBank" + bankNo);

                var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncome);
                $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncome);

                var share = $F("txtShareForJointBank" + bankNo);

                var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

                $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotal.toFixed(2));

            }
            else {
                $('divAverageBalanceJoint' + bankNo).style.display = 'none';
                $('divPrevRepHisJoint' + bankNo).style.display = 'none';

                var avergTotalCto12MonthPr = $F("cmbConsideredCtoForJtBank" + bankNo);
                var considerMarginFinal = $F("txtConsideredMarginBMForJointBank" + bankNo);

                var grossIncome = Math.round(parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100));
                $("txtgrossIncomeForJointBank" + bankNo).setValue(grossIncome);
                $("txtNetAfterPaymentForJointBank" + bankNo).setValue(grossIncome);

                var share = $F("txtShareForJointBank" + bankNo);

                var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

                $("txtNetIncomeForJointBank" + bankNo).setValue(netIncomeTotal.toFixed(2));
            }
        }
        AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNo, accountType, 'BM');
    },

    //Consider Prev Repayment His Off Scb
    considerPrevrepHis: function(bankId, tabName, accountType) {
        var consider = "";
        var offscbEmi = 0;
        var onscbEmi = 0;
        var totalEmi = 0;
        if (tabName == "BM" && accountType == "Pr") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForPrimaryOffScbBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEMI" + i + "ForOffSCBPrimaryBank" + bankId)) && !util.isEmpty($F("txtEMI" + i + "ForOffSCBPrimaryBank" + bankId))) {
                        offscbEmi += parseFloat($F("txtEMI" + i + "ForOffSCBPrimaryBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotal1OnSCBRepHisForPrimaryBank" + bankId)) && !util.isEmpty($F("txtTotal1OnSCBRepHisForPrimaryBank" + bankId))) {
                onscbEmi = $F("txtTotal1OnSCBRepHisForPrimaryBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txttotalForOffSCBForPrimaryBank" + bankId).setValue(offscbEmi);
            $("txtTotalEMIClosingConsideredForPrimaryBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "BM" && accountType == "Jt") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsiderOffScb" + i + "ForJointBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmiOffScb" + i + "ForJointBank" + bankId)) && !util.isEmpty($F("txtEmiOffScb" + i + "ForJointBank" + bankId))) {
                        offscbEmi += parseFloat($F("txtEmiOffScb" + i + "ForJointBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotalOnScbForJointBank" + bankId)) && !util.isEmpty($F("txtTotalOnScbForJointBank" + bankId))) {
                onscbEmi = $F("txtTotalOnScbForJointBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotalOffSCBJointBank" + bankId).setValue(offscbEmi);
            $("txtTotalEMIClosingConsideredPrevRepHisForJointBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "SE" && accountType == "Pr") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForSEPrOffScbBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEMI" + i + "ForOffSCBSEPrBank" + bankId)) && !util.isEmpty($F("txtEMI" + i + "ForOffSCBSEPrBank" + bankId))) {
                        offscbEmi += parseFloat($F("txtEMI" + i + "ForOffSCBSEPrBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotal1OnSCBRepHisForSEPrBank" + bankId)) && !util.isEmpty($F("txtTotal1OnSCBRepHisForSEPrBank" + bankId))) {
                onscbEmi = $F("txtTotal1OnSCBRepHisForSEPrBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txttotalForOffSCBForSEPrBank" + bankId).setValue(offscbEmi);
            $("txtTotalEMIClosingConsideredForSEPrBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "SE" && accountType == "Jt") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForSEJtRepHisOffScbBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEMI" + i + "ForOffSCBSEJtBank" + bankId)) && !util.isEmpty($F("txtEMI" + i + "ForOffSCBSEJtBank" + bankId))) {
                        offscbEmi += parseFloat($F("txtEMI" + i + "ForOffSCBSEJtBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotal1OnSCBRepHisForSEJtBank" + bankId)) && !util.isEmpty($F("txtTotal1OnSCBRepHisForSEJtBank" + bankId))) {
                onscbEmi = $F("txtTotal1OnSCBRepHisForSEJtBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txttotalForOffSCBForSEJtBank" + bankId).setValue(offscbEmi);
            $("txtTotalEMIClosingConsideredForSEJtBank" + bankId).setValue(totalEmi);

        }
    },

    //Consider Prev Repayment His On Scb
    considerPrevrepHisOnScb: function(bankId, tabName, accountType) {
        var consider = "";
        var offscbEmi = 0;
        var onscbEmi = 0;
        var totalEmi = 0;
        if (tabName == "BM" && accountType == "Pr") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForOnSCBReplHisForPrimaryBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmi" + i + "ForOnScbForPrimaryBank" + bankId)) && !util.isEmpty($F("txtEmi" + i + "ForOnScbForPrimaryBank" + bankId))) {
                        onscbEmi += parseFloat($F("txtEmi" + i + "ForOnScbForPrimaryBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txttotalForOffSCBForPrimaryBank" + bankId)) && !util.isEmpty($F("txttotalForOffSCBForPrimaryBank" + bankId))) {
                offscbEmi = $F("txttotalForOffSCBForPrimaryBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotal1OnSCBRepHisForPrimaryBank" + bankId).setValue(onscbEmi);
            $("txtTotalEMIClosingConsideredForPrimaryBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "BM" && accountType == "Jt") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsiderOnScb" + i + "ForJointBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmiOnScb" + i + "ForJointBank" + bankId)) && !util.isEmpty($F("txtEmiOnScb" + i + "ForJointBank" + bankId))) {
                        onscbEmi += parseFloat($F("txtEmiOnScb" + i + "ForJointBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txtTotalOffSCBJointBank" + bankId)) && !util.isEmpty($F("txtTotalOffSCBJointBank" + bankId))) {
                offscbEmi = $F("txtTotalOffSCBJointBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotalOnScbForJointBank" + bankId).setValue(onscbEmi);
            $("txtTotalEMIClosingConsideredPrevRepHisForJointBank" + bankId).setValue(totalEmi);

        }

        if (tabName == "SE" && accountType == "Pr") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForOnSCBReplHisForSEPrBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmi" + i + "ForOnScbForSEPrBank" + bankId)) && !util.isEmpty($F("txtEmi" + i + "ForOnScbForSEPrBank" + bankId))) {
                        onscbEmi += parseFloat($F("txtEmi" + i + "ForOnScbForSEPrBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txttotalForOffSCBForSEPrBank" + bankId)) && !util.isEmpty($F("txttotalForOffSCBForSEPrBank" + bankId))) {
                offscbEmi = $F("txttotalForOffSCBForSEPrBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotal1OnSCBRepHisForSEPrBank" + bankId).setValue(onscbEmi);
            $("txtTotalEMIClosingConsideredForSEPrBank" + bankId).setValue(totalEmi);

        }
        if (tabName == "SE" && accountType == "Jt") {
            for (var i = 1; i < 5; i++) {
                consider = $F("cmbConsider" + i + "ForOnSCBReplHisForSEJtBank" + bankId);
                if (consider == "1") {
                    if (util.isFloat($F("txtEmi" + i + "ForOnScbForSEJtBank" + bankId)) && !util.isEmpty($F("txtEmi" + i + "ForOnScbForSEJtBank" + bankId))) {
                        onscbEmi += parseFloat($F("txtEmi" + i + "ForOnScbForSEJtBank" + bankId));
                    }
                }
            }
            if (util.isFloat($F("txttotalForOffSCBForSEJtBank" + bankId)) && !util.isEmpty($F("txttotalForOffSCBForSEJtBank" + bankId))) {
                offscbEmi = $F("txttotalForOffSCBForSEJtBank" + bankId);
            }
            totalEmi = parseFloat(offscbEmi) + parseFloat(onscbEmi);
            $("txtTotal1OnSCBRepHisForSEJtBank" + bankId).setValue(onscbEmi);
            $("txtTotalEMIClosingConsideredForSEJtBank" + bankId).setValue(totalEmi);

        }
    },

    //Change credit Turn Over For Self Employed For Primary
    changeCreditTurnoverForPrApplicantSelfEmployed: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField)) && !util.isNegativeNumber($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var sixMonthArrayForCto = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        var sixmonthVariable = 0;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo))) {
                totalCtofor12Month += parseFloat($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo));
            }
            if (util.isFloat($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo)) - parseFloat($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo));
            }
            else if (util.isNegativeNumber($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtCreditTurnOver' + i + 'ForSEPrBank' + bankNo)) - parseFloat($F('txtAdjustment' + i + 'ForSEPrBank' + bankNo));
            }
            sixMonthArrayForCto.push(sixmonthVariable);
        }
        $("txtTotalCTOForSEPr12MonthBank" + bankNo).setValue(totalCtofor12Month);

        //Code Comment by Washik
        //        sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
        //        for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
        //            if (sixMonthArrayForCto[s] != "") {
        //                totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
        //            }
        //        }

        //New Code Entered By Washik
        for (var s = 0; s <= 5; s++) {
            if ((sixMonthArrayForCto.length - 1) - s < 0) {
            }
            else {
                if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                    totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                }
            }

        }
        //New Code Ended by Washik

        $("txtTotalCTOForSEPr6MonthBank" + bankNo).setValue(Math.round(totalCtofor6Month));
        var avergTotalCto12MonthPr = (totalCtofor12Month / 12);
        var avergTotalCto6MonthPr = (totalCtofor6Month / 6);
        $("txtAverageCTOForSEPr12MonthBank" + bankNo).setValue(Math.round(avergTotalCto12MonthPr.toFixed(2)));
        $("txtAverageCTOForSEPr6MonthBank" + bankNo).setValue(Math.round(avergTotalCto6MonthPr.toFixed(2)));

        //var linkCto = "<option value='" + Math.round(avergTotalCto12MonthPr.toFixed(2)) + "'>" + Math.round(avergTotalCto12MonthPr.toFixed(2)) + "</option><option value='" + Math.round(avergTotalCto6MonthPr.toFixed(2)) + "'>" + Math.round(avergTotalCto6MonthPr.toFixed(2)) + "</option>";
        var linkCto = "<option value='" + Math.round(totalCtofor12Month.toFixed(2)) + "'>" + Math.round(totalCtofor12Month.toFixed(2)) + "</option><option value='" + Math.round(totalCtofor6Month.toFixed(2)) + "'>" + Math.round(totalCtofor6Month.toFixed(2)) + "</option>";

        $("cmbConsideredCtoForSEPrBank" + bankNo).update(linkCto);
        $("cmbConsideredCtoForSEPrBank" + bankNo).setValue(Math.round(avergTotalCto12MonthPr.toFixed(2)));

        var considerMarginFinal = $F("txtConsiderMarginForSEPrBank" + bankNo);
        var grossIncome = parseFloat((avergTotalCto12MonthPr * considerMarginFinal) / 100);
        $("txtgrossincomeForSEPrBank" + bankNo).setValue(Math.round(grossIncome));
        $("txtNetAfterPaymentForSEPrBank" + bankNo).setValue(Math.round(grossIncome));

        var share = $F("txtShareForSEPrBank" + bankNo);

        var netIncomeTotal = parseFloat((grossIncome * parseFloat(share)) / 100);

        $("txtNetincomeForSEPrBank" + bankNo).setValue(Math.round(netIncomeTotal.toFixed(2)));


    },

    //Change Average Balance For Self Employed Primary
    changeAverageBalanceForPrApplicantSelfEmployed: function(bankNo, tableName, identityField) {
        if (!util.isFloat($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalAvgfor12Month = 0;
        var totalAvgfor6Month = 0;
        var sixMonthArrayForAvg = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo))) {
                totalAvgfor12Month += parseFloat($F('txtAverageBalance' + i + 'ForSEPrBank' + bankNo));
            }
            sixMonthArrayForAvg.push(totalAvgfor12Month);
        }
        sixMonthArrayForAvg = sixMonthArrayForAvg.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForAvg.length - 6; s++) {
            if (sixMonthArrayForAvg[s] != "") {
                totalAvgfor6Month += parseFloat(sixMonthArrayForAvg[s]);
            }
        }
        var avergTotalAvg12Monthjoint = (totalAvgfor12Month / 12);
        var avergTotalAvg6Monthjoint = (totalAvgfor6Month / 6);
        $("txtAverageCTOForSEPr12MonthBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));
        $("txtAverageCTOForSEPr6MonthBank" + bankNo).setValue(avergTotalAvg6Monthjoint.toFixed(2));

        var incomeAssement = $F("cmbIncomeAssessmentForSEPrBank" + bankNo);
        if (incomeAssement == "1") {

            analystDetailsHelper.calculateAverageBalanceForSelfEmployed(bankNo, "Pr");

        }
        var linkAvg = "<option value='" + avergTotalAvg12Monthjoint.toFixed(2) + "'>" + avergTotalAvg12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalAvg6Monthjoint.toFixed(2) + "'>" + avergTotalAvg6Monthjoint.toFixed(2) + "</option>";
        $("cmbConsideredBalanceForSEPrBank" + bankNo).update(linkAvg);
        $("cmbConsideredBalanceForSEPrBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));

    },

    //Change Patients
    changePatients: function(identity, accountType, tabName, bankNo) {
        if (accountType == "Pr" && tabName == "Se") {
            if (util.isFloat($F(identity))) {
                var tablePrAccount = $("tblChamberIncomeForSEPrBank" + bankNo);
                var rowCount = tablePrAccount.rows.length;


                var patientsOld = 0.0, feeOld = 0.0, totalOfPatientsOld = 0.0;
                var patientsNew = 0.0, feeNew = 0.0, totalOfPatientsNew = 0.0;
                var total = 0.0, dayMonth = 0, totalIncome = 0.0, totalChamberIncome = 0.0;


                for (var i = 1; i <= rowCount - 1; i++) {
                    if (util.isFloat($F('txtPatientsOld' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtPatientsOld' + i + 'ForSEPrBank' + bankNo))) {
                        patientsOld = $F('txtPatientsOld' + i + 'ForSEPrBank' + bankNo);
                    }
                    if (util.isFloat($F('txtFeeOld' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtFeeOld' + i + 'ForSEPrBank' + bankNo))) {
                        feeOld = $F('txtFeeOld' + i + 'ForSEPrBank' + bankNo);
                    }
                    if (util.isFloat($F('txtPatientsNew' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtPatientsNew' + i + 'ForSEPrBank' + bankNo))) {
                        patientsNew = $F('txtPatientsNew' + i + 'ForSEPrBank' + bankNo);
                    }
                    if (util.isFloat($F('txtFeeNew' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtFeeNew' + i + 'ForSEPrBank' + bankNo))) {
                        feeNew = $F('txtFeeNew' + i + 'ForSEPrBank' + bankNo);
                    }
                    if (util.isFloat($F('txtDayMonth' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtDayMonth' + i + 'ForSEPrBank' + bankNo))) {
                        dayMonth = $F('txtDayMonth' + i + 'ForSEPrBank' + bankNo);
                    }



                    totalOfPatientsOld = (patientsOld * feeOld);
                    totalOfPatientsNew = (patientsNew * feeNew);
                    total = totalOfPatientsOld + totalOfPatientsNew;
                    totalIncome = total * dayMonth;
                    totalChamberIncome += totalIncome;


                    $('txtTotalOld' + i + 'ForSEPrBank' + bankNo).setValue(totalOfPatientsOld);
                    $('txtTotalNew' + i + 'ForSEPrBank' + bankNo).setValue(totalOfPatientsNew);
                    $('txtTotalOldNew' + i + 'ForSEPr1Bank' + bankNo).setValue(total);
                    $('txtIncome' + i + 'ForSEPrBank' + bankNo).setValue(totalIncome);
                    patientsOld = 0;
                    feeOld = 0;
                    patientsNew = 0;
                    feeNew = 0;
                    dayMonth = 0;
                    totalIncome = 0;
                    totalOfPatientsOld = 0;
                    totalOfPatientsNew = 0;
                    total = 0;
                }
                $('txtTotalChamberIncomeForSEPrBank' + bankNo).setValue(totalChamberIncome);

                var identityPrOther = "txtTotalOtherIncomeForSEPrBank" + bankNo;
                //debugger;
                analystDetailsHelper.calculateFinaltTotalIncome(identityPrOther, accountType, tabName, bankNo);

            }
            else {


                alert("Please type a Valid number Only");
                $(identity).setValue("0");
                //$(identity).focus();
                return false;
            }
        } else if (accountType == "Jt" && tabName == "Se") {
            if (util.isFloat($F(identity))) {
                var tablePrAccount = $("tblChamberIncomeForSEJtBank" + bankNo);
                var rowCount = tablePrAccount.rows.length;


                var patientsOld = 0.0, feeOld = 0.0, totalOfPatientsOld = 0.0;
                var patientsNew = 0.0, feeNew = 0.0, totalOfPatientsNew = 0.0;
                var total = 0.0, dayMonth = 0, totalIncome = 0.0, totalChamberIncome = 0.0;


                for (var i = 1; i <= rowCount - 1; i++) {
                    if (util.isFloat($F('txtPatientsOld' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtPatientsOld' + i + 'ForSEJtBank' + bankNo))) {
                        patientsOld = $F('txtPatientsOld' + i + 'ForSEJtBank' + bankNo);
                    }
                    if (util.isFloat($F('txtFeeOld' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtFeeOld' + i + 'ForSEJtBank' + bankNo))) {
                        feeOld = $F('txtFeeOld' + i + 'ForSEJtBank' + bankNo);
                    }
                    if (util.isFloat($F('txtPatientsNew' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtPatientsNew' + i + 'ForSEJtBank' + bankNo))) {
                        patientsNew = $F('txtPatientsNew' + i + 'ForSEJtBank' + bankNo);
                    }
                    if (util.isFloat($F('txtFeeNew' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtFeeNew' + i + 'ForSEJtBank' + bankNo))) {
                        feeNew = $F('txtFeeNew' + i + 'ForSEJtBank' + bankNo);
                    }
                    if (util.isFloat($F('txtDayMonth' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtDayMonth' + i + 'ForSEJtBank' + bankNo))) {
                        dayMonth = $F('txtDayMonth' + i + 'ForSEJtBank' + bankNo);
                    }



                    totalOfPatientsOld = (patientsOld * feeOld);
                    totalOfPatientsNew = (patientsNew * feeNew);
                    total = totalOfPatientsOld + totalOfPatientsNew;
                    totalIncome = total * dayMonth;
                    totalChamberIncome += totalIncome;


                    $('txtTotalOld' + i + 'ForSEJtBank' + bankNo).setValue(totalOfPatientsOld);
                    $('txtTotalNew' + i + 'ForSEJtBank' + bankNo).setValue(totalOfPatientsNew);
                    $('txtTotalOldNew' + i + 'ForSEJt1Bank' + bankNo).setValue(total);
                    $('txtIncome' + i + 'ForSEJtBank' + bankNo).setValue(totalIncome);
                    patientsOld = 0;
                    feeOld = 0;
                    patientsNew = 0;
                    feeNew = 0;
                    dayMonth = 0;
                    totalIncome = 0;
                    totalOfPatientsOld = 0;
                    totalOfPatientsNew = 0;
                    total = 0;





                }
                $('txtTotalChamberIncomeForSEJtBank' + bankNo).setValue(totalChamberIncome);
                var identityJtOther = "txtTotalOtherIncomeForSEJtBank" + bankNo;
                analystDetailsHelper.calculateFinaltTotalIncome(identityJtOther, accountType, tabName, bankNo);

            }
            else {


                alert("Please type a Valid number Only");
                $(identity).setValue("0");
                //$(identity).focus();
                return false;
            }

        }
    },

    //Calculate Final Income For Chamber
    calculateFinaltTotalIncome: function(identity, accountType, tabName, bankNo) {
        if (identity == "txtTotalOtherIncomeForSEPrBank" + bankNo && accountType == "Pr" && tabName == "Se") {
            // debugger;
            var totalChamberInc = 0, otherIncome = 0;
            if ((util.isFloat($F(identity)) && !util.isEmpty($F(identity)))) {
                if ($F('txtTotalChamberIncomeForSEPrBank' + bankNo) != "") {
                    totalChamberInc = parseFloat($F('txtTotalChamberIncomeForSEPrBank' + bankNo));
                }
                if ($F(identity) != "") {
                    otherIncome = parseFloat($F(identity));
                }
                $('txtTotalIncomeForSEPrBank' + bankNo).setValue(totalChamberInc + otherIncome);


            } else {
                alert("Please type a Valid number Only");
                $(identity).setValue("0");
                //$(identity).focus();
                return false;
            }
        }
        else if (identity == "txtTotalOtherIncomeForSEJtBank" + bankNo && accountType == "Jt" && tabName == "Se") {
            var totalChamberIncJt = 0, otherIncomeJt = 0;
            if ((util.isFloat($F(identity)) && !util.isEmpty($F(identity)))) {
                if ($F('txtTotalChamberIncomeForSEJtBank' + bankNo) != "") {
                    totalChamberIncJt = parseFloat($F('txtTotalChamberIncomeForSEJtBank' + bankNo));
                }
                if ($F(identity) != "") {
                    otherIncomeJt = parseFloat($F(identity));
                }
                $('txtTotalIncomeForSEJtBank' + bankNo).setValue(totalChamberIncJt + otherIncomeJt);


            } else {
                alert("Please type a Valid number Only");
                $(identity).setValue("0");
                //$(identity).focus();
                return false;
            }
        }
        analystDetailsHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, "DC");
    },

    //Change Tution
    changeTution: function(identity, accountType, tabName, bankNo) {
        var noOfStudent = 0;
        var noOfDaysofTution = 0;
        var fees = 0;
        var total = 0;
        if (util.isFloat($F(identity))) {
            if (accountType == "Pr" && tabName == "Se") {
                if (util.isFloat($F('txtNoOfStudentForSEPrBank' + bankNo)) && !util.isEmpty($F('txtNoOfStudentForSEPrBank' + bankNo))) {
                    noOfStudent = $F('txtNoOfStudentForSEPrBank' + bankNo);
                }
                if (util.isFloat($F('txtNoOfDaysForTutionForSEPrBank' + bankNo)) && !util.isEmpty($F('txtNoOfDaysForTutionForSEPrBank' + bankNo))) {
                    noOfDaysofTution = $F('txtNoOfDaysForTutionForSEPrBank' + bankNo);
                }
                if (util.isFloat($F('txtTutionFeeForSEPrBank' + bankNo)) && !util.isEmpty($F('txtTutionFeeForSEPrBank' + bankNo))) {
                    fees = $F('txtTutionFeeForSEPrBank' + bankNo);
                }
                total = noOfStudent * noOfDaysofTution * fees;
                $('txtTotalInComeTutionForSEPrBank' + bankNo).setValue(total);
            }
            else {
                if (util.isFloat($F('txtNoOfStudentForSEJtBank' + bankNo)) && !util.isEmpty($F('txtNoOfStudentForSEJtBank' + bankNo))) {
                    noOfStudent = $F('txtNoOfStudentForSEJtBank' + bankNo);
                }
                if (util.isFloat($F('txtNoOfDaysForTutionForSEJtBank' + bankNo)) && !util.isEmpty($F('txtNoOfDaysForTutionForSEJtBank' + bankNo))) {
                    noOfDaysofTution = $F('txtNoOfDaysForTutionForSEJtBank' + bankNo);
                }
                if (util.isFloat($F('txtTutionFeeForSEJtBank' + bankNo)) && !util.isEmpty($F('txtTutionFeeForSEJtBank' + bankNo))) {
                    fees = $F('txtTutionFeeForSEJtBank' + bankNo);
                }
                total = noOfStudent * noOfDaysofTution * fees;
                $('txtTotalInComeTutionForSEJtBank' + bankNo).setValue(total);
            }
            analystDetailsHelper.setBussinessManCalculationForDoctorAndTeachers(accountType, "TC");


        }
        else {
            alert("Please type a Valid number Only");
            $(identity).setValue("0");
            //$(identity).focus();
            return false;
        }
    },

    //Change Creadit Turn Over For Joint Self Employed
    changeCreditTurnoverForJtApplicantSelfEmployed: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField)) && !util.isNegativeNumber($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var sixMonthArrayForCto = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        var sixmonthVariable = 0;

        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo))) {
                totalCtofor12Month += parseFloat($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo));
            }
            if (util.isFloat($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo)) - parseFloat($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo));
            }
            else if (util.isNegativeNumber($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo))) {
                totalCtofor12Month = totalCtofor12Month - parseFloat($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo));
                sixmonthVariable = parseFloat($F('txtCreditTurnOver' + i + 'ForSEJtBank' + bankNo)) - parseFloat($F('txtAdjustment' + i + 'ForSEJtBank' + bankNo));
            }
            sixMonthArrayForCto.push(sixmonthVariable);
        }
        $("txtTotalCTOForSEJt12MonthBank" + bankNo).setValue(Math.round(totalCtofor12Month));
        //Code Comment by Washik
        //        sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
        //        for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
        //            if (sixMonthArrayForCto[s] != "") {
        //                totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
        //            }
        //        }

        //New Code Entered By Washik
        for (var s = 0; s <= 5; s++) {
            if ((sixMonthArrayForCto.length - 1) - s < 0) {
            }
            else {
                if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                    totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                }
            }

        }
        //New Code Ended by Washik


        $("txtTotalCTOForSEJt6MonthBank" + bankNo).setValue(Math.round(totalCtofor6Month));
        var avergTotalCto12Monthjoint = (totalCtofor12Month / 12);
        var avergTotalCto6Monthjoint = (totalCtofor6Month / 6);
        $("txtAverageCTOForSEJt12MonthBank" + bankNo).setValue(Math.round(avergTotalCto12Monthjoint.toFixed(2)));
        $("txtAverageCTOForSEJt6MonthBank" + bankNo).setValue(Math.round(avergTotalCto6Monthjoint.toFixed(2)));


        //var linkCto = "<option value='" + avergTotalCto12Monthjoint.toFixed(2) + "'>" + avergTotalCto12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalCto6Monthjoint.toFixed(2) + "'>" + avergTotalCto6Monthjoint.toFixed(2) + "</option>";
        var linkCto = "<option value='" + Math.round(totalCtofor12Month.toFixed(2)) + "'>" + Math.round(totalCtofor12Month.toFixed(2)) + "</option><option value='" + Math.round(totalCtofor6Month.toFixed(2)) + "'>" + Math.round(totalCtofor6Month.toFixed(2)) + "</option>";
        $("cmbConsideredCtoForSEJtBank" + bankNo).update(linkCto);
        $("cmbConsideredCtoForSEJtBank" + bankNo).setValue(Math.round(avergTotalCto12Monthjoint.toFixed(2)));

        var considerMarginFinalForJoint = $F("txtConsiderMarginForSEJtBank" + bankNo);

        var grossIncomeForJoint = parseFloat(parseFloat(avergTotalCto12Monthjoint * considerMarginFinalForJoint) / 100);
        $("txtgrossincomeForSEJtBank" + bankNo).setValue(Math.round(grossIncomeForJoint));
        $("txtNetAfterPaymentForSEJtBank" + bankNo).setValue(Math.round(grossIncomeForJoint));

        var shareJoint = $F("txtShareForSEJtBank" + bankNo);

        var netIncomeTotalJoint = parseFloat((grossIncomeForJoint * parseFloat(shareJoint)) / 100);

        $("txtNetincomeForSEJtBank" + bankNo).setValue(Math.round(netIncomeTotalJoint.toFixed(2)));

    },

    //Change Average Balance For Joint Self Employed
    changeAverageBalanceForJtApplicantSelfEmployed: function(bankNo, tableName, identityField) {

        if (!util.isFloat($F(identityField))) {
            alert("Number field is required");
            $(identityField).setValue('0');
            //$(identityField).focus();
            return false;
        }
        var totalAvgfor12Month = 0;
        var totalAvgfor6Month = 0;
        var sixMonthArrayForAvg = [];
        var tablePrAccount = $(tableName);
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {

            if (util.isFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo))) {
                totalAvgfor12Month += parseFloat($F('txtAverageBalance' + i + 'ForSEJtBank' + bankNo));
            }
            sixMonthArrayForAvg.push(totalAvgfor12Month);
        }
        sixMonthArrayForAvg = sixMonthArrayForAvg.sort(function(a, b) { return b - a });
        for (var s = 0; s < sixMonthArrayForAvg.length - 6; s++) {
            if (sixMonthArrayForAvg[s] != "") {
                totalAvgfor6Month += parseFloat(sixMonthArrayForAvg[s]);
            }
        }
        var avergTotalAvg12Monthjoint = (totalAvgfor12Month / 12);
        var avergTotalAvg6Monthjoint = (totalAvgfor6Month / 6);
        $("txtAverageBalanceForSEJt12MonthBank" + bankNo).setValue(avergTotalAvg12Monthjoint.toFixed(2));
        $("txtAverageBalanceForSEJt6MonthBank" + bankNo).setValue(avergTotalAvg6Monthjoint.toFixed(2));

        var incomeAssement = $F("cmbIncomeAssessmentForSEJtBank" + bankNo);
        if (incomeAssement == "1") {
            analystDetailsHelper.calculateAverageBalanceForSelfEmployed(bankNo, "Jt");
        }



        var linkAvg = "<option value='" + avergTotalAvg12Monthjoint + "'>" + avergTotalAvg12Monthjoint + "</option><option value='" + avergTotalAvg6Monthjoint + "'>" + avergTotalAvg6Monthjoint + "</option>";

        $("cmbConsideredBalanceForSEJtBank" + bankNo).update(linkAvg);
        $("cmbConsideredBalanceForSEJtBank" + bankNo).setValue(avergTotalAvg12Monthjoint);

    },

    //Cjange Sequrity PDC
    changeSequrityPdc: function() {
        var seqrityMode = $F("txtInstrumentForFinalizetab");
        var sequritypdc = $F("cmbSequrityPdcForFinalizetab");
        if (sequritypdc == "2" && seqrityMode == "2") {
            $('divrepaymentDetailsForFinalizeTab').style.display = "none";
            $('divNoofPdcForFinalizetab').style.display = "none";

        }
        else {
            $('divrepaymentDetailsForFinalizeTab').style.display = "block";
            $('divNoofPdcForFinalizetab').style.display = "block";
        }
        AnalystDetailsCalculationHelper.calculateRepaymentForFinalize();

    },

    //Change Bank Name For repayment Finalization
    changeBankNameForRepaymentForFinalizeTab: function() {
        var bankId = $F("cmbBankNameForFinalizeTabofrepaymentMethod");
        var bankdetails = Page.otherBankAccountForrepayment.findByProp('BankId', bankId);
        if (bankdetails) {
            $("txtAccountnumberForaBankFinalizeTab").setValue(bankdetails.AccountNumber);
        }
    },

    //Change User
    changeUser: function(identity) {
        var userId = $F(identity);
        var user = Page.userArray.findByProp('USER_ID', userId);
        if (user) {
            var userCode = user.USER_CODE;
            if (identity == "cmbAppraisedby") {
                $("txtAppraiserCode").setValue(userCode);
            }
            else if (identity == "cmbSupportedby") {
                $("txtSupportedbyCode").setValue(userCode);
            }
            else if (identity == "cmbApprovedby") {
                $("txtApprovedbyCode").setValue(userCode);
                analystDetailsManager.GetApproverLimitByUserId(userId);
            }

        }
    },
    changeGeneralInsurence: function() {
        var generalInsurence = $F("cmbGeneralInsurencePreference");
        if (generalInsurence == "-1") {
            $('cmbGeneralInsurence').setValue("2");
            $('cmbGeneralInsurence').disabled = 'false';
        }
        else {
            $('cmbGeneralInsurence').disabled = '';
        }
        analystDetailsHelper.calculateTenorSettings();
    },
    changeLifeInsurence: function() {
        var lifeInsurence = $F("cmbARTAReference");
        if (lifeInsurence == "-1") {
            $('cmbARTA').setValue("2");
            $('cmbARTA').disabled = 'false';
        }
        else {
            $('cmbARTA').disabled = '';
        }
        analystDetailsHelper.calculateTenorSettings();
    },
    changeInstrumentForFinalizeTab: function() {
        var askingrepayment = $F("cmbAskingRepaymentmethod");
        var noofPdc = 0;

        var instrumentForFinalize = $F("txtInstrumentForFinalizetab");
        if (instrumentForFinalize == "-1" && askingrepayment != "-1") {
            $("txtInstrumentForFinalizetab").setValue(askingrepayment);
        }
        if (instrumentForFinalize == "2") {
            $('spnAccountFinalizeRepayment').style.display = 'block';
            $('divScbAccountFinalizeRepayment').style.display = 'block';
            $('cmbSequrityPdcForFinalizetab').disabled = '';
            $("cmbSequrityPdcForFinalizetab").setValue('1');

            var considerTenor = $F("txtCosideredtenorForLc");
            if (!util.isFloat(considerTenor)) {
                noofPdc = 0;
            }
            else {
                noofPdc = parseInt(considerTenor) * 2;
            }

        }
        else if (instrumentForFinalize == "1") {
            $('spnAccountFinalizeRepayment').style.display = 'none';
            $('divScbAccountFinalizeRepayment').style.display = 'none';
            $('cmbSequrityPdcForFinalizetab').disabled = 'disabled';
            $("cmbSequrityPdcForFinalizetab").setValue('2');
            var considerTenorMonth = $F("txtConsideredMarginForLc");
            noofPdc = considerTenorMonth;
        }
        if (instrumentForFinalize != "-1") {
            $("cmbAskingRepaymentmethod").setValue(instrumentForFinalize);
            var instrumentForFinalizeName = $('cmbAskingRepaymentmethod').options[$('cmbAskingRepaymentmethod').selectedIndex].text;
            $("cmbAskingRepaymentmethodForFinalize").setValue(instrumentForFinalizeName);
        }
        $("txtNoOfPdcForFinalizeTab").setValue(noofPdc);
        analystDetailsHelper.changeSequrityPdc();

    },

    createOverDraftStatemntForBank: function(accountType, identity) {
        //if account type =1 , then onscb, accountTye = 2,  then offscb

        if (accountType == 1) {
            var overDraftStatus = $F("cmbStatusForSecuritiesForoverDraft" + identity);
            var originalLimit = $F("txtOverdraftlimit" + identity);
            var interestRate = $F("txtOverdraftInteres" + identity);
            if (Page.primaryProfessionId == 1 || Page.otherProfessionId == 1) {
                if (overDraftStatus == "1") {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('txtLimt' + identity + 'WithScbForBank' + b).setValue(originalLimit);
                        $('txtInterst' + identity + 'WithScbForBank' + b).setValue(interestRate);
                        $('cmbConsider' + identity + 'ForBank' + b).setValue('0');
                    }
                }
                else {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('txtLimt' + identity + 'WithScbForBank' + b).setValue('');
                        $('txtInterst' + identity + 'WithScbForBank' + b).setValue('');
                        $('cmbConsider' + identity + 'ForBank' + b).setValue('0');
                    }
                }
            }
            if (Page.primaryProfessionId == 3 || Page.otherProfessionId == 3 || Page.primaryProfessionId == 4 || Page.otherProfessionId == 4 || Page.primaryProfessionId == 6 || Page.otherProfessionId == 6) {
                if (overDraftStatus == "1") {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('txtLimt' + identity + 'WithScbForSEBank' + b).setValue(originalLimit);
                        $('txtInterst' + identity + 'WithScbForSEBank' + b).setValue(interestRate);
                        $('cmbConsider' + identity + 'WithScbForSEBank' + b).setValue('0');
                    }
                }
                else {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('txtLimt' + identity + 'WithScbForSEBank' + b).setValue('');
                        $('txtInterst' + identity + 'WithScbForSEBank' + b).setValue('');
                        $('cmbConsider' + identity + 'WithScbForSEBank' + b).setValue('0');
                    }
                }
            }
            if (Page.isjoint == true) {
                if (Page.jointPrimaryProfessionId == 1) {
                    if (overDraftStatus == "1") {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('txtLimit' + identity + 'ForJointBank' + b).setValue(originalLimit);
                            $('txtInterest' + identity + 'ForJointBank' + b).setValue(interestRate);
                            $('cmbInterest' + identity + 'ForJointBank' + b).setValue('0');
                        }
                    }
                    else {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('txtLimit' + identity + 'ForJointBank' + b).setValue('');
                            $('txtInterest' + identity + 'ForJointBank' + b).setValue('');
                            $('cmbInterest' + identity + 'ForJointBank' + b).setValue('0');
                        }
                    }
                }
                if (Page.jointPrimaryProfessionId == 3 || Page.jointPrimaryProfessionId == 4 || Page.jointPrimaryProfessionId == 6) {
                    if (overDraftStatus == "1") {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('txtLimt' + identity + 'WithScbForSEJtBank' + b).setValue(originalLimit);
                            $('txtInterst' + identity + 'WithScbForSEJtBank' + b).setValue(interestRate);
                            $('cmbConsider' + identity + 'WithScbForSEJtBank' + b).setValue('0');
                        }
                    }
                    else {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('txtLimt' + identity + 'WithScbForSEJtBank' + b).setValue('');
                            $('txtInterst' + identity + 'WithScbForSEJtBank' + b).setValue('');
                            $('cmbConsider' + identity + 'WithScbForSEJtBank' + b).setValue('0');
                        }
                    }
                }
            }

        }
        if (accountType == 2) {
            var overDraftStatus = $F("cmbStatusForOverDraftOffScb" + identity);
            var originalLimit = $F("txtOriginalLimitForOverDraftOffScb" + identity);
            var interestRate = $F("txtInterestForOverDraftInterest" + identity);
            var bankId = $F("cmbFinancialInstitutionForOverDraftOffSCB" + identity);

            //


            if (Page.primaryProfessionId == 1 || Page.otherProfessionId == 1) {
                if (overDraftStatus == "1") {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('cmbBankName' + identity + 'ForBussinessTabPrBank' + b).update(Page.bankLinkOption);
                        $('cmbBankName' + identity + 'ForBussinessTabPrBank' + b).setValue(bankId);
                        $('txtLimit' + identity + 'ForOffSCBBank' + b).setValue(originalLimit);
                        $('txtInterest' + identity + 'ForOffSCBBank' + b).setValue(interestRate);
                        $('cmbConsider' + identity + 'ForOffScbBank' + b).setValue('0');
                    }
                }
                else {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('cmbBankName' + identity + 'ForBussinessTabPrBank' + b).update('');
                        $('txtLimit' + identity + 'ForOffSCBBank' + b).setValue('');
                        $('txtInterest' + identity + 'ForOffSCBBank' + b).setValue('');
                        $('cmbConsider' + identity + 'ForOffScbBank' + b).setValue('0');
                    }
                }
            }
            if (Page.primaryProfessionId == 3 || Page.otherProfessionId == 3 || Page.primaryProfessionId == 4 || Page.otherProfessionId == 4 || Page.primaryProfessionId == 6 || Page.otherProfessionId == 6) {
                if (overDraftStatus == "1") {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('cmbBankName' + identity + 'ForSEPrBank' + b).update(Page.bankLinkOption);
                        $('cmbBankName' + identity + 'ForSEPrBank' + b).setValue(bankId);
                        $('txtLimit' + identity + 'ForSEOffSCBBank' + b).setValue(originalLimit);
                        $('txtInterest' + identity + 'ForSEOffSCBBank' + b).setValue(interestRate);
                        $('cmbConsider' + identity + 'ForSEOffScbBank' + b).setValue('0');
                    }
                }
                else {
                    for (var b = 1; b <= Page.prCount; b++) {
                        $('cmbBankName' + identity + 'ForSEPrBank' + b).update('');
                        $('txtLimit' + identity + 'ForSEOffSCBBank' + b).setValue('');
                        $('txtInterest' + identity + 'ForSEOffSCBBank' + b).setValue('');
                        $('cmbConsider' + identity + 'ForSEOffScbBank' + b).setValue('0');
                    }
                }
            }
            if (Page.isjoint == true) {
                if (Page.jointPrimaryProfessionId == 1) {
                    if (overDraftStatus == "1") {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('cmbBankName' + identity + 'ForBussinessTabJtBank' + b).update(Page.bankLinkOption);
                            $('cmbBankName' + identity + 'ForBussinessTabJtBank' + b).setValue(bankId);
                            $('txtLimitOffScb' + identity + 'ForJointBank' + b).setValue(originalLimit);
                            $('txtInterestOffSCB' + identity + 'ForJointBank' + b).setValue(interestRate);
                            $('cmbConsiderOffScbBM' + identity + 'ForJointBank' + b).setValue('0');
                        }
                    }
                    else {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('cmbBankName' + identity + 'ForBussinessTabJtBank' + b).update('');
                            $('txtLimitOffScb' + identity + 'ForJointBank' + b).setValue('');
                            $('txtInterestOffSCB' + identity + 'ForJointBank' + b).setValue('');
                            $('cmbConsiderOffScbBM' + identity + 'ForJointBank' + b).setValue('0');
                        }
                    }
                }
                if (Page.jointPrimaryProfessionId == 3 || Page.jointPrimaryProfessionId == 4 || Page.jointPrimaryProfessionId == 6) {
                    if (overDraftStatus == "1") {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('cmbBankName' + identity + 'ForSEJtBank' + b).update(Page.bankLinkOption);
                            $('cmbBankName' + identity + 'ForSEJtBank' + b).setValue(bankId);
                            $('txtLimit' + identity + 'ForSEJtOffSCBBank' + b).setValue(originalLimit);
                            $('txtInterest' + identity + 'ForSEJtOffSCBBank' + b).setValue(interestRate);
                            $('cmbConsider' + identity + 'ForSEJtOffScbBank' + b).setValue('0');
                        }
                    }
                    else {
                        for (var b = 1; b <= Page.jtCount; b++) {
                            $('cmbBankName' + identity + 'ForSEJtBank' + b).update('');
                            $('txtLimit' + identity + 'ForSEJtOffSCBBank' + b).setValue('');
                            $('txtInterest' + identity + 'ForSEJtOffSCBBank' + b).setValue('');
                            $('cmbConsider' + identity + 'ForSEJtOffScbBank' + b).setValue('0');
                        }
                    }
                }
            }
        }
    },
    changeRework: function() {

        var reworkDone = $F("cmbReworkDone");
        if (reworkDone == "1") {
            $('divReworkDet').style.display = "block";
        }
        else {
            $('divReworkDet').style.display = "none";
        }
    },
    changeProduct: function() {
        var productId = $F("cmbProductName");
        if (productId == "3") {
            //labelChangeHelper.showFinanceLabelForAnalyst();
        }
        else {
            //labelChangeHelper.showLoanLabelForAnalyst();
            if (productId == "1") {
                $('cmbARTA').setValue('2');
                $('cmbGeneralInsurence').setValue('2');
                $('txtAppropriteRate').setValue('4.43');
            }
            else {
                $('cmbARTA').disabled = '';
                $('cmbGeneralInsurence').disabled = '';
                $('cmbARTAReference').disabled = '';
            }

        }

        analystDetailsHelper.calculateInterestRate();
    }

};
Event.onReady(Page.init);