﻿
var LoanCalculationSaveManager = {

    save: function() {

        //create object for Auto_An_IncomeSegmentApplicantIncome
        LoanCalculationSaveManager.getApplicantIncomeObject();
        //create object for Auto_An_IncomeSegmentIncome
        LoanCalculationSaveManager.getIncomeObject();
        //create object for Auto_An_RepaymentCapability
        LoanCalculationSaveManager.getRepaymentCapabilityObject();
        //create object for auto_an_loancalculationtenor
        LoanCalculationSaveManager.getLoanCalculationTenorObject();
        //create object for Auto_An_LoanCalculationInterest
        LoanCalculationSaveManager.getLoanCalculationInterestObject();
        //create object for Auto_An_LoanCalculationAmount
        LoanCalculationSaveManager.getLoanCalculationAmountObject();
        //create object for Auto_An_LoanCalculationInsurance
        LoanCalculationSaveManager.getLoanCalculationInsuranceObject();
        //create object for Auto_An_LoanCalculationTotal
        LoanCalculationSaveManager.getLoanCalculationTotalObject();

    },

    getLoanCalculationTotalObject: function() {
        var LoanCalculationTotal = new Object();

        LoanCalculationTotal.AnalystMasterId = $F('txtAnalystMasterID');
        LoanCalculationTotal.TotalLoanAmount = $F('txtloanAmountForLcLoanSummary');
        if (!util.isFloat(LoanCalculationTotal.TotalLoanAmount) && util.isEmpty(LoanCalculationTotal.TotalLoanAmount)) {
            LoanCalculationTotal.TotalLoanAmount = 0;
        }
        LoanCalculationTotal.LTVExcludingInsurance = $F('txtLtvExclusdingInsurence');
        if (!util.isFloat(LoanCalculationTotal.LTVExcludingInsurance) && util.isEmpty(LoanCalculationTotal.LTVExcludingInsurance)) {
            LoanCalculationTotal.LTVExcludingInsurance = 0;
        }
        LoanCalculationTotal.FBRExcludingInsurance = $F('txtDBRExcludingInsurence');
        if (!util.isFloat(LoanCalculationTotal.FBRExcludingInsurance) && util.isEmpty(LoanCalculationTotal.FBRExcludingInsurance)) {
            LoanCalculationTotal.FBRExcludingInsurance = 0;
        }
        LoanCalculationTotal.LTVIncludingInsurance = $F('txtltvIncludingLtvForLcLoanSummary');
        if (!util.isFloat(LoanCalculationTotal.LTVIncludingInsurance) && util.isEmpty(LoanCalculationTotal.LTVIncludingInsurance)) {
            LoanCalculationTotal.LTVIncludingInsurance = 0;
        }
        LoanCalculationTotal.FBRIncludingInsurance = $F('txtDBRIncludingInsurence');
        if (!util.isFloat(LoanCalculationTotal.FBRIncludingInsurance) && util.isEmpty(LoanCalculationTotal.FBRIncludingInsurance)) {
            LoanCalculationTotal.FBRIncludingInsurance = 0;
        }
        LoanCalculationTotal.EMR = $F('txtEmrForlcloanSummary');
        if (!util.isFloat(LoanCalculationTotal.EMR) && util.isEmpty(LoanCalculationTotal.EMR)) {
            LoanCalculationTotal.EMR = 0;
        }
        LoanCalculationTotal.ExtraLTV = $F('txtExtraLtvForLcloanSumary');
        if (!util.isFloat(LoanCalculationTotal.ExtraLTV) && util.isEmpty(LoanCalculationTotal.ExtraLTV)) {
            LoanCalculationTotal.ExtraLTV = 0;
        }
        LoanCalculationTotal.ExtraDBR = $F('txtExtraDBRForLcloanSummary');
        if (!util.isFloat(LoanCalculationTotal.ExtraDBR) && util.isEmpty(LoanCalculationTotal.ExtraDBR)) {
            LoanCalculationTotal.ExtraDBR = 0;
        }
        return LoanCalculationTotal;

    },


    getLoanCalculationInsuranceObject: function() {
        var LoanCalculationInsurance = new Object();

        LoanCalculationInsurance.AnalystMasterId = $F('txtAnalystMasterID');
        LoanCalculationInsurance.GeneralInsurance = $F('txtgeneralInsurenceForLcIns');
        if (!util.isFloat(LoanCalculationInsurance.GeneralInsurance) && util.isEmpty(LoanCalculationInsurance.GeneralInsurance)) {
            LoanCalculationInsurance.GeneralInsurance = 0;
        }
        LoanCalculationInsurance.ARTA = $F('txtArtaForLCInsurenceAmount');
        if (!util.isFloat(LoanCalculationInsurance.ARTA) && util.isEmpty(LoanCalculationInsurance.ARTA)) {
            LoanCalculationInsurance.ARTA = 0;
        }
        LoanCalculationInsurance.TotalInsurance = $F('txtTotalInsurenceForLcInsurence');
        if (!util.isFloat(LoanCalculationInsurance.TotalInsurance) && util.isEmpty(LoanCalculationInsurance.TotalInsurance)) {
            LoanCalculationInsurance.TotalInsurance = 0;
        }
        return LoanCalculationInsurance;
    },


    getLoanCalculationAmountObject: function() {
        var LoanCalculationAmount = new Object();

        LoanCalculationAmount.AnalystMasterId = $F('txtAnalystMasterID');
        LoanCalculationAmount.LTVAllowed = $F('txtLtvAllowedForLcLoanAmount');
        if (!util.isFloat(LoanCalculationAmount.LTVAllowed) && util.isEmpty(LoanCalculationAmount.LTVAllowed)) {
            LoanCalculationAmount.LTVAllowed = 0;
        }
        LoanCalculationAmount.IncomeAllowed = $F('txtIncoomeAllowedForLcLoanAmount');
        if (!util.isFloat(LoanCalculationAmount.IncomeAllowed) && util.isEmpty(LoanCalculationAmount.IncomeAllowed)) {
            LoanCalculationAmount.IncomeAllowed = 0;
        }
        LoanCalculationAmount.BB_CADAllowed = $F('txtbdCadAllowedForLcLoanAmount');
        if (!util.isFloat(LoanCalculationAmount.IncomeAllowed) && util.isEmpty(LoanCalculationAmount.IncomeAllowed)) {
            LoanCalculationAmount.IncomeAllowed = 0;
        }
        LoanCalculationAmount.AskingLoan = $F('txtAskingLoanAmountForLcLoanAmount');
        if (!util.isFloat(LoanCalculationAmount.AskingLoan) && util.isEmpty(LoanCalculationAmount.AskingLoan)) {
            LoanCalculationAmount.AskingLoan = 0;
        }
        LoanCalculationAmount.ExistingFacilityAllowed = $F('txtExistingFacilitiesAllowedForLcLoanAmount');
        if (!util.isFloat(LoanCalculationAmount.ExistingFacilityAllowed) && util.isEmpty(LoanCalculationAmount.ExistingFacilityAllowed)) {
            LoanCalculationAmount.ExistingFacilityAllowed = 0;
        }
        LoanCalculationAmount.AppropriateLoan = $F('txtAppropriteloanAmountForLcLoanAmount');
        if (!util.isFloat(LoanCalculationAmount.AppropriateLoan) && util.isEmpty(LoanCalculationAmount.AppropriateLoan)) {
            LoanCalculationAmount.AppropriateLoan = 0;
        }
        LoanCalculationAmount.ApprovedLoan = $F('txtApprovedLoanAmountForLcLoanAmount');
        if (LoanCalculationAmount.ApprovedLoan == "") {
            LoanCalculationAmount.ApprovedLoan = 0;
        }
        return LoanCalculationAmount;
    },

    getLoanCalculationInterestObject: function() {
        var LoanCalculationInterest = new Object();

        LoanCalculationInterest.AnalystMasterId = $F('txtAnalystMasterID');
        LoanCalculationInterest.VendorAllowed = $F('txtInterestLCVendoreAllowed');
        if (LoanCalculationInterest.VendorAllowed == "") {
            LoanCalculationInterest.VendorAllowed = 0;
        }
        LoanCalculationInterest.ARTAAllowed = $F('txtInterestLCArtaAllowed');
        if (LoanCalculationInterest.ARTAAllowed == "") {
            LoanCalculationInterest.ARTAAllowed = 0;
        }
        LoanCalculationInterest.SegmentAllowed = $F('txtInterestLCSegmentAllowed');
        if (LoanCalculationInterest.SegmentAllowed == "") {
            LoanCalculationInterest.SegmentAllowed = 0;
        }
        LoanCalculationInterest.AskingRate = $F('txtInterestLCAskingRate');
        if (LoanCalculationInterest.AskingRate == "") {
            LoanCalculationInterest.AskingRate = 0;
        }
        LoanCalculationInterest.AppropriateRate = $F('txtAppropriteRate');
        if (LoanCalculationInterest.AppropriateRate == "") {
            LoanCalculationInterest.AppropriateRate = 0;
        }
        LoanCalculationInterest.ConsideredRate = $F('txtInterestLCCosideredRate');
        if (LoanCalculationInterest.ConsideredRate == "") {
            LoanCalculationInterest.ConsideredRate = 0;
        }
        return LoanCalculationInterest;

    },


    getLoanCalculationTenorObject: function() {
        var LoanCalculationTenor = new Object();
        LoanCalculationTenor.AnalystMasterId = $F('txtAnalystMasterID');
        LoanCalculationTenor.ARTAAllowed = $F('txtArtaAllowedFoLc');
        if (LoanCalculationTenor.ARTAAllowed == "") {
            LoanCalculationTenor.ARTAAllowed = 0;
        }
        LoanCalculationTenor.VehicleAllowed = $F('txtvehicleAllowedForLc');
        if (LoanCalculationTenor.VehicleAllowed == "") {
            LoanCalculationTenor.VehicleAllowed = 0;
        }
        LoanCalculationTenor.Level = $F('cmbLabelForTonerLc');
        LoanCalculationTenor.AgeAllowed = $F('txtAgeAllowedForLc');
        if (LoanCalculationTenor.AgeAllowed == "") {
            LoanCalculationTenor.AgeAllowed = 0;
        }
        LoanCalculationTenor.AskingTenor = $F('txtAskingTenorForLc');
        if (LoanCalculationTenor.AskingTenor == "") {
            LoanCalculationTenor.AskingTenor = 0;
        }
        LoanCalculationTenor.AppropriateTenor = $F('txtAppropriteTenorForLc');
        if (LoanCalculationTenor.AppropriateTenor == "") {
            LoanCalculationTenor.AppropriateTenor = 0;
        }
        LoanCalculationTenor.ConsideredTenor = $F('txtCosideredtenorForLc');
        LoanCalculationTenor.ConsideredInMonth = $F('txtConsideredMarginForLc');
        return LoanCalculationTenor;

    },

    getRepaymentCapabilityObject: function() {
        var repaymentCapability = new Object();
        repaymentCapability.AnalystMasterId = $F('txtAnalystMasterID');
        repaymentCapability.DBRLevel = $F('cmbDBRLevel');
        repaymentCapability.AppropriateDBR = $F('txtAppropriateDbr');
        if (repaymentCapability.AppropriateDBR == "") {
            repaymentCapability.AppropriateDBR = 0;
        }
        repaymentCapability.ConsideredDBR = $F('txtConsideredDbr');
        if (repaymentCapability.ConsideredDBR == "") {
            repaymentCapability.ConsideredDBR = 0;
        }
        repaymentCapability.MaxEMICapability = $F('txtMaximumEmiCapability');
        repaymentCapability.ExistingEMI = $F('txtExistingEmi');
        repaymentCapability.CurrentEMICapability = $F('txtCurrentEMICapability');
        if (util.isFloat(repaymentCapability.BalanceSupported) || !util.isEmpty(repaymentCapability.BalanceSupported)) {
            repaymentCapability.BalanceSupported = $F('txtBalanceSupported');
        }
        else {
            repaymentCapability.BalanceSupported = 0;
        }
        repaymentCapability.MaxEMI = $F('txtMaximumEmi');
        return repaymentCapability;

    },

    getIncomeObject: function() {
        var segmentIncome = new Object();
        
        segmentIncome.AnalystMasterId = $F('txtAnalystMasterID');
        segmentIncome.TotalIncome = $F('txtTotalInComeForLcAll');
        if (segmentIncome.TotalIncome == "") {
            segmentIncome.TotalIncome = 0;
        }
        segmentIncome.DeclaredIncome = $F('txtDeclaredIncomeForLcAll');
        if (segmentIncome.DeclaredIncome == "") {
            segmentIncome.DeclaredIncome = 0;
        }
        segmentIncome.AppropriateIncome = $F('txtAppropriteIncomeForLcAll');
        if (segmentIncome.AppropriateIncome == "") {
            segmentIncome.AppropriateIncome = 0;
        }
        segmentIncome.EmployeeSegment = $F('cmbEmployeeSegmentLc');
        segmentIncome.EmployeeSegmentCode = $F('txtSegmentCodeLc');
        segmentIncome.AssesmentMethod = $F('txtAssessmentMethodIdLc');
        if (segmentIncome.AssesmentMethod == "") {
            segmentIncome.AssesmentMethod = "-1";
        }
        segmentIncome.AssesmentMethodCode = $F('txtAssessmentCodeLc');
        segmentIncome.Category = $F('txtEmployeeCategoryIdForLc');
        if (segmentIncome.Category == "") {
            segmentIncome.Category = "-1";
        }
        segmentIncome.CategoryCode = $F('txtEmployeeCategoryCodeForLc');
        segmentIncome.TotalJointIncome = $F('txtTotalJointInComeForLcWithCal');
        if (segmentIncome.TotalJointIncome == "") {
            segmentIncome.TotalJointIncome = 0;
        }
        return segmentIncome;

    },


    getApplicantIncomeObject: function() {

        var ApplicantIncomeList = [];
        var tablePropertyPr = $('tblPrLoanCalculator');
        var rowCountPr = tablePropertyPr.rows.length;

        //PR Applicant
        for (var i = 1; i <= rowCountPr - 1; i++) {
            var ApplicantIncomePR = new Object();
            if (i < 7) {
                if ($F('txtIncomeBForLCPrBank' + i) == '') {

                }
                //            else if ($F('txtPApplicantAccountNumber' + i) == '') {
                //                alert('please select branch');
                //                $('cmbPApplicantBranch' + i).focus();
                //                return false;
                //            }
                else {
                    ApplicantIncomePR.AnalystMasterId = $F('txtAnalystMasterID');
                    ApplicantIncomePR.CalculationSource = $F('txtbankIdForLcPrBank' + i);
                    ApplicantIncomePR.Income = $F('txtIncomeBForLCPrBank' + i);
                    ApplicantIncomePR.IncomeAssesmentMethod = $F('txtAssMethodIdForLcPrBank' + i);
                    if (ApplicantIncomePR.IncomeAssesmentMethod == "") {
                        ApplicantIncomePR.IncomeAssesmentMethod = "-1";
                    }
                    ApplicantIncomePR.IncomeAssesmentCode = $F('txtAssCodeForLCPrBank' + i);
                    ApplicantIncomePR.EmployerCategory = $F('txtAssCatIdForLcPrBank' + i);
                    if (ApplicantIncomePR.EmployerCategory == "") {
                        ApplicantIncomePR.EmployerCategory = "-1";
                    }
                    ApplicantIncomePR.EmployerCategoryCode = $F('txtEmployerCatCodeForLCPrBank' + i);
                    ApplicantIncomePR.Consider = $F('cmbConsiderForLCPrBank' + i);
                    ApplicantIncomePR.ApplicantType = '1';

                    ApplicantIncomeList.push(ApplicantIncomePR);
                }
            }

            if (i == 7) {
                if ($F('txtIncomeBForLCPrSalary') == '') {

                }
                else {
                    ApplicantIncomePR.AnalystMasterId = $F('txtAnalystMasterID');
                    ApplicantIncomePR.CalculationSource = i * -1;
                    ApplicantIncomePR.Income = $F('txtIncomeBForLCPrSalary');
                    ApplicantIncomePR.IncomeAssesmentMethod = $F('txtAssMethodIdForLcPrSalary');
                    if (ApplicantIncomePR.IncomeAssesmentMethod == "") {
                        ApplicantIncomePR.IncomeAssesmentMethod = "-1";
                    }
                    ApplicantIncomePR.IncomeAssesmentCode = $F('txtAssCodeForLCPrSalary');
                    ApplicantIncomePR.EmployerCategory = $F('txtAssCatIdForLcPrSalary');
                    if (ApplicantIncomePR.EmployerCategory == "") {
                        ApplicantIncomePR.EmployerCategory = "-1";
                    }
                    ApplicantIncomePR.EmployerCategoryCode = $F('txtEmployerCatCodeForLCPrSalary');
                    ApplicantIncomePR.Consider = $F('cmbConsiderForLCPrSalary');
                    ApplicantIncomePR.ApplicantType = '1';

                    ApplicantIncomeList.push(ApplicantIncomePR);
                }
            } //end if for salary

            // if land loard

            if (i == 8) {
                if ($F('txtIncomeBForLCPrRent') == '') {

                }
                else {
                    ApplicantIncomePR.AnalystMasterId = $F('txtAnalystMasterID');
                    ApplicantIncomePR.CalculationSource = i * -1;
                    ApplicantIncomePR.Income = $F('txtIncomeBForLCPrRent');
                    ApplicantIncomePR.IncomeAssesmentMethod = $F('txtAssMethodIdForLcPrrent');
                    if (ApplicantIncomePR.IncomeAssesmentMethod == "-1") {
                        ApplicantIncomePR.IncomeAssesmentMethod = "-1";
                    }
                    ApplicantIncomePR.IncomeAssesmentCode = $F('txtAssCodeForLCPrRent');
                    ApplicantIncomePR.EmployerCategory = $F('txtAssCatIdForLcPrRent');
                    if (ApplicantIncomePR.EmployerCategory == "") {
                        ApplicantIncomePR.EmployerCategory = "-1";
                    }
                    ApplicantIncomePR.EmployerCategoryCode = $F('txtEmployerCatCodeForLCPrRent');
                    ApplicantIncomePR.Consider = $F('cmbConsiderForLCPrRent');
                    ApplicantIncomePR.ApplicantType = '1';

                    ApplicantIncomeList.push(ApplicantIncomePR);
                }
            } //end if for land loard

            // if Doctor

            if (i == 9) {
                if ($F('txtIncomeBForLCPrDoctor') == '') {

                }
                else {
                    ApplicantIncomePR.AnalystMasterId = $F('txtAnalystMasterID');
                    ApplicantIncomePR.CalculationSource = i * -1;
                    ApplicantIncomePR.Income = $F('txtIncomeBForLCPrDoctor');
                    ApplicantIncomePR.IncomeAssesmentMethod = $F('txtAssMethodIdForLcPrDoc');
                    if (ApplicantIncomePR.IncomeAssesmentMethod == "") {
                        ApplicantIncomePR.IncomeAssesmentMethod = "-1";
                    }
                    ApplicantIncomePR.IncomeAssesmentCode = $F('txtAssCodeForLCPrDoctor');
                    ApplicantIncomePR.EmployerCategory = $F('txtAssCatIdForLcPrDoc');
                    if (ApplicantIncomePR.EmployerCategory == "") {
                        ApplicantIncomePR.EmployerCategory = "-1";
                    }
                    ApplicantIncomePR.EmployerCategoryCode = $F('txtEmployerCatCodeForLCPrDoctor');
                    ApplicantIncomePR.Consider = $F('cmbConsiderForLCPrDoctor');
                    ApplicantIncomePR.ApplicantType = '1';

                    ApplicantIncomeList.push(ApplicantIncomePR);
                }
            } //end if for Doctor


            // if Teacher

            if (i == 10) {
                if ($F('txtIncomeBForLCPrTeacher') == '') {

                }
                else {
                    ApplicantIncomePR.AnalystMasterId = $F('txtAnalystMasterID');
                    ApplicantIncomePR.CalculationSource = i * -1;
                    ApplicantIncomePR.Income = $F('txtIncomeBForLCPrTeacher');
                    ApplicantIncomePR.IncomeAssesmentMethod = $F('txtAssMethodIdForLcPrTec');
                    if (ApplicantIncomePR.IncomeAssesmentMethod == "") {
                        ApplicantIncomePR.IncomeAssesmentMethod = "-1";
                    }
                    ApplicantIncomePR.IncomeAssesmentCode = $F('txtAssCodeForLCPrTeacher');
                    ApplicantIncomePR.EmployerCategory = $F('txtAssCatIdForLcPrTec');
                    if (ApplicantIncomePR.EmployerCategory == "") {
                        ApplicantIncomePR.EmployerCategory = "-1";
                    }
                    ApplicantIncomePR.EmployerCategoryCode = $F('txtEmployerCatCodeForLCPrTeacher');
                    ApplicantIncomePR.Consider = $F('cmbConsiderForLCPrTeacher');
                    ApplicantIncomePR.ApplicantType = '1';

                    ApplicantIncomeList.push(ApplicantIncomePR);
                }
            } //end if for Teacher

        } //end loop

        // end Primary Applicant


        //------------------------------------------------------------------------------

        // for Joint Applicant

        var reasonOfJointIncome = $F("cmbReasonofJointApplicant");
        if (reasonOfJointIncome == "1") {
            var tablePropertyJt = $('tblJtLoanCalculator');
            var rowCountJt = tablePropertyJt.rows.length;

            //PR Applicant
            for (var i = 1; i <= rowCountJt - 1; i++) {
                var ApplicantIncomeJT = new Object();
                if (i < 7) {
                    if ($F('txtIncomeBForLCJtBank' + i) == '') {

                    }
                    else {
                        ApplicantIncomeJT.AnalystMasterId = $F('txtAnalystMasterID');
                        ApplicantIncomeJT.CalculationSource = $F('txtbankIdForLcJtBank' + i);
                        ApplicantIncomeJT.Income = $F('txtIncomeBForLCJtBank' + i);
                        ApplicantIncomeJT.IncomeAssesmentMethod = $F('txtAssMethodIdForLcJtBank' + i);
                        if (ApplicantIncomeJT.IncomeAssesmentMethod == "") {
                            ApplicantIncomeJT.IncomeAssesmentMethod = "-1";
                        }
                        ApplicantIncomeJT.IncomeAssesmentCode = $F('txtAssCodeForLCJtBank' + i);
                        ApplicantIncomeJT.EmployerCategory = $F('txtAssCatIdForLcJtBank' + i);
                        if (ApplicantIncomeJT.EmployerCategory == "") {
                            ApplicantIncomeJT.EmployerCategory = "-1";
                        }
                        ApplicantIncomeJT.EmployerCategoryCode = $F('txtEmployerCatCodeForLCJtBank' + i);
                        ApplicantIncomeJT.Consider = $F('cmbConsiderForLCJtBank' + i);
                        ApplicantIncomeJT.ApplicantType = '2';

                        ApplicantIncomeList.push(ApplicantIncomeJT);
                    }
                }

                if (i == 7) {
                    if ($F('txtIncomeBForLCJtSalary') == '') {

                    }
                    else {
                        ApplicantIncomeJT.AnalystMasterId = $F('txtAnalystMasterID');
                        ApplicantIncomeJT.CalculationSource = i * -1;
                        ApplicantIncomeJT.Income = $F('txtIncomeBForLCJtSalary');
                        ApplicantIncomeJT.IncomeAssesmentMethod = $F('txtAssMethodIdForLcJtSalary');
                        if (ApplicantIncomeJT.IncomeAssesmentMethod == "") {
                            ApplicantIncomeJT.IncomeAssesmentMethod = "-1";
                        }
                        ApplicantIncomeJT.IncomeAssesmentCode = $F('txtAssCodeForLCJtSalary');
                        ApplicantIncomeJT.EmployerCategory = $F('txtAssCatIdForLcJtSalary');
                        if (ApplicantIncomeJT.EmployerCategory == "") {
                            ApplicantIncomeJT.EmployerCategory = "-1";
                        }
                        ApplicantIncomeJT.EmployerCategoryCode = $F('txtEmployerCatCodeForLCJtSalary');
                        ApplicantIncomeJT.Consider = $F('cmbConsiderForLCJtSalary');
                        ApplicantIncomeJT.ApplicantType = '2';

                        ApplicantIncomeList.push(ApplicantIncomeJT);
                    }
                } //end if for salary

                // if land loard

                if (i == 8) {
                    if ($F('txtIncomeBForLCJtRent') == '') {

                    }
                    else {
                        ApplicantIncomeJT.AnalystMasterId = $F('txtAnalystMasterID');
                        ApplicantIncomeJT.CalculationSource = i * -1;
                        ApplicantIncomeJT.Income = $F('txtIncomeBForLCJtRent');
                        ApplicantIncomeJT.IncomeAssesmentMethod = $F('txtAssMethodIdForLcJtrent');
                        if (ApplicantIncomeJT.IncomeAssesmentMethod == "") {
                            ApplicantIncomeJT.IncomeAssesmentMethod = "-1";
                        }
                        ApplicantIncomeJT.IncomeAssesmentCode = $F('txtAssCodeForLCJtRent');
                        ApplicantIncomeJT.EmployerCategory = $F('txtAssCatIdForLcJtRent');
                        if (ApplicantIncomeJT.EmployerCategory == "") {
                            ApplicantIncomeJT.EmployerCategory = "-1";
                        }
                        ApplicantIncomeJT.EmployerCategoryCode = $F('txtEmployerCatCodeForLCJtRent');
                        ApplicantIncomeJT.Consider = $F('cmbConsiderForLCJtRent');
                        ApplicantIncomeJT.ApplicantType = '2';

                        ApplicantIncomeList.push(ApplicantIncomeJT);
                    }
                } //end if for land loard

                // if Doctor

                if (i == 9) {
                    if ($F('txtIncomeBForLCJtDoctor') == '') {

                    }
                    else {
                        ApplicantIncomeJT.AnalystMasterId = $F('txtAnalystMasterID');
                        ApplicantIncomeJT.CalculationSource = i * -1;
                        ApplicantIncomeJT.Income = $F('txtIncomeBForLCJtDoctor');
                        ApplicantIncomeJT.IncomeAssesmentMethod = $F('txtAssMethodIdForLcJtDoc');
                        if (ApplicantIncomeJT.IncomeAssesmentMethod == "") {
                            ApplicantIncomeJT.IncomeAssesmentMethod = "-1";
                        }
                        ApplicantIncomeJT.IncomeAssesmentCode = $F('txtAssCodeForLCJtDoctor');
                        ApplicantIncomeJT.EmployerCategory = $F('txtAssCatIdForLcJtDoc');
                        if (ApplicantIncomeJT.EmployerCategory == "") {
                            ApplicantIncomeJT.EmployerCategory = "-1";
                        }
                        ApplicantIncomeJT.EmployerCategoryCode = $F('txtEmployerCatCodeForLCJtDoctor');
                        ApplicantIncomeJT.Consider = $F('cmbConsiderForLCJtDoctor');
                        ApplicantIncomeJT.ApplicantType = '2';

                        ApplicantIncomeList.push(ApplicantIncomeJT);
                    }
                } //end if for Doctor


                // if Teacher

                if (i == 10) {
                    if ($F('txtIncomeBForLCJtTeacher') == '') {

                    }
                    else {
                        ApplicantIncomeJT.AnalystMasterId = $F('txtAnalystMasterID');
                        ApplicantIncomeJT.CalculationSource = i * -1;
                        ApplicantIncomeJT.Income = $F('txtIncomeBForLCJtTeacher');
                        ApplicantIncomeJT.IncomeAssesmentMethod = $F('txtAssMethodIdForLcJtTec');
                        if (ApplicantIncomeJT.IncomeAssesmentMethod == "") {
                            ApplicantIncomeJT.IncomeAssesmentMethod = "-1";
                        }
                        ApplicantIncomeJT.IncomeAssesmentCode = $F('txtAssCodeForLCJtTeacher');
                        ApplicantIncomeJT.EmployerCategory = $F('txtAssCatIdForLcJtTec');
                        if (ApplicantIncomeJT.EmployerCategory == "") {
                            ApplicantIncomeJT.EmployerCategory = "-1";
                        }
                        ApplicantIncomeJT.EmployerCategoryCode = $F('txtEmployerCatCodeForLCJtTeacher');
                        ApplicantIncomeJT.Consider = $F('cmbConsiderForLCJtTeacher');
                        ApplicantIncomeJT.ApplicantType = '2';

                        ApplicantIncomeList.push(ApplicantIncomeJT);
                    }
                } //end if for Teacher

            } //end loop

            // End joint
        }

        return ApplicantIncomeList;
    }


};


var LoanCalculationSaveHelper = {


};