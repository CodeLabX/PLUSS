﻿/// <reference path="../JSLibs/json2.js" />


var category = { 1: 'Corporate', 2: 'Individual' };
var accountType = { 1: 'Current', 2: 'Savings', 3: 'STD', 4: 'Others' };
var Page = {
    primaryBankStatement: null,
    primaryBankStatementArray: [],
    prbankStatementId: 0,
    JbankStatementXML: 0,
    PbankStatementXML: 0,
    branchId: 0,
    userType:0,
    jointBankStatement: null,
    jointBankStatementArray: [],
    jtBankStatementId: 0,

    init: function() {
        util.prepareDom();
        bankstatementManager.getAllBankName();
        bankstatementManager.getUserType();
        Event.observe('btnAddPrimaryApplicant', 'click', bankstatementHelper.PBankStatementAVG);
        Event.observe('btnAddJointApplicant', 'click', bankstatementHelper.JBankStatementAVG);
        Event.observe('btnLoanSearch', 'click', bankstatementManager.SearchLoanDetailsByLLID);
        Event.observe('btnSavePrimaryBankStatement', 'click', bankstatementManager.SaveprimaryBankStatement);
        Event.observe('btnSaveJointBankStatement', 'click', bankstatementManager.SaveJointBankStatement);
        Event.observe('btnPrimaryClear', 'click', bankstatementManager.PrimaryClear);
        Event.observe('btnJointClear', 'click', bankstatementManager.JointClear);

    }
};

var bankstatementManager = {
    getUserType: function() {
        var res = PlussAuto.BankStatement.GetUserType();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            Page.userType = res.value;
            
        }

    },
    getAllBankName: function() {
        var res = PlussAuto.BankStatement.GetBank(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        bankstatementHelper.populateBankCombo(res);
    },

    PrimaryClear: function() {
        Page.primaryBankStatement = null;
        bankstatementHelper.clearPrBankStatementForm();
    },

    JointClear: function() {
        Page.jointBankStatement = null;
        bankstatementHelper.clearJtBankStatementForm();
    },

    GetBranch: function(elementId) {
        var bankId = $F(elementId);
        if (bankId == 0) {
        }
        else {
            var res = PlussAuto.BankStatement.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            bankstatementHelper.populateBranchCombo(res, elementId);
        }
    },

    SearchLoanDetailsByLLID: function() {
        var llid = $F("txtLLID");
        var res = PlussAuto.BankStatement.SearchParantBankStatementInformationByLLID(llid);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            if (res.value.length > 0) {
                $('txtPrimaryApplicant').setValue(res.value[0].PrName);
                $('txtProfession').setValue(res.value[0].PrProfession);
                $('txtDateofBirth').setValue(res.value[0].PrDateOfBirth.format());
                $('txtDeclaredIncome').setValue(res.value[0].PrIncomeAmount);
                $('txtVerifiedAddress').setValue(res.value[0].ResidenceAddress);
                $('txtSecondaryApplicant').setValue(res.value[0].JtName);
                $('txtSecondaryProfession').setValue(res.value[0].Jtprofession);
                $('txtSecondaryDateobBirth').setValue(res.value[0].JtDateOfBirth.format());
                $('txtSecondaryDeclaredIncome').setValue(res.value[0].JtIncomeAmount);
                $('txtAutoLoanMasterId').setValue(res.value[0].Auto_LoanMasterId);
                bankstatementManager.renderprimaryBankStatement(res.value[0].LLID);
                if (res.value[0].JointApplication == 1) {
                    $('JBankStatementDiv').style.display = 'block';
                    $('divJointApplicantDetails').style.display = 'block';
                    bankstatementManager.renderJointBankStatement(res.value[0].LLID);
                    if (Page.userType == 9 || Page.userType == 8) {
                        $('divPrBankStatement').style.display = 'block';
                        $('divJointBankStatement').style.display = 'block';
                    }
                    else {
                        $('divJointBankStatement').style.display = 'none';
                        $('divPrBankStatement').style.display = 'none';
                    }
                }
                else {
                    $('JBankStatementDiv').style.display = 'none';
                    $('divJointApplicantDetails').style.display = 'none';
                    if (Page.userType == 9 || Page.userType == 8) {
                        $('divPrBankStatement').style.display = 'block';
                        $('divJointBankStatement').style.display = 'none';
                    }
                    else {
                        $('divPrBankStatement').style.display = 'none';
                        $('divJointBankStatement').style.display = 'none';
                    }
                }
            }
            else {
                alert("Please input correct LLID");
                $('txtLLID').setValue('');
                bankstatementManager.ClearAll();
                bankstatementManager.renderprimaryBankStatement('0');
                bankstatementManager.renderJointBankStatement('0');
            }
        }
    },

    ClearAll: function() {
        bankstatementHelper.clearPrBankStatementForm();
        bankstatementHelper.clearJtBankStatementForm();
        var spreadsheet = document.getElementById("PBankStatementAVG");
        var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
        spreadsheet.XMLData = blankData;
        var Jspreadsheet = document.getElementById("JBankStatementAVG");
        Jspreadsheet.XMLData = blankData;

        var texts = document.getElementsByTagName('input');
        for (var i_tem = 0; i_tem < texts.length; i_tem++) {
            if (texts[i_tem].type == 'text') {
                texts[i_tem].value = '';
            }
            if (texts[i_tem].type == 'hidden') {
                texts[i_tem].value = '0';
            }
        }

        var combo = document.getElementsByTagName('select');
        for (var i_tems = 0; i_tems < combo.length; i_tems++) {
            combo[i_tems].value = '-1';

        }


    },
    SaveprimaryBankStatement: function() {
        if (bankstatementHelper.ValidPrimaryBankStatement()) {
            //var insurence = Page.primaryBankStatementArray.findByProp('InsCompanyId', insCompanyId);
            //objprimaryBankStatement.PrBankStatementId = $F('PrBankStatementId');
            if (Page.primaryBankStatement == null) {
                Page.primaryBankStatement = new Object();
            }
            var objprimaryBankStatement = Page.primaryBankStatement;
            objprimaryBankStatement.LLID = $F('txtLLID');
            objprimaryBankStatement.AutoLoanMasterId = $F('txtAutoLoanMasterId');
            objprimaryBankStatement.BankId = $F('cmbPApplicantBankName');
            objprimaryBankStatement.BranchId = $F('cmbPApplicantBranchName');
            objprimaryBankStatement.AccountName = $F('txtPApplicantAccountName');
            objprimaryBankStatement.AccountNumber = $F('txtPApplicantAccountNumber');
            objprimaryBankStatement.AccountCategory = $F('cmbPApplicantAccountCategory');
            objprimaryBankStatement.AccountType = $F('cmbPApplicantAccountType');

            objprimaryBankStatement.LastUpdateDate = Date.parseUK(Page.primaryBankStatement.LastUpdateDate);
            objprimaryBankStatement.StatementDateFrom = Date.parseUK(Page.primaryBankStatement.StatementDateFrom);
            objprimaryBankStatement.StatementDateTo = Date.parseUK(Page.primaryBankStatement.StatementDateTo);
            if (Page.PbankStatementXML == "" || Page.PbankStatementXML == null) {
                objprimaryBankStatement.Statement = "";
            }
            else {
                objprimaryBankStatement.Statement = Page.PbankStatementXML;
            }

            var res = PlussAuto.BankStatement.SavePrimaryBankStatement(objprimaryBankStatement);
            if (res.error) {
                alert(res.error.Message);
                return;
            }
            if (res.value == "Success") {
                alert("Primary bank statement Save successfully");
                bankstatementHelper.clearPrBankStatementForm();

            }

            bankstatementManager.renderprimaryBankStatement(objprimaryBankStatement.LLID);

        }
    },
    renderprimaryBankStatement: function(llid) {
        var res = PlussAuto.BankStatement.GetPRBankStatementData(llid);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        bankstatementHelper.renderCollectionForPrimaryBankStatement(res);
    },
    SaveJointBankStatement: function() {
        if (bankstatementHelper.ValidJointBankStatement()) {
            if (Page.jointBankStatement == null) {
                Page.jointBankStatement = new Object();
            }

            var objJointBankStatement = Page.jointBankStatement;
            //            objJointBankStatement.JtBankStatementId = $F('txtjtBankStatementId');
            objJointBankStatement.LLID = $F('txtLLID');
            objJointBankStatement.AutoLoanMasterId = $F('txtAutoLoanMasterId');
            objJointBankStatement.BankId = $F('cmbJApplicantBankName');
            objJointBankStatement.BranchId = $F('cmbJApplicantBranchName');
            objJointBankStatement.AccountName = $F('txtJApplicantAccountName');
            objJointBankStatement.AccountNumber = $F('txtJApplicantAccountNumber');
            objJointBankStatement.AccountCategory = $F('cmbJApplicantAccountCategory');
            objJointBankStatement.AccountType = $F('cmbJApplicantAccountType');
            //if (Page.jointBankStatement.LastUpdateDate != "") {
            objJointBankStatement.LastUpdateDate = Date.parseUK(Page.jointBankStatement.LastUpdateDate);
            //}
            //if (Page.jointBankStatement.StatementDateFrom != "") {
            objJointBankStatement.StatementDateFrom = Date.parseUK(Page.jointBankStatement.StatementDateFrom);
            //}
            //if (Page.jointBankStatement.StatementDateTo != "") {
            objJointBankStatement.StatementDateTo = Date.parseUK(Page.jointBankStatement.StatementDateTo);
            //}

            if (Page.JbankStatementXML == "" || Page.JbankStatementXML == null) {
                objJointBankStatement.Statement = "";
            }
            else {
                objJointBankStatement.Statement = Page.JbankStatementXML;
            }

            var res = PlussAuto.BankStatement.SaveJointBankStatement(objJointBankStatement);
            if (res.error) {
                alert(res.error.Message);
                return;
            }
            if (res.value == "Success") {
                alert("Joint bank statement Save successfully");
                bankstatementHelper.clearJtBankStatementForm();
            }

            bankstatementManager.renderJointBankStatement(objJointBankStatement.LLID);

        }
    },
    renderJointBankStatement: function(llid) {
        var res = PlussAuto.BankStatement.GetJTBankStatementData(llid);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        bankstatementHelper.renderCollectionForJointBankStatement(res);
    }
};
var bankstatementHelper = {
    populateBankCombo: function(res) {


        var extraOption = document.createElement("option");
        extraOption.text = 'Select Bank';
        extraOption.value = '-1';
        document.getElementById('cmbPApplicantBankName').options.length = 0;
        document.getElementById('cmbPApplicantBankName').options.add(extraOption);

        document.getElementById('cmbJApplicantBankName').options.length = 0;
        var extraOption1 = document.createElement("option");
        extraOption1.text = 'Select Bank';
        extraOption1.value = '-1';

        document.getElementById('cmbJApplicantBankName').options.add(extraOption1);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].BankName;
            opt.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBankName').options.add(opt);

            var opt1 = document.createElement("option");
            opt1.text = res.value[i].BankName;
            opt1.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBankName').options.add(opt1);

        }
    },

    PBankStatementAVG: function() {

        try {
            var spreadsheet = document.getElementById("PBankStatementAVG");
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            var prBankstatementID = $F('PrBankStatementId');

            if (Page.primaryBankStatement == null) {
                Page.primaryBankStatement = new Object();
            }
            var objprimaryBankStatement = Page.primaryBankStatement;
            objprimaryBankStatement.LLID = $F('txtLLID');
            objprimaryBankStatement.AutoLoanMasterId = $F('txtAutoLoanMasterId');
            objprimaryBankStatement.BankId = $F('cmbPApplicantBankName');
            objprimaryBankStatement.BranchId = $F('cmbPApplicantBranchName');
            objprimaryBankStatement.AccountName = $F('txtPApplicantAccountName');
            objprimaryBankStatement.AccountNumber = $F('txtPApplicantAccountNumber');
            objprimaryBankStatement.AccountCategory = $F('cmbPApplicantAccountCategory');
            objprimaryBankStatement.AccountType = $F('cmbPApplicantAccountType');

            if (objprimaryBankStatement.LastUpdateDate != "01/01/1901") {
                objprimaryBankStatement.LastUpdateDate = bankstatementHelper.changeToSQLDateFormatForPrintToGrid(objprimaryBankStatement.LastUpdateDate);
                objprimaryBankStatement.StatementDateFrom = bankstatementHelper.changeToSQLDateFormatForPrintToGrid(Page.primaryBankStatement.StatementDateFrom);
                objprimaryBankStatement.StatementDateTo = bankstatementHelper.changeToSQLDateFormatForPrintToGrid(Page.primaryBankStatement.StatementDateTo);
            }
            objprimaryBankStatement.Statement = "";

            objprimaryBankStatement.StatementDateFrom = "";
            objprimaryBankStatement.StatementDateTo = "";


            //            if (objprimaryBankStatement.StatementDateFrom == undefined) {
            //                objprimaryBankStatement.StatementDateFrom = "01/01/1901";
            //            }

            //            if (objprimaryBankStatement.StatementDateTo == undefined) {
            //                objprimaryBankStatement.StatementDateTo = "01/01/1901";
            //            }

            var objPrimarybankStatementjson = JSON.stringify(objprimaryBankStatement);

            var llid = $F('txtLLID');
            var oldBankID = $F('oldBankID');
            var oldAccountNO = $F('oldAccountNO');
            if (XMLHttpRequestObject) {

                XMLHttpRequestObject.open("POST", "BankStatement.aspx?operation=save&llid=" + llid + "&applicantType=primaryAVG&prBID=" + prBankstatementID + "&jsonObj=" + objPrimarybankStatementjson + "&oldBankID=" + oldBankID + "&oldAccountNO=" + oldAccountNO);
                XMLHttpRequestObject.onreadystatechange = function() {
                    if (XMLHttpRequestObject.readyState == 4 &&
                        XMLHttpRequestObject.status == 200) {
                        document.getElementById('PBankStatementAVG').XMLData = spreadsheet.XMLData;

                        //                        alert(XMLHttpRequestObject.responseText);
                    }
                }
                XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                XMLHttpRequestObject.setRequestHeader("Content-length", spreadsheet.XMLData.length);
                XMLHttpRequestObject.send(spreadsheet.XMLData);

                bankstatementManager.renderprimaryBankStatement(objprimaryBankStatement.LLID);
                bankstatementHelper.clearPrBankStatementForm();
                spreadsheet = "";
                alert("Data Saved Successfully.");

            }
        } catch (err) {
        }

        //        bankstatementHelper.PBankStatementCRE();
    },

    JBankStatementAVG: function() {

        try {
            var spreadsheet = document.getElementById("JBankStatementAVG");
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            var jtBankstatementID = $F('txtjtBankStatementId');

            if (Page.jointBankStatement == null) {
                Page.jointBankStatement = new Object();
            }
            var objjointBankStatement = Page.jointBankStatement;
            objjointBankStatement.LLID = $F('txtLLID');
            objjointBankStatement.AutoLoanMasterId = $F('txtAutoLoanMasterId');
            objjointBankStatement.BankId = $F('cmbJApplicantBankName');
            objjointBankStatement.BranchId = $F('cmbJApplicantBranchName');
            objjointBankStatement.AccountName = $F('txtJApplicantAccountName');
            objjointBankStatement.AccountNumber = $F('txtJApplicantAccountNumber');
            objjointBankStatement.AccountCategory = $F('cmbJApplicantAccountCategory');
            objjointBankStatement.AccountType = $F('cmbJApplicantAccountType');

            //            objjointBankStatement.StatementDateFrom = Date.parseUK(Page.primaryBankStatement.StatementDateFrom);
            //            if (objjointBankStatement.StatementDateFrom == '') {
            //                objjointBankStatement.StatementDateTo = Date.parseUK(Page.primaryBankStatement.StatementDateTo);
            //            }
            //            objjointBankStatement.Statement = "";
            //            var objjointBankStatementjson = JSON.stringify(objjointBankStatement);


            if (Page.jointBankStatement.LastUpdateDate != "01/01/1901") {
                objjointBankStatement.LastUpdateDate = bankstatementHelper.changeToSQLDateFormatForPrintToGrid(Page.jointBankStatement.LastUpdateDate); //Page.jointBankStatement.LastUpdateDate;
                objjointBankStatement.StatementDateFrom = bankstatementHelper.changeToSQLDateFormatForPrintToGrid(Page.jointBankStatement.StatementDateFrom); //Date.parseUK(Page.jointBankStatement.StatementDateFrom);
                objjointBankStatement.StatementDateTo = bankstatementHelper.changeToSQLDateFormatForPrintToGrid(Page.jointBankStatement.StatementDateTo);
            }
            objjointBankStatement.Statement = "";

            objjointBankStatement.StatementDateFrom = "";
            objjointBankStatement.StatementDateTo = "";

            var objjointBankStatementjson = JSON.stringify(objjointBankStatement);

            var llid = $F('txtLLID');
            var oldBankIDjT = $F('oldBankIDjT');
            var oldAccountNOjT = $F('oldAccountNOjT');

            if (XMLHttpRequestObject) {

                XMLHttpRequestObject.open("POST", "BankStatement.aspx?operation=save&llid=" + llid + "&applicantType=JointAVG&prBID=" + jtBankstatementID + "&jsonObj=" + objjointBankStatementjson + "&oldBankID=" + oldBankIDjT + "&oldAccountNOjT=" + oldAccountNOjT);
                XMLHttpRequestObject.onreadystatechange = function() {
                    if (XMLHttpRequestObject.readyState == 4 &&
                        XMLHttpRequestObject.status == 200) {
                        document.getElementById('JBankStatementAVG').XMLData = spreadsheet.XMLData;

                        //                        alert(XMLHttpRequestObject.responseText);
                    }
                }
                XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                XMLHttpRequestObject.setRequestHeader("Content-length", spreadsheet.XMLData.length);
                XMLHttpRequestObject.send(spreadsheet.XMLData);

                bankstatementManager.renderJointBankStatement(objjointBankStatement.LLID);
                bankstatementHelper.clearJtBankStatementForm();
                spreadsheet = "";

                alert("Data Saved Successfully.");

            }
        } catch (err) {
        }

        //        bankstatementHelper.PBankStatementCRE();

    },

    changeToSQLDateFormatForPrintToGrid: function(value) {
        dtvalue = value.split('/');
        var datetime = dtvalue[1] + "/" + dtvalue[0] + "/" + dtvalue[2];
        return datetime;
    },

    populateBranchCombo: function(res, elementId) {
        var extraOption = document.createElement("option");
        extraOption.text = 'Select Branch';
        extraOption.value = '-1';
        if (elementId == 'cmbPApplicantBankName') {
            document.getElementById('cmbPApplicantBranchName').options.length = 0;
            document.getElementById('cmbPApplicantBranchName').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranchName').options.add(opt);

            }
            if (Page.branchId != 0) {
                $('cmbPApplicantBranchName').setValue(Page.branchId);
            }

        } else if (elementId == 'cmbJApplicantBankName') {
            document.getElementById('cmbJApplicantBranchName').options.length = 0;
            document.getElementById('cmbJApplicantBranchName').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranchName').options.add(opt);

            }
            if (Page.branchId != 0) {
                $('cmbJApplicantBranchName').setValue(Page.branchId);
            }

        }

    },

    ValidPrimaryBankStatement: function() {
        var msg = false;
        if ($F('txtLLID') == "") {
            alert("Please select LLID");
            return msg;
        }
        if ($F('cmbPApplicantBankName') == "-1") {
            alert("Please select a bank");
            return msg;
        }
        if ($F('txtPApplicantAccountName') == "") {
            alert("Please insert primary account name");
            return msg;
        }
        if ($F('txtPApplicantAccountNumber') == "") {
            alert("Please insert primary account number");
            return msg;
        }
        msg = true;
        return msg;
    },
    ValidJointBankStatement: function() {
        var msg = false;
        if ($F('txtLLID') == "") {
            alert("Please select LLID");
            return msg;
        }
        if ($F('cmbJApplicantBankName') == "-1") {
            alert("Please select a bank");
            return msg;
        }
        if ($F('txtJApplicantAccountName') == "") {
            alert("Please insert joint account name");
            return msg;
        }
        if ($F('txtJApplicantAccountNumber') == "") {
            alert("Please insert joint account number");
            return msg;
        }
        msg = true;
        return msg;
    },
    renderCollectionForPrimaryBankStatement: function(res) {

        Page.primaryBankStatementArray = res.value;
        var html = [];
        for (var i = 0; i < Page.primaryBankStatementArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            var fromdate = Page.primaryBankStatementArray[i].StatementDateFrom.format();
            if (fromdate == "01/01/1901") {
                fromdate = "";
            }
            var todate = Page.primaryBankStatementArray[i].StatementDateTo.format();
            if (todate == "01/01/1901") {
                todate = "";
            }
            Page.primaryBankStatementArray[i].StatementDateFrom = fromdate;
            Page.primaryBankStatementArray[i].StatementDateTo = todate;
            Page.primaryBankStatementArray[i].LastUpdateDate = Page.primaryBankStatementArray[i].LastUpdateDate.format();

            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{BANK_NM}</td><td>#{BRAN_NAME}</td><td>#{AccountNumber}</td><td>#{StatementDateFrom}</td><td>#{StatementDateTo}</td><td><a title="Edit" href="javascript:;" onclick="bankstatementHelper.updatePrimaryBankStatement(#{PrBankStatementId})" class="icon iconEdit"></a> \
                                </td></tr>'
                    .format2((i % 2 ? '' : 'odd'), true)
                    .format(Page.primaryBankStatementArray[i])
            );
        }
        $('bankStatementSummerylist').update(html.join(''));
    },
    updatePrimaryBankStatement: function(id) {
        bankstatementHelper.clearPrBankStatementForm();
        var prbankdetails = Page.primaryBankStatementArray.findByProp('PrBankStatementId', id);
        Page.primaryBankStatement = prbankdetails || { id: util.uniqId() };
        if (prbankdetails) {
            $('cmbPApplicantBankName').setValue(prbankdetails.BankId);
            $('oldBankID').setValue(prbankdetails.BankId);

            $('PrBankStatementId').setValue(prbankdetails.PrBankStatementId);
            Page.branchId = prbankdetails.BranchId;
            bankstatementManager.GetBranch('cmbPApplicantBankName');
            $('txtPApplicantAccountName').setValue(prbankdetails.AccountName);
            $('txtPApplicantAccountNumber').setValue(prbankdetails.AccountNumber);
            $('oldAccountNO').setValue(prbankdetails.AccountNumber);

            $('cmbPApplicantAccountCategory').setValue(prbankdetails.AccountCategory);
            $('cmbPApplicantAccountType').setValue(prbankdetails.AccountType);
            var spreadsheet = document.getElementById("PBankStatementAVG");
            var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
            spreadsheet.XMLData = blankData;
            if (prbankdetails.Statement == "") {
                Page.PbankStatementXML = prbankdetails.Statement;
            }
            else {
                spreadsheet.XMLData = prbankdetails.Statement;
                Page.PbankStatementXML = prbankdetails.Statement;
            }
            //            $('PBankStatementAVG').setValue(prbankdetails.Statement);

        }
        if (Page.userType == 9 || Page.userType == 8) {
            $('btnSavePrimaryBankStatement').style.display = 'block';
        }
        else {
            $('btnSavePrimaryBankStatement').style.display = 'none';
        }


    },
    clearPrBankStatementForm: function() {

        $('cmbPApplicantBankName').setValue('-1');
        $('oldBankID').setValue('0');
        $('PrBankStatementId').setValue('0');
        Page.branchId = 0;
        $('cmbPApplicantBranchName').setValue('-1');
        $('txtPApplicantAccountName').setValue('');
        $('txtPApplicantAccountNumber').setValue('');
        $('oldAccountNO').setValue('');
        $('cmbPApplicantAccountCategory').setValue('-1');
        $('cmbPApplicantAccountType').setValue('-1');
        Page.PbankStatementXML = "";
        Page.primaryBankStatement = null;

        var spreadsheet = document.getElementById("PBankStatementAVG");
        var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
        spreadsheet.XMLData = blankData;
        $('btnSavePrimaryBankStatement').style.display = 'block';

        //Page.prbankStatementId = 0;

    },

    clearJtBankStatementForm: function() {
        $('cmbJApplicantBankName').setValue('-1');
        $('oldBankIDjT').setValue('0');
        $('txtjtBankStatementId').setValue('0');
        Page.branchId = 0;
        $('cmbJApplicantBranchName').setValue('-1');
        $('txtJApplicantAccountName').setValue('');
        $('txtJApplicantAccountNumber').setValue('');
        $('oldAccountNOjT').setValue('');
        $('cmbJApplicantAccountCategory').setValue('-1');
        $('cmbJApplicantAccountType').setValue('-1');
        Page.JbankStatementXML = "";
        Page.jointBankStatement = null;

        var spreadsheet = document.getElementById("JBankStatementAVG");
        var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
        spreadsheet.XMLData = blankData;

        $('btnSaveJointBankStatement').style.display = 'block';

        if (Page.userType == 9 || Page.userType == 8) {
            $('btnSaveJointBankStatement').style.display = 'block';
        }
        else {
            $('btnSaveJointBankStatement').style.display = 'none';
        }


        //Page.jtBankStatementId = 0;
    },
    renderCollectionForJointBankStatement: function(res) {
        Page.jointBankStatementArray = res.value;
        var html = [];
        for (var i = 0; i < Page.jointBankStatementArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            var fromdate = Page.jointBankStatementArray[i].StatementDateFrom.format();
            if (fromdate == "01/01/1901") {
                fromdate = "";
            }
            var todate = Page.jointBankStatementArray[i].StatementDateTo.format();
            if (todate == "01/01/1901") {
                todate = "";
            }
            Page.jointBankStatementArray[i].StatementDateFrom = fromdate;
            Page.jointBankStatementArray[i].StatementDateTo = todate;
            Page.jointBankStatementArray[i].LastUpdateDate = Page.jointBankStatementArray[i].LastUpdateDate.format();

            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{BANK_NM}</td><td>#{BRAN_NAME}</td><td>#{AccountNumber}</td><td>#{StatementDateFrom}</td><td>#{StatementDateTo}</td><td><a title="Edit" href="javascript:;" onclick="bankstatementHelper.updateJointBankStatement(#{JtBankStatementId})" class="icon iconEdit"></a> \
                                </td></tr>'
                    .format2((i % 2 ? '' : 'odd'), true)
                    .format(Page.jointBankStatementArray[i])
            );
        }
        $('bankStatementSummerylistforJointApplicant').update(html.join(''));
    },
    renderCollectionForJointBankStatement1: function(res) {
        Page.jointBankStatementArray = res.value;
        var html = [];
        for (var i = 0; i < Page.jointBankStatementArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            //Page.insurenceArray[i].InsTypeName = InsurenceType[Page.insurenceArray[i].InsType];
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{BankName}</td><td>#{BranchName}</td><td>#{AccountNumber}</td><td><a title="Edit" href="javascript:;" onclick="bankStatementHelper.updateJointBankStatement(#{JtBankStatementId})" class="icon iconEdit"></a> \
                                    </td></tr>'
                    .format2((i % 2 ? '' : 'odd'), true)
                    .format(Page.jointBankStatementArray[i])
            );
        }
        $('bankStatementSummerylistforJointApplicant').update(html.join(''));
    },
    updateJointBankStatement: function(id) {
        bankstatementHelper.clearJtBankStatementForm();
        var jtbankdetails = Page.jointBankStatementArray.findByProp('JtBankStatementId', id);
        Page.jointBankStatement = jtbankdetails || { id: util.uniqId() };
        if (jtbankdetails) {
            $('cmbJApplicantBankName').setValue(jtbankdetails.BankId);
            $('oldBankIDjT').setValue(jtbankdetails.BankId);

            $('txtjtBankStatementId').setValue(jtbankdetails.JtBankStatementId);
            Page.branchId = jtbankdetails.BranchId;
            bankstatementManager.GetBranch('cmbJApplicantBankName');
            $('txtJApplicantAccountName').setValue(jtbankdetails.AccountName);
            $('txtJApplicantAccountNumber').setValue(jtbankdetails.AccountNumber);
            $('oldAccountNOjT').setValue(jtbankdetails.AccountNumber);

            $('cmbJApplicantAccountCategory').setValue(jtbankdetails.AccountCategory);
            $('cmbJApplicantAccountType').setValue(jtbankdetails.AccountType);
            var spreadsheet = document.getElementById("JBankStatementAVG");
            var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
            spreadsheet.XMLData = blankData;
            if (jtbankdetails.Statement == "") {
                Page.JbankStatementXML = jtbankdetails.Statement;
            }
            else {
                spreadsheet.XMLData = jtbankdetails.Statement;
                Page.JbankStatementXML = jtbankdetails.Statement;
            }
            //            $('PBankStatementAVG').setValue(prbankdetails.Statement);


        }
    }
};


    Event.onReady(Page.init);