﻿var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    negativeVendor: null,
    negativevendorArray: [],
    vendorArray: [],
    isNew:false,

    init: function() {
        util.prepareDom();
        Event.observe('lnkAddnegativeVendor', 'click', negativeVendorHelper.update);
        Event.observe('lnkSave', 'click', negativeVendorManager.save);
        // Event.observe('cboManufacturer', 'change', negativeVendorHelper.changeManufacture);
        //Event.observe('cmbModel', 'change', negativeVendorHelper.changeModel);
        //Event.observe('btnSearch', 'click', negativeVendorManager.render);
        Event.observe('lnkClose', 'click', util.hideModal.curry('Vendor_Negative_ListingPopupDiv'));

        //Event.observe('lnkUploadSave', 'click', Page.upload);

        // Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Event.observe('lnkNewVendore', 'click', negativeVendorHelper.newVendore);
        Event.observe('lnkHide', 'click', negativeVendorHelper.hideNewVendore);
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });
        negativeVendorManager.render();
        negativeVendorManager.getVendor();
        negativeVendorManager.getStatus();

    },
    goToNextPage: function(pageno) {
       
        Page.pageNo = pageno;
        negativeVendorManager.render();
    }

};

var negativeVendorManager = {
    render: function() {

        var searchParam = "";

        var res = PlussAuto.VendorNegativeListing.GetAutoVendorNegativeList(Page.pageNo, Page.pageSize, searchParam);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        negativeVendorHelper.renderCollection(res);
    },
    getVendor: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.VendorNegativeListing.GetAutoVendor();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.vendorArray = res.value;

        negativeVendorHelper.populateVendorCombo(res);

    },
    getStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.VendorNegativeListing.GetStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        negativeVendorHelper.populateStatusCombo(res);
    },

    save: function() {

        debugger;

        var manufacturerId = $F('cmbVendor');

        var res = "";
        if (Page.isNew == false) {
            if (!util.validateEmpty('cmbVendor')) {
                alert("Select a Vendor");
                return false;
            }
            if ($F('cmbVendor') == "-1") {
                alert("Select a Vendor");
                return false;
            }
            res = PlussAuto.VendorNegativeListing.SaveAuto_NegativeVendor(manufacturerId);
        }
        else {
            if (!util.validateEmpty('txtVendorName')) {
                alert("Please Insert a Vendore Name");
                return false;
            }
            var IRWithARTA = $F('txtIRWithARTA');
            if (!util.isFloat(IRWithARTA)) {
                alert("IR with arta must be a numaric field");
                return false;
            }

            if (!util.validateEmpty('cmbVehicleStatus') || $F('cmbVehicleStatus') == "-1") {
                alert("Please select a Dealer Status");
                return false;
            }
            if (!util.validateEmpty('cmbIsMou') || $F('cmbIsMou') == "-1") {
                alert("Please select Mou Settings");
                return false;
            }


            var a = $F('txtIRWithoutARTA');
            if (!util.isFloat(a)) {
                alert("IR without arta must be a numaric field");
                return false;
            }

            var AVendorMOU = new Object();

            AVendorMOU.VendorName = $F('txtVendorName');
            AVendorMOU.DealerType = $F('cmbVehicleStatus'); // dealar Type
            AVendorMOU.IsMou = $F('cmbIsMou');
            AVendorMOU.Address = $F('txtAddress');
            AVendorMOU.phone = $F('txtPhone');
            AVendorMOU.Website = $F('txtWebsite');

            //AVendorMOU.AutoVendorId = $F('cmbVehicleStatus');
            AVendorMOU.IrWithArta = $F('txtIRWithARTA');
            if (AVendorMOU.IrWithArta == "") {
                AVendorMOU.IrWithArta = 0;
            }
            AVendorMOU.IrWithoutArta = $F('txtIRWithoutARTA');
            if (AVendorMOU.IrWithoutArta == "") {
                AVendorMOU.IrWithoutArta = 0;
            }
            AVendorMOU.ProcessingFee = $F('txtProcessingFee');
            if (AVendorMOU.ProcessingFee == "") {
                AVendorMOU.ProcessingFee = 0;
            }
            AVendorMOU.ValidFrom = Date.parseUK($F('txtValidFrom'));
            AVendorMOU.LastUpdateDate = AVendorMOU.ValidFrom;

            res = PlussAuto.VendorNegativeListing.SaveAuto_NegativeVendorNew(AVendorMOU);
        }


        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("New Negative Vendor Added successfully");
        }

        negativeVendorManager.render();
        negativeVendorManager.getVendor();
        util.hideModal('Vendor_Negative_ListingPopupDiv');




    },



    TanorLtvDeleteById: function(vendornegativeID) {
        var res = PlussAuto.VendorNegativeListing.VendorNegativeById(vendornegativeID);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Neagive Vendor Delete successfully");
        }

        negativeVendorManager.render();
    }

};

var negativeVendorHelper = {
    renderCollection: function(res) {
        Page.negativevendorArray = res.value;
        var html = [];
        for (var i = 0; i < Page.negativevendorArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            Page.negativevendorArray[i]._LastupdateDate = Page.negativevendorArray[i].ListingDate.format();
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{VendorName}</td><td>#{_LastupdateDate}</td><td>\
                            <a title="Delete" href="javascript:;" onclick="negativeVendorHelper.remove(#{NegativeVendorId})" class="icon iconDelete"></a></td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.negativevendorArray[i])
			     );
        }
        $('vendornegativelisting').update(html.join(''));
        var totalCount = 0;

        if (res.value.length != 0) {
            totalCount = Page.negativevendorArray[0].Totalcount;
        }
        Page.pager.reset(totalCount, Page.pageNo);
    },

    update: function(vendornegativeID) {
        Page.isNew = false;
        Page.vehicleId = 0;
        $$('#Vendor_Negative_ListingPopupDiv .txt').invoke('clear');
        ////        negativeVendorHelper.clearForm();
        ////        var tenor = Page.negativevendorArray.findByProp('vendornegativeID', vendornegativeID);
        ////        Page.negativeVendor = tenor || { vendornegativeID: util.uniqId() };
        ////        if (tenor) {
        ////            $('cboManufacturer').setValue(tenor.ManufacturerId);
        ////            Page.vehicleId = tenor.VehicleId;
        ////            negativeVendorManager.getModelbyManufacturerID(tenor.ManufacturerId);

        ////            $('txtManufacturingCountry').setValue(tenor.ManufacturCountry);


        ////            //tanorHel.changeModel();
        ////            $('txtTrimLevel').setValue(tenor.TrimLevel);
        ////            $('txtCC').setValue(tenor.CC);
        ////            $('cmbVehicleStatus').setValue(tenor.StatusId);
        ////            $('txtTenor').setValue(tenor.Tenor);
        ////            $('txtLTV').setValue(tenor.LTV);

        ////        }

        $('cmbVendor').setValue('-1');

        util.showModal('Vendor_Negative_ListingPopupDiv');
    },


    remove: function(vendornegativeID) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            negativeVendorManager.TanorLtvDeleteById(vendornegativeID);
            negativeVendorManager.getVendor();
        }
    },
    populateVendorCombo: function(res) {
        document.getElementById('cmbVendor').options.length = 0;
        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].VendorName;
            opt.value = res.value[i].AutoVendorId;
            document.getElementById('cmbVendor').options.add(opt);
        }
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVendor').options.add(ExtraOpt);


    },
    populateStatusCombo: function(res) {
        document.getElementById('cmbVehicleStatus').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmbVehicleStatus').options.add(opt);
        }
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVehicleStatus').options.add(ExtraOpt);
    },
    
    //////    clearForm: function() {
    //////        $('cboManufacturer').setValue('');
    //////        $('txtManufacturingCountry').setValue('');
    //////        $('cmbModel').setValue('');
    //////        $('txtTrimLevel').setValue('');
    //////        $('txtCC').setValue('');
    //////        $('cmbVehicleStatus').setValue('');
    //////        $('txtTenor').setValue('');
    //////        $('txtLTV').setValue('');
    //////    },




    searchKeypress: function(e) {

        //debugger;
        if (event.keyCode == 13) {
            negativeVendorManager.render();
        }
    },
    newVendore: function() {
        $('newVendore').style.display = 'block';
        $('vendoreList').style.display = 'none';
        document.getElementById("Vendor_Negative_ListingPopupDiv").style.height = "370px";
        Page.isNew = true;
    },
    hideNewVendore: function() {
    $('newVendore').style.display = 'none';
    $('vendoreList').style.display = 'block';
    document.getElementById("Vendor_Negative_ListingPopupDiv").style.height = "150px";
    Page.isNew = false;
    }

};
Event.onReady(Page.init);