﻿var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    LoanSummeryArray: [],
    init: function() {

    util.prepareDom();
    Event.observe('btnLLIdSearch', 'click', analystSummaryManager.getLoanSummarySearch);
    Event.observe('cmbStatus', 'change', analystSummaryManager.getLoanSummarySearch);
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNo'),
            spnTotalPage: $('spntotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });

        analystSummaryManager.getLoanSummary();
        analystSummaryManager.loadState();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        analystSummaryManager.getLoanSummary(Page.pageNo, Page.pageSize);
    }
};

var analystSummaryManager = {
    getLoanSummary: function() {
        var searchKey = $F('txtLLIDMain');
        if (searchKey == "") {
            searchKey = 0;
        }

        var status = $F("cmbStatus");
        searchKey = searchKey + "^" + status;

        //if (util.isDigit(searchKey)) {
            var res = PlussAuto.AnalystSummary.GetLoanSummary(Page.pageNo, Page.pageSize, searchKey);
            //debugger;
            if (res.error) {
                alert(res.error.Message);
                return;
            }
            Page.LoanSummeryArray = res.value;

            analystSummaryHelper.populateLoanSummeryGrid(res);
//        } else {
//            alert("LLID Must to be in Degit");
//            return false;
//        }
    },
    loadState: function() {
        var res = PlussAuto.AnalystSummary.loadState();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analystSummaryHelper.populateStatus(res);
    },
    getLoanSummarySearch: function() {
        Page.pageNo = 0;
        analystSummaryManager.getLoanSummary();
    },
    searchKeypress: function(e) {
        if (e.keyCode == 13) {
            analystSummaryManager.getLoanSummary();
        }
    }

};
var analystSummaryHelper = {
    populateLoanSummeryGrid: function(rss) {
        var html = [];
        for (var i = 0; i < rss.value.length; i++) {
            // var serialNo = i + 1;
            //            <a title="Appeal" href="javascript:;" onclick="loanSummeryHelper.replayPopupShow(#{Auto_LoanMasterId})" class="icon iconReplay"></a>
            var btnWillBe = "";
            if (Page.LoanSummeryArray[i].StatusID == 42 || Page.LoanSummeryArray[i].StatusID == 44 || Page.LoanSummeryArray[i].StatusID == 21 || Page.LoanSummeryArray[i].StatusID == 24) {
                btnWillBe = '<input title="Process" type="button" id="btnAppeal" class="searchButton   widthSize100_per" value="Process" onclick=" analystSummaryHelper.inprocessOfllId(' + Page.LoanSummeryArray[i].Auto_LoanMasterId + ')"/>';
            }
            else if (Page.LoanSummeryArray[i].StatusID == 42 || Page.LoanSummeryArray[i].StatusID == 7 || Page.LoanSummeryArray[i].StatusID == 30 || Page.LoanSummeryArray[i].StatusID == 31|| Page.LoanSummeryArray[i].StatusID == 32 || Page.LoanSummeryArray[i].StatusID == 18 || Page.LoanSummeryArray[i].StatusID == 20 || Page.LoanSummeryArray[i].StatusID == 22 || Page.LoanSummeryArray[i].StatusID == 23 || Page.LoanSummeryArray[i].StatusID == 25 || Page.LoanSummeryArray[i].StatusID == 26 || Page.LoanSummeryArray[i].StatusID == 28 || Page.LoanSummeryArray[i].StatusID == 33 || Page.LoanSummeryArray[i].StatusID == 34 || Page.LoanSummeryArray[i].StatusID == 35) {
                btnWillBe = '<input title="Open" type="button" id="btnPrintBA" class="searchButton   widthSize100_per" value="Open" onclick=" analystSummaryHelper.SetLLIDAndSearch(' + Page.LoanSummeryArray[i].LLID + ')"/>';
            }
            var className = i % 2 ? '' : 'odd';
            Page.LoanSummeryArray[i].btnWillBe = btnWillBe;
            html.push('<tr class="' + className + '"><td>#{LLID}</td><td>#{PrName}</td><td>#{ProductName}</td><td>#{ManufacturerName}</td><td>#{Model}</td><td>#{AppliedAmount}</td><td>#{AskingTenor}</td><td>#{StatusName}</td><td>#{btnWillBe}</td></tr>'
                        .format2((i % 2 ? '' : 'odd'), true)
                        .format(Page.LoanSummeryArray[i])
                );
        }
        $('GridLoanSummery').update(html.join(''));
        var totalCount = 0;

        if (rss.value.length != 0) {
            totalCount = Page.LoanSummeryArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);

    },
    SetLLIDAndSearch: function(llid) {
        var url = "../../UI/AutoLoan/AnalystDetails.aspx?llid=" + llid;
        location.href = url;
    },
    inprocessOfllId: function(llid) {
        var res = PlussAuto.AnalystSummary.AppealedByAutoLoanMasterId(llid);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Process Succeed");
            analystSummaryManager.getLoanSummary();
        } else {
            alert("Process not Succeeded");
        }
    },
    populateStatus: function(res) {
        document.getElementById('cmbStatus').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Default';
        ExtraOpt.value = '-1';
        document.getElementById('cmbStatus').options.add(ExtraOpt);
        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].WorkFlowID;
            document.getElementById('cmbStatus').options.add(opt);
        }
    }
};

Event.onReady(Page.init);