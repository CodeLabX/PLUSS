﻿
var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    autoSegmentSetting: null,
    autoSegmentSettingArray: [],
    segmentID: 0,

    init: function() {
        util.prepareDom();
        Event.observe('lnkAddNewSegment', 'click', autoSegmentSettingHelper.update);
        //        Event.observe('btnUploadDoc', 'click', autoSegmentSettingHelper.showUploader);
        //        //        Event.observe('lnkUploadSave', 'click', autoSegmentSettingManager.save);
        //        Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('uploadPopup'));
        Event.observe('lnkSave', 'click', autoSegmentSettingManager.save);
        Event.observe('lnkClose', 'click', autoSegmentSettingHelper.close);


        //    Event.observe('lnkUploadSave', 'click', Page.upload);

        //    Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });
        autoSegmentSettingManager.render();
        autoSegmentSettingManager.getProfession();

    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        autoSegmentSettingManager.render();
    }

};

var autoSegmentSettingManager = {

    render: function() {
        //        var comapanyName = $F('txtSearch');
        var res = PlussAuto.SegmentSettings.GetSegmentSettingsSummary(Page.pageNo, Page.pageSize);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        autoSegmentSettingHelper.renderCollection(res);
    },

    getProfession: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.SegmentSettings.GetProfession();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        autoSegmentSettingHelper.populateProfessionCombo(res);
    },


    save: function() {
        if (!util.validateEmpty('txtSegmentName')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtSegmentCode')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('cmbProfession')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }


        //        var a = $F('cmbProfession').value;

        var Segment = new Object();

        //        AutoVehicleStatusIR.IrWithArta = $F('txtIRWithARTA');

        Segment.SegmentName = $F('txtSegmentName');
        Segment.SegmentCode = $F('txtSegmentCode');
        Segment.Description = $F('txtDescription');

        //        Segment.Profession = $F('cmbProfession');
        if ($F('cmbProfession') == '-1') {
            alert('Please Select a Profession');
            return;
        }
        else {
            Segment.Profession = $F('cmbProfession');
        }


        if ($F('txtMinLoanAmount') == '') {
            Segment.MinLoanAmt = '0';
        }
        else {
            Segment.MinLoanAmt = $F('txtMinLoanAmount');
            if (!util.isFloat(Segment.MinLoanAmt)) {
                alert("Please Insert number field");
                $('txtMinLoanAmount').setValue('');
                $("txtMinLoanAmount").focus();
                return false;
            }
        }
        if ($F('txtMaxLoanAmount') == '') {
            Segment.MaxLoanAmt = '0';
        }
        else {
            Segment.MaxLoanAmt = $F('txtMaxLoanAmount');
            if (!util.isFloat(Segment.MaxLoanAmt)) {
                alert("Please Insert number field");
                $('txtMaxLoanAmount').setValue('');
                $("txtMaxLoanAmount").focus();
                return false;
            }
        }
        if ($F('txtMinTenor') == '') {
            Segment.MinTanor = '0';
        }
        else {
            Segment.MinTanor = $F('txtMinTenor');
            if (!util.isFloat(Segment.MinTanor)) {
                alert("Please Insert number field");
                $('txtMinTenor').setValue('');
                $("txtMinTenor").focus();
                return false;
            }
        }
        if ($F('txtMaxTenor') == '') {
            Segment.MaxTanor = '0';
        }
        else {
            Segment.MaxTanor = $F('txtMaxTenor');
            if (!util.isFloat(Segment.MaxTanor)) {
                alert("Please Insert number field");
                $('txtMaxTenor').setValue('');
                $("txtMaxTenor").focus();
                return false;
            }
        }
        if ($F('txtIRWithARTA') == '') {
            Segment.IrWithARTA = '0';
        }
        else {
            Segment.IrWithARTA = $F('txtIRWithARTA');
            if (!util.isFloat(Segment.IrWithARTA)) {
                alert("Please Insert number field");
                $('txtIRWithARTA').setValue('');
                $("txtIRWithARTA").focus();
                return false;
            }
        }
        if ($F('txtIRWithoutARTA') == '') {
            Segment.IrWithoutARTA = '0';
        }
        else {
            Segment.IrWithoutARTA = $F('txtIRWithoutARTA');
            if (!util.isFloat(Segment.IrWithoutARTA)) {
                alert("Please Insert number field");
                $('txtIRWithoutARTA').setValue('');
                $("txtIRWithoutARTA").focus();
                return false;
            }
        }
        if ($F('txtMinAgeL1') == '') {
            Segment.MinAgeL1 = '0';
        }
        else {
            Segment.MinAgeL1 = $F('txtMinAgeL1');
            if (!util.isFloat(Segment.MinAgeL1)) {
                alert("Please Insert number field");
                $('txtMinAgeL1').setValue('');
                $("txtMinAgeL1").focus();
                return false;
            }
        }
        if ($F('txtMinAgeL2') == '') {
            Segment.MinAgeL2 = '0';
        }
        else {
            Segment.MinAgeL2 = $F('txtMinAgeL2');
            if (!util.isFloat(Segment.MinAgeL2)) {
                alert("Please Insert number field");
                $('txtMinAgeL2').setValue('');
                $("txtMinAgeL2").focus();
                return false;
            }
        }
        if ($F('txtMaxAgeL1') == '') {
            Segment.MaxAgeL1 = '0';
        }
        else {
            Segment.MaxAgeL1 = $F('txtMaxAgeL1');
            if (!util.isFloat(Segment.MaxAgeL1)) {
                alert("Please Insert number field");
                $('txtMaxAgeL1').setValue('');
                $("txtMaxAgeL1").focus();
                return false;
            }
        }
        if ($F('txtMaxAgeL2') == '') {
            Segment.MaxAgeL2 = '0';
        }
        else {
            Segment.MaxAgeL2 = $F('txtMaxAgeL2');
            if (!util.isFloat(Segment.MaxAgeL2)) {
                alert("Please Insert number field");
                $('txtMaxAgeL2').setValue('');
                $("txtMaxAgeL2").focus();
                return false;
            }
        }
        if ($F('txtLTV1L1') == '') {
            Segment.LTVL1 = '0';
        }
        else {
            Segment.LTVL1 = $F('txtLTV1L1');
            if (!util.isFloat(Segment.LTVL1)) {
                alert("Please Insert number field");
                $('txtLTV1L1').setValue('');
                $("txtLTV1L1").focus();
                return false;
            }
        }
        if ($F('txtLTVL2') == '') {
            Segment.LTVL2 = '0';
        }
        else {
            Segment.LTVL2 = $F('txtLTVL2');
            if (!util.isFloat(Segment.LTVL2)) {
                alert("Please Insert number field");
                $('txtLTVL2').setValue('');
                $("txtLTVL2").focus();
                return false;
            }
        }
        if ($F('txtExperienceL1') == '') {
            Segment.ExperienceL1 = '0';
        }
        else {
            Segment.ExperienceL1 = $F('txtExperienceL1');
            if (!util.isFloat(Segment.ExperienceL1)) {
                alert("Please Insert number field");
                $('txtExperienceL1').setValue('');
                $("txtExperienceL1").focus();
                return false;
            }
        }
        if ($F('txtExperienceL2') == '') {
            Segment.ExperienceL2 = '0';
        }
        else {
            Segment.ExperienceL2 = $F('txtExperienceL2');
            if (!util.isFloat(Segment.ExperienceL2)) {
                alert("Please Insert number field");
                $('txtExperienceL2').setValue('');
                $("txtExperienceL2").focus();
                return false;
            }
        }
        if ($F('txtACRelationshipL1') == '') {
            Segment.AcRelationshipL1 = '0';
        }
        else {
            Segment.AcRelationshipL1 = $F('txtACRelationshipL1');
            if (!util.isFloat(Segment.AcRelationshipL1)) {
                alert("Please Insert number field");
                $('txtAcRelationshipL1').setValue('');
                $("txtAcRelationshipL1").focus();
                return false;
            }
        }
        if ($F('txtACRelationshipL2') == '') {
            Segment.AcRelationshipL2 = '0';
        }
        else {
            Segment.AcRelationshipL2 = $F('txtACRelationshipL2');
            if (!util.isFloat(Segment.AcRelationshipL2)) {
                alert("Please Insert number field");
                $('txtACRelationshipL2').setValue('');
                $("txtACRelationshipL2").focus();
                return false;
            }
        }
        if ($F('txtCriteriaCode') == '') {
            Segment.CriteriaCode = '0';
        }
        else {
            Segment.CriteriaCode = $F('txtCriteriaCode');
            if (!util.isFloat(Segment.CriteriaCode)) {
                alert("Please Insert number field");
                $('txtCriteriaCode').setValue('');
                $("txtCriteriaCode").focus();
                return false;
            }
        }
        if ($F('txtAssetCode') == '') {
            Segment.AssetCode = '0';
        }
        else {
            Segment.AssetCode = $F('txtAssetCode');
            if (!util.isFloat(Segment.AssetCode)) {
                alert("Please Insert number field");
                $('txtAssetCode').setValue('');
                $("txtAssetCode").focus();
                return false;
            }
        }
        if ($F('txtMinDBRL1') == '') {
            Segment.MinDBRL1 = '0';
        }
        else {
            Segment.MinDBRL1 = $F('txtMinDBRL1');
            if (!util.isFloat(Segment.MinDBRL1)) {
                alert("Please Insert number field");
                $('txtMinDBRL1').setValue('');
                $("txtMinDBRL1").focus();
                return false;
            }
        }
        if ($F('txtMinDBRL2') == '') {
            Segment.MinDBRL2 = '0';
        }
        else {
            Segment.MinDBRL2 = $F('txtMinDBRL2');
            if (!util.isFloat(Segment.MinDBRL2)) {
                alert("Please Insert number field");
                $('txtMinDBRL2').setValue('');
                $("txtMinDBRL2").focus();
                return false;
            }
        }
        if ($F('txtMaxDBRL2') == '') {
            Segment.MaxDBRL2 = '0';
        }
        else {
            Segment.MaxDBRL2 = $F('txtMaxDBRL2');
            if (!util.isFloat(Segment.MaxDBRL2)) {
                alert("Please Insert number field");
                $('txtMaxDBRL2').setValue('');
                $("txtMaxDBRL2").focus();
                return false;
            }
        }
        if ($F('txtDBRL3') == '') {
            Segment.DBRL3 = '0';
        }
        else {
            Segment.DBRL3 = $F('txtDBRL3');
            if (!util.isFloat(Segment.DBRL3)) {
                alert("Please Insert number field");
                $('txtDBRL3').setValue('');
                $("txtDBRL3").focus();
                return false;
            }
        }


        if ($F('cmbFieldVisit') == '1') {
            Segment.FieldVisit = true;
        }
        else {
            Segment.FieldVisit = false;
        }

        if ($F('cmbVerification') == '1') {
            Segment.Verification = true;
        }
        else {
            Segment.Verification = false;
        }
        if ($F('cmbLandTelephone') == '1') {
            Segment.LandTelephone = true;
        }
        else {
            Segment.LandTelephone = false;
        }
        if ($F('cmbGuarantee') == '1') {
            Segment.GuranteeReference = true;
        }
        else {
            Segment.GuranteeReference = false;
        }
        //        Segment.FieldVisit = true; //$F('cmbFieldVisit');
        //        Segment.Verification = true; //$F('cmbVerification');
        //        Segment.LandTelephone = true; //$F('cmbLandTelephone');
        //        Segment.GuranteeReference = true; //$F('cmbGuarantee');
        if ($F('txtPrimaryMinIncomeL1') == '') {
            Segment.PrimaryMinIncomeL1 = '0';
        }
        else {
            Segment.PrimaryMinIncomeL1 = $F('txtPrimaryMinIncomeL1');
            if (!util.isFloat(Segment.PrimaryMinIncomeL1)) {
                alert("Please Insert number field");
                $('txtPrimaryMinIncomeL1').setValue('');
                $("txtPrimaryMinIncomeL1").focus();
                return false;
            }
        }
        if ($F('txtPrimaryMinIncomeL2') == '') {
            Segment.PrimaryMinIncomeL2 = '0';
        }
        else {
            Segment.PrimaryMinIncomeL2 = $F('txtPrimaryMinIncomeL2');
            if (!util.isFloat(Segment.PrimaryMinIncomeL2)) {
                alert("Please Insert number field");
                $('txtPrimaryMinIncomeL2').setValue('');
                $("txtPrimaryMinIncomeL2").focus();
                return false;
            }
        }
        if ($F('txtJointMinIncomeL1') == '') {
            Segment.JointMinIncomeL1 = '0';
        }
        else {
            Segment.JointMinIncomeL1 = $F('txtJointMinIncomeL1');
            if (!util.isFloat(Segment.JointMinIncomeL1)) {
                alert("Please Insert number field");
                $('txtJointMinIncomeL1').setValue('');
                $("txtJointMinIncomeL1").focus();
                return false;
            }
        }
        if ($F('txtJointMinIncomeL2') == '') {
            Segment.JointMinIncomeL2 = '0';
        }
        else {
            Segment.JointMinIncomeL2 = $F('txtJointMinIncomeL2');
            if (!util.isFloat(Segment.JointMinIncomeL2)) {
                alert("Please Insert number field");
                $('txtJointMinIncomeL2').setValue('');
                $("txtJointMinIncomeL2").focus();
                return false;
            }
        }
        if ($F('txtComments') == '') {
            Segment.Comments = '';
        }
        else {
            Segment.Comments = $F('txtComments');
        }

        //        Segment.UserId = $F('txtUserId');



        var res = PlussAuto.SegmentSettings.saveSegment(Segment, Page.segmentID);
        //        alert(res.error.Message);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Segment Save successfully");
            autoSegmentSettingHelper.close();
            autoSegmentSettingManager.render();
        }
        if (res.value == "Already exist") {
            alert("Already added this segment code");
        }

        
        //        util.hideModal('companyPopupDiv');





    },
    InactiveSegment: function(segmentId) {
    var res = PlussAuto.SegmentSettings.InactiveSegmentById(segmentId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Insurance and Banca Delete successfully");
        }

        autoSegmentSettingManager.render();
    }
};
var autoSegmentSettingHelper = {

    renderCollection: function(res) {
        Page.autoSegmentSettingArray = res.value;
        var html = [];
        for (var i = 0; i < Page.autoSegmentSettingArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            //Page.insurenceArray[i].InsTypeName = InsurenceType[Page.insurenceArray[i].InsType];
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{SegmentName}</td><td>#{ProfName}</td><td>#{MaxInterestRate}</td>\
            <td>#{MinLoanAmt}</td><td>#{MaxLoanAmt}</td><td>#{MinTanor}</td><td>#{MaxTanor}</td><td>#{MinAgeL1}</td><td>#{MaxAgeL1}</td>\
            <td>#{PrimaryMinIncomeL1}</td><td>#{PrimaryMinIncomeL2}</td>\
            <td><a title="Edit" href="javascript:;" onclick="autoSegmentSettingHelper.update(#{SegmentID})" class="icon iconEdit"></a> \
            <a title="Delete" href="javascript:;" onclick="autoSegmentSettingHelper.remove(#{SegmentID})" class="icon iconDelete"></a>\
                            </td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.autoSegmentSettingArray[i])
			     );
        }
        $('SegmentGrid').update(html.join(''));
        var totalCount = 0;
        if (res.value.length != 0) {
            totalCount = Page.autoSegmentSettingArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);
    },
    update: function(segmentID) {
        $("mainDiv").style.display = 'block';
        $("divSegmentSummary").style.display = 'none';
        $$('#mainDiv .txt').invoke('clear');
        autoSegmentSettingHelper.clearForm();
        var segment = Page.autoSegmentSettingArray.findByProp('SegmentID', segmentID);
        Page.autoSegmentSetting = segment || { segmentID: util.uniqId() };
        Page.segmentID = segmentID;
        if (segment) {
            $('txtSegmentCode').setValue(segment.SegmentCode);
            $('txtSegmentName').setValue(segment.SegmentName);
            $('txtSegmentCode').setValue(segment.SegmentCode);
            $('txtDescription').setValue(segment.Description);

            $('cmbProfession').setValue(segment.Profession);
            $('txtMinLoanAmount').setValue(segment.MinLoanAmt);
            $('txtMaxLoanAmount').setValue(segment.MaxLoanAmt);
            $('txtMinTenor').setValue(segment.MinTanor);
            $('txtMaxTenor').setValue(segment.MaxTanor);
            $('txtIRWithARTA').setValue(segment.IrWithARTA);
            $('txtIRWithoutARTA').setValue(segment.IrWithoutARTA);
            $('txtMinAgeL1').setValue(segment.MinAgeL1);
            $('txtMinAgeL2').setValue(segment.MinAgeL2);
            $('txtMaxAgeL1').setValue(segment.MaxAgeL1);
            $('txtMaxAgeL2').setValue(segment.MaxAgeL2);
            $('txtLTV1L1').setValue(segment.LTVL1);
            $('txtLTVL2').setValue(segment.LTVL2);
            $('txtExperienceL1').setValue(segment.ExperienceL1);
            $('txtExperienceL2').setValue(segment.ExperienceL2);
            $('txtACRelationshipL1').setValue(segment.AcRelationshipL1);
            $('txtACRelationshipL2').setValue(segment.AcRelationshipL2);
            $('txtCriteriaCode').setValue(segment.CriteriaCode);
            $('txtAssetCode').setValue(segment.AssetCode);
            $('txtMinDBRL1').setValue(segment.MinDBRL1);
            $('txtMinDBRL2').setValue(segment.MinDBRL2);
            $('txtMaxDBRL2').setValue(segment.MaxDBRL2);
            $('txtDBRL3').setValue(segment.DBRL3);


            if (!segment.FieldVisit) {
                $('cmbFieldVisit').setValue('0');
            }
            else {
                $('cmbFieldVisit').setValue('1');
            }

            if (!segment.Verification) {
                $('cmbVerification').setValue('0');
            }
            else {
                $('cmbVerification').setValue('1');
            }

            if (!segment.LandTelephone) {
                $('cmbLandTelephone').setValue('0');
            }
            else {
                $('cmbLandTelephone').setValue('1');
            }
            if (!segment.GuranteeReference) {
                $('cmbGuarantee').setValue('0');
            }
            else {
                $('cmbGuarantee').setValue('1');
            }

            $('txtPrimaryMinIncomeL1').setValue(segment.PrimaryMinIncomeL1);
            $('txtPrimaryMinIncomeL2').setValue(segment.PrimaryMinIncomeL2);
            $('txtJointMinIncomeL1').setValue(segment.JointMinIncomeL1);
            $('txtJointMinIncomeL2').setValue(segment.JointMinIncomeL2);
            $('txtComments').setValue(segment.Comments);
        }
        else {
            //            $('cmbCategory').setValue('-1');
            Page.segmentID = '0';
        }

        //        util.showModal('companyPopupDiv');
    },
    remove: function(segmentId) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            autoSegmentSettingManager.InactiveSegment(segmentId);
        }
    },
    clearForm: function() {
        $('txtSegmentName').setValue('');
        $('txtSegmentCode').setValue('');
        $('txtDescription').setValue('');

        $('cmbProfession').setValue('-1');
        $('txtMinLoanAmount').setValue('');
        $('txtMaxLoanAmount').setValue('');
        $('txtMinTenor').setValue('');
        $('txtMaxTenor').setValue('');
        $('txtIRWithARTA').setValue('');
        $('txtIRWithoutARTA').setValue('');
        $('txtMinAgeL1').setValue('');
        $('txtMinAgeL2').setValue('');
        $('txtMaxAgeL1').setValue('');
        $('txtMaxAgeL2').setValue('');
        $('txtLTV1L1').setValue('');
        $('txtLTVL2').setValue('');
        $('txtExperienceL1').setValue('');
        $('txtExperienceL2').setValue('');
        $('txtACRelationshipL1').setValue('');
        $('txtACRelationshipL2').setValue('');
        $('txtCriteriaCode').setValue('');
        $('txtAssetCode').setValue('');
        $('txtMinDBRL1').setValue('');
        $('txtMinDBRL2').setValue('');
        $('txtMaxDBRL2').setValue('');
        $('txtDBRL3').setValue('');

        $('txtPrimaryMinIncomeL1').setValue('');
        $('txtPrimaryMinIncomeL2').setValue('');
        $('txtJointMinIncomeL1').setValue('');
        $('txtJointMinIncomeL2').setValue('');
        $('txtComments').setValue('');
    },
    //    searchKeypress: function(e) {
    //        if (event.keyCode == 13) {
    //            autoSegmentSettingManager.render();
    //        }
    //    }

    populateProfessionCombo: function(res) {
        document.getElementById('cmbProfession').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ProfName;
            opt.value = res.value[i].ProfID;
            document.getElementById('cmbProfession').options.add(opt);
        }

        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '-1';
        document.getElementById('cmbProfession').options.add(ExtraOpt);
        $('cmbProfession').setValue('-1');

    },
    close: function() {
        autoSegmentSettingHelper.clearForm();
        $("mainDiv").style.display = 'none';
        $("divSegmentSummary").style.display = 'block';
    }




};

Event.onReady(Page.init);
