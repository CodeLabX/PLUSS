﻿/// <reference path="../activatables.js" />



var AnalystDetailsCalculationHelper = {
    populateDataByLLId: function(res) {

        //General Tab Start----------------------------------------------------
        //Primary Applicant


        AnalystDetailsCalculationHelper.populatePrimaryApplicantData(res);

        //Check Joint Applicant
        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        if (isJoint == true) {
            $('divForJointApplicant').style.display = 'block';
            $('tblJointAlertVerification').style.display = 'block';
            //Populate Joint Applicant Data
            $("lblTotaljointIncome").innerHTML = "TOTAL JOINT INCOME:";
            Page.isjoint = true;
            Page.jointPrimaryProfessionId = res.value.objAutoJTProfession.PrimaryProfession;

            AnalystDetailsCalculationHelper.populateJointApplicantData(res);
        }
        else {
            $("lblTotaljointIncome").innerHTML = "TOTAL INCOME:";
            $('divForJointApplicant').style.display = 'none';
        }

        //Age Validation
        var currentAgeForPrimary = parseInt($F("txtCurrenAge"));
        Page.age = currentAgeForPrimary;
        var currentAgeForJoin = parseInt($F("txtJtCurrntAge"));
        Page.jointage = currentAgeForJoin;
        $("lblFieldrequiredforPrAge").innerHTML = "";
        if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && currentAgeForJoin < 65 && isJoint == true) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
            $('cmbLabelForDlaLimit').setValue('2');
            //var cmbLabelForDlaLimit11 = document.getElementById('cmbLabelForDlaLimit');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbLabelForDlaLimit11, '2');


        }
        if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && isJoint == false) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
            $('cmbLabelForDlaLimit').setValue('2');
            //var cmbLabelForDlaLimit11 = document.getElementById('cmbLabelForDlaLimit');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbLabelForDlaLimit11, '2');


        }
        if (currentAgeForJoin > 65 && currentAgeForJoin < 69 && currentAgeForPrimary < 65 && isJoint == true) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";
            $('cmbLabelForDlaLimit').setValue('2');
            //var cmbLabelForDlaLimit11 = document.getElementById('cmbLabelForDlaLimit');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbLabelForDlaLimit11, '2');


        }
        if ((currentAgeForPrimary > 68 || currentAgeForJoin > 68) && isJoint == true) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 3 required";
            $('cmbLabelForDlaLimit').setValue('3');
            //var cmbLabelForDlaLimit11 = document.getElementById('cmbLabelForDlaLimit');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbLabelForDlaLimit11, '3');

        }
        if (currentAgeForPrimary > 68 && isJoint == false) {
            $("lblFieldrequiredforPrAge").innerHTML = "Level 3 required";
            $('cmbLabelForDlaLimit').setValue('3');
            //var cmbLabelForDlaLimit11 = document.getElementById('cmbLabelForDlaLimit');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbLabelForDlaLimit11, '3');

        }

        //Applicant Details

        AnalystDetailsCalculationHelper.populateApplicantDetails(res);

        //Vehicle Status Validataion
        $('cmdVehicleStatus').setValue(res.value.objAutoLoanVehicle.VehicleStatus);
        //var cmdVehicleStatus11 = document.getElementById('cmdVehicleStatus');
        //AnalystDetailsCalculationHelper.selectItemByValue(cmdVehicleStatus11, res.value.objAutoLoanVehicle.VehicleStatus);

        if (res.value.objAutoLoanVehicle.VehicleStatus == 3) {
            $('cmbCarVerification').setValue('2');
            //var cmbCarVerification11 = document.getElementById('cmbCarVerification');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbCarVerification11, '2');

        }
        else {
            $('cmbCarVerification').setValue(res.value.objAutoLoanVehicle.CarVerification);
            //var cmbCarVerification11 = document.getElementById('cmbCarVerification');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbCarVerification11, res.value.objAutoLoanVehicle.CarVerification);

            if (res.value.objAutoLoanVehicle.CarVerification == 1) {
                $('divLblDeliveryStatus').style.display = 'block';
                $('divDeliveryStatus').style.display = 'block';
            }
            else {
                $('divLblDeliveryStatus').style.display = 'none';
                $('divDeliveryStatus').style.display = 'none';
            }
        }

        Page.jtCount = res.value.AutoJTBankStatementList.length;
        Page.prCount = res.value.AutoPRBankStatementList.length;

        var primaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var otherProfession = res.value.objAutoPRProfession.OtherProfession;

        Page.primaryProfessionId = primaryProfession;
        Page.otherProfessionId = otherProfession;

        //Create Tab by Type
        AnalystDetailsCalculationHelper.CreateTabbyType(res);


        //Vehicle Details

        AnalystDetailsCalculationHelper.populateVehicleDetails(res);

        //SCB Account Details
        AnalystDetailsCalculationHelper.populateScbAccountDetails(res);

        //Other Account Details
        AnalystDetailsCalculationHelper.populateOtherAccountDetails(res);



        //Auto Step Execution User
        AnalystDetailsCalculationHelper.populateStepExecutionUser(res);

        //Populate Salaried
        AnalystDetailsCalculationHelper.populateSalaried(res);

        //Populate LandLord
        AnalystDetailsCalculationHelper.populateLandLord(res);

        //Populate Exposer
        //Populate Facility On Scb

        AnalystDetailsCalculationHelper.populateFacilityOnScb(res);

        //Populate Sequrity On Scb
        AnalystDetailsCalculationHelper.populateSequrityOnScb(res);

        $('cmbColleterization').setValue(res.value.AutoLoanAnalystMaster.Colleterization);
        $('cmbColleterizationWith').setValue(res.value.AutoLoanAnalystMaster.ColleterizationWith);

        //Populate CIB Information
        AnalystDetailsCalculationHelper.populateCibInfo(res);


        //Populate Facility Off Scb
        AnalystDetailsCalculationHelper.populateFacilityOffScb(res);

        //Populate Sequrity Off Scb
        //AnalystDetailsCalculationHelper.populateSequrityOffScb(res);

        //Create existing Repayment Method
        analystDetailsHelper.createExistingrepaymentGrid();

        Page.bankStatementArray = res.value.AutoBankStatementAvgTotalList;

        //Populate Bank Statement Data

        AnalystDetailsCalculationHelper.populateBankStatementData(res);

        //Populated Edited Data For Analyst
        AnalystDetailsCalculationHelper.populateIncomBankStatementSum(res);
        //General Tab End------------------------------

        //Loan Calculator Tab Start
        //Auto An Income Segment Applicant Income List
        AnalystDetailsCalculationHelper.populateSegmentApplicantIncomelist(res);

        //Populate Segment Income
        AnalystDetailsCalculationHelper.populateSegmentIncome(res);

        //Analyst Loan Calculator Total
        AnalystDetailsCalculationHelper.populateTotalLoanAmount(res);

        //populate Analyst Loan Calculator Insurence Total
        AnalystDetailsCalculationHelper.populateInsurenceTotal(res);

        //Populate Analyst Loan Amount Total
        AnalystDetailsCalculationHelper.populateAnalystLoanAmountTotal(res);

        //Populate Analyst Interest Rate
        AnalystDetailsCalculationHelper.populateAnalystLoanInterestRate(res);

        //Populate Analyst Tenor Rate
        AnalystDetailsCalculationHelper.populateAnalystLoanTenorRate(res);

        //Populate Analyst Repayment Capability
        AnalystDetailsCalculationHelper.populateAnalystRepaymentCapability(res);


        //Loan Calculator Tab End

        //Finalization Tab Start
        //Populate Deviation
        AnalystDetailsCalculationHelper.populateDeviation(res);

        //Calculate Repayment Method for Finalization Form DB
        AnalystDetailsCalculationHelper.calculateRepaymentForFinalize();

        //Populate Deviation found
        AnalystDetailsCalculationHelper.populateDeviationFound(res);

        //Populate Auto Finalization Strength
        AnalystDetailsCalculationHelper.populateFinalizationStrength(res);

        //Populate Auto Finalization Weakness
        AnalystDetailsCalculationHelper.populateFinalizationWeakness(res);

        //Populate Auto Finalization Condition
        AnalystDetailsCalculationHelper.populateFinalizationCondition(res);

        //Populate Auto Finalization Remarks
        AnalystDetailsCalculationHelper.populateFinalizationRemarks(res);

        //Populate Auto Finalization Repayment Method
        AnalystDetailsCalculationHelper.populateFinalizationRepaymentMethod(res);

        //Populate Auto Finalization Required Document
        AnalystDetailsCalculationHelper.populateFinalizationRequiredDocument(res);

        //Populate Auto Finalization Verification Report
        AnalystDetailsCalculationHelper.populateFinalizationVerificationReport(res);

        //Populate Auto Finalization Appraiser and Approver
        AnalystDetailsCalculationHelper.populateFinalizationAppraiserAndApprover(res);

        //Populate Alert verification report
        AnalystDetailsCalculationHelper.Auto_An_AlertVerificationReport(res.value.objAuto_An_AlertVerificationReport);

        //Populate Finalization Rework
        AnalystDetailsCalculationHelper.populateFinalizationRework(res);

        //Populate Finalization Cib Request
        AnalystDetailsCalculationHelper.populateFinalizationCibRequest(res);
        analystDetailsHelper.changeInstrumentForFinalizeTab();
        //Finalization Tab End
        if (isJoint == true) {
            analystDetailsHelper.showJointApplicant();
        }

        //        var a = document.getElementById('cmbVendor');
        //        a.selectedIndex = 20;
        //$('cmbVendor').setValue('148');
    },

    selectItemByValue: function(elmnt, value) {

        for (var i = 0; i < elmnt.options.length; i++) {
            if (elmnt.options[i].value == value) {
                elmnt.selectedIndex = i;
                break;
            }
        }
    },


    //Primary Applicant Data
    populatePrimaryApplicantData: function(res) {

        $('cmbLabelForDlaLimit').setValue('1');
        //var cmbLabelForDlaLimit11 = document.getElementById('cmbLabelForDlaLimit');
        //AnalystDetailsCalculationHelper.selectItemByValue(cmbLabelForDlaLimit11, '1');

        $('txtPrProfessionId').setValue(res.value.objAutoPRProfession.PrimaryProfession);
        $('txtPrOtherProfessionId').setValue(res.value.objAutoPRProfession.OtherProfession);
        $('txtPrimaryApplicant').setValue(res.value.objAutoPRApplicant.PrName);
        $('txtDateOfBirth').setValue(res.value.objAutoPRApplicant.DateofBirth.format());
        $('txtCurrenAge').setValue(res.value.objAutoPRApplicant.Age);
        $('txtPrimaryProfession').setValue(res.value.objAutoPRProfession.PrimaryProfessionName);
        $('txtOtherProfession').setValue(res.value.objAutoPRProfession.OtherProfessionName);
        $('txtDeclaredIncome').setValue(res.value.objAutoPRFinance.DPIAmount);
        $('txtDeclaredIncomeForLcAll').setValue(res.value.objAutoPRFinance.DPIAmount);
        $('txtVerifiedAddress').setValue(res.value.objAutoPRApplicant.ResidenceAddress);
    },

    //Joint Applicant Data
    populateJointApplicantData: function(res) {
        $('txtJointApplicant').setValue(res.value.objAutoJTApplicant.JtName);
        $('txtJtDateOfBirth').setValue(res.value.objAutoJTApplicant.DateofBirth.format());
        $('txtJtCurrntAge').setValue(res.value.objAutoJTApplicant.Age);
        $('txtJtProfession').setValue(res.value.objAutoJTProfession.JtPrimaryProfessionName);
        $('txtJtDeclaredIncome').setValue(res.value.objAutoJTFinance.DPIAmount);
        $('txtDeclaredIncomeForLcJointAll').setValue(res.value.objAutoJTFinance.DPIAmount);
        $('txtJtProfessionId').setValue(res.value.objAutoJTProfession.PrimaryProfession);
        $('analystMasterIdHiddenField').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);
    },

    //Applicant Details
    populateApplicantDetails: function(res) {

        if (res.value.objAutoLoanVehicle.ValuePackAllowed == false) {
            $('cmbValuePack').setValue('0');
            //var cmbValuePack11 = document.getElementById('cmbValuePack');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbValuePack11, '0');

        }
        else {
            $('cmbValuePack').setValue('1');
            //var cmbValuePack11 = document.getElementById('cmbValuePack');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbValuePack11, '1');

        }
        $('cmbProductName').setValue(res.value.objAutoLoanMaster.ProductId);
        //var cmbProductName11 = document.getElementById('cmbProductName');
        //AnalystDetailsCalculationHelper.selectItemByValue(cmbProductName11, res.value.objAutoLoanMaster.ProductId);

        $('txtAskingtenor').setValue(res.value.objAutoLoanMaster.AskingTenor);
        $('txtAskingTenorForLc').setValue(res.value.objAutoLoanMaster.AskingTenor);
        $('txtAskingInterest').setValue(res.value.objAutoLoanMaster.AskingRate);
        $('txtInterestLCAskingRate').setValue(res.value.objAutoLoanMaster.AskingRate);

        if (res.value.objAutoLoanMaster.ProductId == 3) {
            //labelChangeHelper.showFinanceLabelForAnalyst();
        }
        else {
            //labelChangeHelper.showLoanLabelForAnalyst();
            if (res.value.objAutoLoanMaster.ProductId == 1) {
                $('cmbARTA').setValue('2');
                //var cmbARTA11 = document.getElementById('cmbARTA');
                //AnalystDetailsCalculationHelper.selectItemByValue(cmbARTA11, '2');

                $('cmbGeneralInsurence').setValue('2');
                //var cmbGeneralInsurence11 = document.getElementById('cmbGeneralInsurence');
                //AnalystDetailsCalculationHelper.selectItemByValue(cmbGeneralInsurence11, '2');

                $('txtAppropriteRate').setValue('4.43');
                //$('cmbARTA').disabled = 'false';
                //$('cmbGeneralInsurence').disabled = 'false';
                //$('cmbARTAReference').disabled = 'false';
            }
            else {
                $('cmbARTA').disabled = '';
                $('cmbGeneralInsurence').disabled = '';
                $('cmbARTAReference').disabled = '';
            }

        }

        //If Analyst Details Exist then
        if (res.value.AutoLoanAnalystApplication != null) {
            AnalystDetailsCalculationHelper.populateAnalystDetails(res);
        }


    },
    //Populate Analyst Details
    populateAnalystDetails: function(res) {

        if (res.value.AutoLoanAnalystApplication != null) {
            $('txtAutoLoanMasterId').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);
            $('tAnalystRemark').setValue(res.value.AutoLoanAnalystMaster.AnalystRemark);
            $('txtAnalystMasterID').setValue(res.value.AutoLoanAnalystMaster.AnalystMasterId);
            $('txtloanAnalystApplicationID').setValue(res.value.AutoLoanAnalystApplication.Id);
            $('txtVehicleDetailID').setValue(res.value.AutoLoanAnalystVehicleDetail.Id);
            if (res.value.objAutoLoanMaster.JointApplication) {
                $('txtIsJoint').setValue('1');
            } else {
                $('txtIsJoint').setValue('0');
            }
            $('cmbSource1').setValue(res.value.AutoLoanAnalystApplication.Source);

            //var cmbSource11 = document.getElementById('cmbSource1');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbSource11, res.value.AutoLoanAnalystApplication.Source);
            Page.branchNameForSource = res.value.AutoLoanAnalystApplication.BranchId;
            analystDetailsManager.changeBranchNameForSourceOnApplicantDetailsForGeneral();
            //$('cmbBranch').setValue(res.value.AutoLoanAnalystApplication.BranchId);
            //var cmbBranch11 = document.getElementById('cmbBranch');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbBranch11, res.value.AutoLoanAnalystApplication.BranchId);

            $('txtBranchCode').setValue(res.value.AutoLoanAnalystApplication.BranchCode);
            $('cmbValuePack').setValue(res.value.AutoLoanAnalystApplication.ValuePack);
            //var cmbValuePack11 = document.getElementById('cmbValuePack');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbValuePack11, res.value.AutoLoanAnalystApplication.ValuePack);

            $('cmbBrowingRelationship').setValue(res.value.AutoLoanAnalystApplication.BorrowingRelationShip);
            //var cmbBrowingRelationship11 = document.getElementById('cmbBrowingRelationship');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbBrowingRelationship11, res.value.AutoLoanAnalystApplication.BorrowingRelationShip);

            $('txtRelationshipCode').setValue(res.value.AutoLoanAnalystApplication.BorrowingRelationShipCode);
            $('cmbCustomerStatus').setValue(res.value.AutoLoanAnalystApplication.CustomerStatus);
            //var cmbCustomerStatus11 = document.getElementById('cmbCustomerStatus');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbCustomerStatus11, res.value.AutoLoanAnalystApplication.CustomerStatus);


            $('cmbAskingRepaymentmethod').setValue(res.value.AutoLoanAnalystApplication.AskingRepaymentMethod);
            //var cmbAskingRepaymentmethod11 = document.getElementById('cmbAskingRepaymentmethod');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbAskingRepaymentmethod11, res.value.AutoLoanAnalystApplication.AskingRepaymentMethod);

            $('cmbAskingRepaymentmethodForFinalize').setValue(askingrepayment[res.value.AutoLoanAnalystApplication.AskingRepaymentMethod]);
            //var cmbAskingRepaymentmethodForFinalize11 = document.getElementById('cmbAskingRepaymentmethodForFinalize');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbAskingRepaymentmethodForFinalize11, askingrepayment[res.value.AutoLoanAnalystApplication.AskingRepaymentMethod]);

            $('txtInstrumentForFinalizetab').setValue(res.value.AutoLoanAnalystApplication.AskingRepaymentMethod);
            $('cmbARTA').setValue(res.value.AutoLoanAnalystApplication.ARTA);
            //var cmbARTA11 = document.getElementById('cmbARTA');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbARTA11, res.value.AutoLoanAnalystApplication.ARTA);

            $('cmbARTAReference').setValue(res.value.AutoLoanAnalystApplication.ARTAStatus);
            if (res.value.AutoLoanAnalystApplication.ARTAStatus == -1) {
                $('cmbARTA').disabled = 'false';
            }
            else {
                $('cmbARTA').disabled = '';
            }


            //var cmbARTAReference11 = document.getElementById('cmbARTAReference');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbARTAReference11, res.value.AutoLoanAnalystApplication.ARTAStatus);

            $('cmbGeneralInsurence').setValue(res.value.AutoLoanAnalystApplication.GeneralInsurance);
            //var cmbGeneralInsurence11 = document.getElementById('cmbGeneralInsurence');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbGeneralInsurence11, res.value.AutoLoanAnalystApplication.GeneralInsurance);

            $('cmbGeneralInsurencePreference').setValue(res.value.AutoLoanAnalystApplication.GeneralInsurancePreference);
            if (res.value.AutoLoanAnalystApplication.GeneralInsurancePreference == -1) {
                $('cmbGeneralInsurence').disabled = 'false';
            }
            else {
                $('cmbGeneralInsurence').disabled = '';
            }
            if (res.value.AutoLoanAnalystApplication.CashSecured100Per == false) {
                $('cmbCashSecured').setValue('2');
            }
            else {
                $('cmbCashSecured').setValue('1');
            }

            //var cmbCashSecured11 = document.getElementById('cmbCashSecured');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbCashSecured11, res.value.AutoLoanAnalystApplication.CashSecured100Per);
            if (res.value.AutoLoanAnalystApplication.CCBundle == false) {
                $('cmbCCBundle').setValue('2');
            }
            else {
                $('cmbCCBundle').setValue('1');
            }
            //var cmbCCBundle11 = document.getElementById('cmbCCBundle');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbCCBundle11, res.value.AutoLoanAnalystApplication.CCBundle);

            if (res.value.AutoLoanAnalystApplication.CashSecured100Per == 1) {
                $('divSecurityValue').style.display = "block";
                $('divSequrityvaluetextbox').style.display = "block";
                $('divCashForLc').style.display = "block";
                $('txtSecurityValue').setValue(res.value.AutoLoanAnalystApplication.SecurityValue);
            }
            else {
                $('divSecurityValue').style.display = "none";
                $('divSequrityvaluetextbox').style.display = "none";
                $('divCashForLc').style.display = "none";
                $('txtSecurityValue').setValue('0');
            }

        }

    },

    //Vehicle Details
    populateVehicleDetails: function(res) {


        $('cmbManufacturer').setValue(res.value.objAutoLoanVehicle.Auto_ManufactureId);
        var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;
        //var cmbManufacturer11 = document.getElementById('cmbManufacturer');
        //AnalystDetailsCalculationHelper.selectItemByValue(cmbManufacturer11, res.value.objAutoLoanVehicle.Auto_ManufactureId);

        $('txtAutoLoanVehicleId').setValue(res.value.objAutoLoanVehicle.AutoLoanVehicleId);
        Page.vehicleDetailsArray = res.value.objAutoLoanVehicle;
        Page.vehicleId = res.value.objAutoLoanVehicle.VehicleId;
        analystDetailsHelper.changeManufacture();
        $('cmbModel').setValue(res.value.objAutoLoanVehicle.VehicleId);
        //var cmbModel11 = document.getElementById('cmbModel');
        //AnalystDetailsCalculationHelper.selectItemByValue(cmbModel11, res.value.objAutoLoanVehicle.VehicleId);
        if (manufacturName != "Other") {
            analystDetailsHelper.changeModel();
        }
        $('hdnModel').setValue(res.value.objAutoLoanVehicle.Model);
        $('txtVehicleType').setValue(res.value.objAutoLoanVehicle.VehicleType);
        $('txtManufacturaryear').setValue(res.value.objAutoLoanVehicle.ManufacturingYear);
        $("lblManufactureYearAlert").innerHTML = "";
        if (res.value.objAutoLoanVehicle.ManufacturingYear < 2007) {
            //alert("Car Import Docs Required");

            $("lblManufactureYearAlert").innerHTML = "Car Import Docs Required";
            $('divmanuyearalerttext').style.display = "block";
            $('divmanufacalertlbl').style.display = "block";
        }
        else {
            $('divmanuyearalerttext').style.display = "none";
            $('divmanufacalertlbl').style.display = "none";
        }

        $('txtEngineCapacityCC').setValue(res.value.objAutoLoanVehicle.EngineCC);
        $('cmsSeatingCapacity').setValue(res.value.objAutoLoanVehicle.SeatingCapacity);
        $('cmbVendor').setValue(res.value.objAutoLoanVehicle.VendorId);
        //var cmbVendor11 = document.getElementById('cmbVendor');
        //AnalystDetailsCalculationHelper.selectItemByValue(cmbVendor11, res.value.objAutoLoanVehicle.VendorId);


        var vendoreId = $F("cmbVendor");
        if (vendoreId == "-1") {
            $('txtVendorName').disabled = '';
        }
        else {
            var vdr = Page.vendoreArray.findByProp('AutoVendorId', vendoreId);
            if (vdr) {
                $('txtVendoreCode').setValue(vdr.VendorCode);
                $('txtVendorName').disabled = 'false';
            }
        }

        $('txtQuotedPrice').setValue(res.value.objAutoLoanVehicle.QuotedPrice);
        $('txtConsideredprice').setValue(res.value.objAutoLoanVehicle.VerifiedPrice);
        $('cmbPriceConsideredOn').setValue(res.value.objAutoLoanVehicle.PriceConsideredBasedOn);
        //var cmbPriceConsideredOn11 = document.getElementById('cmbPriceConsideredOn');
        //AnalystDetailsCalculationHelper.selectItemByValue(cmbPriceConsideredOn11, res.value.objAutoLoanVehicle.PriceConsideredBasedOn);

        $('txtVehicleStatusCode').setValue(res.value.objAutoLoanVehicle.StatusCode);
        $('txtManufacturarCode').setValue(res.value.objAutoLoanVehicle.ManufacturerCode);
        $('txtVehicleModelCode').setValue(res.value.objAutoLoanVehicle.ModelCode);
        $('txtVehicletypeCode').setValue(res.value.objAutoLoanVehicle.VehicleTypeCode);
        $('txtVendoreCode').setValue(res.value.objAutoLoanVehicle.VendorCode);
        if (res.value.objAutoLoanVehicle.VendoreRelationshipStatus != 1 && res.value.objAutoLoanVehicle.VendoreRelationshipStatus != 0) {
            alert("The Vendore is Negative Listed");
            $('cmbVendoreRelationShip').setValue('2');
            //var cmbVendoreRelationShip11 = document.getElementById('cmbVendoreRelationShip');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbVendoreRelationShip11, '2');

        }
        else {
            $('cmbVendoreRelationShip').setValue(res.value.objAutoLoanVehicle.VendoreRelationshipStatus);
            //var cmbVendoreRelationShip11 = document.getElementById('cmbVendoreRelationShip');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbVendoreRelationShip11, res.value.objAutoLoanVehicle.VendoreRelationshipStatus);

        }

        Page.tenor = res.value.objAutoLoanVehicle.Tenor;
        //if Auto Analyst vehicle Details Exit then
        if (res.value.AutoLoanAnalystVehicleDetail != null) {
            AnalystDetailsCalculationHelper.populateAutoAnalystVehicleDetails(res);
        }

    },

    //Populate Auto Analyst Vehicle Details
    populateAutoAnalystVehicleDetails: function(res) {
        if (res.value.AutoLoanAnalystVehicleDetail != null) {
            $('cmbvaluePackAllowed').setValue(res.value.AutoLoanAnalystVehicleDetail.valuePackAllowed);
            //var cmbvaluePackAllowed11 = document.getElementById('cmbvaluePackAllowed');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbvaluePackAllowed11, res.value.AutoLoanAnalystVehicleDetail.valuePackAllowed);

            $('cmsSeatingCapacity').setValue(res.value.AutoLoanAnalystVehicleDetail.SeatingCapacity);
            //var cmsSeatingCapacity11 = document.getElementById('cmsSeatingCapacity');
            //.selectItemByValue(cmsSeatingCapacity11, res.value.AutoLoanAnalystVehicleDetail.SeatingCapacity);

            $('txtConsideredprice').setValue(res.value.AutoLoanAnalystVehicleDetail.ConsideredPrice);
            $('cmbPriceConsideredOn').setValue(res.value.AutoLoanAnalystVehicleDetail.PriceConsideredOn);
            //var cmbPriceConsideredOn11 = document.getElementById('cmbPriceConsideredOn');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbPriceConsideredOn11, res.value.AutoLoanAnalystVehicleDetail.PriceConsideredOn);

            $('cmbCarVerification').setValue(res.value.AutoLoanAnalystVehicleDetail.CarVarification);
            //var cmbCarVerification11 = document.getElementById('cmbCarVerification');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbCarVerification11, res.value.AutoLoanAnalystVehicleDetail.CarVarification);

            analystDetailsHelper.changeCarVerification();
            $('cmbDeliveryStatus').setValue(res.value.AutoLoanAnalystVehicleDetail.DeliveryStatus);
            //var cmbDeliveryStatus11 = document.getElementById('cmbDeliveryStatus');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbDeliveryStatus11, res.value.AutoLoanAnalystVehicleDetail.DeliveryStatus);

            $('txtregistrationYear').setValue(res.value.AutoLoanAnalystVehicleDetail.RegistrationYear);
        }
    },

    //Populate SCB Account Details
    populateScbAccountDetails: function(res) {
        var scbAccount = "<option value=\"-1\">Account Number</option>";
        if (res.value.AutoApplicantSCBAccountList.length > 0) {
            for (var i = 0; i < res.value.AutoApplicantSCBAccountList.length; i++) {
                var idfieldscbaccount = (i + 1);
                if (idfieldscbaccount < 6) {
                    $('txtAccountNumberForSCB' + idfieldscbaccount).setValue(res.value.AutoApplicantSCBAccountList[i].AccountNumberSCB);
                    $('cmbAccountStatusForSCB' + idfieldscbaccount).setValue(res.value.AutoApplicantSCBAccountList[i].AccountStatus);
                    //var cmbAccountStatusForSCB11 = document.getElementById('cmbAccountStatusForSCB' + idfieldscbaccount);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbAccountStatusForSCB11, res.value.AutoApplicantSCBAccountList[i].AccountStatus);

                    $('cmbPriorityAcHolderForSCB' + idfieldscbaccount).setValue(res.value.AutoApplicantSCBAccountList[i].PriorityHolder);
                    //var cmbPriorityAcHolderForSCB11 = document.getElementById('cmbPriorityAcHolderForSCB' + idfieldscbaccount);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbPriorityAcHolderForSCB11, res.value.AutoApplicantSCBAccountList[i].PriorityHolder);

                    scbAccount += "<option value=\"" + res.value.AutoApplicantSCBAccountList[i].AccountNumberSCB + "\">" + res.value.AutoApplicantSCBAccountList[i].AccountNumberSCB + "</option>"
                }
            }
            $("cmbAccountNumberForSCBForFinalizetab").update(scbAccount);

        }
        else {
            scbAccount += "<option value=\"" + res.value.objAutoPRAccount.AccountNumberForSCB + "\">" + res.value.objAutoPRAccount.AccountNumberForSCB + "</option>"
            $('txtAccountNumberForSCB1').setValue(res.value.objAutoPRAccount.AccountNumberForSCB);
            $("cmbAccountNumberForSCBForFinalizetab").update(scbAccount);
        }
    },

    //Populate Other Account Details
    populateOtherAccountDetails: function(res) {
        for (var i = 0; i < res.value.objAutoPRAccountDetailList.length; i++) {
            var idfield = (i + 1);
            if (idfield < 6) {
                $('cmbBankNameForOther' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].BankId);
                Page.bankIdforotherBranch = res.value.objAutoPRAccountDetailList[i].BankId;
                //var cmbBankNameForOther = document.getElementById('cmbBankNameForOther' + idfield);
                //AnalystDetailsCalculationHelper.selectItemByValue(cmbBankNameForOther, res.value.objAutoPRAccountDetailList[i].BankId);

                analystDetailsManager.GetBranch('cmbBankNameForOther' + idfield);
                $('cmbBranchNameOther' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].BranchId);
                //var cmbBranchNameOther = document.getElementById('cmbBranchNameOther' + idfield);
                //AnalystDetailsCalculationHelper.selectItemByValue(cmbBranchNameOther, res.value.objAutoPRAccountDetailList[i].BranchId);

                $('txtAccountNumberother' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountNumber);
                $('cmbAccountStatus' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountCategory);
                //var cmbAccountStatus = document.getElementById('cmbAccountStatus' + idfield);
                //AnalystDetailsCalculationHelper.selectItemByValue(cmbAccountStatus, res.value.objAutoPRAccountDetailList[i].AccountCategory);

                $('cmbAccountType' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountType);
                //var cmbAccountType = document.getElementById('cmbAccountType' + idfield);
                //AnalystDetailsCalculationHelper.selectItemByValue(cmbAccountType, res.value.objAutoPRAccountDetailList[i].AccountType);

                var bankName = $('cmbBankNameForOther' + idfield).options[res.value.objAutoPRAccountDetailList[i].BankId].text;
                res.value.objAutoPRAccountDetailList[i].BankName = bankName;
            }
        }
        analystDetailsHelper.populaterepaymentbankName(res.value.objAutoPRAccountDetailList);
    },

    //Create Tab by Type
    CreateTabbyType: function(res) {

        var primaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var otherProfession = res.value.objAutoPRProfession.OtherProfession;
        var jointProfession = res.value.objAutoJTProfession.PrimaryProfession;
        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        $('cmbReasonofJointApplicant').setValue(reasonOfJoint[res.value.AutoLoanAnalystMaster.ReasonOfJoint]);
        var primaryProfessionLink = AnalystDetailsCalculationHelper.CreateTabbyProfession(primaryProfession, "Pr", res, 1);
        var otherProfessionLink = AnalystDetailsCalculationHelper.CreateTabbyProfession(otherProfession, "Pr", res, 2);
        var primaryProfessionLinkForJoint = "";
        var jtTab = "";
        var jtDiv = "";
        var jtActive = "";
        Page.tabArray = [];

        if (isJoint == true) {
            primaryProfessionLinkForJoint = AnalystDetailsCalculationHelper.CreateTabbyProfession(jointProfession, "Jt", res, 3);
            var jtLink = primaryProfessionLinkForJoint.split('^');
            jtTab = jtLink[0];
            jtDiv = jtLink[1];
            jtActive = jtLink[2];
            var obj = new Object();
            obj.PrType = 2;
            obj.PrTab = jtLink[2];
            obj.TabLink = jtLink[3]
            Page.tabArray.push(obj);

        }

        var prLink = primaryProfessionLink.split('^');
        var othLink = otherProfessionLink.split('^');
        var objPr = new Object();
        objPr.PrType = 1;
        objPr.PrTab = prLink[2];
        objPr.TabLink = prLink[3]
        if (objPr.PrTab != "") {
            Page.tabArray.push(objPr);
        }

        var objOth = new Object();
        objOth.PrType = 1;
        objOth.PrTab = othLink[2];
        objOth.TabLink = othLink[3]
        if (objOth.PrTab) {
            Page.tabArray.push(objOth);
        }

        var mainTabForIncomeGenerator = "";
        if (jtTab == "") {
            if (othLink[2] == "") {
                //mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold;\"></label><ol class =\"toc\">" + prLink[0] + othLink[0] + "</ol>" + prLink[1] + othLink[1];
                //                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                //                activatables('profs', [prLink[2]]);

                //change Date 07/11/2012 by Rimpo
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + prLink[0] + "</ol>" + prLink[1];
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                activatables('profs', [prLink[2]]);
            }
            else {
                //mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + prLink[0] + "</ol>" + prLink[1];
                //                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                //                activatables('profs', [prLink[2], othLink[2]]);

                //change Date 07/11/2012 by Rimpo
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold;\"></label><ol class =\"toc\">" + prLink[0] + othLink[0] + "</ol>" + prLink[1] + othLink[1];
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                activatables('profs', [prLink[2], othLink[2]]);
            }
        }
        else {
            if (prLink[0] == "") {
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + othLink[0] + jtTab + "</ol>" + othLink[1] + jtDiv;
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                activatables('profs', [othLink[2], jtActive]);
            }
            if (othLink[0] == "") {
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + prLink[0] + jtTab + "</ol>" + prLink[1] + jtDiv;
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                activatables('profs', [prLink[2], jtActive]);
            }
            else if (prLink[0] != "" && othLink[0] != "") {
                mainTabForIncomeGenerator = "<label id=\"lblincomeGeneratorAlertMasg\" style=\"color:Red;font-weight:bold\"></label><ol class =\"toc\">" + prLink[0] + othLink[0] + jtTab + "</ol>" + prLink[1] + othLink[1] + jtDiv;
                $("tabIncomeGenerator").update(mainTabForIncomeGenerator);
                activatables('profs', [prLink[2], othLink[2], jtActive]);
            }

        }

        //Primary

        //Salaried
        if (primaryProfession == 2 || otherProfession == 2) {
            analystDetailsManager.GetEmployerForSalaried('Pr');
        }

        //Land Lord
        if (primaryProfession == 5 || otherProfession == 5) {
            $("cmbEmployeeForLandLordPr").update(Page.sectorLinkOption);
            if (Page.incomeAssementArray.length == 0) {
                analystDetailsManager.getIncomeAssement();
            }
            $("cmbIncomeAssementForLandLordPr").update(Page.incomeAssesmentLinkOption);

        }

        //Bussiness Man
        if (primaryProfession == 1 || otherProfession == 1) {
            var htmlForPrimaryBussinessMan = bussManHtmlHelper.prHtmlForBussinessMan(res);
            var htmlPrBussinessMan = htmlForPrimaryBussinessMan.split('^');
            $("divPrBmBankListShow").update(htmlPrBussinessMan[0]);
            var tabName = [];
            for (var i = 0; i < Page.bussinessManTabArray.length; i++) {
                if (Page.bussinessManTabArray[i].AccountType == "Pr") {
                    tabName.push(Page.bussinessManTabArray[i].TabName);
                }
            }
            activatables('Bank', tabName);
            if (Page.incomeAssementArray.length == 0) {
                analystDetailsManager.getIncomeAssement();
            }
        }

        //Self Employed
        if (primaryProfession == 3 || otherProfession == 3 || primaryProfession == 4 || otherProfession == 4 || primaryProfession == 6 || otherProfession == 6) {
            var htmlForPrimarySelfemployed = selfEmployedHtmlHelper.prHtmlForSelfEmployed(res);
            var htmlPrSelfEmployed = htmlForPrimarySelfemployed.split('^');
            $("divPrSEBankListShow").update(htmlPrSelfEmployed[0]);
            var tabNamePrSe = [];
            for (var i = 0; i < Page.selfEmployedTabArray.length; i++) {
                if (Page.selfEmployedTabArray[i].AccountType == "Pr") {
                    tabNamePrSe.push(Page.selfEmployedTabArray[i].TabName);
                }
            }
            activatables('Bank', tabNamePrSe);
            if (Page.incomeAssementArray.length == 0) {
                analystDetailsManager.getIncomeAssement();
            }
        }


        //Joint
        if (isJoint == true) {
            if (jointProfession == 1) {
                var htmlForJointBussinessMan = bussManHtmlHelper.jtHtmlForBussinessManTab(res);
                var htmlJtBussinessMan = htmlForJointBussinessMan.split('^');
                $("divJtBmBankListShow").update(htmlJtBussinessMan[0]);
                var tabNameJt = [];
                for (var i = 0; i < Page.bussinessManTabArray.length; i++) {
                    if (Page.bussinessManTabArray[i].AccountType == "Jt") {
                        tabNameJt.push(Page.bussinessManTabArray[i].TabName);
                    }
                }
                activatables('Bank', tabNameJt);
                if (Page.incomeAssementArray.length == 0) {
                    analystDetailsManager.getIncomeAssement();
                }
            }
            if (jointProfession == 3 || jointProfession == 4 || jointProfession == 6) {
                var htmlForJointSelfEmployed = selfEmployedHtmlHelper.jtHtmlForSelfEmployed(res);
                var htmlJtSelfEmployed = htmlForJointSelfEmployed.split('^');
                $("divJtSEBankListShow").update(htmlJtSelfEmployed[0]);
                var tabNameJtSe = [];
                for (var i = 0; i < Page.selfEmployedTabArray.length; i++) {
                    if (Page.selfEmployedTabArray[i].AccountType == "Jt") {
                        tabNameJtSe.push(Page.selfEmployedTabArray[i].TabName);
                    }
                }
                activatables('Bank', tabNameJtSe);

                if (Page.incomeAssementArray.length == 0) {
                    analystDetailsManager.getIncomeAssement();
                }
            }
            if (jointProfession == 5) {
                $("cmbEmployeeForLandLordJt").update(Page.sectorLinkOption);
                if (Page.incomeAssementArray.length == 0) {
                    analystDetailsManager.getIncomeAssement();
                }
                $("cmbIncomeAssementForLandLordJt").update(Page.incomeAssesmentLinkOption);
            }
            if (jointProfession == 2) {
                analystDetailsManager.GetEmployerForSalaried('Jt');
            }
        }


    },

    //Create Tab by Profession with Account Type
    CreateTabbyProfession: function(profession, accountType, res, accountNo) {

        var primaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var otherProfession = res.value.objAutoPRProfession.OtherProfession;
        var jointProfession = res.value.objAutoJTProfession.PrimaryProfession;

        var mainTabForIncomeGenerator = "";
        var tabContentForIncomegeneratorStart = "";
        var tabContentFormiddle = "";
        var tabContentForIncomegeneratorEnd = "<div class=\"clear\"></div></div><div class=\"clear\"></div>";
        var tabName = '';
        var tablink = "";

        switch (profession) {
            case 1:

                mainTabForIncomeGenerator = "<li id=\"liTabBussiness\" style=\"*display:inline;\" ><a id=\"lnkbussinessMan\" href=\"#tabBusinessman\" onclick=\"AnalystDetailsCalculationHelper.hideOtherTabforIncomeGenerator('lnkbussinessMan','tabBusinessman')\"><span>Businessman</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabBusinessman\" style=\"*border:0px;\">";
                tabName = 'tabBusinessman';
                tablink = "lnkbussinessMan";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentForIncomegeneratorStart += "<div id=\"divPApplicantIncomeGenerator\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 98%;padding-top: 15px;background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrBmBankListShow\"></div></div>";
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";
                    }

                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = "<div id=\"divjtApplicantIncomeGeneratorBussinessMan\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;width: 98%;padding-top: 15px;background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT APPLICANT</span></div><br>" +
                                "<div id=\"divJtBmBankListShow\"></div></div>";
                    }
                    else {
                        tabContentForIncomegeneratorStart += "<div id=\"divPApplicantIncomeGenerator\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 98%;padding-top: 15px;background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrBmBankListShow\"></div></div>";
                        tabContentFormiddle = "<div class=\"clear\"></div><div id=\"divjtApplicantIncomeGeneratorBussinessMan\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 98%;padding-top: 15px;background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT APPLICANT</span></div>" +
                                "<div id=\"divJtBmBankListShow\"></div></div>";
                    }
                }
                break;
            case 2:
                mainTabForIncomeGenerator = "<li id=\"liTabSalaried\" style=\"*display:inline;\"><a id=\"lnkSalaried\" href=\"#tabSalaried\" onclick=\"AnalystDetailsCalculationHelper.hideOtherTabforIncomeGenerator('lnkSalaried','tabSalaried')\"><span>Salaried</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabSalaried\" style=\"*border:0px;\">";
                tabName = 'tabSalaried';
                tablink = "lnkSalaried";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentForIncomegeneratorStart += "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 98%; border-radius: 15px; margin: 0px;\"><span>PRIMARY APPLICANT</span></div>";
                        tabContentFormiddle = salariedHtmlHelper.prhtmlForSalaried();
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }

                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentForIncomegeneratorStart += "<div id=\"divjtApplicantIncomeGeneratorSalaried\"><div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 100%;*width: 100%;_width: 100%; border-radius: 15px; margin: 0px;\"><span>JOINT APPLICANT</span></div>";
                        tabContentFormiddle = salariedHtmlHelper.jthtmlForSalaried() + "</div>";
                    }
                    else {
                        tabContentForIncomegeneratorStart += "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 100%;*width: 100%;_width: 100%; border-radius: 15px; margin: 0px;\"><span>PRIMARY APPLICANT</span></div>";
                        tabContentFormiddle = salariedHtmlHelper.prhtmlForSalaried();
                        tabContentFormiddle += "<div class=\"clear\"></div><div id=\"divjtApplicantIncomeGeneratorSalaried\"><div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 100%;*width: 100%;_width: 100%; border-radius: 15px; margin: 0px;\"><span>JOINT APPLICANT</span></div>";
                        tabContentFormiddle += salariedHtmlHelper.jthtmlForSalaried() + "</div>";
                    }
                }
                break;
            case 3:
                mainTabForIncomeGenerator = "<li id=\"liTabSelfEmployed\" style=\"*display:inline; float:left;\"><a id=\"lnkSelfEmployed\" href=\"#tabSelf-Employed\" onclick=\"AnalystDetailsCalculationHelper.hideOtherTabforIncomeGenerator('lnkSelfEmployed','tabSelf-Employed')\"><span>Self-Employed</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabSelf-Employed\" style=\"*border:0px;\">";
                tabName = 'tabSelf-Employed';
                tablink = "lnkSelfEmployed";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 98%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div><br>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }
                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = "<div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                    else {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                        tabContentFormiddle += "<div class=\"clear\"></div><div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width: 100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                }
                break;
            case 4:
                mainTabForIncomeGenerator = "<li id=\"liTabSelfEmployed\" style=\"*display:inline;\"><a id=\"lnkSelfEmployed\" href=\"#tabSelf-Employed\" onclick=\"AnalystDetailsCalculationHelper.hideOtherTabforIncomeGenerator('lnkSelfEmployed','tabSelf-Employed')\"><span>Self-Employed</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabSelf-Employed\" style=\"*border:0px;\">";
                tabName = 'tabSelf-Employed';
                tablink = "lnkSelfEmployed";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }
                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = "<div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                    else {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                        tabContentFormiddle += "<div class=\"clear\"></div><div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :98% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :100%;padding-top: 15px; background-color: #005D9A;border: 2px solid #005D9A; color:#ffffff;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";

                    }
                }
                break;
            case 5:
                mainTabForIncomeGenerator = "<li id=\"liTabLandlord\" style=\"*display:inline;\"><a id=\"lnkLandLord\" href=\"#tabLandlord\" onclick=\"AnalystDetailsCalculationHelper.hideOtherTabforIncomeGenerator('lnkLandLord','tabLandlord')\"><span>Landlord</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabLandlord\" style=\"*border:0px;\">";
                tabName = 'tabLandlord';
                tablink = "lnkLandLord";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentFormiddle = landLordHtmlHelper.prHtmlForLandLord();
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }
                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = landLordHtmlHelper.jtHtmlForLandLord();
                    }
                    else {
                        tabContentFormiddle = landLordHtmlHelper.prHtmlForLandLord();
                        tabContentFormiddle += "<div class=\"clear\"></div>" + landLordHtmlHelper.jtHtmlForLandLord();
                    }
                }
                break;
            case 6:
                mainTabForIncomeGenerator = "<li id=\"liTabSelfEmployed\" style=\"*display:inline;\"><a id=\"lnkSelfEmployed\" href=\"#tabSelf-Employed\" onclick=\"AnalystDetailsCalculationHelper.hideOtherTabforIncomeGenerator('lnkSelfEmployed','tabSelf-Employed')\"><span>Self-Employed</span></a></li>";
                tabContentForIncomegeneratorStart = "<div class =\"content\" id =\"tabSelf-Employed\" style=\"*border:0px;\">";
                tabName = 'tabSelf-Employed';
                tablink = "lnkSelfEmployed";
                if (accountType == "Pr") {
                    if (((accountNo == 1) || (accountNo == 2 && profession != primaryProfession)) && profession != jointProfession) {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :98%;padding-top: 15px;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                    }
                    if ((accountNo == 2 && profession == primaryProfession) || accountNo == 1 && profession == jointProfession) {
                        mainTabForIncomeGenerator = "";
                        tabContentForIncomegeneratorStart = "";
                        tabContentFormiddle = "";
                        tabContentForIncomegeneratorEnd = "";
                        tabName = "";

                    }
                }
                else if (accountType == "Jt") {
                    if (profession != primaryProfession && profession != otherProfession) {
                        tabContentFormiddle = "<div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :98%;padding-top: 15px;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                    else {
                        tabContentFormiddle = "<div id=\"divPApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :98%;padding-top: 15px;\"><span>Primary Applicant</span></div>" +
                                "<div id=\"divPrSEBankListShow\"></div></div>";
                        tabContentFormiddle += "<div class=\"clear\"></div><div id=\"divjtApplicantSelf-Employed\" class=\"divLeftBig\" style=\"width :100% ;border: none; padding: 0;display: none;\">" +
                            "<div class=\"divHeadLine\" style=\"height: 25px;*height: 25px;_height: 25px;width :98%;padding-top: 15px;\"><span>JOINT Applicant</span></div>" +
                                "<div id=\"divJtSEBankListShow\"></div></div>";
                    }
                }
                break;
        }
        var tabContentForIncomegenerator = tabContentForIncomegeneratorStart + tabContentFormiddle + tabContentForIncomegeneratorEnd;
        var result = mainTabForIncomeGenerator + "^" + tabContentForIncomegenerator + "^" + tabName + "^" + tablink;
        return result;

    },


    //Populate Deviation
    populateDeviation: function(res) {
        for (var i = 0; i < res.value.Auto_An_FinalizationDeviationList.length; i++) {
            var identityFieldFordev = i + 1;
            $('cmdDeviationLevel' + identityFieldFordev).setValue(res.value.Auto_An_FinalizationDeviationList[i].Level);
            //var cmdDeviationLevel11 = document.getElementById('cmdDeviationLevel' + identityFieldFordev);
            //AnalystDetailsCalculationHelper.selectItemByValue(cmdDeviationLevel11, res.value.Auto_An_FinalizationDeviationList[i].Level);

            analystDetailsManager.getDeviationDescriptionByLevel('cmdDeviationLevel' + identityFieldFordev);
            $('cmdDeviationDescription' + identityFieldFordev).setValue(res.value.Auto_An_FinalizationDeviationList[i].Description);
            //var cmdDeviationDescription11 = document.getElementById('cmdDeviationDescription' + identityFieldFordev);
            //AnalystDetailsCalculationHelper.selectItemByValue(cmdDeviationDescription11, res.value.Auto_An_FinalizationDeviationList[i].Description);

            $('txtDeviationCode' + identityFieldFordev).setValue(res.value.Auto_An_FinalizationDeviationList[i].Code);
        }
    },

    //Populate Deviation Found from Database
    populateDeviationFound: function(res) {
        if (res.value.objAuto_An_FinalizationDevFound != null) {
            $('txtDeviationAgeForfinalizeTab').setValue(res.value.objAuto_An_FinalizationDevFound.Age);
            $('txtDeviationDbrForfinalizeTab').setValue(res.value.objAuto_An_FinalizationDevFound.DBR);
            $('txtSeacitingCapacityDeviationForfinalizeTab').setValue(res.value.objAuto_An_FinalizationDevFound.SeatingCapacilty);
            $('txtSharePortionForFinalizeTab').setValue(res.value.objAuto_An_FinalizationDevFound.SharePortion);
        }
    },

    //Repayment Method for Finalization
    calculateRepaymentForFinalize: function() {

        var repaymentmethod = $F("cmbAskingRepaymentmethod");
        var sequritypdc = $F("cmbSequrityPdcForFinalizetab");
        var pdcAmount = 0;
        var tenorinMonth = 0;
        
        if (repaymentmethod == "2") {

            if (sequritypdc == "1") {

                if (util.isFloat($F("txtEmrForlcloanSummary")) && !util.isEmpty($F("txtEmrForlcloanSummary"))) {
                    pdcAmount = parseFloat($F("txtEmrForlcloanSummary"));
                }
                if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
                    tenorinMonth = (parseFloat($F("txtConsideredMarginForLc")) / 12) * 2;
                }
                pdcAmount = pdcAmount * 6;
            }
            else {
                if (util.isFloat($F("txtEmrForlcloanSummary")) && !util.isEmpty($F("txtEmrForlcloanSummary"))) {
                    pdcAmount = parseFloat($F("txtEmrForlcloanSummary"));
                }
                if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
                    tenorinMonth = (parseFloat($F("txtConsideredMarginForLc")) / 12);
                }
            }



        }
        else {
            if (repaymentmethod == "1") {
                if (util.isFloat($F("txtEmrForlcloanSummary")) && !util.isEmpty($F("txtEmrForlcloanSummary"))) {
                    pdcAmount = parseFloat($F("txtEmrForlcloanSummary"));
                }
                if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
                    tenorinMonth = parseFloat($F("txtConsideredMarginForLc"));
                }
                pdcAmount = pdcAmount; //pdcAmount * 6;
                tenorinMonth = tenorinMonth;
            }
            else {
                pdcAmount = 0;
                tenorinMonth = 0;
            }
        }
        $("txtPdcAmountForFinalizeTab").setValue(pdcAmount.toFixed(2));
        $("txtNoOfPdcForFinalizeTab").setValue(tenorinMonth);

        AnalystDetailsCalculationHelper.populateDeviationfound();


    },

    //Populate Deviation Found
    populateDeviationfound: function() {

        var age = "";
        var deviationage = "";
        if (Page.age > 68) {
            age = "LEVEL - 3";
            $("txtDeviationAgeForfinalizeTab").setValue(age);
        }
        else {
            var label = $F("cmbLabelForTonerLc");
            if (label == "2") {
                age = "LEVEL - 2";
            }
            else {
                age = "";
            }
            //age = "";
            $("txtDeviationAgeForfinalizeTab").setValue(age);
        }
        var dbr = "";
        var appropriateDbr = 0;
        var cosideredDbr = 0;
        if (util.isFloat($F("txtAppropriateDbr")) && !util.isEmpty($F("txtAppropriateDbr"))) {
            appropriateDbr = parseFloat($F("txtAppropriateDbr"));
        }
        if (util.isFloat($F("txtConsideredDbr")) && !util.isEmpty($F("txtConsideredDbr"))) {
            cosideredDbr = parseFloat($F("txtConsideredDbr"));
        }
        var substracdbr = appropriateDbr - cosideredDbr;
        if (parseFloat(substracdbr) < 0) {
            dbr = "LEVEL - 3";
            $("txtDeviationDbrForfinalizeTab").setValue(dbr);
        }
        else {
            var label = $F("cmbDBRLevel");
            if (label == "2" || label == "3") {
                dbr = "LEVEL - 2";
            }
            else {
                dbr = "";
            }
            //dbr = "LEVEL - 2";
            $("txtDeviationDbrForfinalizeTab").setValue(dbr);
        }

        //var seatingCapacity = "";
        //var Seatingcapacityflag = $F("txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab");
        var seatingCapacity = $F("cmsSeatingCapacity");
        //if (Seatingcapacityflag == "YES") {
        if (parseInt(seatingCapacity) > 8) {
            $("txtSeacitingCapacityDeviationForfinalizeTab").setValue("LEVEL - 3");

        }
        else {
            $("txtSeacitingCapacityDeviationForfinalizeTab").setValue("");
        }
        var share = "";
        var shareper = 0;

        for (var i = 1; i <= Page.prCount; i++) {
            if (i < 7) {
                var prProfessionId = $F("txtPrProfessionId");
                var otherProfessionId = $F("txtPrOtherProfessionId");
                if (prProfessionId == "1" || otherProfessionId == "1") {
                    if (util.isFloat($F("txtShareForPrimaryBank" + i)) && !util.isEmpty($F("txtShareForPrimaryBank" + i))) {
                        shareper = parseFloat($F("txtShareForPrimaryBank" + i));
                        if (shareper < 10) {
                            $("txtSharePortionForFinalizeTab").setValue("LEVEL - 3");
                            break;
                        }
                    }
                }
                if (prProfessionId == "3" || otherProfessionId == "3" || prProfessionId == "4" || otherProfessionId == "4" || prProfessionId == "6" || otherProfessionId == "6") {
                    if (util.isFloat($F("txtShareForSEPrBank" + i)) && !util.isEmpty($F("txtShareForSEPrBank" + i))) {
                        shareper = parseFloat($F("txtShareForSEPrBank" + i));
                        if (shareper < 10) {
                            $("txtSharePortionForFinalizeTab").setValue("LEVEL - 3");
                            break;
                        }
                    }
                }
            }

        }
        AnalystDetailsCalculationHelper.populaterequiredDocument();


    },

    //Populate Required Document
    populaterequiredDocument: function() {
        var professionId = $F("txtProfessionIdForLcIncomeAndSegment");

        if (professionId == "2") {
            $("txtLetterOfIntroduction").setValue("MANDATORY");
            $("txtPayslips").setValue("MANDATORY");
            $("txtPayslips").setValue("MANDATORY");

            $("txtTradeLicence").setValue("REQUIRED");
            $("txtMoaAoa").setValue("REQUIRED");
            $("txtPartnershipDeed").setValue("REQUIRED");
            $("txtFrom").setValue("REQUIRED");
            $("txtboardresolution").setValue("REQUIRED");
            $("txtDiplomarcertificate").setValue("REQUIRED");
            $("txtProfessionalcertificate").setValue("REQUIRED");
            $("txtMunicipalTax").setValue("REQUIRED");
            $("txtTitleDeed").setValue("REQUIRED");
            $("txtRentalDeed").setValue("REQUIRED");
            $("txtUtilityBills").setValue("REQUIRED");
            $("txtLoanOfferLetter").setValue("REQUIRED");
        }
        else if (professionId == "1") {
            $("txtTradeLicence").setValue("MANDATORY");
            $("txtMoaAoa").setValue("MANDATORY");
            $("txtPartnershipDeed").setValue("MANDATORY");
            $("txtFrom").setValue("MANDATORY");
            $("txtboardresolution").setValue("MANDATORY");

            $("txtLetterOfIntroduction").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtDiplomarcertificate").setValue("REQUIRED");
            $("txtProfessionalcertificate").setValue("REQUIRED");
            $("txtMunicipalTax").setValue("REQUIRED");
            $("txtTitleDeed").setValue("REQUIRED");
            $("txtRentalDeed").setValue("REQUIRED");
            $("txtUtilityBills").setValue("REQUIRED");
            $("txtLoanOfferLetter").setValue("REQUIRED");
        }
        else if (professionId == "3") {
            $("txtDiplomarcertificate").setValue("MANDATORY");
            $("txtProfessionalcertificate").setValue("MANDATORY");
            $("txtMunicipalTax").setValue("MANDATORY");
            $("txtTitleDeed").setValue("MANDATORY");
            $("txtRentalDeed").setValue("MANDATORY");
            $("txtUtilityBills").setValue("MANDATORY");
            $("txtLoanOfferLetter").setValue("MANDATORY");

            $("txtLetterOfIntroduction").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtTradeLicence").setValue("REQUIRED");
            $("txtMoaAoa").setValue("REQUIRED");
            $("txtPartnershipDeed").setValue("REQUIRED");
            $("txtFrom").setValue("REQUIRED");
            $("txtboardresolution").setValue("REQUIRED");
        }
        else {
            $("txtLetterOfIntroduction").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtTradeLicence").setValue("REQUIRED");
            $("txtMoaAoa").setValue("REQUIRED");
            $("txtPartnershipDeed").setValue("REQUIRED");
            $("txtFrom").setValue("REQUIRED");
            $("txtboardresolution").setValue("REQUIRED");
            $("txtDiplomarcertificate").setValue("REQUIRED");
            $("txtProfessionalcertificate").setValue("REQUIRED");
            $("txtMunicipalTax").setValue("REQUIRED");
            $("txtTitleDeed").setValue("REQUIRED");
            $("txtRentalDeed").setValue("REQUIRED");
            $("txtUtilityBills").setValue("REQUIRED");
            $("txtLoanOfferLetter").setValue("REQUIRED");
        }
        AnalystDetailsCalculationHelper.populateVerificationreport();
    },

    //Populate Verification Report
    populateVerificationreport: function() {
        $("txtCpvreport").setValue("REQUIRED");
        $("txtCarPriceVerificationReport").setValue("REQUIRED");
        $("txtStatementAutheticationreport").setValue("REQUIRED");
        $("txtcreditReport").setValue("REQUIRED");
        $("txtCarVerificationreport").setValue("REQUIRED");
        $("txtUsedCarVerificationreport").setValue("REQUIRED");
        $("txtUsedcarDocs").setValue("NOT REQUIRED");
        $("txtCardrepaymentHistory").setValue("REQUIRED");
        var vehicleStatus = $F("cmdVehicleStatus");
        if (vehicleStatus == "3") {
            $("txtUsedcarDocs").setValue("REQUIRED");
        }
        AnalystDetailsCalculationHelper.populateVerificationreportAlerts();
    },

    //Populate Verification Alert Report
    populateVerificationreportAlerts: function() {
        var segment = $F("cmbEmployeeSegmentLc");
        var appropriateIncome = 0;
        var appropriateIncomeJoint = 0;
        var appropriateloanAmount = 0;
        var approvedLoanAmount = 0;
        var totalLoanAmount = 0;
        var appropriateInterest = 0;
        var cosideredinterest = 0;
        var appropriateTenor = 0;
        var cosideredTenor = 0;
        var maxageDeductedForProgoti = 0;
        var maxAgeDeductedForAlico = 0;
        var arta = "";
        var ltvIncInsurence = 0;
        var extraLtv = 0;
        var appropriateDbr = 0;
        var dbrExcludingIns = 0;
        var extraDbr = 0;
        var seatingCapacity = 0;
        var maxAge = 0;
        var minAge = 0;
        var primaryIncome = 0;
        var jointIncome = 0;
        var ltv = 0;

        if (segment == "-1") {
            $("txtAgeAllowedForLc").setValue('0');

        }
        else {
            var sg = Page.segmentArray.findByProp("SegmentID", segment);

            var label = $F("cmbLabelForTonerLc");
            if (label == 1) {
                maxAge = sg.MaxAgeL1;
                minAge = sg.MinAgeL1;
                primaryIncome = sg.PrimaryMinIncomeL1;
                jointIncome = sg.JointMinIncomeL1;
                ltv = sg.LTVL1;
            }
            else {
                maxAge = sg.MaxAgeL2;
                minAge = sg.MinAgeL2;
                primaryIncome = sg.PrimaryMinIncomeL2;
                jointIncome = sg.JointMinIncomeL2;
                ltv = sg.LTVL2;
            }
            //Max Age
            $("txtMaxAgeAllowed").setValue(maxAge + " YEARS");
            $("txtMaxAgeFound").setValue(Page.age + "YEARS");
            if (Page.age > maxAge) {
                $("txtMaxAgeFlag").setValue("NO");
                document.getElementById('txtMaxAgeFlag').style.background = 'red';
                document.getElementById('txtMaxAgeFlag').style.color = 'white';
            }
            else {
                $("txtMaxAgeFlag").setValue("YES");
                document.getElementById('txtMaxAgeFlag').style.background = '';
                document.getElementById('txtMaxAgeFlag').style.color = '';
            }
            //Min Age
            $("txtMinAgeAllowed").setValue(minAge + " YEARS");
            $("txtMinAgeFound").setValue(Page.age + "YEARS");
            if (minAge > Page.age) {
                $("txtMinAgeFlag").setValue("NO");
                document.getElementById('txtMinAgeFlag').style.background = 'red';
                document.getElementById('txtMinAgeFlag').style.color = 'white';
            }
            else {
                $("txtMinAgeFlag").setValue("YES");
                document.getElementById('txtMinAgeFlag').style.background = '';
                document.getElementById('txtMinAgeFlag').style.color = '';
            }
            //Min Income
            $("txtMinincomeAllowed").setValue(primaryIncome);
            appropriateIncome = $F("txtAppropriteIncomeForLcAll");
            $("txtMinIncomeFound").setValue(appropriateIncome);
            if (primaryIncome > parseFloat(appropriateIncome)) {
                $("txtMinIncomeFlag").setValue("NO");
                document.getElementById('txtMinIncomeFlag').style.background = 'red';
                document.getElementById('txtMinIncomeFlag').style.color = 'white';
            }
            else {
                $("txtMinIncomeFlag").setValue("YES");
                document.getElementById('txtMinIncomeFlag').style.background = '';
                document.getElementById('txtMinIncomeFlag').style.color = '';
            }
            //location
            $("txtLocationForLc").setValue('PDD DEFINED');
            //natinality
            $("txtNatinalityForLc").setValue('BANGLADESHI');

            //Max Age Joint
            $("txtJointMaxAgeAllowed").setValue(maxAge + " YEARS");
            $("txtJointMaxAgeFound").setValue(Page.jointage + "YEARS");
            if (Page.jointage > maxAge) {
                $("txtJointMaxAgeFlag").setValue("NO");
                document.getElementById('txtJointMaxAgeFlag').style.background = 'red';
                document.getElementById('txtJointMaxAgeFlag').style.color = 'white';
            }
            else {
                $("txtJointMaxAgeFlag").setValue("YES");
                document.getElementById('txtJointMaxAgeFlag').style.background = '';
                document.getElementById('txtJointMaxAgeFlag').style.color = '';
            }
            //Min Age Joint
            $("txtJointMinAgeAllowed").setValue(minAge + " YEARS");
            $("txtJointMinAgeFound").setValue(Page.age + "YEARS");
            if (minAge > Page.jointage) {
                $("txtJointminAgeFlag").setValue("NO");
                document.getElementById('txtJointminAgeFlag').style.background = 'red';
                document.getElementById('txtJointminAgeFlag').style.color = 'white';
            }
            else {
                $("txtJointminAgeFlag").setValue("YES");
                document.getElementById('txtJointminAgeFlag').style.background = '';
                document.getElementById('txtJointminAgeFlag').style.color = '';
            }
            //Min Income Joint
            $("txtJointMinIncomeAllowed").setValue(jointIncome);
            appropriateIncomeJoint = $F("txtAppropriteIncomeForLcJointAll");
            $("txtJointMinIncomeFound").setValue(appropriateIncomeJoint);
            if (jointIncome > parseFloat(appropriateIncomeJoint)) {
                $("txtJointMinIncomeFlag").setValue("NO");
                document.getElementById('txtJointMinIncomeFlag').style.background = 'red';
                document.getElementById('txtJointMinIncomeFlag').style.color = 'white';
            }
            else {
                $("txtJointMinIncomeFlag").setValue("YES");
                document.getElementById('txtJointMinIncomeFlag').style.background = '';
                document.getElementById('txtJointMinIncomeFlag').style.color = '';
            }
            // Loan Amount
            appropriateloanAmount = $F("txtAppropriteloanAmountForLcLoanAmount");
            approvedLoanAmount = $F("txtApprovedLoanAmountForLcLoanAmount");

            $("txtLoanAmountAllowed").setValue(appropriateloanAmount);
            $("txtLoanAmountFound").setValue(approvedLoanAmount);
            if (parseFloat(approvedLoanAmount) > parseFloat(appropriateloanAmount)) {
                $("txtLoanAmountFlag").setValue("NO");
                document.getElementById('txtLoanAmountFlag').style.background = 'red';
                document.getElementById('txtLoanAmountFlag').style.color = 'white';
            }
            else {
                $("txtLoanAmountFlag").setValue("YES");
                document.getElementById('txtLoanAmountFlag').style.background = '';
                document.getElementById('txtLoanAmountFlag').style.color = '';
            }
            //BB Max Limit
            $("txtBBMaxlimitAllowed").setValue(sg.MaxLoanAmt);
            totalLoanAmount = $F("txtloanAmountForLcLoanSummary");
            $("txtBBMaxlimitFound").setValue(totalLoanAmount);
            if (parseFloat(totalLoanAmount) > parseFloat(sg.MaxLoanAmt)) {
                $("txtBBMaxlimitFlag").setValue("NO");
                document.getElementById('txtBBMaxlimitFlag').style.background = 'red';
                document.getElementById('txtBBMaxlimitFlag').style.color = 'white';
            }
            else {
                $("txtBBMaxlimitFlag").setValue("YES");
                document.getElementById('txtBBMaxlimitFlag').style.background = '';
                document.getElementById('txtBBMaxlimitFlag').style.color = '';
            }
            //Interest Rate
            appropriateInterest = $F("txtAppropriteRate");
            cosideredinterest = $F("txtInterestLCCosideredRate");
            $("txtInterestRatesAllowed").setValue(appropriateInterest);
            $("txtInterestRatesFound").setValue(cosideredinterest);
            if (parseFloat(cosideredinterest) > parseFloat(appropriateInterest)) {
                $("txtInterestRatesFlag").setValue("NO");
                document.getElementById('txtInterestRatesFlag').style.background = 'red';
                document.getElementById('txtInterestRatesFlag').style.color = 'white';
            }
            else {
                $("txtInterestRatesFlag").setValue("YES");
                document.getElementById('txtInterestRatesFlag').style.background = '';
                document.getElementById('txtInterestRatesFlag').style.color = '';
            }
            //Tenor
            appropriateTenor = $F("txtAppropriteTenorForLc");
            cosideredTenor = $F("txtCosideredtenorForLc");
            $("txtTenorAllowed").setValue(appropriateTenor);
            $("txtTenorFound").setValue(cosideredTenor);
            if (parseFloat(cosideredTenor) > parseFloat(appropriateTenor)) {
                $("txtTenorFlag").setValue("NO");
                document.getElementById('txtTenorFlag').style.background = 'red';
                document.getElementById('txtTenorFlag').style.color = 'white';
            }
            else {
                $("txtTenorFlag").setValue("YES");
                document.getElementById('txtTenorFlag').style.background = '';
                document.getElementById('txtTenorFlag').style.color = '';
            }
            // ARTA ENROLLMENT
            maxageDeductedForProgoti = 60 - Page.age;
            maxAgeDeductedForAlico = 65 - Page.age;
            if (maxageDeductedForProgoti > 0 && maxAgeDeductedForAlico > 0) {
                $("txtArtaEnrollmentAllowed").setValue("CAN ENROLL");
            }
            else {
                $("txtArtaEnrollmentAllowed").setValue("CANNOT ENROLL");
            }
            arta = $('cmbARTA').options[$('cmbARTA').selectedIndex].text;
            $("txtArtaEnrollmentFound").setValue(arta);

            if ($F("txtArtaEnrollmentFound") == "CANNOT ENROLL" && arta == "Yes") {
                $("txtArtaEnrollmentFlag").setValue('NOT ALLOWED');
            }
            else {
                $("txtArtaEnrollmentFlag").setValue('ALLOWED');
            }
            //Ltv including Banka

            $("txtLtvIncludingBankaAllowed").setValue(ltv);
            ltvIncInsurence = $F("txtltvIncludingLtvForLcLoanSummary");
            $("txtLtvIncludingBankaFound").setValue(ltvIncInsurence);

            if (parseFloat(ltvIncInsurence) > ltv) {
                $("txtLtvIncludingBankaFlag").setValue('NO');
                document.getElementById('txtLtvIncludingBankaFlag').style.background = 'red';
                document.getElementById('txtLtvIncludingBankaFlag').style.color = 'white';
            }
            else {
                $("txtLtvIncludingBankaFlag").setValue('YES');
                document.getElementById('txtLtvIncludingBankaFlag').style.background = '';
                document.getElementById('txtLtvIncludingBankaFlag').style.color = '';
            }
            // Max Ltv SPREAD
            $("txtMaxLtvSpreadAllowed").setValue('5');
            extraLtv = $F("txtExtraLtvForLcloanSumary");
            $("txtMaxLtvSpreadFound").setValue(extraLtv);

            if (parseFloat(extraLtv) > 5) {
                $("txtMaxLtvSpreadFlag").setValue('NO');
                document.getElementById('txtMaxLtvSpreadFlag').style.background = 'red';
                document.getElementById('txtMaxLtvSpreadFlag').style.color = 'white';
            }
            else {
                $("txtMaxLtvSpreadFlag").setValue('YES');
                document.getElementById('txtMaxLtvSpreadFlag').style.background = '';
                document.getElementById('txtMaxLtvSpreadFlag').style.color = '';
            }
            //DBR
            appropriateDbr = $F("txtAppropriateDbr");
            $("txtDbrForFtAllowed").setValue(appropriateDbr);
            dbrExcludingIns = $F("txtDBRExcludingInsurence");
            $("txtDbrForFtFound").setValue(dbrExcludingIns);
            if (parseFloat(dbrExcludingIns) > parseFloat(appropriateDbr)) {
                $("txtDbrForFtFlag").setValue('NO');
                document.getElementById('txtDbrForFtFlag').style.background = 'red';
                document.getElementById('txtDbrForFtFlag').style.color = 'white';
            }
            else {
                $("txtDbrForFtFlag").setValue('YES');
                document.getElementById('txtDbrForFtFlag').style.background = '';
                document.getElementById('txtDbrForFtFlag').style.color = '';
            }
            //DBR SPREAD
            $("txtDbrSpreadForFtAllowed").setValue('5');
            extraDbr = $F("txtExtraDBRForLcloanSummary");
            $("txtDbrSpreadForFtFound").setValue(extraDbr);
            if (parseFloat(extraDbr) > 5) {
                $("txtDbrSpreadForFtFlag").setValue('NO');
                document.getElementById('txtDbrSpreadForFtFlag').style.background = 'red';
                document.getElementById('txtDbrSpreadForFtFlag').style.color = 'white';
            }
            else {
                $("txtDbrSpreadForFtFlag").setValue('YES');
                document.getElementById('txtDbrSpreadForFtFlag').style.background = '';
                document.getElementById('txtDbrSpreadForFtFlag').style.color = '';
            }
            //TOTAL AUTO LOAN
            $("txtTotalAutoLoanForFtAllowed").setValue(sg.MaxLoanAmt);
            var outStandingAmount = analystDetailsHelper.calculateOutStandingAmountFortermloan();
            var totalLoanAmountFound = parseFloat(totalLoanAmount) + parseFloat(outStandingAmount);

            $("txtTotalAutoLoanForFtFound").setValue(totalLoanAmountFound);
            if (parseFloat(totalLoanAmountFound) > parseFloat(sg.MaxLoanAmt)) {
                $("txtTotalAutoLoanForFtFlag").setValue('NO');
                document.getElementById('txtTotalAutoLoanForFtFlag').style.background = 'red';
                document.getElementById('txtTotalAutoLoanForFtFlag').style.color = 'white';
            }
            else {
                $("txtTotalAutoLoanForFtFlag").setValue('YES');
                document.getElementById('txtTotalAutoLoanForFtFlag').style.background = '';
                document.getElementById('txtTotalAutoLoanForFtFlag').style.color = '';
            }

            //TOTAL AUTO LOAN
            $("txtApproversDlaAllowed").setValue('');
            $("txtApproversDlaFound").setValue('');
            $("txtApproversDlaFlag").setValue('');

            //CTO Obtained
            $("txtCibObtainedForFt").setValue('REQUIRED');
            $("txtCibObtainedForFtFlag").setValue('YES');

            //Car Age Allowed

            var vehicleId = $F("cmbModel");
            var model = Page.modelArray.findByProp('VehicleId', vehicleId);
            tenorForvehicle = 0;
            if (model) {
                tenorForvehicle = model.Tenor;
            }

            //var tenorForvehicle = $F("txtvehicleAllowedForLc");
            var consideredtenor = $F("txtCosideredtenorForLc");
            $("txtCarAgeAtTenorEndAllowed").setValue(tenorForvehicle);
            $("txtCarAgeAtTenorEndFound").setValue(consideredtenor);

            if (parseFloat(consideredtenor) > parseFloat(tenorForvehicle)) {
                $("txtCarAgeAtTenorEndFlag").setValue('NO');
                document.getElementById('txtCarAgeAtTenorEndFlag').style.background = 'red';
                document.getElementById('txtCarAgeAtTenorEndFlag').style.color = 'white';
            }
            else {
                $("txtCarAgeAtTenorEndFlag").setValue('YES');
                document.getElementById('txtCarAgeAtTenorEndFlag').style.background = '';
                document.getElementById('txtCarAgeAtTenorEndFlag').style.color = '';
            }


            //Seating Capacity
            $("txtSeatingCapacityAllowed").setValue('10');
            seatingCapacity = parseInt($F("cmsSeatingCapacity")) + 1;
            $("txtSeatingCapacityFound").setValue(seatingCapacity);
            if (seatingCapacity > 10) {
                $("txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab").setValue('NO');
                document.getElementById('txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab').style.background = 'red';
                document.getElementById('txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab').style.color = 'white';
            }
            else {
                $("txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab").setValue('YES');
                document.getElementById('txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab').style.background = '';
                document.getElementById('txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab').style.color = '';
            }
            //Car Verification
            var ismou = $F("cmbVendoreRelationShip");
            var vehicleStatus = $F("cmdVehicleStatus");
            if (ismou != "1") {
                $("txtCarVerificationForFinTab").setValue('NOT REQUIRED');
            }
            else {

                var priceConsideredOn = $F("cmbPriceConsideredOn");
                var carverification = $F("cmbCarVerification");
                if (vehicleStatus != "3" && priceConsideredOn != "5" && carverification != "3") {
                    $("txtCarVerificationForFinTab").setValue('REQUIRED');
                }
                else {
                    $("txtCarVerificationForFinTab").setValue('NOT REQUIRED');
                }
            }

            //HYPOTHECATION CHECK

            if (vehicleStatus != "3") {
                $("txthypothecationForFinalizeTab").setValue('CHECKED');
            }
            else {
                $("txthypothecationForFinalizeTab").setValue('REQUIRED');
            }

            //VENDOR STATUS

            var mouStatusName = $('cmbVendoreRelationShip').options[$('cmbVendoreRelationShip').selectedIndex].text;
            $("txtVendoreStatusForFnlTab").setValue(mouStatusName);
        }
    },

    //Populate Auto Finalization Strength
    populateFinalizationStrength: function(res) {

        var strengthOtherCount = 1;
        for (var i = 0; i < res.value.Auto_An_FinalizationSterengthList.length; i++) {
            var identityFieldFordev = i + 1;
            var textToFind = res.value.Auto_An_FinalizationSterengthList[i].Strength;
            var strengthAdded = false;
            if (i < 5) {
                var dd = document.getElementById('cmbStrength' + identityFieldFordev);
                for (var j = 0; j < dd.options.length; j++) {
                    if (dd.options[j].text === textToFind) {
                        dd.selectedIndex = j;
                        strengthAdded = true;
                        break;
                    }
                }
            }
            if (strengthAdded == false && strengthOtherCount < 3) {
                $('tareaStrength' + strengthOtherCount).setValue(res.value.Auto_An_FinalizationSterengthList[i].Strength);
                strengthOtherCount += 1;
            }
        }
    },

    //Populate Auto Finalization Weakness
    populateFinalizationWeakness: function(res) {

        var weaknessOtherCount = 1;
        for (var i = 0; i < res.value.Auto_An_FinalizationWeaknessList.length; i++) {
            var identityFieldForWk = i + 1;
            var textToFind = res.value.Auto_An_FinalizationWeaknessList[i].Weakness;
            var weaknessAdded = false;
            if (i < 5) {
                var dd = document.getElementById('cmbWeakness' + identityFieldForWk);
                for (var j = 0; j < dd.options.length; j++) {
                    if (dd.options[j].text === textToFind) {
                        dd.selectedIndex = j;
                        weaknessAdded = true;
                        break;
                    }
                }
            }
            if (weaknessAdded == false && weaknessOtherCount < 3) {
                $('tareaWeakness' + weaknessOtherCount).setValue(res.value.Auto_An_FinalizationWeaknessList[i].Weakness);
                weaknessOtherCount += 1;
            }
        }
    },

    //Populate Auto Finalization Condition
    populateFinalizationCondition: function(res) {
        var conditionOtherCount = 1;
        for (var i = 0; i < res.value.Auto_An_FinalizationConditionList.length; i++) {
            var identityFieldForWk = i + 1;
            var textToFind = res.value.Auto_An_FinalizationConditionList[i].Condition;
            var conditionAdded = false;
            if (i < 5) {
                var dd = document.getElementById('cmbCondition' + identityFieldForWk);
                for (var j = 0; j < dd.options.length; j++) {
                    if (dd.options[j].text === textToFind) {
                        dd.selectedIndex = j;
                        conditionAdded = true;
                        break;
                    }
                }
            }
            if (conditionAdded == false && conditionOtherCount < 3) {
                $('tareaCondition' + conditionOtherCount).setValue(res.value.Auto_An_FinalizationConditionList[i].Condition);
                conditionOtherCount += 1;
            }

        }
    },

    //Populate Auto Finalization Remarks
    populateFinalizationRemarks: function(res) {
        var remarkOtherCount = 4;
        for (var i = 0; i < res.value.Auto_An_FinalizationRemarkList.length; i++) {
            var identityFieldForRk = i + 1;
            var remarkAdded = false;
            var textToFind = res.value.Auto_An_FinalizationRemarkList[i].Remarks;
            if (i < 3) {
                var dd = document.getElementById('cmbRemark' + identityFieldForRk);
                for (var j = 0; j < dd.options.length; j++) {
                    if (dd.options[j].text === textToFind) {
                        dd.selectedIndex = j;
                        remarkAdded = true;
                        break;
                    }
                }
            }
            if (remarkAdded == false) {
                $('txtRemark' + remarkOtherCount).setValue(res.value.Auto_An_FinalizationRemarkList[i].Remarks);
                remarkOtherCount += 1;
            }
        }
    },


    //Populate Auto Finalization Repayment Method
    populateFinalizationRepaymentMethod: function(res) {
        if (res.value.objAuto_An_FinalizationRepaymentMethod != null) {
            $('cmbAskingRepaymentmethodForFinalize').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.Asking);
            //var cmbAskingRepaymentmethodForFinalize11 = document.getElementById('cmbAskingRepaymentmethodForFinalize');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbAskingRepaymentmethodForFinalize11, res.value.objAuto_An_FinalizationRepaymentMethod.Asking);

            $('txtInstrumentForFinalizetab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.Instrument);
            $('cmbAccountNumberForSCBForFinalizetab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.SCBAccount);
            //var cmbAccountNumberForSCBForFinalizetab11 = document.getElementById('cmbAccountNumberForSCBForFinalizetab');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbAccountNumberForSCBForFinalizetab11, res.value.objAuto_An_FinalizationRepaymentMethod.SCBAccount);

            $('cmbSequrityPdcForFinalizetab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.SecurityPDC);
            //var cmbSequrityPdcForFina = document.getElementById('cmbSequrityPdcForFinalizetab');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbSequrityPdcForFina, res.value.objAuto_An_FinalizationRepaymentMethod.SecurityPDC);

            $('txtNoOfPdcForFinalizeTab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.NoOfPDC);
            $('txtPdcAmountForFinalizeTab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.PDCAmount);
            $('cmbBankNameForFinalizeTabofrepaymentMethod').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.BankID);
            //var cmbBankNameForFinalize = document.getElementById('cmbBankNameForFinalizeTabofrepaymentMethod');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbBankNameForFinalize, res.value.objAuto_An_FinalizationRepaymentMethod.BankID);

            $('txtAccountnumberForaBankFinalizeTab').setValue(res.value.objAuto_An_FinalizationRepaymentMethod.AccountNumber);

        }
    },

    //Populate Auto Finalization Required Document
    populateFinalizationRequiredDocument: function(res) {
        if (res.value.objAuto_An_FinalizationReqDoc != null) {
            $('cmbLetterOfIntroduction').setValue(res.value.objAuto_An_FinalizationReqDoc.LetterOfIntroduction);
            //var cmbLetterOfIntroduction11 = document.getElementById('cmbLetterOfIntroduction');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbLetterOfIntroduction11, res.value.objAuto_An_FinalizationReqDoc.LetterOfIntroduction);

            $('cmbPayslip').setValue(res.value.objAuto_An_FinalizationReqDoc.Payslips);
            //var cmbPayslip11 = document.getElementById('cmbPayslip');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbPayslip11, res.value.objAuto_An_FinalizationReqDoc.Payslips);

            $('cmbtradelicence').setValue(res.value.objAuto_An_FinalizationReqDoc.TradeLicence);
            //var cmbtradelicence11 = document.getElementById('cmbtradelicence');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbtradelicence11, res.value.objAuto_An_FinalizationReqDoc.TradeLicence);

            $('cmbMoaAoa').setValue(res.value.objAuto_An_FinalizationReqDoc.MOA_AOA);
            //var cmbMoaAoa11 = document.getElementById('cmbMoaAoa');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbMoaAoa11, res.value.objAuto_An_FinalizationReqDoc.MOA_AOA);

            $('cmbPartnershipDeed').setValue(res.value.objAuto_An_FinalizationReqDoc.PartnershipDeed);
            //var cmbPartnershipDeed11 = document.getElementById('cmbPartnershipDeed');
            // AnalystDetailsCalculationHelper.selectItemByValue(cmbPartnershipDeed11, res.value.objAuto_An_FinalizationReqDoc.PartnershipDeed);

            $('cmbFrom').setValue(res.value.objAuto_An_FinalizationReqDoc.FormX);
            //  var cmbFrom11 = document.getElementById('cmbFrom');
            //   AnalystDetailsCalculationHelper.selectItemByValue(cmbFrom11, res.value.objAuto_An_FinalizationReqDoc.FormX);

            $('cmbBoardresolution').setValue(res.value.objAuto_An_FinalizationReqDoc.BoardResolution);
            // var cmbBoardresolution11 = document.getElementById('cmbBoardresolution');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbBoardresolution11, res.value.objAuto_An_FinalizationReqDoc.BoardResolution);

            $('cmbDiplomacertificate').setValue(res.value.objAuto_An_FinalizationReqDoc.DiplomaCertificate);
            //  var cmbDiplomacertificate11 = document.getElementById('cmbDiplomacertificate');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbDiplomacertificate11, res.value.objAuto_An_FinalizationReqDoc.DiplomaCertificate);

            $('cmbProfessinoalCertificate').setValue(res.value.objAuto_An_FinalizationReqDoc.ProfessionalCertificate);
            //  var cmbProfessinoalCertificate11 = document.getElementById('cmbProfessinoalCertificate');
            //   AnalystDetailsCalculationHelper.selectItemByValue(cmbProfessinoalCertificate11, res.value.objAuto_An_FinalizationReqDoc.ProfessionalCertificate);

            $('cmbunicipalTax').setValue(res.value.objAuto_An_FinalizationReqDoc.MunicipalTaxRecipt);
            //   var cmbunicipalTax11 = document.getElementById('cmbunicipalTax');
            //    AnalystDetailsCalculationHelper.selectItemByValue(cmbunicipalTax11, res.value.objAuto_An_FinalizationReqDoc.MunicipalTaxRecipt);

            $('cmbTitleDeed').setValue(res.value.objAuto_An_FinalizationReqDoc.TitleDeed);
            //     var cmbTitleDeed11 = document.getElementById('cmbTitleDeed');
            //    AnalystDetailsCalculationHelper.selectItemByValue(cmbTitleDeed11, res.value.objAuto_An_FinalizationReqDoc.TitleDeed);

            $('cmbentalDeed').setValue(res.value.objAuto_An_FinalizationReqDoc.RentalDeed);
            //  var cmbentalDeed11 = document.getElementById('cmbentalDeed');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbentalDeed11, res.value.objAuto_An_FinalizationReqDoc.RentalDeed);

            $('cmbtilityBills').setValue(res.value.objAuto_An_FinalizationReqDoc.UtilityBill);
            // var cmbtilityBills11 = document.getElementById('cmbtilityBills');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbtilityBills11, res.value.objAuto_An_FinalizationReqDoc.UtilityBill);

            $('cmbLoanOfferLetter').setValue(res.value.objAuto_An_FinalizationReqDoc.LoanOfferLatter);
            // var cmbLoanOfferLetter11 = document.getElementById('cmbLoanOfferLetter');
            // AnalystDetailsCalculationHelper.selectItemByValue(cmbLoanOfferLetter11, res.value.objAuto_An_FinalizationReqDoc.LoanOfferLatter);


        }
    },

    //Populate Auto Finalization Verification Report
    populateFinalizationVerificationReport: function(res) {
        if (res.value.objAuto_An_FinalizationVarificationReport != null) {
            $('cmbCpvreport').setValue(res.value.objAuto_An_FinalizationVarificationReport.CPVReport);
            //   var cmbCpvreport11 = document.getElementById('cmbCpvreport');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbCpvreport11, res.value.objAuto_An_FinalizationVarificationReport.CPVReport);

            $('cmbCarPriceVerificationReport').setValue(res.value.objAuto_An_FinalizationVarificationReport.CarPriceVarificationReport);
            // var cmbCarPriceVerificationReport11 = document.getElementById('cmbCarPriceVerificationReport');
            // AnalystDetailsCalculationHelper.selectItemByValue(cmbCarPriceVerificationReport11, res.value.objAuto_An_FinalizationVarificationReport.CarPriceVarificationReport);

            $('cmbStatementAutheticationreport').setValue(res.value.objAuto_An_FinalizationVarificationReport.StatementAuthentication);
            // var cmbStatementAutheticationreport11 = document.getElementById('cmbStatementAutheticationreport');
            // AnalystDetailsCalculationHelper.selectItemByValue(cmbStatementAutheticationreport11, res.value.objAuto_An_FinalizationVarificationReport.StatementAuthentication);

            $('cmbcreditReport').setValue(res.value.objAuto_An_FinalizationVarificationReport.CreditReport);
            // var cmbcreditReport11 = document.getElementById('cmbcreditReport');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbcreditReport11, res.value.objAuto_An_FinalizationVarificationReport.CreditReport);

            $('cmbCarVerificationreport').setValue(res.value.objAuto_An_FinalizationVarificationReport.CarVerificationReport);
            //  var cmbCarVerificationreport11 = document.getElementById('cmbCarVerificationreport');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbCarVerificationreport11, res.value.objAuto_An_FinalizationVarificationReport.CarVerificationReport);

            $('cmbUsedCarVerificationreport').setValue(res.value.objAuto_An_FinalizationVarificationReport.UsedCarValuationReport);
            //   var cmbUsedCarVerificationreport11 = document.getElementById('cmbUsedCarVerificationreport');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbUsedCarVerificationreport11, res.value.objAuto_An_FinalizationVarificationReport.UsedCarValuationReport);

            $('cmbUsedcarDocs').setValue(res.value.objAuto_An_FinalizationVarificationReport.UsedCarDocs);
            //  var cmbUsedcarDocs11 = document.getElementById('cmbUsedcarDocs');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbUsedcarDocs11, res.value.objAuto_An_FinalizationVarificationReport.UsedCarDocs);

            $('cmbCardrepaymentHistory').setValue(res.value.objAuto_An_FinalizationVarificationReport.CardRepaymentHistory);
            //  var cmbCardrepaymentHistory11 = document.getElementById('cmbCardrepaymentHistory');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbCardrepaymentHistory11, res.value.objAuto_An_FinalizationVarificationReport.CardRepaymentHistory);

        }
    },

    //Populate Appraiser and Approver
    populateFinalizationAppraiserAndApprover: function(res) {
        if (res.value.objAuto_An_FinalizationAppraiserAndApprover != null) {
            $('cmbApprovedby').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.ApprovedByUserID);
            //  var cmbApprovedby11 = document.getElementById('cmbApprovedby');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbApprovedby11, res.value.objAuto_An_FinalizationAppraiserAndApprover.ApprovedByUserID);

            $('txtApprovedbyCode').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.ApprovedByUserCode);
            $('txtAutoDLALimit').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.AutoDLALimit);
            $('txtTotalSecuredlimit').setValue(res.value.objAuto_An_FinalizationAppraiserAndApprover.TotalSecuredLimit);
        }
    },

    //Populate Alert Verification Report
    Auto_An_AlertVerificationReport: function(objAuto_An_AlertVerificationReport) {
        if (objAuto_An_AlertVerificationReport != null) {
            $('txtMaxAgeAllowed').setValue(objAuto_An_AlertVerificationReport.MaxAgeAllowed);
            $('txtMaxAgeFound').setValue(objAuto_An_AlertVerificationReport.MaxAgeFound);
            $('txtMaxAgeFlag').setValue(objAuto_An_AlertVerificationReport.MaxAgeFlag);
            $('txtMinAgeAllowed').setValue(objAuto_An_AlertVerificationReport.MinAgeAllowed);
            $('txtMinAgeFound').setValue(objAuto_An_AlertVerificationReport.MinAgeFound);
            $('txtMinAgeFlag').setValue(objAuto_An_AlertVerificationReport.MinAgeFlag);
            $('txtMinincomeAllowed').setValue(objAuto_An_AlertVerificationReport.MinIncomeAllowed);
            $('txtMinIncomeFound').setValue(objAuto_An_AlertVerificationReport.MinIncomeFound);
            $('txtMinIncomeFlag').setValue(objAuto_An_AlertVerificationReport.MinIncomeFlag);
            ////$('cmbLocationForLcFlag').setValue(objAuto_An_AlertVerificationReport.Location);
            $('cmbLocationForLcFlag').setValue(objAuto_An_AlertVerificationReport.LocationCosidered);
            //   var cmbLocationForLcFlag11 = document.getElementById('cmbLocationForLcFlag');
            //  AnalystDetailsCalculationHelper.selectItemByValue(cmbLocationForLcFlag11, objAuto_An_AlertVerificationReport.LocationCosidered);

            ////$('cmbNationalityForLcFlag').setValue(objAuto_An_AlertVerificationReport.Nationality);
            $('cmbNationalityForLcFlag').setValue(objAuto_An_AlertVerificationReport.NationalityConsidered);
            //var cmbNationalityForLcFlag11 = document.getElementById('cmbNationalityForLcFlag');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbNationalityForLcFlag11, objAuto_An_AlertVerificationReport.NationalityConsidered);

            $('txtCarAgeAtTenorEndAllowed').setValue(objAuto_An_AlertVerificationReport.CarAgeAtTenorEndAllowed);
            $('txtCarAgeAtTenorEndFound').setValue(objAuto_An_AlertVerificationReport.CarAgeAtTenorEndFound);
            $('txtCarAgeAtTenorEndFlag').setValue(objAuto_An_AlertVerificationReport.CarAgeAtTenorEndFlag);
            $('txtSeatingCapacityAllowed').setValue(objAuto_An_AlertVerificationReport.SeatingCapacityAllowed);
            $('txtSeatingCapacityFound').setValue(objAuto_An_AlertVerificationReport.SeatingCapacityFound);
            $('txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab').setValue(objAuto_An_AlertVerificationReport.SeatingCapacityFlag);
            $('txtCarVerificationForFinTab').setValue(objAuto_An_AlertVerificationReport.CarVerification);
            $('txtCarVerificationFlagForFinTab').setValue(objAuto_An_AlertVerificationReport.CarVerificationFlag);
            $('txthypothecationForFinalizeTab').setValue(objAuto_An_AlertVerificationReport.HypothecationCheck);
            $('txthypothecationFlagForFinalizeTab').setValue(objAuto_An_AlertVerificationReport.HypothecationCheckFlag);
            ////$('cmbVendoreStatusFlagForFnlTab').setValue(objAuto_An_AlertVerificationReport.VendorStatus);
            $('cmbVendoreStatusFlagForFnlTab').setValue(objAuto_An_AlertVerificationReport.VendorFlag);
            //var cmbVendoreStatusFlagFor = document.getElementById('cmbVendoreStatusFlagForFnlTab');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbVendoreStatusFlagFor, objAuto_An_AlertVerificationReport.VendorFlag);

            $('txtLoanAmountAllowed').setValue(objAuto_An_AlertVerificationReport.LoanAmountAllowed);
            $('txtLoanAmountFound').setValue(objAuto_An_AlertVerificationReport.LoanAmountFound);
            $('txtLoanAmountFlag').setValue(objAuto_An_AlertVerificationReport.LoanAmountFlag);
            $('txtBBMaxlimitAllowed').setValue(objAuto_An_AlertVerificationReport.BBMaxLimitAllowed);
            $('txtBBMaxlimitFound').setValue(objAuto_An_AlertVerificationReport.BBMaxLimitFound);
            $('txtBBMaxlimitFlag').setValue(objAuto_An_AlertVerificationReport.BBMaxLimitFlag);
            $('txtInterestRatesAllowed').setValue(objAuto_An_AlertVerificationReport.InterestRateAllowed);
            $('txtInterestRatesFound').setValue(objAuto_An_AlertVerificationReport.InterestRateFound);
            $('txtInterestRatesFlag').setValue(objAuto_An_AlertVerificationReport.InterestRateFlag);
            $('txtTenorAllowed').setValue(objAuto_An_AlertVerificationReport.TenorAllowed);
            $('txtTenorFound').setValue(objAuto_An_AlertVerificationReport.TenorFound);
            $('txtTenorFlag').setValue(objAuto_An_AlertVerificationReport.TenorFlag);
            $('txtArtaEnrollmentAllowed').setValue(objAuto_An_AlertVerificationReport.ARTAEnrollmentAllowed);
            $('txtArtaEnrollmentFound').setValue(objAuto_An_AlertVerificationReport.ARTAEnrollmentFound);
            $('txtArtaEnrollmentFlag').setValue(objAuto_An_AlertVerificationReport.ARTAEnrollmentFlag);

            $('txtLtvIncludingBankaAllowed').setValue(objAuto_An_AlertVerificationReport.LTVIncBancaAllowed);
            $('txtLtvIncludingBankaFound').setValue(objAuto_An_AlertVerificationReport.LTVIncBancaFound);
            $('txtLtvIncludingBankaFlag').setValue(objAuto_An_AlertVerificationReport.LTVIncBancaFlag);
            $('txtMaxLtvSpreadAllowed').setValue(objAuto_An_AlertVerificationReport.MaxLTVSpreadAllowed);
            $('txtMaxLtvSpreadFound').setValue(objAuto_An_AlertVerificationReport.MaxLTVSpreadFound);
            $('txtMaxLtvSpreadFlag').setValue(objAuto_An_AlertVerificationReport.MaxLTVSpreadFlag);
            $('txtDbrForFtAllowed').setValue(objAuto_An_AlertVerificationReport.DBRAllowed);
            $('txtDbrForFtFound').setValue(objAuto_An_AlertVerificationReport.DBRFound);
            $('txtDbrSpreadForFtAllowed').setValue(5);
            $('txtTotalAutoLoanForFtAllowed').setValue(objAuto_An_AlertVerificationReport.TotalAutoLoanAllowed);
            $('txtTotalAutoLoanForFtFound').setValue(objAuto_An_AlertVerificationReport.TotalAutoLoanFound);
            $('txtTotalAutoLoanForFtFlag').setValue(objAuto_An_AlertVerificationReport.TotalAutoLoanFlag);
            $('txtApproversDlaAllowed').setValue(objAuto_An_AlertVerificationReport.ApproversDLAAllowed);
            $('txtApproversDlaFound').setValue(objAuto_An_AlertVerificationReport.ApproversDLAFound);
            $('txtApproversDlaFlag').setValue(objAuto_An_AlertVerificationReport.ApproversDLAFlag);
            $('txtCibObtainedForFt').setValue(objAuto_An_AlertVerificationReport.CIBOptained);

            $('txtCibObtainedForFtFlag').setValue(objAuto_An_AlertVerificationReport.CIBOptainedFlag);
            $('txtJointMaxAgeAllowed').setValue(objAuto_An_AlertVerificationReport.JtMaxAgeAllowed);
            $('txtJointMaxAgeFound').setValue(objAuto_An_AlertVerificationReport.JtMaxAgeFound);
            $('txtJointMaxAgeFlag').setValue(objAuto_An_AlertVerificationReport.JtMaxAgeFlag);
            $('txtJointMinAgeAllowed').setValue(objAuto_An_AlertVerificationReport.JtMinAgeAllowed);
            $('txtJointMinAgeFound').setValue(objAuto_An_AlertVerificationReport.JtMinAgeFound);
            $('txtJointminAgeFlag').setValue(objAuto_An_AlertVerificationReport.JtMinAgeFlag);
            $('txtJointMinIncomeAllowed').setValue(objAuto_An_AlertVerificationReport.JtMinIncomeAllowed);

            $('txtJointMinIncomeFound').setValue(objAuto_An_AlertVerificationReport.JtMinIncomeFound);
            $('txtJointMinIncomeFlag').setValue(objAuto_An_AlertVerificationReport.JtMinIncomeFlag);
        }
    },

    //Populate Finalization Rework
    populateFinalizationRework: function(res) {

        if (res.value.objAuto_An_FinalizationRework != null) {
            $('cmbReworkDone').setValue(res.value.objAuto_An_FinalizationRework.ReworkDone);
            //var cmbReworkDone11 = document.getElementById('cmbReworkDone');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbReworkDone11, res.value.objAuto_An_FinalizationRework.ReworkDone);

            $('cmbReworkReason').setValue(res.value.objAuto_An_FinalizationRework.ReworkReason);
            $('txtLastApprovalDate').setValue(res.value.objAuto_An_FinalizationRework.LastApprovalDate);
            $('txtReworkDate').setValue(res.value.objAuto_An_FinalizationRework.ReworkDate);
            $('cmbReworkCount').setValue(res.value.objAuto_An_FinalizationRework.ReworkCount);
            //var cmbReworkCount11 = document.getElementById('cmbReworkCount');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbReworkCount11, res.value.objAuto_An_FinalizationRework.ReworkCount);

            $('txtLastApprovalAmount').setValue(res.value.objAuto_An_FinalizationRework.LastApprovalAmount);
            analystDetailsHelper.changeRework();
        }
    },

    //Populate Finalization Cib Request
    populateFinalizationCibRequest: function(res) {
        for (var i = 0; i < res.value.objAuto_An_CibRequestLoan.length; i++) {
            var identityFieldForCib = i + 1;
            $('cmbFacilityForCibRequest' + identityFieldForCib).setValue(res.value.objAuto_An_CibRequestLoan[i].CibFacilityId);
            //var cmbFacilityForCibRequest = document.getElementById('cmbFacilityForCibRequest' + identityFieldForCib);
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbFacilityForCibRequest, res.value.objAuto_An_CibRequestLoan[i].CibFacilityId);

            $('txtLoanAmountCibrequest' + identityFieldForCib).setValue(res.value.objAuto_An_CibRequestLoan[i].CibAmount);
            $('txtTenorForCibRequest' + identityFieldForCib).setValue(res.value.objAuto_An_CibRequestLoan[i].CibTenor);
        }
    },

    //Populate Total Loan Amount
    populateTotalLoanAmount: function(res) {
        if (res.value.objAuto_An_LoanCalculationTotal != null) {
            $('txtloanAmountForLcLoanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.TotalLoanAmount);
            $('txtLtvExclusdingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.LTVExcludingInsurance);
            $('txtDBRExcludingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.FBRExcludingInsurance);
            $('txtltvIncludingLtvForLcLoanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.LTVIncludingInsurance);
            $('txtDBRIncludingInsurence').setValue(res.value.objAuto_An_LoanCalculationTotal.FBRIncludingInsurance);
            $('txtEmrForlcloanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.EMR);
            $('txtExtraLtvForLcloanSumary').setValue(res.value.objAuto_An_LoanCalculationTotal.ExtraLTV);
            $('txtExtraDBRForLcloanSummary').setValue(res.value.objAuto_An_LoanCalculationTotal.ExtraDBR);
        }
    },

    //Analyst Loan Calculator Insurence Total
    populateInsurenceTotal: function(res) {
        if (res.value.objAuto_An_LoanCalculationInsurance != null) {
            $('txtgeneralInsurenceForLcIns').setValue(res.value.objAuto_An_LoanCalculationInsurance.GeneralInsurance);
            $('txtArtaForLCInsurenceAmount').setValue(res.value.objAuto_An_LoanCalculationInsurance.ARTA);
            $('txtTotalInsurenceForLcInsurence').setValue(res.value.objAuto_An_LoanCalculationInsurance.TotalInsurance);

        }
    },

    //Populate Analyst Loan Amount Total
    populateAnalystLoanAmountTotal: function(res) {
        if (res.value.objAuto_An_LoanCalculationAmount != null) {
            $('txtLtvAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.LTVAllowed);
            $('txtIncoomeAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.IncomeAllowed);
            $('txtbdCadAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.BB_CADAllowed);
            $('txtAskingLoanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.AskingLoan);
            $('txtExistingFacilitiesAllowedForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.ExistingFacilityAllowed);
            $('txtAppropriteloanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.AppropriateLoan);
            $('txtApprovedLoanAmountForLcLoanAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.ApprovedLoan);

        }
    },

    //Populate Analyst Interest Rate
    populateAnalystLoanInterestRate: function(res) {
        if (res.value.objAuto_An_LoanCalculationInterest != null) {
            $('txtInterestLCVendoreAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.VendorAllowed);
            $('txtInterestLCArtaAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.ARTAAllowed);
            $('txtInterestLCSegmentAllowed').setValue(res.value.objAuto_An_LoanCalculationInterest.SegmentAllowed);
            $('txtInterestLCAskingRate').setValue(res.value.objAuto_An_LoanCalculationInterest.AskingRate);
            $('txtAppropriteRate').setValue(res.value.objAuto_An_LoanCalculationInterest.AppropriateRate);
            $('txtInterestLCCosideredRate').setValue(res.value.objAuto_An_LoanCalculationInterest.ConsideredRate);

        }
    },

    //Populate Analyst Tenor Rate
    populateAnalystLoanTenorRate: function(res) {
        if (res.value.objAuto_An_LoanCalculationTenor != null) {
            $('txtArtaAllowedFoLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ARTAAllowed);
            $('txtvehicleAllowedForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.VehicleAllowed);
            $('cmbLabelForTonerLc').setValue(res.value.objAuto_An_LoanCalculationTenor.Level);
            //var cmbLabelForTonerLc11 = document.getElementById('cmbLabelForTonerLc');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbLabelForTonerLc11, res.value.objAuto_An_LoanCalculationTenor.Level);

            var label = parseInt($F("cmbLabelForDlaLimit")); //$('cmbLabelForDlaLimit').setValue('1');
            if (res.value.objAuto_An_LoanCalculationTenor.Level > label) {
                $('cmbLabelForDlaLimit').setValue(res.value.objAuto_An_LoanCalculationTenor.Level);
                //var cmbLabelForDlaLimit11 = document.getElementById('cmbLabelForDlaLimit');
                //AnalystDetailsCalculationHelper.selectItemByValue(cmbLabelForDlaLimit11, res.value.objAuto_An_LoanCalculationTenor.Level);

            }
            $('txtAgeAllowedForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AgeAllowed);


            $('txtAskingTenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AskingTenor);
            $('txtAppropriteTenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.AppropriateTenor);
            $('txtCosideredtenorForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ConsideredTenor);
            $('txtConsideredMarginForLc').setValue(res.value.objAuto_An_LoanCalculationTenor.ConsideredInMonth);

        }
    },

    //Populate Analyst Repayment Capability
    populateAnalystRepaymentCapability: function(res) {
        if (res.value.objAuto_An_RepaymentCapability != null) {
            $('cmbDBRLevel').setValue(res.value.objAuto_An_RepaymentCapability.DBRLevel);
            //var cmbDBRLevel11 = document.getElementById('cmbDBRLevel');
            //AnalystDetailsCalculationHelper.selectItemByValue(cmbDBRLevel11, res.value.objAuto_An_RepaymentCapability.DBRLevel);

            var label = parseInt($F("cmbLabelForDlaLimit")); //$('cmbLabelForDlaLimit').setValue('1');
            if (res.value.objAuto_An_RepaymentCapability.DBRLevel > label) {
                $('cmbLabelForDlaLimit').setValue(res.value.objAuto_An_RepaymentCapability.DBRLevel);
            }
            $('txtAppropriateDbr').setValue(res.value.objAuto_An_RepaymentCapability.AppropriateDBR);
            $('txtConsideredDbr').setValue(res.value.objAuto_An_RepaymentCapability.ConsideredDBR);
            $('txtMaximumEmiCapability').setValue(res.value.objAuto_An_RepaymentCapability.MaxEMICapability);
            $('txtExistingEmi').setValue(res.value.objAuto_An_RepaymentCapability.ExistingEMI);
            $('txtCurrentEMICapability').setValue(res.value.objAuto_An_RepaymentCapability.CurrentEMICapability);
            if (res.value.objAuto_An_RepaymentCapability.BalanceSupported == 0) {
                $('txtBalanceSupported').setValue('NOT APPLICABLE');
            }
            else {
                $('txtBalanceSupported').setValue(res.value.objAuto_An_RepaymentCapability.BalanceSupported);
            }
            $('txtMaximumEmi').setValue(res.value.objAuto_An_RepaymentCapability.MaxEMI);
            analystDetailsHelper.changeConsideredDbr();
        }
    },

    //Populate Step Execution User
    populateStepExecutionUser: function(res) {
        if (res.value.objAuto_App_Step_ExecutionByUser != null) {
            $('cmbAppraisedby').setValue(res.value.objAuto_App_Step_ExecutionByUser.AppRaiseBy);
            var usr = Page.userArray.findByProp('USER_ID', res.value.objAuto_App_Step_ExecutionByUser.AppRaiseBy);
            if (usr) {
                $('txtAppraiserCode').setValue(usr.USER_CODE);
            }
            $('cmbSupportedby').setValue(res.value.objAuto_App_Step_ExecutionByUser.AppSupportBy);
            usr = Page.userArray.findByProp('USER_ID', res.value.objAuto_App_Step_ExecutionByUser.AppSupportBy);
            if (usr) {
                $('txtSupportedbyCode').setValue(usr.USER_CODE);
            }
        }
    },

    //Tab Click For LandLord SelfEmployed BussinessMan Salaried
    hideOtherTabforIncomeGenerator: function(linkId, controlId) {
        for (var i = 0; i < Page.tabArray.length; i++) {
            if (Page.tabArray[i].PrTab == controlId) {
                $(controlId).style.display = 'block';
                $(linkId).addClassName('active');
            }
            else {
                $(Page.tabArray[i].PrTab).style.display = 'none';
                $(Page.tabArray[i].TabLink).removeClassName('active');
            }
        }
    },

    //Populate Salaried
    populateSalaried: function(res) {
        if (res.value.objAutoPRProfession.PrimaryProfession == 2 || res.value.objAutoPRProfession.OtherProfession == 2) {
            $('cmbEmployerForSalariedForPr').setValue(res.value.objAutoPRProfession.NameofCompany);
            var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoPRProfession.NameofCompany);
            if (emp) {
                $('txtEmployerCodeForPr').setValue(emp.CompanyCode);
                $('cmbEmployerCategoryForPrSalaried').setValue(emp.CategoryName);
            }
        }
        if (res.value.objAutoJTProfession.PrimaryProfession == 2) {
            $('cmbEmployerForSalariedForJt').setValue(res.value.objAutoJTProfession.NameofCompany);
            var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoJTProfession.NameofCompany);
            if (emp) {
                $('txtEmployerCodeForJt').setValue(emp.CompanyCode);
                $('cmbEmployerCategoryForJtSalaried').setValue(emp.CategoryName);
            }
        }

        for (var i = 0; i < res.value.objAutoIncomeSalariedList.length; i++) {
            //Primary Salaried Applicant
            if (res.value.objAutoIncomeSalariedList[i].AccountType == 1) {
                $('cmbSalaryModeForPr').setValue(res.value.objAutoIncomeSalariedList[i].SalaryMode);
                $('cmbEmployerForSalariedForPr').setValue(res.value.objAutoIncomeSalariedList[i].Employer);
                $('txtRemarksForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].Remarks);

                var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoIncomeSalariedList[i].Employer);
                if (emp) {
                    $('txtEmployerCodeForPr').setValue(emp.CompanyCode);
                    $('cmbEmployerCategoryForPrSalaried').setValue(emp.CategoryName);
                }
                $('txtGrossSalaryForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].GrosSalary);
                $('txtNetSalaryForPr').setValue(res.value.objAutoIncomeSalariedList[i].NetSalary);
                $('cmbVeriableSalaryForPr').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalary);
                if (res.value.objAutoIncomeSalariedList[i].VariableSalary == 1) {
                    $('divVeriableSalaryForPr').style.display = 'block';
                    $('divVariableSalaryCalculationForPr').style.display = 'block';
                    $('txtVeriableSalaryPerForPrSalaried').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent);
                    var totalvrAmountForPr = 0;
                    var identity = 0;
                    if (res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist != null) {
                        for (var j = 0; j < res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist.length; j++) {
                            identity = j + 1;
                            $('cmbMonth' + identity + 'ForPrSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Month);
                            $('txtVaiableAmountForMonth' + identity + 'ForPrSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                            totalvrAmountForPr += parseFloat(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                        }
                    }
                    var variableSalaryAmount = parseFloat(parseFloat(totalvrAmountForPr) * parseFloat(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent)) / 100;

                    $('txtTotalVeriableSalaryForPrSalaried').setValue(variableSalaryAmount.toFixed(2));

                }
                else {
                    $('divVeriableSalaryForPr').style.display = 'none';
                    $('divVariableSalaryCalculationForPr').style.display = 'none';
                }
                $('cmbCarAllowence').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownce);
                $('txtCarAllowenceAmount').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount);
                if (res.value.objAutoIncomeSalariedList[i].CarAllownce == 1) {
                    $('divCarAllowenceForPrSalaried').style.display = 'block';
                    $('txtCarAllowenceForPr').setValue(res.value.objAutoIncomeSalariedList[i].CarAllowncePer);
                    var carAllowanceAmntTotal = parseFloat(parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllowncePer) * parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount)) / 100;
                    $('txtCarAllowenceTotalAmountPr').setValue(carAllowanceAmntTotal.toFixed(2));

                }
                else {
                    $('divCarAllowenceForPrSalaried').style.display = 'none';
                }

                if (res.value.objAutoIncomeSalariedList[i].SalaryMode == 2) {
                    $('changeOwnHouseAmount').setValue(res.value.objAutoIncomeSalariedList[i].OwnHouseBenifit);
                    $('cmbInComeAssementForSalaryForPr').setValue('2');
                    $('cmbInComeAssementForPr').setValue('SL');
                    $('OwnHouseBenifitDivForPrSalaried').style.display = 'block';
                }
                else {
                    $('cmbInComeAssementForSalaryForPr').setValue('1');
                    $('cmbInComeAssementForPr').setValue('SC');
                    $('OwnHouseBenifitDivForPrSalaried').style.display = 'none';
                }
                salariedHtmlHelper.calculateTotalIncome('Pr');
            }
            //Joint Salaried Applicant
            else if (res.value.objAutoIncomeSalariedList[i].AccountType == 2) {
                $('cmbSalaryModeForJt').setValue(res.value.objAutoIncomeSalariedList[i].SalaryMode);
                $('cmbEmployerForSalariedForJt').setValue(res.value.objAutoIncomeSalariedList[i].Employer);
                $('txtRemarksForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].Remarks);
                var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', res.value.objAutoIncomeSalariedList[i].Employer);
                if (emp) {
                    $('txtEmployerCodeForJt').setValue(emp.CompanyCode);
                    $('cmbEmployerCategoryForJtSalaried').setValue(emp.CategoryName);
                }
                $('txtGrossSalaryForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].GrosSalary);
                $('txtNetSalaryForJt').setValue(res.value.objAutoIncomeSalariedList[i].NetSalary);
                $('cmbVeriableSalaryForJt').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalary);
                if (res.value.objAutoIncomeSalariedList[i].VariableSalary == 1) {
                    $('divVeriableSalaryForJt').style.display = 'block';
                    $('divVariableSalaryCalculationForJt').style.display = 'block';
                    $('txtVeriableSalaryPerForJtSalaried').setValue(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent);
                    var totalvrAmountForPr = 0;
                    var identity = 0;
                    for (var j = 0; j < res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist.length; j++) {
                        identity = j + 1;
                        $('cmbMonth' + identity + 'ForJtSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Month);
                        $('txtVaiableAmountForMonth' + identity + 'ForJtSalary').setValue(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                        totalvrAmountForPr += parseFloat(res.value.objAutoIncomeSalariedList[i].ObjSalariedVariableSalaryMonthIncomelist[j].Amount);
                    }
                    var variableSalaryAmount = parseFloat(parseFloat(totalvrAmountForPr) * parseFloat(res.value.objAutoIncomeSalariedList[i].VariableSalaryPercent)) / 100;

                    $('txtTotalVeriableSalaryForJtSalaried').setValue(variableSalaryAmount.toFixed(2));

                }
                else {
                    $('divVeriableSalaryForJt').style.display = 'none';
                    $('divVariableSalaryCalculationForJt').style.display = 'none';
                }
                $('cmbCarAllowenceJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownce);
                $('txtCarAllowenceAmountJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount);
                if (res.value.objAutoIncomeSalariedList[i].CarAllownce == 1) {
                    $('divCarAllowenceForJtSalaried').style.display = 'block';
                    $('txtCarAllowenceForJt').setValue(res.value.objAutoIncomeSalariedList[i].CarAllowncePer);
                    var carAllowanceAmntTotal = parseFloat(parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllowncePer) * parseFloat(res.value.objAutoIncomeSalariedList[i].CarAllownceAmount)) / 100;
                    $('txtCarAllowenceTotalAmountJt').setValue(carAllowanceAmntTotal.toFixed(2));

                }
                else {
                    $('divCarAllowenceForJtSalaried').style.display = 'none';
                }

                if (res.value.objAutoIncomeSalariedList[i].SalaryMode == 2) {
                    $('changeOwnHouseAmountJt').setValue(res.value.objAutoIncomeSalariedList[i].OwnHouseBenifit);
                    $('cmbInComeAssementForSalaryForJt').setValue('2');
                    $('cmbInComeAssementForJt').setValue('SL');
                    $('OwnHouseBenifitDivForJtSalaried').style.display = 'block';
                }
                else {
                    $('cmbInComeAssementForSalaryForJt').setValue('1');
                    $('cmbInComeAssementForJt').setValue('SC');
                    $('OwnHouseBenifitDivForJtSalaried').style.display = 'none';
                }
                salariedHtmlHelper.calculateTotalIncome('Jt');
            }
        }
    },

    //Populate Land Lord
    populateLandLord: function(res) {
        if (res.value.objAutoPRProfession.PrimaryProfession == 5 || res.value.objAutoPRProfession.OtherProfession == 5) {
            $('cmbEmployeeForLandLordPr').setValue('12');
            analystDetailsManager.GetSubSectorForLandLord(12, "Pr");
            $('cmbSubsectorForLandLordPr').setValue('145');
            var sub = Page.subsectorArray.findByProp('SUSE_ID', 145);
            $('txtEmployeeCategoryCodeForLandLordPr').setValue(sub.SUSE_IRCODE);
            $('cmbIncomeAssementForLandLordPr').setValue('5');
            var ass = Page.incomeAssementArray.findByProp('INAM_ID', 5);
            if (ass) {
                $('txtInComeAssementCodeForLandLordPr').setValue(ass.INAM_METHODCODE);
            }
        }
        if (res.value.objAutoJTProfession.PrimaryProfession == 5) {
            $('cmbEmployeeForLandLordJt').setValue('12');
            analystDetailsManager.GetSubSectorForLandLord(12, "Jt");
            $('cmbSubsectorForLandLordJt').setValue('145');
            var sub = Page.subsectorArray.findByProp('SUSE_ID', 145);
            $('txtEmployeeCategoryCodeForLandLordJt').setValue(sub.SUSE_IRCODE);
            $('cmbIncomeAssementForLandLordJt').setValue('5');
            var ass = Page.incomeAssementArray.findByProp('INAM_ID', 5);
            if (ass) {
                $('txtInComeAssementCodeForLandLordJt').setValue(ass.INAM_METHODCODE);
            }
        }

        for (var i = 0; i < res.value.objAutoLandLord.length; i++) {
            //Primary Landlord
            if (res.value.objAutoLandLord[i].AccountType == 1) {
                $('txtAreaRemarksForLandlordPr').setValue(res.value.objAutoLandLord[i].Remarks);
                $('txtReflactionPerForLandlordPr').setValue(res.value.objAutoLandLord[i].Percentage);
                $('txtReflactionrequiredForLandLordPr').setValue(res.value.objAutoLandLord[i].ReflectionRequired);
                $('txttotalrentalIncomeForLandLordPr').setValue(res.value.objAutoLandLord[i].TotalRentalIncome);
                var idFieldForLandlord = 0;
                for (var j = 0; j < res.value.objAutoLandLord[i].Property.length; j++) {
                    idFieldForLandlord = j + 1;
                    $('txtVerifiedrent' + idFieldForLandlord + 'ForLandLordPr').setValue(res.value.objAutoLandLord[i].Property[j].Property);
                    $('txtShare' + idFieldForLandlord + 'ForLandLordPr').setValue(res.value.objAutoLandLord[i].Property[j].Share);
                    var rentalIncomeForPrlandLord = parseFloat(parseFloat(res.value.objAutoLandLord[i].Property[j].Property) * parseFloat(res.value.objAutoLandLord[i].Property[j].Share)) / 100;
                    $('txtRentalIncome' + idFieldForLandlord + 'ForLandLordPr').setValue(rentalIncomeForPrlandLord);
                }
                landLordHtmlHelper.calculaterentalIncome('txtVerifiedrent1ForLandLordPr', 'Pr');

            }
            //Joint Landlord
            else if (res.value.objAutoLandLord[i].AccountType == 2) {
                $('txtAreaRemarksForLandlordJt').setValue(res.value.objAutoLandLord[i].Remarks);
                $('txtReflactionPerForLandlordJt').setValue(res.value.objAutoLandLord[i].Percentage);
                $('txtReflactionrequiredForLandLordJt').setValue(res.value.objAutoLandLord[i].ReflectionRequired);
                $('txttotalrentalIncomeForLandLordJt').setValue(res.value.objAutoLandLord[i].TotalRentalIncome);
                var idFieldForLandlord = 0;
                for (var j = 0; j < res.value.objAutoLandLord[i].Property.length; j++) {
                    idFieldForLandlord = j + 1;
                    $('txtVerifiedrent' + idFieldForLandlord + 'ForLandLordJt').setValue(res.value.objAutoLandLord[i].Property[j].Property);
                    $('txtShare' + idFieldForLandlord + 'ForLandLordJt').setValue(res.value.objAutoLandLord[i].Property[j].Share);
                    var rentalIncomeForPrlandLord = parseFloat(parseFloat(res.value.objAutoLandLord[i].Property[j].Property) * parseFloat(res.value.objAutoLandLord[i].Property[j].Share)) / 100;
                    $('txtRentalIncome' + idFieldForLandlord + 'ForLandLordJt').setValue(rentalIncomeForPrlandLord);
                }
                landLordHtmlHelper.calculaterentalIncome('txtVerifiedrent1ForLandLordJt', 'Jt');
            }
        }
    },

    //Tab Click for Bussiness Man
    BussinessManTabShowHide: function(controlId, tabName, accountType) {

        for (var i = 0; i < Page.bussinessManTabArray.length; i++) {
            if (Page.bussinessManTabArray[i].AccountType == accountType) {
                if (Page.bussinessManTabArray[i].TabName == tabName) {
                    $(tabName).style.display = 'block';
                    $(controlId).addClassName('active');
                }
                else {
                    $(Page.bussinessManTabArray[i].TabName).style.display = 'none';
                    $(Page.bussinessManTabArray[i].TabLink).removeClassName('active');
                }
            }
        }
    },

    //Tab Click For SelfEmployed
    SelfEmployedTabShowHide: function(controlId, tabName, accountType) {

        for (var i = 0; i < Page.selfEmployedTabArray.length; i++) {
            if (Page.selfEmployedTabArray[i].AccountType == accountType) {
                if (Page.selfEmployedTabArray[i].TabName == tabName) {
                    $(tabName).style.display = 'block';
                    $(controlId).addClassName('active');
                }
                else {
                    $(Page.selfEmployedTabArray[i].TabName).style.display = 'none';
                    $(Page.selfEmployedTabArray[i].TabLink).removeClassName('active');
                }
            }
        }
    },

    //Populate Facility ON SCB
    populateFacilityOnScb: function(res) {

        var optlinkForColl = "<option value='-1'>Select From list</option>";
        var prPrimaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var jtPrimaryProfession = res.value.objAutoJTProfession.PrimaryProfession;
        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        Page.facilityListArray = res.value.objAutoLoanFacilityList;

        var j = 0;
        var k = 0;
        var l = 0;
        for (var i = 0; i < res.value.objAutoLoanFacilityList.length; i++) {
            if (res.value.objAutoLoanFacilityList[i].FacilityType == 22 || res.value.objAutoLoanFacilityList[i].FacilityType == 18) {
                var idfieldforExposerForCreditCard = j + 1;
                if (idfieldforExposerForCreditCard < 3) {
                    $('cmbTypeForCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);
                    //var cmbTypeForCreditCard = document.getElementById('cmbTypeForCreditCard' + idfieldforExposerForCreditCard);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbTypeForCreditCard, res.value.objAutoLoanFacilityList[i].FacilityType);

                    var typeforCreditCardobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    $('txtFlagForcreditCard' + idfieldforExposerForCreditCard).setValue(typeforCreditCardobject.FACI_CODE);
                    $('txtOriginalLimitForCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    //$('txtInterestRateCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                    var credInterest = res.value.objAutoLoanFacilityList[i].PresentLimit * (res.value.objAutoLoanFacilityList[i].InterestRate / 100);
                    $('txtInterestForCreditCard' + idfieldforExposerForCreditCard).setValue(Math.round(credInterest.toFixed(2)));
                    j = j + 1;
                }
            }
            else if (res.value.objAutoLoanFacilityList[i].FacilityType == 23) {
                var idfieldforExposerForOverdraft = k + 1;
                if (idfieldforExposerForOverdraft < 4) {
                    var typeforOverDraftobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    $('txtOverdraftFlag' + idfieldforExposerForOverdraft).setValue(typeforOverDraftobject.FACI_CODE);
                    $('txtOverdraftInterest' + idfieldforExposerForOverdraft).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                    $('txtOverdraftlimit' + idfieldforExposerForOverdraft).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    var interst = parseFloat(parseFloat((res.value.objAutoLoanFacilityList[i].PresentLimit * res.value.objAutoLoanFacilityList[i].InterestRate) / 12) / 100);
                    $('txtOverdraftInteres' + idfieldforExposerForOverdraft).setValue(Math.round(interst.toFixed(2)));


                    //Income Generator----------------------------------
                    //OVER DRAFT FACILITIES WITH SCB For bussiness Man Tab--------------------

                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        AnalystDetailsCalculationHelper.bussinessManAndSelfEmployedTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Pr", "BM");

                    }
                    if (jtPrimaryProfession == 1 && isJoint == true) {
                        AnalystDetailsCalculationHelper.bussinessManAndSelfEmployedTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Jt", "BM");
                    }

                    //OVER DRAFT FACILITIES WITH SCB For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        AnalystDetailsCalculationHelper.bussinessManAndSelfEmployedTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Pr", "SE");

                    }
                    if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
                        Page.jointPrimaryProfessionId = jtPrimaryProfession;
                        Page.isjoint = true;
                        AnalystDetailsCalculationHelper.bussinessManAndSelfEmployedTabOverDraftwithSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForOverdraft, "Jt", "SE");
                    }
                }

                k = k + 1;
            }
            else {
                var idfieldforExposerForTermsloans = l + 1;
                if (idfieldforExposerForTermsloans < 5) {
                    $('cmbTypeForTermLoans' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);
                    //var cmbTypeForTermLoans = document.getElementById('cmbTypeForTermLoans' + idfieldforExposerForTermsloans);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbTypeForTermLoans, res.value.objAutoLoanFacilityList[i].FacilityType);


                    var typeforOtherobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                    if (typeforOtherobject != undefined) {
                        $('txtflag' + idfieldforExposerForTermsloans).setValue(typeforOtherobject.FACI_CODE);
                    }
                    $('txtInterestRate' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                    $('txtLimit' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                    $('txtoutStanding' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentBalance);
                    $('txtEMI' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentEMI);
                    $('txtEMIPer' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].EMIPercent);
                    $('cmbStatusForSecurities' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].Status);

                    var emiShare = parseFloat(parseFloat(res.value.objAutoLoanFacilityList[i].PresentEMI) * parseFloat(res.value.objAutoLoanFacilityList[i].EMIPercent / 100));
                    $('txtEMIShare' + idfieldforExposerForTermsloans).setValue(Math.round(emiShare.toFixed(2)));
                    //Preveios repayment On Scb For bussiness Man Tab
                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        if (res.value.objAutoLoanFacilityList[i].Status == 2 || res.value.objAutoLoanFacilityList[i].Status == 3) {
                            AnalystDetailsCalculationHelper.prevRepaymentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Pr", "BM");
                        }
                    }
                    if (jtPrimaryProfession == 1 && isJoint == true) {
                        if (res.value.objAutoLoanFacilityList[i].Status == 2 || res.value.objAutoLoanFacilityList[i].Status == 3) {
                            AnalystDetailsCalculationHelper.prevRepaymentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Jt", "BM");
                        }
                    }

                    //Preveios repayment On Scb For bussiness Man Tab For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        if (res.value.objAutoLoanFacilityList[i].Status == 2 || res.value.objAutoLoanFacilityList[i].Status == 3) {
                            AnalystDetailsCalculationHelper.prevRepaymentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Pr", "SE");
                        }

                    }
                    if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
                        if (res.value.objAutoLoanFacilityList[i].Status == 2 || res.value.objAutoLoanFacilityList[i].Status == 3) {
                            AnalystDetailsCalculationHelper.prevRepaymentOnSCB(res.value.objAutoLoanFacilityList[i], idfieldforExposerForTermsloans, "Jt", "SE");
                        }
                    }
                    if (typeforOtherobject != undefined) {
                        optlinkForColl += "<option value=" + res.value.objAutoLoanFacilityList[i].FacilityType + ">" + typeforOtherobject.FACI_NAME + "</option>";
                    }
                }
                l = l + 1;
            }

        }
        $("cmbColleterization").update(optlinkForColl);
        $("cmbColleterizationWith").update(optlinkForColl);
    },

    //Bussiness Man And Self Employed Tab Over Draft with Scb
    bussinessManAndSelfEmployedTabOverDraftwithSCB: function(objAutoLoanFacilityList, idField, accountType, tabName) {


        var status = objAutoLoanFacilityList.Status;
        if (status == 1) {
            var interst = parseFloat(parseFloat(parseFloat((objAutoLoanFacilityList.PresentLimit * objAutoLoanFacilityList.InterestRate)) / 12) / 100);
            var bankno = 0;
            if (accountType == "Pr" && tabName == "BM") {
                for (var i = 0; i < Page.prCount; i++) {
                    bankno = i + 1;
                    if (bankno < 7) {
                        $('txtLimt' + idField + 'WithScbForBank' + bankno).setValue(objAutoLoanFacilityList.PresentLimit);
                        $('txtInterst' + idField + 'WithScbForBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                    }
                }
            }
            else if (accountType == "Jt" && tabName == "BM") {
                for (var i = 0; i < Page.jtCount; i++) {
                    bankno = i + 1;
                    if (bankno < 7) {
                        $('txtLimit' + idField + 'ForJointBank' + bankno).setValue(objAutoLoanFacilityList.PresentLimit);
                        $('txtInterest' + idField + 'ForJointBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                    }
                }
            }
            else if (accountType == "Pr" && tabName == "SE") {
                for (var i = 0; i < Page.prCount; i++) {
                    bankno = i + 1;
                    if (bankno < 7) {
                        $('txtLimt' + idField + 'WithScbForSEBank' + bankno).setValue(objAutoLoanFacilityList.PresentLimit);
                        $('txtInterst' + idField + 'WithScbForSEBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                    }
                }

            }
            else if (accountType == "Jt" && tabName == "SE") {
                for (var i = 0; i < Page.jtCount; i++) {
                    bankno = i + 1;
                    if (bankno < 7) {
                        $('txtLimt' + idField + 'WithScbForSEJtBank' + bankno).setValue(objAutoLoanFacilityList.PresentLimit);
                        $('txtInterst' + idField + 'WithScbForSEJtBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                    }
                }
            }
        }
    },

    //Previous Repayment On Scb
    prevRepaymentOnSCB: function(objAutoLoanonScbFacilityList, idfieldforExposerForTermsloans, accountType, tabName) {
        //var instituteName = $('cmbFinancialInstitutionForOffSCB' + idfieldforExposerForTermsloans).options[$('cmbFinancialInstitutionForOffSCB' + idfieldforExposerForTermsloans).selectedIndex].text;
        var bankno = 0;

        if (accountType == "Pr" && tabName == "BM") {
            for (var i = 0; i < Page.prCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    //                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank' + bankno).setValue(instituteName);
                    //                    $('txtBank' + idfieldforExposerForTermsloans + 'BMPr' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    //                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    //                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank' + bankno).setValue('0');

                    $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForPrimaryBank' + bankno).setValue(objAutoLoanonScbFacilityList.EMIShare);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForPrimaryBank' + bankno).setValue('0');
                    //var cmbConsiderxx = document.getElementById('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank' + bankno);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbConsiderxx, '0');
                }

            }
        }
        else if (accountType == "Jt" && tabName == "BM") {
            for (var i = 0; i < Page.jtCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    //                    $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue(instituteName); //txtInstituteOffScb
                    //                    $('txtBank' + idfieldforExposerForTermsloans + 'BMJt' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    //                    $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    //                    $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue('0');

                    $('txtEmiOnScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue(objAutoLoanonScbFacilityList.EMIShare);
                    $('cmbConsiderOnScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue('0');
                    //var cmbConsiderOffScbxx = document.getElementById('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbConsiderOffScbxx, '0');
                }

            }
        }
        else if (accountType == "Pr" && tabName == "SE") {
            for (var i = 0; i < Page.prCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    //                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank' + bankno).setValue(instituteName);
                    //                    $('txtBank' + idfieldforExposerForTermsloans + 'SEPr' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    //                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    //                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank' + bankno).setValue('0');
                    $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEPrBank' + bankno).setValue(objAutoLoanonScbFacilityList.EMIShare);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEPrBank' + bankno).setValue('0');
                }
                //var cmbConsiderxx = document.getElementById('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank' + bankno);
                //AnalystDetailsCalculationHelper.selectItemByValue(cmbConsiderxx, '0');

            }
        }
        else if (accountType == "Jt" && tabName == "SE") {
            for (var i = 0; i < Page.jtCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    //                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue(instituteName);
                    //                    $('txtBank' + idfieldforExposerForTermsloans + 'SEJt' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    //                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    //                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank' + bankno).setValue('0');
                    $('txtEmi' + idfieldforExposerForTermsloans + 'ForOnScbForSEJtBank' + bankno).setValue(objAutoLoanonScbFacilityList.EMIShare);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForOnSCBReplHisForSEJtBank' + bankno).setValue('0');
                    //var cmbConsiderxx = document.getElementById('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank' + bankno);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbConsiderxx, '0');
                }

            }
        }
    },

    //Populate CIB Info
    populateCibInfo: function(res) {
        if (res.value.AutoLoanAnalystMaster != null) {
            //$("cmbColleterization").setValue(res.value.AutoLoanAnalystMaster.Colleterization);
            //var cmbColleterization11 = document.getElementById('cmbColleterization');
            // AnalystDetailsCalculationHelper.selectItemByValue(cmbColleterization11, res.value.AutoLoanAnalystMaster.Colleterization);

            //$("cmbColleterizationWith").setValue(res.value.AutoLoanAnalystMaster.ColleterizationWith);
            //   var cmbColleterizationWith11 = document.getElementById('cmbColleterizationWith');
            //   AnalystDetailsCalculationHelper.selectItemByValue(cmbColleterizationWith11, res.value.AutoLoanAnalystMaster.ColleterizationWith);

            //$("cmbBureauHistory").setValue(res.value.AutoLoanAnalystMaster.CibInfoId);
            //  var cmbBureauHistory11 = document.getElementById('cmbBureauHistory');
            //    AnalystDetailsCalculationHelper.selectItemByValue(cmbBureauHistory11, res.value.AutoLoanAnalystMaster.CibInfoId);

            var cib = Page.cibInfo.findByProp('CibInfoId', res.value.AutoLoanAnalystMaster.CibInfoId);
            if (cib) {
                $("cmbBureauHistory").setValue(res.value.AutoLoanAnalystMaster.CibInfoId);
                $("txtCibInfoDesc").setValue(cib.CibInfoDescription);
                $("txtCibInfoCode").setValue(cib.CibInfoCode);

            }
            else {
                $("txtCibInfoDesc").setValue('');
                $("txtCibInfoCode").setValue('');
            }
        }
    },

    //Populate Sequrity On Scb
    populateSequrityOnScb: function(res) {
        var m = 0;
        var n = 0;
        var p = 0;

        for (var i = 0; i < res.value.objAutoLoanSecurityList.length; i++) {
            if (res.value.objAutoLoanSecurityList[i].FacilityType == 22 || res.value.objAutoLoanSecurityList[i].FacilityType == 18) {
                var idfieldforExposerForCreditCardSequrity = p + 1;
                if (idfieldforExposerForCreditCardSequrity < 3) {
                    var typeforCreditCardobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforCreditCardobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        //  var cmbTypeForSequritiesForcreditCard = document.getElementById('cmbTypeForSequritiesForcreditCard' + idfieldforExposerForCreditCardSequrity);
                        //  AnalystDetailsCalculationHelper.selectItemByValue(cmbTypeForSequritiesForcreditCard, res.value.objAutoLoanSecurityList[i].NatureOfSecurity);

                        $('txtSecurityFVForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        //    var cmbStatusForSecuritiesForCreditCard = document.getElementById('cmbStatusForSecuritiesForCreditCard' + idfieldforExposerForCreditCardSequrity);
                        //    AnalystDetailsCalculationHelper.selectItemByValue(cmbStatusForSecuritiesForCreditCard, res.value.objAutoLoanSecurityList[i].Status);


                        //$('txtCreditCardIssuingOffice' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                        //$('txtCreditCardIssuingDate' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());
                        //$('txtCreditCardInterestRate' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);

                        var facalityObjectForCredtcard = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForCredtcard != undefined) {
                            var outStandingForcreditCard = facalityObjectForCredtcard.PresentLimit;

                            var ltvForCreditturnOver = parseFloat(outStandingForcreditCard / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;
                            if (util.isFloat(ltvForCreditturnOver)) {
                                $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(Math.round(ltvForCreditturnOver.toFixed(2)));
                            }
                            else {
                                $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                            }
                        }
                        else {
                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                        }

                    }
                }

                p = p + 1;
            }
            else if (res.value.objAutoLoanSecurityList[i].FacilityType == 23) {
                var idfieldforExposerForOverDraftSequrity = n + 1;
                if (idfieldforExposerForOverDraftSequrity < 4) {
                    var typeforOverDraftobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforOverDraftobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        //  var cmbTypeForSequritiesForoverDraft = document.getElementById('cmbTypeForSequritiesForoverDraft' + idfieldforExposerForOverDraftSequrity);
                        //    AnalystDetailsCalculationHelper.selectItemByValue(cmbTypeForSequritiesForoverDraft, res.value.objAutoLoanSecurityList[i].NatureOfSecurity);

                        $('txtSecurityFVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);

                        $('cmbStatusForSecuritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        //    var cmbStatusForSecuritiesForoverDraft = document.getElementById('cmbStatusForSecuritiesForoverDraft' + idfieldforExposerForOverDraftSequrity);
                        //     AnalystDetailsCalculationHelper.selectItemByValue(cmbStatusForSecuritiesForoverDraft, res.value.objAutoLoanSecurityList[i].Status);


                        //$('txtOverDraftIssuingOffice' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                        //$('txtOverDraftIssuingDate' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());
                        $('txtOverDraftInterestRate' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);


                        var facalityObjectForOverDraft = Page.facilityListArray.findByProp('FacilityId', res.value.objAutoLoanSecurityList[i].FacilityId);
                        if (facalityObjectForOverDraft != undefined) {
                            var originalLimit = facalityObjectForOverDraft.PresentLimit;

                            var ltvForOverDraft = parseFloat(originalLimit / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;
                            if (util.isFloat(ltvForOverDraft)) {
                                $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(Math.round(ltvForOverDraft.toFixed(2)));
                            }
                            else {
                                $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                            }

                        }
                        else {
                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                        }

                    }
                }
                n = n + 1;
            }
            else {
                var idfieldforExposerForTermLoanSequrity = m + 1;
                if (idfieldforExposerForTermLoanSequrity < 5) {
                    var typeforTermLoanobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    //if (typeforTermLoanobjectForSequrity.FACI_CODE == "S") {
                    $('cmbTypeForSequrities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                    //var cmbTypeForSequrities = document.getElementById('cmbTypeForSequrities' + idfieldforExposerForTermLoanSequrity);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbTypeForSequrities, res.value.objAutoLoanSecurityList[i].NatureOfSecurity);

                    $('txtSecurityFV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                    $('txtSecurityPV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                    //$('cmbStatusForSecurities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                    //var cmbStatusForSecurities = document.getElementById('cmbStatusForSecurities' + idfieldforExposerForTermLoanSequrity);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbStatusForSecurities, res.value.objAutoLoanSecurityList[i].Status);


                    //$('txttermloanIssuingOffice' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                    //$('txttermloanIssuingDate' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());
                    $('txttermloanInterestRate' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);

                    var facalityObjectFortermLoan = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (facalityObjectFortermLoan != undefined) {
                        var outStandingFortermloan = facalityObjectFortermLoan.PresentBalance;

                        var ltvFortermLoan = parseFloat(outStandingFortermloan / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;
                        if (util.isFloat(ltvFortermLoan)) {
                            $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue(Math.round(ltvFortermLoan.toFixed(2)));
                        }
                        else {
                            $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                        }
                    }
                    else {
                        $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                    }

                    //}
                }
                m = m + 1;
            }

        }
    },

    //Populate Facility Off Scb
    populateFacilityOffScb: function(res) {
        var prPrimaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var jtPrimaryProfession = res.value.objAutoJTProfession.PrimaryProfession;
        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        var s = 0;
        var d = 0;
        var f = 0;
        var idfieldforOffSCBForCreditCard = 0;
        for (var i = 0; i < res.value.AutoLoanFacilityOffSCBList.length; i++) {
            if (res.value.AutoLoanFacilityOffSCBList[i].Type == 2) {
                idfieldforOffSCBForCreditCard = s + 1;
                if (idfieldforOffSCBForCreditCard < 3) {
                    $('cmbFinancialInstitutionForCreditCardOffSCB' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    //var cmbFinancialInstitutionForCreditCardOffSCB = document.getElementById('cmbFinancialInstitutionForCreditCardOffSCB' + idfieldforOffSCBForCreditCard);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbFinancialInstitutionForCreditCardOffSCB, res.value.AutoLoanFacilityOffSCBList[i].BankID);

                    $('txtOriginalLimitForcreditCardOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    //$('txtInterestRateForCreditCardForOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].InterestRate);
                    var interest = (res.value.AutoLoanFacilityOffSCBList[i].InterestRate / 100) * res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit;
                    $('txtInterestForCreditCardForOffScb' + idfieldforOffSCBForCreditCard).setValue(interest);
                    $('cmbStatusForCreditCardOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);
                    //var cmbStatusForCreditCardOffScb = document.getElementById('cmbStatusForCreditCardOffScb' + idfieldforOffSCBForCreditCard);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbStatusForCreditCardOffScb, res.value.AutoLoanFacilityOffSCBList[i].Status);

                }
                s = s + 1;
            }
            else if (res.value.AutoLoanFacilityOffSCBList[i].Type == 3) {

                idfieldforOffSCBForOverdraft = d + 1;
                if (idfieldforOffSCBForOverdraft < 4) {
                    $('cmbFinancialInstitutionForOverDraftOffSCB' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    //var cmbFinancialInstitutionForOverDraftOffSCB = document.getElementById('cmbFinancialInstitutionForOverDraftOffSCB' + idfieldforOffSCBForOverdraft);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbFinancialInstitutionForOverDraftOffSCB, res.value.AutoLoanFacilityOffSCBList[i].BankID);

                    $('txtOriginalLimitForOverDraftOffScb' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    $('txtinterestRateForOverDraftOffSCB' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].InterestRate);
                    //$('txtInterestForOverDraftInterest' + idfieldforOffSCBForCreditCard).setValue('0');
                    analystDetailsHelper.changeOverDraftInterstForOffSCB(idfieldforOffSCBForOverdraft);
                    $('cmbStatusForOverDraftOffScb' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);
                    //var cmbStatusForOverDraftOffScb = document.getElementById('cmbStatusForOverDraftOffScb' + idfieldforOffSCBForOverdraft);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbStatusForOverDraftOffScb, res.value.AutoLoanFacilityOffSCBList[i].Status);


                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        AnalystDetailsCalculationHelper.bussinessManAndSelfEmployedTabOverDraftwithOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Pr", "BM");

                    }
                    if (jtPrimaryProfession == 1 && isJoint == true) {
                        AnalystDetailsCalculationHelper.bussinessManAndSelfEmployedTabOverDraftwithOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Jt", "BM");
                    }

                    //OVER DRAFT FACILITIES WITH SCB For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        AnalystDetailsCalculationHelper.bussinessManAndSelfEmployedTabOverDraftwithOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Pr", "SE");

                    }
                    if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
                        AnalystDetailsCalculationHelper.bussinessManAndSelfEmployedTabOverDraftwithOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForOverdraft, "Jt", "SE");
                    }

                    //Need to do For BankStatement Tab

                }

                d = d + 1;
            }
            else {
                idfieldforOffSCBForTermsloans = f + 1;
                if (idfieldforOffSCBForTermsloans < 5) {
                    $('cmbFinancialInstitutionForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                    //var cmbFinancialInstitutionForOffSCB = document.getElementById('cmbFinancialInstitutionForOffSCB' + idfieldforOffSCBForTermsloans);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbFinancialInstitutionForOffSCB, res.value.AutoLoanFacilityOffSCBList[i].BankID);

                    $('txtOriginalLimitForOffScb' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                    $('txtEMIforOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMI);
                    //AnalystDetailsCalculationHelper.changeTurnOverShareForOffSCB(f);

                    $('txtEMIPerForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMIPercent);
                    //AnalystDetailsCalculationHelper.changeTurnOverShareForOffSCB(f);
                    $('txtEMIShareForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMIShare);
                    $('cmbStatusForOffScb' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);
                    //var cmbStatusForOffScb = document.getElementById('cmbStatusForOffScb' + idfieldforOffSCBForTermsloans);
                    //AnalystDetailsCalculationHelper.selectItemByValue(cmbStatusForOffScb, res.value.AutoLoanFacilityOffSCBList[i].Status);


                    if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
                        if (res.value.AutoLoanFacilityOffSCBList[i].Status == 2 || res.value.AutoLoanFacilityOffSCBList[i].Status == 3) {
                            AnalystDetailsCalculationHelper.prevRepaymentHistoryOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Pr", "BM");
                        }
                    }
                    if (jtPrimaryProfession == 1 && isJoint == true) {
                        if (res.value.AutoLoanFacilityOffSCBList[i].Status == 2 || res.value.AutoLoanFacilityOffSCBList[i].Status == 3) {
                            AnalystDetailsCalculationHelper.prevRepaymentHistoryOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Jt", "BM");
                        }
                    }

                    //Preveios repayment Of Scb For bussiness Man Tab For Self Employed Tab--------------------

                    if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6 || res.value.objAutoPRProfession.OtherProfession == 4 || res.value.objAutoPRProfession.OtherProfession == 6) {
                        if (res.value.AutoLoanFacilityOffSCBList[i].Status == 2 || res.value.AutoLoanFacilityOffSCBList[i].Status == 3) {
                            AnalystDetailsCalculationHelper.prevRepaymentHistoryOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Pr", "SE");
                        }
                    }
                    if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
                        if (res.value.AutoLoanFacilityOffSCBList[i].Status == 2 || res.value.AutoLoanFacilityOffSCBList[i].Status == 3) {
                            AnalystDetailsCalculationHelper.prevRepaymentHistoryOffSCB(res.value.AutoLoanFacilityOffSCBList[i], idfieldforOffSCBForTermsloans, "Jt", "SE");
                        }
                    }
                }

                f = f + 1;
            }

        }

    },

    //Populate Sequrity Off Scb
    populateSequrityOffScb: function(res) {
        var m = 0;
        var n = 0;
        var p = 0;

        for (var i = 0; i < res.value.objAutoLoanSecurityList.length; i++) {
            if (res.value.objAutoLoanSecurityList[i].FacilityType == 22 || res.value.objAutoLoanSecurityList[i].FacilityType == 18) {
                var idfieldforExposerForCreditCardSequrity = p + 1;
                if (idfieldforExposerForCreditCardSequrity < 3) {
                    var typeforCreditCardobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforCreditCardobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        var facalityObjectForCredtcard = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForCredtcard != undefined) {
                            var outStandingForcreditCard = facalityObjectForCredtcard.PresentLimit;

                            var ltvForCreditturnOver = parseFloat(outStandingForcreditCard / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;

                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(Math.round(ltvForCreditturnOver.toFixed(2)));
                        }
                        else {
                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                        }

                    }
                }
                p = p + 1;
            }
            else if (res.value.objAutoLoanSecurityList[i].FacilityType == 23) {
                var idfieldforExposerForOverDraftSequrity = n + 1;
                if (idfieldforExposerForOverDraftSequrity < 4) {
                    var typeforOverDraftobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforOverDraftobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);



                        var facalityObjectForOverDraft = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForOverDraft != undefined) {
                            var originalLimit = facalityObjectForOverDraft.PresentLimit;

                            var ltvForOverDraft = parseFloat(originalLimit / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;

                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(Math.round(ltvForOverDraft.toFixed(2)));
                        }
                        else {
                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                        }

                    }
                }
                n = n + 1;
            }
            else {
                var idfieldforExposerForTermLoanSequrity = m + 1;
                if (idfieldforExposerForTermLoanSequrity < 5) {
                    var typeforTermLoanobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    //if (typeforTermLoanobjectForSequrity.FACI_CODE == "U") {
                    $('cmbTypeForSequrities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                    $('txtSecurityFV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                    $('txtSecurityPV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                    $('cmbStatusForSecurities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                    var facalityObjectFortermLoan = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (facalityObjectFortermLoan != undefined) {
                        var outStandingFortermloan = facalityObjectFortermLoan.PresentBalance;

                        var ltvFortermLoan = parseFloat(outStandingFortermloan / res.value.objAutoLoanSecurityList[i].XTVRate) * 100;

                        $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue(Math.round(ltvFortermLoan.toFixed(2)));
                    }
                    else {
                        $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                    }

                }

                m = m + 1;
            }

        }
    },

    //bussinessMan And SelfEmployed Tab OverDraftwith Off SCB
    bussinessManAndSelfEmployedTabOverDraftwithOffSCB: function(objAutoLoanFacilityList, idField, accountType, tabName) {


        var status = objAutoLoanFacilityList.Status;
        if (status == 1) {
            var interst = parseFloat(parseFloat(parseFloat((objAutoLoanFacilityList.OriginalLimit * objAutoLoanFacilityList.InterestRate)) / 12) / 100);
            var bankno = 0;
            if (accountType == "Pr" && tabName == "BM") {
                for (var i = 0; i < Page.prCount; i++) {
                    bankno = i + 1;
                    if (bankno < 5) {
                        $('cmbBankName' + idField + 'ForBussinessTabPrBank' + bankno).update(Page.bankLinkOption);
                        $('cmbBankName' + idField + 'ForBussinessTabPrBank' + bankno).setValue(objAutoLoanFacilityList.BankID);
                        $('txtLimit' + idField + 'ForOffSCBBank' + bankno).setValue(objAutoLoanFacilityList.OriginalLimit);
                        $('txtInterest' + idField + 'ForOffSCBBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                        $('cmbConsider' + idField + 'ForOffScbBank' + bankno).setValue('0');
                    }
                }
            }
            else if (accountType == "Jt" && tabName == "BM") {

                for (var i = 0; i < Page.jtCount; i++) {
                    bankno = i + 1;
                    if (bankno < 5) {
                        $('cmbBankName' + idField + 'ForBussinessTabJtBank' + bankno).update(Page.bankLinkOption);
                        $('cmbBankName' + idField + 'ForBussinessTabJtBank' + bankno).setValue(objAutoLoanFacilityList.BankID);
                        $('txtLimitOffScb' + idField + 'ForJointBank' + bankno).setValue(objAutoLoanFacilityList.OriginalLimit);
                        $('txtInterestOffSCB' + idField + 'ForJointBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                        $('cmbConsiderOffScbBM' + idField + 'ForJointBank' + bankno).setValue('0');
                    }
                }

            }
            else if (accountType == "Pr" && tabName == "SE") {
                for (var i = 0; i < Page.prCount; i++) {
                    bankno = i + 1;
                    if (bankno < 5) {
                        $('cmbBankName' + idField + 'ForSEPrBank' + bankno).update(Page.bankLinkOption);
                        $('cmbBankName' + idField + 'ForSEPrBank' + bankno).setValue(objAutoLoanFacilityList.BankID);
                        $('txtLimit' + idField + 'ForSEOffSCBBank' + bankno).setValue(objAutoLoanFacilityList.OriginalLimit);
                        $('txtInterest' + idField + 'ForSEOffSCBBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                        $('cmbConsider' + idField + 'ForSEOffScbBank' + bankno).setValue('0');
                    }
                }
            }
            else if (accountType == "Jt" && tabName == "SE") {
                for (var i = 0; i < Page.jtCount; i++) {
                    bankno = i + 1;
                    if (bankno < 5) {
                        $('cmbBankName' + idField + 'ForSEJtBank' + bankno).update(Page.bankLinkOption);
                        $('cmbBankName' + idField + 'ForSEJtBank' + bankno).setValue(objAutoLoanFacilityList.BankID);
                        $('txtLimit' + idField + 'ForSEJtOffSCBBank' + bankno).setValue(objAutoLoanFacilityList.OriginalLimit);
                        $('txtInterest' + idField + 'ForSEJtOffSCBBank' + bankno).setValue(Math.round(interst.toFixed(2)));
                        $('cmbConsider' + idField + 'ForSEJtOffScbBank' + bankno).setValue('0');
                    }
                }
            }
        }
    },

    //Prev Repayment History For Off Scb
    prevRepaymentHistoryOffSCB: function(objAutoLoanOffScbFacilityList, idfieldforExposerForTermsloans, accountType, tabName) {
        var instituteName = $('cmbFinancialInstitutionForOffSCB' + idfieldforExposerForTermsloans).options[$('cmbFinancialInstitutionForOffSCB' + idfieldforExposerForTermsloans).selectedIndex].text;
        var bankno = 0;
        if (accountType == "Pr" && tabName == "BM") {
            for (var i = 0; i < Page.prCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank' + bankno).setValue(instituteName);
                    $('txtBank' + idfieldforExposerForTermsloans + 'BMPr' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBPrimaryBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForPrimaryOffScbBank' + bankno).setValue('0');
                }
            }
        }
        else if (accountType == "Jt" && tabName == "BM") {
            for (var i = 0; i < Page.jtCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtInstituteOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue(instituteName); //txtInstituteOffScb
                    $('txtBank' + idfieldforExposerForTermsloans + 'BMJt' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    $('txtEmiOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    $('cmbConsiderOffScb' + idfieldforExposerForTermsloans + 'ForJointBank' + bankno).setValue('0');
                }
            }
        }
        else if (accountType == "Pr" && tabName == "SE") {
            for (var i = 0; i < Page.prCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank' + bankno).setValue(instituteName);
                    $('txtBank' + idfieldforExposerForTermsloans + 'SEPr' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEPrBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEPrOffScbBank' + bankno).setValue('0');
                }
            }
        }
        else if (accountType == "Jt" && tabName == "SE") {
            for (var i = 0; i < Page.jtCount; i++) {
                bankno = i + 1;
                if (bankno < 7) {
                    $('txtInstitute' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue(instituteName);
                    $('txtBank' + idfieldforExposerForTermsloans + 'SEJt' + bankno).setValue(objAutoLoanOffScbFacilityList.BankID);
                    $('txtEMI' + idfieldforExposerForTermsloans + 'ForOffSCBSEJtBank' + bankno).setValue(objAutoLoanOffScbFacilityList.EMI);
                    $('cmbConsider' + idfieldforExposerForTermsloans + 'ForSEJtRepHisOffScbBank' + bankno).setValue('0');
                }
            }
        }
    },

    //Populate Bank Statement Data
    populateBankStatementData: function(res) {

        var prPrimaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        var jtPrimaryProfession = res.value.objAutoJTProfession.PrimaryProfession;
        var isJoint = res.value.objAutoLoanMaster.JointApplication;

        if (prPrimaryProfession == 1 || res.value.objAutoPRProfession.OtherProfession == 1) {
            AnalystDetailsCalculationHelper.populatePrBussinessManBankStatementData(res);
        }
        if (jtPrimaryProfession == 1 && isJoint == true) {
            AnalystDetailsCalculationHelper.populateJtBussinessManBankStatementData(res);
        }
        if (prPrimaryProfession == 3 || res.value.objAutoPRProfession.OtherProfession == 3 || prPrimaryProfession == 4 || prPrimaryProfession == 6) {
            AnalystDetailsCalculationHelper.populatePrSelfEmployedBankStatementData(res);
        }
        if ((jtPrimaryProfession == 3 || jtPrimaryProfession == 4 || jtPrimaryProfession == 6) && isJoint == true) {
            AnalystDetailsCalculationHelper.populateJtSelfEmployedBankStatementData(res);
        }
    },

    //Populate Primary BankStatement Data For Bussiness Man
    populatePrBussinessManBankStatementData: function(res) {

        var idFieldForBankStatementForIncomegenerator = 0;
        for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {
            idFieldForBankStatementForIncomegenerator = i + 1;
            if (idFieldForBankStatementForIncomegenerator < 7) {
                $('txtAccountNameBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].AccountName);
                $('txtBankStatementIdBMPr' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].PrBankStatementId);
                $('txtAccountNumberBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].AccountNumber);
                var accounttypeName = AccountType[res.value.AutoPRBankStatementList[i].AccountCategory];
                $('txtAccountTypeBank' + idFieldForBankStatementForIncomegenerator).setValue(accounttypeName);
                $('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').update(Page.bankLinkOption);
                $('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').setValue(res.value.AutoPRBankStatementList[i].BankId);

                var bankName = $('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').options[$('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').selectedIndex].text;
                $("bmprtabBank" + idFieldForBankStatementForIncomegenerator).innerHTML = bankName;

                $('txtbmprStatementBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.AutoPRBankStatementList[i].Statement);




                analystDetailsManager.GetBranchNameForIncomeGenerator('cmbBankNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator', 'cmbBranchNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator');
                $('cmbBranchNameForBank' + idFieldForBankStatementForIncomegenerator + 'IncomeGenerator').setValue(res.value.AutoPRBankStatementList[i].BranchId);

                if (res.value.AutoPRBankStatementList[i].AccountCategory == 2) {
                    $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                }
                else if (res.value.AutoPRBankStatementList[i].AccountCategory != 2 && res.value.objAutoPRProfession.EquityShare < 100) {
                    if (res.value.objAutoPRProfession.EquityShare < 10) {
                        $('cmbLabelForDlaLimit').setValue('3');
                    }
                    $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                    var objPrBm = new Object();
                    objPrBm.BankNo = idFieldForBankStatementForIncomegenerator;
                    objPrBm.isAverageBalance = false;
                    objPrBm.Accounttype = "Pr";
                    objPrBm.TabName = "Bm";
                    Page.isAverageBalancePrimaryArray.push(objPrBm);
                }
                else {
                    $('txtShareForPrimaryBank' + idFieldForBankStatementForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                    $("lblShareForBMPr" + idFieldForBankStatementForIncomegenerator).innerHTML = "";
                    $("lblForAvgBalBMPr" + idFieldForBankStatementForIncomegenerator).innerHTML = "";
                }

                AnalystDetailsCalculationHelper.populateAverageTotalForIncomeGeneratorBussinessManbankStatement(res.value.AutoPRBankStatementList[i].BankId, res.value.AutoPRBankStatementList[i].AccountNumber, "Primary", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementForIncomegenerator);
            }
        }
    },

    //Populate Joint BankStatement Data For Bussiness Man
    populateJtBussinessManBankStatementData: function(res) {
        var idFieldForBankStatementjtForIncomegenerator = 0;

        for (var i = 0; i < res.value.AutoJTBankStatementList.length; i++) {
            idFieldForBankStatementjtForIncomegenerator = i + 1;
            if (idFieldForBankStatementjtForIncomegenerator < 7) {
                $('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).update(Page.bankLinkOption);
                $('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].BankId);
                $('txtAccountName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].AccountName);
                $('txtAccountNumber1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].AccountNumber);
                $('txtBankStatementIdBMJt' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].JtBankStatementId);
                var accounttypeNamejt = AccountType[res.value.AutoJTBankStatementList[i].AccountCategory];
                $('txtCompanyName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(accounttypeNamejt);


                var bankName = $('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).options[res.value.AutoJTBankStatementList[i].BankId].text;
                $("bmjttabBank" + idFieldForBankStatementjtForIncomegenerator).innerHTML = bankName;

                $('txtbmjtStatementBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].Statement);

                //analystDetailsManager.GetBranchNameForIncomeGenerator('cmbBankName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator, 'cmbBranchName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator);
                analystDetailsManager.GetBranchNameForIncomeGeneratorJoint(res.value.AutoJTBankStatementList[i].BankId, 'cmbBranchName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator);
                $('cmbBranchName1ForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.AutoJTBankStatementList[i].BranchId);

                if (res.value.AutoJTBankStatementList[i].AccountCategory == 2) {
                    $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                }
                else if (res.value.AutoJTBankStatementList[i].AccountCategory != 2 && res.value.objAutoJTProfession.EquityShare < 100) {
                    if (res.value.objAutoJTProfession.EquityShare < 10) {
                        $('cmbLabelForDlaLimit').setValue('3');
                    }
                    $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                    var objJtBm = new Object();
                    objJtBm.BankNo = idFieldForBankStatementjtForIncomegenerator;
                    objJtBm.isAverageBalance = false;
                    objJtBm.Accounttype = "Jt";
                    objJtBm.TabName = "Bm";
                    Page.isAverageBalanceJointArray.push(objJtBm);
                }
                else {
                    $('txtShareForJointBank' + idFieldForBankStatementjtForIncomegenerator).setValue(res.value.objAutoPRProfession.EquityShare);
                    $("lblShareForBMJt" + idFieldForBankStatementjtForIncomegenerator).innerHTML = "";
                    $("lblForAvgBalBMJt" + idFieldForBankStatementjtForIncomegenerator).innerHTML = "";
                }

                AnalystDetailsCalculationHelper.populateAverageTotalForIncomeGeneratorBussinessManbankStatement(res.value.AutoJTBankStatementList[i].BankId, res.value.AutoJTBankStatementList[i].AccountNumber, "Joint", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementjtForIncomegenerator);
            }
        }
    },

    //Populate Primary Self Employed Tab
    populatePrSelfEmployedBankStatementData: function(res) {
        var idFieldForBankStatementForSePr = 0;
        var prPrimaryProfession = res.value.objAutoPRProfession.PrimaryProfession;
        for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {

            idFieldForBankStatementForSePr = i + 1;
            if (idFieldForBankStatementForSePr < 7) {
                $('txtAccountNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].AccountName);
                $('txtAccountNumberForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].AccountNumber);
                $('txtBankStatementIdSEPr' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].PrBankStatementId);
                var accounttypeSePrName = AccountType[res.value.AutoPRBankStatementList[i].AccountCategory];
                $('txtAccountTypeForSEBank' + idFieldForBankStatementForSePr).setValue(accounttypeSePrName);
                $('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).update(Page.bankLinkOption);
                $('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].BankId);
                analystDetailsManager.GetBranchNameForIncomeGenerator('cmbBankNameForSEBank' + idFieldForBankStatementForSePr, 'cmbbranchNameForSEBank' + idFieldForBankStatementForSePr);
                $('cmbbranchNameForSEBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].BranchId);

                var bankName = $('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).options[$('cmbBankNameForSEBank' + idFieldForBankStatementForSePr).selectedIndex].text;
                $("seprtabBank" + idFieldForBankStatementForSePr).innerHTML = bankName;

                $('txtseprStatementBank' + idFieldForBankStatementForSePr).setValue(res.value.AutoPRBankStatementList[i].Statement);

                if (prPrimaryProfession == 4) {

                    $('cmbIndustryForSEPrBank' + idFieldForBankStatementForSePr).setValue('12');
                    analystDetailsManager.changeSectorForSelfEmployed(idFieldForBankStatementForSePr, "Pr");
                    $('cmbSubsectorForSEPrBank' + idFieldForBankStatementForSePr).setValue('143');
                    var sub = Page.subsectorArray.findByProp('SUSE_ID', 143);
                    $('txtProfitMarginForSEPrBank' + idFieldForBankStatementForSePr).setValue(sub.SUSE_INTERESTRATE);
                    $('txtProfitMarginCodeForSEPrBank' + idFieldForBankStatementForSePr).setValue(sub.SUSE_IRCODE);

                }

                if (res.value.AutoPRBankStatementList[i].AccountCategory == 2) {
                    $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue(res.value.objAutoPRProfession.EquityShare);
                }
                else if (res.value.AutoPRBankStatementList[i].AccountCategory != 2 && res.value.objAutoPRProfession.EquityShare < 100) {
                    if (res.value.objAutoPRProfession.EquityShare < 10) {
                        $('cmbLabelForDlaLimit').setValue('3');
                    }
                    $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue(res.value.objAutoPRProfession.EquityShare);
                    var objPrSe = new Object();
                    objPrSe.BankNo = idFieldForBankStatementForSePr;
                    objPrSe.isAverageBalance = false;
                    objPrSe.Accounttype = "Pr";
                    objPrSe.TabName = "Se";
                    Page.isAverageBalancePrimaryArray.push(objPrSe);
                }
                else {
                    $('txtShareForSEPrBank' + idFieldForBankStatementForSePr).setValue(res.value.objAutoPRProfession.EquityShare);
                    $("lblShareForSEPr" + idFieldForBankStatementForSePr).innerHTML = "";
                    $("lblForAvgBalSEPr" + idFieldForBankStatementForSePr).innerHTML = "";
                }

                AnalystDetailsCalculationHelper.populateAverageTotalForSelfEmployedBankStatement(res.value.AutoPRBankStatementList[i].BankId, res.value.AutoPRBankStatementList[i].AccountNumber, "Primary", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementForSePr);
            }
        }
    },

    //Populate Joint Self Employed Data
    populateJtSelfEmployedBankStatementData: function(res) {
        var idFieldForBankStatementjtForSe = 0;
        var jtPrimaryProfession = res.value.objAutoJTProfession.PrimaryProfession;
        for (var i = 0; i < res.value.AutoJTBankStatementList.length; i++) {
            idFieldForBankStatementjtForSe = i + 1;
            if (idFieldForBankStatementjtForSe < 7) {
                $('txtAccountNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].AccountName);
                $('txtAccountNumberForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].AccountNumber);
                $('txtBankStatementIdSEJt' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].JtBankStatementId);
                var accounttypeNamejtForSe = AccountType[res.value.AutoJTBankStatementList[i].AccountCategory];
                $('txtAccountTypeForSEJtBank' + idFieldForBankStatementjtForSe).setValue(accounttypeNamejtForSe);
                $('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).update(Page.bankLinkOption);
                $('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].BankId);
                analystDetailsManager.GetBranchNameForIncomeGenerator('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe, 'cmbbranchNameForSEJtBank' + idFieldForBankStatementjtForSe);
                $('cmbbranchNameForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoJTBankStatementList[i].BranchId);


                var bankName = $('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).options[$('cmbBankNameForSEJtBank' + idFieldForBankStatementjtForSe).selectedIndex].text;
                $("sejtBank" + idFieldForBankStatementjtForSe).innerHTML = bankName;

                $('txtsejtStatementBank' + idFieldForBankStatementjtForSe).setValue(res.value.AutoPRBankStatementList[i].Statement);
                if (jtPrimaryProfession == 4) {

                    $('cmbIndustryForSEJtBank' + idFieldForBankStatementjtForSe).setValue('12');
                    analyzeLoanApplicationManager.changeSectorForSelfEmployed(idFieldForBankStatementjtForSe, 'Jt');
                    $('cmbSubsectorForSEJtBank' + idFieldForBankStatementjtForSe).setValue('143');
                    var sub = Page.subsectorArray.findByProp('SUSE_ID', 143);
                    $('txtProfitMarginForSEJtBank' + idFieldForBankStatementjtForSe).setValue(sub.SUSE_INTERESTRATE);
                    $('txtProfitMarginCodeForSEJtBank' + idFieldForBankStatementjtForSe).setValue(sub.SUSE_IRCODE);

                }


                if (res.value.AutoJTBankStatementList[i].AccountCategory == 2) {
                    $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.objAutoJTProfession.EquityShare);
                }
                else if (res.value.AutoJTBankStatementList[i].AccountCategory != 2 && res.value.objAutoJTProfession.EquityShare < 100) {
                    if (res.value.objAutoJTProfession.EquityShare < 10) {
                        $('cmbLabelForDlaLimit').setValue('3');
                    }
                    $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.objAutoJTProfession.EquityShare);
                    var objJtSe = new Object();
                    objJtSe.BankNo = idFieldForBankStatementjtForSe;
                    objJtSe.isAverageBalance = false;
                    objJtSe.Accounttype = "Jt";
                    objJtSe.TabName = "Se";
                    Page.isAverageBalanceJointArray.push(objJtSe);
                }
                else {
                    $('txtShareForSEJtBank' + idFieldForBankStatementjtForSe).setValue(res.value.objAutoJTProfession.EquityShare);
                    $("lblShareForBMJt" + idFieldForBankStatementjtForSe).innerHTML = "";
                    $("lblForAvgBalSEJtBank" + idFieldForBankStatementjtForSe).innerHTML = "";
                }

                AnalystDetailsCalculationHelper.populateAverageTotalForSelfEmployedBankStatement(res.value.AutoJTBankStatementList[i].BankId, res.value.AutoJTBankStatementList[i].AccountNumber, "Joint", res.value.AutoBankStatementAvgTotalList, idFieldForBankStatementjtForSe);
            }

        }
    },

    //Populate Average Total Income Generator Bank Statement For Bussiness Man
    populateAverageTotalForIncomeGeneratorBussinessManbankStatement: function(bankId, accountNumber, typeName, autoBankStatementAvgTotalList, bankNoCount) {

        var j = 0;
        var k = 0;
        var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var averageBalanceFor12Month = 0;
        var averageBalanceFor6Month = 0;
        var sixMonthArrayForCto = [];
        var sixMonthArrayForAverageBalance = [];

        if (typeName == "Primary") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 1) {
                    j += 1;
                    var d = new Date(autoBankStatementAvgTotalList[i].Month);
                    var newDate = monthNames[d.getMonth()] + "-" + d.getFullYear();
                    $("hdn" + j + "ForBMPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtMonth" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(newDate);
                    $("txtcreditTurnOver" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAvarageBalance" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + j + "ForIncomeGeneratorBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                }
            }
            $("txtTotalCTOFor12MonthBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));
            //Code Change by Washik 19/11/2012----------------
            //            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            //            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });

            //            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            //                if (sixMonthArrayForCto[s] != "") {
            //                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            //                }
            //            }
            //            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
            //                if (sixMonthArrayForAverageBalance[s] != "") {
            //                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
            //                }
            //            }

            //New Code Start



            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForCto.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                        totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                    }
                }

            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForAverageBalance.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForAverageBalance[(sixMonthArrayForCto.length - 1) - s] != "") {
                        averageBalanceFor6Month += sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s];
                    }
                }

            }


            $("txtTotalCTOFor6MonthBank" + bankNoCount).setValue(Math.round(totalCtofor6Month));
            var avergTotalCto12Month = Math.round((totalCtofor12Month / 12));
            var avergTotalCto6Month = Math.round((totalCtofor6Month / 6));
            $("txtAverageCTO12MonthBank" + bankNoCount).setValue(avergTotalCto12Month);
            $("txtAverageCTO6MonthBank" + bankNoCount).setValue(avergTotalCto6Month);

            var avgBalanceFor12 = Math.round(averageBalanceFor12Month / 12);
            var avgBalanceFor6 = Math.round(averageBalanceFor6Month / 6);

            $("txtAverageBalanceFor12MonthBank" + bankNoCount).setValue(avgBalanceFor12);
            $("txtAverageBalanceFor6MonthBank" + bankNoCount).setValue(avgBalanceFor6);

            //var linkCto = "<option value='" + Math.round(totalCtofor12Month.toFixed(2)) + "'>" + Math.round(totalCtofor12Month.toFixed(2)) + "</option><option value='" + Math.round(totalCtofor6Month.toFixed(2)) + "'>" + Math.round(totalCtofor6Month.toFixed(2)) + "</option>";
            var linkCto = "<option value='" + Math.round(avergTotalCto12Month.toFixed(2)) + "'>" + Math.round(avergTotalCto12Month.toFixed(2)) + "</option><option value='" + Math.round(avergTotalCto6Month.toFixed(2)) + "'>" + Math.round(avergTotalCto6Month.toFixed(2)) + "</option>";
            //var linkAvg = "<option value='" + avergTotalCto12Month + "'>" + avergTotalCto12Month + "</option><option value='" + avergTotalCto6Month + "'>" + avergTotalCto6Month + "</option>";
            var linkAvg = "<option value='" + avgBalanceFor12 + "'>" + avgBalanceFor12 + "</option><option value='" + avgBalanceFor6 + "'>" + avgBalanceFor6 + "</option>";

            $("cmbConsideredCtoForPrBank" + bankNoCount).update(linkCto);
            $("cmbConsideredBalanceForPrBank" + bankNoCount).update(linkAvg);

            $("cmbConsideredCtoForPrBank" + bankNoCount).setValue(Math.round(totalCtofor12Month.toFixed(2)));
            $("cmbConsideredBalanceForPrBank" + bankNoCount).setValue(Math.round(avergTotalCto12Month));
            $("txtConsiderMarginForPrimaryBank" + bankNoCount).setValue('3');
            var grossincome = parseFloat((totalCtofor12Month * 3) / 100);
            $("txtgrossincomeForPrimaryBank" + bankNoCount).setValue(Math.round(grossincome.toFixed(2)));
            $("txtNetAfterPaymentForPrimaryBank" + bankNoCount).setValue(Math.round(grossincome.toFixed(2)));
            var share = $F("txtShareForPrimaryBank" + bankNoCount);

            var netIncomeTotal = parseFloat((grossincome * parseFloat(share)) / 100);

            $("txtNetincomeForPrimaryBank" + bankNoCount).setValue(Math.round(netIncomeTotal.toFixed(2)));
            $("cmbIncomeAssessmentForPrimaryBank" + bankNoCount).update(Page.incomeAssesmentLinkOption);
            $("cmbIndustryForPrimaryBank" + bankNoCount).update(Page.sectorLinkOption);

            AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNoCount, 'Pr', 'BM');



        }
        else if (typeName == "Joint") {

            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 2) {
                    k += 1;
                    if (k < 13) {
                        $("hdn" + k + "ForBMJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                        $("txtmonthName" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Month);
                        $("txtcreditturnOver" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                        $("txtAverageBalance" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                        $("txtAdjustment" + k + "ForJointBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                        totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                        averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                        sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                        sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                    }
                    else {
                        k = 0;
                    }
                }
            }
            $("txtTotalCTOFor12MonthJointBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));

            //Code Comment by Washik 19/11/2012

            //            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            //            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });
            //            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            //                if (sixMonthArrayForCto[s] != "") {
            //                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            //                }
            //            }
            //            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
            //                if (sixMonthArrayForAverageBalance[s] != "") {
            //                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
            //                }
            //            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForCto.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                        totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                    }
                }

            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForAverageBalance.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s] != "") {
                        averageBalanceFor6Month += sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s];
                    }
                }

            }


            $("txtTotalCTOFor6MonthJointBank" + bankNoCount).setValue(Math.round(totalCtofor6Month));
            var avergTotalCto12Monthjoint = Math.round((totalCtofor12Month / 12));
            var avergTotalCto6Monthjoint = Math.round((totalCtofor6Month / 6));
            $("txtAverageCTOFor12MonthForJointBank" + bankNoCount).setValue(avergTotalCto12Monthjoint);
            $("txtAverageCTOFor6MonthForJointBank" + bankNoCount).setValue(avergTotalCto6Monthjoint);



            var avgBalanceFor12 = Math.round(averageBalanceFor12Month / 12);
            var avgBalanceFor6 = Math.round(averageBalanceFor6Month / 6);

            $("txtAverageBalance12MonthForJointBank" + bankNoCount).setValue(avgBalanceFor12);
            $("txtAverageBalance6MonthForJointBank" + bankNoCount).setValue(avgBalanceFor6);

            //var linkCtoJoint = "<option value='" + Math.round(totalCtofor12Month) + "'>" + Math.round(totalCtofor12Month) + "</option><option value='" + Math.round(totalCtofor6Month) + "'>" + Math.round(totalCtofor6Month) + "</option>";
            var linkCtoJoint = "<option value='" + Math.round(avergTotalCto12Monthjoint) + "'>" + Math.round(avergTotalCto12Monthjoint) + "</option><option value='" + Math.round(avergTotalCto6Monthjoint) + "'>" + Math.round(avergTotalCto6Monthjoint) + "</option>";
            //var linkAvgJoint = "<option value='" + avergTotalCto12Monthjoint + "'>" + avergTotalCto12Monthjoint + "</option><option value='" + avergTotalCto6Monthjoint + "'>" + avergTotalCto6Monthjoint + "</option>";

            var linkAvgJoint = "<option value='" + avgBalanceFor12 + "'>" + avgBalanceFor12 + "</option><option value='" + avgBalanceFor6 + "'>" + avgBalanceFor6 + "</option>";

            $("cmbConsideredCtoForJtBank" + bankNoCount).update(linkCtoJoint);
            $("cmbConsideredBalanceForJtBank" + bankNoCount).update(linkAvgJoint);
            $("cmbConsideredCtoForJtBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));
            $("cmbConsideredBalanceForJtBank" + bankNoCount).setValue(Math.round(avergTotalCto12Monthjoint));
            $("txtConsideredMarginBMForJointBank" + bankNoCount).setValue('3');
            var grossJtincome = parseFloat((totalCtofor12Month * 3) / 100);
            $("txtgrossIncomeForJointBank" + bankNoCount).setValue(Math.round(grossJtincome.toFixed(2)));
            $("txtNetAfterPaymentForJointBank" + bankNoCount).setValue(Math.round(grossJtincome.toFixed(2)));
            var shareJt = $F("txtShareForJointBank" + bankNoCount);

            var netIncomeTotalJt = parseFloat((grossJtincome * parseFloat(shareJt)) / 100);

            $("txtNetIncomeForJointBank" + bankNoCount).setValue(Math.round(netIncomeTotalJt.toFixed(2)));

            $("cmbIncomeAssementForJointBank" + bankNoCount).update(Page.incomeAssesmentLinkOption);
            $("cmbIndustryForJointBank" + bankNoCount).update(Page.sectorLinkOption);
            AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNoCount, 'Jt', 'BM');
        }

    },

    //Populate Average Total Income Generator Bank Statement For Self Employed
    populateAverageTotalForSelfEmployedBankStatement: function(bankId, accountNumber, typeName, autoBankStatementAvgTotalList, bankNoCount) {

        var j = 0;
        var k = 0;
        var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var totalCtofor12Month = 0;
        var totalCtofor6Month = 0;
        var averageBalanceFor12Month = 0;
        var averageBalanceFor6Month = 0;
        var sixMonthArrayForCto = [];
        var sixMonthArrayForAverageBalance = [];
        if (typeName == "Primary") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 1) {
                    j += 1;
                    var d = new Date(autoBankStatementAvgTotalList[i].Month);
                    var newDate = monthNames[d.getMonth()] + "-" + d.getFullYear();
                    $("hdn" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtMonth" + j + "ForSEPrBank" + bankNoCount).setValue(newDate);
                    $("txtCreditTurnOver" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAverageBalance" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + j + "ForSEPrBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                }
            }
            $("txtTotalCTOForSEPr12MonthBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));

            //Code comment by Washik 19/11/2012

            //            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            //            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });

            //            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            //                if (sixMonthArrayForCto[s] != "") {
            //                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            //                }
            //            }
            //            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
            //                if (sixMonthArrayForAverageBalance[s] != "") {
            //                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
            //                }
            //            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForCto.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                        totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                    }
                }

            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForAverageBalance.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s] != "") {
                        averageBalanceFor6Month += sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s];
                    }
                }

            }

            $("txtTotalCTOForSEPr6MonthBank" + bankNoCount).setValue(Math.round(totalCtofor6Month));
            var avergTotalCto12Month = Math.round((totalCtofor12Month / 12));
            var avergTotalCto6Month = Math.round((totalCtofor6Month / 6));
            $("txtAverageCTOForSEPr12MonthBank" + bankNoCount).setValue(avergTotalCto12Month);
            $("txtAverageCTOForSEPr6MonthBank" + bankNoCount).setValue(avergTotalCto6Month);

            var avgBalanceFor12 = Math.round(averageBalanceFor12Month / 12);
            var avgBalanceFor6 = Math.round(averageBalanceFor6Month / 6);

            $("txtAverageBalanceForSEPr12MonthBank" + bankNoCount).setValue(avgBalanceFor12);
            $("txtAverageBalanceForSEPr6MonthBank" + bankNoCount).setValue(avgBalanceFor6);

            //var linkCto = "<option value='" + Math.round(totalCtofor12Month.toFixed(2)) + "'>" + Math.round(totalCtofor12Month.toFixed(2)) + "</option><option value='" + Math.round(totalCtofor6Month.toFixed(2)) + "'>" + Math.round(totalCtofor6Month.toFixed(2)) + "</option>";
            var linkCto = "<option value='" + Math.round(avergTotalCto12Month.toFixed(2)) + "'>" + Math.round(avergTotalCto12Month.toFixed(2)) + "</option><option value='" + Math.round(avergTotalCto6Month.toFixed(2)) + "'>" + Math.round(avergTotalCto6Month.toFixed(2)) + "</option>";
            //var linkAvg = "<option value='" + avergTotalCto12Month + "'>" + avergTotalCto12Month + "</option><option value='" + avergTotalCto6Month + "'>" + avergTotalCto6Month + "</option>";
            var linkAvg = "<option value='" + avgBalanceFor12 + "'>" + avgBalanceFor12 + "</option><option value='" + avgBalanceFor6 + "'>" + avgBalanceFor6 + "</option>";

            $("cmbConsideredCtoForSEPrBank" + bankNoCount).update(linkCto);
            $("cmbConsideredBalanceForSEPrBank" + bankNoCount).update(linkAvg);

            $("cmbConsideredCtoForSEPrBank" + bankNoCount).setValue(Math.round(totalCtofor12Month.toFixed(2))); //E35
            $("cmbConsideredBalanceForSEPrBank" + bankNoCount).setValue(avergTotalCto12Month);
            $("txtConsiderMarginForSEPrBank" + bankNoCount).setValue('3'); //E37

            //=IF(P31="FT",P37,E35*E37)
            var grossincome = parseFloat((totalCtofor12Month * 3) / 100);
            $("txtgrossincomeForSEPrBank" + bankNoCount).setValue(Math.round(grossincome.toFixed(2)));
            $("txtNetAfterPaymentForSEPrBank" + bankNoCount).setValue(Math.round(grossincome.toFixed(2)));
            var share = $F("txtShareForSEPrBank" + bankNoCount);

            var netIncomeTotal = parseFloat((grossincome * parseFloat(share)) / 100);

            $("txtNetincomeForSEPrBank" + bankNoCount).setValue(Math.round(netIncomeTotal.toFixed(2)));
            $("cmbIncomeAssessmentForSEPrBank" + bankNoCount).update(Page.incomeAssesmentLinkOption);
            $("cmbIndustryForSEPrBank" + bankNoCount).update(Page.sectorLinkOption);

            AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNoCount, 'Pr', 'SE');

        }
        else if (typeName == "Joint") {
            for (var i = 0; i < autoBankStatementAvgTotalList.length; i++) {
                if (bankId == autoBankStatementAvgTotalList[i].BankId && accountNumber == autoBankStatementAvgTotalList[i].AccountNumber && autoBankStatementAvgTotalList[i].Type == 2) {
                    k += 1;
                    $("txtMonth" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Month);
                    $("hdn" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].BankStatementAvgTotalId);
                    $("txtCreditTurnOver" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    $("txtAverageBalance" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].AVGTotal);
                    $("txtAdjustment" + k + "ForSEJtBank" + bankNoCount).setValue(autoBankStatementAvgTotalList[i].Adjustment);
                    totalCtofor12Month += parseFloat(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    averageBalanceFor12Month += parseFloat(autoBankStatementAvgTotalList[i].AVGTotal);
                    sixMonthArrayForCto.push(autoBankStatementAvgTotalList[i].TrunOverTotal);
                    sixMonthArrayForAverageBalance.push(autoBankStatementAvgTotalList[i].AVGTotal);
                }
            }

            $("txtTotalCTOForSEJt12MonthBank" + bankNoCount).setValue(Math.round(totalCtofor12Month));

            //Code Comment by Washik 19/11/2012
            //            sixMonthArrayForCto = sixMonthArrayForCto.sort(function(a, b) { return b - a });
            //            sixMonthArrayForAverageBalance = sixMonthArrayForAverageBalance.sort(function(a, b) { return b - a });
            //            for (var s = 0; s < sixMonthArrayForCto.length - 6; s++) {
            //                if (sixMonthArrayForCto[s] != "") {
            //                    totalCtofor6Month += parseFloat(sixMonthArrayForCto[s]);
            //                }
            //            }
            //            for (var s = 0; s < sixMonthArrayForAverageBalance.length - 6; s++) {
            //                if (sixMonthArrayForAverageBalance[s] != "") {
            //                    averageBalanceFor6Month += parseFloat(sixMonthArrayForAverageBalance[s]);
            //                }
            //            }
            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForCto.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s] != "") {
                        totalCtofor6Month += sixMonthArrayForCto[(sixMonthArrayForCto.length - 1) - s];
                    }
                }

            }

            for (var s = 0; s <= 5; s++) {
                if ((sixMonthArrayForAverageBalance.length - 1) - s < 0) {
                }
                else {
                    if (sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s] != "") {
                        averageBalanceFor6Month += sixMonthArrayForAverageBalance[(sixMonthArrayForAverageBalance.length - 1) - s];
                    }
                }

            }


            $("txtTotalCTOForSEJt6MonthBank" + bankNoCount).setValue(Math.round(totalCtofor6Month));
            var avergTotalCto12Monthjoint = Math.round((totalCtofor12Month / 12));
            var avergTotalCto6Monthjoint = Math.round((totalCtofor6Month / 6));
            $("txtAverageCTOForSEJt12MonthBank" + bankNoCount).setValue(avergTotalCto12Monthjoint);
            $("txtAverageCTOForSEJt6MonthBank" + bankNoCount).setValue(avergTotalCto6Monthjoint);

            var avgBalanceFor12 = Math.round(averageBalanceFor12Month / 12);
            var avgBalanceFor6 = Math.round(averageBalanceFor6Month / 6);

            $("txtAverageBalanceForSEJt12MonthBank" + bankNoCount).setValue(avgBalanceFor12);
            $("txtAverageBalanceForSEJt6MonthBank" + bankNoCount).setValue(avgBalanceFor6);

            //var linkCtoJoint = "<option value='" + totalCtofor12Month.toFixed(2) + "'>" + totalCtofor12Month.toFixed(2) + "</option><option value='" + totalCtofor6Month.toFixed(2) + "'>" + totalCtofor6Month.toFixed(2) + "</option>";
            var linkCtoJoint = "<option value='" + avergTotalCto12Monthjoint.toFixed(2) + "'>" + avergTotalCto12Monthjoint.toFixed(2) + "</option><option value='" + avergTotalCto6Monthjoint.toFixed(2) + "'>" + avergTotalCto6Monthjoint.toFixed(2) + "</option>";
            //var linkAvgJoint = "<option value='" + avergTotalCto12Monthjoint + "'>" + avergTotalCto12Monthjoint + "</option><option value='" + avergTotalCto6Monthjoint + "'>" + avergTotalCto6Monthjoint + "</option>";
            var linkAvgJoint = "<option value='" + avgBalanceFor12 + "'>" + avgBalanceFor12 + "</option><option value='" + avgBalanceFor6 + "'>" + avgBalanceFor6 + "</option>";
            $("cmbConsideredCtoForSEJtBank" + bankNoCount).update(linkCtoJoint);
            $("cmbConsideredBalanceForSEJtBank" + bankNoCount).update(linkAvgJoint);

            $("cmbConsideredCtoForSEJtBank" + bankNoCount).setValue(Math.round(totalCtofor12Month.toFixed(2)));
            $("cmbConsideredBalanceForSEJtBank" + bankNoCount).setValue(Math.round(avergTotalCto12Monthjoint));
            $("txtConsiderMarginForSEJtBank" + bankNoCount).setValue("3");
            var grossJtincome = parseFloat((totalCtofor12Month * 3) / 100);
            $("txtgrossincomeForSEJtBank" + bankNoCount).setValue(Math.round(grossJtincome.toFixed(2)));
            $("txtNetAfterPaymentForSEJtBank" + bankNoCount).setValue(Math.round(grossJtincome.toFixed(2)));
            var shareJt = $F("txtShareForSEJtBank" + bankNoCount);
            var netIncomeTotalJt = parseFloat((grossJtincome * parseFloat(shareJt)) / 100);
            $("txtNetincomeForSEJtBank" + bankNoCount).setValue(Math.round(netIncomeTotalJt.toFixed(2)));
            $("cmbIncomeAssessmentForSEJtBank" + bankNoCount).update(Page.incomeAssesmentLinkOption);
            $("cmbIndustryForSEJtBank" + bankNoCount).update(Page.sectorLinkOption);

            AnalystDetailsCalculationHelper.setBankForBussinessManAndSelfEmployedCalculation(bankNoCount, 'Jt', 'SE');
        }

    },

    //Set Bank No Calculation
    setBankForBussinessManAndSelfEmployedCalculation: function(bankno, accountype, tabName) {

        var income = 0;
        var assessmentName = "";
        var assesmentCode = "";
        var employerName = "";
        var employerSubsector = "";
        var employerCode = "";
        var consider = 0;
        var bankName = "";
        var bankId = "0";
        var assMethodId = "0";
        var assCatId = "0";
        var averageBalance = 0;

        if (accountype == "Pr" && tabName == "BM") {
            income = $F("txtNetincomeForPrimaryBank" + bankno);
            $('txtIncomeBForLCPrBank' + bankno).setValue(income);

            averageBalance = $F("txtAverageBalanceFor12MonthBank" + bankno);
            $('txtAverageBalanceIdForLcPrBank' + bankno).setValue(averageBalance);

            if ($F('cmbIncomeAssessmentForPrimaryBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForPrimaryBank' + bankno).options[$('cmbIncomeAssessmentForPrimaryBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCPrBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForPrimaryBank' + bankno);
            $('txtAssCodeForLCPrBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForPrimaryBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForPrimaryBank' + bankno).options[$('cmbIndustryForPrimaryBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForPrimaryBank' + bankno).options[$('cmbSubsectorForPrimaryBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForPrimaryBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForPrimaryBank" + bankno);
            $('txtEmployerCatCodeForLCPrBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCPrBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForBank' + bankno + 'IncomeGenerator').options[$('cmbBankNameForBank' + bankno + 'IncomeGenerator').selectedIndex].text;
            $('tdBank' + bankno + 'ForLCPr').innerHTML = bankName;

            bankId = $F('cmbBankNameForBank' + bankno + 'IncomeGenerator');
            $('txtbankIdForLcPrBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForPrimaryBank' + bankno);
            $('txtAssMethodIdForLcPrBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForPrimaryBank' + bankno);
            $('txtAssCatIdForLcPrBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorPr').style.display = "";

        }
        else if (accountype == "Jt" && tabName == "BM") {
            income = $F("txtNetIncomeForJointBank" + bankno);
            $('txtIncomeBForLCJtBank' + bankno).setValue(income);
            if ($F('cmbIncomeAssementForJointBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssementForJointBank' + bankno).options[$('cmbIncomeAssementForJointBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCJtBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssementCodeForJointBank' + bankno);
            $('txtAssCodeForLCJtBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForJointBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForJointBank' + bankno).options[$('cmbIndustryForJointBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubSectorForJointBank' + bankno).options[$('cmbSubSectorForJointBank' + bankno).selectedIndex].text;
                if ($F('cmbSubSectorForJointBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtSubSectorCode1ForJointBank" + bankno);
            $('txtEmployerCatCodeForLCJtBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCJtBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankName1ForJointBank' + bankno).options[$('cmbBankName1ForJointBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCJt').innerHTML = bankName;
            $("bmjttabBank" + bankno).innerHTML = bankName;

            bankId = $F('cmbBankName1ForJointBank' + bankno);
            $('txtbankIdForLcJtBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssementForJointBank' + bankno);
            $('txtAssMethodIdForLcJtBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForJointBank' + bankno);
            $('txtAssCatIdForLcJtBank' + bankno).setValue(assCatId);
            $('trBank' + bankno + 'ForLoanCalculatorJt').style.display = "";
        }
        else if (accountype == "Pr" && tabName == "SE") {
            income = $F("txtNetincomeForSEPrBank" + bankno);
            $('txtIncomeBForLCPrBank' + bankno).setValue(income);

            averageBalance = $F("txtAverageBalanceForSEPr12MonthBank" + bankno);
            $('txtAverageBalanceIdForLcPrBank' + bankno).setValue(averageBalance);

            if ($F('cmbIncomeAssessmentForSEPrBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForSEPrBank' + bankno).options[$('cmbIncomeAssessmentForSEPrBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCPrBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForSEPrBank' + bankno);
            $('txtAssCodeForLCPrBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForSEPrBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForSEPrBank' + bankno).options[$('cmbIndustryForSEPrBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForSEPrBank' + bankno).options[$('cmbSubsectorForSEPrBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForSEPrBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForSEPrBank" + bankno);
            $('txtEmployerCatCodeForLCPrBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCPrBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForSEBank' + bankno).options[$('cmbBankNameForSEBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCPr').innerHTML = bankName;

            bankId = $F('cmbBankNameForSEBank' + bankno);
            $('txtbankIdForLcPrBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForSEPrBank' + bankno);
            $('txtAssMethodIdForLcPrBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForSEPrBank' + bankno);
            $('txtAssCatIdForLcPrBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorPr').style.display = "";
        }
        else if (accountype == "Jt" && tabName == "SE") {
            income = $F("txtNetincomeForSEJtBank" + bankno);
            $('txtIncomeBForLCJtBank' + bankno).setValue(income);
            if ($F('cmbIncomeAssessmentForSEJtBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForSEJtBank' + bankno).options[$('cmbIncomeAssessmentForSEJtBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCJtBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForSEJtBank' + bankno);
            $('txtAssCodeForLCJtBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForSEJtBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForSEJtBank' + bankno).options[$('cmbIndustryForSEJtBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForSEJtBank' + bankno).options[$('cmbSubsectorForSEJtBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForSEJtBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForSEJtBank" + bankno);
            $('txtEmployerCatCodeForLCJtBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCJtBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCJtBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForSEJtBank' + bankno).options[$('cmbBankNameForSEJtBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCJt').innerHTML = bankName;

            bankId = $F('cmbBankNameForSEJtBank' + bankno);
            $('txtbankIdForLcJtBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForSEJtBank' + bankno);
            $('txtAssMethodIdForLcJtBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForSEJtBank' + bankno);
            $('txtAssCatIdForLcJtBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorJt').style.display = "";
        }

        AnalystDetailsCalculationHelper.CalculateLoanCalculator(accountype);


    },

    //Calculate Loan Calculator
    CalculateLoanCalculator: function(accountType) {
        var totalIncome = 0;
        var income = 0;
        var isConsider = 0;
        var declaredPrIncome = 0;
        var declaredJtIncome = 0;
        var totalJointIncome = 0;
        Page.incomeArrayforlc = [];
        Page.newIncomeArrayforAll = [];

        if (accountType == "Pr") {
            var tablePrAccount = $("tblPrLoanCalculator");
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                income = 0;
                var obj = new Object();
                if (i < 7) {
                    isConsider = $F("cmbConsiderForLCPrBank" + i);
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrBank" + i);
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrBank" + i);
                        obj.AssessmentCode = $F("txtAssCodeForLCPrBank" + i);
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrBank" + i);
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrBank" + i);
                        obj.AssmentMethodId = $F("txtAssMethodIdForLcPrBank" + i);
                        obj.AssCatId = $F("txtAssCatIdForLcPrBank" + i);
                        obj.AverageBalance = $F("txtAverageBalanceIdForLcPrBank" + i);
                        var prProfession = $F("txtPrProfessionId");
                        var otherProfession = $F("txtPrOtherProfessionId");
                        if (prProfession == "1" || otherProfession == "1") {
                            obj.Profession = "1";
                        }
                        else {
                            obj.Profession = "3";
                        }

                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);

                    }
                    else {
                        //;
                    }
                }
                else if (i == 7) {
                    isConsider = $F("cmbConsiderForLCPrSalary");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrSalary");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrSalary");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrSalary");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrSalary");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrSalary");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "2"; //Profession
                        obj.Profession = "2";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                    else {
                        //;
                    }
                }
                else if (i == 8) {
                    isConsider = $F("cmbConsiderForLCPrRent");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrRent");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrRent");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrRent");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrRent");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrRent");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "5"; //Profession
                        obj.Profession = "5";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);

                    }
                    else {
                        //;
                    }
                }
                else if (i == 9) {
                    isConsider = $F("cmbConsiderForLCPrDoctor");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrDoctor");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrDoctor");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrDoctor");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrDoctor");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrDoctor");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "4"; //Profession
                        obj.Profession = "4";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                }
                else if (i == 10) {
                    isConsider = $F("cmbConsiderForLCPrTeacher");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrTeacher");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrTeacher");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrTeacher");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrTeacher");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrTeacher");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "150"; //Subsector
                        obj.Profession = 3;
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                }

                if (util.isFloat(income) && !util.isEmpty(income)) {
                    totalIncome += parseFloat(income);
                }
            }




            $('txtTotalInComeForLcAll').setValue(totalIncome);
            declaredPrIncome = $F("txtDeclaredIncomeForLcAll");
            if (util.isFloat(declaredPrIncome) && !util.isEmpty(declaredPrIncome)) {
                if (totalIncome > parseFloat(declaredPrIncome)) {
                    $('txtAppropriteIncomeForLcAll').setValue(declaredPrIncome);
                }
                else {
                    $('txtAppropriteIncomeForLcAll').setValue(totalIncome);
                }
            }
            else {
                $('txtAppropriteIncomeForLcAll').setValue('0');
            }

        }
        else {
            var tableJtAccount = $("tblJtLoanCalculator");
            var rowCountJt = tableJtAccount.rows.length;
            for (var i = 1; i <= rowCountJt - 1; i++) {
                income = 0;
                if (i < 7) {
                    isConsider = $F("cmbConsiderForLCJtBank" + i);
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtBank" + i);
                    }
                }
                else if (i == 7) {
                    isConsider = $F("cmbConsiderForLCJtSalary");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtSalary");
                    }
                }
                else if (i == 8) {
                    isConsider = $F("cmbConsiderForLCJtRent");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtRent");
                    }
                }
                else if (i == 9) {
                    isConsider = $F("cmbConsiderForLCJtDoctor");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtDoctor");
                    }
                }
                else if (i == 10) {
                    isConsider = $F("cmbConsiderForLCJtTeacher");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtTeacher");
                    }
                }



                if (util.isFloat(income) && !util.isEmpty(income)) {
                    totalIncome += parseFloat(income);
                }
            }
            $('txtTotalIncomeForLcJointAll').setValue(totalIncome);

            declaredJtIncome = $F("txtDeclaredIncomeForLcJointAll");
            if (util.isFloat(declaredJtIncome) && !util.isEmpty(declaredJtIncome)) {
                if (totalIncome > parseFloat(declaredJtIncome)) {
                    $('txtAppropriteIncomeForLcJointAll').setValue(declaredJtIncome);
                }
                else {
                    $('txtAppropriteIncomeForLcJointAll').setValue(totalIncome);
                }
            }
            else {
                $('txtAppropriteIncomeForLcJointAll').setValue('0');
            }
        }

        var reasonofJointApplicant = $F("cmbReasonofJointApplicant");
        if (reasonofJointApplicant == "1") {
            var appropripteIncomePr = $F("txtAppropriteIncomeForLcAll");
            var appropriteIncomeJt = $F("txtAppropriteIncomeForLcJointAll");

            if (util.isFloat(appropripteIncomePr) && !util.isEmpty(appropripteIncomePr)) {
                totalJointIncome += parseFloat(appropripteIncomePr);
            }
            if (util.isFloat(appropriteIncomeJt) && !util.isEmpty(appropriteIncomeJt)) {
                totalJointIncome += parseFloat(appropriteIncomeJt);
            }
            $('txtTotalJointInComeForLcWithCal').setValue(totalJointIncome);

        }
        else {
            var appropripteIncome = $F("txtAppropriteIncomeForLcAll");
            if (util.isFloat(appropripteIncome) && !util.isEmpty(appropripteIncome)) {
                totalJointIncome += parseFloat(appropripteIncome);
            }
            $('txtTotalJointInComeForLcWithCal').setValue(totalJointIncome);
        }

        analystDetailsHelper.changeCashSecured();
        var dbrLevel = $F("cmbDBRLevel");
        if (dbrLevel == "" || dbrLevel == "1") {
            $("txtAppropriateDbr").setValue('50');
        }
        analystDetailsHelper.changeConsideredDbr();
    },

    //Populate Incom Bank Statement Sum
    populateIncomBankStatementSum: function(res) {

        var isJoint = res.value.objAutoLoanMaster.JointApplication;
        for (var i = 0; i < res.value.objAutoAnalystIncomBankStatementSum.length; i++) {
            //Populate Bussiness Man Tab
            if (res.value.objAutoAnalystIncomBankStatementSum[i].TabType == 1) {
                //Populate Bussiness Man Tab For Primary
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 1) {
                    AnalystDetailsCalculationHelper.populateIncomBankStatementSumForPrBussinessMan(res, i);
                }
                //Populate Bussiness Man Tab For Joint
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 2 && isJoint == true) {
                    AnalystDetailsCalculationHelper.populateIncomBankStatementSumForJtBussinessMan(res, i);
                }
            }
            //Populate Self Employed Tab
            if (res.value.objAutoAnalystIncomBankStatementSum[i].TabType == 2) {
                //Populate Self Employed Tab for Primary Tab
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 1) {
                    AnalystDetailsCalculationHelper.populateIncomBankStatementSumForPrSelfEmployed(res, i);
                }
                //Populate Self Employed Tab For Joint Tab
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Type == 2 && isJoint == true) {
                    AnalystDetailsCalculationHelper.populateIncomBankStatementSumForJtSelfEmployed(res, i);
                }
            }
        }
    },

    //Populate Bussiness Man Tab For Primary IncomBankStatementSum
    populateIncomBankStatementSumForPrBussinessMan: function(res, i) {
        var identityFieldForBmPr = 0;

        for (var j = 0; j < Page.prCount; j++) {
            identityFieldForBmPr = j + 1;
            var bankStatementId = parseInt($F("txtBankStatementIdBMPr" + identityFieldForBmPr));
            if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                $('tarearemarksForIncomeGeneratorBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                $('txtTotalCTOFor12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                $('txtAverageCTO12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                $('txtAverageBalanceFor12MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                $('txtTotalCTOFor6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                $('txtAverageCTO6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                $('txtAverageBalanceFor6MonthBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                $('cmbConsideredCtoForPrBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                $('cmbConsideredBalanceForPrBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                $('txtConsiderMarginForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                $('txtgrossincomeForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                $('txtNetAfterPaymentForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                $('txtShareForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                $('txtNetincomeForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                $('cmbIncomeAssessmentForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                var incomeAssBussPr = Page.incomeAssementArray.findByProp('INAM_ID', res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                if (incomeAssBussPr) {
                    $('txtIncomeAssessmentCodeForPrimaryBank' + identityFieldForBmPr).setValue(incomeAssBussPr.INAM_METHODCODE);
                }
                $('cmbIndustryForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                analystDetailsManager.changeSector(identityFieldForBmPr, 'Pr');
                $('cmbSubsectorForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                analystDetailsHelper.changeSubSector(identityFieldForBmPr, 'Pr');
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                    $('divAverageBalancePr' + identityFieldForBmPr).style.display = 'block';
                    $('txt1st9monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                    $('txt1st6MonthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                    $('txtLast6monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                    $('txtLast3monthForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                    $('txtConsiderednarginForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                }

                //Previous Repayment History Bussiness Man Tab IncomBankStatementSum 
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                    $('divPrevRepHis' + identityFieldForBmPr).style.display = 'block';
                    AnalystDetailsCalculationHelper.PopulatePrevRePyHisPrBmIncomBankStatementSum(res, i, identityFieldForBmPr);
                }

                //populate Over Draft Bussiness Man Tab IncomBankStatementSum
                AnalystDetailsCalculationHelper.PopulateOverDraftPrBmIncomBankStatementSum(res, i);

                analystDetailsHelper.changeIncomeAssementForBusssinessMan(identityFieldForBmPr, 'Pr');
                break;
            }
        }
    },

    //Primary Previous Repayment History Bussiness Man Tab IncomBankStatementSum
    PopulatePrevRePyHisPrBmIncomBankStatementSum: function(res, i, identityFieldForBmPr) {

        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {

            $('txttotalForOffSCBForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
            $('txtTotal1OnSCBRepHisForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
            $('txtTotalEMIClosingConsideredForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
            $('txtremarks1ForPrimaryBank' + identityFieldForBmPr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                    var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                    if (parseInt(bankId) < 0) {
                        var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var emi = $F("txtEmi" + hs + "ForOnScbForPrimaryBank" + identityFieldForBmPr);
                            if (parseFloat(emi) == emidb) {
                                $('cmbConsider' + hs + "ForOnSCBReplHisForPrimaryBank" + identityFieldForBmPr).setValue('1');
                                break;
                            }
                        }

                    }
                    else {
                        var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var bankIdtxt = $F("txtBank" + hs + "BMPr" + identityFieldForBmPr);
                            if (parseInt(bankIdtxt) == bankId) {
                                $('cmbConsider' + hs + "ForPrimaryOffScbBank" + identityFieldForBmPr).setValue('1');
                                break;
                            }
                        }
                    }
                }
            }

        }
    },

    //populate Primary Over Draft Bussiness Man Tab IncomBankStatementSum
    PopulateOverDraftPrBmIncomBankStatementSum: function(res, i) {
        var identityFieldForBmPr = 0;
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
            for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                identityFieldForBmPr = ov + 1;
                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                    var tablePrAccount = $("tblOverDraftFacilitiesForPrBMWithScb" + identityFieldForBmPr);
                    var rowCount = tablePrAccount.rows.length;
                    for (var t = 1; t <= rowCount - 1; t++) {
                        var interestovd = parseFloat($F("txtInterst" + t + "WithScbForBank" + identityFieldForBmPr)).toFixed(2);
                        var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForBank" + identityFieldForBmPr)).toFixed(2);
                        var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                        var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                        if (interestovd == interestDb && LimitAmount == limitdb) {
                            $('cmbConsider' + t + "ForBank" + identityFieldForBmPr).setValue('1');
                            break;
                        }
                    }
                }
                else {
                    var tablePrAccountOffScb = $("tblOverDraftFacilitiesForPrBMOffScb" + identityFieldForBmPr);
                    var rowCountOffScb = tablePrAccountOffScb.rows.length;
                    for (var t = 1; t <= rowCountOffScb - 1; t++) {
                        var bankId = parseFloat($F("cmbBankName" + t + "ForBussinessTabPrBank" + identityFieldForBmPr));
                        var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                        if (bankId == bankIdDb) {
                            $('cmbConsider' + t + "ForOffScbBank" + identityFieldForBmPr).setValue('1');
                            break;
                        }
                    }
                }
            }
        }
    },

    //Populate Bussiness Man Tab For Joint
    populateIncomBankStatementSumForJtBussinessMan: function(res, i) {

        var identityFieldForBmJt = 0;
        for (var j = 0; j < Page.jtCount; j++) {
            identityFieldForBmJt = j + 1;
            var bankStatementId = parseInt($F("txtBankStatementIdBMJt" + identityFieldForBmJt));
            if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                //Main Object
                $('txtRemark1ForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                $('txtTotalCTOFor12MonthJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                $('txtAverageCTOFor12MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                $('txtAverageBalance12MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                $('txtTotalCTOFor6MonthJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                $('txtAverageCTOFor6MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                $('txtAverageBalance6MonthForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                $('cmbConsideredCtoForJtBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                $('cmbConsideredBalanceForJtBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                $('txtConsideredMarginBMForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                $('txtgrossIncomeForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                $('txtNetAfterPaymentForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                $('txtShareForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                $('txtNetIncomeForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                $('cmbIncomeAssementForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                var incomeAssBussJt = Page.incomeAssementArray.findByProp('INAM_ID', res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                if (incomeAssBussJt) {
                    $('txtIncomeAssementCodeForJointBank' + identityFieldForBmJt).setValue(incomeAssBussJt.INAM_METHODCODE);
                }

                $('cmbIndustryForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                analystDetailsManager.changeSector(identityFieldForBmJt, 'Jt');
                $('cmbSubSectorForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                analystDetailsHelper.changeSubSector(identityFieldForBmJt, 'Jt');
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                    $('divAverageBalanceJoint' + identityFieldForBmJt).style.display = 'block';
                    $('txtAVGBalanceFor1st9MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                    $('txtAVGBalanceFor1st6MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                    $('txtAVGBalanceForLast6MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                    $('txtAVGBalanceForLast3MontForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                    $('txtConsideredMarginForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                }

                //Previous Repayment History
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                    $('divPrevRepHisJoint' + identityFieldForBmJt).style.display = 'block';
                    AnalystDetailsCalculationHelper.PopulatePrevRePyHisJtBmIncomBankStatementSum(res, i, identityFieldForBmJt);
                }

                //Over Draft Facility

                AnalystDetailsCalculationHelper.PopulateOverDraftJtBmIncomBankStatementSum(res, i);


                analystDetailsHelper.changeIncomeAssementForBusssinessMan(identityFieldForBmJt, 'Jt');
                break;
            }
        }
    },

    //Joint Previous Repayment History Bussiness Man Tab IncomeStatement Sum
    PopulatePrevRePyHisJtBmIncomBankStatementSum: function(res, i, identityFieldForBmJt) {

        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
            $('txtTotalOffSCBJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
            $('txtTotalOnScbForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
            $('txtTotalEMIClosingConsideredPrevRepHisForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
            $('txtRemarksPrevRepHisForJointBank' + identityFieldForBmJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                    var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                    if (parseInt(bankId) < 0) {
                        var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var emi = $F("txtEmiOnScb" + hs + "ForJointBank" + identityFieldForBmJt);
                            if (parseFloat(emi) == emidb) {
                                $('cmbConsiderOnScb' + hs + "ForJointBank" + identityFieldForBmJt).setValue('1');
                                break;
                            }
                        }

                    }
                    else {
                        var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var bankIdtxt = $F("txtBank" + hs + "BMJt" + identityFieldForBmJt);
                            if (parseInt(bankIdtxt) == bankId) {
                                $('cmbConsiderOffScb' + hs + "ForJointBank" + identityFieldForBmJt).setValue('1');
                                break;
                            }
                        }
                    }
                }
            }
        }
    },

    //Populate Joint Over Draft For Bussiness Man IncomeStatement Sum
    PopulateOverDraftJtBmIncomBankStatementSum: function(res, i) {

        var identityFieldForBmJt = 0;
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
            for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                identityFieldForBmJt = ov + 1;
                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                    var tablePrAccount = $("tblOverDraftFacilitiesForJtBMWithScb" + identityFieldForBmJt);
                    var rowCount = tablePrAccount.rows.length;
                    for (var t = 1; t <= rowCount - 1; t++) {
                        var interestovd = parseFloat($F("txtInterest" + t + "ForJointBank" + identityFieldForBmJt)).toFixed(2);
                        var LimitAmount = parseFloat($F("txtLimit" + t + "ForJointBank" + identityFieldForBmJt)).toFixed(2);
                        var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                        var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                        if (interestovd == interestDb && LimitAmount == limitdb) {
                            $('cmbInterest' + t + "ForJointBank" + identityFieldForBmJt).setValue('1');
                            break;
                        }
                    }
                }
                else {
                    var tablePrAccountOffScb = $("tblOverDraftFacilitiesForJtBMOffScb" + identityFieldForBmJt);
                    var rowCountOffScb = tablePrAccountOffScb.rows.length;
                    for (var t = 1; t <= rowCountOffScb - 1; t++) {
                        var bankId = parseFloat($F("cmbBankName" + t + "ForBussinessTabJtBank" + identityFieldForBmJt));
                        var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                        if (bankId == bankIdDb) {
                            $('cmbConsiderOffScbBM' + t + "ForJointBank" + identityFieldForBmJt).setValue('1');
                            break;
                        }
                    }
                }
            }
        }
    },

    //Populate Self Employed Tab for Primary IncomBankStatementSum
    populateIncomBankStatementSumForPrSelfEmployed: function(res, i) {

        var identityFieldForSePr = 0;
        for (var j = 0; j < Page.prCount; j++) {
            identityFieldForSePr = j + 1;
            var bankStatementId = parseInt($F("txtBankStatementIdSEPr" + identityFieldForSePr));
            if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                $('txtremarksForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                $('txtTotalCTOForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                $('txtAverageCTOForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                $('txtAverageBalanceForSEPr12MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                $('txtTotalCTOForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                $('txtAverageCTOForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                $('txtAverageBalanceForSEPr6MonthBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                $('cmbConsideredCtoForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                $('cmbConsideredBalanceForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                $('txtConsiderMarginForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                $('txtgrossincomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                $('txtNetAfterPaymentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                $('txtShareForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                $('txtNetincomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                $('cmbIncomeAssessmentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                var incomeAssSePr = Page.incomeAssementArray.findByProp('INAM_ID', res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                if (incomeAssSePr) {
                    $('txtIncomeAssessmentCodeForSEPrBank' + identityFieldForSePr).setValue(incomeAssSePr.INAM_METHODCODE);
                }
                $('cmbIndustryForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                analystDetailsManager.changeSectorForSelfEmployed(identityFieldForSePr, 'Pr');
                $('cmbSubsectorForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                analystDetailsHelper.changeSubSectorForSelfEmployed(identityFieldForSePr, 'Pr');
                $('cmbPrivateIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome);
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                    $('divAverageBalanceForSEPr' + identityFieldForSePr).style.display = 'block';
                    $('txt1st9monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                    $('txt1st6MonthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                    $('txtLast6monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                    $('txtLast3monthForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                    $('txtConsiderednarginForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                }

                //Previous Repayment History
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                    $('divPrevRepHisForSEPrBank' + identityFieldForSePr).style.display = 'block';
                    AnalystDetailsCalculationHelper.PopulatePrevRePyHisPrSeIncomBankStatementSum(res, i, identityFieldForSePr);
                }

                //OverDeraft facility


                AnalystDetailsCalculationHelper.PopulateOverDraftPrSeIncomBankStatementSum(res, i);

                analystDetailsHelper.changeIncomeAssementForSelfEmployed(identityFieldForSePr, 'Pr');
                //Auto_PracticingDoctor
                if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 143 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                    $('divChamberIncomeForSePrBank' + identityFieldForSePr).style.display = 'block';
                    AnalystDetailsCalculationHelper.populateAutoPrectricingDoctorForPrimary(res, i, identityFieldForSePr);
                    analystDetailsHelper.changePrivateInCome(identityFieldForSePr, 'Pr', 'Se');
                }
                //Auto_TutioningTeacher

                if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 150 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                    $('divTutionForSEPrBank' + identityFieldForSePr).style.display = 'block';
                    AnalystDetailsCalculationHelper.PopulateTutioningTeacher(res, i, identityFieldForSePr);
                    analystDetailsHelper.changePrivateInCome(identityFieldForSePr, 'Pr', 'Se');
                }

                break;
            }
        }
    },

    //Primary Previous Repayment History SelfEmployed Tab IncomBankStatementSum
    PopulatePrevRePyHisPrSeIncomBankStatementSum: function(res, i, identityFieldForSePr) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
            $('txttotalForOffSCBForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
            $('txtTotal1OnSCBRepHisForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
            $('txtTotalEMIClosingConsideredForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
            $('txtremarks1ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                    var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                    if (parseInt(bankId) < 0) {
                        var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var emi = $F("txtEMI" + hs + "ForOffSCBSEPrBank" + identityFieldForSePr);
                            if (parseFloat(emi) == emidb) {
                                $('cmbConsider' + hs + "ForOnSCBReplHisForSEPrBank" + identityFieldForSePr).setValue('1');
                                break;
                            }
                        }

                    }
                    else {
                        var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var bankIdtxt = $F("txtBank" + hs + "SEPr" + identityFieldForSePr);
                            if (parseInt(bankIdtxt) == bankId) {
                                $('cmbConsider' + hs + "ForSEPrOffScbBank" + identityFieldForSePr).setValue('1');
                                break;
                            }
                        }
                    }
                }
            }

        }
    },

    //populate Primary Over Draft Self Employed Tab IncomBankStatementSum
    PopulateOverDraftPrSeIncomBankStatementSum: function(res, i) {
        var identityFieldForSePr = 0;

        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
            for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                identityFieldForSePr = ov + 1;
                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                    var tablePrAccount = $("tblOverDraftFacilitiesForPrSEWithScb" + identityFieldForSePr);
                    var rowCount = tablePrAccount.rows.length;
                    for (var t = 1; t <= rowCount - 1; t++) {
                        var interestOvd = parseFloat($F("txtInterst" + t + "WithScbForSEBank" + identityFieldForSePr)).toFixed(2);
                        var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForSEBank" + identityFieldForSePr)).toFixed(2);
                        var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                        var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                        if (interestOvd == interestDb && LimitAmount == limitdb) {
                            $('cmbConsider' + t + "WithScbForSEBank" + identityFieldForSePr).setValue('1');
                            break;
                        }
                    }
                }
                else {
                    var tablePrAccountOffScb = $("tblOverDraftFacilitiesForPrSEOffScb" + identityFieldForSePr);
                    var rowCountOffScb = tablePrAccountOffScb.rows.length;
                    for (var t = 1; t <= rowCountOffScb - 1; t++) {
                        var bankId = parseFloat($F("cmbBankName" + t + "ForSEPrBank" + identityFieldForSePr));
                        var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                        if (bankId == bankIdDb) {
                            $('cmbConsider' + t + "ForSEOffScbBank" + identityFieldForSePr).setValue('1');
                            break;
                        }
                    }
                }
            }
        }
    },

    //Populate Auto Practicing Doctor For Primary
    populateAutoPrectricingDoctorForPrimary: function(res, i, identityFieldForSePr) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor != null) {
            $('txtTotalChamberIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalChamberIncome);
            $('txtTotalOtherIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.OtherOTIncome);
            $('txtTotalIncomeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalIncome);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome != null) {
                for (var ch = 0; ch < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome.length; ch++) {
                    var chamberId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].ChamberId;
                    $('txtPatientsOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldPatents);
                    $('txtFeeOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldFee);
                    $('txtTotalOld' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldTotal);
                    $('txtPatientsNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewPatents);
                    $('txtFeeNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewFee);
                    $('txtTotalNew' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewTotal);
                    $('txtTotalOldNew' + chamberId + 'ForSEPr1Bank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Total);
                    $('txtDayMonth' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].DayMonth);
                    $('txtIncome' + chamberId + 'ForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Income);
                }
            }
        }
    },

    //Populate Tutioning teacher For Primary
    PopulateTutioningTeacher: function(res, i, identityFieldForSePr) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome != null) {
            $('txtNoOfStudentForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfStudent);
            $('txtNoOfDaysForTutionForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfDaysForTution);
            $('txtTutionFeeForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.Fees);
            $('txtTotalInComeTutionForSEPrBank' + identityFieldForSePr).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.TotalIncome);
        }
    },

    //Populate Self Employed Tab for Joint IncomBankStatementSum
    populateIncomBankStatementSumForJtSelfEmployed: function(res, i) {

        var identityFieldForSeJt = 0;
        for (var j = 0; j < Page.jtCount; j++) {
            identityFieldForSeJt = j + 1;
            var bankStatementId = parseInt($F("txtBankStatementIdSEJt" + identityFieldForSeJt));
            if (bankStatementId == res.value.objAutoAnalystIncomBankStatementSum[i].BankStatementId) {
                //Main Object
                $('txtremarksForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Remarks);
                $('txtTotalCTOForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO12);
                $('txtAverageCTOForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO12);
                $('txtAverageBalanceForSEJt12MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance12);
                $('txtTotalCTOForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].TotalCTO6);
                $('txtAverageCTOForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgCTO6);
                $('txtAverageBalanceForSEJt6MonthBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance6);
                $('cmbConsideredCtoForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredCTO);
                $('cmbConsideredBalanceForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredBalance);
                $('txtConsiderMarginForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                $('txtgrossincomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].GrossIncome);
                $('txtNetAfterPaymentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetAfterPayment);
                $('txtShareForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Share);
                $('txtNetincomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].NetIncome);
                $('cmbIncomeAssessmentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                var incomeAssSeJt = Page.incomeAssementArray.findByProp('INAM_ID', res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod);
                if (incomeAssSeJt) {
                    $('txtIncomeAssessmentCodeForSEJtBank' + identityFieldForSeJt).setValue(incomeAssSeJt.INAM_METHODCODE);
                }
                $('cmbIndustryForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].Industry);
                analystDetailsManager.changeSectorForSelfEmployed(identityFieldForSeJt, 'Jt');
                $('cmbSubsectorForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].SubSector);
                analystDetailsHelper.changeSubSectorForSelfEmployed(identityFieldForSeJt, 'Jt');
                $('cmbPrivateIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome);
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 1) {
                    $('divAverageBalanceForSEJt' + identityFieldForSeJt).style.display = 'block';
                    $('txt1st9monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st9m);
                    $('txt1st6MonthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalance1st6m);
                    $('txtLast6monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast6m);
                    $('txtLast3monthForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AvgBalanceLast3m);
                    $('txtConsiderednarginForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].ConsideredMargin);
                }

                //Preveous Repayment History
                if (res.value.objAutoAnalystIncomBankStatementSum[i].IncomeAssessmentMethod == 7) {
                    $('divPrevRepHisForSEJtBank' + identityFieldForSeJt).style.display = 'block';
                    AnalystDetailsCalculationHelper.PopulatePrevRePyHisJtSeIncomBankStatementSum(res, i, identityFieldForSeJt);
                }

                //Over Draft Facility
                AnalystDetailsCalculationHelper.PopulateOverDraftJtSeIncomBankStatementSum(res, i);
                analystDetailsHelper.changeIncomeAssementForSelfEmployed(identityFieldForSeJt, 'Jt');
                //Auto_PracticingDoctor

                if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 143 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                    $('divChamberIncomeForSeJtBank' + identityFieldForSeJt).style.display = 'block';
                    AnalystDetailsCalculationHelper.populateAutoPrectricingDoctorForJoint(res, i, identityFieldForSeJt);
                    analystDetailsHelper.changePrivateInCome(identityFieldForSeJt, 'Jt', 'Se');
                }
                //Auto_TutioningTeacher

                if (res.value.objAutoAnalystIncomBankStatementSum[i].Industry == 12 && res.value.objAutoAnalystIncomBankStatementSum[i].SubSector == 150 && res.value.objAutoAnalystIncomBankStatementSum[i].PrivateIncome == 1) {
                    $('divTutionJtBank' + identityFieldForSeJt).style.display = 'block';
                    AnalystDetailsCalculationHelper.PopulateTutioningTeacherJoint(res, i, identityFieldForSeJt);
                    analystDetailsHelper.changePrivateInCome(identityFieldForSeJt, 'Jt', 'Se');
                }
                break;
            }
        }
    },

    //Joint Previous Repayment History SelfEmployed Tab IncomBankStatementSum
    PopulatePrevRePyHisJtSeIncomBankStatementSum: function(res, i, identityFieldForSeJt) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory != null) {
            $('txttotalForOffSCBForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OffScb_Total);
            $('txtTotal1OnSCBRepHisForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.OnScb_Total);
            $('txtTotalEMIClosingConsideredForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.TotalEMIClosingConsidered);
            $('txtremarks1ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.Remarks);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb != null) {
                for (var phd = 0; phd < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb.length; phd++) {
                    var bankId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].BankId;
                    if (parseInt(bankId) < 0) {
                        var emidb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var emi = $F("txtEmi" + hs + "ForOnScbForSEJtBank" + identityFieldForSeJt);
                            if (parseFloat(emi) == emidb) {
                                $('cmbConsider' + hs + "ForOnSCBReplHisForSEJtBank" + identityFieldForSeJt).setValue('1');
                                break;
                            }
                        }

                    }
                    else {
                        var emidboff = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPreviousRePaymentHistory.AutoSubPreviousRepaymentHistoryOffScbOnScb[phd].EMI;
                        for (var hs = 1; hs < 5; hs++) {
                            var bankIdtxt = $F("txtBank" + hs + "SEJt" + identityFieldForSeJt);
                            if (parseInt(bankIdtxt) == bankId) {
                                $('cmbConsider' + hs + "ForSEJtRepHisOffScbBank" + identityFieldForSeJt).setValue('1');
                                break;
                            }
                        }
                    }
                }
            }

        }
    },

    //Populate Joint Over Draft For Self Employed IncomeStatement Sum
    PopulateOverDraftJtSeIncomBankStatementSum: function(res, i) {
        var identityFieldForSeJt = 0;
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility != null) {
            for (var ov = 0; ov < res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility.length; ov++) {
                identityFieldForSeJt = ov + 1;
                if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId < 0) {
                    var tablePrAccount = $("tblOverDraftFacilitiesForJtSEWithScb" + identityFieldForSeJt);
                    var rowCount = tablePrAccount.rows.length;
                    for (var t = 1; t <= rowCount - 1; t++) {
                        var interestovd = parseFloat($F("txtInterst" + t + "WithScbForSEJtBank" + identityFieldForSeJt)).toFixed(2);
                        var LimitAmount = parseFloat($F("txtLimt" + t + "WithScbForSEJtBank" + identityFieldForSeJt)).toFixed(2);
                        var limitdb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].LimitAmount.toFixed(2);
                        var interestDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].Interest.toFixed(2);
                        if (interestovd == interestDb && LimitAmount == limitdb) {
                            $('cmbConsider' + t + "WithScbForSEJtBank" + identityFieldForSeJt).setValue('1');
                            break;
                        }
                    }
                }
                else {
                    var tablePrAccountOffScb = $("tblOverDraftFacilitiesForJtSEOffScbBank" + identityFieldForSeJt);
                    var rowCountOffScb = tablePrAccountOffScb.rows.length;
                    for (var t = 1; t <= rowCountOffScb - t; i++) {
                        var bankId = parseFloat($F("cmbBankName" + t + "ForSEJtBank" + identityFieldForSeJt));
                        var bankIdDb = res.value.objAutoAnalystIncomBankStatementSum[i].AutoAnOverDraftFacility[ov].BankId;
                        if (bankId == bankIdDb) {
                            $('cmbConsider' + t + "ForSEJtOffScbBank" + identityFieldForSeJt).setValue('1');
                            break;
                        }
                    }
                }
            }
        }
    },

    //Populate Auto Practicing Doctor For Joint
    populateAutoPrectricingDoctorForJoint: function(res, i, identityFieldForSeJt) {
        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor != null) {
            $('txtTotalChamberIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalChamberIncome);
            $('txtTotalOtherIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.OtherOTIncome);
            $('txtTotalIncomeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.TotalIncome);
            if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome != null) {
                for (var ch = 0; ch < res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome.length; ch++) {
                    var chamberId = res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].ChamberId;
                    $('txtPatientsOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldPatents);
                    $('txtFeeOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldFee);
                    $('txtTotalOld' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].OldTotal);
                    $('txtPatientsNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewPatents);
                    $('txtFeeNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewFee);
                    $('txtTotalNew' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].NewTotal);
                    $('txtTotalOldNew' + chamberId + 'ForSEJt1Bank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Total);
                    $('txtDayMonth' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].DayMonth);
                    $('txtIncome' + chamberId + 'ForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoPracticingDoctor.AutoChamberIncome[ch].Income);
                }
            }
        }
    },

    //Populate Tutioning teacher For Joint
    PopulateTutioningTeacherJoint: function(res, i, identityFieldForSeJt) {

        if (res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome != null) {
            $('txtNoOfStudentForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfStudent);
            $('txtNoOfDaysForTutionForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.NoOfDaysForTution);
            $('txtTutionFeeForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.Fees);
            $('txtTotalInComeTutionForSEJtBank' + identityFieldForSeJt).setValue(res.value.objAutoAnalystIncomBankStatementSum[i].AutoTeacherIncome.TotalIncome);
        }
    },

    //Populate Segment Applicant Income list
    populateSegmentApplicantIncomelist: function(res) {
        for (var i = 0; i < res.value.Auto_An_IncomeSegmentApplicantIncomeList.length; i++) {

            if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].ApplicantType == 1) {
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource > 0) {
                    var identityFieldForLcPr = 0;
                    for (var j = 0; j < Page.prCount; j++) {
                        identityFieldForLcPr = j + 1;
                        var bankId = parseInt($F("txtbankIdForLcPrBank" + identityFieldForLcPr));
                        if (bankId == res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource) {
                            $('cmbConsiderForLCPrBank' + identityFieldForLcPr).setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                            break;
                        }
                    }
                }

                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -7) {
                    $('cmbConsiderForLCPrSalary').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -8) {
                    $('cmbConsiderForLCPrRent').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -9) {
                    $('cmbConsiderForLCPrDoctor').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -10) {
                    $('cmbConsiderForLCPrTeacher').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr');

            }
            else if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].ApplicantType == 2) {
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource > 0) {
                    var identityFieldForLcJt = 0;
                    for (var j = 0; j < Page.jtCount; j++) {
                        identityFieldForLcJt = j + 1;
                        var bankId = parseInt($F("txtbankIdForLcJtBank" + identityFieldForLcJt));
                        if (bankId == res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource) {
                            $('cmbConsiderForLCJtBank' + identityFieldForLcJt).setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                            break;
                        }
                    }
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -7) {
                    $('cmbConsiderForLCJtSalary').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -8) {
                    $('cmbConsiderForLCJtRent').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -9) {
                    $('cmbConsiderForLCJtDoctor').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                if (res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].CalculationSource == -10) {
                    $('cmbConsiderForLCJtTeacher').setValue(res.value.Auto_An_IncomeSegmentApplicantIncomeList[i].Consider);
                }
                AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt');

            }
        }
    },

    //Populate Segment Income
    populateSegmentIncome: function(res) {
        if (res.value.objAuto_An_IncomeSegmentIncome != null) {
            $('cmbEmployeeSegmentLc').setValue(res.value.objAuto_An_IncomeSegmentIncome.EmployeeSegment);
            $('txtSegmentCodeLc').setValue(res.value.objAuto_An_IncomeSegmentIncome.EmployeeSegmentCode);
            if (res.value.AutoLoanAnalystApplication.CashSecured100Per == 1) {
                $('cmbAssessmentMethodForLcCash').setValue(res.value.objAuto_An_IncomeSegmentIncome.AssesmentMethod);
                analystDetailsHelper.changeIncomeAssementMethodForCashSequre();

                $('cmbCategoryForLcCash').setValue(res.value.objAuto_An_IncomeSegmentIncome.Category);
                analystDetailsHelper.changeCategoryForCashSequre();
            }

        }
    }


}; 