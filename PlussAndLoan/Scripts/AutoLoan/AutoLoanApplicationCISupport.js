﻿var ReasonOfJoint = { 'Select a reason': 1, 'Income Consideration': 1, 'Joint Registration': 2, 'Business is in Joint Name': 3, 'Customer is Mariner': 4, 'AC is in Joint Name': 5, 'Others': 6 };
var vehicleType = { 'Sedan': 'S1', 'Station wagon': 'S2', 'SUV': 'S3', 'Pick-Ups': 'S4', 'Microbus': 'S5', 'MPV': 'S6' };
/// <reference path="../activatables.js" />
var AccountType = { 1: 'Company', 2: 'Personal'};
var Page = {
    sourceForGeneralArray: [],

    facilityArray: [],
    facilityListArray: [],
    OfffacilityListArray: [],
    bankStatementArray: [],
    subsectorArray: [],
    incomeAssementArray: [],
    isAverageBalancePrimaryArray: [],
    isAverageBalanceJointArray: [],
    vehicleDetailsArray: null,

    pageSize: 10,
    pageNo: 0,
    pager: null,
    LoanSummeryArray: [],
    loanApplication: null,
    insuranceType: [],
    vendoreArray: [],
    manufactureArray: [],
    vehicleId: 0,
    modelArray: [],
    vehicleStatusArray: [],


    init: function() {
        util.prepareDom();
        Event.observe('cmbReasonofJointApplicant', 'change', analyzeLoanApplicationHelper.showJointApplicant);
        Event.observe('cmbCashSecured', 'change', analyzeLoanApplicationHelper.changeCashSecured);
        Event.observe('cmbARTA', 'change', analyzeLoanApplicationHelper.changeARTA);
        Event.observe('cmbCCBundle', 'change', analyzeLoanApplicationHelper.changeCCBundle);
        Event.observe('cmsSeatingCapacity', 'change', analyzeLoanApplicationHelper.changeSeatingCapacity);
        Event.observe('cmbCarVerification', 'change', analyzeLoanApplicationHelper.changeCarVerification);
        Event.observe('cmbSource1', 'change', analyzeLoanApplicationManager.changeBranchNameForSourceOnApplicantDetailsForGeneral);
        Event.observe('cmbBrowingRelationship', 'change', analyzeLoanApplicationHelper.changeBrowingRelationship);
        Event.observe('cmbARTAReference', 'change', analyzeLoanApplicationManager.SetGeneralInsurance);
        Event.observe('cmbCashSecured', 'change', analyzeLoanApplicationHelper.changeCashSequred);
        Event.observe('txtSequrityValue', 'change', analyzeLoanApplicationHelper.changeSequrityValue);
        Event.observe('cmbVendor', 'change', analyzeLoanApplicationHelper.changeVendore);
        Event.observe('cmbManufacturer', 'change', analyzeLoanApplicationHelper.changeManufacture);
        Event.observe('txtregistrationYear', 'change', analyzeLoanApplicationHelper.changeregistationyear);
        Event.observe('txtManufacturaryear', 'change', analyzeLoanApplicationHelper.changeManufactureryear);
        Event.observe('txtQuotedPrice', 'change', analyzeLoanApplicationHelper.changeQuotedPrice);
        Event.observe('txtConsideredprice', 'change', analyzeLoanApplicationHelper.changeConsideredProce);
        Event.observe('cmdVehicleStatus', 'change', analyzeLoanApplicationHelper.changeVehicleStatus);
        Event.observe('cmbBranch', 'change', analyzeLoanApplicationHelper.changeSourceNameForSourceCodeOnApplicantDetailsForGeneral);
        Event.observe('lnkSave', 'click', analyzeLoanApplicationHelper.ValidateSave);


        //Event.observe('lnkPrint', 'click', LoanApplicationManager.printPreview);
        Event.observe('lnkClose', 'click', analyzeLoanApplicationManager.CloseLoanApp);
        Event.observe('lnkForward', 'click', analyzeLoanApplicationManager.ForwordLoanApplication);
        Event.observe('lnkDefer', 'click', analyzeLoanApplicationManager.HoldLoanApplication);
        Event.observe('lnkBackward', 'click', analyzeLoanApplicationManager.BackwardLoanApplication);

        Event.observe('cmbGeneralInsurencePreference', 'change', analyzeLoanApplicationHelper.changeGeneralInsurence);
        Event.observe('cmbARTAReference', 'change', analyzeLoanApplicationHelper.changeLifeInsurence);


        analyzeLoanApplicationManager.getVehicleStatus();
        analyzeLoanApplicationManager.getManufacturer();
        analyzeLoanApplicationManager.getVendor();
        analyzeLoanApplicationManager.GetAllAutoBranch();
        analyzeLoanApplicationManager.getBank();
        analyzeLoanApplicationManager.getFacility();
        analyzeLoanApplicationManager.getGeneralInsurance();
        //analyzeLoanApplicationManager.getSector();
        //analyzeLoanApplicationManager.getIncomeAssement();



        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNo'),
            spnTotalPage: $('spntotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });


        analyzeLoanApplicationManager.getLoanSummary();


    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        analyzeLoanApplicationManager.getLoanSummary(Page.pageNo, Page.pageSize);
    }





};

var analyzeLoanApplicationManager = {

    BackwardLoanApplication: function() {
        var loanMasterID = $F('txtAutoLoanMasterId');
        var res = PlussAuto.AutoLoanApplicationCISupport.BackwardLoanApp(loanMasterID);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Loan Application Successfully Backwarded.");
            analyzeLoanApplicationManager.getLoanSummary();
            analyzeLoanApplicationManager.CloseLoanApp();

        } else {
            alert("Process Failed");
        }
    },
    CheckMouByVendoreId: function() {
        var vendoreId = $F("cmbVendor");
        var res = PlussAuto.AutoLoanApplicationCISupport.CheckMouByVendoreId(vendoreId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value.VendoreRelationshipStatus != 1 && res.value.VendoreRelationshipStatus != 0) {
            alert("The Vendore is Negative Listed");
            $('cmbVendoreRelationShip').setValue('2');
        }
        else {
            $('cmbVendoreRelationShip').setValue(res.value.VendoreRelationshipStatus);
        }

    },


    SetGeneralInsurance: function() {

        var insuranceID = $F('cmbARTAReference');
        var insurance = Page.insuranceType.findByProp("InsCompanyId", insuranceID);
        if (insurance) {
            $('cmbGeneralInsurence').setValue(insurance.InsType);
        }
    },

    getGeneralInsurance: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanApplicationCISupport.getGeneralInsurance();
        //var insType = res.value[i].InsType;

        Page.insuranceType = res.value;

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbGeneralInsurencePreference').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'No Insurance';
        ExtraOpt.value = '-1';
        document.getElementById('cmbGeneralInsurencePreference').options.add(ExtraOpt);

        document.getElementById('cmbARTAReference').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'No Insurance';
        ExtraOpt.value = '-1';
        document.getElementById('cmbARTAReference').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            var opt1 = document.createElement("option");
            var insType = res.value[i].InsType;
            if (insType == '1' || insType == '3') {
                opt.text = res.value[i].CompanyName;
                opt.value = res.value[i].InsCompanyId;
                document.getElementById('cmbGeneralInsurencePreference').options.add(opt);
            }
            if (insType == '2' || insType == '3') {
                opt1.text = res.value[i].CompanyName;
                opt1.value = res.value[i].InsCompanyId;

                document.getElementById('cmbARTAReference').options.add(opt1);
            }


        }
    },



    HoldLoanApplication: function() {
        var loanMasterID = $F('txtAutoLoanMasterId');
        var res = PlussAuto.AutoLoanApplicationCISupport.HoldLoanApp(loanMasterID);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Loan Application Successfully Holded.");
            analyzeLoanApplicationManager.getLoanSummary();
            analyzeLoanApplicationManager.CloseLoanApp();
            

        } else {
            alert("Process Failed");
        }
    },



    ForwordLoanApplication: function() {
        //Analyst Master

//        if ($F("cmbColleterization") == "-1" && $F("cmbColleterizationWith") != "-1") {
//            alert("Please select Colleterization With");
//            //$("cmbColleterizationWith").focus();
//            return false;
//        }
//        if ($F("cmbColleterization") != "-1" && $F("cmbColleterizationWith") == "-1") {
//            alert("Please select Colleterization");
//            //$("cmbColleterization").focus();
//            return false;
//        }


//        var objAutoAnalistMaster = new Object();


//        objAutoAnalistMaster.AnalystMasterId = $F('txtAnalystMasterID');
//        objAutoAnalistMaster.LlId = $F('txtLLID');
//        objAutoAnalistMaster.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//        objAutoAnalistMaster.IsJoint = $F('txtIsJoint') == '0' ? false : true;
//        objAutoAnalistMaster.ReasonOfJoint = $('cmbReasonofJointApplicant').options[$('cmbReasonofJointApplicant').selectedIndex].text;
//        objAutoAnalistMaster.Colleterization = $F("cmbColleterization");
//        objAutoAnalistMaster.ColleterizationWith = $F("cmbColleterizationWith");


//        // Application Details

//        var objAutoLoanAnalystApplication = new Object();

//        objAutoLoanAnalystApplication.Id = $F('txtloanAnalystApplicationID');
//        objAutoLoanAnalystApplication.AnalystMasterId = $F('txtAnalystMasterID');
//        objAutoLoanAnalystApplication.Source = $F('cmbSource1');
//        objAutoLoanAnalystApplication.BranchId = $F('cmbBranch');
//        objAutoLoanAnalystApplication.BranchCode = $F('txtBranchCode');
//        objAutoLoanAnalystApplication.ValuePack = $F('cmbValuePack');
//        objAutoLoanAnalystApplication.BorrowingRelationShip = $F('cmbBrowingRelationship');
//        objAutoLoanAnalystApplication.BorrowingRelationShipCode = $F('txtRelationshipCode');
//        objAutoLoanAnalystApplication.CustomerStatus = $F('cmbCustomerStatus');
//        objAutoLoanAnalystApplication.AskingRepaymentMethod = $F('cmbAskingRepaymentmethod');
//        objAutoLoanAnalystApplication.ARTA = $F('cmbARTA');
//        objAutoLoanAnalystApplication.ARTAStatus = $F('cmbARTAReference');
//        objAutoLoanAnalystApplication.GeneralInsurance = $F('cmbGeneralInsurence');
//        objAutoLoanAnalystApplication.GeneralInsurancePreference = $F('cmbGeneralInsurencePreference');
//        objAutoLoanAnalystApplication.CashSecured100Per = $F('cmbCashSecured') == '0' ? false : true;
//        objAutoLoanAnalystApplication.CCBundle = $F('cmbCCBundle') == '0' ? false : true;
//        objAutoLoanAnalystApplication.SecurityValue = $F('txtSequrityValue') == '' ? 0 : $F('txtSequrityValue');

//        //auto loan vehicle

//        var objAutoLoanVehicle = new Object();
//        objAutoLoanVehicle.AutoLoanVehicleId = $F('txtAutoLoanVehicleId');
//        objAutoLoanVehicle.VendorId = $F('cmbVendor');

//        //new vendor
//        var objAutoVendor = new Object();
//        objAutoVendor.VendorName = $F('txtVendorName');
//        //            objAutoVendor.DealerType = $F('cmbDealerType');
//        objAutoVendor.DealerType = '1';
//        //new vendor end

//        //continue auto loan vehicle

//        objAutoLoanVehicle.Auto_ManufactureId = $F('cmbManufacturer');
//        var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;

//        if (manufacturName != "Other") {
//            objAutoLoanVehicle.VehicleId = $F('cmbModel');
//            var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
//            if (vehicleName != "Other") {
//                objAutoLoanVehicle.Model = $F('hdnModel');
//                objAutoLoanVehicle.VehicleType = $F('txtVehicleType');
//            }
//            else {
//                objAutoLoanVehicle.Model = vehicleName;
//                objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
//            }
//        }
//        else {
//            objAutoLoanVehicle.VehicleId = "-7"; //Vehicle -7= Others
//            objAutoLoanVehicle.Model = $F('txtModelName');
//            objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
//        }
//        objAutoLoanVehicle.TrimLevel = $F('txtTrimLevel');
//        objAutoLoanVehicle.EngineCC = $F('txtEngineCapacityCC');
//        objAutoLoanVehicle.VehicleStatus = $F('cmdVehicleStatus');

//        objAutoLoanVehicle.ManufacturingYear = $F('txtManufacturaryear') == '' ? '0' : $F('txtManufacturaryear');
//        objAutoLoanVehicle.QuotedPrice = $F('txtQuotedPrice') == '' ? '0' : $F('txtQuotedPrice');
//        objAutoLoanVehicle.VerifiedPrice = $F('txtConsideredprice') == '' ? '0' : $F('txtConsideredprice');

//        objAutoLoanVehicle.PriceConsideredBasedOn = $F('cmbPriceConsideredOn');
//        objAutoLoanVehicle.SeatingCapacity = $F('cmsSeatingCapacity');
//        objAutoLoanVehicle.CarVerification = $F('cmbCarVerification');


//        var objAutoLoanAnalystVehicleDetail = new Object();

//        objAutoLoanAnalystVehicleDetail.Id = $F('txtVehicleDetailID');
//        objAutoLoanAnalystVehicleDetail.AnalystMasterId = $F('txtAnalystMasterID');
//        objAutoLoanAnalystVehicleDetail.valuePackAllowed = $F('cmbvaluePackAllowed') == '0' ? false : true; ;
//        objAutoLoanAnalystVehicleDetail.SeatingCapacity = $F('cmsSeatingCapacity');
//        objAutoLoanAnalystVehicleDetail.ConsideredPrice = $F('txtConsideredprice');
//        objAutoLoanAnalystVehicleDetail.PriceConsideredOn = $F('cmbPriceConsideredOn');
//        objAutoLoanAnalystVehicleDetail.CarVarification = $F('cmbCarVerification');
//        objAutoLoanAnalystVehicleDetail.DeliveryStatus = $F('cmbDeliveryStatus');
//        objAutoLoanAnalystVehicleDetail.RegistrationYear = $F('txtregistrationYear') == '' ? '0' : $F('txtregistrationYear');


//        // With SCB
//        var objAutoApplicantAccountSCBList = [];

//        var tablePrAccount = $('tblApplicantAccountSCB');
//        var rowCount = tablePrAccount.rows.length;
//        for (var i = 1; i <= rowCount - 1; i++) {
//            var objAutoApplicantAccountSCB = new Object();
//            if ($F('txtAccountNumberForSCB' + i) == '') {
//            } else {
//                if ($F('cmbAccountStatusForSCB' + i) == '0') {
//                    alert('please select Account Status');
//                    //$('cmbAccountStatusForSCB' + i).focus();
//                    return false;
//                }

//                else {
//                    objAutoApplicantAccountSCB.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objAutoApplicantAccountSCB.LLID = $F('txtLLID');
//                    objAutoApplicantAccountSCB.AccountNumberSCB = $F('txtAccountNumberForSCB' + i);
//                    objAutoApplicantAccountSCB.AccountStatus = $F('cmbAccountStatusForSCB' + i);
//                    objAutoApplicantAccountSCB.PriorityHolder = $F('cmbPriorityAcHolderForSCB' + i);

//                    objAutoApplicantAccountSCBList.push(objAutoApplicantAccountSCB);

//                }

//            }
//        }
//        //With SCB End


//        // Other Bank

//        var objAutoApplicantAccountOtherList = [];

//        var tableOtherBank = $('tblApplicantAccountOther');
//        var rowCount = tableOtherBank.rows.length;
//        for (var i = 1; i <= rowCount - 1; i++) {
//            var objAutoApplicantAccountOther = new Object();
//            if ($F('cmbBankNameForOther' + i) == '-1') {
//            } else {
//                if ($F('cmbBranchNameOther' + i) == '-1') {
//                    alert('please select Branch');
//                    //$('cmbBranchNameOther' + i).focus();
//                    return false;
//                }

//                else {
//                    if ($F('txtAccountNumberother' + i) == '') {
//                        alert('please Input Account Number');
//                        //$('txtAccountNumberother' + i).focus();
//                        return false;
//                    }

//                    else {

//                        if ($F('cmbAccountStatus' + i) == '0') {
//                            alert('please Select Account Status');
//                            //$('cmbAccountStatus' + i).focus();
//                            return false;
//                        }

//                        else {

//                            if ($F('cmbAccountType' + i) == '0') {
//                                alert('please Select Account Type');
//                                //$('cmbAccountType' + i).focus();
//                                return false;
//                            }

//                            else {


//                                objAutoApplicantAccountOther.AutoLoanMasterId = $F('txtAutoLoanMasterId');

//                                objAutoApplicantAccountOther.BankId = $F('cmbBankNameForOther' + i);
//                                objAutoApplicantAccountOther.BranchId = $F('cmbBranchNameOther' + i);
//                                objAutoApplicantAccountOther.AccountNumber = $F('txtAccountNumberother' + i);
//                                objAutoApplicantAccountOther.AccountCategory = $F('cmbAccountStatus' + i);
//                                objAutoApplicantAccountOther.AccountType = $F('cmbAccountType' + i);

//                                objAutoApplicantAccountOtherList.push(objAutoApplicantAccountOther);
//                            }
//                        }
//                    }
//                }

//            }
//        }

//        // Other bank end





//        //Exposures start

//        //ON SCBFacility start


//        var objAutoFacilityList = [];

//        //Term Loan
//        var tablePrAccount = $('tblFacilityTermLoan');
//        var rowCount = tablePrAccount.rows.length;

//        for (var i = 1; i <= rowCount - 1; i++) {
//            var objAutoFacility = new Object();
//            if ($F('cmbTypeForTermLoans' + i) == '-1') {
//            } else {
//                if ($F('txtInterestRate' + i) == '') {
//                    //alert('please input Interest rate.');
//                    //$('txtInterestRate' + i).focus();
//                    //return false;
//                    $F('txtInterestRate' + i).setValue('0');
//                }
//                else if ($F('txtLimit' + i) == '') {
//                    //alert('please input Limit.');
//                    //$('txtLimit' + i).focus();
//                    //return false;
//                    $F('txtLimit' + i).setValue('0');
//                }
//                else if ($F('txtoutStanding' + i) == '') {
//                    //alert('please input Outstanding.');
//                    //$('txtoutStanding' + i).focus();
//                    //return false;
//                    $F('txtoutStanding' + i).setValue('0');
//                }
//                else if ($F('txtEMI' + i) == '') {
//                    //alert('please input EMI.');
//                    //$('txtEMI' + i).focus();
//                    //return false;
//                    $F('txtEMI' + i).setValue('0');
//                }
//                else if ($F('txtEMIPer' + i) == '') {
//                    //alert('please input EMI Percent.');
//                    //$('txtEMIPer' + i).focus();
//                    //return false;
//                    $F('txtEMIPer' + i).setValue('0');
//                }
//                else {


//                    objAutoFacility.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objAutoFacility.FacilityTypeName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
//                    objAutoFacility.FacilityType = $F('cmbTypeForTermLoans' + i);
//                    objAutoFacility.InterestRate = $F('txtInterestRate' + i) == '' ? '0' : $F('txtInterestRate' + i);
//                    objAutoFacility.PresentBalance = $F('txtoutStanding' + i) == '' ? '0' : $F('txtoutStanding' + i);
//                    objAutoFacility.PresentEMI = $F('txtEMI' + i) == '' ? '0' : $F('txtEMI' + i);
//                    objAutoFacility.PresentLimit = $F('txtLimit' + i) == '' ? '0' : $F('txtLimit' + i);
//                    objAutoFacility.EMIPercent = $F('txtEMIPer' + i) == '' ? '0' : $F('txtEMIPer' + i);
//                    objAutoFacility.EMIShare = $F('txtEMIShare' + i) == '' ? '0' : $F('txtEMIShare' + i);

//                    objAutoFacilityList.push(objAutoFacility);

//                }

//            }
//        }
//        // trem Loan end

//        //CreditCard Start

//        var tablePrAccountCreditCard = $('tblFacilityCreditCard');
//        var rowCountCreditCard = tablePrAccountCreditCard.rows.length;

//        for (var i = 1; i <= rowCountCreditCard - 1; i++) {
//            var objAutoFacilityCreditCard = new Object();
//            if ($F('cmbTypeForCreditCard' + i) == '-1') {
//            } else {
//                if ($F('txtOriginalLimitForCreditCard' + i) == '') {
//                    alert('please input Credit Card Limit.');
//                    //$('txtOriginalLimitForCreditCard' + i).focus();
//                    return false;
//                }
//                else if ($F('txtInterestForCreditCard' + i) == '') {
//                    alert('please input Credit Card Interest.');
//                    //$('txtInterestForCreditCard' + i).focus();
//                    return false;
//                }
//                else {

//                    objAutoFacilityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objAutoFacilityCreditCard.FacilityTypeName = $('cmbTypeForCreditCard' + i).options[$('cmbTypeForCreditCard' + i).selectedIndex].text;
//                    objAutoFacilityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
//                    objAutoFacilityCreditCard.InterestRate = $F('txtInterestRateCreditCard' + i) == '' ? '0' : $F('txtInterestRateCreditCard' + i);
//                    objAutoFacilityCreditCard.PresentLimit = $F('txtOriginalLimitForCreditCard' + i) == '' ? '0' : $F('txtOriginalLimitForCreditCard' + i);

//                    objAutoFacilityList.push(objAutoFacilityCreditCard);

//                }

//            }
//        }


//        //Credit card end


//        //OverDraft Start

//        var tablePrAccountoverDraft = $('tblFacilityOverDraft');
//        var rowCountoverDraft = tablePrAccountoverDraft.rows.length;

//        for (var i = 1; i <= rowCountoverDraft - 1; i++) {
//            var objAutoFacilityOverDraft = new Object();
//            if ($F('txtOverdraftlimit' + i) == '') {
//            } else {
//                if ($F('txtOverdraftInterest' + i) == '') {
//                    alert('please input Interest Rate for Over Draft.');
//                    //$('txtOverdraftInterest' + i).focus();
//                    return false;
//                }
//                else {

//                    objAutoFacilityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objAutoFacilityOverDraft.FacilityTypeName = 'Over Draft';
//                    objAutoFacilityOverDraft.FacilityType = '23';
//                    objAutoFacilityOverDraft.PresentLimit = $F('txtOverdraftlimit' + i) == '' ? '0' : $F('txtOverdraftlimit' + i);
//                    objAutoFacilityOverDraft.InterestRate = $F('txtOverdraftInterest' + i) == '' ? '0' : $F('txtOverdraftInterest' + i);

//                    objAutoFacilityList.push(objAutoFacilityOverDraft);

//                }

//            }
//        }
//        //OverDraft end



//        // start SECURITIES


//        var objAutoSecurityList = [];

//        // start TERM LOANS
//        var tableOnSCBSecurityTermLoan = $('tblOnSCBSecurityTermLoan');
//        var rowCountTremLoan = tableOnSCBSecurityTermLoan.rows.length;

//        for (var i = 1; i <= rowCountTremLoan - 1; i++) {
//            var objAutoSecurityTermLoan = new Object();
//            if ($F('cmbTypeForTermLoans' + i) == '-1') {
//            } else {
//                if ($F('cmbStatusForSecurities' + i) == '-1') {
//                    alert('please Select Status for Security (Term Loan).');
//                    //$('cmbStatusForSecurities' + i).focus();
//                    return false;
//                }

//                else if ($F('txtSecurityFV' + i) == '-1') {
//                    alert('please input FV for Security (Term Loan).');
//                    //$('txtSecurityFV' + i).focus();
//                    return false;
//                }
//                else if ($F('txtSecurityIssuingDateTermLoan' + i) == '-1') {
//                    alert('please input Issue Date for Security (Term Loan).');
//                    //$('txtSecurityIssuingDateTermLoan' + i).focus();
//                    return false;
//                }
//                else if ($F('cmbTypeForSequrities' + i) == '-1') {
//                    alert('please select Security Type (Term Loan).');
//                    //$('cmbTypeForSequrities' + i).focus();
//                    return false;
//                }
//                else if ($F('txtSecurityPV' + i) == '-1') {
//                    alert('please input PV for Security (Term Loan).');
//                    //$('txtSecurityPV' + i).focus();
//                    return false;
//                }

//                else {

//                    objAutoSecurityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objAutoSecurityTermLoan.FacilityType = $F('cmbTypeForTermLoans' + i);
//                    objAutoSecurityTermLoan.NatureOfSecurity = $F('cmbTypeForSequrities' + i);
//                    objAutoSecurityTermLoan.Status = $F('cmbStatusForSecurities' + i);
//                    objAutoSecurityTermLoan.IssuingOffice = $F('txtSecurityIssuingOfficeTermLoan' + i);
//                    objAutoSecurityTermLoan.FaceValue = $F('txtSecurityFV' + i) == '' ? '0' : $F('txtSecurityFV' + i);
//                    objAutoSecurityTermLoan.XTVRate = $F('txtSecurityPV' + i) == '' ? '0' : $F('txtSecurityPV' + i);
//                    objAutoSecurityTermLoan.IssueDate = $F('txtSecurityIssuingDateTermLoan' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateTermLoan' + i));

//                    objAutoSecurityList.push(objAutoSecurityTermLoan);



//                }

//            }
//        }
//        //TERM LOANS end



//        // start CreditCard
//        var tableOnSCBSecurityCreditCard = $('tblOnSCBSecurityCreditCard');
//        var rowCountCreditCard = tableOnSCBSecurityCreditCard.rows.length;

//        for (var i = 1; i <= rowCountCreditCard - 1; i++) {
//            var objAutoSecurityCreditCard = new Object();
//            if ($F('cmbTypeForCreditCard' + i) == '-1') {
//            } else {
//                if ($F('cmbStatusForSecuritiesForCreditCard' + i) == '-1') {
//                    alert('please Select Status for Security (Credit Card).');
//                    //$('cmbStatusForSecuritiesForCreditCard' + i).focus();
//                    return false;
//                }

//                else if ($F('txtSecurityFVForcreditCard' + i) == '-1') {
//                    alert('please input FV for Security (Credit Card).');
//                    //$('txtSecurityFVForcreditCard' + i).focus();
//                    return false;
//                }
//                else if ($F('txtSecurityIssuingDateCreditCard' + i) == '-1') {
//                    alert('please input Issue Date for Security (Credit Card).');
//                    //$('txtSecurityIssuingDateCreditCard' + i).focus();
//                    return false;
//                }
//                else if ($F('cmbTypeForSequritiesForcreditCard' + i) == '-1') {
//                    alert('please select Security Type (Credit Card).');
//                    //$('cmbTypeForSequritiesForcreditCard' + i).focus();
//                    return false;
//                }
//                else if ($F('txtSecurityPVForCreditCard' + i) == '-1') {
//                    alert('please input PV for Security (Credit Card).');
//                    //$('txtSecurityPVForCreditCard' + i).focus();
//                    return false;
//                }

//                else {

//                    objAutoSecurityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objAutoSecurityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
//                    objAutoSecurityCreditCard.NatureOfSecurity = $F('cmbTypeForSequritiesForcreditCard' + i);
//                    objAutoSecurityCreditCard.Status = $F('cmbStatusForSecuritiesForCreditCard' + i);
//                    objAutoSecurityCreditCard.IssuingOffice = $F('txtSecurityIssuingOfficeCreditCard' + i);
//                    objAutoSecurityCreditCard.FaceValue = $F('txtSecurityFVForcreditCard' + i) == '' ? '0' : $F('txtSecurityFVForcreditCard' + i);
//                    objAutoSecurityCreditCard.XTVRate = $F('txtSecurityPVForCreditCard' + i) == '' ? '0' : $F('txtSecurityPVForCreditCard' + i);
//                    objAutoSecurityCreditCard.IssueDate = $F('txtSecurityIssuingDateCreditCard' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateCreditCard' + i));

//                    objAutoSecurityList.push(objAutoSecurityCreditCard);

//                }

//            }
//        }
//        //CreditCard end


//        // start OverDraft
//        var tableOnSCBSecurityOverDraft = $('tblOnSCBSecurityOverDraft');
//        var rowCountOverDraft = tableOnSCBSecurityOverDraft.rows.length;

//        for (var i = 1; i <= rowCountOverDraft - 1; i++) {
//            var objAutoSecurityOverDraft = new Object();
//            if ($F('txtOverdraftlimit' + i) == '') {
//            } else {
//                if ($F('cmbStatusForSecuritiesForoverDraft' + i) == '-1') {
//                    alert('please Select Status for Security (Over Draft).');
//                    //$('cmbStatusForSecuritiesForoverDraft' + i).focus();
//                    return false;
//                }

//                else if ($F('txtSecurityFVForoverDraft' + i) == '-1') {
//                    alert('please input FV for Security (Over Draft).');
//                    //$('txtSecurityFVForoverDraft' + i).focus();
//                    return false;
//                }
//                else if ($F('txtSecurityIssuingDateOverDraft' + i) == '-1') {
//                    alert('please input Issue Date for Security (Over Draft).');
//                    //$('txtSecurityIssuingDateOverDraft' + i).focus();
//                    return false;
//                }
//                else if ($F('cmbTypeForSequritiesForoverDraft' + i) == '-1') {
//                    alert('please select Security Type (Over Draft).');
//                    //$('cmbTypeForSequritiesForoverDraft' + i).focus();
//                    return false;
//                }
//                else if ($F('txtSecurityPVForoverDraft' + i) == '-1') {
//                    alert('please input PV for Security (Over Draft).');
//                    //$('txtSecurityPVForoverDraft' + i).focus();
//                    return false;
//                }

//                else {

//                    objAutoSecurityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objAutoSecurityOverDraft.FacilityType = '23';
//                    objAutoSecurityOverDraft.NatureOfSecurity = $F('cmbTypeForSequritiesForoverDraft' + i);
//                    objAutoSecurityOverDraft.Status = $F('cmbStatusForSecuritiesForoverDraft' + i);
//                    objAutoSecurityOverDraft.IssuingOffice = $F('txtSecurityIssuingOfficeOverDraft' + i);
//                    objAutoSecurityOverDraft.FaceValue = $F('txtSecurityFVForoverDraft' + i) == '' ? '0' : $F('txtSecurityFVForoverDraft' + i);
//                    objAutoSecurityOverDraft.XTVRate = $F('txtSecurityPVForoverDraft' + i) == '' ? '0' : $F('txtSecurityPVForoverDraft' + i);
//                    objAutoSecurityOverDraft.IssueDate = $F('txtSecurityIssuingDateOverDraft' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateOverDraft' + i));

//                    objAutoSecurityList.push(objAutoSecurityOverDraft);

//                }

//            }
//        }
//        //OverDraft end

//        // end SECURITIES



//        //End ON SCB Facility





//        //Off SCB Start
//        //start TermLoan
//        var objOFFSCBFacilityList = [];

//        // start TERM LOANS
//        var tableOFFSCBSecurityTermLoan = $('tblOFFSCBFacilityTermLoan');
//        var rowCountOFFSCBTremLoan = tableOFFSCBSecurityTermLoan.rows.length;

//        for (var i = 1; i <= rowCountOFFSCBTremLoan - 1; i++) {
//            var objOFFSCBFacilityTermLoan = new Object();
//            if ($F('cmbFinancialInstitutionForOffSCB' + i) == '-1') {
//            } else {
//                if ($F('txtOriginalLimitForOffScb' + i) == '') {
//                    alert('please Input original Limit (OFF SCB Term Loan');
//                    //$('txtOriginalLimitForOffScb' + i).focus();
//                    return false;
//                }

//                else if ($F('txtEMIforOffSCB' + i) == '') {
//                    alert('please input EMI (OFF SCB Term Loan).');
//                    //$('txtEMIforOffSCB' + i).focus();
//                    return false;
//                }
//                else if ($F('txtEMIPerForOffSCB' + i) == '-1') {
//                    alert('please input EMI Percent (OFF SCB Term Loan).');
//                    //$('txtEMIPerForOffSCB' + i).focus();
//                    return false;
//                }
//                else if ($F('txtEMIShareForOffSCB' + i) == '-1') {
//                    alert('EMI Share is not calculated(OFF SCB Term Loan).');
//                    //$('txtEMIShareForOffSCB' + i).focus();
//                    return false;
//                }
//                else if ($F('cmbStatusForOffScb' + i) == '0') {
//                    alert('please Select Status (OFF SCB Term Loan).');
//                    //$('cmbStatusForOffScb' + i).focus();
//                    return false;
//                }

//                else {

//                    objOFFSCBFacilityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objOFFSCBFacilityTermLoan.BankID = $F('cmbFinancialInstitutionForOffSCB' + i);
//                    objOFFSCBFacilityTermLoan.OriginalLimit = $F('txtOriginalLimitForOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForOffScb' + i);
//                    objOFFSCBFacilityTermLoan.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
//                    objOFFSCBFacilityTermLoan.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
//                    objOFFSCBFacilityTermLoan.EMIShare = $F('txtEMIShareForOffSCB' + i) == '' ? '0' : $F('txtEMIShareForOffSCB' + i);
//                    objOFFSCBFacilityTermLoan.Status = $F('cmbStatusForOffScb' + i);
//                    objOFFSCBFacilityTermLoan.Type = '1';

//                    objOFFSCBFacilityList.push(objOFFSCBFacilityTermLoan);

//                }

//            }
//        }
//        //TERM LOANS end

//        // start CreditCard
//        var tableOFFSCBSecurityCreditCard = $('tblOFFSCBFacilityCreditCard');
//        var rowCountOFFSCBCreditCard = tableOFFSCBSecurityCreditCard.rows.length;

//        for (var i = 1; i <= rowCountOFFSCBCreditCard - 1; i++) {
//            var objOFFSCBFacilityCreditCard = new Object();
//            if ($F('cmbFinancialInstitutionForCreditCardOffSCB' + i) == '-1') {
//            } else {
//                if ($F('txtOriginalLimitForcreditCardOffScb' + i) == '') {
//                    alert('please Input original Limit (OFF SCB Credit Card');
//                    //$('txtOriginalLimitForcreditCardOffScb' + i).focus();
//                    return false;
//                }

//                else if ($F('txtEMIforOffSCB' + i) == '') {
//                    alert('please input EMI (OFF SCB Credit Card).');
//                    //$('txtEMIforOffSCB' + i).focus();
//                    return false;
//                }
//                else if ($F('txtInterestRateForCreditCardForOffScb' + i) == '-1') {
//                    alert('please input Interest Rate (OFF SCB Credit Card).');
//                    //$('txtInterestRateForCreditCardForOffScb' + i).focus();
//                    return false;
//                }
//                else if ($F('cmbStatusForCreditCardOffScb' + i) == '0') {
//                    alert('please Select Status (OFF SCB Credit Card).');
//                    //$('cmbStatusForCreditCardOffScb' + i).focus();
//                    return false;
//                }

//                else {

//                    objOFFSCBFacilityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objOFFSCBFacilityCreditCard.BankID = $F('cmbFinancialInstitutionForCreditCardOffSCB' + i);
//                    objOFFSCBFacilityCreditCard.OriginalLimit = $F('txtOriginalLimitForcreditCardOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForcreditCardOffScb' + i);
//                    objOFFSCBFacilityCreditCard.InterestRate = $F('txtInterestRateForCreditCardForOffScb' + i) == '' ? '0' : $F('txtInterestRateForCreditCardForOffScb' + i);
//                    objOFFSCBFacilityCreditCard.Status = $F('cmbStatusForCreditCardOffScb' + i);

//                    //                    objOFFSCBFacilityTermLoan.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
//                    //                    objOFFSCBFacilityTermLoan.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
//                    //                    objOFFSCBFacilityTermLoan.EMIShare = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
//                    objOFFSCBFacilityCreditCard.Type = '2';


//                    objOFFSCBFacilityList.push(objOFFSCBFacilityCreditCard);

//                }

//            }
//        }
//        //CreditCard end

//        // start OverDraft
//        var tableOFFSCBSecurityOverDraft = $('tblOFFSCBFacilityOverDraft');
//        var rowCountOFFSCBOverDraft = tableOFFSCBSecurityOverDraft.rows.length;

//        for (var i = 1; i <= rowCountOFFSCBOverDraft - 1; i++) {
//            var objOFFSCBFacilityOverDraft = new Object();
//            if ($F('cmbFinancialInstitutionForOverDraftOffSCB' + i) == '-1') {
//            } else {
//                if ($F('txtOriginalLimitForOverDraftOffScb' + i) == '') {
//                    alert('please Input original Limit (OFF SCB Over Draft');
//                    //$('txtOriginalLimitForOverDraftOffScb' + i).focus();
//                    return false;
//                }

//                else if ($F('txtinterestRateForOverDraftOffSCB' + i) == '') {
//                    alert('please input Interest Rate (OFF SCB Over Draft).');
//                    //$('txtinterestRateForOverDraftOffSCB' + i).focus();
//                    return false;
//                }
//                else if ($F('txtInterestForOverDraftInterest' + i) == '-1') {
//                    alert('Interest did not calculated (OFF SCB Over Draft).');
//                    //$('txtInterestForOverDraftInterest' + i).focus();
//                    return false;
//                }
//                else if ($F('cmbStatusForOverDraftOffScb' + i) == '0') {
//                    alert('please Select Status (OFF SCB Over Draft).');
//                    //$('cmbStatusForOverDraftOffScb' + i).focus();
//                    return false;
//                }

//                else {

//                    objOFFSCBFacilityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
//                    objOFFSCBFacilityOverDraft.BankID = $F('cmbFinancialInstitutionForOverDraftOffSCB' + i);
//                    objOFFSCBFacilityOverDraft.OriginalLimit = $F('txtOriginalLimitForOverDraftOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForOverDraftOffScb' + i);
//                    objOFFSCBFacilityOverDraft.InterestRate = $F('txtinterestRateForOverDraftOffSCB' + i) == '' ? '0' : $F('txtinterestRateForOverDraftOffSCB' + i);
//                    objOFFSCBFacilityOverDraft.Status = $F('cmbStatusForOverDraftOffScb' + i);

//                    //                    objOFFSCBFacilityOverDraft.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
//                    //                    objOFFSCBFacilityOverDraft.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
//                    //                    objOFFSCBFacilityOverDraft.EMIShare = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
//                    objOFFSCBFacilityOverDraft.Type = '3';


//                    objOFFSCBFacilityList.push(objOFFSCBFacilityOverDraft);

//                }

//            }
//        }

        
        //Analyst Master
        if ($F("cmbColleterization") == "-1" && $F("cmbColleterizationWith") != "-1") {
            alert("Please select Colleterization With");
            //$("cmbColleterizationWith").focus();
            return false;
        }
        if ($F("cmbColleterization") != "-1" && $F("cmbColleterizationWith") == "-1") {
            alert("Please select Colleterization");
            //$("cmbColleterization").focus();
            return false;
        }


        var objAutoAnalistMaster = new Object();


        objAutoAnalistMaster.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoAnalistMaster.LlId = $F('txtLLID');
        objAutoAnalistMaster.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        objAutoAnalistMaster.IsJoint = $F('txtIsJoint') == '0' ? false : true;
        objAutoAnalistMaster.ReasonOfJoint = $('cmbReasonofJointApplicant').options[$('cmbReasonofJointApplicant').selectedIndex].text;
        objAutoAnalistMaster.Colleterization = $F("cmbColleterization");
        objAutoAnalistMaster.ColleterizationWith = $F("cmbColleterizationWith");

        // Application Details

        var objAutoLoanAnalystApplication = new Object();

        objAutoLoanAnalystApplication.Id = $F('txtloanAnalystApplicationID');
        objAutoLoanAnalystApplication.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoLoanAnalystApplication.Source = $F('cmbSource1');
        objAutoLoanAnalystApplication.BranchId = $F('cmbBranch');
        objAutoLoanAnalystApplication.BranchCode = $F('txtBranchCode');
        objAutoLoanAnalystApplication.ValuePack = $F('cmbValuePack');
        objAutoLoanAnalystApplication.BorrowingRelationShip = $F('cmbBrowingRelationship');
        objAutoLoanAnalystApplication.BorrowingRelationShipCode = $F('txtRelationshipCode');
        objAutoLoanAnalystApplication.CustomerStatus = $F('cmbCustomerStatus');
        objAutoLoanAnalystApplication.AskingRepaymentMethod = $F('cmbAskingRepaymentmethod');
        objAutoLoanAnalystApplication.ARTA = $F('cmbARTA');
        objAutoLoanAnalystApplication.ARTAStatus = $F('cmbARTAReference');
        objAutoLoanAnalystApplication.GeneralInsurance = $F('cmbGeneralInsurence');
        objAutoLoanAnalystApplication.GeneralInsurancePreference = $F('cmbGeneralInsurencePreference');
        objAutoLoanAnalystApplication.CashSecured100Per = $F('cmbCashSecured') == '0' ? false : true;
        objAutoLoanAnalystApplication.CCBundle = $F('cmbCCBundle') == '0' ? false : true;
        objAutoLoanAnalystApplication.SecurityValue = $F('txtSequrityValue') == '' ? 0 : $F('txtSequrityValue');

        //auto loan vehicle

        var objAutoLoanVehicle = new Object();
        objAutoLoanVehicle.AutoLoanVehicleId = $F('txtAutoLoanVehicleId');
        objAutoLoanVehicle.VendorId = $F('cmbVendor');

        //new vendor
        var objAutoVendor = new Object();
        objAutoVendor.VendorName = $F('txtVendorName');
        //            objAutoVendor.DealerType = $F('cmbDealerType');
        objAutoVendor.DealerType = '1';
        //new vendor end

        //continue auto loan vehicle

        objAutoLoanVehicle.Auto_ManufactureId = $F('cmbManufacturer');

        var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;

        if (manufacturName != "Other") {
            objAutoLoanVehicle.VehicleId = $F('cmbModel');
            var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
            if (vehicleName != "Other") {
                objAutoLoanVehicle.Model = $F('hdnModel');
                objAutoLoanVehicle.VehicleType = $F('txtVehicleType');
            }
            else {
                objAutoLoanVehicle.Model = vehicleName;
                objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
            }

        }
        else {
            objAutoLoanVehicle.VehicleId = "-7"; //Vehicle -7= Others
            objAutoLoanVehicle.Model = $F('txtModelName');
            objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
        }
        objAutoLoanVehicle.TrimLevel = $F('txtTrimLevel');
        objAutoLoanVehicle.EngineCC = $F('txtEngineCapacityCC');
        objAutoLoanVehicle.VehicleStatus = $F('cmdVehicleStatus');
        objAutoLoanVehicle.ManufacturingYear = $F('txtManufacturaryear') == '' ? '0' : $F('txtManufacturaryear');
        objAutoLoanVehicle.QuotedPrice = $F('txtQuotedPrice') == '' ? '0' : $F('txtQuotedPrice');
        objAutoLoanVehicle.VerifiedPrice = $F('txtConsideredprice') == '' ? '0' : $F('txtConsideredprice');

        objAutoLoanVehicle.PriceConsideredBasedOn = $F('cmbPriceConsideredOn');
        objAutoLoanVehicle.SeatingCapacity = $F('cmsSeatingCapacity');
        objAutoLoanVehicle.CarVerification = $F('cmbCarVerification');


        var objAutoLoanAnalystVehicleDetail = new Object();

        objAutoLoanAnalystVehicleDetail.Id = $F('txtVehicleDetailID');
        objAutoLoanAnalystVehicleDetail.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoLoanAnalystVehicleDetail.valuePackAllowed = $F('cmbvaluePackAllowed') == '0' ? false : true; ;
        objAutoLoanAnalystVehicleDetail.SeatingCapacity = $F('cmsSeatingCapacity');
        objAutoLoanAnalystVehicleDetail.ConsideredPrice = $F('txtConsideredprice');
        objAutoLoanAnalystVehicleDetail.PriceConsideredOn = $F('cmbPriceConsideredOn');
        objAutoLoanAnalystVehicleDetail.CarVarification = $F('cmbCarVerification');
        objAutoLoanAnalystVehicleDetail.DeliveryStatus = $F('cmbDeliveryStatus');
        objAutoLoanAnalystVehicleDetail.RegistrationYear = $F('txtregistrationYear') == '' ? '0' : $F('txtregistrationYear'); // $F('txtregistrationYear');


        // With SCB
        var objAutoApplicantAccountSCBList = [];

        var tablePrAccount = $('tblApplicantAccountSCB');
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoApplicantAccountSCB = new Object();
            if ($F('txtAccountNumberForSCB' + i) == '') {
            } else {
                if ($F('cmbAccountStatusForSCB' + i) == '0') {
                    alert('please select Account Status');
                    //$('cmbAccountStatusForSCB' + i).focus();
                    return false;
                }

                else {
                    objAutoApplicantAccountSCB.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objAutoApplicantAccountSCB.LLID = $F('txtLLID');
                    objAutoApplicantAccountSCB.AccountNumberSCB = $F('txtAccountNumberForSCB' + i);
                    objAutoApplicantAccountSCB.AccountStatus = $F('cmbAccountStatusForSCB' + i);
                    objAutoApplicantAccountSCB.PriorityHolder = $F('cmbPriorityAcHolderForSCB' + i);

                    objAutoApplicantAccountSCBList.push(objAutoApplicantAccountSCB);

                }

            }
        }
        //With SCB End


        // Other Bank

        var objAutoApplicantAccountOtherList = [];

        var tableOtherBank = $('tblApplicantAccountOther');
        var rowCount = tableOtherBank.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoApplicantAccountOther = new Object();
            if ($F('cmbBankNameForOther' + i) == '-1') {
            } else {
                if ($F('cmbBranchNameOther' + i) == '-1') {
                    alert('please select Branch');
                    //$('cmbBranchNameOther' + i).focus();
                    return false;
                }

                else {
                    if ($F('txtAccountNumberother' + i) == '') {
                        alert('please Input Account Number');
                        //$('txtAccountNumberother' + i).focus();
                        return false;
                    }

                    else {

                        if ($F('cmbAccountStatus' + i) == '0') {
                            alert('please Select Account Status');
                            //$('cmbAccountStatus' + i).focus();
                            return false;
                        }

                        else {

                            if ($F('cmbAccountType' + i) == '0') {
                                alert('please Select Account Type');
                                //$('cmbAccountType' + i).focus();
                                return false;
                            }

                            else {


                                objAutoApplicantAccountOther.AutoLoanMasterId = $F('txtAutoLoanMasterId');

                                objAutoApplicantAccountOther.BankId = $F('cmbBankNameForOther' + i);
                                objAutoApplicantAccountOther.BranchId = $F('cmbBranchNameOther' + i);
                                objAutoApplicantAccountOther.AccountNumber = $F('txtAccountNumberother' + i);
                                objAutoApplicantAccountOther.AccountCategory = $F('cmbAccountStatus' + i);
                                objAutoApplicantAccountOther.AccountType = $F('cmbAccountType' + i);

                                objAutoApplicantAccountOtherList.push(objAutoApplicantAccountOther);
                            }
                        }
                    }
                }

            }
        }

        // Other bank end





        //Exposures start

        //ON SCBFacility start


        var objAutoFacilityList = [];
        var objAutoSecurityList = [];

        //Term Loan
        var tablePrAccount = $('tblFacilityTermLoan');
        var rowCount = tablePrAccount.rows.length;

        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoFacility = new Object();
            if ($F('cmbTypeForTermLoans' + i) == '-1') {
            }
            else {
                if ($F('txtInterestRate' + i) == '') {
                    //alert('please input Interest rate.');
                    //$('txtInterestRate' + i).focus();
                    //return false;
                    $('txtInterestRate' + i).setValue('0');
                }
                if ($F('txtLimit' + i) == '') {
                    //alert('please input Limit.');
                    //$('txtLimit' + i).focus();
                    //return false;
                    $('txtLimit' + i).setValue('0');
                }
                if ($F('txtoutStanding' + i) == '') {
                    //alert('please input Outstanding.');
                    //$('txtoutStanding' + i).focus();
                    //return false;
                    $('txtoutStanding' + i).setValue('0');
                }
                if ($F('txtEMI' + i) == '') {
                    //alert('please input EMI.');
                    //$('txtEMI' + i).focus();
                    //return false;
                    $('txtEMI' + i).setValue('0');
                }
                if ($F('txtEMIPer' + i) == '') {
                    //alert('please input EMI Percent.');
                    //$('txtEMIPer' + i).focus();
                    //return false;
                    $('txtEMIPer' + i).setValue('0');
                }
                //else {


                objAutoFacility.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacility.FacilityTypeName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                objAutoFacility.FacilityType = $F('cmbTypeForTermLoans' + i);
                objAutoFacility.InterestRate = $F('txtInterestRate' + i) == '' ? '0' : $F('txtInterestRate' + i);
                objAutoFacility.PresentBalance = $F('txtoutStanding' + i) == '' ? '0' : $F('txtoutStanding' + i);
                objAutoFacility.PresentEMI = $F('txtEMI' + i) == '' ? '0' : $F('txtEMI' + i);
                objAutoFacility.PresentLimit = $F('txtLimit' + i) == '' ? '0' : $F('txtLimit' + i);
                objAutoFacility.EMIPercent = $F('txtEMIPer' + i) == '' ? '0' : $F('txtEMIPer' + i);
                objAutoFacility.EMIShare = $F('txtEMIShare' + i) == '' ? '0' : $F('txtEMIShare' + i);

                objAutoFacilityList.push(objAutoFacility);

                var objAutoSecurityTermLoan = new Object();
                objAutoSecurityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityTermLoan.FacilityType = $F('cmbTypeForTermLoans' + i);
                objAutoSecurityTermLoan.NatureOfSecurity = $F('cmbTypeForSequrities' + i);
                objAutoSecurityTermLoan.Status = $F('cmbStatusForSecurities' + i);
                objAutoSecurityTermLoan.FaceValue = $F('txtSecurityFV' + i) == '' ? '0' : $F('txtSecurityFV' + i);
                objAutoSecurityTermLoan.XTVRate = $F('txtSecurityPV' + i) == '' ? '0' : $F('txtSecurityPV' + i);
                objAutoSecurityList.push(objAutoSecurityTermLoan);

                //}

            }
        }
        // trem Loan end

        //CreditCard Start

        var tablePrAccountCreditCard = $('tblFacilityCreditCard');
        var rowCountCreditCard = tablePrAccountCreditCard.rows.length;

        for (var i = 1; i <= rowCountCreditCard - 1; i++) {
            var objAutoFacilityCreditCard = new Object();
            if ($F('cmbTypeForCreditCard' + i) == '-1') {
            }
            else {
                if ($F('txtOriginalLimitForCreditCard' + i) == '') {
                    //alert('please input Credit Card Limit.');
                    //$('txtOriginalLimitForCreditCard' + i).focus();
                    //return false;
                    $('txtOriginalLimitForCreditCard' + i).setValue('0');
                }
                //                else if ($F('txtInterestForCreditCard' + i) == '') {
                //                    alert('please input Credit Card Interest.');
                //                    //$('txtInterestForCreditCard' + i).focus();
                //                    return false;
                //                }
                //else {

                objAutoFacilityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacilityCreditCard.FacilityTypeName = $('cmbTypeForCreditCard' + i).options[$('cmbTypeForCreditCard' + i).selectedIndex].text;
                objAutoFacilityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
                objAutoFacilityCreditCard.InterestRate = 5; //$F('txtInterestRateCreditCard' + i) == '' ? '0' : $F('txtInterestRateCreditCard' + i);
                objAutoFacilityCreditCard.PresentLimit = $F('txtOriginalLimitForCreditCard' + i) == '' ? '0' : $F('txtOriginalLimitForCreditCard' + i);

                objAutoFacilityList.push(objAutoFacilityCreditCard);

                var objAutoSecurityCreditCard = new Object();
                objAutoSecurityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
                objAutoSecurityCreditCard.NatureOfSecurity = $F('cmbTypeForSequritiesForcreditCard' + i);
                objAutoSecurityCreditCard.Status = $F('cmbStatusForSecuritiesForCreditCard' + i);
                objAutoSecurityCreditCard.FaceValue = $F('txtSecurityFVForcreditCard' + i) == '' ? '0' : $F('txtSecurityFVForcreditCard' + i);
                objAutoSecurityCreditCard.XTVRate = $F('txtSecurityPVForCreditCard' + i) == '' ? '0' : $F('txtSecurityPVForCreditCard' + i);

                objAutoSecurityList.push(objAutoSecurityCreditCard);

                //}

            }
        }


        //Credit card end


        //OverDraft Start

        var tablePrAccountoverDraft = $('tblFacilityOverDraft');
        var rowCountoverDraft = tablePrAccountoverDraft.rows.length;

        for (var i = 1; i <= rowCountoverDraft - 1; i++) {
            var objAutoFacilityOverDraft = new Object();
            if ($F('txtOverdraftlimit' + i) == '') {
            }
            else {
                //                if ($F('txtOverdraftInterest' + i) == '') {
                //                    alert('please input Interest Rate for Over Draft.');
                //                    //$('txtOverdraftInterest' + i).focus();
                //                    return false;
                //                }
                //                else {

                objAutoFacilityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacilityOverDraft.FacilityTypeName = 'Over Draft';
                objAutoFacilityOverDraft.FacilityType = '23';
                objAutoFacilityOverDraft.PresentLimit = $F('txtOverdraftlimit' + i) == '' ? '0' : $F('txtOverdraftlimit' + i);
                objAutoFacilityOverDraft.InterestRate = $F('txtOverdraftInterest' + i) == '' ? '0' : $F('txtOverdraftInterest' + i);

                objAutoFacilityList.push(objAutoFacilityOverDraft);

                var objAutoSecurityOverDraft = new Object();
                objAutoSecurityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityOverDraft.FacilityType = '23';
                objAutoSecurityOverDraft.NatureOfSecurity = $F('cmbTypeForSequritiesForoverDraft' + i);
                objAutoSecurityOverDraft.Status = $F('cmbStatusForSecuritiesForoverDraft' + i);
                objAutoSecurityOverDraft.FaceValue = $F('txtSecurityFVForoverDraft' + i) == '' ? '0' : $F('txtSecurityFVForoverDraft' + i);
                objAutoSecurityOverDraft.XTVRate = $F('txtSecurityPVForoverDraft' + i) == '' ? '0' : $F('txtSecurityPVForoverDraft' + i);
                objAutoSecurityList.push(objAutoSecurityOverDraft);

                //               }

            }
        }
        //OverDraft end



        // start SECURITIES


        //var objAutoSecurityList = [];

        // start TERM LOANS
        //        var tableOnSCBSecurityTermLoan = $('tblOnSCBSecurityTermLoan');
        //        var rowCountTremLoan = tableOnSCBSecurityTermLoan.rows.length;

        //        for (var i = 1; i <= rowCountTremLoan - 1; i++) {
        //            var objAutoSecurityTermLoan = new Object();
        //            if ($F('cmbTypeForTermLoans' + i) == '-1') {
        //            } else {
        //                if ($F('cmbStatusForSecurities' + i) == '-1') {
        //                    alert('please Select Status for Security (Term Loan).');
        //                    //$('cmbStatusForSecurities' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityFV' + i) == '-1') {
        //                    //alert('please input FV for Security (Term Loan).');
        //                    //$('txtSecurityFV' + i).focus();
        //                    //return false;
        //                    $('txtSecurityFV' + i).setValue('0');
        //                }
        //                //                else if ($F('txtSecurityIssuingDateTermLoan' + i) == '-1') {
        //                //                    alert('please input Issue Date for Security (Term Loan).');
        //                //                    //$('txtSecurityIssuingDateTermLoan' + i).focus();
        //                //                    return false;
        //                //                }
        //                //                else if ($F('cmbTypeForSequrities' + i) == '-1') {
        //                //                    alert('please select Security Type (Term Loan).');
        //                //                    //$('cmbTypeForSequrities' + i).focus();
        //                //                    return false;
        //                //                }
        //                else if ($F('txtSecurityPV' + i) == '-1') {
        //                    //alert('please input PV for Security (Term Loan).');
        //                    //$('txtSecurityPV' + i).focus();
        //                    //return false;
        //                    $('txtSecurityPV' + i).setValue('0');
        //                }

        //                else {

        //                    objAutoSecurityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        //                    objAutoSecurityTermLoan.FacilityType = $F('cmbTypeForTermLoans' + i);
        //                    objAutoSecurityTermLoan.NatureOfSecurity = $F('cmbTypeForSequrities' + i);
        //                    objAutoSecurityTermLoan.Status = $F('cmbStatusForSecurities' + i);
        //                    //objAutoSecurityTermLoan.IssuingOffice = $F('txtSecurityIssuingOfficeTermLoan' + i);
        //                    objAutoSecurityTermLoan.FaceValue = $F('txtSecurityFV' + i) == '' ? '0' : $F('txtSecurityFV' + i);
        //                    objAutoSecurityTermLoan.XTVRate = $F('txtSecurityPV' + i) == '' ? '0' : $F('txtSecurityPV' + i);
        //                    //objAutoSecurityTermLoan.IssueDate = $F('txtSecurityIssuingDateTermLoan' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateTermLoan' + i));

        //                    objAutoSecurityList.push(objAutoSecurityTermLoan);

        //                }

        //            }
        //        }
        //TERM LOANS end



        // start CreditCard
        //        var tableOnSCBSecurityCreditCard = $('tblOnSCBSecurityCreditCard');
        //        var rowCountCreditCard = tableOnSCBSecurityCreditCard.rows.length;

        //        for (var i = 1; i <= rowCountCreditCard - 1; i++) {
        //            var objAutoSecurityCreditCard = new Object();
        //            if ($F('cmbTypeForCreditCard' + i) == '-1') {
        //            } else {
        //                if ($F('cmbStatusForSecuritiesForCreditCard' + i) == '-1') {
        //                    alert('please Select Status for Security (Credit Card).');
        //                    //$('cmbStatusForSecuritiesForCreditCard' + i).focus();
        //                    return false;
        //                }

        //                else if ($F('txtSecurityFVForcreditCard' + i) == '-1') {
        //                    alert('please input FV for Security (Credit Card).');
        //                    //$('txtSecurityFVForcreditCard' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityIssuingDateCreditCard' + i) == '-1') {
        //                    alert('please input Issue Date for Security (Credit Card).');
        //                    //$('txtSecurityIssuingDateCreditCard' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('cmbTypeForSequritiesForcreditCard' + i) == '-1') {
        //                    alert('please select Security Type (Credit Card).');
        //                    //$('cmbTypeForSequritiesForcreditCard' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityPVForCreditCard' + i) == '-1') {
        //                    alert('please input PV for Security (Credit Card).');
        //                    //$('txtSecurityPVForCreditCard' + i).focus();
        //                    return false;
        //                }

        //                else {

        //                    objAutoSecurityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        //                    objAutoSecurityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
        //                    objAutoSecurityCreditCard.NatureOfSecurity = $F('cmbTypeForSequritiesForcreditCard' + i);
        //                    objAutoSecurityCreditCard.Status = $F('cmbStatusForSecuritiesForCreditCard' + i);
        //                    objAutoSecurityCreditCard.IssuingOffice = $F('txtSecurityIssuingOfficeCreditCard' + i);
        //                    objAutoSecurityCreditCard.FaceValue = $F('txtSecurityFVForcreditCard' + i) == '' ? '0' : $F('txtSecurityFVForcreditCard' + i);
        //                    objAutoSecurityCreditCard.XTVRate = $F('txtSecurityPVForCreditCard' + i) == '' ? '0' : $F('txtSecurityPVForCreditCard' + i);
        //                    objAutoSecurityCreditCard.IssueDate = $F('txtSecurityIssuingDateCreditCard' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateCreditCard' + i));

        //                    objAutoSecurityList.push(objAutoSecurityCreditCard);

        //                }

        //            }
        //        }
        //CreditCard end


        // start OverDraft
        //        var tableOnSCBSecurityOverDraft = $('tblOnSCBSecurityOverDraft');
        //        var rowCountOverDraft = tableOnSCBSecurityOverDraft.rows.length;

        //        for (var i = 1; i <= rowCountOverDraft - 1; i++) {
        //            var objAutoSecurityOverDraft = new Object();
        //            if ($F('txtOverdraftlimit' + i) == '') {
        //            } else {
        //                if ($F('cmbStatusForSecuritiesForoverDraft' + i) == '-1') {
        //                    alert('please Select Status for Security (Over Draft).');
        //                    //$('cmbStatusForSecuritiesForoverDraft' + i).focus();
        //                    return false;
        //                }

        //                else if ($F('txtSecurityFVForoverDraft' + i) == '-1') {
        //                    alert('please input FV for Security (Over Draft).');
        //                    //$('txtSecurityFVForoverDraft' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityIssuingDateOverDraft' + i) == '-1') {
        //                    alert('please input Issue Date for Security (Over Draft).');
        //                    //$('txtSecurityIssuingDateOverDraft' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('cmbTypeForSequritiesForoverDraft' + i) == '-1') {
        //                    alert('please select Security Type (Over Draft).');
        //                    //$('cmbTypeForSequritiesForoverDraft' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityPVForoverDraft' + i) == '-1') {
        //                    alert('please input PV for Security (Over Draft).');
        //                    //$('txtSecurityPVForoverDraft' + i).focus();
        //                    return false;
        //                }

        //                else {

        //                    objAutoSecurityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        //                    objAutoSecurityOverDraft.FacilityType = '23';
        //                    objAutoSecurityOverDraft.NatureOfSecurity = $F('cmbTypeForSequritiesForoverDraft' + i);
        //                    objAutoSecurityOverDraft.Status = $F('cmbStatusForSecuritiesForoverDraft' + i);
        //                    objAutoSecurityOverDraft.IssuingOffice = $F('txtSecurityIssuingOfficeOverDraft' + i);
        //                    objAutoSecurityOverDraft.FaceValue = $F('txtSecurityFVForoverDraft' + i) == '' ? '0' : $F('txtSecurityFVForoverDraft' + i);
        //                    objAutoSecurityOverDraft.XTVRate = $F('txtSecurityPVForoverDraft' + i) == '' ? '0' : $F('txtSecurityPVForoverDraft' + i);
        //                    objAutoSecurityOverDraft.IssueDate = $F('txtSecurityIssuingDateOverDraft' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateOverDraft' + i));

        //                    objAutoSecurityList.push(objAutoSecurityOverDraft);

        //                }

        //            }
        //        }
        //OverDraft end

        // end SECURITIES



        //End ON SCB Facility





        //Off SCB Start
        //start TermLoan
        var objOFFSCBFacilityList = [];

        // start TERM LOANS
        var tableOFFSCBSecurityTermLoan = $('tblOFFSCBFacilityTermLoan');
        var rowCountOFFSCBTremLoan = tableOFFSCBSecurityTermLoan.rows.length;

        for (var i = 1; i <= rowCountOFFSCBTremLoan - 1; i++) {
            var objOFFSCBFacilityTermLoan = new Object();
            if ($F('cmbFinancialInstitutionForOffSCB' + i) == '-1') {
            } else {
                //                if ($F('txtOriginalLimitForOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Term Loan');
                //                    //$('txtOriginalLimitForOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtEMIforOffSCB' + i) == '') {
                //                    alert('please input EMI (OFF SCB Term Loan).');
                //                    // $('txtEMIforOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtEMIPerForOffSCB' + i) == '-1') {
                //                    alert('please input EMI Percent (OFF SCB Term Loan).');
                //                    // $('txtEMIPerForOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtEMIShareForOffSCB' + i) == '-1') {
                //                    alert('EMI Share is not calculated(OFF SCB Term Loan).');
                //                    // $('txtEMIShareForOffSCB' + i).focus();
                //                    return false;
                //                }
                //else 
                if ($F('cmbStatusForOffScb' + i) == '0') {
                    alert('please Select Status (OFF SCB Term Loan).');
                    //  $('cmbStatusForOffScb' + i).focus();
                    return false;
                }

                else {

                    objOFFSCBFacilityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objOFFSCBFacilityTermLoan.BankID = $F('cmbFinancialInstitutionForOffSCB' + i);
                    objOFFSCBFacilityTermLoan.OriginalLimit = $F('txtOriginalLimitForOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForOffScb' + i);
                    objOFFSCBFacilityTermLoan.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
                    objOFFSCBFacilityTermLoan.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    objOFFSCBFacilityTermLoan.EMIShare = $F('txtEMIShareForOffSCB' + i) == '' ? '0' : $F('txtEMIShareForOffSCB' + i);
                    objOFFSCBFacilityTermLoan.Status = $F('cmbStatusForOffScb' + i);
                    objOFFSCBFacilityTermLoan.Type = '1';

                    objOFFSCBFacilityList.push(objOFFSCBFacilityTermLoan);

                }

            }
        }
        //TERM LOANS end

        // start CreditCard
        var tableOFFSCBSecurityCreditCard = $('tblOFFSCBFacilityCreditCard');
        var rowCountOFFSCBCreditCard = tableOFFSCBSecurityCreditCard.rows.length;

        for (var i = 1; i <= rowCountOFFSCBCreditCard - 1; i++) {
            var objOFFSCBFacilityCreditCard = new Object();
            if ($F('cmbFinancialInstitutionForCreditCardOffSCB' + i) == '-1') {
            } else {
                //                if ($F('txtOriginalLimitForcreditCardOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Credit Card');
                //                    //   $('txtOriginalLimitForcreditCardOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtEMIforOffSCB' + i) == '') {
                //                    alert('please input EMI (OFF SCB Credit Card).');
                //                    //    $('txtEMIforOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtInterestRateForCreditCardForOffScb' + i) == '-1') {
                //                    alert('please input Interest Rate (OFF SCB Credit Card).');
                //                    //    $('txtInterestRateForCreditCardForOffScb' + i).focus();
                //                    return false;
                //                }
                //else 
                if ($F('cmbStatusForCreditCardOffScb' + i) == '0') {
                    alert('please Select Status (OFF SCB Credit Card).');
                    //   $('cmbStatusForCreditCardOffScb' + i).focus();
                    return false;
                }

                else {

                    objOFFSCBFacilityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objOFFSCBFacilityCreditCard.BankID = $F('cmbFinancialInstitutionForCreditCardOffSCB' + i);
                    objOFFSCBFacilityCreditCard.OriginalLimit = $F('txtOriginalLimitForcreditCardOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForcreditCardOffScb' + i);
                    objOFFSCBFacilityCreditCard.InterestRate = 5; //$F('txtInterestRateForCreditCardForOffScb' + i) == '' ? '0' : $F('txtInterestRateForCreditCardForOffScb' + i);
                    objOFFSCBFacilityCreditCard.Status = $F('cmbStatusForCreditCardOffScb' + i);

                    //                    objOFFSCBFacilityTermLoan.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
                    //                    objOFFSCBFacilityTermLoan.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    //                    objOFFSCBFacilityTermLoan.EMIShare = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    objOFFSCBFacilityCreditCard.Type = '2';


                    objOFFSCBFacilityList.push(objOFFSCBFacilityCreditCard);

                }

            }
        }
        //CreditCard end

        // start OverDraft
        var tableOFFSCBSecurityOverDraft = $('tblOFFSCBFacilityOverDraft');
        var rowCountOFFSCBOverDraft = tableOFFSCBSecurityOverDraft.rows.length;

        for (var i = 1; i <= rowCountOFFSCBOverDraft - 1; i++) {
            var objOFFSCBFacilityOverDraft = new Object();
            if ($F('cmbFinancialInstitutionForOverDraftOffSCB' + i) == '-1') {
            } else {
                //                if ($F('txtOriginalLimitForOverDraftOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Over Draft');
                //                    //   $('txtOriginalLimitForOverDraftOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtinterestRateForOverDraftOffSCB' + i) == '') {
                //                    alert('please input Interest Rate (OFF SCB Over Draft).');
                //                    //    $('txtinterestRateForOverDraftOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtInterestForOverDraftInterest' + i) == '-1') {
                //                    alert('Interest did not calculated (OFF SCB Over Draft).');
                //                    //    $('txtInterestForOverDraftInterest' + i).focus();
                //                    return false;
                //                }
                //else 
                if ($F('cmbStatusForOverDraftOffScb' + i) == '0') {
                    alert('please Select Status (OFF SCB Over Draft).');
                    //    $('cmbStatusForOverDraftOffScb' + i).focus();
                    return false;
                }

                else {

                    objOFFSCBFacilityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objOFFSCBFacilityOverDraft.BankID = $F('cmbFinancialInstitutionForOverDraftOffSCB' + i);
                    objOFFSCBFacilityOverDraft.OriginalLimit = $F('txtOriginalLimitForOverDraftOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForOverDraftOffScb' + i);
                    objOFFSCBFacilityOverDraft.InterestRate = $F('txtinterestRateForOverDraftOffSCB' + i) == '' ? '0' : $F('txtinterestRateForOverDraftOffSCB' + i);
                    objOFFSCBFacilityOverDraft.Status = $F('cmbStatusForOverDraftOffScb' + i);

                    //                    objOFFSCBFacilityOverDraft.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
                    //                    objOFFSCBFacilityOverDraft.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    //                    objOFFSCBFacilityOverDraft.EMIShare = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    objOFFSCBFacilityOverDraft.Type = '3';


                    objOFFSCBFacilityList.push(objOFFSCBFacilityOverDraft);

                }

            }
        }
        //OverDraft end
        var res = PlussAuto.AutoLoanApplicationCISupport.ForwardLoanApp(objAutoAnalistMaster, objAutoLoanAnalystApplication, objAutoLoanAnalystVehicleDetail, objAutoApplicantAccountSCBList, objAutoApplicantAccountOtherList, objAutoFacilityList, objAutoSecurityList, objOFFSCBFacilityList, objAutoLoanVehicle, objAutoVendor);
        //        var res = PlussAuto.AutoLoanApplicationCISupport.SaveCISupportLoanApp(objAutoAnalistMaster);
        if (res.error) {
            alert(res.error.Message);
            return;
        }

        else if (res.value == "Success") {
            alert("Forward to analyst Successfully.");
            analyzeLoanApplicationManager.getLoanSummary();
            analyzeLoanApplicationManager.CloseLoanApp();
            

        }
        else {
            alert("Forward Operation Failed");
        }
    },


    ClearLoanAppForm: function() {
        var texts = document.getElementsByTagName('input');
        for (var i_tem = 0; i_tem < texts.length; i_tem++) {
            if (texts[i_tem].type == 'text') {
                texts[i_tem].value = '';
            }
            if (texts[i_tem].type == 'hidden') {
                texts[i_tem].value = '0';
            }
        }

        var combo = document.getElementsByTagName('select');
        for (var i_tems = 0; i_tems < combo.length; i_tems++) {
            combo[i_tems].value = '-1';

        }

    },


    CloseLoanApp: function() {
        analyzeLoanApplicationManager.ClearLoanAppForm();

        $("LoanAppUI").style.display = 'none';
        $("divAutoloanSummary").style.display = 'block';
        var url = "../../UI/AutoLoan/AutoLoanApplicationCISupport.aspx";
        location.href = url;

    },

    getLoanSummary: function() {
        var searchKey = $F('txtLLIDMain');
        if (searchKey == "") {
            searchKey = 0;
        }
        if (util.isDigit(searchKey)) {
            var res = PlussAuto.AutoLoanApplicationCISupport.GetLoanSummary(Page.pageNo, Page.pageSize, searchKey);
            //debugger;
            if (res.error) {
                alert(res.error.Message);
                return;
            }
            //            if (res.value.length == 0 || res == null) {
            //                alert("No Data Found !!");
            //                return;
            //            }
            Page.LoanSummeryArray = res.value;

            analyzeLoanApplicationHelper.populateLoanSummeryGrid(res);
        } else {
            alert("LLID Must to be in Degit");
            return false;
        }
    },

    Appeal: function(appiledOnAutoLoanMasterId) {
        var res = PlussAuto.AutoLoanApplicationCISupport.AppealedByAutoLoanMasterId(appiledOnAutoLoanMasterId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Process  Succeed");
            analyzeLoanApplicationManager.getLoanSummary();
            var url = "../../UI/AutoLoan/AutoLoanApplicationCISupport.aspx";
            location.href = url;
        } else {
        alert("Process  not Succeeded");
        }
    },
    SetLLIDAndSearch: function(llid) {
        $('txtLLID').setValue(llid);
        analyzeLoanApplicationManager.GetDataByLLID();
        $("LoanAppUI").style.display = 'block';
        $("divAutoloanSummary").style.display = 'none';

    },



    GetAllAutoBranch: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanApplicationCISupport.GetAllAutoBranch();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbSource1').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Source';
        ExtraOpt.value = '-1';
        document.getElementById('cmbSource1').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].AutoBranchName;
            opt.value = res.value[i].AutoBranchId;
            document.getElementById('cmbSource1').options.add(opt);
        }
    },
    getBank: function() {

        var res = PlussAuto.AutoLoanApplicationCISupport.GetBank(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateBankCombo(res);
    },
    getFacility: function() {

        var res = PlussAuto.AutoLoanApplicationCISupport.getFacility();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        analyzeLoanApplicationHelper.populateFacilityCombo(res);
    },


    GetBranch: function(elementId) {
        // alert(ElementID);
        //        debugger;

        var bankId = $F(elementId);
        if (bankId == 0) {
        }
        else {
            var res = PlussAuto.AutoLoanApplicationCISupport.GetBranch(bankId);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            analyzeLoanApplicationHelper.populateBranchCombo(res, elementId);
        }

    },

    changeBranchNameForSourceOnApplicantDetailsForGeneral: function() {
        var branchId = $F("cmbSource1");
        var res = PlussAuto.AutoLoanApplicationCISupport.GetAllAutoSourceByBranchId(branchId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbBranch').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Source';
        ExtraOpt.value = '-1';
        document.getElementById('cmbBranch').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].SourceName;
            opt.value = res.value[i].AutoSourceId;
            document.getElementById('cmbBranch').options.add(opt);
        }
        Page.sourceForGeneralArray = res.value;
    },
    getVehicleStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanApplicationCISupport.getVehicleStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmdVehicleStatus').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vehicle Status';
        ExtraOpt.value = '-1';
        document.getElementById('cmdVehicleStatus').options.add(ExtraOpt);
        Page.vehicleStatusArray = res.value;
        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmdVehicleStatus').options.add(opt);
        }
    },
    getManufacturer: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanApplicationCISupport.GetAutoManufacturer();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.manufactureArray = res.value;

        document.getElementById('cmbManufacturer').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Manufacturer';
        ExtraOpt.value = '-1';
        document.getElementById('cmbManufacturer').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ManufacturerName;
            opt.value = res.value[i].ManufacturerId;
            document.getElementById('cmbManufacturer').options.add(opt);
        }

    },

    Getvaluepackallowed: function() {
        var manufacturerId = $F('cmbManufacturer');
        var vehicleId = $F("cmbModel");
        var carStatus = $F("cmdVehicleStatus");
        if (manufacturerId == "-1" || vehicleId == "-1" || carStatus == "-1") {
            $('cmbvaluePackAllowed').setValue('0');
            return;
        }

        var res = PlussAuto.AutoLoanApplicationCISupport.getValuePackAllowed(manufacturerId, vehicleId, carStatus);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value != null) {
            $('cmbvaluePackAllowed').setValue(res.value.ValuePackAllowed);
        }
        else {
            $('cmbvaluePackAllowed').setValue('0');
        }

    },

    getModelByManufactureId: function() {
        var manufacturerID = $F('cmbManufacturer');

        //var res = PlussAuto.LoanApplication.GetAutoModelByManufacturerID(manufacturerID);
        var res = PlussAuto.AutoLoanApplicationCISupport.getModelbyManufacturerID(manufacturerID);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.modelArray = res.value;

        document.getElementById('cmbModel').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Model';
        ExtraOpt.value = '-1';
        document.getElementById('cmbModel').options.add(ExtraOpt);
        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            var ch = res.value[i].Model;
            if (res.value[i].TrimLevel != "") {
                ch += "-" + res.value[i].TrimLevel;

            }
            opt.text = ch;
            opt.value = res.value[i].VehicleId;
            document.getElementById('cmbModel').options.add(opt);
        }

        if (Page.vehicleId != 0) {
            $('cmbModel').setValue(Page.vehicleId);

            analyzeLoanApplicationHelper.changeModel();
        }
        //analyzeLoanApplicationManager.getvaluepackallowed();
        analyzeLoanApplicationManager.Getvaluepackallowed();
    },

    GetDataByLLID: function() {
        var llId = $F("txtLLID");
        var res = PlussAuto.AutoLoanApplicationCISupport.GetDataByLLID(llId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else if (res.value == 'No Data found.') {
            alert(res.value);
            return;
        }
        else {
            //Primary Applicant
            $('txtPrProfessionId').setValue(res.value.objAutoPRProfession.PrimaryProfession);


            $('txtPrimaryApplicant').setValue(res.value.objAutoPRApplicant.PrName);
            $('txtDateOfBirth').setValue(res.value.objAutoPRApplicant.DateofBirth.format());
            $('txtCurrenAge').setValue(res.value.objAutoPRApplicant.Age);
            $('txtPrimaryProfession').setValue(res.value.objAutoPRProfession.PrimaryProfessionName);
            $('txtOtherProfession').setValue(res.value.objAutoPRProfession.OtherProfessionName);
            $('txtDeclaredIncome').setValue(res.value.objAutoPRFinance.DPIAmount);
            $('txtVerifiedAddress').setValue(res.value.objAutoPRApplicant.ResidenceAddress);

            //Joint Applicant
            $('txtJointApplicant').setValue(res.value.objAutoJTApplicant.JtName);
            $('txtJtDateOfBirth').setValue(res.value.objAutoJTApplicant.DateofBirth.format());
            $('txtJtCurrntAge').setValue(res.value.objAutoJTApplicant.Age);
            $('txtJtProfession').setValue(res.value.objAutoJTProfession.JtPrimaryProfessionName);
            $('txtJtDeclaredIncome').setValue(res.value.objAutoJTFinance.DPIAmount);

            $('txtJtProfessionId').setValue(res.value.objAutoJTProfession.PrimaryProfession);

            $('cmbReasonofJointApplicant').setValue(ReasonOfJoint[res.value.AutoLoanAnalystMaster.ReasonOfJoint]);


            var currentAgeForPrimary = parseInt($F("txtCurrenAge"));
            var currentAgeForJoin = parseInt($F("txtJtCurrntAge"));

            var jtPrimaryProfession = parseInt($F("txtJtProfessionId"));
            var prPrimaryProfession = parseInt($F("txtPrProfessionId"));


            $("lblFieldrequiredforPrAge").innerHTML = "";
            var isJoint = res.value.objAutoLoanMaster.JointApplication;
            if (isJoint == false) {
                $('divRightForJointMain').style.display = 'none';
            }
            else if (isJoint == true) {
                $('divRightForJointMain').style.display = 'block';
            }

            if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && currentAgeForJoin < 65 && isJoint == true) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";

            }
            if (currentAgeForPrimary > 65 && currentAgeForPrimary < 69 && isJoint == false) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";

            }
            if (currentAgeForJoin > 65 && currentAgeForJoin < 69 && currentAgeForPrimary < 65 && isJoint == true) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 2 required";

            }
            if ((currentAgeForPrimary > 68 || currentAgeForJoin > 68) && isJoint == true) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 3 required";
            }
            if (currentAgeForPrimary > 68 && isJoint == false) {
                $("lblFieldrequiredforPrAge").innerHTML = "Level 3 required";
            }


            //Application Details---------------------------------------------------------------------------------------------------------------------
            //$('cmbSource1').setValue(res.value.objAutoLoanMaster.Source);
            $('cmbProductName').setValue(res.value.objAutoLoanMaster.ProductId);
            $('cmbARTAReference').setValue(res.value.objAutoApplicationInsurance.LifeInsurance);

            if (res.value.objAutoApplicationInsurance.LifeInsurance == 0) {
                $('cmbARTA').setValue('2');
                $('cmbARTA').disabled = 'false';
            }
            else if (res.value.objAutoApplicationInsurance.LifeInsurance != 0 && res.value.objAutoApplicationInsurance.InsuranceFinancewithLoanLife == false) {
                $('cmbARTA').setValue('3');
                $('cmbARTA').disabled = '';
            }
            else if (res.value.objAutoApplicationInsurance.LifeInsurance != 0 && res.value.objAutoApplicationInsurance.InsuranceFinancewithLoanLife == true) {
                $('cmbARTA').setValue('1');
                $('cmbARTA').disabled = '';
            }
            $('cmbGeneralInsurencePreference').setValue(res.value.objAutoApplicationInsurance.GeneralInsurance);
            if (res.value.objAutoApplicationInsurance.GeneralInsurance == 0) {
                $('cmbGeneralInsurence').setValue('2');
                $('cmbGeneralInsurence').disabled = 'false';
            }
            else if (res.value.objAutoApplicationInsurance.GeneralInsurance != 0 && res.value.objAutoApplicationInsurance.InsuranceFinancewithLoan == false) {
                $('cmbGeneralInsurence').setValue('3');
                $('cmbGeneralInsurence').disabled = '';
            }
            else if (res.value.objAutoApplicationInsurance.GeneralInsurance != 0 && res.value.objAutoApplicationInsurance.InsuranceFinancewithLoan == true) {
                $('cmbGeneralInsurence').setValue('1');
                $('cmbGeneralInsurence').disabled = '';
            }


            if (res.value.AutoLoanAnalystApplication != null && res.value.AutoLoanAnalystApplication.Id != 0) {
                $('txtAutoLoanMasterId').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);
                $('txtAnalystMasterID').setValue(res.value.AutoLoanAnalystMaster.AnalystMasterId);
                $('txtloanAnalystApplicationID').setValue(res.value.AutoLoanAnalystApplication.Id);
                $('txtVehicleDetailID').setValue(res.value.AutoLoanAnalystVehicleDetail.Id);


                if (res.value.objAutoLoanMaster.JointApplication) {
                    $('txtIsJoint').setValue('1');
                    $('divRightForJointMain').style.display = 'block';

                } else {
                    $('txtIsJoint').setValue('0');
                    $('divRightForJointMain').style.display = 'none';
                }

                $('cmbSource1').setValue(res.value.AutoLoanAnalystApplication.Source);
                analyzeLoanApplicationManager.changeBranchNameForSourceOnApplicantDetailsForGeneral();
                $('cmbBranch').setValue(res.value.AutoLoanAnalystApplication.BranchId);
                $('txtBranchCode').setValue(res.value.AutoLoanAnalystApplication.BranchCode);
                $('cmbValuePack').setValue(res.value.AutoLoanAnalystApplication.ValuePack);
                $('cmbBrowingRelationship').setValue(res.value.AutoLoanAnalystApplication.BorrowingRelationShip);
                $('txtRelationshipCode').setValue(res.value.AutoLoanAnalystApplication.BorrowingRelationShipCode);
                $('cmbCustomerStatus').setValue(res.value.AutoLoanAnalystApplication.CustomerStatus);
                $('cmbAskingRepaymentmethod').setValue(res.value.AutoLoanAnalystApplication.AskingRepaymentMethod);
                $('cmbARTA').setValue(res.value.AutoLoanAnalystApplication.ARTA);
                $('cmbARTAReference').setValue(res.value.AutoLoanAnalystApplication.ARTAStatus);
                if (res.value.AutoLoanAnalystApplication.ARTAStatus == -1) {
                    $('cmbARTA').disabled = 'false';
                }
                else {
                    $('cmbARTA').disabled = '';
                }
                $('cmbGeneralInsurence').setValue(res.value.AutoLoanAnalystApplication.GeneralInsurance);
                $('cmbGeneralInsurencePreference').setValue(res.value.AutoLoanAnalystApplication.GeneralInsurancePreference);
                if (res.value.AutoLoanAnalystApplication.GeneralInsurancePreference == -1) {
                    $('cmbGeneralInsurence').disabled = 'false';
                }
                else {
                    $('cmbGeneralInsurence').disabled = '';
                }
                $('cmbCashSecured').setValue(res.value.AutoLoanAnalystApplication.CashSecured100Per);
                $('cmbCCBundle').setValue(res.value.AutoLoanAnalystApplication.CCBundle);
                if (res.value.AutoLoanAnalystApplication.CashSecured100Per == 1) {
                    $('divlblSequrityValue').style.display = "block";
                    $('divSequrityValueTextbox').style.display = "block";
                    $('txtSequrityValue').setValue(res.value.AutoLoanAnalystApplication.SecurityValue);
                }
                else {
                    $('divlblSequrityValue').style.display = "none";
                    $('divSequrityValueTextbox').style.display = "none";
                    $('txtSequrityValue').setValue('0');
                }




            }

            else {
                $('txtAutoLoanMasterId').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);
                if (res.value.objAutoLoanMaster.JointApplication) {
                    $('txtIsJoint').setValue('1');
                } else {
                    $('txtIsJoint').setValue('0');
                }


                if (res.value.objAutoLoanVehicle.ValuePackAllowed == false) {
                    $('cmbValuePack').setValue('0');
                } else {
                    $('cmbValuePack').setValue('1');
                }

                var bodyText = "";
                var pattern = "";
                var replaceText = "";
                var replacementtext = "";
                if (res.value.objAutoLoanMaster.ProductId == 3) {
                    labelChangeHelper.showFinanceLabel();
                } else {
                    labelChangeHelper.showLoanLabel();

                    if (res.value.objAutoLoanMaster.ProductId == 1) {
                        $('cmbARTA').setValue('2');
                        $('cmbGeneralInsurence').setValue('2');
                        //$('txtAppropriteRate').setValue('4.43');
                        //$('cmbARTA').disabled = 'false';
                        //$('cmbGeneralInsurence').disabled = 'false';
                        //$('cmbARTAReference').disabled = 'false';
                    } else {
                        $('cmbARTA').disabled = '';
                        $('cmbGeneralInsurence').disabled = '';
                        $('cmbARTAReference').disabled = '';
                    }

                }
            }


            //Vehicle Status---------------------------------------------------------------------------------------------------------------------------
            $('cmdVehicleStatus').setValue(res.value.objAutoLoanVehicle.VehicleStatus);
            if (res.value.objAutoLoanVehicle.VehicleStatus == 3) {
                $('cmbCarVerification').setValue('2');
            }
            else {
                $('cmbCarVerification').setValue(res.value.objAutoLoanVehicle.CarVerification);
                if (res.value.objAutoLoanVehicle.CarVerification == 1) {
                    $('divLblDeliveryStatus').style.display = 'block';
                    $('divDeliveryStatus').style.display = 'block';
                }
                else {
                    $('divLblDeliveryStatus').style.display = 'none';
                    $('divDeliveryStatus').style.display = 'none';
                }
            }
            Page.vehicleDetailsArray = res.value.objAutoLoanVehicle;
            $('cmbManufacturer').setValue(res.value.objAutoLoanVehicle.Auto_ManufactureId);
            $('txtAutoLoanVehicleId').setValue(res.value.objAutoLoanVehicle.AutoLoanVehicleId);
            analyzeLoanApplicationHelper.changeManufacture();
            $('cmbModel').setValue(res.value.objAutoLoanVehicle.VehicleId);
            analyzeLoanApplicationHelper.changeModel();
            $('hdnModel').setValue(res.value.objAutoLoanVehicle.Model);
            $('txtVehicleType').setValue(res.value.objAutoLoanVehicle.VehicleType);
            $('txtManufacturaryear').setValue(res.value.objAutoLoanVehicle.ManufacturingYear);
            $('txtEngineCapacityCC').setValue(res.value.objAutoLoanVehicle.EngineCC);
            $('cmsSeatingCapacity').setValue(res.value.objAutoLoanVehicle.SeatingCapacity);
            $('cmbVendor').setValue(res.value.objAutoLoanVehicle.VendorId);

            var vendoreId = $F("cmbVendor");
            if (vendoreId == "-1") {
                $('txtVendorName').disabled = '';
            }
            else {
                var vdr = Page.vendoreArray.findByProp('AutoVendorId', vendoreId);
                if (vdr) {
                    $('txtVendoreCode').setValue(vdr.VendorCode);
                    $('txtVendorName').disabled = 'false';
                }
            }

            $('txtQuotedPrice').setValue(res.value.objAutoLoanVehicle.QuotedPrice);
            $('txtConsideredprice').setValue(res.value.objAutoLoanVehicle.VerifiedPrice);
            $('cmbPriceConsideredOn').setValue(res.value.objAutoLoanVehicle.PriceConsideredBasedOn);

            $('txtVehicleStatusCode').setValue(res.value.objAutoLoanVehicle.StatusCode);
            $('txtManufacturarCode').setValue(res.value.objAutoLoanVehicle.ManufacturerCode);
            $('txtVehicleModelCode').setValue(res.value.objAutoLoanVehicle.ModelCode);
            $('txtVehicletypeCode').setValue(res.value.objAutoLoanVehicle.VehicleTypeCode);
            $('txtVendoreCode').setValue(res.value.objAutoLoanVehicle.VendorCode);
            if (res.value.objAutoLoanVehicle.VendoreRelationshipStatus != 1 && res.value.objAutoLoanVehicle.VendoreRelationshipStatus != 0) {
                alert("The Vendore is Negative Listed");
                $('cmbVendoreRelationShip').setValue('2');
            }
            else {
                $('cmbVendoreRelationShip').setValue(res.value.objAutoLoanVehicle.VendoreRelationshipStatus);
            }

            //For Analyst part.

            if (res.value.AutoLoanAnalystVehicleDetail != null && res.value.AutoLoanAnalystVehicleDetail.Id != 0) {

                $('cmbvaluePackAllowed').setValue(res.value.AutoLoanAnalystVehicleDetail.valuePackAllowed);
                $('cmsSeatingCapacity').setValue(res.value.AutoLoanAnalystVehicleDetail.SeatingCapacity);
                $('txtConsideredprice').setValue(res.value.AutoLoanAnalystVehicleDetail.ConsideredPrice);
                $('cmbPriceConsideredOn').setValue(res.value.AutoLoanAnalystVehicleDetail.PriceConsideredOn);
                $('cmbCarVerification').setValue(res.value.AutoLoanAnalystVehicleDetail.CarVarification);
                analyzeLoanApplicationHelper.changeCarVerification();
                $('cmbDeliveryStatus').setValue(res.value.AutoLoanAnalystVehicleDetail.DeliveryStatus);
                $('txtregistrationYear').setValue(res.value.AutoLoanAnalystVehicleDetail.RegistrationYear);
            }





            //analyzeLoanApplicationHelper.showTabbyType(res.value.objAutoPRProfession.PrimaryProfession, res.value.objAutoPRProfession.OtherProfession);

            //General Tab

            if (res.value.AutoApplicantSCBAccountList.length > 0) {
                for (var i = 0; i < res.value.AutoApplicantSCBAccountList.length; i++) {
                    var idfield = (i + 1);
                    if (idfield < 6) {
                        $('txtAccountNumberForSCB' + idfield).setValue(res.value.AutoApplicantSCBAccountList[i].AccountNumberSCB);
                        $('cmbAccountStatusForSCB' + idfield).setValue(res.value.AutoApplicantSCBAccountList[i].AccountStatus);
                        $('cmbPriorityAcHolderForSCB' + idfield).setValue(res.value.AutoApplicantSCBAccountList[i].PriorityHolder);
                    }

                }
            }
            else {
                for (var i = 0; i < res.value.objAutoPRAccountList.length; i++) {
                    var idfield = (i + 1);
                    if (idfield < 6) {
                        $('txtAccountNumberForSCB' + idfield).setValue(res.value.objAutoPRAccountList[i].AccountNumberForSCB);
                    }
                }
            }


            for (var i = 0; i < res.value.objAutoPRAccountDetailList.length; i++) {
                if (res.value.objAutoPRAccountDetailList[i].BankId != 42) {
                    var idfield = (i + 1);
                    if (idfield < 6) {
                        $('cmbBankNameForOther' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].BankId);
                        analyzeLoanApplicationManager.GetBranch('cmbBankNameForOther' + idfield);
                        $('cmbBranchNameOther' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].BranchId);
                        $('txtAccountNumberother' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountNumber);
                        $('cmbAccountStatus' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountCategory);
                        $('cmbAccountType' + idfield).setValue(res.value.objAutoPRAccountDetailList[i].AccountType);
                    }
                }
            }

            //Exposures on SCB For Term loans
            var optlinkForColl = "<option value='-1'>Select From list</option>";

            Page.facilityListArray = res.value.objAutoLoanFacilityList;
            var j = 0;
            var k = 0;
            var l = 0;
            for (var i = 0; i < res.value.objAutoLoanFacilityList.length; i++) {
                if (res.value.objAutoLoanFacilityList[i].FacilityType == 22 || res.value.objAutoLoanFacilityList[i].FacilityType == 18) {
                    var idfieldforExposerForCreditCard = j + 1;
                    if (idfieldforExposerForCreditCard < 3) {
                        $('cmbTypeForCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);
                        var typeforCreditCardobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                        $('txtFlagForcreditCard' + idfieldforExposerForCreditCard).setValue(typeforCreditCardobject.FACI_CODE);
                        $('txtOriginalLimitForCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                        //$('txtInterestRateCreditCard' + idfieldforExposerForCreditCard).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                        var credInterest = res.value.objAutoLoanFacilityList[i].PresentLimit * (res.value.objAutoLoanFacilityList[i].InterestRate / 100);
                        $('txtInterestForCreditCard' + idfieldforExposerForCreditCard).setValue(credInterest);
                        j = j + 1;
                    }
                }
                else if (res.value.objAutoLoanFacilityList[i].FacilityType == 23) {
                    var idfieldforExposerForOverdraft = k + 1;
                    if (idfieldforExposerForOverdraft < 4) {
                        var typeforOverDraftobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                        $('txtOverdraftFlag' + idfieldforExposerForOverdraft).setValue(typeforOverDraftobject.FACI_CODE);
                        $('txtOverdraftInterest' + idfieldforExposerForOverdraft).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                        $('txtOverdraftlimit' + idfieldforExposerForOverdraft).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                        var interst = parseFloat(parseFloat((res.value.objAutoLoanFacilityList[i].PresentLimit * res.value.objAutoLoanFacilityList[i].InterestRate / 100)) / 12);
                        $('txtOverdraftInteres' + idfieldforExposerForOverdraft).setValue(interst.toFixed(2));
                        k = k + 1;
                    }
                }
                else {
                    var idfieldforExposerForTermsloans = l + 1;
                    if (idfieldforExposerForTermsloans < 5) {
                        $('cmbTypeForTermLoans' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);
                        var typeforOtherobject = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanFacilityList[i].FacilityType);
                        if (typeforOtherobject) {
                            $('txtflag' + idfieldforExposerForTermsloans).setValue(typeforOtherobject.FACI_CODE);
                        }
                        $('txtInterestRate' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                        $('txtLimit' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                        $('txtoutStanding' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentBalance);
                        $('txtEMI' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].PresentEMI);
                        $('txtEMIPer' + idfieldforExposerForTermsloans).setValue(res.value.objAutoLoanFacilityList[i].EMIPercent);
                        analyzeLoanApplicationHelper.calculateEMIShare(idfieldforExposerForTermsloans);
                        //$('txtEMIShare' + idfieldforExposerForTermsloans).setValue('0');

                        if (typeforOtherobject) {
                            optlinkForColl += "<option value=" + res.value.objAutoLoanFacilityList[i].FacilityType + ">" + typeforOtherobject.FACI_NAME + "</option>";
                        }

                        l = l + 1;
                    }
                }

            }
            $("cmbColleterization").update(optlinkForColl);
            $("cmbColleterizationWith").update(optlinkForColl);

            if (res.value.AutoLoanAnalystMaster != null && res.value.AutoLoanAnalystMaster.AnalystMasterId != 0) {
                $("cmbColleterization").setValue(res.value.AutoLoanAnalystMaster.Colleterization);
                $("cmbColleterizationWith").setValue(res.value.AutoLoanAnalystMaster.ColleterizationWith);
            }

            //OFF SCB Facility List start  OfffacilityListArray


            //Page.OfffacilityListArray = res.value.AutoLoanFacilityOffSCBList;
            var s = 0;
            var d = 0;
            var f = 0;
            for (var i = 0; i < res.value.AutoLoanFacilityOffSCBList.length; i++) {
                if (res.value.AutoLoanFacilityOffSCBList[i].Type == 2) {
                    var idfieldforOffSCBForCreditCard = s + 1;
                    if (idfieldforOffSCBForCreditCard < 3) {
                        $('cmbFinancialInstitutionForCreditCardOffSCB' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                        $('txtOriginalLimitForcreditCardOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                        //$('txtInterestRateForCreditCardForOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].InterestRate);
                        //$('txtInterestForCreditCardForOffScb' + idfieldforOffSCBForCreditCard).setValue('0');
                        analyzeLoanApplicationHelper.changeCreditCardInterestForOffSCB(idfieldforOffSCBForCreditCard);
                        $('cmbStatusForCreditCardOffScb' + idfieldforOffSCBForCreditCard).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);
                        s = s + 1;
                    }
                }
                else if (res.value.AutoLoanFacilityOffSCBList[i].Type == 3) {
                    var idfieldforOffSCBForOverdraft = d + 1;
                    if (idfieldforOffSCBForOverdraft < 4) {
                        $('cmbFinancialInstitutionForOverDraftOffSCB' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                        $('txtOriginalLimitForOverDraftOffScb' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                        $('txtinterestRateForOverDraftOffSCB' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].InterestRate);
                        //$('txtInterestForOverDraftInterest' + idfieldforOffSCBForCreditCard).setValue('0');
                        analyzeLoanApplicationHelper.changeOverDraftInterstForOffSCB(idfieldforOffSCBForOverdraft);
                        $('cmbStatusForOverDraftOffScb' + idfieldforOffSCBForOverdraft).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);
                        d = d + 1;
                    }
                }
                else {
                    var idfieldforOffSCBForTermsloans = f + 1;
                    if (idfieldforOffSCBForTermsloans < 5) {
                        $('cmbFinancialInstitutionForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].BankID);
                        $('txtOriginalLimitForOffScb' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].OriginalLimit);
                        $('txtEMIforOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMI);
                        //analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB(f);

                        $('txtEMIPerForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMIPercent);
                        //analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB(f);
                        $('txtEMIShareForOffSCB' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].EMIShare);
                        $('cmbStatusForOffScb' + idfieldforOffSCBForTermsloans).setValue(res.value.AutoLoanFacilityOffSCBList[i].Status);

                        f = f + 1;
                    }
                }

            }





            //OFF SCB Facility List end





            var m = 0;
            var n = 0;
            var p = 0;


            for (var i = 0; i < res.value.objAutoLoanSecurityList.length; i++) {
                if (res.value.objAutoLoanSecurityList[i].FacilityType == 22 || res.value.objAutoLoanSecurityList[i].FacilityType == 18) {
                    var idfieldforExposerForCreditCardSequrity = p + 1;
                    var typeforCreditCardobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforCreditCardobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForcreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        //$('txtSecurityIssuingOfficeCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                        //$('txtSecurityIssuingDateCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());


                        var facalityObjectForCredtcard = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForCredtcard != undefined) {
                            var outStandingForcreditCard = facalityObjectForCredtcard.PresentLimit;

                            var ltvForCreditturnOver = parseFloat(outStandingForcreditCard / res.value.objAutoLoanSecurityList[i].XTVRate)*100;
                            if (util.isFloat(ltvForCreditturnOver)) {
                                $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue(ltvForCreditturnOver.toFixed(2));
                            }
                            else {
                                $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                            }
                        }
                        else {
                            $('txtLTVForSecurityForCreditCard' + idfieldforExposerForCreditCardSequrity).setValue('0');
                        }

                    }

                    p = p + 1;
                }
                else if (res.value.objAutoLoanSecurityList[i].FacilityType == 23) {
                    var idfieldforExposerForOverDraftSequrity = n + 1;
                    var typeforOverDraftobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                    if (typeforOverDraftobjectForSequrity.FACI_CODE == "S") {
                        $('cmbTypeForSequritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPVForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecuritiesForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        //$('txtSecurityIssuingOfficeOverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                        //$('txtSecurityIssuingDateOverDraft' + idfieldforExposerForOverDraftSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());


                        var facalityObjectForOverDraft = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectForOverDraft != undefined) {
                            var originalLimit = facalityObjectForOverDraft.PresentLimit;

                            var ltvForOverDraft = parseFloat(originalLimit / res.value.objAutoLoanSecurityList[i].XTVRate)*100;
                            if (ltvForOverDraft != Infinity) {
                                $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue(ltvForOverDraft.toFixed(2));
                            }
                            else {
                                $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                            }
                        }
                        else {
                            $('txtLTVForSecurityForoverDraft' + idfieldforExposerForOverDraftSequrity).setValue('0');
                        }

                    }

                    n = n + 1;
                }
                else {
                    var idfieldforExposerForTermLoanSequrity = m + 1;
                    if (idfieldforExposerForTermLoanSequrity < 5) {
                        var typeforTermLoanobjectForSequrity = Page.facilityArray.findByProp('FACI_ID', res.value.objAutoLoanSecurityList[i].FacilityType);
                        //if (typeforTermLoanobjectForSequrity.FACI_CODE == "U") {
                        $('cmbTypeForSequrities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                        $('txtSecurityFV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                        $('txtSecurityPV' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                        $('cmbStatusForSecurities' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].Status);
                        //$('txtSecurityIssuingOfficeTermLoan' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                        //$('txtSecurityIssuingDateTermLoan' + idfieldforExposerForTermLoanSequrity).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());

                        var facalityObjectFortermLoan = Page.facilityListArray.findByProp('FacilityType', res.value.objAutoLoanSecurityList[i].FacilityType);
                        if (facalityObjectFortermLoan != undefined) {
                            var outStandingFortermloan = facalityObjectFortermLoan.PresentBalance;
                            var ltvFortermLoan = parseFloat(outStandingFortermloan / res.value.objAutoLoanSecurityList[i].XTVRate)*100;
                            if (ltvFortermLoan == Infinity) {
                                $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                            }
                            else {
                                if (util.isFloat(ltvFortermLoan)) {
                                    $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue(ltvFortermLoan.toFixed(2));
                                }
                                else {
                                    $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                                }
                            }
                        }
                        else {
                            $('txtLTVForSecurity' + idfieldforExposerForTermLoanSequrity).setValue('0');
                        }

                        //}

                        m = m + 1;
                    }
                }

            }
        }

        //        //6,9,12,20
        //        if (res.value.objAutoLoanMaster.StatusID == 6 || res.value.objAutoLoanMaster.StatusID == 9 || res.value.objAutoLoanMaster.StatusID == 12 || res.value.objAutoLoanMaster.StatusID == 20) {
        //            $("lnkDefer").style.display = 'none';
        //        }
        //        else {
        //            $("lnkDefer").style.display = 'block';
        //        }

    },
    getVendor: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanApplicationCISupport.GetVendor();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.vendoreArray = res.value;

        document.getElementById('cmbVendor').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vendor';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVendor').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].VendorName;
            opt.value = res.value[i].AutoVendorId;
            document.getElementById('cmbVendor').options.add(opt);
        }

    }

};
var analyzeLoanApplicationHelper = {

    searchKeypress: function(e) {
        if (e.keyCode == 13) {
            analyzeLoanApplicationManager.getLoanSummary();
        }
    },


    populateLoanSummeryGrid: function(rss) {
        var html = [];
        for (var i = 0; i < rss.value.length; i++) {
            // var serialNo = i + 1;
            //            <a title="Appeal" href="javascript:;" onclick="loanSummeryHelper.replayPopupShow(#{Auto_LoanMasterId})" class="icon iconReplay"></a>
            var btnWillBe = "";
            if (Page.LoanSummeryArray[i].StatusID == 17 || Page.LoanSummeryArray[i].StatusID == 19 || Page.LoanSummeryArray[i].StatusID == 21 || Page.LoanSummeryArray[i].StatusID == 24) {
                btnWillBe = '<input title="Process" type="button" id="btnAppeal" class="searchButton   widthSize100_per" value="Process" onclick=" analyzeLoanApplicationManager.Appeal(' + Page.LoanSummeryArray[i].Auto_LoanMasterId + ')"/>';
            }
            else if (Page.LoanSummeryArray[i].StatusID == 18 || Page.LoanSummeryArray[i].StatusID == 20 || Page.LoanSummeryArray[i].StatusID == 22 || Page.LoanSummeryArray[i].StatusID == 23 || Page.LoanSummeryArray[i].StatusID == 25 || Page.LoanSummeryArray[i].StatusID == 26 || Page.LoanSummeryArray[i].StatusID == 28 || Page.LoanSummeryArray[i].StatusID == 33 || Page.LoanSummeryArray[i].StatusID == 34 || Page.LoanSummeryArray[i].StatusID == 35) {
                btnWillBe = '<input title="Open" type="button" id="btnPrintBA" class="searchButton   widthSize100_per" value="Open" onclick=" analyzeLoanApplicationManager.SetLLIDAndSearch(' + Page.LoanSummeryArray[i].LLID + ')"/>';
            }
            var className = i % 2 ? '' : 'odd';
            Page.LoanSummeryArray[i].btnWillBe = btnWillBe;
            html.push('<tr class="' + className + '"><td>#{LLID}</td><td>#{PrName}</td><td>#{ProductName}</td><td>#{ManufacturerName}</td><td>#{Model}</td><td>#{AppliedAmount}</td><td>#{AskingTenor}</td><td>#{StatusName}</td><td>#{btnWillBe}</td></tr>'
                    .format2((i % 2 ? '' : 'odd'), true)
                    .format(Page.LoanSummeryArray[i])
            );

        }
        $('GridLoanSummery').update(html.join(''));
        var totalCount = 0;

        if (rss.value.length != 0) {
            totalCount = Page.LoanSummeryArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);

    },


    showJointApplicant: function() {
        var reasonofJointApplicant = $F("cmbReasonofJointApplicant");
        var jtPrimaryProfession = parseInt($F("txtJtProfessionId"));
        var prPrimaryProfession = parseInt($F("txtPrProfessionId"));

    },



    changeCashSecured: function() {
        //        var cashSecured = $F("cmbCashSecured");
        //        if (cashSecured == "1") {
        //            $("lblincomeGeneratorAlertMasg").innerHTML = "Income Assessment Not Required";
        //        }
        //        else {
        //            $("lblincomeGeneratorAlertMasg").innerHTML = "";
        //        }
    },
    changeARTA: function() {
        var arta = $F("cmbARTA");
        if (arta == "1") {
            var age = parseInt($F("txtCurrenAge"));
            if (age > 60 && age <= 65) {
                $('cmbARTAReference').setValue('1');
                $('cmbARTAReference').disabled = 'false';
            }
        }
        else {
            $('cmbARTAReference').setValue('-1');
            $('cmbARTAReference').disabled = '';
        }
    },
    changeCCBundle: function() {
        var arta = $F("cmbARTA");
        var generalInsurence = $F("cmbGeneralInsurence");
        var ccbundle = $F("cmbCCBundle");
        if ((arta == "1" || arta == "3") && (generalInsurence == "1" || generalInsurence == "3") && (ccbundle == "1" || ccbundle == "3")) {
            // Interest Rate is Standard Rate
        }
        else {
            //Following Table
        }

    },
    changeSeatingCapacity: function() {
        var seatingCapacity = parseInt($F("cmsSeatingCapacity"));
        if (seatingCapacity > 9) {
            alert("Cannot be more than 9 without Level 3");
            $('cmsSeatingCapacity').setValue('9');

        }
    },
    changeCarVerification: function() {
        var carVerification = $F("cmbCarVerification");
        if (carVerification == 1) {
            $('divLblDeliveryStatus').style.display = 'block';
            $('divDeliveryStatus').style.display = 'block';

        }
        else {
            $('divLblDeliveryStatus').style.display = 'none';
            $('divDeliveryStatus').style.display = 'none';
        }
    },
    changeSourceNameForSourceCodeOnApplicantDetailsForGeneral: function() {
        var sourceId = parseInt($F("cmbBranch"));
        var source = Page.sourceForGeneralArray.findByProp('AutoSourceId', sourceId);
        if (source) {
            $('txtBranchCode').setValue(source.SourceCode);
        }

    },

    changeBrowingRelationship: function() {
        var browingrelationship = $F("cmbBrowingRelationship");
        $("txtRelationshipCode").setValue(browingrelationship);
    },
    populateBankCombo: function(res) {
        var link = "<option value=\"-1\">Select a bank</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BankID + "\">" + res.value[i].BankName + "</option>";
        }
        $("cmbBankNameForOther1").update(link);
        $("cmbBankNameForOther2").update(link);
        $("cmbBankNameForOther3").update(link);
        $("cmbBankNameForOther4").update(link);
        $("cmbBankNameForOther5").update(link);

        //Exposer Off Scb Finential Institution

        $("cmbFinancialInstitutionForOffSCB1").update(link);
        $("cmbFinancialInstitutionForOffSCB2").update(link);
        $("cmbFinancialInstitutionForOffSCB3").update(link);
        $("cmbFinancialInstitutionForOffSCB4").update(link);
        $("cmbFinancialInstitutionForCreditCardOffSCB1").update(link);
        $("cmbFinancialInstitutionForCreditCardOffSCB2").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB1").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB2").update(link);
        $("cmbFinancialInstitutionForOverDraftOffSCB3").update(link);




    },
    populateFacilityCombo: function(res) {
        var link = "<option value=\"-1\">Select a Type</option>";
        var link1 = "<option value=\"-1\">Select a Type</option>";


        for (var i = 0; i < res.value.length; i++) {
            if (res.value[i].FACI_NAME == "Credit Card ($)".trim() || res.value[i].FACI_NAME == "Credit Card (BDT)".trim()) {
                link1 += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
            }
            else {
                if (res.value[i].FACI_NAME != "Over Draft".trim()) {
                    link += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
                }
            }
        }
        $("cmbTypeForTermLoans1").update(link);
        $("cmbTypeForTermLoans2").update(link);
        $("cmbTypeForTermLoans3").update(link);
        $("cmbTypeForTermLoans4").update(link);

        $("cmbTypeForCreditCard1").update(link1);
        $("cmbTypeForCreditCard2").update(link1);

        Page.facilityArray = res.value;

    },


    changeTypeForExposer: function(elementId) {
        var type = $F(elementId);

        var typeobject = Page.facilityArray.findByProp('FACI_ID', type);
        if (typeobject) {
            switch (elementId) {
                case "cmbTypeForTermLoans1":
                    $("txtflag1").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans2":
                    $("txtflag2").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans3":
                    $("txtflag3").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForTermLoans4":
                    $("txtflag4").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForCreditCard1":
                    $("txtFlagForcreditCard1").setValue(typeobject.FACI_CODE);
                    break;
                case "cmbTypeForCreditCard2":
                    $("txtFlagForcreditCard2").setValue(typeobject.FACI_CODE);
                    break;
            }
        }
        else {
            switch (elementId) {
                case "cmbTypeForTermLoans1":
                    $("txtflag1").setValue('');
                    break;
                case "cmbTypeForTermLoans2":
                    $("txtflag2").setValue('');
                    break;
                case "cmbTypeForTermLoans3":
                    $("txtflag3").setValue('');
                    break;
                case "cmbTypeForTermLoans4":
                    $("txtflag4").setValue('');
                    break;
                case "cmbTypeForCreditCard1":
                    $("txtFlagForcreditCard1").setValue('');
                    break;
                case "cmbTypeForCreditCard2":
                    $("txtFlagForcreditCard2").setValue('');
                    break;
            }
        }

        analyzeLoanApplicationHelper.populateColleterization();
    },
    populateBranchCombo: function(res, elementId) {
        var link = "<option value=\"-1\">Select a Branch</option>";

        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BRAN_ID + "\">" + res.value[i].BRAN_NAME + "</option>";
        }

        if (elementId == "cmbBankNameForOther1") {
            $("cmbBranchNameOther1").update(link);
        }
        else if (elementId == "cmbBankNameForOther2") {
            $("cmbBranchNameOther2").update(link);
        }
        else if (elementId == "cmbBankNameForOther3") {
            $("cmbBranchNameOther3").update(link);
        }
        else if (elementId == "cmbBankNameForOther4") {
            $("cmbBranchNameOther4").update(link);
        }
        else if (elementId == "cmbBankNameForOther5") {
            $("cmbBranchNameOther5").update(link);
        }
    },

    calculateEMIShare: function(rowId) {
        var emi = $F("txtEMI" + rowId);
        var emiPercesnt = $F("txtEMIPer" + rowId);
        if (emi == "") {
            emi = 0;
        }
        if (emiPercesnt == "") {
            emiPercesnt = 0;
        }
        if (!util.isFloat(emi)) {
            alert("Number field is required");
            $('txtEMI' + rowId).setValue('0');
            $("txtEMIShare" + rowId).setValue('0');
            //$("txtEMI" + rowId).focus();
            return false;
        }
        if (!util.isFloat(emiPercesnt)) {
            alert("Number field is required");
            $('txtEMIPer' + rowId).setValue('0');
            $("txtEMIShare" + rowId).setValue('0');
            //$("txtEMIPer" + rowId).focus();
            return false;
        }
        var emiShare = parseFloat(emi * parseFloat(emiPercesnt / 100));

        $("txtEMIShare" + rowId).setValue(emiShare.toFixed(2));
    },
    changeoverDraftInterest: function(rowId) {
        var originalLimit = $F("txtOverdraftlimit" + rowId);
        var interestRate = $F("txtOverdraftInterest" + rowId);
        var sequrityPv = $F("txtSecurityPVForoverDraft" + rowId);
        if (originalLimit == "") {
            originalLimit = 0;
        }
        if (interestRate == "") {
            interestRate = 0;
        }


        if (!util.isFloat(originalLimit)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtOverdraftInteres" + rowId).setValue('0');
            //$("txtOverdraftlimit" + rowId).focus();
            return false;
        }
        if (!util.isFloat(interestRate)) {
            alert("Number field is required");
            $('txtOverdraftInterest' + rowId).setValue('0');
            $("txtOverdraftInteres" + rowId).setValue('0');
            //$("txtOverdraftInterest" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtSecurityPVForoverDraft" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var overDraftInterest = parseFloat(originalLimit * parseFloat((interestRate / 100) / 12));

        $("txtOverdraftInteres" + rowId).setValue(overDraftInterest.toFixed(2));

        if (sequrityPv == "") {
            sequrityPv = 0;
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            return false;
        }

        var ltv = parseFloat(originalLimit / sequrityPv);

        $("txtLTVForSecurityForoverDraft" + rowId).setValue(ltv.toFixed(2));

    },

    changeCreditCardInterestForOffSCB: function(rowId) {
        var origLimit = $F("txtOriginalLimitForcreditCardOffScb" + rowId);
        var inteRate = 5; //$F("txtInterestRateForCreditCardForOffScb" + rowId);

        var interest = (inteRate / 100) * origLimit;
        $('txtInterestForCreditCardForOffScb' + rowId).setValue(interest);
    },


    changeTurnOverShareForOffSCB: function(rowId) {
        var emi = $F("txtEMIforOffSCB" + rowId);
        var emiper = $F("txtEMIPerForOffSCB" + rowId);
        if (emi == "") {
            emi = 0;
        }
        if (emiper == "") {
            emiper = 0;
        }
        if (!util.isFloat(emi)) {
            alert("Number field is required");
            $('txtEMIforOffSCB' + rowId).setValue('0');
            $("txtEMIShareForOffSCB" + rowId).setValue('0');
            //$("txtEMIforOffSCB" + rowId).focus();
            return false;
        }
        if (!util.isFloat(emiper)) {
            alert("Number field is required");
            $('txtEMIPerForOffSCB' + rowId).setValue('0');
            $("txtEMIShareForOffSCB" + rowId).setValue('0');
            //$("txtEMIPerForOffSCB" + rowId).focus();
            return false;
        }
        var emiShare = parseFloat(emi * parseFloat(emiper / 100));

        $("txtEMIShareForOffSCB" + rowId).setValue(emiShare.toFixed(2));
    },
    changeOverDraftInterstForOffSCB: function(rowId) {
        var originalLimit = $F("txtOriginalLimitForOverDraftOffScb" + rowId);
        var interestRate = $F("txtinterestRateForOverDraftOffSCB" + rowId);
        if (originalLimit == "") {
            originalLimit = 0;
        }
        if (interestRate == "") {
            interestRate = 0;
        }
        if (!util.isFloat(originalLimit)) {
            alert("Number field is required");
            $('txtOriginalLimitForOverDraftOffScb' + rowId).setValue('0');
            $("txtInterestForOverDraftInterest" + rowId).setValue('0');
            //$("txtOriginalLimitForOverDraftOffScb" + rowId).focus();
            return false;
        }
        if (!util.isFloat(interestRate)) {
            alert("Number field is required");
            $('txtOriginalLimitForOverDraftOffScb' + rowId).setValue('0');
            $("txtInterestForOverDraftInterest" + rowId).setValue('0');
            //$("txtOriginalLimitForOverDraftOffScb" + rowId).focus();

            return false;
        }
        var overDraftInterest = parseFloat(originalLimit * parseFloat((interestRate / 100) / 12));

        $("txtInterestForOverDraftInterest" + rowId).setValue(overDraftInterest.toFixed(2));
    },
    calculateLTVForSequrity: function(rowId) {
        var outStandingLimit = $F("txtoutStanding" + rowId);
        var sequrityPv = $F("txtSecurityPV" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $("txtSecurityPV" + rowId).setValue('0');
            $("txtLTVForSecurity" + rowId).setValue('0');
            return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtoutStanding' + rowId).setValue('0');
            $("txtLTVForSecurity" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurity' + rowId).setValue('0');
            $("txtSecurityPV" + rowId).setValue('0');
            // $("txtSecurityPV" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv)*100;

        $("txtLTVForSecurity" + rowId).setValue(ltv.toFixed(2));
    },

    calculateLTVForCreditCardSequrity: function(rowId) {

        var outStandingLimit = $F("txtOriginalLimitForCreditCard" + rowId);
        var sequrityPv = $F("txtSecurityPVForCreditCard" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
            $("txtOriginalLimitForCreditCard" + rowId).setValue('0');
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            //return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            $("txtOriginalLimitForCreditCard" + rowId).setValue('0');
            //$("txtOriginalLimitForCreditCard" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurityForCreditCard' + rowId).setValue('0');
            $("txtSecurityPVForCreditCard" + rowId).setValue('0');
            //$("txtSecurityPVForCreditCard" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv)*100;
        if (ltv != Infinity) {
            $("txtLTVForSecurityForCreditCard" + rowId).setValue(ltv.toFixed(2));
        }
        else {
            $("txtLTVForSecurityForCreditCard" + rowId).setValue('0');
        }

        //Calculating credit card facility
        var origLimit = $F("txtOriginalLimitForCreditCard" + rowId);
        var inteRate = 5; //$F("txtInterestRateCreditCard" + rowId);

        var interest = Math.round((inteRate / 100) * origLimit);
        if (interest != Infinity) {
            $('txtInterestForCreditCard' + rowId).setValue(interest);
        }
        else {
            $('txtInterestForCreditCard' + rowId).setValue('0');
        }

    },
    calculateLTVForOverDraftSequrity: function(rowId) {
        var outStandingLimit = $F("txtOverdraftlimit" + rowId);
        var sequrityPv = $F("txtSecurityPVForoverDraft" + rowId);
        if (outStandingLimit == "") {
            outStandingLimit = 0;
        }
        if (sequrityPv == "" || sequrityPv == "0") {
            sequrityPv = 0;
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            return false;
        }
        if (!util.isFloat(outStandingLimit)) {
            alert("Number field is required");
            $('txtOverdraftlimit' + rowId).setValue('0');
            $("txtLTVForSecurityForoverDraft" + rowId).setValue('0');
            //$("txtOverdraftlimit" + rowId).focus();
            return false;
        }
        if (!util.isFloat(sequrityPv)) {
            alert("Number field is required");
            $('txtLTVForSecurityForoverDraft' + rowId).setValue('0');
            $("txtSecurityPVForoverDraft" + rowId).setValue('0');
            //$("txtSecurityPV" + rowId).focus();

            return false;
        }
        var ltv = parseFloat(outStandingLimit / sequrityPv)*100;

        $("txtLTVForSecurityForoverDraft" + rowId).setValue(ltv.toFixed(2));
    },




    ValidateSave: function() {



        //Analyst Master
        if ($F("cmbColleterization") == "-1" && $F("cmbColleterizationWith") != "-1") {
            alert("Please select Colleterization With");
            //$("cmbColleterizationWith").focus();
            return false;
        }
        if ($F("cmbColleterization") != "-1" && $F("cmbColleterizationWith") == "-1") {
            alert("Please select Colleterization");
            //$("cmbColleterization").focus();
            return false;
        }


        var objAutoAnalistMaster = new Object();


        objAutoAnalistMaster.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoAnalistMaster.LlId = $F('txtLLID');
        objAutoAnalistMaster.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        objAutoAnalistMaster.IsJoint = $F('txtIsJoint') == '0' ? false : true;
        objAutoAnalistMaster.ReasonOfJoint = $('cmbReasonofJointApplicant').options[$('cmbReasonofJointApplicant').selectedIndex].text;
        objAutoAnalistMaster.Colleterization = $F("cmbColleterization");
        objAutoAnalistMaster.ColleterizationWith = $F("cmbColleterizationWith");

        // Application Details

        var objAutoLoanAnalystApplication = new Object();

        objAutoLoanAnalystApplication.Id = $F('txtloanAnalystApplicationID');
        objAutoLoanAnalystApplication.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoLoanAnalystApplication.Source = $F('cmbSource1');
        objAutoLoanAnalystApplication.BranchId = $F('cmbBranch');
        objAutoLoanAnalystApplication.BranchCode = $F('txtBranchCode');
        objAutoLoanAnalystApplication.ValuePack = $F('cmbValuePack');
        objAutoLoanAnalystApplication.BorrowingRelationShip = $F('cmbBrowingRelationship');
        objAutoLoanAnalystApplication.BorrowingRelationShipCode = $F('txtRelationshipCode');
        objAutoLoanAnalystApplication.CustomerStatus = $F('cmbCustomerStatus');
        objAutoLoanAnalystApplication.AskingRepaymentMethod = $F('cmbAskingRepaymentmethod');
        objAutoLoanAnalystApplication.ARTA = $F('cmbARTA');
        objAutoLoanAnalystApplication.ARTAStatus = $F('cmbARTAReference');
        objAutoLoanAnalystApplication.GeneralInsurance = $F('cmbGeneralInsurence');
        objAutoLoanAnalystApplication.GeneralInsurancePreference = $F('cmbGeneralInsurencePreference');
        objAutoLoanAnalystApplication.CashSecured100Per = $F('cmbCashSecured') == '0' ? false : true;
        objAutoLoanAnalystApplication.CCBundle = $F('cmbCCBundle') == '0' ? false : true;
        objAutoLoanAnalystApplication.SecurityValue = $F('txtSequrityValue') == '' ? 0 : $F('txtSequrityValue');

        //auto loan vehicle

        var objAutoLoanVehicle = new Object();
        objAutoLoanVehicle.AutoLoanVehicleId = $F('txtAutoLoanVehicleId');
        objAutoLoanVehicle.VendorId = $F('cmbVendor');

        //new vendor
        var objAutoVendor = new Object();
        objAutoVendor.VendorName = $F('txtVendorName');
        //            objAutoVendor.DealerType = $F('cmbDealerType');
        objAutoVendor.DealerType = '1';
        //new vendor end

        //continue auto loan vehicle

        objAutoLoanVehicle.Auto_ManufactureId = $F('cmbManufacturer');

        var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;

        if (manufacturName != "Other") {
            objAutoLoanVehicle.VehicleId = $F('cmbModel');
            var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
            if (vehicleName != "Other") {
                objAutoLoanVehicle.Model = $F('hdnModel');
                objAutoLoanVehicle.VehicleType = $F('txtVehicleType');
            }
            else {
                objAutoLoanVehicle.Model = vehicleName;
                objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
            }

        }
        else {
            objAutoLoanVehicle.VehicleId = "-7"; //Vehicle -7= Others
            objAutoLoanVehicle.Model = $F('txtModelName');
            objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
        }
        objAutoLoanVehicle.TrimLevel = $F('txtTrimLevel');
        objAutoLoanVehicle.EngineCC = $F('txtEngineCapacityCC');
        objAutoLoanVehicle.VehicleStatus = $F('cmdVehicleStatus');
        objAutoLoanVehicle.ManufacturingYear = $F('txtManufacturaryear') == '' ? '0' : $F('txtManufacturaryear');
        objAutoLoanVehicle.QuotedPrice = $F('txtQuotedPrice') == '' ? '0' : $F('txtQuotedPrice');
        objAutoLoanVehicle.VerifiedPrice = $F('txtConsideredprice') == '' ? '0' : $F('txtConsideredprice');

        objAutoLoanVehicle.PriceConsideredBasedOn = $F('cmbPriceConsideredOn');
        objAutoLoanVehicle.SeatingCapacity = $F('cmsSeatingCapacity');
        objAutoLoanVehicle.CarVerification = $F('cmbCarVerification');


        var objAutoLoanAnalystVehicleDetail = new Object();

        objAutoLoanAnalystVehicleDetail.Id = $F('txtVehicleDetailID');
        objAutoLoanAnalystVehicleDetail.AnalystMasterId = $F('txtAnalystMasterID');
        objAutoLoanAnalystVehicleDetail.valuePackAllowed = $F('cmbvaluePackAllowed') == '0' ? false : true; ;
        objAutoLoanAnalystVehicleDetail.SeatingCapacity = $F('cmsSeatingCapacity');
        objAutoLoanAnalystVehicleDetail.ConsideredPrice = $F('txtConsideredprice');
        objAutoLoanAnalystVehicleDetail.PriceConsideredOn = $F('cmbPriceConsideredOn');
        objAutoLoanAnalystVehicleDetail.CarVarification = $F('cmbCarVerification');
        objAutoLoanAnalystVehicleDetail.DeliveryStatus = $F('cmbDeliveryStatus');
        objAutoLoanAnalystVehicleDetail.RegistrationYear = $F('txtregistrationYear') == '' ? '0' : $F('txtregistrationYear'); // $F('txtregistrationYear');


        // With SCB
        var objAutoApplicantAccountSCBList = [];

        var tablePrAccount = $('tblApplicantAccountSCB');
        var rowCount = tablePrAccount.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoApplicantAccountSCB = new Object();
            if ($F('txtAccountNumberForSCB' + i) == '') {
            } else {
                if ($F('cmbAccountStatusForSCB' + i) == '0') {
                    alert('please select Account Status');
                    //$('cmbAccountStatusForSCB' + i).focus();
                    return false;
                }

                else {
                    objAutoApplicantAccountSCB.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objAutoApplicantAccountSCB.LLID = $F('txtLLID');
                    objAutoApplicantAccountSCB.AccountNumberSCB = $F('txtAccountNumberForSCB' + i);
                    objAutoApplicantAccountSCB.AccountStatus = $F('cmbAccountStatusForSCB' + i);
                    objAutoApplicantAccountSCB.PriorityHolder = $F('cmbPriorityAcHolderForSCB' + i);

                    objAutoApplicantAccountSCBList.push(objAutoApplicantAccountSCB);

                }

            }
        }
        //With SCB End


        // Other Bank

        var objAutoApplicantAccountOtherList = [];

        var tableOtherBank = $('tblApplicantAccountOther');
        var rowCount = tableOtherBank.rows.length;
        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoApplicantAccountOther = new Object();
            if ($F('cmbBankNameForOther' + i) == '-1') {
            } else {
                if ($F('cmbBranchNameOther' + i) == '-1') {
                    alert('please select Branch');
                    //$('cmbBranchNameOther' + i).focus();
                    return false;
                }

                else {
                    if ($F('txtAccountNumberother' + i) == '') {
                        alert('please Input Account Number');
                        //$('txtAccountNumberother' + i).focus();
                        return false;
                    }

                    else {

                        if ($F('cmbAccountStatus' + i) == '0') {
                            alert('please Select Account Status');
                            //$('cmbAccountStatus' + i).focus();
                            return false;
                        }

                        else {

                            if ($F('cmbAccountType' + i) == '0') {
                                alert('please Select Account Type');
                                //$('cmbAccountType' + i).focus();
                                return false;
                            }

                            else {


                                objAutoApplicantAccountOther.AutoLoanMasterId = $F('txtAutoLoanMasterId');

                                objAutoApplicantAccountOther.BankId = $F('cmbBankNameForOther' + i);
                                objAutoApplicantAccountOther.BranchId = $F('cmbBranchNameOther' + i);
                                objAutoApplicantAccountOther.AccountNumber = $F('txtAccountNumberother' + i);
                                objAutoApplicantAccountOther.AccountCategory = $F('cmbAccountStatus' + i);
                                objAutoApplicantAccountOther.AccountType = $F('cmbAccountType' + i);

                                objAutoApplicantAccountOtherList.push(objAutoApplicantAccountOther);
                            }
                        }
                    }
                }

            }
        }

        // Other bank end





        //Exposures start

        //ON SCBFacility start


        var objAutoFacilityList = [];
        var objAutoSecurityList = [];

        //Term Loan
        var tablePrAccount = $('tblFacilityTermLoan');
        var rowCount = tablePrAccount.rows.length;

        for (var i = 1; i <= rowCount - 1; i++) {
            var objAutoFacility = new Object();
            if ($F('cmbTypeForTermLoans' + i) == '-1') {
            }
            else {
                if ($F('txtInterestRate' + i) == '') {
                    //alert('please input Interest rate.');
                    //$('txtInterestRate' + i).focus();
                    //return false;
                    $('txtInterestRate' + i).setValue('0');
                }
                if ($F('txtLimit' + i) == '') {
                    //alert('please input Limit.');
                    //$('txtLimit' + i).focus();
                    //return false;
                    $('txtLimit' + i).setValue('0');
                }
                if ($F('txtoutStanding' + i) == '') {
                    //alert('please input Outstanding.');
                    //$('txtoutStanding' + i).focus();
                    //return false;
                    $('txtoutStanding' + i).setValue('0');
                }
                if ($F('txtEMI' + i) == '') {
                    //alert('please input EMI.');
                    //$('txtEMI' + i).focus();
                    //return false;
                    $('txtEMI' + i).setValue('0');
                }
                if ($F('txtEMIPer' + i) == '') {
                    //alert('please input EMI Percent.');
                    //$('txtEMIPer' + i).focus();
                    //return false;
                    $('txtEMIPer' + i).setValue('0');
                }
                //else {


                objAutoFacility.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacility.FacilityTypeName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                objAutoFacility.FacilityType = $F('cmbTypeForTermLoans' + i);
                objAutoFacility.InterestRate = $F('txtInterestRate' + i) == '' ? '0' : $F('txtInterestRate' + i);
                objAutoFacility.PresentBalance = $F('txtoutStanding' + i) == '' ? '0' : $F('txtoutStanding' + i);
                objAutoFacility.PresentEMI = $F('txtEMI' + i) == '' ? '0' : $F('txtEMI' + i);
                objAutoFacility.PresentLimit = $F('txtLimit' + i) == '' ? '0' : $F('txtLimit' + i);
                objAutoFacility.EMIPercent = $F('txtEMIPer' + i) == '' ? '0' : $F('txtEMIPer' + i);
                objAutoFacility.EMIShare = $F('txtEMIShare' + i) == '' ? '0' : $F('txtEMIShare' + i);
                objAutoFacility.EMIShare = $F('txtEMIShare' + i) == '' ? '0' : $F('txtEMIShare' + i);

                objAutoFacilityList.push(objAutoFacility);

                var objAutoSecurityTermLoan = new Object();
                objAutoSecurityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityTermLoan.FacilityType = $F('cmbTypeForTermLoans' + i);
                objAutoSecurityTermLoan.NatureOfSecurity = $F('cmbTypeForSequrities' + i);
                objAutoSecurityTermLoan.Status = $F('cmbStatusForSecurities' + i);
                objAutoSecurityTermLoan.FaceValue = $F('txtSecurityFV' + i) == '' ? '0' : $F('txtSecurityFV' + i);
                objAutoSecurityTermLoan.XTVRate = $F('txtSecurityPV' + i) == '' ? '0' : $F('txtSecurityPV' + i);
                objAutoSecurityList.push(objAutoSecurityTermLoan);

                //}

            }
        }
        // trem Loan end

        //CreditCard Start

        var tablePrAccountCreditCard = $('tblFacilityCreditCard');
        var rowCountCreditCard = tablePrAccountCreditCard.rows.length;

        for (var i = 1; i <= rowCountCreditCard - 1; i++) {
            var objAutoFacilityCreditCard = new Object();
            if ($F('cmbTypeForCreditCard' + i) == '-1') {
            }
            else {
                if ($F('txtOriginalLimitForCreditCard' + i) == '') {
                    //alert('please input Credit Card Limit.');
                    //$('txtOriginalLimitForCreditCard' + i).focus();
                    //return false;
                    $('txtOriginalLimitForCreditCard' + i).setValue('0');
                }
                //                else if ($F('txtInterestForCreditCard' + i) == '') {
                //                    alert('please input Credit Card Interest.');
                //                    //$('txtInterestForCreditCard' + i).focus();
                //                    return false;
                //                }
                //else {

                objAutoFacilityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacilityCreditCard.FacilityTypeName = $('cmbTypeForCreditCard' + i).options[$('cmbTypeForCreditCard' + i).selectedIndex].text;
                objAutoFacilityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
                objAutoFacilityCreditCard.InterestRate = 5; //$F('txtInterestRateCreditCard' + i) == '' ? '0' : $F('txtInterestRateCreditCard' + i);
                objAutoFacilityCreditCard.PresentLimit = $F('txtOriginalLimitForCreditCard' + i) == '' ? '0' : $F('txtOriginalLimitForCreditCard' + i);

                objAutoFacilityList.push(objAutoFacilityCreditCard);

                var objAutoSecurityCreditCard = new Object();
                objAutoSecurityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
                objAutoSecurityCreditCard.NatureOfSecurity = $F('cmbTypeForSequritiesForcreditCard' + i);
                objAutoSecurityCreditCard.Status = $F('cmbStatusForSecuritiesForCreditCard' + i);
                objAutoSecurityCreditCard.FaceValue = $F('txtSecurityFVForcreditCard' + i) == '' ? '0' : $F('txtSecurityFVForcreditCard' + i);
                objAutoSecurityCreditCard.XTVRate = $F('txtSecurityPVForCreditCard' + i) == '' ? '0' : $F('txtSecurityPVForCreditCard' + i);

                objAutoSecurityList.push(objAutoSecurityCreditCard);

                //}

            }
        }


        //Credit card end


        //OverDraft Start

        var tablePrAccountoverDraft = $('tblFacilityOverDraft');
        var rowCountoverDraft = tablePrAccountoverDraft.rows.length;

        for (var i = 1; i <= rowCountoverDraft - 1; i++) {
            var objAutoFacilityOverDraft = new Object();
            if ($F('txtOverdraftlimit' + i) == '') {
            }
            else {
                //                if ($F('txtOverdraftInterest' + i) == '') {
                //                    alert('please input Interest Rate for Over Draft.');
                //                    //$('txtOverdraftInterest' + i).focus();
                //                    return false;
                //                }
                //                else {

                objAutoFacilityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoFacilityOverDraft.FacilityTypeName = 'Over Draft';
                objAutoFacilityOverDraft.FacilityType = '23';
                objAutoFacilityOverDraft.PresentLimit = $F('txtOverdraftlimit' + i) == '' ? '0' : $F('txtOverdraftlimit' + i);
                objAutoFacilityOverDraft.InterestRate = $F('txtOverdraftInterest' + i) == '' ? '0' : $F('txtOverdraftInterest' + i);

                objAutoFacilityList.push(objAutoFacilityOverDraft);

                var objAutoSecurityOverDraft = new Object();
                objAutoSecurityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                objAutoSecurityOverDraft.FacilityType = '23';
                objAutoSecurityOverDraft.NatureOfSecurity = $F('cmbTypeForSequritiesForoverDraft' + i);
                objAutoSecurityOverDraft.Status = $F('cmbStatusForSecuritiesForoverDraft' + i);
                objAutoSecurityOverDraft.FaceValue = $F('txtSecurityFVForoverDraft' + i) == '' ? '0' : $F('txtSecurityFVForoverDraft' + i);
                objAutoSecurityOverDraft.XTVRate = $F('txtSecurityPVForoverDraft' + i) == '' ? '0' : $F('txtSecurityPVForoverDraft' + i);
                objAutoSecurityList.push(objAutoSecurityOverDraft);

                //               }

            }
        }
        //OverDraft end



        // start SECURITIES


        //var objAutoSecurityList = [];

        // start TERM LOANS
        //        var tableOnSCBSecurityTermLoan = $('tblOnSCBSecurityTermLoan');
        //        var rowCountTremLoan = tableOnSCBSecurityTermLoan.rows.length;

        //        for (var i = 1; i <= rowCountTremLoan - 1; i++) {
        //            var objAutoSecurityTermLoan = new Object();
        //            if ($F('cmbTypeForTermLoans' + i) == '-1') {
        //            } else {
        //                if ($F('cmbStatusForSecurities' + i) == '-1') {
        //                    alert('please Select Status for Security (Term Loan).');
        //                    //$('cmbStatusForSecurities' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityFV' + i) == '-1') {
        //                    //alert('please input FV for Security (Term Loan).');
        //                    //$('txtSecurityFV' + i).focus();
        //                    //return false;
        //                    $('txtSecurityFV' + i).setValue('0');
        //                }
        //                //                else if ($F('txtSecurityIssuingDateTermLoan' + i) == '-1') {
        //                //                    alert('please input Issue Date for Security (Term Loan).');
        //                //                    //$('txtSecurityIssuingDateTermLoan' + i).focus();
        //                //                    return false;
        //                //                }
        //                //                else if ($F('cmbTypeForSequrities' + i) == '-1') {
        //                //                    alert('please select Security Type (Term Loan).');
        //                //                    //$('cmbTypeForSequrities' + i).focus();
        //                //                    return false;
        //                //                }
        //                else if ($F('txtSecurityPV' + i) == '-1') {
        //                    //alert('please input PV for Security (Term Loan).');
        //                    //$('txtSecurityPV' + i).focus();
        //                    //return false;
        //                    $('txtSecurityPV' + i).setValue('0');
        //                }

        //                else {

        //                    objAutoSecurityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        //                    objAutoSecurityTermLoan.FacilityType = $F('cmbTypeForTermLoans' + i);
        //                    objAutoSecurityTermLoan.NatureOfSecurity = $F('cmbTypeForSequrities' + i);
        //                    objAutoSecurityTermLoan.Status = $F('cmbStatusForSecurities' + i);
        //                    //objAutoSecurityTermLoan.IssuingOffice = $F('txtSecurityIssuingOfficeTermLoan' + i);
        //                    objAutoSecurityTermLoan.FaceValue = $F('txtSecurityFV' + i) == '' ? '0' : $F('txtSecurityFV' + i);
        //                    objAutoSecurityTermLoan.XTVRate = $F('txtSecurityPV' + i) == '' ? '0' : $F('txtSecurityPV' + i);
        //                    //objAutoSecurityTermLoan.IssueDate = $F('txtSecurityIssuingDateTermLoan' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateTermLoan' + i));

        //                    objAutoSecurityList.push(objAutoSecurityTermLoan);

        //                }

        //            }
        //        }
        //TERM LOANS end



        // start CreditCard
        //        var tableOnSCBSecurityCreditCard = $('tblOnSCBSecurityCreditCard');
        //        var rowCountCreditCard = tableOnSCBSecurityCreditCard.rows.length;

        //        for (var i = 1; i <= rowCountCreditCard - 1; i++) {
        //            var objAutoSecurityCreditCard = new Object();
        //            if ($F('cmbTypeForCreditCard' + i) == '-1') {
        //            } else {
        //                if ($F('cmbStatusForSecuritiesForCreditCard' + i) == '-1') {
        //                    alert('please Select Status for Security (Credit Card).');
        //                    //$('cmbStatusForSecuritiesForCreditCard' + i).focus();
        //                    return false;
        //                }

        //                else if ($F('txtSecurityFVForcreditCard' + i) == '-1') {
        //                    alert('please input FV for Security (Credit Card).');
        //                    //$('txtSecurityFVForcreditCard' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityIssuingDateCreditCard' + i) == '-1') {
        //                    alert('please input Issue Date for Security (Credit Card).');
        //                    //$('txtSecurityIssuingDateCreditCard' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('cmbTypeForSequritiesForcreditCard' + i) == '-1') {
        //                    alert('please select Security Type (Credit Card).');
        //                    //$('cmbTypeForSequritiesForcreditCard' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityPVForCreditCard' + i) == '-1') {
        //                    alert('please input PV for Security (Credit Card).');
        //                    //$('txtSecurityPVForCreditCard' + i).focus();
        //                    return false;
        //                }

        //                else {

        //                    objAutoSecurityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        //                    objAutoSecurityCreditCard.FacilityType = $F('cmbTypeForCreditCard' + i);
        //                    objAutoSecurityCreditCard.NatureOfSecurity = $F('cmbTypeForSequritiesForcreditCard' + i);
        //                    objAutoSecurityCreditCard.Status = $F('cmbStatusForSecuritiesForCreditCard' + i);
        //                    objAutoSecurityCreditCard.IssuingOffice = $F('txtSecurityIssuingOfficeCreditCard' + i);
        //                    objAutoSecurityCreditCard.FaceValue = $F('txtSecurityFVForcreditCard' + i) == '' ? '0' : $F('txtSecurityFVForcreditCard' + i);
        //                    objAutoSecurityCreditCard.XTVRate = $F('txtSecurityPVForCreditCard' + i) == '' ? '0' : $F('txtSecurityPVForCreditCard' + i);
        //                    objAutoSecurityCreditCard.IssueDate = $F('txtSecurityIssuingDateCreditCard' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateCreditCard' + i));

        //                    objAutoSecurityList.push(objAutoSecurityCreditCard);

        //                }

        //            }
        //        }
        //CreditCard end


        // start OverDraft
        //        var tableOnSCBSecurityOverDraft = $('tblOnSCBSecurityOverDraft');
        //        var rowCountOverDraft = tableOnSCBSecurityOverDraft.rows.length;

        //        for (var i = 1; i <= rowCountOverDraft - 1; i++) {
        //            var objAutoSecurityOverDraft = new Object();
        //            if ($F('txtOverdraftlimit' + i) == '') {
        //            } else {
        //                if ($F('cmbStatusForSecuritiesForoverDraft' + i) == '-1') {
        //                    alert('please Select Status for Security (Over Draft).');
        //                    //$('cmbStatusForSecuritiesForoverDraft' + i).focus();
        //                    return false;
        //                }

        //                else if ($F('txtSecurityFVForoverDraft' + i) == '-1') {
        //                    alert('please input FV for Security (Over Draft).');
        //                    //$('txtSecurityFVForoverDraft' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityIssuingDateOverDraft' + i) == '-1') {
        //                    alert('please input Issue Date for Security (Over Draft).');
        //                    //$('txtSecurityIssuingDateOverDraft' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('cmbTypeForSequritiesForoverDraft' + i) == '-1') {
        //                    alert('please select Security Type (Over Draft).');
        //                    //$('cmbTypeForSequritiesForoverDraft' + i).focus();
        //                    return false;
        //                }
        //                else if ($F('txtSecurityPVForoverDraft' + i) == '-1') {
        //                    alert('please input PV for Security (Over Draft).');
        //                    //$('txtSecurityPVForoverDraft' + i).focus();
        //                    return false;
        //                }

        //                else {

        //                    objAutoSecurityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
        //                    objAutoSecurityOverDraft.FacilityType = '23';
        //                    objAutoSecurityOverDraft.NatureOfSecurity = $F('cmbTypeForSequritiesForoverDraft' + i);
        //                    objAutoSecurityOverDraft.Status = $F('cmbStatusForSecuritiesForoverDraft' + i);
        //                    objAutoSecurityOverDraft.IssuingOffice = $F('txtSecurityIssuingOfficeOverDraft' + i);
        //                    objAutoSecurityOverDraft.FaceValue = $F('txtSecurityFVForoverDraft' + i) == '' ? '0' : $F('txtSecurityFVForoverDraft' + i);
        //                    objAutoSecurityOverDraft.XTVRate = $F('txtSecurityPVForoverDraft' + i) == '' ? '0' : $F('txtSecurityPVForoverDraft' + i);
        //                    objAutoSecurityOverDraft.IssueDate = $F('txtSecurityIssuingDateOverDraft' + i) == '' ? Date.parseUK('01/01/0001') : Date.parseUK($F('txtSecurityIssuingDateOverDraft' + i));

        //                    objAutoSecurityList.push(objAutoSecurityOverDraft);

        //                }

        //            }
        //        }
        //OverDraft end

        // end SECURITIES



        //End ON SCB Facility





        //Off SCB Start
        //start TermLoan
        var objOFFSCBFacilityList = [];

        // start TERM LOANS
        var tableOFFSCBSecurityTermLoan = $('tblOFFSCBFacilityTermLoan');
        var rowCountOFFSCBTremLoan = tableOFFSCBSecurityTermLoan.rows.length;

        for (var i = 1; i <= rowCountOFFSCBTremLoan - 1; i++) {
            var objOFFSCBFacilityTermLoan = new Object();
            if ($F('cmbFinancialInstitutionForOffSCB' + i) == '-1') {
            } else {
                //                if ($F('txtOriginalLimitForOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Term Loan');
                //                    //$('txtOriginalLimitForOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtEMIforOffSCB' + i) == '') {
                //                    alert('please input EMI (OFF SCB Term Loan).');
                //                    // $('txtEMIforOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtEMIPerForOffSCB' + i) == '-1') {
                //                    alert('please input EMI Percent (OFF SCB Term Loan).');
                //                    // $('txtEMIPerForOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtEMIShareForOffSCB' + i) == '-1') {
                //                    alert('EMI Share is not calculated(OFF SCB Term Loan).');
                //                    // $('txtEMIShareForOffSCB' + i).focus();
                //                    return false;
                //                }
                //else 
                if ($F('cmbStatusForOffScb' + i) == '0') {
                    alert('please Select Status (OFF SCB Term Loan).');
                    //  $('cmbStatusForOffScb' + i).focus();
                    return false;
                }

                else {

                    objOFFSCBFacilityTermLoan.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objOFFSCBFacilityTermLoan.BankID = $F('cmbFinancialInstitutionForOffSCB' + i);
                    objOFFSCBFacilityTermLoan.OriginalLimit = $F('txtOriginalLimitForOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForOffScb' + i);
                    objOFFSCBFacilityTermLoan.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
                    objOFFSCBFacilityTermLoan.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    objOFFSCBFacilityTermLoan.EMIShare = $F('txtEMIShareForOffSCB' + i) == '' ? '0' : $F('txtEMIShareForOffSCB' + i);
                    objOFFSCBFacilityTermLoan.Status = $F('cmbStatusForOffScb' + i);
                    objOFFSCBFacilityTermLoan.Type = '1';

                    objOFFSCBFacilityList.push(objOFFSCBFacilityTermLoan);

                }

            }
        }
        //TERM LOANS end

        // start CreditCard
        var tableOFFSCBSecurityCreditCard = $('tblOFFSCBFacilityCreditCard');
        var rowCountOFFSCBCreditCard = tableOFFSCBSecurityCreditCard.rows.length;

        for (var i = 1; i <= rowCountOFFSCBCreditCard - 1; i++) {
            var objOFFSCBFacilityCreditCard = new Object();
            if ($F('cmbFinancialInstitutionForCreditCardOffSCB' + i) == '-1') {
            } else {
                //                if ($F('txtOriginalLimitForcreditCardOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Credit Card');
                //                    //   $('txtOriginalLimitForcreditCardOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtEMIforOffSCB' + i) == '') {
                //                    alert('please input EMI (OFF SCB Credit Card).');
                //                    //    $('txtEMIforOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtInterestRateForCreditCardForOffScb' + i) == '-1') {
                //                    alert('please input Interest Rate (OFF SCB Credit Card).');
                //                    //    $('txtInterestRateForCreditCardForOffScb' + i).focus();
                //                    return false;
                //                }
                //else 
                if ($F('cmbStatusForCreditCardOffScb' + i) == '0') {
                    alert('please Select Status (OFF SCB Credit Card).');
                    //   $('cmbStatusForCreditCardOffScb' + i).focus();
                    return false;
                }

                else {

                    objOFFSCBFacilityCreditCard.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objOFFSCBFacilityCreditCard.BankID = $F('cmbFinancialInstitutionForCreditCardOffSCB' + i);
                    objOFFSCBFacilityCreditCard.OriginalLimit = $F('txtOriginalLimitForcreditCardOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForcreditCardOffScb' + i);
                    objOFFSCBFacilityCreditCard.InterestRate = 5; //$F('txtInterestRateForCreditCardForOffScb' + i) == '' ? '0' : $F('txtInterestRateForCreditCardForOffScb' + i);
                    objOFFSCBFacilityCreditCard.Status = $F('cmbStatusForCreditCardOffScb' + i);

                    //                    objOFFSCBFacilityTermLoan.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
                    //                    objOFFSCBFacilityTermLoan.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    //                    objOFFSCBFacilityTermLoan.EMIShare = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    objOFFSCBFacilityCreditCard.Type = '2';


                    objOFFSCBFacilityList.push(objOFFSCBFacilityCreditCard);

                }

            }
        }
        //CreditCard end

        // start OverDraft
        var tableOFFSCBSecurityOverDraft = $('tblOFFSCBFacilityOverDraft');
        var rowCountOFFSCBOverDraft = tableOFFSCBSecurityOverDraft.rows.length;

        for (var i = 1; i <= rowCountOFFSCBOverDraft - 1; i++) {
            var objOFFSCBFacilityOverDraft = new Object();
            if ($F('cmbFinancialInstitutionForOverDraftOffSCB' + i) == '-1') {
            } else {
                //                if ($F('txtOriginalLimitForOverDraftOffScb' + i) == '') {
                //                    alert('please Input original Limit (OFF SCB Over Draft');
                //                    //   $('txtOriginalLimitForOverDraftOffScb' + i).focus();
                //                    return false;
                //                }

                //                else if ($F('txtinterestRateForOverDraftOffSCB' + i) == '') {
                //                    alert('please input Interest Rate (OFF SCB Over Draft).');
                //                    //    $('txtinterestRateForOverDraftOffSCB' + i).focus();
                //                    return false;
                //                }
                //                else if ($F('txtInterestForOverDraftInterest' + i) == '-1') {
                //                    alert('Interest did not calculated (OFF SCB Over Draft).');
                //                    //    $('txtInterestForOverDraftInterest' + i).focus();
                //                    return false;
                //                }
                //else 
                if ($F('cmbStatusForOverDraftOffScb' + i) == '0') {
                    alert('please Select Status (OFF SCB Over Draft).');
                    //    $('cmbStatusForOverDraftOffScb' + i).focus();
                    return false;
                }

                else {

                    objOFFSCBFacilityOverDraft.AutoLoanMasterId = $F('txtAutoLoanMasterId');
                    objOFFSCBFacilityOverDraft.BankID = $F('cmbFinancialInstitutionForOverDraftOffSCB' + i);
                    objOFFSCBFacilityOverDraft.OriginalLimit = $F('txtOriginalLimitForOverDraftOffScb' + i) == '' ? '0' : $F('txtOriginalLimitForOverDraftOffScb' + i);
                    objOFFSCBFacilityOverDraft.InterestRate = $F('txtinterestRateForOverDraftOffSCB' + i) == '' ? '0' : $F('txtinterestRateForOverDraftOffSCB' + i);
                    objOFFSCBFacilityOverDraft.Status = $F('cmbStatusForOverDraftOffScb' + i);

                    //                    objOFFSCBFacilityOverDraft.EMI = $F('txtEMIforOffSCB' + i) == '' ? '0' : $F('txtEMIforOffSCB' + i);
                    //                    objOFFSCBFacilityOverDraft.EMIPercent = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    //                    objOFFSCBFacilityOverDraft.EMIShare = $F('txtEMIPerForOffSCB' + i) == '' ? '0' : $F('txtEMIPerForOffSCB' + i);
                    objOFFSCBFacilityOverDraft.Type = '3';


                    objOFFSCBFacilityList.push(objOFFSCBFacilityOverDraft);

                }

            }
        }
        //OverDraft end
        var res = PlussAuto.AutoLoanApplicationCISupport.SaveCISupportLoanApp(objAutoAnalistMaster, objAutoLoanAnalystApplication, objAutoLoanAnalystVehicleDetail, objAutoApplicantAccountSCBList, objAutoApplicantAccountOtherList, objAutoFacilityList, objAutoSecurityList, objOFFSCBFacilityList, objAutoLoanVehicle, objAutoVendor);
        //        var res = PlussAuto.AutoLoanApplicationCISupport.SaveCISupportLoanApp(objAutoAnalistMaster);
        if (res.error) {
            alert(res.error.Message);
            return;
        }

        else if (res.value == "Success") {
            alert("Save Operation Success.");
            analyzeLoanApplicationManager.CloseLoanApp();

        }
        else {
            alert("Save Operation Failed");
        }
    },
    changeCashSequred: function() {
        var cashSecured = $F("cmbCashSecured");
        if (cashSecured == "1") {
            $('divlblSequrityValue').style.display = "block";
            $('divSequrityValueTextbox').style.display = "block";
        }
        else {
            $('divlblSequrityValue').style.display = "none";
            $('divSequrityValueTextbox').style.display = "none";
        }
    },
    changeSequrityValue: function() {
        var sequrityValue = $F("txtSequrityValue");
        if (!util.isFloat(sequrityValue)) {
            alert("required number field for sequrity value");
            $("txtSequrityValue").setValue('0');
            //$("txtSequrityValue").focus();
            return false;
        }
    },
    changeVendore: function() {
        var vendoreId = $F("cmbVendor");
        if (vendoreId == "-1") {
            $('txtVendorName').disabled = '';
            $('txtVendoreCode').setValue('');
        }
        else {
            var vdr = Page.vendoreArray.findByProp('AutoVendorId', vendoreId);
            if (vdr) {
                $('txtVendoreCode').setValue(vdr.VendorCode);
                $('txtVendorName').disabled = 'false';
                analyzeLoanApplicationManager.CheckMouByVendoreId();
            }
        }
    },
    changeManufacture: function() {
        var manufactureId = $F("cmbManufacturer");
        
        if (manufactureId == "-1") {
            $('txtManufacturarCode').setValue('');
        }
        else {
            var mnf = Page.manufactureArray.findByProp('ManufacturerId', manufactureId);
            if (mnf) {
                $('txtManufacturarCode').setValue(mnf.ManufacturerCode);

                var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;
                if (manufacturName != "Other") {
                    $('cmbModel').style.display = 'block';
                    $('txtVehicleType').style.display = 'block';
                    $('txtModelName').style.display = 'none';
                    $('cmbVehicleType').style.display = 'none';
                    $('txtVehicleModelCode').setValue('');

                    $('txtTrimLevel').disabled = 'false';
                    $('txtEngineCapacityCC').disabled = 'false';
                    $('txtVehicleType').disabled = 'false';
                    
                    analyzeLoanApplicationManager.getModelByManufactureId();
                }
                else {
                    $('cmbModel').style.display = 'none';
                    $('txtVehicleType').style.display = 'none';
                    $('txtModelName').style.display = 'block';
                    $('cmbVehicleType').style.display = 'block';
                    $('txtTrimLevel').disabled = '';
                    $('txtEngineCapacityCC').disabled = '';
                    $('txtVehicleType').disabled = '';

                    if (Page.vehicleDetailsArray != null && Page.vehicleDetailsArray.AutoLoanMasterId != 0) {
                        $('txtModelName').setValue(Page.vehicleDetailsArray.Model);
                        $('txtTrimLevel').setValue(Page.vehicleDetailsArray.TrimLevel);
                        $('txtEngineCapacityCC').setValue(Page.vehicleDetailsArray.EngineCC);
                        //
                        if (Page.vehicleDetailsArray.VehicleType != "") {
                            $('cmbVehicleType').setValue(vehicleType[Page.vehicleDetailsArray.VehicleType]);
                        }
                    }
                }

            }
        }
    },
    changeModel: function() {
        var vehicleId = $F('cmbModel');
        var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
        if (vehicleName != "Other") {
            $('txtVehicleType').style.display = 'block';
            $('cmbVehicleType').style.display = 'none';
            $('txtTrimLevel').disabled = 'false';
            $('txtEngineCapacityCC').disabled = 'false';
            $('txtVehicleType').disabled = 'false';
            var model = Page.modelArray.findByProp('VehicleId', vehicleId);
            if (model) {
                $('txtTrimLevel').setValue(model.TrimLevel);
                $('txtEngineCapacityCC').setValue(model.CC);
                $('hdnModel').setValue(model.Model);
                $('txtVehicleType').setValue(model.Type);
                $('txtManufacturaryear').setValue(model.ManufacturingYear);
                $('txtVehicleModelCode').setValue(model.ModelCode);
            }
            analyzeLoanApplicationManager.Getvaluepackallowed();
        }
        else {
            $('txtVehicleType').style.display = 'none';
            $('cmbVehicleType').style.display = 'block';
            $('txtTrimLevel').disabled = '';
            $('txtEngineCapacityCC').disabled = '';
            $('txtVehicleType').disabled = '';
            if (Page.vehicleDetailsArray != null && Page.vehicleDetailsArray.AutoLoanMasterId != 0) {
                //$('txtModelName').setValue(Page.vehicleDetailsArray.Model);
                $('txtTrimLevel').setValue(Page.vehicleDetailsArray.TrimLevel);
                $('txtEngineCapacityCC').setValue(Page.vehicleDetailsArray.EngineCC);
                //
                if (Page.vehicleDetailsArray.VehicleType != "") {
                    $('cmbVehicleType').setValue(vehicleType[Page.vehicleDetailsArray.VehicleType]);
                }
            }
        }
    },
    changeVehicleStatus: function() {
        var vehicleStatus = $F('cmdVehicleStatus');
        var vs = Page.vehicleStatusArray.findByProp('StatusId', vehicleStatus);
        if (vs) {
            $('txtVehicleStatusCode').setValue(vs.StatusCode);
        }
        else {
            $('txtVehicleStatusCode').setValue('');
        }

        if (vehicleStatus == "3") {
            $('txtregistrationYear').disabled = '';
        }
        else {
            $('txtregistrationYear').disabled = 'false';
        }

        analyzeLoanApplicationManager.Getvaluepackallowed();
    },
    changeregistationyear: function() {
        var regyear = $F("txtregistrationYear");
        if (!util.isDigit(regyear)) {
            alert("Registration Year must me a number Field");
            $('txtregistrationYear').setValue('0');
            // $('txtregistrationYear').focus();
            return false;
        }
    },
    changeManufactureryear: function() {
        var manuyear = $F("txtManufacturaryear");
        if (!util.isDigit(manuyear)) {
            alert("Manufacture Year must me a number Field");
            $('txtManufacturaryear').setValue('0');
            // $('txtManufacturaryear').focus();
            return false;
        }
    },
    populateColleterization: function() {
        var optlinkForColl = "<option value='-1'>Select From list</option>";
        var tablePrAccount = $('tblFacilityTermLoan');
        var rowCount = tablePrAccount.rows.length;

        for (var i = 1; i <= rowCount - 1; i++) {
            if ($F('cmbTypeForTermLoans' + i) == '-1') {
            }
            else {
                var facilityId = $F('cmbTypeForTermLoans' + i);
                var facilityName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                optlinkForColl += "<option value=" + facilityId + ">" + facilityName + "</option>";
            }
        }
        var prevcoll = $F("cmbColleterization");
        var prevCollWith = $F("cmbColleterizationWith");

        $("cmbColleterization").update(optlinkForColl);
        $("cmbColleterizationWith").update(optlinkForColl);

        $("cmbColleterization").setValue(prevcoll);
        $("cmbColleterizationWith").setValue(prevCollWith);

    },
    changeGeneralInsurence: function() {
        var generalInsurence = $F("cmbGeneralInsurencePreference");
        if (generalInsurence == "-1") {
            $('cmbGeneralInsurence').setValue("2");
            $('cmbGeneralInsurence').disabled = 'false';
        }
        else {
            $('cmbGeneralInsurence').disabled = '';
        }
    },
    changeLifeInsurence: function() {
        var lifeInsurence = $F("cmbARTAReference");
        if (lifeInsurence == "-1") {
            $('cmbARTA').setValue("2");
            $('cmbARTA').disabled = 'false';
        }
        else {
            $('cmbARTA').disabled = '';
        }
    },
    changeQuotedPrice: function(elementID) {
        var quotedPrice = $F("txtQuotedPrice");
        var consideredPrice = $F("txtConsideredprice");
        if (!util.isDigit(quotedPrice)) {
            alert("Quoted price must me a number Field");
            $('txtQuotedPrice').setValue('0');
            $('txtConsideredprice').setValue('0');
            // $('txtManufacturaryear').focus();
            return false;
        }
        if (consideredPrice != "") {
            if (parseFloat(consideredPrice) > parseFloat(quotedPrice)) {
                alert("Considered price cannot grater then quoted price");
                $('txtConsideredprice').setValue('0');
            }
        }

    },
    changeConsideredProce: function(elementID) {
        var quotedPrice = $F("txtQuotedPrice");
        var consideredPrice = $F("txtConsideredprice");
        if (!util.isDigit(consideredPrice)) {
            alert("Considered price must me a number Field");
            $('txtConsideredprice').setValue('0');
            // $('txtManufacturaryear').focus();
            return false;
        }
        if (quotedPrice == "") {
            alert("Please insert quoted price before give considered price");
            $('txtConsideredprice').setValue('0');
        }
        else {
            if (parseFloat(consideredPrice) > parseFloat(quotedPrice)) {
                alert("Considered Price cannot grater then quoted price");
                $('txtConsideredprice').setValue('0');
            }
        }

    }

};
Event.onReady(Page.init);