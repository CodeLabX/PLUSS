﻿var labelChangeHelper = {

    showFinanceLabel: function() {

        $("lblLoanterms").innerHTML = "FINANCES";
        $("lblloanTerms1").innerHTML = "FINANCES";
        //$("lblLoanCalculation").innerHTML = "FINANCE";


        $("tdinterest").innerHTML = "RENT";
        $("tdinteresRate").innerHTML = "RENT RATE";
        $("tdinteres1").innerHTML = "RENT";
        $("tdinterest2").innerHTML = "RENT";
        $("tdInterestrate1").innerHTML = "RENT RATE";
        $("tdinterest3").innerHTML = "RENT";

        //EMI
        $("tdEM1").innerHTML = "EMR";
        $("tdEM2").innerHTML = "EMR %";
        $("tdEM3").innerHTML = "EMR SHARE";
        $("tdEM4").innerHTML = "EMR";
        $("tdEM5").innerHTML = "EMR %";
        $("tdEM6").innerHTML = "EMR SHARE";


        //LTV

        $("tdlt1").innerHTML = "FTV:";
        $("tdlt2").innerHTML = "FTV:";
        $("tdlt3").innerHTML = "FTV:";

    },
    showLoanLabel: function() {

        $("lblLoanterms").innerHTML = "LOANS";
        $("lblloanTerms1").innerHTML = "LOANS";
        $("lblloanTerms1").innerHTML = "LOAN";

        $("tdinterest").innerHTML = "INTEREST";
        $("tdinteresRate").innerHTML = "INTEREST RATE";
        $("tdinteres1").innerHTML = "INTEREST";
        $("tdinterest2").innerHTML = "INTEREST";
        $("tdInterestrate1").innerHTML = "INTEREST RATE";
        $("tdinterest3").innerHTML = "INTEREST";

        //EMR
        $("tdEM1").innerHTML = "EMI";
        $("tdEM2").innerHTML = "EMI %";
        $("tdEM3").innerHTML = "EMI SHARE";
        $("tdEM4").innerHTML = "EMI";
        $("tdEM5").innerHTML = "EMI %";
        $("tdEM6").innerHTML = "EMI SHARE";

        $("tdlt1").innerHTML = "LTV:";
        $("tdlt2").innerHTML = "LTV:";
        $("tdlt3").innerHTML = "LTV:";

    },
    showFinanceLabelForAnalyst: function() {

        $("lblLoan").innerHTML = "Finance";
        $("lblLoanMaintab").innerHTML = "Finance";
        $("lblLoanterms").innerHTML = "FINANCES";
        $("lblloanTerms1").innerHTML = "FINANCES";
        //$("lblLoanCalculation").innerHTML = "FINANCE";
        $("lblLoanAmount").innerHTML = "FINANCE";
        $("lblAskingLoan").innerHTML = "Asking Finance";
        $("lblAppropriteLoan").innerHTML = "Appropriate Finance";
        $("lblApprovedloan").innerHTML = "Finance";
        $("spnLoanSummary").innerHTML = "FINANCE SUMMARY";
        $("spnLoanSummary").innerHTML = "Total Finance Amount";
        $("lblLoanOfferLater").innerHTML = "FINANCE";
        $("tdLoan").innerHTML = "FINANCE";
        $("tdloanAmount").innerHTML = "FINANCE AMOUNT";
        $("tdTotalAutoloan").innerHTML = "TOTAL AUTO FINANCE";


        $("tdinterest").innerHTML = "RENT";
        $("tdinteresRate").innerHTML = "RENT RATE";
        $("tdinteres1").innerHTML = "RENT";
        $("tdinterest2").innerHTML = "RENT";
        $("tdInterestrate1").innerHTML = "RENT RATE";
        $("tdinterest3").innerHTML = "RENT";
        $("tdinterest4").innerHTML = "RENT";
        $("tdInterest5").innerHTML = "RENT";
        $("tdinterest6").innerHTML = "RENT";
        $("tdinterest7").innerHTML = "RENT";
        $("tdInterest8").innerHTML = "RENT";
        $("tdinterest9").innerHTML = "RENT";
        $("tdInterest10").innerHTML = "RENT";
        $("tdInterest11").innerHTML = "RENT";
        $("tdInterest12").innerHTML = "RENT";
        $("tdinterest13").innerHTML = "RENT";
        $("tdinterest14").innerHTML = "RENT";
        $("tdinterest15").innerHTML = "RENT";
        $("tdinterest16").innerHTML = "RENT";
        $("tdinterest17").innerHTML = "RENT";
        $("tdinterest18").innerHTML = "RENT";
        $("tdinterest19").innerHTML = "RENT";
        $("tdinterest20").innerHTML = "RENT";
        $("tdinterest21").innerHTML = "RENT";
        $("tdinterest22").innerHTML = "RENT";
        $("tdinterest23").innerHTML = "RENT";
        $("tdinterest24").innerHTML = "RENT";
        $("tdinterest25").innerHTML = "RENT";
        $("tdinterest26").innerHTML = "RENT";
        $("tdinterest27").innerHTML = "RENT";
        $("tdinterest28").innerHTML = "RENT";
        $("tdinterest29").innerHTML = "RENT";
        $("tdinterest30").innerHTML = "RENT";
        $("tdinterest31").innerHTML = "RENT";
        $("tdinterest32").innerHTML = "RENT";
        $("tdinterest33").innerHTML = "RENT";
        $("tdinterest34").innerHTML = "RENT";
        $("tdinterest35").innerHTML = "RENT";
        $("tdinterest36").innerHTML = "RENT";
        $("tdinterest37").innerHTML = "RENT";
        $("tdinterest38").innerHTML = "RENT";
        $("tdinterest39").innerHTML = "RENT";
        $("tdinterest40").innerHTML = "RENT";
        $("tdinterest41").innerHTML = "RENT";
        $("tdinterest42").innerHTML = "RENT";
        $("tdinterest43").innerHTML = "RENT";
        $("tdinterest44").innerHTML = "RENT";
        $("tdinterest45").innerHTML = "RENT";
        $("tdinterest46").innerHTML = "RENT";
        $("tdinterest47").innerHTML = "RENT";
        $("tdinterest48").innerHTML = "RENT";
        $("tdinterest49").innerHTML = "RENT";
        $("tdinterest50").innerHTML = "RENT";
        $("tdinterest51").innerHTML = "RENT";
        $("tdinterest52").innerHTML = "RENT";
        $("tdinterest53").innerHTML = "RENT";
        $("spnInterestPersent").innerHTML = "RENT";
        $("tdinterest54").innerHTML = "RENT RATES";

        //EMI
        $("tdEM1").innerHTML = "EMR";
        $("tdEM2").innerHTML = "EMR %";
        $("tdEM3").innerHTML = "EMR SHARE";
        $("tdEM4").innerHTML = "EMR";
        $("tdEM5").innerHTML = "EMR %";
        $("tdEM6").innerHTML = "EMR SHARE";
        $("tdEM7").innerHTML = "EMR";
        $("tdEM8").innerHTML = "EMR";
        $("tdEM9").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM10").innerHTML = "EMR";
        $("tdEM11").innerHTML = "EMR";
        $("tdEM12").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM13").innerHTML = "EMR";
        $("tdEM14").innerHTML = "EMR";
        $("tdEM15").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM16").innerHTML = "EMR";
        $("tdEM17").innerHTML = "EMR";
        $("tdEM18").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM20").innerHTML = "EMR";
        $("tdEM21").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM22").innerHTML = "EMR";
        $("tdEM23").innerHTML = "EMR";
        $("tdEM24").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM25").innerHTML = "EMR";
        $("tdEM26").innerHTML = "EMR";
        $("tdEM27").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM28").innerHTML = "EMR";
        //$("tdEM29").innerHTML = "EMR";
        $("tdEM30").innerHTML = "EMR";
        $("tdEM31").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM32").innerHTML = "EMR";
        $("tdEM33").innerHTML = "EMR";
        $("tdEM34").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM35").innerHTML = "EMR";
        $("tdEM36").innerHTML = "EMR";
        $("tdEM37").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM38").innerHTML = "EMR";
        $("tdEM39").innerHTML = "EMR";
        $("tdEM40").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM41").innerHTML = "EMR";
        $("tdEM42").innerHTML = "EMR";
        $("tdEM43").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM44").innerHTML = "EMR";
        $("tdEM45").innerHTML = "EMR";
        $("tdEM46").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM47").innerHTML = "EMR";
        $("tdEM48").innerHTML = "EMR";
        $("tdEM49").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM50").innerHTML = "EMR";
        $("tdEM51").innerHTML = "EMR";
        $("tdEM52").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM53").innerHTML = "EMR";
        $("tdEM54").innerHTML = "EMR";
        $("tdEM55").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM56").innerHTML = "EMR";
        $("tdEM57").innerHTML = "EMR";
        $("tdEM58").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM59").innerHTML = "EMR";
        $("tdEM60").innerHTML = "EMR";
        $("tdEM61").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM62").innerHTML = "EMR";
        $("tdEM63").innerHTML = "EMR";
        $("tdEM64").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM65").innerHTML = "EMR";
        $("tdEM66").innerHTML = "EMR";
        $("tdEM67").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM68").innerHTML = "EMR";
        $("tdEM69").innerHTML = "EMR";
        $("tdEM70").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM71").innerHTML = "EMR";
        $("tdEM72").innerHTML = "EMR";
        $("tdEM73").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM74").innerHTML = "EMR";
        $("tdEM75").innerHTML = "EMR";
        $("tdEM76").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM77").innerHTML = "EMR";
        $("tdEM78").innerHTML = "EMR";
        $("tdEM79").innerHTML = "TOTAL EMR CLOSING CONSIDERED:";
        $("tdEM80").innerHTML = "EMR";
        $("tdEM81").innerHTML = "EMR";
        $("tdEM82").innerHTML = "Maximum EMR Capability:";
        $("tdEM83").innerHTML = "Existing EMR(s):";
        $("tdEM84").innerHTML = "Current EMR Capability:";
        $("tdEM85").innerHTML = "Maximum EMR:";

        //LTV

        $("tdlt1").innerHTML = "FTV:";
        $("tdlt2").innerHTML = "FTV:";
        $("tdlt3").innerHTML = "FTV:";
        $("tdlt4").innerHTML = "FTV Allowed:";
        $("tdlt5").innerHTML = "FTV Excluding Insurence:";
        $("tdlt6").innerHTML = "FTV Including Insurance:";
        $("tdlt7").innerHTML = "FBR Excluding Insurance:";
        $("tdlt8").innerHTML = "FBR Including Insurance:";
        $("tdlt9").innerHTML = "Extra FTV:";
        $("tdlt10").innerHTML = "FTV INC. BANCA";
        $("tdlt11").innerHTML = "MAX FTV SPREAD";
        //$("tdlt12").innerHTML = "FTV";

        //DBR

        $("tdDbr1").innerHTML = "FBR Level:";
        $("tdDbr2").innerHTML = "Appropriate FBR %:";
        $("tdDbr3").innerHTML = "Considered FBR %:";
        $("tdDbr4").innerHTML = "*Level-3 FBR Required";
        $("tdDbr5").innerHTML = "Extra FBR:";
        $("tdDbr6").innerHTML = "FBR";
        $("tdDbr7").innerHTML = "FBR";





    },
    showLoanLabelForAnalyst: function() {
        //        var bodyText = $("maindivContent").innerHTML;
        //        var pattern = /EMR/g;
        //        var replaceText = "EMI";
        //        var replacementtext = bodyText.replace(pattern, replaceText);

        //        pattern = /RENT/g;
        //        replaceText = "INTEREST";
        //        replacementtext = replacementtext.replace(pattern, replaceText);

        //        pattern = /FINANCES/g;
        //        replaceText = "LOAN";
        //        replacementtext = replacementtext.replace(pattern, replaceText);

        //        pattern = /FTV/g;
        //        replaceText = "LTV";
        //        replacementtext = replacementtext.replace(pattern, replaceText);

        //        pattern = /FBR/g;
        //        replaceText = "DBR";
        //        replacementtext = replacementtext.replace(pattern, replaceText);

        //        $('maindivContent').innerHTML = replacementtext;

        $("lblLoan").innerHTML = "Loan";
        $("lblLoanMaintab").innerHTML = "Loan";
        $("lblLoanterms").innerHTML = "LOANS";
        $("lblloanTerms1").innerHTML = "LOANS";
        $("lblloanTerms1").innerHTML = "LOAN";
        $("lblLoanAmount").innerHTML = "LOAN";
        $("lblAskingLoan").innerHTML = "Asking Loan";
        $("lblAppropriteLoan").innerHTML = "Appropriate Loan";
        $("lblApprovedloan").innerHTML = "Loan";
        $("spnLoanSummary").innerHTML = "LOAN SUMMARY";
        $("spnLoanSummary").innerHTML = "Total Loan Amount";
        $("lblLoanOfferLater").innerHTML = "LOAN";
        $("tdLoan").innerHTML = "LOAN";
        $("tdloanAmount").innerHTML = "LOAN AMOUNT";
        $("tdTotalAutoloan").innerHTML = "TOTAL AUTO LOAN";



        $("tdinterest").innerHTML = "INTEREST";
        $("tdinteresRate").innerHTML = "INTEREST RATE";
        $("tdinteres1").innerHTML = "INTEREST";
        $("tdinterest2").innerHTML = "INTEREST";
        $("tdInterestrate1").innerHTML = "INTEREST RATE";
        $("tdinterest3").innerHTML = "INTEREST";
        $("tdinterest4").innerHTML = "INTEREST";
        $("tdInterest5").innerHTML = "INTEREST";
        $("tdinterest6").innerHTML = "INTEREST";
        $("tdinterest7").innerHTML = "INTEREST";
        $("tdInterest8").innerHTML = "INTEREST";
        $("tdinterest9").innerHTML = "INTEREST";
        $("tdInterest10").innerHTML = "INTEREST";
        $("tdInterest11").innerHTML = "INTEREST";
        $("tdInterest12").innerHTML = "INTEREST";
        $("tdinterest13").innerHTML = "INTEREST";
        $("tdinterest14").innerHTML = "INTEREST";
        $("tdinterest15").innerHTML = "INTEREST";
        $("tdinterest16").innerHTML = "INTEREST";
        $("tdinterest17").innerHTML = "INTEREST";
        $("tdinterest18").innerHTML = "INTEREST";
        $("tdinterest19").innerHTML = "INTEREST";
        $("tdinterest20").innerHTML = "INTEREST";
        $("tdinterest21").innerHTML = "INTEREST";
        $("tdinterest22").innerHTML = "INTEREST";
        $("tdinterest23").innerHTML = "INTEREST";
        $("tdinterest24").innerHTML = "INTEREST";
        $("tdinterest25").innerHTML = "INTEREST";
        $("tdinterest26").innerHTML = "INTEREST";
        $("tdinterest27").innerHTML = "INTEREST";
        $("tdinterest28").innerHTML = "INTEREST";
        $("tdinterest29").innerHTML = "INTEREST";
        $("tdinterest30").innerHTML = "INTEREST";
        $("tdinterest31").innerHTML = "INTEREST";
        $("tdinterest32").innerHTML = "INTEREST";
        $("tdinterest33").innerHTML = "INTEREST";
        $("tdinterest34").innerHTML = "INTEREST";
        $("tdinterest35").innerHTML = "INTEREST";
        $("tdinterest36").innerHTML = "INTEREST";
        $("tdinterest37").innerHTML = "INTEREST";
        $("tdinterest38").innerHTML = "INTEREST";
        $("tdinterest39").innerHTML = "INTEREST";
        $("tdinterest40").innerHTML = "INTEREST";
        $("tdinterest41").innerHTML = "INTEREST";
        $("tdinterest42").innerHTML = "INTEREST";
        $("tdinterest43").innerHTML = "INTEREST";
        $("tdinterest44").innerHTML = "INTEREST";
        $("tdinterest45").innerHTML = "INTEREST";
        $("tdinterest46").innerHTML = "INTEREST";
        $("tdinterest47").innerHTML = "INTEREST";
        $("tdinterest48").innerHTML = "INTEREST";
        $("tdinterest49").innerHTML = "INTEREST";
        $("tdinterest50").innerHTML = "INTEREST";
        $("tdinterest51").innerHTML = "INTEREST";
        $("tdinterest52").innerHTML = "INTEREST";
        $("tdinterest53").innerHTML = "INTEREST";
        $("spnInterestPersent").innerHTML = "INTEREST";
        $("tdinterest54").innerHTML = "INTEREST RATES";

        //EMR
        $("tdEM1").innerHTML = "EMI";
        $("tdEM2").innerHTML = "EMI %";
        $("tdEM3").innerHTML = "EMI SHARE";
        $("tdEM4").innerHTML = "EMI";
        $("tdEM5").innerHTML = "EMI %";
        $("tdEM6").innerHTML = "EMI SHARE";
        $("tdEM7").innerHTML = "EMI";
        $("tdEM8").innerHTML = "EMI";
        $("tdEM9").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM10").innerHTML = "EMI";
        $("tdEM11").innerHTML = "EMI";
        $("tdEM12").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM13").innerHTML = "EMI";
        $("tdEM14").innerHTML = "EMI";
        $("tdEM15").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM16").innerHTML = "EMI";
        $("tdEM17").innerHTML = "EMI";
        $("tdEM18").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM19").innerHTML = "EMI";
        $("tdEM20").innerHTML = "EMI";
        $("tdEM21").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM22").innerHTML = "EMI";
        $("tdEM23").innerHTML = "EMI";
        $("tdEM24").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM25").innerHTML = "EMI";
        $("tdEM26").innerHTML = "EMI";
        $("tdEM27").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM28").innerHTML = "EMI";
        //$("tdEM29").innerHTML = "EMI";
        $("tdEM30").innerHTML = "EMI";
        $("tdEM31").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM32").innerHTML = "EMI";
        $("tdEM33").innerHTML = "EMI";
        $("tdEM34").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM35").innerHTML = "EMI";
        $("tdEM36").innerHTML = "EMI";
        $("tdEM37").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM38").innerHTML = "EMI";
        $("tdEM39").innerHTML = "EMI";
        $("tdEM40").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM41").innerHTML = "EMI";
        $("tdEM42").innerHTML = "EMI";
        $("tdEM43").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM44").innerHTML = "EMI";
        $("tdEM45").innerHTML = "EMI";
        $("tdEM46").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM47").innerHTML = "EMI";
        $("tdEM48").innerHTML = "EMI";
        $("tdEM49").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM50").innerHTML = "EMI";
        $("tdEM51").innerHTML = "EMI";
        $("tdEM52").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM53").innerHTML = "EMI";
        $("tdEM54").innerHTML = "EMI";
        $("tdEM55").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM56").innerHTML = "EMI";
        $("tdEM57").innerHTML = "EMI";
        $("tdEM58").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM59").innerHTML = "EMI";
        $("tdEM60").innerHTML = "EMI";
        $("tdEM61").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM62").innerHTML = "EMI";
        $("tdEM63").innerHTML = "EMI";
        $("tdEM64").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM65").innerHTML = "EMI";
        $("tdEM66").innerHTML = "EMI";
        $("tdEM67").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM68").innerHTML = "EMI";
        $("tdEM69").innerHTML = "EMI";
        $("tdEM70").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM71").innerHTML = "EMI";
        $("tdEM72").innerHTML = "EMI";
        $("tdEM73").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM74").innerHTML = "EMI";
        $("tdEM75").innerHTML = "EMI";
        $("tdEM76").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM77").innerHTML = "EMI";
        $("tdEM78").innerHTML = "EMI";
        $("tdEM79").innerHTML = "TOTAL EMI CLOSING CONSIDERED:";
        $("tdEM80").innerHTML = "EMI";
        $("tdEM81").innerHTML = "EMI";
        $("tdEM82").innerHTML = "Maximum EMI Capability:";
        $("tdEM83").innerHTML = "Existing EMI(s):";
        $("tdEM84").innerHTML = "Current EMI Capability:";
        $("tdEM85").innerHTML = "Maximum EMI:";


        $("tdlt1").innerHTML = "LTV:";
        $("tdlt2").innerHTML = "LTV:";
        $("tdlt3").innerHTML = "LTV:";
        $("tdlt4").innerHTML = "LTV Allowed:";
        $("tdlt5").innerHTML = "LTV Excluding insurence:";
        $("tdlt6").innerHTML = "LTV Including Insurance:";
        $("tdlt7").innerHTML = "FBR Excluding Insurance:";
        $("tdlt8").innerHTML = "FBR Including Insurance:";
        $("tdlt9").innerHTML = "Extra LTV:";
        $("tdlt10").innerHTML = "LTV INC. BANCA";
        $("tdlt11").innerHTML = "MAX LTV SPREAD";
        //$("tdlt12").innerHTML = "LTV";



        $("tdDbr1").innerHTML = "DBR Level:";
        $("tdDbr2").innerHTML = "Appropriate DBR %:";
        $("tdDbr3").innerHTML = "Considered DBR %:";
        $("tdDbr4").innerHTML = "*Level-3 DBR Required";
        $("tdDbr5").innerHTML = "Extra DBR:";
        $("tdDbr6").innerHTML = "DBR";
        $("tdDbr7").innerHTML = "DBR";
    },
    Auto_An_AlertVerificationReport: function(objAuto_An_AlertVerificationReport) {
        if (objAuto_An_AlertVerificationReport != null) {
            $('txtMaxAgeAllowed').setValue(objAuto_An_AlertVerificationReport.MaxAgeAllowed);
            $('txtMaxAgeFound').setValue(objAuto_An_AlertVerificationReport.MaxAgeFound);
            $('txtMaxAgeFlag').setValue(objAuto_An_AlertVerificationReport.MaxAgeFlag);
            $('txtMinAgeAllowed').setValue(objAuto_An_AlertVerificationReport.MinAgeAllowed);
            $('txtMinAgeFound').setValue(objAuto_An_AlertVerificationReport.MinAgeFound);
            $('txtMinAgeFlag').setValue(objAuto_An_AlertVerificationReport.MinAgeFlag);
            $('txtMinincomeAllowed').setValue(objAuto_An_AlertVerificationReport.MinIncomeAllowed);
            $('txtMinIncomeFound').setValue(objAuto_An_AlertVerificationReport.MinIncomeFound);
            $('txtMinIncomeFlag').setValue(objAuto_An_AlertVerificationReport.MinIncomeFlag);
            //$('cmbLocationForLcFlag').setValue(objAuto_An_AlertVerificationReport.Location);
            $('cmbLocationForLcFlag').setValue(objAuto_An_AlertVerificationReport.LocationCosidered);
            //$('cmbNationalityForLcFlag').setValue(objAuto_An_AlertVerificationReport.Nationality);
            $('cmbNationalityForLcFlag').setValue(objAuto_An_AlertVerificationReport.NationalityConsidered);
            $('txtCarAgeAtTenorEndAllowed').setValue(objAuto_An_AlertVerificationReport.CarAgeAtTenorEndAllowed);
            $('txtCarAgeAtTenorEndFound').setValue(objAuto_An_AlertVerificationReport.CarAgeAtTenorEndFound);
            $('txtCarAgeAtTenorEndFlag').setValue(objAuto_An_AlertVerificationReport.CarAgeAtTenorEndFlag);
            $('txtSeatingCapacityAllowed').setValue(objAuto_An_AlertVerificationReport.SeatingCapacityAllowed);
            $('txtSeatingCapacityFound').setValue(objAuto_An_AlertVerificationReport.SeatingCapacityFound);
            $('txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab').setValue(objAuto_An_AlertVerificationReport.SeatingCapacityFlag);
            $('txtCarVerificationForFinTab').setValue(objAuto_An_AlertVerificationReport.CarVerification);
            $('txtCarVerificationFlagForFinTab').setValue(objAuto_An_AlertVerificationReport.CarVerificationFlag);
            $('txthypothecationForFinalizeTab').setValue(objAuto_An_AlertVerificationReport.HypothecationCheck);
            $('txthypothecationFlagForFinalizeTab').setValue(objAuto_An_AlertVerificationReport.HypothecationCheckFlag);
            //$('cmbVendoreStatusFlagForFnlTab').setValue(objAuto_An_AlertVerificationReport.VendorStatus);
            $('cmbVendoreStatusFlagForFnlTab').setValue(objAuto_An_AlertVerificationReport.VendorFlag);
            $('txtLoanAmountAllowed').setValue(objAuto_An_AlertVerificationReport.LoanAmountAllowed);
            $('txtLoanAmountFound').setValue(objAuto_An_AlertVerificationReport.LoanAmountFound);
            $('txtLoanAmountFlag').setValue(objAuto_An_AlertVerificationReport.LoanAmountFlag);
            $('txtBBMaxlimitAllowed').setValue(objAuto_An_AlertVerificationReport.BBMaxLimitAllowed);
            $('txtBBMaxlimitFound').setValue(objAuto_An_AlertVerificationReport.BBMaxLimitFound);
            $('txtBBMaxlimitFlag').setValue(objAuto_An_AlertVerificationReport.BBMaxLimitFlag);
            $('txtInterestRatesAllowed').setValue(objAuto_An_AlertVerificationReport.InterestRateAllowed);
            $('txtInterestRatesFound').setValue(objAuto_An_AlertVerificationReport.InterestRateFound);
            $('txtInterestRatesFlag').setValue(objAuto_An_AlertVerificationReport.InterestRateFlag);
            $('txtTenorAllowed').setValue(objAuto_An_AlertVerificationReport.TenorAllowed);
            $('txtTenorFound').setValue(objAuto_An_AlertVerificationReport.TenorFound);
            $('txtTenorFlag').setValue(objAuto_An_AlertVerificationReport.TenorFlag);
            $('txtArtaEnrollmentAllowed').setValue(objAuto_An_AlertVerificationReport.ARTAEnrollmentAllowed);
            $('txtArtaEnrollmentFound').setValue(objAuto_An_AlertVerificationReport.ARTAEnrollmentFound);
            $('txtArtaEnrollmentFlag').setValue(objAuto_An_AlertVerificationReport.ARTAEnrollmentFlag);

            $('txtLtvIncludingBankaAllowed').setValue(objAuto_An_AlertVerificationReport.LTVIncBancaAllowed);
            $('txtLtvIncludingBankaFound').setValue(objAuto_An_AlertVerificationReport.LTVIncBancaFound);
            $('txtLtvIncludingBankaFlag').setValue(objAuto_An_AlertVerificationReport.LTVIncBancaFlag);
            $('txtMaxLtvSpreadAllowed').setValue(objAuto_An_AlertVerificationReport.MaxLTVSpreadAllowed);
            $('txtMaxLtvSpreadFound').setValue(objAuto_An_AlertVerificationReport.MaxLTVSpreadFound);
            $('txtMaxLtvSpreadFlag').setValue(objAuto_An_AlertVerificationReport.MaxLTVSpreadFlag);
            $('txtDbrForFtAllowed').setValue(objAuto_An_AlertVerificationReport.DBRAllowed);
            $('txtDbrForFtFound').setValue(objAuto_An_AlertVerificationReport.DBRFound);
            $('txtDbrSpreadForFtAllowed').setValue(objAuto_An_AlertVerificationReport.DBRFlag);
            $('txtTotalAutoLoanForFtAllowed').setValue(objAuto_An_AlertVerificationReport.TotalAutoLoanAllowed);
            $('txtTotalAutoLoanForFtFound').setValue(objAuto_An_AlertVerificationReport.TotalAutoLoanFound);
            $('txtTotalAutoLoanForFtFlag').setValue(objAuto_An_AlertVerificationReport.TotalAutoLoanFlag);
            $('txtApproversDlaAllowed').setValue(objAuto_An_AlertVerificationReport.ApproversDLAAllowed);
            $('txtApproversDlaFound').setValue(objAuto_An_AlertVerificationReport.ApproversDLAFound);
            $('txtApproversDlaFlag').setValue(objAuto_An_AlertVerificationReport.ApproversDLAFlag);
            $('txtCibObtainedForFt').setValue(objAuto_An_AlertVerificationReport.CIBOptained);

            $('txtCibObtainedForFtFlag').setValue(objAuto_An_AlertVerificationReport.CIBOptainedFlag);
            $('txtJointMaxAgeAllowed').setValue(objAuto_An_AlertVerificationReport.JtMaxAgeAllowed);
            $('txtJointMaxAgeFound').setValue(objAuto_An_AlertVerificationReport.JtMaxAgeFound);
            $('txtJointMaxAgeFlag').setValue(objAuto_An_AlertVerificationReport.JtMaxAgeFlag);
            $('txtJointMinAgeAllowed').setValue(objAuto_An_AlertVerificationReport.JtMinAgeAllowed);
            $('txtJointMinAgeFound').setValue(objAuto_An_AlertVerificationReport.JtMinAgeFound);
            $('txtJointminAgeFlag').setValue(objAuto_An_AlertVerificationReport.JtMinAgeFlag);
            $('txtJointMinIncomeAllowed').setValue(objAuto_An_AlertVerificationReport.JtMinIncomeAllowed);

            $('txtJointMinIncomeFound').setValue(objAuto_An_AlertVerificationReport.JtMinIncomeFound);
            $('txtJointMinIncomeFlag').setValue(objAuto_An_AlertVerificationReport.JtMinIncomeFlag);
        }
    }

};