﻿var vehicleType = { 'Sedan': 'S1', 'Station wagon': 'S2', 'SUV': 'S3', 'Pick-Ups': 'S4', 'Microbus': 'S5', 'MPV': 'S6' };
var Page = {
loanApplication: null,
insurenceAndBanka: [],
currentAge: 0,
serverDate: "",
modelArray: [],
vehicleDetailsArray:null,

vehicleId:0,

    init: function() {
        util.prepareDom();
        Event.observe('cmbJointApplication', 'change', LoanApplicationHealper.hideShowDivBasedOn_jointApplicationDiv);
        Event.observe('cmbVendor', 'change', LoanApplicationManager.showHideNewVendor);
        Event.observe('btnLoanSearch', 'click', LoanApplicationManager.GetInfoByLLID);
        Event.observe('lnkPrint', 'click', LoanApplicationManager.printPreview);
        Event.observe('lnkSave', 'click', LoanApplicationManager.SaveLoanButtonClick);
        Event.observe('lnkForward', 'click', LoanApplicationManager.ForwardLoanButtonClick);
        LoanApplicationManager.getServerDateTime();
        LoanApplicationManager.getBank();
        LoanApplicationManager.getVendor();
        LoanApplicationManager.getManufacturer();
        LoanApplicationManager.getProfession();
        LoanApplicationManager.getNameOfCompany();
        LoanApplicationManager.getVehicleStatus();
        LoanApplicationManager.getGeneralInsurance();
        LoanApplicationManager.GetAllSource();
        LoanApplicationManager.getFacility();
        Event.observe('txtPApplicantEmail', 'change', LoanApplicationHealper.changePrimaryEmailValidation);
        Event.observe('txtJApplicantEmail', 'change', LoanApplicationHealper.changeJointEmailValidation);
        Event.observe('txtPApplicantMonthInCurrentProfession', 'change', LoanApplicationHealper.checkIntForCurrentPrProfession);
        Event.observe('txtJApplicantMonthInCurrentProfession', 'change', LoanApplicationHealper.checkIntForCurrentJtProfession);

        Event.observe('cmbGeneralInsurance', 'change', LoanApplicationHealper.changeGeneralInsurence);
        Event.observe('cmbLifeInsurance', 'change', LoanApplicationHealper.changeLifeInsurence);
        
       // Event.observe('txtPApplicantDateOfBirth', 'change', LoanApplicationHealper.changeDateofBirth);
        
        
    }
};


var LoanApplicationManager = {

    printPreview: function() {
        var searchKey = $F('txtLLID');
        window.open('LoanApplicationPrint.aspx?llid=' + searchKey);
    },
    getServerDateTime: function() {
        var res = PlussAuto.LoanApplication.GetServerDateTime();
        Page.serverDate = res.value;

    },

    getBank: function() {

        var res = PlussAuto.LoanApplication.GetBank(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        LoanApplicationHealper.populateBankCombo(res);
    },
    getFacility: function() {

        var res = PlussAuto.LoanApplication.getFacility();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        LoanApplicationHealper.populateFacilityCombo(res);
    },

    getVehicleStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.LoanApplication.getVehicleStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        } document.getElementById('cmdVehicleStatus').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vehicle Status';
        ExtraOpt.value = '-1';
        document.getElementById('cmdVehicleStatus').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmdVehicleStatus').options.add(opt);
        }


        document.getElementById('cmbDealerType').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select a Dealer Type';
        ExtraOpt.value = '-1';
        document.getElementById('cmbDealerType').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmbDealerType').options.add(opt);
        }

    },

    GetAllSource: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.LoanApplication.GetAllSource();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbSource').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Source';
        ExtraOpt.value = '-1';
        document.getElementById('cmbSource').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].SourceName;
            opt.value = res.value[i].SourceId;
            document.getElementById('cmbSource').options.add(opt);
        }
    },


    getGeneralInsurance: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.LoanApplication.getGeneralInsurance();

        if (res.error) {
            alert(res.error.Message);
            return;
        }

        document.getElementById('cmbGeneralInsurance').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'No Insurance';
        ExtraOpt.value = '-1';
        document.getElementById('cmbGeneralInsurance').options.add(ExtraOpt);

        document.getElementById('cmbLifeInsurance').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'No Insurance';
        ExtraOpt.value = '-1';
        document.getElementById('cmbLifeInsurance').options.add(ExtraOpt);
        Page.insurenceAndBanka.push(res.value);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            var opt1 = document.createElement("option");
            var insType = res.value[i].InsType;

            if (insType == '1' || insType == '3') {
                opt.text = res.value[i].CompanyName;
                opt.value = res.value[i].InsCompanyId;

                document.getElementById('cmbGeneralInsurance').options.add(opt);
            }
            if (insType == '2' || insType == '3') {
                opt1.text = res.value[i].CompanyName;
                opt1.value = res.value[i].InsCompanyId;

                document.getElementById('cmbLifeInsurance').options.add(opt1);
            }
        }
    },






    getManufacturingYear: function() {
        //var comapanyName = $F('txtSearch');
        var manufacturerID = $F('cmbManufacturer');
        var model = $F('cmbModel');
        var enigneCC = $F('cmbEngineCC');
        var TrimLevel = $F('cmbTrimLevel');

        var res = PlussAuto.LoanApplication.getManufacturingYear(manufacturerID, model, enigneCC, TrimLevel);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbManufacturingYear').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Manufacturing Year';
        ExtraOpt.value = '-1';
        document.getElementById('cmbManufacturingYear').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ManufacturingYear;
            opt.value = res.value[i].ManufacturingYear;
            document.getElementById('cmbManufacturingYear').options.add(opt);
        }
    },



    getProfession: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.LoanApplication.GetProfession();

        if (res.error) {
            alert(res.error.Message);
            return;
        }

        document.getElementById('cmbPApplicantPrimaryProfession').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Primary Profession';
        ExtraOpt.value = '-1';
        document.getElementById('cmbPApplicantPrimaryProfession').options.add(ExtraOpt);

        document.getElementById('cmbJApplicantPrimaryProfession').options.length = 0;
        var ExtraOpt1 = document.createElement("option");
        ExtraOpt1.text = 'Select Primary Profession';
        ExtraOpt1.value = '-1';
        document.getElementById('cmbJApplicantPrimaryProfession').options.add(ExtraOpt1);


        document.getElementById('cmbReferenceOccupation1').options.length = 0;
        var ExtraOpt2 = document.createElement("option");
        ExtraOpt2.text = 'Select Occupation';
        ExtraOpt2.value = '-1';
        document.getElementById('cmbReferenceOccupation1').options.add(ExtraOpt2);

        document.getElementById('cmbReferenceOccupation2').options.length = 0;
        var ExtraOpt3 = document.createElement("option");
        ExtraOpt3.text = 'Select Occupation';
        ExtraOpt3.value = '-1';
        document.getElementById('cmbReferenceOccupation2').options.add(ExtraOpt3);


        document.getElementById('cmbPApplicantOtherProfession').options.length = 0;
        var ExtraOpt4 = document.createElement("option");
        ExtraOpt4.text = 'Select Other Profession';
        ExtraOpt4.value = '-1';
        document.getElementById('cmbPApplicantOtherProfession').options.add(ExtraOpt4);

        document.getElementById('cmbJApplicantOtherProfession').options.length = 0;
        var ExtraOpt5 = document.createElement("option");
        ExtraOpt5.text = 'Select Other Profession';
        ExtraOpt5.value = '-1';
        document.getElementById('cmbJApplicantOtherProfession').options.add(ExtraOpt5);


        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ProfName;
            opt.value = res.value[i].ProfID;
            document.getElementById('cmbPApplicantPrimaryProfession').options.add(opt);

            var opt1 = document.createElement("option");
            opt1.text = res.value[i].ProfName;
            opt1.value = res.value[i].ProfID;
            document.getElementById('cmbJApplicantPrimaryProfession').options.add(opt1);


            var opt2 = document.createElement("option");
            opt2.text = res.value[i].ProfName;
            opt2.value = res.value[i].ProfID;
            document.getElementById('cmbPApplicantOtherProfession').options.add(opt2);

            var opt3 = document.createElement("option");
            opt3.text = res.value[i].ProfName;
            opt3.value = res.value[i].ProfID;
            document.getElementById('cmbJApplicantOtherProfession').options.add(opt3);

            var opt4 = document.createElement("option");
            opt4.text = res.value[i].ProfName;
            opt4.value = res.value[i].ProfID;
            document.getElementById('cmbReferenceOccupation1').options.add(opt4);

            var opt5 = document.createElement("option");
            opt5.text = res.value[i].ProfName;
            opt5.value = res.value[i].ProfID;
            document.getElementById('cmbReferenceOccupation2').options.add(opt5);

        }
    },


    getNameOfCompany: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.LoanApplication.getNameOfCompany();

        if (res.error) {
            alert(res.error.Message);
            return;
        }

        document.getElementById('cmbPApplicantNameOfCompany').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Company';
        ExtraOpt.value = '-1';
        document.getElementById('cmbPApplicantNameOfCompany').options.add(ExtraOpt);

        document.getElementById('cmbJApplicantNameOfCompany').options.length = 0;
        var ExtraOpt1 = document.createElement("option");
        ExtraOpt1.text = 'Select Company';
        ExtraOpt1.value = '-1';
        document.getElementById('cmbJApplicantNameOfCompany').options.add(ExtraOpt1);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].CompanyName;
            opt.value = res.value[i].AutoCompanyId;
            document.getElementById('cmbPApplicantNameOfCompany').options.add(opt);

            var opt1 = document.createElement("option");
            opt1.text = res.value[i].CompanyName;
            opt1.value = res.value[i].AutoCompanyId;
            document.getElementById('cmbJApplicantNameOfCompany').options.add(opt1);
        }

    },


    getVendor: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.LoanApplication.GetVendor();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        //        Page.vendorArray = res.value;

        document.getElementById('cmbVendor').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vendor';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVendor').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].VendorName;
            opt.value = res.value[i].AutoVendorId;
            document.getElementById('cmbVendor').options.add(opt);
        }

    },

    getManufacturer: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.LoanApplication.GetAutoManufacturer();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        //        Page.manufacturerArray = res.value;
        //
        document.getElementById('cmbManufacturer').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Manufacturer';
        ExtraOpt.value = '-1';
        document.getElementById('cmbManufacturer').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ManufacturerName;
            opt.value = res.value[i].ManufacturerId;
            document.getElementById('cmbManufacturer').options.add(opt);
        }

    },

    changeManufacture: function() {
        var manufacturerID = $F('cmbManufacturer');
        var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;
        if (manufacturName != "Other") {
            $('cmbModel').style.display = 'block';
            $('txtVehicleType').style.display = 'block';
            $('txtModelName').style.display = 'none';
            $('cmbVehicleType').style.display = 'none';
            $('txtTrimLevel').disabled = 'false';
            $('txtEngineCC').disabled = 'false';
            $('txtVehicleType').disabled = 'false';

            //var res = PlussAuto.LoanApplication.GetAutoModelByManufacturerID(manufacturerID);
            var res = PlussAuto.LoanApplication.getModelbyManufacturerID(manufacturerID);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            Page.modelArray = res.value;

            document.getElementById('cmbModel').options.length = 0;
            var ExtraOpt = document.createElement("option");
            ExtraOpt.text = 'Select Model';
            ExtraOpt.value = '-1';
            document.getElementById('cmbModel').options.add(ExtraOpt);

            //        for (var i = 0; i < res.value.length; i++) {
            //            var opt = document.createElement("option");
            //            var ch = res.value[i].Model;
            ////            if (res.value[i].TrimLevel != "") {
            ////                ch += "-" + res.value[i].TrimLevel;
            ////            }

            //            opt.text = ch;
            //            //            opt.value = res.value[i].VehicleId;
            //            opt.value = ch;
            //            document.getElementById('cmbModel').options.add(opt);
            //        }
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                var ch = res.value[i].Model;
                if (res.value[i].TrimLevel != "") {
                    ch += "-" + res.value[i].TrimLevel;

                }
                opt.text = ch;
                opt.value = res.value[i].VehicleId;
                document.getElementById('cmbModel').options.add(opt);
            }

            if (Page.vehicleId != 0) {
                $('cmbModel').setValue(Page.vehicleId);

                LoanApplicationManager.changeModel();
            }
        }
        else {
            $('cmbModel').style.display = 'none';
            $('txtVehicleType').style.display = 'none';
            $('txtModelName').style.display = 'block';
            $('cmbVehicleType').style.display = 'block';
            $('txtTrimLevel').disabled = '';
            $('txtEngineCC').disabled = '';
            $('txtVehicleType').disabled = '';

            if (Page.vehicleDetailsArray != null && Page.vehicleDetailsArray.AutoLoanMasterId != 0) {
                $('txtModelName').setValue(Page.vehicleDetailsArray.Model);
                $('txtTrimLevel').setValue(Page.vehicleDetailsArray.TrimLevel);
                $('txtEngineCC').setValue(Page.vehicleDetailsArray.EngineCC);
                //
                if (Page.vehicleDetailsArray.VehicleType != "") {
                    $('cmbVehicleType').setValue(vehicleType[Page.vehicleDetailsArray.VehicleType]);
                }
            }

        }


    },

    changeModel: function() {
        //Arif Code
        //var manufacturerID = $F('cmbManufacturer');
        //var model = $F('cmbModel');


        //        var res = PlussAuto.LoanApplication.GetAutoTrimLevelByModel(manufacturerID, model);

        //        if (res.error) {
        //            alert(res.error.Message);
        //            return;
        //        }

        //        document.getElementById('cmbTrimLevel').options.length = 0;
        //        var ExtraOpt = document.createElement("option");
        //        ExtraOpt.text = 'Select Trim Level';
        //        ExtraOpt.value = '';
        //        document.getElementById('cmbTrimLevel').options.add(ExtraOpt);

        //        for (var i = 0; i < res.value.length; i++) {
        //            var opt = document.createElement("option");
        //            var ch = res.value[i].TrimLevel;

        //            opt.text = ch;
        //            opt.value = ch;
        //            document.getElementById('cmbTrimLevel').options.add(opt);
        //        }
        //        LoanApplicationManager.changeTrimLevel();

        //New Code Change by Rimpo
        var vehicleId = $F('cmbModel');
        var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
        if (vehicleName != "Other") {
            var model = Page.modelArray.findByProp('VehicleId', vehicleId);
            if (model) {
                $('txtVehicleType').style.display = 'block';
                $('cmbVehicleType').style.display = 'none';
                $('txtTrimLevel').disabled = 'false';
                $('txtEngineCC').disabled = 'false';
                $('txtVehicleType').disabled = 'false';
                $('txtTrimLevel').setValue(model.TrimLevel);
                $('txtEngineCC').setValue(model.CC);
                $('hdnModel').setValue(model.Model);
                $('txtVehicleType').setValue(model.Type);
                $('txtManufacturingyear').setValue(model.ManufacturingYear);
                var vehicleStatus = $F("cmdVehicleStatus");
                if (util.isDigit(model.ManufacturingYear)) {
                    LoanApplicationManager.getPrice(vehicleId, model.ManufacturingYear, vehicleStatus);
                }
            }
        }
        else {
            $('txtVehicleType').style.display = 'none';
            $('cmbVehicleType').style.display = 'block';
            $('txtTrimLevel').disabled = '';
            $('txtEngineCC').disabled = '';
            $('txtVehicleType').disabled = '';
            if (Page.vehicleDetailsArray != null && Page.vehicleDetailsArray.AutoLoanMasterId != 0) {
                //$('txtModelName').setValue(Page.vehicleDetailsArray.Model);
                $('txtTrimLevel').setValue(Page.vehicleDetailsArray.TrimLevel);
                $('txtEngineCC').setValue(Page.vehicleDetailsArray.EngineCC);
                //
                if (Page.vehicleDetailsArray.VehicleType != "") {
                    $('cmbVehicleType').setValue(vehicleType[Page.vehicleDetailsArray.VehicleType]);
                }
            }
        }

    },


    showHideNewVendor: function() {

        var vendorID = $F('cmbVendor');
        if (vendorID == '-1') {
            $('txtVendorName').disabled = '';
            $('cmbDealerType').setValue('1');
        }
        else {
            $('txtVendorName').disabled = 'false';
            $('cmbDealerType').setValue('-1');
        }
    },

    changeTrimLevel: function() {

        var manufacturerID = $F('cmbManufacturer');
        var model = $F('cmbModel');
        var trimLevel = $F('cmbTrimLevel');


        var res = PlussAuto.LoanApplication.GetAutoEngineCCByModel_TrimLevel(manufacturerID, model, trimLevel);

        if (res.error) {
            alert(res.error.Message);
            return;
        }

        document.getElementById('cmbEngineCC').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Engine CC';
        ExtraOpt.value = '-1';
        document.getElementById('cmbEngineCC').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            var ch = res.value[i].CC;

            opt.text = ch;
            opt.value = ch;
            document.getElementById('cmbEngineCC').options.add(opt);
        }
    },

    changeEngineCC: function(res) {

        var manufacturerID = $F('cmbManufacturer');
        var model = $F('cmbModel');
        var enigneCC = $F('cmbEngineCC');
        var TrimLevel = $F('cmbTrimLevel');


        var res = PlussAuto.LoanApplication.GetAutoVehicleType(manufacturerID, model, enigneCC, TrimLevel);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value.length == 0) {
        }
        else {
            var type = res.value[0].Type;
            $('txtVehicleType').setValue(type);
        }
        LoanApplicationManager.getManufacturingYear();

    },


    changeManufacturingYear: function(res) {
        //Arif Code Comment by Rimpo
        //        var manufacturerID = $F('cmbManufacturer');
        //        var model = $F('cmbModel');
        //        var enigneCC = $F('cmbEngineCC');
        //        var TrimLevel = $F('cmbTrimLevel');
        //        var manufacturingYear = $F('cmbManufacturingYear');


        //        var res = PlussAuto.LoanApplication.GetPrice(manufacturerID, model, enigneCC, TrimLevel, manufacturingYear);

        //        if (res.error) {
        //            alert(res.error.Message);
        //            return;
        //        }
        //        if (res.value.length == 0) {
        //        }
        //        else {
        //            var minimumPrice = res.value[0].MinimumPrice;
        //            $('txtVerifiedPrice').setValue(minimumPrice);
        //        }
        //Code by Rimpo
        var manufacturingYear = $F('txtManufacturingyear');
        if (!util.isDigit(manufacturingYear)) {
            alert("Required Number filed.");
            $('txtManufacturingyear').setValue("0");
            //$('txtManufacturingyear').focus();
            return;
        }
        var vehicleId = $F('cmbModel');
        var vehicleStatus = $F("cmdVehicleStatus");
        LoanApplicationManager.getPrice(vehicleId, manufacturingYear, vehicleStatus);

    },
    changeVehicleStatus: function() {
        var manufacturingYear = $F('txtManufacturingyear');
        if (util.isDigit(manufacturingYear)) {
            var vehicleId = $F('cmbModel');
            var vehicleStatus = $F("cmdVehicleStatus");
            LoanApplicationManager.getPrice(vehicleId, manufacturingYear, vehicleStatus);
        }

    },
    getPrice: function(vehicleId, manufacturingYear, vehicleStatus) {
        var res = PlussAuto.LoanApplication.GetPriceByVehicleId(vehicleId, manufacturingYear, vehicleStatus);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value.length == 0) {
        }
        else {
            var minimumPrice = res.value[0].MinimumPrice;
            if (minimumPrice != 0) {
                $('txtVerifiedPrice').setValue(minimumPrice);
            }
        }
    },


    //Get Infor by LLID
    GetInfoByLLID: function() {

        var searchKey = $F('txtLLID');
        var res = PlussAuto.LoanApplication.getLoanLocetorInfoByLLID(searchKey);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else if (res.value == 'No Data') {
            alert('No Data Found. Invalid LLID');

            var texts = document.getElementsByTagName('input');
            for (var i_tem = 0; i_tem < texts.length; i_tem++) {
                if (texts[i_tem].type == 'text') {
                    texts[i_tem].value = '';

                }
            }
            $('txtAutoLoanMasterId').setValue('0');
            $('txtParentLLID').setValue('0');
            $('txtPApplicantID').setValue('0');
            $('txtJApplicantID').setValue('0');
            $('txtPrProfessionId').setValue('0');
            $('txtJtProfessionId').setValue('0');
            $('txtAutoLoanVehicleId').setValue('0');
            $('txtPrAccountId').setValue('0');
            $('txtJtAccountId').setValue('0');
            $('txtPrAccountDetailsId1').setValue('0');
            $('txtPrAccountDetailsId2').setValue('0');
            $('txtPrAccountDetailsId3').setValue('0');
            $('txtPrAccountDetailsId4').setValue('0');
            $('txtPrAccountDetailsId5').setValue('0');
            $('txtJtAccountDetailsId1').setValue('0');
            $('txtJtAccountDetailsId2').setValue('0');
            $('txtJtAccountDetailsId3').setValue('0');
            $('txtJtAccountDetailsId4').setValue('0');
            $('txtJtAccountDetailsId5').setValue('0');
            $('txtPrFinanceId').setValue('0');
            $('txtJtFinanceId').setValue('0');
            $('txtReferenceId').setValue('0');
            $('txtFacilityId1').setValue('0');
            $('txtFacilityId2').setValue('0');
            $('txtFacilityId3').setValue('0');
            $('txtFacilityId4').setValue('0');
            $('txtFacilityId5').setValue('0');
            $('txtSecurityId1').setValue('0');
            $('txtSecurityId2').setValue('0');
            $('txtSecurityId3').setValue('0');
            $('txtSecurityId4').setValue('0');
            $('txtSecurityId5').setValue('0');


            return;
        }
        var name = res.value.ApplicantName;
        if (name != undefined) {

            $('txtPApplicantName').setValue(res.value.ApplicantName);
            $('txtSourceNAme').setValue(res.value.SourcePersonName);
            $('txtReceiveDate').setValue(res.value.RecieveDate.format());
            $('txtAutoLoanMasterId').setValue('0');
            $('txtPApplicantID').setValue('0');
            $('txtJApplicantID').setValue('0');
            $('txtPrProfessionId').setValue('0');
            $('txtJtProfessionId').setValue('0');
            $('txtAutoLoanVehicleId').setValue('0');
            $('txtPrAccountId').setValue('0');
            $('txtJtAccountId').setValue('0');
            $('txtPrAccountDetailsId1').setValue('0');
            $('txtPrAccountDetailsId2').setValue('0');
            $('txtPrAccountDetailsId3').setValue('0');
            $('txtPrAccountDetailsId4').setValue('0');
            $('txtPrAccountDetailsId5').setValue('0');
            $('txtJtAccountDetailsId1').setValue('0');
            $('txtJtAccountDetailsId2').setValue('0');
            $('txtJtAccountDetailsId3').setValue('0');
            $('txtJtAccountDetailsId4').setValue('0');
            $('txtJtAccountDetailsId5').setValue('0');
            $('txtPrFinanceId').setValue('0');
            $('txtJtFinanceId').setValue('0');
            $('txtReferenceId').setValue('0');
            $('txtFacilityId1').setValue('0');
            $('txtFacilityId2').setValue('0');
            $('txtFacilityId3').setValue('0');
            $('txtFacilityId4').setValue('0');
            $('txtFacilityId5').setValue('0');
            $('txtSecurityId1').setValue('0');
            $('txtSecurityId2').setValue('0');
            $('txtSecurityId3').setValue('0');
            $('txtSecurityId4').setValue('0');
            $('txtSecurityId5').setValue('0');
            //Account Number For Scb
            $('Text54').setValue(res.value.AccountNo);
            $('cmbSource').setValue(res.value.BranchId);
            if (res.value.DateOfBirth != "") {
                $('txtPApplicantDateOfBirth').setValue(res.value.DateOfBirth.format());
            }
            $('txtAppliedAmount').setValue(res.value.LoanApplyAmount);
            $('txtPApplicantNameofOrganization').setValue(res.value.Organization);
            $('txtPApplicantTINNumber').setValue(res.value.TinNo);


        }
        else {
            var sourceName = res.value.objAutoLoanMaster.SourceName;
            if (sourceName != '' && sourceName != undefined) {
                Page.loanApplication = res.value;
                // Header Section
                $('cmbProductName').setValue(res.value.objAutoLoanMaster.ProductId);
                $('txtSourceNAme').setValue(res.value.objAutoLoanMaster.SourceName);
                $('cmbSource').setValue(res.value.objAutoLoanMaster.Source);
                $('txtSourceCode').setValue(res.value.objAutoLoanMaster.SourceCode);
                $('txtAppliedDate').setValue(res.value.objAutoLoanMaster.AppliedDate.format());
                $('txtAppliedAmount').setValue(res.value.objAutoLoanMaster.AppliedAmount);
                $('txtReceiveDate').setValue(res.value.objAutoLoanMaster.ReceiveDate.format());
                $('cmbAskingTenor').setValue(res.value.objAutoLoanMaster.AskingTenor);
                $('txtAskingRate').setValue(res.value.objAutoLoanMaster.AskingRate);
                $('txtAutoLoanMasterId').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);
                $('txtParentLLID').setValue(res.value.objAutoLoanMaster.ParentLLID);

                if (res.value.objAutoLoanMaster.JointApplication == false) {
                    $('cmbJointApplication').setValue('0');
                }
                else {
                    $('cmbJointApplication').setValue('1');
                }
                LoanApplicationHealper.hideShowDivBasedOn_jointApplicationDiv();
                //            if(res.value.objAutoLoanMaster.JointApplication == cmbJointApplication

                // Applicant Primary
                $('txtPApplicantID').setValue(res.value.objAutoPRApplicant.PrApplicantId);
                $('txtPApplicantName').setValue(res.value.objAutoPRApplicant.PrName);
                $('cmbGenderForPrimary').setValue(res.value.objAutoPRApplicant.Gender);
                $('txtPApplicantRelationshipNumber').setValue(res.value.objAutoPRApplicant.RelationShipNumber);
                $('txtPApplicantTINNumber').setValue(res.value.objAutoPRApplicant.TINNumber);
                $('txtPApplicantFatherName').setValue(res.value.objAutoPRApplicant.FathersName);
                $('txtPApplicantMotherName').setValue(res.value.objAutoPRApplicant.MothersName);
                $('txtPApplicantDateOfBirth').setValue(res.value.objAutoPRApplicant.DateofBirth.format());
                $('cmbPApplicantMaritalStatus').setValue(res.value.objAutoPRApplicant.MaritalStatus);
                $('txtPApplicantSpouseName').setValue(res.value.objAutoPRApplicant.SpouseName);
                $('txtPApplicantSpouseProfession').setValue(res.value.objAutoPRApplicant.SpouseProfession);
                $('txtPApplicantSpouseWorkAddress').setValue(res.value.objAutoPRApplicant.SpouseWorkAddress);
                $('txtPApplicantSpouseContactNumber').setValue(res.value.objAutoPRApplicant.SpouseContactNumber);
                $('txtPApplicantRelationshipWithPrimary').setValue(res.value.objAutoPRApplicant.RelationShipwithPrimary);
                $('txtPApplicantNationality').setValue(res.value.objAutoPRApplicant.Nationality);
                $('cmbPApplicantIdentificationNumber').setValue(res.value.objAutoPRApplicant.IdentificationNumberType);
                $('txtPApplicantIdentificationNumber').setValue(res.value.objAutoPRApplicant.IdentificationNumber);
                $('cmbPApplicantIdentificationDocument').setValue(res.value.objAutoPRApplicant.IdentificationDocument);
                $('txtPApplicantNumberOfDependents').setValue(res.value.objAutoPRApplicant.NumberofDependents);
                $('cmbPApplicantHighestEducationLevel').setValue(res.value.objAutoPRApplicant.HighestEducation);
                $('txtPApplicantResidenceAddress').setValue(res.value.objAutoPRApplicant.ResidenceAddress);
                $('txtPApplicantMailingAddress').setValue(res.value.objAutoPRApplicant.MailingAddress);
                $('txtPApplicantPermanentAddress').setValue(res.value.objAutoPRApplicant.PermanentAddress);
                $('cmbPApplicantResidenceStatus').setValue(res.value.objAutoPRApplicant.ResidenceStatus);
                $('cmbPApplicantOwnershipDocument').setValue(res.value.objAutoPRApplicant.OwnershipDocument);
                $('cmbPApplicantResidenceStatusYear').setValue(res.value.objAutoPRApplicant.ResidenceStatusYear);
                $('txtPApplicantPhoneRescident').setValue(res.value.objAutoPRApplicant.PhoneResidence);
                $('txtpApplicantPhoneMobile').setValue(res.value.objAutoPRApplicant.Mobile);
                $('txtPApplicantEmail').setValue(res.value.objAutoPRApplicant.Email);
                $('cmbPAllicantDirectorShipWithAnyBank').setValue(res.value.objAutoPRApplicant.DirectorshipwithanyBank);
                // Applicant Primary End

                // Applicant JOINT
                $('txtJApplicantID').setValue(res.value.objAutoPRApplicant.JtApplicantId);
                $('txtJApplicantName').setValue(res.value.objAutoJTApplicant.JtName);
                $('cmbJointGender').setValue(res.value.objAutoJTApplicant.Gender);
                $('txtJApplicantRelationshipNumber').setValue(res.value.objAutoJTApplicant.RelationShipNumber);
                $('txtJApplicantTINNumber').setValue(res.value.objAutoJTApplicant.TINNumber);
                $('txtJApplicantFatherName').setValue(res.value.objAutoJTApplicant.FathersName);
                $('txtJApplicantMotherName').setValue(res.value.objAutoJTApplicant.MothersName);
                $('txtJApplicantDateOfBirth').setValue(res.value.objAutoJTApplicant.DateofBirth.format());
                $('cmbJApplicantMaritalStatus').setValue(res.value.objAutoJTApplicant.MaritalStatus);
                $('txtJApplicantSpouseName').setValue(res.value.objAutoJTApplicant.SpouseName);
                $('txtJApplicantSpouseProfession').setValue(res.value.objAutoJTApplicant.SpouseProfession);
                $('txtJApplicantSpouseWorkAddress').setValue(res.value.objAutoJTApplicant.SpouseWorkAddress);
                $('txtJApplicantSpouseContactNumber').setValue(res.value.objAutoJTApplicant.SpouseContactNumber);
                $('txtJApplicantRelationshipWithPrimary').setValue(res.value.objAutoJTApplicant.RelationShipwithPrimary);
                $('txtJApplicantNationality').setValue(res.value.objAutoJTApplicant.Nationality);
                $('cmbJApplicantIdentificationNumber').setValue(res.value.objAutoJTApplicant.IdentificationNumberType);
                $('txtJApplicantIdentificationNumber').setValue(res.value.objAutoJTApplicant.IdentificationNumber);
                $('cmbJApplicantIdentificationDocument').setValue(res.value.objAutoJTApplicant.IdentificationDocument);
                $('txtJApplicantNumberOfDependents').setValue(res.value.objAutoJTApplicant.NumberofDependents);
                $('cmbJApplicantHighestEducationLevel').setValue(res.value.objAutoJTApplicant.HighestEducation);
                $('txtJApplicantResidenceAddress').setValue(res.value.objAutoJTApplicant.ResidenceAddress);
                $('txtJApplicantMailingAddress').setValue(res.value.objAutoJTApplicant.MailingAddress);
                $('txtJApplicantPermanentAddress').setValue(res.value.objAutoJTApplicant.PermanentAddress);
                $('cmbJApplicantResidenceStatus').setValue(res.value.objAutoJTApplicant.ResidenceStatus);
                $('cmbJApplicantOwnershipDocument').setValue(res.value.objAutoJTApplicant.OwnershipDocument);
                $('cmbJApplicantResidenceStatusYear').setValue(res.value.objAutoJTApplicant.ResidenceStatusYear);
                $('txtJApplicantPhoneRescident').setValue(res.value.objAutoJTApplicant.PhoneResidence);
                $('txtJApplicantPhoneMobile').setValue(res.value.objAutoJTApplicant.Mobile);
                $('txtJApplicantEmail').setValue(res.value.objAutoJTApplicant.Email);
                $('cmbJAllicantDirectorShipWithAnyBank').setValue(res.value.objAutoJTApplicant.DirectorshipwithanyBank);
                // Applicant JOINT end

                // Profession Primary

                $('txtPrProfessionId').setValue(res.value.objAutoPRProfession.PrProfessionId);

                $('cmbPApplicantPrimaryProfession').setValue(res.value.objAutoPRProfession.PrimaryProfession);
                $('cmbPApplicantOtherProfession').setValue(res.value.objAutoPRProfession.OtherProfession);
                $('txtPApplicantMonthInCurrentProfession').setValue(res.value.objAutoPRProfession.MonthinCurrentProfession);
                $('cmbPApplicantNameOfCompany').setValue(res.value.objAutoPRProfession.NameofCompany);
                $('txtPApplicantNameofOrganization').setValue(res.value.objAutoPRProfession.NameofOrganization);
                $('txtPApplicantDesignation').setValue(res.value.objAutoPRProfession.Designation);
                $('txtPApplicantNatureOfBusiness').setValue(res.value.objAutoPRProfession.NatureOfBussiness);
                $('txtPApplicantYearsInBusiness').setValue(res.value.objAutoPRProfession.YearsInBussiness);
                $('cmdPApplicantOfficeStatus').setValue(res.value.objAutoPRProfession.OfficeStatus);
                $('txtPApplicantAddress').setValue(res.value.objAutoPRProfession.Address);
                $('txtPApplicantOfficePhone').setValue(res.value.objAutoPRProfession.OfficePhone);
                $('cmbPApplicantOwnershipType').setValue(res.value.objAutoPRProfession.OwnerShipType);
                $('cmbPApplicantEmploymentStatus').setValue(res.value.objAutoPRProfession.EmploymentStatus);
                $('txtPApplicantTotalProfessionalExperience').setValue(res.value.objAutoPRProfession.TotalProfessionalExperience);
                $('txtPApplicantNumberOfEmployees').setValue(res.value.objAutoPRProfession.NumberOfEmployees);
                $('txtPApplicantEquity_Share').setValue(res.value.objAutoPRProfession.EquityShare);
                $('txtPApplicantPrimaryIncomeSource').setValue(res.value.objAutoPRProfession.PrIncomeSource);
                $('txtPApplicantOtherIncomeSource').setValue(res.value.objAutoPRProfession.OtherIncomeSource);
                $('txtPApplicantNoOfFloorsRented').setValue(res.value.objAutoPRProfession.NoOfFloorsRented);
                $('cmbPApplicantNatureOfRentedFloors').setValue(res.value.objAutoPRProfession.NatureOfrentedFloors);
                $('txtPApplicantRentedAresInSFT').setValue(res.value.objAutoPRProfession.RentedAresInSFT);
                $('txtPApplicantConstructionCompletingYear').setValue(res.value.objAutoPRProfession.ConstructionCompletingYear);


                // Profession JOINT
                $('txtJtProfessionId').setValue(res.value.objAutoJTProfession.JtProfessionId);

                $('cmbJApplicantPrimaryProfession').setValue(res.value.objAutoJTProfession.PrimaryProfession);
                $('cmbJApplicantOtherProfession').setValue(res.value.objAutoJTProfession.OtherProfession);
                $('txtJApplicantMonthInCurrentProfession').setValue(res.value.objAutoJTProfession.MonthinCurrentProfession);
                $('cmbJApplicantNameOfCompany').setValue(res.value.objAutoJTProfession.NameofCompany);
                $('txtJApplicantNameofOrganization').setValue(res.value.objAutoJTProfession.NameofOrganization);
                $('txtJApplicantDesignation').setValue(res.value.objAutoJTProfession.Designation);
                $('txtJApplicantNatureOfBusiness').setValue(res.value.objAutoJTProfession.NatureOfBussiness);
                $('txtJApplicantYearsInBusiness').setValue(res.value.objAutoJTProfession.YearsInBussiness);
                $('cmdJApplicantOfficeStatus').setValue(res.value.objAutoJTProfession.OfficeStatus);
                $('txtJApplicantAddress').setValue(res.value.objAutoJTProfession.Address);
                $('txtJApplicantOfficePhone').setValue(res.value.objAutoJTProfession.OfficePhone);
                $('cmbJApplicantOwnershipType').setValue(res.value.objAutoJTProfession.OwnerShipType);
                $('cmbJApplicantEmploymentStatus').setValue(res.value.objAutoJTProfession.EmploymentStatus);
                $('txtJApplicantTotalProfessionalExperience').setValue(res.value.objAutoJTProfession.TotalProfessionalExperience);
                $('txtJApplicantNumberOfEmployees').setValue(res.value.objAutoJTProfession.NumberOfEmployees);
                $('txtJApplicantEquity_Share').setValue(res.value.objAutoJTProfession.EquityShare);
                $('txtJApplicantPrimaryIncomeSource').setValue(res.value.objAutoJTProfession.PrIncomeSource);
                $('txtJApplicantOtherIncomeSource').setValue(res.value.objAutoJTProfession.OtherIncomeSource);
                $('txtJApplicantNoOfFloorsRented').setValue(res.value.objAutoJTProfession.NoOfFloorsRented);
                $('cmbJApplicantNatureOfRentedFloors').setValue(res.value.objAutoJTProfession.NatureOfrentedFloors);
                $('txtJApplicantRentedAresInSFT').setValue(res.value.objAutoJTProfession.RentedAresInSFT);
                $('txtJApplicantConstructionCompletingYear').setValue(res.value.objAutoJTProfession.ConstructionCompletingYear);


                // Vehicle
                Page.vehicleDetailsArray = res.value.objAutoLoanVehicle;
                $('txtAutoLoanVehicleId').setValue(res.value.objAutoLoanVehicle.AutoLoanVehicleId);

                $('cmbVendor').setValue(res.value.objAutoLoanVehicle.VendorId);
                $('cmbManufacturer').setValue(res.value.objAutoLoanVehicle.Auto_ManufactureId);
                LoanApplicationManager.changeManufacture();
                $('cmbModel').setValue(res.value.objAutoLoanVehicle.VehicleId);
                LoanApplicationManager.changeModel();
                //                $('cmbTrimLevel').setValue(res.value.objAutoLoanVehicle.TrimLevel);
                //                LoanApplicationManager.changeTrimLevel();
                //                $('cmbEngineCC').setValue(res.value.objAutoLoanVehicle.EngineCC);
                //                LoanApplicationManager.changeEngineCC();
                $('txtVehicleType').setValue(res.value.objAutoLoanVehicle.VehicleType);
                $('cmdVehicleStatus').setValue(res.value.objAutoLoanVehicle.VehicleStatus);
                $('txtManufacturingyear').setValue(res.value.objAutoLoanVehicle.ManufacturingYear);
                LoanApplicationManager.changeManufacturingYear();
                $('txtQuotedPrice').setValue(res.value.objAutoLoanVehicle.QuotedPrice);
                $('txtVerifiedPrice').setValue(res.value.objAutoLoanVehicle.VerifiedPrice);
                $('cmbPriceConsideredBasedOn').setValue(res.value.objAutoLoanVehicle.PriceConsideredBasedOn);
                $('txtSeatingCapacity').setValue(res.value.objAutoLoanVehicle.SeatingCapacity);
                $('cmbCarVerification').setValue(res.value.objAutoLoanVehicle.CarVerification);


                // Vehicle Insurance

                $('cmbGeneralInsurance').setValue(res.value.objAutoApplicationInsurance.GeneralInsurance);
                $('cmbLifeInsurance').setValue(res.value.objAutoApplicationInsurance.LifeInsurance);
                if (res.value.objAutoApplicationInsurance.GeneralInsurance != "-1") {
                    $('cmbInsuranceFinancewithLoan').setValue(res.value.objAutoApplicationInsurance.InsuranceFinancewithLoan);
                    LoanApplicationHealper.changeGeneralInsurence();
                }
                
                if (res.value.objAutoApplicationInsurance.LifeInsurance != "-1") {
                    LoanApplicationHealper.changeLifeInsurence();
                    $('cmbInsuranceFinancewithLoanLife').setValue(res.value.objAutoApplicationInsurance.InsuranceFinancewithLoanLife);
                }

                // Accounts Primary

                $('txtPrAccountId').setValue(res.value.objAutoPRAccount.PrAccountId);

                $('Text54').setValue(res.value.objAutoPRAccount.AccountNumberForSCB);
                $('Text95').setValue(res.value.objAutoPRAccount.CreditCardNumberForSCB);


                // Accounts JOINT

                $('txtJtAccountId').setValue(res.value.objAutoPRAccount.jtAccountId);
                $('Text55').setValue(res.value.objAutoJTAccount.AccountNumberForSCB);
                $('Text59').setValue(res.value.objAutoJTAccount.CreditCardNumberForSCB);




                //Primary Applicant Other Bank Account

                if (res.value.objAutoPRAccountDetailList.length != null) {

                    for (var i = 0; i < res.value.objAutoPRAccountDetailList.length; i++) {

                        var rowNo = i + 1;
                        if (rowNo < 6) {
                            $('txtPrAccountDetailsId' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].PrAccountDetailsId);

                            $('cmbPApplicantBank' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].BankId);
                            LoanApplicationManager.GetBranch('cmbPApplicantBank' + rowNo);
                            $('cmbPApplicantBranch' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].BranchId);
                            $('txtPApplicantAccountNumber' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].AccountNumber);
                            $('cmbPApplicantAccountCategory' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].AccountCategory);
                            $('cmbPApplicantAccountType' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].AccountType);
                        }

                    }

                }

                //Joint Applicant Other Bank Account

                if (res.value.objAutoJtAccountDetailList.length != null) {

                    for (var i = 0; i < res.value.objAutoJtAccountDetailList.length; i++) {

                        var rowNo = i + 1;
                        if (rowNo < 6) {
                            $('txtJtAccountDetailsId' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].JtAccountDetailsId);
                            $('cmbJApplicantBank' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].BankId);
                            LoanApplicationManager.GetBranch('cmbJApplicantBank' + rowNo);
                            $('cmbJApplicantBranch' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].BranchId);
                            $('txtJApplicantAccountNumber' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].AccountNumber);
                            $('cmbJApplicantAccountCategory' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].AccountCategory);
                            $('cmbJApplicantAccountType' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].AccountType);
                        }
                    }
                }


                //Financial Primary

                $('txtPrFinanceId').setValue(res.value.objAutoPRFinance.PrFinanceId);
                $('txtPApplicantDeclaredPrimaryIncomeSourch').setValue(res.value.objAutoPRFinance.DPISource);
                $('txtPApplicantDeclaredPrimaryIncomeAmount').setValue(res.value.objAutoPRFinance.DPIAmount);
                $('txtPApplicantDeclaredOtherIncomeSourch').setValue(res.value.objAutoPRFinance.DOISource);
                $('txtPApplicantDeclaredOtherIncomeAmount').setValue(res.value.objAutoPRFinance.DOIAmount);
                $('txtPApplicantRentUtilities').setValue(res.value.objAutoPRFinance.EDRentAndUtilities);
                $('txtPApplicantFoodClothing').setValue(res.value.objAutoPRFinance.EDFoodAndClothing);
                $('txtPApplicantEducation').setValue(res.value.objAutoPRFinance.EDEducation);
                $('txtPApplicantLoanRepayment').setValue(res.value.objAutoPRFinance.EDLoanRePayment);
                $('txtPApplicantOthers').setValue(res.value.objAutoPRFinance.EDOther);
                $('txtPApplicantRepaymentto').setValue(res.value.objAutoPRFinance.RepaymentTo);
                $('txtPApplicantRepaymentfor').setValue(res.value.objAutoPRFinance.PaymentFor);



                //Financial Joint

                $('txtJtFinanceId').setValue(res.value.objAutoJTFinance.JtFinanceId);
                $('txtJApplicantDeclaredPrimaryIncomeSourch').setValue(res.value.objAutoJTFinance.DPISource);
                $('txtJApplicantDeclaredPrimaryIncomeAmount').setValue(res.value.objAutoJTFinance.DPIAmount);
                $('txtJApplicantDeclaredOtherIncomeSourch').setValue(res.value.objAutoJTFinance.DOISource);
                $('txtJApplicantDeclaredOtherIncomeAmount').setValue(res.value.objAutoJTFinance.DOIAmount);
                $('txtJApplicantRentUtilities').setValue(res.value.objAutoJTFinance.EDRentAndUtilities);
                $('txtJApplicantFoodClothing').setValue(res.value.objAutoJTFinance.EDFoodAndClothing);
                $('txtJApplicantEducation').setValue(res.value.objAutoJTFinance.EDEducation);
                $('txtJApplicantLoanRepayment').setValue(res.value.objAutoJTFinance.EDLoanRePayment);
                $('txtJApplicantOthers').setValue(res.value.objAutoJTFinance.EDOther);
                $('txtJApplicantRepaymentto').setValue(res.value.objAutoJTFinance.RepaymentTo);
                $('txtJApplicantRepaymentfor').setValue(res.value.objAutoJTFinance.PaymentFor);


                // References 1

                $('txtReferenceId').setValue(res.value.objAutoLoanReference.ReferenceId);
                $('txtReferenceName1').setValue(res.value.objAutoLoanReference.ReferenceName1);
                $('txtReferenceName2').setValue(res.value.objAutoLoanReference.ReferenceName2);
                $('txtReferenceRelationship1').setValue(res.value.objAutoLoanReference.Relationship1);
                $('txtReferenceRelationship2').setValue(res.value.objAutoLoanReference.Relationship2);
                $('cmbReferenceOccupation1').setValue(res.value.objAutoLoanReference.Occupation1);
                $('cmbReferenceOccupation2').setValue(res.value.objAutoLoanReference.Occupation2);
                $('txtReferenceNameOfTheOrganization1').setValue(res.value.objAutoLoanReference.OrganizationName1);
                $('txtReferenceNameOfTheOrganization2').setValue(res.value.objAutoLoanReference.OrganizationName2);
                $('txtReferenceDesignation1').setValue(res.value.objAutoLoanReference.Designation1);
                $('txtReferenceDesignation2').setValue(res.value.objAutoLoanReference.Designation2);
                $('txtReferenceWorkAddress1').setValue(res.value.objAutoLoanReference.WorkAddress1);
                $('txtReferenceWorkAddress2').setValue(res.value.objAutoLoanReference.WorkAddress2);
                $('txtReferenceResidenceAddress1').setValue(res.value.objAutoLoanReference.ResidenceAddress1);
                $('txtReferenceResidenceAddress2').setValue(res.value.objAutoLoanReference.ResidenceAddress2);
                $('txtReferencePhoneNumber1').setValue(res.value.objAutoLoanReference.PhoneNumber1);
                $('txtReferencePhoneNumber2').setValue(res.value.objAutoLoanReference.PhoneNumber2);
                $('txtReferenceMobileNumber1').setValue(res.value.objAutoLoanReference.MobileNumber1);
                $('txtReferenceMobileNumber2').setValue(res.value.objAutoLoanReference.MobileNumber2);


                // Facility Schedule
                if (res.value.objAutoLoanFacilityList.length) {

                    for (var i = 0; i < res.value.objAutoLoanFacilityList.length; i++) {

                        var rowNo = i + 1;

                        if (rowNo < 6) {
                            var textToFind = res.value.objAutoLoanFacilityList[i].FacilityType;

                            //                        var dd = document.getElementById('cmbFacilityScheduleFacilityType' + rowNo);
                            //                        for (var j = 0; i < dd.options.length; j++) {
                            //                            if (dd.options[j].text === textToFind) {
                            //                                dd.selectedIndex = j;
                            //                                break;
                            //                            }
                            //                        }
                            $('txtFacilityId' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].FacilityId);
                            $('cmbFacilityScheduleFacilityType' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);
                            $('txtFacilityScheduleInterestRate' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                            $('txtFacilitySchedulePresentBalance' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].PresentBalance);
                            $('txtFacilitySchedulePresentEMI' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].PresentEMI);
                            $('txtFacilitySchedulePresentLimit' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                            $('txtFacilityScheduleProposedLimit' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].ProposedLimit);
                            $('txtFacilityScheduleRepaymentAgreement' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].RepaymentAgrement);

                            //Sequrity List
                            $('txtSecurityId' + rowNo).setValue('0');
                            $('cmbStatusForSecurities' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].Status);
                            $('txtSecurityScheduleNatureOfSecurity' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].NatureOfSecurity);
                            $('txtSecurityScheduleFaceValue' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].FaceValue);
                            $('txtSecurityScheduleXTVRate' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].XTVRate);
                            $('txtSecurityScheduleInterestRate' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                        }
                    }
                }


                // Security Schedule

                //                if (res.value.objAutoLoanSecurityList.length) {

                //                    for (var i = 0; i < res.value.objAutoLoanSecurityList.length; i++) {

                //                        var rowNo = i + 1;
                //                        if (rowNo < 6) {

                //                            //                        var textToFind = res.value.objAutoLoanSecurityList[i].NatureOfSecurity;

                //                            //                        var dd = document.getElementById('txtSecurityScheduleNatureOfSecurity' + rowNo);
                //                            //                        for (var j = 0; i < dd.options.length; j++) {
                //                            //                            if (dd.options[j].text === textToFind) {
                //                            //                                dd.selectedIndex = j;
                //                            //                                break;
                //                            //                            }
                //                            //                        }
                //                            $('txtSecurityId' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].SecurityId);
                //                            $('cmbStatusForSecurities' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].Status);
                //                            //$('cmbfacilityTypeForSequrity' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].FacilityType);
                //                            $('txtSecurityScheduleNatureOfSecurity' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                //                            //$('txtSecurityScheduleIssuingOffice' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                //                            $('txtSecurityScheduleFaceValue' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                //                            $('txtSecurityScheduleXTVRate' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                //                            //$('txtSecurityScheduleIssueDate' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());
                //                            $('txtSecurityScheduleInterestRate' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);
                //                        }
                //                    }
                //                }

                //        $('txtPApplicantName').setValue(res.objAutoLoanMaster.value.ApplicantName);
                //            $('txtSourceNAme').setValue(res.value.SourcePersonName);
                //           LoanApplicationHealper.LoadTemplate();

            }

            else {
                alert("No information found.");
            }
        }
    },


    GetBranch: function(ElementID) {
        // alert(ElementID);
        //        debugger;
        var bankID = $F(ElementID);
        if (bankID == 0) {
        }
        else {
            var res = PlussAuto.LoanApplication.GetBranch(bankID);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            LoanApplicationHealper.populateBranchCombo(res, ElementID);
        }

    },
    //Save Loan Application
    SaveLoanButtonClick: function() {
        var stateId = 42;
        LoanApplicationManager.SaveLoanApplication(stateId);
    },
    ForwardLoanButtonClick: function() {
        var appliedAmount = parseFloat($F('txtPApplicantDeclaredPrimaryIncomeAmount'));
        if (appliedAmount < 20000 || $F('txtPApplicantDeclaredPrimaryIncomeAmount') == "") {
            alert("Declare Income cannot less then 20,000.00 tk. for Primary Applicant");
            return false;
        }
        if ($F('cmbJointApplication') == "1") {
            var appliedAmountForJoint = parseFloat($F('txtJApplicantDeclaredPrimaryIncomeAmount'));
            if (appliedAmountForJoint < 10000 || $F('txtJApplicantDeclaredPrimaryIncomeAmount') == "") {
                alert("Declare Income cannot less then 10,000.00 tk. for Joint Applicant");
                return false;
            }
        }
        var stateId = 40;
        LoanApplicationManager.SaveLoanApplication(stateId);
    },

    SaveLoanApplication: function(stateId) {
        if (LoanApplicationHealper.validetLoanApplicationForm()) {
            //        if (true) {
            //Loan master
            var objAutoLoanMaster = new Object();
            objAutoLoanMaster.Auto_LoanMasterId = $F('txtAutoLoanMasterId');
            objAutoLoanMaster.ParentLLID = $F("txtParentLLID")
            objAutoLoanMaster.LLID = $F('txtLLID');
            objAutoLoanMaster.StatusID = stateId;
            objAutoLoanMaster.ProductId = $F('cmbProductName');
            objAutoLoanMaster.Source = $F('cmbSource');
            objAutoLoanMaster.SourceCode = $F('txtSourceCode');
            objAutoLoanMaster.SourceName = $F('txtSourceNAme');
            objAutoLoanMaster.AppliedDate = Date.parseUK($F('txtAppliedDate'));
            objAutoLoanMaster.AppliedAmount = $F('txtAppliedAmount');
            //objAutoLoanMaster.ReceiveDate = Date.parseUK($F('txtReceiveDate'));   // Change by Washik confirm by Farshad Receive Date will be hidden.
            objAutoLoanMaster.ReceiveDate = Date.parseUK($F('txtAppliedDate'));
            objAutoLoanMaster.AskingTenor = $F('cmbAskingTenor');
            objAutoLoanMaster.AskingRate = $F('txtAskingRate');
            //objAutoLoanMaster.StatusID = '1';
            if ($F('cmbJointApplication') == '1') {
                objAutoLoanMaster.JointApplication = true;
            }
            else {
                objAutoLoanMaster.JointApplication = false;
            }
            objAutoLoanMaster.ReferenceLLId = '0';

            // PR Applicant
            var objAutoPRApplicant = new Object();
            objAutoPRApplicant.PrApplicantId = $F('txtPApplicantID');
            objAutoPRApplicant.PrName = $F('txtPApplicantName');
            objAutoPRApplicant.Gender = $F('cmbGenderForPrimary');

            objAutoPRApplicant.RelationShipNumber = $F('txtPApplicantRelationshipNumber');
            objAutoPRApplicant.TINNumber = $F('txtPApplicantTINNumber');
            objAutoPRApplicant.FathersName = $F('txtPApplicantFatherName');
            objAutoPRApplicant.MothersName = $F('txtPApplicantMotherName');
            objAutoPRApplicant.DateofBirth = Date.parseUK($F('txtPApplicantDateOfBirth'));
            objAutoPRApplicant.MaritalStatus = $F('cmbPApplicantMaritalStatus');
            objAutoPRApplicant.SpouseName = $F('txtPApplicantSpouseName');
            objAutoPRApplicant.SpouseProfession = $F('txtPApplicantSpouseProfession');
            objAutoPRApplicant.SpouseWorkAddress = $F('txtPApplicantSpouseWorkAddress');
            objAutoPRApplicant.SpouseContactNumber = $F('txtPApplicantSpouseContactNumber');
            objAutoPRApplicant.RelationShipwithPrimary = $F('txtPApplicantRelationshipWithPrimary');
            objAutoPRApplicant.Nationality = $F('txtPApplicantNationality');

            objAutoPRApplicant.IdentificationNumberType = $F('cmbPApplicantIdentificationNumber');
            objAutoPRApplicant.IdentificationNumber = $F('txtPApplicantIdentificationNumber');
            objAutoPRApplicant.IdentificationDocument = $F('cmbPApplicantIdentificationDocument');

            objAutoPRApplicant.NumberofDependents = $F('txtPApplicantNumberOfDependents');
            objAutoPRApplicant.HighestEducation = $F('cmbPApplicantHighestEducationLevel');
            objAutoPRApplicant.ResidenceAddress = $F('txtPApplicantResidenceAddress');
            objAutoPRApplicant.MailingAddress = $F('txtPApplicantMailingAddress');
            objAutoPRApplicant.PermanentAddress = $F('txtPApplicantPermanentAddress');
            objAutoPRApplicant.ResidenceStatus = $F('cmbPApplicantResidenceStatus');
            objAutoPRApplicant.OwnershipDocument = $F('cmbPApplicantOwnershipDocument');
            objAutoPRApplicant.ResidenceStatusYear = $F('cmbPApplicantResidenceStatusYear');
            objAutoPRApplicant.PhoneResidence = $F('txtPApplicantPhoneRescident');
            objAutoPRApplicant.Mobile = $F('txtpApplicantPhoneMobile');
            objAutoPRApplicant.Email = $F('txtPApplicantEmail');
            if ($F('cmbPAllicantDirectorShipWithAnyBank') == '1') {
                objAutoPRApplicant.DirectorshipwithanyBank = true;
            }
            else {
                objAutoPRApplicant.DirectorshipwithanyBank = false;
            }

            // JT Applicant
            var objAutoJTApplicant = new Object();
            objAutoJTApplicant.JtApplicantId = $F('txtJApplicantID');
            objAutoJTApplicant.JtName = $F('txtJApplicantName');
            objAutoJTApplicant.Gender = $F('cmbJointGender');
            objAutoJTApplicant.RelationShipNumber = $F('txtJApplicantRelationshipNumber');
            objAutoJTApplicant.TINNumber = $F('txtJApplicantTINNumber');
            objAutoJTApplicant.FathersName = $F('txtJApplicantFatherName');
            objAutoJTApplicant.MothersName = $F('txtJApplicantMotherName');
            objAutoJTApplicant.DateofBirth = Date.parseUK($F('txtJApplicantDateOfBirth'));
            objAutoJTApplicant.MaritalStatus = $F('cmbJApplicantMaritalStatus');
            objAutoJTApplicant.SpouseName = $F('txtJApplicantSpouseName');
            objAutoJTApplicant.SpouseProfession = $F('txtJApplicantSpouseProfession');
            objAutoJTApplicant.SpouseWorkAddress = $F('txtJApplicantSpouseWorkAddress');
            objAutoJTApplicant.SpouseContactNumber = $F('txtJApplicantSpouseContactNumber');
            objAutoJTApplicant.RelationShipwithPrimary = $F('txtJApplicantRelationshipWithPrimary');
            objAutoJTApplicant.Nationality = $F('txtJApplicantNationality');
            objAutoJTApplicant.IdentificationNumberType = $F('cmbJApplicantIdentificationNumber');
            objAutoJTApplicant.IdentificationNumber = $F('txtJApplicantIdentificationNumber');
            objAutoJTApplicant.IdentificationDocument = $F('cmbJApplicantIdentificationDocument');
            objAutoJTApplicant.NumberofDependents = $F('txtJApplicantNumberOfDependents');
            objAutoJTApplicant.HighestEducation = $F('cmbJApplicantHighestEducationLevel');
            objAutoJTApplicant.ResidenceAddress = $F('txtJApplicantResidenceAddress');
            objAutoJTApplicant.MailingAddress = $F('txtJApplicantMailingAddress');
            objAutoJTApplicant.PermanentAddress = $F('txtJApplicantPermanentAddress');
            objAutoJTApplicant.ResidenceStatus = $F('cmbJApplicantResidenceStatus');
            objAutoJTApplicant.OwnershipDocument = $F('cmbJApplicantOwnershipDocument');
            objAutoJTApplicant.ResidenceStatusYear = $F('cmbJApplicantResidenceStatusYear');
            objAutoJTApplicant.PhoneResidence = $F('txtJApplicantPhoneRescident');
            objAutoJTApplicant.Mobile = $F('txtJApplicantPhoneMobile');
            objAutoJTApplicant.Email = $F('txtJApplicantEmail');
            if ($F('cmbJAllicantDirectorShipWithAnyBank') == '1') {
                objAutoJTApplicant.DirectorshipwithanyBank = true;
            }
            else {
                objAutoJTApplicant.DirectorshipwithanyBank = false;
            }


            //PR Profession
            var objAutoPRProfession = new Object();
            objAutoPRProfession.PrProfessionId = $F('txtPrProfessionId');
            objAutoPRProfession.PrimaryProfession = $F('cmbPApplicantPrimaryProfession');
            objAutoPRProfession.OtherProfession = $F('cmbPApplicantOtherProfession');
            objAutoPRProfession.MonthinCurrentProfession = $F('txtPApplicantMonthInCurrentProfession');
            objAutoPRProfession.NameofCompany = $F('cmbPApplicantNameOfCompany');
            objAutoPRProfession.NameofOrganization = $F('txtPApplicantNameofOrganization');
            objAutoPRProfession.Designation = $F('txtPApplicantDesignation');
            objAutoPRProfession.NatureOfBussiness = $F('txtPApplicantNatureOfBusiness');
            objAutoPRProfession.YearsInBussiness = $F('txtPApplicantYearsInBusiness');
            objAutoPRProfession.OfficeStatus = $F('cmdPApplicantOfficeStatus');
            objAutoPRProfession.Address = $F('txtPApplicantAddress');
            objAutoPRProfession.OfficePhone = $F('txtPApplicantOfficePhone');
            objAutoPRProfession.OwnerShipType = $F('cmbPApplicantOwnershipType');
            objAutoPRProfession.EmploymentStatus = $F('cmbPApplicantEmploymentStatus');
            objAutoPRProfession.TotalProfessionalExperience = $F('txtPApplicantTotalProfessionalExperience');

            objAutoPRProfession.NumberOfEmployees = $F('txtPApplicantNumberOfEmployees') == '' ? '0' : $F('txtPApplicantNumberOfEmployees');
            objAutoPRProfession.EquityShare = $F('txtPApplicantEquity_Share') == '' ? '0' : $F('txtPApplicantEquity_Share');
            objAutoPRProfession.PrIncomeSource = $F('txtPApplicantPrimaryIncomeSource');
            objAutoPRProfession.OtherIncomeSource = $F('txtPApplicantOtherIncomeSource');
            objAutoPRProfession.NoOfFloorsRented = $F('txtPApplicantNoOfFloorsRented') == '' ? '0' : $F('txtPApplicantNoOfFloorsRented');
            objAutoPRProfession.NatureOfrentedFloors = $F('cmbPApplicantNatureOfRentedFloors');
            objAutoPRProfession.RentedAresInSFT = $F('txtPApplicantRentedAresInSFT');
            objAutoPRProfession.ConstructionCompletingYear = $F('txtPApplicantConstructionCompletingYear');

            //Jt Profession
            var objAutoJTProfession = new Object();
            objAutoJTProfession.JtProfessionId = $F('txtJtProfessionId');
            objAutoJTProfession.PrimaryProfession = $F('cmbJApplicantPrimaryProfession');
            objAutoJTProfession.OtherProfession = $F('cmbJApplicantOtherProfession');
            objAutoJTProfession.MonthinCurrentProfession = $F('txtJApplicantMonthInCurrentProfession');
            objAutoJTProfession.NameofCompany = $F('cmbJApplicantNameOfCompany');
            objAutoJTProfession.NameofOrganization = $F('txtJApplicantNameofOrganization');
            objAutoJTProfession.Designation = $F('txtJApplicantDesignation');
            objAutoJTProfession.NatureOfBussiness = $F('txtJApplicantNatureOfBusiness');
            objAutoJTProfession.YearsInBussiness = $F('txtJApplicantYearsInBusiness');
            //            debugger;
            objAutoJTProfession.OfficeStatus = $F('cmdJApplicantOfficeStatus');
            objAutoJTProfession.Address = $F('txtJApplicantAddress');
            objAutoJTProfession.OfficePhone = $F('txtJApplicantOfficePhone');
            objAutoJTProfession.OwnerShipType = $F('cmbJApplicantOwnershipType');
            objAutoJTProfession.EmploymentStatus = $F('cmbJApplicantEmploymentStatus');
            objAutoJTProfession.TotalProfessionalExperience = $F('txtJApplicantTotalProfessionalExperience');

            objAutoJTProfession.NumberOfEmployees = $F('txtJApplicantNumberOfEmployees') == '' ? '0' : $F('txtJApplicantNumberOfEmployees');
            objAutoJTProfession.EquityShare = $F('txtJApplicantEquity_Share') == '' ? '0' : $F('txtJApplicantEquity_Share');

            objAutoJTProfession.PrIncomeSource = $F('txtJApplicantPrimaryIncomeSource');
            objAutoJTProfession.OtherIncomeSource = $F('txtJApplicantOtherIncomeSource');

            objAutoJTProfession.NoOfFloorsRented = $F('txtJApplicantNoOfFloorsRented') == '' ? '0' : $F('txtJApplicantNoOfFloorsRented');

            objAutoJTProfession.NatureOfrentedFloors = $F('cmbJApplicantNatureOfRentedFloors');
            objAutoJTProfession.RentedAresInSFT = $F('txtJApplicantRentedAresInSFT');
            objAutoJTProfession.ConstructionCompletingYear = $F('txtJApplicantConstructionCompletingYear');

            //auto loan vehicle
            var objAutoLoanVehicle = new Object();
            objAutoLoanVehicle.AutoLoanVehicleId = $F('txtAutoLoanVehicleId');
            objAutoLoanVehicle.VendorId = $F('cmbVendor');

            //new vendor
            var objAutoVendor = new Object();
            objAutoVendor.VendorName = $F('txtVendorName');
            //            objAutoVendor.DealerType = $F('cmbDealerType');
            objAutoVendor.DealerType = '1';
            objAutoVendor.DealerType = '1';
            //new vendor end

            //continue auto loan vehicle

            objAutoLoanVehicle.Auto_ManufactureId = $F('cmbManufacturer');
            //Change by Washik Confirmed by Farshad SCB Date 1/11/2012----------------------------------
            var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;

            if (manufacturName != "Other") {
                objAutoLoanVehicle.VehicleId = $F('cmbModel');
                var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
                if (vehicleName != "Other") {
                    objAutoLoanVehicle.Model = $F('hdnModel');
                    objAutoLoanVehicle.VehicleType = $F('txtVehicleType');
                }
                else {
                    objAutoLoanVehicle.Model = vehicleName;
                    objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
                }
            }
            else {
                objAutoLoanVehicle.VehicleId = "-7"; //Vehicle -7= Others
                objAutoLoanVehicle.Model = $F('txtModelName');
                objAutoLoanVehicle.VehicleType = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text;
            }
            objAutoLoanVehicle.TrimLevel = $F('txtTrimLevel');
            objAutoLoanVehicle.EngineCC = $F('txtEngineCC');
            objAutoLoanVehicle.VehicleStatus = $F('cmdVehicleStatus');

            objAutoLoanVehicle.ManufacturingYear = $F('txtManufacturingyear') == '' ? '0' : $F('txtManufacturingyear');
            objAutoLoanVehicle.QuotedPrice = $F('txtQuotedPrice') == '' ? '0' : $F('txtQuotedPrice');
            objAutoLoanVehicle.VerifiedPrice = $F('txtVerifiedPrice') == '' ? '0' : $F('txtVerifiedPrice');

            objAutoLoanVehicle.PriceConsideredBasedOn = $F('cmbPriceConsideredBasedOn');
            objAutoLoanVehicle.SeatingCapacity = $F('txtSeatingCapacity');
            objAutoLoanVehicle.CarVerification = $F('cmbCarVerification');


            // auto application insurance
            
            var objAutoApplicationInsurance = new Object();
            objAutoApplicationInsurance.GeneralInsurance = $F('cmbGeneralInsurance');
            objAutoApplicationInsurance.LifeInsurance = $F('cmbLifeInsurance');
            if ($F('cmbInsuranceFinancewithLoan') == '1') {
                objAutoApplicationInsurance.InsuranceFinancewithLoan = true;
            }
            else {
                objAutoApplicationInsurance.InsuranceFinancewithLoan = false;
            }
            if ($F('cmbInsuranceFinancewithLoanLife') == '1') {
                objAutoApplicationInsurance.InsuranceFinancewithLoanLife = true;
            }
            else {
                objAutoApplicationInsurance.InsuranceFinancewithLoanLife = false;
            }


            // PR Account
            var objAutoPRAccount = new Object();
            objAutoPRAccount.PrAccountId = $F('txtPrAccountId');
            objAutoPRAccount.AccountNumberForSCB = $F('Text54');
            objAutoPRAccount.CreditCardNumberForSCB = $F('Text95');

            var objAutoJTAccount = new Object();
            objAutoJTAccount.jtAccountId = $F('txtJtAccountId');
            objAutoJTAccount.AccountNumberForSCB = $F('Text55');
            objAutoJTAccount.CreditCardNumberForSCB = $F('Text59');


            //pr account detail
            var objAutoPRAccountDetailList = [];

            var tablePrAccount = $('primaryBankAccTable');
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                var objAutoPRAccountDetail = new Object();
                if ($F('cmbPApplicantBank' + i) == '-1') {
                }
                else {
                    if ($F('cmbPApplicantBranch' + i) == '-1') {
                        alert('please select branch');
                        //$('cmbPApplicantBranch' + i).focus();
                        return false;
                    }
                    else {
                        if ($F('txtPApplicantAccountNumber' + i) == '') {
                            alert('please input Account Number');
                            //$('txtPApplicantAccountNumber' + i).focus();
                            return false;
                        }
                        else {
                            objAutoPRAccountDetail.PrAccountDetailsId = $F('txtPrAccountDetailsId' + i);
                            objAutoPRAccountDetail.BankId = $F('cmbPApplicantBank' + i);
                            objAutoPRAccountDetail.BranchId = $F('cmbPApplicantBranch' + i);
                            objAutoPRAccountDetail.AccountNumber = $F('txtPApplicantAccountNumber' + i);
                            objAutoPRAccountDetail.AccountCategory = $F('cmbPApplicantAccountCategory' + i);
                            objAutoPRAccountDetail.AccountType = $F('cmbPApplicantAccountType' + i);
                            objAutoPRAccountDetailList.push(objAutoPRAccountDetail);
                        }

                    }
                }
                //                objAutoPRAccountDetail.AccountCategory = $F('cmbPApplicantAccountCategory' + i);
                //                objAutoPRAccountDetail.AccountType = $F('cmbPApplicantAccountType' + i);
                //                if (objAutoPRAccountDetail.AccountNumber == undefined) {
                //                }
                //                else {
                //                    objAutoPRAccountDetailList.push(objAutoPRAccountDetail);
                //                }
            }



            var objAutoJtAccountDetailList = [];

            var tableJtAccount = $('jointBankAccTable');
            var rowCount = tableJtAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                var objAutoJtAccountDetail = new Object();
                if ($F('cmbJApplicantBank' + i) == '-1') {
                }
                else {
                    if ($F('cmbJApplicantBranch' + i) == '-1') {
                        alert('please select branch');
                        //$('cmbJApplicantBranch' + i).focus();
                        return false;
                    }
                    else {
                        if ($F('txtJApplicantAccountNumber' + i) == '') {
                            alert('please input Account Number');
                            //$('txtJApplicantAccountNumber' + i).focus();
                            return false;
                        }
                        else {
                            objAutoJtAccountDetail.JtAccountDetailsId = $F('txtJtAccountDetailsId' + i);
                            objAutoJtAccountDetail.BankId = $F('cmbJApplicantBank' + i);
                            objAutoJtAccountDetail.BranchId = $F('cmbJApplicantBranch' + i);
                            objAutoJtAccountDetail.AccountNumber = $F('txtJApplicantAccountNumber' + i);
                            objAutoJtAccountDetail.AccountCategory = $F('cmbJApplicantAccountCategory' + i);
                            objAutoJtAccountDetail.AccountType = $F('cmbJApplicantAccountType' + i);
                            objAutoJtAccountDetailList.push(objAutoJtAccountDetail);

                        }
                    }
                }
                //                objAutoJtAccountDetail.AccountCategory = $F('cmbJApplicantAccountCategory' + i);
                //                objAutoJtAccountDetail.AccountType = $F('cmbJApplicantAccountType' + i);
                //                if (objAutoJtAccountDetail.AccountNumber == undefined) {
                //                }
                //                else {
                //                    objAutoJtAccountDetailList.push(objAutoJtAccountDetail);
                //                }
            }
            // PR Finance
            var objAutoPRFinance = new Object();
            objAutoPRFinance.PrFinanceId = $F('txtPrFinanceId');
            objAutoPRFinance.DPISource = $F('txtPApplicantDeclaredPrimaryIncomeSourch');
            objAutoPRFinance.DPIAmount = $F('txtPApplicantDeclaredPrimaryIncomeAmount') == '' ? '0' : $F('txtPApplicantDeclaredPrimaryIncomeAmount');

            objAutoPRFinance.DOISource = $F('txtPApplicantDeclaredOtherIncomeSourch');
            //            debugger;
            objAutoPRFinance.DOIAmount = $F('txtPApplicantDeclaredOtherIncomeAmount') == '' ? '0' : $F('txtPApplicantDeclaredOtherIncomeAmount');
            objAutoPRFinance.EDRentAndUtilities = $F('txtPApplicantRentUtilities') == '' ? '0' : $F('txtPApplicantRentUtilities');
            objAutoPRFinance.EDFoodAndClothing = $F('txtPApplicantFoodClothing') == '' ? '0' : $F('txtPApplicantFoodClothing');
            objAutoPRFinance.EDEducation = $F('txtPApplicantEducation') == '' ? '0' : $F('txtPApplicantEducation');
            objAutoPRFinance.EDLoanRePayment = $F('txtPApplicantLoanRepayment') == '' ? '0' : $F('txtPApplicantLoanRepayment');
            objAutoPRFinance.EDOther = $F('txtPApplicantOthers') == '' ? '0' : $F('txtPApplicantOthers');

            objAutoPRFinance.RepaymentTo = $F('txtPApplicantRepaymentto');
            objAutoPRFinance.PaymentFor = $F('txtPApplicantRepaymentfor');


            // JT Finance

            var objAutoJTFinance = new Object();
            objAutoJTFinance.JtFinanceId = $F('txtJtFinanceId');
            objAutoJTFinance.DPISource = $F('txtJApplicantDeclaredPrimaryIncomeSourch');
            objAutoJTFinance.DPIAmount = $F('txtJApplicantDeclaredPrimaryIncomeAmount') == '' ? '0' : $F('txtJApplicantDeclaredPrimaryIncomeAmount');
            objAutoJTFinance.DOISource = $F('txtJApplicantDeclaredOtherIncomeSourch');

            objAutoJTFinance.DOIAmount = $F('txtJApplicantDeclaredOtherIncomeAmount') == '' ? '0' : $F('txtJApplicantDeclaredOtherIncomeAmount');
            objAutoJTFinance.EDRentAndUtilities = $F('txtJApplicantRentUtilities') == '' ? '0' : $F('txtJApplicantRentUtilities');
            objAutoJTFinance.EDFoodAndClothing = $F('txtJApplicantFoodClothing') == '' ? '0' : $F('txtJApplicantFoodClothing');
            objAutoJTFinance.EDEducation = $F('txtJApplicantEducation') == '' ? '0' : $F('txtJApplicantEducation');
            objAutoJTFinance.EDLoanRePayment = $F('txtJApplicantLoanRepayment') == '' ? '0' : $F('txtJApplicantLoanRepayment');
            objAutoJTFinance.EDOther = $F('txtJApplicantOthers') == '' ? '0' : $F('txtJApplicantOthers');

            objAutoJTFinance.RepaymentTo = $F('txtJApplicantRepaymentto');
            objAutoJTFinance.PaymentFor = $F('txtJApplicantRepaymentfor');


            // Reference
            var objAutoLoanReference = new Object();
            objAutoLoanReference.ReferenceId = $F('txtReferenceId');
            objAutoLoanReference.ReferenceName1 = $F('txtReferenceName1');
            objAutoLoanReference.ReferenceName2 = $F('txtReferenceName2');
            objAutoLoanReference.Relationship1 = $F('txtReferenceRelationship1');
            objAutoLoanReference.Relationship2 = $F('txtReferenceRelationship2');
            objAutoLoanReference.Occupation1 = $F('cmbReferenceOccupation1');
            objAutoLoanReference.Occupation2 = $F('cmbReferenceOccupation2');
            objAutoLoanReference.OrganizationName1 = $F('txtReferenceNameOfTheOrganization1');
            objAutoLoanReference.OrganizationName2 = $F('txtReferenceNameOfTheOrganization2');
            objAutoLoanReference.Designation1 = $F('txtReferenceDesignation1');
            objAutoLoanReference.Designation2 = $F('txtReferenceDesignation2');
            objAutoLoanReference.WorkAddress1 = $F('txtReferenceWorkAddress1');
            objAutoLoanReference.WorkAddress2 = $F('txtReferenceWorkAddress2');
            objAutoLoanReference.ResidenceAddress1 = $F('txtReferenceResidenceAddress1');
            objAutoLoanReference.ResidenceAddress2 = $F('txtReferenceResidenceAddress2');
            objAutoLoanReference.PhoneNumber1 = $F('txtReferencePhoneNumber1');
            objAutoLoanReference.PhoneNumber2 = $F('txtReferencePhoneNumber2');
            objAutoLoanReference.MobileNumber1 = $F('txtReferenceMobileNumber1');
            objAutoLoanReference.MobileNumber2 = $F('txtReferenceMobileNumber2');


            var objAutoLoanFacilityList = [];
            var objAutoLoanSecurityList = [];
            var tableFacility = $('tlbFacility');
            var rowCount = tableFacility.rows.length;

            for (var i = 1; i < rowCount - 1; i++) {
                var objAutoLoanFacility = new Object();
                if ($F('cmbFacilityScheduleFacilityType' + i) == '-1') {
                }
                else {


                    //                    var st = $('cmbFacilityScheduleFacilityType' + i).options[$('cmbFacilityScheduleFacilityType' + i).selectedIndex].text;

                    //objAutoLoanFacility.FacilityType = $F('cmbFacilityScheduleFacilityType' + i);

                    objAutoLoanFacility.FacilityId = $F('txtFacilityId' + i);
                    objAutoLoanFacility.FacilityTypeName = $('cmbFacilityScheduleFacilityType' + i).options[$('cmbFacilityScheduleFacilityType' + i).selectedIndex].text;
                    objAutoLoanFacility.FacilityType = $F('cmbFacilityScheduleFacilityType' + i);
                    objAutoLoanFacility.InterestRate = $F('txtFacilityScheduleInterestRate' + i) == '' ? '0' : $F('txtFacilityScheduleInterestRate' + i);
                    objAutoLoanFacility.PresentBalance = $F('txtFacilitySchedulePresentBalance' + i) == '' ? '0' : $F('txtFacilitySchedulePresentBalance' + i);
                    objAutoLoanFacility.PresentEMI = $F('txtFacilitySchedulePresentEMI' + i) == '' ? '0' : $F('txtFacilitySchedulePresentEMI' + i);
                    objAutoLoanFacility.PresentLimit = $F('txtFacilitySchedulePresentLimit' + i) == '' ? '0' : $F('txtFacilitySchedulePresentLimit' + i);
                    objAutoLoanFacility.ProposedLimit = $F('txtFacilityScheduleProposedLimit' + i) == '' ? '0' : $F('txtFacilityScheduleProposedLimit' + i);
                    objAutoLoanFacility.RepaymentAgrement = $F('txtFacilityScheduleRepaymentAgreement' + i) == '' ? '0' : $F('txtFacilityScheduleRepaymentAgreement' + i);

                    objAutoLoanFacilityList.push(objAutoLoanFacility);

                    var objAutoLoanSecurity = new Object();
                    objAutoLoanSecurity.SecurityId = $F('txtSecurityId' + i);
                    objAutoLoanSecurity.FacilityType = $F('cmbFacilityScheduleFacilityType' + i);
                    objAutoLoanSecurity.Status = $F('cmbStatusForSecurities' + i);
                    objAutoLoanSecurity.NatureOfSecurity = $F('txtSecurityScheduleNatureOfSecurity' + i);
                    objAutoLoanSecurity.NatureOfSecurityName = $('txtSecurityScheduleNatureOfSecurity' + i).options[$('txtSecurityScheduleNatureOfSecurity' + i).selectedIndex].text;
                    objAutoLoanSecurity.IssuingOffice = "";
                    objAutoLoanSecurity.FaceValue = $F('txtSecurityScheduleFaceValue' + i) == '' ? '0' : $F('txtSecurityScheduleFaceValue' + i);
                    objAutoLoanSecurity.XTVRate = $F('txtSecurityScheduleXTVRate' + i) == '' ? '0' : $F('txtSecurityScheduleXTVRate' + i);
                    objAutoLoanSecurity.IssueDate = Date.parseUK('01/01/0001');
                    objAutoLoanSecurity.InterestRate = $F('txtSecurityScheduleInterestRate' + i) == '' ? '0' : $F('txtSecurityScheduleInterestRate' + i);
                    objAutoLoanSecurityList.push(objAutoLoanSecurity);

                }

            }
            //Change By Washik Confirmed By Farshad 1/11/2012 Start-----------------------------------------------------------------
            //            var objAutoLoanSecurityList = [];
            //            var tableSecurity = $('tlbSecurity');
            //            var rowCount = tableSecurity.rows.length;

            //            for (var i = 1; i < rowCount; i++) {
            //                var objAutoLoanSecurity = new Object();
            //                if ($F('cmbfacilityTypeForSequrity' + i) == '-1') {
            //                }
            //                else {

            //                    objAutoLoanSecurity.SecurityId = $F('txtSecurityId' + i);

            //                    objAutoLoanSecurity.FacilityType = $F('cmbfacilityTypeForSequrity' + i);

            //                    objAutoLoanSecurity.NatureOfSecurity = $F('txtSecurityScheduleNatureOfSecurity' + i);
            //                    objAutoLoanSecurity.NatureOfSecurityName = $('txtSecurityScheduleNatureOfSecurity' + i).options[$('txtSecurityScheduleNatureOfSecurity' + i).selectedIndex].text;

            //                    objAutoLoanSecurity.IssuingOffice = $F('txtSecurityScheduleIssuingOffice' + i);

            //                    objAutoLoanSecurity.FaceValue = $F('txtSecurityScheduleFaceValue' + i) == '' ? '0' : $F('txtSecurityScheduleFaceValue' + i);
            //                    objAutoLoanSecurity.XTVRate = $F('txtSecurityScheduleXTVRate' + i) == '' ? '0' : $F('txtSecurityScheduleXTVRate' + i);

            //                    if ($F('txtSecurityScheduleIssueDate' + i) == '') {
            //                        objAutoLoanSecurity.IssueDate = Date.parseUK('01/01/0001');
            //                    }
            //                    else {
            //                        objAutoLoanSecurity.IssueDate = Date.parseUK($F('txtSecurityScheduleIssueDate' + i));
            //                    }

            //                    objAutoLoanSecurity.InterestRate = $F('txtSecurityScheduleInterestRate' + i) == '' ? '0' : $F('txtSecurityScheduleInterestRate' + i);

            //                    if (objAutoLoanSecurity.NatureOfSecurity == '') {
            //                    }
            //                    else {
            //                        objAutoLoanSecurityList.push(objAutoLoanSecurity);
            //                    }
            //                }
            //            }

            //Change End by Washik

            //            // PR bank statement
            //            var objPRBankStatement = new Object();
            //            objPRBankStatement.PrBankStatementId = '0';
            //            objPRBankStatement.LLID = $F('txtLLID');
            //            objPRBankStatement.BankId = $F('cmbPApplicantBankName');
            //            objPRBankStatement.BranchId = $F('cmbPApplicantBranchName');
            //            objPRBankStatement.AccountName = $F('txtPApplicantAccountName');
            //            objPRBankStatement.AccountNumber = $F('txtPApplicantAccountNumber');
            //            objPRBankStatement.AccountCategory = $F('cmbPApplicantAccountCategory');
            //            objPRBankStatement.AccountType = $F('cmbPApplicantAccountType');
            //            //            objPRBankStatement.Statement = $F('PBankStatement');

            //            // JT bank statement
            //            var objJtBankStatement = new Object();
            //            objJtBankStatement.JtBankStatementId = '0';
            //            objJtBankStatement.LLID = $F('txtLLID');
            //            objJtBankStatement.BankId = $F('cmbJApplicantBankName');
            //            objJtBankStatement.BranchId = $F('cmbJApplicantBranchName');
            //            objJtBankStatement.AccountName = $F('txtJApplicantAccountName');
            //            objJtBankStatement.AccountNumber = $F('txtJApplicantAccountNumber');
            //            objJtBankStatement.AccountCategory = $F('cmbJApplicantAccountCategory');
            //            objJtBankStatement.AccountType = $F('cmbJApplicantAccountType');
            //            //            objPRBankStatement.Statement = $F('PBankStatement');


            //            debugger;
            //            var PsaveStatement = LoanApplicationHealper.PSaveBankStatement();
            //            //var JsaveStatement = LoanApplicationHealper.JSaveBankStatement();

            var res = PlussAuto.LoanApplication.SaveLoanApplication(objAutoLoanMaster, objAutoPRApplicant, objAutoJTApplicant, objAutoPRProfession, objAutoJTProfession, objAutoLoanVehicle, objAutoPRAccount, objAutoJTAccount, objAutoPRAccountDetailList, objAutoJtAccountDetailList, objAutoPRFinance, objAutoJTFinance, objAutoLoanReference, objAutoLoanFacilityList, objAutoLoanSecurityList, objAutoApplicationInsurance, objAutoVendor); //, objPRBankStatement, objJtBankStatement);
            //            var res = PlussAuto.LoanApplication.SaveLoanApplication(objPRBankStatement);
            //            alert('objPRBankStatement');


            if (res.error) {
                alert(res.error.Message);
                return;
            }
            if (res.value == "Success") {
                alert("Loan Application Save successfully.");
                var url = "../../UI/AutoLoan/LoanApplication.aspx";
                window.location.href = url;
                //window.focus();
            }
            else {
                alert("Loan application Failed to Save");
            }

        }

    }

};

var LoanApplicationHealper = {
    hideShowDivBasedOn_jointApplicationDiv: function() {



        //        $('JApplicantdivHeader').style.display = ($('JApplicantdivHeader').style.display == 'none') ? 'block' : 'none';
        //        //        $('cmbJApplicantProfession').setValue($F('cmbPApplicantProfession'));
        //        $('JApplicantControldiv').style.display = ($('JApplicantControldiv').style.display == 'none') ? 'block' : 'none';
        //        $('JProfessionHeaderDiv').style.display = ($('JProfessionHeaderDiv').style.display == 'none') ? 'block' : 'none';
        //        $('JProfessionControlDiv').style.display = ($('JProfessionControlDiv').style.display == 'none') ? 'block' : 'none';
        //        $('JAccountHeaderDiv').style.display = ($('JAccountHeaderDiv').style.display == 'none') ? 'block' : 'none';
        //        $('JAccountControlDiv').style.display = ($('JAccountControlDiv').style.display == 'none') ? 'block' : 'none';
        //        $('Account_JointApplicantOtherBankAccount').style.display = ($('Account_JointApplicantOtherBankAccount').style.display == 'none') ? 'block' : 'none';
        //        $('JFinanceHeaderDiv').style.display = ($('JFinanceHeaderDiv').style.display == 'none') ? 'block' : 'none';
        //        $('JFinanceControlDiv').style.display = ($('JFinanceControlDiv').style.display == 'none') ? 'block' : 'none';
        //        $('JBankStatementDiv').style.display = ($('JBankStatementDiv').style.display == 'none') ? 'block' : 'none';



        var isJoint = $F("cmbJointApplication");

        if (isJoint == '1') {
            document.getElementById('JApplicantdivHeader').style.display = 'block';
            //        $('cmbJApplicantProfession').setValue($F('cmbPApplicantProfession'));
            document.getElementById('JApplicantControldiv').style.display = 'block';
            document.getElementById('JProfessionHeaderDiv').style.display = 'block';
            document.getElementById('JProfessionControlDiv').style.display = 'block';
            document.getElementById('JAccountHeaderDiv').style.display = 'block';
            document.getElementById('JAccountControlDiv').style.display = 'block';
            document.getElementById('Account_JointApplicantOtherBankAccount').style.display = 'block';
            document.getElementById('JFinanceHeaderDiv').style.display = 'block';
            document.getElementById('JFinanceControlDiv').style.display = 'block';
            //            document.getElementById('JBankStatementDiv').style.display = 'block';
        }
        else {
            document.getElementById('JApplicantdivHeader').style.display = 'none';
            //        $('cmbJApplicantProfession').setValue($F('cmbPApplicantProfession'));
            document.getElementById('JApplicantControldiv').style.display = 'none';
            document.getElementById('JProfessionHeaderDiv').style.display = 'none';
            document.getElementById('JProfessionControlDiv').style.display = 'none';
            document.getElementById('JAccountHeaderDiv').style.display = 'none';
            document.getElementById('JAccountControlDiv').style.display = 'none';

            document.getElementById('JFinanceHeaderDiv').style.display = 'none';
            document.getElementById('JFinanceControlDiv').style.display = 'none';
            //            document.getElementById('JBankStatementDiv').style.display = 'none';
            document.getElementById('Account_JointApplicantOtherBankAccount').style.display = 'none';
        }











    },
    populateFacilityCombo: function(res) {
        var link = "<option value=\"-1\">Select a Facility</option>";


        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
        }
        $("cmbFacilityScheduleFacilityType1").update(link);
        $("cmbFacilityScheduleFacilityType2").update(link);
        $("cmbFacilityScheduleFacilityType3").update(link);
        $("cmbFacilityScheduleFacilityType4").update(link);
        $("cmbFacilityScheduleFacilityType5").update(link);

        //        $("cmbfacilityTypeForSequrity1").update(link);
        //        $("cmbfacilityTypeForSequrity2").update(link);
        //        $("cmbfacilityTypeForSequrity3").update(link);
        //        $("cmbfacilityTypeForSequrity4").update(link);
        //        $("cmbfacilityTypeForSequrity5").update(link);

        Page.facilityArray = res.value;

    },
    populateBranchCombo: function(res, elementID) {

        //primary applicant start

        var extraOption = document.createElement("option");
        extraOption.text = 'Select Branch';
        extraOption.value = '-1';

        if (elementID == 'cmbPApplicantBank1') {
            document.getElementById('cmbPApplicantBranch1').options.length = 0;
            document.getElementById('cmbPApplicantBranch1').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch1').options.add(opt);

            }
        }


        if (elementID == 'cmbPApplicantBank2') {
            document.getElementById('cmbPApplicantBranch2').options.length = 0;
            document.getElementById('cmbPApplicantBranch2').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch2').options.add(opt);

            }
        }

        if (elementID == 'cmbPApplicantBank3') {
            document.getElementById('cmbPApplicantBranch3').options.length = 0;
            document.getElementById('cmbPApplicantBranch3').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch3').options.add(opt);

            }
        }

        if (elementID == 'cmbPApplicantBank4') {
            document.getElementById('cmbPApplicantBranch4').options.length = 0;
            document.getElementById('cmbPApplicantBranch4').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch4').options.add(opt);

            }
        }

        if (elementID == 'cmbPApplicantBank5') {
            document.getElementById('cmbPApplicantBranch5').options.length = 0;
            document.getElementById('cmbPApplicantBranch5').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch5').options.add(opt);

            }
        }
        // Primary Applicant End

        //Joint applicant start.

        if (elementID == 'cmbJApplicantBank1') {
            document.getElementById('cmbJApplicantBranch1').options.length = 0;
            document.getElementById('cmbJApplicantBranch1').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch1').options.add(opt);

            }
        }


        if (elementID == 'cmbJApplicantBank2') {
            document.getElementById('cmbJApplicantBranch2').options.length = 0;
            document.getElementById('cmbJApplicantBranch2').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch2').options.add(opt);

            }
        }

        if (elementID == 'cmbJApplicantBank3') {
            document.getElementById('cmbJApplicantBranch3').options.length = 0;
            document.getElementById('cmbJApplicantBranch3').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch3').options.add(opt);

            }
        }

        if (elementID == 'cmbJApplicantBank4') {
            document.getElementById('cmbJApplicantBranch4').options.length = 0;
            document.getElementById('cmbJApplicantBranch4').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch4').options.add(opt);

            }
        }

        if (elementID == 'cmbJApplicantBank5') {
            document.getElementById('cmbJApplicantBranch5').options.length = 0;
            document.getElementById('cmbJApplicantBranch5').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch5').options.add(opt);

            }
        }

        //        if (elementID == 'cmbPApplicantBankName') {
        //            document.getElementById('cmbPApplicantBranchName').options.length = 0;
        //            document.getElementById('cmbPApplicantBranchName').options.add(extraOption);
        //            for (var i = 0; i < res.value.length; i++) {
        //                var opt = document.createElement("option");
        //                opt.text = res.value[i].BRAN_NAME;
        //                opt.value = res.value[i].BRAN_ID;
        //                document.getElementById('cmbPApplicantBranchName').options.add(opt);

        //            }
        //        }

        //        if (elementID == 'cmbJApplicantBankName') {
        //            document.getElementById('cmbJApplicantBranchName').options.length = 0;
        //            document.getElementById('cmbJApplicantBranchName').options.add(extraOption);
        //            for (var i = 0; i < res.value.length; i++) {
        //                var opt = document.createElement("option");
        //                opt.text = res.value[i].BRAN_NAME;
        //                opt.value = res.value[i].BRAN_ID;
        //                document.getElementById('cmbJApplicantBranchName').options.add(opt);

        //            }
        //        }


    },

    populateBankCombo: function(res) {


        var extraOption = document.createElement("option");
        extraOption.text = 'Select Bank';
        extraOption.value = '-1';

        document.getElementById('cmbPApplicantBank1').options.length = 0;
        document.getElementById('cmbPApplicantBank1').options.add(extraOption);

        document.getElementById('cmbPApplicantBank2').options.length = 0;
        var extraOption1 = document.createElement("option");
        extraOption1.text = 'Select Bank';
        extraOption1.value = '-1';

        document.getElementById('cmbPApplicantBank2').options.add(extraOption1);

        document.getElementById('cmbPApplicantBank3').options.length = 0;
        var extraOption2 = document.createElement("option");
        extraOption2.text = 'Select Bank';
        extraOption2.value = '-1';
        document.getElementById('cmbPApplicantBank3').options.add(extraOption2);

        document.getElementById('cmbPApplicantBank4').options.length = 0;
        var extraOption3 = document.createElement("option");
        extraOption3.text = 'Select Bank';
        extraOption3.value = '-1';
        document.getElementById('cmbPApplicantBank4').options.add(extraOption3);

        document.getElementById('cmbPApplicantBank5').options.length = 0;
        var extraOption4 = document.createElement("option");
        extraOption4.text = 'Select Bank';
        extraOption4.value = '-1';
        document.getElementById('cmbPApplicantBank5').options.add(extraOption4);


        //        document.getElementById('cmbPApplicantBankName').options.length = 0;
        //        var extraOption5 = document.createElement("option");
        //        extraOption5.text = 'Select Bank';
        //        extraOption5.value = '-1';
        //        document.getElementById('cmbPApplicantBankName').options.add(extraOption5);

        //        document.getElementById('cmbJApplicantBankName').options.length = 0;
        //        var extraOption6 = document.createElement("option");
        //        extraOption6.text = 'Select Bank';
        //        extraOption6.value = '-1';
        //        document.getElementById('cmbJApplicantBankName').options.add(extraOption6);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].BankName;
            opt.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank1').options.add(opt);

            var opt1 = document.createElement("option");
            opt1.text = res.value[i].BankName;
            opt1.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank2').options.add(opt1);

            var opt2 = document.createElement("option");
            opt2.text = res.value[i].BankName;
            opt2.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank3').options.add(opt2);

            var opt3 = document.createElement("option");
            opt3.text = res.value[i].BankName;
            opt3.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank4').options.add(opt3);

            var opt4 = document.createElement("option");
            opt4.text = res.value[i].BankName;
            opt4.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank5').options.add(opt4);

            var opt5 = document.createElement("option");
            opt5.text = res.value[i].BankName;
            opt5.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank1').options.add(opt5);

            var opt6 = document.createElement("option");
            opt6.text = res.value[i].BankName;
            opt6.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank2').options.add(opt6);

            var opt7 = document.createElement("option");
            opt7.text = res.value[i].BankName;
            opt7.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank3').options.add(opt7);

            var opt8 = document.createElement("option");
            opt8.text = res.value[i].BankName;
            opt8.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank4').options.add(opt8);

            var opt9 = document.createElement("option");
            opt9.text = res.value[i].BankName;
            opt9.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank5').options.add(opt9);

            //            var opt10 = document.createElement("option");
            //            opt10.text = res.value[i].BankName;
            //            opt10.value = res.value[i].BankID;
            //            document.getElementById('cmbPApplicantBankName').options.add(opt10);

            //            var opt11 = document.createElement("option");
            //            opt11.text = res.value[i].BankName;
            //            opt11.value = res.value[i].BankID;
            //            document.getElementById('cmbJApplicantBankName').options.add(opt11);




        }
    },


    PSaveBankStatement: function() {
        try {
            var spreadsheet = document.getElementById("PBankStatement");
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            var llid = $F('txtLLID');
            if (XMLHttpRequestObject) {

                XMLHttpRequestObject.open("POST", "LoanApplication.aspx?operation=save&llid=" + llid + "&applicantType=primary");
                XMLHttpRequestObject.onreadystatechange = function() {
                    if (XMLHttpRequestObject.readyState == 4 &&
                  XMLHttpRequestObject.status == 200) {
                        document.getElementById('PBankStatement').XMLData = spreadsheet.XMLData;

                        //alert(XMLHttpRequestObject.responseText)               
                    }
                }
                XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                XMLHttpRequestObject.setRequestHeader("Content-length", spreadsheet.XMLData.length);
                XMLHttpRequestObject.send(spreadsheet.XMLData);

                LoanApplicationHealper.JSaveBankStatement();
            }
        }
        catch (err)
          { }

    },


    JSaveBankStatement: function() {
        try {
            var spreadsheet = document.getElementById("JBankStatement");
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            var llid = $F('txtLLID');
            if (XMLHttpRequestObject) {

                XMLHttpRequestObject.open("POST", "LoanApplication.aspx?operation=save&llid=" + llid + "&applicantType=joint");
                XMLHttpRequestObject.onreadystatechange = function() {
                    if (XMLHttpRequestObject.readyState == 4 &&
                  XMLHttpRequestObject.status == 200) {
                        document.getElementById('JBankStatement').XMLData = spreadsheet.XMLData;

                        //alert(XMLHttpRequestObject.responseText)               
                    }
                }
                XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                XMLHttpRequestObject.setRequestHeader("Content-length", spreadsheet.XMLData.length);
                XMLHttpRequestObject.send(spreadsheet.XMLData);
            }
        }
        catch (err)
          { }
    },


    LoadTemplate: function() {
        LoanApplicationHealper.PRLoadTemplate();
        //        LoanApplicationHealper.JTLoadTemplate();
    },




    PRLoadTemplate: function() {
        try {

            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            var spreadsheet = document.getElementById("PBankStatement");

            var llid = $F('txtLLID');
            var BankStatement = PlussAuto.LoanApplication.GetBankStatement(llid);

            if (XMLHttpRequestObject) {
                var llid = $F('txtLLID');
                XMLHttpRequestObject.open("POST", "LoanApplication.aspx?operation=loadTemplate&llid=" + llid + "&applicantType=primary");
                XMLHttpRequestObject.onreadystatechange = function() {
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                        spreadsheet.XMLData = XMLHttpRequestObject.responseText;
                        //                        spreadsheet.XMLData = BankStatement.value.PRStatement;
                        //alert(XMLHttpRequestObject.readyState );
                    }
                }
                XMLHttpRequestObject.send(null);
                //                document.getElementById('<%= lblMessage.ClientID %>').innerText = XMLHttpRequestObject.responseText;
                //               debugger;
                //                LoanApplicationHealper.JTLoadTemplate();

            }
        }
        catch (err)
          { }

    },


    JTLoadTemplate: function() {

        try {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
            var spreadsheet = document.getElementById("JBankStatement");
            if (XMLHttpRequestObject) {
                //                var documenttype = document.getElementById("Select1")
                var llid = $F('txtLLID');
                XMLHttpRequestObject.open("POST", "LoanApplication.aspx?operation=loadTemplate&llid=" + llid + "&applicantType=joint");
                XMLHttpRequestObject.onreadystatechange = function() {
                    if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                        spreadsheet.XMLData = XMLHttpRequestObject.responseText;
                        //alert(XMLHttpRequestObject.readyState );
                    }
                }
                XMLHttpRequestObject.send(null);
                //                document.getElementById('<%= lblMessage.ClientID %>').innerText = XMLHttpRequestObject.responseText;
            }
        }
        catch (err)
          { }

    },






    validetLoanApplicationForm: function() {
        var message = false;
        if ($F('txtLLID') == "") {
            alert("Please input Loan locator Id for loan application");
            //$('txtLLID').focus();
            return message;
        }
        if ($F('cmbSource') == "-1") {
            alert("Please select a source.");
            //$('cmbSource').focus();
            return message;
        }
        if ($F('txtAppliedAmount') == "") {
            alert("Please input Applied Amount.");
            //$('txtAppliedAmount').focus();
            return message;
        }

        if ($F('txtAppliedDate') == "") {
            alert("Please input Applied Date.");
            //$('txtAppliedDate').focus();
            return message;
        }
        if ($F('cmbAskingTenor') == "0") {
            alert("Please select asking tenor.");
            //$('cmbAskingTenor').focus();
            return message;
        }
        if ($F('txtAskingRate') == "") {
            alert("Please input asking interest rate.");
            //$('txtAskingRate').focus();
            return message;
        }

        if ($F('txtAskingRate') == "") {
            alert("Please input asking interest rate.");
            //$('txtAskingRate').focus();
            return message;
        }

        var vendorId = $F("cmbVendor");
        var vendoreName = $F("txtVendorName");
        if (vendorId != "-1" || vendoreName != "") {
            if ($F('cmbManufacturer') == "-1") {
                alert("Please Select a vehicle Manufactara.");
                return message;
            }

            var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;
            if (manufacturName != "Other") {
                if ($F('cmbModel') == "-1") {
                    alert("Please Select a vehicle Model.");
                    return message;
                }
            }
            else {
                if ($F('txtModelName') == "") {
                    alert("Please Insert a vehicle Model.");
                    return message;
                }
            }

            if ($F('cmdVehicleStatus') == "-1") {
                alert("Please Select a vehicle Status.");
                return message;
            }
            var manufacturingYear = $F('txtManufacturingyear');
            if (!util.isDigit(manufacturingYear)) {
                alert("Manufacturing year must be a number field");
                return message;
            }
        }

        //        if ($F('txtReceiveDate') == "") {
        //            alert("Please input Receive Date.");
        //            //$('txtReceiveDate').focus();
        //            return message;
        //        }
        //        var appliedAmount = parseFloat($F('txtPApplicantDeclaredPrimaryIncomeAmount'));
        //        if (appliedAmount < 20000 || $F('txtPApplicantDeclaredPrimaryIncomeAmount') == "") {
        //            alert("Declare Income cannot less then 20,000.00 tk. for Primary Applicant");
        //            //$('txtPApplicantDeclaredPrimaryIncomeAmount').focus();
        //            return message;
        //        }
        //        if ($F('cmbManufacturer') == "-1") {
        //            alert("Please Select a vehicle Manufactara.");
        //            return message;
        //        }
        //        debugger;
        if ($F('cmbJointApplication') == "1") {

            if ($F('txtPApplicantName') == "") {
                alert("Please input primary applicant name.");
                //$('txtPApplicantName').focus();
                return message;
            }
            if ($F('txtJApplicantName') == "") {
                alert("Please input joint applicant name.");
                //$('txtJApplicantName').focus();
                return message;
            }
            if ($F('txtPApplicantTINNumber') == "") {
                alert("Please input primary applicant TIN number.");
                //$('txtPApplicantTINNumber').focus();
                return message;
            }
            if ($F('txtJApplicantTINNumber') == "") {
                alert("Please input joint applicant TIN number.");
                //$('txtJApplicantTINNumber').focus();
                return message;
            }
            if ($F('txtPApplicantDateOfBirth') == "") {
                alert("Please input primary applicant date of birth.");
                //$('txtPApplicantDateOfBirth').focus();
                return message;
            }
            if ($F('txtJApplicantDateOfBirth') == "") {
                alert("Please input joint applicant date of birth.");
                //$('txtJApplicantDateOfBirth').focus();
                return message;
            }
            if ($F('cmbPApplicantHighestEducationLevel') == "-1") {
                alert("Please Select primary applicant highest education level.");
                //$('cmbPApplicantHighestEducationLevel').focus();
                return message;
            }
            if ($F('cmbJApplicantHighestEducationLevel') == "-1") {
                alert("Please Select joint applicant highest education level.");
                //$('cmbJApplicantHighestEducationLevel').focus();
                return message;
            }
            if ($F('cmbPApplicantPrimaryProfession') == "-1") {
                alert("Please Select primary applicant profession.");
                //$('cmbPApplicantProfession').focus();
                return message;
            }
            if ($F('cmbJApplicantPrimaryProfession') == "-1") {
                alert("Please Select joint applicant profession.");
                //$('cmbJApplicantPrimaryProfession').focus();
                return message;
            }
            if ($F('txtPApplicantMonthInCurrentProfession') == "") {
                alert("Please input primary applicant month in current profession.");
                //$('txtPApplicantMonthInCurrentProfession').focus();
                return message;
            }
            if ($F('txtJApplicantMonthInCurrentProfession') == "") {
                alert("Please input joint applicant month in current profession.");
                //$('txtJApplicantMonthInCurrentProfession').focus();
                return message;
            }
            if ($F('txtAppliedAmount') == "") {
                alert("Please input Applied Amount.");
                //$('txtAppliedAmount').focus();
                return message;
            }
            //            var appliedAmountForJoint = parseFloat($F('txtJApplicantDeclaredPrimaryIncomeAmount'));
            //            if (appliedAmountForJoint < 10000 || $F('txtJApplicantDeclaredPrimaryIncomeAmount') == "") {
            //                alert("Declare Income cannot less then 10,000.00 tk. for Joint Applicant");
            //                //$('txtJApplicantDeclaredPrimaryIncomeAmount').focus();
            //                return message;
            //            }


        }
        else {

            if ($F('txtPApplicantName') == "") {
                alert("Please input primary applicant name.");
                //$('txtPApplicantName').focus();
                return message;
            }
            if ($F('txtPApplicantTINNumber') == "") {
                alert("Please input primary applicant TIN number.");
                //$('txtPApplicantTINNumber').focus();
                return message;
            }
            if ($F('txtPApplicantDateOfBirth') == "") {
                alert("Please input primary applicant date of birth.");
                //$('txtPApplicantDateOfBirth').focus();
                return message;
            }
            if ($F('cmbPApplicantHighestEducationLevel') == "-1") {
                alert("Please Select primary applicant highest education level.");
                //$('cmbPApplicantHighestEducationLevel').focus();
                return message;
            }
            if ($F('cmbPApplicantPrimaryProfession') == "-1") {
                alert("Please Select primary applicant profession.");
                //$('cmbPApplicantPrimaryProfession').focus();
                return message;
            }
            if ($F('txtPApplicantMonthInCurrentProfession') == "") {
                alert("Please input primary applicant month in current profession.");
                //$('txtPApplicantMonthInCurrentProfession').focus();
                return message;
            }

        }


        message = true;
        return message;
    },

    checkInputIsNumber: function(elementID) {
        if (!util.isFloat($F(elementID))) {
            alert("Required Number filed.");
            $(elementID).setValue("0");
            //$(elementID).focus();
            return;
        }

    },
    changePrimaryEmailValidation: function() {
        var prEmail = $F("txtPApplicantEmail");
        if (!util.isEmail(prEmail)) {
            alert("Invalid Email Address");
            $('txtPApplicantEmail').setValue("");
            //$('txtPApplicantEmail').focus();
            return
        }
    },
    checkIntForCurrentPrProfession: function() {
        var prProfession = $F("txtPApplicantMonthInCurrentProfession");
        if (!util.isDigit(prProfession)) {
            alert("Required number field");
            $('txtPApplicantMonthInCurrentProfession').setValue("");
            //$('txtJApplicantEmail').focus();
            return
        }
    },
    checkIntForCurrentJtProfession: function() {
        var jtProfession = $F("txtJApplicantMonthInCurrentProfession");
        if (!util.isDigit(jtProfession)) {
            alert("Required number field");
            $('txtJApplicantMonthInCurrentProfession').setValue("");
            //$('txtJApplicantEmail').focus();
            return
        }
    },

    GetLLIDOnKeyPress: function(e) {
        if (event.keyCode == 13) {
            LoanApplicationManager.GetInfoByLLID();
        }
    },
    changeGeneralInsurence: function() {
        var generalInsurence = $F("cmbGeneralInsurance");
        if (generalInsurence == "-1") {
            $('cmbInsuranceFinancewithLoan').setValue("0");
            $('cmbInsuranceFinancewithLoan').disabled = 'false';
        }
        else {
            $('cmbInsuranceFinancewithLoan').disabled = '';
        }
    },
    changeLifeInsurence: function() {
        var lifeInsurence = $F("cmbLifeInsurance");
        if (lifeInsurence == "-1") {
            $('cmbInsuranceFinancewithLoanLife').setValue("0");
            $('cmbInsuranceFinancewithLoanLife').disabled = 'false';
        }
        else {
            $('cmbInsuranceFinancewithLoanLife').disabled = '';
        }
    },
    changeJointEmailValidation: function() {
        var jtEmail = $F("txtJApplicantEmail");
        if (!util.isEmail(jtEmail)) {
            alert("Invalid Email Address");
            $('txtJApplicantEmail').setValue("");
            //$('txtJApplicantEmail').focus();
            return
        }
    } //,
    //    changeDateofBirth: function() {
    //        debugger;
    //        var dateofBirth = $F("changeDateofBirth");
    //        var dayDiff = util.dateDiff(Page.serverDate, dateofBirth);
    //        for (var i = 0; i < res.value.length; i++) {
    //            var opt = document.createElement("option");
    //            var opt1 = document.createElement("option");
    //            var insType = res.value[i].InsType;

    //            if (insType == '1' || insType == '3') {
    //                opt.text = res.value[i].CompanyName;
    //                opt.value = res.value[i].InsCompanyId;

    //                document.getElementById('cmbGeneralInsurance').options.add(opt);
    //            }
    //            if (insType == '2' || insType == '3') {
    //                opt1.text = res.value[i].CompanyName;
    //                opt1.value = res.value[i].InsCompanyId;

    //                document.getElementById('cmbLifeInsurance').options.add(opt1);
    //            }
    //        }
    //    }

};

Event.onReady(Page.init);