﻿var salariedHtmlHelper = {
    //Primary Applicant Content For Salaried Tab
    prhtmlForSalaried: function() {
        var prhtml = "";
        var prhtmlLeft = "<div class='divLeftBig'  style='width :45%;padding: 0px; border: none;'>" +
"<div class = 'groupContainer' style='margin: 0px; width: 100%; border-radius: 15px; padding-left: 5px; padding-right:5px;'>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Salary Mode:</label></span>" +
"<div class='divLeft divSizeWidth70_per '>" +
"<select id='cmbSalaryModeForPr' class=\"comboSizeH_24 comboSizeWidth101_per comboSizeActive\" onchange=\"salariedHtmlHelper.changeSalarymode('Pr')\">" +
"<option value='-1'>Select a Salary Mode</option>" +
"<option value='1'>CASH SALARIED</option>" +
"<option value='2'>SALARY DEBIT</option>" +
"<option value='3'>CHECK SALARIED</option>" +
"</select>" +
"</div>" +
"</div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Employer:</label></span>" +
"<div class='divLeft divSizeWidth50_per'>" +
"<select id=\"cmbEmployerForSalariedForPr\" class=\"comboSizeH_24 comboSizeWidth101_per comboSizeActive\" onchange=\"salariedHtmlHelper.changeEmployer('Pr')\">" +
"</select>" +
"</div>" +
"<div class='divRight divSizeWidth16_per'>" +
"<input id='txtEmployerCodeForPr' class='txtBoxSize_H22' style=\"width:71px;\" type='text' disabled='disabled' />" +
"</div>" +
"</div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Employer Category:</label></span>" +
"<div class='divLeft divSizeWidth70_per'>" +
"<select id='cmbEmployerCategoryForPrSalaried' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'>" +
"<option value='-1'>Select a Salary Code</option>" +
"<option value='A'>Cat A</option>" +
"<option value='B'>Cat B</option>" +
"<option value='C'>No Cat</option>" +
"</select>" +
"</div>" +
"</div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Income Assessment:</label></span>" +
"<div class='divLeft divSizeWidth50_per'>" +
"<select id='cmbInComeAssementForSalaryForPr' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'>" +
"<option value='-1'>Select a Salary Mode</option><option value='1'>Salaried in Cash</option><option value='2'>Salaried</option></select></div>" +
"<div class='divMiddile divSizeWidth16_per'>" +
"<select id='cmbInComeAssementForPr' style='width: 73px;' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'>" +
"<option value='-1'>Select a Salary Mode</option><option value='SC'>SC</option><option value='SL'>SL</option></select></div></div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Gross Salary:</label></span>" +
"<div class='divLeft divSizeWidth70_per'>" +
"<input id=\"txtGrossSalaryForPrSalaried\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeGrossSalaray(this.id,'Pr')\"/></div></div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Net Salary:</label></span>" +
"<div class='divLeft divSizeWidth70_per'>" +
"<input id=\"txtNetSalaryForPr\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeNetSalaray(this.id,'Pr')\" /></div></div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Variable Salary:</label></span>" +
"<div class='divLeft divSizeWidth70_per'>" +
"<select id=\"cmbVeriableSalaryForPr\" class=\"comboSizeH_24 comboSizeWidth101_per comboSizeActive\" onchange=\"salariedHtmlHelper.changeVeriavleSalary('Pr')\">" +
"<option value='-1'>Select from List</option><option value='1'>Yes</option><option value='0'>No</option></select>   </div></div>" +
"<div class='div3in1' id='divVeriableSalaryForPr' style='display:none;'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>VARIABLE SALARY:</label></span>" +
"<div class='divLeft divSizeWidth30_per'>" +
"<input id=\"txtVeriableSalaryPerForPrSalaried\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeVaiableSalaryPer(this.id,'Pr')\" /></div>" +
"<div class='divLeft' style='width: 7%;*width: 7%;_width: 7%; border: none; text-align: center;'>" +
"<span>TK.</span></div><div class='divLeft divSizeWidth28_per'>" +
"<input id='txtTotalVeriableSalaryForPrSalaried' style=\"width:118px;\" class='txtBoxSize_H22' type='text' disabled='disabled' /></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Car Allowance:</label></span>" +
"<div class='divLeft divSizeWidth28_per'>" +
"<select id=\"cmbCarAllowence\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"salariedHtmlHelper.changeCarAllowence('Pr')\">" +
"<option value='-1'>Select from List</option><option value='1'>Yes</option><option value='0'>No</option></select></div>" +
"<div class='divLeft' style='width: 7%;*width: 7%;_width: 7%; border: none; text-align: center;'><span>TK.</span></div>" +
"<div class='divLeft divSizeWidth28_per'>" +
"<input id=\"txtCarAllowenceAmount\" class=\"txtBoxSize_H22 comboSizeActive\" style=\"width:125px;\" type=\"text\" onchange=\"salariedHtmlHelper.changeCarAllowance(this.id,'Pr')\" /></div></div>" +
"<div class='div3in1' id='divCarAllowenceForPrSalaried' style='display:none;'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 90px;*width: 90px;_width: 90px;'>Car Allowance:</label></span>" +
"<div class='divLeft divSizeWidth30_per' style='width: 22%;_width: 22%;*width: 22%;'>" +
"<input id=\"txtCarAllowenceForPr\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeCarAllowance(this.id,'Pr')\"/></div>" +
"<div class='divLeft' style='width: 3%;*width: 3%;_width: 3%; border: none; text-align: center;'><span>%</span></div>" +
"<div class='divLeft divSizeWidth30_per' ><input id='txtCarAllowenceTotalAmountPr' style=\"width:164px;\" class='txtBoxSize_H22' type='text' disabled='disabled' /></div></div>" +
"<div style='clear:both;'></div><div class='div3in1' id='OwnHouseBenifitDivForPrSalaried' style='display:none;'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Own House Benefit:</label></span>" +
"<div class='divLeft divSizeWidth70_per'>" +
"<select id=\"changeOwnHouseAmount\" class=\"comboSizeH_24 comboSizeWidth101_per comboSizeActive\" onchange=\"salariedHtmlHelper.changeOwnHouseAmount('Pr')\">" +
"<option value='-1'>Select from List</option><option value='1'>Yes</option><option value='0'>No</option></select></div></div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Total Income:</label></span>" +
"<div class='divLeft divSizeWidth70_per'><input id='txtTotalInComeForPrSalaried' class='txtBoxSize_H22' type='text' disabled='disabled'/></div></div>" +
"<div class='clear'></div></div></div>";

        var prhtmlRight = "<div class='divRightBig'  style='width :45%'>" +
"<div class='fieldDiv'><label class='lbl'>Remarks:</label></div>" +
"<div class='fieldDiv'><textarea id='txtRemarksForPrSalaried' class='txtFieldBig comboSizeActive' cols='1' rows='3' style='height: 84px;_height: 84px;*height: 84px; width :99%;_width :99%;*width :99%;'></textarea> </div></div>" +
"<div class='divRightBig'  style='width :45%; display:none;' id='divVariableSalaryCalculationForPr'>" +
"<div class='fieldDiv'><span>VARIABLE SALARY CALCULATION</span></div>" +
"<div style='text-align:left; font-size:medium;font-weight:bold;padding: 2px;'>" +
"<div style='border:none;'>" +
"<table class='tableStyle' id='tblVariableSalaryCalculationForPrSalaried' ><tr class='theader'><td >MONTH</td><td >AMOUNT (BDT)</td></tr><tr>" +
"<td>" +
"<select id='cmbMonth1ForPrSalary' class='comboSizeH_24 comboSizeWidth101_per comboSizeActive'>" +
"<option value = '1'>JAN</option><option value = '2'>FEB</option><option value = '3'>MAR</option><option value = '4'>APR</option><option value = '5'>MAY</option>" +
"<option value = '6'>JUN</option><option value = '7'>JUL</option><option value = '8'>AUG</option><option value = '9'>SEP</option><option value = '10'>OCT</option>" +
"<option value = '11'>NOV</option><option value = '12'>DEC</option></select></td>" +
"<td><input id=\"txtVaiableAmountForMonth1ForPrSalary\" class=\"comboSizeActive\" type =\"text\" onchange=\"salariedHtmlHelper.changeVariableAmount(this.id,'Pr')\"/></td></tr><tr><td>" +
"<select id='cmbMonth2ForPrSalary' class='comboSizeH_24 comboSizeWidth101_per comboSizeActive'>" +
"<option value = '1'>JAN</option><option value = '2'>FEB</option><option value = '3'>MAR</option><option value = '4'>APR</option><option value = '5'>MAY</option>" +
"<option value = '6'>JUN</option><option value = '7'>JUL</option><option value = '8'>AUG</option><option value = '9'>SEP</option><option value = '10'>OCT</option>" +
"<option value = '11'>NOV</option><option value = '12'>DEC</option></select></td>" +
"<td><input id=\"txtVaiableAmountForMonth2ForPrSalary\" class=\"comboSizeActive\" type =\"text\" onchange=\"salariedHtmlHelper.changeVariableAmount(this.id,'Pr')\"/></td></tr><tr><td>" +
"<select id='cmbMonth3ForPrSalary' class='comboSizeH_24 comboSizeWidth101_per comboSizeActive'><option value = '1'>JAN</option><option value = '2'>FEB</option>" +
"<option value = '3'>MAR</option><option value = '4'>APR</option><option value = '5'>MAY</option><option value = '6'>JUN</option><option value = '7'>JUL</option>" +
"<option value = '8'>AUG</option><option value = '9'>SEP</option><option value = '10'>OCT</option><option value = '11'>NOV</option><option value = '12'>DEC</option></select></td>" +
"<td><input id=\"txtVaiableAmountForMonth3ForPrSalary\" class=\"comboSizeActive\" type =\"text\" onchange=\"salariedHtmlHelper.changeVariableAmount(this.id,'Pr')\"/></td></tr></table></div>" +
"<input type='hidden' id='txtAverageBalanceForPrSalaried' /></div></div>";
        prhtml = prhtmlLeft + prhtmlRight;
        return prhtml;
    },

    //Joint Applicant Content For Salaried Tab
    jthtmlForSalaried: function() {
        var jtHtml = "";
        var jtHtmlLeft = "<div class='divLeftBig'  style='width :45%;padding: 0px; border: none;'>" +
"<div class = 'groupContainer' style='margin: 0px; width: 100%; border-radius: 15px; padding-left: 5px; padding-right:5px;'>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Salary Mode:</label></span>" +
"<div class='divLeft divSizeWidth70_per '>" +
"<select id=\"cmbSalaryModeForJt\" class=\"comboSizeH_24 comboSizeWidth101_per comboSizeActive\" onchange=\"salariedHtmlHelper.changeSalarymode('Jt')\">" +
"<option value='-1'>Select a Salary Mode</option><option value='1'>CASH SALARIED</option><option value='2'>SALARY DEBIT</option><option value='3'>CHECK SALARIED</option></select></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Employer:</label></span>" +
"<div class='divLeft divSizeWidth50_per'>" +
"<select id=\"cmbEmployerForSalariedForJt\" class=\"comboSizeH_24 comboSizeWidth101_per comboSizeActive\" onchange=\"salariedHtmlHelper.changeEmployer('Jt')\"></select></div>" +
"<div class='divRight divSizeWidth16_per'><input id='txtEmployerCodeForJt' class='txtBoxSize_H22' style=\"width:71px;\" type='text' disabled='disabled' /></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Employer Category:</label></span>" +
"<div class='divLeft divSizeWidth70_per'><select id='cmbEmployerCategoryForJtSalaried' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'>" +
"<option value='-1'>Select a Salary Code</option><option value='A'>Cat A</option><option value='B'>Cat B</option><option value='C'>No Cat</option></select></div></div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Income Assessment:</label></span>" +
"<div class='divLeft divSizeWidth50_per'><select id='cmbInComeAssementForSalaryForJt' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'>" +
"<option value='-1'>Select a Salary Mode</option><option value='1'>Salaried in Cash</option><option value='2'>Salaried</option></select></div>" +
"<div class='divMiddile divSizeWidth16_per'><select id='cmbInComeAssementForJt' style='width: 73px;' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'>" +
"<option value='-1'>Select a Salary Mode</option><option value='SC'>SC</option><option value='SL'>SL</option></select></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Gross Salary:</label></span>" +
"<div class='divLeft divSizeWidth70_per'><input id=\"txtGrossSalaryForJtSalaried\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeGrossSalaray(this.id,'Jt')\"/></div></div>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Net Salary:</label></span>" +
"<div class='divLeft divSizeWidth70_per'><input id=\"txtNetSalaryForJt\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeNetSalaray(this.id,'Jt')\" /></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Variable Salary:</label></span><div class='divLeft divSizeWidth70_per'>" +
"<select id=\"cmbVeriableSalaryForJt\" class=\"comboSizeH_24 comboSizeWidth101_per comboSizeActive\" onchange=\"salariedHtmlHelper.changeVeriavleSalary('Jt')\">" +
"<option value='-1'>Select from List</option><option value='1'>Yes</option><option value='0'>No</option></select></div></div>" +
"<div class='div3in1' id='divVeriableSalaryForJt' style='display:none;'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>VARIABLE SALARY:</label></span>" +
"<div class='divLeft divSizeWidth30_per'><input id=\"txtVeriableSalaryPerForJtSalaried\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeVaiableSalaryPer(this.id,'Jt')\" /></div>" +
"<div class='divLeft' style='width: 7%;*width: 7%;_width: 7%; border: none; text-align: center;'><span>TK.</span></div><div class='divLeft divSizeWidth28_per'>" +
"<input id='txtTotalVeriableSalaryForJtSalaried' style=\"width:118px;\" class='txtBoxSize_H22' type='text' disabled='disabled' /></div></div><div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Car Allowance:</label></span><div class='divLeft divSizeWidth30_per'>" +
"<select id=\"cmbCarAllowenceJt\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"salariedHtmlHelper.changeCarAllowence('Jt')\"><option value='-1'>Select from List</option>" +
"<option value='1'>Yes</option><option value='0'>No</option></select>  </div><div class='divLeft' style='width: 7%;*width: 7%;_width: 7%; border: none; text-align: center;'><span>TK.</span></div>" +
"<div class='divLeft divSizeWidth28_per'><input id=\"txtCarAllowenceAmountJt\" style=\"width:125px;\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeCarAllowance(this.id,'Jt')\" /></div></div>" +
"<div class='div3in1' id='divCarAllowenceForJtSalaried' style='display:none;'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 90px;*width: 90px;_width: 90px;'>Car Allowance:</label></span>" +
"<div class='divLeft divSizeWidth30_per' style='width: 22%;_width: 22%;*width: 22%;'>" +
"<input id=\"txtCarAllowenceForJt\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\" onchange=\"salariedHtmlHelper.changeCarAllowance(this.id,'Jt')\"/></div>" +
"<div class='divLeft' style='width: 3%;*width: 3%;_width: 3%; border: none; text-align: center;'><span>%</span></div><div class='divLeft divSizeWidth30_per' >" +
"<input id='txtCarAllowenceTotalAmountJt' class='txtBoxSize_H22' style=\"width:164px;\" type='text' disabled='disabled' /></div></div>" +
"<div style='clear:both;'></div>" +
"<div class='div3in1' id='OwnHouseBenifitDivForJtSalaried' style='display:none;'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Own House Benefit:</label></span>" +
"<div class='divLeft divSizeWidth70_per'>" +
"<select id=\"changeOwnHouseAmountJt\" class=\"comboSizeH_24 comboSizeWidth101_per comboSizeActive\" onchange=\"salariedHtmlHelper.changeOwnHouseAmount('Jt')\">" +
"<option value='-1'>Select from List</option>" +
"<option value='1'>Yes</option>" +
"<option value='0'>No</option></select>   </div></div><div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Total Income:</label></span>" +
"<div class='divLeft divSizeWidth70_per'><input id='txtTotalInComeForJtSalaried' class='txtBoxSize_H22' type='text' disabled='disabled'/></div></div>" +
"<div class='clear'></div></div></div>";

        var jtHtmlRight = "<div class='divRightBig'  style='width :45%'><div class='fieldDiv'><label class='lbl'>Remarks:</label></div>" +
"<div class='fieldDiv'><textarea id='txtRemarksForJtSalaried' class='txtFieldBig comboSizeActive' cols='1' rows='3' style='height: 84px;_height: 84px;*height: 84px; width :99%;_width :99%;*width :99%;'></textarea> </div></div>" +
"<div class='divRightBig'  style='width :45%; display:none;' id='divVariableSalaryCalculationForJt'>" +
"<div class='fieldDiv'><span>VARIABLE SALARY CALCULATION</span></div>" +
"<div style='text-align:left; font-size:medium;font-weight:bold;padding: 2px;'>" +
"<div style='border:none;'><table class='tableStyle' id='tblVariableSalaryCalculationForJtSalaried'><tr class='theader'><td >MONTH</td><td >AMOUNT (BDT)</td></tr><tr><td>" +
"<select id='cmbMonth1ForJtSalary' class='comboSizeH_24 comboSizeWidth101_per comboSizeActive'><option value = '1'>JAN</option><option value = '2'>FEB</option>" +
"<option value = '3'>MAR</option><option value = '4'>APR</option><option value = '5'>MAY</option><option value = '6'>JUN</option><option value = '7'>JUL</option>" +
"<option value = '8'>AUG</option><option value = '9'>SEP</option><option value = '10'>OCT</option><option value = '11'>NOV</option><option value = '12'>DEC</option></select></td>" +
"<td><input id=\"txtVaiableAmountForMonth1ForJtSalary\" class=\"comboSizeActive\" type =\"text\" onchange=\"salariedHtmlHelper.changeVariableAmount(this.id,'Jt')\"/></td></tr><tr><td>" +
"<select id='cmbMonth2ForJtSalary' class='comboSizeH_24 comboSizeWidth101_per comboSizeActive'><option value = '1'>JAN</option><option value = '2'>FEB</option><option value = '3'>MAR</option>" +
"<option value = '4'>APR</option><option value = '5'>MAY</option><option value = '6'>JUN</option><option value = '7'>JUL</option><option value = '8'>AUG</option>" +
"<option value = '9'>SEP</option><option value = '10'>OCT</option><option value = '11'>NOV</option><option value = '12'>DEC</option></select></td>" +
"<td><input id=\"txtVaiableAmountForMonth2ForJtSalary\" class=\"comboSizeActive\" type =\"text\" onchange=\"salariedHtmlHelper.changeVariableAmount(this.id,'Jt')\"/></td></tr><tr><td>" +
"<select id='cmbMonth3ForJtSalary' class='comboSizeH_24 comboSizeWidth101_per comboSizeActive'><option value = '1'>JAN</option><option value = '2'>FEB</option><option value = '3'>MAR</option>" +
"<option value = '4'>APR</option><option value = '5'>MAY</option><option value = '6'>JUN</option><option value = '7'>JUL</option><option value = '8'>AUG</option>" +
"<option value = '9'>SEP</option><option value = '10'>OCT</option><option value = '11'>NOV</option><option value = '12'>DEC</option></select></td>" +
"<td><input id=\"txtVaiableAmountForMonth3ForJtSalary\" class=\"comboSizeActive\" type =\"text\" onchange=\"salariedHtmlHelper.changeVariableAmount(this.id,'Jt')\"/></td></tr></table></div>" +
"<input type='hidden' id='txtAverageBalanceForJtSalaried' /></div></div>";

        jtHtml = jtHtmlLeft + jtHtmlRight;
        return jtHtml;

    },
    
    //Change Salary Mode
    changeSalarymode: function(accountType) {
        if (accountType == "Pr") {
            var salaryMode = $F("cmbSalaryModeForPr");
            if (salaryMode == "2") {
                $('cmbInComeAssementForSalaryForPr').setValue('2');
                $('cmbInComeAssementForPr').setValue('SL');
                $('OwnHouseBenifitDivForPrSalaried').style.display = 'block';
            }
            else {
                $('cmbInComeAssementForSalaryForPr').setValue('1');
                $('cmbInComeAssementForPr').setValue('SC');
                $('OwnHouseBenifitDivForPrSalaried').style.display = 'none';
            }
        }
        else {
            var salaryMode = $F("cmbSalaryModeForJt");
            if (salaryMode == "2") {
                $('cmbInComeAssementForSalaryForJt').setValue('2');
                $('cmbInComeAssementForJt').setValue('SL');
                $('OwnHouseBenifitDivForJtSalaried').style.display = 'block';
            }
            else {
                $('cmbInComeAssementForSalaryForJt').setValue('1');
                $('cmbInComeAssementForJt').setValue('SC');
                $('OwnHouseBenifitDivForJtSalaried').style.display = 'none';
            }
        }
        salariedHtmlHelper.calculateTotalIncome(accountType);
    },
    
    //Change Employer
    changeEmployer: function(accountType) {
        if (accountType == "Pr") {
            var employer = $F("cmbEmployerForSalariedForPr");
            if (employer == "-1") {
                $('txtEmployerCodeForPr').setValue('');
                $('cmbEmployerCategoryForPrSalaried').setValue('-1');
                return false;
            }
            var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', employer);
            if (emp) {
                $('txtEmployerCodeForPr').setValue(emp.CompanyCode);
                $('cmbEmployerCategoryForPrSalaried').setValue(emp.CategoryName);
            }
        }
        else {
            var employer = $F("cmbEmployerForSalariedForJt");
            if (employer == "-1") {
                $('txtEmployerCodeForJt').setValue('');
                $('cmbEmployerCategoryForJtSalaried').setValue('-1');
                return false;
            }
            var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', employer);
            if (emp) {
                $('txtEmployerCodeForJt').setValue(emp.CompanyCode);
                $('cmbEmployerCategoryForJtSalaried').setValue(emp.CategoryName);
            }
        }
    },
    
    //Change Gross Salary
    changeGrossSalaray: function(controlId, accountType) {
        
        if (util.isFloat($F(controlId)) && !util.isEmpty($F(controlId))) {
            //
        }
        else {
            alert("Please insert number Field On Gross Salary");
            $(controlId).setValue(0);
        }
    },
    
    //Change Net Salary
    changeNetSalaray: function(identity, accountType) {
        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            if (accountType == "Pr") {
                if (parseFloat($F(identity)) > parseFloat($F("txtGrossSalaryForPrSalaried"))) {
                    alert("Net Amount cannot be grater then gross amount");
                    $(identity).setValue(0);
                    return false;
                }
            }
            else {
                if (parseFloat($F(identity)) > parseFloat($F("txtGrossSalaryForJtSalaried"))) {
                    alert("Net Amount cannot be grater then gross amount");
                    $(identity).setValue(0);
                    return false;
                }
            }


            salariedHtmlHelper.calculateTotalIncome(accountType);
        }
        else {
            alert("Please insert number Field");
            $(identity).setValue(0);
            salariedHtmlHelper.calculateTotalIncome(accountType);
        }
    },
    
    //Change Veriable Salary
    changeVeriavleSalary: function(accountType) {
        if (accountType == "Pr") {
            var variableSalary = $F("cmbVeriableSalaryForPr");
            if (variableSalary == "1") {
                $('divVeriableSalaryForPr').style.display = 'block';
                $('divVariableSalaryCalculationForPr').style.display = 'block';
            }
            else {
                $('divVeriableSalaryForPr').style.display = 'none';
                $('divVariableSalaryCalculationForPr').style.display = 'none';
            }
        }
        else {
            var variableSalary = $F("cmbVeriableSalaryForJt");
            if (variableSalary == "1") {
                $('divVeriableSalaryForJt').style.display = 'block';
                $('divVariableSalaryCalculationForJt').style.display = 'block';
            }
            else {
                $('divVeriableSalaryForJt').style.display = 'none';
                $('divVariableSalaryCalculationForJt').style.display = 'none';
            }
        }
        salariedHtmlHelper.calculateTotalIncome(accountType);
    },
    
    //Change Variable Salary Per
    changeVaiableSalaryPer: function(controlId, accountType) {
        if (util.isFloat($F(controlId)) && !util.isEmpty($F(controlId))) {
            salariedHtmlHelper.calculateTotalIncome(accountType);
        }
        else {
            alert("Please insert number Field");
            $(controlId).setValue(0);
            salariedHtmlHelper.calculateTotalIncome(accountType);
        }
    },

    //change CarAllowence 
    changeCarAllowence: function(accountType) {
        if (accountType == "Pr") {
            var carallowence = $F("cmbCarAllowence");
            if (carallowence == "1") {
                $('divCarAllowenceForPrSalaried').style.display = 'block';
            }
            else {
                $('divCarAllowenceForPrSalaried').style.display = 'none';
            }
        }
        else {
            var carallowence = $F("cmbCarAllowenceJt");
            if (carallowence == "1") {
                $('divCarAllowenceForJtSalaried').style.display = 'block';
            }
            else {
                $('divCarAllowenceForJtSalaried').style.display = 'none';
            }

        }
        salariedHtmlHelper.calculateTotalIncome(accountType);
    },

    //Change changeCarAllowance
    changeCarAllowance: function(identity, accountType) {
        var totalCarAllowanceAmount = 0;
        var amount = 0;
        var per = 0;
        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            if (accountType == "Pr") {
                if (util.isFloat($F("txtCarAllowenceAmount")) && !util.isEmpty($F("txtCarAllowenceAmount"))) {
                    amount = parseFloat($F("txtCarAllowenceAmount"));
                }
                if (util.isFloat($F("txtCarAllowenceForPr")) && !util.isEmpty($F("txtCarAllowenceForPr"))) {
                    per = parseFloat($F("txtCarAllowenceForPr"));
                }
                totalCarAllowanceAmount = parseFloat((amount * per) / 100);
                $("txtCarAllowenceTotalAmountPr").setValue(totalCarAllowanceAmount.toFixed(2));

            }
            else {
                if (util.isFloat($F("txtCarAllowenceAmountJt")) && !util.isEmpty($F("txtCarAllowenceAmountJt"))) {
                    amount = parseFloat($F("txtCarAllowenceAmountJt"));
                }
                if (util.isFloat($F("txtCarAllowenceForJt")) && !util.isEmpty($F("txtCarAllowenceForJt"))) {
                    per = parseFloat($F("txtCarAllowenceForJt"));
                }
                totalCarAllowanceAmount = parseFloat((amount * per) / 100);
                $("txtCarAllowenceTotalAmountJt").setValue(totalCarAllowanceAmount.toFixed(2));
            }
            salariedHtmlHelper.calculateTotalIncome(accountType);
        }
        else {
            alert("Please insert number Field");
            $(identity).setValue(0);
            if (accountType == "Pr") {
                $("txtAverageBalanceForPrSalaried").setValue("0");
            }
            else {
                //
            }
            salariedHtmlHelper.calculateTotalIncome(accountType);
        }
    },
    
    //Change OwnHouse Amount
    changeOwnHouseAmount: function(accountType) {
        salariedHtmlHelper.calculateTotalIncome(accountType);
    },

    //Change Variable amount
    changeVariableAmount: function(identity, accountType) {
        var totalVariableAmount = 0;
        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            if (accountType == "Pr") {
                if (util.isFloat($F("txtVaiableAmountForMonth1ForPrSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth1ForPrSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth1ForPrSalary"));
                }
                if (util.isFloat($F("txtVaiableAmountForMonth2ForPrSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth2ForPrSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth2ForPrSalary"));
                }
                if (util.isFloat($F("txtVaiableAmountForMonth3ForPrSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth3ForPrSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth3ForPrSalary"));
                }
                totalVariableAmount = parseFloat(totalVariableAmount / 3);
                $("txtAverageBalanceForPrSalaried").setValue(totalVariableAmount.toFixed(2));
                salariedHtmlHelper.calculateTotalIncome(accountType);

            }
            else {
                if (util.isFloat($F("txtVaiableAmountForMonth1ForJtSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth1ForJtSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth1ForJtSalary"));
                }
                if (util.isFloat($F("txtVaiableAmountForMonth2ForJtSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth2ForJtSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth2ForJtSalary"));
                }
                if (util.isFloat($F("txtVaiableAmountForMonth3ForJtSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth3ForJtSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth3ForJtSalary"));
                }
                totalVariableAmount = parseFloat(totalVariableAmount / 3);
                $("txtAverageBalanceForJtSalaried").setValue(totalVariableAmount.toFixed(2));
                salariedHtmlHelper.calculateTotalIncome(accountType);
            }



        }
        else {
            alert("Please insert number Field");
            $(identity).setValue(0);
            if (accountType == "Pr") {
                $("txtAverageBalanceForPrSalaried").setValue("0");
            }
            else {
                $("txtAverageBalanceForJtSalaried").setValue("0");
            }
            salariedHtmlHelper.calculateTotalIncome(accountType);
        }
    },

    //Calculate Total Income for Salary
    calculateTotalIncome: function(accountType) {
        var totalIncome = 0;
        var grossSalary = 0;
        var netSalary = 0;
        var carAllowence = 0;
        var salaryCollection = 0;
        var variableSalTotal = 0;
        var totalCarAllowence = 0;
        if (accountType == "Pr") {

            var salarMode = $F("cmbSalaryModeForPr");

            if (util.isFloat($F('txtNetSalaryForPr')) && !util.isEmpty($F('txtNetSalaryForPr'))) {
                netSalary = $F('txtNetSalaryForPr'); //DA16
            }

            var variableSal = $F("cmbVeriableSalaryForPr");
            if (variableSal == "1") {
                var avgSalaryCollection = 0;
                if (util.isFloat($F('txtAverageBalanceForPrSalaried')) && !util.isEmpty($F('txtAverageBalanceForPrSalaried'))) {
                    avgSalaryCollection = parseFloat($F('txtAverageBalanceForPrSalaried'));
                }
                var vairableSalPer = 0;
                if (util.isFloat($F('txtVeriableSalaryPerForPrSalaried')) && !util.isEmpty($F('txtVeriableSalaryPerForPrSalaried'))) {
                    vairableSalPer = parseFloat($F('txtVeriableSalaryPerForPrSalaried'));
                }
                var totalAmountWithPersentege = parseFloat((avgSalaryCollection * vairableSalPer) / 100);
                variableSalTotal = parseFloat(netSalary) + totalAmountWithPersentege;
                $('txtTotalVeriableSalaryForPrSalaried').setValue(totalAmountWithPersentege.toFixed(2));

            }
            else {
                variableSalTotal = netSalary;
            }

            var ownHouseBenifit = $F("changeOwnHouseAmount");
            if (ownHouseBenifit == "1" && salarMode == "2") {
                salaryCollection = variableSalTotal * 1.2;
            }
            else {
                salaryCollection = variableSalTotal;
            }
            var carAllowence = $F("cmbCarAllowence");
            if (carAllowence == "1") {
                var carAllowencePer = 0;
                if (util.isFloat($F('txtCarAllowenceForPr')) && !util.isEmpty($F('txtCarAllowenceForPr'))) {
                    carAllowencePer = parseFloat($F('txtCarAllowenceForPr'));
                }
                var carAllowenceAmount = 0;
                if (util.isFloat($F('txtCarAllowenceAmount')) && !util.isEmpty($F('txtCarAllowenceAmount'))) {
                    carAllowenceAmount = parseFloat($F('txtCarAllowenceAmount'));
                }
                totalCarAllowence = parseFloat((carAllowenceAmount * carAllowencePer) / 100);
                $('txtCarAllowenceTotalAmountPr').setValue(totalCarAllowence.toFixed(2));

            }

            totalIncome = parseFloat(salaryCollection) + parseFloat(totalCarAllowence);
            $('txtTotalInComeForPrSalaried').setValue(totalIncome);

        }
        else {


            if (util.isFloat($F('txtGrossSalaryForJtSalaried')) && !util.isEmpty($F('txtGrossSalaryForJtSalaried'))) {
                grossSalary = $F('txtGrossSalaryForJtSalaried');
            }
            if (util.isFloat($F('txtNetSalaryForJt')) && !util.isEmpty($F('txtNetSalaryForJt'))) {
                netSalary = $F('txtNetSalaryForJt'); //DA16
            }

            var variableSal = $F("cmbVeriableSalaryForJt");
            if (variableSal == "1") {
                var avgSalaryCollection = 0;
                if (util.isFloat($F('txtAverageBalanceForJtSalaried')) && !util.isEmpty($F('txtAverageBalanceForJtSalaried'))) {
                    avgSalaryCollection = parseFloat($F('txtAverageBalanceForJtSalaried'));
                }
                var vairableSalPer = 0;
                if (util.isFloat($F('txtVeriableSalaryPerForJtSalaried')) && !util.isEmpty($F('txtVeriableSalaryPerForJtSalaried'))) {
                    vairableSalPer = parseFloat($F('txtVeriableSalaryPerForJtSalaried'));
                }
                var totalAmountWithPersentege = parseFloat((avgSalaryCollection * vairableSalPer) / 100);
                variableSalTotal = parseFloat(netSalary) + totalAmountWithPersentege;
                $('txtTotalVeriableSalaryForJtSalaried').setValue(totalAmountWithPersentege.toFixed(2));

            }
            else {
                variableSalTotal = netSalary;
            }

            var ownHouseBenifit = $F("changeOwnHouseAmountJt");
            if (ownHouseBenifit == "1") {
                salaryCollection = variableSalTotal * 1.2;
            }
            else {
                salaryCollection = variableSalTotal;
            }
            var carAllowence = $F("cmbCarAllowenceJt");
            if (carAllowence == "1") {
                var carAllowencePer = 0;
                if (util.isFloat($F('txtCarAllowenceForJt')) && !util.isEmpty($F('txtCarAllowenceForJt'))) {
                    carAllowencePer = parseFloat($F('txtCarAllowenceForJt'));
                }
                var carAllowenceAmount = 0;
                if (util.isFloat($F('txtCarAllowenceAmountJt')) && !util.isEmpty($F('txtCarAllowenceAmountJt'))) {
                    carAllowenceAmount = parseFloat($F('txtCarAllowenceAmountJt'));
                }
                totalCarAllowence = parseFloat((carAllowenceAmount * carAllowencePer) / 100);
                $('txtCarAllowenceTotalAmountJt').setValue(totalCarAllowence.toFixed(2));

            }

            totalIncome = parseFloat(salaryCollection) + parseFloat(totalCarAllowence);
            $('txtTotalInComeForJtSalaried').setValue(totalIncome);
        }
        salariedHtmlHelper.setSalaryCalculation(accountType);
    },
    
    //Set Salary Calculation
    setSalaryCalculation: function(accountType) {
        var income = 0;
        var assessmentName = "";
        var assesmentCode = "";
        var employerName = "";
        var employerCode = "";
        var assMethodId = "";
        var assCatId = "";
        if (accountType == "Pr") {

            income = $F("txtTotalInComeForPrSalaried");
            $('txtIncomeBForLCPrSalary').setValue(income);

            assessmentName = $('cmbInComeAssementForSalaryForPr').options[$('cmbInComeAssementForSalaryForPr').selectedIndex].text;
            $('txtInComeAssMethLCPrSalary').setValue(assessmentName);

            assesmentCode = $('cmbInComeAssementForPr').options[$('cmbInComeAssementForPr').selectedIndex].text;
            $('txtAssCodeForLCPrSalary').setValue(assesmentCode);

            employerName = $('cmbEmployerForSalariedForPr').options[$('cmbEmployerForSalariedForPr').selectedIndex].text;
            $('txtEmployerCategoryForLCPrSalary').setValue(employerName);

            employerCode = $F("txtEmployerCodeForPr");
            $('txtEmployerCatCodeForLCPrSalary').setValue(employerCode);

            assMethodId = $F('cmbInComeAssementForSalaryForPr');
            $('txtAssMethodIdForLcPrSalary').setValue(assMethodId);

            assCatId = $F('cmbEmployerForSalariedForPr');
            $('txtAssCatIdForLcPrSalary').setValue(assCatId);

            $('cmbConsiderForLCPrSalary').setValue("0");
            $('trSalariedForLoanCalculatorPr').style.display = "";
        }
        else {
            income = $F("txtTotalInComeForJtSalaried");
            $('txtIncomeBForLCJtSalary').setValue(income);

            assessmentName = $('cmbInComeAssementForSalaryForJt').options[$('cmbInComeAssementForSalaryForJt').selectedIndex].text;
            $('txtInComeAssMethLCJtSalary').setValue(assessmentName);

            assesmentCode = $('cmbInComeAssementForJt').options[$('cmbInComeAssementForJt').selectedIndex].text;
            $('txtAssCodeForLCJtSalary').setValue(assesmentCode);

            employerName = $('cmbEmployerForSalariedForJt').options[$('cmbEmployerForSalariedForJt').selectedIndex].text;
            $('txtEmployerCategoryForLCJtSalary').setValue(employerName);

            employerCode = $F("txtEmployerCodeForJt");
            $('txtEmployerCatCodeForLCJtSalary').setValue(employerCode);

            assMethodId = $F('cmbInComeAssementForSalaryForJt');
            $('txtAssMethodIdForLcJtSalary').setValue(assMethodId);

            assCatId = $F('cmbEmployerForSalariedForJt');
            $('txtAssCodeForLCJtSalary').setValue(assCatId);

            $('cmbConsiderForLCJtSalary').setValue("0");
            $('trSalariedForLoanCalculatorJt').style.display = "";
        }
    }

};

var landLordHtmlHelper = {
    //Primary Applicant Content For LandLord Tab
    prHtmlForLandLord: function() {
        var prhtml = "<div id='divPrApplicantIncomeGeneratorLandlord' style ='padding: 0;margin: 0; display: inline-block'>" +
"<div class='divHeadLine' style='height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 100%;*width: 100%;_width: 100%; border-radius: 15px; margin: 0px;'><span>PRIMARY APPLICANT</span></div>" +
"<div class='divLeftBig'  style='width :45%;padding: 0px; border: none;'>" +
"<div class = 'groupContainer' style='margin: 0px; width: 100%; border-radius: 15px; padding-left: 5px; padding-right:5px;'>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Employer Category:</label></span><div class='divLeft divSizeWidth50_per'>" +
"<select id='cmbEmployeeForLandLordPr' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'><option value='-1'>Select from List</option></select></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Sub-Sector:</label></span>" +
"<div class='divLeft divSizeWidth50_per'><select id='cmbSubsectorForLandLordPr' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'><option value='-1'>Select from List</option></select></div>" +
"<div class='divRight divSizeWidth18_per'><input id='txtEmployeeCategoryCodeForLandLordPr' class='txtBoxSize_H22' type='text' disabled='disabled'/></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Income Assessment:</label></span><div class='divLeft divSizeWidth50_per'>" +
"<select id='cmbIncomeAssementForLandLordPr' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'><option value='-1'>Select from List</option></select></div>" +
"<div class='divRight divSizeWidth18_per'><input id='txtInComeAssementCodeForLandLordPr' class='txtBoxSize_H22' type='text' disabled='disabled'/></div></div>" +
"<div style='text-align:left; font-size:medium;font-weight:bold;padding: 2px;'><div style='border:none;'>" +
"<table id='tblPropertyPr' class='tableStyle' ><tr class='theader'><td >[+]</td><td >VERIFIED RENT</td><td >SHARE</td><td >RENTAL INCOME</td></tr>" +
"<tr><td>PROPERTY-1</td><td><input id=\"txtVerifiedrent1ForLandLordPr\" class=\"comboSizeActive\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Pr')\"/></td><td><input id=\"txtShare1ForLandLordPr\" class=\"comboSizeActive\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Pr')\"/></td>" +
"<td><input id='txtRentalIncome1ForLandLordPr' type ='text' disabled='disabled'/></td></tr>" +
"<tr><td>PROPERTY-2</td><td><input class=\"comboSizeActive\" id=\"txtVerifiedrent2ForLandLordPr\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Pr')\"/></td>" +
"<td><input id=\"txtShare2ForLandLordPr\" class=\"comboSizeActive\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Pr')\"/></td>" +
"<td><input id='txtRentalIncome2ForLandLordPr' type ='text' disabled='disabled'/></td></tr>" +
"<tr><td>PROPERTY-3</td><td><input class=\"comboSizeActive\" id=\"txtVerifiedrent3ForLandLordPr\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Pr')\"/></td>" +
"<td><input id=\"txtShare3ForLandLordPr\" class=\"comboSizeActive\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Pr')\"/></td>" +
"<td><input id='txtRentalIncome3ForLandLordPr' type ='text' disabled='disabled'/></td></tr>" +
"<tr><td colspan='3' style ='text-align: right;'>TOTAL RENTAL INCOME =</td><td> <input id='txttotalrentalIncomeForLandLordPr' class='txtBoxSize_H22' type='text' disabled='disabled' /></td></tr></table></div></div><div class='clear'></div></div></div>" +
"<div class='divRightBig'  style='width :45%'><div class='fieldDiv'><label class='lbl'>Remarks:</label></div>" +
"<div class='fieldDiv'><textarea id='txtAreaRemarksForLandlordPr' class='txtFieldBig comboSizeActive' cols='1' rows='3' style='height: 84px;_height: 84px;*height: 84px; width :99%;_width :99%;*width :99%;'></textarea> </div></div>" +
"<div class='divRightBig'  style='width :45%'><div class='div3in1'>" +
"<span class='spnLeftLabel50_per'><label class='lbl' style='width: 190px;'>REFELECTION % (MIN):</label></span><div class='divLeft divSizeWidth50_per'>" +
"<input id='txtReflactionPerForLandlordPr' class='txtBoxSize_H22 ' type='text' disabled='disabled'/></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel50_per'><label class='lbl' style='width: 190px;'>REFELECTION REQUIRED (MIN):</label></span>" +
"<div class='divLeft divSizeWidth50_per'><input id='txtReflactionrequiredForLandLordPr' class='txtBoxSize_H22 ' type='text' disabled='disabled'/></div></div></div></div><div class='clear'></div>";
        return prhtml;
    },

    //Joint Applicant content for Landlord Tab
    jtHtmlForLandLord: function() {
        var jtHtml = "<div id='divjtApplicantIncomeGeneratorLandlord' style ='padding: 0;margin: 0; display: none; xdisplay: inline-block'>" +
"<div class='divHeadLine' style='height: 25px;*height: 25px;_height: 25px;padding-top: 15px; width: 100%;*width: 100%;_width: 100%; border-radius: 15px; margin: 0px;'><span>JOINT APPLICANT</span></div>" +
"<div class='divLeftBig'  style='width :45%;padding: 0px; border: none;'>" +
"<div class = 'groupContainer' style='margin: 0px; width: 100%; border-radius: 15px; padding-left: 5px; padding-right:5px;'>" +
"<div class='div3in1'>" +
"<span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Employer Category:</label></span>" +
"<div class='divLeft divSizeWidth50_per'><select id='cmbEmployeeForLandLordJt' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'><option value='-1'>Select from List</option></select></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Sub-Sector:</label></span>" +
"<div class='divLeft divSizeWidth50_per'><select id='cmbSubsectorForLandLordJt' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'><option value='-1'>Select from List</option></select></div>" +
"<div class='divRight divSizeWidth18_per'><input id='txtEmployeeCategoryCodeForLandLordJt' class='txtBoxSize_H22' type='text' disabled='disabled'/></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel30_per'><label class='lbl' style='width: 120px;'>Income Assessment:</label></span>" +
"<div class='divLeft divSizeWidth50_per'><select id='cmbIncomeAssementForLandLordJt' class='comboSizeH_24 comboSizeWidth101_per' disabled='disabled'><option value='-1'>Select from List</option></select></div>" +
"<div class='divRight divSizeWidth18_per'><input id='txtInComeAssementCodeForLandLordJt' class='txtBoxSize_H22' type='text' disabled='disabled'/></div></div>" +
"<div style='text-align:left; font-size:medium;font-weight:bold;padding: 2px;'><div style='border:none;'><table id='tblPropertyJt' class='tableStyle' ><tr class='theader'><td >[+]</td><td >VERIFIED RENT</td><td >SHARE</td><td >RENTAL INCOME</td></tr>" +
"<tr><td>PROPERTY-1</td><td><input id=\"txtVerifiedrent1ForLandLordJt\" class=\"comboSizeActive\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Jt')\"/></td><td><input class=\"comboSizeActive\" id=\"txtShare1ForLandLordJt\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Jt')\"/></td><td><input id='txtRentalIncome1ForLandLordJt' type ='text' disabled='disabled'/></td></tr>" +
"<tr><td>PROPERTY-2</td><td><input id=\"txtVerifiedrent2ForLandLordJt\" class=\"comboSizeActive\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Jt')\"/></td><td><input class=\"comboSizeActive\" id=\"txtShare2ForLandLordJt\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Jt')\"/></td><td><input id='txtRentalIncome2ForLandLordJt' type ='text' disabled='disabled'/></td></tr>" +
"<tr><td>PROPERTY-3</td><td><input id=\"txtVerifiedrent3ForLandLordJt\" class=\"comboSizeActive\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Jt')\"/></td><td><input class=\"comboSizeActive\" id=\"txtShare3ForLandLordJt\" type =\"text\" onchange=\"landLordHtmlHelper.calculaterentalIncome(this.id,'Jt')\"/></td><td><input id='txtRentalIncome3ForLandLordJt' type ='text' disabled='disabled'/></td></tr>" +
"<tr><td colspan='3' style ='text-align: right;'>TOTAL RENTAL INCOME =</td><td><input id='txttotalrentalIncomeForLandLordJt' class='txtBoxSize_H22' type='text' disabled='disabled' /></td></tr></table></div></div>" +
"<div class='clear'></div></div></div><div class='divRightBig'  style='width :45%'><div class='fieldDiv'><label class='lbl'>Remarks:</label></div>" +
"<div class='fieldDiv'><textarea id='txtAreaRemarksForLandlordJt' class='txtFieldBig comboSizeActive' cols='1' rows='3' style='height: 84px;_height: 84px;*height: 84px; width :99%;_width :99%;*width :99%;'></textarea> </div></div>" +
"<div class='divRightBig'  style='width :45%'><div class='div3in1'><span class='spnLeftLabel50_per'><label class='lbl' style='width: 190px;'>REFELECTION % (MIN):</label></span>" +
"<div class='divLeft divSizeWidth50_per'><input id='txtReflactionPerForLandlordJt' class='txtBoxSize_H22 ' type='text' disabled='disabled'/></div></div>" +
"<div class='div3in1'><span class='spnLeftLabel50_per'><label class='lbl' style='width: 190px;'>REFELECTION REQUIRED (MIN):</label></span><div class='divLeft divSizeWidth50_per'><input id='txtReflactionrequiredForLandLordJt' class='txtBoxSize_H22 ' type='text' disabled='disabled'/></div></div></div></div>";
        return jtHtml;
    },
    
    //Calculate Rental Income
    calculaterentalIncome: function(identity, accounttype) {
        var verifiedrent1Total = 0;
        var verifiedrent2Total = 0;
        var verifiedrent3Total = 0;

        var rental1 = 0;
        var rental2 = 0;
        var rental3 = 0;

        var share1 = 0;
        var share2 = 0;
        var share3 = 0;
        var totalrentalIncome = 0;
        var per = 0;
        var reflectionrequired = 0;

        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            //Primary Applicant
            if (accounttype == "Pr") {
                if (util.isFloat($F('txtVerifiedrent1ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent1ForLandLordPr'))) {
                    rental1 = parseFloat($F('txtVerifiedrent1ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare1ForLandLordPr')) && !util.isEmpty($F('txtShare1ForLandLordPr'))) {
                    share1 = parseFloat($F('txtShare1ForLandLordPr'));
                }

                verifiedrent1Total = parseFloat((parseFloat(rental1) * parseFloat(share1)) / 100);

                if (util.isFloat($F('txtVerifiedrent2ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent2ForLandLordPr'))) {
                    rental2 = parseFloat($F('txtVerifiedrent2ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare2ForLandLordPr')) && !util.isEmpty($F('txtShare2ForLandLordPr'))) {
                    share2 = parseFloat($F('txtShare2ForLandLordPr'));
                }
                verifiedrent2Total = parseFloat((parseFloat(rental2) * parseFloat(share2)) / 100);
                if (util.isFloat($F('txtVerifiedrent3ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent3ForLandLordPr'))) {
                    rental3 = parseFloat($F('txtVerifiedrent3ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare3ForLandLordPr')) && !util.isEmpty($F('txtShare3ForLandLordPr'))) {
                    share3 = parseFloat($F('txtShare3ForLandLordPr'));
                }
                verifiedrent3Total = parseFloat((parseFloat(rental3) * parseFloat(share3)) / 100);
                totalrentalIncome = parseFloat(verifiedrent1Total) + parseFloat(verifiedrent2Total) + parseFloat(verifiedrent3Total); //DE39

                if (totalrentalIncome >= 32500 && totalrentalIncome < 49999) {
                    per = 40;
                }
                if (totalrentalIncome >= 50000 && totalrentalIncome < 75000) {
                    per = 50;
                }
                if (totalrentalIncome >= 75000 && totalrentalIncome < 150000) {
                    per = 55;
                }
                if (totalrentalIncome >= 150000) {
                    per = 60;
                }


                reflectionrequired = parseFloat((totalrentalIncome * per) / 100);
                $('txtRentalIncome1ForLandLordPr').setValue(verifiedrent1Total.toFixed(2));
                $('txtRentalIncome2ForLandLordPr').setValue(verifiedrent2Total.toFixed(2));
                $('txtRentalIncome3ForLandLordPr').setValue(verifiedrent3Total.toFixed(2));
                $('txttotalrentalIncomeForLandLordPr').setValue(totalrentalIncome.toFixed(2));
                $('txtReflactionPerForLandlordPr').setValue(per);
                $('txtReflactionrequiredForLandLordPr').setValue(reflectionrequired.toFixed(2));

            }
            //Joint Applicant
            else {
                if (util.isFloat($F('txtVerifiedrent1ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent1ForLandLordJt'))) {
                    rental1 = parseFloat($F('txtVerifiedrent1ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare1ForLandLordJt')) && !util.isEmpty($F('txtShare1ForLandLordJt'))) {
                    share1 = parseFloat($F('txtShare1ForLandLordJt'));
                }

                verifiedrent1Total = parseFloat((parseFloat(rental1) * parseFloat(share1)) / 100);

                if (util.isFloat($F('txtVerifiedrent2ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent2ForLandLordJt'))) {
                    rental2 = parseFloat($F('txtVerifiedrent2ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare2ForLandLordJt')) && !util.isEmpty($F('txtShare2ForLandLordJt'))) {
                    share2 = parseFloat($F('txtShare2ForLandLordJt'));
                }
                verifiedrent2Total = parseFloat((parseFloat(rental2) * parseFloat(share2)) / 100);
                if (util.isFloat($F('txtVerifiedrent3ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent3ForLandLordJt'))) {
                    rental3 = parseFloat($F('txtVerifiedrent3ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare3ForLandLordJt')) && !util.isEmpty($F('txtShare3ForLandLordJt'))) {
                    share3 = parseFloat($F('txtShare3ForLandLordJt'));
                }
                verifiedrent3Total = parseFloat((parseFloat(rental3) * parseFloat(share3)) / 100);
                totalrentalIncome = parseFloat(verifiedrent1Total) + parseFloat(verifiedrent2Total) + parseFloat(verifiedrent3Total);

                if (totalrentalIncome >= 32500 && totalrentalIncome < 49999) {
                    per = 40;
                }
                if (totalrentalIncome >= 50000 && totalrentalIncome < 75000) {
                    per = 50;
                }
                if (totalrentalIncome >= 75000 && totalrentalIncome < 150000) {
                    per = 55;
                }
                if (totalrentalIncome >= 150000) {
                    per = 60;
                }


                reflectionrequired = parseFloat((totalrentalIncome * per) / 100);
                $('txtRentalIncome1ForLandLordJt').setValue(verifiedrent1Total.toFixed(2));
                $('txtRentalIncome2ForLandLordJt').setValue(verifiedrent2Total.toFixed(2));
                $('txtRentalIncome3ForLandLordJt').setValue(verifiedrent3Total.toFixed(2));
                $('txttotalrentalIncomeForLandLordJt').setValue(totalrentalIncome.toFixed(2));
                $('txtReflactionPerForLandlordJt').setValue(per);
                $('txtReflactionrequiredForLandLordJt').setValue(reflectionrequired.toFixed(2));
            }
            landLordHtmlHelper.setLandLordCalculation(accounttype);
        }
        else {
            alert("Required Number Field");
        }
    },
    
    //Set Land Lord Calculation
    setLandLordCalculation: function(accountType) {

        var income = 0;
        var assessmentName = "";
        var assesmentCode = "";
        var employerName = "";
        var employerSubsector = "";
        var employerCode = "";
        var assMethodId = 0;
        var assCatId = 0;

        if (accountType == "Pr") {

            income = $F("txttotalrentalIncomeForLandLordPr");
            $('txtIncomeBForLCPrRent').setValue(income);

            assessmentName = $('cmbIncomeAssementForLandLordPr').options[$('cmbIncomeAssementForLandLordPr').selectedIndex].text;
            $('txtInComeAssMethLCPrRent').setValue(assessmentName);

            assesmentCode = $F('txtInComeAssementCodeForLandLordPr');
            $('txtAssCodeForLCPrRent').setValue(assesmentCode);

            employerName = $('cmbEmployeeForLandLordPr').options[$('cmbEmployeeForLandLordPr').selectedIndex].text;
            employerSubsector = $('cmbSubsectorForLandLordPr').options[$('cmbSubsectorForLandLordPr').selectedIndex].text;
            $('txtEmployerCategoryForLCPrRent').setValue(employerName + "-" + employerSubsector);

            employerCode = $F("txtEmployeeCategoryCodeForLandLordPr");
            $('txtEmployerCatCodeForLCPrRent').setValue(employerCode);

            assMethodId = $F('cmbIncomeAssementForLandLordPr');
            $('txtAssMethodIdForLcPrrent').setValue(assMethodId);

            assCatId = $F('cmbEmployeeForLandLordPr');
            $('txtAssCatIdForLcPrRent').setValue(assCatId);

            $('cmbConsiderForLCPrRent').setValue("0");
            $('trLandlordForLoanCalculatorPr').style.display = "";
        }
        else {
            income = $F("txttotalrentalIncomeForLandLordJt");
            $('txtIncomeBForLCJtRent').setValue(income);

            assessmentName = $('cmbIncomeAssementForLandLordJt').options[$('cmbIncomeAssementForLandLordJt').selectedIndex].text;
            $('txtInComeAssMethLCJtRent').setValue(assessmentName);

            assesmentCode = $F('txtInComeAssementCodeForLandLordJt');
            $('txtAssCodeForLCJtRent').setValue(assesmentCode);

            employerName = $('cmbEmployeeForLandLordJt').options[$('cmbEmployeeForLandLordJt').selectedIndex].text;
            employerSubsector = $('cmbSubsectorForLandLordJt').options[$('cmbSubsectorForLandLordJt').selectedIndex].text;
            $('txtEmployerCategoryForLCJtRent').setValue(employerName + "-" + employerSubsector);

            employerCode = $F("txtEmployeeCategoryCodeForLandLordJt");
            $('txtEmployerCatCodeForLCJtRent').setValue(employerCode);

            assMethodId = $F('cmbIncomeAssementForLandLordJt');
            $('txtAssMethodIdForLcJtrent').setValue(assMethodId);

            assCatId = $F('cmbEmployeeForLandLordJt');
            $('txtAssCatIdForLcJtRent').setValue(assCatId);

            $('cmbConsiderForLCJtRent').setValue("0");
            $('trLandlordForLoanCalculatorJt').style.display = "";
        }
        
        
    }
};

var bussManHtmlHelper = {
    //Primary bussiness Man Tab
    prHtmlForBussinessMan: function(res) {
        var prhtml = "";
        var olhtmlStart = "<ol class =\"toc\">";
        var olhtmlEnd = "</ol>";
        var lihtml = "";
        var bankhtml = "";
        var tabLink = "";
        var tabName = "";
        for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {
            var identity = i + 1;
            if (identity < 7) {
                lihtml += "<li id=\"prBankStatementIncomeGenerator" + identity + "\" style=\"*display:inline;\"><a id=\"linkBMPrBank" + identity + "\" href=\"#Bank-" + identity + "\" onclick=\"AnalystDetailsCalculationHelper.BussinessManTabShowHide('linkBMPrBank" + identity + "','Bank-" + identity + "', 'Pr');\"><span id=\"bmprtabBank" + identity + "\">Bank-" + identity + "</span></a></li>";
                var objTab = new Object();
                objTab.AccountType = 'Pr';
                objTab.TabName = "Bank-" + identity;
                objTab.TabLink = 'linkBMPrBank' + identity;
                Page.bussinessManTabArray.push(objTab);
                if (tabName == "") {
                    tabName = 'Bank-' + identity;
                }
                else {
                    tabName += "," + 'Bank-' + identity;
                }
                if (tabLink == "") {
                    tabLink = 'linkBMPrBank' + identity;
                }
                else {
                    tabLink += "," + 'linkBMPrBank' + identity;
                }
                bankhtml += "	<div class=\"content\" id=\"Bank-" + identity + "\" style=\"*border:0px;\">	" +
"	<div class=\"divLeftBig\" style=\"width :50%\">	" +
"	<div class=\"content\" id=\"Div6\" style=\"border: none; padding: 0px;\">	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\" >Account Name:</label>	" +
"	<input id=\"txtAccountNameBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" style =\"width: 306px;*width: 306px;_width: 306px;\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBankStatementIdBMPr" + identity + "\" />	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Account Number:</label>	" +
"	<input class=\"txtFieldBig\" id=\"txtAccountNumberBank" + identity + "\" type=\"text\" disabled=\"disabled\" style =\"width: 180px;*width: 180px;_width: 180px;\"/>	" +
"	<input id=\"txtAccountTypeBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 120px;*width: 120px;_width: 120px;\"/>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Bank Name:</label>	" +
"	<select id=\"cmbBankNameForBank" + identity + "IncomeGenerator\" class=\"comboSizeH_24\" style =\"width: 310px;*width: 310px;_width: 310px;\" disabled=\"disabled\" onchange=\"analyzeLoanApplicationManager.GetBranchNameForIncomeGenerator('cmbBankNameForBank" + identity + "IncomeGenerator','cmbBranchNameForBank" + identity + "IncomeGenerator')\">	" +
"	</select>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Branch Name:</label>	" +
"	<select id=\"cmbBranchNameForBank" + identity + "IncomeGenerator\" class=\"txtFieldBig\"  style =\"width: 310px;*width: 310px;_width: 310px;\" disabled=\"disabled\">	" +
"	<option value=\"-1\">Select a branch</option>	" +
"	</select>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divRightBig\"  style=\"width :45%\">	" +
"	<div class=\"fieldDiv\"><label class=\"lbl\">Remarks:</label></div>	" +
"	<div class=\"fieldDiv\"><textarea id=\"tarearemarksForIncomeGeneratorBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" cols=\"1\" rows=\"3\" style=\"height: 84px;_height: 84px;*height: 84px; width :99%;_width :99%;*width :99%;\"></textarea> </div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div class=\"divLeftBig\"  style=\"width :50%\">	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;\">                           	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" id=\"tblPrBankStatementForIncomeGenerator" + identity + "\" >	" +
"	<tr class=\"theader\">	" +
"	<td style=\"width: 10%;\">MONTHS</td>	" +
"	<td style=\"width: 30%;\">CREDIT TURNOVER</td>	" +
"	<td style=\"width: 30%;\">AVERAGE BALANCE</td>	" +
"	<td style=\"width: 30%;\">ADJUSTMENT (-)</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn1ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth1ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditTurnOver1ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver1ForIncomeGeneratorBank" + identity + "')\" type =\"text\"/></td>	" +
"	<td><input id=\"txtAvarageBalance1ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance1ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment1ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment1ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn2ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth2ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditTurnOver2ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver2ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance2ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance2ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment2ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment2ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn3ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth3ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditTurnOver3ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver3ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance3ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance3ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment3ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment3ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn4ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth4ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditTurnOver4ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver4ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance4ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance4ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment4ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment4ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn5ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth5ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditTurnOver5ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver5ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance5ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance5ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment5ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment5ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn6ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth6ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditTurnOver6ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver6ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance6ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance6ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment6ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment6ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn7ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth7ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditTurnOver7ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver7ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance7ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance7ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment7ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment7ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn8ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth8ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditTurnOver8ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver8ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance8ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance8ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment8ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment8ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn9ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth9ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditTurnOver9ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver9ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance9ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance9ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment9ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment9ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn10ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth10ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditTurnOver10ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver10ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance10ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance10ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment10ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment10ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn11ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth11ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditTurnOver11ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator1','txtcreditTurnOver11ForIncomeGeneratorBank1')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance11ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator1','txtAvarageBalance11ForIncomeGeneratorBank1')\"/></td>	" +
"	<td><input id=\"txtAdjustment11ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator1','txtAdjustment11ForIncomeGeneratorBank1')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn12ForBMPrBank" + identity + "\" type =\"hidden\" />	" +
"	<input id=\"txtMonth12ForIncomeGeneratorBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditTurnOver12ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtcreditTurnOver12ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAvarageBalance12ForIncomeGeneratorBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAvarageBalance12ForIncomeGeneratorBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment12ForIncomeGeneratorBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicant(" + identity + ",'tblPrBankStatementForIncomeGenerator" + identity + "','txtAdjustment12ForIncomeGeneratorBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	</table>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divHeaderTitel\">	" +
"	<input type=\"button\" value=\"Bank Statement\"  style=\" cursor: pointer; border-radius: 3px;\" id=\"btnbmprBankStatement" + identity + "\" onclick=\"bankStatementHelper.showBankStatement(" + identity + ",'Pr', 'BM')\"/>	" +
"	<input type=\"hidden\" id=\"txtbmprStatementBank" + identity + "\" />	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divRightBig\"  style=\"width :45%;padding: 0px; border: none;\">	" +
"	<div class =\"divHeadLine\" style=\"margin: 0px; width: 99%;\"><span>OVER DRAFT FACILITIES</span></div>	" +
"	<div class = \"groupContainer\" style=\"margin: 0px; width: 99%;\">	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;padding: 2px;\">WITH SCB                                	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" id=\"tblOverDraftFacilitiesForPrBMWithScb" + identity + "\" >	" +
"	<tr class=\"theader\">	" +
"	<td >LIMIT</td>	" +
"	<td id=\"tdinterest4\">INTEREST</td>	" +
"	<td >CONSIDER</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtLimt1WithScbForBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterst1WithScbForBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider1ForBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtLimt2WithScbForBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterst2WithScbForBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider2ForBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtLimt3WithScbForBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterst3WithScbForBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider3ForBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtLimt4WithScbForBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterst4WithScbForBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider4ForBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	</table>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;padding: 2px; margin-top: 5px;\">OFF-SCB                                	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" id=\"tblOverDraftFacilitiesForPrBMOffScb" + identity + "\" >	" +
"	<tr class=\"theader\">	" +
"	<td >BANK/FI</td>	" +
"	<td >LIMIT</td>	" +
"	<td id=\"tdInterest5\">INTEREST</td>	" +
"	<td >CONSIDER</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<select id=\"cmbBankName1ForBussinessTabPrBank" + identity + "\" disabled=\"disabled\"></select>	" +
"	</td>	" +
"	<td><input id=\"txtLimit1ForOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterest1ForOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider1ForOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'BM')\">	" +
"	<option value = \"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<select id=\"cmbBankName2ForBussinessTabPrBank" + identity + "\" disabled=\"disabled\"></select>	" +
"	</td>	" +
"	<td><input id=\"txtLimit2ForOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterest2ForOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider2ForOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<select id=\"cmbBankName3ForBussinessTabPrBank" + identity + "\" disabled=\"disabled\"></select>	" +
"	</td>	" +
"	<td><input id=\"txtLimit3ForOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterest3ForOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider3ForOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<select id=\"cmbBankName4ForBussinessTabPrBank" + identity + "\" disabled=\"disabled\"></select>	" +
"	</td>	" +
"	<td><input id=\"txtLimit4ForOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterest4ForOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider4ForOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	</table>	" +
"	<input type=\"hidden\" id=\"txtTotallimitForOoverDraftBMPrBank" + identity + "\" />	" +
"	<input type=\"hidden\" id=\"txtTotalInterestForOverDraftBMPrBank" + identity + "\" />	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div class=\"divLeftBig\" style=\"width :50%\">	" +
"	<div class=\"divMiddileSubHead\" style=\" width: 155px; font-size: small; border: none; padding: 0; margin: 0 0 0 119px ;\">12 MONTHS</div>	" +
"	<div class=\"divRightSubHead\" style=\" width: 155px; font-size: small; border: none ; padding: 0; margin: 0;\">6 MONTHS</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div class=\"content\" id=\"Div3\" style=\"border: none; padding: 0;\">	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Total CTO:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">	" +
"	<input id=\"txtTotalCTOFor12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">	" +
"	<input id=\"txtTotalCTOFor6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Average CTO</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">	" +
"	<input id=\"txtAverageCTO12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">	" +
"	<input id=\"txtAverageCTO6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Average Balance:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">	" +
"	<input id=\"txtAverageBalanceFor12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">	" +
"	<input id=\"txtAverageBalanceFor6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divLeftBig\" style=\"width :50%\">	" +
"	<div class=\"content\" id=\"Div4\" style=\"border: none; padding: 0;\">	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Considered CTO:</label>	" +
"	<select id=\"cmbConsideredCtoForPrBank" + identity + "\" class=\"comboSizeActive\" style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Pr', 'BM')\">	" +
"	</select>	" +
"	<label class=\"lbl\" style=\"width: 112px;\">Considered Balance:</label>	" +
"	<select id=\"cmbConsideredBalanceForPrBank" + identity + "\" class=\"comboSizeActive\" style =\"width: 95px;*width: 95px;_width: 95px;\"></select>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Considered Margin:</label>	" +
"	<input id=\"txtConsiderMarginForPrimaryBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" type=\"text\"  style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Pr', 'BM')\"/>	" +
"	<label class=\"lbl\">Gross Income:</label>	" +
"	<input id=\"txtgrossincomeForPrimaryBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 95px;*width: 95px;_width: 95px;\"/>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Net After Payment:</label>	" +
"	<input id=\"txtNetAfterPaymentForPrimaryBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 95px;*width: 95px;_width: 95px;\"/>	" +
"	<label class=\"lbl\">Share:</label>	" +
"	<input id=\"txtShareForPrimaryBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" type=\"text\"  style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Pr', 'BM')\"/>	" +
"	<label id=\"lblShareForBMPr" + identity + "\" style=\"font-weight:bold;color:red;\"></label>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Net Income:</label>	" +
"	<input id=\"txtNetincomeForPrimaryBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 310px;*width: 310px;_width: 310px;\"/>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divRight\"  style=\"width :45%;padding: 0px; border: none; margin-top: -195px; float: right\"><br><br>" +
"	<div class = \"groupContainer\" style=\"margin: 0px; width: 99%;\">	" +
"	<div class=\"div3in1\">	" +
"	<label id=\"lblForAvgBalBMPr" + identity + "\" style=\"font-weight: bold;color:red;\"></label>	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Income Assessment:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">	" +
"	<select id=\"cmbIncomeAssessmentForPrimaryBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changeIncomeAssement(" + identity + ",'Pr')\">	" +
"	</select>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">	" +
"	<input id=\"txtIncomeAssessmentCodeForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Industry:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">	" +
"	<select id=\"cmbIndustryForPrimaryBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsManager.changeSector(" + identity + ",'Pr')\">	" +
"	</select>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Sub-Sector:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">	" +
"	<select id=\"cmbSubsectorForPrimaryBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changeSubSector(" + identity + ",'Pr')\">	" +
"	<option value=\"-1\">Select a Sub-Sector</option>	" +
"	</select>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Profit Margin Range:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:35%;padding:0px; border:none;\">	" +
"	<input id=\"txtProfitMarginForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;*width:30%;_width:29%; padding:0px; border:none;\">	" +
"	<input id=\"txtProfitMarginCodeForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div id=\"divAverageBalancePr" + identity + "\" style=\"display:none;\">	" +
"	<div class=\"divHeadLine\" style=\"margin: 0px; width: 99%; background-color: white; text-align: right;border: none;\">	" +
"	<span style=\"color: black; margin-right: 50px;\">AVERAGE BALANCE</span>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;_width: 140px;*width: 140px;\">1ST 9 MONTHS:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txt1st9monthForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">1ST 6 MONTHS:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txt1st6MonthForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">LAST 6 MONTHS:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txtLast6monthForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;width: 140px;\">LAST 3 MONTHS:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txtLast3monthForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">CONSIDERED MARGIN:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input class=\"txtBoxSize_H22\" id=\"txtConsiderednarginForPrimaryBank" + identity + "\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div id=\"divPrevRepHis" + identity + "\" style=\" width:100%;_width:100%;*width:100%;float: left; display:none; \">	" +
"	<div class=\"divHeadLine\" style=\"width: 98%;*width: 98%;_width: 98%;\"><span>Previous Repayment History</span></div>	" +
"	<div class=\"groupContainer\" style=\"width: 98%;*width: 98%;_width: 98%;\">	" +
"	<div class=\"divLeftBig\" style=\"width: 407px;*width: 407px;_width: 407px;\"> 	" +
"	<table style=\"width:100%;\">	" +
"	<tr><td>	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;\">OFF SCB                               	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" >	" +
"	<tr class=\"theader\">	" +
"	<td style=\"width: 35%;\">INSTITUTE</td>	" +
"	<td style=\"width: 35%;\" id=\"tdEM7\">EMI</td>	" +
"	<td style=\"width: 30%;\">CONSIDER</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"txtInstitute1ForOffSCBPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBank1BMPr" + identity + "\" />	" +
"	</td>	" +
"	<td><input id=\"txtEMI1ForOffSCBPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider1ForPrimaryOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'BM','Pr')\" ><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"txtInstitute2ForOffSCBPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBank2BMPr" + identity + "\" />	" +
"	</td>	" +
"	<td><input id=\"txtEMI2ForOffSCBPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider2ForPrimaryOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'BM','Pr')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"txtInstitute3ForOffSCBPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBank3BMPr" + identity + "\" />	" +
"	</td>	" +
"	<td><input id=\"txtEMI3ForOffSCBPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider3ForPrimaryOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'BM','Pr')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"txtInstitute4ForOffSCBPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBank4BMPr" + identity + "\" />	" +
"	</td>	" +
"	<td><input id=\"txtEMI4ForOffSCBPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider4ForPrimaryOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'BM','Pr')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td colspan=\"2\" style=\"text-align: right; font-weight: bold\">Total =</td>	" +
"	<td><input id=\"txttotalForOffSCBForPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	</tr>	" +
"	</table>	" +
"	</div>	" +
"	</div>	" +
"	</td>	" +
"	</tr>	" +
"	</table>	" +
"	</div >        	" +
"	<div class=\"divRightBig\" style=\"width: 408px;*width: 408px;_width: 408px;\">  	" +
"	<table style=\"width:100%;\">	" +
"	<tr><td>	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;\">ON SCB                                	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" >	" +
"	<tr class=\"theader\">	" +
"	<td style=\"width: 75%;*width: 75%;_width: 75%;\" id=\"tdEM8\">EMI</td>	" +
"	<td style=\"width: 25%;*width: 25%;_width: 25%;\">CONSIDER</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtEmi1ForOnScbForPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider1ForOnSCBReplHisForPrimaryBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'BM','Pr')\" ><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtEmi2ForOnScbForPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider2ForOnSCBReplHisForPrimaryBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'BM','Pr')\" ><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtEmi3ForOnScbForPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider3ForOnSCBReplHisForPrimaryBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'BM','Pr')\" ><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtEmi4ForOnScbForPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsider4ForOnSCBReplHisForPrimaryBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'BM','Pr')\" ><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td style=\"text-align: right; font-weight: bold\">Total =</td>	" +
"	<td><input id=\"txtTotal1OnSCBRepHisForPrimaryBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	</tr>	" +
"	</table>	" +
"	</div>	" +
"	</div>	" +
"	</td>	" +
"	</tr>	" +
"	</table>	" +
"	</div >	" +
"	<div class =\"clear\"></div>	" +
"	</div>	" +
"	<div class=\"divHeaderTitel\">	" +
"	<div class=\"div3in1\" style=\"float: left;\">	" +
"	<span class=\"spnLeftLabel1\" style=\"padding-left: 22px;\"><label class=\"lbl\" style=\"font-size:medium; width: 285px;*width: 285px;_width: 285px;\" id=\"tdEM9\">TOTAL EMI CLOSING CONSIDERED:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:10%;*width:10%;_width:10%;padding:0px; border:none;\">	" +
"	<input id=\"txtTotalEMIClosingConsideredForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"font-size:medium;width: 97px;*width: 97px;_width: 97px; padding-right: 8px; text-align: right;\">Remarks:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:40%;*width:40%;_width:40%;padding:0px; border:none;\">	" +
"	<input id=\"txtremarks1ForPrimaryBank" + identity + "\" class=\"txtBoxSize_H22 comboSizeActive\" type=\"text\"/>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class =\"clear\"></div>	" +
"	</div>	";
            }
        }
        prhtml = olhtmlStart + lihtml + olhtmlEnd + bankhtml + "^" + tabName + "^" + tabLink;
        return prhtml;
    },

    //Joint bussiness Man Tab
    jtHtmlForBussinessManTab: function(res) {
        var jthtml = "";
        var olhtmlStart = "<ol class =\"toc\">";
        var olhtmlEnd = "</ol>";
        var lihtml = "";
        var bankhtml = "";
        var tabName = "";
        var tablink = "";

        for (var i = 0; i < res.value.AutoJTBankStatementList.length; i++) {
            var identity = i + 1;
            if (identity < 7) {
                lihtml += "<li id=\"jtBankStatementIncomeGenerator" + identity + "\" style=\"*display:inline;\"><a id=\"lnkBMJtBank" + identity + "\" href=\"#jBank-" + identity + "\" onclick=\"AnalystDetailsCalculationHelper.BussinessManTabShowHide('lnkBMJtBank" + identity + "','jBank-" + identity + "', 'Jt');\"><span id=\"bmjttabBank" + identity + "\">Bank-" + identity + "</span></a></li>";
                var objTab = new Object();
                objTab.AccountType = 'Jt';
                objTab.TabName = "jBank-" + identity;
                objTab.TabLink = 'lnkBMJtBank' + identity;
                
                Page.bussinessManTabArray.push(objTab);
                if (tabName == "") {
                    tabName = 'jBank-' + identity;
                }
                else {
                    tabName += "," + 'jBank-' + identity;
                }
                if (tablink == "") {
                    tablink = 'lnkBMJtBank' + identity;
                }
                else {
                    tablink += "," + 'lnkBMJtBank' + identity;
                }

                bankhtml += "	<div class=\"content\" id=\"jBank-" + identity + "\" style=\"*border:0px;\">" +
"	<div class=\"divLeftBig\" style=\"width :50%\">	" +
"	<div class=\"content\" id=\"Div31\" style=\"border: none; padding: 0px;\">	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\" >Account Name:</label>	" +
"	<input id=\"txtAccountName1ForJointBank" + identity + "\" disabled=\"disabled\" class=\"txtFieldBig\" type=\"text\" style =\"width: 306px;*width: 306px;_width: 306px;\"/>	" +
"	<input type=\"hidden\" id=\"txtBankStatementIdBMJt" + identity + "\" />	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Account Number:</label>	" +
"	<input id=\"txtAccountNumber1ForJointBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 180px;*width: 180px;_width: 180px;\"/>	" +
"	<input id=\"txtCompanyName1ForJointBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 120px;*width: 120px;_width: 120px;\"/>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Bank Name:</label>	" +
"	<select id=\"cmbBankName1ForJointBank" + identity + "\" disabled=\"disabled\" class=\"comboSizeH_24\" style =\"width: 310px;*width: 310px;_width: 310px;\" onchange=\"analyzeLoanApplicationManager.GetBranchNameForIncomeGenerator('cmbBankName1ForJointBank" + identity + "','cmbBranchName1ForJointBank" + identity + "')\">	" +
"	</select>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Branch Name:</label>	" +
"	<select id=\"cmbBranchName1ForJointBank" + identity + "\" disabled=\"disabled\" class=\"txtFieldBig\"  style =\"width: 310px;*width: 310px;_width: 310px;\">	" +
"	<option value=\"-1\">Select from List</option>	" +
"	</select>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divRightBig\"  style=\"width :45%\">	" +
"	<div class=\"fieldDiv\"><label class=\"lbl\">Remarks:</label></div>	" +
"	<div class=\"fieldDiv\"><textarea id=\"txtRemark1ForJointBank" + identity + "\" class=\"txtFieldBig\" cols=\"1\" rows=\"3\" style=\"height: 84px;_height: 84px;*height: 84px; width :99%;_width :99%;*width :99%;\"></textarea> </div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div class=\"divLeftBig\"  style=\"width :50%\">	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;\">                             	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" id=\"tblJtBankStatementForIncomeGenerator" + identity + "\" >	" +
"	<tr class=\"theader\">	" +
"	<td style=\"width: 10%;\">MONTHS</td>	" +
"	<td style=\"width: 30%;\">CREDIT TURNOVER</td>	" +
"	<td style=\"width: 30%;\">AVERAGE BALANCE</td>	" +
"	<td style=\"width: 30%;\">ADJUSTMENT (-)</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn1ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName1ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditturnOver1ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver1ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance1ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance1ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment1ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment1ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn2ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName2ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditturnOver2ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver2ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance2ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance2ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment2ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment2ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn3ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName3ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditturnOver3ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver3ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance3ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance3ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment3ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment3ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn4ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName4ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditturnOver4ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver4ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance4ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance4ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment4ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment4ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn5ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName5ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditturnOver5ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver5ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance5ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance5ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment5ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment5ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn6ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName6ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditturnOver6ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver6ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance6ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance6ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment6ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment6ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn7ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName7ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditturnOver7ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver7ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance7ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance7ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment7ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment7ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn8ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName8ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditturnOver8ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver8ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance8ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance8ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment8ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment8ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn9ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName9ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditturnOver9ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver9ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance9ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance9ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment9ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment9ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn10ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName10ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditturnOver10ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver10ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance10ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance10ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment10ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment10ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn11ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName11ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtcreditturnOver11ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver11ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance11ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance11ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment11ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment11ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"hdn12ForBMJtBank" + identity + "\" type=\"hidden\" />	" +
"	<input id=\"txtmonthName12ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>	" +
"	<td><input id=\"txtcreditturnOver12ForJointBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtcreditturnOver12ForJointBank" + identity + "')\" /></td>	" +
"	<td><input id=\"txtAverageBalance12ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAverageBalance12ForJointBank" + identity + "')\"/></td>	" +
"	<td><input id=\"txtAdjustment12ForJointBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicant(" + identity + ",'tblJtBankStatementForIncomeGenerator" + identity + "','txtAdjustment12ForJointBank" + identity + "')\" /></td>	" +
"	</tr>	" +
"	</table>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divHeaderTitel\">	" +
"	<input type=\"button\" value=\"Bank Statement\"  style=\" cursor: pointer; border-radius: 3px;\" id=\"btnbmjtBankStatement" + identity + "\" onclick=\"bankStatementHelper.showBankStatement(" + identity + ",'Jt', 'BM')\"/>	" +
"	<input type=\"hidden\" id=\"txtbmjtStatementBank" + identity + "\" />	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divRightBig\"  style=\"width :45%;padding: 0px; border: none;\">	" +
"	<div class =\"divHeadLine\" style=\"margin: 0px; width: 99%;\"><span>OVER DRAFT FACILITIES</span></div>	" +
"	<div class = \"groupContainer\" style=\"margin: 0px; width: 99%;\">	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;padding: 2px;\">WITH SCB                                	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" id=\"tblOverDraftFacilitiesForJtBMWithScb" + identity + "\">	" +
"	<tr class=\"theader\">	" +
"	<td >LIMIT</td>	" +
"	<td id=\"tdinterest16\">INTEREST</td>	" +
"	<td >CONSIDER</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtLimit1ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterest1ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbInterest1ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtLimit2ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterest2ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbInterest2ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtLimit3ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterest3ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbInterest3ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtLimit4ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterest4ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbInterest4ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	</table>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;padding: 2px; margin-top: 5px;\">OFF-SCB                                	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" id=\"tblOverDraftFacilitiesForJtBMOffScb" + identity + "\">	" +
"	<tr class=\"theader\">	" +
"	<td >BANK/FI</td>	" +
"	<td >LIMIT</td>	" +
"	<td id=\"tdinterest17\">INTEREST</td>	" +
"	<td >CONSIDER</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<select id=\"cmbBankName1ForBussinessTabJtBank" + identity + "\" disabled=\"disabled\"></select>	" +
"	</td>	" +
"	<td><input id=\"txtLimitOffScb1ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterestOffSCB1ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOffScbBM1ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<select id=\"cmbBankName2ForBussinessTabJtBank" + identity + "\" disabled=\"disabled\"></select>	" +
"	</td>	" +
"	<td><input id=\"txtLimitOffScb2ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterestOffSCB2ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOffScbBM2ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<select id=\"cmbBankName3ForBussinessTabJtBank" + identity + "\" disabled=\"disabled\"></select>	" +
"	</td>	" +
"	<td><input id=\"txtLimitOffScb3ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterestOffSCB3ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOffScbBM3ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<select id=\"cmbBankName4ForBussinessTabJtBank" + identity + "\" disabled=\"disabled\"></select>	" +
"	</td>	" +
"	<td><input id=\"txtLimitOffScb4ForJointBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>	" +
"	<td><input id=\"txtInterestOffSCB4ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOffScbBM4ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'BM')\">	" +
"	<option value=\"0\">No</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	</select>	" +
"	</td>	" +
"	</tr>	" +
"	</table>	" +
"	<input type=\"hidden\" id=\"txtTotallimitForOoverDraftBMJtBank" + identity + "\" />	" +
"	<input type=\"hidden\" id=\"txtTotalInterestForOverDraftBMJtBank" + identity + "\" />	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div class=\"divLeftBig\" style=\"width :50%\">	" +
"	<div class=\"divMiddileSubHead\" style=\" width: 155px; font-size: small; border: none; padding: 0; margin: 0 0 0 119px ;\">12 MONTHS</div>	" +
"	<div class=\"divRightSubHead\" style=\" width: 155px; font-size: small; border: none ; padding: 0; margin: 0;\">6 MONTHS</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div class=\"content\" id=\"Div32\" style=\"border: none; padding: 0;\">	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Total CTO:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">	" +
"	<input class=\"txtBoxSize_H22\" id=\"txtTotalCTOFor12MonthJointBank" + identity + "\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">	" +
"	<input class=\"txtBoxSize_H22\" id=\"txtTotalCTOFor6MonthJointBank" + identity + "\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Average CTO</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">	" +
"	<input class=\"txtBoxSize_H22\" id=\"txtAverageCTOFor12MonthForJointBank" + identity + "\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">	" +
"	<input class=\"txtBoxSize_H22\" id=\"txtAverageCTOFor6MonthForJointBank" + identity + "\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Average Balance:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">	" +
"	<input class=\"txtBoxSize_H22\" id=\"txtAverageBalance12MonthForJointBank" + identity + "\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">	" +
"	<input class=\"txtBoxSize_H22\" id=\"txtAverageBalance6MonthForJointBank" + identity + "\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divLeftBig\" style=\"width :50%\">	" +
"	<div class=\"content\" id=\"Div33\" style=\"border: none; padding: 0;\">	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Considered CTO:</label>	" +
"	<select id=\"cmbConsideredCtoForJtBank" + identity + "\" class=\"comboSizeActive\" style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Jt', 'BM')\">	" +
"	</select>	" +
"	<label class=\"lbl\" style=\"width: 112px;\">Considered Balance:</label>	" +
"	<select id=\"cmbConsideredBalanceForJtBank" + identity + "\" class=\"comboSizeActive\" style =\"width: 95px;*width: 95px;_width: 95px;\"></select>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Considered Margin:</label>	" +
"	<input id=\"txtConsideredMarginBMForJointBank" + identity + "\" class=\"comboSizeActive\" class=\"txtFieldBig\" type=\"text\"  style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Jt', 'BM')\"/>	" +
"	<label class=\"lbl\">Gross Income:</label>	" +
"	<input id=\"txtgrossIncomeForJointBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 95px;*width: 95px;_width: 95px;\"/>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Net After Payment:</label>	" +
"	<input id=\"txtNetAfterPaymentForJointBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 95px;*width: 95px;_width: 95px;\"/>	" +
"	<label class=\"lbl\">Share:</label>	" +
"	<input id=\"txtShareForJointBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" type=\"text\"  style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Jt', 'BM')\"/>	" +
"	<label id=\"lblShareForBMJt" + identity + "\" style=\"font-weight:bold;color:red;\"></label>	" +
"	</div>	" +
"	<div class=\"fieldDiv\">	" +
"	<label class=\"lbl\">Net Income:</label>	" +
"	<input id=\"txtNetIncomeForJointBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 310px;*width: 310px;_width: 310px;\"/>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"divRight\"  style=\"width :45%;padding: 0px; border: none; margin-top: -195px; float: right\">	" +
"	<div class=\"clear\"></div><br><br>	" +
"	<div class = \"groupContainer\" style=\"margin: 0px; width: 99%;\">	" +
"	<div class=\"div3in1\">	" +
"	<label id=\"lblForAvgBalBMJt" + identity + "\" style=\"font-weight: bold;color:red;\"></label>	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Income Assessment:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">	" +
"	<select id=\"cmbIncomeAssementForJointBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changeIncomeAssement(" + identity + ",'Jt')\">	" +
"	</select>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">	" +
"	<input id=\"txtIncomeAssementCodeForJointBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Industry:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">	" +
"	<select id=\"cmbIndustryForJointBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsManager.changeSector(" + identity + ",'Jt')\"> 	" +
"	</select>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Sub-Sector:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">	" +
"	<select id=\"cmbSubSectorForJointBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changeSubSector(" + identity + ",'Jt')\">	" +
"	<option value=\"-1\">Select a Sub-Sector</option>	" +
"	</select>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Profit Margin Range:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:35%;padding:0px; border:none;\">	" +
"	<input id=\"txtProfitMarginForJointBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<div class=\"divMiddile\" style=\"width:30%;*width:30%;_width:29%; padding:0px; border:none;\">	" +
"	<input id=\"txtSubSectorCode1ForJointBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div id=\"divAverageBalanceJoint" + identity + "\" style=\"display:none;\">	" +
"	<div class=\"divHeadLine\" style=\"margin: 0px; width: 99%; background-color: white; text-align: right;border: none;\"><span style=\"color: black; margin-right: 50px;;\">AVERAGE BALANCE</span></div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;_width: 140px;*width: 140px;\">1ST 9 MONTHS:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txtAVGBalanceFor1st9MontForJointBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">1ST 6 MONTHS:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txtAVGBalanceFor1st6MontForJointBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">LAST 6 MONTHS:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txtAVGBalanceForLast6MontForJointBank" + identity + "\" class=\"txtBoxSize_H22\"  disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;width: 140px;\">LAST 3 MONTHS:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txtAVGBalanceForLast3MontForJointBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"div3in1\">	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">CONSIDERED MARGIN:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">	" +
"	<input id=\"txtConsideredMarginForJointBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	</div>	" +
"	</div>	" +
"	<div class=\"clear\"></div>	" +
"	<div id=\"divPrevRepHisJoint" + identity + "\" style=\" width:100%;_width:100%;*width:100%;float: left; display:none;\">	" +
"	<div class=\"divHeadLine\" style=\"width: 98%;*width: 98%;_width: 98%;\"><span>Previous Repayment History</span></div>	" +
"	<div class=\"groupContainer\" style=\"width: 98%;*width: 98%;_width: 98%;\">	" +
"	<div class=\"divLeftBig\" style=\"width: 407px;*width: 407px;_width: 407px;\"> 	" +
"	<table style=\"width:100%;\">	" +
"	<tr><td>	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;\">OFF SCB                               	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" >	" +
"	<tr class=\"theader\">	" +
"	<td style=\"width: 35%;\">INSTITUTE</td>	" +
"	<td style=\"width: 35%;\" id=\"tdEM25\">EMI</td>	" +
"	<td style=\"width: 30%;\">CONSIDER</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"txtInstituteOffScb1ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBank1BMJt" + identity + "\" />	" +
"	</td>	" +
"	<td><input id=\"txtEmiOffScb1ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOffScb1ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'BM','Jt')\" ><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"txtInstituteOffScb2ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBank2BMJt" + identity + "\" />	" +
"	</td>	" +
"	<td><input id=\"txtEmiOffScb2ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOffScb2ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'BM','Jt')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"txtInstituteOffScb3ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBank3BMJt" + identity + "\" />	" +
"	</td>	" +
"	<td><input id=\"txtEmiOffScb3ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOffScb3ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'BM','Jt')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td>	" +
"	<input id=\"txtInstituteOffScb4ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>	" +
"	<input type=\"hidden\" id=\"txtBank4BMJt" + identity + "\" />	" +
"	</td>	" +
"	<td><input id=\"txtEmiOffScb4ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOffScb4ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'BM','Jt')\" ><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td colspan=\"2\" style=\"text-align: right; font-weight: bold\">Total =</td>	" +
"	<td><input id=\"txtTotalOffSCBJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	</tr>	" +
"	</table>	" +
"	</div>	" +
"	</div>	" +
"	</td>	" +
"	</tr>	" +
"	</table>	" +
"	</div >        	" +
"	<div class=\"divRightBig\" style=\"width: 408px;*width: 408px;_width: 408px;\">  	" +
"	<table style=\"width:100%;\">	" +
"	<tr><td>	" +
"	<div style=\"text-align:left; font-size:medium;font-weight:bold;\">ON SCB                                	" +
"	<div style=\"border:none;\">	" +
"	<table class=\"tableStyle\" >	" +
"	<tr class=\"theader\">	" +
"	<td style=\"width: 75%;*width: 75%;_width: 75%;\" id=\"tdEM26\">EMI</td>	" +
"	<td style=\"width: 25%;*width: 25%;_width: 25%;\">CONSIDER</td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtEmiOnScb1ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOnScb1ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'BM','Jt')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtEmiOnScb2ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOnScb2ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'BM','Jt')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtEmiOnScb3ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOnScb3ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'BM','Jt')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td><input id=\"txtEmiOnScb4ForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	<td><select id=\"cmbConsiderOnScb4ForJointBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'BM','Jt')\"><option value=\"-1\">Select</option>	" +
"	<option value=\"1\">Yes</option>	" +
"	<option value=\"0\">No</option>	" +
"	</select></td>	" +
"	</tr>	" +
"	<tr>	" +
"	<td style=\"text-align: right; font-weight: bold\">Total =</td>	" +
"	<td><input id=\"txtTotalOnScbForJointBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>	" +
"	</tr>	" +
"	</table>	" +
"	</div>	" +
"	</div>	" +
"	</td>	" +
"	</tr>	" +
"	</table>	" +
"	</div >	" +
"	<div class =\"clear\"></div>	" +
"	</div>	" +
"	<div class=\"divHeaderTitel\">	" +
"	<div class=\"div3in1\" style=\"float: left;\">	" +
"	<span class=\"spnLeftLabel1\" style=\"padding-left: 22px;\"><label class=\"lbl\" style=\"font-size:medium; width: 285px;*width: 285px;_width: 285px;\" id=\"tdEM27\">TOTAL EMI CLOSING CONSIDERED:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:10%;*width:10%;_width:10%;padding:0px; border:none;\">	" +
"	<input id=\"txtTotalEMIClosingConsideredPrevRepHisForJointBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>	" +
"	</div>	" +
"	<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"font-size:medium;width: 97px;*width: 97px;_width: 97px; padding-right: 8px; text-align: right;\">Remarks:</label></span>	" +
"	<div class=\"divLeft\" style=\"width:40%;*width:40%;_width:40%;padding:0px; border:none;\">	" +
"	<input id=\"txtRemarksPrevRepHisForJointBank" + identity + "\" class=\"comboSizeActive\" type=\"text\"/>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	</div>	" +
"	<div class =\"clear\"></div>	" +
"	</div>";
            }
        }
        jthtml = olhtmlStart + lihtml + olhtmlEnd + bankhtml + "^" + tabName + "^" + tablink;
        return jthtml;
    }

};

var selfEmployedHtmlHelper = {
    //Primary SelfEmployed Helper
    prHtmlForSelfEmployed: function(res) {
        var prhtml = "";
        var olhtmlStart = "<ol class =\"toc\">";
        var olhtmlEnd = "</ol>";
        var lihtml = "";
        var bankhtml = "";
        var tabName = "";
        var tablink = "";
        for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {
            var identity = i + 1;
            if (identity < 7) {
                lihtml += "<li id=\"prBankStatementSelfEmployed" + identity + "\" style=\"*display:inline;\"><a id=\"lnkSEPrBank" + identity + "\" href=\"#SelfEmployedBank-" + identity + "\" onclick=\"AnalystDetailsCalculationHelper.SelfEmployedTabShowHide('lnkSEPrBank" + identity + "','SelfEmployedBank-" + identity + "', 'Pr');\"><span id=\"seprtabBank" + identity + "\">Bank-" + identity + "</span></a></li>";
                var objTab = new Object();
                objTab.AccountType = 'Pr';
                objTab.TabName = "SelfEmployedBank-" + identity;
                objTab.TabLink = 'lnkSEPrBank' + identity;
                Page.selfEmployedTabArray.push(objTab);
                if (tabName == "") {
                    tabName = 'SelfEmployedBank-' + identity;
                }
                else {
                    tabName += "," + 'SelfEmployedBank-' + identity;
                }

                if (tablink == "") {
                    tablink = 'lnkSEPrBank' + identity;
                }
                else {
                    tablink += "," + 'lnkSEPrBank' + identity;
                }

                bankhtml += "<div class=\"content\" id=\"SelfEmployedBank-" + identity + "\" style=\"*border:0px;\">" +
"<div class=\"divLeftBig\" style=\"width :50%\">" +
"<div class=\"content\" id=\"Div55\" style=\"border: none; padding: 0px;\">" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\" >Account Name:</label>" +
"<input id=\"txtAccountNameForSEBank" + identity + "\" disabled=\"disabled\" class=\"txtFieldBig\" type=\"text\" style =\"width: 306px;*width: 306px;_width: 306px;\"/>" +
"<input type=\"hidden\" id=\"txtBankStatementIdSEPr" + identity + "\" />" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Account Number:</label>" +
"<input id=\"txtAccountNumberForSEBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 180px;*width: 180px;_width: 180px;\"/>" +
"<input id=\"txtAccountTypeForSEBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 120px;*width: 120px;_width: 120px;\"/>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Bank Name:</label>" +
"<select id=\"cmbBankNameForSEBank" + identity + "\" disabled=\"disabled\" class=\"comboSizeH_24\" style =\"width: 310px;*width: 310px;_width: 310px;\">" +
"</select>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Branch Name:</label>" +
"<select id=\"cmbbranchNameForSEBank" + identity + "\" disabled=\"disabled\" class=\"txtFieldBig\"  style =\"width: 310px;*width: 310px;_width: 310px;\">" +
"</select>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"divRightBig\"  style=\"width :45%\">" +
"<div class=\"fieldDiv\"><label class=\"lbl\">Remarks:</label></div>" +
"<div class=\"fieldDiv\"><textarea id=\"txtremarksForSEPrBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" cols=\"1\" rows=\"3\" style=\"height: 84px;_height: 84px;*height: 84px; width :99%;_width :99%;*width :99%;\"></textarea> </div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<div class=\"divLeftBig\"  style=\"width :50%\">" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;\">" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" id=\"tblPrBankStatementForSEBank" + identity + "\">" +
"<tr class=\"theader\">" +
"<td style=\"width: 10%;\">MONTHS</td>" +
"<td style=\"width: 30%;\">CREDIT TURNOVER</td>" +
"<td style=\"width: 30%;\">AVERAGE BALANCE</td>" +
"<td style=\"width: 30%;\">ADJUSTMENT (-)</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn1ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth1ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver1ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver1ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance1ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance1ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment1ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment1ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn2ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth2ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver2ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver2ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance2ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance2ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment2ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment2ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn3ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth3ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver3ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver3ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance3ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance3ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment3ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment3ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn4ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth4ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver4ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver4ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance4ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance4ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment4ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment4ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn5ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth5ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver5ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver5ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance5ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance5ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment5ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment5ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn6ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth6ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver6ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver6ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance6ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance6ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment6ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment6ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn7ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth7ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver7ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver7ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance7ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance7ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment7ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment7ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn8ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth8ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver8ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver8ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance8ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance8ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment8ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment8ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn9ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth9ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver9ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver9ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance9ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance9ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment9ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment9ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn10ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth10ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver10ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver10ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance10ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance10ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment10ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment10ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn11ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth11ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver11ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver11ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance11ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance11ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment11ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment11ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn12ForSEPrBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth12ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver12ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtCreditTurnOver12ForSEBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance12ForSEPrBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAverageBalance12ForSEPrBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment12ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForPrApplicantSelfEmployed(" + identity + ",'tblPrBankStatementForSEBank" + identity + "','txtAdjustment12ForSEPrBank" + identity + "')\"/></td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"<div class=\"divHeaderTitel\">" +
"<input type=\"button\" value=\"Bank Statement\"  style=\" cursor: pointer; border-radius: 3px;\" id=\"btnseprBankStatement" + identity + "\" onclick=\"bankStatementHelper.showBankStatement(" + identity + ",'Pr', 'SE')\"/>" +
"<input type=\"hidden\" id=\"txtseprStatementBank" + identity + "\" />" +
"</div>" +
"</div>" +
"<div class=\"divRightBig\"  style=\"width :45%;padding: 0px; border: none;\">" +
"<div class =\"divHeadLine\" style=\"margin: 0px; width: 99%;\"><span>OVER DRAFT FACILITIES</span></div>" +
"<div class = \"groupContainer\" style=\"margin: 0px; width: 99%;\">" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;padding: 2px;\">ON SCB" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" id=\"tblOverDraftFacilitiesForPrSEWithScb" + identity + "\">" +
"<tr class=\"theader\">" +
"<td >LIMIT</td>" +
"<td id=\"tdinterest28\">INTEREST</td>" +
"<td >CONSIDER</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtLimt1WithScbForSEBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterst1WithScbForSEBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider1WithScbForSEBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'SE')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtLimt2WithScbForSEBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterst2WithScbForSEBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider2WithScbForSEBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'SE')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtLimt3WithScbForSEBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterst3WithScbForSEBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider3WithScbForSEBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'SE')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtLimt4WithScbForSEBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterst4WithScbForSEBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider4WithScbForSEBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'SE')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;padding: 2px; margin-top: 5px;\">OFF-SCB" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" id=\"tblOverDraftFacilitiesForPrSEOffScb" + identity + "\">" +
"<tr class=\"theader\">" +
"<td >BANK/FI</td>" +
"<td >LIMIT</td>" +
"<td id=\"tdinterest29\">INTEREST</td>" +
"<td >CONSIDER</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<select id=\"cmbBankName1ForSEPrBank" + identity + "\" disabled=\"disabled\"></select>" +
"</td>" +
"<td><input id=\"txtLimit1ForSEOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterest1ForSEOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider1ForSEOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'SE')\">" +
"<option value = \"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<select id=\"cmbBankName2ForSEPrBank" + identity + "\" disabled=\"disabled\"></select>" +
"</td>" +
"<td><input id=\"txtLimit2ForSEOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterest2ForSEOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider2ForSEOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'SE')\">" +
"<option value = \"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<select id=\"cmbBankName3ForSEPrBank" + identity + "\" disabled=\"disabled\"></select>" +
"</td>" +
"<td><input id=\"txtLimit3ForSEOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterest3ForSEOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider3ForSEOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'SE')\">" +
"<option value = \"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<select id=\"cmbBankName4ForSEPrBank" + identity + "\" disabled=\"disabled\"></select>" +
"</td>" +
"<td><input id=\"txtLimit4ForSEOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterest4ForSEOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider4ForSEOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Pr', 'SE')\">" +
"<option value = \"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"</table>" +
"<input type=\"hidden\" id=\"txtTotallimitForOoverDraftSEPrBank" + identity + "\" />" +
"<input type=\"hidden\" id=\"txtTotalInterestForOverDraftSEPrBank" + identity + "\" />" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<div class=\"divLeftBig\" style=\"width :50%\">" +
"<div class=\"divMiddileSubHead\" style=\" width: 155px; font-size: small; border: none; padding: 0; margin: 0 0 0 119px ;\">12 MONTHS</div>" +
"<div class=\"divRightSubHead\" style=\" width: 155px; font-size: small; border: none ; padding: 0; margin: 0;\">6 MONTHS</div>" +
"<div class=\"clear\"></div>" +
"<div class=\"content\" id=\"Div18\" style=\"border: none; padding: 0;\">" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Total CTO:</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">" +
"<input id=\"txtTotalCTOForSEPr12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">" +
"<input id=\"txtTotalCTOForSEPr6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Average CTO</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">" +
"<input id=\"txtAverageCTOForSEPr12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">" +
"<input id=\"txtAverageCTOForSEPr6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Average Balance:</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">" +
"<input id=\"txtAverageBalanceForSEPr12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">" +
"<input id=\"txtAverageBalanceForSEPr6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"divLeftBig\" style=\"width :50%\">" +
"<div class=\"content\" id=\"Div22\" style=\"border: none; padding: 0;\">" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Considered CTO:</label>" +
"<select id=\"cmbConsideredCtoForSEPrBank" + identity + "\" class=\"comboSizeActive\" style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Pr', 'SE')\">" +
"</select>" +
"<label class=\"lbl\" style=\"width: 112px;\">Considered Balance:</label>" +
"<select id=\"cmbConsideredBalanceForSEPrBank" + identity + "\" class=\"comboSizeActive\" style =\"width: 95px;*width: 95px;_width: 95px;\"></select>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Considered Margin:</label>" +
"<input id=\"txtConsiderMarginForSEPrBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" type=\"text\"  style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Pr', 'SE')\"/>" +
"<label class=\"lbl\">Gross Income:</label>" +
"<input id=\"txtgrossincomeForSEPrBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 95px;*width: 95px;_width: 95px;\"/>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Net After Payment:</label>" +
"<input id=\"txtNetAfterPaymentForSEPrBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 95px;*width: 95px;_width: 95px;\"/>" +
"<label class=\"lbl\">Share:</label>" +
"<input id=\"txtShareForSEPrBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" type=\"text\"  style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Pr', 'SE')\"/>" +
"<label id=\"lblShareForSEPr" + identity + "\" style=\"font-weight:bold;color:red;\"></label>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Net Income:</label>" +
"<input id=\"txtNetincomeForSEPrBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 310px;*width: 310px;_width: 310px;\"/>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"divRight\"  style=\"width :45%;padding: 0px; border: none; margin-top: -180px; float: right\">" +
"<div class = \"groupContainer\" style=\"margin: 0px; width: 99%; border-radius: 15px; padding-top: 5px;\">" +
"<div class=\"div3in1\">" +
"<label id=\"lblForAvgBalSEPr" + identity + "\" style=\"font-weight: bold;color:red;\"></label>" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Income Assessment:</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">" +
"<select id=\"cmbIncomeAssessmentForSEPrBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changeIncomeAssementForSelfEmployed(" + identity + ",'Pr')\">" +
"</select>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">" +
"<input id=\"txtIncomeAssessmentCodeForSEPrBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Industry:</label></span>" +
"<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">" +
"<select id=\"cmbIndustryForSEPrBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsManager.changeSectorForSelfEmployed(" + identity + ",'Pr')\">" +
"</select>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Sub-Sector:</label></span>" +
"<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">" +
"<select id=\"cmbSubsectorForSEPrBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changeSubSectorForSelfEmployed(" + identity + ",'Pr')\">" +
"<option value=\"-1\">Select a Sub-Sector</option>" +
"</select>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Private Income:</label></span>" +
"<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">" +
"<select id=\"cmbPrivateIncomeForSEPrBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changePrivateInCome(" + identity + ",'Pr','Se')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Profit Margin Range:</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:35%;padding:0px; border:none;\">" +
"<input id=\"txtProfitMarginForSEPrBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;*width:30%;_width:29%; padding:0px; border:none;\">" +
"<input id=\"txtProfitMarginCodeForSEPrBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div id=\"divAverageBalanceForSEPr" + identity + "\" style=\"display:none;\">" +
"<div class=\"divHeadLine\" style=\"margin: 0px; width: 99%; background-color: white; text-align: right;border: none;\"><span style=\"color: black; margin-right: 50px;;\">AVERAGE BALANCE</span></div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;_width: 140px;*width: 140px;\">1ST 9 MONTHS:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input id=\"txt1st9monthForSEPrBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">1ST 6 MONTHS:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input id=\"txt1st6MonthForSEPrBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">LAST 6 MONTHS:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input id=\"txtLast6monthForSEPrBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;width: 140px;\">LAST 3 MONTHS:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input id=\"txtLast3monthForSEPrBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">CONSIDERED MARGIN:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input class=\"txtBoxSize_H22\" id=\"txtConsiderednarginForSEPrBank" + identity + "\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<div id=\"Div26\" style=\" width:100%;_width:100%;*width:100%;float: left; \">" +
"<div class=\"groupContainer\" style=\"width: 100%;*width: 100%;_width: 100%; border-radius:15px; padding-top: 5px; margin: 0;\">" +
"<div id=\"divChamberIncomeForSePrBank" + identity + "\" style=\"display:none;\">" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;\">CHAMBER INCOME " +
"<div style=\"border:none;\">" +
"<table class=\"tableStyleSmall\" style=\"text-align:center;border:solid 1px gray;\" id=\"tblChamberIncomeForSEPrBank" + identity + "\" >" +
"<tr class=\"theader\">" +
"<td style=\"text-align:left;\">CHAMBER NAME [+]</td>" +
"<td colspan=\"3\">" +
"<table style=\"width:100%; text-align:center;\">" +
"<tr>" +
"<td colspan=\"3\">FROM OLD PATIENTS</td>" +
"</tr>" +
"<tr>" +
"<td style=\"width:34%\">PATIENTS</td>" +
"<td style=\"width:33%\">FEE</td>" +
"<td style=\"width:33%\">TOTAL</td>" +
"</tr>" +
"</table>" +
"</td>" +
"<td colspan=\"3\">" +
"<table style=\"width:100%; text-align:center;\">" +
"<tr>" +
"<td colspan=\"3\">FROM NEW PATIENTS</td>" +
"</tr>" +
"<tr>" +
"<td style=\"width:34%\">PATIENTS</td>" +
"<td style=\"width:33%\">FEE</td>" +
"<td style=\"width:33%\">TOTAL</td>" +
"</tr>" +
"</table>" +
"</td>" +
"<td>TOTAL</td>" +
"<td>DAY/MONTHS</td>" +
"<td>INCOME</td>" +
"</tr>" +
"<tr>" +
"<td>CHAMBER-1</td>" +
"<td><input id=\"txtPatientsOld1ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeOld1ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalOld1ForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><input id=\"txtPatientsNew1ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeNew1ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalNew1ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtTotalOldNew1ForSEPr1Bank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtDayMonth1ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtIncome1ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"</tr>" +
"<tr>" +
"<td>CHAMBER-2</td>" +
"<td><input id=\"txtPatientsOld2ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeOld2ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalOld2ForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><input id=\"txtPatientsNew2ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeNew2ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalNew2ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtTotalOldNew2ForSEPr1Bank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtDayMonth2ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtIncome2ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"</tr>" +
"<tr>" +
"<td>CHAMBER-3</td>" +
"<td><input id=\"txtPatientsOld3ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeOld3ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalOld3ForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><input id=\"txtPatientsNew3ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeNew3ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalNew3ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtTotalOldNew3ForSEPr1Bank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtDayMonth3ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtIncome3ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"</tr>" +
"<tr>" +
"<td>CHAMBER-4</td>" +
"<td><input id=\"txtPatientsOld4ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeOld4ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalOld4ForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><input id=\"txtPatientsNew4ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeNew4ForSEPrBank" + identity + "\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalNew4ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtTotalOldNew4ForSEPr1Bank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtDayMonth4ForSEPrBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Pr','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtIncome4ForSEPrBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<div class=\"div3in1\" style=\"width: 34%;*width: 34%;_width: 34%; float: left;\">" +
"<span class=\"spnLeftLabel30_per\" style=\"width: 32%;_width: 32%;*width: 32%;\"><label class=\"lbl\">Chamber Income:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per\">" +
"<input id=\"txtTotalChamberIncomeForSEPrBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\" style=\"width: 33%;*width: 33%;_width: 33%;float: left;\">" +
"<span class=\"spnLeftLabel30_per\" style=\"width: 26%;_width: 26%;*width: 26%;\"><label class=\"lbl\">Other Income:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per \">" +
"<input id=\"txtTotalOtherIncomeForSEPrBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"analystDetailsHelper.calculateFinaltTotalIncome(this.id, 'Pr','Se'," + identity + ")\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\" style=\"width: 33%;*width: 33%;_width: 33%;float: left;\">" +
"<span class=\"spnLeftLabel30_per\" style=\"width: 25%;_width: 25%;*width: 25%;\"><label class=\"lbl\">Total Income:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per\">" +
"<input id=\"txtTotalIncomeForSEPrBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<br/>" +
"<div id=\"divTutionForSEPrBank" + identity + "\" style=\"display:none;\">" +
"<div class=\"divHeadLineWhiteBg\" style=\"border: none; text-align: left;\"><span>TUTIONING TEACHER</span></div>" +
"<div class=\"div3in1\" style=\"border: solid 1px gray; padding: 2px;\">" +
"<div class=\"div3in1 widthSize25_per\" style=\"float: left; \">" +
"<span class=\"spnLeftLabel30_per widthSize35_per\"><label class=\"lbl\">No of Student:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per\" style=\" margin-bottom: 0px;\">" +
"<input id=\"txtNoOfStudentForSEPrBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"analystDetailsHelper.changeTution(this.id, 'Pr', 'Se'," + identity + ")\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1 widthSize25_per\" style=\"float: left;\">" +
"<span class=\"spnLeftLabel30_per widthSize55_per\">No of Days for Tution:</span>" +
"<div class=\"divLeft widthSize40_per \" style=\" margin-bottom: 0px; border:none; padding: 0px;\">" +
"<input id=\"txtNoOfDaysForTutionForSEPrBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"analystDetailsHelper.changeTution(this.id, 'Pr', 'Se'," + identity + ")\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1 widthSize25_per\" style=\"float: left;\">" +
"<span class=\"spnLeftLabel30_per widthSize20_per\" style=\"text-align: right;\">Fees:</span>" +
"<div class=\"divLeft divSizeWidth60_per\" style=\" margin-bottom: 0px;\">" +
"<input id=\"txtTutionFeeForSEPrBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"analystDetailsHelper.changeTution(this.id, 'Pr', 'Se'," + identity + ")\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1 widthSize25_per\" style=\"float: left;\">" +
"<span class=\"spnLeftLabel30_per widthSize35_per\"><label class=\"lbl\">Total Income:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per\" style=\" margin-bottom: 0px;\">" +
"<input id=\"txtTotalInComeTutionForSEPrBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<br/>" +
"<div id=\"divPrevRepHisForSEPrBank" + identity + "\" style=\" width:100%;_width:100%;*width:100%;float: left; display:none;\">" +
"<div class=\"divHeadLine widthSize100_per\" style=\"margin: 0px;\"><span>Previous Repayment History</span></div>" +
"<div class=\"groupContainer widthSize100_per\" style=\"margin: 0px;\">" +
"<div class=\"divLeftBig\" style=\"width: 407px;*width: 407px;_width: 407px;\"> " +
"<table style=\"width:100%;\">" +
"<tr><td>" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;\">OFF SCB" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" >" +
"<tr class=\"theader\">" +
"<td style=\"width: 35%;\">INSTITUTE</td>" +
"<td style=\"width: 35%;\" id=\"tdEM44\">EMI</td>" +
"<td style=\"width: 30%;\">CONSIDER</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"txtInstitute1ForOffSCBSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>" +
"<input type=\"hidden\" id=\"txtBank1SEPr" + identity + "\" />" +
"</td>" +
"<td><input id=\"txtEMI1ForOffSCBSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider1ForSEPrOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'SE','Pr')\" >" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"txtInstitute2ForOffSCBSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>" +
"<input type=\"hidden\" id=\"txtBank2SEPr" + identity + "\" />" +
"</td>" +
"<td><input id=\"txtEMI2ForOffSCBSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider2ForSEPrOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'SE','Pr')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"txtInstitute3ForOffSCBSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>" +
"<input type=\"hidden\" id=\"txtBank3SEPr" + identity + "\" />" +
"</td>" +
"<td><input id=\"txtEMI3ForOffSCBSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider3ForSEPrOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'SE','Pr')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"txtInstitute4ForOffSCBSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>" +
"<input type=\"hidden\" id=\"txtBank4SEPr" + identity + "\" />" +
"</td>" +
"<td><input id=\"txtEMI4ForOffSCBSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider4ForSEPrOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'SE','Pr')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td colspan=\"2\" style=\"text-align: right; font-weight: bold\">Total =</td>" +
"<td><input id=\"txttotalForOffSCBForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"</td>" +
"</tr>" +
"</table>" +
"</div >" +
"<div class=\"divRightBig\" style=\"width: 408px;*width: 408px;_width: 408px;\">" +
"<table style=\"width:100%;\">" +
"<tr><td>" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;\">ON SCB" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" >" +
"<tr class=\"theader\">" +
"<td style=\"width: 75%;*width: 75%;_width: 75%;\" id=\"tdEM45\">EMI</td>" +
"<td style=\"width: 25%;*width: 25%;_width: 25%;\">CONSIDER</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtEmi1ForOnScbForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider1ForOnSCBReplHisForSEPrBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'SE','Pr')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtEmi2ForOnScbForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider2ForOnSCBReplHisForSEPrBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'SE','Pr')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtEmi3ForOnScbForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider3ForOnSCBReplHisForSEPrBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'SE','Pr')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtEmi4ForOnScbForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider4ForOnSCBReplHisForSEPrBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'SE','Pr')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td style=\"text-align: right; font-weight: bold\">Total =</td>" +
"<td><input id=\"txtTotal1OnSCBRepHisForSEPrBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"</td>" +
"</tr>" +
"</table>" +
"</div >" +
"<div class =\"clear\"></div>" +
"</div>" +
"<div class=\"divHeaderTitel\">" +
"<div class=\"div3in1\" style=\"float: left;\">" +
"<span class=\"spnLeftLabel1\" style=\"padding-left: 22px;\"><label class=\"lbl\" style=\"font-size:medium; width: 285px;*width: 285px;_width: 285px;\" id=\"tdEM46\">TOTAL EMI CLOSING CONSIDERED:</label></span>" +
"<div class=\"divLeft\" style=\"width:10%;*width:10%;_width:10%;padding:0px; border:none;\">" +
"<input id=\"txtTotalEMIClosingConsideredForSEPrBank" + identity + "\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"font-size:medium;width: 97px;*width: 97px;_width: 97px; padding-right: 8px; text-align: right;\">Remarks:</label></span>" +
"<div class=\"divLeft\" style=\"width:40%;*width:40%;_width:40%;padding:0px; border:none;\">" +
"<input id=\"txtremarks1ForSEPrBank" + identity + "\" class=\"comboSizeActive\" type=\"text\"/>" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class =\"clear\"></div>" +
"</div>" +
"</div>" +
"<div class =\"clear\"></div>" +
"</div>";
            }
        }
        prhtml = olhtmlStart + lihtml + olhtmlEnd + bankhtml + "^" + tabName + "^" + tablink;
        return prhtml;
    },

    //Joint SelfEmployed Helper
    jtHtmlForSelfEmployed: function(res) {
        var prhtml = "";
        var olhtmlStart = "<ol class =\"toc\">";
        var olhtmlEnd = "</ol>";
        var lihtml = "";
        var bankhtml = "";
        var tabName = "";
        var tablink = "";
        for (var i = 0; i < res.value.AutoPRBankStatementList.length; i++) {
            var identity = i + 1;
            if (identity < 7) {
                lihtml += "<li id=\"jtBankStatementSelfEmployed" + identity + "\" style=\"*display:inline;\"><a id=\"lnkSEJtBank" + identity + "\" href=\"#jSelfEmployedBank-" + identity + "\" onclick=\"AnalystDetailsCalculationHelper.SelfEmployedTabShowHide('lnkSEJtBank" + identity + "','jSelfEmployedBank-" + identity + "', 'Jt');\"><span id=\"sejtBank" + identity + "\">Bank-" + identity + "</span></a></li>";
                var objTab = new Object();
                objTab.AccountType = 'Jt';
                objTab.TabName = "jSelfEmployedBank-" + identity;
                objTab.TabLink = 'lnkSEJtBank' + identity;
                Page.selfEmployedTabArray.push(objTab);
                if (tabName == "") {
                    tabName = 'jSelfEmployedBank-' + identity;
                }
                else {
                    tabName += "," + 'jSelfEmployedBank-' + identity;
                }

                if (tablink == "") {
                    tablink = 'lnkSEJtBank' + identity;
                }
                else {
                    tablink += "," + 'lnkSEJtBank' + identity;
                }

                bankhtml += "<div class=\"content\" id=\"jSelfEmployedBank-" + identity + "\" style=\"*border:0px;\">" +
"<div class=\"divLeftBig\" style=\"width :50%\">" +
"<div class=\"content\" id=\"Div61\" style=\"border: none; padding: 0px;\">" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\" >Account Name:</label>" +
"<input id=\"txtAccountNameForSEJtBank" + identity + "\" disabled=\"disabled\" class=\"txtFieldBig\" type=\"text\" style =\"width: 306px;*width: 306px;_width: 306px;\"/>" +
"<input type=\"hidden\" id=\"txtBankStatementIdSEJt" + identity + "\" />" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Account Number:</label>" +
"<input id=\"txtAccountNumberForSEJtBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 180px;*width: 180px;_width: 180px;\"/>" +
"<input id=\"txtAccountTypeForSEJtBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 120px;*width: 120px;_width: 120px;\"/>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Bank Name:</label>" +
"<select id=\"cmbBankNameForSEJtBank" + identity + "\" disabled=\"disabled\" class=\"comboSizeH_24\" style =\"width: 310px;*width: 310px;_width: 310px;\">" +
"</select>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Branch Name:</label>" +
"<select id=\"cmbbranchNameForSEJtBank" + identity + "\" disabled=\"disabled\" class=\"txtFieldBig\"  style =\"width: 310px;*width: 310px;_width: 310px;\">" +
"</select>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"divRightBig\"  style=\"width :45%\">" +
"<div class=\"fieldDiv\"><label class=\"lbl\">Remarks:</label></div>" +
"<div class=\"fieldDiv\"><textarea id=\"txtremarksForSEJtBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" cols=\"1\" rows=\"3\" style=\"height: 84px;_height: 84px;*height: 84px; width :99%;_width :99%;*width :99%;\"></textarea> </div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<div class=\"divLeftBig\" style=\"width :50%\">" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;\">" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" id=\"tblJtBankStatementForSEBank" + identity + "\">" +
"<tr class=\"theader\">" +
"<td style=\"width: 10%;\">MONTHS</td>" +
"<td style=\"width: 30%;\">CREDIT TURNOVER</td>" +
"<td style=\"width: 30%;\">AVERAGE BALANCE</td>" +
"<td style=\"width: 30%;\">ADJUSTMENT (-)</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn1ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth1ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver1ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver1ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance1ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance1ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment1ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment1ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn2ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth2ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver2ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver2ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance2ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance2ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment2ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment2ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn3ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth3ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver3ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver3ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance3ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance3ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment3ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment3ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn4ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth4ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver4ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver4ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance4ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance4ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment4ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment4ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn5ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth5ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver5ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver5ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance5ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance5ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment5ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment5ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn6ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth6ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver6ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver6ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance6ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance6ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment6ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment6ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn7ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth7ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver7ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver7ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance7ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance7ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment7ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment7ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn8ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth8ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver8ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver8ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance8ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance8ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment8ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment8ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn9ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth9ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver9ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver9ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance9ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance9ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment9ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment9ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn10ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth10ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver10ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver10ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance10ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance10ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment10ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment10ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn11ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth11ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver11ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver11ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance11ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance11ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment11ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment11ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<input id=\"hdn12ForSEJtBank" + identity + "\" type =\"hidden\" />" +
"<input id=\"txtMonth12ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtCreditTurnOver12ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtCreditTurnOver12ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAverageBalance12ForSEJtBank" + identity + "\" disabled=\"disabled\" type =\"text\" onchange=\"analystDetailsHelper.changeAverageBalanceForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAverageBalance12ForSEJtBank" + identity + "')\"/></td>" +
"<td><input id=\"txtAdjustment12ForSEJtBank" + identity + "\" type =\"text\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeCreditTurnoverForJtApplicantSelfEmployed(" + identity + ",'tblJtBankStatementForSEBank" + identity + "','txtAdjustment12ForSEJtBank" + identity + "')\"/></td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"<div class=\"divHeaderTitel\">" +
"<input type=\"button\" value=\"Bank Statement\"  style=\" cursor: pointer; border-radius: 3px;\" id=\"btnsejtBankStatement" + identity + "\" onclick=\"bankStatementHelper.showBankStatement(" + identity + ",'Jt', 'SE')\"/>" +
"<input type=\"hidden\" id=\"txtsejtStatementBank" + identity + "\" />" +
"</div>" +
"</div>" +
"<div class=\"divRightBig\"  style=\"width :45%;padding: 0px; border: none;\">" +
"<div class =\"divHeadLine\" style=\"margin: 0px; width: 99%;\"><span>OVER DRAFT FACILITIES</span></div>" +
"<div class = \"groupContainer\" style=\"margin: 0px; width: 99%;\">" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;padding: 2px;\">ON SCB" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" id=\"tblOverDraftFacilitiesForJtSEWithScb" + identity + "\">" +
"<tr class=\"theader\">" +
"<td >LIMIT</td>" +
"<td id=\"tdinterest40\">INTEREST</td>" +
"<td >CONSIDER</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtLimt1WithScbForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterst1WithScbForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider1WithScbForSEJtBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'SE')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtLimt2WithScbForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterst2WithScbForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider2WithScbForSEJtBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'SE')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtLimt3WithScbForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterst3WithScbForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider3WithScbForSEJtBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'SE')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtLimt4WithScbForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterst4WithScbForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider4WithScbForSEJtBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'SE')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;padding: 2px; margin-top: 5px;\">OFF-SCB" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" id=\"tblOverDraftFacilitiesForJtSEOffScbBank" + identity + "\">" +
"<tr class=\"theader\">" +
"<td >BANK/FI</td>" +
"<td >LIMIT</td>" +
"<td id=\"tdinterest41\">INTEREST</td>" +
"<td >CONSIDER</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<select id=\"cmbBankName1ForSEJtBank" + identity + "\" disabled=\"disabled\"></select>" +
"</td>" +
"<td><input id=\"txtLimit1ForSEJtOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterest1ForSEJtOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider1ForSEJtOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'SE')\">" +
"<option value = \"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<select id=\"cmbBankName2ForSEJtBank" + identity + "\" disabled=\"disabled\"></select>" +
"</td>" +
"<td><input id=\"txtLimit2ForSEJtOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterest2ForSEJtOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider2ForSEJtOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'SE')\">" +
"<option value = \"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<select id=\"cmbBankName3ForSEJtBank" + identity + "\" disabled=\"disabled\"></select>" +
"</td>" +
"<td><input id=\"txtLimit3ForSEJtOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterest3ForSEJtOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider3ForSEJtOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'SE')\">" +
"<option value = \"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"<tr>" +
"<td>" +
"<select id=\"cmbBankName4ForSEJtBank" + identity + "\" disabled=\"disabled\"></select>" +
"</td>" +
"<td><input id=\"txtLimit4ForSEJtOffSCBBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtInterest4ForSEJtOffSCBBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider4ForSEJtOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.changeConsiderForOverDraftWithScb(" + identity + ",'Jt', 'SE')\">" +
"<option value = \"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</td>" +
"</tr>" +
"</table>" +
"<input type=\"hidden\" id=\"txtTotallimitForOoverDraftSEJtBank" + identity + "\" />" +
"<input type=\"hidden\" id=\"txtTotalInterestForOverDraftSEJtBank" + identity + "\" />" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<div class=\"divLeftBig\" style=\"width :50%\">" +
"<div class=\"divMiddileSubHead\" style=\" width: 155px; font-size: small; border: none; padding: 0; margin: 0 0 0 119px ;\">12 MONTHS</div>" +
"<div class=\"divRightSubHead\" style=\" width: 155px; font-size: small; border: none ; padding: 0; margin: 0;\">6 MONTHS</div>" +
"<div class=\"clear\"></div>" +
"<div class=\"content\" id=\"Div76\" style=\"border: none; padding: 0;\">" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Total CTO:</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">" +
"<input id=\"txtTotalCTOForSEJt12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">" +
"<input id=\"txtTotalCTOForSEJt6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Average CTO</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">" +
"<input id=\"txtAverageCTOForSEJt12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">" +
"<input id=\"txtAverageCTOForSEJt6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Average Balance:</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">" +
"<input id=\"txtAverageBalanceForSEJt12MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">" +
"<input id=\"txtAverageBalanceForSEJt6MonthBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"divLeftBig\" style=\"width :50%\">" +
"<div class=\"content\" id=\"Div77\" style=\"border: none; padding: 0;\">" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Considered CTO:</label>" +
"<select id=\"cmbConsideredCtoForSEJtBank" + identity + "\" class=\"comboSizeActive\" style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Jt', 'SE')\">" +
"</select>" +
"<label class=\"lbl\" style=\"width: 112px;\">Considered Balance:</label>" +
"<select id=\"cmbConsideredBalanceForSEJtBank" + identity + "\" class=\"comboSizeActive\" style =\"width: 95px;*width: 95px;_width: 95px;\"></select>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Considered Margin:</label>" +
"<input id=\"txtConsiderMarginForSEJtBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" type=\"text\"  style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Jt', 'SE')\"/>" +
"<label class=\"lbl\">Gross Income:</label>" +
"<input id=\"txtgrossincomeForSEJtBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 95px;*width: 95px;_width: 95px;\"/>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Net After Payment:</label>" +
"<input id=\"txtNetAfterPaymentForSEJtBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 95px;*width: 95px;_width: 95px;\"/>" +
"<label class=\"lbl\">Share:</label>" +
"<input id=\"txtShareForSEJtBank" + identity + "\" class=\"txtFieldBig comboSizeActive\" type=\"text\"  style =\"width: 95px;*width: 95px;_width: 95px;\" onchange=\"analystDetailsHelper.changeConsiderCto(" + identity + ",'Jt', 'SE')\"/>" +
"<label id=\"lblShareForSEJtBank" + identity + "\" style=\"font-weight:bold;color:red;\"></label>" +
"</div>" +
"<div class=\"fieldDiv\">" +
"<label class=\"lbl\">Net Income:</label>" +
"<input id=\"txtNetincomeForSEJtBank" + identity + "\" class=\"txtFieldBig\" type=\"text\" disabled=\"disabled\" style =\"width: 310px;*width: 310px;_width: 310px;\"/>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"divRight\"  style=\"width :45%;padding: 0px; border: none; margin-top: -180px; float: right\">" +
"<div class = \"groupContainer\" style=\"margin: 0px; width: 99%; border-radius: 15px; padding-top: 5px;\">" +
"<div class=\"div3in1\">" +
"<label id=\"lblForAvgBalSEJtBank" + identity + "\" style=\"font-weight: bold;color:red;\"></label>" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Income Assessment:</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:36%;padding:0px; border:none;\">" +
"<select id=\"cmbIncomeAssessmentForSEJtBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changeIncomeAssementForSelfEmployed(" + identity + ",'Jt')\">" +
"</select>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;_width:30%;*_width:30%; padding:0px; border:none;\">" +
"<input id=\"txtIncomeAssessmentCodeForSEJtBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Industry:</label></span>" +
"<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">" +
"<select id=\"cmbIndustryForSEJtBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsManager.changeSectorForSelfEmployed(" + identity + ",'Jt')\">" +
"</select>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Sub-Sector:</label></span>" +
"<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">" +
"<select id=\"cmbSubsectorForSEJtBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changeSubSectorForSelfEmployed(" + identity + ",'Jt')\">" +
"<option value=\"-1\">Select a Sub-Sector</option>" +
"</select>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Private Income:</label></span>" +
"<div class=\"divLeft\" style=\"width:68%;*width:68%;_width:68%;padding:0px; border:none;\">" +
"<select id=\"cmbPrivateIncomeForSEJtBank" + identity + "\" class=\"comboSizeH_24 comboSizeActive\" onchange=\"analystDetailsHelper.changePrivateInCome(" + identity + ",'Jt','Se')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 120px;\">Profit Margin Range:</label></span>" +
"<div class=\"divLeft\" style=\"width:36%;*width:36%;_width:35%;padding:0px; border:none;\">" +
"<input id=\"txtProfitMarginForSEJtBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<div class=\"divMiddile\" style=\"width:30%;*width:30%;_width:29%; padding:0px; border:none;\">" +
"<input id=\"txtProfitMarginCodeForSEJtBank" + identity + "\" class=\"txtBoxSize_H22\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div id=\"divAverageBalanceForSEJt" + identity + "\" style=\"display:none;\">" +
"<div class=\"divHeadLine\" style=\"margin: 0px; width: 99%; background-color: white; text-align: right;border: none;\"><span style=\"color: black; margin-right: 50px;;\">AVERAGE BALANCE</span></div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;_width: 140px;*width: 140px;\">1ST 9 MONTHS:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input id=\"txt1st9monthForSEJtBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">1ST 6 MONTHS:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input id=\"txt1st6MonthForSEJtBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">LAST 6 MONTHS:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input id=\"txtLast6monthForSEJtBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;width: 140px;\">LAST 3 MONTHS:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input id=\"txtLast3monthForSEJtBank" + identity + "\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"width: 140px;*width: 140px;_width: 140px;\">CONSIDERED MARGIN:</label></span>" +
"<div class=\"divLeft\" style=\"width:62%;*width:62%;_width:62%;padding:0px; border:none;\">" +
"<input class=\"txtBoxSize_H22\" id=\"txtConsiderednarginForSEJtBank" + identity + "\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<div id=\"Div78\" style=\" width:100%;_width:100%;*width:100%;float: left; \">" +
"<div class=\"xgroupContainer\" style=\"width: 100%;*width: 100%;_width: 100%; border-radius:15px; padding-top: 5px; margin: 0;\">" +
"<div id=\"divChamberIncomeForSeJtBank" + identity + "\" style=\"display: none;\">" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;\">CHAMBER INCOME" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyleSmall\" style=\"text-align:center;border:solid 1px gray;\" id=\"tblChamberIncomeForSEJtBank" + identity + "\" >" +
"<tr class=\"theader\">" +
"<td style=\"text-align:left;\">CHAMBER NAME [+]</td>" +
"<td colspan=\"3\">" +
"<table style=\"width:100%; text-align:center;\">" +
"<tr>" +
"<td colspan=\"3\">FROM OLD PATIENTS</td>" +
"</tr>" +
"<tr>" +
"<td style=\"width:34%\">PATIENTS</td>" +
"<td style=\"width:33%\">FEE</td>" +
"<td style=\"width:33%\">TOTAL</td>" +
"</tr>" +
"</table>" +
"</td>" +
"<td colspan=\"3\">" +
"<table style=\"width:100%; text-align:center;\">" +
"<tr>" +
"<td colspan=\"3\">FROM NEW PATIENTS</td>" +
"</tr>" +
"<tr>" +
"<td style=\"width:34%\">PATIENTS</td>" +
"<td style=\"width:33%\">FEE</td>" +
"<td style=\"width:33%\">TOTAL</td>" +
"</tr>" +
"</table>" +
"</td>" +
"<td>TOTAL</td>" +
"<td>DAY/MONTHS</td>" +
"<td>INCOME</td>" +
"</tr>" +
"<tr>" +
"<td>CHAMBER-1</td>" +
"<td><input id=\"txtPatientsOld1ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeOld1ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalOld1ForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><input id=\"txtPatientsNew1ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeNew1ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalNew1ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtTotalOldNew1ForSEJt1Bank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtDayMonth1ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtIncome1ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"</tr>" +
"<tr>" +
"<td>CHAMBER-2</td>" +
"<td><input id=\"txtPatientsOld2ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeOld2ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalOld2ForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><input id=\"txtPatientsNew2ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeNew2ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalNew2ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtTotalOldNew2ForSEJt1Bank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtDayMonth2ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtIncome2ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"</tr>" +
"<tr>" +
"<td>CHAMBER-3</td>" +
"<td><input id=\"txtPatientsOld3ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeOld3ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalOld3ForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><input id=\"txtPatientsNew3ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeNew3ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalNew3ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtTotalOldNew3ForSEJt1Bank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtDayMonth3ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtIncome3ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"</tr>" +
"<tr>" +
"<td>CHAMBER-4</td>" +
"<td><input id=\"txtPatientsOld4ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeOld4ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalOld4ForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><input id=\"txtPatientsNew4ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtFeeNew4ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtTotalNew4ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\" /></td>" +
"<td><input id=\"txtTotalOldNew4ForSEJt1Bank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"<td><input id=\"txtDayMonth4ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type =\"text\" onchange=\"analystDetailsHelper.changePatients(this.id, 'Jt','Se'," + identity + ")\"/></td>" +
"<td><input id=\"txtIncome4ForSEJtBank" + identity + "\" type =\"text\"disabled=\"disabled\"/></td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\">" +
"<div class=\"div3in1\" style=\"width: 34%;*width: 34%;_width: 34%; float: left;\">" +
"<span class=\"spnLeftLabel30_per\" style=\"width: 32%;_width: 32%;*width: 32%;\"><label class=\"lbl\">Chamber Income:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per\">" +
"<input id=\"txtTotalChamberIncomeForSEJtBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\" style=\"width: 33%;*width: 33%;_width: 33%;float: left;\">" +
"<span class=\"spnLeftLabel30_per\" style=\"width: 26%;_width: 26%;*width: 26%;\"><label class=\"lbl\">Other Income:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per \">" +
"<input id=\"txtTotalOtherIncomeForSEJtBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"analystDetailsHelper.calculateFinaltTotalIncome(this.id, 'Jt','Se'," + identity + ")\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1\" style=\"width: 33%;*width: 33%;_width: 33%;float: left;\">" +
"<span class=\"spnLeftLabel30_per\" style=\"width: 25%;_width: 25%;*width: 25%;\"><label class=\"lbl\">Total Income:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per\">" +
"<input id=\"txtTotalIncomeForSEJtBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<br/>" +
"<div id=\"divTutionJtBank" + identity + "\" style=\"display:none;\">" +
"<div class=\"divHeadLineWhiteBg\" style=\"border: none; text-align: left;\"><span>TUTIONING TEACHER</span></div>" +
"<div class=\"div3in1\" style=\"border: solid 1px gray; padding: 2px;\">" +
"<div class=\"div3in1 widthSize25_per\" style=\"float: left; \">" +
"<span class=\"spnLeftLabel30_per widthSize35_per\"><label class=\"lbl\">No of Student:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per\" style=\" margin-bottom: 0px;\">" +
"<input id=\"txtNoOfStudentForSEJtBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"analystDetailsHelper.changeTution(this.id, 'Jt', 'Se'," + identity + ")\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1 widthSize25_per\" style=\"float: left;\">" +
"<span class=\"spnLeftLabel30_per widthSize55_per\">No of Days for Tution:</span>" +
"<div class=\"divLeft widthSize40_per \" style=\" margin-bottom: 0px; border:none; padding: 0px;\">" +
"<input id=\"txtNoOfDaysForTutionForSEJtBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"analystDetailsHelper.changeTution(this.id, 'Jt', 'Se'," + identity + ")\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1 widthSize25_per\" style=\"float: left;\">" +
"<span class=\"spnLeftLabel30_per widthSize20_per\" style=\"text-align: right;\">Fees:</span>" +
"<div class=\"divLeft divSizeWidth60_per\" style=\" margin-bottom: 0px;\">" +
"<input id=\"txtTutionFeeForSEJtBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22 comboSizeActive\" onchange=\"analystDetailsHelper.changeTution(this.id, 'Jt', 'Se'," + identity + ")\"/>" +
"</div>" +
"</div>" +
"<div class=\"div3in1 widthSize25_per\" style=\"float: left;\">" +
"<span class=\"spnLeftLabel30_per widthSize35_per\"><label class=\"lbl\">Total Income:</label></span>" +
"<div class=\"divLeft divSizeWidth60_per\" style=\" margin-bottom: 0px;\">" +
"<input id=\"txtTotalInComeTutionForSEJtBank" + identity + "\" type =\"text\" class=\"txtBoxSize_H22\" disabled=\"disabled\"/>" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class=\"clear\"></div>" +
"<br/>" +
"<div id=\"divPrevRepHisForSEJtBank" + identity + "\" style=\" width:100%;_width:100%;*width:100%;float: left; display:none;\">" +
"<div class=\"divHeadLine widthSize100_per\" style=\"margin: 0px;\"><span>Previous Repayment History</span></div>" +
"<div class=\"groupContainer widthSize100_per\" style=\"margin: 0px;\">" +
"<div class=\"divLeftBig\" style=\"width: 407px;*width: 407px;_width: 407px;\"> " +
"<table style=\"width:100%;\">" +
"<tr><td>" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;\">OFF SCB" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" >" +
"<tr class=\"theader\">" +
"<td style=\"width: 35%;\">INSTITUTE</td>" +
"<td style=\"width: 35%;\" id=\"tdEM62\">EMI</td>" +
"<td style=\"width: 30%;\">CONSIDER</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtInstitute1ForOffSCBSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>" +
"<input type=\"hidden\" id=\"txtBank1SEJt" + identity + "\" />" +
"</td>" +
"<td><input id=\"txtEMI1ForOffSCBSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider1ForSEJtRepHisOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'SE','Jt')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtInstitute2ForOffSCBSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>" +
"<input type=\"hidden\" id=\"txtBank2SEJt" + identity + "\" />" +
"</td>" +
"<td><input id=\"txtEMI2ForOffSCBSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider2ForSEJtRepHisOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'SE','Jt')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtInstitute3ForOffSCBSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>" +
"<input type=\"hidden\" id=\"txtBank3SEJt" + identity + "\" />" +
"</td>" +
"<td><input id=\"txtEMI3ForOffSCBSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider3ForSEJtRepHisOffScbBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'SE','Jt')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtInstitute4ForOffSCBSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/>" +
"<input type=\"hidden\" id=\"txtBank4SEJt" + identity + "\" />" +
"</td>" +
"<td><input id=\"txtEMI4ForOffSCBSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider4ForSEJtRepHisOffScbBank1\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHis(" + identity + ",'SE','Jt')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td colspan=\"2\" style=\"text-align: right; font-weight: bold\">Total =</td>" +
"<td><input id=\"txttotalForOffSCBForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"</td>" +
"</tr>" +
"</table>" +
"</div > " +
"<div class=\"divRightBig\" style=\"width: 408px;*width: 408px;_width: 408px;\">  " +
"<table style=\"width:100%;\">" +
"<tr><td>" +
"<div style=\"text-align:left; font-size:medium;font-weight:bold;\">ON SCB" +
"<div style=\"border:none;\">" +
"<table class=\"tableStyle\" >" +
"<tr class=\"theader\">" +
"<td style=\"width: 75%;*width: 75%;_width: 75%;\" id=\"tdEM63\">EMI</td>" +
"<td style=\"width: 25%;*width: 25%;_width: 25%;\">CONSIDER</td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtEmi1ForOnScbForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider1ForOnSCBReplHisForSEJtBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'SE','Jt')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtEmi2ForOnScbForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider2ForOnSCBReplHisForSEJtBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'SE','Jt')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtEmi3ForOnScbForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider3ForOnSCBReplHisForSEJtBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'SE','Jt')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td><input id=\"txtEmi4ForOnScbForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"<td><select id=\"cmbConsider4ForOnSCBReplHisForSEJtBank" + identity + "\" class=\"comboSizeActive\" onchange=\"analystDetailsHelper.considerPrevrepHisOnScb(" + identity + ",'SE','Jt')\">" +
"<option value=\"0\">No</option>" +
"<option value=\"1\">Yes</option>" +
"</select></td>" +
"</tr>" +
"<tr>" +
"<td style=\"text-align: right; font-weight: bold\">Total =</td>" +
"<td><input id=\"txtTotal1OnSCBRepHisForSEJtBank" + identity + "\" type =\"text\" disabled=\"disabled\"/></td>" +
"</tr>" +
"</table>" +
"</div>" +
"</div>" +
"</td>" +
"</tr>" +
"</table>" +
"</div >" +
"<div class =\"clear\"></div>" +
"</div>" +
"<div class=\"divHeaderTitel\">" +
"<div class=\"div3in1\" style=\"float: left;\">" +
"<span class=\"spnLeftLabel1\" style=\"padding-left: 22px;\"><label class=\"lbl\" style=\"font-size:medium; width: 285px;*width: 285px;_width: 285px;\" id=\"tdEM64\">TOTAL EMI CLOSING CONSIDERED:</label></span>" +
"<div class=\"divLeft\" style=\"width:10%;*width:10%;_width:10%;padding:0px; border:none;\">" +
"<input id=\"txtTotalEMIClosingConsideredForSEJtBank" + identity + "\" type=\"text\" disabled=\"disabled\"/>" +
"</div>" +
"<span class=\"spnLeftLabel1\"><label class=\"lbl\" style=\"font-size:medium;width: 97px;*width: 97px;_width: 97px; padding-right: 8px; text-align: right;\">Remarks:</label></span>" +
"<div class=\"divLeft\" style=\"width:40%;*width:40%;_width:40%;padding:0px; border:none;\">" +
"<input id=\"txtremarks1ForSEJtBank" + identity + "\" class=\"comboSizeActive\" type=\"text\"/>" +
"</div>" +
"</div>" +
"</div>" +
"</div>" +
"<div class =\"clear\"></div>" +
"</div>" +
"</div>" +
"<div class =\"clear\"></div>" +
"</div>";
            }
        }
        prhtml = olhtmlStart + lihtml + olhtmlEnd + bankhtml + "^" + tabName + "^" + tablink;
        return prhtml;
    }
};


var bankStatementHelper = {
    showBankStatement: function(bankNo, accountType, tabName) {

        var statement = "";
        var spreadsheet = document.getElementById("BankStatementAVG");
        var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
        spreadsheet.XMLData = blankData;
        if (accountType == "Pr" && tabName == "BM") {
            var bankName = $('cmbBankNameForBank' + bankNo + 'IncomeGenerator').options[$('cmbBankNameForBank' + bankNo + 'IncomeGenerator').selectedIndex].text;
            $('spnBankStatementName').innerHTML = bankName;
            statement = $F("txtbmprStatementBank" + bankNo);
            if (statement != "") {
                spreadsheet.XMLData = statement;
            }
        }
        if (accountType == "Jt" && tabName == "BM") {
            var bankName = $('cmbBankName1ForJointBank' + bankNo).options[$('cmbBankName1ForJointBank' + bankNo).selectedIndex].text;
            $('spnBankStatementName').innerHTML = bankName;
            statement = $F("txtbmjtStatementBank" + bankNo);
            if (statement != "") {
                spreadsheet.XMLData = statement;
            }
        }
        if (accountType == "Pr" && tabName == "SE") {
            var bankName = $('cmbBankNameForSEBank' + bankNo).options[$('cmbBankNameForSEBank' + bankNo).selectedIndex].text;
            $('spnBankStatementName').innerHTML = bankName;
            statement = $F("txtseprStatementBank" + bankNo);
            if (statement != "") {
                spreadsheet.XMLData = statement;
            }
        }
        if (accountType == "Jt" && tabName == "SE") {
            var bankName = $('cmbBankNameForSEJtBank' + bankNo).options[$('cmbBankNameForSEJtBank' + bankNo).selectedIndex].text;
            $('spnBankStatementName').innerHTML = bankName;
            statement = $F("txtsejtStatementBank" + bankNo);
            if (statement != "") {
                spreadsheet.XMLData = statement;
            }
        }

        util.showModal('bankStatementPopupDiv');

    },
    closePopup: function() {

        util.hideModal.curry('bankStatementPopupDiv');
        var spreadsheet = document.getElementById("BankStatementAVG");
        var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
        spreadsheet.XMLData = blankData;

    }


};