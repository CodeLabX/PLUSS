﻿
var InsurenceType = { 1: 'General', 2: 'Life', 3: 'General & Life' };
var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    insurence: null,
    IRSettingArray: [],

    init: function() {
    util.prepareDom();
    Event.observe('lnkAddNewIRSettingsForVehicle', 'click', IRVehicleSettingsHelper.update);
    Event.observe('lnkSave', 'click', IRVehicleSettingsManager.save);
//    Event.observe('btnSearch', 'click', IRVehicleSettingsManager.render);
    Event.observe('lnkClose', 'click', util.hideModal.curry('IRSettingsPopupDiv'));
    
//    Event.observe('lnkUploadSave', 'click', Page.upload);
    
//    Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });
        
        IRVehicleSettingsManager.render();
        IRVehicleSettingsManager.getStatus();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        IRVehicleSettingsManager.render();
        IRVehicleSettingsManager.getStatus();
    }

};

var IRVehicleSettingsManager = {

    render: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.IRSettingsForVehicleStatus.GetAutoVehicleStatusIRSummary(Page.pageNo, Page.pageSize);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        IRVehicleSettingsHelper.renderCollection(res);
    },
    getStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.IRSettingsForVehicleStatus.GetStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        IRVehicleSettingsHelper.populateStatusCombo(res);
    },


    save: function() {
        if (!util.validateEmpty('txtIRWithARTA')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        var IRWithARTA = $F('txtIRWithARTA');
        if (!util.isFloat(IRWithARTA)) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        if (!util.validateEmpty('cmbVehicleStatus')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }


        var a = $F('txtIRWithoutARTA');
        if (!util.isFloat(a)) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        var AutoVehicleStatusIR = Page.insurence;
        AutoVehicleStatusIR.IrWithArta = $F('txtIRWithARTA');
        AutoVehicleStatusIR.VehicleStatus = $F('cmbVehicleStatus');
        AutoVehicleStatusIR.IrWithoutArta = $F('txtIRWithoutARTA');

        var res = PlussAuto.IRSettingsForVehicleStatus.SaveAuto_IRVehicleStatus(AutoVehicleStatusIR);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("IR Settings for Vehicle Save successfully");
        }

        IRVehicleSettingsManager.render();
        util.hideModal('IRSettingsPopupDiv');
    },
    VehicleStatusIRDeleteById: function(vehicleStatusIRId) {
        var res = PlussAuto.IRSettingsForVehicleStatus.IRVehicleStatusDeleteById(vehicleStatusIRId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("IR Settings for Vehicle Delete successfully");
        }

        IRVehicleSettingsManager.render();
    }
};
var IRVehicleSettingsHelper = {
    renderCollection: function(res) {
        Page.IRSettingArray = res.value;
        var html = [];
        for (var i = 0; i < Page.IRSettingArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            Page.IRSettingArray[i].InsTypeName = InsurenceType[Page.IRSettingArray[i].InsType];
            Page.IRSettingArray[i]._LastupdateDate = Page.IRSettingArray[i].LastUpdateDate.format();
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{StatusName}</td><td>#{IrWithArta}</td><td>#{IrWithoutArta}</td><td>#{_LastupdateDate}</td><td><a title="Edit" href="javascript:;" onclick="IRVehicleSettingsHelper.update(#{VehicleStatusIRId})" class="icon iconEdit"></a> \
                            <a title="Delete" href="javascript:;" onclick="IRVehicleSettingsHelper.remove(#{VehicleStatusIRId})" class="icon iconDelete"></a></td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.IRSettingArray[i])
			     );
        }
        $('IRSettingsList').update(html.join(''));
        if (res.value.length != 0) {
            Page.pager.reset(Page.IRSettingArray[0].TotalCount, Page.pageNo);
        }


    },
    update: function(vehicleStatusIRId) {
        $$('#IRSettingsPopupDiv .txt').invoke('clear');
        IRVehicleSettingsHelper.clearForm();

        var insurence = Page.IRSettingArray.findByProp('VehicleStatusIRId', vehicleStatusIRId);
        Page.insurence = insurence || { vehicleStatusIRId: util.uniqId() };
        if (insurence) {
            $('txtIRWithARTA').setValue(insurence.IrWithArta);
            $('cmbVehicleStatus').setValue(insurence.VehicleStatus);
            $('txtIRWithoutARTA').setValue(insurence.IrWithoutArta);
            //            $('txtVehicleCC').setValue(insurence.LastUpdateDate);
            ////            $('txtInsurancePer').setValue(insurence.InsurancePercent);
        }
        else {
            $('cmbVehicleStatus').setValue('-1');
        }

        util.showModal('IRSettingsPopupDiv');
    },
    remove: function(VehicleStatusIRId) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            IRVehicleSettingsManager.VehicleStatusIRDeleteById(VehicleStatusIRId);
        }

    },
    clearForm: function() {
        $('txtIRWithARTA').setValue('');
        $('cmbVehicleStatus').setValue('1');
        $('txtIRWithoutARTA').setValue('');
        //        $('txtVehicleCC').setValue('');
        //        $('txtInsurancePer').setValue('');
    },
    //    searchKeypress: function(e) {
    //        if (event.keyCode == 13) {
    //            IRVehicleSettingsManager.render();
    //        }
    //    },
    populateStatusCombo: function(res) {
        document.getElementById('cmbVehicleStatus').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmbVehicleStatus').options.add(opt);
        }

        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVehicleStatus').options.add(ExtraOpt);


    }

};

Event.onReady(Page.init);
