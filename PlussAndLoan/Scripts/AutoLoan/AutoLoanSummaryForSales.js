﻿var vehicleType = { 'Sedan': 'S1', 'Station wagon': 'S2', 'SUV': 'S3', 'Pick-Ups': 'S4', 'Microbus': 'S5', 'MPV': 'S6' };

var Page = {
    pageSize: 20,
    pageNo: 0,
    pager: null,
    ApplicationStatusListArray: [],
    LoanSummeryArray: [],
    vehicleId: 0,
    modelArray: [],
    insurenceAndBanka: [],
    vehicleDetailsArray: [],
    
    init: function() {
        util.prepareDom();
        Event.observe('btnLLIdSearch', 'click', loanSummeryForSalesManager.getLoanSummarySearch);
        Event.observe('lnkPrint', 'click', loanSummeryForSalesHelper.printPreview);
        Event.observe('lnkClose', 'click', loanSummeryForSalesHelper.closeLoanApplication);
        Event.observe('lnkSave', 'click', loanSummeryForSalesManager.appealForThisLLID);
        Event.observe('lnkClosePopup', 'click', util.hideModal.curry('newLLPopupDiv'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNo'),
            spnTotalPage: $('spntotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });


        Event.observe('cmbGeneralInsurance', 'change', loanSummeryForSalesHelper.changeGeneralInsurence);
        Event.observe('cmbLifeInsurance', 'change', loanSummeryForSalesHelper.changeLifeInsurence);

        loanSummeryForSalesManager.getLoanSummary();
        loanSummeryForSalesManager.getBank();
        loanSummeryForSalesManager.getVendor();
        loanSummeryForSalesManager.getManufacturer();
        loanSummeryForSalesManager.getProfession();
        loanSummeryForSalesManager.getNameOfCompany();
        loanSummeryForSalesManager.getVehicleStatus();
        loanSummeryForSalesManager.getGeneralInsurance();
        loanSummeryForSalesManager.GetAllSource();
        loanSummeryForSalesManager.getFacility();


    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        loanSummeryForSalesManager.getLoanSummary(Page.pageNo, Page.pageSize);
    }
};

var loanSummeryForSalesManager = {
    getLoanSummary: function() {
        var searchKey = $F('txtLLIDSearch');
        if (searchKey == "") {
            searchKey = 0;
        }
        if (util.isDigit(searchKey)) {
            var res = PlussAuto.AutoLoanSummaryForSales.GetLoanSummary(Page.pageNo, Page.pageSize, searchKey);
            //debugger;
            if (res.error) {
                alert(res.error.Message);
                return;
            }
            Page.LoanSummeryArray = res.value;

            loanSummeryForSalesHelper.populateLoanSummeryGrid(res);
        } else {
            alert("LLID Must to be in Degit");
            return false;
        }
    },
    appealForThisLLID: function() {

        var hdllid = $F("hdnLLID");
        var newLLId = "0";
        var masterId = $F("hdnMasterId");
        var llIdType = document.getElementsByName('group1');
        var val = "1";
        for (var i = 0; i < llIdType.length; i++) {
            if (llIdType[i].checked) {
                val = llIdType[i].value
            }
        }
        if (val == 2) {
            newLLId = $F("txtNewLLID");
        }
        var res = PlussAuto.AutoLoanSummaryForSales.appealForThisLLID(hdllid, newLLId, masterId, val);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            if (res.value == "10 Days Expired") {
                alert("Your 10 days appealed days is over. Now you must create a new LLID to appealed this loan");
            }
            else {
                alert("Appealed  Successfully");
                loanSummeryForSalesManager.getLoanSummary();
                util.hideModal.curry('newLLPopupDiv');
            }
        }

    },
    getLoanSummarySearch: function() {
        Page.pageNo = 0;
        loanSummeryForSalesManager.getLoanSummary();
    },
    getBank: function() {

        var res = PlussAuto.AutoLoanSummaryForSales.GetBank(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        loanSummeryForSalesHelper.populateBankCombo(res);
    },
    getVendor: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanSummaryForSales.GetVendor();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        //        Page.vendorArray = res.value;

        document.getElementById('cmbVendor').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vendor';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVendor').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].VendorName;
            opt.value = res.value[i].AutoVendorId;
            document.getElementById('cmbVendor').options.add(opt);
        }

    },
    getManufacturer: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanSummaryForSales.GetAutoManufacturer();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        //        Page.manufacturerArray = res.value;
        //
        document.getElementById('cmbManufacturer').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Manufacturer';
        ExtraOpt.value = '-1';
        document.getElementById('cmbManufacturer').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ManufacturerName;
            opt.value = res.value[i].ManufacturerId;
            document.getElementById('cmbManufacturer').options.add(opt);
        }

    },
    getProfession: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanSummaryForSales.GetProfession();

        if (res.error) {
            alert(res.error.Message);
            return;
        }

        document.getElementById('cmbPApplicantPrimaryProfession').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Primary Profession';
        ExtraOpt.value = '-1';
        document.getElementById('cmbPApplicantPrimaryProfession').options.add(ExtraOpt);

        document.getElementById('cmbJApplicantPrimaryProfession').options.length = 0;
        var ExtraOpt1 = document.createElement("option");
        ExtraOpt1.text = 'Select Primary Profession';
        ExtraOpt1.value = '-1';
        document.getElementById('cmbJApplicantPrimaryProfession').options.add(ExtraOpt1);


        document.getElementById('cmbReferenceOccupation1').options.length = 0;
        var ExtraOpt2 = document.createElement("option");
        ExtraOpt2.text = 'Select Occupation';
        ExtraOpt2.value = '-1';
        document.getElementById('cmbReferenceOccupation1').options.add(ExtraOpt2);

        document.getElementById('cmbReferenceOccupation2').options.length = 0;
        var ExtraOpt3 = document.createElement("option");
        ExtraOpt3.text = 'Select Occupation';
        ExtraOpt3.value = '-1';
        document.getElementById('cmbReferenceOccupation2').options.add(ExtraOpt3);


        document.getElementById('cmbPApplicantOtherProfession').options.length = 0;
        var ExtraOpt4 = document.createElement("option");
        ExtraOpt4.text = 'Select Other Profession';
        ExtraOpt4.value = '-1';
        document.getElementById('cmbPApplicantOtherProfession').options.add(ExtraOpt4);

        document.getElementById('cmbJApplicantOtherProfession').options.length = 0;
        var ExtraOpt5 = document.createElement("option");
        ExtraOpt5.text = 'Select Other Profession';
        ExtraOpt5.value = '-1';
        document.getElementById('cmbJApplicantOtherProfession').options.add(ExtraOpt5);


        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ProfName;
            opt.value = res.value[i].ProfID;
            document.getElementById('cmbPApplicantPrimaryProfession').options.add(opt);

            var opt1 = document.createElement("option");
            opt1.text = res.value[i].ProfName;
            opt1.value = res.value[i].ProfID;
            document.getElementById('cmbJApplicantPrimaryProfession').options.add(opt1);


            var opt2 = document.createElement("option");
            opt2.text = res.value[i].ProfName;
            opt2.value = res.value[i].ProfID;
            document.getElementById('cmbPApplicantOtherProfession').options.add(opt2);

            var opt3 = document.createElement("option");
            opt3.text = res.value[i].ProfName;
            opt3.value = res.value[i].ProfID;
            document.getElementById('cmbJApplicantOtherProfession').options.add(opt3);

            var opt4 = document.createElement("option");
            opt4.text = res.value[i].ProfName;
            opt4.value = res.value[i].ProfID;
            document.getElementById('cmbReferenceOccupation1').options.add(opt4);

            var opt5 = document.createElement("option");
            opt5.text = res.value[i].ProfName;
            opt5.value = res.value[i].ProfID;
            document.getElementById('cmbReferenceOccupation2').options.add(opt5);

        }
    },
    getNameOfCompany: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanSummaryForSales.getNameOfCompany();

        if (res.error) {
            alert(res.error.Message);
            return;
        }

        document.getElementById('cmbPApplicantNameOfCompany').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Company';
        ExtraOpt.value = '-1';
        document.getElementById('cmbPApplicantNameOfCompany').options.add(ExtraOpt);

        document.getElementById('cmbJApplicantNameOfCompany').options.length = 0;
        var ExtraOpt1 = document.createElement("option");
        ExtraOpt1.text = 'Select Company';
        ExtraOpt1.value = '-1';
        document.getElementById('cmbJApplicantNameOfCompany').options.add(ExtraOpt1);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].CompanyName;
            opt.value = res.value[i].AutoCompanyId;
            document.getElementById('cmbPApplicantNameOfCompany').options.add(opt);

            var opt1 = document.createElement("option");
            opt1.text = res.value[i].CompanyName;
            opt1.value = res.value[i].AutoCompanyId;
            document.getElementById('cmbJApplicantNameOfCompany').options.add(opt1);
        }

    },
    getVehicleStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanSummaryForSales.getVehicleStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmdVehicleStatus').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Vehicle Status';
        ExtraOpt.value = '-1';
        document.getElementById('cmdVehicleStatus').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmdVehicleStatus').options.add(opt);
        }
    },
    getGeneralInsurance: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanSummaryForSales.getGeneralInsurance();

        if (res.error) {
            alert(res.error.Message);
            return;
        }

        document.getElementById('cmbGeneralInsurance').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'No Insurance';
        ExtraOpt.value = '-1';
        document.getElementById('cmbGeneralInsurance').options.add(ExtraOpt);

        document.getElementById('cmbLifeInsurance').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'No Insurance';
        ExtraOpt.value = '-1';
        document.getElementById('cmbLifeInsurance').options.add(ExtraOpt);
        Page.insurenceAndBanka.push(res.value);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            var opt1 = document.createElement("option");
            var insType = res.value[i].InsType;

            if (insType == '1' || insType == '3') {
                opt.text = res.value[i].CompanyName;
                opt.value = res.value[i].InsCompanyId;

                document.getElementById('cmbGeneralInsurance').options.add(opt);
            }
            if (insType == '2' || insType == '3') {
                opt1.text = res.value[i].CompanyName;
                opt1.value = res.value[i].InsCompanyId;

                document.getElementById('cmbLifeInsurance').options.add(opt1);
            }
        }
    },
    GetAllSource: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.AutoLoanSummaryForSales.GetAllSource();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        document.getElementById('cmbSource').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Source';
        ExtraOpt.value = '-1';
        document.getElementById('cmbSource').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].SourceName;
            opt.value = res.value[i].SourceId;
            document.getElementById('cmbSource').options.add(opt);
        }
    },
    getFacility: function() {

        var res = PlussAuto.AutoLoanSummaryForSales.getFacility();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        loanSummeryForSalesHelper.populateFacilityCombo(res);
    },
    GetInfoByLLID: function() {

        var searchKey = $F('txtLLID');

        var res = PlussAuto.AutoLoanSummaryForSales.getLoanLocetorInfoByLLID(searchKey);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else if (res.value == 'No Data') {
            alert('No Data Found. Invalid LLID');

            var texts = document.getElementsByTagName('input');
            for (var i_tem = 0; i_tem < texts.length; i_tem++) {
                if (texts[i_tem].type == 'text') {
                    texts[i_tem].value = '';
                }
            }
            $('txtAutoLoanMasterId').setValue('0');
            $('txtPApplicantID').setValue('0');
            $('txtJApplicantID').setValue('0');
            $('txtPrProfessionId').setValue('0');
            $('txtJtProfessionId').setValue('0');
            $('txtAutoLoanVehicleId').setValue('0');
            $('txtPrAccountId').setValue('0');
            $('txtJtAccountId').setValue('0');
            $('txtPrAccountDetailsId1').setValue('0');
            $('txtPrAccountDetailsId2').setValue('0');
            $('txtPrAccountDetailsId3').setValue('0');
            $('txtPrAccountDetailsId4').setValue('0');
            $('txtPrAccountDetailsId5').setValue('0');
            $('txtJtAccountDetailsId1').setValue('0');
            $('txtJtAccountDetailsId2').setValue('0');
            $('txtJtAccountDetailsId3').setValue('0');
            $('txtJtAccountDetailsId4').setValue('0');
            $('txtJtAccountDetailsId5').setValue('0');
            $('txtPrFinanceId').setValue('0');
            $('txtJtFinanceId').setValue('0');
            $('txtReferenceId').setValue('0');
            $('txtFacilityId1').setValue('0');
            $('txtFacilityId2').setValue('0');
            $('txtFacilityId3').setValue('0');
            $('txtFacilityId4').setValue('0');
            $('txtFacilityId5').setValue('0');
            $('txtSecurityId1').setValue('0');
            $('txtSecurityId2').setValue('0');
            $('txtSecurityId3').setValue('0');
            $('txtSecurityId4').setValue('0');
            $('txtSecurityId5').setValue('0');


            return;
        }
        var name = res.value.ApplicantName;
        if (name != undefined) {

            $('txtPApplicantName').setValue(res.value.ApplicantName);
            $('txtSourceNAme').setValue(res.value.SourcePersonName);
            $('txtReceiveDate').setValue(res.value.RecieveDate.format());
            $('txtAutoLoanMasterId').setValue('0');
            $('txtPApplicantID').setValue('0');
            $('txtJApplicantID').setValue('0');
            $('txtPrProfessionId').setValue('0');
            $('txtJtProfessionId').setValue('0');
            $('txtAutoLoanVehicleId').setValue('0');
            $('txtPrAccountId').setValue('0');
            $('txtJtAccountId').setValue('0');
            $('txtPrAccountDetailsId1').setValue('0');
            $('txtPrAccountDetailsId2').setValue('0');
            $('txtPrAccountDetailsId3').setValue('0');
            $('txtPrAccountDetailsId4').setValue('0');
            $('txtPrAccountDetailsId5').setValue('0');
            $('txtJtAccountDetailsId1').setValue('0');
            $('txtJtAccountDetailsId2').setValue('0');
            $('txtJtAccountDetailsId3').setValue('0');
            $('txtJtAccountDetailsId4').setValue('0');
            $('txtJtAccountDetailsId5').setValue('0');
            $('txtPrFinanceId').setValue('0');
            $('txtJtFinanceId').setValue('0');
            $('txtReferenceId').setValue('0');
            $('txtFacilityId1').setValue('0');
            $('txtFacilityId2').setValue('0');
            $('txtFacilityId3').setValue('0');
            $('txtFacilityId4').setValue('0');
            $('txtFacilityId5').setValue('0');
            $('txtSecurityId1').setValue('0');
            $('txtSecurityId2').setValue('0');
            $('txtSecurityId3').setValue('0');
            $('txtSecurityId4').setValue('0');
            $('txtSecurityId5').setValue('0');
        }
        else {
            var sourceName = res.value.objAutoLoanMaster.SourceName;
            if (sourceName != '' && sourceName != undefined) {
                Page.loanApplication = res.value;
                // Header Section
                $('cmbProductName').setValue(res.value.objAutoLoanMaster.ProductId);
                $('txtSourceNAme').setValue(res.value.objAutoLoanMaster.SourceName);
                $('cmbSource').setValue(res.value.objAutoLoanMaster.Source);
                $('txtSourceCode').setValue(res.value.objAutoLoanMaster.SourceCode);
                $('txtAppliedDate').setValue(res.value.objAutoLoanMaster.AppliedDate.format());
                $('txtAppliedAmount').setValue(res.value.objAutoLoanMaster.AppliedAmount);
                $('txtReceiveDate').setValue(res.value.objAutoLoanMaster.ReceiveDate.format());
                $('cmbAskingTenor').setValue(res.value.objAutoLoanMaster.AskingTenor);
                $('txtAskingRate').setValue(res.value.objAutoLoanMaster.AskingRate);
                $('txtAutoLoanMasterId').setValue(res.value.objAutoLoanMaster.Auto_LoanMasterId);
                if (res.value.objAutoLoanMaster.JointApplication == false) {
                    $('cmbJointApplication').setValue('0');
                }
                else {
                    $('cmbJointApplication').setValue('1');
                }
                loanSummeryForSalesHelper.hideShowDivBasedOn_jointApplicationDiv();
                //            if(res.value.objAutoLoanMaster.JointApplication == cmbJointApplication

                // Applicant Primary
                $('txtPApplicantID').setValue(res.value.objAutoPRApplicant.PrApplicantId);
                $('txtPApplicantName').setValue(res.value.objAutoPRApplicant.PrName);
                $('cmbGenderForPrimary').setValue(res.value.objAutoPRApplicant.Gender);
                $('txtPApplicantRelationshipNumber').setValue(res.value.objAutoPRApplicant.RelationShipNumber);
                $('txtPApplicantTINNumber').setValue(res.value.objAutoPRApplicant.TINNumber);
                $('txtPApplicantFatherName').setValue(res.value.objAutoPRApplicant.FathersName);
                $('txtPApplicantMotherName').setValue(res.value.objAutoPRApplicant.MothersName);
                $('txtPApplicantDateOfBirth').setValue(res.value.objAutoPRApplicant.DateofBirth.format());
                $('cmbPApplicantMaritalStatus').setValue(res.value.objAutoPRApplicant.MaritalStatus);
                $('txtPApplicantSpouseName').setValue(res.value.objAutoPRApplicant.SpouseName);
                $('txtPApplicantSpouseProfession').setValue(res.value.objAutoPRApplicant.SpouseProfession);
                $('txtPApplicantSpouseWorkAddress').setValue(res.value.objAutoPRApplicant.SpouseWorkAddress);
                $('txtPApplicantSpouseContactNumber').setValue(res.value.objAutoPRApplicant.SpouseContactNumber);
                $('txtPApplicantRelationshipWithPrimary').setValue(res.value.objAutoPRApplicant.RelationShipwithPrimary);
                $('txtPApplicantNationality').setValue(res.value.objAutoPRApplicant.Nationality);
                $('cmbPApplicantIdentificationNumber').setValue(res.value.objAutoPRApplicant.IdentificationNumberType);
                $('txtPApplicantIdentificationNumber').setValue(res.value.objAutoPRApplicant.IdentificationNumber);
                $('cmbPApplicantIdentificationDocument').setValue(res.value.objAutoPRApplicant.IdentificationDocument);
                $('txtPApplicantNumberOfDependents').setValue(res.value.objAutoPRApplicant.NumberofDependents);
                $('cmbPApplicantHighestEducationLevel').setValue(res.value.objAutoPRApplicant.HighestEducation);
                $('txtPApplicantResidenceAddress').setValue(res.value.objAutoPRApplicant.ResidenceAddress);
                $('txtPApplicantMailingAddress').setValue(res.value.objAutoPRApplicant.MailingAddress);
                $('txtPApplicantPermanentAddress').setValue(res.value.objAutoPRApplicant.PermanentAddress);
                $('cmbPApplicantResidenceStatus').setValue(res.value.objAutoPRApplicant.ResidenceStatus);
                $('cmbPApplicantOwnershipDocument').setValue(res.value.objAutoPRApplicant.OwnershipDocument);
                $('cmbPApplicantResidenceStatusYear').setValue(res.value.objAutoPRApplicant.ResidenceStatusYear);
                $('txtPApplicantPhoneRescident').setValue(res.value.objAutoPRApplicant.PhoneResidence);
                $('txtpApplicantPhoneMobile').setValue(res.value.objAutoPRApplicant.Mobile);
                $('txtPApplicantEmail').setValue(res.value.objAutoPRApplicant.Email);
                $('cmbPAllicantDirectorShipWithAnyBank').setValue(res.value.objAutoPRApplicant.DirectorshipwithanyBank);
                // Applicant Primary End

                // Applicant JOINT
                $('txtJApplicantID').setValue(res.value.objAutoPRApplicant.JtApplicantId);
                $('txtJApplicantName').setValue(res.value.objAutoJTApplicant.JtName);
                $('cmbJointGender').setValue(res.value.objAutoJTApplicant.Gender);
                $('txtJApplicantRelationshipNumber').setValue(res.value.objAutoJTApplicant.RelationShipNumber);
                $('txtJApplicantTINNumber').setValue(res.value.objAutoJTApplicant.TINNumber);
                $('txtJApplicantFatherName').setValue(res.value.objAutoJTApplicant.FathersName);
                $('txtJApplicantMotherName').setValue(res.value.objAutoJTApplicant.MothersName);
                $('txtJApplicantDateOfBirth').setValue(res.value.objAutoJTApplicant.DateofBirth.format());
                $('cmbJApplicantMaritalStatus').setValue(res.value.objAutoJTApplicant.MaritalStatus);
                $('txtJApplicantSpouseName').setValue(res.value.objAutoJTApplicant.SpouseName);
                $('txtJApplicantSpouseProfession').setValue(res.value.objAutoJTApplicant.SpouseProfession);
                $('txtJApplicantSpouseWorkAddress').setValue(res.value.objAutoJTApplicant.SpouseWorkAddress);
                $('txtJApplicantSpouseContactNumber').setValue(res.value.objAutoJTApplicant.SpouseContactNumber);
                $('txtJApplicantRelationshipWithPrimary').setValue(res.value.objAutoJTApplicant.RelationShipwithPrimary);
                $('txtJApplicantNationality').setValue(res.value.objAutoJTApplicant.Nationality);
                $('cmbJApplicantIdentificationNumber').setValue(res.value.objAutoJTApplicant.IdentificationNumberType);
                $('txtJApplicantIdentificationNumber').setValue(res.value.objAutoJTApplicant.IdentificationNumber);
                $('cmbJApplicantIdentificationDocument').setValue(res.value.objAutoJTApplicant.IdentificationDocument);
                $('txtJApplicantNumberOfDependents').setValue(res.value.objAutoJTApplicant.NumberofDependents);
                $('cmbJApplicantHighestEducationLevel').setValue(res.value.objAutoJTApplicant.HighestEducation);
                $('txtJApplicantResidenceAddress').setValue(res.value.objAutoJTApplicant.ResidenceAddress);
                $('txtJApplicantMailingAddress').setValue(res.value.objAutoJTApplicant.MailingAddress);
                $('txtJApplicantPermanentAddress').setValue(res.value.objAutoJTApplicant.PermanentAddress);
                $('cmbJApplicantResidenceStatus').setValue(res.value.objAutoJTApplicant.ResidenceStatus);
                $('cmbJApplicantOwnershipDocument').setValue(res.value.objAutoJTApplicant.OwnershipDocument);
                $('cmbJApplicantResidenceStatusYear').setValue(res.value.objAutoJTApplicant.ResidenceStatusYear);
                $('txtJApplicantPhoneRescident').setValue(res.value.objAutoJTApplicant.PhoneResidence);
                $('txtJApplicantPhoneMobile').setValue(res.value.objAutoJTApplicant.Mobile);
                $('txtJApplicantEmail').setValue(res.value.objAutoJTApplicant.Email);
                $('cmbJAllicantDirectorShipWithAnyBank').setValue(res.value.objAutoJTApplicant.DirectorshipwithanyBank);
                // Applicant JOINT end

                // Profession Primary

                $('txtPrProfessionId').setValue(res.value.objAutoPRProfession.PrProfessionId);

                $('cmbPApplicantPrimaryProfession').setValue(res.value.objAutoPRProfession.PrimaryProfession);
                $('cmbPApplicantOtherProfession').setValue(res.value.objAutoPRProfession.OtherProfession);
                $('txtPApplicantMonthInCurrentProfession').setValue(res.value.objAutoPRProfession.MonthinCurrentProfession);
                $('cmbPApplicantNameOfCompany').setValue(res.value.objAutoPRProfession.NameofCompany);
                $('txtPApplicantNameofOrganization').setValue(res.value.objAutoPRProfession.NameofOrganization);
                $('txtPApplicantDesignation').setValue(res.value.objAutoPRProfession.Designation);
                $('txtPApplicantNatureOfBusiness').setValue(res.value.objAutoPRProfession.NatureOfBussiness);
                $('txtPApplicantYearsInBusiness').setValue(res.value.objAutoPRProfession.YearsInBussiness);
                $('cmdPApplicantOfficeStatus').setValue(res.value.objAutoPRProfession.OfficeStatus);
                $('txtPApplicantAddress').setValue(res.value.objAutoPRProfession.Address);
                $('txtPApplicantOfficePhone').setValue(res.value.objAutoPRProfession.OfficePhone);
                $('cmbPApplicantOwnershipType').setValue(res.value.objAutoPRProfession.OwnerShipType);
                $('cmbPApplicantEmploymentStatus').setValue(res.value.objAutoPRProfession.EmploymentStatus);
                $('txtPApplicantTotalProfessionalExperience').setValue(res.value.objAutoPRProfession.TotalProfessionalExperience);
                $('txtPApplicantNumberOfEmployees').setValue(res.value.objAutoPRProfession.NumberOfEmployees);
                $('txtPApplicantEquity_Share').setValue(res.value.objAutoPRProfession.EquityShare);
                $('txtPApplicantPrimaryIncomeSource').setValue(res.value.objAutoPRProfession.PrIncomeSource);
                $('txtPApplicantOtherIncomeSource').setValue(res.value.objAutoPRProfession.OtherIncomeSource);
                $('txtPApplicantNoOfFloorsRented').setValue(res.value.objAutoPRProfession.NoOfFloorsRented);
                $('cmbPApplicantNatureOfRentedFloors').setValue(res.value.objAutoPRProfession.NatureOfrentedFloors);
                $('txtPApplicantRentedAresInSFT').setValue(res.value.objAutoPRProfession.RentedAresInSFT);
                $('txtPApplicantConstructionCompletingYear').setValue(res.value.objAutoPRProfession.ConstructionCompletingYear);


                // Profession JOINT
                $('txtJtProfessionId').setValue(res.value.objAutoJTProfession.JtProfessionId);

                $('cmbJApplicantPrimaryProfession').setValue(res.value.objAutoJTProfession.PrimaryProfession);
                $('cmbJApplicantOtherProfession').setValue(res.value.objAutoJTProfession.OtherProfession);
                $('txtJApplicantMonthInCurrentProfession').setValue(res.value.objAutoJTProfession.MonthinCurrentProfession);
                $('cmbJApplicantNameOfCompany').setValue(res.value.objAutoJTProfession.NameofCompany);
                $('txtJApplicantNameofOrganization').setValue(res.value.objAutoJTProfession.NameofOrganization);
                $('txtJApplicantDesignation').setValue(res.value.objAutoJTProfession.Designation);
                $('txtJApplicantNatureOfBusiness').setValue(res.value.objAutoJTProfession.NatureOfBussiness);
                $('txtJApplicantYearsInBusiness').setValue(res.value.objAutoJTProfession.YearsInBussiness);
                $('cmdJApplicantOfficeStatus').setValue(res.value.objAutoJTProfession.OfficeStatus);
                $('txtJApplicantAddress').setValue(res.value.objAutoJTProfession.Address);
                $('txtJApplicantOfficePhone').setValue(res.value.objAutoJTProfession.OfficePhone);
                $('cmbJApplicantOwnershipType').setValue(res.value.objAutoJTProfession.OwnerShipType);
                $('cmbJApplicantEmploymentStatus').setValue(res.value.objAutoJTProfession.EmploymentStatus);
                $('txtJApplicantTotalProfessionalExperience').setValue(res.value.objAutoJTProfession.TotalProfessionalExperience);
                $('txtJApplicantNumberOfEmployees').setValue(res.value.objAutoJTProfession.NumberOfEmployees);
                $('txtJApplicantEquity_Share').setValue(res.value.objAutoJTProfession.EquityShare);
                $('txtJApplicantPrimaryIncomeSource').setValue(res.value.objAutoJTProfession.PrIncomeSource);
                $('txtJApplicantOtherIncomeSource').setValue(res.value.objAutoJTProfession.OtherIncomeSource);
                $('txtJApplicantNoOfFloorsRented').setValue(res.value.objAutoJTProfession.NoOfFloorsRented);
                $('cmbJApplicantNatureOfRentedFloors').setValue(res.value.objAutoJTProfession.NatureOfrentedFloors);
                $('txtJApplicantRentedAresInSFT').setValue(res.value.objAutoJTProfession.RentedAresInSFT);
                $('txtJApplicantConstructionCompletingYear').setValue(res.value.objAutoJTProfession.ConstructionCompletingYear);


                // Vehicle
                $('txtAutoLoanVehicleId').setValue(res.value.objAutoLoanVehicle.AutoLoanVehicleId);

                $('cmbVendor').setValue(res.value.objAutoLoanVehicle.VendorId);
                Page.vehicleDetailsArray = res.value.objAutoLoanVehicle;
                $('cmbManufacturer').setValue(res.value.objAutoLoanVehicle.Auto_ManufactureId);
                loanSummeryForSalesManager.changeManufacture();
                $('cmbModel').setValue(res.value.objAutoLoanVehicle.VehicleId);
                loanSummeryForSalesManager.changeModel();
                $('txtVehicleType').setValue(res.value.objAutoLoanVehicle.VehicleType);
                $('cmdVehicleStatus').setValue(res.value.objAutoLoanVehicle.VehicleStatus);
                $('txtManufacturingyear').setValue(res.value.objAutoLoanVehicle.ManufacturingYear);
                loanSummeryForSalesManager.changeManufacturingYear();
                $('txtQuotedPrice').setValue(res.value.objAutoLoanVehicle.QuotedPrice);
                $('txtVerifiedPrice').setValue(res.value.objAutoLoanVehicle.VerifiedPrice);
                $('cmbPriceConsideredBasedOn').setValue(res.value.objAutoLoanVehicle.PriceConsideredBasedOn);
                $('txtSeatingCapacity').setValue(res.value.objAutoLoanVehicle.SeatingCapacity);
                $('cmbCarVerification').setValue(res.value.objAutoLoanVehicle.CarVerification);


                // Vehicle Insurance

                $('cmbGeneralInsurance').setValue(res.value.objAutoApplicationInsurance.GeneralInsurance);
                $('cmbLifeInsurance').setValue(res.value.objAutoApplicationInsurance.LifeInsurance);
                if (res.value.objAutoApplicationInsurance.GeneralInsurance != "-1") {
                    $('cmbInsuranceFinancewithLoan').setValue(res.value.objAutoApplicationInsurance.InsuranceFinancewithLoan);
                    loanSummeryForSalesHelper.changeGeneralInsurence();
                }
                if (res.value.objAutoApplicationInsurance.LifeInsurance != "-1") {
                    loanSummeryForSalesHelper.changeLifeInsurence();
                    $('cmbInsuranceFinancewithLoanLife').setValue(res.value.objAutoApplicationInsurance.InsuranceFinancewithLoanLife);
                }
                //$('cmbInsuranceFinancewithLoan').setValue(res.value.objAutoApplicationInsurance.InsuranceFinancewithLoan);
                //$('cmbInsuranceFinancewithLoanLife').setValue(res.value.objAutoApplicationInsurance.InsuranceFinancewithLoanLife);

                // Accounts Primary

                $('txtPrAccountId').setValue(res.value.objAutoPRAccount.PrAccountId);

                $('Text54').setValue(res.value.objAutoPRAccount.AccountNumberForSCB);
                $('Text95').setValue(res.value.objAutoPRAccount.CreditCardNumberForSCB);


                // Accounts JOINT

                $('txtJtAccountId').setValue(res.value.objAutoPRAccount.jtAccountId);
                $('Text55').setValue(res.value.objAutoJTAccount.AccountNumberForSCB);
                $('Text59').setValue(res.value.objAutoJTAccount.CreditCardNumberForSCB);




                //Primary Applicant Other Bank Account

                if (res.value.objAutoPRAccountDetailList.length != null) {

                    for (var i = 0; i < res.value.objAutoPRAccountDetailList.length; i++) {

                        var rowNo = i + 1;
                        if (rowNo < 6) {
                            $('txtPrAccountDetailsId' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].PrAccountDetailsId);

                            $('cmbPApplicantBank' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].BankId);
                            loanSummeryForSalesManager.GetBranch('cmbPApplicantBank' + rowNo);
                            $('cmbPApplicantBranch' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].BranchId);
                            $('txtPApplicantAccountNumber' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].AccountNumber);
                            $('cmbPApplicantAccountCategory' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].AccountCategory);
                            $('cmbPApplicantAccountType' + rowNo).setValue(res.value.objAutoPRAccountDetailList[i].AccountType);
                        }

                    }

                }

                //Joint Applicant Other Bank Account

                if (res.value.objAutoJtAccountDetailList.length != null && res.value.objAutoJtAccountDetailList.length != 0) {

                    for (var i = 0; i < res.value.objAutoJtAccountDetailList.length; i++) {

                        var rowNo = i + 1;
                        if (rowNo < 6) {
                            $('txtJtAccountDetailsId' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].JtAccountDetailsId);
                            $('cmbJApplicantBank' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].BankId);
                            loanSummeryForSalesManager.GetBranch('cmbJApplicantBank' + rowNo);
                            $('cmbJApplicantBranch' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].BranchId);
                            $('txtJApplicantAccountNumber' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].AccountNumber);
                            $('cmbJApplicantAccountCategory' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].AccountCategory);
                            $('cmbJApplicantAccountType' + rowNo).setValue(res.value.objAutoJtAccountDetailList[i].AccountType);
                        }

                    }
                }


                //Financial Primary

                $('txtPrFinanceId').setValue(res.value.objAutoPRFinance.PrFinanceId);
                $('txtPApplicantDeclaredPrimaryIncomeSourch').setValue(res.value.objAutoPRFinance.DPISource);
                $('txtPApplicantDeclaredPrimaryIncomeAmount').setValue(res.value.objAutoPRFinance.DPIAmount);
                $('txtPApplicantDeclaredOtherIncomeSourch').setValue(res.value.objAutoPRFinance.DOISource);
                $('txtPApplicantDeclaredOtherIncomeAmount').setValue(res.value.objAutoPRFinance.DOIAmount);
                $('txtPApplicantRentUtilities').setValue(res.value.objAutoPRFinance.EDRentAndUtilities);
                $('txtPApplicantFoodClothing').setValue(res.value.objAutoPRFinance.EDFoodAndClothing);
                $('txtPApplicantEducation').setValue(res.value.objAutoPRFinance.EDEducation);
                $('txtPApplicantLoanRepayment').setValue(res.value.objAutoPRFinance.EDLoanRePayment);
                $('txtPApplicantOthers').setValue(res.value.objAutoPRFinance.EDOther);
                $('txtPApplicantRepaymentto').setValue(res.value.objAutoPRFinance.RepaymentTo);
                $('txtPApplicantRepaymentfor').setValue(res.value.objAutoPRFinance.PaymentFor);



                //Financial Joint

                $('txtJtFinanceId').setValue(res.value.objAutoJTFinance.JtFinanceId);
                $('txtJApplicantDeclaredPrimaryIncomeSourch').setValue(res.value.objAutoJTFinance.DPISource);
                $('txtJApplicantDeclaredPrimaryIncomeAmount').setValue(res.value.objAutoJTFinance.DPIAmount);
                $('txtJApplicantDeclaredOtherIncomeSourch').setValue(res.value.objAutoJTFinance.DOISource);
                $('txtJApplicantDeclaredOtherIncomeAmount').setValue(res.value.objAutoJTFinance.DOIAmount);
                $('txtJApplicantRentUtilities').setValue(res.value.objAutoJTFinance.EDRentAndUtilities);
                $('txtJApplicantFoodClothing').setValue(res.value.objAutoJTFinance.EDFoodAndClothing);
                $('txtJApplicantEducation').setValue(res.value.objAutoJTFinance.EDEducation);
                $('txtJApplicantLoanRepayment').setValue(res.value.objAutoJTFinance.EDLoanRePayment);
                $('txtJApplicantOthers').setValue(res.value.objAutoJTFinance.EDOther);
                $('txtJApplicantRepaymentto').setValue(res.value.objAutoJTFinance.RepaymentTo);
                $('txtJApplicantRepaymentfor').setValue(res.value.objAutoJTFinance.PaymentFor);


                // References 1

                $('txtReferenceId').setValue(res.value.objAutoLoanReference.ReferenceId);
                $('txtReferenceName1').setValue(res.value.objAutoLoanReference.ReferenceName1);
                $('txtReferenceName2').setValue(res.value.objAutoLoanReference.ReferenceName2);
                $('txtReferenceRelationship1').setValue(res.value.objAutoLoanReference.Relationship1);
                $('txtReferenceRelationship2').setValue(res.value.objAutoLoanReference.Relationship2);
                $('cmbReferenceOccupation1').setValue(res.value.objAutoLoanReference.Occupation1);
                $('cmbReferenceOccupation2').setValue(res.value.objAutoLoanReference.Occupation2);
                $('txtReferenceNameOfTheOrganization1').setValue(res.value.objAutoLoanReference.OrganizationName1);
                $('txtReferenceNameOfTheOrganization2').setValue(res.value.objAutoLoanReference.OrganizationName2);
                $('txtReferenceDesignation1').setValue(res.value.objAutoLoanReference.Designation1);
                $('txtReferenceDesignation2').setValue(res.value.objAutoLoanReference.Designation2);
                $('txtReferenceWorkAddress1').setValue(res.value.objAutoLoanReference.WorkAddress1);
                $('txtReferenceWorkAddress2').setValue(res.value.objAutoLoanReference.WorkAddress2);
                $('txtReferenceResidenceAddress1').setValue(res.value.objAutoLoanReference.ResidenceAddress1);
                $('txtReferenceResidenceAddress2').setValue(res.value.objAutoLoanReference.ResidenceAddress2);
                $('txtReferencePhoneNumber1').setValue(res.value.objAutoLoanReference.PhoneNumber1);
                $('txtReferencePhoneNumber2').setValue(res.value.objAutoLoanReference.PhoneNumber2);
                $('txtReferenceMobileNumber1').setValue(res.value.objAutoLoanReference.MobileNumber1);
                $('txtReferenceMobileNumber2').setValue(res.value.objAutoLoanReference.MobileNumber2);


                // Facility Schedule
                if (res.value.objAutoLoanFacilityList.length) {

                    for (var i = 0; i < res.value.objAutoLoanFacilityList.length; i++) {

                        var rowNo = i + 1;


                        var textToFind = res.value.objAutoLoanFacilityList[i].FacilityType;
                        $('txtFacilityId' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].FacilityId);
                        $('cmbFacilityScheduleFacilityType' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].FacilityType);
                        $('txtFacilityScheduleInterestRate' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);
                        $('txtFacilitySchedulePresentBalance' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].PresentBalance);
                        $('txtFacilitySchedulePresentEMI' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].PresentEMI);
                        $('txtFacilitySchedulePresentLimit' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].PresentLimit);
                        $('txtFacilityScheduleProposedLimit' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].ProposedLimit);
                        $('txtFacilityScheduleRepaymentAgreement' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].RepaymentAgrement);

                        //Sequarity List
                        $('txtSecurityId' + rowNo).setValue('0');
                        $('cmbStatusForSecurities' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].Status);
                        $('txtSecurityScheduleNatureOfSecurity' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].NatureOfSecurity);
                        $('txtSecurityScheduleFaceValue' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].FaceValue);
                        $('txtSecurityScheduleXTVRate' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].XTVRate);
                        $('txtSecurityScheduleInterestRate' + rowNo).setValue(res.value.objAutoLoanFacilityList[i].InterestRate);

                    }
                }


                // Security Schedule
                //                if (res.value.objAutoLoanSecurityList.length) {

                //                    for (var i = 0; i < res.value.objAutoLoanSecurityList.length; i++) {

                //                        var rowNo = i + 1;
                //                        $('txtSecurityId' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].SecurityId);
                //                        //$('cmbfacilityTypeForSequrity' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].FacilityType);
                //                        $('cmbStatusForSecurities' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].Status);
                //                        $('txtSecurityScheduleNatureOfSecurity' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].NatureOfSecurity);
                //                        //$('txtSecurityScheduleIssuingOffice' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].IssuingOffice);
                //                        $('txtSecurityScheduleFaceValue' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].FaceValue);
                //                        $('txtSecurityScheduleXTVRate' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].XTVRate);
                //                        //$('txtSecurityScheduleIssueDate' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].IssueDate.format());
                //                        $('txtSecurityScheduleInterestRate' + rowNo).setValue(res.value.objAutoLoanSecurityList[i].InterestRate);

                //                    }
                //                }

            }

            else {
                alert("No information found.");
            }
        }
    },
    //    changeManufacture: function() {
    //        var manufacturerID = $F('cmbManufacturer');

    //        //var res = PlussAuto.LoanApplication.GetAutoModelByManufacturerID(manufacturerID);
    //        var res = PlussAuto.AutoLoanSummaryForSales.getModelbyManufacturerID(manufacturerID);

    //        if (res.error) {
    //            alert(res.error.Message);
    //            return;
    //        }
    //        Page.modelArray = res.value;

    //        document.getElementById('cmbModel').options.length = 0;
    //        var ExtraOpt = document.createElement("option");
    //        ExtraOpt.text = 'Select Model';
    //        ExtraOpt.value = '-1';
    //        document.getElementById('cmbModel').options.add(ExtraOpt);
    //        for (var i = 0; i < res.value.length; i++) {
    //            var opt = document.createElement("option");
    //            var ch = res.value[i].Model;
    //            if (res.value[i].TrimLevel != "") {
    //                ch += "-" + res.value[i].TrimLevel;

    //            }
    //            opt.text = ch;
    //            opt.value = res.value[i].VehicleId;
    //            document.getElementById('cmbModel').options.add(opt);
    //        }

    //        if (Page.vehicleId != 0) {
    //            $('cmbModel').setValue(Page.vehicleId);

    //            loanSummeryForSalesManager.changeModel();
    //        }


    //    },
    changeManufacture: function() {
        var manufacturerID = $F('cmbManufacturer');
        var manufacturName = $('cmbManufacturer').options[$('cmbManufacturer').selectedIndex].text;
        if (manufacturName != "Other") {
            $('cmbModel').style.display = 'block';
            $('txtVehicleType').style.display = 'block';
            $('txtModelName').style.display = 'none';
            $('cmbVehicleType').style.display = 'none';
            $('txtTrimLevel').disabled = 'false';
            $('txtEngineCC').disabled = 'false';
            $('txtVehicleType').disabled = 'false';

            //var res = PlussAuto.LoanApplication.GetAutoModelByManufacturerID(manufacturerID);
            var res = PlussAuto.AutoLoanSummaryForSales.getModelbyManufacturerID(manufacturerID);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            Page.modelArray = res.value;

            document.getElementById('cmbModel').options.length = 0;
            var ExtraOpt = document.createElement("option");
            ExtraOpt.text = 'Select Model';
            ExtraOpt.value = '-1';
            document.getElementById('cmbModel').options.add(ExtraOpt);

            //        for (var i = 0; i < res.value.length; i++) {
            //            var opt = document.createElement("option");
            //            var ch = res.value[i].Model;
            ////            if (res.value[i].TrimLevel != "") {
            ////                ch += "-" + res.value[i].TrimLevel;
            ////            }

            //            opt.text = ch;
            //            //            opt.value = res.value[i].VehicleId;
            //            opt.value = ch;
            //            document.getElementById('cmbModel').options.add(opt);
            //        }
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                var ch = res.value[i].Model;
                if (res.value[i].TrimLevel != "") {
                    ch += "-" + res.value[i].TrimLevel;

                }
                opt.text = ch;
                opt.value = res.value[i].VehicleId;
                document.getElementById('cmbModel').options.add(opt);
            }

            if (Page.vehicleId != 0) {
                $('cmbModel').setValue(Page.vehicleId);

                LoanApplicationManager.changeModel();
            }
        }
        else {
            $('cmbModel').style.display = 'none';
            $('txtVehicleType').style.display = 'none';
            $('txtModelName').style.display = 'block';
            $('cmbVehicleType').style.display = 'block';
            $('txtTrimLevel').disabled = '';
            $('txtEngineCC').disabled = '';
            $('txtVehicleType').disabled = '';

            if (Page.vehicleDetailsArray != null && Page.vehicleDetailsArray.AutoLoanMasterId != 0) {
                $('txtModelName').setValue(Page.vehicleDetailsArray.Model);
                $('txtTrimLevel').setValue(Page.vehicleDetailsArray.TrimLevel);
                $('txtEngineCC').setValue(Page.vehicleDetailsArray.EngineCC);
                //
                if (Page.vehicleDetailsArray.VehicleType != "") {
                    $('cmbVehicleType').setValue(vehicleType[Page.vehicleDetailsArray.VehicleType]);
                }
            }

        }


    },
    //    changeModel: function() {

    //        var vehicleId = $F('cmbModel');
    //        var model = Page.modelArray.findByProp('VehicleId', vehicleId);
    //        if (model) {
    //            $('txtTrimLevel').setValue(model.TrimLevel);
    //            $('txtEngineCC').setValue(model.CC);
    //            $('hdnModel').setValue(model.Model);
    //            $('txtVehicleType').setValue(model.Type);
    //            $('txtManufacturingyear').setValue(model.ManufacturingYear);
    //            var vehicleStatus = $F("cmdVehicleStatus");
    //            if (util.isDigit(model.ManufacturingYear)) {
    //                loanSummeryForSalesManager.getPrice(vehicleId, model.ManufacturingYear, vehicleStatus);
    //            }
    //        }

    //    },
    changeModel: function() {
        //Arif Code
        //var manufacturerID = $F('cmbManufacturer');
        //var model = $F('cmbModel');


        //        var res = PlussAuto.LoanApplication.GetAutoTrimLevelByModel(manufacturerID, model);

        //        if (res.error) {
        //            alert(res.error.Message);
        //            return;
        //        }

        //        document.getElementById('cmbTrimLevel').options.length = 0;
        //        var ExtraOpt = document.createElement("option");
        //        ExtraOpt.text = 'Select Trim Level';
        //        ExtraOpt.value = '';
        //        document.getElementById('cmbTrimLevel').options.add(ExtraOpt);

        //        for (var i = 0; i < res.value.length; i++) {
        //            var opt = document.createElement("option");
        //            var ch = res.value[i].TrimLevel;

        //            opt.text = ch;
        //            opt.value = ch;
        //            document.getElementById('cmbTrimLevel').options.add(opt);
        //        }
        //        LoanApplicationManager.changeTrimLevel();

        //New Code Change by Rimpo
        var vehicleId = $F('cmbModel');
        var vehicleName = $('cmbModel').options[$('cmbModel').selectedIndex].text;
        if (vehicleName != "Other") {
            var model = Page.modelArray.findByProp('VehicleId', vehicleId);
            if (model) {
                $('txtVehicleType').style.display = 'block';
                $('cmbVehicleType').style.display = 'none';
                $('txtTrimLevel').disabled = 'false';
                $('txtEngineCC').disabled = 'false';
                $('txtVehicleType').disabled = 'false';
                $('txtTrimLevel').setValue(model.TrimLevel);
                $('txtEngineCC').setValue(model.CC);
                $('hdnModel').setValue(model.Model);
                $('txtVehicleType').setValue(model.Type);
                $('txtManufacturingyear').setValue(model.ManufacturingYear);
                var vehicleStatus = $F("cmdVehicleStatus");
                if (util.isDigit(model.ManufacturingYear)) {
                    LoanApplicationManager.getPrice(vehicleId, model.ManufacturingYear, vehicleStatus);
                }
            }
        }
        else {
            $('txtVehicleType').style.display = 'none';
            $('cmbVehicleType').style.display = 'block';
            $('txtTrimLevel').disabled = '';
            $('txtEngineCC').disabled = '';
            $('txtVehicleType').disabled = '';
            if (Page.vehicleDetailsArray != null && Page.vehicleDetailsArray.AutoLoanMasterId != 0) {
                //$('txtModelName').setValue(Page.vehicleDetailsArray.Model);
                $('txtTrimLevel').setValue(Page.vehicleDetailsArray.TrimLevel);
                $('txtEngineCC').setValue(Page.vehicleDetailsArray.EngineCC);
                //
                if (Page.vehicleDetailsArray.VehicleType != "") {
                    $('cmbVehicleType').setValue(vehicleType[Page.vehicleDetailsArray.VehicleType]);
                }
            }
        }

    },
    changeManufacturingYear: function(res) {
        var manufacturingYear = $F('txtManufacturingyear');
        if (!util.isDigit(manufacturingYear)) {
            alert("Required Number filed.");
            $('txtManufacturingyear').setValue("0");
            //$('txtManufacturingyear').focus();
            return;
        }
        var vehicleId = $F('cmbModel');
        var vehicleStatus = $F("cmdVehicleStatus");
        loanSummeryForSalesManager.getPrice(vehicleId, manufacturingYear, vehicleStatus);

    },
    GetBranch: function(ElementID) {
        var bankID = $F(ElementID);
        if (bankID == 0) {
        }
        else {
            var res = PlussAuto.AutoLoanSummaryForSales.GetBranch(bankID);

            if (res.error) {
                alert(res.error.Message);
                return;
            }
            loanSummeryForSalesHelper.populateBranchCombo(res, ElementID);
        }

    },
    getPrice: function(vehicleId, manufacturingYear, vehicleStatus) {
        var res = PlussAuto.AutoLoanSummaryForSales.GetPriceByVehicleId(vehicleId, manufacturingYear, vehicleStatus);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value.length == 0) {
        }
        else {
            var minimumPrice = res.value[0].MinimumPrice;
            if (minimumPrice != 0) {
                $('txtVerifiedPrice').setValue(minimumPrice);
            }
        }
    },

    Appeal: function(appiledOnAutoLoanMasterId, LLID) {
        $('hdnLLID').setValue(LLID);
        $('hdnMasterId').setValue(appiledOnAutoLoanMasterId);
        util.showModal('newLLPopupDiv');

        //        var res = PlussAuto.AutoLoanSummaryForSales.AppealedByAutoLoanMasterId(appiledOnAutoLoanMasterId);
        //        if (res.error) {
        //            alert(res.error.Message);
        //            return;
        //        }
        //        if (res.value == "Success") {
        //            alert("Appeal Succeed");
        //            loanSummeryForSalesManager.getLoanSummary();
        //        } else {
        //            alert("Appeal not Succeeded");
        //        }
    },
    printBaLetter: function(llid) {

        var url = "../../UI/AutoLoan/BALetterPrint.aspx?llid=" + llid;


        window.open(url, '_blank');
        window.focus();
    },
    OpenLoanApplication: function(llid) {
        $('divLoanSummaryForSales').style.display = 'none';
        $('loanDetailsForSales').style.display = 'block';
        $('txtLLID').setValue(llid);
        loanSummeryForSalesManager.GetInfoByLLID();
    }
};

var loanSummeryForSalesHelper = {


    populateLoanSummeryGrid: function(rss) {
        var html = [];
        for (var i = 0; i < rss.value.length; i++) {
            // var serialNo = i + 1;
            //            <a title="Appeal" href="javascript:;" onclick="loanSummeryForSalesHelper.replayPopupShow(#{Auto_LoanMasterId})" class="icon iconReplay"></a>
            var btnWillBe = "";
            if (Page.LoanSummeryArray[i].StatusID == 2 || Page.LoanSummeryArray[i].StatusID == 37 || Page.LoanSummeryArray[i].StatusID == 38) {
                btnWillBe = '<input title="Appeal" type="button" id="btnAppeal" class="searchButton   widthSize100_per" value="Appeal" onclick="loanSummeryForSalesManager.Appealed(' + Page.LoanSummeryArray[i].Auto_LoanMasterId + "," + Page.LoanSummeryArray[i].LLID + ')"/>';
            }
            else if (Page.LoanSummeryArray[i].StatusID == 30 || Page.LoanSummeryArray[i].StatusID == 31 || Page.LoanSummeryArray[i].StatusID == 32) {
                btnWillBe = '<input title="Print BA Letter" type="button" id="btnPrintBA" class="searchButton   widthSize100_per" value="Print BA Letter" onclick=" loanSummeryForSalesManager.printBaLetter(' + Page.LoanSummeryArray[i].LLID + ')"/>';
            }
            //            else if (Page.LoanSummeryArray[i].StatusID == 17) {
            else {
                btnWillBe = '<input title="Open" type="button" id="btnOpen" class="searchButton   widthSize100_per" value="Open" onclick=" loanSummeryForSalesManager.OpenLoanApplication(' + Page.LoanSummeryArray[i].LLID + ')"/>';
            }
            var className = i % 2 ? '' : 'odd';
            Page.LoanSummeryArray[i].btnWillBe = btnWillBe;
            html.push('<tr class="' + className + '"><td>#{LLID}</td><td>#{PrName}</td><td>#{ProductName}</td><td>#{ManufacturerName}</td><td>#{Model}</td><td>#{AppliedAmount}</td><td>#{AskingTenor}</td><td>#{StatusName}</td><td>#{btnWillBe}</td></tr>'
                    .format2((i % 2 ? '' : 'odd'), true)
                    .format(Page.LoanSummeryArray[i])
            );
        }
        $('GridLoanSummery').update(html.join(''));
        var totalCount = 0;

        if (rss.value.length != 0) {
            totalCount = Page.LoanSummeryArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);

    },
    clear: function() {
        $('txtApplicantName').setValue('');
        $('txtApplicationDate').setValue('');
        $('txtDateOfBirth').setValue('');
        $('txtAppliedAmount').setValue('');
        $('txtAskingTenor').setValue('');
    },




    searchKeypress: function(e) {
        if (e.keyCode == 13) {
            loanSummeryForSalesManager.getLoanSummary();
        }
    },
    populateBankCombo: function(res) {


        var extraOption = document.createElement("option");
        extraOption.text = 'Select Bank';
        extraOption.value = '-1';

        document.getElementById('cmbPApplicantBank1').options.length = 0;
        document.getElementById('cmbPApplicantBank1').options.add(extraOption);

        document.getElementById('cmbPApplicantBank2').options.length = 0;
        var extraOption1 = document.createElement("option");
        extraOption1.text = 'Select Bank';
        extraOption1.value = '-1';

        document.getElementById('cmbPApplicantBank2').options.add(extraOption1);

        document.getElementById('cmbPApplicantBank3').options.length = 0;
        var extraOption2 = document.createElement("option");
        extraOption2.text = 'Select Bank';
        extraOption2.value = '-1';
        document.getElementById('cmbPApplicantBank3').options.add(extraOption2);

        document.getElementById('cmbPApplicantBank4').options.length = 0;
        var extraOption3 = document.createElement("option");
        extraOption3.text = 'Select Bank';
        extraOption3.value = '-1';
        document.getElementById('cmbPApplicantBank4').options.add(extraOption3);

        document.getElementById('cmbPApplicantBank5').options.length = 0;
        var extraOption4 = document.createElement("option");
        extraOption4.text = 'Select Bank';
        extraOption4.value = '-1';
        document.getElementById('cmbPApplicantBank5').options.add(extraOption4);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].BankName;
            opt.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank1').options.add(opt);

            var opt1 = document.createElement("option");
            opt1.text = res.value[i].BankName;
            opt1.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank2').options.add(opt1);

            var opt2 = document.createElement("option");
            opt2.text = res.value[i].BankName;
            opt2.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank3').options.add(opt2);

            var opt3 = document.createElement("option");
            opt3.text = res.value[i].BankName;
            opt3.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank4').options.add(opt3);

            var opt4 = document.createElement("option");
            opt4.text = res.value[i].BankName;
            opt4.value = res.value[i].BankID;
            document.getElementById('cmbPApplicantBank5').options.add(opt4);

            var opt5 = document.createElement("option");
            opt5.text = res.value[i].BankName;
            opt5.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank1').options.add(opt5);

            var opt6 = document.createElement("option");
            opt6.text = res.value[i].BankName;
            opt6.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank2').options.add(opt6);

            var opt7 = document.createElement("option");
            opt7.text = res.value[i].BankName;
            opt7.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank3').options.add(opt7);

            var opt8 = document.createElement("option");
            opt8.text = res.value[i].BankName;
            opt8.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank4').options.add(opt8);

            var opt9 = document.createElement("option");
            opt9.text = res.value[i].BankName;
            opt9.value = res.value[i].BankID;
            document.getElementById('cmbJApplicantBank5').options.add(opt9);

        }
    },
    populateFacilityCombo: function(res) {
        var link = "<option value=\"-1\">Select a Facility</option>";


        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].FACI_ID + "\">" + res.value[i].FACI_NAME + "</option>";
        }
        $("cmbFacilityScheduleFacilityType1").update(link);
        $("cmbFacilityScheduleFacilityType2").update(link);
        $("cmbFacilityScheduleFacilityType3").update(link);
        $("cmbFacilityScheduleFacilityType4").update(link);
        $("cmbFacilityScheduleFacilityType5").update(link);

        //        $("cmbfacilityTypeForSequrity1").update(link);
        //        $("cmbfacilityTypeForSequrity2").update(link);
        //        $("cmbfacilityTypeForSequrity3").update(link);
        //        $("cmbfacilityTypeForSequrity4").update(link);
        //        $("cmbfacilityTypeForSequrity5").update(link);

        Page.facilityArray = res.value;

    },
    hideShowDivBasedOn_jointApplicationDiv: function() {
        var isJoint = $F("cmbJointApplication");

        if (isJoint == '1') {
            document.getElementById('JApplicantdivHeader').style.display = 'block';
            document.getElementById('JApplicantControldiv').style.display = 'block';
            document.getElementById('JProfessionHeaderDiv').style.display = 'block';
            document.getElementById('JProfessionControlDiv').style.display = 'block';
            document.getElementById('JAccountHeaderDiv').style.display = 'block';
            document.getElementById('JAccountControlDiv').style.display = 'block';
            document.getElementById('Account_JointApplicantOtherBankAccount').style.display = 'block';
            document.getElementById('JFinanceHeaderDiv').style.display = 'block';
            document.getElementById('JFinanceControlDiv').style.display = 'block';
        }
        else {
            document.getElementById('JApplicantdivHeader').style.display = 'none';
            document.getElementById('JApplicantControldiv').style.display = 'none';
            document.getElementById('JProfessionHeaderDiv').style.display = 'none';
            document.getElementById('JProfessionControlDiv').style.display = 'none';
            document.getElementById('JAccountHeaderDiv').style.display = 'none';
            document.getElementById('JAccountControlDiv').style.display = 'none';

            document.getElementById('JFinanceHeaderDiv').style.display = 'none';
            document.getElementById('JFinanceControlDiv').style.display = 'none';
            document.getElementById('Account_JointApplicantOtherBankAccount').style.display = 'none';
        }
    },
    populateBranchCombo: function(res, elementID) {

        //primary applicant start

        var extraOption = document.createElement("option");
        extraOption.text = 'Select Branch';
        extraOption.value = '-1';

        if (elementID == 'cmbPApplicantBank1') {
            document.getElementById('cmbPApplicantBranch1').options.length = 0;
            document.getElementById('cmbPApplicantBranch1').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch1').options.add(opt);

            }
        }


        if (elementID == 'cmbPApplicantBank2') {
            document.getElementById('cmbPApplicantBranch2').options.length = 0;
            document.getElementById('cmbPApplicantBranch2').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch2').options.add(opt);

            }
        }

        if (elementID == 'cmbPApplicantBank3') {
            document.getElementById('cmbPApplicantBranch3').options.length = 0;
            document.getElementById('cmbPApplicantBranch3').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch3').options.add(opt);

            }
        }

        if (elementID == 'cmbPApplicantBank4') {
            document.getElementById('cmbPApplicantBranch4').options.length = 0;
            document.getElementById('cmbPApplicantBranch4').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch4').options.add(opt);

            }
        }

        if (elementID == 'cmbPApplicantBank5') {
            document.getElementById('cmbPApplicantBranch5').options.length = 0;
            document.getElementById('cmbPApplicantBranch5').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbPApplicantBranch5').options.add(opt);

            }
        }
        // Primary Applicant End

        //Joint applicant start.

        if (elementID == 'cmbJApplicantBank1') {
            document.getElementById('cmbJApplicantBranch1').options.length = 0;
            document.getElementById('cmbJApplicantBranch1').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch1').options.add(opt);

            }
        }


        if (elementID == 'cmbJApplicantBank2') {
            document.getElementById('cmbJApplicantBranch2').options.length = 0;
            document.getElementById('cmbJApplicantBranch2').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch2').options.add(opt);

            }
        }

        if (elementID == 'cmbJApplicantBank3') {
            document.getElementById('cmbJApplicantBranch3').options.length = 0;
            document.getElementById('cmbJApplicantBranch3').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch3').options.add(opt);

            }
        }

        if (elementID == 'cmbJApplicantBank4') {
            document.getElementById('cmbJApplicantBranch4').options.length = 0;
            document.getElementById('cmbJApplicantBranch4').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch4').options.add(opt);

            }
        }

        if (elementID == 'cmbJApplicantBank5') {
            document.getElementById('cmbJApplicantBranch5').options.length = 0;
            document.getElementById('cmbJApplicantBranch5').options.add(extraOption);
            for (var i = 0; i < res.value.length; i++) {
                var opt = document.createElement("option");
                opt.text = res.value[i].BRAN_NAME;
                opt.value = res.value[i].BRAN_ID;
                document.getElementById('cmbJApplicantBranch5').options.add(opt);

            }
        }
    },
    printPreview: function() {
        var searchKey = $F('txtLLID');
        window.open('LoanApplicationPrint.aspx?llid=' + searchKey);
    },
    closeLoanApplication: function() {
        $('divLoanSummaryForSales').style.display = 'block';
        $('loanDetailsForSales').style.display = 'none';
    },
    changeGeneralInsurence: function() {
        var generalInsurence = $F("cmbGeneralInsurance");
        if (generalInsurence == "-1") {
            $('cmbInsuranceFinancewithLoan').setValue("0");
            $('cmbInsuranceFinancewithLoan').disabled = 'false';
        }
        else {
            $('cmbInsuranceFinancewithLoan').disabled = '';
        }
    },
    changeLifeInsurence: function() {
        var lifeInsurence = $F("cmbLifeInsurance");
        if (lifeInsurence == "-1") {
            $('cmbInsuranceFinancewithLoanLife').setValue("0");
            $('cmbInsuranceFinancewithLoanLife').disabled = 'false';
        }
        else {
            $('cmbInsuranceFinancewithLoanLife').disabled = '';
        }
    },
    clickRadio: function(val) {
    if (val == 1) {
        $('divLLID').style.display = 'none';
        }
        else {
            $('divLLID').style.display = 'block';
        }
    }
};
Event.onReady(Page.init);