﻿var categoryType = { 'A': 'Cat A', 'B': 'Cat B', 'C': 'No Cat' };
var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    iRSettingsforPayroll: null,
    iRSettingsforPayrollArray: [],

    init: function() {
        util.prepareDom();
        Event.observe('lnkAddNewCompany', 'click', iRSettingsforPayrollHelper.update);
//        Event.observe('btnUploadDoc', 'click', iRSettingsforPayrollHelper.showUploader);
//        //        Event.observe('lnkUploadSave', 'click', iRSettingsforPayrollManager.save);
        //        Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('uploadPopup'));
        Event.observe('lnkSave', 'click', iRSettingsforPayrollManager.save);
        Event.observe('lnkClose', 'click', util.hideModal.curry('companyPopupDiv'));


        //    Event.observe('lnkUploadSave', 'click', Page.upload);

        //    Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });
        iRSettingsforPayrollManager.render();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        iRSettingsforPayrollManager.render();
    }

};

var iRSettingsforPayrollManager = {

    render: function() {
        var comapanyName = $F('txtSearch');
        var res = PlussAuto.IRSettingsForPayroll.GetPayRollSettingsSummary(Page.pageNo, Page.pageSize, comapanyName);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        iRSettingsforPayrollHelper.renderCollection(res);
    },


    save: function() {
        
        if (!util.validateEmpty('txtCompanyName')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtCompanyCode')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('cmbCategory')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtInterestRate')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtProcessingFee')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        var Payroll = Page.iRSettingsforPayroll;
        Payroll.CompanyName = $F('txtCompanyName');
        Payroll.CategoryName = $F('cmbCategory');
        Payroll.PayrollRate = $F('txtInterestRate');
        Payroll.ProcessingFee = $F('txtProcessingFee');
        Payroll.CompanyCode = $F('txtCompanyCode');
        Payroll.Remarks = $F('tareaRemarks');


        var res = PlussAuto.IRSettingsForPayroll.savePayroll(Payroll);
        //        alert(res.error.Message);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Payroll Save successfully");
        }

        iRSettingsforPayrollManager.render();
        util.hideModal('companyPopupDiv');





    }
    //    InsuranceDeleteById: function(insurenceId) {
    //        var res = PlussAuto.InsuranceAndBANCASettings.InsuranceDeleteById(insurenceId);
    //        if (res.error) {
    //            alert(res.error.Message);
    //            return;
    //        }
    //        if (res.value == "Success") {
    //            alert("Insurance and Banca Delete successfully");
    //        }

    //        iRSettingsforPayrollManager.render();
    //    }
};
var iRSettingsforPayrollHelper = {

    renderCollection: function(res) {
        Page.iRSettingsforPayrollArray = res.value;
        var html = [];
        for (var i = 0; i < Page.iRSettingsforPayrollArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            Page.iRSettingsforPayrollArray[i].CatType = categoryType[Page.iRSettingsforPayrollArray[i].CategoryName];
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{CompanyName}</td><td>#{CatType}</td><td>#{PayrollRate}</td><td>#{ProcessingFee}</td><td><a title="Edit" href="javascript:;" onclick="iRSettingsforPayrollHelper.update(#{AutoPayRateId})" class="icon iconEdit"></a> \
                            </td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.iRSettingsforPayrollArray[i])
			     );
        }
        $('companyListList').update(html.join(''));
        var totalCount = 0;
        if (res.value.length != 0) {
            totalCount = Page.iRSettingsforPayrollArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);
    },
    update: function(autoPayRateId) {
        $$('#companyPopupDiv .txt').invoke('clear');
        iRSettingsforPayrollHelper.clearForm();
        var Payroll = Page.iRSettingsforPayrollArray.findByProp('AutoPayRateId', autoPayRateId);
        Page.iRSettingsforPayroll = Payroll || { autoPayRateId: util.uniqId() };
        if (Payroll) {
            $('txtCompanyName').setValue(Payroll.CompanyName);
            $('txtCompanyCode').setValue(Payroll.CompanyCode);
            $('cmbCategory').setValue(Payroll.CategoryName);
            $('txtInterestRate').setValue(Payroll.PayrollRate);
            $('txtProcessingFee').setValue(Payroll.ProcessingFee);
            $('tareaRemarks').setValue(Payroll.Remarks);
        }
        else {
            $('cmbCategory').setValue('-1');
        }

        util.showModal('companyPopupDiv');
    },
    //    remove: function(insCompanyId) {
    //        var confirmed = window.confirm("Do you want to delete this data?");
    //        if (confirmed) {
    //            iRSettingsforPayrollManager.InsuranceDeleteById(insCompanyId);
    //        }
    //    },
    clearForm: function() {
        $('txtCompanyName').setValue('');
        $('txtCompanyCode').setValue('');

        $('cmbCategory').setValue('1');
        $('txtInterestRate').setValue('');
        $('txtProcessingFee').setValue('');
        $('tareaRemarks').setValue('');
    },
    searchKeypress: function(e) {
        if (event.keyCode == 13) {
            iRSettingsforPayrollManager.render();
        }
    }
};

Event.onReady(Page.init);
