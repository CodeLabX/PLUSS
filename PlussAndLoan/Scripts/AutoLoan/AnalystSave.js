﻿var analystSaveManager = {
    inprocessOfllId: function(llid) {
        var res = PlussAuto.AnalyzeLoanApplication.AppealedByAutoLoanMasterId(llid);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Precess Succeed");
            analyzeLoanApplicationManager.getLoanSummary();
        } else {
            alert("Process not Succeeded");
        }
    },
    SetLLIDAndSearch: function(llid) {
        //    analyzeLoanApplicationManager.getVehicleStatus();
        //    analyzeLoanApplicationManager.getManufacturer();
        //    analyzeLoanApplicationManager.getVendor();
        //    analyzeLoanApplicationManager.GetAllAutoBranch();
        //    analyzeLoanApplicationManager.getBank();
        //    analyzeLoanApplicationManager.getFacility();
        //    analyzeLoanApplicationManager.getSector();
        //    analyzeLoanApplicationManager.getCibInfo();
        //    analyzeLoanApplicationManager.getIncomeAssement();


        analyzeLoanApplicationManager.GetEmployerForSalaried();

        //Segment Settings
        analyzeLoanApplicationManager.GetEmployeSegment();
        analyzeLoanApplicationManager.getGeneralInsurance();
        analyzeLoanApplicationManager.renderOtherDamage();
        analyzeLoanApplicationManager.GetUserData();
        FinalizationTabManager.FinalizationTabOnLoad();





        $('txtLLID').setValue(llid);
        $('maindivContent').style.display = 'block';
        $('divAutoloanSummary').style.display = 'none';
        $("spnWait").innerHTML = "Please wait ....";
        analyzeLoanApplicationManager.GetDataByLLID();
    },
    close: function() {
        var url = "../../UI/AutoLoan/AnalystSummary.aspx";
        document.location.href = url;
        //window.open(url);
    },
    printBooklet: function() {
        var llid = $F("ctl00_ContentPlaceHolder_txtLLID");
        var url = "../../UI/AutoLoan/LoanApplicationPrint.aspx?llid=" + llid;
        window.open(url, '_blank');
        window.focus();
    },
    printOfferLetter: function() {
        var llid = $F("ctl00_ContentPlaceHolder_txtLLID");
        var url = "../../UI/AutoLoan/OfferLetterPrint.aspx?llid=" + llid;
        window.open(url, '_blank');
        window.focus();
    },
    printLLI: function() {
        var llid = $F("ctl00_ContentPlaceHolder_txtLLID");
        var url = "../../UI/AutoLoan/LLIPrint.aspx?llid=" + llid;
        window.open(url, '_blank');
        window.focus();
    },
    printBasel: function() {
        var llid = $F("ctl00_ContentPlaceHolder_txtLLID");
        var url = "../../UI/AutoLoan/BASEL2Print.aspx?llid=" + llid;
        window.open(url, '_blank');
        window.focus();

    },
    printLoanCheckList: function() {
        var llid = $F("ctl00_ContentPlaceHolder_txtLLID");
        var url = "../../UI/AutoLoan/AutoLoanCheckListPrint.aspx?llid=" + llid;
        window.open(url, '_blank');
        window.focus();
    },
    printDocumentList: function() {
        var llid = $F("ctl00_ContentPlaceHolder_txtLLID");
        var url = "../../UI/AutoLoan/DocumentCheckPrint.aspx?llid=" + llid;
        window.open(url, '_blank');
        window.focus();
    },

    printIncomeAssmentList: function() {
        //        var llid = $F("ctl00_ContentPlaceHolder_txtLLID");
        //        var url = "../../UI/AutoLoan/DocumentCheckPrint.aspx?llid=" + llid;
        //        window.open(url, '_blank');
        //        window.focus();
        //        debugger;
        //        var html = "";
        //        var incomeGenerator = $("tabIncomeGenerator").innerHTML;
        //        var loanCalculator = $("tabLoanCalculator").innerHTML;

        //        var totalhtml = incomeGenerator + loanCalculator;
        //        var res = PlussAuto.AnalystDetails.SaveIncomeAssementHtml(totalhtml);
        //        if (res.error) {
        //            alert(res.error.Message);
        //            return;
        //        }
        //        if (res.value == "Success") {
        //            var url = "../../UI/AutoLoan/IncomeAssementPrint.aspx";
        //            window.open(url, '_blank');
        //            window.focus();
        //        } else {
        //            alert("Print out not Succeeded");
        //        }

                var llid = $F("ctl00_ContentPlaceHolder_txtLLID");
                var url = "../../UI/AutoLoan/IncomeAssementPrint.aspx?llid=" + llid;
                window.open(url, '_blank');
                window.focus();

    },

    //Decline
    Decline: function() {
        var autoloanMasterId = $F("txtAutoLoanMasterId");
        var remarks = $F("tAnalystRemark");

        var res = PlussAuto.AnalystDetails.ChangeStateForAnalyst(autoloanMasterId, remarks, 38);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Decline Succeed");
            //            analystSaveManager.close();
            //            analyzeLoanApplicationManager.getLoanSummary();
            var url = "../../UI/AutoLoan/AnalystSummary.aspx";
            location.href = url;
        } else {
            alert("Decline not Succeeded");
        }
    },

    //Reject
    Reject: function() {
        var autoloanMasterId = $F("txtAutoLoanMasterId");
        var remarks = $F("tAnalystRemark");

        var res = PlussAuto.AnalystDetails.ChangeStateForAnalyst(autoloanMasterId, remarks, 37);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Reject Succeed");
            //            analystSaveManager.close();
            //            analyzeLoanApplicationManager.getLoanSummary();
            var url = "../../UI/AutoLoan/AnalystSummary.aspx";
            location.href = url;
        } else {
            alert("Reject not Succeeded");
        }
    },

    //On Hold
    OnHold: function() {
        var autoloanMasterId = $F("txtAutoLoanMasterId");
        var remarks = $F("tAnalystRemark");

        var res = PlussAuto.AnalystDetails.ChangeStateForAnalyst(autoloanMasterId, remarks, 28);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("On-Hold Succeed");
            //            analystSaveManager.close();
            //            analyzeLoanApplicationManager.getLoanSummary();
            var url = "../../UI/AutoLoan/AnalystSummary.aspx";
            location.href = url;
        } else {
            alert("On-Hold not Succeeded");
        }
    },

    //Back Ward
    Backward: function() {
        var autoloanMasterId = $F("txtAutoLoanMasterId");
        var remarks = $F("tAnalystRemark");

        var res = PlussAuto.AnalystDetails.ChangeStateForAnalyst(autoloanMasterId, remarks, 21);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Backward Succeed");
            //            analystSaveManager.close();
            //            analyzeLoanApplicationManager.getLoanSummary();
            var url = "../../UI/AutoLoan/AnalystSummary.aspx";
            location.href = url;
        } else {
            alert("Backward not Succeeded");
        }
    },

    //ForWard
    Forward: function() {

        var approverId = $F("cmbApprovedby");
        if (approverId == "-1" || approverId == "0") {
            alert("Please select a approver");
            //$("cmbApprovedby").focus();
            return false;
        }

        analystSaveHelper.CreateSaveObject(27);
        alert("Forward Succeeded");
        var url = "../../UI/AutoLoan/AnalystSummary.aspx";
        location.href = url;
    },

    //Save Data
    SaveData: function() {
        analystSaveHelper.CreateSaveObject(0);
    }

};

var analystSaveHelper = {
    CreateSaveObject: function(stateid) {

        if (analystSaveHelper.validateSaveobject(stateid)) {
            //General Information


            var objAutoAnalistMaster = saveObjectHelper.anlystMasterObject();
            var objAutoLoanAnalystApplication = saveObjectHelper.autoLoanAnalystApplication();
            var objAutoLoanAnalystVehicleDetail = saveObjectHelper.autoLoanAnalystVehicleDetail();
            var objAutoApplicantAccountScbList = saveObjectHelper.autoApplicantAccountSCBList();
            var objAutoApplicantAccountOtherList = saveObjectHelper.autoApplicantAccountOtherList();
            var objAutoFacilityListForScb = saveObjectHelper.autoFacilityListForScb();
            var objAutoSecurityListForScb = saveObjectHelper.AutoSecurityListForScb();
            var objOffscbFacilityList = saveObjectHelper.OFFSCBFacilityList();

            var objAutoLoanVehicle = saveObjectHelper.CreateAutoLoanVehicleObject();
            var objAutoLoanVendore = saveObjectHelper.CreateAutoLoanVendoreObject();

            //Loan Calculator

            //Total Loan Amount
            var objLoanCalculationTotal = LoanCalculationSaveManager.getLoanCalculationTotalObject();
            //INSURANCE (BDT)
            var objLoanCalculationInsurance = LoanCalculationSaveManager.getLoanCalculationInsuranceObject();
            //LOAN AMOUNT (BDT)
            var objLoanCalculationAmount = LoanCalculationSaveManager.getLoanCalculationAmountObject();
            //INTEREST
            var objLoanCalculationInterest = LoanCalculationSaveManager.getLoanCalculationInterestObject();
            //TENOR (Years)
            var objLoanCalculationTenor = LoanCalculationSaveManager.getLoanCalculationTenorObject();
            //EXISTING REPAYMENTS DBR
            var objRepaymentCapability = LoanCalculationSaveManager.getRepaymentCapabilityObject();
            //SEGMENTATION
            var objSegmentIncome = LoanCalculationSaveManager.getIncomeObject();
            //INCOME
            var objApplicantIncomeList = LoanCalculationSaveManager.getApplicantIncomeObject();
            //LOAN FINILAZITATION

            //create object for Auto_An_FinalizationDeviation
            var objFinalizationDeviationObject = FinalizationSaveManager.getFinalizationDeviationObject();
            //create object for Auto_An_FinalizationDevFound
            var objFinalizationDevFoundObject = FinalizationSaveManager.getFinalizationDevFoundObject();
            //create object for Auto_An_FinalizationSterength
            
            var objFinalizationSterengthObject = FinalizationSaveManager.getFinalizationSterengthObject();
            //create object for Auto_An_FinalizationWeakness
            var objFinalizationWeaknessObject = FinalizationSaveManager.getFinalizationWeaknessObject();
            //create object for Auto_An_FinalizationRepaymentMethod
            var objFinalizationRepaymentMethodObject = FinalizationSaveManager.getFinalizationRepaymentMethodObject();
            //create object for Auto_An_FinalizationCondition
            var objFinalizationConditionObject = FinalizationSaveManager.getFinalizationConditionObject();
            //create object for Auto_An_FinalizationRemark
            var objFinalizationRemarkObject = FinalizationSaveManager.getFinalizationRemarkObject();
            //create object for Auto_An_FinalizationReqDoc
            var objFinalizationReqDocObject = FinalizationSaveManager.getFinalizationReqDocObject();
            //create object for auto verification report
            var objVerificationReportObject = FinalizationSaveManager.getVerificationReportObject();

            //create object for auto Appriser object
            var objAppriserObject = FinalizationSaveManager.getAppriserAndApproverObject();

            //create object for Auto_An_AlertVerificationReport
            var objAlertVerificationReportObject = FinalizationSaveManager.getAlertVerificationReportObject();
            //create object for Auto_An_FinalizationRework
            var objFinalizationReworkObject = FinalizationSaveManager.getFinalizationReworkObject();
            //Create Cib Requested Loan

            var objCibrequestLoan = FinalizationSaveManager.getCibRequestLoanObject();

            // Income Generator
            //Landlord Primary and joint
            var objLandLord = saveObjectHelper.LandlordObject();
            //Salaried Primary and Joint

            var objSalaried = saveObjectHelper.SalariedObject();

            //Income Bank Statement
            var objIncomeForBankStatementList = saveObjectHelper.IncomeForBankStatement();

            //Save Object create---------------------------------------------
            var objAutoAnalystAll = new Object();
            //General Tab
            objAutoAnalystAll.AutoAnalistMaster = objAutoAnalistMaster;
            objAutoAnalystAll.AutoLoanAnalystApplication = objAutoLoanAnalystApplication;
            objAutoAnalystAll.AutoLoanVehicle = objAutoLoanVehicle;
            objAutoAnalystAll.AutoLoanVendore = objAutoLoanVendore;


            objAutoAnalystAll.AutoLoanAnalystVehicleDetail = objAutoLoanAnalystVehicleDetail;
            objAutoAnalystAll.AutoApplicantAccountScbList = objAutoApplicantAccountScbList;
            objAutoAnalystAll.AutoApplicantAccountOtherList = objAutoApplicantAccountOtherList;
            objAutoAnalystAll.AutoFacilityList = objAutoFacilityListForScb;
            objAutoAnalystAll.AutoSecurityList = objAutoSecurityListForScb;
            objAutoAnalystAll.OFfscbFacilityList = objOffscbFacilityList;
            //Income Generator
            objAutoAnalystAll.Salaried = objSalaried;
            objAutoAnalystAll.LandLord = objLandLord;
            objAutoAnalystAll.AutoAnalystIncomBankStatementSum = objIncomeForBankStatementList;
            //Loan Calculator
            objAutoAnalystAll.AutoAnLoanCalculationTotal = objLoanCalculationTotal;
            objAutoAnalystAll.AutoAnLoanCalculationInsurance = objLoanCalculationInsurance;
            objAutoAnalystAll.AutoAnLoanCalculationAmount = objLoanCalculationAmount;
            objAutoAnalystAll.AutoAnLoanCalculationInterest = objLoanCalculationInterest;
            objAutoAnalystAll.AutoAnLoanCalculationTenor = objLoanCalculationTenor;
            objAutoAnalystAll.AutoAnRepaymentCapability = objRepaymentCapability;
            objAutoAnalystAll.AutoAnIncomeSegmentIncome = objSegmentIncome;
            objAutoAnalystAll.AutoAnIncomeSegmentApplicantIncomeList = objApplicantIncomeList;
            //Finalization Tab
            objAutoAnalystAll.AutoAnFinalizationDeviation = objFinalizationDeviationObject;
            objAutoAnalystAll.AutoAnFinalizationDevFound = objFinalizationDevFoundObject;
            objAutoAnalystAll.AutoAnFinalizationSterength = objFinalizationSterengthObject;
            objAutoAnalystAll.AutoAnFinalizationWeakness = objFinalizationWeaknessObject;
            objAutoAnalystAll.AutoAnFinalizationRepaymentMethod = objFinalizationRepaymentMethodObject;
            objAutoAnalystAll.AutoAnFinalizationCondition = objFinalizationConditionObject;
            objAutoAnalystAll.AutoAnFinalizationRemark = objFinalizationRemarkObject;
            objAutoAnalystAll.AutoAnFinalizationReqDoc = objFinalizationReqDocObject;
            objAutoAnalystAll.AutoAnFinalizationAppraiserAndApprover = objAppriserObject;
            objAutoAnalystAll.AutoAnFinalizationVarificationReport = objVerificationReportObject;
            objAutoAnalystAll.AutoAnAlertVerificationReport = objAlertVerificationReportObject;
            objAutoAnalystAll.AutoAnFinalizationRework = objFinalizationReworkObject;
            objAutoAnalystAll.AutoCibrequestLoan = objCibrequestLoan;

            //var res = PlussAuto.AnalyzeLoanApplication.SaveAnalystLoanApplication(objAutoAnalystAll, stateid);
            var res = PlussAuto.AnalystDetails.SaveAnalystLoanApplication(objAutoAnalystAll, stateid);
            if (res.error) {
                alert(res.error.Message);
                return;
            }
            else {
                if (res.value == "Success") {
                    alert("Data Save Successfully");
                    var url = "../../UI/AutoLoan/AnalystSummary.aspx";
                    location.href = url;
                }
                else {
                    alert("Failed to Save Data");
                }
            }



        }
    },
    //Validate
    validateSaveobject: function(stateId) {
        var mesg = true;
        if ($F("cmbColleterization") == "-1" && $F("cmbColleterizationWith") != "-1") {
            alert("Please select Colleterization With");
            //$("cmbColleterizationWith").focus();
            mesg = false;
        }
        if ($F("cmbColleterization") != "-1" && $F("cmbColleterizationWith") == "-1") {
            alert("Please select Colleterization");
            //$("cmbColleterization").focus();
            mesg = false;
        }
        if ($F("cmbApprovedby") == "-1" && stateId != 0) {
            alert("Please select Approver Name in finalization Tab");
            mesg = false;
        }

        if (parseInt($F("txtloanAmountForLcLoanSummary")) > 2000000) {
            alert("Maximum Loan Amount cannot more then 2000000");
            mesg = false;
        }

        return mesg;
    }

};