﻿var vehicleType = { 'Sedan': 'S1', 'Station wagon': 'S2', 'SUV': 'S3', 'Pick-Ups': 'S4', 'Microbus': 'S5', 'MPV': 'S6' };
var Page = {
    pageSize: 15,
    pageNo: 0,
    pager: null,
    tanor: null,
    tanorArray: [],
    manufacturerArray: [],
    modelArray: [],
    vehicleId:0,

    init: function() {
        util.prepareDom();
        Event.observe('lnkAddTanor', 'click', tanorHelper.update);
        Event.observe('lnkSave', 'click', tanorManager.save);
        Event.observe('cboManufacturer', 'change', tanorHelper.changeManufacture);
        Event.observe('cmbModel', 'change', tanorHelper.changeModel);
        Event.observe('btnSearch', 'click', tanorManager.render);
        Event.observe('lnkClose', 'click', util.hideModal.curry('tanorAndLTVPopupDiv'));

        //Event.observe('lnkUploadSave', 'click', Page.upload);

  //        Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });
        tanorManager.render();
        tanorManager.getManufacturer();
        tanorManager.getStatus();
        
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        tanorManager.render();
    }

};

var tanorManager = {
    render: function() {

        var searchParam = $F('txtSearch');
        var res = PlussAuto.TenorAndLTVSettings.GetTenorList(Page.pageNo, Page.pageSize, searchParam);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        tanorHelper.renderCollection(res);
    },

    getManufacturer: function() {

        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.TenorAndLTVSettings.GetAutoManufacturer();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.manufacturerArray = res.value;

        tanorHelper.populateManufacturerCombo(res);

    },
    getStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.TenorAndLTVSettings.GetStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        tanorHelper.populateStatusCombo(res);
    },


    save: function() {
        if (!util.validateEmpty('cboManufacturer')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('cmbModel')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }


        if (!util.validateEmpty('cmbVehicleStatus')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        if (!util.validateEmpty('txtTenor')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateNumber('txtTenor')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtLTV')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('cmbValuePackAllowed')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
      
        var tanorAndltv = Page.tanor;
        tanorAndltv.ManufacturerId = $F('cboManufacturer');

        tanorAndltv.VehicleId = $F('cmbModel');

        tanorAndltv.StatusId = $F('cmbVehicleStatus');
        tanorAndltv.Tenor = $F('txtTenor');
        tanorAndltv.LTV = $F('txtLTV');
        tanorAndltv.ValuePackAllowed = $F('cmbValuePackAllowed');




        var res = PlussAuto.TenorAndLTVSettings.SaveAuto_TenorAndLTV(tanorAndltv);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Tenor & LTV Save successfully");
        }

        tanorManager.render();
        util.hideModal('tanorAndLTVPopupDiv');




    },

    getModelbyManufacturerID: function(manufacturerId) {
        var res = PlussAuto.TenorAndLTVSettings.getModelbyManufacturerID(manufacturerId);

        if (res.error) {
            alert(res.error.Message);
            return;
        }

        Page.modelArray = res.value;

        tanorHelper.populateModelCombo(res);
    },
    TanorLtvDeleteById: function(tanorLtvID) {
    var res = PlussAuto.TenorAndLTVSettings.TanorLtvDeleteById(tanorLtvID);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Tenor and LTV Delete successfully");
        }

        tanorManager.render();
    }

};

var tanorHelper = {
    renderCollection: function(res) {
        Page.tanorArray = res.value;
        var html = [];
        for (var i = 0; i < Page.tanorArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{ManufacturerName}</td><td>#{ManufacturCountry}</td><td>#{Model}</td><td>#{StatusName}</td><td>#{Tenor}</td><td>#{LTV}</td><td><a title="Edit" href="javascript:;" onclick="tanorHelper.update(#{TanorLtvID})" class="icon iconEdit"></a> \
                            <a title="Delete" href="javascript:;" onclick="tanorHelper.remove(#{TanorLtvID})" class="icon iconDelete"></a></td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.tanorArray[i])
			     );
        }
        $('tanorlist').update(html.join(''));
        var totalCount = 0;
        if (res.value.length != 0) {
            totalCount = Page.tanorArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);
    },

    update: function(tanorLtvID) {
        Page.vehicleId = 0;
        $$('#tanorAndLTVPopupDiv .txt').invoke('clear');
        tanorHelper.clearForm();
        var tenor = Page.tanorArray.findByProp('TanorLtvID', tanorLtvID);
        Page.tanor = tenor || { tanorLtvID: util.uniqId() };
        if (tenor) {
            $('cboManufacturer').setValue(tenor.ManufacturerId);
            Page.vehicleId = tenor.VehicleId;
            tanorManager.getModelbyManufacturerID(tenor.ManufacturerId);

            $('txtManufacturingCountry').setValue(tenor.ManufacturCountry);
            //tanorHel.changeModel();
            $('txtTrimLevel').setValue(tenor.TrimLevel);
            $('txtCC').setValue(tenor.CC);
            $('cmbVehicleStatus').setValue(tenor.StatusId);
            $('txtTenor').setValue(tenor.Tenor);
            $('txtLTV').setValue(tenor.LTV);
            $('cmbValuePackAllowed').setValue(tenor.ValuePackAllowed);
            $('cmbVehicleType').setValue(vehicleType[tenor.VehicleType]);
        }


        util.showModal('tanorAndLTVPopupDiv');
    },


    remove: function(tanorLtvID) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            tanorManager.TanorLtvDeleteById(tanorLtvID);
        }
    },
    clearForm: function() {
        $('cboManufacturer').setValue('');
        $('txtManufacturingCountry').setValue('');
        $('cmbModel').setValue('');
        $('txtTrimLevel').setValue('');
        $('txtCC').setValue('');
        $('cmbVehicleStatus').setValue('');
        $('txtTenor').setValue('');
        $('txtLTV').setValue('');
        $('cmbValuePackAllowed').setValue('1');
        $('cmbVehicleType').setValue('-1');
    },

    populateManufacturerCombo: function(res) {

        //document.getElementById('cboManufacturer').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ManufacturerName;
            opt.value = res.value[i].ManufacturerId;
            document.getElementById('cboManufacturer').options.add(opt);
        }

    },
    populateModelCombo: function(res) {

        document.getElementById('cmbModel').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            var ch = res.value[i].Model;
            if (res.value[i].TrimLevel != "") {
                ch += "-" + res.value[i].TrimLevel;

            }
            opt.text = ch;
            opt.value = res.value[i].VehicleId;
            document.getElementById('cmbModel').options.add(opt);
        }

        if (Page.vehicleId != 0) {
            $('cmbModel').setValue(Page.vehicleId);

            tanorHelper.changeModel();
        }

    },

    searchKeypress: function(e) {

        //debugger;
        if (event.keyCode == 13) {
            tanorManager.render();
        }
    },
    populateStatusCombo: function(res) {
        //document.getElementById('cmbVehicleStatus').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmbVehicleStatus').options.add(opt);
        }
    },
    changeManufacture: function() {

        var manufacturerID = $F('cboManufacturer');

        var manufacturer = Page.manufacturerArray.findByProp('ManufacturerId', manufacturerID);
        $('txtManufacturingCountry').setValue(manufacturer.ManufacturCountry);
        tanorManager.getModelbyManufacturerID(manufacturerID);
    },
    changeModel: function() {

        var modelID = $F('cmbModel');
        var model = Page.modelArray.findByProp('VehicleId', modelID);
        if (model) {
            $('txtCC').setValue(model.CC);
            $('txtTrimLevel').setValue(model.TrimLevel);
            $('cmbVehicleType').setValue(vehicleType[model.Type]);
        }
        //tanorManager.getTenorandCC(modelID)
    }
};
Event.onReady(Page.init);