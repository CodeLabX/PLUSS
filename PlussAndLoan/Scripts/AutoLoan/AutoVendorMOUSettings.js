﻿
var InsurenceType = { 1: 'General', 2: 'Life', 3: 'General & Life' };
var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    insurence: null,
    IRSettingArray: [],

    init: function() {
        util.prepareDom();
        Event.observe('lnkAddNewIRSettingsForVehicle', 'click', VendorMOUSettingsHelper.update);
        Event.observe('lnkSave', 'click', VendorMOUSettingsManager.save);
        //    Event.observe('btnSearch', 'click', VendorMOUSettingsManager.render);
        Event.observe('lnkClose', 'click', util.hideModal.curry('VendorMOUSettingsPopupDiv'));

        //    Event.observe('lnkUploadSave', 'click', Page.upload);

        //    Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });

        VendorMOUSettingsManager.render();
        VendorMOUSettingsManager.getStatus();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        VendorMOUSettingsManager.render();
        VendorMOUSettingsManager.getStatus();
    }

};

var VendorMOUSettingsManager = {

    render: function() {
        var vendorName = $F('txtSearch');
        var res = PlussAuto.VendorMOUSettings.GetAutoVendorMOUSummary(Page.pageNo, Page.pageSize, vendorName);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        VendorMOUSettingsHelper.renderCollection(res);
    },
    getStatus: function() {
        //var comapanyName = $F('txtSearch');
        var res = PlussAuto.VendorMOUSettings.GetStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        VendorMOUSettingsHelper.populateStatusCombo(res);
    },


    save: function() {
        if (!util.validateEmpty('txtVendorName')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        var IRWithARTA = $F('txtIRWithARTA');
        if (!util.isFloat(IRWithARTA)) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        if (!util.validateEmpty('cmbVehicleStatus')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('cmbIsMou')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }


        var a = $F('txtIRWithoutARTA');
        if (!util.isFloat(a)) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        var AVendorMOU = Page.insurence;

        AVendorMOU.VendorName = $F('txtVendorName');
        AVendorMOU.VendorCode = $F('txtVendorCode');
        AVendorMOU.DealerType = $F('cmbVehicleStatus'); // dealar Type
        AVendorMOU.IsMou = $F('cmbIsMou');
        AVendorMOU.Address = $F('txtAddress');
        AVendorMOU.phone = $F('txtPhone');
        AVendorMOU.Website = $F('txtWebsite');

        //AVendorMOU.AutoVendorId = $F('cmbVehicleStatus');
        AVendorMOU.IrWithArta = $F('txtIRWithARTA');
        if (AVendorMOU.IrWithArta == "") {
            AVendorMOU.IrWithArta = 0;
        }
        AVendorMOU.IrWithoutArta = $F('txtIRWithoutARTA');
        if (AVendorMOU.IrWithoutArta == "") {
            AVendorMOU.IrWithoutArta = 0;
        }
        AVendorMOU.ProcessingFee = $F('txtProcessingFee');
        if (AVendorMOU.ProcessingFee == "") {
            AVendorMOU.ProcessingFee = 0;
        }
        AVendorMOU.ValidFrom = Date.parseUK($F('txtValidFrom'));
        AVendorMOU.LastUpdateDate = AVendorMOU.ValidFrom;

        var res = PlussAuto.VendorMOUSettings.SaveAuto_VendorMOU(AVendorMOU);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Vendor MOU Settings Save successfully");
        }

        VendorMOUSettingsManager.render();
        util.hideModal('VendorMOUSettingsPopupDiv');
    },
    VendorMOUDeleteById: function(VendorMouId) {
        var res = PlussAuto.VendorMOUSettings.AutoVendorMOUDeleteById(VendorMouId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Vendor Delete successfully");
        }

        VendorMOUSettingsManager.render();
    }
};
var VendorMOUSettingsHelper = {
    renderCollection: function(res) {
        Page.IRSettingArray = res.value;
        var html = [];
        for (var i = 0; i < Page.IRSettingArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            //            Page.IRSettingArray[i].InsTypeName = InsurenceType[Page.IRSettingArray[i].InsType];
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{VendorName}</td><td>#{phone}</td><td>#{DealerStatus}</td><td>#{IrWithArta}</td><td>#{IrWithoutArta}</td>\
            <td><a title="Edit" href="javascript:;" onclick="VendorMOUSettingsHelper.update(#{AutoVendorId})" class="icon iconEdit"></a><a title="Delete" href="javascript:;" onclick="VendorMOUSettingsHelper.remove(#{AutoVendorId})" class="icon iconDelete"></a> \
                            </td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.IRSettingArray[i])
			     );
        }
        $('IRSettingsList').update(html.join(''));
        if (res.value.length != 0) {
            Page.pager.reset(Page.IRSettingArray[0].TotalCount, Page.pageNo);
        }


    },
    
    update: function(vendorMouId) {
        $$('#VendorMOUSettingsPopupDiv .txt').invoke('clear');
        VendorMOUSettingsHelper.clearForm();
        var insurence = Page.IRSettingArray.findByProp('AutoVendorId', vendorMouId);
        Page.insurence = insurence || { vendorMouId: util.uniqId() };
        if (insurence) {
            $('txtVendorName').setValue(insurence.VendorName);
            $('txtVendorCode').setValue(insurence.VendorCode);
            $('cmbVehicleStatus').setValue(insurence.DealerType);
            $('cmbIsMou').setValue(insurence.IsMou);
            $('txtAddress').setValue(insurence.Address);
            $('txtPhone').setValue(insurence.phone);
            $('txtWebsite').setValue(insurence.Website);
            $('txtIRWithARTA').setValue(insurence.IrWithArta);
            $('txtIRWithoutARTA').setValue(insurence.IrWithoutArta);
            $('txtProcessingFee').setValue(insurence.ProcessingFee);
            $('txtValidFrom').setValue(insurence.ValidFrom.format());
            //            $('txtValidFrom').setValue(insurence.ValidFrom);
        }

        else {
            $('cmbVehicleStatus').setValue('-1');
        }



        util.showModal('VendorMOUSettingsPopupDiv');
    },
    remove: function(VendorMouId) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            VendorMOUSettingsManager.VendorMOUDeleteById(VendorMouId);
        }

    },
    clearForm: function() {
        $('txtIRWithARTA').setValue('');
        $('cmbVehicleStatus').setValue('1');
        $('txtIRWithoutARTA').setValue('');
        $('cmbIsMou').setValue('1');
        //        $('txtVehicleCC').setValue('');
        //        $('txtInsurancePer').setValue('');
    },
    searchKeypress: function(e) {
        if (event.keyCode == 13) {
            VendorMOUSettingsManager.render();
        }
    },
    populateStatusCombo: function(res) {
        document.getElementById('cmbVehicleStatus').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmbVehicleStatus').options.add(opt);
        }
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVehicleStatus').options.add(ExtraOpt);
    }
};

Event.onReady(Page.init);
