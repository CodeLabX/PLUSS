﻿
var productName = { 1: 'Staff Auto', 2: 'Conventional Auto', 3: 'Saadiq Auto' };
var vehicleStatus = { 1: 'Staff Auto', 2: 'Conventional Auto', 3: 'Saadiq Auto' };
var Page = {
    pageSize: 15,
    pageNo: 0,
    pager: null,
    LoanSummeryArray: [],
    loanApplication: null,


    init: function() {
    util.prepareDom();




    Event.observe('btnCancel', 'click', ApproverManager.CloseLoanApp);
    Event.observe('btnApprove', 'click', ApproverManager.Approved);
    Event.observe('btnApproveWithCondition', 'click', ApproverManager.ApproveWithCondition);
    Event.observe('btnReject', 'click', ApproverManager.Reject);
    Event.observe('btnDecline', 'click', ApproverManager.Decline);
    Event.observe('btnBackToAnalyst', 'click', ApproverManager.BackToAnalyst);
    ApproverManager.GetEmployeSegment();
    ApproverManager.getSector();
    ApproverManager.getIncomeAssement();
    ApproverManager.getManufacturer();
    ApproverManager.getVehicleStatus();
        
        
        
        Event.observe('btnLLIdSearch', 'click', ApproverManager.getLoanSummarySearch);
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNo'),
            spnTotalPage: $('spntotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });


        ApproverManager.getLoanSummary();

        var res = PlussAuto.Auto_ApproverSummary.getRemarkStrangthConditionWeaknessAll();
        ApproverHelper.getStregthConditionWeakness(res);

        var res = PlussAuto.Auto_ApproverSummary.getDeviationLevel();
        ApproverHelper.PopulateDeviationLevel(res);

        var res = PlussAuto.Auto_ApproverSummary.GetBank(1);
        ApproverHelper.PopulateOffBank(res);

        var res = PlussAuto.Auto_ApproverSummary.getDeviationByLevel(0);
        ApproverHelper.PolulateDeviationDescription(res);



    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        ApproverManager.getLoanSummary(Page.pageNo, Page.pageSize);
    }
};

var ApproverManager = {

    Decline: function() {
        var loanID = $F('txtAutoLoanMasterId');
        var remark = $F('tApproverRemark');
        var res = PlussAuto.Auto_ApproverSummary.Decline(loanID, remark);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else if (res.value == 'Success') {
            alert("Operation Successfull");
            ApproverManager.getLoanSummary();
            ApproverManager.CloseLoanApp();
        }
        else {
            alert("Operation Failed");
        }
    },


    Reject: function() {
        var loanID = $F('txtAutoLoanMasterId');
        var remrk = $F('tApproverRemark');
        var res = PlussAuto.Auto_ApproverSummary.Reject(loanID, remrk);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else if (res.value == 'Success') {
            alert("Operation Successfull");
            ApproverManager.getLoanSummary();
            ApproverManager.CloseLoanApp();
        }
        else {
            alert("Operation Failed");
        }
    },

    BackToAnalyst: function() {
        var loanID = $F('txtAutoLoanMasterId');
        var remrk = $F('tApproverRemark');
        var res = PlussAuto.Auto_ApproverSummary.BackToAnalyst(loanID, remrk);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else if (res.value == 'Success') {
            alert("Operation Successfull");
            ApproverManager.getLoanSummary();
            ApproverManager.CloseLoanApp();
        }
        else {
            alert("Operation Failed");
        }
    },
    ConditionallyApproved: function() {
        var loanID = $F('txtAutoLoanMasterId');
        var res = PlussAuto.Auto_ApproverSummary.ConditionallyApproved(loanID);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else if (res.value == 'Success') {
            alert("Operation Successfull");
            ApproverManager.getLoanSummary();
            ApproverManager.CloseLoanApp();
        }
        else {
            alert("Operation Failed");
        }
    },


    ApproveWithCondition: function() {
        var loanID = $F('txtAutoLoanMasterId');
        var remark = $F('tApproverRemark');

        if ($('chkCondition1').checked == true || $('chkCondition2').checked == true || $('chkCondition3').checked == true || $('chkCondition4').checked == true || $('chkCondition5').checked == true) {
            ApproverManager.ConditionallyApproved(loanID, remark);
        }

        else {

            var res = PlussAuto.Auto_ApproverSummary.ApproveWithCondition(loanID, remark);

            if (res.error) {
                alert(res.error.Message);
                return;
            } else if (res.value == 'Success') {
                alert("Operation Successfull");
                ApproverManager.getLoanSummary();
                ApproverManager.CloseLoanApp();
            } else {
                alert("Operation Failed");
            }
        }
    },



    Approved: function() {
        var loanID = $F('txtAutoLoanMasterId');
        var remark = $F('tApproverRemark');
        var res = PlussAuto.Auto_ApproverSummary.ApprovedAutoLoan(loanID, remark);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else if (res.value == 'Success') {
            alert("Operation Successfull");
            ApproverManager.getLoanSummary();
            ApproverManager.CloseLoanApp();
        }
        else {
            alert("Operation Failed");
        }
    },



    getLoanSummary: function() {
        var searchKey = $F('txtLLIDMain');
        if (searchKey == "") {
            searchKey = 0;
        }
        if (util.isDigit(searchKey)) {
            var res = PlussAuto.Auto_ApproverSummary.GetLoanSummary(Page.pageNo, Page.pageSize, searchKey);
            //debugger;
            if (res.error) {
                alert(res.error.Message);
                return;
            }
            //            if (res.value.length == 0 || res == null) {
            //                alert("No Data Found !!");
            //                return;
            //            }
            Page.LoanSummeryArray = res.value;

            ApproverHelper.populateLoanSummeryGrid(res);
        } else {
            alert("LLID Must to be in Degit");
            return false;
        }
    },

    getLoanSummarySearch: function() {
        Page.pageNo = 0;
        ApproverManager.getLoanSummary();
    },

    SetLLIDAndSearch: function(llid) {
        $('txtLLID').setValue(llid);
        $("divLoanDetails").style.display = 'block';
        $("divAutoloanSummary").style.display = 'none';
        ApproverManager.GetDataByLLID(llid);
    },

    GetEmployeSegment: function() {
        var res = PlussAuto.Auto_ApproverSummary.GetEmployeSegment();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        ApproverHelper.populateSegmentCombo(res);
    },

    getSector: function() {

        var res = PlussAuto.Auto_ApproverSummary.GetSector();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        ApproverHelper.populateSectorCombo(res);
    },

    getIncomeAssement: function() {
        var res = PlussAuto.Auto_ApproverSummary.GetIncomeAssement();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        ApproverHelper.populateIncomeAssementCombo(res);
    },
    getManufacturer: function() {
        var res = PlussAuto.Auto_ApproverSummary.GetAutoManufacturer();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        ApproverHelper.populateManufactureCombo(res);
    },
    getVehicleStatus: function() {
        var res = PlussAuto.Auto_ApproverSummary.getVehicleStatus(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        ApproverHelper.populateVehicleStatusCombo(res);
    },

    GetDataByLLID: function(llid) {

        var llid = $F('txtLLID');
        var res = PlussAuto.Auto_ApproverSummary.GetAllLoanInfoByLLID(llid);

        $('txtAutoLoanMasterId').setValue(res.value.AutoLoanAnalystMaster.AutoLoanMasterId);


        //Customar Information

        $('txtDateOfBirth').setValue(res.value.objAutoPRApplicant.DateofBirth.format());
        $('txtAge').setValue(res.value.objAutoPRApplicant.Age);
        $('txtDeclaredIncomePr').setValue(res.value.objAutoPRFinance.DPIAmount);
        $('txtDeclaredIncomeJt').setValue(res.value.objAutoJTFinance.DPIAmount);
        $('txtAssesedIncome').setValue(res.value.objAuto_An_IncomeSegmentIncome.TotalIncome);
        $('txuJointAppIncome').setValue('');

        $('cmbEmployeeSegment').setValue(res.value.objAuto_An_IncomeSegmentIncome.EmployeeSegment);

        if (res.value.objAutoPRProfession.PrimaryProfession == 2 || res.value.objAutoPRProfession.PrimaryProfession == 5) {
            $('cmbEmployeeCategory').setValue(12); // Professional Category
        }
        else {
            $('cmbEmployeeCategory').setValue(res.value.objAuto_An_IncomeSegmentIncome.Category);
        }

        $('txtEmployerCategoryCode').setValue(res.value.objAuto_An_IncomeSegmentIncome.CategoryCode);
        if (res.value.objAutoPRProfession.PrimaryProfession == 2 && res.value.objAuto_An_IncomeSegmentIncome.Category == 2) {
            $('cmbAssessmentMethod').setValue(-110); // Salarid
        }
        else if (res.value.objAutoPRProfession.PrimaryProfession == 2 && res.value.objAuto_An_IncomeSegmentIncome.Category == 1) {
            $('cmbAssessmentMethod').setValue(-111); // Salarid in Cash
        }
        else if (res.value.objAutoPRProfession.PrimaryProfession == 5) {
            $('cmbAssessmentMethod').setValue(-112); // Professional Category - Landlord
        }
        else {
            $('cmbAssessmentMethod').setValue(res.value.objAuto_An_IncomeSegmentIncome.AssesmentMethod);
        }
        $('txtAssesmentMethodCode').setValue(res.value.objAuto_An_IncomeSegmentIncome.AssesmentMethodCode);
        $('txtAppropiateIncome').setValue(res.value.objAuto_An_IncomeSegmentIncome.AppropriateIncome);
        $('txtTotalIncome').setValue(res.value.objAuto_An_IncomeSegmentIncome.TotalJointIncome);
        $('txtCustomarName').setValue(res.value.objAutoPRApplicant.PrName);
        var jointAppIncome = 0;
        var isJoint = res.value.objAutoLoanMaster.JointApplication
        if (isJoint) {
            jointAppIncome = res.value.objAuto_An_IncomeSegmentIncome.TotalJointIncome - res.value.objAuto_An_IncomeSegmentIncome.AppropriateIncome;
        }

        $('txuJointAppIncome').setValue(jointAppIncome);

        // Customar Information End


        //VENDOR & VEHICLE INFORMATION
        $('tVendor').setValue(res.value.objAutoVendorMou.VendorName);
        $('cmbProductName').setValue(res.value.objAutoLoanMaster.ProductId);
        $('tModel').setValue(res.value.objAutoLoanVehicle.Model);
        $('cmbVehicleStatue').setValue(res.value.objAutoLoanVehicle.VehicleStatus);
        $('tManufacturingYear').setValue(res.value.objAutoLoanVehicle.ManufacturingYear);
        $('tquotedPrice').setValue(res.value.objAutoLoanVehicle.QuotedPrice);
        $('cmbPriceConsideredOn').setValue(res.value.AutoLoanAnalystVehicleDetail.PriceConsideredOn);
        $('cmbVendoreRelationShip').setValue(res.value.objAutoVendorMou.IsMou);
        $('cmbVendorCategory').setValue(res.value.objAutoVendorMou.DealerType);
        $('cmbBrand').setValue(res.value.objAutoLoanVehicle.Auto_ManufactureId);
        $('tEngineCC').setValue(res.value.objAutoLoanVehicle.EngineCC);
        $('tVehicleType').setValue(res.value.objAutoLoanVehicle.VehicleType);
        $('tSeatingCapacity').setValue(res.value.AutoLoanAnalystVehicleDetail.SeatingCapacity);
        $('tconsideredPrice').setValue(res.value.AutoLoanAnalystVehicleDetail.ConsideredPrice);
        $('cmbCarVerification').setValue(res.value.AutoLoanAnalystVehicleDetail.CarVarification);
        // VENDOR & VEHICLE INFORMATION  End



        //LOAN INFORMATION
        $('tPOAmount').setValue(res.value.objAuto_An_LoanCalculationAmount.ApprovedLoan);
        $('tGeneralInsurance').setValue(res.value.objAuto_An_LoanCalculationInsurance.GeneralInsurance);
        $('tARTA').setValue(res.value.objAuto_An_LoanCalculationInsurance.ARTA);
        $('tTotalLoanAmount').setValue(res.value.objAuto_An_LoanCalculationTotal.TotalLoanAmount);
        $('tARTAFrom').setValue('');
        $('tInterestRate').setValue(res.value.objAuto_An_LoanCalculationInterest.ConsideredRate);
        $('tTenor').setValue(res.value.objAuto_An_LoanCalculationTenor.ConsideredTenor);
        $('tEMI').setValue(res.value.objAuto_An_RepaymentCapability.MaxEMI);
        $('tProfitMargine').setValue('');
        $('tLTV').setValue(res.value.objAuto_An_LoanCalculationTotal.LTVExcludingInsurance);
        $('tDBR').setValue(res.value.objAuto_An_LoanCalculationTotal.FBRExcludingInsurance);

        var expiryYear = new Date();
        var year = expiryYear.getFullYear();
        year += res.value.objAuto_An_LoanCalculationTenor.ConsideredTenor;
        $('tLoanExpiryYear').setValue(year);
        //LOAN INFORMATION


        //Deviation

        for (var i = 0; i < res.value.Auto_An_FinalizationDeviationList.length; i++) {
            var identity = i + 1;
            if (identity < 4) {
                $('cmbDeviationLevel' + identity).setValue(res.value.Auto_An_FinalizationDeviationList[i].Level);
                $('cmbDeviationDescription' + identity).setValue(res.value.Auto_An_FinalizationDeviationList[i].Description);
                $('tDeviationCode' + identity).setValue(res.value.Auto_An_FinalizationDeviationList[i].Code);
            }
        }

        //        $('cmbDeviationLevel2').setValue(res.value.Auto_An_FinalizationDeviationList[1].Level);
        //        $('cmbDeviationDescription2').setValue(res.value.Auto_An_FinalizationDeviationList[1].Description);
        //        $('tDeviationCode2').setValue(res.value.Auto_An_FinalizationDeviationList[1].Code);

        //        $('cmbDeviationLevel3').setValue(res.value.Auto_An_FinalizationDeviationList[1].Level);
        //        $('cmbDeviationDescription3').setValue(res.value.Auto_An_FinalizationDeviationList[1].Description);
        //        $('tDeviationCode3').setValue(res.value.Auto_An_FinalizationDeviationList[1].Code);

        //Deviation


        //Deviation Found

        $('tDevFoundAge').setValue(res.value.objAuto_An_FinalizationDevFound.Age);
        $('tDevFoundDBR').setValue(res.value.objAuto_An_FinalizationDevFound.DBR);
        $('tDevFoundSeatingCapacity').setValue(res.value.objAuto_An_FinalizationDevFound.SeatingCapacilty);
        $('tDevFoundSharePortion').setValue(res.value.objAuto_An_FinalizationDevFound.SharePortion);

        //Deviation Found End


        //Strength

        //        for (var i = 1; i <= res.value.Auto_An_FinalizationSterengthList.length; i++) {
        //            $('cmbStrength' + i).setValue(res.value.Auto_An_FinalizationSterengthList[i - 1].Strength);

        //        }
        var strengthOtherCount = 1;
        for (var i = 0; i < res.value.Auto_An_FinalizationSterengthList.length; i++) {
            var identityFieldFordev = i + 1;
            var textToFind = res.value.Auto_An_FinalizationSterengthList[i].Strength;
            var strengthAdded = false;
            if (i < 5) {
                var dd = document.getElementById('cmbStrength' + identityFieldFordev);
                for (var j = 0; j < dd.options.length; j++) {
                    if (dd.options[j].text === textToFind) {
                        dd.selectedIndex = j;
                        strengthAdded = true;
                        break;
                    }
                }
            }
            if (strengthAdded == false && strengthOtherCount < 3) {
                $('txtStrength' + strengthOtherCount).setValue(res.value.Auto_An_FinalizationSterengthList[i].Strength);
                strengthOtherCount += 1;
            }
        }

        //Strength End


        //Weakness

        //        for (var i = 1; i <= res.value.Auto_An_FinalizationWeaknessList.length; i++) {
        //            $('cmbWeakness' + i).setValue(res.value.Auto_An_FinalizationWeaknessList[i - 1].Weakness);

        //        }
        var weaknessOtherCount = 1;
        for (var i = 0; i < res.value.Auto_An_FinalizationWeaknessList.length; i++) {
            var identityFieldForWk = i + 1;
            var textToFind = res.value.Auto_An_FinalizationWeaknessList[i].Weakness;
            var weaknessAdded = false;
            if (i < 5) {
                var dd = document.getElementById('cmbWeakness' + identityFieldForWk);
                for (var j = 0; j < dd.options.length; j++) {
                    if (dd.options[j].text === textToFind) {
                        dd.selectedIndex = j;
                        weaknessAdded = true;
                        break;
                    }
                }
            }
            if (weaknessAdded == false && weaknessOtherCount < 3) {
                $('txtWeakness' + weaknessOtherCount).setValue(res.value.Auto_An_FinalizationWeaknessList[i].Weakness);
                weaknessOtherCount += 1;
            }
        }

        //Weakness End


        //Condition

        //        for (var i = 1; i <= res.value.Auto_An_FinalizationConditionList.length; i++) {
        //            $('cmbCondition' + i).setValue(res.value.Auto_An_FinalizationConditionList[i - 1].Condition);

        //        }
        var conditionOtherCount = 1;
        for (var i = 0; i < res.value.Auto_An_FinalizationConditionList.length; i++) {
            var identityFieldForWk = i + 1;
            var textToFind = res.value.Auto_An_FinalizationConditionList[i].Condition;
            var conditionAdded = false;
            if (i < 5) {
                var dd = document.getElementById('cmbCondition' + identityFieldForWk);
                for (var j = 0; j < dd.options.length; j++) {
                    if (dd.options[j].text === textToFind) {
                        dd.selectedIndex = j;
                        conditionAdded = true;
                        break;
                    }
                }
            }
            if (conditionAdded == false && conditionOtherCount < 3) {
                $('txtCondition' + conditionOtherCount).setValue(res.value.Auto_An_FinalizationConditionList[i].Condition);
                conditionOtherCount += 1;
            }
        }

        //Condition End

        //Remark
        $('tAnalystRemark').setValue(res.value.AutoLoanAnalystMaster.AnalystRemark);
        $('tApproverRemark').setValue(res.value.AutoLoanAnalystMaster.ApproverRemark);
        //Remark End

        //Exposer

        var onSCBindex = 1;
        var OFFScbIndex = 1;
        for (i = 1; i <= res.value.Auto_An_OverDraftFacilityList.length; i++) {

            if (res.value.Auto_An_OverDraftFacilityList[i - 1].BankId == '-1') {

                $('tLimit' + onSCBindex).setValue(res.value.Auto_An_OverDraftFacilityList[i - 1].LimitAmount);
                $('tInterest' + onSCBindex).setValue(res.value.Auto_An_OverDraftFacilityList[i - 1].Interest);
                $('tConsidered' + onSCBindex).setValue(res.value.Auto_An_OverDraftFacilityList[i - 1].Consider);

                onSCBindex += 1;
            }

            else {
                $('cmbBank' + OFFScbIndex).setValue(res.value.Auto_An_OverDraftFacilityList[i - 1].BankId);
                $('tOffLimit' + OFFScbIndex).setValue(res.value.Auto_An_OverDraftFacilityList[i - 1].LimitAmount);
                $('tOffInterest' + OFFScbIndex).setValue(res.value.Auto_An_OverDraftFacilityList[i - 1].Interest);
                $('tOffConsidered' + OFFScbIndex).setValue(res.value.Auto_An_OverDraftFacilityList[i - 1].Consider);

                OFFScbIndex += 1;
            }

        }






        //Exposer end

    },


    Appeal: function(appiledOnAutoLoanMasterId) {
        var res = PlussAuto.Auto_ApproverSummary.AppealedByAutoLoanMasterId(appiledOnAutoLoanMasterId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Process Succeed");
            ApproverManager.getLoanSummary();
        } else {
            alert("Process not Succeeded");
        }
    },
    CloseLoanApp: function() {
        //analyzeLoanApplicationManager.ClearLoanAppForm();
        ApproverManager.ClearAllFields();
        $("divLoanDetails").style.display = 'none';
        $("divAutoloanSummary").style.display = 'block';

        var url = "../../UI/AutoLoan/Auto_ApproverSummary.aspx";
        location.href = url;

    },

    ClearAllFields: function() {
        //analyzeLoanApplicationManager.ClearLoanAppForm();

        var texts = document.getElementsByTagName('input');
        for (var i_tem = 0; i_tem < texts.length; i_tem++) {
            if (texts[i_tem].type == 'text') {
                texts[i_tem].value = '';
            }
            if (texts[i_tem].type == 'hidden') {
                texts[i_tem].value = '0';
            }
        }

        var combo = document.getElementsByTagName('select');
        for (var i_tems = 0; i_tems < combo.length; i_tems++) {
            combo[i_tems].value = '-1';

        }
    }





};

var ApproverHelper = {


    populateLoanSummeryGrid: function(rss) {
        var html = [];
        for (var i = 0; i < rss.value.length; i++) {
            var btnWillBe = "";
            if (Page.LoanSummeryArray[i].StatusID == 17 || Page.LoanSummeryArray[i].StatusID == 19 || Page.LoanSummeryArray[i].StatusID == 21 || Page.LoanSummeryArray[i].StatusID == 24 || Page.LoanSummeryArray[i].StatusID == 27) {
                btnWillBe = '<input title="Process" type="button" id="btnAppeal" class="searchButton   widthSize100_per" value="Process" onclick=" ApproverManager.Appeal(' + Page.LoanSummeryArray[i].Auto_LoanMasterId + ')"/>';
            }
            else if (Page.LoanSummeryArray[i].StatusID == 18 || Page.LoanSummeryArray[i].StatusID == 20 || Page.LoanSummeryArray[i].StatusID == 22 || Page.LoanSummeryArray[i].StatusID == 23 || Page.LoanSummeryArray[i].StatusID == 25 || Page.LoanSummeryArray[i].StatusID == 26 || Page.LoanSummeryArray[i].StatusID == 28 || Page.LoanSummeryArray[i].StatusID == 29 || Page.LoanSummeryArray[i].StatusID == 33 || Page.LoanSummeryArray[i].StatusID == 34 || Page.LoanSummeryArray[i].StatusID == 35) {
                btnWillBe = '<input title="Open" type="button" id="btnPrintBA" class="searchButton   widthSize100_per" value="Open" onclick="ApproverManager.SetLLIDAndSearch(' + Page.LoanSummeryArray[i].LLID + ')"/>';
            }
            



            var className = i % 2 ? '' : 'odd';
            Page.LoanSummeryArray[i].btnWillBe = btnWillBe;
            html.push('<tr class="' + className + '"><td>#{LLID}</td><td>#{PrName}</td><td>#{ProductName}</td><td>#{ManufacturerName}</td><td>#{Model}</td><td>#{AppliedAmount}</td><td>#{AskingTenor}</td><td>#{StatusName}</td><td>#{btnWillBe}</td></tr>'
                    .format2((i % 2 ? '' : 'odd'), true)
                    .format(Page.LoanSummeryArray[i])
            );
        }
        $('GridLoanSummery').update(html.join(''));
        var totalCount = 0;

        if (rss.value.length != 0) {
            totalCount = Page.LoanSummeryArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);

    },

    clear: function() {
        $('txtApplicantName').setValue('');
        $('txtApplicationDate').setValue('');
        $('txtDateOfBirth').setValue('');
        $('txtAppliedAmount').setValue('');
        $('txtAskingTenor').setValue('');
    },

    getStregthConditionWeakness: function(res) {

        var linkStrength = "<option value=\"-1\">Select a Strength</option>";
        var linkWeakness = "<option value=\"-1\">Select a Weakness</option>";
        var linkCondition = "<option value=\"-1\">Select a Condition</option><option value=\"Board Resoulation Required For PDC/SI\">Board Resoulation Required For PDC/SI</option><option value=\"Car Import Docs is Required\">Car Import Docs is Required</option><option value=\"To be Paid in Cash amount\">To be Paid in Cash amount</option><option value=\"ARTA waiver required\">ARTA waiver required</option><option value=\"Registration to be verified\">Registration to be verified</option>";
        var linkRemark = "<option value=\"-1\">Select a Remark</option>";

        for (var i = 0; i < res.value.length; i++) {

            if (res.value[i].REMA_DTYPE == '2') {
                linkCondition += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
            else if (res.value[i].REMA_DTYPE == '3') {
                linkStrength += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
            else if (res.value[i].REMA_DTYPE == '4') {
                linkWeakness += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
        }

        for (var j = 1; j <= 5; j++) {
            $("cmbStrength" + j).update(linkStrength);
            $("cmbWeakness" + j).update(linkWeakness);
            $("cmbCondition" + j).update(linkCondition);
            //            if (j < 4) {
            //                $("cmbRemark" + j).update(linkRemark);
            //            }

        }

    },

    PopulateDeviationLevel: function(res) {

        var link = "<option value=\"-1\">Select a Level</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].DeviationFor + "\">" + "Level " + res.value[i].DeviationFor + "</option>";
        }
        for (var j = 1; j <= 3; j++) {
            $("cmbDeviationLevel" + j).update(link);
        }
    },

    PopulateOffBank: function(res) {

        var link = "<option value=\"-1\">Select</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].BankID + "\">" + res.value[i].BankName + "</option>";
        }
        for (var j = 1; j <= 4; j++) {
            $("cmbBank" + j).update(link);
        }
    },
    
    PolulateDeviationDescription: function(res) {

        var link = "<option value=\"-1\">Select a Description</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].DeviationId + "\">" + res.value[i].Description + "</option>";
        }
        for (var j = 1; j <= 3; j++) {
            $("cmbDeviationDescription" + j).update(link);
        }

    },

    searchKeypress: function(e) {
        if (e.keyCode == 13) {
            ApproverManager.getLoanSummary();
        }
    },
    disableApprovedButton: function(cb) {

        var chk = $(cb);
        if (chk.checked == true) {
            //chk.checked = true;
            $('btnApprove').disabled = 'false';
        } else {
            $('btnApprove').disabled = '';           
        }
    },
    
    populateSegmentCombo: function(res) {
        var link = "<option value=\"-1\">Select a Segment</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SegmentID + "\">" + res.value[i].SegmentName + "</option>";
        }

        $("cmbEmployeeSegment").update(link);
    },
    populateSectorCombo: function(res) {
        var link = "<option value=\"-1\">Select a Sector</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].SECT_ID + "\">" + res.value[i].SECT_NAME + "</option>";
        }
        $("cmbEmployeeCategory").update(link);
    },
    
    populateIncomeAssementCombo: function(res) {
        var link = "<option value=\"-1\">Select an income Assement</option>";
        link += "<option value=\"-110\">Salarid</option>";
        link += "<option value=\"-111\">Salarid in Cash</option>";
        link += "<option value=\"-112\">Landlord</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].INAM_ID + "\">" + res.value[i].INAM_METHOD + "</option>";
        }
        $("cmbAssessmentMethod").update(link);
    },
    
    populateManufactureCombo: function(res) {
        var link = "<option value=\"-1\">Select a Manufacturer</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].ManufacturerId + "\">" + res.value[i].ManufacturerName + "</option>";
        }

        $("cmbBrand").update(link);
    },
    populateVehicleStatusCombo: function(res) {
        var link = "<option value=\"-1\">Select a Vehicle Status</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].StatusId + "\">" + res.value[i].StatusName + "</option>";
        }
        $("cmbVehicleStatue").update(link);
        $("cmbVendorCategory").update(link);
        
    }
};



Event.onReady(Page.init);