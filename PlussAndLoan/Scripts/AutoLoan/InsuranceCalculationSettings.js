﻿

var Page = {

    init: function() {
        util.prepareDom();

        Event.observe('lnkAddInsurance', 'click', InsurenceCalculatingSettingsHelper.addnewInsurenceCalculation);
        Event.observe('lnkSave', 'click', InsurenceCalculatingSettingsHelper.ValidateData);
        InsurenceCalculatingSettingsManager.render();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        InsurenceCalculatingSettingsManager.render();
    }

};

var InsurenceCalculatingSettingsManager = {

    render: function() {

        var res = PlussAuto.InsuranceCalculationSettings.GetOtherDamage();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            $('txtOtherChargeId').setValue(res.value.OtherChargeId);
            $('txtaccidentOrTheft').setValue(res.value.AccidentOrTheft.toFixed(2));
            $('txtCommision').setValue(res.value.Commission.toFixed(2));
            $('txtVat').setValue(res.value.Vat.toFixed(2));
            $('txtPassenger').setValue(res.value.PassengerPerSeatingCap.toFixed(2));
            $('txtAmountofDriver').setValue(res.value.AmountofDriverSeat.toFixed(2));
            $('txtStampCharge').setValue(res.value.StampCharge.toFixed(2));
            $('txtDriverSeatCharge').setValue(res.value.DriverSeatCharge.toFixed(2));


            InsurenceCalculatingSettingsManager.renderOwnDamage();
        }

    },
    renderOwnDamage: function() {

        var res = PlussAuto.InsuranceCalculationSettings.GetOwnDamage();
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        else {
            InsurenceCalculatingSettingsHelper.renderCollection(res);
        }

    },

    save: function(objOwnDamageList, objOtherCharge) {
        var res = PlussAuto.InsuranceCalculationSettings.SaveAutoInsurenceCalculationsettings(objOwnDamageList, objOtherCharge);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Insurance Calculation Settings Save successfully");
        }
    },
    InsuranceDeleteById: function(insurenceId) {
        var res = PlussAuto.InsuranceAndBANCASettings.InsuranceDeleteById(insurenceId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Insurance and Banca Delete successfully");
        }

        InsurenceCalculatingSettingsManager.render();
    }
};
var InsurenceCalculatingSettingsHelper = {
    renderCollection: function(res) {
        for (var i = 0; i < res.value.length; i++) {
            var table = document.getElementById('ownDamagedTable');
            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);
            row.id = "row" + rowCount;


            var cell1 = row.insertCell(0);
            var element1 = document.createElement("input");
            //element1.type = "text";
            //element1.name = "ccFrom";
            element1.value = res.value[i].CCFrom;
            cell1.appendChild(element1);

            var cell2 = row.insertCell(1);
            var element2 = document.createElement("input");
            //element2.type = "text";
            //element2.name = "ccTo";
            element2.value = res.value[i].CCTo;
            cell2.appendChild(element2);

            var cell3 = row.insertCell(2);
            var element3 = document.createElement("input");
            //element3.type = "text";
            //element3.name = "OwnDamage";
            element3.value = res.value[i].OwnDamageCharge.toFixed(2);
            //element3.addClassName('gridTextboxforOwnDamage');
            cell3.appendChild(element3);

            var cell4 = row.insertCell(3);
            var element4 = document.createElement("input");
            //element4.type = "text";
            //element4.name = "Actliblity";
            element4.value = res.value[i].ActLiability.toFixed(2);
            //element4.id = "row4thElement" + rowCount;
            //element4.addClassName('gridTextboxforOwnDamage');

            cell4.appendChild(element4);

            var cell5 = row.insertCell(4);

            cell5.innerHTML = "<a title=\"Delete\" href=\"javascript:;\" onclick=\"javascript:Element.remove(" + "row" + rowCount + ")\" class=\"icon iconDelete\"></a>";
        }
    },
    addnewInsurenceCalculation: function() {
        var table = document.getElementById('ownDamagedTable');
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        row.id = "row" + rowCount;


        var cell1 = row.insertCell(0);
        var element1 = document.createElement("input");
        //element1.type = "text";
        //element1.name = "ccFrom";
        cell1.appendChild(element1);

        var cell2 = row.insertCell(1);
        var element2 = document.createElement("input");
        //element2.type = "text";
        //element2.name = "ccTo";
        cell2.appendChild(element2);

        var cell3 = row.insertCell(2);
        var element3 = document.createElement("input");
        //element3.type = "text";
        //element3.name = "OwnDamage";
        //element3.addClassName('gridTextboxforOwnDamage');
        cell3.appendChild(element3);

        var cell4 = row.insertCell(3);
        var element4 = document.createElement("input");
        //element4.type = "text";
        //element4.name = "Actliblity";
        //element4.id = "row4thElement" + rowCount;
        //element4.addClassName('gridTextboxforOwnDamage');

        cell4.appendChild(element4);

        var cell5 = row.insertCell(4);

        cell5.innerHTML = "<a title=\"Delete\" href=\"javascript:;\" onclick=\"javascript:Element.remove(" + "row" + rowCount + ")\" class=\"icon iconDelete\"></a>";

    },
    ValidateData: function() {

        //Create Object for Other Charge-----------------------------------------------------------------------------
        var objAutoOthercharge = new Object();
        objAutoOthercharge.OtherChargeId = $F("txtOtherChargeId");

        objAutoOthercharge.AccidentOrTheft = $F("txtaccidentOrTheft");
        if (objAutoOthercharge.AccidentOrTheft == "") {
            objAutoOthercharge.AccidentOrTheft = 0;
        }
        else {
            if (!util.isFloat(objAutoOthercharge.AccidentOrTheft)) {
                alert("Please insert number field");
                $('txtaccidentOrTheft').setValue('');
                $("txtaccidentOrTheft").focus();
                return false;
            }
        }

        objAutoOthercharge.Commission = $F("txtCommision");
        if (objAutoOthercharge.Commission == "") {
            objAutoOthercharge.Commission = 0;
        }
        else {
            if (!util.isFloat(objAutoOthercharge.Commission)) {
                alert("Please insert number field");
                $('txtCommision').setValue('');
                $("txtCommision").focus();
                return false;
            }
        }

        objAutoOthercharge.Vat = $F("txtVat");
        if (objAutoOthercharge.Vat == "") {
            objAutoOthercharge.Vat = 0;
        }
        else {
            if (!util.isFloat(objAutoOthercharge.Vat)) {
                alert("Please insert number field");
                $('txtVat').setValue('');
                $("txtVat").focus();

                return false;
            }
        }

        objAutoOthercharge.PassengerPerSeatingCap = $F("txtPassenger");
        if (objAutoOthercharge.PassengerPerSeatingCap == "") {
            objAutoOthercharge.PassengerPerSeatingCap = 0;
        }
        else {
            if (!util.isFloat(objAutoOthercharge.PassengerPerSeatingCap)) {
                alert("Please insert number field");
                $('txtPassenger').setValue('');
                $("txtPassenger").focus();
                return false;
            }
        }

        objAutoOthercharge.AmountofDriverSeat = $F("txtAmountofDriver");
        if (objAutoOthercharge.AmountofDriverSeat == "") {
            objAutoOthercharge.AmountofDriverSeat = 0;
        }
        else {
            if (!util.isFloat(objAutoOthercharge.AmountofDriverSeat)) {
                alert("Please insert number field");
                $('txtAmountofDriver').setValue('');
                $("txtAmountofDriver").focus();
                return false;
            }
        }

        objAutoOthercharge.StampCharge = $F("txtStampCharge");
        if (objAutoOthercharge.StampCharge == "") {
            objAutoOthercharge.StampCharge = 0;
        }
        else {
            if (!util.isFloat(objAutoOthercharge.StampCharge)) {
                alert("Please insert number field");
                $('txtStampCharge').setValue('');
                $("txtStampCharge").focus();
                return false;
            }
        }

        objAutoOthercharge.DriverSeatCharge = $F("txtDriverSeatCharge");
        if (objAutoOthercharge.DriverSeatCharge == "") {
            objAutoOthercharge.DriverSeatCharge = 0;
        }
        else {
            if (!util.isFloat(objAutoOthercharge.DriverSeatCharge)) {
                alert("Please insert number field");
                $('txtDriverSeatCharge').setValue('');
                $("txtDriverSeatCharge").focus();
                return false;
            }
        }
        //End object for Other Charge -----------------------------------------------------------------------------------

        //Create object for Damage Charge -------------------------------------------------------------------------------

        var tableforDamageCharge = $('ownDamagedTable');
        var rowCount = tableforDamageCharge.rows.length;
        if (rowCount == 1) {
            alert("Please insert at least one OWN DAMAGE");
            return false;
        }
        var objAutoOwnDamageList = [];
        for (var i = 1; i <= rowCount - 1; i++) {

            var objAutoOwnDamage = new Object();

            //objAutoOwnDamage.CCFrom = tableforDamageCharge.rows[i].cells[0].innerHTML.replace('<INPUT value=', '').slice(0, -1);
            objAutoOwnDamage.CCFrom = tableforDamageCharge.rows[i].cells[0].innerHTML.replace('<INPUT value=', '').slice(0, -1);
            if (objAutoOwnDamage.CCFrom == "") {
                alert("CCFrom cannot be empty");
                return false;
            }
            objAutoOwnDamage.CCTo = tableforDamageCharge.rows[i].cells[1].innerHTML.replace('<INPUT value=', '').slice(0, -1);
            if (objAutoOwnDamage.CCTo == "") {
                alert("CCTo cannot be empty");
                return false;
            }
            objAutoOwnDamage.OwnDamageCharge = tableforDamageCharge.rows[i].cells[2].innerHTML.replace('<INPUT value=', '').slice(0, -1);
            if (objAutoOwnDamage.OwnDamageCharge == "") {
                objAutoOwnDamage.OwnDamageCharge = 0;
            }
            else {
                if (!util.isFloat(objAutoOwnDamage.OwnDamageCharge)) {
                    alert("Please insert number field");
                    return false;
                }
            }
            objAutoOwnDamage.ActLiability = tableforDamageCharge.rows[i].cells[3].innerHTML.replace('<INPUT value=', '').slice(0, -1);

            if (objAutoOwnDamage.ActLiability == "") {
                objAutoOwnDamage.ActLiability = 0;
            }
            else {
                if (!util.isFloat(objAutoOwnDamage.ActLiability)) {
                    alert("Please insert number field");
                    return false;
                }
            }

            objAutoOwnDamageList.push(objAutoOwnDamage);
        }

        //End object for Damage Charge------------------------------------------------------------------------------------
        InsurenceCalculatingSettingsManager.save(objAutoOwnDamageList, objAutoOthercharge);
    }
};

Event.onReady(Page.init);
