﻿
var FinalizationTabManager = {

    FinalizationTabOnLoad: function() {

        var res = PlussAuto.AnalyzeLoanApplication.getDeviationLevel();
        FinalizationTabHelper.PopulateDeviationLevel(res);

        res = PlussAuto.AnalyzeLoanApplication.getRemarkStrangthConditionWeaknessAll();
        FinalizationTabHelper.getStregthConditionWeakness(res);

    },

    getDeviationDescriptionByLevel: function(controlID) {
        var level = $F(controlID);
        var controlNumber = 0;
        if (controlID == 'cmdDeviationLevel1') {
            controlNumber = 1;
        }
        else if (controlID == 'cmdDeviationLevel2') {
            controlNumber = 2;
        }
        else if (controlID == 'cmdDeviationLevel3') {
            controlNumber = 3;
        }
        else if (controlID == 'cmdDeviationLevel4') {
            controlNumber = 4;
        }

        var res = PlussAuto.AnalyzeLoanApplication.getDeviationByLevel(level);

        FinalizationTabHelper.PolulateDeviationDescription(res, controlNumber);
    },

    getDeviationCodeByID: function(controlID) {
        var level = $F(controlID);
        var res = PlussAuto.AnalyzeLoanApplication.getDeviationCodeByID(level);

        if (controlID == 'cmdDeviationDescription1') {
            $('txtDeviationCode1').setValue(res.value);
        }
        else if (controlID == 'cmdDeviationDescription2') {
            $('txtDeviationCode2').setValue(res.value);
        }
        else if (controlID == 'cmdDeviationDescription3') {
            $('txtDeviationCode3').setValue(res.value);
        }
        else if (controlID == 'cmdDeviationDescription4') {
            $('txtDeviationCode4').setValue(res.value);
        }
    }

};


var FinalizationTabHelper = {

    PolulateDeviationDescription: function(res, controlNumber) {

        document.getElementById('cmdDeviationDescription' + controlNumber).options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select Description';
        ExtraOpt.value = '-1';
        document.getElementById('cmdDeviationDescription' + controlNumber).options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].Description;
            opt.value = res.value[i].DeviationId;
            document.getElementById('cmdDeviationDescription' + controlNumber).options.add(opt);
        }

    },


    PopulateDeviationLevel: function(res) {

        var link = "<option value=\"-1\">Select a Level</option>";
        for (var i = 0; i < res.value.length; i++) {
            link += "<option value=\"" + res.value[i].DeviationFor + "\">" + "Level " + res.value[i].DeviationFor + "</option>";
        }
        for (var j = 1; j <= 4; j++) {
            $("cmdDeviationLevel" + j).update(link);
        }
    },

    getStregthConditionWeakness: function(res) {

        var linkStrength = "<option value=\"-1\">Select a Strength</option>";
        var linkWeakness = "<option value=\"-1\">Select a Weakness</option>";
        var linkCondition = "<option value=\"-1\">Select a Condition</option>";
        var linkRemark = "<option value=\"-1\">Select a Remark</option>";

        for (var i = 0; i < res.value.length; i++) {

            if (res.value[i].REMA_DTYPE == '1') {
                linkRemark += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
            else if (res.value[i].REMA_DTYPE == '2') {
                linkCondition += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
            else if (res.value[i].REMA_DTYPE == '3') {
                linkStrength += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
            else if (res.value[i].REMA_DTYPE == '4') {
                linkWeakness += "<option value=\"" + res.value[i].REMA_NAME + "\">" + res.value[i].REMA_NAME + "</option>";
            }
        }

        for (var j = 1; j <= 5; j++) {
            $("cmbStrength" + j).update(linkStrength);
            $("cmbWeakness" + j).update(linkWeakness);
            $("cmbCondition" + j).update(linkCondition);
            if (j < 4) {
                $("cmbRemark" + j).update(linkRemark);
            }

        }

    }




};