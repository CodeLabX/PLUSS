﻿
var InsurenceType = { 1: 'General', 2: 'Life', 3: 'General & Life' };
var Page = {
    pageSize: 15,
    pageNo: 0,
    pager: null,
    insurence: null,
    insurenceArray: [],

    init: function() {
    util.prepareDom();
    
    Event.observe('lnkAddInsuranceBanka', 'click', InsuranceAndBANCASettingsHelper.update);
    Event.observe('lnkSave', 'click', InsuranceAndBANCASettingsManager.save);
    Event.observe('btnSearch', 'click', InsuranceAndBANCASettingsManager.render);
    Event.observe('lnkClose', 'click', util.hideModal.curry('insurencePopupDiv'));
    
//    Event.observe('lnkUploadSave', 'click', Page.upload);
    
//    Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });
        InsuranceAndBANCASettingsManager.render();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        InsuranceAndBANCASettingsManager.render();
    }

};

var InsuranceAndBANCASettingsManager = {

    render: function() {
        var comapanyName = $F('txtSearch');
        var res = PlussAuto.InsuranceAndBANCASettings.GetInsuranceAndBancaSettingsSummary(Page.pageNo, Page.pageSize, comapanyName);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        InsuranceAndBANCASettingsHelper.renderCollection(res);
    },


    save: function() {
        if (!util.validateEmpty('txtCompanyName')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('cmbInsurenceType')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateNumber('txtAgeLimit')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        //        if (!util.validateNumber('txtVehicleCC')) {
        //            util.stop(util.error, null, event);
        //            util.error = null;
        //            return;
        //        }
        if (!util.isFloat($F('txtInsurancePer'))) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.isFloat($F('txtArtaRate'))) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        var insurance = Page.insurence;
        insurance.CompanyName = $F('txtCompanyName');
        insurance.InsType = $F('cmbInsurenceType');
        insurance.InsAgeLimit = $F('txtAgeLimit');
        //insurance.InsVehicleCc = $F('txtVehicleCC');
        insurance.InsVehicleCc = '0';
        insurance.InsurancePercent = $F('txtInsurancePer');
        insurance.ArtaRate = $F('txtArtaRate');
        //insurance.Tenor = $F('txtTenor');
        insurance.Tenor = '0';

        var res = PlussAuto.InsuranceAndBANCASettings.SaveAuto_InsuranceBanca(insurance);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Insurance and Banca Save successfully");
        }

        InsuranceAndBANCASettingsManager.render();
        util.hideModal('insurencePopupDiv');
    },
    InsuranceDeleteById: function(insurenceId) {
        var res = PlussAuto.InsuranceAndBANCASettings.InsuranceDeleteById(insurenceId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Insurance and Banca Delete successfully");
        }

        InsuranceAndBANCASettingsManager.render();
    }
};
var InsuranceAndBANCASettingsHelper = {
    renderCollection: function(res) {
        Page.insurenceArray = res.value;
        var html = [];
        for (var i = 0; i < Page.insurenceArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            Page.insurenceArray[i].InsTypeName = InsurenceType[Page.insurenceArray[i].InsType];
            //            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{CompanyName}</td><td>#{InsTypeName}</td><td>#{InsAgeLimit}</td><td>#{InsVehicleCc}</td><td>#{InsurancePercent}</td><td>#{Tenor}</td><td><a title="Edit" href="javascript:;" onclick="InsuranceAndBANCASettingsHelper.update(#{InsCompanyId})" class="icon iconEdit"></a> \
            //                            <a title="Delete" href="javascript:;" onclick="InsuranceAndBANCASettingsHelper.remove(#{InsCompanyId})" class="icon iconDelete"></a></td></tr>'
            //			         .format2((i % 2 ? '' : 'odd'), true)
            //			         .format(Page.insurenceArray[i])
            //			     );
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{CompanyName}</td><td>#{InsTypeName}</td><td>#{InsAgeLimit}</td><td>#{InsurancePercent}</td><td>#{ArtaRate}</td><td><a title="Edit" href="javascript:;" onclick="InsuranceAndBANCASettingsHelper.update(#{InsCompanyId})" class="icon iconEdit"></a> \
                            <a title="Delete" href="javascript:;" onclick="InsuranceAndBANCASettingsHelper.remove(#{InsCompanyId})" class="icon iconDelete"></a></td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.insurenceArray[i])
			     );
        }
        $('insurenceList').update(html.join(''));

        var totalCount = 0;

        if (res.value.length != 0) {
            totalCount = Page.insurenceArray[0].TotalCount;
        }
        Page.pager.reset(totalCount, Page.pageNo);

        // Page.pager.reset(Page.insurenceArray[0].TotalCount, Page.pageNo);
    },
    update: function(insCompanyId) {
        $$('#insurencePopupDiv .txt').invoke('clear');
        InsuranceAndBANCASettingsHelper.clearForm();
        var insurence = Page.insurenceArray.findByProp('InsCompanyId', insCompanyId);
        Page.insurence = insurence || { insCompanyId: util.uniqId() };
        if (insurence) {
            $('txtCompanyName').setValue(insurence.CompanyName);
            $('cmbInsurenceType').setValue(insurence.InsType);
            $('txtAgeLimit').setValue(insurence.InsAgeLimit);
            //$('txtVehicleCC').setValue(insurence.InsVehicleCc);
            $('txtInsurancePer').setValue(insurence.InsurancePercent);
            $('txtArtaRate').setValue(insurence.ArtaRate);
            //$('txtTenor').setValue(insurence.Tenor);
        }
        util.showModal('insurencePopupDiv');
    },
    remove: function(insCompanyId) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            InsuranceAndBANCASettingsManager.InsuranceDeleteById(insCompanyId);
        }
    },
    clearForm: function() {
        $('txtCompanyName').setValue('');
        $('cmbInsurenceType').setValue('1');
        $('txtAgeLimit').setValue('');
        //$('txtVehicleCC').setValue('');
        $('txtInsurancePer').setValue('');
        $('txtArtaRate').setValue('');
        //$('txtTenor').setValue('');

    },
    searchKeypress: function(e) {
        if (event.keyCode == 13) {
            InsuranceAndBANCASettingsManager.render();
        }
    }
};

Event.onReady(Page.init);
