﻿
var FinalizationSaveManager = {

    save: function() {

        //create object for Auto_An_FinalizationDeviation
        var FinalizationDeviationObject = LoanCalculationSaveManager.getFinalizationDeviationObject();
        //create object for Auto_An_FinalizationDevFound
        var FinalizationDevFoundObject = LoanCalculationSaveManager.getFinalizationDevFoundObject();
        //create object for Auto_An_FinalizationSterength
        var FinalizationSterengthObject = LoanCalculationSaveManager.getFinalizationSterengthObject();
        //create object for Auto_An_FinalizationWeakness
        var FinalizationWeaknessObject = LoanCalculationSaveManager.getFinalizationWeaknessObject();
        //create object for Auto_An_FinalizationRepaymentMethod
        var FinalizationRepaymentMethodObject = LoanCalculationSaveManager.getFinalizationRepaymentMethodObject();
        //create object for Auto_An_FinalizationCondition
        var FinalizationConditionObject = LoanCalculationSaveManager.getFinalizationConditionObject();
        //create object for Auto_An_FinalizationRemark
        var FinalizationRemarkObject = LoanCalculationSaveManager.getFinalizationRemarkObject();
        //create object for Auto_An_FinalizationReqDoc
        var FinalizationReqDocObject = LoanCalculationSaveManager.getFinalizationReqDocObject();
        //create object for Auto_An_AlertVerificationReport
        var AlertVerificationReportObject = LoanCalculationSaveManager.getAlertVerificationReportObject();
        //create object for Auto_An_FinalizationRework
        var FinalizationReworkObject = LoanCalculationSaveManager.getFinalizationReworkObject();

    },

    getFinalizationReworkObject: function() {

        var FinalizationRework = new Object();

        //FinalizationRework.ID = $F('txtAnalystMasterID');
        FinalizationRework.AnalystMasterId = $F('txtAnalystMasterID');
        FinalizationRework.ReworkDone = $F('cmbReworkDone');
        FinalizationRework.ReworkReason = $F('cmbReworkReason');
        if ($F('txtLastApprovalDate') != "") {
            FinalizationRework.LastApprovalDate = $F('txtLastApprovalDate');
        }

        if ($F('txtReworkDate') != "") {
            FinalizationRework.ReworkDate = $F('txtReworkDate');
        }


        FinalizationRework.ReworkCount = $F('cmbReworkCount');
        FinalizationRework.LastApprovalAmount = $F('txtLastApprovalAmount');
        if (FinalizationRework.LastApprovalAmount == "") {
            FinalizationRework.LastApprovalAmount = "0";
        }

        return FinalizationRework;

    },

    getCibRequestLoanObject: function() {

        var cibArray = [];


        var tableProperty = $('tblCibrequestLoans');
        var rowCount = tableProperty.rows.length;

        for (var i = 1; i <= 5; i++) {
            var cibFacilityId = $F("cmbFacilityForCibRequest" + i);
            var cibAmount = $F("txtLoanAmountCibrequest" + i);
            var cibTenor = $F("txtTenorForCibRequest" + i);
            if (cibFacilityId != "-1" && cibAmount != "" && cibTenor != "") {
                var objCib = new Object();
                objCib.AnalystMasterId = $F('txtAnalystMasterID');
                objCib.CibFacilityId = cibFacilityId;
                if (!util.isFloat(cibAmount) && !util.isNegativeNumber(cibAmount)) {
                    cibAmount = 0;
                }
                if (!util.isFloat(cibTenor) && !util.isNegativeNumber(cibTenor)) {
                    cibTenor = 0;
                }
                objCib.CibAmount = cibAmount;
                objCib.CibTenor = cibTenor;
                cibArray.push(objCib);

            }
        }
        return cibArray;

    },



    getAlertVerificationReportObject: function() {

        var AlertVerificationReport = new Object();

        //AlertVerificationReport.ID = $F('txtAnalystMasterID');
        AlertVerificationReport.AnalystMasterId = $F('txtAnalystMasterID');
        AlertVerificationReport.MaxAgeAllowed = $F('txtMaxAgeAllowed');
        AlertVerificationReport.MaxAgeFound = $F('txtMaxAgeFound');
        AlertVerificationReport.MaxAgeFlag = $F('txtMaxAgeFlag');
        AlertVerificationReport.MinAgeAllowed = $F('txtMinAgeAllowed');
        AlertVerificationReport.MinAgeFound = $F('txtMinAgeFound');
        AlertVerificationReport.MinAgeFlag = $F('txtMinAgeFlag');
        AlertVerificationReport.MinIncomeAllowed = $F('txtMinincomeAllowed');
        AlertVerificationReport.MinIncomeFound = $F('txtMinIncomeFound');
        AlertVerificationReport.MinIncomeFlag = $F('txtMinIncomeFlag');
        AlertVerificationReport.Location = $F('txtLocationForLc');
        AlertVerificationReport.LocationCosidered = $F('cmbLocationForLcFlag');
        AlertVerificationReport.Nationality = $F('txtNatinalityForLc');
        AlertVerificationReport.NationalityConsidered = $F('cmbNationalityForLcFlag');
        AlertVerificationReport.CarAgeAtTenorEndAllowed = $F('txtCarAgeAtTenorEndAllowed');
        AlertVerificationReport.CarAgeAtTenorEndFound = $F('txtCarAgeAtTenorEndFound');
        AlertVerificationReport.CarAgeAtTenorEndFlag = $F('txtCarAgeAtTenorEndFlag');
        AlertVerificationReport.SeatingCapacityAllowed = $F('txtSeatingCapacityAllowed');
        AlertVerificationReport.SeatingCapacityFound = $F('txtSeatingCapacityFound');
        AlertVerificationReport.SeatingCapacityFlag = $F('txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab');
        AlertVerificationReport.CarVerification = $F('txtCarVerificationForFinTab');
        AlertVerificationReport.CarVerificationFlag = $F('txtCarVerificationFlagForFinTab');
        AlertVerificationReport.HypothecationCheck = $F('txthypothecationForFinalizeTab');
        AlertVerificationReport.HypothecationCheckFlag = $F('txthypothecationFlagForFinalizeTab');
        AlertVerificationReport.VendorStatus = $F('txtVendoreStatusForFnlTab');
        AlertVerificationReport.VendorFlag = $F('cmbVendoreStatusFlagForFnlTab');

        AlertVerificationReport.LoanAmountAllowed = $F('txtLoanAmountAllowed');
        AlertVerificationReport.LoanAmountFound = $F('txtLoanAmountFound');
        AlertVerificationReport.LoanAmountFlag = $F('txtLoanAmountFlag');
        AlertVerificationReport.BBMaxLimitAllowed = $F('txtBBMaxlimitAllowed');
        AlertVerificationReport.BBMaxLimitFound = $F('txtBBMaxlimitFound');
        AlertVerificationReport.BBMaxLimitFlag = $F('txtBBMaxlimitFlag');
        AlertVerificationReport.InterestRateAllowed = $F('txtInterestRatesAllowed');
        AlertVerificationReport.InterestRateFound = $F('txtInterestRatesFound');
        AlertVerificationReport.InterestRateFlag = $F('txtInterestRatesFlag');
        AlertVerificationReport.TenorAllowed = $F('txtTenorAllowed');
        AlertVerificationReport.TenorFound = $F('txtTenorFound');
        AlertVerificationReport.TenorFlag = $F('txtTenorFlag');
        AlertVerificationReport.ARTAEnrollmentAllowed = $F('txtArtaEnrollmentAllowed');
        AlertVerificationReport.ARTAEnrollmentFound = $F('txtArtaEnrollmentFound');
        AlertVerificationReport.ARTAEnrollmentFlag = $F('txtArtaEnrollmentFlag');
        AlertVerificationReport.LTVIncBancaAllowed = $F('txtLtvIncludingBankaAllowed');
        AlertVerificationReport.LTVIncBancaFound = $F('txtLtvIncludingBankaFound');
        AlertVerificationReport.LTVIncBancaFlag = $F('txtLtvIncludingBankaFlag');
        AlertVerificationReport.MaxLTVSpreadAllowed = $F('txtMaxLtvSpreadAllowed');
        AlertVerificationReport.MaxLTVSpreadFound = $F('txtMaxLtvSpreadFound');
        AlertVerificationReport.MaxLTVSpreadFlag = $F('txtMaxLtvSpreadFlag');
        AlertVerificationReport.DBRAllowed = $F('txtDbrForFtAllowed');
        AlertVerificationReport.DBRFound = $F('txtDbrForFtFound');
        AlertVerificationReport.DBRFlag = $F('txtDbrForFtFlag');
        AlertVerificationReport.TotalAutoLoanAllowed = $F('txtTotalAutoLoanForFtAllowed');
        AlertVerificationReport.TotalAutoLoanFound = $F('txtTotalAutoLoanForFtFound');
        AlertVerificationReport.TotalAutoLoanFlag = $F('txtTotalAutoLoanForFtFlag');
        AlertVerificationReport.ApproversDLAAllowed = $F('txtApproversDlaAllowed');
        AlertVerificationReport.ApproversDLAFound = $F('txtApproversDlaFound');
        AlertVerificationReport.ApproversDLAFlag = $F('txtApproversDlaFlag');
        AlertVerificationReport.CIBOptained = $F('txtCibObtainedForFt');
        AlertVerificationReport.CIBOptainedFlag = $F('txtCibObtainedForFtFlag');

        AlertVerificationReport.JtMaxAgeAllowed = $F('txtJointMinAgeAllowed');
        AlertVerificationReport.JtMaxAgeFound = $F('txtJointMinAgeFound');
        AlertVerificationReport.JtMaxAgeFlag = $F('txtJointminAgeFlag');
        AlertVerificationReport.JtMinAgeAllowed = $F('txtJointMaxAgeAllowed');
        AlertVerificationReport.JtMinAgeFound = $F('txtJointMaxAgeFound');
        AlertVerificationReport.JtMinAgeFlag = $F('txtJointMaxAgeFlag');
        AlertVerificationReport.JtMinIncomeAllowed = $F('txtJointMinIncomeAllowed');
        AlertVerificationReport.JtMinIncomeFound = $F('txtJointMinIncomeFound');
        AlertVerificationReport.JtMinIncomeFlag = $F('txtJointMinIncomeFlag');

        return AlertVerificationReport;
    },



    getFinalizationReqDocObject: function() {

        var FinalizationReqDoc = new Object();

        //FinalizationReqDoc.ID = $F('txtAnalystMasterID');
        FinalizationReqDoc.AnalystMasterId = $F('txtAnalystMasterID');
        FinalizationReqDoc.LetterOfIntroduction = $F('cmbLetterOfIntroduction');
        FinalizationReqDoc.Payslips = $F('cmbPayslip');
        FinalizationReqDoc.TradeLicence = $F('cmbtradelicence');
        FinalizationReqDoc.MOA_AOA = $F('cmbMoaAoa');
        FinalizationReqDoc.PartnershipDeed = $F('cmbPartnershipDeed');
        FinalizationReqDoc.FormX = $F('cmbFrom');
        FinalizationReqDoc.BoardResolution = $F('cmbBoardresolution');
        FinalizationReqDoc.DiplomaCertificate = $F('cmbDiplomacertificate');
        FinalizationReqDoc.ProfessionalCertificate = $F('cmbProfessinoalCertificate');
        FinalizationReqDoc.MunicipalTaxRecipt = $F('cmbunicipalTax');
        FinalizationReqDoc.TitleDeed = $F('cmbTitleDeed');
        FinalizationReqDoc.RentalDeed = $F('cmbentalDeed');
        FinalizationReqDoc.UtilityBill = $F('cmbtilityBills');
        FinalizationReqDoc.LoanOfferLatter = $F('cmbLoanOfferLetter');

        return FinalizationReqDoc;
    },
    getVerificationReportObject: function() {
        var verificationReportObject = new Object();
        verificationReportObject.AnalystMasterId = $F('txtAnalystMasterID');
        verificationReportObject.CPVReport = $F('cmbCpvreport');
        verificationReportObject.CarPriceVarificationReport = $F('cmbCarPriceVerificationReport');
        verificationReportObject.StatementAuthentication = $F('cmbStatementAutheticationreport');
        verificationReportObject.CreditReport = $F('cmbcreditReport');
        verificationReportObject.CarVerificationReport = $F('cmbCarVerificationreport');
        verificationReportObject.UsedCarValuationReport = $F('cmbUsedCarVerificationreport');
        verificationReportObject.UsedCarDocs = $F('cmbUsedcarDocs');
        verificationReportObject.CardRepaymentHistory = $F('cmbCardrepaymentHistory');
        return verificationReportObject;

    },
    getAppriserAndApproverObject: function() {
        var appriserAndApproverObject = new Object();
        appriserAndApproverObject.AnalystMasterId = $F('txtAnalystMasterID');
        appriserAndApproverObject.AppraisedByUserID = $F('cmbAppraisedby');
        appriserAndApproverObject.AppraisedByUserCode = $F('txtAppraiserCode');
        if (appriserAndApproverObject.AppraisedByUserCode == "") {
            appriserAndApproverObject.AppraisedByUserCode = "0";
        }
        appriserAndApproverObject.SupportedByUserID = $F('cmbSupportedby');
        appriserAndApproverObject.SupportedByUserCode = $F('txtSupportedbyCode');
        if (appriserAndApproverObject.SupportedByUserCode == "") {
            appriserAndApproverObject.SupportedByUserCode = "0";
        }
        appriserAndApproverObject.ApprovedByUserID = $F('cmbApprovedby');
        appriserAndApproverObject.ApprovedByUserCode = $F('txtApprovedbyCode');
        if (appriserAndApproverObject.ApprovedByUserCode == "") {
            appriserAndApproverObject.ApprovedByUserCode = "0";
        }
        appriserAndApproverObject.AutoDLALimit = $F('txtAutoDLALimit');
        if (appriserAndApproverObject.AutoDLALimit == "") {
            appriserAndApproverObject.AutoDLALimit = "0";
        }
        appriserAndApproverObject.TotalSecuredLimit = $F('txtTotalSecuredlimit');
        if (appriserAndApproverObject.TotalSecuredLimit == "") {
            appriserAndApproverObject.TotalSecuredLimit = "0";
        }
        return appriserAndApproverObject;

    },
    getFinalizationRemarkObject: function() {

        var FinalizationRemarkList = [];
        var tableProperty = $('tblfinalizationRemark');
        var rowCount = tableProperty.rows.length;

        for (var i = 1; i <= 5; i++) {

            var FinalizationRemark = new Object();

            if (i < 4) {
                if ($F('cmbRemark' + i) == '-1') {

                }
                else {
                    FinalizationRemark.AnalystMasterId = $F('txtAnalystMasterID');
                    FinalizationRemark.Remarks = $F('cmbRemark' + i);

                    FinalizationRemarkList.push(FinalizationRemark);
                }
            }
            else {
                if ($F('txtRemark' + i) == '') {

                }
                else {

                    FinalizationRemark.AnalystMasterId = $F('txtAnalystMasterID');
                    FinalizationRemark.Remarks = $F('txtRemark' + i);

                    FinalizationRemarkList.push(FinalizationRemark);
                }
            }
        }



        return FinalizationRemarkList;
    },


    getFinalizationConditionObject: function() {

        var FinalizationConditionList = [];
        var tableProperty = $('tblFinalizationCondition');
        var rowCount = tableProperty.rows.length;

        for (var i = 1; i <= rowCount; i++) {

            var FinalizationCondition = new Object();

            if ($F('cmbCondition' + i) == '-1') {

            }
            else {
                FinalizationCondition.AnalystMasterId = $F('txtAnalystMasterID');
                FinalizationCondition.Condition = $F('cmbCondition' + i);

                FinalizationConditionList.push(FinalizationCondition);
            }
        }
        for (var j = 1; j < 3; j++) {
            var FinalizationCondition = new Object();

            if ($F('tareaCondition' + j) == "") {

            }
            else {
                FinalizationCondition.AnalystMasterId = $F('txtAnalystMasterID');
                FinalizationCondition.Condition = $F('tareaCondition' + j);

                FinalizationConditionList.push(FinalizationCondition);
            }
        }
        return FinalizationConditionList;
    },


    getFinalizationRepaymentMethodObject: function() {

        var FinalizationRepaymentMethod = new Object();
        
        //FinalizationRepaymentMethod.ID = $F('txtAnalystMasterID');
        FinalizationRepaymentMethod.AnalystMasterId = $F('txtAnalystMasterID');
        FinalizationRepaymentMethod.Asking = $F('cmbAskingRepaymentmethodForFinalize');
        FinalizationRepaymentMethod.Instrument = $F('txtInstrumentForFinalizetab');
        FinalizationRepaymentMethod.SecurityPDC = $F('cmbSequrityPdcForFinalizetab');
        FinalizationRepaymentMethod.PDCAmount = $F('txtPdcAmountForFinalizeTab');
        FinalizationRepaymentMethod.BankID = $F('cmbBankNameForFinalizeTabofrepaymentMethod');
        FinalizationRepaymentMethod.SCBAccount = $F('cmbAccountNumberForSCBForFinalizetab');
        FinalizationRepaymentMethod.NoOfPDC = $F('txtNoOfPdcForFinalizeTab');
        FinalizationRepaymentMethod.AccountNumber = $F('txtAccountnumberForaBankFinalizeTab');

        return FinalizationRepaymentMethod;
    },

    getFinalizationWeaknessObject: function() {

        var FinalizationWeaknessList = [];
        var tableProperty = $('tblFinalizationWeakness');
        var rowCount = tableProperty.rows.length;

        for (var i = 1; i <= rowCount; i++) {

            var FinalizationWeakness = new Object();

            if ($F('cmbWeakness' + i) == '-1') {

            }
            else {
                FinalizationWeakness.AnalystMasterId = $F('txtAnalystMasterID');
                FinalizationWeakness.Weakness = $F('cmbWeakness' + i);

                FinalizationWeaknessList.push(FinalizationWeakness);
            }
        }
        for (var j = 1; j < 3; j++) {
            var FinalizationWeakness = new Object();

            if ($F('tareaWeakness' + j) == "") {

            }
            else {
                FinalizationWeakness.AnalystMasterId = $F('txtAnalystMasterID');
                FinalizationWeakness.Weakness = $F('tareaWeakness' + j);

                FinalizationWeaknessList.push(FinalizationWeakness);
            }
        }

        return FinalizationWeaknessList;
    },


    getFinalizationSterengthObject: function() {

        var FinalizationSterengthList = [];
        var tableProperty = $('tblFinalizationStrength');
        var rowCount = tableProperty.rows.length;

        for (var i = 1; i <= rowCount; i++) {

            var FinalizationSterength = new Object();

            if ($F('cmbStrength' + i) == '-1') {

            }
            else {
                FinalizationSterength.AnalystMasterId = $F('txtAnalystMasterID');
                FinalizationSterength.Strength = $F('cmbStrength' + i);

                FinalizationSterengthList.push(FinalizationSterength);

            }
        }
        for (var j = 1; j < 3; j++) {
            var FinalizationSterength = new Object();

            if ($F('tareaStrength' + j) == "") {

            }
            else {
                FinalizationSterength.AnalystMasterId = $F('txtAnalystMasterID');
                FinalizationSterength.Strength = $F('tareaStrength' + j);

                FinalizationSterengthList.push(FinalizationSterength);
            }
        }

        return FinalizationSterengthList;
    },


    getFinalizationDevFoundObject: function() {

        var FinalizationDevFound = new Object();

        FinalizationDevFound.AnalystMasterId = $F('txtAnalystMasterID');
        FinalizationDevFound.Age = $F('txtDeviationAgeForfinalizeTab');
        if (FinalizationDevFound.Age == "") {
            FinalizationDevFound.Age = "0";
        }
        FinalizationDevFound.DBR = $F('txtDeviationDbrForfinalizeTab');
        if (FinalizationDevFound.DBR == "") {
            FinalizationDevFound.DBR = "0";
        }
        FinalizationDevFound.SeatingCapacilty = $F('txtSeacitingCapacityDeviationForfinalizeTab');
        FinalizationDevFound.SharePortion = $F('txtSharePortionForFinalizeTab');

        return FinalizationDevFound;
    },


    getFinalizationDeviationObject: function() {

        var FinalizationDeviationList = [];
        var tableProperty = $('tblFinalizationDeviation');
        var rowCount = tableProperty.rows.length;

        for (var i = 1; i <= rowCount - 1; i++) {

            var FinalizationDeviation = new Object();

            if ($F('txtDeviationCode' + i) == '') {

            }
            else if ($F('cmdDeviationLevel' + i) == '-1') {
                alert("Please Select Deviation Level in Finalization Tab.");
                //$('cmdDeviationLevel' + i).focus();
                return false;
            }
            else if ($F('cmdDeviationDescription' + i) == '-1') {
                alert("Please Select Deviation Level in Finalization Tab.");
                //$('cmdDeviationDescription' + i).focus();
                return false;
            }
            else {
                FinalizationDeviation.AnalystMasterId = $F('txtAnalystMasterID');
                FinalizationDeviation.Level = $F('cmdDeviationLevel' + i);
                FinalizationDeviation.Description = $F('cmdDeviationDescription' + i);
                FinalizationDeviation.Code = $F('txtDeviationCode' + i);

                FinalizationDeviationList.push(FinalizationDeviation);
            }
        }
        return FinalizationDeviationList;
    }

};


var FinalizationSaveHelper = {


};