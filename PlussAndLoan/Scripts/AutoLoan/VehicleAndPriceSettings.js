﻿
var vehicleType = { 'Sedan': 'S1', 'Station wagon': 'S2', 'SUV': 'S3', 'Pick-Ups': 'S4', 'Microbus': 'S5', 'MPV': 'S6' };

var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    vehicles: null,
    vehiclesArray: [],
    autoPriceArray: [],
    vehicleId: 0,
    vehicleName: "",
    manufacturerArray: [],

    init: function() {
        util.prepareDom();
        Event.observe('btnClose', 'click', vehiclesHelper.closePriceSummary);
        Event.observe('lnkAddPrice', 'click', vehiclesHelper.openPricePopup);
        Event.observe('lnkAddVehicle', 'click', vehiclesHelper.update);
        Event.observe('lnkSavePrice', 'click', vehiclesManager.SavePrice);
        Event.observe('lnkClosePrice', 'click', util.hideModal.curry('priceSummaryPopupDiv'));
        Event.observe('lnkSaveVehicle', 'click', vehiclesManager.SaveVehicle);
        Event.observe('lnkCloseVehicle', 'click', util.hideModal.curry('vehiclePopupDiv'));
        Event.observe('cmbMenufacture', 'change', vehiclesHelper.changeManufectureName);
        Event.observe('btnSearch', 'click', vehiclesManager.renderVehicleSummary);
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });
        vehiclesManager.renderVehicleSummary();
        vehiclesManager.getManufacture();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        vehiclesManager.renderVehicleSummary();
    }

};

var vehiclesManager = {
    renderVehicleSummary: function() {
        var condition = $F('txtSearch');

        var res = AutoLoan.VehicleAndPriceSettings.GetVehicleSummary(Page.pageNo, Page.pageSize, condition);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        vehiclesHelper.renderVehicleCollection(res);
    },
    renderAutoPriceSummaryByVehicleId: function(vehicleId) {
        Page.vehicleId = vehicleId;
        var res = PlussAuto.VehicleAndPriceSettings.GetAutoPriceByVehicleId(vehicleId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        vehiclesHelper.renderAutoPriceCollection(res);
    },
    getStatus: function() {
        var res = PlussAuto.VehicleAndPriceSettings.GetStatusForPrice(1);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        vehiclesHelper.populateStatusCombo(res);
    },
    getManufacture: function () {
        debugger;
        var res = PlussAuto.VehicleAndPriceSettings.GetAutoManufacturer();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.manufacturerArray = res.value;

        vehiclesHelper.populateManufacturerCombo(res);
    },
    SavePrice: function() {
        if (!util.validateEmpty('cmbVehicleStatusForPriceSummary')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtManufacturingYear')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateNumber('txtManufacturingYear')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtMinimumPrice')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateNumber('txtMinimumPrice')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtMaximumPrice')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateNumber('txtMaximumPrice')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('cmbActiveStatus')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        var objPrice = new Object();
        objPrice.VehicleId = Page.vehicleId;
        objPrice.StatusId = $F('cmbVehicleStatusForPriceSummary');
        objPrice.ManufacturingYear = $F('txtManufacturingYear');
        objPrice.MinimumPrice = $F('txtMinimumPrice');
        objPrice.MaximumPrice = $F('txtMaximumPrice');
        objPrice.IsActive = $F('cmbActiveStatus');

        var res = PlussAuto.VehicleAndPriceSettings.SaveAutoPrice(objPrice);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Price Save successfully");
        }
        util.hideModal('priceSummaryPopupDiv');
        vehiclesManager.renderAutoPriceSummaryByVehicleId(Page.vehicleId);
    },
    SaveVehicle: function() {
        if (!util.validateEmpty('cmbMenufacture')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtModel')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        if (!util.validateEmpty('txtCC')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateNumber('txtCC')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        
        var vehicle = Page.vehicles;
        vehicle.ManufacturerId = $F('cmbMenufacture');
        vehicle.ManufacturCountry = $F('txtManufacturingCountry');
        vehicle.ManufacturerCode = $F('txtManufacturingCode');
        vehicle.Model = $F('txtModel');
        vehicle.ModelCode = $F('txtModelCode');
        vehicle.TrimLevel = $F('txtTrimLevel');
        vehicle.Type = $('cmbVehicleType').options[$('cmbVehicleType').selectedIndex].text; //$F('cmbVehicleType');
        vehicle.CC = $F('txtCC');
        vehicle.ChassisCode = $F('txtChassisCode');

        var res = PlussAuto.VehicleAndPriceSettings.SaveAutoVehicle(vehicle);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Vehicle Save successfully");
        }

        vehiclesManager.renderVehicleSummary();
        util.hideModal('vehiclePopupDiv');
    }
};
var vehiclesHelper = {

    renderVehicleCollection: function(res) {
        Page.vehiclesArray = res.value;
        var html = [];
        for (var i = 0; i < Page.vehiclesArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';

            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{ManufacturerName}</td><td>#{Model}</td><td>#{TrimLevel}</td><td>#{CC}</td><td>#{ChassisCode}</td><td>#{Type}</td><td><a title="Edit" href="javascript:;" onclick="vehiclesHelper.update(#{VehicleId})" class="icon iconEdit"></a> \
                                <a title="Price Details" href="javascript:;" onclick="vehiclesHelper.autoPriceDetails(#{VehicleId})" class="icon iconSearchGrid"></a></td></tr>'
			             .format2((i % 2 ? '' : 'odd'), true)
			             .format(Page.vehiclesArray[i])
			         );
        }
        $('tblvehicleSummary').update(html.join(''));
        var totalCount = 0;
        if (res.value.length != 0) {
            totalCount = Page.vehiclesArray[0].TotalCount;
        }

        Page.pager.reset(totalCount, Page.pageNo);
    },
    renderAutoPriceCollection: function(res) {
        Page.autoPriceArray = res.value;
        var html = [];
        for (var i = 0; i < Page.autoPriceArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';

            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{StatusName}</td><td>#{ManufacturingYear}</td><td>#{MinimumPrice}</td><td>#{MaximumPrice}</td></tr>'
			                 .format2((i % 2 ? '' : 'odd'), true)
			                 .format(Page.autoPriceArray[i])
			             );
        }
        $('tblPriceSummary').update(html.join(''));

    },
    update: function(vehicleId) {
        $$('#vehiclePopupDiv .txt').invoke('clear');


        vehiclesHelper.clearVehiclepopupForm();
        var vechicle = Page.vehiclesArray.findByProp('VehicleId', vehicleId);
        Page.vehicles = vechicle || { vehicleId: util.uniqId() };
        if (vechicle) {
            $('cmbMenufacture').setValue(vechicle.ManufacturerId);
            $('txtManufacturingCountry').setValue(vechicle.ManufacturCountry);
            $('txtManufacturingCode').setValue(vechicle.ManufacturerCode);
            $('txtModel').setValue(vechicle.Model);
            $('txtModelCode').setValue(vechicle.ModelCode);
            $('txtTrimLevel').setValue(vechicle.TrimLevel);
            $('txtCC').setValue(vechicle.CC);
            $('cmbVehicleType').setValue(vehicleType[vechicle.Type]);
            $('txtChassisCode').setValue(vechicle.ChassisCode);
        }
        else {
            $('cmbVehicleType').setValue('-1');
            $('cmbMenufacture').setValue('-1');
        }

        util.showModal('vehiclePopupDiv');
    },
    autoPriceDetails: function(vehicleId) {
        $('priceSummaryDiv').show();
        $('vehicleSummaryDiv').hide();
        var vehicle = Page.vehiclesArray.findByProp('VehicleId', vehicleId);

        var item = vehicle.Model;
        if (vehicle.TrimLevel != "") {
            item += "-" + vehicle.TrimLevel;
        }
        Page.vehicleName = item;
        $('spnVehicleName').update(item);
        vehiclesManager.renderAutoPriceSummaryByVehicleId(vehicleId);
    },
    uploadCallback: function(msg) {
        alert(msg);
    },
    closePriceSummary: function() {
        $('priceSummaryDiv').hide();
        $('vehicleSummaryDiv').show();
        Page.vehicleId = 0;
        Page.vehicleName = "";
        $('tblPriceSummary').update('');
    },
    openPricePopup: function() {
        $$('#priceSummaryPopupDiv .txt').invoke('clear');
        vehiclesHelper.clearPricePopupForm();
        $('txtVehiclereference').setValue(Page.vehicleName);
        vehiclesManager.getStatus();
        util.showModal('priceSummaryPopupDiv');
    },

    populateStatusCombo: function(res) {
        document.getElementById('cmbVehicleStatusForPriceSummary').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmbVehicleStatusForPriceSummary').options.add(opt);
        }

    },
    populateManufacturerCombo: function(res) {
        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].ManufacturerName;
            opt.value = res.value[i].ManufacturerId;
            document.getElementById('cmbMenufacture').options.add(opt);
        }
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '-1';
        document.getElementById('cmbMenufacture').options.add(ExtraOpt);
        $('cmbMenufacture').value = 1;
    },
    changeManufectureName: function() {
        var manufactureId = $F('cmbMenufacture');
        var manufacturer = Page.manufacturerArray.findByProp('ManufacturerId', manufactureId);
        if (manufacturer) {
            $('txtManufacturingCountry').setValue(manufacturer.ManufacturCountry);
            $('txtManufacturingCode').setValue(manufacturer.ManufacturerCode);
        }
    },
    clearVehiclepopupForm: function() {
        $('cmbMenufacture').setValue('');
        $('txtManufacturingCountry').setValue('');
        $('txtManufacturingCode').setValue('');
        $('txtModel').setValue('');
        $('txtModelCode').setValue('');
        $('txtTrimLevel').setValue('');
        $('txtCC').setValue('');
        $('cmbVehicleType').setValue('');
        $('txtChassisCode').setValue('');
    },
    clearPricePopupForm: function() {
        $('txtVehiclereference').setValue('');
        $('txtManufacturingCountry').setValue('');
        $('cmbVehicleStatusForPriceSummary').setValue('');
        $('txtManufacturingYear').setValue('');
        $('txtMinimumPrice').setValue('');
        $('txtMaximumPrice').setValue('');
        $('cmbActiveStatus').setValue('');
    },
    searchKeypress: function(event) {
        if (event.keyCode == 13) {
            vehiclesManager.renderVehicleSummary();
        }
    }

};



Event.onReady(Page.init);