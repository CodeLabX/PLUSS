﻿var bankStatementHelper = {
    showBankStatement: function(bankNo, accountType, tabName) {

        var statement = "";
        var spreadsheet = document.getElementById("BankStatementAVG");
        var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
        spreadsheet.XMLData = blankData;
        if (accountType == "Pr" && tabName == "BM") {
            var bankName = $('cmbBankNameForBank' + bankNo + 'IncomeGenerator').options[$('cmbBankNameForBank' + bankNo + 'IncomeGenerator').selectedIndex].text;
            $('spnBankStatementName').innerHTML = bankName;
            statement = $F("txtbmprStatementBank" + bankNo);
            if (statement != "") {
                spreadsheet.XMLData = statement;
            }
        }
        if (accountType == "Jt" && tabName == "BM") {
            var bankName = $('cmbBankName1ForJointBank' + bankNo).options[$('cmbBankName1ForJointBank' + bankNo).selectedIndex].text;
            $('spnBankStatementName').innerHTML = bankName;
            statement = $F("txtbmjtStatementBank" + bankNo);
            if (statement != "") {
                spreadsheet.XMLData = statement;
            }
        }
        if (accountType == "Pr" && tabName == "SE") {
            var bankName = $('cmbBankNameForSEBank' + bankNo).options[$('cmbBankNameForSEBank' + bankNo).selectedIndex].text;
            $('spnBankStatementName').innerHTML = bankName;
            statement = $F("txtseprStatementBank" + bankNo);
            if (statement != "") {
                spreadsheet.XMLData = statement;
            }
        }
        if (accountType == "Jt" && tabName == "SE") {
            var bankName = $('cmbBankNameForSEJtBank' + bankNo).options[$('cmbBankNameForSEJtBank' + bankNo).selectedIndex].text;
            $('spnBankStatementName').innerHTML = bankName;
            statement = $F("txtsejtStatementBank" + bankNo);
            if (statement != "") {
                spreadsheet.XMLData = statement;
            }
        }

        util.showModal('bankStatementPopupDiv');

    },
    closePopup: function() {

        util.hideModal.curry('bankStatementPopupDiv');
        var spreadsheet = document.getElementById("BankStatementAVG");
        var blankData = "<?xml version=\"1.0\"?><ss:Workbook xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:c=\"urn:schemas-microsoft-com:office:component:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\"> <x:ExcelWorkbook>  <x:ProtectStructure>False</x:ProtectStructure>  <x:ActiveSheet>0</x:ActiveSheet> </x:ExcelWorkbook> <ss:Styles>  <ss:Style ss:ID=\"Default\">   <ss:Alignment ss:Horizontal=\"Automatic\" ss:Rotate=\"0.0\" ss:Vertical=\"Bottom\"    ss:ReadingOrder=\"Context\"/>   <ss:Borders>   </ss:Borders>   <ss:Font ss:FontName=\"Arial\" ss:Size=\"10\" ss:Color=\"Automatic\" ss:Bold=\"0\"    ss:Italic=\"0\" ss:Underline=\"None\"/>   <ss:Interior ss:Color=\"Automatic\" ss:Pattern=\"None\"/>   <ss:NumberFormat ss:Format=\"General\"/>   <ss:Protection ss:Protected=\"1\"/>  </ss:Style> </ss:Styles> <c:ComponentOptions>  <c:Label>   <c:Caption>Microsoft Office Spreadsheet</c:Caption>  </c:Label>  <c:MaxHeight>80%</c:MaxHeight>  <c:MaxWidth>80%</c:MaxWidth>  <c:NextSheetNumber>1</c:NextSheetNumber> </c:ComponentOptions> <x:WorkbookOptions>  <c:OWCVersion>12.0.0.6502         </c:OWCVersion> </x:WorkbookOptions> <ss:Worksheet ss:Name=\"Sheet0\">  <x:WorksheetOptions>   <x:Selected/>   <x:ViewableRange>R1:R262144</x:ViewableRange>   <x:Selection>R1C1</x:Selection>   <x:TopRowVisible>0</x:TopRowVisible>   <x:LeftColumnVisible>0</x:LeftColumnVisible>   <x:ProtectContents>False</x:ProtectContents>  </x:WorksheetOptions>  <c:WorksheetOptions>  </c:WorksheetOptions> </ss:Worksheet></ss:Workbook>";
        spreadsheet.XMLData = blankData;

    }
    

};

var chamberHelper = {
    changePatients: function(identity, accountType, tabName, bankNo) {
        if (accountType == "Pr" && tabName == "Se") {
            if (util.isFloat($F(identity))) {
                var tablePrAccount = $("tblChamberIncomeForSEPrBank" + bankNo);
                var rowCount = tablePrAccount.rows.length;


                var patientsOld = 0.0, feeOld = 0.0, totalOfPatientsOld = 0.0;
                var patientsNew = 0.0, feeNew = 0.0, totalOfPatientsNew = 0.0;
                var total = 0.0, dayMonth = 0, totalIncome = 0.0, totalChamberIncome = 0.0;


                for (var i = 1; i <= rowCount - 1; i++) {
                    if (util.isFloat($F('txtPatientsOld' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtPatientsOld' + i + 'ForSEPrBank' + bankNo))) {
                        patientsOld = $F('txtPatientsOld' + i + 'ForSEPrBank' + bankNo);
                    }
                    if (util.isFloat($F('txtFeeOld' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtFeeOld' + i + 'ForSEPrBank' + bankNo))) {
                        feeOld = $F('txtFeeOld' + i + 'ForSEPrBank' + bankNo);
                    }
                    if (util.isFloat($F('txtPatientsNew' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtPatientsNew' + i + 'ForSEPrBank' + bankNo))) {
                        patientsNew = $F('txtPatientsNew' + i + 'ForSEPrBank' + bankNo);
                    }
                    if (util.isFloat($F('txtFeeNew' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtFeeNew' + i + 'ForSEPrBank' + bankNo))) {
                        feeNew = $F('txtFeeNew' + i + 'ForSEPrBank' + bankNo);
                    }
                    if (util.isFloat($F('txtDayMonth' + i + 'ForSEPrBank' + bankNo)) && !util.isEmpty($F('txtDayMonth' + i + 'ForSEPrBank' + bankNo))) {
                        dayMonth = $F('txtDayMonth' + i + 'ForSEPrBank' + bankNo);
                    }



                    totalOfPatientsOld = (patientsOld * feeOld);
                    totalOfPatientsNew = (patientsNew * feeNew);
                    total = totalOfPatientsOld + totalOfPatientsNew;
                    totalIncome = total * dayMonth;
                    totalChamberIncome += totalIncome;


                    $('txtTotalOld' + i + 'ForSEPrBank' + bankNo).setValue(totalOfPatientsOld);
                    $('txtTotalNew' + i + 'ForSEPrBank' + bankNo).setValue(totalOfPatientsNew);
                    $('txtTotalOldNew' + i + 'ForSEPr1Bank' + bankNo).setValue(total);
                    $('txtIncome' + i + 'ForSEPrBank' + bankNo).setValue(totalIncome);
                    patientsOld = 0;
                    feeOld = 0;
                    patientsNew = 0;
                    feeNew = 0;
                    dayMonth = 0;
                    totalIncome = 0;
                    totalOfPatientsOld = 0;
                    totalOfPatientsNew = 0;
                    total = 0;





                }
                $('txtTotalChamberIncomeForSEPrBank' + bankNo).setValue(totalChamberIncome);

                var identityPrOther = "txtTotalOtherIncomeForSEPrBank" + bankNo;
                //debugger;
                chamberHelper.calculateFinaltTotalIncome(identityPrOther, accountType, tabName, bankNo);

            }
            else {


                alert("Please type a Valid number Only");
                $(identity).setValue("0");
                //$(identity).focus();
                return false;
            }
        } else if (accountType == "Jt" && tabName == "Se") {
            if (util.isFloat($F(identity))) {
                var tablePrAccount = $("tblChamberIncomeForSEJtBank" + bankNo);
                var rowCount = tablePrAccount.rows.length;


                var patientsOld = 0.0, feeOld = 0.0, totalOfPatientsOld = 0.0;
                var patientsNew = 0.0, feeNew = 0.0, totalOfPatientsNew = 0.0;
                var total = 0.0, dayMonth = 0, totalIncome = 0.0, totalChamberIncome = 0.0;


                for (var i = 1; i <= rowCount - 1; i++) {
                    if (util.isFloat($F('txtPatientsOld' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtPatientsOld' + i + 'ForSEJtBank' + bankNo))) {
                        patientsOld = $F('txtPatientsOld' + i + 'ForSEJtBank' + bankNo);
                    }
                    if (util.isFloat($F('txtFeeOld' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtFeeOld' + i + 'ForSEJtBank' + bankNo))) {
                        feeOld = $F('txtFeeOld' + i + 'ForSEJtBank' + bankNo);
                    }
                    if (util.isFloat($F('txtPatientsNew' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtPatientsNew' + i + 'ForSEJtBank' + bankNo))) {
                        patientsNew = $F('txtPatientsNew' + i + 'ForSEJtBank' + bankNo);
                    }
                    if (util.isFloat($F('txtFeeNew' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtFeeNew' + i + 'ForSEJtBank' + bankNo))) {
                        feeNew = $F('txtFeeNew' + i + 'ForSEJtBank' + bankNo);
                    }
                    if (util.isFloat($F('txtDayMonth' + i + 'ForSEJtBank' + bankNo)) && !util.isEmpty($F('txtDayMonth' + i + 'ForSEJtBank' + bankNo))) {
                        dayMonth = $F('txtDayMonth' + i + 'ForSEJtBank' + bankNo);
                    }



                    totalOfPatientsOld = (patientsOld * feeOld);
                    totalOfPatientsNew = (patientsNew * feeNew);
                    total = totalOfPatientsOld + totalOfPatientsNew;
                    totalIncome = total * dayMonth;
                    totalChamberIncome += totalIncome;


                    $('txtTotalOld' + i + 'ForSEJtBank' + bankNo).setValue(totalOfPatientsOld);
                    $('txtTotalNew' + i + 'ForSEJtBank' + bankNo).setValue(totalOfPatientsNew);
                    $('txtTotalOldNew' + i + 'ForSEJt1Bank' + bankNo).setValue(total);
                    $('txtIncome' + i + 'ForSEJtBank' + bankNo).setValue(totalIncome);
                    patientsOld = 0;
                    feeOld = 0;
                    patientsNew = 0;
                    feeNew = 0;
                    dayMonth = 0;
                    totalIncome = 0;
                    totalOfPatientsOld = 0;
                    totalOfPatientsNew = 0;
                    total = 0;





                }
                $('txtTotalChamberIncomeForSEJtBank' + bankNo).setValue(totalChamberIncome);
                var identityJtOther = "txtTotalOtherIncomeForSEJtBank" + bankNo;
                chamberHelper.calculateFinaltTotalIncome(identityJtOther, accountType, tabName, bankNo);

            }
            else {


                alert("Please type a Valid number Only");
                $(identity).setValue("0");
                //$(identity).focus();
                return false;
            }

        }
    },

    calculateFinaltTotalIncome: function(identity, accountType, tabName, bankNo) {
        if (identity == "txtTotalOtherIncomeForSEPrBank" + bankNo && accountType == "Pr" && tabName == "Se") {
            // debugger;
            var totalChamberInc = 0, otherIncome = 0;
            if ((util.isFloat($F(identity)) && !util.isEmpty($F(identity)))) {
                if ($F('txtTotalChamberIncomeForSEPrBank' + bankNo) != "") {
                    totalChamberInc = parseFloat($F('txtTotalChamberIncomeForSEPrBank' + bankNo));
                }
                if ($F(identity) != "") {
                    otherIncome = parseFloat($F(identity));
                }
                $('txtTotalIncomeForSEPrBank' + bankNo).setValue(totalChamberInc + otherIncome);


            } else {
                alert("Please type a Valid number Only");
                $(identity).setValue("0");
                //$(identity).focus();
                return false;
            }
        }
        else if (identity == "txtTotalOtherIncomeForSEJtBank" + bankNo && accountType == "Jt" && tabName == "Se") {
            var totalChamberIncJt = 0, otherIncomeJt = 0;
            if ((util.isFloat($F(identity)) && !util.isEmpty($F(identity)))) {
                if ($F('txtTotalChamberIncomeForSEJtBank' + bankNo) != "") {
                    totalChamberIncJt = parseFloat($F('txtTotalChamberIncomeForSEJtBank' + bankNo));
                }
                if ($F(identity) != "") {
                    otherIncomeJt = parseFloat($F(identity));
                }
                $('txtTotalIncomeForSEJtBank' + bankNo).setValue(totalChamberIncJt + otherIncomeJt);


            } else {
                alert("Please type a Valid number Only");
                $(identity).setValue("0");
                //$(identity).focus();
                return false;
            }
        }
        loanCalculatorHelPer.setBussinessManCalculationForDoctorAndTeachers(accountType, "DC");
    }

};

var teacherHelper = {
    changeTution: function(identity, accountType, tabName, bankNo) {
        var noOfStudent = 0;
        var noOfDaysofTution = 0;
        var fees = 0;
        var total = 0;
        if (util.isFloat($F(identity))) {
            if (accountType == "Pr" && tabName == "Se") {
                if (util.isFloat($F('txtNoOfStudentForSEPrBank' + bankNo)) && !util.isEmpty($F('txtNoOfStudentForSEPrBank' + bankNo))) {
                    noOfStudent = $F('txtNoOfStudentForSEPrBank' + bankNo);
                }
                if (util.isFloat($F('txtNoOfDaysForTutionForSEPrBank' + bankNo)) && !util.isEmpty($F('txtNoOfDaysForTutionForSEPrBank' + bankNo))) {
                    noOfDaysofTution = $F('txtNoOfDaysForTutionForSEPrBank' + bankNo);
                }
                if (util.isFloat($F('txtTutionFeeForSEPrBank' + bankNo)) && !util.isEmpty($F('txtTutionFeeForSEPrBank' + bankNo))) {
                    fees = $F('txtTutionFeeForSEPrBank' + bankNo);
                }
                total = noOfStudent * noOfDaysofTution * fees;
                $('txtTotalInComeTutionForSEPrBank' + bankNo).setValue(total);
            }
            else {
                if (util.isFloat($F('txtNoOfStudentForSEJtBank' + bankNo)) && !util.isEmpty($F('txtNoOfStudentForSEJtBank' + bankNo))) {
                    noOfStudent = $F('txtNoOfStudentForSEJtBank' + bankNo);
                }
                if (util.isFloat($F('txtNoOfDaysForTutionForSEJtBank' + bankNo)) && !util.isEmpty($F('txtNoOfDaysForTutionForSEJtBank' + bankNo))) {
                    noOfDaysofTution = $F('txtNoOfDaysForTutionForSEJtBank' + bankNo);
                }
                if (util.isFloat($F('txtTutionFeeForSEJtBank' + bankNo)) && !util.isEmpty($F('txtTutionFeeForSEJtBank' + bankNo))) {
                    fees = $F('txtTutionFeeForSEJtBank' + bankNo);
                }
                total = noOfStudent * noOfDaysofTution * fees;
                $('txtTotalInComeTutionForSEJtBank' + bankNo).setValue(total);
            }
            loanCalculatorHelPer.setBussinessManCalculationForDoctorAndTeachers(accountType, "TC");


        }
        else {
            alert("Please type a Valid number Only");
            $(identity).setValue("0");
            //$(identity).focus();
            return false;
        }
    }

};

var salaredHelper = {
    changeSalarymode: function(accountType) {
        if (accountType == "Pr") {
            var salaryMode = $F("cmbSalaryModeForPr");
            if (salaryMode == "2") {
                $('cmbInComeAssementForSalaryForPr').setValue('2');
                $('cmbInComeAssementForPr').setValue('SL');
                $('OwnHouseBenifitDivForPrSalaried').style.display = 'block';
            }
            else {
                $('cmbInComeAssementForSalaryForPr').setValue('1');
                $('cmbInComeAssementForPr').setValue('SC');
                $('OwnHouseBenifitDivForPrSalaried').style.display = 'none';
            }
        }
        else {
            var salaryMode = $F("cmbSalaryModeForJt");
            if (salaryMode == "2") {
                $('cmbInComeAssementForSalaryForJt').setValue('2');
                $('cmbInComeAssementForJt').setValue('SL');
                $('OwnHouseBenifitDivForJtSalaried').style.display = 'block';
            }
            else {
                $('cmbInComeAssementForSalaryForJt').setValue('1');
                $('cmbInComeAssementForJt').setValue('SC');
                $('OwnHouseBenifitDivForJtSalaried').style.display = 'none';
            }
        }
        salaredHelper.calculateTotalIncome(accountType);
    },
    changeEmployer: function(accountType) {
        if (accountType == "Pr") {
            var employer = $F("cmbEmployerForSalariedForPr");
            if (employer == "-1") {
                $('txtEmployerCodeForPr').setValue('');
                $('cmbEmployerCategoryForPrSalaried').setValue('-1');
                return false;
            }
            var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', employer);
            if (emp) {
                $('txtEmployerCodeForPr').setValue(emp.CompanyCode);
                $('cmbEmployerCategoryForPrSalaried').setValue(emp.CategoryName);
            }
        }
        else {
            var employer = $F("cmbEmployerForSalariedForJt");
            if (employer == "-1") {
                $('txtEmployerCodeForJt').setValue('');
                $('cmbEmployerCategoryForJtSalaried').setValue('-1');
                return false;
            }
            var emp = Page.employerForSalariedArray.findByProp('AutoCompanyId', employer);
            if (emp) {
                $('txtEmployerCodeForJt').setValue(emp.CompanyCode);
                $('cmbEmployerCategoryForJtSalaried').setValue(emp.CategoryName);
            }
        }
    },
    changeVeriavleSalary: function(accountType) {
        if (accountType == "Pr") {
            var variableSalary = $F("cmbVeriableSalaryForPr");
            if (variableSalary == "1") {
                $('divVeriableSalaryForPr').style.display = 'block';
                $('divVariableSalaryCalculationForPr').style.display = 'block';
            }
            else {
                $('divVeriableSalaryForPr').style.display = 'none';
                $('divVariableSalaryCalculationForPr').style.display = 'none';
            }
        }
        else {
            var variableSalary = $F("cmbVeriableSalaryForJt");
            if (variableSalary == "1") {
                $('divVeriableSalaryForJt').style.display = 'block';
                $('divVariableSalaryCalculationForJt').style.display = 'block';
            }
            else {
                $('divVeriableSalaryForJt').style.display = 'none';
                $('divVariableSalaryCalculationForJt').style.display = 'none';
            }
        }
        salaredHelper.calculateTotalIncome(accountType);
    },
    changeCarAllowence: function(accountType) {

        if (accountType == "Pr") {
            var carallowence = $F("cmbCarAllowence");
            if (carallowence == "1") {
                $('divCarAllowenceForPrSalaried').style.display = 'block';
            }
            else {
                $('divCarAllowenceForPrSalaried').style.display = 'none';
            }
        }
        else {
            var carallowence = $F("cmbCarAllowenceJt");
            if (carallowence == "1") {
                $('divCarAllowenceForJtSalaried').style.display = 'block';
            }
            else {
                $('divCarAllowenceForJtSalaried').style.display = 'none';
            }

        }
        salaredHelper.calculateTotalIncome(accountType);
    },
    changeCarAllowance: function(identity, accountType) {
        var totalCarAllowanceAmount = 0;
        var amount = 0;
        var per = 0;
        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            if (accountType == "Pr") {
                if (util.isFloat($F("txtCarAllowenceAmount")) && !util.isEmpty($F("txtCarAllowenceAmount"))) {
                    amount = parseFloat($F("txtCarAllowenceAmount"));
                }
                if (util.isFloat($F("txtCarAllowenceForPr")) && !util.isEmpty($F("txtCarAllowenceForPr"))) {
                    per = parseFloat($F("txtCarAllowenceForPr"));
                }
                totalCarAllowanceAmount = parseFloat((amount * per) / 100);
                $("txtCarAllowenceTotalAmountPr").setValue(totalCarAllowanceAmount.toFixed(2));

            }
            else {
                if (util.isFloat($F("txtCarAllowenceAmountJt")) && !util.isEmpty($F("txtCarAllowenceAmountJt"))) {
                    amount = parseFloat($F("txtCarAllowenceAmountJt"));
                }
                if (util.isFloat($F("txtCarAllowenceForJt")) && !util.isEmpty($F("txtCarAllowenceForJt"))) {
                    per = parseFloat($F("txtCarAllowenceForJt"));
                }
                totalCarAllowanceAmount = parseFloat((amount * per) / 100);
                $("txtCarAllowenceTotalAmountJt").setValue(totalCarAllowanceAmount.toFixed(2));
            }
            salaredHelper.calculateTotalIncome(accountType);
        }
        else {
            alert("Please insert number Field");
            $(identity).setValue(0);
            if (accountType == "Pr") {
                $("txtAverageBalanceForPrSalaried").setValue("0");
            }
            else {
                //
            }
            salaredHelper.calculateTotalIncome(accountType);
        }
    },
    changeOwnHouseAmount: function(accountType) {
        salaredHelper.calculateTotalIncome(accountType);
    },
    changeNetSalaray: function(identity, accountType) {
        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            if (accountType == "Pr") {
                if (parseFloat($F(identity)) > parseFloat($F("txtGrossSalaryForPrSalaried"))) {
                    alert("Net Amount cannot be grater then gross amount");
                    $(identity).setValue(0);
                    return false;
                }
            }
            else {
                if (parseFloat($F(identity)) > parseFloat($F("txtGrossSalaryForJtSalaried"))) {
                    alert("Net Amount cannot be grater then gross amount");
                    $(identity).setValue(0);
                    return false;
                }
            }


            salaredHelper.calculateTotalIncome(accountType);
        }
        else {
            alert("Please insert number Field");
            $(identity).setValue(0);
            salaredHelper.calculateTotalIncome(accountType);
        }
    },
    changeGrossSalaray: function(identity, accountType) {
        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            //
        }
        else {
            alert("Please insert number Field On Gross Salary");
            $(identity).setValue(0);
        }
    },
    changeVaiableSalaryPer: function(identity, accountType) {
        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            salaredHelper.calculateTotalIncome(accountType);
        }
        else {
            alert("Please insert number Field");
            $(identity).setValue(0);
            salaredHelper.calculateTotalIncome(accountType);
        }
    },
    changeVariableAmount: function(identity, accountType) {
        var totalVariableAmount = 0;
        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            if (accountType == "Pr") {
                if (util.isFloat($F("txtVaiableAmountForMonth1ForPrSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth1ForPrSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth1ForPrSalary"));
                }
                if (util.isFloat($F("txtVaiableAmountForMonth2ForPrSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth2ForPrSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth2ForPrSalary"));
                }
                if (util.isFloat($F("txtVaiableAmountForMonth3ForPrSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth3ForPrSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth3ForPrSalary"));
                }
                totalVariableAmount = parseFloat(totalVariableAmount / 3);
                $("txtAverageBalanceForPrSalaried").setValue(totalVariableAmount.toFixed(2));
                salaredHelper.calculateTotalIncome(accountType);

            }
            else {
                if (util.isFloat($F("txtVaiableAmountForMonth1ForJtSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth1ForJtSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth1ForJtSalary"));
                }
                if (util.isFloat($F("txtVaiableAmountForMonth2ForJtSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth2ForJtSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth2ForJtSalary"));
                }
                if (util.isFloat($F("txtVaiableAmountForMonth3ForJtSalary")) && !util.isEmpty($F("txtVaiableAmountForMonth3ForJtSalary"))) {
                    totalVariableAmount += parseFloat($F("txtVaiableAmountForMonth3ForJtSalary"));
                }
                totalVariableAmount = parseFloat(totalVariableAmount / 3);
                $("txtAverageBalanceForJtSalaried").setValue(totalVariableAmount.toFixed(2));
                salaredHelper.calculateTotalIncome(accountType);
            }



        }
        else {
            alert("Please insert number Field");
            $(identity).setValue(0);
            if (accountType == "Pr") {
                $("txtAverageBalanceForPrSalaried").setValue("0");
            }
            else {
                $("txtAverageBalanceForJtSalaried").setValue("0");
            }
            salaredHelper.calculateTotalIncome(accountType);
        }
    },
    calculateTotalIncome: function(accountType) {
        var totalIncome = 0;
        var grossSalary = 0;
        var netSalary = 0;
        var carAllowence = 0;
        var salaryCollection = 0;
        var variableSalTotal = 0;
        var totalCarAllowence = 0;
        if (accountType == "Pr") {

            var salarMode = $F("cmbSalaryModeForPr");

            if (util.isFloat($F('txtNetSalaryForPr')) && !util.isEmpty($F('txtNetSalaryForPr'))) {
                netSalary = $F('txtNetSalaryForPr'); //DA16
            }

            var variableSal = $F("cmbVeriableSalaryForPr");
            if (variableSal == "1") {
                var avgSalaryCollection = 0;
                if (util.isFloat($F('txtAverageBalanceForPrSalaried')) && !util.isEmpty($F('txtAverageBalanceForPrSalaried'))) {
                    avgSalaryCollection = parseFloat($F('txtAverageBalanceForPrSalaried'));
                }
                var vairableSalPer = 0;
                if (util.isFloat($F('txtVeriableSalaryPerForPrSalaried')) && !util.isEmpty($F('txtVeriableSalaryPerForPrSalaried'))) {
                    vairableSalPer = parseFloat($F('txtVeriableSalaryPerForPrSalaried'));
                }
                var totalAmountWithPersentege = parseFloat((avgSalaryCollection * vairableSalPer) / 100);
                variableSalTotal = parseFloat(netSalary) + totalAmountWithPersentege;
                $('txtTotalVeriableSalaryForPrSalaried').setValue(totalAmountWithPersentege.toFixed(2));

            }
            else {
                variableSalTotal = netSalary;
            }

            var ownHouseBenifit = $F("changeOwnHouseAmount");
            if (ownHouseBenifit == "1" && salarMode == "2") {
                salaryCollection = variableSalTotal * 1.2;
            }
            else {
                salaryCollection = variableSalTotal;
            }
            var carAllowence = $F("cmbCarAllowence");
            if (carAllowence == "1") {
                var carAllowencePer = 0;
                if (util.isFloat($F('txtCarAllowenceForPr')) && !util.isEmpty($F('txtCarAllowenceForPr'))) {
                    carAllowencePer = parseFloat($F('txtCarAllowenceForPr'));
                }
                var carAllowenceAmount = 0;
                if (util.isFloat($F('txtCarAllowenceAmount')) && !util.isEmpty($F('txtCarAllowenceAmount'))) {
                    carAllowenceAmount = parseFloat($F('txtCarAllowenceAmount'));
                }
                totalCarAllowence = parseFloat((carAllowenceAmount * carAllowencePer) / 100);
                $('txtCarAllowenceTotalAmountPr').setValue(totalCarAllowence.toFixed(2));

            }

            totalIncome = parseFloat(salaryCollection) + parseFloat(totalCarAllowence);
            $('txtTotalInComeForPrSalaried').setValue(totalIncome);

        }
        else {


            if (util.isFloat($F('txtGrossSalaryForJtSalaried')) && !util.isEmpty($F('txtGrossSalaryForJtSalaried'))) {
                grossSalary = $F('txtGrossSalaryForJtSalaried');
            }
            if (util.isFloat($F('txtNetSalaryForJt')) && !util.isEmpty($F('txtNetSalaryForJt'))) {
                netSalary = $F('txtNetSalaryForJt'); //DA16
            }

            var variableSal = $F("cmbVeriableSalaryForJt");
            if (variableSal == "1") {
                var avgSalaryCollection = 0;
                if (util.isFloat($F('txtAverageBalanceForJtSalaried')) && !util.isEmpty($F('txtAverageBalanceForJtSalaried'))) {
                    avgSalaryCollection = parseFloat($F('txtAverageBalanceForJtSalaried'));
                }
                var vairableSalPer = 0;
                if (util.isFloat($F('txtVeriableSalaryPerForJtSalaried')) && !util.isEmpty($F('txtVeriableSalaryPerForJtSalaried'))) {
                    vairableSalPer = parseFloat($F('txtVeriableSalaryPerForJtSalaried'));
                }
                var totalAmountWithPersentege = parseFloat((avgSalaryCollection * vairableSalPer) / 100);
                variableSalTotal = parseFloat(netSalary) + totalAmountWithPersentege;
                $('txtTotalVeriableSalaryForJtSalaried').setValue(totalAmountWithPersentege.toFixed(2));

            }
            else {
                variableSalTotal = netSalary;
            }

            var ownHouseBenifit = $F("changeOwnHouseAmountJt");
            if (ownHouseBenifit == "1") {
                salaryCollection = variableSalTotal * 1.2;
            }
            else {
                salaryCollection = variableSalTotal;
            }
            var carAllowence = $F("cmbCarAllowenceJt");
            if (carAllowence == "1") {
                var carAllowencePer = 0;
                if (util.isFloat($F('txtCarAllowenceForJt')) && !util.isEmpty($F('txtCarAllowenceForJt'))) {
                    carAllowencePer = parseFloat($F('txtCarAllowenceForJt'));
                }
                var carAllowenceAmount = 0;
                if (util.isFloat($F('txtCarAllowenceAmount')) && !util.isEmpty($F('txtCarAllowenceAmount'))) {
                    carAllowenceAmount = parseFloat($F('txtCarAllowenceAmountJt'));
                }
                totalCarAllowence = parseFloat((carAllowenceAmount * carAllowencePer) / 100);
                $('txtCarAllowenceTotalAmountJt').setValue(totalCarAllowence.toFixed(2));

            }

            totalIncome = parseFloat(salaryCollection) + parseFloat(totalCarAllowence);
            $('txtTotalInComeForJtSalaried').setValue(totalIncome);
        }
        loanCalculatorHelPer.setSalaryCalculation(accountType);
    }
};

var landLordHelper = {
    calculaterentalIncome: function(identity, accounttype) {
        var verifiedrent1Total = 0;
        var verifiedrent2Total = 0;
        var verifiedrent3Total = 0;

        var rental1 = 0;
        var rental2 = 0;
        var rental3 = 0;

        var share1 = 0;
        var share2 = 0;
        var share3 = 0;
        var totalrentalIncome = 0;
        var per = 0;
        var reflectionrequired = 0;

        if (util.isFloat($F(identity)) && !util.isEmpty($F(identity))) {
            if (accounttype == "Pr") {
                if (util.isFloat($F('txtVerifiedrent1ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent1ForLandLordPr'))) {
                    rental1 = parseFloat($F('txtVerifiedrent1ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare1ForLandLordPr')) && !util.isEmpty($F('txtShare1ForLandLordPr'))) {
                    share1 = parseFloat($F('txtShare1ForLandLordPr'));
                }

                verifiedrent1Total = parseFloat((parseFloat(rental1) * parseFloat(share1)) / 100);

                if (util.isFloat($F('txtVerifiedrent2ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent2ForLandLordPr'))) {
                    rental2 = parseFloat($F('txtVerifiedrent2ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare2ForLandLordPr')) && !util.isEmpty($F('txtShare2ForLandLordPr'))) {
                    share2 = parseFloat($F('txtShare2ForLandLordPr'));
                }
                verifiedrent2Total = parseFloat((parseFloat(rental2) * parseFloat(share2)) / 100);
                if (util.isFloat($F('txtVerifiedrent3ForLandLordPr')) && !util.isEmpty($F('txtVerifiedrent3ForLandLordPr'))) {
                    rental3 = parseFloat($F('txtVerifiedrent3ForLandLordPr'));
                }
                if (util.isFloat($F('txtShare3ForLandLordPr')) && !util.isEmpty($F('txtShare3ForLandLordPr'))) {
                    share3 = parseFloat($F('txtShare3ForLandLordPr'));
                }
                verifiedrent3Total = parseFloat((parseFloat(rental3) * parseFloat(share3)) / 100);
                totalrentalIncome = parseFloat(verifiedrent1Total) + parseFloat(verifiedrent2Total) + parseFloat(verifiedrent3Total); //DE39

                if (totalrentalIncome >= 32500 && totalrentalIncome < 49999) {
                    per = 40;
                }
                if (totalrentalIncome >= 50000 && totalrentalIncome < 75000) {
                    per = 50;
                }
                if (totalrentalIncome >= 75000 && totalrentalIncome < 150000) {
                    per = 55;
                }
                if (totalrentalIncome >= 150000) {
                    per = 60;
                }


                reflectionrequired = parseFloat((totalrentalIncome * per) / 100);
                $('txtRentalIncome1ForLandLordPr').setValue(verifiedrent1Total.toFixed(2));
                $('txtRentalIncome2ForLandLordPr').setValue(verifiedrent2Total.toFixed(2));
                $('txtRentalIncome3ForLandLordPr').setValue(verifiedrent3Total.toFixed(2));
                $('txttotalrentalIncomeForLandLordPr').setValue(totalrentalIncome.toFixed(2));
                $('txtReflactionPerForLandlordPr').setValue(per);
                $('txtReflactionrequiredForLandLordPr').setValue(reflectionrequired.toFixed(2));

            }
            else {
                if (util.isFloat($F('txtVerifiedrent1ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent1ForLandLordJt'))) {
                    rental1 = parseFloat($F('txtVerifiedrent1ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare1ForLandLordJt')) && !util.isEmpty($F('txtShare1ForLandLordJt'))) {
                    share1 = parseFloat($F('txtShare1ForLandLordJt'));
                }

                verifiedrent1Total = parseFloat((parseFloat(rental1) * parseFloat(share1)) / 100);

                if (util.isFloat($F('txtVerifiedrent2ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent2ForLandLordJt'))) {
                    rental2 = parseFloat($F('txtVerifiedrent2ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare2ForLandLordJt')) && !util.isEmpty($F('txtShare2ForLandLordJt'))) {
                    share2 = parseFloat($F('txtShare2ForLandLordJt'));
                }
                verifiedrent2Total = parseFloat((parseFloat(rental2) * parseFloat(share2)) / 100);
                if (util.isFloat($F('txtVerifiedrent3ForLandLordJt')) && !util.isEmpty($F('txtVerifiedrent3ForLandLordJt'))) {
                    rental3 = parseFloat($F('txtVerifiedrent3ForLandLordJt'));
                }
                if (util.isFloat($F('txtShare3ForLandLordJt')) && !util.isEmpty($F('txtShare3ForLandLordJt'))) {
                    share3 = parseFloat($F('txtShare3ForLandLordJt'));
                }
                verifiedrent3Total = parseFloat((parseFloat(rental3) * parseFloat(share3)) / 100);
                totalrentalIncome = parseFloat(verifiedrent1Total) + parseFloat(verifiedrent2Total) + parseFloat(verifiedrent3Total);

                if (totalrentalIncome >= 32500 && totalrentalIncome < 49999) {
                    per = 40;
                }
                if (totalrentalIncome >= 50000 && totalrentalIncome < 75000) {
                    per = 50;
                }
                if (totalrentalIncome >= 75000 && totalrentalIncome < 150000) {
                    per = 55;
                }
                if (totalrentalIncome >= 150000) {
                    per = 60;
                }


                reflectionrequired = parseFloat((totalrentalIncome * per) / 100);
                $('txtRentalIncome1ForLandLordJt').setValue(verifiedrent1Total.toFixed(2));
                $('txtRentalIncome2ForLandLordJt').setValue(verifiedrent2Total.toFixed(2));
                $('txtRentalIncome3ForLandLordJt').setValue(verifiedrent3Total.toFixed(2));
                $('txttotalrentalIncomeForLandLordJt').setValue(totalrentalIncome.toFixed(2));
                $('txtReflactionPerForLandlordJt').setValue(per);
                $('txtReflactionrequiredForLandLordJt').setValue(reflectionrequired.toFixed(2));
            }
            loanCalculatorHelPer.setLandLordCalculation(accounttype);

        }
        else {
            alert("Required Number Field");
        }


    }
};


var loanCalculatorHelPer = {
    setSalaryCalculation: function(accountType) {
        var income = 0;
        var assessmentName = "";
        var assesmentCode = "";
        var employerName = "";
        var employerCode = "";
        var assMethodId = "";
        var assCatId = "";
        if (accountType == "Pr") {

            income = $F("txtTotalInComeForPrSalaried");
            $('txtIncomeBForLCPrSalary').setValue(income);

            assessmentName = $('cmbInComeAssementForSalaryForPr').options[$('cmbInComeAssementForSalaryForPr').selectedIndex].text;
            $('txtInComeAssMethLCPrSalary').setValue(assessmentName);

            assesmentCode = $('cmbInComeAssementForPr').options[$('cmbInComeAssementForPr').selectedIndex].text;
            $('txtAssCodeForLCPrSalary').setValue(assesmentCode);

            employerName = $('cmbEmployerForSalariedForPr').options[$('cmbEmployerForSalariedForPr').selectedIndex].text;
            $('txtEmployerCategoryForLCPrSalary').setValue(employerName);

            employerCode = $F("txtEmployerCodeForPr");
            $('txtEmployerCatCodeForLCPrSalary').setValue(employerCode);

            assMethodId = $F('cmbInComeAssementForSalaryForPr');
            $('txtAssMethodIdForLcPrSalary').setValue(assMethodId);

            assCatId = $F('cmbEmployerForSalariedForPr');
            $('txtAssCatIdForLcPrSalary').setValue(assCatId);

            $('cmbConsiderForLCPrSalary').setValue("0");
        }
        else {
            income = $F("txtTotalInComeForJtSalaried");
            $('txtIncomeBForLCJtSalary').setValue(income);

            assessmentName = $('cmbInComeAssementForSalaryForJt').options[$('cmbInComeAssementForSalaryForJt').selectedIndex].text;
            $('txtInComeAssMethLCJtSalary').setValue(assessmentName);

            assesmentCode = $('cmbInComeAssementForJt').options[$('cmbInComeAssementForJt').selectedIndex].text;
            $('txtAssCodeForLCJtSalary').setValue(assesmentCode);

            employerName = $('cmbEmployerForSalariedForJt').options[$('cmbEmployerForSalariedForJt').selectedIndex].text;
            $('txtEmployerCategoryForLCJtSalary').setValue(employerName);

            employerCode = $F("txtEmployerCodeForJt");
            $('txtEmployerCatCodeForLCJtSalary').setValue(employerCode);

            assMethodId = $F('cmbInComeAssementForSalaryForJt');
            $('txtAssMethodIdForLcJtSalary').setValue(assMethodId);

            assCatId = $F('cmbEmployerForSalariedForJt');
            $('txtAssCodeForLCJtSalary').setValue(assCatId);

            $('cmbConsiderForLCJtSalary').setValue("0");
            $('trSalariedForLoanCalculatorJt').style.display = "";
        }
    },
    setLandLordCalculation: function(accountType) {

        var income = 0;
        var assessmentName = "";
        var assesmentCode = "";
        var employerName = "";
        var employerSubsector = "";
        var employerCode = "";
        var assMethodId = 0;
        var assCatId = 0;

        if (accountType == "Pr") {

            income = $F("txttotalrentalIncomeForLandLordPr");
            $('txtIncomeBForLCPrRent').setValue(income);

            assessmentName = $('cmbIncomeAssementForLandLordPr').options[$('cmbIncomeAssementForLandLordPr').selectedIndex].text;
            $('txtInComeAssMethLCPrRent').setValue(assessmentName);

            assesmentCode = $F('txtInComeAssementCodeForLandLordPr');
            $('txtAssCodeForLCPrRent').setValue(assesmentCode);

            employerName = $('cmbEmployeeForLandLordPr').options[$('cmbEmployeeForLandLordPr').selectedIndex].text;
            employerSubsector = $('cmbSubsectorForLandLordPr').options[$('cmbSubsectorForLandLordPr').selectedIndex].text;
            $('txtEmployerCategoryForLCPrRent').setValue(employerName + "-" + employerSubsector);

            employerCode = $F("txtEmployeeCategoryCodeForLandLordPr");
            $('txtEmployerCatCodeForLCPrRent').setValue(employerCode);

            assMethodId = $F('cmbIncomeAssementForLandLordPr');
            $('txtAssMethodIdForLcPrrent').setValue(assMethodId);

            assCatId = $F('cmbEmployeeForLandLordPr');
            $('txtAssCatIdForLcPrRent').setValue(assCatId);

            $('cmbConsiderForLCPrRent').setValue("0");
        }
        else {
            income = $F("txttotalrentalIncomeForLandLordJt");
            $('txtIncomeBForLCJtRent').setValue(income);

            assessmentName = $('cmbIncomeAssementForLandLordJt').options[$('cmbIncomeAssementForLandLordJt').selectedIndex].text;
            $('txtInComeAssMethLCJtRent').setValue(assessmentName);

            assesmentCode = $F('txtInComeAssementCodeForLandLordJt');
            $('txtAssCodeForLCJtRent').setValue(assesmentCode);

            employerName = $('cmbEmployeeForLandLordJt').options[$('cmbEmployeeForLandLordJt').selectedIndex].text;
            employerSubsector = $('cmbSubsectorForLandLordJt').options[$('cmbSubsectorForLandLordJt').selectedIndex].text;
            $('txtEmployerCategoryForLCJtRent').setValue(employerName + "-" + employerSubsector);

            employerCode = $F("txtEmployeeCategoryCodeForLandLordJt");
            $('txtEmployerCatCodeForLCJtRent').setValue(employerCode);

            assMethodId = $F('cmbIncomeAssementForLandLordJt');
            $('txtAssMethodIdForLcJtrent').setValue(assMethodId);

            assCatId = $F('cmbEmployeeForLandLordJt');
            $('txtAssCatIdForLcJtRent').setValue(assCatId);

            $('cmbConsiderForLCJtRent').setValue("0");
            $('trLandlordForLoanCalculatorJt').style.display = "";
        }
    },
    setBussinessManCalculation: function(bankno, accountype, tabName) {

        var income = 0;
        var assessmentName = "";
        var assesmentCode = "";
        var employerName = "";
        var employerSubsector = "";
        var employerCode = "";
        var consider = 0;
        var bankName = "";
        var bankId = "0";
        var assMethodId = "0";
        var assCatId = "0";
        var averageBalance = 0;

        if (accountype == "Pr" && tabName == "BM") {
            income = $F("txtNetincomeForPrimaryBank" + bankno);
            $('txtIncomeBForLCPrBank' + bankno).setValue(income);

            averageBalance = $F("txtAverageBalanceFor12MonthBank" + bankno);
            $('txtAverageBalanceIdForLcPrBank' + bankno).setValue(averageBalance);

            if ($F('cmbIncomeAssessmentForPrimaryBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForPrimaryBank' + bankno).options[$('cmbIncomeAssessmentForPrimaryBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCPrBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForPrimaryBank' + bankno);
            $('txtAssCodeForLCPrBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForPrimaryBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForPrimaryBank' + bankno).options[$('cmbIndustryForPrimaryBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForPrimaryBank' + bankno).options[$('cmbSubsectorForPrimaryBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForPrimaryBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForPrimaryBank" + bankno);
            $('txtEmployerCatCodeForLCPrBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCPrBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForBank' + bankno + 'IncomeGenerator').options[$('cmbBankNameForBank' + bankno + 'IncomeGenerator').selectedIndex].text;
            $('tdBank' + bankno + 'ForLCPr').innerHTML = bankName;

            bankId = $F('cmbBankNameForBank' + bankno + 'IncomeGenerator');
            $('txtbankIdForLcPrBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForPrimaryBank' + bankno);
            $('txtAssMethodIdForLcPrBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForPrimaryBank' + bankno);
            $('txtAssCatIdForLcPrBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorPr').style.display = "";

        }
        else if (accountype == "Jt" && tabName == "BM") {
            income = $F("txtNetIncomeForJointBank" + bankno);
            $('txtIncomeBForLCJtBank' + bankno).setValue(income);
            if ($F('cmbIncomeAssementForJointBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssementForJointBank' + bankno).options[$('cmbIncomeAssementForJointBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCJtBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssementCodeForJointBank' + bankno);
            $('txtAssCodeForLCJtBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForJointBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForJointBank' + bankno).options[$('cmbIndustryForJointBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubSectorForJointBank' + bankno).options[$('cmbSubSectorForJointBank' + bankno).selectedIndex].text;
                if ($F('cmbSubSectorForJointBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtSubSectorCode1ForJointBank" + bankno);
            $('txtEmployerCatCodeForLCJtBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCJtBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankName1ForJointBank' + bankno).options[$('cmbBankName1ForJointBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCJt').innerHTML = bankName;
            $("bmjttabBank" + bankno).innerHTML = bankName;

            bankId = $F('cmbBankName1ForJointBank' + bankno);
            $('txtbankIdForLcJtBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssementForJointBank' + bankno);
            $('txtAssMethodIdForLcJtBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForJointBank' + bankno);
            $('txtAssCatIdForLcJtBank' + bankno).setValue(assCatId);
        }
        else if (accountype == "Pr" && tabName == "SE") {
            income = $F("txtNetincomeForSEPrBank" + bankno);
            $('txtIncomeBForLCPrBank' + bankno).setValue(income);

            averageBalance = $F("txtAverageBalanceForSEPr12MonthBank" + bankno);
            $('txtAverageBalanceIdForLcPrBank' + bankno).setValue(averageBalance);

            if ($F('cmbIncomeAssessmentForSEPrBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForSEPrBank' + bankno).options[$('cmbIncomeAssessmentForSEPrBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCPrBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForSEPrBank' + bankno);
            $('txtAssCodeForLCPrBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForSEPrBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForSEPrBank' + bankno).options[$('cmbIndustryForSEPrBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForSEPrBank' + bankno).options[$('cmbSubsectorForSEPrBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForSEPrBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCPrBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForSEPrBank" + bankno);
            $('txtEmployerCatCodeForLCPrBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCPrBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCPrBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForSEBank' + bankno).options[$('cmbBankNameForSEBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCPr').innerHTML = bankName;

            bankId = $F('cmbBankNameForSEBank' + bankno);
            $('txtbankIdForLcPrBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForSEPrBank' + bankno);
            $('txtAssMethodIdForLcPrBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForSEPrBank' + bankno);
            $('txtAssCatIdForLcPrBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorPr').style.display = "";
        }
        else if (accountype == "Jt" && tabName == "SE") {
            income = $F("txtNetincomeForSEJtBank" + bankno);
            $('txtIncomeBForLCJtBank' + bankno).setValue(income);
            if ($F('cmbIncomeAssessmentForSEJtBank' + bankno) != "-1") {
                assessmentName = $('cmbIncomeAssessmentForSEJtBank' + bankno).options[$('cmbIncomeAssessmentForSEJtBank' + bankno).selectedIndex].text;
                $('txtInComeAssMethLCJtBank' + bankno).setValue(assessmentName);
            }

            assesmentCode = $F('txtIncomeAssessmentCodeForSEJtBank' + bankno);
            $('txtAssCodeForLCJtBank' + bankno).setValue(assesmentCode);

            if ($F('cmbIndustryForSEJtBank' + bankno) != "-1") {
                employerName = $('cmbIndustryForSEJtBank' + bankno).options[$('cmbIndustryForSEJtBank' + bankno).selectedIndex].text;
                employerSubsector = $('cmbSubsectorForSEJtBank' + bankno).options[$('cmbSubsectorForSEJtBank' + bankno).selectedIndex].text;
                if ($F('cmbSubsectorForSEJtBank' + bankno) == "-1") {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName);
                }
                else {
                    $('txtEmployerCategoryForLCJtBank' + bankno).setValue(employerName + "-" + employerSubsector);
                }

            }

            employerCode = $F("txtProfitMarginCodeForSEJtBank" + bankno);
            $('txtEmployerCatCodeForLCJtBank' + bankno).setValue(employerCode);
            consider = $F("cmbConsiderForLCJtBank" + bankno);
            if (consider != "1") {
                $('cmbConsiderForLCJtBank' + bankno).setValue("0");
            }
            bankName = $('cmbBankNameForSEJtBank' + bankno).options[$('cmbBankNameForSEJtBank' + bankno).selectedIndex].text;
            $('tdBank' + bankno + 'ForLCJt').innerHTML = bankName;

            bankId = $F('cmbBankNameForSEJtBank' + bankno);
            $('txtbankIdForLcJtBank' + bankno).setValue(bankId);

            assMethodId = $F('cmbIncomeAssessmentForSEJtBank' + bankno);
            $('txtAssMethodIdForLcJtBank' + bankno).setValue(assMethodId);

            assCatId = $F('cmbIndustryForSEJtBank' + bankno);
            $('txtAssCatIdForLcJtBank' + bankno).setValue(assCatId);

            $('trBank' + bankno + 'ForLoanCalculatorJt').style.display = "";
        }
        loanCalculatorHelPer.CalculateLoanCalculator(accountype);


    },
    changeIncomeAssementMethodForCashSequre: function() {
        var incomeAssId = $F("cmbAssessmentMethodForLcCash");
        var incomeAssBuss = Page.incomeAssementArray.findByProp('INAM_ID', incomeAssId);
        if (incomeAssBuss) {
            $("txtAssessmentCodeLc").setValue(incomeAssBuss.INAM_METHODCODE);
            $("txtAssessmentMethodIdLc").setValue(incomeAssId);
            $("txtAssessmentMethodLc").setValue(incomeAssBuss.INAM_METHOD);
        }
        else {
            $("txtAssessmentCodeLc").setValue('FF');
            $("txtAssessmentMethodIdLc").setValue('-1');
            $("txtAssessmentMethodLc").setValue('Income Assessment Not Required');
        }
    },
    changeCategoryForCashSequre: function() {
        var categoryId = $F("cmbCategoryForLcCash");
        if (categoryId != "-1") {

            var categoryName = $('cmbCategoryForLcCash').options[$('cmbCategoryForLcCash').selectedIndex].text;
            $("txtEmployeeCategoryIdForLc").setValue(categoryId);
            $("txtEmployeeCategoryForLc").setValue(categoryName);
            $("txtEmployeeCategoryCodeForLc").setValue('N/A');
        }
    },
    changeCibInfo: function() {
        var cibInfoId = $F("cmbBureauHistory");
        var cib = Page.cibInfo.findByProp('CibInfoId', cibInfoId);
        if (cib) {
            $("txtCibInfoDesc").setValue(cib.CibInfoDescription);
            $("txtCibInfoCode").setValue(cib.CibInfoCode);

        }
        else {
            $("txtCibInfoDesc").setValue('');
            $("txtCibInfoCode").setValue('');
        }
    },
    checkDoctorChambers: function(accountType, tabName) {

        var isDoctor = false;
        var isTeacher = false;
        if (accountType == "Pr" && tabName == "DC") {
            for (var i = 1; i < 7; i++) {
                if ($F("cmbIndustryForSEPrBank" + i) == "12" && $F("cmbSubsectorForSEPrBank" + i) == "143" && $F("cmbPrivateIncomeForSEPrBank" + i) == "1") {
                    isDoctor = true;
                    break;
                }
            }
            if (isDoctor) {
                loanCalculatorHelPer.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trDoctorForLoanCalculatorPr').style.display = "none";
            }

        }
        else if (accountType == "Jt" && tabName == "DC") {
            for (var i = 1; i < 7; i++) {
                if ($F("cmbIndustryForSEJtBank" + i) == "12" && $F("cmbSubsectorForSEJtBank" + i) == "143" && $F("cmbPrivateIncomeForSEJtBank" + i) == "1") {
                    isDoctor = true;
                    break;
                }
            }
            if (isDoctor) {
                loanCalculatorHelPer.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trDoctorForLoanCalculatorJt').style.display = "none";
            }

        }
        else if (accountType == "Pr" && tabName == "TC") {
            for (var i = 1; i < 7; i++) {
                if ($F("cmbIndustryForSEPrBank" + i) == "12" && $F("cmbSubsectorForSEPrBank" + i) == "150" && $F("cmbPrivateIncomeForSEPrBank" + i) == "1") {
                    isTeacher = true;
                    break;
                }
            }
            if (isTeacher) {
                loanCalculatorHelPer.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trTeacherForLoanCalculatorPr').style.display = "none";
            }

        }
        else if (accountType == "Jt" && tabName == "TC") {
            for (var i = 1; i < 7; i++) {
                if ($F("cmbIndustryForSEJtBank" + i) == "12" && $F("cmbSubsectorForSEJtBank" + i) == "150" && $F("cmbPrivateIncomeForSEJtBank" + i) == "1") {
                    isTeacher = true;
                    break;
                }
            }
            if (isTeacher) {
                loanCalculatorHelPer.setBussinessManCalculationForDoctorAndTeachers(accountType, tabName);
            }
            else {
                $('trTeacherForLoanCalculatorJt').style.display = "none";
            }

        }
    },

    setBussinessManCalculationForDoctorAndTeachers: function(accountType, tabName) {
        var income = 0;
        var consider = 0;

        if (accountType == "Pr" && tabName == "DC") {
            if (util.isFloat($F('txtTotalIncomeForSEPrBank1')) && !util.isEmpty($F('txtTotalIncomeForSEPrBank1'))) {
                income += parseFloat($F('txtTotalIncomeForSEPrBank1'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEPrBank2')) && !util.isEmpty($F('txtTotalIncomeForSEPrBank2'))) {
                income += parseFloat($F('txtTotalIncomeForSEPrBank2'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEPrBank3')) && !util.isEmpty($F('txtTotalIncomeForSEPrBank3'))) {
                income += parseFloat($F('txtTotalIncomeForSEPrBank3'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEPrBank4')) && !util.isEmpty($F('txtTotalIncomeForSEPrBank4'))) {
                income += parseFloat($F('txtTotalIncomeForSEPrBank4'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEPrBank5')) && !util.isEmpty($F('txtTotalIncomeForSEPrBank5'))) {
                income += parseFloat($F('txtTotalIncomeForSEPrBank5'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEPrBank6')) && !util.isEmpty($F('txtTotalIncomeForSEPrBank6'))) {
                income += parseFloat($F('txtTotalIncomeForSEPrBank6'));
            }
            $('txtIncomeBForLCPrDoctor').setValue(income);

            $('txtInComeAssMethLCPrDoctor').setValue("Doctor");
            $('txtAssCodeForLCPrDoctor').setValue('FD');
            $('txtAssMethodIdForLcPrDoc').setValue('4');
            $('txtAssCatIdForLcPrDoc').setValue('12');


            $('txtEmployerCategoryForLCPrDoctor').setValue('Professional - Doctors');
            $('txtEmployerCatCodeForLCPrDoctor').setValue('YA1');




            consider = $F("cmbConsiderForLCPrDoctor");
            if (consider != "1") {
                $('cmbConsiderForLCPrDoctor').setValue("0");
            }
            $('trDoctorForLoanCalculatorPr').style.display = "";
        }
        else if (accountType == "Jt" && tabName == "DC") {
            if (util.isFloat($F('txtTotalIncomeForSEJtBank1')) && !util.isEmpty($F('txtTotalIncomeForSEJtBank1'))) {
                income += parseFloat($F('txtTotalIncomeForSEJtBank1'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEJtBank2')) && !util.isEmpty($F('txtTotalIncomeForSEJtBank2'))) {
                income += parseFloat($F('txtTotalIncomeForSEJtBank2'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEJtBank3')) && !util.isEmpty($F('txtTotalIncomeForSEJtBank3'))) {
                income += parseFloat($F('txtTotalIncomeForSEJtBank3'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEJtBank4')) && !util.isEmpty($F('txtTotalIncomeForSEJtBank4'))) {
                income += parseFloat($F('txtTotalIncomeForSEJtBank4'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEJtBank5')) && !util.isEmpty($F('txtTotalIncomeForSEJtBank5'))) {
                income += parseFloat($F('txtTotalIncomeForSEPrBank5'));
            }
            if (util.isFloat($F('txtTotalIncomeForSEJtBank6')) && !util.isEmpty($F('txtTotalIncomeForSEJtBank6'))) {
                income += parseFloat($F('txtTotalIncomeForSEJtBank6'));
            }
            $('txtIncomeBForLCJtDoctor').setValue(income);

            $('txtInComeAssMethLCJtDoctor').setValue("Doctor");
            $('txtAssCodeForLCJtDoctor').setValue('FD');

            $('txtEmployerCategoryForLCJtDoctor').setValue('Professional - Doctors');
            $('txtEmployerCatCodeForLCJtDoctor').setValue('YA1');
            $('txtAssMethodIdForLcJtDoc').setValue('4');
            $('txtAssCatIdForLcJtDoc').setValue('12');

            consider = $F("cmbConsiderForLCJtDoctor");
            if (consider != "1") {
                $('cmbConsiderForLCJtDoctor').setValue("0");
            }
            $('trDoctorForLoanCalculatorJt').style.display = "";
        }
        else if (accountType == "Pr" && tabName == "TC") {
            if (util.isFloat($F('txtTotalInComeTutionForSEPrBank1')) && !util.isEmpty($F('txtTotalInComeTutionForSEPrBank1'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEPrBank1'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEPrBank2')) && !util.isEmpty($F('txtTotalInComeTutionForSEPrBank2'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEPrBank2'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEPrBank3')) && !util.isEmpty($F('txtTotalInComeTutionForSEPrBank3'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEPrBank3'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEPrBank4')) && !util.isEmpty($F('txtTotalInComeTutionForSEPrBank4'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEPrBank4'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEPrBank5')) && !util.isEmpty($F('txtTotalInComeTutionForSEPrBank5'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEPrBank5'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEPrBank6')) && !util.isEmpty($F('txtTotalInComeTutionForSEPrBank6'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEPrBank6'));
            }
            $('txtIncomeBForLCPrTeacher').setValue(income);

            $('txtInComeAssMethLCPrTeacher').setValue("Teachers");
            $('txtAssCodeForLCPrTeacher').setValue('TS');

            $('txtEmployerCategoryForLCPrTeacher').setValue('Professional - Teachers');
            $('txtEmployerCatCodeForLCPrTeacher').setValue('YT1');

            $('txtAssMethodIdForLcPrTec').setValue('150');
            $('txtAssCatIdForLcPrTec').setValue('12');

            consider = $F("cmbConsiderForLCPrTeacher");
            if (consider != "1") {
                $('cmbConsiderForLCPrTeacher').setValue("0");
            }
            $('trTeacherForLoanCalculatorPr').style.display = "";
        }
        else if (accountType == "Jt" && tabName == "TC") {
            if (util.isFloat($F('txtTotalInComeTutionForSEJtBank1')) && !util.isEmpty($F('txtTotalInComeTutionForSEJtBank1'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEJtBank1'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEJtBank2')) && !util.isEmpty($F('txtTotalInComeTutionForSEJtBank2'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEJtBank2'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEJtBank3')) && !util.isEmpty($F('txtTotalInComeTutionForSEJtBank3'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEJtBank3'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEJtBank4')) && !util.isEmpty($F('txtTotalInComeTutionForSEJtBank4'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEJtBank4'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEJtBank5')) && !util.isEmpty($F('txtTotalInComeTutionForSEJtBank5'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEJtBank5'));
            }
            if (util.isFloat($F('txtTotalInComeTutionForSEJtBank6')) && !util.isEmpty($F('txtTotalInComeTutionForSEJtBank6'))) {
                income += parseFloat($F('txtTotalInComeTutionForSEJtBank6'));
            }
            $('txtIncomeBForLCJtTeacher').setValue(income);

            $('txtInComeAssMethLCJtTeacher').setValue("Teachers");
            $('txtAssCodeForLCJtTeacher').setValue('TS');
            $('txtAssMethodIdForLcJtTec').setValue('150');
            $('txtAssCatIdForLcJtTec').setValue('12');
            $('txtEmployerCategoryForLCJtTeacher').setValue('Professional - Teachers');
            $('txtEmployerCatCodeForLCJtTeacher').setValue('YT1');
            consider = $F("cmbConsiderForLCJtTeacher");
            if (consider != "1") {
                $('cmbConsiderForLCJtTeacher').setValue("0");
            }
            $('trTeacherForLoanCalculatorJt').style.display = "";
        }
        loanCalculatorHelPer.CalculateLoanCalculator(accountType);
    },
    CalculateLoanCalculator: function(accountType) {
        var totalIncome = 0;
        var income = 0;
        var isConsider = 0;
        var declaredPrIncome = 0;
        var declaredJtIncome = 0;
        var totalJointIncome = 0;
        Page.incomeArrayforlc = [];
        Page.newIncomeArrayforAll = [];

        if (accountType == "Pr") {
            var tablePrAccount = $("tblPrLoanCalculator");
            var rowCount = tablePrAccount.rows.length;
            for (var i = 1; i <= rowCount - 1; i++) {
                income = 0;
                var obj = new Object();
                if (i < 7) {
                    isConsider = $F("cmbConsiderForLCPrBank" + i);
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrBank" + i);
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrBank" + i);
                        obj.AssessmentCode = $F("txtAssCodeForLCPrBank" + i);
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrBank" + i);
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrBank" + i);
                        obj.AssmentMethodId = $F("txtAssMethodIdForLcPrBank" + i);
                        obj.AssCatId = $F("txtAssCatIdForLcPrBank" + i);
                        obj.AverageBalance = $F("txtAverageBalanceIdForLcPrBank" + i);
                        var prProfession = $F("txtPrProfessionId");
                        var otherProfession = $F("txtPrOtherProfessionId");
                        if (prProfession == "1" || otherProfession == "1") {
                            obj.Profession = "1";
                        }
                        else {
                            obj.Profession = "3";
                        }

                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);

                    }
                    else {
                        //;
                    }
                }
                else if (i == 7) {
                    isConsider = $F("cmbConsiderForLCPrSalary");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrSalary");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrSalary");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrSalary");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrSalary");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrSalary");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "2"; //Profession
                        obj.Profession = "2";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                    else {
                        //;
                    }
                }
                else if (i == 8) {
                    isConsider = $F("cmbConsiderForLCPrRent");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrRent");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrRent");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrRent");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrRent");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrRent");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "5"; //Profession
                        obj.Profession = "5";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);

                    }
                    else {
                        //;
                    }
                }
                else if (i == 9) {
                    isConsider = $F("cmbConsiderForLCPrDoctor");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrDoctor");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrDoctor");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrDoctor");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrDoctor");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrDoctor");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "4"; //Profession
                        obj.Profession = "4";
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                }
                else if (i == 10) {
                    isConsider = $F("cmbConsiderForLCPrTeacher");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCPrTeacher");
                        obj.Income = income;
                        obj.Assessmentmethod = $F("txtInComeAssMethLCPrTeacher");
                        obj.AssessmentCode = $F("txtAssCodeForLCPrTeacher");
                        obj.EmployeeCategory = $F("txtEmployerCategoryForLCPrTeacher");
                        obj.EmployeeCategoryCode = $F("txtEmployerCatCodeForLCPrTeacher");
                        obj.AssmentMethodId = "12";
                        obj.AssCatId = "150"; //Subsector
                        obj.Profession = 3;
                        Page.incomeArrayforlc.push(obj);
                        Page.newIncomeArrayforAll.push(income);
                    }
                }

                if (util.isFloat(income) && !util.isEmpty(income)) {
                    totalIncome += parseFloat(income);
                }
            }
            $('txtTotalInComeForLcAll').setValue(totalIncome);
            declaredPrIncome = $F("txtDeclaredIncomeForLcAll");
            if (util.isFloat(declaredPrIncome) && !util.isEmpty(declaredPrIncome)) {
                if (totalIncome > parseFloat(declaredPrIncome)) {
                    $('txtAppropriteIncomeForLcAll').setValue(declaredPrIncome);
                }
                else {
                    $('txtAppropriteIncomeForLcAll').setValue(totalIncome);
                }
            }
            else {
                $('txtAppropriteIncomeForLcAll').setValue('0');
            }

        }
        else {
            var tableJtAccount = $("tblJtLoanCalculator");
            var rowCountJt = tableJtAccount.rows.length;
            for (var i = 1; i <= rowCountJt - 1; i++) {
                income = 0;
                if (i < 7) {
                    isConsider = $F("cmbConsiderForLCJtBank" + i);
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtBank" + i);
                    }
                }
                else if (i == 7) {
                    isConsider = $F("cmbConsiderForLCJtSalary");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtSalary");
                    }
                }
                else if (i == 8) {
                    isConsider = $F("cmbConsiderForLCJtRent");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtRent");
                    }
                }
                else if (i == 9) {
                    isConsider = $F("cmbConsiderForLCJtDoctor");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtDoctor");
                    }
                }
                else if (i == 10) {
                    isConsider = $F("cmbConsiderForLCJtTeacher");
                    if (isConsider == "1") {
                        income = $F("txtIncomeBForLCJtTeacher");
                    }
                }



                if (util.isFloat(income) && !util.isEmpty(income)) {
                    totalIncome += parseFloat(income);
                }
            }
            $('txtTotalIncomeForLcJointAll').setValue(totalIncome);

            declaredJtIncome = $F("txtDeclaredIncomeForLcJointAll");
            if (util.isFloat(declaredJtIncome) && !util.isEmpty(declaredJtIncome)) {
                if (totalIncome > parseFloat(declaredJtIncome)) {
                    $('txtAppropriteIncomeForLcJointAll').setValue(declaredJtIncome);
                }
                else {
                    $('txtAppropriteIncomeForLcJointAll').setValue(totalIncome);
                }
            }
            else {
                $('txtAppropriteIncomeForLcJointAll').setValue('0');
            }
        }

        var reasonofJointApplicant = $F("cmbReasonofJointApplicant");
        if (reasonofJointApplicant == "1") {
            var appropripteIncomePr = $F("txtAppropriteIncomeForLcAll");
            var appropriteIncomeJt = $F("txtAppropriteIncomeForLcJointAll");

            if (util.isFloat(appropripteIncomePr) && !util.isEmpty(appropripteIncomePr)) {
                totalJointIncome += parseFloat(appropripteIncomePr);
            }
            if (util.isFloat(appropriteIncomeJt) && !util.isEmpty(appropriteIncomeJt)) {
                totalJointIncome += parseFloat(appropriteIncomeJt);
            }
            $('txtTotalJointInComeForLcWithCal').setValue(totalJointIncome);

        }
        else {
            var appropripteIncome = $F("txtAppropriteIncomeForLcAll");
            if (util.isFloat(appropripteIncome) && !util.isEmpty(appropripteIncome)) {
                totalJointIncome += parseFloat(appropripteIncome);
            }
            $('txtTotalJointInComeForLcWithCal').setValue(totalJointIncome);
        }
        analyzeLoanApplicationHelper.changeCashSecured();
        var dbrLevel = $F("cmbDBRLevel");
        if (dbrLevel == "" || dbrLevel == "1") {
            $("txtAppropriateDbr").setValue('50');
        }
        analyzeLoanApplicationHelper.changeConsideredDbr();


    },
    createExistingrepaymentGrid: function() {
        var limit = 0;
        var interest = 0;
        var typeName = "";
        var bankName = "";
        var emiShare = 0;
        var totalInterestOnScbCreditCard = 0;
        var totalInterestOffScbCreditcard = 0;
        var totalEmiShareOnScb = 0;
        var totalEmiShareOffScb = 0;
        //credit card----------------
        for (var j = 1; j < 3; j++) {
            var sequrityForCreditCard = $F("cmbStatusForSecuritiesForCreditCard" + j);
            if (sequrityForCreditCard == "1") {
                limit = $F("txtOriginalLimitForCreditCard" + j);
                interest = $F("txtInterestForCreditCard" + j);
                if (util.isFloat(interest) && !util.isEmpty(interest)) {
                    $("txtLimitCarditcardForLcExistingPaymentForScb" + j).setValue(limit);
                    $("txtInterestcreditcardForLcExistingPaymentForScb" + j).setValue(interest);
                    totalInterestOnScbCreditCard += parseFloat(interest);
                }
                limit = 0;
                interest = 0;
            }
            else {
                $("txtLimitCarditcardForLcExistingPaymentForScb" + j).setValue('');
                $("txtInterestcreditcardForLcExistingPaymentForScb" + j).setValue('');
            }
            var sequrityCardOffScb = $F("cmbStatusForCreditCardOffScb" + j);
            if (sequrityCardOffScb == "1") {
                limit = $F("txtOriginalLimitForcreditCardOffScb" + j);
                interest = $F("txtInterestForCreditCardForOffScb" + j);
                if (util.isFloat(interest) && !util.isEmpty(interest)) {
                    $("txtLimitCarditcardForLcExistingPaymentForOffScb" + j).setValue(limit);
                    $("txtInterestcreditcardForLcExistingPaymentOnScb" + j).setValue(interest);
                    totalInterestOffScbCreditcard += parseFloat(interest);
                }
                limit = 0;
                interest = 0;
            }
            else {
                $("txtLimitCarditcardForLcExistingPaymentForOffScb" + j).setValue('');
                $("txtInterestcreditcardForLcExistingPaymentOnScb" + j).setValue('');
            }
        }
        $("txtTotalinterestForOnScbLcExistingrepayment").setValue(totalInterestOnScbCreditCard);
        $("txtTotalinterestForOffScbLcExistingrepayment").setValue(totalInterestOffScbCreditcard);
        //Term loans
        var sequrityForTerLoanOnScb = "";
        var sequrityForTermLoanOffScb = "";
        for (var i = 1; i < 5; i++) {
            sequrityForTerLoanOnScb = $F("cmbStatusForSecurities" + i);
            if (sequrityForTerLoanOnScb == "1") {
                typeName = $('cmbTypeForTermLoans' + i).options[$('cmbTypeForTermLoans' + i).selectedIndex].text;
                emiShare = $F("txtEMIShare" + i);
                if (util.isFloat(emiShare) && !util.isEmpty(emiShare)) {
                    $("txtTypeForLcExistingPayment" + i).setValue(typeName);
                    $("txtEmiForLcExistingPayment" + i).setValue(emiShare);
                    totalEmiShareOnScb += parseFloat(emiShare);
                }

            }
            else {
                $("txtTypeForLcExistingPayment" + i).setValue('');
                $("txtEmiForLcExistingPayment" + i).setValue('');
            }
            sequrityForTermLoanOffScb = $F("cmbStatusForOffScb" + i);
            var bankId = $F("cmbFinancialInstitutionForOffSCB" + i);
            if (sequrityForTermLoanOffScb == "1") {
                bankName = $('cmbFinancialInstitutionForOffSCB' + i).options[$('cmbFinancialInstitutionForOffSCB' + i).selectedIndex].text;
                emiShare = $F("txtEMIShareForOffSCB" + i);
                if (util.isFloat(emiShare) && !util.isEmpty(emiShare)) {
                    $("txtBankForLcExistingPaymentOffScb" + i).setValue(bankName);
                    $("txtEmiForLcExistingPaymentOffScb" + i).setValue(emiShare);
                    totalEmiShareOffScb += parseFloat(emiShare);
                }

            }
            else {
                $("txtBankForLcExistingPaymentOffScb" + i).setValue('');
                $("txtEmiForLcExistingPaymentOffScb" + i).setValue('');
            }

        }
        $("txtTotalEmiForLcExistingPaymentOnScb").setValue(totalEmiShareOnScb);
        $("txtTotalEmiForLcExistingPaymentOffScb").setValue(totalEmiShareOffScb);

        analyzeLoanApplicationHelper.changeConsideredDbr();

    },

    changeAskingInterest: function() {
        var askingIntersetRate = $F("txtAskingInterest");
        if (!util.isFloat(askingIntersetRate)) {
            alert("Asking rate must be a number field");
            $("txtAskingInterest").setValue('0');
            //$("txtAskingInterest").focus();
            return false;
        }

        $("txtInterestLCAskingRate").setValue(askingIntersetRate);
        loanCalculatorHelPer.calculateTenorSettings();
    },
    changeAskingTenor: function() {
        var askingTenor = $F("txtAskingtenor");
        if (!util.isFloat(askingTenor)) {
            alert("Asking rate must be a number field");
            $("txtAskingInterest").setValue('0');
            //$("txtAskingInterest").focus();
            return false;
        }

        $("txtAskingTenorForLc").setValue(askingTenor);
        loanCalculatorHelPer.calculateTenorSettings();
    },
    changeAskingRepaymentMethod: function() {
        var askingRepaymentMethod = $F("cmbAskingRepaymentmethod");
        if (askingRepaymentMethod != "-1") {
            var repaymentMethodName = askingrepayment[parseInt(askingRepaymentMethod)];
            $('cmbAskingRepaymentmethodForFinalize').setValue(repaymentMethodName);
            $('txtInstrumentForFinalizetab').setValue(askingRepaymentMethod);
        }

    },
    changeArtareference: function() {
        loanCalculatorHelPer.calculateTenorSettings();
    },
    changeGeneralInsurence: function() {
        loanCalculatorHelPer.calculateTenorSettings();
    },
    changeQuotedPrice: function() {
        var qutoedPrice = $F("txtQuotedPrice");
        if (!util.isFloat(qutoedPrice)) {
            alert("Price must be a number field");
            $("txtQuotedPrice").setValue('0');
            //$("txtQuotedPrice").focus();
            return false;
        }
        loanCalculatorHelPer.calculateTenorSettings();
    },
    changeregistationyear: function() {
        var regiyear = $F("txtregistrationYear");
        if (!util.isDigit(regiyear)) {
            alert("Year must be a number field");
            $("txtregistrationYear").setValue('0');
            //$("txtregistrationYear").focus();
            return false;
        }
        loanCalculatorHelPer.calculateTenorSettings();
    },


    changeConsideredPrice: function() {
        var consideredPrice = $F("txtConsideredprice");
        if (!util.isFloat(consideredPrice)) {
            alert("Price must be a number field");
            $("txtConsideredprice").setValue('0');
            //$("txtConsideredprice").focus();
            return false;
        }
        loanCalculatorHelPer.calculateTenorSettings();
    },
    calculateTenorSettings: function() {

        var artaPreferenceId = $F("cmbARTAReference");
        var artaAllowed = 0;
        var artaPreference = Page.artaPreference.findByProp("InsCompanyId", artaPreferenceId);
        if (artaPreference) {
            artaAllowed = artaPreference.Tenor;
            $("txtArtaAllowedFoLc").setValue(artaAllowed);
        }
        $("txtvehicleAllowedForLc").setValue(Page.tenor);
        var label = $F("cmbLabelForTonerLc");
        var segment = $F("cmbEmployeeSegmentLc");

        if (segment == "-1") {
            $("txtAgeAllowedForLc").setValue('0');

        }
        else {
            var sg = Page.segmentArray.findByProp("SegmentID", segment);
            if (label == "2") {
                //$("cmbLabelForTonerLc").setValue('1');
                var minageForlbl2 = sg.MinAgeL1;
                var maxageForlbl2 = sg.MaxAgeL1;
                if (parseFloat(Page.age) >= parseFloat(minageForlbl2) && parseFloat(Page.age) <= parseFloat(maxageForlbl2)) {
                    $("txtAgeAllowedForLc").setValue(sg.MaxTanor);
                }
                else {
                    $("txtAgeAllowedForLc").setValue(sg.MinTanor);
                }
            }
            else {
                //$("cmbLabelForTonerLc").setValue('1');
                var minageForlbl1 = sg.MinAgeL1;
                var maxageForlbl1 = sg.MaxAgeL1;
                if (parseFloat(Page.age) >= parseFloat(minageForlbl1) && parseFloat(Page.age) <= parseFloat(maxageForlbl1)) {
                    $("txtAgeAllowedForLc").setValue(sg.MaxTanor);
                }
                else {
                    $("txtAgeAllowedForLc").setValue(sg.MinTanor);
                }
            }
        }

        var askingTenor = parseFloat($F("txtAskingtenor")) / 12;
        $("txtAskingTenorForLc").setValue(askingTenor);
        if (parseFloat(artaAllowed) > parseFloat(askingTenor)) {
            $("txtAppropriteTenorForLc").setValue(askingTenor);
        }
        else {
            $("txtAppropriteTenorForLc").setValue(artaAllowed);
        }



        if (util.isFloat($F("txtCosideredtenorForLc")) && !util.isEmpty($F("txtCosideredtenorForLc"))) {
            var consideredTenor = parseFloat($F("txtCosideredtenorForLc")) * 12;
            $("txtConsideredMarginForLc").setValue(consideredTenor);
        }
        else {
            $("txtCosideredtenorForLc").setValue('0');
            var consideredTenor = parseFloat($F("txtCosideredtenorForLc")) * 12;
            $("txtConsideredMarginForLc").setValue(consideredTenor);
        }
        loanCalculatorHelPer.calculateInterestRate();

        var lbl = parseInt($F("cmbLabelForDlaLimit"));
        if (parseInt(label) > lbl) {
            $("cmbLabelForDlaLimit").setValue(label);
            var approverId = $F("cmbApprovedby");
            if (approverId != "-1") {
                analystfinalizationHelper.GetApproverLimitByUserId(approverId);
            }
        }
        else {
            analyzeLoanApplicationHelper.setagewiseLebel();
        }




    },
    calculateInterestRate: function() {

        var artaPreferenceId = $F("cmbARTAReference");
        var artaAllowed = 0;
        var vendoreArtaAllowed = 0;
        var segmentAllowed = 0;
        var appropriateAllowed = 0;
        var bdcatAllowed = 0;
        var interestAllowedArray = [];
        var artaPreference = Page.artaPreference.findByProp("InsCompanyId", artaPreferenceId);
        if (artaPreference) {
            artaAllowed = artaPreference.InsurancePercent;
        }

        var arta = $F("cmbARTA");
        if (arta == "1") {
            $("txtInterestLCVendoreAllowed").setValue($F("txtVendorwithArta"));
            $("txtInterestLCArtaAllowed").setValue(artaAllowed);
            vendoreArtaAllowed = $F("txtVendorwithArta");
            interestAllowedArray.push(artaAllowed);
            interestAllowedArray.push(vendoreArtaAllowed);
        }
        else {
            $("txtInterestLCVendoreAllowed").setValue($F("txtVendorwithoutArta"));
            $("txtInterestLCArtaAllowed").setValue('0');
            vendoreArtaAllowed = $F("txtVendorwithoutArta");
            interestAllowedArray.push(vendoreArtaAllowed);
        }
        var segment = $F("cmbEmployeeSegmentLc");

        if (segment == "-1") {
            $("txtAgeAllowedForLc").setValue('0');

        }
        else {
            var sg = Page.segmentArray.findByProp("SegmentID", segment);
            if (sg) {
                if (arta == "1") {
                    $("txtInterestLCSegmentAllowed").setValue(sg.IrWithARTA);
                    segmentAllowed = sg.IrWithARTA;
                    interestAllowedArray.push(segmentAllowed);
                }
                else {
                    $("txtInterestLCSegmentAllowed").setValue(sg.IrWithoutARTA);
                    segmentAllowed = sg.IrWithoutARTA;
                    interestAllowedArray.push(segmentAllowed);
                }
                bdcatAllowed = sg.MaxLoanAmt;
            }
            else {
                $("txtInterestLCSegmentAllowed").setValue('0');
            }
        }
        var productId = $F("cmbProductName");
        var artamin = interestAllowedArray.sort(function(a, b) { return a - b });

        if (productId == "1") {
            $("txtAppropriteRate").setValue('4.43');
        }
        else {
            $("txtAppropriteRate").setValue(artamin[0]);
        }
        $("txtInterestLCAskingRate").setValue($F("txtAskingInterest"));

        //General Insurence
        var gins = $F("cmbGeneralInsurence");
        if (gins == "1") {
            var ownDamage = 0;
            var actliability = 0;
            var ingineCc = $F("txtEngineCapacityCC");
            for (var i = 0; i < Page.ownDamageArray.length; i++) {
                var ccfrom = Page.ownDamageArray[i].CCFrom;
                var ccTo = Page.ownDamageArray[i].CCTo;
                if (parseInt(ingineCc) > parseInt(ccfrom) && parseInt(ingineCc) < parseInt(ccTo)) {
                    ownDamage = Page.ownDamageArray[i].OwnDamageCharge;
                    actliability = Page.ownDamageArray[i].ActLiability;
                    break;
                }
            }


            var seatingCapacity = $F("cmsSeatingCapacity");
            if (seatingCapacity == "-1") {
                seatingCapacity = 0;
            }
            var passengerPerseatingCapacity = $F("txtPassenger");
            var passengerAmount = parseFloat(seatingCapacity) * parseFloat(passengerPerseatingCapacity);
            var driverSeatAmount = $F("txtAmountofDriver");

            var quotedPrice = $F("txtQuotedPrice");
            var acidentoftheft = (parseFloat(quotedPrice) * parseFloat($F("txtaccidentOrTheft"))) / 100;
            var totalbeforeVat = parseFloat(driverSeatAmount) + parseFloat(passengerAmount) + parseFloat(acidentoftheft) + parseFloat(actliability) + parseFloat(ownDamage);

            var vat = ((parseFloat(totalbeforeVat) * parseFloat($F("txtVat"))) / 100);
            var stampcharge = $F("txtStampCharge");
            var commision = ((parseFloat(totalbeforeVat) * parseFloat($F("txtCommision"))) / 100);
            var grossAmount = parseFloat(totalbeforeVat) + parseFloat(vat) + parseFloat(stampcharge);
            var generalInsurenceAmount = parseFloat(grossAmount) - parseFloat(commision);
            var finalAmount = Math.round(generalInsurenceAmount);
            $("txtgeneralInsurenceForLcIns").setValue(finalAmount);
            $("txtArtaForLCInsurenceAmount").setValue('0');
            $("txtTotalInsurenceForLcInsurence").setValue(finalAmount);

        }
        else {
            $("txtgeneralInsurenceForLcIns").setValue('0');
            var approvedamount = 0;
            var loantenor = 0;
            //var age = Page.age;
            if (util.isFloat($F("txtApprovedLoanAmountForLcLoanAmount")) && !util.isEmpty($F("txtApprovedLoanAmountForLcLoanAmount"))) {
                approvedamount = $F("txtApprovedLoanAmountForLcLoanAmount");
            }
            if (util.isFloat($F("txtCosideredtenorForLc")) && !util.isEmpty($F("txtCosideredtenorForLc"))) {
                loantenor = $F("txtCosideredtenorForLc");
            }



            var insurenceAmount = parseFloat((parseFloat(approvedamount) * parseFloat(artaAllowed)) / 100) * parseFloat(loantenor);
            $("txtArtaForLCInsurenceAmount").setValue(insurenceAmount);
            $("txtTotalInsurenceForLcInsurence").setValue(insurenceAmount);

        }



        //End General Insurence


        //Loan Amount--------------------------

        var consideredPrice = (parseFloat($F("txtConsideredprice")) * 0.3);
        var generalInsurence = $F("txtgeneralInsurenceForLcIns");
        var ltvAllowed = 0;
        if (util.isFloat(generalInsurence) && !util.isEmpty(generalInsurence)) {
            ltvAllowed = consideredPrice - generalInsurence;
        }
        else {
            ltvAllowed = consideredPrice;
        }
        $("txtLtvAllowedForLcLoanAmount").setValue(ltvAllowed);

        var maxEmi = 0;
        if (util.isFloat($F("txtMaximumEmi")) && !util.isEmpty($F("txtMaximumEmi"))) {
            maxEmi = parseFloat($F("txtMaximumEmi"));
        }
        var consideredInterest = 0;
        if (util.isFloat($F("txtInterestLCCosideredRate")) && !util.isEmpty($F("txtInterestLCCosideredRate"))) {
            consideredInterest = parseFloat($F("txtInterestLCCosideredRate"));
        }

        $("txtAskingLoanAmountForLcLoanAmount").setValue($F("txtQuotedPrice"));

        var tenor = 0;
        if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
            tenor = parseFloat($F("txtConsideredMarginForLc"));
        }

        var interestperlack = loanCalculatorHelPer.PMT(parseFloat(parseFloat(consideredInterest) / 100) / 12, tenor, -100000);
        if (!util.isFloat(interestperlack)) {
            interestperlack = 0;
        }
        var incomeAllowed = (parseFloat(maxEmi) / interestperlack) * 100000;

        if (!util.isFloat(incomeAllowed)) {
            incomeAllowed = 0;
        }


        var existingFacilityAllowed = 0;
        var appincomeArray = [];

        var totalPrevEmi = loanCalculatorHelPer.calculatePrevHistoryEmi();

        $("txtIncoomeAllowedForLcLoanAmount").setValue(incomeAllowed); //Need to be calculate
        appincomeArray.push(incomeAllowed);


        $("txtbdCadAllowedForLcLoanAmount").setValue(bdcatAllowed);
        appincomeArray.push(bdcatAllowed);

        var outStandingAmount = loanCalculatorHelPer.calculateOutStandingAmountFortermloan();
        existingFacilityAllowed = parseFloat(bdcatAllowed) - parseFloat(outStandingAmount);

        $("txtExistingFacilitiesAllowedForLcLoanAmount").setValue(existingFacilityAllowed);
        appincomeArray.push(existingFacilityAllowed);
        appincomeArray.push(ltvAllowed);
        appincomeArray.push(parseFloat($F("txtQuotedPrice")));
        if (totalPrevEmi == 0) {
            var apploanamountArray = appincomeArray.sort(function(a, b) { return a - b });
            $("txtAppropriteloanAmountForLcLoanAmount").setValue(apploanamountArray[0]);
        }
        else {
            if (ltvAllowed > existingFacilityAllowed) {
                $("txtAppropriteloanAmountForLcLoanAmount").setValue(existingFacilityAllowed);
            }
            else {
                $("txtAppropriteloanAmountForLcLoanAmount").setValue(ltvAllowed);
            }
        }
        //Need to be calculate
        loanCalculatorHelPer.calculateTotalLoanAmount();

    },
    calculateTotalLoanAmount: function() {
        var approvedAmount = 0;
        var totalInsurenceAmount = 0;
        var artaAllowed = 0;
        var artaPreferenceId = $F("cmbARTAReference");
        var artaPreference = Page.artaPreference.findByProp("InsCompanyId", artaPreferenceId);
        if (artaPreference) {
            artaAllowed = artaPreference.InsurancePercent;
        }
        if (util.isFloat($F("txtApprovedLoanAmountForLcLoanAmount")) && !util.isEmpty($F("txtApprovedLoanAmountForLcLoanAmount"))) {
            approvedAmount = parseFloat($F("txtApprovedLoanAmountForLcLoanAmount"));
        }
        if (util.isFloat($F("txtTotalInsurenceForLcInsurence")) && !util.isEmpty($F("txtTotalInsurenceForLcInsurence"))) {
            totalInsurenceAmount = parseFloat($F("txtTotalInsurenceForLcInsurence"));
        }
        var totalLoanAmount = parseFloat(approvedAmount) + parseFloat(totalInsurenceAmount);
        $("txtloanAmountForLcLoanSummary").setValue(totalLoanAmount);

        var productId = $F("cmbProductName");
        var emr = 0;
        var interestrate = 0;
        var tenor = 0;
        var generalinsurence = 0;
        if (util.isFloat($F("txtInterestLCCosideredRate")) && !util.isEmpty($F("txtInterestLCCosideredRate"))) {
            interestrate = (parseFloat(parseFloat($F("txtInterestLCCosideredRate")) / 100) / 12);
        }
        if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
            tenor = parseFloat($F("txtConsideredMarginForLc"));
        }
        if (util.isFloat($F("txtgeneralInsurenceForLcIns")) && !util.isEmpty($F("txtgeneralInsurenceForLcIns"))) {
            generalinsurence = parseFloat($F("txtgeneralInsurenceForLcIns"));
        }


        var emiPerlack = loanCalculatorHelPer.PMT(interestrate, tenor, -100000);
        if (!util.isFloat(emiPerlack)) {
            emiPerlack = 0;
        }
        var emionPo = Math.round(parseFloat(approvedAmount) * parseFloat(emiPerlack) / 100000);
        var generalInsurencePerlack = Math.round(parseFloat(parseFloat(emiPerlack) * parseFloat(generalinsurence)) / 100000);
        var lifeInsurence = approvedAmount * parseFloat(parseFloat(artaAllowed) / 100) * parseFloat(tenor / 12);
        var lifeinsurencePerlack = (parseFloat(lifeInsurence) * parseFloat(emionPo)) / 100000;
        emr = parseFloat(emionPo) + parseFloat(generalInsurencePerlack) + parseFloat(lifeinsurencePerlack);
        if (productId == "1") {
            $("txtEmrForlcloanSummary").setValue(emr.toFixed(2));
        }
        else {
            $("txtEmrForlcloanSummary").setValue(Math.round(emr));
        }

        //
        var ltvExcludingInsurence = 0;
        var ltbIncludingInsurence = 0;
        var consideredPrice = 0;
        if (util.isFloat($F("txtConsideredprice")) && !util.isEmpty($F("txtConsideredprice"))) {
            consideredPrice = parseFloat($F("txtConsideredprice"));
        }
        if (consideredPrice != 0) {
            ltvExcludingInsurence = parseFloat(approvedAmount / consideredPrice);
            $("txtLtvExclusdingInsurence").setValue(ltvExcludingInsurence.toFixed(2));

            ltbIncludingInsurence = parseFloat(totalLoanAmount / consideredPrice);
            $("txtltvIncludingLtvForLcLoanSummary").setValue(ltbIncludingInsurence.toFixed(2));
        }


        //        var ltbIncludingInsurence = parseFloat(totalLoanAmount / consideredPrice);
        //        $("txtltvIncludingLtvForLcLoanSummary").setValue(ltbIncludingInsurence.toFixed(2));
        var extraltv = ltbIncludingInsurence - ltvExcludingInsurence;
        $("txtExtraLtvForLcloanSumary").setValue(extraltv.toFixed(2));
        var dbrExcludingInsurence = 0;
        var existingEmi = 0;
        var totalJointIncome = 0;
        if (util.isFloat($F("txtExistingEmi")) && !util.isEmpty($F("txtExistingEmi"))) {
            existingEmi = parseFloat($F("txtExistingEmi"));
        }
        if (util.isFloat($F("txtTotalJointInComeForLcWithCal")) && !util.isEmpty($F("txtTotalJointInComeForLcWithCal"))) {
            totalJointIncome = parseFloat($F("txtTotalJointInComeForLcWithCal"));
        }

        dbrExcludingInsurence = parseFloat((emionPo + existingEmi) / totalJointIncome);

        if (!util.isFloat(dbrExcludingInsurence)) {
            dbrExcludingInsurence = 0;
        }

        $("txtDBRExcludingInsurence").setValue(dbrExcludingInsurence.toFixed(2));

        var dbrIncludingInsurence = 0;
        dbrIncludingInsurence = parseFloat((emr + existingEmi) / totalJointIncome);
        if (!util.isFloat(dbrIncludingInsurence)) {
            dbrIncludingInsurence = 0;
        }
        $("txtDBRIncludingInsurence").setValue(dbrIncludingInsurence.toFixed(2));

        var extraDbr = dbrIncludingInsurence - dbrExcludingInsurence;
        $("txtExtraDBRForLcloanSummary").setValue(extraDbr.toFixed(2));
        analystfinalizationHelper.calculateRepaymentForFinalize();

    },

    PMT: function(i, n, p) {
        return i * p * Math.pow((1 + i), n) / (1 - Math.pow((1 + i), n));
    },


    calculatePrevHistoryEmi: function() {
        var totalAmount = 0;
        for (var i = 1; i < Page.prCount; i++) {
            if (util.isFloat($F("txttotalForOffSCBForPrimaryBank" + i)) && !util.isEmpty($F("txttotalForOffSCBForPrimaryBank" + i))) {
                totalAmount += parseFloat($F("txttotalForOffSCBForPrimaryBank" + i));
            }
            if (util.isFloat($F("txtTotalEMIClosingConsideredForSEPrBank" + i)) && !util.isEmpty($F("txtTotalEMIClosingConsideredForSEPrBank" + i))) {
                totalAmount += parseFloat($F("txtTotalEMIClosingConsideredForSEPrBank" + i));
            }
        }
        return totalAmount;
    },

    isMax: function(listofArray, value) {
        var ismax = true;
        for (var i = 0; i < listofArray.length; i++) {
            if (parseFloat(value) < parseFloat(listofArray[i])) {
                ismax = false;
                break;
            }
        }
        return ismax;
    },
    calculateOutStandingAmountFortermloan: function() {
        var outStandingAmount = 0;
        for (var i = 1; i < 5; i++) {
            if (util.isFloat($F("txtoutStanding" + i)) && !util.isEmpty($F("txtoutStanding" + i))) {
                outStandingAmount += parseFloat($F("txtoutStanding" + i));
            }
        }
        return outStandingAmount;
    }

};

analystfinalizationHelper = {
    //Repayment Method For finalize tab----------------------------------------------------------------------------------------------------

    calculateRepaymentForFinalize: function() {
        var repaymentmethod = $F("cmbAskingRepaymentmethod");
        var sequritypdc = $F("cmbSequrityPdcForFinalizetab");
        var pdcAmount = 0;
        var tenorinMonth = 0;
        if (repaymentmethod == "2") {
            if (util.isFloat($F("txtEmrForlcloanSummary")) && !util.isEmpty($F("txtEmrForlcloanSummary"))) {
                pdcAmount = parseFloat($F("txtEmrForlcloanSummary"));
            }
            if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
                tenorinMonth = parseFloat($F("txtConsideredMarginForLc"));
            }
        }
        else {
            if (sequritypdc == "1") {
                if (util.isFloat($F("txtEmrForlcloanSummary")) && !util.isEmpty($F("txtEmrForlcloanSummary"))) {
                    pdcAmount = parseFloat($F("txtEmrForlcloanSummary"));
                }
                if (util.isFloat($F("txtConsideredMarginForLc")) && !util.isEmpty($F("txtConsideredMarginForLc"))) {
                    tenorinMonth = parseFloat($F("txtConsideredMarginForLc")) / 12;
                }
                pdcAmount = pdcAmount * 6;
                tenorinMonth = tenorinMonth * 2;
            }
            else {
                pdcAmount = 0;
                tenorinMonth = 0;
            }
        }
        $("txtPdcAmountForFinalizeTab").setValue(pdcAmount);
        $("txtNoOfPdcForFinalizeTab").setValue(tenorinMonth);

        analystfinalizationHelper.populateDeviationfound();


    },
    populaterepaymentbankName: function(objOtherBanklist) {
        var link = "<option value=\"-1\">Select a bank</option>";
        for (var i = 0; i < objOtherBanklist.length; i++) {
            link += "<option value=\"" + objOtherBanklist[i].BankId + "\">" + objOtherBanklist[i].BankName + "</option>";
        }
        $("cmbBankNameForFinalizeTabofrepaymentMethod").update(link);
        Page.otherBankAccountForrepayment = objOtherBanklist;

    },
    changeBankNameForRepaymentForFinalizeTab: function() {
        var bankId = $F("cmbBankNameForFinalizeTabofrepaymentMethod");
        var bankdetails = Page.otherBankAccountForrepayment.findByProp('BankId', bankId);
        if (bankdetails) {
            $("txtAccountnumberForaBankFinalizeTab").setValue(bankdetails.AccountNumber);
        }
    },
    changeSequrityPdc: function() {
        var sequritypdc = $F("cmbSequrityPdcForFinalizetab");
        if (sequritypdc == "2") {
            $('divrepaymentDetailsForFinalizeTab').style.display = "none";
            $('divNoofPdcForFinalizetab').style.display = "none";

        }
        else {
            $('divrepaymentDetailsForFinalizeTab').style.display = "block";
            $('divNoofPdcForFinalizetab').style.display = "block";
        }
    },
    //End repayment method For finalize Tab

    //Deviation found for finalize Tab Start-----------------------------------------------------------

    populateDeviationfound: function() {
        var age = "";
        var deviationage = "";
        if (Page.age > 68) {
            age = "LEVEL - 3";
            $("txtDeviationAgeForfinalizeTab").setValue(age);
        }
        if (age == "") {
            var label = $F("cmbLabelForTonerLc");
            if (label == "2") {
                age = "LEVEL - 2";
            }
            else {
                age = "";
            }
            $("txtDeviationAgeForfinalizeTab").setValue(age);
        }
        var dbr = "";
        var appropriateDbr = 0;
        var cosideredDbr = 0;
        if (util.isFloat($F("txtAppropriateDbr")) && !util.isEmpty($F("txtAppropriateDbr"))) {
            appropriateDbr = parseFloat($F("txtAppropriateDbr"));
        }
        if (util.isFloat($F("txtConsideredDbr")) && !util.isEmpty($F("txtConsideredDbr"))) {
            cosideredDbr = parseFloat($F("txtConsideredDbr"));
        }
        var substracdbr = cosideredDbr - appropriateDbr;
        if (parseFloat(substracdbr) < 0) {
            dbr = "LEVEL - 3";
            $("txtDeviationDbrForfinalizeTab").setValue(dbr);
        }
        else {
            var label = $F("cmbLabelForTonerLc");
            if (label == "2") {
                dbr = "LEVEL - 2";
            }
            else {
                dbr = "";
            }
            $("txtDeviationDbrForfinalizeTab").setValue(dbr);
        }

        var seatingCapacity = "";
        var Seatingcapacityflag = $F("txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab");
        if (Seatingcapacityflag == "Yes") {
            $("txtSeacitingCapacityDeviationForfinalizeTab").setValue("");
        }
        else {
            $("txtSeacitingCapacityDeviationForfinalizeTab").setValue("LEVEL - 3");
        }
        var share = "";
        var shareper = 0;

        for (var i = 1; i <= Page.prCount; i++) {
            var prProfessionId = $F("txtPrProfessionId");
            var otherProfessionId = $F("txtPrOtherProfessionId");
            if (prProfessionId == "1" || otherProfessionId == "1") {
                if (util.isFloat($F("txtShareForPrimaryBank" + i)) && !util.isEmpty($F("txtShareForPrimaryBank" + i))) {
                    shareper = parseFloat($F("txtShareForPrimaryBank" + i));
                    if (shareper < 10) {
                        $("txtSharePortionForFinalizeTab").setValue("LEVEL - 3");
                        break;
                    }
                }
            }
            if (prProfessionId == "3" || otherProfessionId == "3" || prProfessionId == "4" || otherProfessionId == "4" || prProfessionId == "6" || otherProfessionId == "6") {
                if (util.isFloat($F("txtShareForSEPrBank" + i)) && !util.isEmpty($F("txtShareForSEPrBank" + i))) {
                    shareper = parseFloat($F("txtShareForSEPrBank" + i));
                    if (shareper < 10) {
                        $("txtSharePortionForFinalizeTab").setValue("LEVEL - 3");
                        break;
                    }
                }
            }

        }
        analystfinalizationHelper.populaterequiredDocument();


    },

    //Deviation found for finalize Tab End-----------------------------------------------------------------

    //populate required Document Start----------------------------------------------------------------------

    populaterequiredDocument: function() {
        var professionId = $F("txtProfessionIdForLcIncomeAndSegment");
        if (professionId == "1") {
            $("txtLetterOfIntroduction").setValue("MANDATORY");
            $("txtPayslips").setValue("MANDATORY");
            $("txtPayslips").setValue("MANDATORY");

            $("txtTradeLicence").setValue("REQUIRED");
            $("txtMoaAoa").setValue("REQUIRED");
            $("txtPartnershipDeed").setValue("REQUIRED");
            $("txtFrom").setValue("REQUIRED");
            $("txtboardresolution").setValue("REQUIRED");
            $("txtDiplomarcertificate").setValue("REQUIRED");
            $("txtProfessionalcertificate").setValue("REQUIRED");
            $("txtMunicipalTax").setValue("REQUIRED");
            $("txtTitleDeed").setValue("REQUIRED");
            $("txtRentalDeed").setValue("REQUIRED");
            $("txtUtilityBills").setValue("REQUIRED");
            $("txtLoanOfferLetter").setValue("REQUIRED");
        }
        else if (professionId == "2") {
            $("txtTradeLicence").setValue("MANDATORY");
            $("txtMoaAoa").setValue("MANDATORY");
            $("txtPartnershipDeed").setValue("MANDATORY");
            $("txtFrom").setValue("MANDATORY");
            $("txtboardresolution").setValue("MANDATORY");

            $("txtLetterOfIntroduction").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtDiplomarcertificate").setValue("REQUIRED");
            $("txtProfessionalcertificate").setValue("REQUIRED");
            $("txtMunicipalTax").setValue("REQUIRED");
            $("txtTitleDeed").setValue("REQUIRED");
            $("txtRentalDeed").setValue("REQUIRED");
            $("txtUtilityBills").setValue("REQUIRED");
            $("txtLoanOfferLetter").setValue("REQUIRED");
        }
        else if (professionId == "3") {
            $("txtDiplomarcertificate").setValue("MANDATORY");
            $("txtProfessionalcertificate").setValue("MANDATORY");
            $("txtMunicipalTax").setValue("MANDATORY");
            $("txtTitleDeed").setValue("MANDATORY");
            $("txtRentalDeed").setValue("MANDATORY");
            $("txtUtilityBills").setValue("MANDATORY");
            $("txtLoanOfferLetter").setValue("MANDATORY");

            $("txtLetterOfIntroduction").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtTradeLicence").setValue("REQUIRED");
            $("txtMoaAoa").setValue("REQUIRED");
            $("txtPartnershipDeed").setValue("REQUIRED");
            $("txtFrom").setValue("REQUIRED");
            $("txtboardresolution").setValue("REQUIRED");
        }
        else {
            $("txtLetterOfIntroduction").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtPayslips").setValue("REQUIRED");
            $("txtTradeLicence").setValue("REQUIRED");
            $("txtMoaAoa").setValue("REQUIRED");
            $("txtPartnershipDeed").setValue("REQUIRED");
            $("txtFrom").setValue("REQUIRED");
            $("txtboardresolution").setValue("REQUIRED");
            $("txtDiplomarcertificate").setValue("REQUIRED");
            $("txtProfessionalcertificate").setValue("REQUIRED");
            $("txtMunicipalTax").setValue("REQUIRED");
            $("txtTitleDeed").setValue("REQUIRED");
            $("txtRentalDeed").setValue("REQUIRED");
            $("txtUtilityBills").setValue("REQUIRED");
            $("txtLoanOfferLetter").setValue("REQUIRED");
        }
        analystfinalizationHelper.populateVerificationreport();
    },

    //End required Document---------------------------------------------------------------------------------

    //Start Verification report-----------------------------------------------------------------------------

    populateVerificationreport: function() {
        $("txtCpvreport").setValue("REQUIRED");
        $("txtCarPriceVerificationReport").setValue("REQUIRED");
        $("txtStatementAutheticationreport").setValue("REQUIRED");
        $("txtcreditReport").setValue("REQUIRED");
        $("txtCarVerificationreport").setValue("REQUIRED");
        $("txtUsedCarVerificationreport").setValue("REQUIRED");
        $("txtUsedcarDocs").setValue("NOT REQUIRED");
        $("txtCardrepaymentHistory").setValue("REQUIRED");
        var vehicleStatus = $F("cmdVehicleStatus");
        if (vehicleStatus == "3") {
            $("txtUsedcarDocs").setValue("REQUIRED");
        }
        analystfinalizationHelper.populateVerificationreportAlerts();
    },


    //End Verification Report--------------------------------------------------------------------------------
    //Verifcation report Alert Start-------------------------------------------------------------------------
    populateVerificationreportAlerts: function() {
        var segment = $F("cmbEmployeeSegmentLc");
        var appropriateIncome = 0;
        var appropriateIncomeJoint = 0;
        var appropriateloanAmount = 0;
        var approvedLoanAmount = 0;
        var totalLoanAmount = 0;
        var appropriateInterest = 0;
        var cosideredinterest = 0;
        var appropriateTenor = 0;
        var cosideredTenor = 0;
        var maxageDeductedForProgoti = 0;
        var maxAgeDeductedForAlico = 0;
        var arta = "";
        var ltvIncInsurence = 0;
        var extraLtv = 0;
        var appropriateDbr = 0;
        var dbrExcludingIns = 0;
        var extraDbr = 0;
        var seatingCapacity = 0;
        var maxAge = 0;
        var minAge = 0;
        var primaryIncome = 0;
        var jointIncome = 0;
        var ltv = 0;

        if (segment == "-1") {
            $("txtAgeAllowedForLc").setValue('0');

        }
        else {
            var sg = Page.segmentArray.findByProp("SegmentID", segment);

            var label = $F("cmbLabelForTonerLc");
            if (label == 1) {
                maxAge = sg.MaxAgeL1;
                minAge = sg.MinAgeL1;
                primaryIncome = sg.PrimaryMinIncomeL1;
                jointIncome = sg.JointMinIncomeL1;
                ltv = sg.LTVL1;
            }
            else {
                maxAge = sg.MaxAgeL2;
                minAge = sg.MinAgeL2;
                primaryIncome = sg.PrimaryMinIncomeL2;
                jointIncome = sg.JointMinIncomeL2;
                ltv = sg.LTVL2;
            }
            //Max Age
            $("txtMaxAgeAllowed").setValue(maxAge + " YEARS");
            $("txtMaxAgeFound").setValue(Page.age + "YEARS");
            if (Page.age > maxAge) {
                $("txtMaxAgeFlag").setValue("NO");
            }
            else {
                $("txtMaxAgeFlag").setValue("YES");
            }
            //Min Age
            $("txtMinAgeAllowed").setValue(minAge + " YEARS");
            $("txtMinAgeFound").setValue(Page.age + "YEARS");
            if (minAge > Page.age) {
                $("txtMinAgeFlag").setValue("NO");
            }
            else {
                $("txtMinAgeFlag").setValue("YES");
            }
            //Min Income
            $("txtMinincomeAllowed").setValue(primaryIncome);
            appropriateIncome = $F("txtAppropriteIncomeForLcAll");
            $("txtMinIncomeFound").setValue(appropriateIncome);
            if (primaryIncome > parseFloat(appropriateIncome)) {
                $("txtMinIncomeFlag").setValue("NO");
            }
            else {
                $("txtMinIncomeFlag").setValue("YES");
            }
            //location
            $("txtLocationForLc").setValue('PDD DEFINED');
            //natinality
            $("txtNatinalityForLc").setValue('BANGLADESHI');

            //Max Age Joint
            $("txtJointMaxAgeAllowed").setValue(maxAge + " YEARS");
            $("txtJointMaxAgeFound").setValue(Page.jointage + "YEARS");
            if (Page.jointage > maxAge) {
                $("txtJointMaxAgeFlag").setValue("NO");
            }
            else {
                $("txtJointMaxAgeFlag").setValue("YES");
            }
            //Min Age Joint
            $("txtJointMinAgeAllowed").setValue(minAge + " YEARS");
            $("txtJointMinAgeFound").setValue(Page.age + "YEARS");
            if (minAge > Page.jointage) {
                $("txtJointminAgeFlag").setValue("NO");
            }
            else {
                $("txtJointminAgeFlag").setValue("YES");
            }
            //Min Income Joint
            $("txtJointMinIncomeAllowed").setValue(jointIncome);
            appropriateIncomeJoint = $F("txtAppropriteIncomeForLcJointAll");
            $("txtJointMinIncomeFound").setValue(appropriateIncomeJoint);
            if (jointIncome > parseFloat(appropriateIncomeJoint)) {
                $("txtJointMinIncomeFlag").setValue("NO");
            }
            else {
                $("txtJointMinIncomeFlag").setValue("YES");
            }
            // Loan Amount
            appropriateloanAmount = $F("txtAppropriteloanAmountForLcLoanAmount");
            approvedLoanAmount = $F("txtApprovedLoanAmountForLcLoanAmount");

            $("txtLoanAmountAllowed").setValue(appropriateloanAmount);
            $("txtLoanAmountFound").setValue(approvedLoanAmount);
            if (parseFloat(approvedLoanAmount) > parseFloat(appropriateloanAmount)) {
                $("txtLoanAmountFlag").setValue("NO");
            }
            else {
                $("txtLoanAmountFlag").setValue("YES");
            }
            //BB Max Limit
            $("txtBBMaxlimitAllowed").setValue(sg.MaxLoanAmt);
            totalLoanAmount = $F("txtloanAmountForLcLoanSummary");
            $("txtBBMaxlimitFound").setValue(totalLoanAmount);
            if (parseFloat(totalLoanAmount) > parseFloat(sg.MaxLoanAmt)) {
                $("txtBBMaxlimitFlag").setValue("NO");
            }
            else {
                $("txtBBMaxlimitFlag").setValue("YES");
            }
            //Interest Rate
            appropriateInterest = $F("txtAppropriteRate");
            cosideredinterest = $F("txtInterestLCCosideredRate");
            $("txtInterestRatesAllowed").setValue(appropriateInterest);
            $("txtInterestRatesFound").setValue(cosideredinterest);
            if (parseFloat(cosideredinterest) > parseFloat(appropriateInterest)) {
                $("txtInterestRatesFlag").setValue("NO");
            }
            else {
                $("txtInterestRatesFlag").setValue("YES");
            }
            //Tenor
            appropriateTenor = $F("txtAppropriteTenorForLc");
            cosideredTenor = $F("txtCosideredtenorForLc");
            $("txtTenorAllowed").setValue(appropriateTenor);
            $("txtTenorFound").setValue(cosideredTenor);
            if (parseFloat(cosideredTenor) > parseFloat(appropriateTenor)) {
                $("txtTenorFlag").setValue("NO");
            }
            else {
                $("txtTenorFlag").setValue("YES");
            }
            // ARTA ENROLLMENT
            maxageDeductedForProgoti = 60 - Page.age;
            maxAgeDeductedForAlico = 65 - Page.age;
            if (maxageDeductedForProgoti > 0 && maxAgeDeductedForAlico > 0) {
                $("txtArtaEnrollmentAllowed").setValue("CAN ENROLL");
            }
            else {
                $("txtArtaEnrollmentAllowed").setValue("CANNOT ENROLL");
            }
            arta = $('cmbARTA').options[$('cmbARTA').selectedIndex].text;
            $("txtArtaEnrollmentFound").setValue(arta);

            if ($F("txtArtaEnrollmentFound") == "CANNOT ENROLL" && arta == "Yes") {
                $("txtArtaEnrollmentFlag").setValue('NOT ALLOWED');
            }
            else {
                $("txtArtaEnrollmentFlag").setValue('ALLOWED');
            }
            //Ltv including Banka

            $("txtLtvIncludingBankaAllowed").setValue(ltv);
            ltvIncInsurence = $F("txtltvIncludingLtvForLcLoanSummary");
            $("txtLtvIncludingBankaFound").setValue(ltvIncInsurence);

            if (parseFloat(ltvIncInsurence) > ltv) {
                $("txtLtvIncludingBankaFlag").setValue('NO');
            }
            else {
                $("txtLtvIncludingBankaFlag").setValue('YES');
            }
            // Max Ltv SPREAD
            $("txtMaxLtvSpreadAllowed").setValue('5');
            extraLtv = $F("txtExtraLtvForLcloanSumary");
            $("txtMaxLtvSpreadFound").setValue(extraLtv);

            if (parseFloat(extraLtv) > 5) {
                $("txtMaxLtvSpreadFlag").setValue('NO');
            }
            else {
                $("txtMaxLtvSpreadFlag").setValue('YES');
            }
            //DBR
            appropriateDbr = $F("txtAppropriateDbr");
            $("txtDbrForFtAllowed").setValue(appropriateDbr);
            dbrExcludingIns = $F("txtDBRExcludingInsurence");
            $("txtDbrForFtFound").setValue(dbrExcludingIns);
            if (parseFloat(dbrExcludingIns) > parseFloat(appropriateDbr)) {
                $("txtDbrForFtFlag").setValue('NO');
            }
            else {
                $("txtDbrForFtFlag").setValue('YES');
            }
            //DBR SPREAD
            $("txtDbrSpreadForFtAllowed").setValue('5');
            extraDbr = $F("txtExtraDBRForLcloanSummary");
            $("txtDbrSpreadForFtFound").setValue(extraDbr);
            if (parseFloat(extraDbr) > 5) {
                $("txtDbrSpreadForFtFlag").setValue('NO');
            }
            else {
                $("txtDbrSpreadForFtFlag").setValue('YES');
            }
            //TOTAL AUTO LOAN
            $("txtTotalAutoLoanForFtAllowed").setValue(sg.MaxLoanAmt);
            var outStandingAmount = loanCalculatorHelPer.calculateOutStandingAmountFortermloan();
            var totalLoanAmountFound = parseFloat(totalLoanAmount) + parseFloat(outStandingAmount);

            $("txtTotalAutoLoanForFtFound").setValue(totalLoanAmountFound);
            if (parseFloat(totalLoanAmountFound) > parseFloat(sg.MaxLoanAmt)) {
                $("txtTotalAutoLoanForFtFlag").setValue('NO');
            }
            else {
                $("txtTotalAutoLoanForFtFlag").setValue('YES');
            }

            //TOTAL AUTO LOAN
            $("txtApproversDlaAllowed").setValue('');
            $("txtApproversDlaFound").setValue('');
            $("txtApproversDlaFlag").setValue('');

            //CTO Obtained
            $("txtCibObtainedForFt").setValue('REQUIRED');
            $("txtCibObtainedForFtFlag").setValue('YES');

            //Car Age Allowed

            var tenorForvehicle = $F("txtvehicleAllowedForLc");
            var consideredtenor = $F("txtCosideredtenorForLc");
            $("txtCarAgeAtTenorEndAllowed").setValue(tenorForvehicle);
            $("txtCarAgeAtTenorEndFound").setValue(consideredtenor);

            if (parseFloat(consideredtenor) > parseFloat(tenorForvehicle)) {
                $("txtCarAgeAtTenorEndFlag").setValue('NO');
            }
            else {
                $("txtCarAgeAtTenorEndFlag").setValue('YES');
            }


            //Seating Capacity
            $("txtSeatingCapacityAllowed").setValue('10');
            seatingCapacity = parseInt($F("cmsSeatingCapacity")) + 1;
            $("txtSeatingCapacityFound").setValue(seatingCapacity);
            if (seatingCapacity > 10) {
                $("txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab").setValue('NO');
            }
            else {
                $("txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab").setValue('YES');
            }
            //Car Verification
            var ismou = $F("cmbVendoreRelationShip");
            var vehicleStatus = $F("cmdVehicleStatus");
            if (ismou != "1") {
                $("txtCarVerificationForFinTab").setValue('NOT REQUIRED');
            }
            else {

                var priceConsideredOn = $F("cmbPriceConsideredOn");
                var carverification = $F("cmbCarVerification");
                if (vehicleStatus != "3" && priceConsideredOn != "5" && carverification != "3") {
                    $("txtCarVerificationForFinTab").setValue('REQUIRED');
                }
                else {
                    $("txtCarVerificationForFinTab").setValue('NOT REQUIRED');
                }
            }

            //HYPOTHECATION CHECK

            if (vehicleStatus != "3") {
                $("txthypothecationForFinalizeTab").setValue('CHECKED');
            }
            else {
                $("txthypothecationForFinalizeTab").setValue('REQUIRED');
            }

            //VENDOR STATUS

            var mouStatusName = $('cmbVendoreRelationShip').options[$('cmbVendoreRelationShip').selectedIndex].text;
            $("txtVendoreStatusForFnlTab").setValue(mouStatusName);
        }



    },

    changeUser: function(identity) {
        var userId = $F(identity);
        var user = Page.userArray.findByProp('USER_ID', userId);
        if (user) {
            var userCode = user.USER_CODE;
            if (identity == "cmbAppraisedby") {
                $("txtAppraiserCode").setValue(userCode);
            }
            else if (identity == "cmbSupportedby") {
                $("txtSupportedbyCode").setValue(userCode);
            }
            else if (identity == "cmbApprovedby") {
                $("txtApprovedbyCode").setValue(userCode);
                analystfinalizationHelper.GetApproverLimitByUserId(userId);
            }

        }
    },
    GetApproverLimitByUserId: function(userId) {
        var res = PlussAuto.AnalyzeLoanApplication.GetApproverLimitByUserId(userId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.approverLimit = res.value;
        if (res.value != null) {
            var label = parseInt($F("cmbLabelForDlaLimit"));
            switch (label) {
                case 1:
                    var totalLoanAmount = parseFloat($F("txtloanAmountForLcLoanSummary"));
                    var totaldlalimitAmount = res.value.TotalDLAAmountL1;
                    if (totalLoanAmount > totaldlalimitAmount) {
                        alert("Loan Amount Cross Total DLA Amount. Please reseat another approver");
                        $("txtAutoDLALimit").setValue('0');
                        $("txtTotalSecuredlimit").setValue('0');
                        $("txtApprovedbyCode").setValue('');
                        $("cmbApprovedby").setValue('-1');
                    }
                    else {
                        $("txtAutoDLALimit").setValue(totaldlalimitAmount);
                        $("txtTotalSecuredlimit").setValue(res.value.LEVEL1_AMT);
                        $("txtApproversDlaAllowed").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFound").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFlag").setValue('YES');
                    }
                    break;
                case 2:
                    var totalLoanAmount = parseFloat($F("txtloanAmountForLcLoanSummary"));
                    var totaldlalimitAmount = res.value.TotalDLAAmountL2;
                    if (totalLoanAmount > totaldlalimitAmount) {
                        alert("Loan Amount Cross Total DLA Amount. Please reseat another approver");
                        $("txtAutoDLALimit").setValue('0');
                        $("txtTotalSecuredlimit").setValue('0');
                        $("txtApprovedbyCode").setValue('');
                        $("cmbApprovedby").setValue('-1');
                    }
                    else {
                        $("txtAutoDLALimit").setValue(totaldlalimitAmount);
                        $("txtTotalSecuredlimit").setValue(res.value.LEVEL2_AMT);
                        $("txtApproversDlaAllowed").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFound").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFlag").setValue('YES');
                    }
                    break;
                case 3:
                    var totalLoanAmount = parseFloat($F("txtloanAmountForLcLoanSummary"));
                    var totaldlalimitAmount = res.value.TotalDLAAmountL3;
                    if (totalLoanAmount > totaldlalimitAmount) {
                        alert("Loan Amount Cross Total DLA Amount. Please reseat another approver");
                        $("txtAutoDLALimit").setValue('0');
                        $("txtTotalSecuredlimit").setValue('0');
                        $("txtApprovedbyCode").setValue('');
                        $("cmbApprovedby").setValue('-1');
                    }
                    else {
                        $("txtAutoDLALimit").setValue(totaldlalimitAmount);
                        $("txtTotalSecuredlimit").setValue(res.value.LEVEL3_AMT);
                        $("txtApproversDlaAllowed").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFound").setValue(totaldlalimitAmount);
                        $("txtApproversDlaFlag").setValue('YES');
                    }
                    break;
            }

        }
        else {
            alert("This approver don't have any limit");
            $("txtAutoDLALimit").setValue('0');
            $("txtTotalSecuredlimit").setValue('0');
            $("cmbApprovedby").setValue('-1');
            $("txtApprovedbyCode").setValue('');
            $("txtApproversDlaFlag").setValue('NO');

        }

    }


};

