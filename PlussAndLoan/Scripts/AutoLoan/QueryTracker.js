﻿var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,

    pageSizeStatus: 1,
    pageNoStatus: 0,
    pagerStatus: null,


    negativeVendor: null,
    QueryTrackerListArray: [],
    ApplicationStatusListArray: [],
    QueryToArray: [],
    loginUserName: "",
    ReplayOnTrackerId:0,

    init: function() {
        util.prepareDom();
        Event.observe('btnAddNewQuery', 'click', queryTrackerHelper.addNewQueryPopupShow);
        Event.observe('btnPostNewQuery', 'click', queryTrackerManager.save);
        Event.observe('btnClose', 'click', util.hideModal.curry('newQueryPopupDiv'));
        Event.observe('btnReplay', 'click', queryTrackerManager.Reply);
        Event.observe('btnCloseReplay', 'click', util.hideModal.curry('divQueryReplayPopup'));
        Event.observe('btnLLIdSearch', 'click', queryTrackerManager.search);
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNo'),
            spnTotalPage: $('spntotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });


        queryTrackerManager.getQueryTo();
        

    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        queryTrackerManager.searchQueryDetailsByLlId();
    }
};

var queryTrackerManager = {
    search: function() {
        queryTrackerManager.searchDetailsByLlId();
        queryTrackerManager.searchQueryDetailsByLlId();
        queryTrackerManager.searchApplicationStatusByLlId();
    },
    searchDetailsByLlId: function() {
        queryTrackerHelper.clear();
        var searchKey = $F('txtLLID');
        var res = PlussAuto.QueryTracker.searchDetailsByLlId(Page.pageNo, Page.pageSize, searchKey);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        queryTrackerHelper.fillSearchResult(res);
    },

    searchQueryDetailsByLlId: function() {
        var searchKey = $F('txtLLID');
        var res = PlussAuto.QueryTracker.GetQueryTrackerListByLLID(Page.pageNo, Page.pageSize, searchKey);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        queryTrackerHelper.renderQueryDetails(res);
    },

    searchApplicationStatusByLlId: function() {
        // debugger;
        var searchKey = $F('txtLLID');
        var res = PlussAuto.QueryTracker.SearchApplicationStatusByLLID(searchKey);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        queryTrackerHelper.renderApplicationStatus(res);
    },
    getQueryTo: function() {
        var res = PlussAuto.QueryTracker.GetQueryTo();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        Page.QueryToArray = res.value;

        queryTrackerHelper.populateQueryToCmb(res);

    },

    save: function() {
        if (!util.validateEmpty('cmbQueryTo')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if ($F('cmbQueryTo') == -1) {
            alert("Please Select Query To");
            $('cmbQueryTo').focus();
            return;
        }

        var objNewQuery = Object();
        var llid = $F('txtNewQueryLLID');
        objNewQuery.LLId = llid;
        //objNewQuery.PostingById = 'Will set from .CS file'
        objNewQuery.QueryToId = $F('cmbQueryTo');
        objNewQuery.Comments = $F('txtNewQuery');

        var res = PlussAuto.QueryTracker.SaveNewQueryByLLID(objNewQuery);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("New Query Saved for LLId ='\"" + llid + "\"'");
            $('txtNewQuery').setValue('');
            //$$('#newQueryPopupDiv .txt').invoke('clear');
            queryTrackerManager.searchQueryDetailsByLlId();
            util.hideModal('newQueryPopupDiv');

        }
    },
    Reply: function() {
       // debugger;
        var objReplay = Page.QueryTrackerListArray.findByProp('TrackerId', Page.ReplayOnTrackerId);
        var objReplayOnQuery = Object();
        var llid = $F('txtReplyOnLLID');
        objReplayOnQuery.LLId = llid;
        objReplayOnQuery.RefTrackerId = objReplay.TrackerId;
        //objNewQuery.PostingById = 'Will set from .CS file'
        objReplayOnQuery.QueryToId = objReplay.QueryToId;
        objReplayOnQuery.Comments = $F('txtReplayQueryDetails');

        var res = PlussAuto.QueryTracker.ReplyOnQueryByLLID(objReplayOnQuery);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("New Query Saved for LLId ='\"" + llid + "\"'");
            $('txtNewQuery').setValue('');
            //$$('#newQueryPopupDiv .txt').invoke('clear');
            queryTrackerManager.searchQueryDetailsByLlId();
            util.hideModal('newQueryPopupDiv');

        }
    }
};

var queryTrackerHelper = {

    fillSearchResult: function(rss) {

        var objSearchRes = rss.value;
        //debugger;
        if (objSearchRes.LlId != 0) {
            //debugger;
            Page.loginUserName = objSearchRes.LoginUserName;
            $('txtApplicantName').setValue(objSearchRes.PrName);
            $('txtApplicationDate').setValue(objSearchRes.AppliedDate.format());
            $('txtDateOfBirth').setValue(objSearchRes.DateOfBirth.format());
            $('txtAppliedAmount').setValue(objSearchRes.AppliedAmount);
            $('txtAskingTenor').setValue(objSearchRes.AskingTenor);
            $('btnAddNewQuery').style.display = 'block';

        } else {
            var searchKey = $F('txtLLID');
            queryTrackerManager.searchQueryDetailsByLlId();
            queryTrackerManager.searchApplicationStatusByLlId();
            alert("Result not Found for LLID :" + searchKey);

        }

    },
    renderQueryDetails: function(rss) {
        Page.QueryTrackerListArray = rss.value;
        var html = [];
        for (var i = 0; i < Page.QueryTrackerListArray.length; i++) {
            // var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            Page.QueryTrackerListArray[i]._QueryDate = Page.QueryTrackerListArray[i].QueryDate.format();
            html.push('<tr class="' + className + '"><td>#{_QueryDate}</td><td>#{UserAndDepartment}</td><td>#{Comments}</td><td>\
                            <a title="Reply" href="javascript:;" onclick="queryTrackerHelper.replayPopupShow(#{TrackerId})" class="icon iconReplay"></a></td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.QueryTrackerListArray[i])
			     );
        }
        $('GridQueryDetails').update(html.join(''));
        var totalCount = 0;

        if (rss.value.length != 0) {
            totalCount = Page.QueryTrackerListArray[0].Totalcount;
        }
        Page.pager.reset(totalCount, Page.pageNo);
    },

    renderApplicationStatus: function(rss) {

        Page.ApplicationStatusListArray = rss.value;
        //debugger;
        var html = [];
        for (var i = 0; i < Page.ApplicationStatusListArray.length; i++) {
            // var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            Page.ApplicationStatusListArray[i]._ActionDate = Page.ApplicationStatusListArray[i].ActionDate.format();
            html.push('<tr class="' + className + '"><td>#{_ActionDate}</td><td>#{StatusName}</td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.ApplicationStatusListArray[i])
			     );

            if (i == Page.ApplicationStatusListArray.length - 1) {
                $('divCurrentStatus').style.display = 'block';
                $('spnCurrentStatus').innerHTML=Page.ApplicationStatusListArray[i].StatusName;
            }

        }
        $('GridApplicationStatus').update(html.join(''));
        //        var totalCount = 0;

        //        if (Page.ApplicationStatusListArray.length != 0) {
        //            totalCount = Page.ApplicationStatusListArray[0].Totalcount;
        //        }
        //        Page.pager.reset(totalCount, Page.pageNoStatus);
    },

    addNewQueryPopupShow: function() {
        var LlId = $F('txtLLID');
        //debugger;
        $$('#newQueryPopupDiv .txt').invoke('clear');
        $('txtNewQueryLLID').setValue($F('txtLLID'));
        $('txtPostedBy').setValue(Page.loginUserName);
        queryTrackerManager.getQueryTo();
        $('cmbQueryTo').setValue('-1');
        util.showModal('newQueryPopupDiv');
    },



    ////    Reply: function(TrackerId) {
    ////        var confirmed = window.confirm("Do you want to delete this data?");
    ////        if (confirmed) {
    ////            queryTrackerManager.TanorLtvDeleteById(vendornegativeID);
    ////            queryTrackerManager.getQueryTo();
    ////        }
    ////    },
    populateQueryToCmb: function(res) {
        document.getElementById('cmbQueryTo').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select';
        ExtraOpt.value = '-1';
        document.getElementById('cmbQueryTo').options.add(ExtraOpt);
        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].UsLeName;
            opt.value = res.value[i].UsLeId;
            document.getElementById('cmbQueryTo').options.add(opt);
        }



    },
    clear: function() {
        $('txtApplicantName').setValue('');
        $('txtApplicationDate').setValue('');
        $('txtDateOfBirth').setValue('');
        $('txtAppliedAmount').setValue('');
        $('txtAskingTenor').setValue('');
    },




    searchKeypress: function(e) {
        if (e.keyCode == 13) {
            queryTrackerManager.search();
        }
    },
    replayPopupShow: function(trackerId) {
        Page.ReplayOnTrackerId = trackerId;
        $$('#divQueryReplayPopup .txt').invoke('clear');
        queryTrackerHelper.clearForm();
        var objReplayOnQuery = Page.QueryTrackerListArray.findByProp('TrackerId', trackerId);
        $('txtReplyOnLLID').setValue(objReplayOnQuery.LLId);
        $('txtQueriedBy').setValue(objReplayOnQuery.QueriedBy);
        $('txtReply_ReplyBy').setValue(Page.loginUserName);
        $('txtQueryDetails').setValue(objReplayOnQuery.Comments);
        $('txtReplayQueryDetails').setValue('');
        util.showModal('divQueryReplayPopup');
    },

    clearForm: function() {
        $('txtReplyOnLLID').setValue('');
        $('txtQueriedBy').setValue('');
        $('txtReply_ReplyBy').setValue('');
        $('txtQueryDetails').setValue('');
        $('txtReplayQueryDetails').setValue('');
    }

};
Event.onReady(Page.init);