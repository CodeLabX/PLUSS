﻿
var DaviationFor = { 1: 'Income Assessment', 2: 'LEVEL-2', 3: 'LEVEL-3' };
var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    daviation: null,
    DaviationSettingArray: [],

    init: function() {
        util.prepareDom();
        Event.observe('lnkAddNew', 'click', DaviationSettingsHelper.update);
        Event.observe('lnkSave', 'click', DaviationSettingsManager.save);
        //    Event.observe('btnSearch', 'click', DaviationSettingsManager.render);
        Event.observe('lnkClose', 'click', util.hideModal.curry('DaviationPopupDiv'));

        //    Event.observe('lnkUploadSave', 'click', Page.upload);

        //    Event.observe('lnkUploadCancel', 'click', util.hideModal.curry('newsUploader'));
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });

        DaviationSettingsManager.render();
//        DaviationSettingsManager.getStatus();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        DaviationSettingsManager.render();
//        DaviationSettingsManager.getStatus();
    }

};

var DaviationSettingsManager = {
    
    render: function () {
        debugger;
        var search = $F('txtSearch');
        var res = PlussAuto.DeviationSettings.GetDeviationSettingsSummary(Page.pageNo, Page.pageSize, search);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        DaviationSettingsHelper.renderCollection(res);
    },
    //    getStatus: function() {
    //        //var comapanyName = $F('txtSearch');
    //        var res = PlussAuto.DeviationSettings.GetStatus(1);

    //        if (res.error) {
    //            alert(res.error.Message);
    //            return;
    //        }
    //        DaviationSettingsHelper.populateStatusCombo(res);
    //    },


    save: function() {
        //        if (!util.validateEmpty('txtIRWithARTA')) {
        //            util.stop(util.error, null, event);
        //            util.error = null;
        //            return;
        //        }
        //        var IRWithARTA = $F('txtIRWithARTA');
        //        if (!util.isFloat(IRWithARTA)) {
        //            util.stop(util.error, null, event);
        //            util.error = null;
        //            return;
        //        }

        //        if (!util.validateEmpty('cmbVehicleStatus')) {
        //            util.stop(util.error, null, event);
        //            util.error = null;
        //            return;
        //        }


        //        var a = $F('txtIRWithoutARTA');
        //        if (!util.isFloat(a)) {
        //            util.stop(util.error, null, event);
        //            util.error = null;
        //            return;
        //        }
        debugger;
        var AutoDeviationSettings = Page.daviation;
        AutoDeviationSettings.DeviationCode = $F('txtDeviationCode');
        AutoDeviationSettings.DeviationFor = $F('cmbDaviationFor');
        AutoDeviationSettings.Description = $F('txtDescription');
        if (AutoDeviationSettings.DeviationFor == "-1") {
            alert("Please select deviation for");
            return false;
        }
        if (!util.validateEmpty('cmbDaviationFor')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtDeviationCode')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtDescription')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }

        var res = PlussAuto.DeviationSettings.SaveDeviationSettings(AutoDeviationSettings);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Deviation Settings Save successfully");
            DaviationSettingsManager.render();
            util.hideModal('DaviationPopupDiv');
        }
        if (res.value == "Already exist") {
            alert("Deviation Code Already Exist");
        }

        
    },
    DeviationSettingsDeleteById: function(DeviationId) {
        var res = PlussAuto.DeviationSettings.DeviationSettingsDeleteById(DeviationId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Deviation Settings Delete successfully");
        }

        DaviationSettingsManager.render();
    }
};
var DaviationSettingsHelper = {
    renderCollection: function(res) {
        Page.DaviationSettingArray = res.value;
        var html = [];
        for (var i = 0; i < Page.DaviationSettingArray.length; i++) {
            var serialNo = i + 1;
            var className = i % 2 ? '' : 'odd';
            Page.DaviationSettingArray[i].DeviationForName = DaviationFor[Page.DaviationSettingArray[i].DeviationFor];
            html.push('<tr class="' + className + '"><td>' + serialNo + '</td><td>#{DeviationForName}</td><td>#{Description}</td><td>#{DeviationCode}</td>\
            <td><a title="Edit" href="javascript:;" onclick="DaviationSettingsHelper.update(#{DeviationId})" class="icon iconEdit"></a> \
            <a title="Delete" href="javascript:;" onclick="DaviationSettingsHelper.remove(#{DeviationId})" class="icon iconDelete"></a>\
                            </td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.DaviationSettingArray[i])
			     );
        }
        $('tblDaviationSettings').update(html.join(''));
        if (res.value.length != 0) {
            Page.pager.reset(Page.DaviationSettingArray[0].TotalCount, Page.pageNo);
        }


    },
    update: function(deviationId) {
        $$('#DaviationPopupDiv .txt').invoke('clear');
        DaviationSettingsHelper.clearForm();

        var daviation = Page.DaviationSettingArray.findByProp('DeviationId', deviationId);
        Page.daviation = daviation || { DeviationId: util.uniqId() };
        if (daviation) {
            $('txtDescription').setValue(daviation.Description);
            $('cmbDaviationFor').setValue(daviation.DeviationFor);
            $('txtDeviationCode').setValue(daviation.DeviationCode);
        }
        else {
            $('cmbDaviationFor').setValue('-1');
        }

        util.showModal('DaviationPopupDiv');
    },
    remove: function(deviationId) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            DaviationSettingsManager.DeviationSettingsDeleteById(deviationId);
        }

    },
    clearForm: function() {
        $('txtDescription').setValue('');
        $('cmbDaviationFor').setValue('-1');
        $('txtDeviationCode').setValue('');
        //        $('txtVehicleCC').setValue('');
        //        $('txtInsurancePer').setValue('');
    },
    searchKeypress: function(e) {
        if (event.keyCode == 13) {
            DaviationSettingsManager.render();
        }
    },
    populateStatusCombo: function(res) {
        document.getElementById('cmbVehicleStatus').options.length = 0;

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].StatusName;
            opt.value = res.value[i].StatusId;
            document.getElementById('cmbVehicleStatus').options.add(opt);
        }

        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '-1';
        document.getElementById('cmbVehicleStatus').options.add(ExtraOpt);


    }

};

Event.onReady(Page.init);
