﻿var Page = {
    pageSize: 10,
    pageNo: 0,
    pager: null,
    branchAndSource: null,
    branchAndSourceArray: [],

    init: function() {
        util.prepareDom();
        Event.observe('lnkAddNewSource', 'click', branchAndSourceHelper.update);
        Event.observe('lnkSave', 'click', branchAndSourceManager.save);
        Event.observe('lnkClose', 'click', util.hideModal.curry('SourcePopupDiv'));
        Event.observe('lnkAddNewbranch', 'click', branchAndSourceHelper.newbranch);
        Event.observe('txtSearch', 'keypress', branchAndSourceHelper.searchKeypress);
        Event.observe('btnSearch', 'click', branchAndSourceManager.render);
        Page.pager = new Pager(Page.pageSize, Page.goToNextPage, {
            pager: $('Pager'),
            txtPageNo: $('txtPageNO'),
            spnTotalPage: $('SpnTotalPage'),
            nextPage: $('lnkNext'),
            prevPage: $('lnkPrev')
        });

        branchAndSourceManager.render();
        branchAndSourceManager.getAllbranchName();
    },
    goToNextPage: function(pageno) {
        Page.pageNo = pageno;
        branchAndSourceManager.render();
    }

};

var branchAndSourceManager = {

    render: function() {
        var searchtext = $F('txtSearch');
        var res = PlussAuto.BranchNameWithSource.GetAllSourceNameWithBranch(Page.pageNo, Page.pageSize, searchtext);

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        branchAndSourceHelper.renderCollection(res);
    },
    getAllbranchName: function() {
        var searchtext = $F('txtSearch');
        var res = PlussAuto.BranchNameWithSource.GetAllbranchName();

        if (res.error) {
            alert(res.error.Message);
            return;
        }
        branchAndSourceHelper.populatebranchCollection(res);
    },


    save: function() {
        
        if (!util.validateEmpty('cmbbranchName') && !util.validateEmpty('txtbranchName')) {
            alert("Branch Name is required");
            return;
        }
        if (!util.validateEmpty('txtSourceName')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        if (!util.validateEmpty('txtSourceCode')) {
            util.stop(util.error, null, event);
            util.error = null;
            return;
        }
        

        var sourceAndbranch = Page.branchAndSource;
        sourceAndbranch.AutoBranchId = $F('cmbbranchName');
        sourceAndbranch.AutoBranchName = $F('txtbranchName');
        sourceAndbranch.SourceName = $F('txtSourceName'); // dealar Type
        sourceAndbranch.SourceCode = $F('txtSourceCode');

        
        var res = PlussAuto.BranchNameWithSource.SaveAutoBranchAndSource(sourceAndbranch);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Branch and Source Settings Save successfully");
            branchAndSourceManager.getAllbranchName();
            branchAndSourceManager.render();
            util.hideModal('SourcePopupDiv');
        }
        if (res.value == "Already exist") {
            alert("Source Code already exist");
        }

       
    },
    InactiveSource: function(sourceId) {
    var res = PlussAuto.BranchNameWithSource.InactiveSourceById(sourceId);
        if (res.error) {
            alert(res.error.Message);
            return;
        }
        if (res.value == "Success") {
            alert("Source Delete successfully");
        }

        branchAndSourceManager.render();
    }
};
var branchAndSourceHelper = {
    renderCollection: function(res) {
        Page.branchAndSourceArray = res.value;
        var html = [];
        for (var i = 0; i < Page.branchAndSourceArray.length; i++) {
            var className = i % 2 ? '' : 'odd';
            html.push('<tr class="' + className + '"><td>#{AutoBranchName}</td><td>#{SourceName}</td><td>#{SourceCode}</td>\
            <td><a title="Edit" href="javascript:;" onclick="branchAndSourceHelper.update(#{AutoSourceId})" class="icon iconEdit"></a> \
            <a title="Delete" href="javascript:;" onclick="branchAndSourceHelper.remove(#{AutoSourceId})" class="icon iconDelete"></a>\
                            </td></tr>'
			         .format2((i % 2 ? '' : 'odd'), true)
			         .format(Page.branchAndSourceArray[i])
			     );
        }
        $('branchAndSourceList').update(html.join(''));
        if (res.value.length != 0) {
            Page.pager.reset(Page.branchAndSourceArray[0].TotalCount, Page.pageNo);
        }


    },
    remove: function(autoSourceId) {
        var confirmed = window.confirm("Do you want to delete this data?");
        if (confirmed) {
            branchAndSourceManager.InactiveSource(autoSourceId);
        }
    },
    update: function(autoSourceId) {
        $$('#SourcePopupDiv .txt').invoke('clear');
        branchAndSourceHelper.clearForm();
        var source = Page.branchAndSourceArray.findByProp('AutoSourceId', autoSourceId);
        Page.branchAndSource = source || { vendorMouId: util.uniqId() };
        if (source) {
            $('txtbranchName').setValue(source.AutoBranchName);
            $('txtSourceName').setValue(source.SourceName);
            $('txtSourceCode').setValue(source.SourceCode);
            $('cmbbranchName').setValue(source.AutoBranchId);
            $('txtbranchName').disabled = 'false';
            $('branchNameWithCombo').style.display = 'none';
            $('branchNameWithtextbox').style.display = 'block';

        }
        else {

            $('txtbranchName').disabled = '';
            $('branchNameWithtextbox').style.display = 'none';
            $('branchNameWithCombo').style.display = 'block';
        }
        util.showModal('SourcePopupDiv');
    },
    clearForm: function() {
        
        $('txtbranchName').setValue('');
        $('txtSourceName').setValue('');
        $('txtSourceCode').setValue('');
    },
    searchKeypress: function(e) {
        if (event.keyCode == 13) {
            branchAndSourceManager.render();
        }
    },
    newbranch: function() {
        $('branchNameWithtextbox').style.display = 'block';
        $('branchNameWithCombo').style.display = 'none';
    },
    populatebranchCollection: function(res) {
        document.getElementById('cmbbranchName').options.length = 0;
        var ExtraOpt = document.createElement("option");
        ExtraOpt.text = 'Select From List';
        ExtraOpt.value = '0';
        document.getElementById('cmbbranchName').options.add(ExtraOpt);

        for (var i = 0; i < res.value.length; i++) {
            var opt = document.createElement("option");
            opt.text = res.value[i].AutoBranchName;
            opt.value = res.value[i].AutoBranchId;
            document.getElementById('cmbbranchName').options.add(opt);
        }

    }
};

Event.onReady(Page.init);
