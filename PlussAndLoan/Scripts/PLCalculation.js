

        function JS_FunctionLabelChange()
        {
            window.onerror = handleError;
            switch(document.getElementById('ctl00_ContentPlaceHolder_plDropdownList').value)
            {
            case "1":
                document.getElementById('ctl00_ContentPlaceHolder_loanTypeLabel').innerHTML = "Loan Type";
                document.getElementById('ctl00_ContentPlaceHolder_bBPDDAllowedMaxLoanAmountLabel').innerHTML = "BB & PDD allowed max Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanAmount').innerHTML = "DBR allowable max Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountLabel').innerHTML = "Avg. Balance supported max Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_mEUAllowableMaxLoanAmountLabel').innerHTML = "MUE allowable max Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_appliedLoanAmountLabel').innerHTML = "Applied Loan Amount";
                document.getElementById('ctl00_ContentPlaceHolder_appropriatedLoanAmountLabel').innerHTML = "Appropriated Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountLabel').innerHTML = "Proposed Loan Amount";
                document.getElementById('ctl00_ContentPlaceHolder_interestRateLabel').innerHTML = "Interest Rate";
                document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountLabel').innerHTML = "Proposed Loan Amount (Including BANCA)";
                document.getElementById('ctl00_ContentPlaceHolder_dbrLabel').innerHTML = "DBR";
                document.getElementById('ctl00_ContentPlaceHolder_bancaDBRLabel').innerHTML = "BANCA DBR";
                document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRLabel').innerHTML = "DBR WITHOUT BANCA";
                document.getElementById('ctl00_ContentPlaceHolder_emilabel').innerHTML = "EMI";
                document.getElementById('ctl00_ContentPlaceHolder_propotionateMonthlyInterestLabel').innerHTML = "Propotionate Monthly Interest ";
                CashSecurityVisibleFalse();
            break;
            case "4":
                document.getElementById('ctl00_ContentPlaceHolder_loanTypeLabel').innerHTML = "Loan Type";
                document.getElementById('ctl00_ContentPlaceHolder_bBPDDAllowedMaxLoanAmountLabel').innerHTML = "BB & PDD allowed max Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanAmount').innerHTML = "DBR allowable max Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountLabel').innerHTML = "Avg. Balance supported max Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_mEUAllowableMaxLoanAmountLabel').innerHTML = "MUE allowable max Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_appliedLoanAmountLabel').innerHTML = "Applied Loan Amount";
                document.getElementById('ctl00_ContentPlaceHolder_appropriatedLoanAmountLabel').innerHTML = "Appropriated Loan amount";
                document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountLabel').innerHTML = "Proposed Loan Amount";
                document.getElementById('ctl00_ContentPlaceHolder_interestRateLabel').innerHTML = "Interest Rate";
                document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountLabel').innerHTML = "Proposed Loan Amount (Including BANCA)";
                document.getElementById('ctl00_ContentPlaceHolder_dbrLabel').innerHTML = "DBR";
                document.getElementById('ctl00_ContentPlaceHolder_bancaDBRLabel').innerHTML = "BANCA DBR";
                document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRLabel').innerHTML = "DBR WITHOUT BANCA";
                document.getElementById('ctl00_ContentPlaceHolder_emilabel').innerHTML = "EMI";
                document.getElementById('ctl00_ContentPlaceHolder_propotionateMonthlyInterestLabel').innerHTML = "Propotionate Monthly Interest ";
                CashSecurityVisibleTrue();
            break;
            case "12":
                document.getElementById('ctl00_ContentPlaceHolder_loanTypeLabel').innerHTML = "Finance Type";
                document.getElementById('ctl00_ContentPlaceHolder_bBPDDAllowedMaxLoanAmountLabel').innerHTML = "BB & PDD allowed max Finance amount";
                document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanAmount').innerHTML = "DBR allowable max Finance amount";
                document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountLabel').innerHTML="Avg. Balance supported max Finance amount";
                document.getElementById('ctl00_ContentPlaceHolder_mEUAllowableMaxLoanAmountLabel').innerHTML = "MUE allowable max Finance amount";
                document.getElementById('ctl00_ContentPlaceHolder_appliedLoanAmountLabel').innerHTML = "Applied Finance Amount";
                document.getElementById('ctl00_ContentPlaceHolder_appropriatedLoanAmountLabel').innerHTML = "Appropriated Finance amount";
                document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountLabel').innerHTML = "Proposed Finance Amount";
                document.getElementById('ctl00_ContentPlaceHolder_interestRateLabel').innerHTML = "Profit Rate ";
                document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountLabel').innerHTML = "Proposed Finance Amount (Including BANCA)";
                document.getElementById('ctl00_ContentPlaceHolder_dbrLabel').innerHTML = "FBR";
                document.getElementById('ctl00_ContentPlaceHolder_bancaDBRLabel').innerHTML = "BANCA FBR";
                document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRLabel').innerHTML = "FBR WITHOUT BANCA ";
                document.getElementById('ctl00_ContentPlaceHolder_emilabel').innerHTML = "EMR";
                document.getElementById('ctl00_ContentPlaceHolder_propotionateMonthlyInterestLabel').innerHTML = "Propotionate Monthly Finance ";
                CashSecurityVisibleFalse();
            break;            
            }

        }
        function JS_FunctionCashSecurity()
        {
	        window.onerror = handleError;
            switch(document.getElementById('ctl00_ContentPlaceHolder_plDropdownList').value)
            {
                case "1":
                    CashSecurityVisibleFalse();
                break;
                case "4":
                    CashSecurityVisibleTrue();
                break;
                case "12":
                    CashSecurityVisibleFalse();
                break;
            }
        }
        function CashSecurityVisibleTrue()
        {
	        window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_cashsecurityLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_securityDropDownList').style.visibility  = "visible";
        }
        function CashSecurityVisibleFalse()
        {
	        window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_cashsecurityLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_securityDropDownList').style.visibility  = "hidden";
        }
        function JS_FunctionBancaFieldChange()
        {
            window.onerror = handleError;
            switch(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)
            {
            case "0":
            BancaFieldVisibleFalse();
            break;
            case "1":
            BancaFieldVisibleTrue();
            break;
            case "2":
            BancaFieldVisibleFalse();
            break;
            }
        }
        function BancaFieldVisibleTrue()
        {
	        window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountTextBox').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaTextBox').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRTextBox').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaDBRLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaDBRTextBox').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaDBRPerLabel').style.visibility  = "visible";
            
            document.getElementById('ctl00_ContentPlaceHolder_bancaMUETextBox').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaMueLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_bancaMUEXLabel').style.visibility  = "visible";
        }
        function BancaFieldVisibleFalse()
        {
	        window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountTextBox').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaTextBox').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRTextBox').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaDBRLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaDBRTextBox').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaDBRPerLabel').style.visibility  = "hidden";
            
            document.getElementById('ctl00_ContentPlaceHolder_bancaMUETextBox').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaMueLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_bancaMUEXLabel').style.visibility  = "hidden";

        }
        function JS_FunctionOnePlussShow()
        {
		    window.onerror = handleError;
            switch(document.getElementById('ctl00_ContentPlaceHolder_companyTypeDropDownList').value)
            {
            case "0":
            OneAddVisibleFalse();
            break;
            case "1":
            OneAddVisibleTrue();
            break;
            case "2":
            OneAddVisibleFalse();
            break;
            }
        }
        function JS_FunctionFieldChange()
        {
	        window.onerror = handleError;
        if(document.getElementById('ctl00_ContentPlaceHolder_segmentCodeDropDownList').value>0)
        {       
                var segment=document.getElementById('ctl00_ContentPlaceHolder_segmentCodeDropDownList').value;
                switch (segment)
                {
                    case "1":
                        IncomeLevelTextChange("Monthly Salary Income ");
                        CompanyInfoVisibleTrue();
                        document.getElementById('ctl00_ContentPlaceHolder_companyNameLabel').innerHTML = "Company Name ";
                        SectorInfoVisibleFalse();
                        PercentageOfOwnershipVisibleFalse();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleFalse();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleFalse();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "2":
                        IncomeLevelTextChange("Monthly Salary Income ");
                        CompanyInfoVisibleTrue();
                        document.getElementById('ctl00_ContentPlaceHolder_companyNameLabel').innerHTML = "Company Name ";
                        SectorInfoVisibleFalse();
                        PercentageOfOwnershipVisibleFalse();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleFalse();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleFalse();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "3":
                        IncomeLevelTextChange("Monthly Salary Income ");
                        CompanyInfoVisibleTrue();
                        document.getElementById('ctl00_ContentPlaceHolder_companyNameLabel').innerHTML = "Company Name ";
                        SectorInfoVisibleFalse();
                        PercentageOfOwnershipVisibleFalse();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleFalse();                        
                        switch(document.getElementById('ctl00_ContentPlaceHolder_companyTypeDropDownList').value)
                        {
                        case "0":
                        OneAddVisibleFalse();
                        break;
                        case "1":
                        OneAddVisibleTrue();
                        break;
                        case "2":
                        OneAddVisibleFalse();
                        break;
                        }
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleFalse();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "4":
                        IncomeLevelTextChange("Monthly Salary Income ");
                        CompanyInfoVisibleTrue();
                        document.getElementById('ctl00_ContentPlaceHolder_companyNameLabel').innerHTML = "Company Name ";
                        SectorInfoVisibleFalse();
                        PercentageOfOwnershipVisibleFalse();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleFalse();
                        switch(document.getElementById('ctl00_ContentPlaceHolder_companyTypeDropDownList').value)
                        {
                        case "0":
                        OneAddVisibleFalse();
                        break;
                        case "1":
                        OneAddVisibleTrue();
                        break;
                        case "2":
                        OneAddVisibleFalse();
                        break;
                        }
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleFalse();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "5":
                        IncomeLevelTextChange("Monthly Salary Income ");
                        CompanyInfoVisibleTrue();
                        document.getElementById('ctl00_ContentPlaceHolder_companyNameLabel').innerHTML = "Company Name ";
                        SectorInfoVisibleFalse();
                        PercentageOfOwnershipVisibleFalse();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleFalse();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleFalse();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "6":
                        IncomeLevelTextChange("Monthly Turnover ");
                        CompanyInfoVisibleFalse();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleTrue();
                        AverageBalanceVisibleTrue();
                        IndustryProfitVisibleTrue();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleTrue();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "7":
                        IncomeLevelTextChange("Monthly Turnover ");
                        CompanyInfoVisibleFalse();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleTrue();
                        AverageBalanceVisibleTrue();
                        IndustryProfitVisibleTrue();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleTrue();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "8":
                        IncomeLevelTextChange("Monthly Turnover ");
                        CompanyInfoVisibleFalse();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleTrue();
                        AverageBalanceVisibleTrue();
                        IndustryProfitVisibleTrue();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleTrue();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "9":
                        IncomeLevelTextChange("Monthly Turnover ");
                        CompanyInfoVisibleFalse();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleTrue();
                        AverageBalanceVisibleTrue();
                        IndustryProfitVisibleTrue();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleTrue();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "10":
                        IncomeLevelTextChange("Monthly Turnover ");
                        CompanyInfoVisibleFalse();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleTrue();
                        AverageBalanceVisibleTrue();
                        IndustryProfitVisibleTrue();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleTrue();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "11":
                        IncomeLevelTextChange("Monthly Turnover ");
                        CompanyInfoVisibleFalse();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleTrue();
                        AverageBalanceVisibleTrue();
                        IndustryProfitVisibleTrue();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleTrue();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "12"://D1
                        IncomeLevelTextChange("Monthly Turnover ");
                        CompanyInfoVisibleTrue();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleFalse();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleTrue();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowTrue();
                        break;
                    case "13"://D2
                        IncomeLevelTextChange("Monthly Turnover ");
                        CompanyInfoVisibleTrue();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleFalse();
                        //AverageBalanceVisibleTrue();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleTrue();
                        OneAddVisibleFalse();
                        //avgBalanceSupportedMaxLoanAmountVisibleTrue();
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowTrue();
                        break;
                    case "14":
                        IncomeLevelTextChange("Monthly Rent Income ");
                        CompanyInfoVisibleFalse();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleTrue();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleFalse();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                    case "15":
                        IncomeLevelTextChange("Monthly Rent Income ");
                        CompanyInfoVisibleFalse();
                        SectorInfoVisibleTrue();
                        PercentageOfOwnershipVisibleTrue();
                        AverageBalanceVisibleFalse();
                        IndustryProfitVisibleFalse();
                        OneAddVisibleFalse();
                        avgBalanceSupportedMaxLoanAmountVisibleFalse();
                        profitMarginTextBoxVisibleTrue();
                        JS_FunctionEMIFactor();
                        CategoryShowFalse();
                        break;
                        default:
                        break;
                }
                JS_FunctionEMIFactor();
            }
        }
        function CategoryShowTrue()
        {
            document.getElementById('ctl00_ContentPlaceHolder_categoryDropDownList').style.visibility  = "visible";
        }
        function CategoryShowFalse()
        {
            document.getElementById('ctl00_ContentPlaceHolder_categoryDropDownList').style.visibility  = "hidden";
        }
        function IncomeLevelTextChange(changeText)
        {
            document.getElementById('ctl00_ContentPlaceHolder_monthlySalaryIncomeLabel').innerHTML = changeText;
        }
        function profitMarginTextBoxVisibleTrue()
        {
	        window.onerror = handleError;
	        document.getElementById('ctl00_ContentPlaceHolder_profitMarginLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_profitMarginTextBox').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_profitMarginLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_profitMarginPersientLabel').style.visibility  = "visible";
        }
        function profitMarginTextBoxVisibleFalse()
        {
		    window.onerror = handleError;
		    document.getElementById('ctl00_ContentPlaceHolder_profitMarginLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_profitMarginTextBox').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_profitMarginLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_profitMarginPersientLabel').style.visibility  = "hidden";
        }
      
        function CompanyInfoVisibleTrue()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_companyNameLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_companyNameLabel').innerHTML = "Hospital Name ";
            //companyNameLabel
            document.getElementById('ctl00_ContentPlaceHolder_companyNameDropDownList').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_companTypeLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_companyTypeDropDownList').style.visibility  = "visible";
            document.getElementById('companyInfoButton').style.visibility  = "visible";
        }
        function CompanyInfoVisibleFalse()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_companyNameLabel').style.visibility  = "hidden";            
            //companyNameLabel            
            document.getElementById('ctl00_ContentPlaceHolder_companyNameDropDownList').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_companTypeLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_companyTypeDropDownList').style.visibility  = "hidden";
            document.getElementById('companyInfoButton').style.visibility  = "hidden";
        }
        function SectorInfoVisibleTrue()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_sectorLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_sectorDropDownList').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_subSectorLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_subSectorDropDownList').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_subsectorTextBox').style.visibility  = "visible";
        }
        function SectorInfoVisibleFalse()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_sectorLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_sectorDropDownList').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_subSectorLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_subSectorDropDownList').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_subsectorTextBox').style.visibility  = "hidden";
        }
        function PercentageOfOwnershipVisibleTrue()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_percentageOfOwnershipLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_percentageOfOwnershipTextBox').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_Label3').style.visibility  = "visible";
        }
        function PercentageOfOwnershipVisibleFalse()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_percentageOfOwnershipLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_percentageOfOwnershipTextBox').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_Label3').style.visibility  = "hidden";
        }
        function IndustryProfitVisibleTrue()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_industryProfitLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_industryProfitTextBox').style.visibility  = "visible";
        }
        function IndustryProfitVisibleFalse()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_industryProfitLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_industryProfitTextBox').style.visibility  = "hidden";
        }
        function AverageBalanceVisibleTrue()
        {
            document.getElementById('ctl00_ContentPlaceHolder_averageBalanceTextBox').value="";
            document.getElementById('ctl00_ContentPlaceHolder_averageBalanceTextBox').value=document.getElementById('ctl00_ContentPlaceHolder_averageBalanceHiddenField').value;
            document.getElementById('ctl00_ContentPlaceHolder_averageBalanceLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_averageBalanceTextBox').style.visibility  = "visible";
        }
        function AverageBalanceVisibleFalse()
        {
		    window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_averageBalanceTextBox').value="";
            document.getElementById('ctl00_ContentPlaceHolder_averageBalanceLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_averageBalanceTextBox').style.visibility  = "hidden";
        }
        function OneAddVisibleTrue()
        {
	    window.onerror = handleError;
           // document.getElementById('ctl00_ContentPlaceHolder_oneAddDropDownList').style.visibility  = "visible";
        }
        function OneAddVisibleFalse()
        {
	    window.onerror = handleError;
            //document.getElementById('ctl00_ContentPlaceHolder_oneAddDropDownList').style.visibility  = "hidden";
        }
        function avgBalanceSupportedMaxLoanAmountVisibleTrue()
        {
	        window.onerror = handleError;
            document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountLabel').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountTextBox').style.visibility  = "visible";
        }
        function avgBalanceSupportedMaxLoanAmountVisibleFalse()
        {
	        window.onerror = handleError;            
            document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountLabel').style.visibility  = "hidden";
            document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountTextBox').style.visibility  = "hidden";
        }
        
        function JS_FunctionNetIncomeCalculation()
        {
	     window.onerror = handleError;
         var NetIncome;
         var monthlySalaryIncome=document.getElementById('ctl00_ContentPlaceHolder_monthlySalaryIncomeTextBox').value;
         var profitMargin=document.getElementById('ctl00_ContentPlaceHolder_profitMarginTextBox').value;
         var otherIncome=document.getElementById('ctl00_ContentPlaceHolder_otherIncomeTextBox').value;
         var percentageOfOwnership=document.getElementById('ctl00_ContentPlaceHolder_percentageOfOwnershipTextBox').value;
         var ODValue=document.getElementById('ctl00_ContentPlaceHolder_proportionateMonthlyInterestTextBox').value;
         var profitMarginResult;
         var PercentageOfOwnerShipResult;         
         if(monthlySalaryIncome!=null)
         {
            if(profitMargin>0)
            {
                profitMarginResult=parseFloat(profitMargin);
            }
            else
            {
                profitMarginResult=100;
            }
            if(percentageOfOwnership>0)
            {
                PercentageOfOwnerShipResult=parseFloat(percentageOfOwnership);
            }
            else
            {
                PercentageOfOwnerShipResult=100;
            }
            if(otherIncome>0)
            {
                otherIncome=parseFloat(otherIncome);
            }
            else
            {
                otherIncome=0;
            }
            switch(document.getElementById('ctl00_ContentPlaceHolder_segmentCodeDropDownList').value)
            {
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                ODValue=0;
                break;
                default:
                if(ODValue>0)
                {
                ODValue=parseFloat(ODValue)
                }
                else
                {
                ODValue=0;
                }
                break;
            }

            NetIncome = ((parseFloat(monthlySalaryIncome)*parseFloat(profitMarginResult)/100)*parseFloat(PercentageOfOwnerShipResult)/100)+parseFloat(otherIncome)-parseFloat(ODValue);
            document.getElementById('ctl00_ContentPlaceHolder_netIncomeTextBox').value=NetIncome;
         }
        }
        function JS_FunctionBancaCalculation()
        {
	        window.onerror = handleError;
            var ProposedLoanAmount=1;
            var Tenure=document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value;
                ProposedLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;
            var Banca;
            switch(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)
                {
                    case "0":
                        document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountTextBox').value="";
                        document.getElementById('ctl00_ContentPlaceHolder_bancaTextBox').value="";
                        document.getElementById('ctl00_ContentPlaceHolder_bancaMUETextBox').value="";
                    break;
                    case "1":
                        Banca=parseFloat(parseFloat(parseFloat(ProposedLoanAmount)*0.57*parseFloat(Tenure))/12)/100;
                        document.getElementById('ctl00_ContentPlaceHolder_bancaTextBox').value=Math.ceil(Banca);
                        document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountTextBox').value=Math.floor(parseFloat(ProposedLoanAmount)+parseFloat(Banca));
                        JS_FunctionBancaMUE();
                    break;
                    case "2":
                        document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountTextBox').value="";
                        document.getElementById('ctl00_ContentPlaceHolder_bancaTextBox').value="";
                        document.getElementById('ctl00_ContentPlaceHolder_bancaMUETextBox').value="";
                    break;

                }       
        }
        
        function JS_FunctionDBRAllowedMaxLoan()
        {
	        window.onerror = handleError;
	        var MinValue=1;
            var DBRvalue=1;
            var GridMulValue;
            var ResidenrialMulValue=0;
            var GridMultiplir=document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanTextBox2').value;
            var NetIncome=document.getElementById('ctl00_ContentPlaceHolder_netIncomeTextBox').value;
            var DeclaredIncome=document.getElementById('ctl00_ContentPlaceHolder_declaredIncomeTextBox').value;
            var ResidentialStatus;
            MinValue=Math.min(NetIncome,DeclaredIncome);
            
            switch(document.getElementById('ctl00_ContentPlaceHolder_segmentCodeDropDownList').value)
            {
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                switch(document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value)
                {
                    case "0":
                    case "1":
                        ResidentialStatus=0.25;
                    break;
                    case "2":
                        ResidentialStatus=1;
                    break;
                }
                break;
                default:
                    switch (document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value)
                    {
                        case "0":
                        case "1":
                            ResidentialStatus=0.1;
                        break;
                        case "2":
                            ResidentialStatus=1;
                        break;
                    }
                    break;
            }
            //alert(document.getElementById('ctl00_ContentPlaceHolder_segmentCodeDropDownList').value+"  "+ResidentialStatus);
            if(document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value!="2")
            {
                var resValue=Math.min(NetIncome,DeclaredIncome)*parseFloat(ResidentialStatus);
                ResidenrialMulValue=Math.min(30000,resValue);
            }
            //ResidenrialMulValue=parseFloat(MinValue)*parseFloat(ResidentialStatus);
            //var check30k=0;
            //check30k = parseFloat(ResidenrialMulValue) - parseFloat(MinValue);
            //if (check30k > 29999)
            //{
                //ResidenrialMulValue = MinValue;
            //}   
            var MaxDBR=MinValue+ResidenrialMulValue;         
            //GridMulValue=parseFloat(ResidenrialMulValue)*parseFloat(GridMultiplir)/100;
            GridMulValue=parseFloat(MaxDBR)*parseFloat(GridMultiplir)/100;
            DBRvalue=parseFloat(GridMulValue) - JS_FunctionTotalEMI();
            //alert(GridMulValue+"     " +JS_FunctionTotalEMI() +"   "+DBRvalue+"  "+ MaxDBR+" ResidenrialMulValue="+ResidenrialMulValue+" resValue= "+resValue);
            document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanTextBox1').value=Math.round(parseFloat(parseFloat(DBRvalue)/JS_FunctionEMIFactor())*1000);
            if(document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanTextBox1').value<0)
            {
                document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanTextBox1').style.color='Red';
            }
            else
            {
                document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanTextBox1').style.color='Black';
            }
            JS_FunctionMEUAllowableMaxLoanAmount();            
            JS_FunctionDBRParcent();
            JS_FunctionEMICalculation();            
            JS_FunctionMUECalculation();
            JS_FunctionBancaDBR();
            JS_FunctionAppropriatedLoanAmount();
        }
        function JS_FunctionDBRParcent()
        {
	        window.onerror = handleError;
            var NetIncome=document.getElementById('ctl00_ContentPlaceHolder_netIncomeTextBox').value;
            var DeclaredIncome=document.getElementById('ctl00_ContentPlaceHolder_declaredIncomeTextBox').value;
            var ResidentialStatus;
            var MulValue=0;
            var MinValue;
            var EMISum=1;
            var BancaEMISum=1;
            switch(document.getElementById('ctl00_ContentPlaceHolder_segmentCodeDropDownList').value)
            {
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                switch(document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value)
                {
                    case "0":
                    case "1":
                        ResidentialStatus=0.25;
                    break;
                    case "2":
                        ResidentialStatus=1;
                    break;
                }
                break;
                default:
                    switch (document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value)
                    {
                        case "0":
                        case "1":
                            ResidentialStatus=0.1;
                        break;
                        case "2":
                            ResidentialStatus=1;
                        break;
                    }
               break;
            }
            MinValue=Math.min(NetIncome,DeclaredIncome);
            if(document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value!="2")
            {
            var resValue=Math.min(NetIncome,DeclaredIncome)*parseFloat(ResidentialStatus);
            MulValue=Math.min(30000,resValue);
            }
            MulValue=MinValue+MulValue; 
            switch(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)
            {
                case "0":
                EMISum = JS_FunctionTotalEMI() + JS_FunctionEMICalculation();
                break;
                case "1":
                EMISum = JS_FunctionTotalEMI() + JS_FunctionEMICalculation();
                BancaEMISum = parseFloat(JS_FunctionTotalEMI()) + parseFloat(JS_FunctionBancaEMICalculation());
                break;
                case "2":
                EMISum = JS_FunctionTotalEMI() + JS_FunctionEMICalculation();
                break;
            }   
            var EMIDiv = (EMISum/MulValue);
            var BancaEMIDiv = (BancaEMISum/MulValue);
            var DBRParcent = parseFloat(EMIDiv)*100;
            var BancaDBRParcent = parseFloat(BancaEMIDiv)*100;          
            switch(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)
            {
                case "0":
                document.getElementById('ctl00_ContentPlaceHolder_dbrTextBox').value=round_decimals(DBRParcent,2);
                document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRTextBox').value="";
                break;
                case "1":
                document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRTextBox').value=round_decimals(BancaDBRParcent,2);
                document.getElementById('ctl00_ContentPlaceHolder_dbrTextBox').value=round_decimals(DBRParcent,2);
                break;
                case "2":
                document.getElementById('ctl00_ContentPlaceHolder_dbrTextBox').value=round_decimals(DBRParcent,2);
                document.getElementById('ctl00_ContentPlaceHolder_bancaWithoutDBRTextBox').value="";
                break;
            }         
        }
        function JS_FunctionAppropriatedLoanAmount()
        {
	        window.onerror = handleError;
            var value1=document.getElementById('ctl00_ContentPlaceHolder_averageBalanceTextBox').value;
            var percentageOfOwnership=document.getElementById('ctl00_ContentPlaceHolder_percentageOfOwnershipTextBox').value;
            var value2=document.getElementById('ctl00_ContentPlaceHolder_bBPDDAllowedMaxLoanAmountTextBox').value;
            var value3=document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanTextBox1').value;
            var value4=document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').value;
            var value5=document.getElementById('ctl00_ContentPlaceHolder_appliedLoanAmountTextBox').value;
            var appropriatedLoanAmountResult;
            if(value1>0)
            {
                if(percentageOfOwnership>0)
                {
                    document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountTextBox').value=Math.round((((value1*percentageOfOwnership)/100)/round_decimals(JS_FunctionEMIFactor(),2))*1000);
                }
                else
                {
                    document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountTextBox').value=Math.round((value1/round_decimals(JS_FunctionEMIFactor(),2))*1000);
                }                             
                appropriatedLoanAmountResult=Math.min(Math.min(Math.min(Math.min(((value1/JS_FunctionEMIFactor())*1000),value2),value3),value4),value5);
            }
            else
            {
                appropriatedLoanAmountResult=Math.min(Math.min(Math.min(value2,value3),value4),value5);
                document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountTextBox').value="";
            }
            document.getElementById('ctl00_ContentPlaceHolder_appropriatedLoanAmountTextBox').value=Math.round(appropriatedLoanAmountResult);
            JS_LoanOfferdBridge();
            if(document.getElementById('ctl00_ContentPlaceHolder_appropriatedLoanAmountTextBox').value<0)
            {
                document.getElementById('ctl00_ContentPlaceHolder_appropriatedLoanAmountTextBox').style.color='Red';
            }
            else
            {
                document.getElementById('ctl00_ContentPlaceHolder_appropriatedLoanAmountTextBox').style.color='White';
            }
            
        }
        function JS_LoanOfferdBridge()        
        {
            window.onerror = handleError;
            var comments="";
            var minLoanamount=0;
            var minLoanLesAmount=0;
            var segmentMultiplierLoanAmount=0;
            var segmentWiseMinLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_segmentWiseMinLoanAmountHiddenField').value;
            var appropriatedLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_appropriatedLoanAmountTextBox').value;
            var proposedLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;
            if(appropriatedLoanAmount=="NaN")
            {
                appropriatedLoanAmount=0;
            }
            else
            {
                appropriatedLoanAmount=appropriatedLoanAmount;
            }
            if(parseFloat(proposedLoanAmount)>0)
            {
                proposedLoanAmount=proposedLoanAmount;
            }
            else
            {
                proposedLoanAmount=0;
            }
                minLoanamount=Math.min(parseFloat(appropriatedLoanAmount),parseFloat(proposedLoanAmount));
                if(appropriatedLoanAmount < parseFloat(segmentWiseMinLoanAmount))       
                {
                    minLoanLesAmount=parseFloat(segmentWiseMinLoanAmount)-minLoanamount;
                    segmentMultiplierLoanAmount=parseFloat(parseFloat(segmentWiseMinLoanAmount)*0.2);
                    if(minLoanLesAmount>segmentMultiplierLoanAmount)
                    {
                        comments = "Loan cannot be offered, L3 required";
                    }
                    else
                    {
                        comments = "Loan cannot be offered, L2 required";
                    }
                }
                else
                {
                    comments = "";
                } 
            //alert("segmentWiseMinLoanAmount="+segmentWiseMinLoanAmount+"; \n minLoanamount="+minLoanamount+";\n minLoanLesAmount="+minLoanLesAmount+"; \n segmentMultiplierLoanAmount="+segmentMultiplierLoanAmount+"; \n comments="+comments); 
            document.getElementById('ctl00_ContentPlaceHolder_labelDevisionLabel').innerHTML = comments;              
        }
        function JS_FunctionTotalEMI()
        {
	        window.onerror = handleError;
            var TotalEMI;
            var EMIwithSCB=0;
            var OtherEMI=0;
            var InstaByInstalment=0;
            var SCCL=0;
            var InstaByBalance=0;
                EMIwithSCB=document.getElementById('ctl00_ContentPlaceHolder_emiWithSCBTextBox').value;
                OtherEMI=document.getElementById('ctl00_ContentPlaceHolder_otherEMITextBox').value;
                InstaByInstalment=document.getElementById('ctl00_ContentPlaceHolder_instabuyInstallmentTextBox').value;
                SCCL=document.getElementById('ctl00_ContentPlaceHolder_scbCreditLimitTextBox').value;
                InstaByBalance=document.getElementById('ctl00_ContentPlaceHolder_instabuyBalanceTextBox').value;
            var OD=0;
            var SCCLEMI=0;
            if(SCCL>0)
            {
                if(((SCCL-InstaByBalance)*0.03)<499)
                {
                    SCCLEMI=500;
                }
                else
                {
                    SCCLEMI=(SCCL-InstaByBalance)*0.03;
                }
            }
            switch(document.getElementById('ctl00_ContentPlaceHolder_segmentCodeDropDownList').value)
            {
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                OD=document.getElementById('ctl00_ContentPlaceHolder_proportionateMonthlyInterestTextBox').value;
                break;
            }
            TotalEMI=round_decimals(parseFloat('0'+SCCLEMI)+parseFloat('0'+EMIwithSCB)+parseFloat('0'+OtherEMI)+parseFloat('0'+InstaByInstalment)+parseFloat('0'+OD),4);
            return TotalEMI;
        }
        function JS_FunctionMaxLoanAmount()
        {
	        window.onerror = handleError;
            var PLIPLFLOS=document.getElementById('ctl00_ContentPlaceHolder_plOutstandingTextBox').value;
            if(PLIPLFLOS>0)
            {
                document.getElementById('ctl00_ContentPlaceHolder_bBPDDAllowedMaxLoanAmountTextBox').value=1000000-parseFloat(PLIPLFLOS);
            }
            else
            {
                document.getElementById('ctl00_ContentPlaceHolder_bBPDDAllowedMaxLoanAmountTextBox').value=1000000;
            }
            JS_FunctionAppropriatedLoanAmount();
        }
        function JS_FunctionMEUAllowableMaxLoanAmount()
        {
	        window.onerror = handleError;
            var Value1;
            var SmalValue;
            var GridMulValue=document.getElementById('ctl00_ContentPlaceHolder_maxLoanAmountAllowableMUETextBox').value;
            var NetIncome=document.getElementById('ctl00_ContentPlaceHolder_netIncomeTextBox').value;
            var DeclaredIncome=document.getElementById('ctl00_ContentPlaceHolder_declaredIncomeTextBox').value;
            var SCCL=document.getElementById('ctl00_ContentPlaceHolder_scbCreditLimitTextBox').value;
            var PLIPLFLOS=document.getElementById('ctl00_ContentPlaceHolder_plOutstandingTextBox').value;            
            var FlSecurity=document.getElementById('ctl00_ContentPlaceHolder_flSecurityTextBox').value;
            SmalValue=Math.min(NetIncome,DeclaredIncome);
            Value1=parseFloat(SmalValue)*parseFloat(GridMulValue)-parseFloat('0'+SCCL)-parseFloat('0'+PLIPLFLOS)+parseFloat('0'+FlSecurity);          
            
            switch(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)
            {
                case "0":
                document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').value=Math.round(Value1);
                break;
                case "1":
                document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').value=Math.round(Value1);//Math.round(BancaAllowMUE/BancaDivValue);
                break;
                case "2":
                document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').value=Math.round(Value1);
                break;
            } 
            if(document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').value<0)
            {
                document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').style.color='Red';
            }
            else
            {
                document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').style.color='Black';
            }
            JS_FunctionMUECalculation();
            JS_FunctionAppropriatedLoanAmount();           
        }

        function JS_FunctionEMIFactor()
        {
	        window.onerror = handleError;   
            var EMIFactor;
            var Intrest_rate=document.getElementById('ctl00_ContentPlaceHolder_interestRateTextBox').value;
            var Tenure=document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value;
            var IR;
           if(Intrest_rate>0 && Tenure>0)
           {
               Intrest_rate=parseFloat(Intrest_rate);
               Tenure=Tenure;
            }
            else
            {
                Intrest_rate=100;
                Tenure=1;
            }
            IR=Intrest_rate/100;
            EMIFactor= PMT(IR/12,Tenure,1000);
            document.getElementById('ctl00_ContentPlaceHolder_emiFactorTextBox').value=EMIFactor;
            return EMIFactor;
            JS_FunctionAppropriatedLoanAmount();

        }        
        function JS_FunctionEMICalculation()
        {
	        window.onerror = handleError;
           var Principal_value;
           switch(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)
            {
                case "0":
                case "2":
                Principal_value = document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;
                break;
                case "1":
                Principal_value = document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountTextBox').value;
                break;
            } 
            var Intrest_rate=document.getElementById('ctl00_ContentPlaceHolder_interestRateTextBox').value;            
            var Tenure=document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value;
            var SafthPlus=Math.round(Principal_value*0.075/100);
            var EMICalculation;
            if(Principal_value>0)
            {
                Principal_value=Principal_value;
            }
            else
            {
                Principal_value=1;
            }
            if(Intrest_rate>0 && Tenure>0 && Principal_value>0)
            {
                var IR=parseFloat(Intrest_rate)/100;
                EMICalculation= Math.ceil(PMT(IR/12,Tenure,Principal_value));
                switch(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)
                {
                    case "0":
                        EMICalculation=EMICalculation;
                        document.getElementById('ctl00_ContentPlaceHolder_emiTextBox').value=EMICalculation;
                    break;
                    case "1":
                        EMICalculation=EMICalculation;
                        document.getElementById('ctl00_ContentPlaceHolder_emiTextBox').value=EMICalculation;//+parseFloat(SafthPlus);
                    break;
                    case "2":
                        EMICalculation=EMICalculation;
                        document.getElementById('ctl00_ContentPlaceHolder_emiTextBox').value=EMICalculation;
                    break;
                }                
            }
            return EMICalculation;
        }
        function JS_FunctionBancaEMICalculation()
        {
	        window.onerror = handleError;
            var Principal_value;
            Principal_value=document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;
            var Intrest_rate=document.getElementById('ctl00_ContentPlaceHolder_interestRateTextBox').value;            
            var Tenure=document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value;
            var BancaEMICalculation;
            if(Principal_value>0)
            {
                Principal_value=Principal_value;
            }
            else
            {
                Principal_value=1;
            }
            if(Intrest_rate>0 && Tenure>0 && Principal_value>0)
            {
                var IR=Intrest_rate/100;
                BancaEMICalculation= Math.ceil(PMT(IR/12,Tenure,Principal_value));
            }
            return BancaEMICalculation;
        }
        function JS_FunctionBancaWithoutEMICalculation()
        {
	        window.onerror = handleError;
            var Principal_value;
                Principal_value=document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;;
            var Intrest_rate=document.getElementById('ctl00_ContentPlaceHolder_interestRateTextBox').value;            
            var Tenure=document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value;
            var BancaEMICalculation;
            if(Principal_value>0)
            {
                Principal_value=Principal_value;
            }
            else
            {
                Principal_value=1;
            }
            if(Intrest_rate>0 && Tenure>0 && Principal_value>0)
            {
                var IR=Intrest_rate/100;
                BancaEMICalculation= Math.ceil(PMT(Principal_value, IR/12, Tenure));
            }
            return BancaEMICalculation;
        }
        function JS_FunctionMUECalculation()
        {
	        window.onerror = handleError;
            var MinValue;
            var Result;
            var PropostLoanAmount=1;
            switch(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)
                {
                    case "0":
                    PropostLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;
                    break;
                    case "1":
                    PropostLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountTextBox').value;
                    break;
                    case "2":
                    PropostLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;
                    break;
                } 
            var NetIncome=document.getElementById('ctl00_ContentPlaceHolder_netIncomeTextBox').value;
            var DeclaredIncome=document.getElementById('ctl00_ContentPlaceHolder_declaredIncomeTextBox').value;
            var SCCL=document.getElementById('ctl00_ContentPlaceHolder_scbCreditLimitTextBox').value;
            var PLIPLFLOS=document.getElementById('ctl00_ContentPlaceHolder_plOutstandingTextBox').value;            
            var FlSecurity=document.getElementById('ctl00_ContentPlaceHolder_flSecurityTextBox').value;
            MinValue=Math.min(NetIncome,DeclaredIncome);
            //if(PropostLoanAmount>0)
            //{
                Result=parseFloat('0'+PropostLoanAmount)+parseFloat('0'+SCCL)+parseFloat('0'+PLIPLFLOS)-parseFloat('0'+FlSecurity);
            //}
            document.getElementById('ctl00_ContentPlaceHolder_mueTextBox').value=round_decimals(Result/MinValue,2);       
        }
        function JS_FunctionBancaDBR()
        {
	        window.onerror = handleError;
            var NetIncome=document.getElementById('ctl00_ContentPlaceHolder_netIncomeTextBox').value;
            var DeclaredIncome=document.getElementById('ctl00_ContentPlaceHolder_declaredIncomeTextBox').value;
            var ResidentialStatus=1;
            var MulValue=0;
            switch(document.getElementById('ctl00_ContentPlaceHolder_segmentCodeDropDownList').value)
            {
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                switch(document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value)
                {
                    case "0":
                    case "1":
                        ResidentialStatus=0.25;
                    break;
                    case "2":
                        ResidentialStatus=1;
                    break;
                }
                break;
                default:
                    switch (document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value)
                    {
                        case "0":
                        case "1":
                            ResidentialStatus=0.1;
                        break;
                        case "2":
                            ResidentialStatus=1;
                        break;
                    }
                break;
            }
            var BancaDBR;
            var BancaValue;
            var Banca=document.getElementById('ctl00_ContentPlaceHolder_bancaTextBox').value;
            var Intrest_rate=document.getElementById('ctl00_ContentPlaceHolder_interestRateTextBox').value;
            var Tenure=document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value;
            var Multiplir=document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanTextBox2').value;
            if(Banca>0)
            {
                if(Intrest_rate>0 && Tenure>0)
                {
                    var IR=Intrest_rate/100;
                    BancaValue=PMTForBancaDBR(IR/12,Tenure,Banca);
                }
            }
            var MinValue=Math.min(NetIncome,DeclaredIncome);
//---------------------------------------------------------
            if(document.getElementById('ctl00_ContentPlaceHolder_residentialStatusDropDownList').value!="2")
            {
                var resValue=Math.min(NetIncome,DeclaredIncome)*parseFloat(ResidentialStatus);
                MulValue=Math.min(30000,resValue);
            }
            MulValue=MinValue+MulValue; 
//---------------------------------------------------------
            var MulValue2=(MulValue*Multiplir)/100;
            BancaDBR=(parseFloat(BancaValue)/MulValue2)*100;
            document.getElementById('ctl00_ContentPlaceHolder_bancaDBRTextBox').value=round_decimals(BancaDBR,2);//Math.round(parseFloat(BancaDBR)/MulValue);
        
        }
        function JS_FunctionBancaMUE()
        {
            var subValue=0;
            var BancaMUE=0;
            var NetIncome=document.getElementById('ctl00_ContentPlaceHolder_netIncomeTextBox').value;
            var DeclaredIncome=document.getElementById('ctl00_ContentPlaceHolder_declaredIncomeTextBox').value;            
            var mUEAllowableMaxLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').value;
            var proposedLoanAmountIncludeBanca=document.getElementById('ctl00_ContentPlaceHolder_bancaLoanAmountTextBox').value;
            
            if(parseFloat(proposedLoanAmountIncludeBanca)>parseFloat(mUEAllowableMaxLoanAmount))
            {
                subValue=parseFloat(proposedLoanAmountIncludeBanca)-parseFloat(mUEAllowableMaxLoanAmount);
                BancaMUE=subValue/Math.min(NetIncome,DeclaredIncome);
            }
            document.getElementById('ctl00_ContentPlaceHolder_bancaMUETextBox').value=round_decimals(BancaMUE,2);

        }
        function FV(IR, NP, PMT)
        {
            var vFV = PMT * (Math.pow(1 + IR, NP) - 1) / IR;
            return vFV;
        }
        function PMT(IR, NP, PV)
        {
              var vPMT = (PV * IR) / (1 - Math.pow(1 + IR, -NP));
              return round_decimals(vPMT,10);
        }
        function PMTForBancaDBR(IR, NP, PV)
        {
              var vPMT = (PV * IR) / (1 - Math.pow(1 + IR, -NP));
              return round_decimals(vPMT,4);
        }
        function PV(IR, NP, PMT)
        {
              var vPV = PMT * (1 - Math.pow(1 + IR, -NP)) / IR;
              return vPV;
        }
        function round_decimals(original_number, decimals)
        {
            var result1 = original_number * Math.pow(10, decimals);
            var result2 = Math.round(result1);
            var result3 = result2 / Math.pow(10, decimals);
            return result3;
        }
        function JS_FunctionODCalculation()
        {
	        window.onerror = handleError;
            var resultQ1;
            var resultQ2;
            var resultQ3;
            var ODResult;
            var Ownership=document.getElementById('ctl00_ContentPlaceHolder_percentageOfOwnershipTextBox').value;

            var Q11=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl02_q1TextBox').value;
            var Q12=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl02_q2TextBox').value;
            var Q13=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl02_q3TextBox').value;
            var Q14=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl02_q4TextBox').value;
            
            var Q21=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl03_q1TextBox').value;
            var Q22=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl03_q2TextBox').value;
            var Q23=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl03_q3TextBox').value;
            var Q24=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl03_q4TextBox').value;
            
            var Q31=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl04_q1TextBox').value;
            var Q32=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl04_q2TextBox').value;
            var Q33=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl04_q3TextBox').value;
            var Q34=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl04_q4TextBox').value;
            
            var Limit1=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl02_limitTextBox').value;
            var rat1=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl02_rateTextBox').value;
            
            var Limit2=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl03_limitTextBox').value;
            var rat2=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl03_rateTextBox').value;
            
            var Limit3=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl04_limitTextBox').value;
            var rat3=document.getElementById('ctl00_ContentPlaceHolder_overDraftGridView_ctl04_rateTextBox').value;
            
         if(Q11!=null ||Q21!=null || Q31 !=null || Q12!=null || Q13!=null || Q22!=null || Q23!=null || Q32!=null || Q33 !=null || Q34!=null || Q24!=null || Q14)
         {
            document.getElementById('ctl00_ContentPlaceHolder_proportionateMonthlyInterestTextBox').style.visibility  = "visible";
            document.getElementById('ctl00_ContentPlaceHolder_proportionateMonthlyInterestTextBox').value=0;
            var Q1=(parseFloat('0'+Q11) + parseFloat('0'+Q12) + parseFloat('0'+Q13) + parseFloat('0'+Q14))/12;
            var Q2=(parseFloat('0'+Q21) + parseFloat('0'+Q22) + parseFloat('0'+Q23) + parseFloat('0'+Q24))/12;
            var Q3=(parseFloat('0'+Q31) + parseFloat('0'+Q32) + parseFloat('0'+Q33) + parseFloat('0'+Q34))/12;

            var LimitQ1=(parseFloat('0'+Limit1)*parseFloat('0'+rat1)/100)/12;
            var LimitQ2=(parseFloat('0'+Limit2)*parseFloat('0'+rat2)/100)/12;
            var LimitQ3=(parseFloat('0'+Limit3)*parseFloat('0'+rat3)/100)/12;
            
            if(Q1>0 )
            {
                resultQ1=Q1;
            }
            else
            {
                resultQ1=LimitQ1;
            }
            if( Q2>0)
            {
                resultQ2=Q2;
            }
            else
            {
                resultQ2=LimitQ2;
            }
            if(Q3>0)
            {
                resultQ3=Q3;
            }
            else
            {
                resultQ3=LimitQ3;
            }
            if(Ownership>0)
            {
                document.getElementById('ctl00_ContentPlaceHolder_proportionateMonthlyInterestTextBox').value = round_decimals(((resultQ1+resultQ2+resultQ3)*parseFloat('0'+Ownership)/100),4);
            }
            else
            {
                document.getElementById('ctl00_ContentPlaceHolder_proportionateMonthlyInterestTextBox').value = round_decimals((resultQ1+resultQ2+resultQ3),4);
            }
         }
         else
         {
            document.getElementById('ctl00_ContentPlaceHolder_proportionateMonthlyInterestTextBox').style.visibility  = "hidden";
         }
         JS_FunctionNetIncomeCalculation();
         JS_FunctionDBRAllowedMaxLoan();
         JS_FunctionMUECalculation();
        }
        function JS_FunCkeckLoanAmount()
        {
	        window.onerror = handleError;
			if(parseInt(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value)==1)
            {
				var existingLoan=document.getElementById('ctl00_ContentPlaceHolder_plOutstandingTextBox').value;
				var security=0;//document.getElementById('ctl00_ContentPlaceHolder_flSecurityTextBox').value;
				var propostLoanAmunt=document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;
				var Tenure=document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value;
				var FirstValue=0;
				var SecondValue=0;
				var sugestLoanAmount=0;
					FirstValue=(1000000-(parseFloat('0'+existingLoan)+parseFloat('0'+security)))*100;
					var banca=round_decimals((0.57*parseInt(Tenure))/12,2);
					SecondValue=100+banca;
					sugestLoanAmount=round_decimals(parseFloat(FirstValue)/parseFloat(SecondValue),0);        
				if(parseFloat(sugestLoanAmount)<parseFloat(propostLoanAmunt))
				{
					alert("Loan amount cannot be more then Tk 10 LAC \nBanca Supported Max Loan Amount would be Tk "+ sugestLoanAmount +" \nInput this Loan Amount ?");
					document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value=sugestLoanAmount;
					JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();
					return false;
				}
				else
				{
					return true;
				} 
			}
        }
        function JS_FunctionCheckMinLoanAmount()
        {
            var minLoanAmount=0;
            var comments="";
            var avgBalanceSupportedMaxLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountTextBox').value;
            var DBPDDAllowedMaxLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_bBPDDAllowedMaxLoanAmountTextBox').value;
            var DBRAllowableMaxLoan=document.getElementById('ctl00_ContentPlaceHolder_dBRAllowableMaxLoanTextBox1').value;
            var MEUAllowableMaxLoanAmount=document.getElementById('ctl00_ContentPlaceHolder_mUEAllowableMaxLoanAmountTextBox').value;
            
            var propostLoanAmunt=document.getElementById('ctl00_ContentPlaceHolder_proposedLoanAmountTextBox').value;

            if(avgBalanceSupportedMaxLoanAmount>0)
            {
                 minLoanAmount=Math.min(Math.min(Math.min(DBPDDAllowedMaxLoanAmount,DBRAllowableMaxLoan),MEUAllowableMaxLoanAmount),avgBalanceSupportedMaxLoanAmount);
            }
            else
            {
                 minLoanAmount=Math.min(Math.min(DBPDDAllowedMaxLoanAmount,DBRAllowableMaxLoan),MEUAllowableMaxLoanAmount);            
            }
            if(propostLoanAmunt>minLoanAmount)
            {
                alert("Loan amount cannot be more then = Tk "+minLoanAmount);
            }
            else
            {
                
            }
        }

	    function JS_FunctionShowCompanyInfo(obj)
	    {
	    	window.onerror = handleError;
	        var companyId;
	       // if(document.getElementById('ctl00_ContentPlaceHolder_companyIdHiddenField').value!="" || document.getElementById('ctl00_ContentPlaceHolder_companyNameDropDownList').value >0)
	       if(document.getElementById('ctl00_ContentPlaceHolder_companyNameDropDownList').value!="" || document.getElementById('ctl00_ContentPlaceHolder_companyNameDropDownList').value >0)
	        {
	            companyId = document.getElementById('ctl00_ContentPlaceHolder_companyNameDropDownList').value;
	            if(companyId>0)
	            {
	                ajax_showCompanyInfo(window.event,'../UI/CompanyInfoTooltrip.aspx?companyId='+parseInt(companyId),obj);
	            }
	        }
	        else
	        {
	            alert("Not found company name");
	        }
	    }
	function JS_FunctionMechodChange()
	{
	    if(document.getElementById('ctl00_ContentPlaceHolder_incAssmntMethodDropDownList').value==7)
	    {
	        document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountLabel').innerHTML = "Previous EMI supported max loan amount";
	        document.getElementById('ctl00_ContentPlaceHolder_averageBalanceLabel').innerHTML = "Previous EMI Amount";
	    }
	    else
	    {
	    	document.getElementById('ctl00_ContentPlaceHolder_avgBalanceSupportedMaxLoanAmountLabel').innerHTML = "Avg. Balance supported max Loan amount";
	        document.getElementById('ctl00_ContentPlaceHolder_averageBalanceLabel').innerHTML = "50% of Average Balance";
	    }
	}
	function JS_FunctionCheckAgeForBanca()
	{
	    var TenorAge=0;
	    var TotalAge=0;
	    var Age=document.getElementById('ctl00_ContentPlaceHolder_ageHF').value;
	    var Tenure=document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value;
	    if(document.getElementById('ctl00_ContentPlaceHolder_tenureTextBox').value!="" && parseInt('0'+Age)>1)
	      {
	       TenorAge=parseInt(Tenure/12);	
	       TotalAge=parseInt(Age)+parseInt(TenorAge);       
	        if(document.getElementById('ctl00_ContentPlaceHolder_safetyPlusDropDownList').value=="1")
            {
               if(parseInt(TotalAge)>66)
               {
                  alert("Customer Age Grater then 65 \nAge="+Age+" \nTenorAge="+TenorAge+"\nTotalAge="+TotalAge);
               }
            }
	        
	      }
	    
	}    
	function handleError (err, url, line)
	{
		alert ('Oops, something went wrong.\n' +
			 'Please dont wary go on.... \n' + 
			 'Error text: ' + err + '\n' + 
			 'Location  : ' + url + '\n' +
			 'Line no   : ' + line + '\n');
		 return true; // let the browser handle the error */
	}
