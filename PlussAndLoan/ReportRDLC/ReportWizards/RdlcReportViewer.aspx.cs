﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using PlussAndLoan.ReportRDLC.DataSet;

namespace PlussAndLoan.ReportRDLC.ReportWizards
{
    public partial class RdlcReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var path = Session["path"].ToString();
                var data = (object)(Session["rdlcData"]);
                string reportDataSourceName = Session["ReportDataSourceName"].ToString();

                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath(path);
                //ReportDataSource datasource = new ReportDataSource("UserList", data);
                ReportDataSource datasource = new ReportDataSource(reportDataSourceName, data);

                var ps = Session["UserId"].ToString();
                var name = Session["UserName"].ToString();
                ReportParameter rp = new ReportParameter("user", name + " (" + ps + " )");
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp });
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
        }
    }
}