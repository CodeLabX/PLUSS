﻿using System;
using System.Drawing;
using Bat.Common;
using WebConfigEncryption;

namespace LAMS.Web.UI
{
    public partial class WebConfigEncryption : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetExistConnectionString();
            }
        }

        protected void ddlConnectionName_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetExistConnectionString();
        }

        protected void txtConfigure_Click(object sender, EventArgs e)
        {

            string server = txtServerName.Value;
            string database = txtDatabaseName.Value;
            string user = txtDbUserId.Value;
            string password = txtDbPassword.Value;
            string conPass = txtConPassword.Value;
            if (conPass != password)
            {
                lblMessage.Text = "Password mis match, Try again";
                return;

            }
            if (string.IsNullOrEmpty(server) || string.IsNullOrEmpty(database) ||
                string.IsNullOrEmpty(user) | string.IsNullOrEmpty(password))
            {
                lblMessage.Text = "Input field left blunk.";
            }
            else
            {
                ConstringManager configMgr = new ConstringManager(Request.ApplicationPath, ddlConnectionName.Text);
                int IsConfig = configMgr.SetConnectionString(server, database, user, password, "2800");
                if (IsConfig == 1)
                {
                    string path = Request.ApplicationPath;
                    ConnectionStringManager manager = new ConnectionStringManager(path);
                    manager.EncryptConstring("conString");
                    lblMessage.Text = "Web Config configured successfully!";
                    lblMessage.ForeColor = Color.Green;
                }

            }
        }

        private void GetExistConnectionString()
        {

            ConnectionPropertie conn = Connection.GetConnectionProp(ddlConnectionName.SelectedValue);
            txtServerName.Value = conn.ServerName;
            txtDatabaseName.Value = conn.DatabaseName;
            txtDbUserId.Value = conn.UserId;
            txtDbPassword.Value = "";

        }
    }
}
