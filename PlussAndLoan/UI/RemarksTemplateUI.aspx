﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_RemarksTemplateUI" Title="Remarks Template" Codebehind="RemarksTemplateUI.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">

<div style="margin-top:30px; margin-left:10px">
<table width="100%" cellpadding="0" cellspacing="0">
<tr style="font-weight:bold">
<td align="center" style="font-size:16px">
Remarks/Condition Template
</td>
</tr>
<tr style="font-weight:bold">
<td>
&nbsp;
</td>
</tr>
<tr align="center">
<td>
    <table>
    <tr>
    <td style="width:100px; padding-right:10px" align="right">Template Type</td>
    <td align="left">
        <asp:DropDownList ID="cmbTempleteType" runat="server" Height="20px" Width="201px">
            <asp:ListItem Value="1">Remarks</asp:ListItem>
            <asp:ListItem Value="2">Condition</asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td style="width:100px; padding-right:10px" align="right"></td>
    <td align="left">
        <asp:HiddenField ID="remarksIdHiddenField" runat="server" />
    </td>
    </tr>
    <tr>
    <td style="width:100px; padding-right:20px" align="right" valign="top">Comments: </td>
    <td align="left">
        <asp:TextBox ID="remarksTextBox" runat="server" Width="350px" 
            TextMode="MultiLine"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td style="width:100px; padding-right:20px" align="right">Templete For: </td>
    <td align="left">
        <asp:DropDownList ID="remarksForDropDownList" runat="server" Width="200px"></asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td align="right" style="padding-right:20px">
        <asp:Label ID="activeLabel" runat="server" Text="Active: "></asp:Label>
    </td>
    <td align="left">
        <asp:DropDownList id="statusDropDowonList" runat="server">
        <asp:ListItem>Active</asp:ListItem>
        <asp:ListItem>Inactive</asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    </table>
</td>
</tr>
<tr align="center">
<td>
    <asp:Button ID="saveButton" runat="server" Text="Save" Width="100px" 
        Height="27px" onclick="saveButton_Click" />
    <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" 
        Height="27px" onclick="clearButton_Click" />
</td>
</tr>
<tr>
<td>&nbsp</td>
</tr>
<tr>
<td style="padding-left:2px">
    <asp:TextBox ID="searchTextBox" runat="server" Width="150px"></asp:TextBox>
    &nbsp
    <asp:Button ID="searchbutton" runat="server" Text="Search" Width="65px" 
        onclick="searchbutton_Click" />
</td>
</tr>
<tr>
    <td colspan="3" style="padding-left:2px">
     <div id="gridbox"  style="background-color:white; width:780px; height:285px;"></div>   
    </td>
</tr>
</table>
    <div style="display: none" runat="server" id="remarksDiv"></div>
</div>
<script type="text/javascript">
	    var mygrid;
	    mygrid = new dhtmlXGridObject('gridbox');
	    mygrid.setImagePath("../codebase/imgs/");
	    mygrid.setHeader("SL.No.,Type,'',Templete For,Comments,Status,''");
	    mygrid.setInitWidths("50,100,0,130,400,80,0");
	    mygrid.setColAlign("right,left,left,left,left,left,left");
	    mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	    mygrid.setColSorting("int,int,str,int,str,str,int");
	    //mygrid.setSkin("modern");
	    mygrid.setSkin("light");	 
	    mygrid.init();
	    mygrid.enableSmartRendering(true,20);
    // mygrid.loadXML("../includes/RemarksList.xml");
	    mygrid.parse(document.getElementById("xml_data"));
	    mygrid.attachEvent("onRowSelect",DoOnRowSelected);
        function DoOnRowSelected()
        {
            if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0) {
              document.getElementById('ctl00_ContentPlaceHolder_saveButton').value="Update";
              document.getElementById('ctl00_ContentPlaceHolder_cmbTempleteType').value = mygrid.cells(mygrid.getSelectedId(),6).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_remarksTextBox').value = mygrid.cells(mygrid.getSelectedId(),4).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_remarksForDropDownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(), 2).getValue());
              document.getElementById('ctl00_ContentPlaceHolder_statusDropDowonList').value = mygrid.cells(mygrid.getSelectedId(), 5).getValue();
             }
             else
             {
              document.getElementById('ctl00_ContentPlaceHolder_saveButton').value="Save";
             }
        }
	</script>
</asp:Content>
