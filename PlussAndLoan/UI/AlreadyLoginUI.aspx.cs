﻿using System;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class AlreadyLoginUI : System.Web.UI.Page
    {
        enum USERTYPE : short
        {
            Admin = 1,
            Analyst = 2,
            SupportUser = 3,
            Approver = 4,
            ScoreAdmin =5
        }
        enum ERRORMSGTYPE : short
        {
            DatabaseFail = 1,
            InvalidUserId = 2,
            InvalidPassword = 3
        }

        UserManager userManagerObj;
        private UserAccessLogManager userAccessLogManagerObj = null;
        private UserInfoHistoryManager userInfoHistoryManagerObj = null;
        private UserAccessLog userAccessLogObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            userAccessLogManagerObj = new UserAccessLogManager();
            userInfoHistoryManagerObj = new UserInfoHistoryManager();
            Page.Form.DefaultFocus = userIdTextBox.ClientID;
            Bat.Common.Connection.ConnectionStringName = "conString";
            Bat.Common.Connection.dateFormat = "yyyy-MM-dd";
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count != 0)
                {
                    userIdTextBox.Text = Convert.ToString(Request.QueryString["userid"]);
                    passwordTextBox.Text = Convert.ToString(Request.QueryString["userName"]);
                    conMsgLabel.Text = "This user already login "+ System.Environment.NewLine +"do you wand to login ?";
                }
            }
        }
    
        private void SaveAccessLog(string loginId,int successStatus,int unSuccessStatus,int userLoginStatus)
        {
            int insertAccessLog = 0;
            userAccessLogObj = new UserAccessLog();
            userAccessLogObj.UserLogInId = loginId.ToString();
            userAccessLogObj.IP = Request.UserHostAddress.ToString(); //Request.ServerVariables["REMOTE_ADDR"].ToString();
            userAccessLogObj.LogInCount = 0;
            userAccessLogObj.LogInDateTime = DateTime.Now;
            userAccessLogObj.userLoginSuccessStatus = successStatus;
            userAccessLogObj.userLoginUnSuccessStatus = unSuccessStatus;
            userAccessLogObj.userLoginStatus = userLoginStatus;
            userAccessLogObj.AccessFor = "LOGIN";
            insertAccessLog = userAccessLogManagerObj.SendDataInToDB(userAccessLogObj);
        }

        protected void noButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/LoginUI.aspx");
        }
        protected void yesButton_Click(object sender, EventArgs e)
        {
            new AT(this).AuditAndTraial("Re-login","");
            SaveAccessLog(userIdTextBox.Text, 0, 0, 0);
            Response.Redirect("~/LoginUI.aspx");
        }
    }
}
