﻿using AzUtilities;
using BLL;
using BusinessEntities;
using DAL;
using DAL.SecurityMatrixGateways;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI
{
    public partial class PageAccessDenialMaster : System.Web.UI.MasterPage
    {
        private UserAccessLogManager userAccessLogManagerObj = null;
        private PageAccessPermission _permissions;

        protected void Page_Load(object sender, EventArgs e)
        {
            userAccessLogManagerObj = new UserAccessLogManager();

            var loginId = (string)Session["UserId"];
            //userIdHiddenField.Value = loginId;

            _permissions = new PageAccessPermission();
            userIdHiddenField.Value = (string)Session["UserId"];

            User userT = (User)Session["UserDetails"];

            //if (!_permissions.HasUserAccess(Context, userT))
            //    Response.Redirect("~/UI/PageAccessDenied.aspx");

            try
            {
                DataTable dt = _permissions.BindMenuData(0, userT);
                _permissions.DynamicMenuControlPopulation(dt, 0, null, MenuSpace, userT);
            }
            catch (Exception)
            {
                Response.Redirect("~/LoginUI.aspx");
            }
        }
        protected void logout_Click(object sender, EventArgs e)
        {
            var userId = Session["UserId"].ToString();
            LoginUser.LoginUserToSystem.RemoveAll(u => u.UserId == userId);

            SaveAccessLog(userIdHiddenField.Value, 0, 0, 0);
            new AT(this).AuditAndTraial("Logout", "Logout");
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            removeCookie();
            Response.Redirect("~/LoginUI.aspx");
        }
        private void SaveAccessLog(string loginId, int successStatus, int unSuccessStatus, int userLoginStatus)
        {
            int insertAccessLog = 0;
            UserAccessLog userAccessLogObj = new UserAccessLog();
            userAccessLogObj.UserLogInId = loginId.ToString();
            userAccessLogObj.IP = System.Net.Dns.GetHostName(); ;
            userAccessLogObj.LogInCount = 0;
            //userAccessLogObj.LogInDateTime = DateTime.Now;
            userAccessLogObj.LogOutDateTime = DateTime.Now;
            userAccessLogObj.userLoginSuccessStatus = 0;
            userAccessLogObj.userLoginUnSuccessStatus = 0;
            userAccessLogObj.userLoginStatus = 0;
            userAccessLogObj.AccessFor = "LOGOUT";
            insertAccessLog = userAccessLogManagerObj.SendDataInToDB(userAccessLogObj);
        }

        private void removeCookie()
        {
            HttpCookie myCookie = new HttpCookie("LOGIN");
            Response.Cookies["LOGIN"].Expires = DateTime.Now.AddDays(-365);

            string currentCookieValue = "";
            if (Response.Cookies["LOGIN"].Value == "" && Response.Cookies["LOGIN"].Expires == DateTime.MinValue)
            {
                currentCookieValue = Request.Cookies["LOGIN"].Value;
                Response.Cookies.Remove("LOGIN");
            }
            else
            {
                myCookie = new HttpCookie("LOGIN");
                myCookie.Values.Add("LoginId", "");
                myCookie.Values.Add("status", "0");
                currentCookieValue = Response.Cookies["LOGIN"].Value;
            }
        }
    }
}