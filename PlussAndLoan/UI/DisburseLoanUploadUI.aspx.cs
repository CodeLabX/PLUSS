﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Web.UI;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_DisburseLoanUploadUI : System.Web.UI.Page
    {
        List<DeDupInfo> deDupInfoObjList = null;
        DeDupInfoManager dedupeManagerObj = null;
        ProductManager productManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            deDupInfoObjList = new List<DeDupInfo>();
            dedupeManagerObj = new DeDupInfoManager();
            dateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            try
            {
                string stype = string.Empty;
                string noOfRecord = string.Empty;
                string lowIndex = string.Empty;
                string strOperation = string.Empty;
                string strsearchKey = string.Empty;
                string qDate = string.Empty;
                strOperation = Convert.ToString(Request.QueryString["operation"]);
                noOfRecord = Convert.ToString(Request.QueryString["NoofRecord"]);
                lowIndex = Convert.ToString(Request.QueryString["LowIndex"]);
                strsearchKey = Convert.ToString(Request.QueryString["SEARCHKEY"]);
                qDate = Convert.ToString(Request.QueryString["QUERYDATE"]);
                if (strOperation == "upload")
                {
                    StreamReader streamReader = new StreamReader(Request.InputStream);
                    string fileNameAndPath = streamReader.ReadToEnd();
                    this.Response.Write(UploadExcelFile(fileNameAndPath));
                    this.Response.Flush();
                    this.Response.Close();
                    this.Response.End();

                }
                else if (strOperation == "save")
                {
                    if (!string.IsNullOrEmpty(noOfRecord))
                    {
                        this.Response.Write(SaveUploadData(Convert.ToInt32(noOfRecord), Convert.ToInt32(lowIndex)));
                        this.Response.Flush();
                        this.Response.Close();
                        this.Response.End();
                    }
                }
                else if (strOperation == "LoadUploadData" && strsearchKey != "")
                {
                    this.Response.Write(LoadUploadData(strsearchKey, qDate));
                    this.Response.Flush();
                    this.Response.Close();
                    this.Response.End();
                }

            }

            catch (Exception ex)
            {
                CustomException.Save(ex, "Load Disburse Loan");
                this.Response.Write("error occured" + ex.Message);
                this.Response.Flush();
                this.Response.Close();
                this.Response.End();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();", true);

        }
        protected void ButtonUploadFile_Click(object sender, System.EventArgs e)
        {

            if (FileUpload.HasFile)
            {
                try
                {
                    string fileName = "";
                    string path = Request.PhysicalApplicationPath;
                    path += @"Uploads\";
                    fileName = Server.HtmlEncode(FileUpload.FileName);
                    path += fileName;
                    FileUpload.SaveAs(path);
                    Session["fileName"] = path;
                    LabelUpload.Text = "Upload File Name: " +
                                       FileUpload.PostedFile.FileName + "<br>" +
                                       "Type: " + FileUpload.PostedFile.ContentType +
                                       " File Size: " + FileUpload.PostedFile.ContentLength +
                                       " kb<br>";
                }
                catch (System.NullReferenceException ex)
                {
                    LabelUpload.Text = "Error: " + ex.Message;
                    CustomException.Save(ex, "Upload Disburse Loan");
                }
                new AT(this).AuditAndTraial("Bank Information", LabelUpload.Text);
            }
            else
            {
                LabelUpload.Text = "Please select a file to upload.";
            }


        }
    

        private string UploadExcelFile(string fileNameAndPath)
        {

            string returnValu = string.Empty;
            long slNo = 0;
            if (string.IsNullOrEmpty(Convert.ToString(Session["fileName"])))
            {
                returnValu = "Please select file name #";
            }

            try
            {

                string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source=" + (Session["fileName"].ToString() + (";" + "Extended Properties=\"Excel 8.0;\""))));
                DataTable dataTableObj = new DataTable();
                OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);
                dataAdapterObj.Fill(dataTableObj);
                foreach (DataRow dr in dataTableObj.Rows)
                {
                    DeDupInfo deDupInfoObj = new DeDupInfo();
                    if (Convert.ToString(dr[0]) != "")
                    {
                        deDupInfoObj.Name = dr[0].ToString();
                        deDupInfoObj.FatherName = dr[2].ToString();
                        deDupInfoObj.MotherName = dr[3].ToString();
                        if (!String.IsNullOrEmpty(dr[4].ToString()))
                        {
                            deDupInfoObj.DateOfBirth = Convert.ToDateTime(dr[4].ToString());
                        }
                        else
                        {
                            deDupInfoObj.DateOfBirth = DateTime.Now;
                        }
                        deDupInfoObj.ScbMaterNo = dr[1].ToString();
                        deDupInfoObj.DeclineReason = dr[5].ToString();
                        if (!String.IsNullOrEmpty(dr[6].ToString()))
                        {
                            deDupInfoObj.DeclineDate = Convert.ToDateTime(dr[6].ToString());
                        }
                        else
                        {
                            deDupInfoObj.DeclineDate = DateTime.Now;
                        }
                        deDupInfoObj.BusinessEmployeeName = dr[7].ToString();
                        deDupInfoObj.Address1 = dr[8].ToString();
                        deDupInfoObj.Address2 = dr[9].ToString();
                        deDupInfoObj.Phone1 = dr[10].ToString();
                        deDupInfoObj.Phone2 = dr[11].ToString();
                        deDupInfoObj.Mobile1 = dr[12].ToString();
                        deDupInfoObj.Mobile2 = dr[13].ToString();
                        if (!String.IsNullOrEmpty(dr[14].ToString()))
                        {
                            deDupInfoObj.ProductId = dedupeManagerObj.GetProductId(dr[14].ToString());
                        }
                        else
                        {
                            deDupInfoObj.ProductId = 0;
                        }

                        deDupInfoObj.BankBranch = dr[15].ToString();
                        deDupInfoObj.Source = dr[16].ToString();
                        deDupInfoObj.Status = criteriaDropDownList.Text;//dr[17].ToString();
                        deDupInfoObj.Remarks = dr[18].ToString();
                        if (!String.IsNullOrEmpty(dr[19].ToString()))
                        {
                            deDupInfoObj.Tin = dr[19].ToString();
                        }
                        deDupInfoObj.IdType = dr[20].ToString();
                        deDupInfoObj.IdNo = dr[21].ToString();
                        deDupInfoObj.IdType1 = dr[22].ToString();
                        deDupInfoObj.IdNo1 = dr[23].ToString();
                        deDupInfoObj.IdType2 = dr[24].ToString();
                        deDupInfoObj.IdNo2 = dr[25].ToString();
                        deDupInfoObj.IdType3 = dr[26].ToString();
                        deDupInfoObj.IdNo3 = dr[27].ToString();
                        deDupInfoObj.IdType4 = dr[28].ToString();
                        deDupInfoObj.IdNo4 = dr[29].ToString();
                        deDupInfoObj.ResidentaileStatus = dr[30].ToString();
                        deDupInfoObj.EducationalLevel = dr[31].ToString();
                        deDupInfoObj.MaritalStatus = dr[32].ToString();
                        deDupInfoObj.LoanAccountNo = dr[33].ToString();
                        deDupInfoObj.Active = "Y";
                        deDupInfoObj.CriteriaType = dr[17].ToString();
                        deDupInfoObj.EntryDate = DateTime.Now;
                        deDupInfoObjList.Add(deDupInfoObj);
                        slNo = slNo + 1;
                    }
                }
                Session["dedupInfoListS"] = deDupInfoObjList;
                Session["fileName"] = null;
                returnValu = deDupInfoObjList.Count.ToString() + "#";
            }
            catch (Exception ex)
            {
                returnValu = ex.Message + "#";
            }
            return returnValu;
        }
        private string SaveUploadData(Int32 noOfRecord, Int32 lowIndex)
        {
            string returnValu = string.Empty;
            Int32 count = 0;
            Int32 noOfRecordCount = 0;
            List<DeDupInfo> deDupeObjList = new List<DeDupInfo>();
            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            deDupInfoObjList = (List<DeDupInfo>)Session["dedupInfoListS"];
            //recourCountHiddenField.Value = "";
            if (deDupInfoObjList.Count > 0)
            {
                dedupeManagerObj.DeleteTempData(deDupInfoObjList[0].Status);

                Int32 modResult = Convert.ToInt32(deDupInfoObjList.Count % 100);
                for (Int32 Index = 0; Index < deDupInfoObjList.Count; Index++)
                {
                    deDupeObjList.Add(deDupInfoObjList[Index]);
                    count++;
                    if (count >= 100)
                    {
                        dedupeManagerObj.SendDedupeInToDB(deDupeObjList);
                        deDupeObjList.Clear();
                        count = 0;
                        Session["uploadCount"] = noOfRecordCount.ToString();
                    }
                    noOfRecordCount++;
                }
                if (modResult > 0)
                {
                    dedupeManagerObj.SendDedupeInToDB(deDupeObjList);
                    deDupeObjList.Clear();
                    count = 0;
                    Session["uploadCount"] = noOfRecordCount.ToString();
                }
            }
            if (deDupInfoObjList.Count == noOfRecordCount)
            {
                Session["dedupInfoListS"] = null;
            }
            return returnValu = noOfRecordCount.ToString() + "#";
        }
        protected void uploadButton_Click(object sender, EventArgs e)
        {
            var tablestart = "<table>";
            tablestart += "<tr><td></td></tr>";
            var tabledata = string.Empty;
            tabledata += "<tr><td>Data uploading</td></tr>";
            var tableend = "</table>";
            if (tabledata.Trim().Length > 0)
            {
                var callpop = "showBox('" + tablestart.Trim() + tabledata.Trim() + tableend.Trim() + "');";
                ScriptManager.RegisterStartupScript(this, GetType(), "popup", callpop, true);
            }
        }
        private String LoadUploadData(string strsearchKey, string qDate)
        {
            StringBuilder sbSourceData = new StringBuilder();
            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            if (strsearchKey.Length > 0)
            {
                deDupInfoObjList = dedupeManagerObj.GetUploadResult(Convert.ToDateTime(qDate, new System.Globalization.CultureInfo("ru-RU")), Convert.ToDateTime(qDate, new System.Globalization.CultureInfo("ru-RU")), strsearchKey, 0, 65000);
            }
            try
            {
                sbSourceData.Append("<rows>");
                foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
                {
                    sbSourceData.Append("<row id='" + deDupInfoObj.Id.ToString() + "'>");
                    sbSourceData.Append("<cell>" + deDupInfoObj.Id.ToString() + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Name) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.AccountNo.MasterAccNo1) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.FatherName) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.MotherName) + "</cell>");
                    sbSourceData.Append("<cell>" + deDupInfoObj.DateOfBirth.ToString("dd-MM-yyyy") + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.DeclineReason) + "</cell>");
                    sbSourceData.Append("<cell>" + deDupInfoObj.DeclineDate.ToString("dd-MM-yyyy") + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Address1) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Address2) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ContactNumber.ContactNo1) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ContactNumber.ContactNo2) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ContactNumber.ContactNo3) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ContactNumber.ContactNo4) + "</cell>");
                    string productName = string.Empty;
                    if (deDupInfoObj.ProductId > 0)
                    {
                        productManagerObj = new ProductManager();
                        productName = productManagerObj.GetProductName(deDupInfoObj.ProductId);
                    }
                    else
                    {
                        productName = "";
                    }
                    sbSourceData.Append("<cell>" + AddSlash(productName) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.BankBranch) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Source) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Status) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Remarks) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Tin) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type1) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id1) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type2) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id2) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type3) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id3) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type4) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id4) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type5) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id5) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ResidentaileStatus) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.EducationalLevel) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.MaritalStatus) + "</cell>");
                    sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.LoanAccountNo) + "</cell>");
                    sbSourceData.Append("</row>");
                }
                sbSourceData.Append("</rows>");

            }
            catch (Exception ex)
            {

            }
            return sbSourceData.ToString() + "#";
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (!String.IsNullOrEmpty(sMessage))
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

    }
}
