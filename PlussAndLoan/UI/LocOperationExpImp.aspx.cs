﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using LoanLocatorSerevice;
using Microsoft.VisualBasic.FileIO;

namespace PlussAndLoan.UI
{
    public partial class LocOperationExpImp : System.Web.UI.Page
    {
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        private readonly LoanReportService _service = new LoanReportService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadProduct();
                LoadLoanState();
                LoadDepartment();
                txt_startDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                txt_toDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        private void LoadProduct()
        {
            List<Product> productListObj = _loanApplicationManagerObj.GetAllProductByLearner();
            var productObj = new Product { ProductId = 0, ProductName = "" };
            productListObj.Insert(0, productObj);
            sel_product.DataTextField = "ProductName";
            sel_product.DataValueField = "ProductId";
            sel_product.DataSource = productListObj;
            sel_product.DataBind();
        }

        private void LoadLoanState()
        {
            var dt = _loanApplicationManagerObj.GetLoanStates();
            var loanStates = DataTableToList.GetList<Loan>(dt);
            var state = new Loan() { LoanState = 0, StateName = "Select Loan Status" };
            loanStates.Insert(0, state);
            sel_loanStatus.DataTextField = "StateName";
            sel_loanStatus.DataValueField = "LoanState";
            sel_loanStatus.DataSource = loanStates;
            sel_loanStatus.DataBind();
        }

        private void LoadDepartment()
        {
            var dt = _loanApplicationManagerObj.GetDepartment();
            var departmentList = DataTableToList.GetList<Department>(dt);
            var state = new Department() { LOAN_DEPT_ID = 0, LOAN_DEPT_NAME = "Select Department Name" };
            departmentList.Insert(0, state);
            cmbDepartment.DataTextField = "LOAN_DEPT_NAME";
            cmbDepartment.DataValueField = "LOAN_DEPT_ID";
            cmbDepartment.DataSource = departmentList;
            cmbDepartment.DataBind();
        }

        protected void Search_onClick__(object sender, EventArgs e)
        {
            int productId = Convert.ToInt32(sel_product.Value);
            int processId = Convert.ToInt32(sel_loanStatus.Value);
            int departmentId = Convert.ToInt32(cmbDepartment.Value);
            string startDate = txt_startDate.Value;
            string endDate = txt_toDate.Value;
            var dt = _service.GetExpImp(productId, departmentId, processId, startDate, endDate);
            if (dt != null)
            {
                div_processResultDisplay.InnerHtml = "";

                var noofField = dt != null ? dt.Columns.Count : 100;
                var colWith = noofField != 0 ? 100 / noofField : 0;
                if (colWith * noofField > 100)
                {
                    colWith = colWith - 1;
                }
                var successCount = 0;
                var chckIndex = 0;
                var tableBodyData = "";
                var tabelHeadRowName = "";
                var fieldData = "";
                var index = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    fieldData = "";
                    for (index = 0; index < noofField; index++)
                    {
                        string name = dt.Columns[index].ColumnName;
                        if (chckIndex == 0)
                        {
                            if (name != "noofCount" && name != "DEPT_ID" && name != "PROCESS_STATUS_ID")
                            {
                                tabelHeadRowName += "<td width='" + colWith + "%'><label class='LabelHead'>" + name +
                                                    "</label></td>";
                            }
                            else
                            {

                            }
                        }
                        if (name != "noofCount" && name != "DEPT_ID" && name != "PROCESS_STATUS_ID")
                        {
                            fieldData += "<td><label>" + dt.Rows[i][index] + "</label></td>	";
                        }
                        else { }


                    }

                    tableBodyData += "<tr align='center'>" + fieldData + "</tr>";
                    successCount = successCount + 1;
                    chckIndex++;

                }
                var tableHeadData = "<tr align='center'  bgcolor='#CCCCCC'><td colspan='" + noofField + "' > <label class='LabelHead' >" + successCount + " Records found </td></tr><tr align='center'  bgcolor='#CCCCCC'>" + tabelHeadRowName + "</tr>";
                div_processResultDisplay.InnerHtml = "<table border='1' cellpadding='0' cellspacing='0' width='100%' class='tableWidth'>" + tableHeadData + tableBodyData + "</table>";

            }

        }

        protected void ExportCsv(object o, EventArgs e)
        {

            var filename = "ExpImpReportExport_" + DateTime.Now.Ticks + ".csv";
            var csv = string.Empty;
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Charset = "";
            Response.ContentType = "application/text";

            int productId = Convert.ToInt32(sel_product.Value);
            int processId = Convert.ToInt32(sel_loanStatus.Value);
            int departmentId = Convert.ToInt32(cmbDepartment.Value);
            string startDate = txt_startDate.Value;
            string endDate = txt_toDate.Value;
            var dt = _service.GetExpImp(productId, departmentId, processId, startDate, endDate);
            if (dt != null)
            {
                csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ';'));
                csv += "\r\n";

                foreach (DataRow row in dt.Rows)
                {
                    csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(";", "").Replace("\t", "").Replace("'", "").Replace(",", "") + ';'));
                    csv += "\r\n";
                }

                //Download the CSV file.

                Response.Output.Write(csv);
                Response.Flush();
                Response.End();
            }

        }


        protected void btnUpload_OnClick(object sender, EventArgs e)
        {

            var successCount = 0;
            var successTableHead = "";
            var successTableBodyData = "";
            var failedTableBodyData = "";
            var failedTableHead = "";
            var tableHead = "";
            var leftTableBodyData = "";
            var rightTableBodyData = "";
            var failedCount = 0;
            var loaninfo_departmentId = 0;
            var colSpan = 5;
            var loan_status = 19;
            int llidIndex = 0;
            int processIndex = 1;
            int statusIndex = 2;
            var dateTimeIndex = 3;
            int remarksIndex = 4;
            int amountIndex = 5;
            int levelIndex = 6;
            int tenureIndex = 7;
            var user = (LoanLocatorUser)Session["loanUser"];
            int personId = user.ID_LEARNER;
            int depId = user.LOAN_DEPT_ID;

            string csvPath = Server.MapPath("~/Uploads/") + Path.GetFileName(file_browseSoftCopy.PostedFile.FileName);
           
            file_browseSoftCopy.SaveAs(csvPath);

            DataTable csvData = new DataTable();

            using (TextFieldParser csvReader = new TextFieldParser(csvPath))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;
                string[] colFields = csvReader.ReadFields();
                foreach (string column in colFields)
                {
                    DataColumn datecolumn = new DataColumn(column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }
                while (!csvReader.EndOfData)
                {
                    int j = 0;

                    string[] fieldData = csvReader.ReadFields();
                    //Making empty value as null
                    if (!string.IsNullOrEmpty(fieldData[j]))
                    {
                        // csvData.Rows.Add();
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }


                    }

                    csvData.Rows.Add(fieldData);
                }
            }
            var uploadCsv = _service.update_data(csvData, personId, depId);
            for (int rowIndex = 1; rowIndex < csvData.Rows.Count; rowIndex++)
            {
                if (uploadCsv.Result > 0)
                {

                    if (uploadCsv.Status == 5 || uploadCsv.Status == 15)
                    {
                        successTableBodyData += "<tr><td><label>" + csvData.Rows[rowIndex][llidIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][processIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][statusIndex] + "</label></td>" +
                                                 "<td><label>" + csvData.Rows[rowIndex][dateTimeIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][remarksIndex] + "</label></td>" +
                                               
                                                " <td><label>" + csvData.Rows[rowIndex][amountIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][levelIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][tenureIndex] +
                                                "</label></td></tr>";
                    }
                    else
                    {
                        successTableBodyData += "<tr><td><label>" + csvData.Rows[rowIndex][llidIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][processIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][statusIndex] + "</label></td>" +
                                               
                                                "<td><label>" + csvData.Rows[rowIndex][dateTimeIndex] + "</label></td>" +
                                                 "<td><label>" + csvData.Rows[rowIndex][remarksIndex] + "</label></td>" +
                                                "</tr>";
                    }
                    successCount += 1;
                }

                else
                {
                    if (uploadCsv.Status == 5 || uploadCsv.Status == 15)
                    {
                        failedTableBodyData += "<tr><td><label>" + csvData.Rows[rowIndex][llidIndex] + "</label></td>" +
                                               "<td><label>" + csvData.Rows[rowIndex][processIndex] + "</label></td>" +
                                               "<td><label>" + csvData.Rows[rowIndex][statusIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][dateTimeIndex] + "</label></td>" +
                                               "<td><label>" + csvData.Rows[rowIndex][remarksIndex] + "</label></td>" +
                                              
                                               " <td><label>" + csvData.Rows[rowIndex][amountIndex] + "</label></td>" +
                                               "<td><label>" + csvData.Rows[rowIndex][levelIndex] + "</label></td>" +
                                               "<td><label>" + csvData.Rows[rowIndex][tenureIndex] +
                                               "</label></td></tr>";
                    }
                    else
                    {
                        failedTableBodyData += "<tr><td><label>" + csvData.Rows[rowIndex][llidIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][processIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][statusIndex] + "</label></td>" +
                                                
                                                "<td><label>" + csvData.Rows[rowIndex][dateTimeIndex] + "</label></td>" +
                                                "<td><label>" + csvData.Rows[rowIndex][remarksIndex] + "</label></td>" +
                                                "</tr>";
                    }
                    failedCount += 1;
                }
                successTableHead = "<tr bgcolor='#CCCCCC' align='center'><td colspan='" + colSpan +
                                   "' align='center'><label class='LabelHead'>" + successCount +
                                   " Record upload successfully</label></td></tr>";
                failedTableHead = "<tr bgcolor='#CCCCCC' align='center'> <td colspan='" + colSpan +
                                  "' align='center'><label class='LabelHead'>" + failedCount +
                                  " Record can not upload successfully</label></td></tr>";

            }
            if (uploadCsv.Status == 5 || uploadCsv.Status == 15)
            {
                tableHead = "<tr bgcolor='#CCCCCC' align='center'>" +
                     " <td width='20%'><label class='LabelSubHead'>LLId</label></td>" +
                          "<td width='12%'><label class='LabelSubHead'>Process</label></td>" +
                          "<td width='12%'><label class='LabelSubHead'>Status</label></td>" +
                          "<td width='12%'><label class='LabelSubHead'>DateTime</label></td>" +
                          "<td width='12%'><label class='LabelSubHead'>Remarks</label></td>" +
                          "<td width='16%'><label class='LabelSubHead'>Approved Amount</label></td>" +
                          "<td width='12%'><label class='LabelSubHead'>Level</label></td>" +
                          "<td width='12%'><label class='LabelSubHead'>Tenure</label></td></tr>";
            }
            else
            {
                tableHead = "<tr bgcolor='#CCCCCC' align='center'>" +
                           " <td width='20%'><label class='LabelSubHead'>LLId</label></td>" +
                           "<td width='12%'><label class='LabelSubHead'>Process</label></td>" +
                           "<td width='12%'><label class='LabelSubHead'>Status</label></td>" +
                           " <td width='12%'><label class='LabelSubHead'>DateTime</label></td>" +
                           "<td width='12%'><label class='LabelSubHead'>Remarks</label></td>" +
                           "</tr>";
            }
            leftTableBodyData = successTableHead + tableHead + successTableBodyData;
            rightTableBodyData = failedTableHead + tableHead + failedTableBodyData;
            div_processResultDisplay.InnerHtml = "<table width='100%' border='0'><tr>" +
                 " <td width='50%' valign='top'><table width='100%' border='1' class='tableWidth' >" + leftTableBodyData + "</table></td>" +
                 "<td width='50%' valign='top'><table width='100%' border='1' class='tableWidth'>" + rightTableBodyData + "</table></td></tr></table>";


        }
    }


}

