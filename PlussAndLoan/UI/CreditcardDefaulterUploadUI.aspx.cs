﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_CreditcardDefalterUploadUI : System.Web.UI.Page
    {
        public DeDupInfoManager deDupInfoManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        Int32 rowsPerPage = 100;
        Int32 pageNum = 1;
        Int32 offset = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            deDupInfoManagerObj = new DeDupInfoManager();
            if (Request.QueryString.Count != 0)
            {
                pageNum = Convert.ToInt32(Request.QueryString["pageIndex"]);
                offset = (pageNum - 1) * rowsPerPage;
            }
            BindData(offset, rowsPerPage);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "onLoadPage();", true);
        }
        private void BindData(Int32 offset, Int32 rowsPerPage)
        {
            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            string type = "";
            type = criteriaDropDownList.Text;
            deDupInfoObjList = deDupInfoManagerObj.GetUploadResult(DateTime.Now, DateTime.Now, type, offset, rowsPerPage);
            StringBuilder headTable = new StringBuilder();
            StringBuilder viewTable = new StringBuilder();
            StringBuilder pagingTable = new StringBuilder();
            if (deDupInfoObjList != null)
            {
                headTable.Append("<table id='dataTable' name='dataTable' bordercolor='#F3F2F7' width='100%'  border='0px' cellpadding='0px' cellspacing='0px' >");
                headTable.Append("<thead>");
                headTable.Append("<tr bgcolor='#FOEEE4'>");
                headTable.Append("<td style='width:50px;'><b>SL.</b></td>");
                headTable.Append("<td style='width:200px;'><b>Name</b></td>");
                headTable.Append("<td style='width:200px;'><b>Father's Name</b></td>");
                headTable.Append("<td style='width:200px;'><b>Mother's Name</b></td>");
                headTable.Append("<td style='width:90px;'><b>DOB</b></td>");
                headTable.Append("<td style='width:90px;'><b>Master No</b></td>");
                headTable.Append("<td style='width:90px;'><b>Declin Reson</b></td>");
                headTable.Append("<td style='width:90px;'><b>Declin Date</b></td>");
                headTable.Append("<td style='width:90px;'><b>Business Name</b></td>");
                headTable.Append("<td style='width:90px;'><b>Add1</b></td>");
                headTable.Append("<td style='width:90px;'><b>Add2</b></td>");
                headTable.Append("<td style='width:90px;'><b>Ph1</b></td>");
                headTable.Append("<td style='width:90px;'><b>Ph2</b></td>");
                headTable.Append("<td style='width:90px;'><b>Mob1</b></td>");
                headTable.Append("<td style='width:90px;'><b>Mob2</b></td>");
                headTable.Append("<td style='width:90px;'><b>Source</b></td>");
                headTable.Append("<td style='width:90px;'><b>Status</b></td>");
                headTable.Append("<td style='width:90px;'><b>Remarks</b></td>");
                headTable.Append("</tr>");
                headTable.Append("</thead>");
                //headTable.Append("</table>");
                //tableHeadDiv.InnerHtml = headTable.ToString();
                //viewTable.Append("<table id='dataTable' width='100%'>");
                viewTable.Append(headTable);
                viewTable.Append("<tbody>");
                foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
                {
                    viewTable.Append("<tr>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Id.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Name.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.FatherName.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.MotherName.ToString() + "</td>");
                    if (deDupInfoObj.DateOfBirth.ToString() != "")
                    {
                        viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DateOfBirth.ToString("dd-MM-yyyy") + "</td>");
                    }
                    else
                    {
                        viewTable.Append("<td class='TdStyle'>" + "" + "</td>");
                    }
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.AccountNo.MasterAccNo1.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DeclineReason.ToString() + "</td>");
                    if (deDupInfoObj.DeclineDate.ToString() != "")
                    {
                        viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DeclineDate.ToString("dd-MM-yyyy") + "</td>");
                    }
                    else
                    {
                        viewTable.Append("<td class='TdStyle'>" + "" + "</td>");
                    }
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.BusinessEmployeeName.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Address1.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Address2.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo1.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo2.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo3.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo4.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Source.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Status.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Remarks.ToString() + "</td>");
                    viewTable.Append("</tr>");
                }
                viewTable.Append("</tbody>");
                viewTable.Append("</table>");
                recordViewDiv.InnerHtml = viewTable.ToString();
                Int32 numrows = deDupInfoManagerObj.GetTolalRecord(DateTime.Now, DateTime.Now, type);
                Int32 maxPage = numrows / rowsPerPage;
                maxPage = maxPage + 1;
                string nav = "";
                string prev = "";
                string first = "";
                string next = "";
                string last = "";
                Int32 page = 0;
                for (page = 1; page <= maxPage; page++)
                {
                    if (page == pageNum)
                    {
                        nav = page.ToString();
                    }
                    else
                    {
                        nav += " <a href=../UI/CreditcardDefaulterUploadUI.aspx?pageIndex=" + page.ToString() + ">" + page.ToString() + "</a> ";
                    }
                }
                if (pageNum > 1)
                {
                    page = pageNum - 1;
                    prev = " <a href=../UI/CreditcardDefaulterUploadUI.aspx?pageIndex=" + page.ToString() + ">Prev</a> ";
                    first = " <a href=../UI/CreditcardDefaulterUploadUI.aspx?pageIndex=1>First Page</a>";
                }
                else
                {
                    prev = "&nbsp;";
                    first = "&nbsp;";
                }

                if (pageNum < maxPage)
                {
                    page = pageNum + 1;
                    next = " <a href=../UI/CreditcardDefaulterUploadUI.aspx?pageIndex=" + page + ">Next</a> ";

                    last = " <a href=../UI/CreditcardDefaulterUploadUI.aspx?pageIndex=" + maxPage + ">Last Page</a> ";
                }
                else
                {
                    next = "&nbsp;";
                    last = "&nbsp;";
                }
                pagingTable.Append("<table width='100%'>");
                pagingTable.Append("<tr>");
                pagingTable.Append("<td>" + first.ToString() + prev.ToString() + nav.ToString() + next.ToString() + last.ToString() + "</td>");
                pagingTable.Append("</tr>");
                pagingTable.Append("</table>");
                pagingDiv.InnerHtml = pagingTable.ToString();

            }
        }
        protected void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string path = Request.PhysicalApplicationPath;
                path += @"Uploads\";
                if (FileUpload.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload.FileName);
                    path += fileName;
                    FileUpload.SaveAs(path);
                    ReadExcleFile(path);
                    new AT(this).AuditAndTraial("Creadit Card Defaulder Upload","Upload Successfull");
                }
                else
                {
                    Alert.Show("Please select Excel file");
                    FileUpload.Focus();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Credit card Defalter Upload");
            }
        }
        private void ReadExcleFile(string fileName)
        {
            long slNo = 1;
            int returnvalue = 0;
            long recordCount = 0;

            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            if (fileName != "")
            {
                string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source="
                                                                        + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));

                DataTable dataTableObj = new DataTable();
                OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);
                try
                {
                    dataAdapterObj.Fill(dataTableObj);
                    foreach (DataRow dr in dataTableObj.Rows)
                    {
                        DeDupInfo deDupInfoObj = new DeDupInfo();
                        if (Convert.ToString(dr[0]) != "")
                        {
                            deDupInfoObj.Name = dr[0].ToString();
                            deDupInfoObj.FatherName = dr[2].ToString();
                            deDupInfoObj.MotherName = dr[3].ToString();
                            if (!String.IsNullOrEmpty(dr[4].ToString()))
                            {
                                deDupInfoObj.DateOfBirth = Convert.ToDateTime(dr[4].ToString());
                            }
                            else
                            {
                                deDupInfoObj.DateOfBirth = DateTime.Now;
                            }
                            deDupInfoObj.ScbMaterNo = dr[1].ToString();
                            deDupInfoObj.DeclineReason = dr[5].ToString();
                            if (!String.IsNullOrEmpty(dr[6].ToString()))
                            {
                                deDupInfoObj.DeclineDate = Convert.ToDateTime(dr[6].ToString());
                            }
                            else
                            {
                                deDupInfoObj.DeclineDate = DateTime.Now;
                            }
                            deDupInfoObj.BusinessEmployeeName = dr[7].ToString();
                            deDupInfoObj.Address1 = dr[8].ToString();
                            deDupInfoObj.Address2 = dr[9].ToString();
                            deDupInfoObj.Phone1 = dr[10].ToString();
                            deDupInfoObj.Phone2 = dr[11].ToString();
                            deDupInfoObj.Mobile1 = dr[12].ToString();
                            deDupInfoObj.Mobile2 = dr[13].ToString();
                            if (!String.IsNullOrEmpty(dr[14].ToString()))
                            {
                                deDupInfoObj.ProductId = deDupInfoManagerObj.GetProductId(dr[14].ToString());
                            }
                            else
                            {
                                deDupInfoObj.ProductId = 0;
                            }

                            deDupInfoObj.BankBranch = dr[15].ToString();
                            deDupInfoObj.Source = dr[16].ToString();
                            deDupInfoObj.Status = criteriaDropDownList.Text;//dr[17].ToString();
                            deDupInfoObj.Remarks = dr[18].ToString();
                            if (!String.IsNullOrEmpty(dr[19].ToString()))
                            {
                                deDupInfoObj.Tin = dr[19].ToString();
                            }
                            deDupInfoObj.IdType = dr[20].ToString();
                            deDupInfoObj.IdNo = dr[21].ToString();
                            deDupInfoObj.IdType1 = dr[22].ToString();
                            deDupInfoObj.IdNo1 = dr[23].ToString();
                            deDupInfoObj.IdType2 = dr[24].ToString();
                            deDupInfoObj.IdNo2 = dr[25].ToString();
                            deDupInfoObj.IdType3 = dr[26].ToString();
                            deDupInfoObj.IdNo3 = dr[27].ToString();
                            deDupInfoObj.IdType4 = dr[28].ToString();
                            deDupInfoObj.IdNo4 = dr[29].ToString();
                            deDupInfoObj.ResidentaileStatus = dr[30].ToString();
                            deDupInfoObj.EducationalLevel = dr[31].ToString();
                            deDupInfoObj.MaritalStatus = dr[32].ToString();
                            deDupInfoObj.LoanAccountNo = dr[33].ToString();
                            deDupInfoObj.Active = "Y";
                            deDupInfoObj.CriteriaType = dr[17].ToString();
                            deDupInfoObj.EntryDate = DateTime.Now;
                            deDupInfoObjList.Add(deDupInfoObj);
                            slNo = slNo + 1;
                            recordCount = recordCount + 1;
                            if (recordCount >= 500)
                            {
                                StringBuilder responseTable = new StringBuilder();
                                //PrintProgressBar();
                                returnvalue += deDupInfoManagerObj.SendDedupeInToDB(deDupInfoObjList);
                                HiddenFieldcount.Value = returnvalue.ToString();
                                //Session["uploadCount"] = returnvalue.ToString();
                                deDupInfoObjList.Clear();
                                recordCount = 0;
                                //ClearProgressBar();
                                responseTable.Append("<table id='responseTable' name='responseTable' bordercolor='#F3F2F7' width='50%'  border='2' cellpadding='0px' cellspacing='0px' >");
                                responseTable.Append("<tr><td><b>Please wait data uploading..</b></td></tr>");
                                responseTable.Append("<tr><td><b>" + returnvalue.ToString() + " Data has been upload complete</b></td></tr>");
                                responseTable.Append("<tr><td><b>&nbsp</b></td></tr></table>");
                                HttpContext.Current.Response.Write(responseTable.ToString());
                                HttpContext.Current.Response.Flush();
                            }
                        }
                    }
                    if (recordCount < 500 && deDupInfoObjList.Count > 0)
                    {
                        StringBuilder responseTable1 = new StringBuilder();
                        //PrintProgressBar();
                        returnvalue += deDupInfoManagerObj.SendDedupeInToDB(deDupInfoObjList);
                        HiddenFieldcount.Value = returnvalue.ToString();
                        deDupInfoObjList.Clear();
                        recordCount = 0;
                        //ClearProgressBar();
                        responseTable1.Append("<table id='responseTable' name='responseTable' bordercolor='#F3F2F7' width='50%'  border='0px' cellpadding='0px' cellspacing='0px' >");
                        responseTable1.Append("<tr><td><b>Please wait data uploading..</b></td></tr>");
                        responseTable1.Append("<tr><td><b>" + returnvalue.ToString() + " Data has been upload complete</b></td></tr>");
                        responseTable1.Append("<tr><td><b>&nbsp</b></td></tr></table>");
                        HttpContext.Current.Response.Write(responseTable1.ToString());
                        HttpContext.Current.Response.Flush();

                        //Session["uploadCount"] = returnvalue.ToString();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "LoadPopupWindows();", true);

                    }



                }
                catch (Exception ex)
                {
                    Alert.Show(ex.Message);
                    return;
                }
            }
            //if (deDupInfoObjList.Count > 0)
            //{
            //    UploadDNCInfoList(deDupInfoObjList);
            //}
        }
        public static void PrintProgressBar()
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("<div id='updiv' style='Font-weight:bold;font-size:11pt;Left:320px;COLOR:black;font-family:verdana;Position:absolute;Top:140px;Text-Align:center;'>");
            sb.Append("&nbsp;<script> var up_div=document.getElementById('updiv');up_div.innerText='';</script>");
            sb.Append("<script language=javascript>");
            sb.Append("var dts=0; var dtmax=20;");
            sb.Append("function ShowWait(){var output;output='Please wait while uploading!';dts++;if(dts>=dtmax)dts=1;");
            sb.Append("for(var x=0;x < dts; x++){output+='';}up_div.innerText=output;up_div.style.color='red';}");
            sb.Append("function StartShowWait(){up_div.style.visibility='visible';ShowWait();window.setInterval('ShowWait()',100);}");
            sb.Append("StartShowWait();</script>");
            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.Flush();
        }
        public static void ClearProgressBar()
        {

            StringBuilder sbc = new StringBuilder();
            sbc.Append("<script language='javascript'>");
            sbc.Append("alert('Upload process completed successfully!');");
            sbc.Append("up_div.style.visibility='hidden';");
            sbc.Append("history.go(-1)");
            sbc.Append("</script>");
            HttpContext.Current.Response.Write(sbc);

        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0 && sMessage != "")
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private void UploadDNCInfoList(List<DeDupInfo> deDupInfoObjList)
        {
            int ListInsert = 0;
            if (deDupInfoObjList.Count > 0)
            {
                ListInsert = deDupInfoManagerObj.SendDedupInfoInToDB(deDupInfoObjList);
            }
            if (ListInsert == INSERTE || ListInsert == UPDATE)
            {
                Alert.Show("Upload Successfully");
            }
            else
            {
                Alert.Show("Error in data Please check excel file");
            }
        }
        public static string UploadFile(string destUrl, string sourcePath)
        {

            try
            {

                Uri destUri = new Uri(destUrl);

                FileStream inStream = File.OpenRead(sourcePath);

                WebRequest req = WebRequest.Create(destUri);

                req.Method = "PUT";

                req.Headers.Add("Overwrite", "F");

                req.Timeout = System.Threading.Timeout.Infinite;

                req.Credentials = CredentialCache.DefaultCredentials;

                Stream outStream = req.GetRequestStream();

                string status = CopyStream(inStream, outStream);

                if (status == "success")
                {

                    outStream.Close();

                    WebResponse ores = req.GetResponse();

                    return "success";

                }

                else
                {

                    return status;

                }

            }

            catch (WebException we)
            {

                return we.Message;

            }

            catch (System.Exception ee)
            {

                return ee.Message;

            }

        }

        public static string DownloadFile(string sourceUrl, string destFolder)
        {

            try
            {

                System.Uri sourceUri = new System.Uri(sourceUrl);

                WebRequest req = WebRequest.Create(sourceUri);

                req.Method = "GET";

                req.Timeout = System.Threading.Timeout.Infinite;

                req.Credentials = CredentialCache.DefaultCredentials;

                WebResponse res = req.GetResponse();

                Stream inStream = res.GetResponseStream();

                FileStream fs = new FileStream(destFolder, FileMode.OpenOrCreate);

                string status = CopyStream(inStream, fs);

                if (status == "success")
                {

                    inStream.Close();

                    return "success";

                }

                else
                {

                    inStream.Close();

                    return status;

                }

            }

            catch (WebException we)
            {

                return we.Message;

            }

            catch (System.Exception ee)
            {

                return ee.Message;

            }

        }

        private static string CopyStream(Stream inStream, Stream outStream)
        {

            try
            {

                byte[] buffer = new byte[1024];

                for (; ; )
                {

                    int numBytesRead = inStream.Read(buffer, 0, buffer.Length);

                    if (numBytesRead <= 0)

                        break;

                    outStream.Write(buffer, 0, numBytesRead);

                }

                inStream.Close();

                return "success";

            }

            catch (System.Exception ee)
            {

                return ee.Message;

            }

        }
        ////List<DeDupInfo> deDupInfoObjList = null;
        ////DeDupInfoManager dedupeManagerObj = null;
        ////ProductManager productManagerObj = null;
        ////protected void Page_Load(object sender, EventArgs e)
        ////{
        ////    deDupInfoObjList = new List<DeDupInfo>();
        ////    dedupeManagerObj = new DeDupInfoManager();
        ////    dateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
        ////    try
        ////    {
        ////        string stype = string.Empty;
        ////        string noOfRecord = string.Empty;
        ////        string lowIndex = string.Empty;
        ////        string strOperation = string.Empty;
        ////        string strsearchKey = string.Empty;
        ////        string qDate = string.Empty;
        ////        strOperation = Convert.ToString(Request.QueryString["operation"]);
        ////        noOfRecord = Convert.ToString(Request.QueryString["NoofRecord"]);
        ////        lowIndex = Convert.ToString(Request.QueryString["LowIndex"]);
        ////        strsearchKey = Convert.ToString(Request.QueryString["SEARCHKEY"]);
        ////        qDate = Convert.ToString(Request.QueryString["QUERYDATE"]);
        ////        if (strOperation == "upload")
        ////        {
        ////            StreamReader streamReader = new StreamReader(Request.InputStream);
        ////            string fileNameAndPath = streamReader.ReadToEnd();
        ////            this.Response.Write(UploadExcelFile(fileNameAndPath));
        ////            this.Response.Flush();
        ////            this.Response.Close();
        ////            this.Response.End();

        ////        }
        ////        else if (strOperation == "save")
        ////        {
        ////            if (!string.IsNullOrEmpty(noOfRecord))
        ////            {
        ////                this.Response.Write(SaveUploadData(Convert.ToInt32(noOfRecord), Convert.ToInt32(lowIndex)));
        ////                this.Response.Flush();
        ////                this.Response.Close();
        ////                this.Response.End();
        ////            }
        ////        }
        ////        else if (strOperation == "LoadUploadData" && strsearchKey != "")
        ////        {
        ////            this.Response.Write(LoadUploadData(strsearchKey, qDate));
        ////            this.Response.Flush();
        ////            this.Response.Close();
        ////            this.Response.End();
        ////        }

        ////    }

        ////    catch (Exception ex)
        ////    {
        ////        this.Response.Write("error occured" + ex.Message);
        ////        this.Response.Flush();
        ////        this.Response.Close();
        ////        this.Response.End();
        ////    }
        ////    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();", true);

        ////}
        ////protected void ButtonUploadFile_Click(object sender, System.EventArgs e)
        ////{

        ////    if (FileUpload.HasFile)
        ////    {
        ////        try
        ////        {
        ////            string fileName = "";
        ////            string path = Request.PhysicalApplicationPath;
        ////            path += @"Uploads\";
        ////            fileName = Server.HtmlEncode(FileUpload.FileName);
        ////            path += fileName;
        ////            FileUpload.SaveAs(path);
        ////            Session["fileName"] = path;
        ////            LabelUpload.Text = "Upload File Name: " +
        ////                FileUpload.PostedFile.FileName + "<br>" +
        ////                "Type: " + FileUpload.PostedFile.ContentType +
        ////                " File Size: " + FileUpload.PostedFile.ContentLength +
        ////                " kb<br>";
        ////        }
        ////        catch (System.NullReferenceException ex)
        ////        {
        ////            LabelUpload.Text = "Error: " + ex.Message;
        ////        }
        ////    }
        ////    else
        ////    {
        ////        LabelUpload.Text = "Please select a file to upload.";
        ////    }

        ////}


        ////private string UploadExcelFile(string fileNameAndPath)
        ////{

        ////    string returnValu = string.Empty;
        ////    long slNo = 0;
        ////    if (string.IsNullOrEmpty(Convert.ToString(Session["fileName"])))
        ////    {
        ////        returnValu = "Please select file name #";
        ////    }

        ////    try
        ////    {

        ////        string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source=" + (Session["fileName"].ToString() + (";" + "Extended Properties=\"Excel 8.0;\""))));
        ////        DataTable dataTableObj = new DataTable();
        ////        OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);
        ////        dataAdapterObj.Fill(dataTableObj);
        ////        foreach (DataRow dr in dataTableObj.Rows)
        ////        {
        ////            DeDupInfo deDupInfoObj = new DeDupInfo();
        ////            if (Convert.ToString(dr[0]) != "")
        ////            {
        ////                deDupInfoObj.Name = dr[0].ToString();
        ////                deDupInfoObj.FatherName = dr[2].ToString();
        ////                deDupInfoObj.MotherName = dr[3].ToString();
        ////                if (!String.IsNullOrEmpty(dr[4].ToString()))
        ////                {
        ////                    deDupInfoObj.DateOfBirth = Convert.ToDateTime(dr[4].ToString());
        ////                }
        ////                else
        ////                {
        ////                    deDupInfoObj.DateOfBirth = DateTime.Now;
        ////                }
        ////                deDupInfoObj.ScbMaterNo = dr[1].ToString();
        ////                deDupInfoObj.DeclineReason = dr[5].ToString();
        ////                if (!String.IsNullOrEmpty(dr[6].ToString()))
        ////                {
        ////                    deDupInfoObj.DeclineDate = Convert.ToDateTime(dr[6].ToString());
        ////                }
        ////                else
        ////                {
        ////                    deDupInfoObj.DeclineDate = DateTime.Now;
        ////                }
        ////                deDupInfoObj.BusinessEmployeeName = dr[7].ToString();
        ////                deDupInfoObj.Address1 = dr[8].ToString();
        ////                deDupInfoObj.Address2 = dr[9].ToString();
        ////                deDupInfoObj.Phone1 = dr[10].ToString();
        ////                deDupInfoObj.Phone2 = dr[11].ToString();
        ////                deDupInfoObj.Mobile1 = dr[12].ToString();
        ////                deDupInfoObj.Mobile2 = dr[13].ToString();
        ////                if (!String.IsNullOrEmpty(dr[14].ToString()))
        ////                {
        ////                    deDupInfoObj.ProductId = dedupeManagerObj.GetProductId(dr[14].ToString());
        ////                }
        ////                else
        ////                {
        ////                    deDupInfoObj.ProductId = 0;
        ////                }

        ////                deDupInfoObj.BankBranch = dr[15].ToString();
        ////                deDupInfoObj.Source = dr[16].ToString();
        ////                deDupInfoObj.Status = criteriaDropDownList.Text;//dr[17].ToString();
        ////                deDupInfoObj.Remarks = dr[18].ToString();
        ////                if (!String.IsNullOrEmpty(dr[19].ToString()))
        ////                {
        ////                    deDupInfoObj.Tin = dr[19].ToString();
        ////                }
        ////                deDupInfoObj.IdType = dr[20].ToString();
        ////                deDupInfoObj.IdNo = dr[21].ToString();
        ////                deDupInfoObj.IdType1 = dr[22].ToString();
        ////                deDupInfoObj.IdNo1 = dr[23].ToString();
        ////                deDupInfoObj.IdType2 = dr[24].ToString();
        ////                deDupInfoObj.IdNo2 = dr[25].ToString();
        ////                deDupInfoObj.IdType3 = dr[26].ToString();
        ////                deDupInfoObj.IdNo3 = dr[27].ToString();
        ////                deDupInfoObj.IdType4 = dr[28].ToString();
        ////                deDupInfoObj.IdNo4 = dr[29].ToString();
        ////                deDupInfoObj.ResidentaileStatus = dr[30].ToString();
        ////                deDupInfoObj.EducationalLevel = dr[31].ToString();
        ////                deDupInfoObj.MaritalStatus = dr[32].ToString();
        ////                deDupInfoObj.LoanAccountNo = dr[33].ToString();
        ////                deDupInfoObj.Active = "Y";
        ////                deDupInfoObj.CriteriaType = dr[17].ToString();
        ////                deDupInfoObj.EntryDate = DateTime.Now;
        ////                deDupInfoObjList.Add(deDupInfoObj);
        ////                slNo = slNo + 1;
        ////            }
        ////        }
        ////        Session["dedupInfoListS"] = deDupInfoObjList;
        ////        Session["fileName"] = null;
        ////        returnValu = deDupInfoObjList.Count.ToString() + "#";
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        returnValu = ex.Message + "#";
        ////    }
        ////    return returnValu;
        ////}
        ////private string SaveUploadData(Int32 noOfRecord, Int32 lowIndex)
        ////{
        ////    string returnValu = string.Empty;
        ////    Int32 count = 0;
        ////    Int32 noOfRecordCount = 0;
        ////    List<DeDupInfo> deDupeObjList = new List<DeDupInfo>();
        ////    List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
        ////    deDupInfoObjList = (List<DeDupInfo>)Session["dedupInfoListS"];
        ////    //recourCountHiddenField.Value = "";
        ////    if (deDupInfoObjList.Count > 0)
        ////    {
        ////        dedupeManagerObj.DeleteTempData(deDupInfoObjList[0].Status);

        ////        Int32 modResult = Convert.ToInt32(deDupInfoObjList.Count % 100);
        ////        for (Int32 Index = 0; Index < deDupInfoObjList.Count; Index++)
        ////        {
        ////            deDupeObjList.Add(deDupInfoObjList[Index]);
        ////            count++;
        ////            if (count >= 100)
        ////            {
        ////                dedupeManagerObj.SendDedupeInToDB(deDupeObjList);
        ////                deDupeObjList.Clear();
        ////                count = 0;
        ////                Session["uploadCount"] = noOfRecordCount.ToString();
        ////            }
        ////            noOfRecordCount++;
        ////        }
        ////        if (modResult > 0)
        ////        {
        ////            dedupeManagerObj.SendDedupeInToDB(deDupeObjList);
        ////            deDupeObjList.Clear();
        ////            count = 0;
        ////            Session["uploadCount"] = noOfRecordCount.ToString();
        ////        }
        ////    }
        ////    if (deDupInfoObjList.Count == noOfRecordCount)
        ////    {
        ////        Session["dedupInfoListS"] = null;
        ////    }
        ////    return returnValu = noOfRecordCount.ToString() + "#";
        ////}
        ////protected void uploadButton_Click(object sender, EventArgs e)
        ////{
        ////    var tablestart = "<table>";
        ////    tablestart += "<tr><td></td></tr>";
        ////    var tabledata = string.Empty;
        ////    tabledata += "<tr><td>Data uploading</td></tr>";
        ////    var tableend = "</table>";
        ////    if (tabledata.Trim().Length > 0)
        ////    {
        ////        var callpop = "showBox('" + tablestart.Trim() + tabledata.Trim() + tableend.Trim() + "');";
        ////        ScriptManager.RegisterStartupScript(this, GetType(), "popup", callpop, true);
        ////    }
        ////}
        ////private String LoadUploadData(string strsearchKey, string qDate)
        ////{
        ////    StringBuilder sbSourceData = new StringBuilder();
        ////    List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
        ////    if (strsearchKey.Length > 0)
        ////    {
        ////        deDupInfoObjList = dedupeManagerObj.GetUploadResult(Convert.ToDateTime(qDate, new System.Globalization.CultureInfo("ru-RU")), Convert.ToDateTime(qDate, new System.Globalization.CultureInfo("ru-RU")), strsearchKey, 0, 65000);
        ////    }
        ////    try
        ////    {
        ////        sbSourceData.Append("<rows>");
        ////        foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
        ////        {
        ////            sbSourceData.Append("<row id='" + deDupInfoObj.Id.ToString() + "'>");
        ////            sbSourceData.Append("<cell>" + deDupInfoObj.Id.ToString() + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Name) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.AccountNo.MasterAccNo1) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.FatherName) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.MotherName) + "</cell>");
        ////            sbSourceData.Append("<cell>" + deDupInfoObj.DateOfBirth.ToString("dd-MM-yyyy") + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.DeclineReason) + "</cell>");
        ////            sbSourceData.Append("<cell>" + deDupInfoObj.DeclineDate.ToString("dd-MM-yyyy") + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.BusinessEmployeeName) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Address1) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Address2) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ContactNumber.ContactNo1) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ContactNumber.ContactNo2) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ContactNumber.ContactNo3) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ContactNumber.ContactNo4) + "</cell>");
        ////            string productName = string.Empty;
        ////            if (deDupInfoObj.ProductId > 0)
        ////            {
        ////                productManagerObj = new ProductManager();
        ////                productName = productManagerObj.GetProductName(deDupInfoObj.ProductId);
        ////            }
        ////            else
        ////            {
        ////                productName = "";
        ////            }
        ////            sbSourceData.Append("<cell>" + AddSlash(productName) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.BankBranch) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Source) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Status) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Remarks) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.Tin) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type1) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id1) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type2) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id2) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type3) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id3) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type4) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id4) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Type5) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ApplicantId.Id5) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.ResidentaileStatus) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.EducationalLevel) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.MaritalStatus) + "</cell>");
        ////            sbSourceData.Append("<cell>" + AddSlash(deDupInfoObj.LoanAccountNo) + "</cell>");
        ////            sbSourceData.Append("</row>");
        ////        }
        ////        sbSourceData.Append("</rows>");

        ////    }
        ////    catch (Exception ex)
        ////    {

        ////    }
        ////    return sbSourceData.ToString() + "#";
        ////}
        ////public string AddSlash(string sMessage)
        ////{
        ////    string returnMessage = "";
        ////    if (!String.IsNullOrEmpty(sMessage))
        ////    {
        ////        sMessage = sMessage.Replace("&", "&amp;");
        ////        sMessage = sMessage.Replace(@"<", @"&gt;");
        ////        sMessage = sMessage.Replace(@">", @"&lt;");
        ////        returnMessage = sMessage;
        ////    }
        ////    return returnMessage;
        ////}
    }
}
