﻿using System;
using System.Data;
using BLL;
using DAL;
using BusinessEntities;
using AzUtilities;
using System.IO;
using AzUtilities.Common;
using System.Linq;
using BLL.Report;

namespace PlussAndLoan.UI
{
    public partial class UI_AuditTrailReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                startDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
                endDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
        protected void cancelButton_Click(object sender, EventArgs e)
        {
            startDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
            endDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        protected void showAuditTrailReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (startDateTextBox.Text == null)
                {
                    errMsgLabel.Text = "Please insert Start Date";
                }
                else if (endDateTextBox.Text == null)
                {
                    errMsgLabel.Text = "Please insert End Date";
                }
                else
                {
                    Session["path"] = "AuditTrail.rdlc";
                    var data = new UserManager().GetAuditTrailDataTable(Convert.ToDateTime(startDateTextBox.Text).ToString("yyyy-MM-dd"), Convert.ToDateTime(endDateTextBox.Text).ToString("yyyy-MM-dd"));
                    if (data != null)
                    {
                        data.Columns.Add("fromDate", typeof(System.DateTime));
                        data.Columns.Add("toDate", typeof(System.DateTime));
                        foreach (DataRow row in data.Rows)
                        {
                            row["fromDate"] = Convert.ToDateTime(startDateTextBox.Text).ToString("dd-MMMM-yyyy");
                            row["toDate"] = Convert.ToDateTime(endDateTextBox.Text).ToString("dd-MMMM-yyyy");
                        }
                    }
                    Session["ReportDataSourceName"] = "DataSet1";
                    Session["rdlcData"] = data;
                    new AT(this).AuditAndTraial("Audit Trial (PDF)", "Report");
                    Response.Redirect("~/ReportRDLC/ReportWizards/RdlcReportViewer.aspx");
                }
            }
            catch (Exception ex)
            {
                string mess = ex.Message;
                CustomException.Save(ex, "Cusomer data Upload Failed");
            }
        }


        protected void btnReportCSV_Click(object sender, EventArgs e)
        {
            try
            {
                var data = new UserManager().GetAuditTrailDataTable(Convert.ToDateTime(startDateTextBox.Text).ToString("yyyy-MM-dd"), Convert.ToDateTime(endDateTextBox.Text).ToString("yyyy-MM-dd"));
                new AT(this).AuditAndTraial("Audit-Trail (CSV)", "Report");
                PrintDump(data, "Audit-Trail-Report");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }


        private void PrintDump(DataTable dt, string reportName)
        {
            try
            {
                var filename = reportName + DateTime.Now.Ticks + ".csv";
                var csv = string.Empty;
                if (dt != null)
                {
                    csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ','));
                    csv += "\r\n";

                    foreach (DataRow row in dt.Rows)
                    {
                        csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", " ").Replace(";", " ").Replace("&amp", "&").Replace("&AMP", "&").Replace("amp", " ").Replace("AMP", " ") + ','));
                        csv += "\r\n";
                    }
                }

                MemoryStream memoryStream = new MemoryStream();
                TextWriter textWriter = new StreamWriter(memoryStream);
                textWriter.Write(csv);
                textWriter.Flush(); // added this line
                byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.BinaryWrite(bytesInStream);

            }
            catch (Exception e)
            {
                CustomException.Save(e, "User List Report :: - PrintDump");
                LogFile.WriteLog(e);
            }
            Response.End();
        }
    }
}
