﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_PayrollUI : System.Web.UI.Page
    {
        private PayRollManager payRollManagerObj = new PayRollManager();
        private PayRoll tPlussCompanyObj;
        private List<PayRoll> payRollObjList;
        public SegmentManager segmentManagerObj =null;// new SegmentManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            segmentManagerObj =  new SegmentManager();
            if (!Page.IsPostBack)
            {
                LoadSegmentCode();
                LoadCompanyEntry();
                LoadExistingPayRollData("");
            }
            CatApprovalDateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
        }
        private void LoadCompanyEntry()
        {
           
            List<PayRoll> payRollObjList = new List<PayRoll>();

            payRollObjList = payRollManagerObj.GetAllCompany();
         
            try
            {
                var xml = "<xml id='xml_data'><rows>";
                
                foreach (PayRoll payRollObj in payRollObjList)
                {
                    xml+="<row id='" + payRollObj.CompID.ToString() + "'>";

                    xml+="<cell>";
                    xml+=payRollObj.CompID.ToString();
                    xml+="</cell>";

                    xml+="<cell>";
                    xml+=AddSlash(payRollObj.CompName.ToString());
                    xml+="</cell>";
                    xml+="</row>";
                }
                xml += "</rows></xml>";
                companyDiv.InnerHtml = xml;
            }
            finally
            {
              
            }
            
        }
        private void LoadSegmentCode()
        {

            SegmentCodedropDownList.DataSource = segmentManagerObj.SelectSegmentList();
            SegmentCodedropDownList.DataTextField = "Name";
            SegmentCodedropDownList.DataValueField = "Name";
            SegmentCodedropDownList.DataBind();
        }

        protected void searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (searchTextBox.Text.Trim() != "")
                {
                    LoadExistingPayRollData(searchTextBox.Text);
                }
                else
                {
                    LoadExistingPayRollData("");
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Search Payroll");
            }
        }
        private void LoadExistingPayRollData(string searchKey)
        {
            List<PayRoll> payRollObjList = new List<PayRoll>();
           
            if (searchKey == "")
            {
                payRollObjList = payRollManagerObj.SearchAllPayRoll();
            }
            else
            {
                payRollObjList = payRollManagerObj.SearchPayRoll(searchKey);
            }
            try
            {
                var xml = "<xml id='payroll_data'><rows>";
                foreach (PayRoll payRollObj in payRollObjList)
                {
                    xml+="<row id='" + payRollObj.CompID.ToString() + "'>";

                    xml+="<cell>";
                    xml+=payRollObj.CompID.ToString();
                    xml+="</cell>";

                    xml+="<cell>";
                    xml+=(AddSlash(payRollObj.CompName.ToString()));
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompSegmentID.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompStructure.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompDocategorized.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompCatstatus.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompMue.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompIrwitheosb.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompIrwithouteosb.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(payRollObj.CompCcucondition.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(payRollObj.CompCcuremarks.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompProposdby.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.CompApprovaldate.ToString("dd-MM-yyyy");
                    xml+="</cell>";
                    if (payRollObj.CompStatus == 1)
                    {
                        xml+="<cell>";
                        xml+="Active";
                        xml+="</cell>";
                    }
                    else
                    {
                        xml+="<cell>";
                        xml+="Inactive";
                        xml+="</cell>";
                    }
                    xml+="<cell>";
                    xml+=payRollObj.ExtraBenefit.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.ExtraField1.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.ExtraField2.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.ExtraField3.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=payRollObj.ExtraField4.ToString();
                    xml+="</cell>";
                    xml+="</row>";
                }
                xml += "</rows></xml>";
                payrollDiv.InnerHtml = xml;
            }
            finally
            {
            }
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.IsValidPage())
                {
                    if (!IsExisting())
                    {
                        if (Convert.ToString(HiddenField1.Value) == "")
                        {
                            this.InsertData();
                        }
                        else
                        {
                            this.UpdateData();
                            // else { Alert.Show("Select a data first !"); }
                     
                        }
                    }
                    else { Alert.Show("Company Name '" + companyNameTextBox.Text + "' already exist"); }
                }
                else
                {
                    { Alert.Show("input some data first !"); }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Payroll");
            }
        
        }

        private bool IsExisting()
        {
            payRollManagerObj = new PayRollManager();
            if (HiddenField1.Value == "")// duplicate checking while inserting
            {
                List<PayRoll> dummyPayRollObjList = new List<PayRoll>();
                dummyPayRollObjList = payRollManagerObj.SearchAllPayRoll();

                PayRoll dummyPayRoll = new PayRoll();
                dummyPayRoll = dummyPayRollObjList.Find(p => p.CompName == companyNameTextBox.Text);//VS-2008

                if (dummyPayRoll != null)
                { return true; }
                else
                { return false; }
            }
            else             // duplicate checking while updating
            {
                List<PayRoll> dummyPayRollObjList = new List<PayRoll>();
                dummyPayRollObjList = payRollManagerObj.SearchAllPayRoll();

                PayRoll dummyPayRoll = new PayRoll();
                dummyPayRoll = dummyPayRollObjList.Find(p => p.CompName == companyNameTextBox.Text);
                if (dummyPayRoll != null)
                {
                    if (dummyPayRoll.CompID.ToString() == HiddenField1.Value)
                    { return false; }
                    else
                    { return true; }
                }
                else { return false; }
            }
        }

        private bool IsValidPage()
        {
            if (companyNameTextBox.Text == "")
            {
                Alert.Show("Company name is missing");
                companyNameTextBox.Focus();
                return false;
            }
            else if (mueTextBox.Text == "")
            {
                Alert.Show("MUE is missing");
                mueTextBox.Focus();
                return false;
            }
            else if (IRwithEOSBTextBox.Text == "")
            {
                Alert.Show("IR with EOSB is missing");
                IRwithEOSBTextBox.Focus();
                return false;
            }
            else if (IRwithoutEOSBTextBox.Text == "")
            {
                Alert.Show("IR without EOSB is missing");
                IRwithoutEOSBTextBox.Focus();
                return false;
            }
            else if (CCUConditionTextBox.Text == "")
            {
                Alert.Show("CCU condition is missing");
                CCUConditionTextBox.Focus();
                return false;
            }
            else if (CCURemarksTextBox.Text == "")
            {
                Alert.Show("CCU remarks is missing");
                CCURemarksTextBox.Focus();
                return false;
            }
            else if (ProposdByTextBox.Text == "")
            {
                Alert.Show("Proposed By is missing");
                ProposdByTextBox.Focus();
                return false;
            }
            else if (!(DateFormat.IsDate(CatApprovalDateTextBox.Text)) || CatApprovalDateTextBox.Text == "")
            {
                Alert.Show("Date is missing");
                CatApprovalDateTextBox.Focus();
                return false;
            }
            else
            { return true; }
        }


        private int DeleteData()
        {
            int status = 0;
            payRollManagerObj = new PayRollManager();
            status = payRollManagerObj.DeletePayroll(Convert.ToInt32(HiddenField1.Value));
            new AT(this).AuditAndTraial("Payroll", "Delete");
            return status;
        }

        private void UpdateData()
        {
            tPlussCompanyObj = new PayRoll();

            tPlussCompanyObj.CompID = Convert.ToInt32(HiddenField1.Value);
            tPlussCompanyObj.CompName = Convert.ToString(companyNameTextBox.Text);
            tPlussCompanyObj.CompSegmentID = Convert.ToString(SegmentCodedropDownList.SelectedValue.ToString());
            tPlussCompanyObj.CompStructure = Convert.ToString(CompanyStructuredropDownList.Text);
            tPlussCompanyObj.CompDocategorized = Convert.ToString(DoCategorizeddropDownList.Text);
            tPlussCompanyObj.CompCatstatus = Convert.ToString(CatStatusdropDownList.Text);
            tPlussCompanyObj.CompMue = Convert.ToDouble(mueTextBox.Text);
            tPlussCompanyObj.CompIrwitheosb = Convert.ToString(IRwithEOSBTextBox.Text);
            tPlussCompanyObj.CompIrwithouteosb = Convert.ToString(IRwithoutEOSBTextBox.Text);
            tPlussCompanyObj.CompCcucondition = Convert.ToString(CCUConditionTextBox.Text);
            tPlussCompanyObj.CompCcuremarks = Convert.ToString(CCURemarksTextBox.Text);
            tPlussCompanyObj.CompProposdby = Convert.ToString(ProposdByTextBox.Text);
            tPlussCompanyObj.CompApprovaldate = Convert.ToDateTime(CatApprovalDateTextBox.Text, new System.Globalization.CultureInfo("ru-RU"));
            if (StatusdropDownList.Text == "Active")
            {
                tPlussCompanyObj.CompStatus = 1;
            }
            else
            { tPlussCompanyObj.CompStatus = 0; }
            tPlussCompanyObj.ExtraBenefit = Convert.ToString(extraBenefitDropDownList.Text);
            tPlussCompanyObj.ExtraField1 = Convert.ToString(extraField1TextBox.Text);
            tPlussCompanyObj.ExtraField2 = Convert.ToString(extraField2TextBox.Text);
            tPlussCompanyObj.ExtraField3 = Convert.ToString(extraField3TextBox.Text);
            tPlussCompanyObj.ExtraField4 = Convert.ToString(extraField4TextBox.Text);
            payRollManagerObj = new PayRollManager();

            int updateStatus = payRollManagerObj.UpdatePayroll(tPlussCompanyObj);
            var msg = "";
            if (updateStatus == 1)
            {
                msg = "Update sucessfull";
                LoadExistingPayRollData("");
                LoadCompanyEntry();
                this.ClearAll();
            }
            else
            {
                msg = "Update Un-sucessfull";
            }
            new AT(this).AuditAndTraial("Payroll", msg);
            Alert.Show(msg);
        }

        private void InsertData()
        {
            tPlussCompanyObj = new PayRoll();

            tPlussCompanyObj.CompName = Convert.ToString(companyNameTextBox.Text);
            tPlussCompanyObj.CompSegmentID = Convert.ToString(SegmentCodedropDownList.SelectedValue.ToString());
            tPlussCompanyObj.CompStructure = Convert.ToString(CompanyStructuredropDownList.Text);
            tPlussCompanyObj.CompDocategorized = Convert.ToString(DoCategorizeddropDownList.Text);
            tPlussCompanyObj.CompCatstatus = Convert.ToString(CatStatusdropDownList.Text);
            tPlussCompanyObj.CompMue = Convert.ToDouble(mueTextBox.Text);
            tPlussCompanyObj.CompIrwitheosb = Convert.ToString(IRwithEOSBTextBox.Text);
            tPlussCompanyObj.CompIrwithouteosb = Convert.ToString(IRwithoutEOSBTextBox.Text);
            tPlussCompanyObj.CompCcucondition = Convert.ToString(CCUConditionTextBox.Text);
            tPlussCompanyObj.CompCcuremarks = Convert.ToString(CCURemarksTextBox.Text);
            tPlussCompanyObj.CompProposdby = Convert.ToString(ProposdByTextBox.Text);
            tPlussCompanyObj.CompApprovaldate = Convert.ToDateTime(CatApprovalDateTextBox.Text, new System.Globalization.CultureInfo("ru-RU"));
            if (StatusdropDownList.Text == "Active")
            {
                tPlussCompanyObj.CompStatus = 1;
            }
            else
            { tPlussCompanyObj.CompStatus = 0; }
            tPlussCompanyObj.ExtraBenefit = Convert.ToString(extraBenefitDropDownList.Text);
            tPlussCompanyObj.ExtraField1 = Convert.ToString(extraField1TextBox.Text);
            tPlussCompanyObj.ExtraField2 = Convert.ToString(extraField2TextBox.Text);
            tPlussCompanyObj.ExtraField3 = Convert.ToString(extraField3TextBox.Text);
            tPlussCompanyObj.ExtraField4 = Convert.ToString(extraField4TextBox.Text);

            payRollManagerObj = new PayRollManager();

            int insertStatus = payRollManagerObj.InsertPayroll(tPlussCompanyObj);
            var msg = "";
            if (insertStatus == 1)
            {
                msg = "Save sucessfull";
                //saveButton.Text = "Update";
                LoadCompanyEntry();
                LoadExistingPayRollData("");
                this.ClearAll();
            }
            else
            {
                msg = "Save un-sucessfull";
            }
            new AT(this).AuditAndTraial("Payroll", msg);
            Alert.Show(msg);
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string path = Request.PhysicalApplicationPath;
                path += @"Uploads\";
                if (payrollFileUpload.HasFile)
                {
                    fileName = Server.HtmlEncode(payrollFileUpload.FileName);
                    path += fileName;
                    payrollFileUpload.SaveAs(path);

                    ReadExcleFile(path);
                }
                else
                {
                    Alert.Show("Please select Excel file");
                    payrollFileUpload.Focus();
                }
                new AT(this).AuditAndTraial("Upload Payroll","Upload");
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Upload Payroll");
            }
        }

        private void ReadExcleFile(string fileName)
        {
            if (fileName != "")
            {
                //string fileName = payrollFileUpload.PostedFile.FileName;
                string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source="
                                                                        + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));

                DataTable dataTableObj = new DataTable();
                OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);

                try
                {
                    dataAdapterObj.Fill(dataTableObj);

                    payRollObjList = new List<PayRoll>();
                    foreach (DataRow dr in dataTableObj.Rows)
                    {
                        tPlussCompanyObj = new PayRoll();
                        if (Convert.ToString(dr[0]) != "")
                        {
                            //tPlussCompanyObj.CompID = Convert.ToInt32(dr[0].ToString());
                            tPlussCompanyObj.CompName = dr[0].ToString();
                            tPlussCompanyObj.CompSegmentID = dr[1].ToString();
                            tPlussCompanyObj.CompStructure = dr[2].ToString();
                            tPlussCompanyObj.CompDocategorized = dr[3].ToString();
                            tPlussCompanyObj.CompIrwitheosb = dr[4].ToString();
                            tPlussCompanyObj.CompIrwithouteosb = dr[5].ToString();
                            tPlussCompanyObj.CompCatstatus = dr[6].ToString();
                            if (dr[7].ToString() != "")
                            {
                                tPlussCompanyObj.CompMue = Convert.ToDouble(dr[7].ToString());
                            }
                            else
                            {
                                tPlussCompanyObj.CompMue = 0;
                            }
                        
                            tPlussCompanyObj.CompCcucondition = dr[8].ToString();
                            tPlussCompanyObj.CompCcuremarks = dr[9].ToString();
                            tPlussCompanyObj.CompProposdby = dr[10].ToString();
                            if (dr[11].ToString() != "")
                            {
                                tPlussCompanyObj.CompApprovaldate = Convert.ToDateTime(dr[11].ToString());
                            }
                            else
                            {
                                tPlussCompanyObj.CompApprovaldate = DateTime.Now;
                            }
                            tPlussCompanyObj.CompStatus = 1;
                            tPlussCompanyObj.ExtraBenefit = Convert.ToString(dr[12].ToString());
                            tPlussCompanyObj.ExtraField1 = "";
                            tPlussCompanyObj.ExtraField2 = "";
                            tPlussCompanyObj.ExtraField3 = "";
                            tPlussCompanyObj.ExtraField4 = "";

                            payRollObjList.Add(tPlussCompanyObj);
                        }
                    }

                    payRollManagerObj = new PayRollManager();
                    int returnValue = 0;
                    long upLoadtStatus = 0;
                    long errorStatus = 0;
                    foreach (PayRoll tPlussCompanyObj in payRollObjList)
                    {

                        returnValue = payRollManagerObj.InsertPayroll(tPlussCompanyObj);
                        if (returnValue > 0)
                        {
                            upLoadtStatus = upLoadtStatus + 1;
                        }
                        else
                        {
                            errorStatus = errorStatus + 1;
                        }
                    }

                    if (upLoadtStatus > 0)
                    {
                        Alert.Show(upLoadtStatus + " Rows Data upload sucessfull");
                    }
                    if (errorStatus > 0)
                    {
                        Alert.Show(errorStatus + " Rows Data Cannot upload sucessfull");
                    }
                    payRollObjList.Clear();
                    LoadExistingPayRollData("");
                }
                catch (Exception ex)
                {
                    Alert.Show(ex.Message);

                }
            }

            this.ClearAll();
        }

        protected void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (HiddenField1.Value.ToString() != "")
                {
                    int status = this.DeleteData();
                    this.ClearAll();
                    Alert.Show("Deleted !");
                }
                else { }
                new AT(this).AuditAndTraial("Delete", "Delete Payroll");
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Delete Payroll");
            }
        }


        protected void clearButton_Click(object sender, EventArgs e)
        {
            this.ClearAll();
            //errorMsgLabel.Text = "";
        }
        private void ClearAll()
        {
            companyNameTextBox.Text = "";
            mueTextBox.Text = "";
            IRwithEOSBTextBox.Text = "";
            IRwithoutEOSBTextBox.Text = "";
            CCUConditionTextBox.Text = "";
            CCURemarksTextBox.Text = "";
            ProposdByTextBox.Text = "";
            CatApprovalDateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            HiddenField1.Value = "";
            saveButton.Text = "Save";
            searchTextBox.Text = "";
            StatusdropDownList.SelectedIndex = 1;
            CompanyStructuredropDownList.SelectedIndex = 0;
            SegmentCodedropDownList.SelectedIndex = 0;
            DoCategorizeddropDownList.SelectedIndex = 0;
            CatStatusdropDownList.SelectedIndex = 0;
            extraBenefitDropDownList.SelectedIndex = 0;
            extraField1TextBox.Text="";
            extraField2TextBox.Text="";
            extraField3TextBox.Text="";
            extraField4TextBox.Text="";


        }

        protected void deleteCompanyNameButton_Click(object sender, EventArgs e)
        {
            try
            {
                int DeleteStatus = 0;
                if (companyNameTextBox.Text == "")
                {
                    Alert.Show("Please Select Company name in the list");
                    companyNameTextBox.Focus();
                }
                else
                {
                    DeleteStatus = DeleteData();
                    if (DeleteStatus > 0)
                    {
                        Alert.Show("Delete Successfully");
                        LoadCompanyEntry();
                        LoadExistingPayRollData("");
                        ClearAll();
                    }
                    else
                    {
                        Alert.Show("Can not delete data from database");
                    }
                    new AT(this).AuditAndTraial("Delete Company in Payroll", "Delete");
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Delete Company in Payroll");
            }
        }

        //protected void dataGridView_SelectionChanged(object sender, EventArgs e)
        //{
        //    GridViewRow row = gvrPayroll.SelectedRow;
        //    var id = row.Cells[1].Text;
        //    var name = row.Cells[2].Text;
        //    HiddenField1.Value = id;
        //    companyNameTextBox.Text = name;
        //}
    }
}
