﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorAdmin.Master" AutoEventWireup="true" CodeBehind="AccessLoginReports.aspx.cs" Inherits="PlussAndLoan.AccessLoginReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function PrintDiv() {
            //debugger;
            var myContentToPrint = document.getElementById("viewDiv");
            var myWindowToPrint = window.open('', '', 'width=800,height=600,toolbar=0,scrollbars=-1,status=0,resizable=-1,location=0,directories=0');
            myWindowToPrint.document.write(myContentToPrint.innerHTML);
            myWindowToPrint.document.close();
            myWindowToPrint.focus();
            myWindowToPrint.print();
            myWindowToPrint.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="insertDiv" runat="server">
        <table width="780" border="0" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
            <tr bgcolor="#CEDFBD">
                <td height="30" colspan="11" valign="middle" style="text-align: center;">
                    <div style="text-align: center; font-weight: bold;"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><h1 style="text-align: center;">Application Status Wise Reprot</h1></font></div>
                </td>
            </tr>

            <tr bgcolor="BDD3A5">
                <td height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                    <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Start Date :&nbsp;</font></div>

                </td>
                <td width="226" height="30" align="center" valign="middle" bgcolor="#EFF3E7">
                    <div align="left">
                        <font color="#FFFFFF" face="Courier New, Courier, mono"> </font><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
                         
                   
                    <asp:DropDownList ID="DayDropDownList" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="MonthDropDownList" runat="server"> </asp:DropDownList>  
                    <asp:DropDownList ID="YearDropDownList" runat="server"> </asp:DropDownList>   

              </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font face="Verdana, Arial, Helvetica, sans-serif"> </font><font color="#FFFFFF" face="Courier New, Courier, mono"> </font>
                    </div>

                </td>
                <td width="75" height="30" align="center" valign="middle" bgcolor="#EFF3E7">&nbsp;TO End Date </td>
                <td height="30" align="left" valign="middle" bgcolor="#EFF3E7">

                    <asp:DropDownList ID="ToDateDayList" runat="server"></asp:DropDownList>
                    <asp:DropDownList ID="ToDateMonthList" runat="server"></asp:DropDownList>
                    <asp:DropDownList ID="ToDateYearList" runat="server"></asp:DropDownList>


                </td>
            </tr>
            <tr bgcolor="BDD3A5">
                <td width="193" height="30" align="right" valign="middle" bgcolor="#D5E2C5">
                    <div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Staff Id:&nbsp;</font></div>
                </td>
                <td height="30" colspan="2" align="left" valign="middle" bgcolor="#EFF3E7" style="background-color: #D5E2C5;"><font face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
               <asp:TextBox ID="staffIdTextBox" runat="server" Width="200px"></asp:TextBox>
              </font>&nbsp; 
                </td>
                <td style="background-color: #D5E2C5;">&nbsp;</td>
            </tr>
            <tr bgcolor="#CEDFBD">
                <td height="30">
                    <div align="right"></div>
                </td>
                <td height="30" colspan="2" align="left"><font face="Verdana, Arial, Helvetica, sans-serif">
                    <asp:Button runat="server" Text="Show Access Login Report" ID="btnShow" OnClick="btnShow_OnClick" style="border:1px solid #83b67a; background-color: #D8EED5; text-align: center;font-size: 13px;" Height="25px" Width="206px" />
                        <td height="30">&nbsp;</td>
                       </font>
                </td>
            </tr>
        </table>
    </div>
    <div id="ShowReportDiv" runat="server">
        <div id="viewDiv">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100%" align="center" valign="top">
                    <table width="780" border="0" cellspacing="0" cellpadding="0">
                    <tr align="left" valign="top"> 
                      <td height="47" background="image/blueline.gif"><img src="images/blueline.gif" width="554" height="47"></td>
                      <td width="226" height="47"><img src="images/logo.gif" width="226" height="47"></td>
                    </tr>
                </table>
                  <table width="780"  border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="780" height="47" align="left" valign="middle"> <div align="left"><font color="#333333" size="2" face="Verdana, Arial, Helvetica, sans-serif"><img src="images/ltopbg.jpg" width="450" height="47"></font> 
                        </div>
                        <div align="left"></div></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
                <div>
                    <table width="780" height="79" cellpadding="2" cellspacing="1" bordercolor="#00CCCC" style='border: 1px solid #333333;'>
                        <tr bgcolor="BDD3A5" height="30">
                            <td height="20" colspan="5" align="left" valign="middle">
                            </td>
                           
                            <td align="center" valign="middle" bgcolor="BDD3A5">
                      
                                <asp:ImageButton  ID="imgexpand" runat="server" ImageAlign="Bottom" ImageUrl="images\printer.gif" Height="18px" Width="50px" OnClientClick="PrintDiv()" />

                            </td>
                           </tr>
                        <tr bgcolor="BDD3A5">
                            <td width="10%" height="40" bgcolor="#D5E2C5"><b><font size="2" face="Verdana, Arial, Helvetica, sans-serif"></font></b>
                                <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></div>
                            </td>
                            <td width="23%" bgcolor="#E7F7C6">
                                <div align="left" id="fromDiv" runat="server">
                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
		      </font>
                                </div>
                            </td>
                            <td width="15%" bgcolor="#D5E2C5">
                                <div align="center">
                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>To</b></font>
                                </div>
                            </td>
                            <td width="16%" bgcolor="#E7F7C6">
                                <div align="left"  runat="server">
                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
		 
                  </font>
                                </div>
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="2"> 
                <div align="center" id="toDiv" runat="server"></div>
                <div align="center"></div>
                </font></td>
                            <td width="11%" bgcolor="#D5E2C5">
                                <div align="right">
                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b></b></font>
                                </div>
                            </td>
                            <td width="24%" colspan="3" bgcolor="#E7F7C6">
                                <div align="left" id="yearDiv" runat="server">
                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="2">
		     </font>
                                </div>
                            </td>
                        </tr>
                    </table>

                </div>
                <br />
                <div id="divAccessReport" style="overflow-y: scroll; height: 380px; width: 100%;" class="GridviewBorder">
                    <asp:GridView ID="AccessGridView" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        Font-Size="11px" PageSize="8"
                        Width="100%" EnableModelValidation="True" GridLines="Horizontal" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">
                        <RowStyle BackColor="White" ForeColor="#333333" />
                        <Columns>
                            <asp:TemplateField HeaderText="Staff ID" HeaderStyle-Width="40%" HeaderStyle-Height="25px">
                                <ItemTemplate>
                                    <asp:Label ID="USERNAME" runat="server" Text='<%# Bind("USERNAME") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" HeaderStyle-Width="40%">
                                <ItemTemplate>
                                    <asp:Label ID="Name" runat="server" Text='<%# Bind("NAME_LEARNER") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                          
                            <asp:TemplateField HeaderText="Login Time" HeaderStyle-Width="40%">
                                <ItemTemplate>
                                    <asp:Label ID="LOGED_IN_TIME" runat="server" Text='<%# Bind("LOGED_IN_TIME") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Logout Time" HeaderStyle-Width="40%">
                                <ItemTemplate>
                                    <asp:Label ID="LOGED_OUT_TIME" runat="server" Text='<%# Bind("LOGED_OUT_TIME") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                           
                        </Columns>
                        <HeaderStyle BackColor="#BDD3A5" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            &nbsp;&nbsp;No data found.
                        </EmptyDataTemplate>
                        <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <FooterStyle CssClass="GvFixedFooter" BackColor="White" ForeColor="#333333" />
                    </asp:GridView>
                </div>
            </div>
    </div>
</asp:Content>
