﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/UserSwitch.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UserSwitchHome" Codebehind="UserSwitchHome.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .btnSwitcher {
            width: 200px;
            height: 40px;
            font-weight: bold;
            font-size: larger;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <div>
        <div style="height: 365px; padding-top: 70px;">
            <center>
                <table cellspacing="0" width="800 px"  cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="loginError"></asp:Label><br/>
                                <asp:Label runat="server" ID="loginStatus"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            
                                <img alt="" height="280 px" src="../Images/HopePage.jpg" />
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnPluss" Visible="false" runat="server" Text="Pluss" CssClass="btnSwitcher" OnClick="btnPluss_Click" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                          <asp:Button ID="btnLoanLocator" Visible="false" runat="server" Text="Loan Locator" CssClass="btnSwitcher" OnClick="btnLoanLocator_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </center>
        </div>
    </div>
    
</asp:Content>

