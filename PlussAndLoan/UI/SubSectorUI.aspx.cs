﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_SubSectorUI : System.Web.UI.Page
    {
        public SubSectorManager subSectorManagerObj = null;
        public SectorManager sectorManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            subSectorManagerObj = new SubSectorManager();
            sectorManagerObj = new SectorManager();
            if (!Page.IsPostBack)
            {
                LoadSectorComboBoxData();
                LoadData("");
                subSectorIdTextBox.Text = subSectorManagerObj.SelectMaxSubSectorId().ToString();
                subSectorIdHidden.Value = subSectorIdTextBox.Text;
            }
        }
        public void LoadSectorComboBoxData()
        {
            List<Sector> sectorObjList = sectorManagerObj.SelectSectorList();
            sectorObjList=sectorObjList.FindAll(InactiveSubSector => InactiveSubSector.Status != 0);
            Sector sectorObj = new Sector();
            sectorObj.Id = 0;
            sectorObj.Name = "Select sector name";
            sectorObjList.Insert(0,sectorObj);
            sectorNameDropDownList.DataSource = sectorObjList;
            sectorNameDropDownList.DataTextField = "Name";
            sectorNameDropDownList.DataValueField = "Id";
            sectorNameDropDownList.DataBind();
        }
        private void LoadData(string searchKey)
        {
            List<SubSector> subSectorObjList = new List<SubSector>();
            if (searchKey == "")
            {
                subSectorObjList = subSectorManagerObj.SelectSubSectorObjList();
            }
            else
            {
                subSectorObjList = subSectorManagerObj.SelectSubSectorObjList(searchKey);
            }
            try
            {
                var xml = "<xml id='xml_data'><rows>";
                foreach (SubSector subSectorObj in subSectorObjList)
                {
                   xml+="<row id='" + subSectorObj.Id.ToString() + "'>";

                   xml+="<cell>";
                   xml+=subSectorObj.Id.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=subSectorObj.SectorId.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=AddSlash(subSectorObj.SectorName.ToString());
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=AddSlash(subSectorObj.Name.ToString());
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=subSectorObj.Code.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=subSectorObj.InterestRate.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=subSectorObj.IRCode.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=subSectorObj.Color.ToString();
                   xml+="</cell>";
                    if (subSectorObj.Status == 1)
                    {
                       xml+="<cell>";
                       xml+="Active";
                       xml+="</cell>";
                    }
                    else
                    {
                       xml+="<cell>";
                       xml+="Inactive";
                       xml+="</cell>";
                    }
                   xml+="</row>";
                }
                xml += "</rows></xml>";
                subSectorDiv.InnerHtml = xml;
            }
            finally
            {
            }
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                int subSector = 0;
                SubSector subSectorObj = new SubSector();
                if (IsBlank())
                {
                    subSectorObj.Id = Convert.ToInt64(subSectorIdHidden.Value);
                    subSectorObj.SectorId = Convert.ToInt32(sectorNameDropDownList.SelectedValue.ToString());
                    subSectorObj.Name = Convert.ToString(subSetorNameTextBox.Text);
                    subSectorObj.Code = Convert.ToString(sectorCodeTextBox.Text);
                    subSectorObj.InterestRate = Convert.ToString(interestTextBox.Text);
                    subSectorObj.IRCode = Convert.ToString(codeTextBox.Text);
                    subSectorObj.Color = Convert.ToString(colorDownList.Text);
                    if (statusDropDownList.Text == "Active")
                    {
                        subSectorObj.Status = 1;
                    }
                    else
                    {
                        subSectorObj.Status = 0;
                    }
                    subSector = subSectorManagerObj.SendSubSectorInfoInToDB(subSectorObj);
                    var msg = "";
                    if(subSector==INSERTE)
                    {
                        msg = "Save successfully";
                        LoadData("");
                        ClearField();
                    }
                    else if (subSector == UPDATE)
                    {
                        msg = "Update successfully";
                        LoadData("");
                        ClearField();
                    }
                    else if(subSector == ERROR)
                    {
                        msg = "Error in data!";
                    }
                    new AT(this).AuditAndTraial("Save Sub Sector", msg);
                    Alert.Show(msg);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Sub Sector");
            }

        }
        private bool IsBlank()
        {
            if (sectorNameDropDownList.SelectedIndex == 0)
            {
                Alert.Show("Please select sector name");
                sectorNameDropDownList.Focus();
                return false;
            }
            else if (subSetorNameTextBox.Text == "")
            {
                Alert.Show("Please input subsector name");
                subSetorNameTextBox.Focus();
                return false;
            }
            else if (sectorCodeTextBox.Text == "")
            {
                Alert.Show("Please input sub sector code");
                sectorCodeTextBox.Focus();
                return false;
            }
            else if (interestTextBox.Text == "")
            {
                Alert.Show("Please input interest rate");
                interestTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearField();
        }
        private void ClearField()
        {
            subSectorIdTextBox.Text = subSectorManagerObj.SelectMaxSubSectorId().ToString();
            subSectorIdHidden.Value = subSectorIdTextBox.Text;
            sectorNameDropDownList.SelectedIndex = 0;
            subSetorNameTextBox.Text = "";
            sectorCodeTextBox.Text = "";
            interestTextBox.Text = "";
            codeTextBox.Text = "";
            colorDownList.SelectedIndex = 0;
            statusDropDownList.SelectedIndex = 0;
        }
        protected void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string path = Request.PhysicalApplicationPath;
                path += @"Uploads\";
                if (FileUpload.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload.FileName);
                    path += fileName;
                    FileUpload.SaveAs(path);

                    ReadExcleFile(path);
                }
                else
                {
                    Alert.Show("Please select Excel file");
                    FileUpload.Focus();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Upload Sub Sector");
            }
        }
        private void ReadExcleFile(string fileName)
        {
            string sectorNameLiset = null;
            List<SubSector> subSectorObjList = new List<SubSector>();
            if (fileName != "")
            {
                string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source="
                                                                        + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));

                DataTable dataTableObj = new DataTable();
                OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);

                try
                {
                    dataAdapterObj.Fill(dataTableObj);


                    foreach (DataRow dataRow in dataTableObj.Rows)
                    {
                        SubSector subSectorObj = new SubSector();
                        subSectorObj.SectorName = dataRow[0].ToString().Replace("&", "");
                        subSectorObj.SectorId = subSectorManagerObj.SelectSectorId(subSectorObj.SectorName.Trim());
                        if (subSectorObj.SectorId == 0)
                        {
                            sectorNameLiset = sectorNameLiset + dataRow[1].ToString().Replace("&", "") + "\n";
                        }
                        subSectorObj.Name = dataRow[1].ToString().Replace("&", "");
                        subSectorObj.Code = dataRow[2].ToString().Replace("&", "");
                        subSectorObj.InterestRate = dataRow[3].ToString().Replace("&", "");
                        subSectorObj.IRCode = dataRow[4].ToString().Replace("&", "");
                        subSectorObj.Color = dataRow[5].ToString().Replace("&", "");
                        subSectorObj.Status = 1;
                        if (subSectorObj.SectorId > 0)
                        {
                            subSectorObjList.Add(subSectorObj);
                        }
                    }
                }

                catch (Exception ex)
                {
                    Alert.Show(ex.Message);
                }

                if (sectorNameLiset.Length > 0)
                {
                    //deleteButton.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete?');");
                    //ClientScript.RegisterStartupScript(this.Page.GetType(), "Report", "if(window.confirm('Do you want to see report?')){}else{}", true);
                    //Alert.Show(sectorNameLiset + "Sector Name are not match");
                }
                if (subSectorObjList.Count > 0)
                {
                    UploadSubSectorList(subSectorObjList);
                }
            }
        }
        private void UploadSubSectorList(List<SubSector> subSectorObjList)
        {
            int ListInsert = 0;
            if (subSectorObjList.Count > 0)
            {
                ListInsert = subSectorManagerObj.SendSubSectorListInfoInToDB(subSectorObjList);
            }
            if (ListInsert == INSERTE || ListInsert == UPDATE)
            {
                Alert.Show("Upload Successfully");
                LoadData("");
            }
            else
            {
                Alert.Show("Error in data ! \n Please check excel file");
            }
        }
        protected void searchbutton_Click(object sender, EventArgs e)
        {
            string searchKey = null;
            if (searchTextBox.Text != "")
            {
                searchKey = searchTextBox.Text;
                if (searchTextBox.Text == "Active")
                {
                    searchKey = "1";
                }
                else if (searchTextBox.Text == "Inactive")
                {
                    searchKey = "0";
                }
                LoadData(searchKey);
            }
            else
            {
                LoadData("");
            }
        }
    }
}
