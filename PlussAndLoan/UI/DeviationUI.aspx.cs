﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_DeviationUI : System.Web.UI.Page
    {
        PddDeviation pddDeviationObj = new PddDeviation();
        PddDeviationManager pddDeviationManagerObj = new PddDeviationManager();
        List<PddDeviation> pddDeviationObjList = new List<PddDeviation>();
        RemarksManager remarksManagerObj = new RemarksManager();

        List<Product> productObj = new List<Product>();
        ProductManager productManagerObj = new ProductManager();

        PLManager pLManagerObj = new PLManager();
        PL pLObj = new PL();

        CustomerManager customerManagerObj = new CustomerManager();
        List<CustomerDetail> customerDetailObjList = new List<CustomerDetail>();
        Customer customerObj = new Customer();

        BankInformationManager bankInformationManagerObj = new BankInformationManager();
        List<BankInformation> bankInformationObjList = new List<BankInformation>();

        SegmentManager segmentManagerObj = new SegmentManager();
        Segment segmentObj = new Segment();

        protected void Page_Load(object sender, EventArgs e)
        {
            Bat.Common.Connection.ConnectionStringName = "conString";
            printButton.Attributes.Add("onclick", "PrintDeviation()");

            if (!IsPostBack)
            {
                List<string> level = new List<string>();
                level.Add("");
                for (int i = 2; i < 4; i++)
                {
                    level.Add(i.ToString());
                }
                levelDropDownList1.DataSource = level;
                levelDropDownList1.DataBind();
                levelDropDownList1.SelectedIndex = 0;
                levelDropDownList2.DataSource = level;
                levelDropDownList2.DataBind();
                levelDropDownList2.SelectedIndex = 0;
                levelDropDownList3.DataSource = level;
                levelDropDownList3.DataBind();
                levelDropDownList3.SelectedIndex = 0;
                levelDropDownList4.DataSource = level;
                levelDropDownList4.DataBind();
                levelDropDownList4.SelectedIndex = 0;

                List<string> descriptionList = new List<string>();
                descriptionList.Add("");
                descriptionDropDownList1.DataSource = descriptionList;
                descriptionDropDownList1.DataBind();
                descriptionDropDownList2.DataSource = descriptionList;
                descriptionDropDownList2.DataBind();
                descriptionDropDownList3.DataSource = descriptionList;
                descriptionDropDownList3.DataBind();
                descriptionDropDownList4.DataSource = descriptionList;
                descriptionDropDownList4.DataBind();

                List<string> productList = new List<string>();
                productList.Add("");
                foreach (Product p in productManagerObj.GetAllProducts())
                {
                    productList.Add(p.ProdName);
                }
                previousRepaymentLoanDropDownList1.DataSource = productList;
                previousRepaymentLoanDropDownList1.DataBind();

                previousRepaymentLoanDropDownList2.DataSource = productList;
                previousRepaymentLoanDropDownList2.DataBind();

                repaymentNotLoanTypeDropDownList.DataSource = productList;
                repaymentNotLoanTypeDropDownList.DataBind();

                FillRemarksGridView();
          

                bankInformationObjList = bankInformationManagerObj.GetAllBankInfo();
                List<string> bankList = new List<string>();
                bankList.Add("");
          
                BankInformation bankInformationOBJ = new BankInformation();
                bankInformationOBJ.Id = 0;
                bankInformationOBJ.Name = "";
                bankInformationObjList.Insert(0, bankInformationOBJ);
                relationshipDropDownList.DataSource = bankInformationObjList;
                relationshipDropDownList.DataTextField = "Name";
                relationshipDropDownList.DataValueField = "Id";
                relationshipDropDownList.DataBind();

                llidTextBox.Text = Session["LLID"] == null ? "" : Convert.ToString(Session["LLID"]);
            }
            relationshipYearTextBox.Attributes.Add("onkeyup", "AutoFormatDate('" + relationshipYearTextBox.ClientID + "');");
            relationshipYearTextBox.Attributes.Add("onblur", "DateFormate('" + relationshipYearTextBox.ClientID + "');");
            ccRepaymentDateTextBox.Attributes.Add("onkeyup", "AutoFormatDate('" + ccRepaymentDateTextBox.ClientID + "');");
            ccRepaymentDateTextBox.Attributes.Add("onblur", "DateFormate('" + ccRepaymentDateTextBox.ClientID + "');");
            previousRepaymentDateTextBox1.Attributes.Add("onkeyup", "AutoFormatDate('" + previousRepaymentDateTextBox1.ClientID + "');");
            previousRepaymentDateTextBox1.Attributes.Add("onblur", "DateFormate('" + previousRepaymentDateTextBox1.ClientID + "');");
            previousRepaymentDateTextBox2.Attributes.Add("onkeyup", "AutoFormatDate('" + previousRepaymentDateTextBox2.ClientID + "');");
            previousRepaymentDateTextBox2.Attributes.Add("onblur", "DateFormate('" + previousRepaymentDateTextBox2.ClientID + "');");

        }


        private void FillRemarksGridView()
        {
            List<Remarks> remarksListObj = remarksManagerObj.SelectRemarksList(1);
            remarksGridView.DataSource = remarksListObj;
            remarksGridView.DataBind();
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //if (hiddenField1.Value != "")
                //{
                if (IsValidPage() && 0 < customerManagerObj.GetCustomerId(llidTextBox.Text))
                {
                    Deviation deviation = new Deviation();

                    if (hiddenField1.Value != "")
                    {
                        deviation.DeviID = Convert.ToInt32(hiddenField1.Value);
                    }
                    else
                    {
                        deviation.DeviID = 0;
                    }

                    deviation.DeviLlid = llidTextBox.Text;
                    //deviation.DeviPlID = Convert.ToInt32(plidTextBox.Text);
                    if (levelDropDownList1.SelectedIndex > 0)
                    {
                        deviation.DeviLevel1 = Convert.ToInt32(levelDropDownList1.SelectedValue.ToString());
                        deviation.DeviDescription1ID = Convert.ToInt32("0" + descriptionDropDownList1.SelectedValue);
                        deviation.DeviCode1 = codeTextBox1.Text;
                    }
                    else
                    {
                        deviation.DeviLevel1 = 0;
                        deviation.DeviDescription1ID = 0;
                        deviation.DeviCode1 = "";
                    }
                    if (levelDropDownList2.SelectedIndex > 0)
                    {
                        deviation.DeviLevel2 = Convert.ToInt32(levelDropDownList2.SelectedValue.ToString());
                        deviation.DeviDescription2ID = Convert.ToInt32("0" + descriptionDropDownList2.SelectedValue);
                        deviation.DeviCode2 = codeTextBox2.Text;
                    }
                    else
                    {
                        deviation.DeviLevel2 = 0;
                        deviation.DeviDescription2ID = 0;
                        deviation.DeviCode2 = "";
                    }
                    if (levelDropDownList3.SelectedIndex > 0)
                    {
                        deviation.DeviLevel3 = Convert.ToInt32(levelDropDownList3.SelectedValue.ToString());
                        deviation.DeviDescription3ID = Convert.ToInt32("0" + descriptionDropDownList3.SelectedValue);
                        deviation.DeviCode3 = codeTextBox3.Text;
                    }
                    else
                    {
                        deviation.DeviLevel3 = 0;
                        deviation.DeviDescription3ID = 0;
                        deviation.DeviCode3 = "";
                    }
                    if (levelDropDownList4.SelectedIndex > 0)
                    {
                        deviation.DeviLevel4 = Convert.ToInt32(levelDropDownList4.SelectedValue.ToString());
                        deviation.DeviDescription4ID = Convert.ToInt32("0" + descriptionDropDownList4.SelectedValue);
                        deviation.DeviCode4 = codeTextBox4.Text;
                    }
                    else
                    {
                        deviation.DeviLevel4 = 0;
                        deviation.DeviDescription4ID = 0;
                        deviation.DeviCode4 = "";
                    }

                    /*---------------- strength box -----------------------------*/
                    //deviation.DeviRelationshipwith1 = relationshipDropDownList.SelectedIndex;
                    try
                    {
                        deviation.DeviRelationshipwith1 = relationshipDropDownList.SelectedValue == "" ? 0 : Convert.ToInt16(relationshipDropDownList.SelectedValue.ToString());
                    }
                    catch (Exception ex)
                    {
                        deviation.DeviRelationshipwith1 = 0;
                    }
                    deviation.DeviRelationshipwith2 = relationshipStatusDropDownList.SelectedIndex;
                    if (DateFormat.IsDate(relationshipYearTextBox.Text))
                    {
                        deviation.DeviSince = Convert.ToDateTime(relationshipYearTextBox.Text, new CultureInfo("ru-RU"));
                    }
                    deviation.DeviResidentialstatus = recedentialStatusDropDownList.SelectedIndex;

                    deviation.DeviCreditcardrepayment = ccRepaymentStatusDropDownList.SelectedIndex;
                    if (ccRepaymentLimitDropDownList.SelectedItem != null)
                    {
                        deviation.DeviLimit = ccRepaymentLimitDropDownList.SelectedItem.Value;
                    }
                    if (ccRepaymentDateTextBox.Text != null && ccRepaymentDateTextBox.Text != "")
                    {
                        deviation.DeviAvailedsince = Convert.ToDateTime(ccRepaymentDateTextBox.Text, new CultureInfo("ru-RU"));
                    }
                    else
                    {
                        deviation.DeviAvailedsince = Convert.ToDateTime("01-01-1800");
                    }

                    deviation.DeviPreviousrepaymentId1 = previousRepaymentStatusDropDownList1.SelectedIndex;
                    deviation.DeviLoantypeId1 = previousRepaymentLoanDropDownList1.SelectedIndex;
                    if (previousRepaymentDateTextBox1.Text != null && previousRepaymentDateTextBox1.Text != "")
                    {
                        deviation.DeviAvailedsince1 = Convert.ToDateTime(previousRepaymentDateTextBox1.Text, new CultureInfo("ru-RU"));
                    }
                    else
                    {
                        deviation.DeviAvailedsince1 = Convert.ToDateTime("01-01-1800");
                    }
                    deviation.DeviPreviousrepaymentId2 = previousRepaymentLoanDropDownList2.SelectedIndex;
                    deviation.DeviLoantypeId2 = previousRepaymentStatusDropDownList2.SelectedIndex;
                    if (previousRepaymentDateTextBox2.Text != null && previousRepaymentDateTextBox2.Text != "")
                    {
                        deviation.DeviAvailedsince2 = Convert.ToDateTime(previousRepaymentDateTextBox2.Text, new CultureInfo("ru-RU"));
                    }
                    else
                    {
                        deviation.DeviAvailedsince2 = Convert.ToDateTime("01-01-1800");
                    }

                    /*----------------- ########## ok ###### ---------------------------*/
                    if (strengthOthersDropDownList.SelectedItem != null) 
                    {
                        deviation.DeviOthersID = strengthOthersDropDownList.SelectedItem.Text;
                    }
             

                    /*----------------- weakness box ---------------------------*/
                    deviation.DeviRepaymentnotsatisfactory = RepaymentNotSatisfactoryDropDownList.SelectedIndex;
                    deviation.DeviLoantypeId3 = repaymentNotLoanTypeDropDownList.SelectedIndex;
                    deviation.DeviDpdID = weaknessDpdDropDownList.SelectedIndex;
                    deviation.DeviCashsalaried = cashSalariedStatusDropDownList.SelectedIndex;
                    deviation.DeviFrequentchangereturn = freqCheckReturnStatusDropDownList.SelectedIndex;
                    deviation.DeviOthers = weaknessOthersTextBox.Text;
                    deviation.UserId = Convert.ToInt32(Session["Id"].ToString());
                    deviation.EntryDate = DateTime.Now;
                    /*------------- hiru vi code -------------------------------*/
                    string remarksData = "";
                    foreach (GridViewRow gvr in remarksGridView.Rows)
                    {
                        if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked == true)
                        {
                            remarksData += (gvr.FindControl("idLabel") as Label).Text + "#" + (gvr.FindControl("remarksTextBox") as TextBox).Text + "*";
                        }
                    }
                    if (remarksData.Length > 0)
                    {
                        remarksData = remarksData.Substring(0, remarksData.Length - 1);
                    }
                    deviation.DeviRemarkscondition1ID = Convert.ToString(RemoveSpecialChar(remarksData.ToString()));
                    /*----------------------------------------------------------*/

                    DeviationManager deviationManagerObj = new DeviationManager();
                    var msg = "";
                    if (saveButton.Text != "Update")
                    {
                        int status = deviationManagerObj.InsertDeviationInfo(deviation);
                        if (status == 1)
                        {
                            msg = "Save successful";
                            this.clearButton_Click(sender, e);
                        }
                        else
                        {
                            msg = "Un-successful";
                        }
                    }
                    else
                    {
                        int status = deviationManagerObj.UpdateDeviationInfo(deviation);
                        if (status == 1)
                        {
                            msg = "Update successful";
                            this.clearButton_Click(sender, e);
                        }
                        else
                        {
                            msg="Un-successful";
                       
                        }
                    }
                    new AT(this).AuditAndTraial("Deviation",msg);
                    Alert.Show(msg);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Deviation Save");
            }
            //Session["LLID"] = "";
            //}
            //else { Alert.Show("Search an existing Data first !"); }
        }

        public string RemoveSpecialChar(string sMessage)
        {
            string returnMessage = "";
            if (!string.IsNullOrEmpty(sMessage))
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private bool IsValidPage()
        {
            if (llidTextBox.Text == "")
            {
                Alert.Show("Please input LLId");
                llidTextBox.Focus();
                return false;
            }
            if (dbrTextBox.Text != "" || tenorTextBox.Text != "" || minIncomeTextBox.Text != "" || mueTextBox.Text != "" || minAgeTextBox.Text != "" || maxAgeTextBox.Text != "" || bancaDBRTextBox.Text != "" || bancaMUETextBox.Text != "")
            {
                if (levelDropDownList1.SelectedIndex == 0 & levelDropDownList2.SelectedIndex == 0 & levelDropDownList3.SelectedIndex == 0 & levelDropDownList4.SelectedIndex == 0)
                {
                    Alert.Show("Please select  Level & Description ");
                    levelDropDownList1.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        protected void searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (llidTextBox.Text != "")
                {
                    pLObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llidTextBox.Text));
                    if (pLObj != null)
                    {
                        if (pLObj.PlId > 0)
                        {
                            List<string> ccLimit = new List<string>();
                            ccLimit.Add("");
                            ccLimit.Add(pLObj.PlScbcreditcardlimit.ToString());
                            ccLimit.Add("Closed");
                            ccRepaymentLimitDropDownList.DataSource = ccLimit;
                            ccRepaymentLimitDropDownList.DataBind();
                            ccRepaymentLimitDropDownList.SelectedIndex = 1;
                            PDDDeviationCheck(pLObj);
                            recedentialStatusDropDownList.SelectedIndex = pLObj.PlResiedentialstatusId + 1;

                            customerDetailObjList = customerManagerObj.GetCustomerDetailInfo(llidTextBox.Text);
                            List<string> disDate = new List<string>();
                            disDate.Add("");
                            foreach (CustomerDetail c in customerDetailObjList)
                            {
                                disDate.Add(c.DisbursDate.ToString("dd-MM-yyyy"));
                            }
                            List<string> othersTextBoxData = new List<string>();
                            othersTextBoxData.Add("");
                            othersTextBoxData.Add("EOSB Assigned");
                            othersTextBoxData.Add("EOSB Assigned & Applicant is from CEP listed Company");
                            othersTextBoxData.Add("Applicant is from CEP listed Company");
                            othersTextBoxData.Add("SCB Staff");

                            customerObj = customerManagerObj.GetCustomerInfo(llidTextBox.Text);
                            if (pLObj.PlSegmentId > 3 && customerObj != null)
                            {
                                othersTextBoxData.Add("Satisfactory utilization of OD facilities with " + customerObj.PDC);
                                othersTextBoxData.Add("a/c relationship with SCB since ");
                            }
                            else
                            {
                                othersTextBoxData.Add("Satisfactory utilization of OD facilities with");
                                othersTextBoxData.Add("a/c relationship with SCB since ");
                            }
                            othersTextBoxData.Add("Has land phone at office / residence");
                            othersTextBoxData.Add("Reflection of LC instruments in statement");

                            strengthOthersDropDownList.DataSource = othersTextBoxData;
                            strengthOthersDropDownList.DataBind();



                            Deviation deviation = null;// new Deviation();
                            List<Deviation> deviationObjList = new List<Deviation>();
                            DeviationManager deviationManagerObj = new DeviationManager();
                            try
                            {
                                deviationObjList = deviationManagerObj.GetAllDeviationInfoByLlId(llidTextBox.Text);
                                if (deviationObjList.Count > 0)
                                {
                                    deviation = new Deviation();
                                    deviation = deviationObjList[0];
                                }
                            }
                            catch { }//Alert.Show("Invalid LL-ID"); }

                            /*---- searchin in deviation table -----*/
                            if (deviation != null)
                            {
                                hiddenField1.Value = deviation.DeviID.ToString();
                                //plidTextBox.Text = deviation.DeviPlID.ToString();
                                if (deviation.DeviLevel1 > 0)
                                {
                                    levelDropDownList1.SelectedValue = deviation.DeviLevel1.ToString();
                                    levelDropDownList1_SelectedIndexChanged(sender, e);
                                    descriptionDropDownList1.SelectedValue = Convert.ToString(deviation.DeviDescription1ID);
                                    codeTextBox1.Text = deviation.DeviCode1;
                                }
                                if (deviation.DeviLevel2 > 0)
                                {
                                    levelDropDownList2.SelectedValue = deviation.DeviLevel2.ToString();
                                    levelDropDownList2_SelectedIndexChanged(sender, e);
                                    descriptionDropDownList2.SelectedValue = Convert.ToString(deviation.DeviDescription2ID);
                                    codeTextBox2.Text = deviation.DeviCode2;
                                }
                                if (deviation.DeviLevel3 > 0)
                                {
                                    levelDropDownList3.SelectedValue = deviation.DeviLevel3.ToString();
                                    levelDropDownList3_SelectedIndexChanged(sender, e);
                                    descriptionDropDownList3.SelectedValue = Convert.ToString(deviation.DeviDescription3ID);
                                    codeTextBox3.Text = deviation.DeviCode3;
                                }
                                if (deviation.DeviLevel4 > 0)
                                {
                                    levelDropDownList4.SelectedValue = deviation.DeviLevel4.ToString();
                                    levelDropDownList4_SelectedIndexChanged(sender, e);
                                    descriptionDropDownList4.SelectedValue = Convert.ToString(deviation.DeviDescription4ID);
                                    codeTextBox4.Text = deviation.DeviCode4;
                                }

                                /*---------------- strength box -----------------------------*/
                                try
                                {
                                    relationshipDropDownList.SelectedValue = deviation.DeviRelationshipwith1.ToString();
                                }
                                catch (Exception ex)
                                {
                                }

                                relationshipStatusDropDownList.SelectedIndex = Convert.ToInt32(deviation.DeviRelationshipwith2);
                                if (DateFormat.IsDate(deviation.DeviSince))
                                {
                                    relationshipYearTextBox.Text = Convert.ToString(deviation.DeviSince.ToString("dd-MM-yyyy"));
                                }
                                recedentialStatusDropDownList.SelectedIndex = Convert.ToInt32(deviation.DeviResidentialstatus);

                                ccRepaymentStatusDropDownList.SelectedIndex = Convert.ToInt32(deviation.DeviCreditcardrepayment);
                                if (deviation.DeviLimit != "")
                                {
                                    ccRepaymentLimitDropDownList.SelectedValue = Convert.ToString(deviation.DeviLimit);
                                }
                                if (DateFormat.IsDate(deviation.DeviAvailedsince))
                                {
                                    ccRepaymentDateTextBox.Text = Convert.ToString(deviation.DeviAvailedsince.ToString("dd-MM-yyyy"));
                                }

                                previousRepaymentStatusDropDownList1.SelectedIndex = Convert.ToInt32(deviation.DeviPreviousrepaymentId1);
                                previousRepaymentLoanDropDownList1.SelectedIndex = Convert.ToInt32(deviation.DeviLoantypeId1);
                                if (DateFormat.IsDate(deviation.DeviAvailedsince1))
                                {
                                    previousRepaymentDateTextBox1.Text = Convert.ToString(deviation.DeviAvailedsince1.ToString("dd-MM-yyyy"));
                                }
                                previousRepaymentLoanDropDownList2.SelectedIndex = Convert.ToInt32(deviation.DeviPreviousrepaymentId2);
                                previousRepaymentStatusDropDownList2.SelectedIndex = Convert.ToInt32(deviation.DeviLoantypeId2);
                                if (DateFormat.IsDate(deviation.DeviAvailedsince2))
                                {
                                    previousRepaymentDateTextBox2.Text = Convert.ToString(deviation.DeviAvailedsince2.ToString("dd-MM-yyyy"));
                                }

                                strengthOthersDropDownList.Text = deviation.DeviOthersID;

                                /*----------------- weakness box ---------------------------*/
                                RepaymentNotSatisfactoryDropDownList.SelectedIndex = Convert.ToInt32(deviation.DeviRepaymentnotsatisfactory);
                                repaymentNotLoanTypeDropDownList.SelectedIndex = Convert.ToInt32(deviation.DeviLoantypeId3);
                                weaknessDpdDropDownList.SelectedIndex = Convert.ToInt32(deviation.DeviDpdID);
                                cashSalariedStatusDropDownList.SelectedIndex = Convert.ToInt32(deviation.DeviCashsalaried);
                                freqCheckReturnStatusDropDownList.SelectedIndex = Convert.ToInt32(deviation.DeviFrequentchangereturn);
                                weaknessOthersTextBox.Text = deviation.DeviOthers;

                                saveButton.Text = "Update";

                                if (deviation.DeviRemarkscondition1ID.Length > 0)
                                {
                                    string[] rowData = deviation.DeviRemarkscondition1ID.Split('*');
                                    for (int i = 0; i < rowData.Length; i++)
                                    {
                                        string[] idData = rowData[i].Split('#');
                                        if (idData[0].Length > 0)
                                        {
                                            foreach (GridViewRow gvr in remarksGridView.Rows)
                                            {
                                                if ((gvr.FindControl("idLabel") as Label).Text == idData[0])
                                                {
                                                    (gvr.FindControl("IdCheckBox") as CheckBox).Checked = true;
                                                    (gvr.FindControl("remarksTextBox") as TextBox).Text = idData[1].ToString();
                                                }
                                            }
                                        }
                                    }
                                }

                            }

                        }
                        if (pLObj != null)
                        {
                            //if (pLObj.Status == 0 && pLObj.UserId.ToString() == Session["Id"].ToString())
                            if (pLObj.Status == 0)
                            {
                                saveButton.Enabled = true;
                            }
                            else
                            {
                                saveButton.Enabled = false;
                            }
                        }
                    }
                }
                else
                { Alert.Show("Give a LLID"); }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Searching Deviation");
            }
        }
        private void PDDDeviationCheck(PL pLObj)
        {

            segmentObj = segmentManagerObj.SelectSegment(pLObj.PlSegmentId);
            //----------------------------DBR Bridge---------------------------------------------
            double plDBR = 0;
            double bridgeDBR = 0;
            if (pLObj.PlSafetyplusId == 1)
            {
                plDBR = pLObj.PlBancaWithoutDBR;
            }
            else
            {
                plDBR = pLObj.PlDbr;
            }
            double value = 0;
            string DBRvalue = "";
            value = Math.Min(pLObj.PlNetincome, pLObj.PlDeclaredincome);
            if (pLObj.PlLoantypeId == 3)//TouUp
            {
                if (value < 29999)
                {
                    DBRvalue = segmentObj.L1MultiPlire29.ToString();
                }
                else if (value < 50000)
                {
                    DBRvalue = segmentObj.L1MultiPlire50.ToString();
                }
                else if (value < 99999)
                {
                    DBRvalue = segmentObj.L1MultiPlire99.ToString();
                }
                else
                {
                    DBRvalue = segmentObj.L1MultiPlire1K.ToString();
                }
            }
            else
            {
                if (value < 29999)
                {
                    DBRvalue = segmentObj.DBR29.ToString();
                }
                else if (value < 50000)
                {
                    DBRvalue = segmentObj.DBR50.ToString();
                }
                else if (value < 99999)
                {
                    DBRvalue = segmentObj.DBR99.ToString();
                }
                else
                {
                    DBRvalue = segmentObj.DBR1K.ToString();
                }
            }

            if (pLObj.PlDbrmultiplier >= plDBR & pLObj.PlLevel == "L1")
            {
                dbrTextBox.Text = "";
                dbrTextBox.BackColor = System.Drawing.Color.White;
            }
            else
            {

                bridgeDBR = plDBR - Convert.ToDouble(DBRvalue);
                //bridgeDBR = pLObj.PlDbrmultiplier-plDBR;
                if (plDBR <= Convert.ToDouble(DBRvalue))
                {
                    dbrTextBox.Text = "";
                    dbrTextBox.BackColor = System.Drawing.Color.White;
                }
                else if (plDBR > Convert.ToDouble(DBRvalue))
                {
                    if (bridgeDBR / 100 > 0.05)
                    {
                        dbrTextBox.Text = "3";
                        dbrTextBox.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        dbrTextBox.Text = "2";
                        dbrTextBox.BackColor = System.Drawing.Color.Violet;
                    }
                }
            }
            //}
            //---------------------------------End DBR Bridge------------------------------------
            //--------------------------------Tenor Bridge--------------------------------------
            double tempTenor = 0;

            switch (pLObj.PlSegmentId.ToString())
            {
                case "1":
                case "2":
                case "3":
                case "4":
                case "6":
                case "7":
                case "8":
                case "9":
                case "10":
                case "11":
                case "12":
                case "13":
                case "14":
                case "15":
                    if (pLObj.PlProposedloanamount < segmentObj.MinLoanAmoutn)
                    {
                        tempTenor = 24;
                    }
                    else
                    {
                        if (pLObj.PlProposedloanamount < 100000)
                        {
                            tempTenor = 24;
                        }
                        else if (pLObj.PlProposedloanamount < 250000)
                        {
                            tempTenor = 48;
                        }
                        else
                        {
                            tempTenor = 60;
                        }
                    }
                    if (pLObj.PlTenure <= tempTenor)
                    {
                        tenorTextBox.Text = "";
                        tenorTextBox.BackColor = System.Drawing.Color.White;
                    }
                    else if (pLObj.PlTenure > tempTenor)
                    {
                        tenorTextBox.Text = "2";
                        tenorTextBox.BackColor = System.Drawing.Color.Violet;
                    }
                    break;
                case "5":

                    if (pLObj.PlTenure <= 24)
                    {
                        tenorTextBox.Text = "";
                        tenorTextBox.BackColor = System.Drawing.Color.White;
                    }
                    else if (pLObj.PlTenure > 24)
                    {
                        tenorTextBox.Text = "2";
                        tenorTextBox.BackColor = System.Drawing.Color.Violet;
                    }
                    else if (pLObj.PlTenure > 48)
                    {
                        tenorTextBox.Text = "3";
                        tenorTextBox.BackColor = System.Drawing.Color.Red;
                    }
                    break;
            }

            //--------------------------------End Tenor Bridge--------------------------------------
            //--------------------------------Min Income Bridge--------------------------------------
            if (Math.Min(pLObj.PlNetincome, pLObj.PlDeclaredincome) >= segmentObj.MinIncome)
            {
                minIncomeTextBox.Text = "";
                minIncomeTextBox.BackColor = System.Drawing.Color.White;
            }
            else if (Math.Min(pLObj.PlNetincome, pLObj.PlDeclaredincome) < segmentObj.MinIncome)
            {
                if (segmentObj.MinIncome - Math.Min(pLObj.PlNetincome, pLObj.PlDeclaredincome) > segmentObj.MinIncome * 0.1)
                {
                    minIncomeTextBox.Text = "3";
                    minIncomeTextBox.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    minIncomeTextBox.Text = "2";
                    minIncomeTextBox.BackColor = System.Drawing.Color.Violet;
                }
            }
            //--------------------------------Min Income Bridge--------------------------------------
            //------------------------MUE Bridge------------------------------------
            //if ((pLObj.PlProposedloanamount + pLObj.PlScbcreditcardlimit + pLObj.PlPliplfloutstanding - pLObj.PlFlsecuityos) / Math.Min(pLObj.PlNetincome, pLObj.PlDeclaredincome) > pLObj.PlDbrmultiplier)

            if (pLObj.PlProposedloanamount > pLObj.PlMuemaxloanamount)
            {
                mueTextBox.Text = "3";
                mueTextBox.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                mueTextBox.Text = String.Empty;
                mueTextBox.BackColor = System.Drawing.Color.White;
            }
            //double muebridge = pLObj.PlMue - pLObj.PlMuemultiplier;
            //if (muebridge > 0.5)
            //{
            //    mueTextBox.Text = "3";
            //    mueTextBox.BackColor = System.Drawing.Color.Red;
            //}
            //else
            //{
            //    mueTextBox.Text = String.Empty;
            //    mueTextBox.BackColor = System.Drawing.Color.White;
            //}
            //-----------------------End MUE Bridge-------------------------------------
            //-----------------------Min Age Bridge-------------------------------------
            LoanInformationManager loanInformationManagerObj = new LoanInformationManager();
            LoanInformation loanInformationObj = new LoanInformation();
            loanInformationObj = loanInformationManagerObj.GetParticularApplicantLoanInfo(Convert.ToInt32(llidTextBox.Text));
            int bridgeMinAge = 0;
            int bridgeMaxAge = 0;
            if (pLObj.PlSegmentId == 12 || pLObj.PlSegmentId == 13) // doctor
            {
                if (DateFormat.IsDate(loanInformationObj.DOB))
                {
                    bridgeMinAge = AgeCalculation(loanInformationObj.DOB, DateTime.Now);
                    if (bridgeMinAge < 25)
                    {
                        minAgeTextBox.Text = "3";
                        minAgeTextBox.BackColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        minAgeTextBox.Text = "";
                        minAgeTextBox.BackColor = System.Drawing.Color.White;
                    }
                }
            }
            else
            {
                if (pLObj.PlSegmentId == 1 || pLObj.PlSegmentId == 2 || pLObj.PlSegmentId == 3 || pLObj.PlSegmentId == 4 || pLObj.PlSegmentId == 5)
                {
                    if (DateFormat.IsDate(loanInformationObj.DOB))
                    {
                        bridgeMinAge = AgeCalculation(loanInformationObj.DOB, DateTime.Now);
                        if (bridgeMinAge < 21)
                        {
                            minAgeTextBox.Text = "3";
                            minAgeTextBox.BackColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            minAgeTextBox.Text = "";
                            minAgeTextBox.BackColor = System.Drawing.Color.White;
                        }
                    }
                }
                else
                {
                    if (DateFormat.IsDate(loanInformationObj.DOB))
                    {
                        bridgeMinAge = AgeCalculation(loanInformationObj.DOB, DateTime.Now);
                        if (bridgeMinAge < 23)
                        {
                            minAgeTextBox.Text = "3";
                            minAgeTextBox.BackColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            minAgeTextBox.Text = "";
                            minAgeTextBox.BackColor = System.Drawing.Color.White;
                        }
                    }
                }
            }
            //-----------------------End Min Age Bridge-------------------------------------
            //-----------------------Max Age Bridge-------------------------------------
            try      // using try catch because there is no chance of error if order of data entry is maintained. but if not maintained and a data is inserted manuaaly the to handle esception, using try catch
            {
                if (DateFormat.IsDate(loanInformationObj.DOB))
                {
                    bridgeMaxAge = AgeCalculation(loanInformationObj.DOB, DateTime.Now);
                    if ((bridgeMaxAge + (segmentObj.Tenor / 12)) > 65)
                    {
                        if ((bridgeMaxAge + (segmentObj.Tenor / 12)) > 68)
                        {
                            maxAgeTextBox.Text = "3";
                            maxAgeTextBox.BackColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            maxAgeTextBox.Text = "2";
                            maxAgeTextBox.BackColor = System.Drawing.Color.Violet;
                        }
                    }
                    else
                    {
                        maxAgeTextBox.Text = "";
                        maxAgeTextBox.BackColor = System.Drawing.Color.White;
                    }
                }
            }
            catch { }
            //-----------------------End Max Age Bridge-------------------------------------
            //----------------------------Banca DBR Bridge---------------------------------------------
            if (pLObj.PlSafetyplusId == 1)
            {
                if (pLObj.PlBancaDBR > 4 && pLObj.PlBancaDBR < 6)
                {
                    bancaDBRTextBox.Text = "2";
                    bancaDBRTextBox.BackColor = System.Drawing.Color.Violet;
                }
                else if (pLObj.PlBancaDBR > 6)
                {
                    bancaDBRTextBox.Text = "3";
                    bancaDBRTextBox.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    bancaDBRTextBox.Text = "";
                    bancaDBRTextBox.BackColor = System.Drawing.Color.White;
                }
            }
            else
            {
                bancaDBRTextBox.Text = "";
                bancaDBRTextBox.BackColor = System.Drawing.Color.White;
            }
            //---------------------------------End Banca DBR Bridge------------------------------------
            //----------------------------Banca MUE Bridge---------------------------------------------
            if (pLObj.PlSafetyplusId == 1)
            {
                if (pLObj.PlBancaMUE > 0.5 & pLObj.PlBancaMUE < 1)
                {
                    bancaMUETextBox.Text = "2";
                    bancaMUETextBox.BackColor = System.Drawing.Color.Violet;
                }
                else if (pLObj.PlBancaMUE > 1)
                {
                    bancaMUETextBox.Text = "3";
                    bancaMUETextBox.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    bancaMUETextBox.Text = "";
                    bancaMUETextBox.BackColor = System.Drawing.Color.White;
                }
            }
            else
            {
                bancaMUETextBox.Text = "";
                bancaMUETextBox.BackColor = System.Drawing.Color.White;
            }
            //---------------------------------End Banca MUE Bridge------------------------------------
        }

        public int AgeCalculation(DateTime myDOB, DateTime FutureDate)
        {
            int years = 0;
            int months = 0;
            int days = 0;

            DateTime tmpMyDOB = new DateTime(myDOB.Year, myDOB.Month, 1);

            DateTime tmpFutureDate = new DateTime(FutureDate.Year, FutureDate.Month, 1);

            while (tmpMyDOB.AddYears(years).AddMonths(months) < tmpFutureDate)
            {
                months++;
                if (months > 12)
                {
                    years++;
                    months = months - 12;
                }
            }

            if (FutureDate.Day >= myDOB.Day)
            {
                days = days + FutureDate.Day - myDOB.Day;
            }
            else
            {
                months--;
                if (months < 0)
                {
                    years--;
                    months = months + 12;
                }
                days +=
                    DateTime.DaysInMonth(
                        FutureDate.AddMonths(-1).Year, FutureDate.AddMonths(-1).Month
                        ) + FutureDate.Day - myDOB.Day;

            }

            if (DateTime.IsLeapYear(myDOB.Year) && myDOB.Month == 2 && myDOB.Day == 29)
            {
                if (FutureDate >= new DateTime(FutureDate.Year, 3, 1))
                    days++;
            }
            //string strAge = years + " Years " + months + " Months";
            return years;
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            hiddenField1.Value = "";
            llidTextBox.Text = "";
            //plidTextBox.Text = "";
            maxAgeTextBox.BackColor = System.Drawing.Color.White;
            minAgeTextBox.BackColor = System.Drawing.Color.White;
            mueTextBox.BackColor = System.Drawing.Color.White;
            minIncomeTextBox.BackColor = System.Drawing.Color.White;
            tenorTextBox.BackColor = System.Drawing.Color.White;
            dbrTextBox.BackColor = System.Drawing.Color.White;
            levelDropDownList1.SelectedIndex = 0;
            levelDropDownList1_SelectedIndexChanged(sender, e);
            codeTextBox1.Text = "";
            levelDropDownList2.SelectedIndex = 0;
            levelDropDownList2_SelectedIndexChanged(sender, e);
            codeTextBox2.Text = "";
            levelDropDownList3.SelectedIndex = 0;
            levelDropDownList3_SelectedIndexChanged(sender, e);
            codeTextBox3.Text = "";
            levelDropDownList4.SelectedIndex = 0;
            levelDropDownList4_SelectedIndexChanged(sender, e);
            codeTextBox4.Text = "";

            /*---------------- strength box -----------------------------*/
            relationshipDropDownList.SelectedIndex = 0;
            relationshipStatusDropDownList.SelectedIndex = 0;
            relationshipYearTextBox.Text = "";
            recedentialStatusDropDownList.SelectedIndex = 0;

            ccRepaymentStatusDropDownList.SelectedIndex = 0;
            ccRepaymentLimitDropDownList.SelectedIndex = 0;
            ccRepaymentDateTextBox.Text = "";

            previousRepaymentLoanDropDownList1.SelectedIndex = 0;
            previousRepaymentStatusDropDownList1.SelectedIndex = 0;
            previousRepaymentDateTextBox1.Text = "";
            previousRepaymentLoanDropDownList2.SelectedIndex = 0;
            previousRepaymentStatusDropDownList2.SelectedIndex = 0;
            previousRepaymentDateTextBox2.Text = "";
            strengthOthersDropDownList.SelectedIndex = 0;

            /*----------------- weakness box ---------------------------*/
            RepaymentNotSatisfactoryDropDownList.SelectedIndex = 0;
            repaymentNotLoanTypeDropDownList.SelectedIndex = 0;
            weaknessDpdDropDownList.SelectedIndex = 0;
            cashSalariedStatusDropDownList.SelectedIndex = 0;
            freqCheckReturnStatusDropDownList.SelectedIndex = 0;
            weaknessOthersTextBox.Text = "";

            saveButton.Text = "Save";
            saveButton.Enabled = true;
            FillRemarksGridView();

            dbrTextBox.Text = "";
            tenorTextBox.Text = "";
            minIncomeTextBox.Text = "";
            mueTextBox.Text = "";
            minAgeTextBox.Text = "";
            maxAgeTextBox.Text = "";
        }


        protected void levelDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (levelDropDownList1.SelectedValue != "")
            {
                pddDeviationObjList = pddDeviationManagerObj.GetPddDeviationOnLevel(Convert.ToInt32(levelDropDownList1.SelectedValue));
                PddDeviation pddDeviationObj = new PddDeviation();
                pddDeviationObj.Id = 0;
                pddDeviationObj.Description = "";
                pddDeviationObjList.Insert(0, pddDeviationObj);
                descriptionDropDownList1.DataSource = pddDeviationObjList;
                descriptionDropDownList1.DataTextField = "Description";
                descriptionDropDownList1.DataValueField = "Id";
                descriptionDropDownList1.DataBind();
                if (descriptionDropDownList1.SelectedValue != "0")
                {
                    codeTextBox1.Text = pddDeviationManagerObj.GetPddDeviationCode(descriptionDropDownList1.SelectedValue);
                }
                else
                { codeTextBox1.Text = ""; }
            }

            else
            {
                descriptionDropDownList1.Items.Clear();
                codeTextBox1.Text = "";
            }
        }
        protected void levelDropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (levelDropDownList2.SelectedValue != "")
            {
                pddDeviationObjList = pddDeviationManagerObj.GetPddDeviationOnLevel(Convert.ToInt32(levelDropDownList2.SelectedValue));
                PddDeviation pddDeviationObj = new PddDeviation();
                pddDeviationObj.Id = 0;
                pddDeviationObj.Description = "";
                pddDeviationObjList.Insert(0, pddDeviationObj);
                descriptionDropDownList2.DataSource = pddDeviationObjList;
                descriptionDropDownList2.DataTextField = "Description";
                descriptionDropDownList2.DataValueField = "Id";
                descriptionDropDownList2.DataBind();
                if (descriptionDropDownList2.SelectedValue != "0")
                {
                    codeTextBox2.Text = pddDeviationManagerObj.GetPddDeviationCode(descriptionDropDownList2.SelectedValue);
                }
                else
                { codeTextBox2.Text = ""; }
            }
            else
            {
                descriptionDropDownList2.Items.Clear();
                codeTextBox2.Text = "";
            }
        }
        protected void levelDropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (levelDropDownList3.SelectedValue != "")
            {
                pddDeviationObjList = pddDeviationManagerObj.GetPddDeviationOnLevel(Convert.ToInt32(levelDropDownList3.SelectedValue));
                PddDeviation pddDeviationObj = new PddDeviation();
                pddDeviationObj.Id = 0;
                pddDeviationObj.Description = "";
                pddDeviationObjList.Insert(0, pddDeviationObj);
                descriptionDropDownList3.DataSource = pddDeviationObjList;
                descriptionDropDownList3.DataTextField = "Description";
                descriptionDropDownList3.DataValueField = "Id";
                descriptionDropDownList3.DataBind();
                if (descriptionDropDownList3.SelectedValue != "0")
                {
                    codeTextBox3.Text = pddDeviationManagerObj.GetPddDeviationCode(descriptionDropDownList3.SelectedValue);
                }
                else
                { codeTextBox3.Text = ""; }
            }
            else
            {
                descriptionDropDownList3.Items.Clear();
                codeTextBox3.Text = "";
            }
        }
        protected void levelDropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (levelDropDownList4.SelectedValue != "")
            {
                pddDeviationObjList = pddDeviationManagerObj.GetPddDeviationOnLevel(Convert.ToInt32(levelDropDownList4.SelectedValue));
                PddDeviation pddDeviationObj = new PddDeviation();
                pddDeviationObj.Id = 0;
                pddDeviationObj.Description = "";
                pddDeviationObjList.Insert(0, pddDeviationObj);
                descriptionDropDownList4.DataSource = pddDeviationObjList;
                descriptionDropDownList4.DataTextField = "Description";
                descriptionDropDownList4.DataValueField = "Id";
                descriptionDropDownList4.DataBind();
                if (descriptionDropDownList4.SelectedValue != "0")
                {
                    codeTextBox4.Text = pddDeviationManagerObj.GetPddDeviationCode(descriptionDropDownList4.SelectedValue);
                }
                else
                { codeTextBox4.Text = ""; }
            }
            else
            {
                descriptionDropDownList4.Items.Clear();
                codeTextBox4.Text = "";
            }
        }

        protected void descriptionDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (descriptionDropDownList1.SelectedIndex > 0)
            {
                codeTextBox1.Text = pddDeviationManagerObj.GetPddDeviationCode(descriptionDropDownList1.SelectedValue);
            }
            else
            { codeTextBox1.Text = ""; }
        }
        protected void descriptionDropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (descriptionDropDownList2.SelectedIndex > 0)
            {
                codeTextBox2.Text = pddDeviationManagerObj.GetPddDeviationCode(descriptionDropDownList2.SelectedValue);
            }
            else
            { codeTextBox2.Text = ""; }
        }
        protected void descriptionDropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (descriptionDropDownList3.SelectedIndex > 0)
            {
                codeTextBox3.Text = pddDeviationManagerObj.GetPddDeviationCode(descriptionDropDownList3.SelectedValue);
            }
            else
            { codeTextBox3.Text = ""; }
        }
        protected void descriptionDropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (descriptionDropDownList4.SelectedIndex > 0)
            {
                codeTextBox4.Text = pddDeviationManagerObj.GetPddDeviationCode(descriptionDropDownList4.SelectedValue);
            }
            else
            { codeTextBox4.Text = ""; }
        }

    }
}

