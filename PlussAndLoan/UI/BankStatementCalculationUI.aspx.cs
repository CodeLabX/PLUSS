﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Xml;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BankStatementCalculationUI : System.Web.UI.Page
    {
        private string lLId = string.Empty;
        private string bankId = string.Empty;
        private string branchId = string.Empty;
        private string aCNo = string.Empty;
        private string aCName = string.Empty;
        private string acTypeId = string.Empty;
        private string bankName = string.Empty;
        private string branchName = string.Empty;
        private BankStatementManager bankStatementManagerObj = new BankStatementManager();
        private ExcelTemplateManager excelTemplateManagerObj = new ExcelTemplateManager();
        private BankStatementCalculationManager BSCManagerObj = null;
        public RemarksManager remarksManagerObj = null;
        private XmlDocument xmlDoc = new XmlDocument();
        private XmlNode msgDataNode;
        private ScbManager scbManagerObj = null;
        public ApplicantOtherBankAccount otherBankAccountObj = null;
        public BankStatement bankStatementObj = null;
        public SCBBankNameListManager SCBBNListManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {

            otherBankAccountObj = new ApplicantOtherBankAccount();
            bankStatementManagerObj = new BankStatementManager();
            SCBBNListManagerObj = new SCBBankNameListManager();
            BSCManagerObj = new BankStatementCalculationManager();
            remarksManagerObj = new RemarksManager();
            scbManagerObj = new ScbManager();
            try
            {
                string Id = string.Empty;
                string updatedBy = string.Empty;
                string strOperation = string.Empty;
                strOperation = Convert.ToString(Request.QueryString["operation"]);
                lLId = Convert.ToString(Request.QueryString["LLId"]);
                bankId = Convert.ToString(Request.QueryString["BANKID"]);
                branchId = Convert.ToString(Request.QueryString["BRANCHID"]);
                aCNo = Convert.ToString(Request.QueryString["ACNO"]);
                aCName = Convert.ToString(Request.QueryString["ACNAME"]);
                acTypeId = Convert.ToString(Request.QueryString["ACTYPEID"]);
                Id = Convert.ToString(Request.QueryString["ID"]);
                string startIndex = Convert.ToString(Request.QueryString["StartIndex"]);
                string endIndex = Convert.ToString(Request.QueryString["EndIndex"]);
                string itemIndex = Convert.ToString(Request.QueryString["NumOfItem"]);
                string masterId = Convert.ToString(Request.QueryString["MASTERID"]);
                bankName = Convert.ToString(Request.QueryString["BANKNAME"]);
                branchName = Convert.ToString(Request.QueryString["BRANCHNAME"]);
                if (strOperation == "save")
                {
                    StreamReader streamReader = new StreamReader(Request.InputStream);
                    string spreadsheetdata = streamReader.ReadToEnd();
                    Response.Write(SaveStatementData(spreadsheetdata));
                    //Response.End();
                }
                else if (strOperation == "loadTemplate")
                {
                    if (lLId.Length > 0 & bankId.Length > 0 & branchId.Length > 0 & aCNo.Length > 0)
                    {
                        Response.Write(GetStatementData(Convert.ToInt64(lLId), Convert.ToInt16(bankId), Convert.ToInt16(branchId), aCNo));
                    }
                }
                else if (strOperation == "LoadDefaultTemplate")
                {
                    if (Id.Length > 0)
                    {
                        Response.Write(GetTemplate(6));
                        //Response.End();
                    }
                }
                else if (strOperation == "Calculation")
                {
                    StreamReader streamReader2 = new StreamReader(Request.InputStream);
                    string spreadsheetdata2 = streamReader2.ReadToEnd();
                    if (bankName.Equals("SCB"))
                    {
                        if (!string.IsNullOrEmpty(masterId))
                        {
                            Response.Write(GetSCBHistorycalData(Convert.ToInt32(masterId), startIndex, endIndex, itemIndex));
                        }
                    }
                    else
                    {
                        Response.Write(GetCalculationData(spreadsheetdata2, startIndex, endIndex, itemIndex));
                    }
                    //Response.End();
                }
                else if (strOperation == "saveCalculation")
                {
                    StreamReader streamReaderObj = new StreamReader(Request.InputStream);
                    string calXMLData = streamReaderObj.ReadToEnd();
                    Response.Write(SaveCalculation(calXMLData));
                    //Response.End();
                }
                else if (strOperation == "LoadExistingData")
                {
                    StreamReader SRLLIdObj = new StreamReader(Request.InputStream);
                    string srLLId = SRLLIdObj.ReadToEnd();
                    Response.Write(LoadExistCalculationData(srLLId));
                    //Response.End();
                }
            }
            catch (Exception ex)
            {
                Response.Write("error occured");
                Response.End();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();calculationGridOnLoad();", true);
            if (!Page.IsPostBack)
            {
                FillRemarksGridView();
            }

           
        }
        private void FillRemarksGridView()
        {
            List<Remarks> remarksListObj = remarksManagerObj.SelectRemarksList(3);
            remarksGridView.DataSource = remarksListObj;
            remarksGridView.DataBind();
        }
        private String SaveCalculation(string calXMLData)
        {
            string sReturnData = String.Empty;
            string[] splitData = calXMLData.Split('#');
            string[] headData = splitData[0].Split(',');
            BankStatementCalculation BSCObj = new BankStatementCalculation();
            BSCObj.Id = 0;
            BSCObj.LlId = Convert.ToInt64(headData[0].ToString());
            BSCObj.Total12MonthCreditTurnover = Convert.ToDouble(headData[1].ToString());
            BSCObj.Average12MonthCreditTurnover = Convert.ToDouble(headData[2].ToString());
            BSCObj.Total6MonthCreditTurnover = Convert.ToDouble(headData[3].ToString());
            BSCObj.Average6MonthCreditTurnover = Convert.ToDouble(headData[4].ToString());

            BSCObj.Total12MonthAverageBalance = Convert.ToDouble(headData[5].ToString());
            BSCObj.Average12MonthAverageBalance = Convert.ToDouble(headData[6].ToString());
            BSCObj.Month1250PerAverageBalance = Convert.ToDouble(headData[9].ToString());
            BSCObj.Total6MonthAverageBalance = Convert.ToDouble(headData[8].ToString());
            BSCObj.Average6MonthAverageBalance =Convert.ToDouble(headData[7].ToString());
            BSCObj.Month650PerAverageBalance = Convert.ToDouble(headData[10].ToString());

            BSCObj.Month12MaxBalanceDate = Convert.ToInt16(headData[11].ToString());
            BSCObj.Month6MaxBalanceDate = Convert.ToInt16(headData[12].ToString());    
        
            BSCObj.CreditPart = Convert.ToInt32(headData[13].ToString());
            BSCObj.AveragePart = Convert.ToInt32(headData[14].ToString());

            BSCObj.Remarks = Convert.ToString(headData[15].ToString());

            BSCObj.XmlData = Convert.ToString(splitData[1].Trim().ToString());
            BSCObj.EntryDate = DateTime.Now;
            BSCObj.UserId = Convert.ToInt32(Session["Id"].ToString()); 
            int n=BSCManagerObj.SendBankStatementCalculationInToDB(BSCObj);
            new AT(this).AuditAndTraial("Save Bank Statement Calculation", "Save Successfully");
            return sReturnData = "Save Successfully  #";
        }
        private String LoadExistCalculationData(string LLID)
        {
            string sReturnData = String.Empty;

            string sHeadData = String.Empty;
            string sXmlData = String.Empty;

            if (LLID.Length > 1)
            {
                BankStatementCalculation BSCObj = BSCManagerObj.GetBankStatementCalculation(Convert.ToInt64(LLID.Trim().ToString()));
                if (BSCObj!=null)
                {
                    sHeadData = BSCObj.LlId.ToString() + "," +
                                BSCObj.Total12MonthCreditTurnover.ToString() + "," +
                                BSCObj.Total12MonthAverageBalance.ToString() + "," +
                                BSCObj.Average12MonthCreditTurnover.ToString() + "," +
                                BSCObj.Average12MonthAverageBalance.ToString() + "," +
                                BSCObj.Month1250PerAverageBalance.ToString() + "," +
                                BSCObj.Month12MaxBalanceDate.ToString() + "," +
                                BSCObj.Total6MonthCreditTurnover.ToString() + "," +
                                BSCObj.Total6MonthAverageBalance.ToString() + "," +
                                BSCObj.Average6MonthCreditTurnover.ToString() + "," +
                                BSCObj.Average6MonthAverageBalance.ToString() + "," +
                                BSCObj.Month650PerAverageBalance.ToString() + "," +
                                BSCObj.Month6MaxBalanceDate.ToString() + "," +
                                BSCObj.CreditPart.ToString() + "," +
                                BSCObj.AveragePart.ToString() + "," +
                                BSCObj.Remarks.ToString();
                    sXmlData = BSCObj.XmlData.ToString(); 
                }
           
            }
            return sReturnData = sHeadData + "#" + sXmlData + "*";
        }
        private string GetMonth(string MonthName)
        {
            string retuValue = string.Empty;
            switch (MonthName)
            {
                case "Jan":
                    retuValue = "01";
                    break;
                case "Feb":
                    retuValue = "02";
                    break;
                case "Mar":
                    retuValue = "03";
                    break;
                case "Apr":
                    retuValue = "04";
                    break;
                case "May":
                    retuValue = "05";
                    break;
                case "Jun":
                    retuValue = "06";
                    break;
                case "Jul":
                    retuValue = "07";
                    break;
                case "Aug":
                    retuValue = "08";
                    break;
                case "Sep":
                    retuValue = "09";
                    break;
                case "Oct":
                    retuValue = "10";
                    break;
                case "Nov":
                    retuValue = "11";
                    break;
                case "Dec":
                    retuValue = "12";
                    break;
            }
            return retuValue;
        }
        private String GetSCBHistorycalData(Int32 masterId,string startMonth, string endMonth, string xmlRowId)
        {
            string[] sdate = startMonth.Split('-');
            string[] edate = endMonth.Split('-');
            startMonth = "20" + sdate[1] + "-" + GetMonth(sdate[0]) + "-01";
            endMonth = "20" + edate[1] + "-" + GetMonth(edate[0]) + "-01";
            string sReturnData = String.Empty;
            StringBuilder calculationDatsSB = new StringBuilder();
            int columnIndex = 0;
            int rowIndex = 0;
            string maxBalDate = "0";
            string monthName = string.Empty;
            List<ScbDetails> scbDetailsObjList = scbManagerObj.GetScbDetailsData(masterId, Convert.ToDateTime(startMonth), Convert.ToDateTime(endMonth));
            try
            {
                if (scbDetailsObjList.Count > 0)
                {
                    Int32 index = 1, tempIndex = 0;
                    Int32 ConditionNum = 0;

                    xmlRowId = (Convert.ToInt32(xmlRowId) + 1).ToString();
                    calculationDatsSB.Append("<rows>");
                    foreach (ScbDetails scbDetailsObj in scbDetailsObjList)
                    {
                        calculationDatsSB.Append("<row id='" + xmlRowId + "'>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(xmlRowId);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(bankId);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(bankName);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(branchId);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(branchName);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(aCNo);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(acTypeId);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(aCName);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(Convert.ToDateTime(scbDetailsObj.scbdPeriod).ToString("MMM-yy"));
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        if (!string.IsNullOrEmpty(scbDetailsObj.scbdAdjustmentcto.ToString()))
                        {
                            calculationDatsSB.Append(Convert.ToDouble(scbDetailsObj.scbdAdjustmentcto).ToString("##,###"));
                        }
                        else
                        {
                            calculationDatsSB.Append("0");
                        }
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(scbDetailsObj.scbdAdjmonthcto.ToString());
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        if (!string.IsNullOrEmpty(scbDetailsObj.scbdAverageledgerbalance.ToString()))
                        {
                            calculationDatsSB.Append(Convert.ToDouble(scbDetailsObj.scbdAverageledgerbalance).ToString("##,###"));
                        }
                        else
                        {
                            calculationDatsSB.Append("0");
                        }
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(maxBalDate);
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("<cell>");
                        calculationDatsSB.Append(scbDetailsObj.scbdAdjmontaveragebalance.ToString());
                        calculationDatsSB.Append("</cell>");
                        calculationDatsSB.Append("</row>");
                        xmlRowId = (Convert.ToInt32(xmlRowId) + 1).ToString();
                        if (Convert.ToInt32(xmlRowId) == 13)
                        {
                            break;
                        }

                    }
                    calculationDatsSB.Append("</rows>");
                    calculationDatsSB.Append("#");
                }
            }
            catch (Exception ex)
            {
                string erro = columnIndex.ToString() + "  " + rowIndex.ToString();
            }
            return calculationDatsSB.ToString();
        }
        private String GetCalculationData(string sData, string startMonth, string endMonth, string xmlRowId)
        {
            double AverageBalSum=0;
            double CreditTurnOverSum = 0;
            string sReturnData = String.Empty;
            StringBuilder calculationDatsSB = new StringBuilder();
            XmlNodeList dataNodeCellList;
            int columnIndex = 0;
            int rowIndex = 0;
            string maxBalDate = string.Empty;
            string monthName = string.Empty;
            List<string> nodedataList2 = new List<string>();
            try
            {
                string parseData = ParseValue(sData, "ss:Table");
                parseData = parseData.Replace("ss:table", "Table").Replace("ss:", "");
                xmlDoc.LoadXml(parseData);
                List<string> nodedataList = new List<string>();            
                dataNodeCellList = xmlDoc.SelectNodes("//Row");
                xmlDoc.LoadXml("<Table>" + dataNodeCellList[1].InnerXml + "</Table>");
                XmlNodeList xmlNodeList2 = xmlDoc.SelectNodes("//Cell");
                Int32 index = 1,tempIndex=0;
                Int32 ConditionNum = 0;
                for(int i=0; i<12;i++)
                {
                    if (Convert.ToDateTime(xmlNodeList2.Item(i).InnerText).ToString("MMM-yy") == startMonth)
                    {
                        tempIndex = index;
                    }
                    if (Convert.ToDateTime(xmlNodeList2.Item(i).InnerText).ToString("MMM-yy") == endMonth)
                    {
                        ConditionNum = index;
                    }
                    index = index + 1;
                }
                //calculationDatsSB.Append("<?xml version='1.0' encoding='iso-8859-1'?>");
                xmlRowId = (Convert.ToInt32(xmlRowId) + 1).ToString();
                calculationDatsSB.Append("<rows>");
                for (columnIndex = tempIndex; columnIndex <= ConditionNum; columnIndex++)
                {
                    for (rowIndex = 1; rowIndex < dataNodeCellList.Count; rowIndex++)
                    {
                        xmlDoc.LoadXml("<Table>" + dataNodeCellList[rowIndex].InnerXml + "</Table>");
                        XmlNodeList xmlNodeList = xmlDoc.SelectNodes("//Cell");

                        if (rowIndex == 6)
                        {
                            maxBalDate = xmlNodeList.Item(columnIndex - 1).InnerText;
                        }
                        if (rowIndex < 6 && rowIndex > 1)
                        {
                            AverageBalSum = AverageBalSum + Convert.ToDouble("0" + xmlNodeList.Item(columnIndex).InnerText);
                        }
                        if (rowIndex > 9)
                        {
                            CreditTurnOverSum = CreditTurnOverSum + Convert.ToDouble("0" + xmlNodeList.Item(columnIndex - 1).InnerText);
                        }
                        if (rowIndex == 1)
                        {
                            monthName = xmlNodeList.Item(columnIndex - 1).InnerText;
                        }
                    }
                    //nodedataList.Add(AverageBalSum.ToString() + "   " + CreditTurnOverSum.ToString());
                    calculationDatsSB.Append("<row id='" + xmlRowId + "'>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(xmlRowId);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(bankId);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(bankName);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(branchId);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(branchName);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(aCNo);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(acTypeId);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(aCName);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(Convert.ToDateTime(monthName).ToString("MMM-yy"));
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append((CreditTurnOverSum * 1000).ToString("##,###"));
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append("0");
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append((Math.Round(((AverageBalSum * 1000))/4)).ToString("##,###"));
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append(maxBalDate);
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("<cell>");
                    calculationDatsSB.Append("0");
                    calculationDatsSB.Append("</cell>");
                    calculationDatsSB.Append("</row>");
                    AverageBalSum = 0;
                    CreditTurnOverSum = 0;
                    xmlRowId = (Convert.ToInt32(xmlRowId) + 1).ToString();
                    if (Convert.ToInt32(xmlRowId) == 13)
                    {
                        break;
                    }
                
                }
                calculationDatsSB.Append("</rows>");
                calculationDatsSB.Append("#");

            }
            catch(Exception ex)
            {
                string erro = columnIndex.ToString() + "  " + rowIndex.ToString();
            }
            return calculationDatsSB.ToString();
        }

    
    
    
        private String GetTemplate(Int32 Id)
        {
            string sReturnData = String.Empty;
            List<ExcelTemplate> excelTemplateObjList = new List<ExcelTemplate>();
            try
            {
                excelTemplateObjList = excelTemplateManagerObj.GetAllExcelTemplate(Id);
            }
            catch (Exception ex)
            {
            }
            return sReturnData = excelTemplateObjList[0].SheetText;
        }
        private String SaveStatementData(string sData)
        {
            String sReturnData = String.Empty;
            int returnVlue = -1;
            try
            {
                XmlNodeList msgDataNodeList;
                string parseData = ParseValue(sData, "ss:Table");
                parseData = parseData.Replace("ss:table", "Table").Replace("ss:", "");
                xmlDoc.LoadXml(parseData);
                //msgDataNode = xmlDoc.SelectSingleNode("//Row");
                msgDataNodeList = xmlDoc.SelectNodes("//Row");
                string data = msgDataNodeList[1].SelectNodes("//Data")[12].InnerText;
                BankStatement bankStatementObj = new BankStatement();
                bankStatementObj.BankStatementId = Convert.ToInt32("0");
                bankStatementObj.LlId = Convert.ToInt64("0" + lLId);
                bankStatementObj.Bank_ID = Convert.ToInt16("0" + bankId);
                bankStatementObj.Branch_ID = Convert.ToInt16("0" + branchId);
                bankStatementObj.ACNO = aCNo;
                bankStatementObj.ACType_Id = Convert.ToInt16("0" + acTypeId);
                bankStatementObj.ACName = aCName;
                bankStatementObj.StartMonth = msgDataNodeList[1].SelectNodes("//Data")[1].InnerText;
                bankStatementObj.EndMonth = msgDataNodeList[1].SelectNodes("//Data")[12].InnerText;
                bankStatementObj.Statement = sData;
                bankStatementObj.EntryDate = DateTime.Now;
                bankStatementObj.UserId = 0;
                returnVlue = bankStatementManagerObj.SendBankStatementInToDB(bankStatementObj);
                if (returnVlue > -1)
                {
                    sReturnData = "Update Successfully  #";
                }
                else
                {
                    sReturnData = "Update Error  #";
                }
           
            }
            catch (Exception ex)
            {
                sReturnData = ex.Message.ToString()+" #";
                CustomException.Save(ex, "Save Save Statement Data");
            }
            new AT(this).AuditAndTraial("Save Bank Statement", sReturnData);
            return sReturnData;    
        }
        public string ParseValue(string xmlString, string xmlTag)
        {
            string TagValue = null;
            string StringXML = xmlString;
            string TagXML = xmlTag;
            xmlString = xmlString.ToUpper();
            string StartTag = xmlTag.ToUpper();
            string EndTag = "/" + xmlTag.ToUpper();
            int j = 0;
            int i = xmlString.IndexOf(StartTag, 0);
            if (i >= 0)
                j = xmlString.IndexOf(EndTag, 0);
            if (i >= 0 && j >= i)
            {
                i = i + StartTag.Length;
                TagValue = "<" + StartTag.ToLower() + " " + StringXML.Substring(i, j - i) + EndTag.ToLower() + ">";
            }
            else
            {
                TagValue = "";
            }
            return TagValue;
        }
        private String GetStatementData(Int64 LLId, Int16 BankId, Int16 BranchId, string ACNo)
        {
            string sReturnData = String.Empty;
            BankStatement bankStatementObj = new BankStatement();
            try
            {
                bankStatementObj = bankStatementManagerObj.GetBankStatement(LLId, BankId, BranchId, ACNo);
                if (bankStatementObj != null)
                {
                    sReturnData = bankStatementObj.Statement;
                }
                else
                {
                    sReturnData = GetTemplate(6);
                }
            }
            catch (Exception ex)
            {
            }
            return sReturnData;
        }








    }
}
