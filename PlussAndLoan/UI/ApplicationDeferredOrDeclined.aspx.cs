﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI
{
    public partial class ApplicationDeferredOrDeclined : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string product = (string) Session["proName"];
            string stat = (string) Session["state"]; 
            string month = (string) Session["month"];
            int year = (int)Session["year"];
            DataTable dt = (DataTable) Session["appReport"];


            string status = "Case: " + stat;

            titleDiv.InnerText = status;
            productDiv.InnerText = product;
            monthDiv.InnerText = month;
            yearDiv.InnerText = year.ToString();

            reportgridView.DataSource = dt;
            reportgridView.DataBind();

        }
    }
}