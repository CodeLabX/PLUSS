﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BLL.Report;
using BusinessEntities;
using BusinessEntities.Report;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_PLUI : System.Web.UI.Page
    {
        public RemarksManager remarksManagerObj = null;
        public IncomeAcessmentMethodManager incomeAcessmentMethodManagerObj = null;
        public SegmentManager segmentManagerObj = null;
        public SectorManager sectorManagerObj = null;
        public SubSectorManager subSectorManagerObj = null;
        public LoanApplicationManager loanApplicationManagerObj = null;
        public LoanInformation loanInformationObj = null;
        public PLManager pLManagerObj = null;
        public ProductManager productManagerObj = null;
        public PayRollManager payRollManagerObj = null;
        public ActionPointsManager actionPointsManagerObj =null;
        public ScoreCardConditionManager scoreCardConditionManagerObj = null;
        public LoanScorePointManager loanScorePointManagerObj = null;
        public AnalystApproveManager analystApproveManagerObj = null;
        public LoanApplicationQueueManager loanApplicationQueueManagerObj = null;
        public BankStatementCalculationManager bankStatementCalculationManagerObj = null;
        public SalaryManager salaryManagerObj = null;
        public ActivityReportManager activityReportManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
    
        protected void Page_Load(object sender, EventArgs e)
        {
            remarksManagerObj = new RemarksManager();
            incomeAcessmentMethodManagerObj = new IncomeAcessmentMethodManager();
            segmentManagerObj = new SegmentManager();
            sectorManagerObj = new SectorManager();
            subSectorManagerObj = new SubSectorManager();
            loanApplicationManagerObj = new LoanApplicationManager();
            loanInformationObj = new LoanInformation();
            pLManagerObj = new PLManager();
            productManagerObj = new ProductManager();
            payRollManagerObj = new PayRollManager();
            actionPointsManagerObj = new ActionPointsManager();
            scoreCardConditionManagerObj = new ScoreCardConditionManager();
            loanScorePointManagerObj = new LoanScorePointManager();
            analystApproveManagerObj = new AnalystApproveManager();
            loanApplicationQueueManagerObj = new LoanApplicationQueueManager();
            bankStatementCalculationManagerObj = new BankStatementCalculationManager();
            salaryManagerObj = new SalaryManager();
            activityReportManagerObj = new ActivityReportManager();

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);

            emiTextBox.Text = Request[this.emiTextBox.UniqueID];
            dBRAllowableMaxLoanTextBox1.Text = Request[this.dBRAllowableMaxLoanTextBox1.UniqueID];
            dBRAllowableMaxLoanTextBox2.Text = Request[this.dBRAllowableMaxLoanTextBox2.UniqueID];
            dbrTextBox.Text = Request[this.dbrTextBox.UniqueID];
            mueTextBox.Text = Request[this.mueTextBox.UniqueID];
            bancaLoanAmountTextBox.Text = Request[this.bancaLoanAmountTextBox.UniqueID];
            bancaTextBox.Text = Request[this.bancaTextBox.UniqueID];
            emiFactorTextBox.Text = Request[this.emiFactorTextBox.UniqueID];
            bancaDBRTextBox.Text = Request[this.bancaDBRTextBox.UniqueID];
            bancaWithoutDBRTextBox.Text = Request[this.bancaWithoutDBRTextBox.UniqueID];
            appliedLoanAmountTextBox.Text = Request[this.appliedLoanAmountTextBox.UniqueID];
            appropriatedLoanAmountTextBox.Text = Request[this.appropriatedLoanAmountTextBox.UniqueID];
            avgBalanceSupportedMaxLoanAmountTextBox.Text = Request[this.avgBalanceSupportedMaxLoanAmountTextBox.UniqueID];
            mUEAllowableMaxLoanAmountTextBox.Text = Request[this.mUEAllowableMaxLoanAmountTextBox.UniqueID];
            maxLoanAmountAllowableMUETextBox.Text = Request[this.maxLoanAmountAllowableMUETextBox.UniqueID];
            dBRAllowableMaxLoanTextBox2.Text = Request[this.dBRAllowableMaxLoanTextBox2.UniqueID];
            netIncomeTextBox.Text = Request[this.netIncomeTextBox.UniqueID];
            monthlySalaryIncomeTextBox.Text = Request[this.monthlySalaryIncomeTextBox.UniqueID];
            bBPDDAllowedMaxLoanAmountTextBox.Text = Request[this.bBPDDAllowedMaxLoanAmountTextBox.UniqueID];
            bancaMUETextBox.Text = Request[this.bancaMUETextBox.UniqueID];
            profitMarginTextBox.Text = Request[this.profitMarginTextBox.UniqueID];
            //companyTypeDropDownList.Text = Request[this.companyTypeDropDownList.UniqueID];
            //gMueHiddenField.Value = Request[this.gMueHiddenField.ID];
            if (!Page.IsPostBack)
            {
                DataSet ds = new DataSet();
                ds.ReadXml(Server.MapPath("~/App_Code/PLOverdraft.xml"));
                overDraftGridView.DataSource = ds;
                overDraftGridView.DataBind();
                LoadInitalData();
                sectorDropDownList.AutoPostBack = true;
                subSectorDropDownList.AutoPostBack = true;
                segmentCodeDropDownList.AutoPostBack = true;
                dBRAllowableMaxLoanDropDownList.AutoPostBack = true;
                residentialStatusDropDownList.AutoPostBack = true;
                oneAddDropDownList.AutoPostBack = true;
                categoryDropDownList.AutoPostBack = true;

                declineButton.Enabled = false;
                deferButton.Enabled = false;
                forwardButton.Enabled = false;

            }
            monthlySalaryIncomeTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunctionAppropriatedLoanAmount();JS_FunctionMaxLoanAmount();JS_FunctionNetIncomeCalculation()");
            profitMarginTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunctionAppropriatedLoanAmount();JS_FunctionMaxLoanAmount();JS_FunctionNetIncomeCalculation()");
            otherIncomeTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunctionAppropriatedLoanAmount();JS_FunctionMaxLoanAmount();JS_FunctionNetIncomeCalculation()");
            plDropdownList.Attributes.Add("onChange", "JS_FunctionLabelChange();JS_FunctionCashSecurity()");
            segmentCodeDropDownList.Attributes.Add("onChange", "JS_FunctionFieldChange();JS_FunctionNetIncomeCalculation()");//;JS_FunctionNetIncomeCalculation()
            segmentCodeDropDownList.Attributes.Add("onblur", "JS_FunctionNetIncomeCalculation();JS_FunctionEMIFactor();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount()");
            profitMarginTextBox.Attributes.Add("ontextchanged", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunctionAppropriatedLoanAmount();JS_FunctionMaxLoanAmount();JS_FunctionNetIncomeCalculation()");
            incAssmntMethodDropDownList.Attributes.Add("onChange", "JS_FunctionMechodChange()");
            foreach (GridViewRow gvr in overDraftGridView.Rows)
            {
                (gvr.FindControl("limitTextBox") as TextBox).Attributes.Add("onkeyup", "JS_FunctionODCalculation()");
                (gvr.FindControl("rateTextBox") as TextBox).Attributes.Add("onkeyup", "JS_FunctionODCalculation()");
                (gvr.FindControl("q1TextBox") as TextBox).Attributes.Add("onkeyup", "JS_FunctionODCalculation()");
                (gvr.FindControl("q2TextBox") as TextBox).Attributes.Add("onkeyup", "JS_FunctionODCalculation()");
                (gvr.FindControl("q3TextBox") as TextBox).Attributes.Add("onkeyup", "JS_FunctionODCalculation()");
                (gvr.FindControl("q4TextBox") as TextBox).Attributes.Add("onkeyup", "JS_FunctionODCalculation()");
            }
            percentageOfOwnershipTextBox.Attributes.Add("onkeyup", "JS_FunctionODCalculation()");

            interestRateTextBox.Attributes.Add("onkeyup", "JS_FunctionOnePlussShow();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();JS_FunctionEMIFactor();JS_FunctionAppropriatedLoanAmount()");
            tenureTextBox.Attributes.Add("onkeyup", "JS_FunctionOnePlussShow();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();JS_FunctionEMIFactor();JS_FunctionAppropriatedLoanAmount()");
            plOutstandingTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunctionAppropriatedLoanAmount();JS_FunctionMaxLoanAmount()");
            averageBalanceTextBox.Attributes.Add("onblur", "JS_FunctionAppropriatedLoanAmount()");
            emiWithSCBTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaDBR()");
            otherEMITextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaDBR()");
            instabuyInstallmentTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaDBR()");
            scbCreditLimitTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunctionAppropriatedLoanAmount();");
            instabuyBalanceTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaDBR()");
            proportionateMonthlyInterestTextBox.Attributes.Add("onChange", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaDBR();JS_FunctionNetIncomeCalculation()");
            residentialStatusDropDownList.Attributes.Add("onChange", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaCalculation();JS_FunctionBancaDBR()");
            proposedLoanAmountTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount()");
            loanTypeDropDownList.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount()");
            safetyPlusDropDownList.Attributes.Add("onChange", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();JS_FunctionCheckAgeForBanca()");
            companyTypeDropDownList.Attributes.Add("onChange", "JS_FunctionOnePlussShow();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount()");
            monthlySalaryIncomeTextBox.Attributes.Add("onblur", "JS_FunctionAppropriatedLoanAmount()");
            declaredIncomeTextBox.Attributes.Add("onblur", "JS_FunctionAppropriatedLoanAmount()");
            flSecurityTextBox.Attributes.Add("onkeyup", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount()");
            categoryDropDownList.Attributes.Add("onChange", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount()");
            proposedLoanAmountTextBox.Attributes.Add("onblur", "JS_FunctionCheckAgeForBanca();JS_FunctionCheckMinLoanAmount()");

        }
    
        private void LoadInitalData()
        {
            List<Segment> segmentObjList = segmentManagerObj.SelectSegmentList();
            Segment segmentObj = new Segment();
            segmentObj.Id = 0;
            segmentObj.Name = "Select segment";
            segmentObjList.Insert(0,segmentObj);
            segmentCodeDropDownList.DataSource = segmentObjList;
            segmentCodeDropDownList.DataTextField = "Name";
            segmentCodeDropDownList.DataValueField = "Id";
            segmentCodeDropDownList.DataBind();
            List<Incomeacesmentmethod> incomeacesmentmethodObjList = incomeAcessmentMethodManagerObj.SelectIncomeAcesmentMethodList();
            incAssmntMethodDropDownList.DataSource = incomeacesmentmethodObjList;
            incAssmntMethodDropDownList.DataTextField = "Name";
            incAssmntMethodDropDownList.DataValueField = "Id";
            incAssmntMethodDropDownList.DataBind();

            List<Sector> sectorObjList = sectorManagerObj.SelectSectorList();
            sectorObjList = sectorObjList.FindAll(InactiveSubSector => InactiveSubSector.Status != 0);
            Sector sectorObj = new Sector();
            sectorObj.Id = 0;
            sectorObj.Name = "Select sector name";
            sectorObjList.Insert(0, sectorObj);
            sectorDropDownList.DataSource = sectorObjList;
            sectorDropDownList.DataTextField = "Name";
            sectorDropDownList.DataValueField = "Id";
            sectorDropDownList.DataBind();

            List<Product> productObjList = productManagerObj.GetAllProducts();
            Product productObj = new Product();
            productObj.ProdID = 0;
            productObj.ProdName = "Null";
            productObjList.Insert(0, productObj);
            emiWithSCBDropdownList.DataSource = productObjList;
            emiWithSCBDropdownList.DataTextField = "ProdName";
            emiWithSCBDropdownList.DataValueField = "ProdID";
            emiWithSCBDropdownList.DataBind();
            LoadPayRollInfo();
            FillRemarksGridView();
        }
        private void LoadPayRollInfo()
        {


            List<PayRoll> payRollObjList = payRollManagerObj.SearchAllPayRoll();
            PayRoll payRoll = new PayRoll();
            payRoll.CompID = 0;
            payRoll.CompName = "Not found company name";
            payRollObjList.Insert(0,payRoll);
            companyNameDropDownList.DataSource = payRollObjList;
            companyNameDropDownList.DataTextField = "CompName";
            companyNameDropDownList.DataValueField = "CompID";
            companyNameDropDownList.DataBind();
        }
        private void FillRemarksGridView()
        {
            //List<Remarks> remarksListObj = remarksManagerObj.SelectRemarksList(1);
            //remarksGridView.DataSource = remarksListObj;
            //remarksGridView.DataBind();
        }
        protected void subSectorDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (subSectorDropDownList.SelectedIndex > 0)
            {
                LoadSubSectorCode();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale", "ajax_showTooltip(window.event,'../UI/ActionTooltrip.aspx?subSectorId=" + subSectorDropDownList.SelectedValue.ToString() + "',this);", true);
            }
        }
        private void LoadSubSectorCode()
        {
            if (subSectorDropDownList.SelectedIndex > 0)
            {
                List<SubSector> subSectorObjList = (List<SubSector>)Session["subSectorList"];//subSectorManagerObj.SelectSubSectorSectorIdWise(Convert.ToInt32(sectorDropDownList.SelectedValue.ToString()));
                foreach (SubSector subSectorObj in subSectorObjList)
                {
                    if (Convert.ToInt64(subSectorDropDownList.SelectedValue.ToString()) == subSectorObj.Id)
                    {
                        subsectorTextBox.Text = subSectorObj.Code.ToString();
                        industryProfitTextBox.Text = subSectorObj.InterestRate.ToString();
                        break;
                    }
                    else
                    {
                        subsectorTextBox.Text = "";
                    }
                }
            }
        }
        protected void sectorDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sectorDropDownList.SelectedIndex > 0)
            {
                loadSubSector(0);
                subsectorTextBox.Text = "";
            }
        }
        private void loadSubSector(int selectIndex)
        {
            List<SubSector> subSectorObjList = subSectorManagerObj.SelectSubSectorSectorIdWise(Convert.ToInt32(sectorDropDownList.SelectedValue.ToString()));
            Session["subSectorList"] = subSectorObjList;
            SubSector subSectorObj = new SubSector();
            subSectorObj.Id = 0;
            subSectorObj.Name = "Select subscetor name";
            subSectorObjList.Insert(0, subSectorObj);
            subSectorDropDownList.DataSource = subSectorObjList;
            subSectorDropDownList.DataTextField = "Name";
            subSectorDropDownList.DataValueField = "Id";
            subSectorDropDownList.DataBind();
            subSectorDropDownList.SelectedValue = selectIndex.ToString();
        }
        protected void plDropdownList_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        public string AgeCalculation(DateTime myDOB, DateTime CurrentDate)
        {
            int years = 0;
            int months = 0;
            int days = 0;

            DateTime tmpMyDOB = new DateTime(myDOB.Year, myDOB.Month, 1);

            DateTime tmpFutureDate = new DateTime(CurrentDate.Year, CurrentDate.Month, 1);

            while (tmpMyDOB.AddYears(years).AddMonths(months) < tmpFutureDate)
            {
                months++;
                if (months >= 12)
                {
                    years++;
                    months = months - 12;
                }
            }

            if (CurrentDate.Day >= myDOB.Day)
            {
                days = days + CurrentDate.Day - myDOB.Day;
            }
            else
            {
                months--;
                if (months < 0)
                {
                    years--;
                    months = months + 11;
                }
                days +=
                    DateTime.DaysInMonth(
                        CurrentDate.AddMonths(-1).Year, CurrentDate.AddMonths(-1).Month
                        ) + CurrentDate.Day - myDOB.Day;

            }

            if (DateTime.IsLeapYear(myDOB.Year) && myDOB.Month == 2 && myDOB.Day == 29)
            {
                if (CurrentDate >= new DateTime(CurrentDate.Year, 3, 1))
                    days++;
            }
            ageHF.Value = years.ToString();
            string strAge = years + " Years " + months + " Months";
            return strAge;
        }

        protected void findButton_Click(object sender, EventArgs e)
        {
            if (llidTextBox.Text == "")
            {

                Alert.Show("Please insert valid LLId");

            }
            else {
        
                Session["LLID"] = Convert.ToInt64(llidTextBox.Text);
                ageHF.Value = "";
                if (LoadExistingPLInfo(Convert.ToInt64(llidTextBox.Text)) == false)
                {
                    clearButton_Click(sender, e);
                    loanInformationObj = loanApplicationManagerObj.GetLoanInformation(Convert.ToInt32(llidTextBox.Text));
                    if (loanInformationObj != null && loanInformationObj.LLId > 0)
                    {
                        if (LoadBankCalculation(Convert.ToInt64(llidTextBox.Text)) == false)
                        {
                            LoadSalaryCalculation(Convert.ToInt64(llidTextBox.Text));
                        }
                        ageLabel.Text = AgeCalculation(loanInformationObj.DOB, DateTime.Now);
                        if (Convert.ToInt16(ageLabel.Text.Substring(0, 2)) > 55)
                        {
                            Alert.Show("Not Eligible for BANCA as Age Greater Than 55 years!");
                            safetyPlusDropDownList.SelectedIndex = 1;
                        }
                        switch (loanInformationObj.ProductId)
                        {
                            case 1:
                                plDropdownList.SelectedValue = loanInformationObj.ProductId.ToString();
                                loanTypeLabel.Text = "Loan Type";
                                bBPDDAllowedMaxLoanAmountLabel.Text = "BB & PDD allowed max Loan amount";
                                dBRAllowableMaxLoanAmount.Text = "DBR allowable max Loan amount";
                                avgBalanceSupportedMaxLoanAmountLabel.Text = "Avg. Balance supported max Loan amount";
                                mEUAllowableMaxLoanAmountLabel.Text = "MUE allowable max Loan amount";
                                appliedLoanAmountLabel.Text = "Applied Loan Amount";
                                appropriatedLoanAmountLabel.Text = "Appropriated Loan amount";
                                proposedLoanAmountLabel.Text = "Proposed Loan Amount";
                                messageLabel.Text = "";
                                cashsecurityLabel.Style.Add("visibility", "hidden");
                                securityDropDownList.Style.Add("visibility", "hidden");
                                break;
                            case 4:
                                plDropdownList.SelectedValue = loanInformationObj.ProductId.ToString();
                                loanTypeLabel.Text = "Loan Type";
                                bBPDDAllowedMaxLoanAmountLabel.Text = "BB & PDD allowed max Loan amount";
                                dBRAllowableMaxLoanAmount.Text = "DBR allowable max Loan amount";
                                avgBalanceSupportedMaxLoanAmountLabel.Text = "Avg. Balance supported max Loan amount";
                                mEUAllowableMaxLoanAmountLabel.Text = "MUE allowable max Loan amount";
                                appliedLoanAmountLabel.Text = "Applied Loan Amount";
                                appropriatedLoanAmountLabel.Text = "Appropriated Loan amount";
                                proposedLoanAmountLabel.Text = "Proposed Loan Amount";
                                messageLabel.Text = "";
                                cashsecurityLabel.Style.Add("visibility", "visible");
                                securityDropDownList.Style.Add("visibility", "visible");
                                break;
                            case 12:
                                plDropdownList.SelectedValue = loanInformationObj.ProductId.ToString();
                                loanTypeLabel.Text = "Finance Type";
                                bBPDDAllowedMaxLoanAmountLabel.Text = "BB & PDD allowed max Finance amount";
                                dBRAllowableMaxLoanAmount.Text = "DBR allowable max Finance amount";
                                avgBalanceSupportedMaxLoanAmountLabel.Text = "Avg. Balance supported max Finance amount";
                                mEUAllowableMaxLoanAmountLabel.Text = "MUE allowable max Finance amount";
                                appliedLoanAmountLabel.Text = "Applied Finance Amount";
                                appropriatedLoanAmountLabel.Text = "Appropriated Finance amount";
                                proposedLoanAmountLabel.Text = "Proposed Finance Amount";
                                messageLabel.Text = "";
                                cashsecurityLabel.Style.Add("visibility", "hidden");
                                securityDropDownList.Style.Add("visibility", "hidden");
                                break;
                            default:
                                messageLabel.Text = "LLID " + llidTextBox.Text + " does not belong to PL,IPF,FL";
                                break;
                        }
                        professionLabel.Text = loanInformationObj.Profession.ToString();
                        educationLevelHiddenField.Value = loanInformationObj.EducationalLevel.ToString();
                        jobDurationHiddenField.Value = loanInformationObj.JobDuration.ToString();
                        Int32 professinoId = pLManagerObj.SelectProfessionId(loanInformationObj.Profession);
                        switch (professinoId.ToString())
                        {

                            case "2":
                            case "4":
                                SectorInfoVisibleFalse();
                                if (loanInformationObj.Business.ToString() != null)
                                {
                                    LoadCompanyInfo(loanInformationObj.Business.ToString());
                                }
                                break;
                            default:

                                break;
                        }
                        switch (professinoId.ToString())
                        {

                            case "1":
                            case "3":
                            case "4":
                            case "5":
                            case "6":
                                SectorInfoVisibleTrue();
                                break;
                            default:
                                SectorInfoVisibleTrue();
                                break;
                        }
                        int residentialStatus = 0;
                        switch (loanInformationObj.ResidentialStatus.ToString())
                        {
                            case "Own":
                                residentialStatus = 0;
                                break;
                            case "Family Owned":
                                residentialStatus = 1;
                                break;
                            default:
                                residentialStatus = 2;
                                break;
                        }
                        residentialStatusDropDownList.SelectedValue = residentialStatus.ToString();
                        //loanInformationObj.ResidentialStatus.ToString();
                        declaredIncomeTextBox.Text = loanInformationObj.DeclaredAmount.ToString();
                        appliedLoanAmountTextBox.Text = loanInformationObj.AppliedAmount.ToString();


                        if (loanInformationObj.ProductId == 1 || loanInformationObj.ProductId == 4 ||
                            loanInformationObj.ProductId == 12)
                        {
                            declineButton.Enabled = true;
                            deferButton.Enabled = true;
                            forwardButton.Enabled = true;
                        }
                        else
                        {
                            declineButton.Enabled = false;
                            deferButton.Enabled = false;
                            forwardButton.Enabled = false;
                        }
                    }
                    else
                    {
                        Alert.Show("No Data Fount");
                        clearButton_Click(sender, e);
                        llidTextBox.Focus();
                    }
                }
            }
        }
        private bool LoadBankCalculation(Int64 LLId)
        {
            bool returnValue = false;
            BankStatementCalculation BSCObj = bankStatementCalculationManagerObj.GetBankStatementCalculation(LLId);
            if (BSCObj == null)
            {
                returnValue = false;
            }
            else
            {
                if (BSCObj.CreditPart == 12)
                {
                    monthlySalaryIncomeTextBox.Text = BSCObj.Average12MonthCreditTurnover.ToString();
                    netIncomeTextBox.Text = BSCObj.Average12MonthCreditTurnover.ToString();
                    returnValue = true;
                }
                else if (BSCObj.CreditPart == 6)
                {
                    monthlySalaryIncomeTextBox.Text = BSCObj.Average6MonthCreditTurnover.ToString();
                    netIncomeTextBox.Text = BSCObj.Average6MonthCreditTurnover.ToString();
                    returnValue = true;
                }
                else
                {
                    monthlySalaryIncomeTextBox.Text = "";
                    netIncomeTextBox.Text = "";
                    returnValue = false;
                }
                if (BSCObj.AveragePart == 12)
                {
                    averageBalanceTextBox.Text = BSCObj.Month1250PerAverageBalance.ToString();
                    averageBalanceHiddenField.Value = BSCObj.Month1250PerAverageBalance.ToString();
                    returnValue = true;
                }
                else if (BSCObj.AveragePart == 6)
                {
                    averageBalanceTextBox.Text = BSCObj.Month650PerAverageBalance.ToString();
                    averageBalanceHiddenField.Value = BSCObj.Month650PerAverageBalance.ToString();
                    returnValue = true;
                }
                else
                {
                    averageBalanceTextBox.Text="";
                    averageBalanceHiddenField.Value = "";
                    returnValue = false;
                }
            }
            return returnValue;
        }
        private void LoadSalaryCalculation(Int64 LLId)
        {
            Salary salaryObj = new Salary();
            List<Salary> salaryObjList = new List<Salary>();
            salaryObjList = salaryManagerObj.GetAllSalaryInfoByLlId(LLId.ToString());
            if (salaryObjList.Count > 0)
            {
                salaryObj = salaryObjList[0];
                if (salaryObj.SalaNetincome > 0)
                {
                    monthlySalaryIncomeTextBox.Text = salaryObj.SalaNetincome.ToString();
                    netIncomeTextBox.Text = salaryObj.SalaNetincome.ToString();
                }
                if (salaryObj.SalaAmount2 > 0)
                {
                    averageBalanceTextBox.Text = salaryObj.SalaAmount2.ToString();
                    averageBalanceHiddenField.Value = salaryObj.SalaAmount2.ToString();
                }
            }
        }
        private void SectorInfoVisibleTrue()
        {
            sectorLabel.Style.Add("visibility", "visible");
            sectorDropDownList.Style.Add("visibility", "visible");
            subSectorLabel.Style.Add("visibility", "visible");
            subSectorDropDownList.Style.Add("visibility", "visible");
            subsectorTextBox.Style.Add("visibility", "visible");
        }
        private void SectorInfoVisibleFalse()
        {
            sectorLabel.Style.Add("visibility", "hidden");
            sectorDropDownList.Style.Add("visibility", "hidden");
            subSectorLabel.Style.Add("visibility", "hidden");
            subSectorDropDownList.Style.Add("visibility", "hidden");
            subsectorTextBox.Style.Add("visibility", "hidden");
        }

        private void OneAddVisibleTrue()
        {
            oneAddDropDownList.Style.Add("visibility", "visible");
        }
        private void OneAddVisibleFalse()
        {
            oneAddDropDownList.Style.Add("visibility", "hidden");
        }

        private void LoadCompanyInfo(string companyName)
        {
            if (companyName.Length > 0)
            {
                int companyId = 0;
                List<PayRoll> payRollObjList = new List<PayRoll>();
                PayRoll payRollObj = payRollManagerObj.SelectCompanyInfo(0,companyName);
                if (payRollObj!=null)
                {
                    if (payRollObj.CompID>0)
                    {
                        payRollObjList.Add(payRollObj);
                        companyId = payRollObj.CompID;
                        companyIdHiddenField.Value=payRollObj.CompID.ToString();
                        switch (payRollObj.CompStructure)
                        {
                            case"MNC":
                                companyTypeDropDownList.SelectedIndex = 1;
                                break;
                            case "LLC":
                                companyTypeDropDownList.SelectedIndex = 2;
                                break;
                            case "LC":
                                companyTypeDropDownList.SelectedIndex = 3;
                                break;
                        }
                    }
                }            
                try
                {
                    companyNameDropDownList.SelectedValue = companyId.ToString();
                }
                catch(Exception ex)
                {
                    companyNameDropDownList.SelectedValue = "0";
                }
            }
        }
        protected void segmentCodeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (segmentCodeDropDownList.SelectedIndex > 0)
            {
                LoadSegmentInfo(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
                LoadDoctorSegmentValue();
                AddOneFunction(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale", "JS_FunctionEMIFactor();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionEMIFactor();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);

            }        
        }
        private void AddOneFunction(int segmentId)
        {
            switch (segmentId.ToString())
            {
                case "1":
                    //OneAddVisibleFalse();
                    OneAddVisibleTrue();
                    break;
                case "2":
                    //OneAddVisibleFalse();
                    OneAddVisibleTrue();
                    break;
                case "3":
                    switch (companyTypeDropDownList.SelectedValue.ToString())
                    {
                        case "0":
                            OneAddVisibleTrue();
                            break;
                        case "1":
                            OneAddVisibleTrue();
                            break;
                        case "2":
                            //OneAddVisibleFalse();
                            OneAddVisibleTrue();
                            break;
                    }
                    break;
                case "4":
                    switch (companyTypeDropDownList.SelectedValue.ToString())
                    {
                        case "0":
                            OneAddVisibleTrue();
                            break;
                        case "1":
                            OneAddVisibleTrue();
                            break;
                        case "2":
                            OneAddVisibleTrue();
                            //OneAddVisibleFalse();
                            break;
                    }
                    break;
                case "5":
                    OneAddVisibleTrue();
                    //OneAddVisibleFalse();
                    break;
                case "6":
                    OneAddVisibleFalse();
                    break;
                case "7":
                    OneAddVisibleFalse();
                    break;
                case "8":
                    OneAddVisibleFalse();
                    break;
                case "9":
                    OneAddVisibleFalse();
                    break;
                case "10":
                    OneAddVisibleFalse();
                    break;
                case "11":
                    OneAddVisibleFalse();
                    break;
                case "12":
                    OneAddVisibleFalse();
                    break;
                case "13":
                    OneAddVisibleFalse();
                    break;
                case "14":
                    OneAddVisibleFalse();
                    break;
                case "15":
                    OneAddVisibleFalse();
                    break;
                default:
                    break;
            }
        }
        private void LoadSegmentInfo(int segmentId)
        {
            double check30k = 0;
            double netIncomeMinVlue = 0;
            double netIncomeVlue = 0;
            double ResidentialStatus = 1;
            double residenrialMulValue = 0;
            PayRoll payRollObj = new PayRoll();
            Segment segmentObj = segmentManagerObj.SelectSegment(segmentId);
            string EducationLevel = educationLevelHiddenField.Value.ToString();
            string workExp = jobDurationHiddenField.Value.ToString();
            if (segmentObj != null)
            {
                segmentWiseMinLoanAmountHiddenField.Value = segmentObj.MinLoanAmoutn.ToString();
                if (declaredIncomeTextBox.Text != "" && netIncomeTextBox.Text != "")
                {
                    switch (segmentId.ToString())
                    {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                        case "5":
                            switch (residentialStatusDropDownList.SelectedValue.ToString())
                            {
                                case "0":
                                case "1":
                                    ResidentialStatus = 1.25;
                                    break;
                                case "2":
                                    ResidentialStatus = 1;
                                    break;
                            }
                            break;
                        case "6":
                        case "7":
                        case "8":
                        case "12":
                        case "13":
                        case "14":
                        case "15":
                            switch (residentialStatusDropDownList.SelectedValue.ToString())
                            {
                                case "0":
                                case "1":
                                    ResidentialStatus = 1.10;
                                    break;
                                case "2":
                                    ResidentialStatus = 1;
                                    break;
                            }
                            break;
                    }
                    double profitMargin = 100;
                    double percentageOfOwnership = 100;
                    double ODValue = 0;
                    if ((profitMarginTextBox.Text!="") && (Convert.ToDouble(profitMarginTextBox.Text)>0))
                    {
                        profitMargin = Convert.ToDouble(profitMarginTextBox.Text);
                    }
                    if ((percentageOfOwnershipTextBox.Text!="") && (Convert.ToDouble(percentageOfOwnershipTextBox.Text) > 0))
                    {
                        percentageOfOwnership = Convert.ToDouble(percentageOfOwnershipTextBox.Text);
                    }
                    if ((proportionateMonthlyInterestTextBox.Text!="") && (Convert.ToDouble(proportionateMonthlyInterestTextBox.Text) > 0))
                    {
                        ODValue = Convert.ToDouble(proportionateMonthlyInterestTextBox.Text);
                    }
                    if ((monthlySalaryIncomeTextBox.Text!="") && (Convert.ToDouble(monthlySalaryIncomeTextBox.Text) > 0))
                    {
                        netIncomeTextBox.Text = ((((Convert.ToDouble(monthlySalaryIncomeTextBox.Text) * Convert.ToDouble(profitMargin)) / 100) * (Convert.ToDouble(percentageOfOwnership) / 100)) + (Convert.ToDouble("0" + otherIncomeTextBox.Text)) - (ODValue)).ToString();
                    }
                    netIncomeMinVlue = Math.Min(Convert.ToDouble(declaredIncomeTextBox.Text), Convert.ToDouble(netIncomeTextBox.Text));
                    residenrialMulValue = Convert.ToDouble(netIncomeMinVlue) * ResidentialStatus;
                    check30k = Convert.ToDouble(residenrialMulValue) - Convert.ToDouble(netIncomeMinVlue);
                    if (check30k < 30001)
                    {
                        netIncomeVlue = residenrialMulValue;
                    }
                    else
                    {
                        netIncomeVlue = netIncomeMinVlue;
                    }
                }

                switch (loanTypeDropDownList.SelectedValue)
                {
                    case "1":                    
                    case "2":
                        dBRAllowableMaxLoanTextBox2.Text = NewLoanCondition(netIncomeVlue, segmentObj);
                        break;
                    case "3":
                        dBRAllowableMaxLoanTextBox2.Text = TopUpLoanCondition(netIncomeVlue, segmentObj);
                        break;
                }            
                bBPDDAllowedMaxLoanAmountTextBox.Text = segmentObj.MaxLoanAmoutn.ToString();
                interestRateTextBox.Text = segmentObj.IR.ToString();
                tenureTextBox.Text = segmentObj.Tenor.ToString();

                switch (segmentCodeDropDownList.SelectedValue.ToString())
                {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                        if (companyNameDropDownList.SelectedIndex != 0)
                        {
                            //  payRollObj = payRollManagerObj.SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.Text);
                            payRollObj = SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.SelectedItem.Text, segmentCodeDropDownList.SelectedItem.Text);// companyNameDropDownList.Text);
                            if (payRollObj != null)
                            {
                                try
                                {
                                    if (segmentObj.Name.ToUpper().Trim() == "X1" & payRollObj.CompSegmentID.ToUpper().Trim() == "X1")
                                    {
                                        interestRateTextBox.Text = payRollObj.CompIrwitheosb.ToString();
                                        maxLoanAmountAllowableMUETextBox.Text = payRollObj.CompMue.ToString();
                                        gMueHiddenField.Value = payRollObj.CompMue.ToString();
                                    }
                                    else if (segmentObj.Name.ToUpper().Trim() == "Y1" & payRollObj.CompSegmentID.ToUpper().Trim() == "Y1")
                                    {
                                        interestRateTextBox.Text = payRollObj.CompIrwitheosb.ToString();
                                        maxLoanAmountAllowableMUETextBox.Text = payRollObj.CompMue.ToString();
                                        gMueHiddenField.Value = payRollObj.CompMue.ToString();
                                    }
                                        //tarek
                                        //Z1
                                        #region Z1
                                    else if (segmentObj.Name.ToUpper().Trim() == "Z1" & payRollObj.CompSegmentID.ToUpper().Trim() == "Z1")
                                    {
                                   
                                        if (segmentObj.Name.ToUpper().Trim() == "Z1" & payRollObj.CompCatstatus.ToUpper().Trim() == "B")
                                        {

                                            // interestRateTextBox.Text = payRollObj.CompIrwithouteosb.ToString();
                                            maxLoanAmountAllowableMUETextBox.Text = "10";// payRollObj.CompMue.ToString();
                                            gMueHiddenField.Value = payRollObj.CompMue.ToString();
                                        }
                                        else if (segmentObj.Name.ToUpper().Trim() == "Z1" & payRollObj.CompCatstatus.ToUpper().Trim() == "C")
                                        {

                                            //interestRateTextBox.Text = payRollObj.CompIrwithouteosb.ToString();
                                            maxLoanAmountAllowableMUETextBox.Text = "8";// payRollObj.CompMue.ToString();
                                            gMueHiddenField.Value = payRollObj.CompMue.ToString();
                                        }
                                        interestRateTextBox.Text = payRollObj.CompIrwithouteosb.ToString();
                                    }
                                
                               
                                    #endregion
                                    #region Z2
                                    if (segmentObj.Name.ToUpper().Trim() == "Z2" & payRollObj.CompSegmentID.ToUpper().Trim() == "Z2")
                                    {
                                        if (segmentObj.Name.ToUpper().Trim() == "Z2" & payRollObj.CompCatstatus.ToUpper().Trim() == "B")
                                        {
                                            // interestRateTextBox.Text = payRollObj.CompIrwithouteosb.ToString();
                                            maxLoanAmountAllowableMUETextBox.Text = "8";// payRollObj.CompMue.ToString();
                                            gMueHiddenField.Value = payRollObj.CompMue.ToString();
                                        }
                                        else if (segmentObj.Name.ToUpper().Trim() == "Z2" & payRollObj.CompCatstatus.ToUpper().Trim() == "C")
                                        {
                                            // interestRateTextBox.Text = payRollObj.CompIrwithouteosb.ToString();
                                            maxLoanAmountAllowableMUETextBox.Text = "6";// payRollObj.CompMue.ToString();
                                            gMueHiddenField.Value = payRollObj.CompMue.ToString();
                                        }
                                        interestRateTextBox.Text = payRollObj.CompIrwithouteosb.ToString();
                                    }


                              
                                        #endregion
                                        //Z2

                                        //
                                    else
                                    {
                                        if (payRollObj.CompCatstatus.Trim() != "Unapproved")
                                        {
                                            interestRateTextBox.Text = segmentObj.IR.ToString();

                                        }
                                        else
                                        {
                                            interestRateTextBox.Text = payRollObj.CompIrwithouteosb.ToString();
                                        }
                                                                        
                                        maxLoanAmountAllowableMUETextBox.Text = payRollObj.CompMue.ToString();
                                        gMueHiddenField.Value = payRollObj.CompMue.ToString();
                                    }
                                }
                                catch
                                {
                                }
                            }
                            else
                            {
                                switch (companyTypeDropDownList.SelectedValue.ToString())
                                {
                                    case "1":
                                        maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUEMul.ToString();
                                        gMueHiddenField.Value = segmentObj.MUEMul.ToString();
                                        break;
                                    case "2":
                                        maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUELar.ToString();
                                        gMueHiddenField.Value = segmentObj.MUELar.ToString();
                                        break;
                                    case "3":
                                        maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUELoc.ToString();
                                        gMueHiddenField.Value = segmentObj.MUELoc.ToString();
                                        break;
                                }

                                interestRateTextBox.Text = segmentObj.IR.ToString();
                            }
                        }
                        else
                        {
                            maxLoanAmountAllowableMUETextBox.Text = segmentObj.MultiPlire.ToString();
                            gMueHiddenField.Value = segmentObj.MultiPlire.ToString();
                        }
                        break;
                    case "12":
                        if (companyNameDropDownList.SelectedIndex != 0)
                        {
                            payRollObj = payRollManagerObj.SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()),companyNameDropDownList.Text);

                            if (payRollObj != null)
                            {
                                maxLoanAmountAllowableMUETextBox.Text = payRollObj.CompMue.ToString();
                                gMueHiddenField.Value = payRollObj.CompMue.ToString();

                            }
                            else
                            {
                                maxLoanAmountAllowableMUETextBox.Text = segmentObj.MultiPlire.ToString();
                                gMueHiddenField.Value = segmentObj.MultiPlire.ToString();
                            }

                        }
                        else
                        {
                            maxLoanAmountAllowableMUETextBox.Text = segmentObj.MultiPlire.ToString();
                            gMueHiddenField.Value = segmentObj.MultiPlire.ToString();
                        }
                        break;
                    case "13":
                        if (companyNameDropDownList.SelectedIndex != 0)
                        {
                            payRollObj = payRollManagerObj.SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.Text);

                            if (payRollObj != null)
                            {
                                maxLoanAmountAllowableMUETextBox.Text = payRollObj.CompMue.ToString();
                                gMueHiddenField.Value = payRollObj.CompMue.ToString();

                            }
                            else
                            {
                                maxLoanAmountAllowableMUETextBox.Text = segmentObj.MultiPlire.ToString();
                                gMueHiddenField.Value = segmentObj.MultiPlire.ToString();
                            }
                        }
                        else
                        {
                            maxLoanAmountAllowableMUETextBox.Text = segmentObj.MultiPlire.ToString();
                            gMueHiddenField.Value = segmentObj.MultiPlire.ToString();
                        }
                        break;
                    default:
                        maxLoanAmountAllowableMUETextBox.Text = segmentObj.MultiPlire.ToString();
                        gMueHiddenField.Value = segmentObj.MultiPlire.ToString();
                        break;
                }           

            }
        }
        private string NewLoanCondition(double IncomeValue, Segment segmentObj)
        {
            string returnValue = string.Empty;
            switch (dBRAllowableMaxLoanDropDownList.SelectedValue)
            {
                case "L1":
                    returnValue = NewLoanLevelOneRange(IncomeValue, segmentObj);
                    break;
                case "L2":
                    returnValue = NewLoanLevelTwoRange(IncomeValue, segmentObj);
                    break;
            }
            return returnValue;
        }
        private string TopUpLoanCondition(double IncomeValue, Segment segmentObj)
        {
            string returnValue = string.Empty;
            switch (dBRAllowableMaxLoanDropDownList.SelectedValue)
            {
                case "L1":
                    returnValue = TopUpLoanLevelOneRange(IncomeValue, segmentObj);
                    break;
                case "L2":
                    returnValue = TopUpLoanLevelTwoRange(IncomeValue, segmentObj);
                    break;
            }
            return returnValue;
        }
        private string NewLoanLevelOneRange(double IncomeValue, Segment segmentObj)
        {
            Int32 retuntValue = 0;
            if (IncomeValue <= 29999)
            {
                retuntValue = segmentObj.DBR29;
            }
            else if(IncomeValue <= 49999)
            {
                retuntValue = segmentObj.DBR50;
            }
            else if (IncomeValue <= 99999)
            {
                retuntValue = segmentObj.DBR99;
            }
            else if (IncomeValue > 99999)
            {
                retuntValue = segmentObj.DBR1K;
            }
            return retuntValue.ToString();
        }
        private string NewLoanLevelTwoRange(double IncomeValue, Segment segmentObj)
        {
            Int32 retuntValue = 0;
            if (IncomeValue <= 29999)
            {
                retuntValue = segmentObj.L2MultiPlire29;
            }
            else if (IncomeValue <= 49999)
            {
                retuntValue = segmentObj.L2MultiPlire50;
            }
            else if (IncomeValue <= 99999)
            {
                retuntValue = segmentObj.L2MultiPlire99;
            }
            else if (IncomeValue > 99999)
            {
                retuntValue = segmentObj.L2MultiPlire1K;
            }
            return retuntValue.ToString();

        }
        private string TopUpLoanLevelOneRange(double IncomeValue, Segment segmentObj)
        {
            Int32 retuntValue = 0;
            if (IncomeValue <= 29999)
            {
                retuntValue = segmentObj.L1MultiPlire29;
            }
            else if (IncomeValue <= 49999)
            {
                retuntValue = segmentObj.L1MultiPlire50;
            }
            else if (IncomeValue <= 99999)
            {
                retuntValue = segmentObj.L1MultiPlire99;
            }
            else if (IncomeValue > 99999)
            {
                retuntValue = segmentObj.L1MultiPlire1K;
            }
            return retuntValue.ToString();
        }
        private string TopUpLoanLevelTwoRange(double IncomeValue, Segment segmentObj)
        {
            Int32 retuntValue = 0;
            if (IncomeValue <= 29999)
            {
                retuntValue = segmentObj.L2TopMultiPlire29;
            }
            else if (IncomeValue <= 49999)
            {
                retuntValue = segmentObj.L2TopMultiPlire50;
            }
            else if (IncomeValue <= 99999)
            {
                retuntValue = segmentObj.L2TopMultiPlire99;
            }
            else if (IncomeValue > 99999)
            {
                retuntValue = segmentObj.L2TopMultiPlire1K;
            }
            return retuntValue.ToString();
        }

        private void LoadDoctorSegmentValue()
        {
            switch (segmentCodeDropDownList.SelectedValue.ToString())
            {
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                    incAssmntMethodDropDownList.SelectedValue = "6";
                    profitMarginTextBox.Text = "";
                    break;
                default:
                    incAssmntMethodDropDownList.SelectedValue = "1";
                    profitMarginTextBox.Text = "";
                    break;
                case "12":
                    profitMarginTextBox.Text = "100";
                    break;
                case "13":
                    profitMarginTextBox.Text = "75";
                    break;
            }
        }

        protected void forwardButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsBlankData())
                {
                    SaveData(9,0);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Forward PL");
            }
        }
        private void ScoreCalculation()
        {
            double workExperiencePoint = 0;
            double incomeRatioPoint = 0;
            double residenceTypePoint = 0;
            double maritalStatusPoint = 0;
            double qualificationPoint = 0;
            double grossMonthlyIncomePoint = 0;
            double spouseOccupationPoint = 0;
            double vechilePoint = 0;
            string result = null; 
            LoanScorePoint loanScorePointObj = new LoanScorePoint();
            List<ScoreCardCondition> scoreCardConditionObjList = scoreCardConditionManagerObj.GetScoreCardCondition();
            loanInformationObj = loanApplicationManagerObj.GetLoanInformation(Convert.ToInt32(llidTextBox.Text));
            if (loanInformationObj != null)
            {
                foreach (ScoreCardCondition scoreCardConditionObj in scoreCardConditionObjList)
                {
                    switch (scoreCardConditionObj.VariableName.Name.ToUpper().Trim())
                    {
                        case "WORKEXPERIENCE":
                            if (loanInformationObj.JobDuration>0)
                            {
                                if (loanInformationObj.JobDuration < scoreCardConditionObj.CompareValue)
                                {
                                    workExperiencePoint = scoreCardConditionObj.NegativeValue;
                                }
                                else if (loanInformationObj.JobDuration >= scoreCardConditionObj.CompareValue)
                                {
                                    workExperiencePoint = scoreCardConditionObj.ScoreValue;
                                }
                            }
                            else
                            {
                                workExperiencePoint = scoreCardConditionObj.NegativeValue;
                            }
                            break;
                        case "INCOMERATIO":
                            double netIncome=Math.Min(Convert.ToDouble(netIncomeTextBox.Text),Convert.ToDouble(declaredIncomeTextBox.Text));
                            double loadAmount = Convert.ToDouble("0"+appliedLoanAmountTextBox.Text);
                            double incomeRatio = 0;
                            if(netIncome>0)
                            {
                                incomeRatio = Math.Round(loadAmount / netIncome,2);
                            }
                            if (incomeRatio <= scoreCardConditionObj.CompareValue)
                            {
                                incomeRatioPoint = scoreCardConditionObj.ScoreValue;
                            }
                            else
                            {
                                incomeRatioPoint = scoreCardConditionObj.NegativeValue;
                            }
                            break;
                        case "RESIDENCETYPE":
                            if (loanInformationObj.ResidentialStatus!=null)
                            {
                                switch (loanInformationObj.ResidentialStatus.Replace(" ","").ToUpper().Trim())
                                {
                                    case "OWN":
                                        residenceTypePoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    case "FAMILYOWNED":
                                        residenceTypePoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    case "OTHERS":
                                        residenceTypePoint = scoreCardConditionObj.NegativeValue;
                                        break;
                                }
                            }
                            else
                            {
                                residenceTypePoint = scoreCardConditionObj.NegativeValue;
                            }
                            break;
                        case "MARITALSTATUS":
                            if (loanInformationObj.MaritalStatus != null)
                            {
                                switch (loanInformationObj.MaritalStatus.Replace(" ", "").ToUpper().Trim())
                                {
                                    case "MARRIED":
                                        maritalStatusPoint = scoreCardConditionObj.NegativeValue;
                                        break;
                                    case "SINGLE":
                                        maritalStatusPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    default:
                                        maritalStatusPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                }
                            }
                            else
                            {
                                maritalStatusPoint = scoreCardConditionObj.NegativeValue;
                            }
                            break;
                        case "QUALIFICATION":
                            if (loanInformationObj.EducationalLevel != null)
                            {
                                switch (loanInformationObj.EducationalLevel.Replace(" ", "").ToUpper().Trim().ToString())
                                {
                                    case "DIP":
                                    case "GRD":
                                    case "HSC":
                                        qualificationPoint = scoreCardConditionObj.NegativeValue;
                                        break;
                                    default:
                                        qualificationPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                }
                            }
                            else
                            {
                                qualificationPoint = scoreCardConditionObj.NegativeValue;
                            }
                            break;
                        case "GROSSMONTHLYINCOME":
                            double minimumIncome = Math.Min(Convert.ToDouble(netIncomeTextBox.Text), Convert.ToDouble(declaredIncomeTextBox.Text));
                            double appliedLoadAmount = Convert.ToDouble("0" + appliedLoanAmountTextBox.Text);
                            double grossIncome = 0;
                            if (minimumIncome > 0)
                            {
                                grossIncome = Math.Round(appliedLoadAmount / minimumIncome, 2);
                            }
                            if (grossIncome <= scoreCardConditionObj.CompareValue)
                            {
                                grossMonthlyIncomePoint = scoreCardConditionObj.ScoreValue;
                            }
                            else
                            {
                                grossMonthlyIncomePoint = scoreCardConditionObj.NegativeValue;
                            }
                            break;
                        case "SPOUSESOCCUPATION":
                            if (loanInformationObj.SpousOcupation != null)
                            {
                                switch (loanInformationObj.SpousOcupation.Replace(" ", "").Trim().ToUpper().ToString())
                                {
                                    case "BUSINESS":
                                        spouseOccupationPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    case "SALARIED":
                                        spouseOccupationPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    case "SELFEMPLOYED":
                                        spouseOccupationPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    case "DOCTOR":
                                        spouseOccupationPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    case "LANDLORD":
                                        spouseOccupationPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    case "OTHERS":
                                        spouseOccupationPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                    default:
                                        spouseOccupationPoint = scoreCardConditionObj.ScoreValue;
                                        break;
                                }
                            }
                            else
                            {
                                spouseOccupationPoint = scoreCardConditionObj.NegativeValue;
                            }
                            break;
                        case "VEHICLE":                       
                        
                            if (loanInformationObj.VechileId == 0)//0="Yes"
                            {
                                vechilePoint=scoreCardConditionObj.ScoreValue;
                            }
                            else if (loanInformationObj.VechileId == 1)//1="No"
                            {
                                vechilePoint=scoreCardConditionObj.NegativeValue;
                            }
                            else
                            {
                                vechilePoint = scoreCardConditionObj.NegativeValue;
                            }
                            break;
                    }
                }

            }

            double totalPoint = workExperiencePoint + incomeRatioPoint + residenceTypePoint + maritalStatusPoint + qualificationPoint + grossMonthlyIncomePoint + spouseOccupationPoint + vechilePoint;
            loanScorePointObj.Id = 0;
            loanScorePointObj.LlId = Convert.ToInt64(llidTextBox.Text);
            loanScorePointObj.CutOfScore = 400;
            loanScorePointObj.IntreceptValue = 423;
            loanScorePointObj.WorkExperiencePoint = workExperiencePoint;
            loanScorePointObj.IncomeRatioPoint = incomeRatioPoint;
            loanScorePointObj.ResidentypePoint = residenceTypePoint;
            loanScorePointObj.MaritalStatusPoint = maritalStatusPoint;
            loanScorePointObj.QualificationPoint = qualificationPoint;
            loanScorePointObj.GrossMonthlyIncomePoint = grossMonthlyIncomePoint;
            loanScorePointObj.SpouseOccupationPoint = spouseOccupationPoint;
            loanScorePointObj.VehiclPoint = vechilePoint;
            loanScorePointObj.TotalPoint = loanScorePointObj.IntreceptValue + totalPoint;
            if (loanScorePointObj.TotalPoint <400)
            {
                result = "REJECTED";
            }
            else
            {
                result = "APPROVED";
            }
            loanScorePointObj.Result = result.ToString();
            int Ok = loanScorePointManagerObj.SendLoanScorePointInToDB(loanScorePointObj);
        }
        private bool IsBlankData()
        {
            if (llidTextBox.Text == "")
            {
                Alert.Show("Please input LLId");
                llidTextBox.Focus();
                return false;
            }
            else if (segmentCodeDropDownList.SelectedIndex == 0)
            {
                Alert.Show("Please select segment code");
                segmentCodeDropDownList.Focus();
                return false;
            }
            else if (plDropdownList.SelectedIndex == 0)
            {
                Alert.Show("Please Select PL/IPL/FL");
                plDropdownList.Focus();
                return false;
            }
            else if (monthlySalaryIncomeTextBox.Text == "")
            {
                Alert.Show("Please input NetIncome");
                monthlySalaryIncomeTextBox.Focus();
                return false;
            }
            else if (netIncomeTextBox.Text == "")
            {
                Alert.Show("Please input NetIncome");
                netIncomeTextBox.Focus();
                return false;
            }
            else if (declaredIncomeTextBox.Text == "")
            {
                Alert.Show("Please input Declared Income");
                declaredIncomeTextBox.Focus();
                return false;
            }
            else if (dBRAllowableMaxLoanTextBox1.Text == "")
            {
                Alert.Show("Empty DBR allowable Max Loan Amount !");
                dBRAllowableMaxLoanTextBox1.Focus();
                return false;
            }
            else if (mUEAllowableMaxLoanAmountTextBox.Text == "")
            {
                Alert.Show("Empty MUE Allowable Max Loan Amount !");
                mUEAllowableMaxLoanAmountTextBox.Focus();
                return false;
            }
            else if (appropriatedLoanAmountTextBox.Text == "")
            {
                Alert.Show("Empty Appropriated Loan Amount !");
                appropriatedLoanAmountTextBox.Focus();
                return false;
            }
            else if (proposedLoanAmountTextBox.Text == "")
            {
                Alert.Show("Empty Proposed Loan Amount !");
                proposedLoanAmountTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        private void SaveData(int loanStatusId, int pLStatus)
        {
            string remarksData = "";
            int PLInsert = 0;
            PL pLObj =new PL();
            if (segmentCodeDropDownList.SelectedIndex > 0)
            {
                pLObj.PlId = 0;
                pLObj.PlLlid = Convert.ToInt64(llidTextBox.Text);
                pLObj.PlProductId = Convert.ToInt32(plDropdownList.SelectedValue.ToString());
                pLObj.PlSegmentId = Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString());
                pLObj.PlLoantypeId = Convert.ToInt32(loanTypeDropDownList.SelectedValue.ToString());
                pLObj.PlIncomeacessmentmethodId = Convert.ToInt32(incAssmntMethodDropDownList.SelectedValue.ToString());
                pLObj.PlSectorId = Convert.ToInt32("0" + sectorDropDownList.SelectedValue.ToString());
                pLObj.PlSubsectorId = Convert.ToInt32("0" + subSectorDropDownList.SelectedValue.ToString());
                pLObj.PlCompanyId = Convert.ToInt32("0" + companyNameDropDownList.SelectedValue.ToString());
                pLObj.PlCompanycatagoryId = Convert.ToInt32("0");
                pLObj.PlCompanytypeId = Convert.ToInt32("0" + companyTypeDropDownList.SelectedValue.ToString());
                pLObj.PlDocatagorizedId = Convert.ToInt32("0" );
                pLObj.PlCompanysegmentId = Convert.ToInt32("0");
                pLObj.PlDoctorCategory = categoryDropDownList.SelectedValue.ToString();
                pLObj.PlIncome = Convert.ToDouble(monthlySalaryIncomeTextBox.Text);
                try
                {
                    pLObj.PlOwnership = Convert.ToDouble(percentageOfOwnershipTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlOwnership = 0;
                }
                try
                {
                    pLObj.PlProfitmargin = Convert.ToDouble(profitMarginTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlProfitmargin = 0;
                }
            
                pLObj.PlOtherincomesourceId = Convert.ToInt32("0" + otherIncomeDropDownList.SelectedIndex.ToString());
                try
                {
                    pLObj.PlOtherincome = Convert.ToDouble(otherIncomeTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlOtherincome = 0;
                }
            
                pLObj.PlNetincome = Convert.ToDouble(netIncomeTextBox.Text);
                pLObj.PlDeclaredincome = Convert.ToDouble(declaredIncomeTextBox.Text);
                try
                {
                    pLObj.PlScbcreditcardlimit = Convert.ToDouble(scbCreditLimitTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlScbcreditcardlimit = 0;
                }
            
                pLObj.PlIndustryprofitmarginrat = Convert.ToString(industryProfitTextBox.Text);
                try
                {
                    pLObj.PlPliplfloutstanding = Convert.ToDouble(plOutstandingTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlPliplfloutstanding = 0;
                }
                try
                {
                    pLObj.PlMonthlyinterest = Convert.ToDouble(proportionateMonthlyInterestTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlMonthlyinterest = 0;
                }
            
                pLObj.PlResiedentialstatusId = Convert.ToInt32(residentialStatusDropDownList.SelectedValue.ToString());
                try
                {
                    pLObj.PlFlsecuityos = Convert.ToDouble(flSecurityTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlFlsecuityos = 0;
                }
            
                try
                {
                    pLObj.PlAveragebalance = Convert.ToDouble(averageBalanceTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlAveragebalance = 0;
                }
            
                try
                {
                    pLObj.PlEmiwithscbsourceId = Convert.ToInt32(emiWithSCBDropdownList.SelectedValue.ToString());
                }
                catch (Exception)
                {

                    pLObj.PlEmiwithscbsourceId = 0;
                }
            
                try
                {
                    pLObj.PlEmiwithscb = Convert.ToDouble(emiWithSCBTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlEmiwithscb = 0;
                }
                pLObj.PlOtheremisource = Convert.ToString(otherEMITextBoxWithLabel.Text);

            
                try
                {
                    pLObj.PlOtheremi = Convert.ToDouble(otherEMITextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlOtheremi = 0;
                }
            
                try
                {
                    pLObj.PlInstabuybalance = Convert.ToDouble(instabuyBalanceTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlInstabuybalance = 0;
                }
            
                try
                {
                    pLObj.PlInstabuyinstallment = Convert.ToDouble(instabuyInstallmentTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlInstabuyinstallment = 0;
                }
            
                try
                {
                    pLObj.PlBbpddmaxloanamound = Convert.ToDouble(bBPDDAllowedMaxLoanAmountTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlBbpddmaxloanamound = 0;
                }
                pLObj.PlDbrmultiplier = Convert.ToInt32(dBRAllowableMaxLoanTextBox2.Text);
                pLObj.PlAddOneId = Convert.ToInt32("0" + oneAddDropDownList.SelectedValue.ToString());
                try
                {
                    pLObj.PlDbrmaxloanamount = Convert.ToDouble(dBRAllowableMaxLoanTextBox1.Text);
                }
                catch (Exception)
                {

                    pLObj.PlDbrmaxloanamount = 0;
                }
            
                try
                {
                    pLObj.PlAvgbalancmaxloanamount = Convert.ToDouble(avgBalanceSupportedMaxLoanAmountTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlAvgbalancmaxloanamount = 0;
                }
            
                try
                {
                    pLObj.PlMuemultiplier = Convert.ToDouble(maxLoanAmountAllowableMUETextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlMuemultiplier = 0;
                }
            
                try
                {
                    pLObj.PlMuemaxloanamount = Convert.ToDouble(mUEAllowableMaxLoanAmountTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlMuemaxloanamount = 0;
                }
            
                try
                {
                    pLObj.PlInterestrat = Convert.ToDouble(interestRateTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlInterestrat = 0;
                }
            
                try
                {
                    pLObj.PlTenure = Convert.ToInt32(tenureTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlTenure = 0;
                }
            
                pLObj.PlFirstpayment = Convert.ToInt32(firstPaymentDropDownList.SelectedValue.ToString());
                pLObj.PlSafetyplusId = Convert.ToInt32(safetyPlusDropDownList.SelectedValue.ToString());
                pLObj.PlCurrentAge = ageLabel.Text;
                pLObj.PlSecurity = securityDropDownList.SelectedValue.ToString();
                try
                {
                    pLObj.PlAppliedloanamount = Convert.ToDouble(appliedLoanAmountTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlAppliedloanamount = 0;
                }
            
                try
                {
                    pLObj.PlAppropriatedloanamount = Convert.ToDouble(appropriatedLoanAmountTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlAppropriatedloanamount = 0;
                }
            
                try
                {
                    pLObj.PlProposedloanamount = Convert.ToDouble(proposedLoanAmountTextBox.Text);
                }
                catch (Exception)
                {

                    pLObj.PlProposedloanamount = 0;
                }

                pLObj.PlLevel = dBRAllowableMaxLoanDropDownList.SelectedValue.ToString();
                try
                {
                    pLObj.PlBancaLoanAmount = Convert.ToDouble(bancaLoanAmountTextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {

                    pLObj.PlBancaLoanAmount = 0;
                }
            
                try
                {
                    pLObj.PlBanca = Convert.ToDouble(bancaTextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {

                    pLObj.PlBanca = 0;
                }
                try
                {
                    pLObj.PlBancaDBR = Convert.ToDouble(bancaDBRTextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {
                    pLObj.PlBancaDBR = 0;
                }
                try
                {
                    pLObj.PlBancaWithoutDBR = Convert.ToDouble(bancaWithoutDBRTextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {
                    pLObj.PlBancaWithoutDBR = 0;
                }
            
                try
                {
                    pLObj.PlBancaMUE = Convert.ToDouble(bancaMUETextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {

                    pLObj.PlBancaMUE=0;
                }
            
                try
                {
                    pLObj.PlEmi = Convert.ToDouble(emiTextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {
                
                    pLObj.PlEmi =0;
                }
            
                try
                {
                    pLObj.PlEmifactor = Convert.ToDouble(emiFactorTextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {
                
                    pLObj.PlEmifactor =0;
                }
                try
                {
                    pLObj.PlDbr = Convert.ToDouble(dbrTextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {
                    pLObj.PlDbr=0;
                }
                //
                try
                {
                    pLObj.PlMue = Convert.ToDouble(mueTextBox.Text.Replace("NaN", "0").Replace("Infinity", "0"));
                }
                catch (Exception)
                {
                
                    pLObj.PlMue =0;
                }

                //foreach (GridViewRow gvr in remarksGridView.Rows)
                //{
                //    if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked == true)
                //    {
                //        remarksData += (gvr.FindControl("idLabel") as Label).Text + "#" + (gvr.FindControl("remarksTextBox") as TextBox).Text + "*";
                //    }
                //}
                //if (remarksData.Length > 0)
                //{
                //    remarksData = remarksData.Substring(0, remarksData.Length - 1);
                //}
                //pLObj.PlRemarks = Convert.ToString(remarksData.ToString());
                pLObj.PlRemarks = remarksTextBox.Text;
                pLObj.UserId = Convert.ToInt32(Session["Id"].ToString());
                pLObj.EntryDate = DateTime.Now;
                pLObj.Status = pLStatus;
                PLInsert = pLManagerObj.SendPLInToDB(pLObj);
                if (PLInsert == INSERTE)
                {
                    SaveOverDraft();
                    ScoreCalculation();
                    analystApproveManagerObj.UpdatePlussLoanStatus(pLObj.PlLlid, loanStatusId);

                    ActivityReportObject activityReportObj = new ActivityReportObject();

                    if (pLObj.PlProductId == 4)
                    {
                        activityReportObj.PoFL = 1;
                    }
                    else if (pLObj.PlProductId == 1 && pLObj.PlSegmentId < 6)
                    {
                        activityReportObj.PoPLSAL = 1;
                    }
                    else if (pLObj.PlProductId == 1 && pLObj.PlSegmentId > 5)
                    {
                        activityReportObj.PoPLBIZ = 1;
                    }
                    else if (pLObj.PlProductId == 12)
                    {
                        activityReportObj.PoIPF = 1;
                    }
                    else if (pLObj.PlProductId > 12)
                    {
                        activityReportObj.PoSTFL = 1;
                    }
                    activityReportObj.Date = DateTime.Now;
                    activityReportManagerObj.SendProcessStatus(activityReportObj);
                    Alert.Show("Save Successfuly");
                    new AT(this).AuditAndTraial("PL", "Save Successfuly");
                    FieldClear();
                    GridClear();
                }
                else if (PLInsert == UPDATE)
                {
                    SaveOverDraft();
                    ScoreCalculation();
                    analystApproveManagerObj.UpdatePlussLoanStatus(pLObj.PlLlid, loanStatusId);
                    Alert.Show("Update Successfuly");
                    new AT(this).AuditAndTraial("PL", "Update Successfuly");
                    FieldClear();
                    GridClear();
                }
                else if (PLInsert == ERROR)
                {
                    Alert.Show("Error in Data !");
                }
            }
        }
        public string RemoveSpecialChar(string sMessage)
        {
            string returnMessage = "";
            if (!string.IsNullOrEmpty(sMessage))
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private void SaveOverDraft()
        {
            List<PLOverDraft> pLOverDraftObjList = new List<PLOverDraft>();
            PLOverDraft pLOverDraftObj = null;
            //bool IsOD=false;
            //foreach (GridViewRow gvrck in overDraftGridView.Rows)
            //{
            //    if ((gvrck.FindControl("limitTextBox") as TextBox).Text != "" || (gvrck.FindControl("q1TextBox") as TextBox).Text != "")
            //    {
            //        IsOD = true;
            //    }
            //}
            //if (IsOD == true)
            //{
            Int64 PLId=pLManagerObj.GetPLId(Convert.ToInt64(llidTextBox.Text));
            if (PLId > 0)
            {
                foreach (GridViewRow gvr in overDraftGridView.Rows)
                {
                    pLOverDraftObj = new PLOverDraft();
                    pLOverDraftObj.Id = 0;
                    pLOverDraftObj.PLId = PLId;
                    pLOverDraftObj.LlId = Convert.ToInt64(llidTextBox.Text);
                    pLOverDraftObj.Limit = Convert.ToDouble("0" + (gvr.FindControl("limitTextBox") as TextBox).Text);
                    pLOverDraftObj.Rate = Convert.ToDouble("0" + (gvr.FindControl("rateTextBox") as TextBox).Text);
                    pLOverDraftObj.Q1 = Convert.ToDouble("0" + (gvr.FindControl("q1TextBox") as TextBox).Text);
                    pLOverDraftObj.Q2 = Convert.ToDouble("0" + (gvr.FindControl("q2TextBox") as TextBox).Text);
                    pLOverDraftObj.Q3 = Convert.ToDouble("0" + (gvr.FindControl("q3TextBox") as TextBox).Text);
                    pLOverDraftObj.Q4 = Convert.ToDouble("0" + (gvr.FindControl("q4TextBox") as TextBox).Text);
                    pLOverDraftObj.OldId = Convert.ToInt32("0" + (gvr.FindControl("OldIdLabel") as Label).Text);
                    pLOverDraftObjList.Add(pLOverDraftObj);
                }
                int sendODDB = pLManagerObj.SendOverDraftInToDB(pLOverDraftObjList);
            }
            //}
        }
        private void LoadOverDraft(Int64 lLId)
        {
            int i = 0;
            List<PLOverDraft> pLOverDraftObjList = new List<PLOverDraft>();
            pLOverDraftObjList = pLManagerObj.SelectOverDraft(lLId);
            if (pLOverDraftObjList.Count > 0)
            {
                foreach (GridViewRow gvr in overDraftGridView.Rows)
                {
                    (gvr.FindControl("limitTextBox") as TextBox).Text = pLOverDraftObjList[i].Limit.ToString();
                    (gvr.FindControl("rateTextBox") as TextBox).Text = pLOverDraftObjList[i].Rate.ToString();
                    (gvr.FindControl("q1TextBox") as TextBox).Text = pLOverDraftObjList[i].Q1.ToString();
                    (gvr.FindControl("q2TextBox") as TextBox).Text = pLOverDraftObjList[i].Q2.ToString();
                    (gvr.FindControl("q3TextBox") as TextBox).Text = pLOverDraftObjList[i].Q3.ToString();
                    (gvr.FindControl("q4TextBox") as TextBox).Text = pLOverDraftObjList[i].Q4.ToString();
                    (gvr.FindControl("OldIdLabel") as Label).Text = pLOverDraftObjList[i].Id.ToString();
                    i = i + 1;
                    if (i > 2)
                    {
                        break;
                    }
                }
            }
        }
        private void FieldClear()
        {
            remarksTextBox.Text="";
            plDropdownList.SelectedIndex = 0;
            segmentCodeDropDownList.SelectedIndex=0;
            loanTypeDropDownList.SelectedIndex=0;
            incAssmntMethodDropDownList.SelectedIndex=0;
            sectorDropDownList.SelectedIndex=0;
            subsectorTextBox.Text = "";
            companyNameDropDownList.SelectedIndex = 0;
            companyTypeDropDownList.SelectedIndex = 0;
            monthlySalaryIncomeTextBox.Text = "";
            percentageOfOwnershipTextBox.Text = "";
            profitMarginTextBox.Text = "";
            otherIncomeDropDownList.SelectedIndex = 0;
            otherIncomeTextBox.Text = "";
            netIncomeTextBox.Text = "";
            declaredIncomeTextBox.Text = "";
            scbCreditLimitTextBox.Text = "";
            industryProfitTextBox.Text = "";
            plOutstandingTextBox.Text = "";
            proportionateMonthlyInterestTextBox.Text = "";
            residentialStatusDropDownList.SelectedIndex = 0;
            flSecurityTextBox.Text = "";
            averageBalanceTextBox.Text = "";
            emiWithSCBDropdownList.SelectedIndex = 0;
            emiWithSCBTextBox.Text = "";
            otherEMITextBoxWithLabel.Text = "";
            otherEMITextBox.Text = "";
            instabuyBalanceTextBox.Text = "";
            instabuyInstallmentTextBox.Text = "";
            dBRAllowableMaxLoanDropDownList.SelectedIndex = 0;
            bBPDDAllowedMaxLoanAmountTextBox.Text="";
            dBRAllowableMaxLoanTextBox2.Text = "";
            dBRAllowableMaxLoanDropDownList.SelectedIndex = 0;
            dBRAllowableMaxLoanTextBox1.Text = "";
            avgBalanceSupportedMaxLoanAmountTextBox.Text = "";
            maxLoanAmountAllowableMUETextBox.Text = "";
            mUEAllowableMaxLoanAmountTextBox.Text = "";
            interestRateTextBox.Text = "";
            tenureTextBox.Text = "";
            firstPaymentDropDownList.SelectedIndex = 0;
            safetyPlusDropDownList.SelectedIndex = 0;
            appliedLoanAmountTextBox.Text = "";
            appropriatedLoanAmountTextBox.Text = "";
            proposedLoanAmountTextBox.Text = "";
            emiTextBox.Text = "";
            emiFactorTextBox.Text = "";
            dbrTextBox.Text = "";
            mueTextBox.Text = "";
            professionLabel.Text = "";
            bancaLoanAmountTextBox.Text = "";
            bancaTextBox.Text = "";
            bancaWithoutDBRTextBox.Text = "";
            bancaDBRTextBox.Text = "";
            ageLabel.Text = "";
            companyIdHiddenField.Value = "";
            securityDropDownList.SelectedIndex = 0;
            declineButton.Enabled = true;
            deferButton.Enabled = true;
            forwardButton.Enabled = true;
            Session["CreditTurnOver"] = "";
            Session["AverageBalance"] = "";
            averageBalanceHiddenField.Value = "";
            gMueHiddenField.Value = "";
            bancaMUETextBox.Text = "";
            categoryDropDownList.SelectedIndex = 0;
            dbrHF.Value = "";
            ageHF.Value = "";
        }
        private void GridClear()
        {
            foreach (GridViewRow gvr in overDraftGridView.Rows)
            {
                (gvr.FindControl("limitTextBox") as TextBox).Text = "";
                (gvr.FindControl("rateTextBox") as TextBox).Text = "";
                (gvr.FindControl("q1TextBox") as TextBox).Text = "";
                (gvr.FindControl("q2TextBox") as TextBox).Text = "";
                (gvr.FindControl("q3TextBox") as TextBox).Text = "";
                (gvr.FindControl("q4TextBox") as TextBox).Text = "";
            }
        }
        protected void declineButton_Click(object sender, EventArgs e)
        {
            try
            {
                string targetURL = null;
                if (llidTextBox.Text != "")
                {
                    targetURL = "DeclineUI.aspx?lLId=" + llidTextBox.Text.Trim();
                    ScriptManager.RegisterClientScriptBlock(declineButton, this.GetType(), "declineButton", "<script type='text/javascript'>window.open('../UI/DeclineUI.aspx?lLId=" + llidTextBox.Text.Trim() + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1')</script>", false);
                }
                else
                {
                    Alert.Show("Please input LLId");
                    llidTextBox.Focus();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Decline PL");
            }
        }
        protected void deferButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsBlankData())
                {
                    if(ChechRemarks())
                    {
                        SaveData(3,2);
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save PL Defer");
            }
        }
        private bool ChechRemarks()
        {
            Int32 checkCount = 0;
            //foreach (GridViewRow gvr in remarksGridView.Rows)
            //{
            //    if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked == true)
            //    {
            checkCount = checkCount + 1;
            //    }
            //}
            if (checkCount==0)
            {
                Alert.Show("Please Select Remarks");
                return false;
            }
            else
            {
                return true;
            }
        }
        private bool LoadExistingPLInfo(Int64 lLId)
        {
            PL pLObj = new PL();
            pLObj = pLManagerObj.SelectPLInfo(lLId);
            if (pLObj != null && pLObj.PlId > 0)
            {
                llidTextBox.Text = pLObj.PlLlid.ToString();
                plDropdownList.SelectedValue = pLObj.PlProductId.ToString();
                if (pLObj.PlProductId == 1 || pLObj.PlProductId == 12)
                {
                    cashsecurityLabel.Style.Add("visibility", "hidden");
                    securityDropDownList.Style.Add("visibility", "hidden");
                }
                else
                {
                    cashsecurityLabel.Style.Add("visibility", "visible");
                    securityDropDownList.Style.Add("visibility", "visible");
                }
                segmentCodeDropDownList.SelectedValue = pLObj.PlSegmentId.ToString();
                ShowHideFields(pLObj.PlSegmentId);
                loanTypeDropDownList.SelectedValue = pLObj.PlLoantypeId.ToString();
                incAssmntMethodDropDownList.SelectedValue = pLObj.PlIncomeacessmentmethodId.ToString();
                try
                {
                    sectorDropDownList.SelectedValue = pLObj.PlSectorId.ToString();
                    loadSubSector(pLObj.PlSubsectorId);
                    subSectorDropDownList.SelectedValue = pLObj.PlSubsectorId.ToString();
                    LoadSubSectorCode();
                }
                catch
                {
                }
                loanInformationObj = loanApplicationManagerObj.GetLoanInformation(Convert.ToInt32(llidTextBox.Text));
                try
                {

                    payRollManagerObj=new PayRollManager();
                
                    companyNameDropDownList.SelectedValue=pLObj.PlCompanyId.ToString();
                    companyIdHiddenField.Value = pLObj.PlCompanyId.ToString();
                    companyTypeDropDownList.SelectedValue = pLObj.PlCompanytypeId.ToString();
                    if (pLObj.PlCompanytypeId == 0 || pLObj.PlCompanytypeId == 2)
                    {
                        //OneAddVisibleFalse();
                    }
                    else
                    {
                        //OneAddVisibleTrue();
                    }
                }
                catch (Exception ex)
                {
                }
                if (loanInformationObj.Business.ToString() != null)
                {
                    List<PayRoll> payRollObjList = payRollManagerObj.SearchPayRoll(loanInformationObj.Business.ToString());
                    if (payRollObjList.Count > 0)
                    {
                        try
                        {

                            companyNameDropDownList.SelectedValue = payRollObjList[0].CompID.ToString();
                            companyIdHiddenField.Value = payRollObjList[0].CompID.ToString();
                        }
                        catch
                        {
                        }
                    }
                    //LoadCompanyInfo(loanInformationObj.Business.ToString());
                }
                categoryDropDownList.SelectedValue = pLObj.PlDoctorCategory.ToString();
                monthlySalaryIncomeTextBox.Text = pLObj.PlIncome.ToString();
                percentageOfOwnershipTextBox.Text = pLObj.PlOwnership.ToString();
                profitMarginTextBox.Text = pLObj.PlProfitmargin.ToString();
                otherIncomeDropDownList.SelectedValue = pLObj.PlOtherincomesourceId.ToString();
                otherIncomeTextBox.Text = pLObj.PlOtherincome.ToString();
                netIncomeTextBox.Text = pLObj.PlNetincome.ToString();
                declaredIncomeTextBox.Text = pLObj.PlDeclaredincome.ToString();
                scbCreditLimitTextBox.Text = pLObj.PlScbcreditcardlimit.ToString();
                industryProfitTextBox.Text = pLObj.PlIndustryprofitmarginrat.ToString();
                plOutstandingTextBox.Text = pLObj.PlPliplfloutstanding.ToString();
                proportionateMonthlyInterestTextBox.Text = pLObj.PlMonthlyinterest.ToString();
                residentialStatusDropDownList.SelectedValue = pLObj.PlResiedentialstatusId.ToString();
                flSecurityTextBox.Text = pLObj.PlFlsecuityos.ToString();
                averageBalanceTextBox.Text = pLObj.PlAveragebalance.ToString();
                emiWithSCBDropdownList.SelectedValue = pLObj.PlEmiwithscbsourceId.ToString();
                emiWithSCBTextBox.Text = pLObj.PlEmiwithscb.ToString();
                otherEMITextBoxWithLabel.Text = pLObj.PlOtheremisource.ToString();
                otherEMITextBox.Text = pLObj.PlOtheremi.ToString();
                instabuyBalanceTextBox.Text = pLObj.PlInstabuybalance.ToString();
                instabuyInstallmentTextBox.Text = pLObj.PlInstabuyinstallment.ToString();
                bBPDDAllowedMaxLoanAmountTextBox.Text = pLObj.PlBbpddmaxloanamound.ToString();
                dBRAllowableMaxLoanTextBox2.Text = pLObj.PlDbrmultiplier.ToString();
                dbrHF.Value = pLObj.PlDbrmultiplier.ToString();
                dBRAllowableMaxLoanDropDownList.SelectedValue = pLObj.PlLevel.ToString();
                dBRAllowableMaxLoanTextBox1.Text = pLObj.PlDbrmaxloanamount.ToString();
                avgBalanceSupportedMaxLoanAmountTextBox.Text = pLObj.PlAvgbalancmaxloanamount.ToString();
                maxLoanAmountAllowableMUETextBox.Text = pLObj.PlMuemultiplier.ToString();
                gMueHiddenField.Value = pLObj.PlMuemultiplier.ToString();
                mUEAllowableMaxLoanAmountTextBox.Text = pLObj.PlMuemaxloanamount.ToString();
                if (oneAddDropDownList.Visible == true)
                {
                    oneAddDropDownList.SelectedValue = pLObj.PlAddOneId.ToString();
                }
                interestRateTextBox.Text = pLObj.PlInterestrat.ToString();
                tenureTextBox.Text = pLObj.PlTenure.ToString();
                firstPaymentDropDownList.SelectedValue = pLObj.PlFirstpayment.ToString();
                if (pLObj.PlSafetyplusId == 0 || pLObj.PlSafetyplusId == 2)
                {
                    HideBancaInfo();
                }
                else
                {
                    ShowBancaInfo();
                }
                safetyPlusDropDownList.SelectedValue = pLObj.PlSafetyplusId.ToString();
                appliedLoanAmountTextBox.Text = pLObj.PlAppliedloanamount.ToString();
                appropriatedLoanAmountTextBox.Text = pLObj.PlAppropriatedloanamount.ToString();
                proposedLoanAmountTextBox.Text = pLObj.PlProposedloanamount.ToString();
                dBRAllowableMaxLoanDropDownList.SelectedValue = pLObj.PlLevel.ToString();
                emiTextBox.Text = pLObj.PlEmi.ToString();
                emiFactorTextBox.Text = pLObj.PlEmifactor.ToString();
                dbrTextBox.Text = pLObj.PlDbr.ToString();
                mueTextBox.Text = pLObj.PlMue.ToString();
                bancaLoanAmountTextBox.Text = pLObj.PlBancaLoanAmount.ToString();
                bancaTextBox.Text = pLObj.PlBanca.ToString();
                bancaWithoutDBRTextBox.Text = pLObj.PlBancaWithoutDBR.ToString();
                bancaDBRTextBox.Text = pLObj.PlBancaDBR.ToString();
                bancaMUETextBox.Text = pLObj.PlBancaMUE.ToString();
                ageLabel.Text = pLObj.PlCurrentAge.ToString();
                if(Convert.ToInt16(ageLabel.Text.Substring(0,2))>55)
                {
                    Alert.Show("Not Eligible for BANCA as Age Greater Than 55 years!");
                    safetyPlusDropDownList.SelectedIndex = 1;
                }
                securityDropDownList.SelectedValue = pLObj.PlSecurity.ToString();
                remarksTextBox.Text = pLObj.PlRemarks.ToString();
                LoadOverDraft(pLObj.PlLlid);
                AddOneFunction(pLObj.PlSegmentId);
                //if (pLObj.Status == 0 && pLObj.UserId.ToString() == Session["Id"].ToString())
                if (pLObj.Status == 0)
                {
                    declineButton.Enabled = true;
                    deferButton.Enabled = true;
                    forwardButton.Enabled = true;
                }
                else
                {
                    declineButton.Enabled = false;
                    deferButton.Enabled = false;
                    forwardButton.Enabled = false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        private void ShowHideFields(Int32 segmentId)
        {
            if (segmentId > 0)
            {
                switch (segmentId)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        IncomeLevelTextChange("Monthly Salary Income ");
                        HidePercentageOfOwnership();
                        HideSectorSubSector();
                        HideShowAverageBalance();
                        CompanyInfoVisibleTrue();
                        CategoryShowFalse();
                        //IndustryProfitVisibleFalse();
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                        IncomeLevelTextChange("Monthly Turnover ");
                        HidePercentageOfOwnership();
                        ShowSectorSubSector();
                        ShowAverageBalance();
                        CompanyInfoVisibleFalse();
                        CategoryShowFalse();
                        //IndustryProfitVisibleTrue();
                        break;
                    case 12:
                        IncomeLevelTextChange("Monthly Turnover ");
                        HidePercentageOfOwnership();
                        ShowSectorSubSector();
                        HideShowAverageBalance();
                        CompanyInfoVisibleTrue();
                        CategoryShowTrue();
                        //IndustryProfitVisibleTrue();
                        break;
                    case 13:
                        IncomeLevelTextChange("Monthly Turnover ");
                        HidePercentageOfOwnership();
                        ShowSectorSubSector();
                        HideShowAverageBalance();
                        CompanyInfoVisibleTrue();
                        CategoryShowTrue();
                        //IndustryProfitVisibleTrue();
                        break;
                    case 14:
                        IncomeLevelTextChange("Monthly Rent Income ");
                        ShowPercentageOfOwnership();
                        ShowSectorSubSector();
                        HideShowAverageBalance();
                        CompanyInfoVisibleFalse();
                        CategoryShowFalse();
                        //IndustryProfitVisibleFalse();
                        break;
                    case 15:
                        IncomeLevelTextChange("Monthly Rent Income ");
                        ShowPercentageOfOwnership();
                        ShowSectorSubSector();
                        HideShowAverageBalance();
                        CompanyInfoVisibleFalse();
                        CategoryShowFalse();
                        //IndustryProfitVisibleFalse();
                        break;
                }
            }
        }
        private void IncomeLevelTextChange(string changeText)
        {
            monthlySalaryIncomeLabel.Text = changeText;
        }
        //private void IndustryProfitVisibleTrue()
        //{
        //    industryProfitLabel.Style.Add("visibility", "visible");
        //    industryProfitTextBox.Style.Add("visibility", "visible");
        //}
        //private void IndustryProfitVisibleFalse()
        //{
        //    industryProfitLabel.Style.Add("visibility", "hidden");
        //    industryProfitTextBox.Style.Add("visibility", "hidden");
        //}
        private void ShowSectorSubSector()
        {
            sectorLabel.Style.Add("visibility", "visible");
            sectorDropDownList.Style.Add("visibility", "visible");
            subSectorLabel.Style.Add("visibility", "visible");
            subSectorDropDownList.Style.Add("visibility", "visible");
            subsectorTextBox.Style.Add("visibility", "visible");
        }
        private void HideSectorSubSector()
        {
            sectorLabel.Style.Add("visibility", "hidden");
            sectorDropDownList.Style.Add("visibility", "hidden");
            subSectorLabel.Style.Add("visibility", "hidden");
            subSectorDropDownList.Style.Add("visibility", "hidden");
            subsectorTextBox.Style.Add("visibility", "hidden");
        }
        private void ShowAverageBalance()
        {
            averageBalanceLabel.Style.Add("visibility", "visible");
            averageBalanceTextBox.Style.Add("visibility", "visible");
            avgBalanceSupportedMaxLoanAmountLabel.Style.Add("visibility", "visible");
            avgBalanceSupportedMaxLoanAmountTextBox.Style.Add("visibility", "visible");
        }
        private void HideShowAverageBalance()
        {
            averageBalanceLabel.Style.Add("visibility", "hidden");
            averageBalanceTextBox.Style.Add("visibility", "hidden");
            avgBalanceSupportedMaxLoanAmountLabel.Style.Add("visibility", "hidden");
            avgBalanceSupportedMaxLoanAmountTextBox.Style.Add("visibility", "hidden");
        }
        private void ShowPercentageOfOwnership()
        {
            percentageOfOwnershipTextBox.Style.Add("visibility", "visible");
            percentageOfOwnershipLabel.Style.Add("visibility", "visible");
            Label3.Style.Add("visibility", "visible");
        }
        private void HidePercentageOfOwnership()
        {
            if (segmentCodeDropDownList.SelectedIndex > 5 && segmentCodeDropDownList.SelectedIndex < 12)
            {
                ShowPercentageOfOwnership();
                //companyInfoButton.Style.Add("visibility", "hidden");
            }
            else
            {
                //percentageOfOwnershipTextBox.Text = "100";
                percentageOfOwnershipTextBox.Style.Add("visibility", "hidden");
                percentageOfOwnershipLabel.Style.Add("visibility", "hidden");
                Label3.Style.Add("visibility", "hidden");
            }
            //if (segmentCodeDropDownList.SelectedIndex > 0)
            //{
            //    percentageOfOwnershipTextBox.Style.Add("visibility", "hidden");
            //    percentageOfOwnershipLabel.Style.Add("visibility", "hidden");
            //    Label3.Style.Add("visibility", "hidden");
            //}  
        }

        private void CategoryShowTrue()
        {
            categoryDropDownList.Style.Add("visibility", "visible");
        }
        private void CategoryShowFalse()
        {
            categoryDropDownList.Style.Add("visibility", "hidden");
        }
    
        private void HideBancaInfo()
        {
            bancaLoanAmountTextBox.Style.Add("visibility", "hidden");
            bancaTextBox.Style.Add("visibility", "hidden");
            bancaWithoutDBRTextBox.Style.Add("visibility", "hidden");
            bancaDBRTextBox.Style.Add("visibility", "hidden");
            bancaLoanAmountLabel.Style.Add("visibility", "hidden");
            bancaLabel.Style.Add("visibility", "hidden");
            bancaWithoutDBRLabel.Style.Add("visibility", "hidden");
            bancaDBRLabel.Style.Add("visibility", "hidden");
            bancaDBRPerLabel.Style.Add("visibility", "hidden");
        }
        private void ShowBancaInfo()
        {
            bancaLoanAmountTextBox.Style.Add("visibility", "visible");
            bancaTextBox.Style.Add("visibility", "visible");
            bancaWithoutDBRTextBox.Style.Add("visibility", "visible");
            bancaDBRTextBox.Style.Add("visibility", "visible");
            bancaLoanAmountLabel.Style.Add("visibility", "visible");
            bancaLabel.Style.Add("visibility", "visible");
            bancaWithoutDBRLabel.Style.Add("visibility", "visible");
            bancaDBRLabel.Style.Add("visibility", "visible");
            bancaDBRPerLabel.Style.Add("visibility", "visible");

        }
        private void CompanyInfoVisibleTrue()
        {
            companyNameLabel.Style.Add("visibility", "visible");
            companyNameDropDownList.Style.Add("visibility", "visible");
            companTypeLabel.Style.Add("visibility", "visible");
            companyTypeDropDownList.Style.Add("visibility", "visible");
            //companyInfoButton.Style.Add("visibility", "visible");
        }
        private void CompanyInfoVisibleFalse()
        {
            companyNameLabel.Style.Add("visibility", "hidden");
            companyNameDropDownList.Style.Add("visibility", "hidden");
            companTypeLabel.Style.Add("visibility", "hidden");
            companyTypeDropDownList.Style.Add("visibility", "hidden");
            //companyInfoButton.Style.Add("visibility", "hidden");
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            FieldClear();
            GridClear();
            FillRemarksGridView();
        }

        protected void loanTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //segmentCodeDropDownList_SelectedIndexChanged(sender, e);
            LoadSegmentInfo(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ale", "JS_FunctionEMIFactor();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);
        }
        protected void dBRAllowableMaxLoanDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (segmentCodeDropDownList.SelectedIndex > 0)
            {
                Segment segmentObj = segmentManagerObj.SelectSegment(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
                if (segmentObj != null)
                {
                    LoadSegmentInfo(segmentObj.Id);
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ale", "JS_FunctionEMIFactor();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);

        }
        protected void oneAddDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (oneAddDropDownList.Visible == true)
            {
                if (oneAddDropDownList.SelectedIndex > 0)
                {
                    if (oneAddDropDownList.SelectedValue == "1")
                    {
                        maxLoanAmountAllowableMUETextBox.Text = (Convert.ToInt32("0" + gMueHiddenField.Value.ToString()) + 1).ToString();
                    }
                    else if (oneAddDropDownList.SelectedValue == "2")
                    {
                        maxLoanAmountAllowableMUETextBox.Text = (Convert.ToInt32("0" + gMueHiddenField.Value.ToString()) + 2).ToString();
                    }
                    else
                    {
                        maxLoanAmountAllowableMUETextBox.Text = (Convert.ToInt32("0" + gMueHiddenField.Value.ToString())).ToString();
                    }
                }
                else
                {
                    maxLoanAmountAllowableMUETextBox.Text = (Convert.ToInt32("0" + gMueHiddenField.Value.ToString())).ToString();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale", "JS_FunctionEMIFactor();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionDBRAllowedMaxLoan();", true);
            }
        }
        protected void monthlySalaryIncomeTextBox_TextChanged(object sender, EventArgs e)
        {
            //segmentCodeDropDownList_SelectedIndexChanged(sender, e);
            LoadSegmentInfo(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
        }
        protected void otherIncomeTextBox_TextChanged(object sender, EventArgs e)
        {
            //segmentCodeDropDownList_SelectedIndexChanged(sender, e);
            LoadSegmentInfo(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
        }
        //protected void profitMarginTextBox_TextChanged(object sender, EventArgs e)
        //{
        //    segmentCodeDropDownList_SelectedIndexChanged(sender, e);
        //}
        //protected void profitMarginTextBox_DataBinding(object sender, EventArgs e)
        //{
        //    segmentCodeDropDownList_SelectedIndexChanged(sender, e);
        //}
        protected void declaredIncomeTextBox_TextChanged(object sender, EventArgs e)
        {
            segmentCodeDropDownList_SelectedIndexChanged(sender, e);
        }
        protected void residentialStatusDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            segmentCodeDropDownList_SelectedIndexChanged(sender, e);
        }
        protected void categoryDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (categoryDropDownList.SelectedValue != "")
            {
                if (segmentCodeDropDownList.SelectedValue.ToString() == "12")
                {
                    switch (categoryDropDownList.SelectedValue.Substring(0, 1).ToString())
                    {
                        case "A":
                            gMueHiddenField.Value = string.Empty;
                            gMueHiddenField.Value = "18";
                            maxLoanAmountAllowableMUETextBox.Text = "18";                        
                            break;
                        case "B":
                            gMueHiddenField.Value = string.Empty;
                            gMueHiddenField.Value = "16";
                            maxLoanAmountAllowableMUETextBox.Text = "16";                        
                            break;
                        case "C":
                            gMueHiddenField.Value = string.Empty;
                            gMueHiddenField.Value = "14";
                            maxLoanAmountAllowableMUETextBox.Text = "14";                        
                            break;
                    }
                }
                else if (segmentCodeDropDownList.SelectedValue.ToString() == "13")
                {
                    switch (categoryDropDownList.SelectedValue.Substring(0, 1).ToString())
                    {
                        case "A":
                            gMueHiddenField.Value = string.Empty;
                            gMueHiddenField.Value = "16";
                            maxLoanAmountAllowableMUETextBox.Text = "16";                        
                            break;
                        case "B":
                            gMueHiddenField.Value = string.Empty;
                            gMueHiddenField.Value = "14";
                            maxLoanAmountAllowableMUETextBox.Text = "14";                        
                            break;
                        case "C":
                            gMueHiddenField.Value = string.Empty;
                            gMueHiddenField.Value = "12";
                            maxLoanAmountAllowableMUETextBox.Text = "12";                        
                            break;
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionDBRAllowedMaxLoan();JS_FunctionMEUAllowableMaxLoanAmount();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);
            }

        }


        protected void companyTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            PayRoll payRollObj = new PayRoll();
            //payRollObj = payRollManagerObj.SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.SelectedItem.Text);// companyNameDropDownList.Text);
            payRollObj = SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.SelectedItem.Text, segmentCodeDropDownList.SelectedItem.Text);// companyNameDropDownList.Text);
            if (payRollObj == null)
            {
                Segment segmentObj = segmentManagerObj.SelectSegment(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
                if (segmentObj != null)
                {
                    string MUE = "";
                    switch (companyTypeDropDownList.SelectedValue.ToString())
                    {
                        case "1":
                            maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUEMul.ToString();
                            gMueHiddenField.Value = segmentObj.MUEMul.ToString();                    
                   
                            break;
                        case "2":
                            maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUELar.ToString();
                            gMueHiddenField.Value = segmentObj.MUELar.ToString();
                   
                            break;
                        case "3":
                            maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUELoc.ToString();
                            gMueHiddenField.Value = segmentObj.MUELoc.ToString();
                            break;
                    }
            
                }
                else
                {
                    //----------------------------------------------------------------------------------------
    
                    if (companyNameDropDownList.SelectedIndex != 0)
                
                    {
                        // payRollObj = payRollManagerObj.SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.SelectedItem.Text);// companyNameDropDownList.Text);

                        if (payRollObj != null)
                        {
                            maxLoanAmountAllowableMUETextBox.Text = payRollObj.CompMue.ToString();
                            gMueHiddenField.Value = payRollObj.CompMue.ToString();

                        }
                    }

                    //-----------------------------------------------------------------------------------------
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionEMIFactor();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);
            }
        }
        protected void profitMarginTextBox_TextChanged(object sender, EventArgs e)
        {
            if (segmentCodeDropDownList.SelectedIndex > 0)
            {
                LoadSegmentInfo(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
                AddOneFunction(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale", "JS_FunctionEMIFactor();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionEMIFactor();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);

            }  
        }
        protected void netIncomeTextBox_TextChanged(object sender, EventArgs e)
        {
            //if (segmentCodeDropDownList.SelectedIndex > 0)
            //{
            //    LoadSegmentInfo(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
            //    AddOneFunction(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ale", "JS_FunctionEMIFactor();", true);
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionEMIFactor();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);
            //} 
            ChangeDBR();
        }
        private void ChangeDBR()
        {
            if (segmentCodeDropDownList.SelectedIndex > 0)
            {
                LoadSegmentInfo(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
                AddOneFunction(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale", "JS_FunctionEMIFactor();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionEMIFactor();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);
            } 
        }
        protected void limitTextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeDBR();
        }
        protected void rateTextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeDBR();
        }
        protected void q1TextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeDBR();
        }
        protected void q2TextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeDBR();
        }
        protected void q3TextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeDBR();
        }
        protected void q4TextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeDBR();
        }
        protected void percentageOfOwnershipTextBox_TextChanged(object sender, EventArgs e)
        {
            ChangeDBR();
        }
        protected void companyNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSegmentInfo(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
            companyTypeDropDownList.SelectedIndex = 0;
            //PayRoll payRollObj = new PayRoll();
            ////payRollObj = payRollManagerObj.SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.SelectedItem.Text);// companyNameDropDownList.Text);
            //payRollObj = SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.SelectedItem.Text, segmentCodeDropDownList.SelectedItem.Text);// companyNameDropDownList.Text);
            //if (payRollObj != null)
            //{
            //    if (payRollObj.CompCatstatus.Trim() != "Unapproved")
            //    {
            //        Segment segmentObj = segmentManagerObj.SelectSegment(Convert.ToInt32(segmentCodeDropDownList.SelectedValue.ToString()));
            //        if (segmentObj != null)
            //        {
            //            switch (companyTypeDropDownList.SelectedValue.ToString())
            //            {
            //                case "1":
            //                    maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUEMul.ToString();
            //                    gMueHiddenField.Value = segmentObj.MUEMul.ToString();
            //                    break;
            //                case "2":
            //                    maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUELar.ToString();
            //                    gMueHiddenField.Value = segmentObj.MUELar.ToString();
            //                    break;
            //                case "3":
            //                    maxLoanAmountAllowableMUETextBox.Text = segmentObj.MUELoc.ToString();
            //                    gMueHiddenField.Value = segmentObj.MUELoc.ToString();
            //                    break;
            //            }
            //        }
            //    }
           
            //}
            //else
            //{
            //    //----------------------------------------------------------------------------------------
            //    string test = companyNameDropDownList.Text;
            //    test = companyNameDropDownList.SelectedItem.Value;
            //    test = companyNameDropDownList.SelectedItem.Text;
            //    if (companyNameDropDownList.SelectedIndex != 0)
            //    {
            //        // payRollObj = payRollManagerObj.SelectCompanyInfo(Convert.ToInt32(companyNameDropDownList.SelectedValue.ToString()), companyNameDropDownList.SelectedItem.Text);// companyNameDropDownList.Text);

            //        if (payRollObj != null)
            //        {
            //            maxLoanAmountAllowableMUETextBox.Text = payRollObj.CompMue.ToString();
            //            gMueHiddenField.Value = payRollObj.CompMue.ToString();
            //            interestRateTextBox.Text = payRollObj.CompIrwithouteosb.ToString();

            //        }

            //    }

            //    //-----------------------------------------------------------------------------------------
            //}
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ale1", "JS_FunctionEMIFactor();JS_FunctionDBRAllowedMaxLoan();JS_FunctionBancaFieldChange();JS_FunctionBancaCalculation();JS_FunctionEMICalculation();JS_FunctionDBRParcent();JS_FunctionBancaDBR();JS_FunctionMUECalculation();JS_FunCkeckLoanAmount();", true);
        }

        public PayRoll SelectCompanyInfo(Int32 companyId, string companyName,string segmentId)
        {
            string idCondition = string.Empty;
            string nameCondition = string.Empty;
            if (companyId > 0)
            {
                idCondition = " AND COMP_ID=" + companyId;
            }
            else
            {
                idCondition = string.Empty;
            }
            if (!string.IsNullOrEmpty(companyName))
            {
                nameCondition = " AND TRIM(UCASE(COMP_NAME)) LIKE ('%" + companyName.ToUpper().Trim() + "%')";
            }
            else
            {
                nameCondition = string.Empty;
            }
            string queryString = "SELECT COMP_ID, COMP_NAME, COMP_SEGMENT_ID, " +
                                 "COMP_STRUCTURE, COMP_DOCATEGORIZED, COMP_CATSTATUS, COMP_MUE, COMP_IRWITHEOSB, " +
                                 "COMP_IRWITHOUTEOSB, COMP_CCUCONDITION, COMP_CCUREMARKS, " +
                                 "COMP_PROPOSDBY, COMP_APPROVALDATE, COMP_STATUS,COMP_EXTRABENEFIT,COMP_EXTRAFIELD1,COMP_EXTRAFIELD2,COMP_EXTRAFIELD3,COMP_EXTRAFIELD4 " +
                                 "FROM t_pluss_company " +
                                 "WHERE COMP_ENTRYSTATUS=1 " + idCondition + nameCondition + " AND COMP_SEGMENT_ID='" + segmentId + "' ORDER BY COMP_NAME";
            PayRoll tPlussCompanyObj = null;
            DataTable payRollDataTableObj = DbQueryManager.GetTable(queryString);
            if (payRollDataTableObj != null)
            {
                DataTableReader payRollDataReaderObj = payRollDataTableObj.CreateDataReader();

                while (payRollDataReaderObj.Read())
                {
                    tPlussCompanyObj = new PayRoll();
                    tPlussCompanyObj.CompID = Convert.ToInt32(payRollDataReaderObj["Comp_ID"]);
                    tPlussCompanyObj.CompName = Convert.ToString(payRollDataReaderObj["Comp_Name"]);
                    tPlussCompanyObj.CompSegmentID = Convert.ToString(payRollDataReaderObj["Comp_Segment_ID"]);
                    tPlussCompanyObj.CompStructure = Convert.ToString(payRollDataReaderObj["Comp_Structure"]);
                    tPlussCompanyObj.CompDocategorized = Convert.ToString(payRollDataReaderObj["Comp_Docategorized"]);
                    tPlussCompanyObj.CompCatstatus = Convert.ToString(payRollDataReaderObj["Comp_Catstatus"]);
                    tPlussCompanyObj.CompMue = Convert.ToDouble("0" + payRollDataReaderObj["Comp_Mue"]);
                    tPlussCompanyObj.CompIrwitheosb = Convert.ToString(payRollDataReaderObj["Comp_Irwitheosb"]);
                    tPlussCompanyObj.CompIrwithouteosb = Convert.ToString(payRollDataReaderObj["Comp_Irwithouteosb"]);
                    tPlussCompanyObj.CompCcucondition = Convert.ToString(payRollDataReaderObj["Comp_Ccucondition"]);
                    tPlussCompanyObj.CompCcuremarks = Convert.ToString(payRollDataReaderObj["Comp_Ccuremarks"]);
                    tPlussCompanyObj.CompProposdby = Convert.ToString(payRollDataReaderObj["Comp_Proposdby"]);
                    tPlussCompanyObj.CompApprovaldate = Convert.ToDateTime(payRollDataReaderObj["Comp_Approvaldate"]);
                    tPlussCompanyObj.CompStatus = Convert.ToInt32(payRollDataReaderObj["Comp_Status"]);
                    tPlussCompanyObj.ExtraBenefit = Convert.ToString(payRollDataReaderObj["COMP_EXTRABENEFIT"]);
                    tPlussCompanyObj.ExtraField1 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD1"]);
                    tPlussCompanyObj.ExtraField2 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD2"]);
                    tPlussCompanyObj.ExtraField3 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD3"]);
                    tPlussCompanyObj.ExtraField4 = Convert.ToString(payRollDataReaderObj["COMP_EXTRAFIELD4"]);
                }
                payRollDataReaderObj.Close();

            }
            return tPlussCompanyObj;
        }

        //MUE
        // public void LoadMUE(string segmentType,string companyCategory,string companyName

  
    }
}
