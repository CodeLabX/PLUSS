﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_ScoreCardCondUI" Title="Untitled Page" Codebehind="ScoreCardCondUI.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div style="margin-top: 30px; margin-left: 30px">
        <table class="style1" cellpadding="0" cellspacing="0">
            <tr style="font-weight: bold">
                <td align="center" style="font-size: 25px">
                    Score Card Condition Setting
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="clearButton" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:HiddenField ID="idHiddenField" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr align="center">
                <td class="style2">
                    <table>
                        <tr align="center">
                            <td align="right" style="width: 100px">
                                Variable Name:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="clearButton" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="addButton" EventName="Click" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:DropDownList ID="variableNameDropDownList" runat="server" Width="180px">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                &nbsp
                            </td>
                            <td align="right" style="width: 100px">
                                Comp Op:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="clearButton" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="addButton" EventName="Click" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:DropDownList ID="compOpDropDownList" runat="server" Width="150px">
                                            <asp:ListItem Value="Null">Null</asp:ListItem>
                                            <asp:ListItem Value="&lt;">&lt;</asp:ListItem>
                                            <asp:ListItem Value="&gt;">&gt;</asp:ListItem>
                                            <asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
                                            <asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
                                            <asp:ListItem Value="==">==</asp:ListItem>
                                            <asp:ListItem>Married</asp:ListItem>
                                            <asp:ListItem>DIP,GRD,HSC</asp:ListItem>
                                            <asp:ListItem>Yes</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr align="center">
                            <td align="right" style="width: 100px">
                                Comp Value:
                            </td>
                            <td align="left">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:TextBox ID="compValueTextBox" runat="server"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                &nbsp
                            </td>
                            <td align="right" style="width: 100px">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr align="center">
                            <td align="right" style="width: 100px">
                                Positive Value:
                            </td>
                            <td align="left">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:TextBox ID="scoreValueTextBox" runat="server" Width="150px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                &nbsp
                            </td>
                            <td align="right" style="width: 100px">
                                &nbsp;Negative Value:
                            </td>
                            <td align="left">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:TextBox ID="negativeValueTextBox" runat="server"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 100px">
                                Cut off Score:
                            </td>
                            <td align="left">
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:TextBox ID="cutOffScoreTextBox" runat="server" Width="150px" ReadOnly="true"
                                            BorderWidth="0px" Font-Bold="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="style4">
                                Intercept Value:
                            </td>
                            <td align="left" class="style5">
                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:TextBox ID="interceptValueTextBox" runat="server" Width="150px" ReadOnly="true"
                                            BorderWidth="0px" Font-Bold="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td colspan="3" class="style5">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="style4">
                                Status:
                            </td>
                            <td align="left" class="style5">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="clearButton" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="addButton" EventName="Click" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:DropDownList ID="logicalOpDropDownList" runat="server" Width="150px">
                                            <asp:ListItem Value="Active">Active</asp:ListItem>
                                            <asp:ListItem Value="Inactive">Inactive</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td colspan="3" class="style5">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Button ID="addButton" runat="server" Text="Add" Width="80px" Height="25px" OnClick="addButton_Click" />
                            &nbsp;&nbsp;<asp:Button ID="clearButton" runat="server" Height="25px" Text="Clear"
                                Width="80px" OnClick="clearButton_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp;
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="conditionGridView" EventName="SelectedIndexChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="searchTextBox" runat="server" Width="150px"></asp:TextBox>
                    &nbsp
                    <asp:Button ID="searchbutton" runat="server" Text="Search" Width="65px" OnClick="searchbutton_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <table class="GvHeader" width="740px">
                        <tr>
                            <td width="70" align="left">
                                SL No.
                            </td>
                            <td width="150" align="center">
                                Variable Name
                            </td>
                            <td width="90" align="justify">
                                Comp Operator
                            </td>
                            <td width="90" align="justify">
                                Comp Value
                            </td>
                            <td width="90" align="justify">
                                Positive Value
                            </td>
                            <td width="90" align="justify">
                                Negative Value
                            </td>
                            <td width="100" align="justify">
                                Status
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <div id="scoreCardGridviewDiv" style="overflow-y: scroll; height: 160px; width: 720px;"
                        class="GridviewBorder">
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="addButton" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="searchbutton" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:GridView ID="conditionGridView" runat="server" AutoGenerateColumns="False" PageSize="5"
                                    OnSelectedIndexChanged="conditionGridView_SelectedIndexChanged" Width="720px"
                                    ShowHeader="False" OnRowDataBound="conditionGridView_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sl No." HeaderStyle-Width="40px">
                                            <ItemTemplate>
                                                <asp:Label ID="slNoLabel" runat="server" Width="20px" Visible="true" />
                                                <%# Container.DataItemIndex + 1 %>
                                                <asp:Label ID="slNoLabel2" runat="server" Width="20px" Visible="true" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="70px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Id" Visible="false" HeaderStyle-Width="70px">
                                            <ItemTemplate>
                                                <asp:Label ID="idLabel" runat="server" Text='<%# Bind("Id") %>' Width="70px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="70px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Variable Name" HeaderStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:Label ID="variableIdLabel" runat="server" Text='<%# Eval("VariableId") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="variableNameLabel" runat="server" Width="150px" Text='<%# Eval("VariableName.Name") %>'
                                                    Height="20px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="150px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comp Operator" HeaderStyle-Width="90px">
                                            <ItemTemplate>
                                                <asp:Label ID="compOperatorLabel" runat="server" Text='<%# Bind("CompareOperator") %>'
                                                    Width="90px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="90px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comp Value" HeaderStyle-Width="90px">
                                            <ItemTemplate>
                                                <asp:Label ID="compValueLabel" runat="server" Text='<%# Bind("CompareValue") %>'
                                                    Width="90px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="90px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Positive Value" HeaderStyle-Width="90px">
                                            <ItemTemplate>
                                                <asp:Label ID="scoreValueLabel" runat="server" Text='<%# Bind("ScoreValue") %>' Width="90px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="90px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Negative Value" HeaderStyle-Width="90px">
                                            <ItemTemplate>
                                                <asp:Label ID="negativeValueLabel" runat="server" Text='<%# Bind("NegativeValue") %>'
                                                    Width="90px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="90px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="logicalOperatorLabel" runat="server" Text='<%# Bind("Status") %>'
                                                    Width="100px"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="120px"></HeaderStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        &nbsp;&nbsp;No data found.
                                    </EmptyDataTemplate>
                                    <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle CssClass="FixedHeader" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="height: 50px">
                    &nbsp
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style1
        {
            height: 4px;
        }
        .style2
        {
            height: 25px;
        }
        .style4
        {
            width: 100px;
            height: 19px;
        }
        .style5
        {
            height: 19px;
        }
    </style>
</asp:Content>
