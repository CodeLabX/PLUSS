﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using BLL.Report;
using BusinessEntities.Report;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_DalyActivityReportUI : System.Web.UI.Page
    {
        private decimal totalPrice;
        private int totalNonNullPriceCount;
        private ActivityReportManager activityReportManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                activityReportManagerObj = new ActivityReportManager();
                LoadActivityReportData();
                LoadAnalystReportData();
                LoadDailyNoofApprovedVolume();
                LoadChannelWiseApprovedVolume();
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Load Daily Report");
            }
        }
        private void LoadActivityReportData()
        {
            List<ActivityReportObject> activityReportObjList = activityReportManagerObj.GetActivityReportDataList(DateTime.Now);
            receivedGridView.DataSource = activityReportObjList;
            receivedGridView.DataBind();
            pendingGridView.DataSource = activityReportObjList;
            pendingGridView.DataBind();
            processGridView.DataSource = activityReportObjList;
            processGridView.DataBind();
        }

        private void LoadAnalystReportData()
        {
            List<ActivityReportObject> activityReportObjList = activityReportManagerObj.GetActivityReportByUser(DateTime.Now,DateTime.Now);
            userProcessGridView.DataSource = activityReportObjList;
            userProcessGridView.DataBind();
        }

        private void LoadDailyNoofApprovedVolume()
        {
            List<DailyNoofApprovedVolume> dailyNoofApprovedVolumeObjList = activityReportManagerObj.GetDailyNoOfApproveVolume();
            dailyNoofApprovedVolumeGridView.DataSource = dailyNoofApprovedVolumeObjList;
            dailyNoofApprovedVolumeGridView.DataBind();
        }
        private void LoadChannelWiseApprovedVolume()
        {
            List<DailyNoofApprovedVolume> channelWiseApprovedVolumeObjList = activityReportManagerObj.DailyNoOfApprovedVolumeChannelWise();
            channelWiseGridView.DataSource = channelWiseApprovedVolumeObjList;
            channelWiseGridView.DataBind();
        }
        protected void receivedGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Int64 totalFl = 0;
            Int64 totalPLSEL = 0;
            Int64 totalPSBIZ = 0;
            Int64 totalIPF = 0;
            Int64 totalSTFL = 0;
            Int64 total = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                foreach (GridViewRow row in receivedGridView.Rows)
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells[6].ToString()))
                    {
                        totalFl += Convert.ToInt64(row.Cells[1].Text.ToString());
                        totalPLSEL += Convert.ToInt64(row.Cells[2].Text.ToString());
                        totalPSBIZ += Convert.ToInt64(row.Cells[3].Text.ToString());
                        totalIPF += Convert.ToInt64(row.Cells[4].Text.ToString());
                        totalSTFL += Convert.ToInt64(row.Cells[5].Text.ToString());
                        total += Convert.ToInt64(row.Cells[6].Text.ToString());
                        totalNonNullPriceCount++;
                    }
                }
                e.Row.Cells[0].Text = "Total: ";
                e.Row.Cells[1].Text = totalFl.ToString();
                e.Row.Cells[2].Text = totalPLSEL.ToString();
                e.Row.Cells[3].Text = totalPSBIZ.ToString();
                e.Row.Cells[4].Text = totalIPF.ToString();
                e.Row.Cells[5].Text = totalSTFL.ToString();
                e.Row.Cells[6].Text = total.ToString();
                totalPrice = 0;
                totalNonNullPriceCount = 0;
            }
        }

        protected void pendingGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Int64 totalFl = 0;
            Int64 totalPLSEL = 0;
            Int64 totalPSBIZ = 0;
            Int64 totalIPF = 0;
            Int64 totalSTFL = 0;
            Int64 total = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                foreach (GridViewRow row in pendingGridView.Rows)
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells[6].ToString()))
                    {
                        totalFl += Convert.ToInt64(row.Cells[1].Text.ToString());
                        totalPLSEL += Convert.ToInt64(row.Cells[2].Text.ToString());
                        totalPSBIZ += Convert.ToInt64(row.Cells[3].Text.ToString());
                        totalIPF += Convert.ToInt64(row.Cells[4].Text.ToString());
                        totalSTFL += Convert.ToInt64(row.Cells[5].Text.ToString());
                        total += Convert.ToInt64(row.Cells[6].Text.ToString());
                        totalNonNullPriceCount++;
                    }
                }
                e.Row.Cells[0].Text = "Total: ";
                e.Row.Cells[1].Text = totalFl.ToString();
                e.Row.Cells[2].Text = totalPLSEL.ToString();
                e.Row.Cells[3].Text = totalPSBIZ.ToString();
                e.Row.Cells[4].Text = totalIPF.ToString();
                e.Row.Cells[5].Text = totalSTFL.ToString();
                e.Row.Cells[6].Text = total.ToString();
                totalPrice = 0;
                totalNonNullPriceCount = 0;
            }
        }

        protected void processGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Int64 totalFl = 0;
            Int64 totalPLSEL = 0;
            Int64 totalPSBIZ = 0;
            Int64 totalIPF = 0;
            Int64 totalSTFL = 0;
            Int64 total = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                foreach (GridViewRow row in processGridView.Rows)
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells[6].ToString()))
                    {
                        totalFl += Convert.ToInt64(row.Cells[1].Text.ToString());
                        totalPLSEL += Convert.ToInt64(row.Cells[2].Text.ToString());
                        totalPSBIZ += Convert.ToInt64(row.Cells[3].Text.ToString());
                        totalIPF += Convert.ToInt64(row.Cells[4].Text.ToString());
                        totalSTFL += Convert.ToInt64(row.Cells[5].Text.ToString());
                        total += Convert.ToInt64(row.Cells[6].Text.ToString());
                        totalNonNullPriceCount++;
                    }
                }
                e.Row.Cells[0].Text = "Total: ";
                e.Row.Cells[1].Text = totalFl.ToString();
                e.Row.Cells[2].Text = totalPLSEL.ToString();
                e.Row.Cells[3].Text = totalPSBIZ.ToString();
                e.Row.Cells[4].Text = totalIPF.ToString();
                e.Row.Cells[5].Text = totalSTFL.ToString();
                e.Row.Cells[6].Text = total.ToString();
                totalPrice = 0;
                totalNonNullPriceCount = 0;
            }
        }

        protected void userProcessGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Int64 totalFl = 0;
            Int64 totalPLSEL = 0;
            Int64 totalPSBIZ = 0;
            Int64 totalIPF = 0;
            Int64 totalSTFL = 0;
            Int64 total = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                foreach (GridViewRow row in userProcessGridView.Rows)
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells[6].ToString()))
                    {
                        totalFl += Convert.ToInt64(row.Cells[1].Text.ToString());
                        totalPLSEL += Convert.ToInt64(row.Cells[2].Text.ToString());
                        totalPSBIZ += Convert.ToInt64(row.Cells[3].Text.ToString());
                        totalIPF += Convert.ToInt64(row.Cells[4].Text.ToString());
                        totalSTFL += Convert.ToInt64(row.Cells[5].Text.ToString());
                        total += Convert.ToInt64(row.Cells[6].Text.ToString());
                        totalNonNullPriceCount++;
                    }
                }
                e.Row.Cells[0].Text = "Total: ";
                e.Row.Cells[1].Text = totalFl.ToString();
                e.Row.Cells[2].Text = totalPLSEL.ToString();
                e.Row.Cells[3].Text = totalPSBIZ.ToString();
                e.Row.Cells[4].Text = totalIPF.ToString();
                e.Row.Cells[5].Text = totalSTFL.ToString();
                e.Row.Cells[6].Text = total.ToString();
                totalPrice = 0;
                totalNonNullPriceCount = 0;
            }
        }

        protected void dailyNoofApprovedVolumeGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            double totalLWDVolume = 0;
            double totalMTDVolume = 0;
            double totalYTDVolume = 0;
            Int64 totalLWDQty = 0;
            Int64 totalMTDQty = 0;
            Int64 totalYTDQty = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                foreach (GridViewRow row in dailyNoofApprovedVolumeGridView.Rows)
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells[6].ToString()))
                    {
                        totalLWDVolume += Convert.ToDouble(row.Cells[2].Text.ToString());
                        totalMTDVolume += Convert.ToDouble(row.Cells[4].Text.ToString());
                        totalYTDVolume += Convert.ToDouble(row.Cells[6].Text.ToString());

                        totalLWDQty += Convert.ToInt64(row.Cells[1].Text.ToString());
                        totalMTDQty += Convert.ToInt64(row.Cells[3].Text.ToString());
                        totalYTDQty += Convert.ToInt64(row.Cells[5].Text.ToString());
                    }
                }
                e.Row.Cells[0].Text = "Total: ";
                e.Row.Cells[1].Text = totalLWDQty.ToString();
                e.Row.Cells[3].Text = totalMTDQty.ToString();
                e.Row.Cells[5].Text = totalYTDQty.ToString();

                e.Row.Cells[2].Text = totalLWDVolume.ToString("##,###.00");
                e.Row.Cells[4].Text = totalMTDVolume.ToString("##,###.00");
                e.Row.Cells[6].Text = totalYTDVolume.ToString("##,###.00");
                totalPrice = 0;
                totalNonNullPriceCount = 0;
            }
        }
        protected void channelWiseGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            double totalDSVolume = 0;
            double totalSDVolume = 0;
            Int64 totalDSQty = 0;
            Int64 totalSDQty = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                foreach (GridViewRow row in channelWiseGridView.Rows)
                {
                    if (!string.IsNullOrEmpty(e.Row.Cells[4].ToString()))
                    {
                        totalDSVolume += Convert.ToDouble(row.Cells[2].Text.ToString());
                        totalSDVolume += Convert.ToDouble(row.Cells[4].Text.ToString());

                        totalDSQty += Convert.ToInt64(row.Cells[1].Text.ToString());
                        totalSDQty += Convert.ToInt64(row.Cells[3].Text.ToString());
                    }
                }
                e.Row.Cells[0].Text = "Total: ";
                e.Row.Cells[1].Text = totalDSQty.ToString();
                e.Row.Cells[3].Text = totalSDQty.ToString();

                e.Row.Cells[2].Text = totalDSVolume.ToString("##,###.00");
                e.Row.Cells[4].Text = totalSDVolume.ToString("##,###.00");
            }
        }
    }
}
