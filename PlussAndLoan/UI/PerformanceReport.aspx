﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PerformanceReport.aspx.cs" Inherits="PlussAndLoan.UI.PerformanceReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Performance Report</title>
        <script language="javascript" type="text/javascript">
            function PrintDiv() {
                //debugger;
                var myContentToPrint = document.getElementById("printDiv");
                var myWindowToPrint = window.open('', '', 'width=100%,height=100%,toolbar=0,scrollbars=-1,status=0,resizable=-1,location=0,directories=0');
                myWindowToPrint.document.write(myContentToPrint.innerHTML);
                myWindowToPrint.document.close();
                myWindowToPrint.focus();
                myWindowToPrint.print();
                myWindowToPrint.close();
                
            }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="printDiv">
    <table width="103%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td>
        
        <font color="#0033CC" size="4" face="Verdana, Arial, Helvetica, sans-serif"><strong>aPerfometer</strong></font> 
    </td>
    <td width="246"   align="right">
        <asp:Button runat="server" Text="BACK" ID="btnBack"  style="border:1px solid #83b67a; background-color: #ccffcc; text-align: center;font-size: 13px;" Height="25" Width="50" OnClick="btnBack_Click"/>
        
    </td>
  
    <td width="75"  align="left">
         <asp:imagebutton id="imgexpand" runat="server" imagealign="Bottom" imageurl="images\printer.gif" height="25px" width="78px" onclientclick="PrintDiv()" /> 
     <%-- <input type="button" name="Submit1" value="PRINT" onClick="window.print();"style="border:1px solid #999990; background-color:#ccffcc">--%>
    </td>
  </tr>
  <tr> 
    <td width="425" height="24"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
       <div id="titleDiv" runat="server" style="text-align: left; font-weight: bold; font-size: 12px"> </div>
    </font></td>
  </tr>
</table>
   	<table width="100%"  border="1px" align="center" cellpadding="0" cellspacing="0" bgcolor="#ccffcc">
	<tr>
	    <td><font size=2> <div id="product" runat="server" style="text-align: left; font-weight: bold;"> </div> </font></td>
	</tr>

   	</table>  
        

   <asp:table ID="performTable" width="100%"  border="1px" align="center" cellpadding="0" cellspacing="0" bgcolor="#ccffcc" runat="server">    
       
      <asp:TableRow>
           <asp:TableCell rowspan="3"><div align=center><strong><font color=#0066CC size=2 face= Arial>Source</font></strong></div>  </asp:TableCell>
	 <asp:TableCell ColumnSpan="4"  ><div align=center><font face= Arial><strong><font color=#0066CC size=2>Received</font></strong></font></div> </asp:TableCell>
	 <asp:TableCell ColumnSpan="4" ><div align=center><font color=#0066CC size=2 face= Arial><strong>Approved</strong></font></div> </asp:TableCell>
	 <asp:TableCell ColumnSpan="4" ><div align=center><font face= Arial><strong><font color=#0066CC size=2>Target</font></strong></font></div> </asp:TableCell>
	 <asp:TableCell ColumnSpan="4" ><div align=center><font color=#0066CC size=2 face= Arial><strong>Variance</strong></font></div> </asp:TableCell>
   </asp:TableRow>
       
        <asp:TableRow>
	            <asp:TableCell   ColumnSpan="2"><div align=center><font color=#0066CC size=2><strong><font face=Arial>Month</font></strong></font></div> </asp:TableCell>
	 <asp:TableCell  ColumnSpan="2"><div align=center><font color=#0066CC size=2><strong><font face=Arial>YTD</font></strong></font></div> </asp:TableCell>
	 <asp:TableCell ColumnSpan="2"><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Month</font></strong></font></div> </asp:TableCell>
	 <asp:TableCell  ColumnSpan="2"><div align=center><font color=#0066CC><strong><font size=2 face=Arial>YTD</font></strong></font></div> </asp:TableCell>
	
	 <asp:TableCell ColumnSpan="2"><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Month</font></strong></font></div> </asp:TableCell>
	 <asp:TableCell  ColumnSpan="2"><div align=center><font color=#0066CC><strong><font size=2 face=Arial>YTD</font></strong></font></div> </asp:TableCell>
	 <asp:TableCell  ColumnSpan="2"><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Month</font></strong></font></div> </asp:TableCell>
	 <asp:TableCell ColumnSpan="2"><div align=center><font color=#0066CC><strong><font size=2 face=Arial>YTD</font></strong></font></div> </asp:TableCell>
  </asp:TableRow>
       
   <asp:TableRow>
            <asp:TableCell><div align=center><font color=#0066CC size=2><strong><font face=Arial>Qty</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC size=2><strong><font face=Arial>Value</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Qty</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Value</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Qty</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Value</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Qty</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Value</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Qty</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Value</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Qty</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Value</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Qty</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Value</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Qty</font></strong></font></div></asp:TableCell>
	 <asp:TableCell><div align=center><font color=#0066CC><strong><font size=2 face=Arial>Value</font></strong></font></div></asp:TableCell>
  </asp:TableRow>
   </asp:table>    

        
        
    <br/>    
        

    </div>
    </form>
</body>
</html>
