﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI
{
    public partial class loc_GuestEditLoan : System.Web.UI.Page
    {
        readonly LocatorApplicationService _service = new LocatorApplicationService();
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HideAllActionButtons();
                int loanId = Convert.ToInt32(Request.QueryString["id"]);
                LoadLoanInformation(loanId);
                HideDivForm();
            }
        }

        private void HideDivForm()
        {
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
        }

        private void LoadLoanInformation(int id)
        {
            var loan = _service.GetLoanApplications(id);
            var maker = 0;
            var prodId = 0;
            var deci = 0;
            var sourceId = 0;
            var cond = 0;
            var appId = 0;
            var oriId = 0;
            var type = 0;
            var deptType = 0;
            var personId = 0;
            var amount = 0;
            var deliDeptId = 0;
            var accNo = "";
            var pers_id = 0;

            foreach (DataRow row in loan.Rows)
            {
                llid.InnerText = row["LLId"].ToString();
                spProduct.InnerText = row["ProductName"].ToString();
                spSource.InnerText = row["SourceName"].ToString();
                spDept.InnerText = row["DepartmentName"].ToString();
                spSubmissionDate.InnerText = Convert.ToDateTime(row["RecieveDate"]).ToString("yyyy-MM-dd");
                spApplicationDate.InnerText = Convert.ToDateTime(row["AppliedDate"]).ToString("yyyy-MM-dd");
                spAccNo.InnerText = row["AccNo"].ToString();
                spLoanAcc.InnerText = row["LoanAccNo"].ToString();

                var accTypeId = Convert.ToInt32(row["AccTypeId"]);
                switch (accTypeId)
                {
                    case 1: spAccType.InnerText = "CEP"; break;
                    case 2: spAccType.InnerText = "Priority"; break;
                    case 3: spAccType.InnerText = "CEP & Priority"; break;
                    default: spAccType.InnerText = ""; break;
                }


                spSubmitBy.InnerText = row["SubmitedBy"].ToString();
                spAmount.InnerText = row["AppliedAmount"].ToString();
                spConApproAmnt.InnerText = row["ApprovedAmount"].ToString();
                spCustomer.InnerText = row["ApplicantName"].ToString();
                spOrganization.InnerText = row["Organization"].ToString();
                spProfession.InnerText = row["Profession"].ToString();
                spLoanStatus.InnerText = row["StateName"].ToString();
                spDecission.InnerText = row["DecisionStatus"].ToString();
                //spLoanAcc.InnerText = row["LoanAccNo"].ToString();
                //spLoanAcc.InnerText = row["LoanAccNo"].ToString();
                if (!string.IsNullOrEmpty(row["Maker"].ToString())) { maker = Convert.ToInt32(row["Maker"]); }
                prodId = Convert.ToInt32(row["ProductId"]);
                if (!string.IsNullOrEmpty(row["DecisionStatusId"].ToString())) { deci = Convert.ToInt32(row["DecisionStatusId"]); }
                sourceId = Convert.ToInt32(row["SourceId"]);
                cond = Convert.ToInt32(row["LoanStatusId"]);
                appId = Convert.ToInt32(row["LLId"]);
                deliDeptId = Convert.ToInt32(row["DeliDeptId"]);
                var user = (LoanLocatorUser)Session["loanUser"];
                type = user.LOAN_DEPT_ID;
                oriId = user.LOAN_DEPT_ID;
                deptType = Convert.ToInt32(row["DeptType"]);
                personId = user.ID_LEARNER;//Convert.ToInt32(row["PersonId"]);
                amount = Convert.ToInt32(row["AppliedAmount"]);
                accNo = row["AccNo"].ToString();
                pers_id = Convert.ToInt32(row["PERS_ID"]);
                if (!string.IsNullOrEmpty(row["Maker"].ToString()))
                { maker = Convert.ToInt32(row["Maker"]); }
            }

            //Other Loan Details Information
            var otherInfo = _service.GetLoanRequestDetails(Convert.ToInt32(llid.InnerText));
            if (otherInfo != null)
            {
                var table = "<table class='az-gridTb' style='margin-top: -1px'><thead><tr><th>Date Time</th><th>Person Name</th><th>Dept Name</th><th>Process status</th><th>Remarks</th><th>Individual TAT</th></tr></thead>";
                int i = 0;
                foreach (DataRow row in otherInfo.Rows)
                {
                    if (i == 0)
                    {
                        var total = row["TAT"].ToString();
                        spTotatlTat.InnerText = string.IsNullOrEmpty(total) ? "0" : total;
                        i++;
                    }


                    table += "<tr>";
                    //spDate.InnerText = Convert.ToDateTime(row["DATE_TIME"]).ToString("yyyy-MM-dd");
                    //spPerson.InnerText = row["NAME_LEARNER"].ToString();
                    //spOtherDept.InnerText = row["LOAN_DEPT_NAME"].ToString();
                    table += "<td>" + Convert.ToDateTime(row["DATE_TIME"]).ToString("yyyy-MM-dd") + "</td>";
                    table += "<td>" + row["NAME_LEARNER"].ToString() + "</td>";
                    table += "<td>" + row["LOAN_DEPT_NAME"].ToString() + "</td>";

                    var lastDd = 0;
                    if (Convert.ToInt32(row["PROCESS_STATUS_ID"]) == 2 && lastDd == 0)
                    {
                        lastDd = 1;
                        //spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                        //<a href=\"show_def_dec.php?id=$row->LOAN_APPL_ID&lst=2\"><font color=#0000cc>$row->LOAN_STATUS_NAME</font></a>
                    }
                    else if (Convert.ToInt32(row["PROCESS_STATUS_ID"]) == 6 && lastDd == 0)
                    {
                        lastDd = 1;
                        //spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                        //<a href=\"show_def_dec.php?id=$row->LOAN_APPL_ID&lst=6\"><font color=#0000cc>$row->LOAN_STATUS_NAME</font></a>
                    }
                    else
                    {
                        // spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                    }
                    // spRemarks.InnerText = row["REMARKS"].ToString();
                    // spTat.InnerText = row["TAT"].ToString();
                    table += "<td>" + row["REMARKS"].ToString() + "</td>";
                    table += "<td>" + row["TAT"].ToString() + "</td>";
                    table += "</tr>";
                }
                tdDetails.InnerHtml = table + "</table>";
            }

            //// CIB Report 
            var cibStatus = _service.GetCibStatus(Convert.ToInt32(llid.InnerText));

            var status = DataTableToList.GetList<Cib>(cibStatus);
            Cib cib = null;
            if (status != null)
            {
                cib = new Cib();
                cib = status.FirstOrDefault();
            }
            var cibCurrentStatus = "";

            if (cib != null && cib.REMARKS == "Report Received")
            {
                cibRec.Checked = true;
                systemErr.Checked = false;
                underTaking.Checked = false;
                cibCurrentStatus = cib.REMARKS;
            }
            else if (cib != null && cib.REMARKS == "System Error")
            {
                cibRec.Checked = false;
                systemErr.Checked = true;
                underTaking.Checked = false;
                cibCurrentStatus = cib.REMARKS;
            }
            else if (cib != null && cib.REMARKS == "Undertaking Returned")
            {
                cibRec.Checked = false;
                systemErr.Checked = false;
                underTaking.Checked = true;
                cibCurrentStatus = cib.REMARKS;
            }

            PopulateDynamicButtons(prodId, deci, sourceId, cond, appId, oriId, type, deptType, personId, maker, amount, accNo, deliDeptId, pers_id);

        }

        private void PopulateDynamicButtons(int prodId, int deci, int sourceId, int cond, int appId, int oriId, int type, int deptType, int personId, int maker, int amount, string accNo, int deliDeptId, int pers_id)
        {
            #region store action param

            var param = new ActionParam();
            param.prodId = prodId;
            param.deci = deci;
            param.sourceId = sourceId;
            param.cond = cond;
            param.appId = appId;
            param.oriId = oriId;
            param.type = type;
            param.deptType = deptType;
            param.personId = personId;
            param.maker = maker;
            param.amount = amount;
            param.loanBtn = "Forward to Loan-Ops";
            param.crdBtn = "Forward to Credit";
            param.AccountNumber = accNo;
            param.DeliDeptId = deliDeptId;
            param.PersId = pers_id;
            param.Op = 0;
            #endregion



            if (IsPostBack) return;
            const string loanBtn = "Forward to Loan-Ops";
            const string crdBtn = "Forward to Credit";
            var loanStateId = 0;
            var jobState = 0;
            var loanUser = (LoanLocatorUser)Session["loanUser"];
            var userLevel = loanUser.ST_USER_LEVEL;

            if ((oriId == 1 || oriId == 2 || oriId == 3) && (cond == 8))
            {
                //transfer_src_option.php
                btnOtherVc.Visible = true;
                //admin_loan.php? //OnServerClick='OnClickUpdate(" + appId + "," + oriId + ")
                btnUpdate.Visible = true;
            }
            if (oriId == deliDeptId)
            {
                var loanDpt = _service.GetLoanDept(type);
                type = Convert.ToInt32(loanDpt.Rows[0]["LOAN_DEPT_TYPE"]);
                // transfer to other sources.
                if (type == 101 && personId == pers_id)
                {
                    status_update.Value = "101";
                    //transfer_src_option.php
                    btnOtherVc.Visible = true;
                    if (cond == 13 || cond == 12)
                    {
                        if (cond == 13)
                        {
                            //remarks.php //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
                            btnLoanOps.Visible = true;

                            btnLoanOpsCr.Visible = true;
                            //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")'
                            //btnLoanOpsCr.InnerText = crdBtn;
                        }
                        else
                        {
                            btnLoanOpsCr.Visible = true;//OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")
                            //btnLoanOpsCr.InnerText = crdBtn;
                        }
                    }
                    else
                    {
                        btnLoanOpsCr.Visible = true; //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")
                        //btnLoanOpsCr.InnerText = crdBtn;

                        btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
                    }
                    btnUpdate.Visible = true; //OnServerClick='OnClickUpdate(" + appId + "," + oriId + ")
                }
                if (type == 201)
                { //credit unit
                    status_update.Value = "201";
                    if (cond == 8 || cond == 12 || cond == 9 || cond == 16 || cond == 17 || cond == 18)
                    {
                        btnOnProcess.Visible = true; // OnServerClick='OnClickOnProcess()
                    }
                    else if (cond == 5)
                    {
                        btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
                        btnUpdateProfile.Visible = true; //OnServerClick='OnClickUpdateProfile(' + appId + ') //edit_optional.php
                    }
                    else if (cond == 7) { }
                    else
                    { // cond=8
                        var status = _service.GetLoanStatusByType(type);

                        //  for (var i = 0; i < status.length; i++) {
                        foreach (DataRow row in status.Rows)
                        {
                            loanStateId = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                            var loanStateName = row["LOAN_STATUS_NAME"].ToString();
                            jobState = Convert.ToInt32(row["JOB_STAT"]);
                            param.JobState = jobState;
                            param.LoanStateId = loanStateId;
                            param.LoanStateName = loanStateName;
                            param.Backfrom = 1;
                            //  }

                            if (loanStateId == 5)
                            {
                                btnOnStatus.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                btnOnStatus.InnerText = loanStateName;

                            }
                            else if (loanStateId == 9 || ((loanStateId == 16 || loanStateId == 18) && prodId == 11 /* 11 = Mortgage Loan  */))
                            {
                                if (loanStateName != "Verification")
                                {
                                    btnOnStatus.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                    btnOnStatus.InnerText = loanStateName;
                                }
                            }
                            else if (loanStateId == 2 || loanStateId == 6 || loanStateId == 15)//DECLINED-2 OR DEFFERED-6 OR CONDITIONALLY APPROVED		 
                            {
                                if (loanStateId == 15)
                                {
                                    //approver.php
                                    btnOnStatus.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                    btnOnStatus.InnerText = loanStateName;
                                }
                                else
                                {
                                    //def_dec.php
                                    btnOnStatus.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                    btnOnStatus.InnerText = loanStateName;
                                }
                            }
                        }
                    }
                    //assetstatus.php
                    btnToSource.Visible = true; //OnServerClick='OnClickToSource(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                    //remarks.php
                    btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + ")

                    //edit_optional.php
                    btnUpdateProfile.Visible = true; //OnServerClick='OnClickUpdateProfile(" + appId + ")
                    //remarks.php
                    btnOnHold.Visible = true; //OnServerClick='OnClickOnHold(" + appId + "," + type + ")

                }
                if (type == 301)
                {
                    status_update.Value = "301";
                    if (oriId == 5)
                    {
                        if (userLevel == 1 && maker != 1)
                        {
                            if (cond == 10 || cond == 17)
                            {
                                btnOnProcess.Visible = true; //OnServerClick='OnClickOnProcess()
                            }

                            else
                            {
                                //echo'<input name=loanid type=hidden value=$id>';
                                btnOnHold.Visible = true; //OnServerClick='OnClickOnHold(" + appId + "," + type + ")
                                btnToSource.Visible = true; //OnServerClick='OnClickToSource(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                btnLoanOpsCr.Visible = true; //OnServerClick='OnClickToCredit(" + appId + "," + type + ")
                                //btnLoanOpsCr.InnerText = crdBtn;
                                if (deci == 5) //deci_status 5 means approved..
                                {
                                    //remarks.php
                                    btnDisbursed.Visible = true; //OnServerClick='OnClickDisbursed(" + appId + "," + type + ")
                                }
                                //update_disburseinfo.php
                                btnUpdateProfile.Visible = true; //OnServerClick='OnClickDisbursed(" + appId + ")
                            }
                        }

                        if (userLevel == 2 && maker == 1 && cond != 7)
                        {
                            //approve.php
                            btnLoanDisbursed.Visible = true; //OnServerClick='OnClickLoanDisbursed(" + appId + "," + type + "," + personId + ",10,4)
                            //remarks.php
                            btnToMaker.Visible = true; //OnServerClick='OnClickToMaker(" + appId + "," + type + ")
                        }
                    }
                }
            }

            if (cond != 7)
            {
                btnCheckList.Visible = true; //loan_check_list.php
                param.Op = 1;
            }
            param.status_update = Convert.ToInt32(status_update.Value);
            Session["param"] = param;
        }

        private void HideAllActionButtons()
        {
            btnOtherVc.Visible = false;
            btnUpdate.Visible = false;
            btnLoanOps.Visible = false;
            btnLoanOpsCr.Visible = false;
            btnOnProcess.Visible = false;
            btnUpdateProfile.Visible = false;
            btnOnStatus.Visible = false;
            btnToSource.Visible = false;
            btnOnHold.Visible = false;
            btnDisbursed.Visible = false;
            btnLoanDisbursed.Visible = false;
            btnToMaker.Visible = false;
            btnCheckList.Visible = false;
        }

        #region Other VC

        protected void OnClickOtherVc(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            otherVcAppId.InnerText = param.appId.ToString();

            divViewApplication.Visible = false;
            toOtherVc.Visible = true;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;

        }

        public void OnClickOtherVcOk(object sender, EventArgs e)
        {
            var user = otherUserNumber.Text;
            var param = (ActionParam)Session["param"];
            var res = _service.SaveOtherVc(param, user);
            Response.Write("<script>alert('" + res + "')</script>");
            CloseOtherVc(new object(), new EventArgs());

        }

        public void CloseOtherVc(object sender, EventArgs e)
        {
            divViewApplication.Visible = true;
            toOtherVc.Visible = false;
        }

        #endregion

        #region Update Loan
        protected void OnClickUpdate(object sender, EventArgs e)
        {
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = true;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            ProductDropDownListItem();
            SourceDropDownListItem();
            ProfessionDropDownListItems();
            var param = (ActionParam)Session["param"];
            LoadApplication(param);
        }
        private void LoadApplication(ActionParam param)
        {
            var loan = _service.GetLoanApplications(param.appId);
            var obj = DataTableToList.GetList<LoanInformation>(loan).FirstOrDefault();

            adProduct.SelectedValue = obj.ProductId.ToString();
            adApplicationDate.Value = obj.AppliedDate.ToString("yyyy - MM - dd");
            adSource.SelectedValue = obj.SourceId.ToString();
            switch (obj.AccTypeId)
            {
                case 1:
                    chkAdCpf.Checked = true;
                    chkAdPriority.Checked = false;
                    break;
                case 2:
                    chkAdPriority.Checked = true;
                    chkAdCpf.Checked = false;
                    break;
                case 3:
                    chkAdCpf.Checked = true;
                    chkAdPriority.Checked = true;
                    break;
                default: spAccType.InnerText = ""; break;
            }

            //if (obj.AccType == "CPF")
            //{
            //    chkAdCpf.Checked = true;
            //    chkAdPriority.Checked = false;
            //}
            //else
            //{
            //    chkAdPriority.Checked = true;
            //    chkAdCpf.Checked = false;
            //}
            var acc = obj.AccNo.Split('-');
            adAccPre.Value = acc[0];
            adAccMid.Value = acc[1];
            adAccLast.Value = acc[2];
            txtAdCust.Value = obj.ApplicantName;
            txtAdAmount.Value = obj.AppliedAmount.ToString();
            adProfession.SelectedValue = obj.ProfessionId.ToString();
            txtAdOrganization.Value = obj.Organization;
            adDeclined.Value = obj.PreDeclined.ToString();

        }
        private void ProfessionDropDownListItems()
        {
            List<Profession> professionListObj = _loanApplicationManagerObj.GetAllProfession();
            Profession professionObj = new Profession();
            professionObj.Id = 0;
            professionObj.Name = "";
            professionListObj.Insert(0, professionObj);
            adProfession.DataSource = professionListObj;
            adProfession.DataTextField = "Name";
            adProfession.DataValueField = "Id";
            adProfession.DataBind();
        }
        private void SourceDropDownListItem()
        {
            List<Source> sourceLisObj = _loanApplicationManagerObj.GetAllSource();
            Source sourceObj = new Source();
            sourceObj.SourceId = 0;
            sourceObj.SourceName = "";
            sourceLisObj.Insert(0, sourceObj);
            adSource.DataTextField = "SourceName";
            adSource.DataValueField = "SourceId";
            adSource.DataSource = sourceLisObj;
            adSource.DataBind();
        }
        private void ProductDropDownListItem()
        {
            List<Product> productListObj = _loanApplicationManagerObj.GetAllProduct();
            var productObj = new Product { ProductId = 0, ProductName = "" };
            productListObj.Insert(0, productObj);
            adProduct.DataTextField = "ProductName";
            adProduct.DataValueField = "ProductId";
            adProduct.DataSource = productListObj;
            adProduct.DataBind();
        }
        public void CloseUpdate(object sender, EventArgs e)
        {
            divViewApplication.Visible = true;
            divUpdate.Visible = false;

        }
        public void UpdateLoanInfo(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            var loan = GetLoanObj();
            var selectedLoan = _service.GetLoanApplications(param.appId);
            var obj = DataTableToList.GetList<LoanInformation>(selectedLoan).FirstOrDefault();
            var res = _service.UpdateLoanInfo(loan, obj);
            if (res == "1")
            {
                Response.Write("<script>alert('Loan Updated Successfully')</script>");
                LoadLoanInformation(param.appId);
                CloseUpdate(new object(), new EventArgs());
            }
        }
        private Loan GetLoanObj()
        {
            try
            {
                var obj = new Loan();
                obj.SubmissionDate = DateTime.Now;
                obj.ProductId = Convert.ToInt32(adProduct.SelectedValue);
                obj.PreAccNo = adAccPre.Value;
                obj.MidAccNo = adAccMid.Value;
                obj.PostAccNo = adAccLast.Value;
                obj.CustomerName = txtAdCust.Value;
                obj.AppliedAmount = Convert.ToInt32(txtAdAmount.Value);
                obj.SubmitedBy = Convert.ToInt32(Session["Id"].ToString());
                obj.EntryBranch = Convert.ToInt32(adSource.SelectedValue);
                obj.PreDeclined = adDeclined.Value;
                obj.ApplicationDate = Convert.ToDateTime(adApplicationDate.Value); ;
                obj.Cep = chkAdCpf.Checked;
                obj.Prio = chkAdPriority.Checked;
                if (obj.Cep && obj.Prio) { obj.AccType = 3; }
                else if (obj.Prio) { obj.AccType = 2; }
                else if (obj.Cep) { obj.AccType = 1; }
                else { obj.AccType = 4; }
                obj.ApplicantProfession = Convert.ToInt32(adProfession.SelectedValue);
                obj.Organization = txtAdOrganization.Value;
                return obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Loan Ops

        protected void OnClickLoanOps(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = true;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            btnLoanOpsRe.Value = param.loanBtn;


        }

        public void OnClickLoanOpsPost(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            var remarks = lopsRemarks.Value;
            var res = _service.UpdateLoanOps(param, remarks, false, "ForwLoanDocs");
            if (res == "1")
            {
                Response.Write("<script>alert('Loan Updated Successfully')</script>");
                OnClickLoanOpsClose(new object(), new EventArgs());
            }
            else if (res == "0")
            {
                Response.Write("<script>alert('Loan not update, Try again!')</script>");
            }
            else
            {
                Response.Write("<script>alert('" + res + "')</script>");
            }
        }

        public void OnClickLoanOpsClose(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = true;
            divLoanOps.Visible = false;
            LoadLoanInformation(param.appId);

        }

        #endregion


        #region Loan Credit

        protected void OnClickToCredit(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = true;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            btnToCrSave.Value = param.crdBtn;
        }

        public void OnClickToCreditPost(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            var remarks = toCrRemarks.Value;
            var res = _service.UpdateLoanOps(param, remarks, false, "ForwCrea");
            if (res == "1")
            {
                Response.Write("<script>alert('Loan Updated Successfully')</script>");
                OnClickToCreditClose(new object(), new EventArgs());
            }
            else if (res == "0")
            {
                Response.Write("<script>alert('Loan not update, Try again!')</script>");
            }
            else
            {
                Response.Write("<script>alert('" + res + "')</script>");
            }
        }

        public void OnClickToCreditClose(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = true;
            divToCredit.Visible = false;
            LoadLoanInformation(param.appId);

        }

        #endregion


        protected void OnClickOnProcess(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = true;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
        }

        #region Update Personal Profile

        protected void OnClickUpdateProfile(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = true;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            var loan = _service.GetLoanApplications(param.appId);
            var obj = DataTableToList.GetList<LoanInformation>(loan).FirstOrDefault();
            cust.InnerText = " of" + obj.ApplicantName + "( Loan ID:" + obj.LLId + " )";
            owner.Value = obj.Owner;
            mother.Value = obj.MotherName;
            tenor.Value = obj.Tenor.ToString();
            ud.SelectedValue = obj.UserDefine;
            ud2.Value = obj.UserDefine2;
            ud3.Value = obj.UserDefine3;
            ud4.Value = obj.UserDefine4;
        }

        public void UpdateProfile(object o, EventArgs e)
        {
            var res = "";
            try
            {
                var param = (ActionParam)Session["param"];
                var own = owner.Value;
                var motherName = mother.Value;
                var teno = tenor.Value;
                var userdef = ud.SelectedValue;
                var userDef2 = ud2.Value;
                var userDef3 = ud3.Value;
                var userDef4 = ud4.Value;
                res = _service.UpdateProfile(param, own, motherName, teno, userdef, userDef2, userDef3, userDef4);
                CloseProfile(new object(), new EventArgs());
            }
            catch (Exception exception)
            {
                res = "Optional  Status not Updated";
            }
            Response.Write("<script>alert('" + res + "')</script>");

        }

        public void CloseProfile(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = true;
            divUpdateProfile.Visible = false;
            LoadLoanInformation(param.appId);
        }

        #endregion

        #region On Status

        protected void OnClickOnStatus(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = true;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            if (param.LoanStateId == 15)
            {
                PopulateView(param);
            }
            else
            {
                // def_dec.php?
                PopulateView(param);
            }
        }

        public void SaveOnStatus(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            //$id=$HTTP_POST_VARS['id'];
            //$lst=$HTTP_POST_VARS['lst'];
            //$type=$HTTP_POST_VARS['type'];
            //$pers=$HTTP_POST_VARS['pers'];
            //$job=$HTTP_POST_VARS['job'];
            //$ldep=$_SESSION['ori_id'];

            ////echo $id.$type.$pers."<BR>".$job."<BR>".$lst;



            decimal approvedAmount = 0;
            if (!string.IsNullOrEmpty(ap_amou.Value))
            {
                approvedAmount = Convert.ToDecimal(ap_amou.Value);
            }
            decimal condapprovamou = 0;
            if (!string.IsNullOrEmpty(cond_approv_amou.Value))
            {
                condapprovamou = Convert.ToDecimal(cond_approv_amou.Value);
            }
            decimal disbursedAmo = 0;
            if (!string.IsNullOrEmpty(disb_amou.Value))
            {
                disbursedAmo = Convert.ToDecimal(disb_amou.Value);
            }
            var objTenor = tenor.Value;
            var objRemarks = remarks.Value;
            var year = y1.DataValueField;
            var month = m1.DataValueField;
            var day = d1.DataValueField;
            var disDate = year + '-' + month + '-' + day;
            var submit = hdnSubmit.Value;
            var preAcc = pre_acc.Value;
            var midAcc = mid_acc.Value;
            var postAcc = post_acc.Value;
            var loanac = preAcc + "-" + midAcc + "-" + postAcc;
            var loanAccExist = loan_ac.Value;
            var defdec = def_dec.DataValueField;
            var apprlvl = appr_lvl.DataValueField;
            var dt = DateTime.Now;
            //$dt1=date("Y:m:d");
            var res = _service.SaveOnStatus(param, approvedAmount, condapprovamou, objTenor, objRemarks, year, month,
                day, disDate, submit, preAcc, midAcc, postAcc, loanac, loanAccExist, defdec, apprlvl, disbursedAmo);
            var msg = res.Split('\\');
            if (msg.Length > 0)
            {
                Response.Write("<script>alert(" + msg[0] + ")</script>");

            }
            else
            {
                Response.Write("<script>alert(" + res + ")</script>");
            }

            CloseOnStatus(new object(), new EventArgs());


        }

        public void CloseOnStatus(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = true;
            divOnStatus.Visible = false;
            LoadLoanInformation(param.appId);
        }

        private void PopulateView(ActionParam param)
        {
            var type = param.type;
            var name = "";
            var name1 = param.LoanStateName;
            //var stateName = "";
            //var html = "";
            //html += "<table width='90%' border='0' align='center' cellpadding='0' cellspacing='1' bordercolor='#00CCCC' bgcolor='#e7f7c6'>";
            //html += "<tr align='right' valign='middle' bgcolor='#BDD3A5'>";
            //html += "<td height='24' colspan='2'>&nbsp;</td> </tr> <tr align='right' valign='middle' bgcolor='#BDD3A5'>";
            //html += "<td width='33%' height='25' bgcolor='#D5E2C5'> <div align=right><font color='#000000' size='2' face='Verdana, Arial, Helvetica, sans-serif'>Loan Status:</font></div></td>";
            //html += "<td height='25' align=left valign='middle' bgcolor='#E7F7C6'> <font size='2' face='Verdana, Arial, Helvetica, sans-serif'> ";
            //html += "<font color=#000000><b>&nbsp;&nbsp;" + name1 + "</b></font></font></td></tr>";

            dvStateName.InnerText = name1;

            r210.Visible = false;
            r220.Visible = false;
            r211.Visible = false;
            r212.Visible = false;

            if (param.JobState == 21)
            {
                r210.Visible = true;
                r211.Visible = true;
                r212.Visible = true;
                //html += "<tr bgcolor=#e7f7c6 id='r210' runat='server'>";

                //html += "<td ><div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif><font color=#FF0000></font>Approved amount: </font></font></div></td>";
                //html += "<td colspan=3><input type=text name=ap_amou id='ap_amou' runat='server' style='border:1px solid #003300; background-color:#F5F8FC' value='' ></td></tr><tr bgcolor=#e7f7c6> ";
                //html += "<td ><div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif><font color=#FF0000></font>Approved Level: </font></font></div></td>";

                //html += "<td colspan=3><select name='appr_lvl' id='appr_lvl' runat='server'>";
                //html += "<option value=''></option>";
                //html += "<option value=1>1</option>";
                //html += "<option value=2>2</option>";
                //html += "<option value=3>3</option>";
                //html += "</select></td>";

                //html += "</tr><tr id='r212' runat='server'>";

                //html += "<td height='30' bgcolor='#D5E2C5'><div align=right><font face='Verdana, Arial, Helvetica, sans-serif'><font color=#FF0000></font>Term:</font></div></td>";
                //html += "<td height='30' bgcolor='#E7F7C6'><font face='Verdana, Arial, Helvetica, sans-serif'>";
                //html += "<input name='tenor'  id='tenor' runat='server' type='text' id='tenor' style='border:1px solid #000033; background-color:#F5F8FC; ' value='' size='20'> </font></td></tr>";

            }
            if (param.JobState == 22)
            {
                r220.Visible = true;

                //html += "<tr id='r220' runat='server'><td bgcolor=#D5E2C5 ><div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif><font color=#FF0000></font>Amount: </font></font></div></td>";
                //html += "<td colspan=3><input type=text name=cond_approv_amou  id='cond_approv_amou' runat='server' style='border:1px solid #003300; background-color:#F5F8FC' value='' ></td></tr>";

            }

            r40.Visible = false;
            r41.Visible = false;
            r42.Visible = false;
            tblAcc.Visible = false;
            loan_ac.Visible = false;
            if (param.LoanStateId == 4)
            {

                r40.Visible = true;
                r41.Visible = true;
                r42.Visible = true;

                //html += "<tr id='r40'  runat='server' bgcolor=#e7f7c6><td bgcolor='#D5E2C5'><div align='right'>Loan Disbursement date: </div></td><td colspan='2' ><font color=#000000 face=Courier New, Courier, mono>";
                //html += DayDropDwon();

                //html += "</font><font size=2 color=#000000 face=Courier new, courier, mono> ";
                //html += MonthDropDwon();
                //html += YearDropDwon();

                //html += "</font><font Size=2 color=#000000 face=Courier New, Courier, mono>&nbsp;</font></td></tr>";

                //html += "<tr id='r41'  runat='server' bgcolor=#e7f7c6>";
                //html += "<td height=29 bgcolor='#D5E2C5'><div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif><font color=#FF0000></font>Disburse amount: </font></font></div></td>";
                //html += "<td colspan=2><input type=text name=disb_amou  id='disb_amou' runat='server' style='border:1px solid #003300; background-color:#F5F8FC' value='' ></td> </tr>";
                //html += "<tr bgcolor=#e7f7c6   id='r42'  runat='server'> <td height=29 bgcolor='#D5E2C5'> <div align=right><font size=2 color=#000000 face=Verdana, Arial, Helvetica, sans-serif>Loan A\\C No:</font></div></td>";
                //html += "<td colspan=2>";
                if (string.IsNullOrEmpty(param.AccountNumber))
                {
                    tblAcc.Visible = true;

                    //html += "<table width='50%' border='0' id='tblAcc' runat='server'><tr>";
                    //html += "<td width='5%'> <input name='pre_acc'  id='pre_acc' runat='server' type='text'   style='border:1px solid #000033; background-color:#F5F8FC;' size='4' maxlength='2' onKeyup='autotab(this, document.form1.mid_acc)'></td>";
                    //html += "<td width='2%' align='center' valign='middle'><strong><font size='4'>-</font></strong></td>";
                    //html += "<td width='18%'> <input name='mid_acc'  id='mid_acc' runat='server' type='text'   style='border:1px solid #000033; background-color:#F5F8FC; ' size='15' maxlength='7' onKeyup='autotab(this, document.form1.post_acc)'></td>";
                    //html += "<td width='2%' align='center' valign='middle'><strong><font size='4'>-</font></strong></td>";
                    //html += "<td width='25%'> <input name='post_acc'  id='post_acc' runat='server' type='text'  style='border:1px solid #000033; background-color:#F5F8FC; ' size='4' maxlength='2'></td>  </tr></table>";


                }
                else
                {
                    loan_ac.Visible = true;
                    loan_ac.Value = param.AccountNumber;

                    //html += "<input name=loan_ac  id='loan_ac' runat='server' type=text value='" + param.AccountNumber + "' style='border:1px solid #000033; background-color:#F5F8FC;' disabled>";
                }
                //html += "</td></tr>";
            }
            r260.Visible = false;
            if (param.LoanStateId == 2 || param.LoanStateId == 6)
            {
                r260.Visible = true;
                if (param.LoanStateId == 2)
                {
                    name = "Declined Reason:";
                    type = 1;
                }
                else
                {
                    name = "Deferred Reason:";
                    type = 2;
                }
                r260Text.InnerText = name;

                //html += "<tr bgcolor=#e7f7c6 id='r260' runat='server'>";
                //html +="<td width=38%><div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif>" +name + "</font></font></div></td>";
                //html += "<td colspan=3>";
                GetDeferredDeclineReason(param.prodId, param.type);
                // html += "</td></tr>";
            }

            //html += "<tr bgcolor='#BDD3A5'>";
            //html += "<td align='right' valign='middle' bgcolor='#D5E2C5'> <div align='right'><font color='#000000' size='2' face='Verdana, Arial, Helvetica, sans-serif'>Remarks (If any) :</font></div></td>";
            //html += "<td bgcolor='#E7F7C6'> <font size='2' face='Verdana, Arial, Helvetica, sans-serif'>";
            //html += "<textarea name='remarks'  id='remarks' runat='server' cols='50' rows='3' style='border:1px solid #000033; background-color:#F5F8FC'></textarea></font></td></tr>";

            //html += "<tr bgcolor='#BDD3A5'>";
            //html += "<td height='40'><font size='2' face='Verdana, Arial, Helvetica, sans-serif'>&nbsp;</font></td>";
            //html += "<td height='40' > <font size='2' face='Verdana, Arial, Helvetica, sans-serif'>";
            if (param.JobState == 10)
            {
                hdnSubmit.Value = name1;
                // html += "<input name='Submit' type='hidden'  value='" + name1 + "'>";
                // html += "<input type='button'  id='submit1' runat='server' name='Submit' value='" + name1 + "'  onServerClick='SaveOnStatus' style='border:1px solid #83b67a; background-color:#D8EED5'>";
            }

            else if (param.JobState == 22)
            {
                hdnSubmit.Value = name1;
                //html += "<input name='Submit' type='hidden'  value='" + name1 + "'>";
                //html += "<input type='button' name='Submit'   id='submit4' runat='server' value='" + name1 + "'  onServerClick='SaveOnStatus' style='border:1px solid #83b67a; background-color:#D8EED5'>";
            }
            else
            {

                //html += "<input name='Submit'   id='submit3' runat='server' type='submit' onServerClick='SaveOnStatus' style='border:1px solid #83b67a; background-color:#D8EED5'  value='" + name1 + "'>";
            }
            //html += "&nbsp;<input name='Submit2'   id='submit2' runat='server' type='button' style='border:1px solid #83b67a; background-color:#D8EED5' OnClick='__doPostBack('ctl00$ContentPlaceHolder$submit2','')' value='Close'>";
            //html += "&nbsp;<input type='button' id='aa' runat='server' OnClick='__doPostBack('ctl00$ContentPlaceHolder$aa','')' value ='Close2'/>";
            //html += "</font></td> </tr></table>";
            //<input type='hidden' name='id' value='<? echo $id; ?>'>
            //<input type='hidden' name='type' value='<? echo $type; ?>'>
            //<input type='hidden' name='pers' value='<? echo $pers; ?>'>
            //<input type='hidden' name='job' value='<? echo $job; ?>'>
            //<input type='hidden' name='lst' value='<? echo $lst; ?>'> 
            //<input type='hidden' name='prev_status' value='<? echo $row->LOAN_STATUS_ID; ?>'>
            //onStatusHtm.InnerHtml = html;
            submit4.Value = name1;
        }

        private void GetDeferredDeclineReason(int prodId, int type)
        {

            var data = _service.GetDeferredDeclineReason(prodId, type);
            def_dec.DataTextField = "REAS_NAME";
            def_dec.DataValueField = "REAS_ID";
            def_dec.DataSource = data;
            def_dec.DataBind();

        }

        private string YearDropDwon()
        {
            var htm = " <select name=y1 id='y1'  runat='server'>";
            var y = DateTime.Now.Year;
            for (var i = 2004; i < 2021; i++)
            {
                htm += y == i
                    ? " <option value=" + i + " selected> " + i + " </option>"
                    : " <option value=" + i + "> " + i + " </option>";
            }
            return htm + "</select>";
        }

        private string MonthDropDwon()
        {
            var htm =
                "<select name=m1 id='m1'  runat='server' style='border:1px solid #4179C0; background-color:#F5F8FC'>";
            var m = DateTime.Now.Month;
            var j = 0;
            var month1 = new string[]
            {
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
                "November", "December"
            };
            for (var i = 0; i < 12; i++)
            {
                j = i + 1;
                htm += m == j
                    ? " <option value=" + j + " selected>" + month1[i] + "</option>"
                    : " <option value=" + j + ">" + month1[i] + "</option>";
            }
            htm += "</select>";
            return htm;
        }

        private string DayDropDwon()
        {
            var conent = "<select name=d1 id=d1  runat='server'>";
            var d = DateTime.Now.Day;
            for (var i = 1; i < 32; i++)
            {
                if (d == i)
                    conent += " <option value=" + i + " selected> " + i + " </option>";
                else
                    conent += " <option value=i> " + i + " </option>";
            }
            return conent + "</select>";
        }

        #endregion


        #region To Source

        protected void OnClickToSource(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = true;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;


            if (param.LoanStateId == 11)
                status.Text = "Forward to Loan admin( LLID:" + param.appId + ")";
            if (param.LoanStateId == 12)
                status.Text = "Send Back to Credit( LLID:" + param.appId + ")";
            if (param.LoanStateId == 3)
                status.Text = "Send Back to Source( LLID:" + param.appId + ")";

        }

        protected void SubmitToOnSource(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            var remarks = toSourceRemarksArea.Value;

            if (string.IsNullOrEmpty(remarks))
            {
                Response.Write("<script>alert('Please write remarks')</script>");
                return;
            }
            var res = _service.SubmitToOnSource(param, remarks);
            Response.Write("<script>alert('" + res + "')</script>");
            CloseToOnSource(new object(), new EventArgs());
        }

        protected void CloseToOnSource(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = true;
            divToSource.Visible = false;
            LoadLoanInformation(param.appId);
        }

        #endregion


        #region On Hold

        protected void OnClickOnHold(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = true;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            sel_onhold.DataValueField = "ONHOLD_NAME";
            sel_onhold.DataTextField = "ONHOLD_NAME";
            sel_onhold.DataSource = _service.GetOnHolds();
            sel_onhold.DataBind();
        }

        protected void OnHoldSubmit(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            var remarks = onHoldRemarks.Value;
            var res = _service.UpdateLoanOps(param, remarks, false, "On_Hold");
            if (res == "1")
            {
                Response.Write("<script>alert('Loan Updated Successfully')</script>");
                OnHodlClose(new object(), new EventArgs());
            }
            else if (res == "0")
            {
                Response.Write("<script>alert('Loan not update, Try again!')</script>");
            }
            else
            {
                Response.Write("<script>alert('" + res + "')</script>");
            }
        }

        protected void OnHodlClose(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = true;
            divOnHold.Visible = false;
            LoadLoanInformation(param.appId);
        }

        #endregion



        protected void OnClickDisbursed(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = true;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
        }
        protected void OnClickLoanDisbursed(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = true;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
        }
        protected void OnClickToMaker(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = true;
        }

        protected void OnClickToCheckList(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = true;
        }

        //protected void cibRec_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cibRec.Checked)
        //    {
        //        cibRec.Checked = true;
        //        systemErr.Checked = false;
        //        underTaking.Checked = false;
        //    }
        //    else
        //    {
        //        cibRec.Checked = false;
        //        systemErr.Checked = true;
        //        underTaking.Checked = false;
        //    }
        //}
        //protected void systemErr_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (systemErr.Checked)
        //    {
        //        cibRec.Checked = false;
        //        systemErr.Checked = true;
        //        underTaking.Checked = false;
        //    }
        //    else
        //    {
        //        cibRec.Checked = false;
        //        systemErr.Checked = false;
        //        underTaking.Checked = true;
        //    }
        //}
        //protected void underTaking_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (underTaking.Checked)
        //    {
        //        cibRec.Checked = false;
        //        systemErr.Checked = false;
        //        underTaking.Checked = true;
        //    }
        //    else
        //    {
        //        cibRec.Checked = true;
        //        systemErr.Checked = false;
        //        underTaking.Checked = false;
        //    }
        //}

    }
}