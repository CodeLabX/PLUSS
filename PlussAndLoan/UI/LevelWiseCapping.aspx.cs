﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI
{
    public partial class LevelWiseCapping : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            divDate.Text = Session["date1"].ToString() + " TO " + Session["date2"].ToString();
            DataTable dt = (DataTable)Session["Capp"];
            DataTable level = (DataTable)Session["level"];
            DataTable result = (DataTable)Session["total"];
            int y2 = 0;
            decimal tot = 0;
            decimal tpc = 0;
            decimal total2 = 0;
            decimal total = 0;
            decimal amou = 0;
            decimal capv = 0;
            decimal perc = 0;
            decimal space = 0;
            var prodName = "";
            var lev = 0;
            if (level != null || dt != null || result != null)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                if (dt != null)
                {


                    if (y2 == 2004)
                        tot = (Convert.ToDecimal(result.Rows[0]["total"]) +
                               Convert.ToDecimal(level.Rows[0]["LCOD_L2"]) +
                               Convert.ToDecimal(level.Rows[0]["LCOD_L3"]));
                    else
                    {
                        tot = Convert.ToDecimal(result.Rows[0]["total"]);
                    }

                    total = tot;

                    //for (int i = 0; i < dt.Rows.Count; i++)
                   // {
                       
                            amou = Convert.ToDecimal(dt.Rows[i]["tot"]);
                            prodName = level.Rows[i]["PROD_NAME"].ToString();
                            lev = Convert.ToInt32(dt.Rows[i]["APPRV_LEVEL"].ToString()) ;
                            capv = 0;
                            if (y2 == 2004)
                            {
                                if (Convert.ToInt32(dt.Rows[i]["APPRV_LEVEL"]) == 1)
                                {
                                    amou = Convert.ToDecimal(level.Rows[i]["LCOD_L1"]) + amou;
                                    capv = Convert.ToDecimal(level.Rows[i]["LEVE_CAPP_L1"]);
                                }
                                else if (Convert.ToInt32(dt.Rows[i]["APPRV_LEVEL"]) == 2)
                                {
                                    amou = amou + Convert.ToDecimal(level.Rows[i]["LCOD_L2"]);
                                    capv = Convert.ToDecimal(level.Rows[i]["LEVE_CAPP_L2"]);
                                }
                                else if (Convert.ToInt32(dt.Rows[i]["APPRV_LEVEL"]) == 3)
                                {
                                    amou = amou + Convert.ToDecimal(level.Rows[i]["LCOD_L3"]);
                                    capv = Convert.ToDecimal(level.Rows[i]["LEVE_CAPP_L3"]);
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(dt.Rows[i]["APPRV_LEVEL"]) == 1)
                                {
                                    if (!String.IsNullOrEmpty(level.Rows[i]["LEVE_CAPP_L1"].ToString()))
                                        capv = Convert.ToDecimal(level.Rows[i]["LEVE_CAPP_L1"]);
                                }
                                else if (Convert.ToInt32(dt.Rows[i]["APPRV_LEVEL"]) == 2)
                                {
                                    capv = Convert.ToDecimal(level.Rows[i]["LEVE_CAPP_L2"]);
                                }
                                else if (Convert.ToInt32(dt.Rows[i]["APPRV_LEVEL"]) == 3)
                                {
                                    capv = Convert.ToDecimal(level.Rows[i]["LEVE_CAPP_L3"]);
                                }
                            }
                            perc = ((amou / tot) * 100);
                            tpc = tpc + ((amou / tot) * 100);
                            total2 = total2 + amou;
                            space = Math.Round(perc - capv, 2);
                            perc = Math.Round(perc, 3);


                           
                     //   }
                    TableRow row = new TableRow();
                    row.Cells.Add(new TableCell { Text = lev.ToString() });
                    row.Cells.Add(new TableCell { Text = amou.ToString() });
                    row.Cells.Add(new TableCell { Text = perc.ToString() + "%" });
                    row.Cells.Add(new TableCell { Text = capv.ToString() + "%" });
                    row.Cells.Add(new TableCell { Text = space.ToString() + "%" });
                    LevelReport.Rows.Add(row);
                        }
               
                }
                TableRow row1 = new TableRow();
                row1.Cells.Add(new TableCell { Text = "Total" });
                row1.Cells.Add(new TableCell { Text = tot.ToString() });
                row1.Cells.Add(new TableCell { Text = Math.Round(tpc, 2).ToString() + "%" });
                row1.Cells.Add(new TableCell { Text = "&nbsp;" });
                row1.Cells.Add(new TableCell { Text = "&nbsp;" });
                LevelReport.Rows.Add(row1);
                ProdName.Text = prodName;
                
            }

        }

        protected void Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
            Session.Clear();
        }

        protected void Close_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
            Session.Clear();
        }
    }
}

