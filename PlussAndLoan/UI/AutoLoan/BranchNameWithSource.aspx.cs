﻿using System;
using System.Collections.Generic;
using System.IO;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class BranchNameWithSource : System.Web.UI.Page
    {
        [AjaxNamespace("BranchNameWithSource")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(BranchNameWithSource));
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranchWithSource> GetAllSourceNameWithBranch(int pageNo, int pageSize, string searchtext)
        {
            pageNo = pageNo * pageSize;

            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);


            List<AutoBranchWithSource> objBranchNameWithSourcelist =
                autoBranchAndSourceService.GetAllSourceNameWithBranch(pageNo, pageSize, searchtext);
            return objBranchNameWithSourcelist;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranchWithSource> GetAllbranchName()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);


            List<AutoBranchWithSource> objBranchNameWithSourcelist =
                autoBranchAndSourceService.GetAllbranchName();
            return objBranchNameWithSourcelist;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAutoBranchAndSource(AutoBranchWithSource sourceAndbranch)
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.SaveAutoBranchAndSource(sourceAndbranch);
            return res;
        }

        protected void btnUploadSave_Click(object sender, EventArgs e)
        {
            var res = uploadFile();
            if (res == "Upload success" || res == "Success")
            {
                string msg = "Operation Successfull.";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");
            }
            else
            {
                string msg = "Operation partially completed. Found Error with follwoing rows (" + res + ")";

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");

            }

        }

        public string uploadFile()
        {
            string name = "";
            string returnValue = "";
            if (!fileDocumentUpload.FileContent.CanRead)
            {

                returnValue = "Please select Valid file";
            }
            else
            {
                try
                {
                    string serverPath = Request.PhysicalApplicationPath;
                    var directory = Server.MapPath(@"../Uploads/Auto");

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    DateTime date = DateTime.Now;
                    name = date.ToString("yyyyMMddHHmmss") + "_" +
                           fileDocumentUpload.FileName;
                    fileDocumentUpload.SaveAs(Path.Combine(directory, name));


                    returnValue = "Upload success";
                    returnValue = DumpExcelFile(Convert.ToString(Path.Combine(directory, name)));

                }
                catch (Exception ex)
                {
                    returnValue = "Upload Failed";
                }

            }
            return returnValue;
        }


        public string DumpExcelFile(string excelFilePath)
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var branchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = branchAndSourceService.DumpExcelFileForBranchAndService(excelFilePath);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string InactiveSourceById(int sourceId)
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.InactiveSourceById(sourceId);
            return res;
        }
    }
}
