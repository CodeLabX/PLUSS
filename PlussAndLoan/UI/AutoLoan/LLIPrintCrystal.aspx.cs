﻿using System;
using System.Collections.Generic;
using AutoLoanService.Entity.CheckListPrint;
using CrystalDecisions.CrystalReports.Engine;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class UI_AutoLoan_LLIPrintCrystal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                LLIPrintEntity b2 = new LLIPrintEntity();
                List<LLIPrintEntity> b2List = new List<LLIPrintEntity>();

                b2 = (LLIPrintEntity)Session["LLI"];
                b2List.Add(b2);

                ReportDocument rd = new ReportDocument();
                string path = "";

                if (b2List[0].lblProductId == "3") {
                    path = Server.MapPath("PrintCheckList/LLIPrintSaadiq.rpt");
                }
                else
                {
                    path = Server.MapPath("PrintCheckList/LLIPrint.rpt");
                }
                rd.Load(path);
                rd.SetDataSource(b2List);
                crViewer.ReportSource = rd;
                //rd.PrintToPrinter(1, true, 0, 0);
            }
            catch (Exception ex)
            {

            }

        }
    }
}
