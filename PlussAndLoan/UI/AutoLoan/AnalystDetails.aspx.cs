﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class AnalystDetails : System.Web.UI.Page
    {
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;


        protected void Page_PreInit(object sender, EventArgs e)
        {
            //string callingFrom = Request.QueryString["CallFrom"];
            if (Session["MasterPageUrlForAnalystDetails"] != null)
            {
                this.MasterPageFile = Session["MasterPageUrlForAnalystDetails"].ToString();
            }

        }


        [AjaxNamespace("AnalystDetails")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(AnalystDetails));

            var llid = Request.QueryString["llid"];
            if (llid != "" && IsNumeric(llid))
            {
                txtLLID.Value = llid;
                Session["LLId"] = llid;
                spnWait.InnerHtml = "Please Wait some moment .........";
            }
        }

        public static bool IsNumeric(string text)
        {
            return string.IsNullOrEmpty(text) ? false :
                    Regex.IsMatch(text, @"^\s*\-?\d+(\.\d+)?\s*$");
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> getVehicleStatus(int statusID)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVehicleStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVendor> GetVendor()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetVendor();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranchWithSource> GetAllAutoBranch()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetAllbranchName();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBank> GetBank(int status)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBank(status);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoFacility> getFacility()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetFacility();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussSector> GetSector()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetAllPlussSector();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<auto_CibInfo> getCibInfo()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getCibInfo();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoSegmentSettings> GetEmployeSegment()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetEmployeSegment();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<Auto_InsuranceBanca> getGeneralInsurance()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getGeneralInsurance();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoOwnDamage> GetOwnDamage()
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);


            List<AutoOwnDamage> objAutoOwnDamageList = autoLoanDataService.GetOwnDamage();
            return objAutoOwnDamageList;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoOthercharge GetOtherDamage()
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);


            var objAutoOthercharge = autoLoanDataService.GetOtherDamage();
            return objAutoOthercharge;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<t_pluss_user> gett_pluss_userAll()
        {
            var userId = Convert.ToInt32(Session["Id"].ToString());
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.gett_pluss_userAll(userId);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<DaviationSettingsEntity> getDeviationLevel()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getDeviationLevel();
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<t_Pluss_Remark> getRemarkStrangthConditionWeaknessAll()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getRemarkStrangthConditionWeaknessAll();
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranchWithSource> GetAllAutoSourceByBranchId(int branchId)
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetAllAutoSourceByBranchId(branchId);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> getModelbyManufacturerID(int manufacturerID)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            List<AutoVehicle> objList_AutoModel = tanorAndLTVService.GetModelList(manufacturerID);
            return objList_AutoModel;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoBrandTenorLTV getValuePackAllowed(int manufacturerID, int vehicleId, int vehicleStatus)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            AutoBrandTenorLTV objAutoBrandTenorLTV = tanorAndLTVService.getvaluepackallowed(manufacturerID, vehicleId, vehicleStatus);
            return objAutoBrandTenorLTV;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoLoanVehicle CheckMouByVendoreId(int vendoreId)
        {

            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var userID = Convert.ToInt32(Session["Id"].ToString());

            AutoLoanVehicle objAutoLoanVehicle = autoLoanAssessmentService.CheckMouByVendoreId(vendoreId);

            return objAutoLoanVehicle;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranch> GetBranch(int bankID)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBranch(bankID);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public Object GetDataByLLID()
        {
            //ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            //var autoLoanDataService = new LoanApplicationService(repository);
            //var objAutoLoanApplicationAll = autoLoanDataService.getLoanLocetorInfoByLLID(llId);
            //return objAutoLoanApplicationAll;

            var objAutoLoanApplicationAll = new Object();
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objAutoLoanApplicationAll;
            }
            else
            {
                var llId = Convert.ToInt32(Session["LLId"]);
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                objAutoLoanApplicationAll = autoLoanAssessmentService.getLoanLocetorInfoByLLID(llId, statePermission);
            }

            //Label lblWait = this.FindControl("spnWait") as Label;
            //lblWait.Text = "";
            //spnWait.InnerText = "";
            return objAutoLoanApplicationAll;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<DaviationSettingsEntity> getDeviationByLevel(int deviationFor)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getDeviationByLevel(deviationFor);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPayrollRate> GetEmployerForSalaried()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetEmployerForSalaried();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussSubSector> GetSubSector(int sectorId)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetPlussSubSectorBySectorID(sectorId);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussIncomeAcesmentMethod> GetIncomeAssement()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetPlussIncomeAcesmentMethod();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public t_pluss_userapproverlimit GetApproverLimitByUserId(int userId)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetApproverLimitByUserId(userId);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAnalystLoanApplication(Auto_AnalystAll objAnalystAll, int stateId)
        {

            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var userID = Convert.ToInt32(Session["Id"].ToString());

            var res = autoLoanAssessmentService.SaveAnalystLoanApplication(objAnalystAll, stateId, userID);

            return res;
            //return "Failed";
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string ChangeStateForAnalyst(int autoLoanMasterId, string remark, int stateId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;
                objAutoStatusHistory.StatusID = stateId;
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.ChangeStateForAnalyst(objAutoStatusHistory, remark);
                //SendData(remark);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return res;

        }


        private void SendData(string remark)
        {

            var All = new Object();
            var statePermission = (List<AutoUserTypeVsWorkflow>) Session["AutoWorkFlowPermission"];

            var llId = Convert.ToInt32(Session["LLId"]);
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            All = autoLoanAssessmentService.getLoanLocetorInfoByLLID(llId, statePermission);
            AutoLoanApplicationAll AutoAll = new AutoLoanApplicationAll();
            AutoAll = (AutoLoanApplicationAll) All;



            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            DeDupInfo deDupInfoObj = new DeDupInfo();
            string masterAccNo = "";
            deDupInfoObj.Name = AutoAll.objAutoPRApplicant.PrName;
            if (!String.IsNullOrEmpty(Convert.ToString(AutoAll.objAutoPRApplicant.DateofBirth)))
            {
                try
                {
                    deDupInfoObj.DateOfBirth =
                        Convert.ToDateTime(Convert.ToString(AutoAll.objAutoPRApplicant.DateofBirth),
                                           new System.Globalization.CultureInfo("ru-RU"));
                }
                catch
                {

                }
            }
            deDupInfoObj.FatherName = AutoAll.objAutoPRApplicant.FathersName;
            deDupInfoObj.MotherName = AutoAll.objAutoPRApplicant.MothersName;
            deDupInfoObj.EducationalLevel = AutoAll.objAutoPRApplicant.HighestEducation;
            deDupInfoObj.MaritalStatus = AutoAll.objAutoPRApplicant.MaritalStatus;
            deDupInfoObj.ResidentaileStatus = AutoAll.objAutoPRApplicant.ResidenceStatus;
            deDupInfoObj.Address2 = AutoAll.objAutoPRApplicant.PermanentAddress;
            deDupInfoObj.Tin = AutoAll.objAutoPRApplicant.TINNumber;

            deDupInfoObj.BusinessEmployeeName = AutoAll.objAutoPRProfession.NameofOrganization;
            deDupInfoObj.Address1 = AutoAll.objAutoPRProfession.Address;
            deDupInfoObj.Phone1 = AutoAll.objAutoPRProfession.OfficePhone;
            deDupInfoObj.Phone2 = AutoAll.objAutoPRApplicant.PhoneResidence;
            deDupInfoObj.Mobile1 = AutoAll.objAutoPRApplicant.Mobile;
            deDupInfoObj.Mobile2 = AutoAll.objAutoPRApplicant.SpouseContactNumber;

            for (int i = 0; i <= AutoAll.AutoApplicantSCBAccountList.Count; i++)
            {
                masterAccNo += AutoAll.AutoApplicantSCBAccountList[i].AccountNumberSCB;
                masterAccNo += ",";

            }
            if (masterAccNo != "")
            {
                if (masterAccNo.Length > 1)
                {
                    masterAccNo = masterAccNo.Substring(0, masterAccNo.Length - 1);
                }
            }
            deDupInfoObj.ScbMaterNo = masterAccNo.ToString();

            deDupInfoObj.IdType = AutoAll.objAutoPRApplicant.IdentificationNumberType;
            deDupInfoObj.IdNo = AutoAll.objAutoPRApplicant.IdentificationNumber;
            deDupInfoObj.IdType1 = "";
            deDupInfoObj.IdNo1 = "";
            deDupInfoObj.IdType2 = "";
            deDupInfoObj.IdNo2 = "";
            deDupInfoObj.IdType3 = "";
            deDupInfoObj.IdNo3 = "";
            deDupInfoObj.IdType4 = "";
            deDupInfoObj.IdNo4 = "";
            deDupInfoObj.ProductId = AutoAll.objAutoLoanMaster.ProductId;
            deDupInfoObj.Source = AutoAll.objAutoLoanMaster.SourceName;
            deDupInfoObj.BankBranch = AutoAll.AutoLoanAnalystApplication.BranchName;
            deDupInfoObj.DeclineDate = DateTime.Now; //Convert.ToDateTime(declineDateTextBox.Text.Trim());
            deDupInfoObj.DeclineReason = remark;
            deDupInfoObj.Remarks = remark;
            deDupInfoObj.Status = "N";
            deDupInfoObj.Active = "Y";
            deDupInfoObj.LoanAccountNo = Convert.ToString(llId);
            deDupInfoObj.CriteriaType = "D";
            deDupInfoObj.EntryDate = DateTime.Now;
            deDupInfoObjList.Add(deDupInfoObj);

            int ListInsert = 0;
            int countCheckedCheckBox = 0;

            var deDupInfoManagerObj = new DeDupInfoManager();
            ListInsert = deDupInfoManagerObj.SendDedupInfoInToDB(deDupInfoObjList);

            //if (ListInsert == INSERTE || ListInsert == UPDATE)
            //{
            //    UpdateStatus();
            //}
        }

        //private void UpdateStatus()
        //{
        //    int llid = Convert.ToInt32(negativeListPersonLLIdTextBox.Text);
        //    int declineFlag = 2;
        //    if (negativeListPersonStatusDropDownList.Text != "")
        //    {
        //        if (negativeListPersonStatusDropDownList.Text.Equals("N"))
        //        {
        //            declineFlag = 2;
        //        }
        //        else
        //        {
        //            declineFlag = 17;
        //        }
        //    }
        //    else
        //    {
        //        declineFlag = 17;
        //    }
        //    int updateFlag = 1;
        //    if (declineFlag != 17)
        //    {
        //        updateFlag = loanApplicationManagerObj.SetForwardStatus(llid, declineFlag, (string)Session["UserId"]);
        //    }
        //    if (updateFlag > -3)
        //    {
        //        int deleteFlag = loanApplicationManagerObj.DeleteDeferReason(llid);
        //        if (!String.IsNullOrEmpty(deleteFlag.ToString()))
        //        {
        //            List<Remarks> remarksListObj = new List<Remarks>();
        //            List<Int32> remarksIdListObj = new List<Int32>();
        //            foreach (GridViewRow gvr in remarksGridView.Rows)
        //            {
        //                if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked)
        //                {
        //                    Remarks remarksObj = new Remarks();
        //                    Int32 remarksIdObj;
        //                    remarksIdObj = Convert.ToInt32((gvr.FindControl("idLabel") as Label).Text);
        //                    remarksIdListObj.Add(remarksIdObj);
        //                    remarksObj.Id = remarksIdObj;
        //                    remarksObj.Name = (gvr.FindControl("remarksTextBox") as TextBox).Text;
        //                    remarksListObj.Add(remarksObj);
        //                }
        //            }
        //            int insertFlag = loanApplicationManagerObj.InsertDeferReason(llid, remarksIdListObj);
        //            if (insertFlag > -1)
        //            {
        //                LoanApplicationDetails loanApplicationDetailsObj = new LoanApplicationDetails();
        //                loanApplicationDetailsObj.LAId = llid;

        //                loanApplicationDetailsObj.PersonId = (string)Session["UserId"].ToString();
        //                loanApplicationDetailsObj.DeptId = 4;
        //                if (negativeListPersonStatusDropDownList.Text != "")
        //                {
        //                    if (negativeListPersonStatusDropDownList.Text.Equals("N"))
        //                    {
        //                        loanApplicationDetailsObj.ProcessStatusId = 2;
        //                    }
        //                    else
        //                    {
        //                        loanApplicationDetailsObj.ProcessStatusId = 17;
        //                    }
        //                }
        //                else
        //                {
        //                    loanApplicationDetailsObj.ProcessStatusId = 17;
        //                }

        //                int insertLoanAppDetailsFlag = loanApplicationManagerObj.InsertDeferReasonDetails(loanApplicationDetailsObj, remarksListObj);
        //                if (insertLoanAppDetailsFlag > 0)
        //                {
        //                    Alert.Show("Declined Successfully");
        //                    FildClear();
        //                }
        //                else
        //                {
        //                    Alert.Show("Decline Failed! Please Check Status in the Loan Locator System");
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Alert.Show("Decline Failed! Please Check Status in the Loan Locator System");
        //    }
        //}


    }
}
