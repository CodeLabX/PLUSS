﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.BASEL2Print" Codebehind="BASEL2Print.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pluss | BASEL II</title>
    
    <style type="text/css">
table.reference {
    background-color: #FFFFFF;
    border: 1px solid #C3C3C3;
    border-collapse: collapse;
    width: 100%;
}
table.reference th {
    background-color: #E5EECC;
    border: 1px solid #C3C3C3;
    padding: 0px;
    vertical-align: top;
}
table.reference td {
    border: 1px solid #C3C3C3;
    padding: 0px;
    vertical-align: top;
    font-size:12px;
} 
        .style1
        {
            width: 276px;
        }
        .style2
        {
            width: 347px;
        }
        .style3
        {
            width: 100%;
        }
        .style4
        {
        }
        .style6
        {
            width: 276px;
            text-align: left;
        }
        .style7
        {
            width: 132px;
        }
        .style8
        {
            width: 276px;
            height: 16px;
        }
        .style9
        {
            width: 347px;
            height: 16px;
        }
        .style11
        {
            width: 121px;
        }
        .style12
        {
            width: 120px;
        }
        .style13
        {
            width: 122px;
        }
    </style>
    
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 6.5in; height: auto; text-align:left;">
            <div style ="width:auto; height:20px;">
        <asp:Image ID="Image1" runat="server" ImageUrl="../../Image/headBorder.JPG" 
            Height="20px" Width="624px" />
    </div>

    AUTO LOAN CHECKLIST
        <br />
        <b>BASEL II DATA CHECKLIST </b><table class="reference" style="width: 100%;">
            <tr style="background-color: #808080; font-weight: bold;">
                <td class="style1">
                    FIELD NAME</td>
                <td>
                    ACCEPTABLE VALUE / SOURCE</td>
            </tr>
            <tr>
                <td class="style1">
                    Customer Name</td>
                <td>
                    <asp:Label ID="lCustomarName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Customer Master</td>
                <td>
                    <asp:Label ID="lCustomerMaster" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Nationality</td>
                <td>
                    BANGLADESHI</td>
            </tr>
            <tr>
                <td class="style1">
                    Gender Code</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style13" style="font-size: 9px">
                                • MALE</td>
                            <td class="style4" style="font-size: 9px">
                                • FEMALE</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Marital Status</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • MARRIED</td>
                            <td class="style4" style="font-size: 9px">
                                • SINGLE</td>
                            <td class="style4" style="font-size: 9px">
                                • OTHERS</td>
                        </tr>
                    </table>
                   <%-- <asp:Label ID="lMaritalStatus" runat="server"></asp:Label>--%>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Highest Education Qualification</td>
                <td>
                    <asp:Label ID="lHighestEducation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Employment Status Code</td>
                <td>
                    <asp:Label ID="lEmploymentStatusCode" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Business Establish Date</td>
                <td>
                    <asp:Label ID="lBusinessEstablishDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Month’s of Total Work Experience</td>
                <td>
                    <asp:Label ID="lMonthOfTotalWorkExperience" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Month’s in Current Job</td>
                <td>
                    <asp:Label ID="lMonthInCurrentJob" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Date of Joining</td>
                <td>
                    <asp:Label ID="lDateOfJoining" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Monthly Income</td>
                <td>
                    
                    <div style="width:45%; float:left;">
                        .</div>
                    
                    <div style="width:10%; float:left;">
                    <asp:Label ID="sdfsadfsd" runat="server">or</asp:Label>
                    </div>
                    
                    <div style="width:45%; float:left;">
                        .</div>
                    
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Other Monthly Income</td>
                <td>
                    <asp:Label ID="lOtherMonthlyIncome" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Monthly Loan Repayments ( Loan with Other Bank)</td>
                <td>
                    <asp:Label ID="lMonthlyLoanRepayment" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Outstanding Amount ( Loan With Other Bank)</td>
                <td>
                    <asp:Label ID="lOutstandingAmount" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Type of Account with Other Bank</td>
                <td>
                    <asp:Label ID="lTypeOfAccount" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Loan Account Number</td>
                <td>
                    <asp:Label ID="lLoanAccountNumber" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Date Loan Booked</td>
                <td>
                    <asp:Label ID="lDateLoanBooked" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Designation / Title</td>
                <td>
                    <asp:Label ID="lDesignation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Department</td>
                <td>
                    <asp:Label ID="lDepartment" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Date Of Birth (DD/ MM / YYYY)</td>
                <td>
                    <asp:Label ID="lDateOfBirth" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td>
                    <b>CREDIT INITIATION SHEET</b> </td>
            </tr>
            <tr>
                <td class="style1">
                    Withdrawal Date</td>
                <td>
                    <asp:Label ID="lWithdrawalDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Withdrawal Reason</td>
                <td>
                    <asp:Label ID="lWithdrawalReason" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Override Flag</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style11" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Pre Approval Flag (Always No, if Yes CCU to mention)</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style12" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Income Verification Flag</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                            <td style="font-size: 9px">
                                • NA</td>
                        </tr>
                    </table>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Employment Status Verification Flag</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                            <td style="font-size: 9px">
                                • NA</td>
                        </tr>
                    </table>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Credit Policy</td>
                <td>
                    <asp:Label ID="lCreditPolicy" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    SCB Unsecured Exposure</td>
                <td>
                    <asp:Label ID="lSCBUnsecuredExposure" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    SCB Secured Exposure</td>
                <td>
                    <asp:Label ID="lSCBSecuredExposure" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    LTV / FTV Ratio</td>
                <td>
                    <div style="width: 49.5%; float: left; height:auto;">
                    <asp:Label ID="lLTV" runat="server"></asp:Label>
                    </div>
                    <div style="width: 50%; float: left; height:auto;">
                    <asp:Label ID="lFTV" runat="server"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Total LTV / FTV Ratio</td>
                <td>
                    <div style="width: 49.5%; float: left; height:auto;">
                    <asp:Label ID="lTotalLTV" runat="server"></asp:Label>
                    </div>
                    <div style="width: 50%; float: left; height:auto;">
                    <asp:Label ID="lTotalFTV" runat="server"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Residence Address Verification Flag</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                            <td style="font-size: 9px">
                                • NA</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Home Telephone Verification Flag</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                            <td style="font-size: 9px">
                                • NA</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Office Telephone Verification Flag</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                            <td style="font-size: 9px">
                                • NA</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Mobile Telephone Verification Flag</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                            <td style="font-size: 9px">
                                • NA</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Deviation Code (Asset Ops to fill)</td>
                <td>
                    <asp:Label ID="lDeviationCodeAssetOps" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    DI / FB Ratio</td>
                <td>
                    <asp:Label ID="lDIRatio" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Internal Blacklist Flag</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style13" style="font-size: 9px">
                                • YES</td>
                            <td class="style4" style="font-size: 9px">
                                • NO</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Internal Blacklist Reason</td>
                <td>
                    <asp:Label ID="lInternalBacklistReason" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Residence State</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • DHAKA</td>
                            <td style="font-size: 9px">
                                • CHITTAGONG</td>
                            <td style="font-size: 9px">
                                • SYLHET</td>
                            <td style="font-size: 9px">
                                • KHULNA</td>
                            <td style="font-size: 9px">
                                • BOGRA</td>
                        </tr>
                    </table>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                Residence Type</td>
                <td>
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • OWNED</td>
                            <td style="font-size: 9px">
                                • COMPANY</td>
                            <td style="font-size: 9px">
                                • PARENTAL</td>
                            <td style="font-size: 9px">
                                • RENTED</td>
                        </tr>
                    </table>
                    </td>
            </tr>
            </table>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        <br />
        <br />
        <table class="reference" style="width: 100%;">
            <tr style="background-color: #808080; font-weight: bold;">
                <td class="style1">
                                        FIELD NAME<td class="style2">
                    ACCEPTABLE VALUE / SOURCE</td>
            </tr>
            <tr>
                <td class="style1">
                    YearYears of Residence at this Address</td>
                <td class="style2">
                    <asp:Label ID="lYearsOfResidence" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    If Rented Rental Per Month</td>
                <td class="style2" >
                    <asp:Label ID="lRentalPerMonth" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    No of Dependents</td>
                <td class="style2">
                    <asp:Label ID="lNoOfDependents" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Home Tel No</td>
                <td class="style2">
                    <asp:Label ID="lHomeTelNo" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Office Tel No</td>
                <td class="style2">
                    <asp:Label ID="lOfficeTelNo" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Mobile Tel No</td>
                <td class="style2">
                    <asp:Label ID="lMobileNo" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Number of Cars</td>
                <td class="style2">
                    <asp:Label ID="lNumbreOfCars" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Package / Promotion / Pre Approval Code</td>
                <td class="style2">
                    <asp:Label ID="lPackagePromoCode" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Guarantor 1 SCB Flag</td>
                <td class="style2">
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • YES</td>
                            <td style="font-size: 9px">
                                • NO</td>
                            <td style="font-size: 9px">
                                • NA</td>
                        </tr>
                    </table>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Co-Borrower 1 Relationship</td>
                <td class="style2">
                    <asp:Label ID="lCoBorrower1Relationship" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Co-Borrower 1 SCB Flag</td>
                <td class="style2">
                    <asp:Label ID="lCoBorrower1SCBFlag" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Purchase Date (For Auto &amp; Mortgage Only)</td>
                <td class="style2">
                    <asp:Label ID="lPurchaseDate" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Application Date</td>
                <td class="style2">
                    <asp:Label ID="lApplicationDate" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Application Channel>
                <td class="style2">
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • BRANCH</td>
                            <td class="style4" style="font-size: 9px">
                                • TELESALES</td>
                            <td class="style4" style="font-size: 9px">
                                • DIRECT SALES</td>
                            <td class="style4" style="font-size: 9px">
                                • OTHER</td>
                        </tr>
                    </table>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Secured Account Flag</td>
                <td class="style2">
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • YES - FOR AUTO &amp; MORTGAGE</td>
                            <td style="font-size: 9px">
                                • NO - FOR PERSONAL LOAN</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style8">
                    Top Top Up Amount</td>
                <td class="style9">
                    <asp:Label ID="lTopUpAmount" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Number of Top Up</td>
                <td class="style2">
                    <asp:Label ID="lNumberOfTopUp" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Top Up 1 Amount</td>
                <td class="style2">
                    <asp:Label ID="lTopUp1Amount" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Top Up 1 Date</td>
                <td class="style2">
                    <asp:Label ID="lTopUp1Date" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Top Up 2 Amount</td>
                <td class="style2">
                    <asp:Label ID="lTopUp2Amount" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Top Up 2 Date</td>
                <td class="style2">
                    <asp:Label ID="lTopUp2Date" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Top Up 3 Amount</td>
                <td class="style2">
                    <asp:Label ID="lTopUp3Amount" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Top Up 3 Date</td>
                <td class="style2">
                    <asp:Label ID="lTopUp3Date" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Price of Car</td>
                <td class="style2">
                    <asp:Label ID="lPriceOfCar" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Current Market Value ( for used cars)</td>
                <td class="style2">
                    <asp:Label ID="lCurrentMarketValue" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Car Model</td>
                <td class="style2">
                    <asp:Label ID="lCarModel" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Car Made Year</td>
                <td class="style2">
                    <asp:Label ID="lCarMadeYear" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Engine Size</td>
                <td class="style2">
                    <asp:Label ID="lEngineCC" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Car Usage</td>
                <td class="style2">
                    <asp:Label ID="lCarUsage" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Dealer Code</td>
                <td class="style2">
                    <asp:Label ID="lDealerCode" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1" style="vertical-align: middle">
                    Account Close Reason</td>
                <td class="style2">
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style7" style="font-size: 9px">
                                • CHARGE OFF</td>
                            <td style="font-size: 9px">
                                • PROPERT REDEEMED</td>
                        </tr>
                        <tr>
                            <td class="style7" style="font-size: 9px">
                                • CHARGE OFF</td>
                            <td style="font-size: 9px">
                                • REFINANCE</td>
                        </tr>
                        <tr>
                            <td class="style7" style="font-size: 9px">
                                • LOAN MATURED</td>
                            <td style="font-size: 9px">
                                • OTHER EARLY REDEMPTION</td>
                        </tr>
                    </table>
                    </td>
            </tr>
            <tr>
                <td class="style6" style="vertical-align: middle">
                    Charge Off Reason Code</td>
                <td class="style2" style="font-size: 9px">
                    <table class="style3" style="font-size: 9px">
                        <tr>
                            <td class="style4" colspan="2" style="font-size: 9px">
                                TO BE INPUT ONLY UPON CHARGING OFF AN ACCOUNT &amp; NOT OTHERWISE</td>
                        </tr>
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                Values</td>
                            <td style="font-size: 9px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • C= CREDIT LOSS</td>
                            <td class="style4" style="font-size: 9px">
                                • B= BANKRUPTCY</td>
                        </tr>
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • F= FRAUD LOSS</td>
                            <td style="font-size: 9px">
                                • T= COUNTERFEIT</td>
                        </tr>
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • L= JOB LOST</td>
                            <td style="font-size: 9px">
                                • P= PROPERTY DAMAGED</td>
                        </tr>
                        <tr>
                            <td class="style4" style="font-size: 9px">
                                • S= SKIPPED</td>
                            <td style="font-size: 9px">
                                &nbsp;</td>
                        </tr>
                    </table>
                    
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Name of Friend / Relative (Father’s / Mother’s Name)</td>
                <td class="style2">
                    <asp:Label ID="lNameOfFriend" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    Friend / Relative Contact</td>
                <td class="style2">
                    <asp:Label ID="lFriendRelativeContact" runat="server"></asp:Label>
                    </td>
            </tr>
            </table>
        <br />
        <br />
        <br />
        <br />
        <div style="border-top-style: solid; border-top-width: 3px; border-top-color: #000001; width: 250px; float: left;">
            AUTHORIZED SIGNATURE (CCU)</div>
        <div style="border-top-style: solid; border-top-width: 0px; border-top-color: #000001; width: 150px; float: left; display:block;">.
        </div>
        <div style="border-top-style: solid; border-top-width: 3px; border-top-color: #000001; width: 200px; float: left;">
            INPUT BY ( LOAN ADMIN)</div>
        <br />
            <br />
            <br />
    <div style ="width:auto; height:20px;">
        <asp:Image ID="Image2" runat="server" ImageUrl="../../Image/headBorder.JPG" 
            Height="20px" Width="624px" />
    </div>
            
            
            
            
            
            
            
            
            
    </div>
    </form>
</body>
</html>
