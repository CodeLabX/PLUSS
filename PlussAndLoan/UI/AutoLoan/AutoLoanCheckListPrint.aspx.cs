﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Entity.CheckListPrint;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class AutoLoanCheckListPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int llid = 0;

            llid = Convert.ToInt32(Request.QueryString["llid"]);

            if (llid > 0)
            {
                try
                {
                    AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                    var loanAssesmentService = new AutoLoanAssessmentService(repository);
                    AutoLoanApplicationAll res = new AutoLoanApplicationAll();
                    var res1 = loanAssesmentService.getLoanLocetorInfoByLLID(llid);
                    res = (AutoLoanApplicationAll)res1;

                    setValueToControl(res);
                    ConfigureReportObject();
                    
                }
                catch(Exception ex)
                {

                }
            }

        }

        private void ConfigureReportObject()
        {
            LoanCheckListPrintEntity loanCheck = new LoanCheckListPrintEntity();

            #region
            loanCheck.Label1 = Label1.Text.Trim();
            loanCheck.lLLID = lLLID.Text.Trim();
            loanCheck.lblProductId = lblProductId.Text.Trim();
            loanCheck.lApplicant = lApplicant.Text.Trim();
            loanCheck.lProduct = lProduct.Text.Trim();
            loanCheck.lAge = lAge.Text.Trim();
            loanCheck.lSource = lSource.Text.Trim();
            loanCheck.lIncome = lIncome.Text.Trim();
            loanCheck.lSubmissionDate = lSubmissionDate.Text.Trim();
            loanCheck.lBorrowingRelationship = lBorrowingRelationship.Text.Trim();
            loanCheck.lAppraisalDate = lAppraisalDate.Text.Trim();
            loanCheck.lApplicationType = lApplicationType.Text.Trim();
            loanCheck.lPurpose = lPurpose.Text.Trim();
            loanCheck.lBrand = lBrand.Text.Trim();
            loanCheck.lEngine = lEngine.Text.Trim();
            loanCheck.lQuotedPrice = lQuotedPrice.Text.Trim();
            loanCheck.lModel = lModel.Text.Trim();
            loanCheck.lManufacturingYear = lManufacturingYear.Text.Trim();
            loanCheck.lConsideredPrice = lConsideredPrice.Text.Trim();
            loanCheck.lVehicleType = lVehicleType.Text.Trim();
            loanCheck.lSeatingCapacity = lSeatingCapacity.Text.Trim();
            loanCheck.lConsiderationON = lConsiderationON.Text.Trim();
            loanCheck.lVehicleStatus = lVehicleStatus.Text.Trim();
            loanCheck.lAgeAtLoanExpory = lAgeAtLoanExpory.Text.Trim();
            loanCheck.lCarVerification = lCarVerification.Text.Trim();
            loanCheck.lVendor = lVendor.Text.Trim();
            loanCheck.lVendorCategory = lVendorCategory.Text.Trim();
            loanCheck.lVerificationStatus = lVerificationStatus.Text.Trim();
            loanCheck.lMou = lMou.Text.Trim();
            loanCheck.lPOAmount = lPOAmount.Text.Trim();
            loanCheck.lLTV = lLTV.Text.Trim();
            loanCheck.lRepayment = lRepayment.Text.Trim();
            loanCheck.lGenInsurance = lGenInsurance.Text.Trim();
            loanCheck.lLTVWithInsurance = lLTVWithInsurance.Text.Trim();
            loanCheck.lBankName = lBankName.Text.Trim();
            loanCheck.lARTA = lARTA.Text.Trim();
            loanCheck.lExtraLTV = lExtraLTV.Text.Trim();
            loanCheck.lAccountNumber = lAccountNumber.Text.Trim();
            loanCheck.lTotalLoan = lTotalLoan.Text.Trim();
            loanCheck.lAllowedDBR = lAllowedDBR.Text.Trim();
            loanCheck.lSecurityPDC = lSecurityPDC.Text.Trim();
            loanCheck.lEMI = lEMI.Text.Trim();
            loanCheck.lGivenDBR = lGivenDBR.Text.Trim();
            loanCheck.lSecPDCAmount = lSecPDCAmount.Text.Trim();
            loanCheck.lTenor = lTenor.Text.Trim();
            loanCheck.lDBRWithInsurance = lDBRWithInsurance.Text.Trim();
            loanCheck.lNoOfSecPDC = lNoOfSecPDC.Text.Trim();
            loanCheck.lInterestRate = lInterestRate.Text.Trim();
            loanCheck.lExtraDBR = lExtraDBR.Text.Trim();
            loanCheck.lSecPDCBank = lSecPDCBank.Text.Trim();
            loanCheck.lARTAFrom = lARTAFrom.Text.Trim();
            loanCheck.lLoanExpiry = lLoanExpiry.Text.Trim();
            loanCheck.lACNumber = lACNumber.Text.Trim();
            loanCheck.lTermLoanType1 = lTermLoanType1.Text.Trim();
            loanCheck.lTermLoanFlag1 = lTermLoanFlag1.Text.Trim();
            loanCheck.lTermLoanOriginalAmount1 = lTermLoanOriginalAmount1.Text.Trim();
            loanCheck.lTermLoanOutstanding1 = lTermLoanOutstanding1.Text.Trim();
            loanCheck.lTermLoanEMI1 = lTermLoanEMI1.Text.Trim();
            loanCheck.lTermLoanSecurityType1 = lTermLoanSecurityType1.Text.Trim();
            loanCheck.lTermLoanSecurityFV1 = lTermLoanSecurityFV1.Text.Trim();
            loanCheck.lTermLoanSecurityPV1 = lTermLoanSecurityPV1.Text.Trim();
            loanCheck.lTermLoanLTV1 = lTermLoanLTV1.Text.Trim();
            loanCheck.lTermLoanType2 = lTermLoanType2.Text.Trim();
            loanCheck.lTermLoanFlag2 = lTermLoanFlag2.Text.Trim();
            loanCheck.lTermLoanOriginalAmount2 = lTermLoanOriginalAmount2.Text.Trim();
            loanCheck.lTermLoanOutstanding2 = lTermLoanOutstanding2.Text.Trim();
            loanCheck.lTermLoanEMI2 = lTermLoanEMI2.Text.Trim();
            loanCheck.lTermLoanSecurityType2 = lTermLoanSecurityType2.Text.Trim();
            loanCheck.lTermLoanSecurityFV2 = lTermLoanSecurityFV2.Text.Trim();
            loanCheck.lTermLoanSecurityPV2 = lTermLoanSecurityPV2.Text.Trim();
            loanCheck.lTermLoanLTV2 = lTermLoanLTV2.Text.Trim();
            loanCheck.lTermLoanType3 = lTermLoanType3.Text.Trim();
            loanCheck.lTermLoanFlag3 = lTermLoanFlag3.Text.Trim();
            loanCheck.lTermLoanOriginalAmount3 = lTermLoanOriginalAmount3.Text.Trim();
            loanCheck.lTermLoanOutstanding3 = lTermLoanOutstanding3.Text.Trim();
            loanCheck.lTermLoanEMI3 = lTermLoanEMI3.Text.Trim();
            loanCheck.lTermLoanSecurityType3 = lTermLoanSecurityType3.Text.Trim();
            loanCheck.lTermLoanSecurityFV3 = lTermLoanSecurityFV3.Text.Trim();
            loanCheck.lTermLoanSecurityPV3 = lTermLoanSecurityPV3.Text.Trim();
            loanCheck.lTermLoanLTV3 = lTermLoanLTV3.Text.Trim();
            loanCheck.lTermLoanType4 = lTermLoanType4.Text.Trim();
            loanCheck.lTermLoanFlag4 = lTermLoanFlag4.Text.Trim();
            loanCheck.lTermLoanOriginalAmount4 = lTermLoanOriginalAmount4.Text.Trim();
            loanCheck.lTermLoanOutstanding4 = lTermLoanOutstanding4.Text.Trim();
            loanCheck.lTermLoanEMI4 = lTermLoanEMI4.Text.Trim();
            loanCheck.lTermLoanSecurityType4 = lTermLoanSecurityType4.Text.Trim();
            loanCheck.lTermLoanSecurityFV4 = lTermLoanSecurityFV4.Text.Trim();
            loanCheck.lTermLoanSecurityPV4 = lTermLoanSecurityPV4.Text.Trim();
            loanCheck.lTermLoanLTV4 = lTermLoanLTV4.Text.Trim();
            loanCheck.lCreditCardType1 = lCreditCardType1.Text.Trim();
            loanCheck.lCreditCardFlag1 = lCreditCardFlag1.Text.Trim();
            loanCheck.lCreditCardOriginalAmount1 = lCreditCardOriginalAmount1.Text.Trim();
            loanCheck.lCreditCardOutstanding1 = lCreditCardOutstanding1.Text.Trim();
            loanCheck.lCreditCardEMI1 = lCreditCardEMI1.Text.Trim();
            loanCheck.lCreditCardSecurityType1 = lCreditCardSecurityType1.Text.Trim();
            loanCheck.lCreditCardSecurityFV1 = lCreditCardSecurityFV1.Text.Trim();
            loanCheck.lCreditCardSecurityPV1 = lCreditCardSecurityPV1.Text.Trim();
            loanCheck.lCreditCardLTV1 = lCreditCardLTV1.Text.Trim();
            loanCheck.lCreditCardType2 = lCreditCardType2.Text.Trim();
            loanCheck.lCreditCardFlag2 = lCreditCardFlag2.Text.Trim();
            loanCheck.lCreditCardOriginalAmount2 = lCreditCardOriginalAmount2.Text.Trim();
            loanCheck.lCreditCardOutstanding2 = lCreditCardOutstanding2.Text.Trim();
            loanCheck.lCreditCardEMI2 = lCreditCardEMI2.Text.Trim();
            loanCheck.lCreditCardSecurityType2 = lCreditCardSecurityType2.Text.Trim();
            loanCheck.lCreditCardSecurityFV2 = lCreditCardSecurityFV2.Text.Trim();
            loanCheck.lCreditCardSecurityPV2 = lCreditCardSecurityPV2.Text.Trim();
            loanCheck.lCreditCardLTV2 = lCreditCardLTV2.Text.Trim();
            loanCheck.lThisLoanFlag = lThisLoanFlag.Text.Trim();
            loanCheck.lThisLoanOgiginalAmount = lThisLoanOgiginalAmount.Text.Trim();
            loanCheck.lThisLoanOutstanding = lThisLoanOutstanding.Text.Trim();
            loanCheck.lThisLoanEMI = lThisLoanEMI.Text.Trim();
            loanCheck.lThisLoanSecType = lThisLoanSecType.Text.Trim();
            loanCheck.lThisLoanSecurityFV = lThisLoanSecurityFV.Text.Trim();
            loanCheck.lThisLoanSecurityPV = lThisLoanSecurityPV.Text.Trim();
            loanCheck.lThisLoanLTV = lThisLoanLTV.Text.Trim();
            loanCheck.lOverDraftType1 = lOverDraftType1.Text.Trim();
            loanCheck.lOverDraftFlag1 = lOverDraftFlag1.Text.Trim();
            loanCheck.lOverDraftOriginalAmount1 = lOverDraftOriginalAmount1.Text.Trim();
            loanCheck.lOverDraftOutstanding1 = lOverDraftOutstanding1.Text.Trim();
            loanCheck.lOverDraftEMI1 = lOverDraftEMI1.Text.Trim();
            loanCheck.lOverDraftSecurityType1 = lOverDraftSecurityType1.Text.Trim();
            loanCheck.lOverDraftSecurityFV1 = lOverDraftSecurityFV1.Text.Trim();
            loanCheck.lOverDraftSecurityPV1 = lOverDraftSecurityPV1.Text.Trim();
            loanCheck.lOverDraftLTV1 = lOverDraftLTV1.Text.Trim();
            loanCheck.lOverDraftType2 = lOverDraftType2.Text.Trim();
            loanCheck.lOverDraftFlag2 = lOverDraftFlag2.Text.Trim();
            loanCheck.lOverDraftOriginalAmount2 = lOverDraftOriginalAmount2.Text.Trim();
            loanCheck.lOverDraftOutstanding2 = lOverDraftOutstanding2.Text.Trim();
            loanCheck.lOverDraftEMI2 = lOverDraftEMI2.Text.Trim();
            loanCheck.lOverDraftSecurityType2 = lOverDraftSecurityType2.Text.Trim();
            loanCheck.lOverDraftSecurityFV2 = lOverDraftSecurityFV2.Text.Trim();
            loanCheck.lOverDraftSecurityPV2 = lOverDraftSecurityPV2.Text.Trim();
            loanCheck.lOverDraftLTV2 = lOverDraftLTV2.Text.Trim();
            loanCheck.lOverDraftType3 = lOverDraftType3.Text.Trim();
            loanCheck.lOverDraftFlag3 = lOverDraftFlag3.Text.Trim();
            loanCheck.lOverDraftOriginalAmount3 = lOverDraftOriginalAmount3.Text.Trim();
            loanCheck.lOverDraftOutstanding3 = lOverDraftOutstanding3.Text.Trim();
            loanCheck.lOverDraftEMI3 = lOverDraftEMI3.Text.Trim();
            loanCheck.lOverDraftSecurityType3 = lOverDraftSecurityType3.Text.Trim();
            loanCheck.lOverDraftSecurityFV3 = lOverDraftSecurityFV3.Text.Trim();
            loanCheck.lOverDraftSecurityPV3 = lOverDraftSecurityPV3.Text.Trim();
            loanCheck.lOverDraftLTV3 = lOverDraftLTV3.Text.Trim();
            loanCheck.lOffSCBTermLoanBank1 = lOffSCBTermLoanBank1.Text.Trim();
            loanCheck.lOffSCBTermLoanOriginalLimit1 = lOffSCBTermLoanOriginalLimit1.Text.Trim();
            loanCheck.lOffSCBTermLoanEMI1 = lOffSCBTermLoanEMI1.Text.Trim();
            loanCheck.lOffSCBTermLoanBank2 = lOffSCBTermLoanBank2.Text.Trim();
            loanCheck.lOffSCBTermLoanOriginalLimit2 = lOffSCBTermLoanOriginalLimit2.Text.Trim();
            loanCheck.lOffSCBTermLoanEMI2 = lOffSCBTermLoanEMI2.Text.Trim();
            loanCheck.lOffSCBTermLoanBank3 = lOffSCBTermLoanBank3.Text.Trim();
            loanCheck.lOffSCBTermLoanOriginalLimit3 = lOffSCBTermLoanOriginalLimit3.Text.Trim();
            loanCheck.lOffSCBTermLoanEMI3 = lOffSCBTermLoanEMI3.Text.Trim();
            loanCheck.lOffSCBTermLoanBank4 = lOffSCBTermLoanBank4.Text.Trim();
            loanCheck.lOffSCBTermLoanOriginalLimit4 = lOffSCBTermLoanOriginalLimit4.Text.Trim();
            loanCheck.lOffSCBTermLoanEMI4 = lOffSCBTermLoanEMI4.Text.Trim();
            loanCheck.lOffSCBCreditCardBank1 = lOffSCBCreditCardBank1.Text.Trim();
            loanCheck.lOffSCBCreditCardOriginalLimit1 = lOffSCBCreditCardOriginalLimit1.Text.Trim();
            loanCheck.lOffSCBCreditCardEMI1 = lOffSCBCreditCardEMI1.Text.Trim();
            loanCheck.lOffSCBCreditCardBank2 = lOffSCBCreditCardBank2.Text.Trim();
            loanCheck.lOffSCBCreditCardOriginalLimit2 = lOffSCBCreditCardOriginalLimit2.Text.Trim();
            loanCheck.lOffSCBCreditCardEMI2 = lOffSCBCreditCardEMI2.Text.Trim();
            loanCheck.lOffSCBOverDraftBank1 = lOffSCBOverDraftBank1.Text.Trim();
            loanCheck.lOffSCBOverDraftOriginalLimit1 = lOffSCBOverDraftOriginalLimit1.Text.Trim();
            loanCheck.lOffSCBOverDraftBank2 = lOffSCBOverDraftBank2.Text.Trim();
            loanCheck.lOffSCBOverDraftOriginalLimit2 = lOffSCBOverDraftOriginalLimit2.Text.Trim();
            loanCheck.lOffSCBOverDraftBank3 = lOffSCBOverDraftBank3.Text.Trim();
            loanCheck.lOffSCBOverDraftOriginalLimit3 = lOffSCBOverDraftOriginalLimit3.Text.Trim();
            loanCheck.lTotalSCBExposures = lTotalSCBExposures.Text.Trim();
            loanCheck.lTotalLTV = lTotalLTV.Text.Trim();
            loanCheck.lTotalAggregateSecutityLoanRatio = lTotalAggregateSecutityLoanRatio.Text.Trim();
            loanCheck.lSurplus = lSurplus.Text.Trim();
            loanCheck.lTotalAutoLoan = lTotalAutoLoan.Text.Trim();
            loanCheck.lTotalMonthCommitments = lTotalMonthCommitments.Text.Trim();
            loanCheck.lTotalSCBSecuredLoans = lTotalSCBSecuredLoans.Text.Trim();
            loanCheck.lTotalSCBUnsecuredLoans = lTotalSCBUnsecuredLoans.Text.Trim();
            loanCheck.lCollerterization = lCollerterization.Text.Trim();
            loanCheck.Label163 = Label163.Text.Trim();
            loanCheck.lWithCollerterization = lWithCollerterization.Text.Trim();
            loanCheck.lAppRaisedBy1 = lAppRaisedBy1.Text.Trim();
            loanCheck.lAppSupportedBy1 = lAppSupportedBy1.Text.Trim();
            loanCheck.lStrength1 = lStrength1.Text.Trim();
            loanCheck.lStrength2 = lStrength2.Text.Trim();
            loanCheck.lStrength3 = lStrength3.Text.Trim();
            loanCheck.lStrength4 = lStrength4.Text.Trim();
            loanCheck.lStrength5 = lStrength5.Text.Trim();
            loanCheck.lStrength6 = lStrength6.Text.Trim();
            loanCheck.lStrength7 = lStrength7.Text.Trim();
            loanCheck.lWeakness1 = lWeakness1.Text.Trim();
            loanCheck.lWeakness2 = lWeakness2.Text.Trim();
            loanCheck.lWeakness3 = lWeakness3.Text.Trim();
            loanCheck.lWeakness4 = lWeakness4.Text.Trim();
            loanCheck.lWeakness5 = lWeakness5.Text.Trim();
            loanCheck.lWeakness6 = lWeakness6.Text.Trim();
            loanCheck.lWeakness7 = lWeakness7.Text.Trim();
            loanCheck.lDeviationLevel1 = lDeviationLevel1.Text.Trim();
            loanCheck.lDeviationDescription1 = lDeviationDescription1.Text.Trim();
            loanCheck.lDeviationCode1 = lDeviationCode1.Text.Trim();
            loanCheck.lDeviationLevel2 = lDeviationLevel2.Text.Trim();
            loanCheck.lDeviationDescription2 = lDeviationDescription2.Text.Trim();
            loanCheck.lDeviationCode2 = lDeviationCode2.Text.Trim();
            loanCheck.lDeviationLevel3 = lDeviationLevel3.Text.Trim();
            loanCheck.lDeviationDescription3 = lDeviationDescription3.Text.Trim();
            loanCheck.lDeviationCode3 = lDeviationCode3.Text.Trim();
            loanCheck.lDeviationLevel4 = lDeviationLevel4.Text.Trim();
            loanCheck.lDeviationDescription4 = lDeviationDescription4.Text.Trim();
            loanCheck.lDeviationCode4 = lDeviationCode4.Text.Trim();
            loanCheck.lUnderWritingLevel = lUnderWritingLevel.Text.Trim();
            loanCheck.lRemark1 = lRemark1.Text.Trim();
            loanCheck.lRemark2 = lRemark2.Text.Trim();
            loanCheck.lRemark3 = lRemark3.Text.Trim();
            loanCheck.lRemark4 = lRemark4.Text.Trim();
            loanCheck.lRemark5 = lRemark5.Text.Trim();
            loanCheck.lCondition1 = lCondition1.Text.Trim();
            loanCheck.Label265 = Label265.Text.Trim();
            loanCheck.Label266 = Label266.Text.Trim();
            loanCheck.lCondition2 = lCondition2.Text.Trim();
            loanCheck.Label268 = Label268.Text.Trim();
            loanCheck.Label267 = Label267.Text.Trim();
            loanCheck.lCondition3 = lCondition3.Text.Trim();
            loanCheck.Label269 = Label269.Text.Trim();
            loanCheck.Label270 = Label270.Text.Trim();
            loanCheck.lCondition4 = lCondition4.Text.Trim();
            loanCheck.Label272 = Label272.Text.Trim();
            loanCheck.Label271 = Label271.Text.Trim();
            loanCheck.lCondition5 = lCondition5.Text.Trim();
            loanCheck.Label273 = Label273.Text.Trim();
            loanCheck.Label274 = Label274.Text.Trim();
            loanCheck.lCondition6 = lCondition6.Text.Trim();
            loanCheck.Label2 = Label2.Text.Trim();
            loanCheck.Label3 = Label3.Text.Trim();
            loanCheck.lCondition7 = lCondition7.Text.Trim();
            loanCheck.Label4 = Label4.Text.Trim();
            loanCheck.Label5 = Label5.Text.Trim();
            loanCheck.lReworkDone = lReworkDone.Text.Trim();
            loanCheck.lReworkCount = lReworkCount.Text.Trim();
            loanCheck.lReworkReason = lReworkReason.Text.Trim();
            loanCheck.lUnderWritingDate = lUnderWritingDate.Text.Trim();
            loanCheck.lValidityUpTo = lValidityUpTo.Text.Trim();
            loanCheck.lMaxLoan = lMaxLoan.Text.Trim();
            loanCheck.lMaxLoanFlag = lMaxLoanFlag.Text.Trim();
            loanCheck.lExperience = lExperience.Text.Trim();
            loanCheck.lExperienceFlag = lExperienceFlag.Text.Trim();
            loanCheck.lMinLoan = lMinLoan.Text.Trim();
            loanCheck.lMinLoanFlag = lMinLoanFlag.Text.Trim();
            loanCheck.lAccountRelationshipe = lAccountRelationshipe.Text.Trim();
            loanCheck.lAccountRelationshipeFlag = lAccountRelationshipeFlag.Text.Trim();
            loanCheck.lLoanPurpose = lLoanPurpose.Text.Trim();
            loanCheck.lLoanPurposeFlag = lLoanPurposeFlag.Text.Trim();
            loanCheck.lMinimumIncome = lMinimumIncome.Text.Trim();
            loanCheck.lMinimumIncomeFlag = lMinimumIncomeFlag.Text.Trim();
            loanCheck.lSigment = lSigment.Text.Trim();
            loanCheck.lSigmentFlag = lSigmentFlag.Text.Trim();
            loanCheck.lPhisicalVarification = lPhisicalVarification.Text.Trim();
            loanCheck.lPhisicalVarificationFlag = lPhisicalVarificationFlag.Text.Trim();
            loanCheck.lMinAge = lMinAge.Text.Trim();
            loanCheck.lMinAgeFlag = lMinAgeFlag.Text.Trim();
            loanCheck.lPadTenor = lPadTenor.Text.Trim();
            loanCheck.lPadTenorFlag = lPadTenorFlag.Text.Trim();
            loanCheck.lMaxAge = lMaxAge.Text.Trim();
            loanCheck.lMaxAgeFlag = lMaxAgeFlag.Text.Trim();
            loanCheck.lNationality = lNationality.Text.Trim();
            loanCheck.lNationalityFlag = lNationalityFlag.Text.Trim();
            loanCheck.lTelephone = lTelephone.Text.Trim();
            loanCheck.lTelephoneFlag = lTelephoneFlag.Text.Trim();
            loanCheck.lLocation = lLocation.Text.Trim();
            loanCheck.lLocationFlag = lLocationFlag.Text.Trim();
            loanCheck.lAppRaisedBy = lAppRaisedBy.Text.Trim();
            loanCheck.lAppSupportedBy = lAppSupportedBy.Text.Trim();
            loanCheck.lAppApprovedBy = lAppApprovedBy.Text.Trim();
            loanCheck.lApproverComments = lApproverComments.Text.Trim();
            loanCheck.lUnderWritingLabel = lUnderWritingLevel.Text.Trim();
            loanCheck.lUnderWritingDate = lUnderWritingDate.Text.Trim();
            loanCheck.lValidityUpTo = lValidityUpTo.Text.Trim();
            #endregion

            //loanCheck.ListPrintEntity loanCheck. = new LoanCheckListPrintEntity();
            Session["LoanCheckListObject"] = loanCheck;
            Response.Redirect("AutoLoanCheckListPrintCrystal.aspx");


        }


        private void setValueToControl(AutoLoanApplicationAll res)
        {
            lLLID.Text = res.objAutoLoanMaster.LLID.ToString();
            double TotalSCBExposer = 0;
            double TotalSecurityFV = 0;
            double TotalLTV = 0;
            double AggregateSecurityLoanRatio = 0;
            double TotalSecurityPV = 0;
            double SurPlus = 0;
            double OnSCBEMI = 0;
            double OffSCBEMI = 0;
            double TotalMonthlyCommitments = 0;
            double TotalSCBSecuredLoan = 0;
            double TotalSCBUnSecuredLoan = 0;

            //lProduct.Text = res.objAutoLoanMaster.ProductId.ToString();
            var productName = "";
            switch (res.objAutoLoanMaster.ProductId) { 
                case 1:
                    productName = "Staff Auto";
                    break;
                case 2:
                    productName = "Conventional Auto";
                    break;
                case 3:
                    productName = "Saadiq Auto";
                    break;
            }
            lProduct.Text = productName;
            lblProductId.Text = res.objAutoLoanMaster.ProductId.ToString();

            lSource.Text = res.AutoLoanAnalystApplication.BranchName;//res.objAutoLoanMaster.SourceName;
            lSubmissionDate.Text = res.objAutoLoanMaster.ReceiveDate.ToString("yyyy-MM-dd"); 
            lApplicationType.Text = res.objAutoLoanMaster.JointApplication == true ? "Joint" : "Individual";
            lAppraisalDate.Text = res.objAutoLoanMaster.AppliedDate.ToString("yyyy-MM-dd");
            lPurpose.Text = "VEHICLE PURCHASE";
            lApplicant.Text = res.objAutoLoanMaster.JointApplication == true
                                  ? res.objAutoPRApplicant.PrName + ", " + res.objAutoJTApplicant.JtName
                                  : res.objAutoPRApplicant.PrName;

            lAge.Text = res.objAutoPRApplicant.Age.ToString();
            lIncome.Text = Convert.ToString(res.objAuto_An_IncomeSegmentIncome.TotalIncome) + " BDT";//res.objAutoPRFinance.DPIAmount.ToString();
            var browingRelationship = "";

            switch (res.AutoLoanAnalystApplication.BorrowingRelationShip) { 
                case "FTMB":
                    browingRelationship = "First Time Borrower";
                    break;
                case "NFTB":
                    browingRelationship = "NON FTB";
                    break;
                case "TMOB":
                    browingRelationship = "12 MOB";
                    break;
                case "TFMB":
                    browingRelationship = "24 MOB";
                    break;

            }

            lBorrowingRelationship.Text = Convert.ToString(browingRelationship);// add later.

            #region Vehicle

            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);
            List<AutoMenufacturer> objAutoManufacturer = autoManufactuerService.GetAutoManufacturer();

            var manufacturarname = "";
            for (var i = 0; i < objAutoManufacturer.Count; i++) {
                if (res.objAutoLoanVehicle.Auto_ManufactureId == objAutoManufacturer[i].ManufacturerId) {
                    manufacturarname = objAutoManufacturer[i].ManufacturerName;
                    break;
                }
            }


            lBrand.Text = manufacturarname.ToString();
            lModel.Text = res.objAutoLoanVehicle.Model;
            lVehicleType.Text = res.objAutoLoanVehicle.VehicleType;

            var vehicleStatus = "";
            switch (res.objAutoLoanVehicle.VehicleStatus)
            {
                case 1:
                    vehicleStatus = "New Car";
                    break;
                case 2:
                    vehicleStatus = "Recondition Car";
                    break;
                case 3:
                    vehicleStatus = "Used Car";
                    break;

            }

            lVehicleStatus.Text = vehicleStatus;
            lVendor.Text = res.objAutoVendor.VendorName;
            lMou.Text = res.objAutoVendorMou.IsMou.ToString() == "0"? "NO":"YES";
            lEngine.Text = res.objAutoLoanVehicle.EngineCC;


            lQuotedPrice.Text = res.objAutoLoanVehicle.QuotedPrice.ToString() + " BDT";
            lManufacturingYear.Text = res.objAutoLoanVehicle.ManufacturingYear.ToString();
            lConsideredPrice.Text = res.objAutoLoanVehicle.VerifiedPrice.ToString() + " BDT";
            lSeatingCapacity.Text = res.objAutoLoanVehicle.SeatingCapacity;
            
            lAgeAtLoanExpory.Text = Convert.ToString(DateTime.Now.Year - res.objAutoLoanVehicle.ManufacturingYear + res.objAuto_An_LoanCalculationTenor.ConsideredTenor); //set leater
            

            var vendoreCategoryName = "";
            switch (res.objAutoLoanVehicle.VendoreRelationshipStatus)
            {
                case 0:
                    vendoreCategoryName = "NON-MOU";
                    break;
                case 1:
                    vendoreCategoryName = "MOU";
                    break;
                case 2:
                    vendoreCategoryName = "NEGATIVE LISTED";
                    break;

            }


            lVendorCategory.Text = vendoreCategoryName;
            

            var carverification = "";
            switch (res.objAutoLoanVehicle.CarVerification)
            {
                case "1":
                    carverification = "Done";
                    break;
                case "2":
                    carverification = "Valued & Verified";
                    break;
                case "3":
                    carverification = "Not Required";
                    break;

            }
            lCarVerification.Text = carverification;
            var verificationStatus = "";
            switch (res.AutoLoanAnalystVehicleDetail.DeliveryStatus)
            {
                case 1:
                    verificationStatus = "Car Delivered";
                    break;
                case 2:
                    verificationStatus = "Car At Port";
                    break;
                case 3:
                    verificationStatus = "Car At Vendor";
                    break;

            }
            lVerificationStatus.Text = verificationStatus;


            
            var priceConsideredBasedOn = "";
            switch (res.objAutoLoanVehicle.PriceConsideredBasedOn)
            {
                case "1":
                    priceConsideredBasedOn = "Index Value";
                    break;
                case "2":
                    priceConsideredBasedOn = "Phone";
                    break;
                case "3":
                    priceConsideredBasedOn = "Price Verify";
                    break;
                case "4":
                    priceConsideredBasedOn = "Quotation";
                    break;
                case "5":
                    priceConsideredBasedOn = "NA";
                    break;

            }
            lConsiderationON.Text = priceConsideredBasedOn;



            #endregion Vehicle

            #region Loan Information

            #region comment
            //double a = PMT(.15, 5, -100000);

            //double interestRate = res.objAuto_An_LoanCalculationInterest == null ? .1 : res.objAuto_An_LoanCalculationInterest.ConsideredRate;
            //interestRate = interestRate / 100;
            //double tenor = res.objAuto_An_LoanCalculationTenor == null ? .1 : res.objAuto_An_LoanCalculationTenor.ConsideredTenor;
            //int perLac = -100000;

            //double a = PMT(interestRate, tenor, perLac);
            //int poAmount = Convert.ToInt32(a);
            //if (tenor == .1)
            //{
            //    lPOAmount.Text = "N/A";
            //}
            //else
            //{
            //    lPOAmount.Text = Convert.ToString(poAmount);
            //}
            #endregion comment

            lPOAmount.Text = Convert.ToString(res.objAuto_An_LoanCalculationAmount.ApprovedLoan) + " BDT";

            lLTV.Text = res.objAuto_An_LoanCalculationTotal == null ? "N/A" : res.objAuto_An_LoanCalculationTotal.LTVExcludingInsurance.ToString() + "%";
            
            //string repaymentMethodInstrument = Convert.ToString(res.objAuto_An_FinalizationRepaymentMethod.Instrument);

            lRepayment.Text = res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : Convert.ToString(res.objAuto_An_FinalizationRepaymentMethod.Instrument) == "1" ? "PDC" : "SI";


            lGenInsurance.Text = res.objAuto_An_LoanCalculationInsurance == null ? "N/A" : res.objAuto_An_LoanCalculationInsurance.GeneralInsurance.ToString() + " BDT";
            lLTVWithInsurance.Text = res.objAuto_An_LoanCalculationTotal == null
                                         ? "N/A"
                                         : res.objAuto_An_LoanCalculationTotal.LTVIncludingInsurance.ToString() + "%";
            //lBankName.Text = repaymentMethodInstrument == "2" ? "SCB" : res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.BankName.ToString();
            lBankName.Text = res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.BankName.ToString()== "1"? res.objAuto_An_FinalizationRepaymentMethod.BankName.ToString() : "SCB";

            string arta = Convert.ToString(res.AutoLoanAnalystApplication.ARTA);
            

            if (arta == "1") {
                lARTA.Text = res.objAuto_An_LoanCalculationInsurance.ARTA == null ? "N/A" : res.objAuto_An_LoanCalculationInsurance.ARTA.ToString() + " BDT";
            }
            if (arta == "3")
            {
                lARTA.Text = res.objAuto_An_LoanCalculationInsurance.ARTA == null ? "N/A" : res.objAuto_An_LoanCalculationInsurance.ARTA.ToString() + " BDT (CASH)";
            }
            else {
                lARTA.Text = "0 BDT";
            }
            lExtraLTV.Text = res.objAuto_An_LoanCalculationTotal == null ? "N/A" : res.objAuto_An_LoanCalculationTotal.ExtraLTV.ToString() + "%";

            //lAccountNumber.Text = res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.SCBAccount;
            lAccountNumber.Text = res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : Convert.ToString(res.objAuto_An_FinalizationRepaymentMethod.Instrument) == "2"? res.objAuto_An_FinalizationRepaymentMethod.SCBAccount :  res.objAuto_An_FinalizationRepaymentMethod.AccountNumber;

            lTotalLoan.Text = res.objAuto_An_LoanCalculationTotal == null ? "N/A" : res.objAuto_An_LoanCalculationTotal.TotalLoanAmount.ToString() + " BDT";
            lAllowedDBR.Text = res.objAuto_An_RepaymentCapability == null ? "N/A" : res.objAuto_An_RepaymentCapability.AppropriateDBR.ToString() + "%";
            lSecurityPDC.Text = res.objAuto_An_FinalizationRepaymentMethod == null
                                    ? "N/A"
                                    : res.objAuto_An_FinalizationRepaymentMethod.SecurityPDC == 1 ? "Yes" : "No";
            lEMI.Text = res.objAuto_An_LoanCalculationTotal == null ? "N/A" : res.objAuto_An_LoanCalculationTotal.EMR.ToString() + " BDT";
            lGivenDBR.Text = res.objAuto_An_RepaymentCapability == null ? "N/A" : res.objAuto_An_RepaymentCapability.ConsideredDBR.ToString() + "%";
            lSecPDCAmount.Text = res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.PDCAmount.ToString();
            lTenor.Text = res.objAuto_An_LoanCalculationTenor == null ? "N/A" : res.objAuto_An_LoanCalculationTenor.ConsideredTenor.ToString() + " YEARS";
            lDBRWithInsurance.Text = res.objAuto_An_LoanCalculationTotal == null ? "N/A" : res.objAuto_An_LoanCalculationTotal.FBRIncludingInsurance.ToString() + "%";
            lNoOfSecPDC.Text = res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.NoOfPDC.ToString();
            lInterestRate.Text = res.objAuto_An_LoanCalculationInterest == null ? "N/A" : res.objAuto_An_LoanCalculationInterest.ConsideredRate.ToString() + "%";
            lExtraDBR.Text = res.objAuto_An_LoanCalculationTotal == null ? "N/A" : res.objAuto_An_LoanCalculationTotal.ExtraDBR.ToString() + "%";
            lSecPDCBank.Text = res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.BankName.ToString();
            lARTAFrom.Text = res.AutoLoanAnalystApplication == null ? "N/A" : Convert.ToString(res.AutoLoanAnalystApplication.ARTAName);
            try
            {
                lLoanExpiry.Text = string.IsNullOrEmpty(Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredTenor)) ? "" : Convert.ToString(DateTime.Now.Date.Year + Convert.ToInt32(res.objAuto_An_LoanCalculationTenor.ConsideredTenor));
            }
            catch {
                lLoanExpiry.Text = "";
            }
            lACNumber.Text = res.objAuto_An_FinalizationRepaymentMethod == null ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.AccountNumber;

            #endregion Loan Information

            #region Exposer

            #region ON SCB

            int termLoanID = 1;
            int overDraftID = 1;
            int creditCardID = 1;


            #region TermLoan and OverDraft Credit Card
            for (int i = 1; i <= res.objAutoLoanFacilityList.Count; i++)
            {
                if (res.objAutoLoanFacilityList[i - 1].FacilityType == 23)
                {

                }
                if (res.objAutoLoanFacilityList[i - 1].Status == 1)
                {
                    if (res.objAutoLoanFacilityList[i - 1].FacilityType == 18 || res.objAutoLoanFacilityList[i - 1].FacilityType == 22)
                    {
                        //will added in Credit card region
                    }
                    else
                    {
                        TotalSCBExposer += res.objAutoLoanFacilityList[i - 1].PresentBalance;
                    }
                    TotalSecurityFV += res.objAutoLoanFacilityList[i - 1].FaceValue;
                    TotalSecurityPV += res.objAutoLoanFacilityList[i - 1].XTVRate;
                    if (res.objAutoLoanFacilityList[i - 1].FacilityType == 18 || res.objAutoLoanFacilityList[i - 1].FacilityType == 22)
                    {
                        //will added in Credit card region
                    }
                    else
                    {
                        OnSCBEMI += res.objAutoLoanFacilityList[i - 1].PresentEMI;
                    }
                    if (res.objAutoLoanFacilityList[i - 1].FacilityFlag == "S")
                    {
                        if (res.objAutoLoanFacilityList[i - 1].FacilityType == 18 || res.objAutoLoanFacilityList[i - 1].FacilityType == 22)
                        {
                            //will added in Credit card region
                        }
                        else
                        {
                            if (res.objAutoLoanFacilityList[i - 1].FacilityType == 23)
                            {
                                TotalSCBSecuredLoan += res.objAutoLoanFacilityList[i - 1].PresentLimit;
                            }
                            else
                            {
                                TotalSCBSecuredLoan += res.objAutoLoanFacilityList[i - 1].PresentBalance;
                            }
                        }
                    }
                    if (res.objAutoLoanFacilityList[i - 1].FacilityFlag == "U")
                    {
                        if (res.objAutoLoanFacilityList[i - 1].FacilityType == 18 || res.objAutoLoanFacilityList[i - 1].FacilityType == 22)
                        {
                            //will added in Credit card region
                        }
                        else
                        {
                            if (res.objAutoLoanFacilityList[i - 1].FacilityType == 23)
                            {
                                TotalSCBUnSecuredLoan += res.objAutoLoanFacilityList[i - 1].PresentLimit;
                            }
                            else
                            {
                                TotalSCBUnSecuredLoan += res.objAutoLoanFacilityList[i - 1].PresentBalance;
                            }
                        }
                    }

                }

                if(res.objAutoLoanFacilityList[i-1].FacilityType == 18 || res.objAutoLoanFacilityList[i-1].FacilityType == 22)
                {
                    if (res.objAutoLoanFacilityList[i - 1].Status == 1)
                    {
                        // Credit Card
                        #region Credit Card
                        TotalSCBExposer += res.objAutoLoanFacilityList[i - 1].PresentLimit;
                        if (res.objAutoLoanFacilityList[i - 1].FacilityFlag == "U")
                        {
                            TotalSCBUnSecuredLoan += res.objAutoLoanFacilityList[i - 1].PresentLimit;
                        }
                        else
                        {
                            TotalSCBSecuredLoan += res.objAutoLoanFacilityList[i - 1].PresentLimit;
                        }


                        Label lCreditCardType = this.FindControl("lCreditCardType" + creditCardID) as Label;
                        lCreditCardType.Text = res.objAutoLoanFacilityList[i - 1].FacilityTypeName;

                        Label lCreditCardFlag = this.FindControl("lCreditCardFlag" + creditCardID) as Label;
                        lCreditCardFlag.Text = res.objAutoLoanFacilityList[i - 1].FacilityFlag;

                        Label lCreditCardOriginalAmount = this.FindControl("lCreditCardOriginalAmount" + creditCardID) as Label;
                        lCreditCardOriginalAmount.Text = res.objAutoLoanFacilityList[i - 1].PresentLimit.ToString();

                        Label lCreditCardOutstanding = this.FindControl("lCreditCardOutstanding" + creditCardID) as Label;
                        lCreditCardOutstanding.Text = res.objAutoLoanFacilityList[i - 1].PresentLimit.ToString();

                        Label lCreditCardEMI = this.FindControl("lCreditCardEMI" + creditCardID) as Label;
                        double CCemi = 0;
                        try
                        {
                            CCemi = (res.objAutoLoanFacilityList[i - 1].InterestRate / 100) * res.objAutoLoanFacilityList[i - 1].PresentLimit;
                        }
                        catch (Exception ex)
                        {
                            CCemi = 0;
                        }
                        lCreditCardEMI.Text = CCemi.ToString();
                        OnSCBEMI += CCemi;

                        Label lCreditCardSecurityType = this.FindControl("lCreditCardSecurityType" + creditCardID) as Label;
                        switch (res.objAutoLoanFacilityList[i - 1].NatureOfSecurity.ToString())
                        {
                            case "-1":
                                lCreditCardSecurityType.Text = "N/A";
                                break;
                            case "1":
                                lCreditCardSecurityType.Text = "FD (SCB)";
                                break;
                            case "2":
                                lCreditCardSecurityType.Text = "Ezee FD";
                                break;
                            case "3":
                                lCreditCardSecurityType.Text = "MSS (SCB)";
                                break;
                            case "4":
                                lCreditCardSecurityType.Text = "CESS (SCB)";
                                break;
                            case "5":
                                lCreditCardSecurityType.Text = "RFCD";
                                break;
                            case "6":
                                lCreditCardSecurityType.Text = "WEDB-SCB";
                                break;
                            case "7":
                                lCreditCardSecurityType.Text = "WEDB-OTHER";
                                break;
                            case "8":
                                lCreditCardSecurityType.Text = "ICB Unit";
                                break;
                            case "9":
                                lCreditCardSecurityType.Text = "USD Bond DPB";
                                break;
                            case "10":
                                lCreditCardSecurityType.Text = "USD Bond DIB";
                                break;
                            case "11":
                                lCreditCardSecurityType.Text = "MORTGAGE SECURITY";
                                break;
                            case "12":
                                lCreditCardSecurityType.Text = "CAR REGISTRATION+INSURANCE";
                                break;
                            default:
                                lCreditCardSecurityType.Text = "N/A";
                                break;
                        }
                        Label lCreditCardSecurityFV = this.FindControl("lCreditCardSecurityFV" + creditCardID) as Label;
                        lCreditCardSecurityFV.Text = res.objAutoLoanFacilityList[i - 1].FaceValue.ToString();

                        Label lCreditCardSecurityPV = this.FindControl("lCreditCardSecurityPV" + creditCardID) as Label;
                        lCreditCardSecurityPV.Text = res.objAutoLoanFacilityList[i - 1].XTVRate.ToString();


                        var ltv = res.objAutoLoanFacilityList[i - 1].PresentBalance /
                                  res.objAutoLoanFacilityList[i - 1].XTVRate;

                        Label lCreditCardLTV = this.FindControl("lCreditCardLTV" + creditCardID) as Label;
                        lCreditCardLTV.Text = ltv.ToString("#.##");

                        creditCardID += 1;

                        #endregion
                    }
                    else
                    {

                    }
                }
                else if (res.objAutoLoanFacilityList[i - 1].FacilityType == 23)
                {
                    if (res.objAutoLoanFacilityList[i - 1].Status == 1)
                    {
                        // over draft
                        #region OverDraft
                        //lOverDraftType1.Text = res.objAutoLoanFacilityList[i - 1];
                        Label lOverDraftType = this.FindControl("lOverDraftType" + overDraftID) as Label;
                        lOverDraftType.Text = res.objAutoLoanFacilityList[i - 1].FacilityTypeName;

                        //lOverDraftFlag1.Text = res.objAutoLoanFacilityList[i - 1];
                        Label lOverDraftFlag = this.FindControl("lOverDraftFlag" + overDraftID) as Label;
                        lOverDraftFlag.Text = res.objAutoLoanFacilityList[i - 1].FacilityFlag;

                        //lOverDraftOriginalAmount1.Text = res.objAutoLoanFacilityList[i - 1];
                        Label lOverDraftOriginalAmount = this.FindControl("lOverDraftOriginalAmount" + overDraftID) as Label;
                        lOverDraftOriginalAmount.Text = res.objAutoLoanFacilityList[i - 1].PresentLimit.ToString();

                        //lOverDraftOutstanding1.Text = res.objAutoLoanFacilityList[i - 1];
                        Label lOverDraftOutstanding = this.FindControl("lOverDraftOutstanding" + overDraftID) as Label;
                        lOverDraftOutstanding.Text = res.objAutoLoanFacilityList[i - 1].PresentBalance.ToString();

                        //lOverDraftEMI1.Text = res.objAutoLoanFacilityList[i - 1];
                        Label lOverDraftEMI = this.FindControl("lOverDraftEMI" + overDraftID) as Label;
                        lOverDraftEMI.Text = res.objAutoLoanFacilityList[i - 1].PresentEMI.ToString();

                        //lOverDraftSecurityType1.Text = res.objAutoLoanFacilityList[i - 1];
                        Label lOverDraftSecurityType = this.FindControl("lOverDraftSecurityType" + overDraftID) as Label;
                        switch (res.objAutoLoanFacilityList[i - 1].NatureOfSecurity.ToString())
                        {
                            case "-1":
                                lOverDraftSecurityType.Text = "N/A";
                                break;
                            case "1":
                                lOverDraftSecurityType.Text = "FD (SCB)";
                                break;
                            case "2":
                                lOverDraftSecurityType.Text = "Ezee FD";
                                break;
                            case "3":
                                lOverDraftSecurityType.Text = "MSS (SCB)";
                                break;
                            case "4":
                                lOverDraftSecurityType.Text = "CESS (SCB)";
                                break;
                            case "5":
                                lOverDraftSecurityType.Text = "RFCD";
                                break;
                            case "6":
                                lOverDraftSecurityType.Text = "WEDB-SCB";
                                break;
                            case "7":
                                lOverDraftSecurityType.Text = "WEDB-OTHER";
                                break;
                            case "8":
                                lOverDraftSecurityType.Text = "ICB Unit";
                                break;
                            case "9":
                                lOverDraftSecurityType.Text = "USD Bond DPB";
                                break;
                            case "10":
                                lOverDraftSecurityType.Text = "USD Bond DIB";
                                break;
                            case "11":
                                lOverDraftSecurityType.Text = "MORTGAGE SECURITY";
                                break;
                            case "12":
                                lOverDraftSecurityType.Text = "CAR REGISTRATION+INSURANCE";
                                break;
                            default:
                                lOverDraftSecurityType.Text = "N/A";
                                break;
                        }
                        //lOverDraftSecurityType.Text = res.objAutoLoanFacilityList[i - 1].NatureOfSecurity.ToString();

                        //lOverDraftSecurityFV1.Text = res.objAutoLoanFacilityList[i - 1];
                        Label lOverDraftSecurityFV = this.FindControl("lOverDraftSecurityFV" + overDraftID) as Label;
                        lOverDraftSecurityFV.Text = res.objAutoLoanFacilityList[i - 1].FaceValue.ToString();

                        //lOverDraftSecurityPV1.Text = res.objAutoLoanFacilityList[i - 1];
                        Label lOverDraftSecurityPV = this.FindControl("lOverDraftSecurityPV" + overDraftID) as Label;
                        lOverDraftSecurityPV.Text = res.objAutoLoanFacilityList[i - 1].XTVRate.ToString();

                        //lOverDraftLTV1.Text = res.objAutoLoanFacilityList[i - 1];

                        var ltv = res.objAutoLoanFacilityList[i - 1].PresentBalance /
                                  res.objAutoLoanFacilityList[i - 1].XTVRate;

                        Label lOverDraftLTV = this.FindControl("lOverDraftLTV" + overDraftID) as Label;
                        lOverDraftLTV.Text = ltv.ToString("#.##");

                        overDraftID += 1;
                        #endregion
                    }
                    else
                    {

                    }

                }
                else
                {
                    if (res.objAutoLoanFacilityList[i - 1].Status == 1)
                    {
                        #region TermLoan
                        Label lTermLoanType = this.FindControl("lTermLoanType" + termLoanID) as Label;
                        lTermLoanType.Text = res.objAutoLoanFacilityList[i - 1].FacilityTypeName.ToString();

                        Label lTermLoanFlag = this.FindControl("lTermLoanFlag" + termLoanID) as Label;
                        lTermLoanFlag.Text = res.objAutoLoanFacilityList[i - 1].FacilityFlag;

                        Label lTermLoanOriginalAmount = this.FindControl("lTermLoanOriginalAmount" + termLoanID) as Label;
                        lTermLoanOriginalAmount.Text = res.objAutoLoanFacilityList[i - 1].PresentLimit.ToString();

                        Label lTermLoanOutstanding = this.FindControl("lTermLoanOutstanding" + termLoanID) as Label;
                        lTermLoanOutstanding.Text = res.objAutoLoanFacilityList[i - 1].PresentBalance.ToString();

                        Label lTermLoanEMI = this.FindControl("lTermLoanEMI" + termLoanID) as Label;
                        lTermLoanEMI.Text = res.objAutoLoanFacilityList[i - 1].PresentEMI.ToString();

                        Label lTermLoanSecurityType = this.FindControl("lTermLoanSecurityType" + termLoanID) as Label;
                        //lTermLoanSecurityType.Text = res.objAutoLoanFacilityList[i - 1].NatureOfSecurity.ToString();
                        switch (res.objAutoLoanFacilityList[i - 1].NatureOfSecurity.ToString())
                        {
                            case "-1":
                                lTermLoanSecurityType.Text = "N/A";
                                break;
                            case "1":
                                lTermLoanSecurityType.Text = "FD (SCB)";
                                break;
                            case "2":
                                lTermLoanSecurityType.Text = "Ezee FD";
                                break;
                            case "3":
                                lTermLoanSecurityType.Text = "MSS (SCB)";
                                break;
                            case "4":
                                lTermLoanSecurityType.Text = "CESS (SCB)";
                                break;
                            case "5":
                                lTermLoanSecurityType.Text = "RFCD";
                                break;
                            case "6":
                                lTermLoanSecurityType.Text = "WEDB-SCB";
                                break;
                            case "7":
                                lTermLoanSecurityType.Text = "WEDB-OTHER";
                                break;
                            case "8":
                                lTermLoanSecurityType.Text = "ICB Unit";
                                break;
                            case "9":
                                lTermLoanSecurityType.Text = "USD Bond DPB";
                                break;
                            case "10":
                                lTermLoanSecurityType.Text = "USD Bond DIB";
                                break;
                            case "11":
                                lTermLoanSecurityType.Text = "MORTGAGE SECURITY";
                                break;
                            case "12":
                                lTermLoanSecurityType.Text = "CAR REGISTRATION+INSURANCE";
                                break;
                            default:
                                lTermLoanSecurityType.Text = "N/A";
                                break;
                        }


                        Label lTermLoanSecurityFV = this.FindControl("lTermLoanSecurityFV" + termLoanID) as Label;
                        lTermLoanSecurityFV.Text = res.objAutoLoanFacilityList[i - 1].FaceValue.ToString();

                        Label lTermLoanSecurityPV = this.FindControl("lTermLoanSecurityPV" + termLoanID) as Label;
                        lTermLoanSecurityPV.Text = res.objAutoLoanFacilityList[i - 1].XTVRate.ToString();

                        var ltv = res.objAutoLoanFacilityList[i - 1].PresentBalance /
                                  res.objAutoLoanFacilityList[i - 1].XTVRate;
                        try
                        {
                            int ltv1 = Convert.ToInt32(ltv);
                        }
                        catch (Exception ex)
                        {
                            int ltv1 = 0;
                        }

                        Label lTermLoanLTV = this.FindControl("lTermLoanLTV" + termLoanID) as Label;
                        lTermLoanLTV.Text = (ltv *100).ToString("#.##") + "%";
                        termLoanID += 1;
                        #endregion 
                    }
                    else
                    {
                    }

                }
            }//end loop


            #endregion

            #region This Loan

            lThisLoanFlag.Text = "S";
            lThisLoanOgiginalAmount.Text = Convert.ToString(res.objAuto_An_LoanCalculationTotal.TotalLoanAmount);
            lThisLoanOutstanding.Text = Convert.ToString(res.objAuto_An_LoanCalculationTotal.TotalLoanAmount);
            lThisLoanEMI.Text = Convert.ToString(res.objAuto_An_LoanCalculationTotal.EMR);

            if (res.AutoLoanAnalystApplication.CashSecured100Per)
            {
                lThisLoanSecType.Text = "Cash Salarid";
                lThisLoanSecurityFV.Text = Convert.ToString(res.AutoLoanAnalystApplication.SecurityValue);
                lThisLoanSecurityPV.Text = Convert.ToString(res.AutoLoanAnalystApplication.SecurityValue);
                TotalSecurityFV += res.AutoLoanAnalystApplication.SecurityValue;
                TotalSecurityPV += res.AutoLoanAnalystApplication.SecurityValue;
            }
            else
            {
                lThisLoanSecType.Text = "Car Registration + Insurance";
                lThisLoanSecurityFV.Text = Convert.ToString(res.AutoLoanAnalystVehicleDetail.ConsideredPrice);
                lThisLoanSecurityPV.Text = Convert.ToString(res.AutoLoanAnalystVehicleDetail.ConsideredPrice);
                TotalSecurityFV += res.AutoLoanAnalystVehicleDetail.ConsideredPrice;
                TotalSecurityPV += res.AutoLoanAnalystVehicleDetail.ConsideredPrice;
            }
            lThisLoanLTV.Text = res.objAuto_An_LoanCalculationTotal == null ? "N/A" : res.objAuto_An_LoanCalculationTotal.LTVExcludingInsurance.ToString();

            #endregion 



            #endregion ON SCB

            #region OFF SCB

            int OfftermLoanID = 1;
            int OffoverDraftID = 1;
            int OffCreditCardID = 1;

            for (int i = 1; i <= res.AutoLoanFacilityOffSCBList.Count; i++)
            {
                if (Convert.ToInt32(res.AutoLoanFacilityOffSCBList[i - 1].Status) == 1)
                {
                    OffSCBEMI += res.AutoLoanFacilityOffSCBList[i - 1].EMI;
                }

                if (res.AutoLoanFacilityOffSCBList[i - 1].Type == 2)
                {
                    if (Convert.ToInt32(res.AutoLoanFacilityOffSCBList[i - 1].Status) == 1)
                    {
                        Label lOffSCBCreditCardBank = this.FindControl("lOffSCBCreditCardBank" + OffCreditCardID) as Label;
                        lOffSCBCreditCardBank.Text = res.AutoLoanFacilityOffSCBList[i - 1].BankName.ToString();

                        Label lOffSCBCreditCardOriginalLimit = this.FindControl("lOffSCBCreditCardOriginalLimit" + OffCreditCardID) as Label;
                        lOffSCBCreditCardOriginalLimit.Text = res.AutoLoanFacilityOffSCBList[i - 1].OriginalLimit.ToString();

                        Label lOffSCBCreditCardEMI = this.FindControl("lOffSCBCreditCardEMI" + OffCreditCardID) as Label;
                        lOffSCBCreditCardEMI.Text = res.AutoLoanFacilityOffSCBList[i - 1].EMI.ToString();

                        OffCreditCardID += 1;
                    }
                }
                else if (res.AutoLoanFacilityOffSCBList[i - 1].Type == 3)
                {
                    if (Convert.ToInt32(res.AutoLoanFacilityOffSCBList[i - 1].Status) == 1)
                    {
                        Label lOffSCBOverDraftBank = this.FindControl("lOffSCBOverDraftBank" + OffoverDraftID) as Label;
                        lOffSCBOverDraftBank.Text = res.AutoLoanFacilityOffSCBList[i - 1].BankName.ToString();

                        Label lOffSCBOverDraftOriginalLimit = this.FindControl("lOffSCBOverDraftOriginalLimit" + OffoverDraftID) as Label;
                        lOffSCBOverDraftOriginalLimit.Text = res.AutoLoanFacilityOffSCBList[i - 1].OriginalLimit.ToString();

                        OffoverDraftID += 1;
                    }

                }
                else
                {
                    //Term Loan

                    if (Convert.ToInt32(res.AutoLoanFacilityOffSCBList[i - 1].Status) == 1)
                    {
                        Label lOffSCBTermLoanBank = this.FindControl("lOffSCBTermLoanBank" + OfftermLoanID) as Label;
                        lOffSCBTermLoanBank.Text = res.AutoLoanFacilityOffSCBList[i - 1].BankName.ToString();

                        Label lOffSCBTermLoanOriginalLimit = this.FindControl("lOffSCBTermLoanOriginalLimit" + OfftermLoanID) as Label;
                        lOffSCBTermLoanOriginalLimit.Text = res.AutoLoanFacilityOffSCBList[i - 1].OriginalLimit.ToString();

                        Label lOffSCBTermLoanEMI = this.FindControl("lOffSCBTermLoanEMI" + OfftermLoanID) as Label;
                        lOffSCBTermLoanEMI.Text = res.AutoLoanFacilityOffSCBList[i - 1].EMI.ToString();

                        OfftermLoanID += 1;
                    }
                }
            }// end loop


            #endregion OFF SCB

            TotalSCBExposer += res.objAuto_An_LoanCalculationTotal.TotalLoanAmount;
            TotalLTV = (TotalSCBExposer / TotalSecurityFV) * 100;
            AggregateSecurityLoanRatio = (TotalSecurityFV / TotalSCBExposer) * 100;
            SurPlus = TotalSecurityPV - TotalSCBExposer;
            TotalMonthlyCommitments = OnSCBEMI + OffSCBEMI + res.objAuto_An_LoanCalculationTotal.EMR;

            #endregion Exposer

            #region Strength

            //Auto_An_FinalizationSterengthList
            for (int i = 1; i <= res.Auto_An_FinalizationSterengthList.Count; i++)
            {
                Label lStrength = this.FindControl("lStrength" + i) as Label;
                lStrength.Text = res.Auto_An_FinalizationSterengthList[i - 1].Strength;
            }

            #endregion Strength

            #region Weakness

            //Auto_An_FinalizationWeaknessList
            for (int i = 1; i <= res.Auto_An_FinalizationWeaknessList.Count; i++)
            {
                Label lWeakness = this.FindControl("lWeakness" + i) as Label;
                lWeakness.Text = res.Auto_An_FinalizationWeaknessList[i - 1].Weakness;
            }

            #endregion Weakness

            #region Deviation

            //Auto_An_FinalizationDeviationList

            var underWritingDeviation = "";

            for (int i = 1; i <= res.Auto_An_FinalizationDeviationList.Count; i++)
            {
                Label lDeviationLevel = this.FindControl("lDeviationLevel" + i) as Label;
                lDeviationLevel.Text = "Level " + res.Auto_An_FinalizationDeviationList[i - 1].Level.ToString();

                Label lDeviationDescription = this.FindControl("lDeviationDescription" + i) as Label;
                lDeviationDescription.Text = res.Auto_An_FinalizationDeviationList[i - 1].DescriptionText;

                Label lDeviationCode = this.FindControl("lDeviationCode" + i) as Label;
                lDeviationCode.Text = res.Auto_An_FinalizationDeviationList[i - 1].Code;

            }

            if (res.Auto_An_FinalizationDeviationList.Count > 0)
            {
                var label = res.Auto_An_FinalizationDeviationList[0].Level;
                if (label == 2)
                {
                    underWritingDeviation = "LEVEL-2";
                }
                else if (label == 3)
                {
                    underWritingDeviation = "LEVEL-3";
                }
                else
                {
                    underWritingDeviation = "LEVEL-1";
                }

            }
            else
            {
                underWritingDeviation = "LEVEL-1";
            }
            lUnderWritingLevel.Text = underWritingDeviation;

            #endregion Deviation

            #region Remark

            //Auto_An_FinalizationRemarkList
            for (int i = 1; i <= res.Auto_An_FinalizationRemarkList.Count; i++)
            {
                Label lRemark = this.FindControl("lRemark" + i) as Label;
                lRemark.Text = res.Auto_An_FinalizationRemarkList[i - 1].Remarks.ToString();

            }

            #endregion Remark

            #region Condition

            //Auto_An_FinalizationConditionList
            for (int i = 1; i <= res.Auto_An_FinalizationConditionList.Count; i++)
            {
                Label lCondition = this.FindControl("lCondition" + i) as Label;
                lCondition.Text = res.Auto_An_FinalizationConditionList[i - 1].Condition.ToString();

            }

            #endregion Remark

            #region Rework and Validity

            //objAuto_An_FinalizationRework
            if (res.objAuto_An_FinalizationRework != null)
            {
                if (res.objAuto_An_FinalizationRework.ReworkDone == 1)
                {
                    lReworkDone.Text = res.objAuto_An_FinalizationRework.ReworkDone == 0 ? "No" : "Yes";
                    var reworkCounttext = "";
                    if (res.objAuto_An_FinalizationRework.ReworkCount == 1)
                    {
                        reworkCounttext = "1st";
                    }
                    if (res.objAuto_An_FinalizationRework.ReworkCount == 2)
                    {
                        reworkCounttext = "2nd";
                    }

                    lReworkCount.Text = reworkCounttext.ToString();
                    lReworkReason.Text = res.objAuto_An_FinalizationRework.ReworkReason;
                    lUnderWritingDate.Text = res.objAuto_An_FinalizationRework.ReworkDate;
                    if (res.objAuto_An_FinalizationRework.ReworkDate != "")
                    {
                        lValidityUpTo.Text = Convert.ToString(Convert.ToDateTime(res.objAuto_An_FinalizationRework.ReworkDate).AddDays(45).ToString("dd/MM/yyyy"));
                    }
                }
            }

            #endregion Rework and Validity

            #region Pad Parameter
            #region Comment
            //lMaxLoan.Text = res.objAutoSegmentSettings.MaxLoanAmt.ToString();
            //lMinLoan.Text = res.objAutoSegmentSettings.MinLoanAmt.ToString();
            //lPurpose.Text = "Vehicle Purpose";
            //lSigment.Text = res.objAutoSegmentSettings.SegmentName;
            ////if(res.objAuto_An_LoanCalculationTenor.Level == 1)
            //lMinAge.Text = res.objAuto_An_LoanCalculationTenor.Level == 1
            //                   ? res.objAutoSegmentSettings.MinAgeL1.ToString()
            //                   : res.objAutoSegmentSettings.MinAgeL2.ToString();

            //lMaxAge.Text = res.objAuto_An_LoanCalculationTenor.Level == 1
            //                   ? res.objAutoSegmentSettings.MaxAgeL1.ToString()
            //                   : res.objAutoSegmentSettings.MaxAgeL2.ToString();
            //lTelephone.Text = res.objAutoPRApplicant.PhoneResidence;
            //lMinimumIncome.Text = res.objAuto_An_IncomeSegmentIncome.AppropriateIncome.ToString();
            //lTenor.Text = res.objAuto_An_LoanCalculationTenor.ConsideredTenor.ToString();
            //lNationality.Text = res.objAuto_An_AlertVerificationReport.Nationality;
            //lLocation.Text = res.objAuto_An_AlertVerificationReport.Location;
            #endregion comment


            lMaxLoan.Text = res.objAuto_An_AlertVerificationReport==null?"": res.objAuto_An_AlertVerificationReport.LoanAmountAllowed;
            lMaxLoanFlag.Text = res.objAuto_An_AlertVerificationReport== null?"": res.objAuto_An_AlertVerificationReport.LoanAmountFlag;
            lExperience.Text = res.objAutoPRProfession.MonthinCurrentProfession;
            int experience = 0;
            if (!string.IsNullOrEmpty(lExperience.Text))
            {
                try
                {
                    experience = Convert.ToInt32(lExperience.Text);
                }
                catch (Exception ex)
                {
                    experience = 0;
                }
            }
            lExperienceFlag.Text = res.objAutoPRProfession.PrimaryProfession == 2 ? experience >= 6 ? "Yes" : "N0" : experience >= 36 ? "Yes" : "N0";
            lMinLoan.Text = "200000";
            lMinLoanFlag.Text = "Yes";
            lAccountRelationshipe.Text = res.objAutoPRApplicant.RelationShipNumber;
            lAccountRelationshipeFlag.Text = "Yes";
            lLoanPurpose.Text = "VEHICLE PURCHASE";
            lLoanPurposeFlag.Text = "Yes";
            lMinimumIncome.Text = res.objAuto_An_AlertVerificationReport== null?"": res.objAuto_An_AlertVerificationReport.MinIncomeAllowed;
            lMinimumIncomeFlag.Text = res.objAuto_An_AlertVerificationReport== null?"": res.objAuto_An_AlertVerificationReport.MinIncomeFlag;
            lSigment.Text = res.objAutoSegmentSettings==null? "": res.objAutoSegmentSettings.SegmentName;
            lSigmentFlag.Text = "";
            lPhisicalVarification.Text = "REQUIRED";
            lPhisicalVarificationFlag.Text = "Yes";
            lMinAge.Text = res.objAuto_An_AlertVerificationReport == null?"": res.objAuto_An_AlertVerificationReport.MinAgeAllowed;
            lMinAgeFlag.Text = res.objAuto_An_AlertVerificationReport==null?"": res.objAuto_An_AlertVerificationReport.MinAgeFlag;
            lPadTenor.Text = res.objAuto_An_AlertVerificationReport==null?"": res.objAuto_An_AlertVerificationReport.TenorAllowed;
            lPadTenorFlag.Text = res.objAuto_An_AlertVerificationReport==null?"" : res.objAuto_An_AlertVerificationReport.TenorFlag;
            lMaxAge.Text = res.objAuto_An_AlertVerificationReport==null?"": res.objAuto_An_AlertVerificationReport.MaxAgeAllowed;
            lMaxAgeFlag.Text = res.objAuto_An_AlertVerificationReport==null? "" : res.objAuto_An_AlertVerificationReport.MaxAgeFlag;
            lNationality.Text = res.objAuto_An_AlertVerificationReport==null?"": res.objAuto_An_AlertVerificationReport.Nationality;
            lNationalityFlag.Text = res.objAuto_An_AlertVerificationReport==null?"": res.objAuto_An_AlertVerificationReport.NationalityConsidered == "1"? "Yes": "No";
            lTelephone.Text = string.IsNullOrEmpty(res.objAutoPRApplicant.PhoneResidence) ? "N/A" : res.objAutoPRApplicant.PhoneResidence;
            lTelephoneFlag.Text = string.IsNullOrEmpty(res.objAutoPRApplicant.PhoneResidence) ? "No": "Yes";
            lLocation.Text = res.objAuto_An_AlertVerificationReport==null? "": res.objAuto_An_AlertVerificationReport.Location;
            lLocationFlag.Text = res.objAuto_An_AlertVerificationReport==null? "No": res.objAuto_An_AlertVerificationReport.LocationCosidered == "1"? "Yes": "No";




            #endregion Pad Parameter

            #region Agrigate Calculation

            lTotalSCBExposures.Text = TotalSCBExposer.ToString("#.##");
            lTotalLTV.Text = TotalLTV.ToString("#.##") + "%";
            lTotalAggregateSecutityLoanRatio.Text = AggregateSecurityLoanRatio.ToString("#.##") + "%";
            lSurplus.Text = SurPlus.ToString("#.##");
            lTotalAutoLoan.Text = (res.objAuto_An_LoanCalculationTotal.TotalLoanAmount - (2000000 - res.objAuto_An_LoanCalculationAmount.ExistingFacilityAllowed)).ToString("#.##"); //Convert.ToString(res.objAuto_An_LoanCalculationAmount.ApprovedLoan);
            lTotalMonthCommitments.Text = TotalMonthlyCommitments.ToString("#.##");
            lTotalSCBSecuredLoans.Text = TotalSCBSecuredLoan.ToString("#.##");
            lTotalSCBUnsecuredLoans.Text = TotalSCBUnSecuredLoan.ToString("#.##");
            lCollerterization.Text = res.AutoLoanAnalystMaster.ColleterizationName;
            lWithCollerterization.Text = res.AutoLoanAnalystMaster.ColleterizationWithName;

            #endregion


            #region Others

            lAppRaisedBy1.Text = res.objAutoLoanMaster.SourceName;
            lAppRaisedBy.Text = res.objAutoLoanMaster.SourceName;
            lAppSupportedBy1.Text = res.objAuto_App_Step_ExecutionByUser.AppSupportByName;
            lAppSupportedBy.Text = res.objAuto_App_Step_ExecutionByUser.AppSupportByName;
            lAppApprovedBy.Text = res.objAuto_App_Step_ExecutionByUser.AppApprovedByName;
            lApproverComments.Text = res.AutoLoanAnalystMaster.ApproverRemark;
            #endregion Others

            BlankRow();

        }


        private void BlankRow()
        {
            #region Exposer

            #region ON SCB
            //Term Loan
            for (int a = 1; a <= 4; a++)
            {
                Label lTermLoanType = this.FindControl("lTermLoanType" + a) as Label;

                if (string.IsNullOrEmpty(lTermLoanType.Text))
                {
                    lTermLoanType.Text = "-";
                }
            }
            //Over Draft
            for (int a = 1; a <= 3; a++)
            {
                Label lOverDraftType = this.FindControl("lOverDraftType" + a) as Label;
                if (string.IsNullOrEmpty(lOverDraftType.Text))
                {
                    lOverDraftType.Text = "-";
                }
            }

            //Credit Card
            for (int a = 1; a <= 2; a++)
            {
                Label lCreditCardType = this.FindControl("lCreditCardType" + a) as Label;

                if (string.IsNullOrEmpty(lCreditCardType.Text))
                {
                    lCreditCardType.Text = "-";
                }
            }

            #endregion ON SCB

            #region OFF SCB

            //Label lOffSCBTermLoanBank = this.FindControl("lOffSCBTermLoanBank" + OfftermLoanID) as Label;
            //lOffSCBTermLoanBank.Text = res.AutoLoanFacilityOffSCBList[i - 1].BankName.ToString();
            //Label lOffSCBOverDraftBank = this.FindControl("lOffSCBOverDraftBank" + OffoverDraftID) as Label;
            //lOffSCBOverDraftBank.Text = res.AutoLoanFacilityOffSCBList[i - 1].BankName.ToString();
            //Label lOffSCBCreditCardBank = this.FindControl("lOffSCBCreditCardBank" + OffCreditCardID) as Label;
            //Term Loan
            for (int a = 1; a <= 4; a++)
            {
                Label lOffSCBTermLoanBank = this.FindControl("lOffSCBTermLoanBank" + a) as Label;

                if (string.IsNullOrEmpty(lOffSCBTermLoanBank.Text))
                {
                    lOffSCBTermLoanBank.Text = "-";
                }
            }
            //Over Draft
            for (int a = 1; a <= 3; a++)
            {
                Label lOffSCBOverDraftBank = this.FindControl("lOffSCBOverDraftBank" + a) as Label;
                if (string.IsNullOrEmpty(lOffSCBOverDraftBank.Text))
                {
                    lOffSCBOverDraftBank.Text = "-";
                }
            }

            //Credit Card
            for (int a = 1; a <= 2; a++)
            {
                Label lOffSCBCreditCardBank = this.FindControl("lOffSCBCreditCardBank" + a) as Label;

                if (string.IsNullOrEmpty(lOffSCBCreditCardBank.Text))
                {
                    lOffSCBCreditCardBank.Text = "-";
                }
            }


            #endregion OFF SCB



            #endregion
        }



        #region comment
        //var interestperlack = loanCalculatorHelPer.PMT(parseFloat(parseFloat(consideredInterest) / 100) / 12, tenor, -100000);

//PMT: function(i, n, p) {
//        return i * p * Math.pow((1 + i), n) / (1 - Math.pow((1 + i), n));
        //    },
        #endregion 

        private double PMT(double interestRate, double n, int p)
        {
            return interestRate * p * Math.Pow((1 + interestRate), n) / (1 - Math.Pow((1 + interestRate), n));
        }


    }
}
