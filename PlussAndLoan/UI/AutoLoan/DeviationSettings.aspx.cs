﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class DeviationSettings : System.Web.UI.Page
    {
        [AjaxNamespace("DeviationSettings")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(DeviationSettings));
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<DaviationSettingsEntity> GetDeviationSettingsSummary(int pageNo, int pageSize, string searchKey)
        {
            pageNo = pageNo * pageSize;

            DaviationSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var daviationSettingsService = new DaviationSettingsService(repository);


            List<DaviationSettingsEntity> objList_DaviationSettingsEntity =
                daviationSettingsService.GetDeviationSettingsSummary(pageNo, pageSize, searchKey);
            return objList_DaviationSettingsEntity;
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveDeviationSettings(DaviationSettingsEntity AutoDeviationSettings)
        {
            AutoDeviationSettings.UserId = Convert.ToInt32(Session["Id"]);
            DaviationSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var daviationSettingsService = new DaviationSettingsService(repository);
            var res = daviationSettingsService.SaveDeviationSettings(AutoDeviationSettings);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string DeviationSettingsDeleteById(int deviationId)
        {
            var userId = Convert.ToInt32(Session["Id"]);
            DaviationSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var daviationSettingsService = new DaviationSettingsService(repository);
            var res = daviationSettingsService.InactiveSettingsById(deviationId, userId);
            return res;

        }



    }
}
