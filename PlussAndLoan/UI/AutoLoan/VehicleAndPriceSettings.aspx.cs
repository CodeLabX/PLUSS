﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class VehicleAndPriceSettings : System.Web.UI.Page
    {
         
        [AjaxNamespace("VehicleAndPriceSettings")]

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(VehicleAndPriceSettings));
            //this.GetStatus();
        }

        protected void btnUploadSave_Click(object sender, EventArgs e)
        {
            string name = "";
            string returnValue = "";
            string msg = "";
            if (!fileDocumentUpload.FileContent.CanRead)
            {
                msg = "'Please select a file.'}";
            }
            else if (fileDocumentUpload.PostedFile.FileName == "" || fileDocumentUpload.PostedFile.FileName.ToUpper().Substring(fileDocumentUpload.PostedFile.FileName.Length - 3, 3) != "XLS")
            {
                msg = "'You are trying to upload wrong file format. Please upload a *.xls file.'";
            }
            else
            {
                try
                {
                    string serverPath = Request.PhysicalApplicationPath;
                    var directory = Server.MapPath(@"../Temp");
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    DateTime date = DateTime.Now;
                    name = date.ToString("yyyyMMddHHmmss") + "_" +
                             fileDocumentUpload.FileName;
                    fileDocumentUpload.SaveAs(Path.Combine(directory, name));

                    int uploadType = Convert.ToInt32(cmbUploadType.SelectedItem.Value);

                    msg = DumpExcelFile(Convert.ToString(Path.Combine(directory, name)), uploadType);

                }
                catch (Exception ex)
                {
                    msg = string.Format(JavaScriptSerializer.Serialize(ex.Message));
                }


            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
        }
        public string DumpExcelFile(string excelFilePath, int uploadType)
        {
            IVehicleAndPriceSettings repository = new AutoLoanDataService.AutoLoanDataService();
            var vehicleAndPriceSettingsService = new VehicleAndPriceSettingsService(repository);
            var res = vehicleAndPriceSettingsService.DumpExcelFile(excelFilePath, uploadType);
            return res;
            //return "Success";

        }
        private void GetStatus()
        {
            AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new AutoVendorMOUService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVendorMOUService.GetStatus(1);
            cmbUploadType.Items.Add(new ListItem("Select From List", "-1"));

            for (int i = 0; i < objList_AutoStatus.Count; i++)
            {
                cmbUploadType.Items.Add(new ListItem(objList_AutoStatus[i].StatusName, objList_AutoStatus[i].StatusId.ToString()));
            }
            cmbUploadType.SelectedValue = "-1";


        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> GetStatusForPrice(int statusType)
        {
            AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new AutoVendorMOUService(repository);


            List<AutoStatus> objList_AutoStatus = autoVendorMOUService.GetStatus(statusType);
            return objList_AutoStatus;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetVehicleSummary(int pageNo, int pageSize, string search)
        {
            pageNo = pageNo * pageSize;

            IVehicleAndPriceSettings repository = new AutoLoanDataService.AutoLoanDataService();
            var vehicleAndPriceSettingsService = new VehicleAndPriceSettingsService(repository);


            List<AutoVehicle> objListAutoVehicle =
                vehicleAndPriceSettingsService.GetVehicleSummary(pageNo, pageSize, search);
            return objListAutoVehicle;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPrice> GetAutoPriceByVehicleId(int vehicleId)
        {
            IVehicleAndPriceSettings repository = new AutoLoanDataService.AutoLoanDataService();
            var vehicleAndPriceSettingsService = new VehicleAndPriceSettingsService(repository);


            List<AutoPrice> objListAutoPrice =
                vehicleAndPriceSettingsService.GetAutoPriceByVehicleId(vehicleId);
            return objListAutoPrice;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAutoPrice(AutoPrice autoPrice)
        {
            IVehicleAndPriceSettings repository = new AutoLoanDataService.AutoLoanDataService();
            var vehicleAndPriceSettingsService = new VehicleAndPriceSettingsService(repository);
            var res = vehicleAndPriceSettingsService.SaveAutoPrice(autoPrice);
            return res;

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;


        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAutoVehicle(AutoVehicle autoVehicle)
        {
            IVehicleAndPriceSettings repository = new AutoLoanDataService.AutoLoanDataService();
            var vehicleAndPriceSettingsService = new VehicleAndPriceSettingsService(repository);
            var res = vehicleAndPriceSettingsService.SaveAutoVehicle(autoVehicle);
            return res;

        }


    }
}
