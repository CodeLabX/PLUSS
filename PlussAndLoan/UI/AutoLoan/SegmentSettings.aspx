<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.SegmentSettings" Title="Segment Settings" Codebehind="SegmentSettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../CSS/LoanApplicationTabContent.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            text-align: right;
            width: 120px;
        }
        .inputFieldCSS
        {
            width:230px;
        }
        .style2
        {
            text-align: right;
            width: 140px;
        }
        
        #nav_SegmentSettings a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <script src="../../Scripts/AutoLoan/AutoSegmentSettings.js" type="text/javascript"></script>

    <div id="Content">
    <%--For Pager --%>
    <div id="divSegmentSummary">
    <div class="pageHeaderDiv">Segment Settings</div>
    <div class="SearchwithPager">
    <div id="Pager" class="pager"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
    </div>
    <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					<col width="50px" />
					<col width="100px" />
					<col width="50px" />
					<col width="50px" />
					<col width="50px" />
					<col width="50px" />
					<col width="50px" />
					<col width="50px" />
					<col width="50px" />
					<col width="50px" />
					<col width="50px" />
					<col width="50px" />
					<col width="100px" />
				</colgroup>
				<thead>
					<tr>
						<th>
							SL
						</th>
						<th>
						    Segment
						</th>
						<th>
						    Profession
						</th>
						<th>
						    Max IR
						</th>
						<th>
						    Min Loan
						</th>
						<th>
							Max Loan
						</th>
						<th>
						    Min Tenor
						</th>
						<th>
						    Max Tenor
						</th>
						<th>
						    Min Age
						</th>
						<th>
						    Max Age
						</th>
						<th>
						    Min Income
						</th>
						<th>
						    Max Income
						</th>
						<th> <a id="lnkAddNewSegment" class="iconlink iconlinkAdd" href="javascript:;">New Segment </a>
						</th>
					</tr>
				</thead>
				<tbody id="SegmentGrid">
				</tbody>
			</table>
			
	</div>
    <br />
    <br />
    <%--<div style="text-align:center; font-size: x-large; font-weight: bold; background-color:#95B3d7">Segment Detail</div>--%>
   
    
   <div id="mainDiv" class="mainDivOfTabContent" style="display:none;">
    <div class="pageHeaderDiv">Segment Details</div>
    <br />
       <table style="width: 100%;">
           <tr>
               <td class="style1">
                   Segment Name:</td>
               <td>
                   <input id="txtSegmentName" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Segment Code:</td>
               <td>
                   <input id="txtSegmentCode" type="text" class="inputFieldCSS" maxlength="2" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Description:</td>
               <td colspan="3">
                   <input id="txtDescription" type="text" style="width:650px;" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Profession:</td>
               <td>
                   <select id="cmbProfession" name="D1" class="inputFieldCSS">
                       <option>
                       </option>
                   </select></td>
              <%-- <td class="style2">
                   Max Interest Rate:</td>
               <td>
                   <input id="txtMaxInterestRate" type="text" class="inputFieldCSS" /></td>--%>
           </tr>
           <tr>
               <td class="style1">
                   IR With ARTA:</td>
               <td>
                   <input id="txtIRWithARTA" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   IR Without ARTA:</td>
               <td>
                   <input id="txtIRWithoutARTA" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Min Loan Amount:</td>
               <td>
                   <input id="txtMinLoanAmount" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Max Loan Amount:</td>
               <td>
                   <input id="txtMaxLoanAmount" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Min Tenor:</td>
               <td>
                   <input id="txtMinTenor" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Max Tenor:</td>
               <td>
                   <input id="txtMaxTenor" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Min Age (L1):</td>
               <td>
                   <input id="txtMinAgeL1" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Min Age (L2):</td>
               <td>
                   <input id="txtMinAgeL2" type="text" style=" width:150px;" /><i>Years Below L1</i></td>
           </tr>
           <tr>
               <td class="style1">
                   Max Age:</td>
               <td>
                   <input id="txtMaxAgeL1" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Max Age (L2):</td>
               <td>
                   <input id="txtMaxAgeL2" type="text" style=" width:150px;" /><i>Years Above L1</i></td>
           </tr>
           <tr>
               <td class="style1">
                   LTV (Loan to Value):</td>
               <td>
                   <input id="txtLTV1L1" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   LTV (L2):</td>
               <td>
                   <input id="txtLTVL2" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Experience (L1):</td>
               <td>
                   <input id="txtExperienceL1" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Experience (L2):</td>
               <td>
                   <input id="txtExperienceL2" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   A/C Relationship (L1):</td>
               <td>
                   <input id="txtACRelationshipL1" type="text" style=" width:160px;" /><i>month/credit</i></td>
               <td class="style2">
                   A/C Relationship (L2):</td>
               <td>
                   <input id="txtACRelationshipL2" type="text" style=" width:160px;" /><i>month/credit</i></td>
           </tr>
           <tr>
               <td class="style1">
                   Criteria Code:</td>
               <td>
                   <input id="txtCriteriaCode" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Asset Code:</td>
               <td>
                   <input id="txtAssetCode" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Min DBR - L1:</td>
               <td>
                   <input id="txtMinDBRL1" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Min DBR � L2:</td>
               <td>
                   <input id="txtMinDBRL2" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Max DBR � L2:</td>
               <td>
                   <input id="txtMaxDBRL2" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   DBR � L3:</td>
               <td>
                   <input id="txtDBRL3" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Field Visit:</td>
               <td>
                   <select id="cmbFieldVisit" name="D2" class="inputFieldCSS">
		                <option value="1">Required</option>
		                <option value="0">Not Required</option>
                   </select></td>
               <td class="style2">
                   Verification:</td>
               <td>
                   <select id="cmbVerification" name="D4" class="inputFieldCSS">
		                <option value="1">Required</option>
		                <option value="0">Not Required</option>
                   </select></td>
           </tr>
           <tr>
               <td class="style1">
                   Land Telephone:</td>
               <td>
                   <select id="cmbLandTelephone" name="D3" class="inputFieldCSS">
		                <option value="1">Required</option>
		                <option value="0">Not Required</option>
                   </select></td>
               <td class="style2">
                   Guarantee/Reference</td>
               <td>
                   <select id="cmbGuarantee" name="D5" class="inputFieldCSS">
		                <option value="1">Required</option>
		                <option value="0">Not Required</option>
                   </select></td>
           </tr>
           <tr>
               <td class="style1">
                   Primary Min Income:</td>
               <td>
                   <input id="txtPrimaryMinIncomeL1" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Primary Min Income (L2):</td>
               <td>
                   <input id="txtPrimaryMinIncomeL2" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Joint Min Income:</td>
               <td>
                   <input id="txtJointMinIncomeL1" type="text" class="inputFieldCSS" /></td>
               <td class="style2">
                   Joint Min Income (L2):</td>
               <td>
                   <input id="txtJointMinIncomeL2" type="text" class="inputFieldCSS" /></td>
           </tr>
           <tr>
               <td class="style1">
                   Comments:</td>
               <td colspan="3">
                   <input id="txtComments" type="text" style="width:650px;" /></td>
           </tr>
           <tr>
               <td class="style1">
                   &nbsp;</td>
               <td>
                   &nbsp;</td>
               <td class="style2">
                   &nbsp;</td>
               <td>
                   &nbsp;</td>
           </tr>
           </table><br />
		<div style="text-align:center;">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		</div>
   </div>
    
    </div>
    




</asp:Content>

