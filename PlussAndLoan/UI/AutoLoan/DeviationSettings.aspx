﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.DeviationSettings" Title="Daviation Settings" Codebehind="DeviationSettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<div class="pageHeaderDiv">Daviation Settings</div>
    <script src="../../Scripts/AutoLoan/DaviationSettings.js" type="text/javascript"></script>
    
    
        <style type="text/css">
        #nav_DeviationSettings a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>

    <%--All Aspx code will goes on Content div--%>
    <div id="Content">
    <%--For Pager --%>
    <div class="SearchwithPager">
<div class="search">
    <input type="text" id="txtSearch" class="txt txtSearch widthSize35_per" onkeypress="DaviationSettingsHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search" onclick="DaviationSettingsManager.render();" />
    </div> 
    <div id="Pager" class="pager pagerRight_4per"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
    </div>
    <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					<col width="50px" />
					<col width="100px" />
					<col width="250px" />
					<col width="150px" />
					<col width="150px" />
				</colgroup>
				<thead>
					<tr>
						<th>
							SL
						</th>
						<th>
						    Daviation For
						</th>
						<th>
						    Daviation Description
						</th>
						<th>
						    Daviation Code
						</th>
						<th> <a id="lnkAddNew" class="iconlink iconlinkAdd" href="javascript:;">Add New Daviation</a>
						</th>
					</tr>
				</thead>
				<tbody id="tblDaviationSettings">
				</tbody>
			</table>
    
    </div>
    
    <div class="modal" id="DaviationPopupDiv" style="display:none">
		<h3>Daviation Settings</h3>
		<label for="cmbDaviationFor">Daviation For</label>
		<select id="cmbDaviationFor" class="txt" title="Daviation For" >
            <option value="-1" selected="selected">Select a Type</option>
            <option value="1">Income Assessment</option>
            <option value="2">LEVEL-2</option>
            <option value="3">LEVEL-3</option>
		</select>
		 <br/>
		<label for="txtDeviationCode">Deviation Code:</label> <input class="txt" id="txtDeviationCode" title="Deviation Code" maxlength="3"/><br/>
		 <label for="txtDescription">Description:</label> <input class="txt" id="txtDescription" title="Description"/><br/>
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>


</asp:Content>

