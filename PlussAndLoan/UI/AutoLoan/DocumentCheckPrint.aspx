﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PlussAndLoan.UI.AutoLoan.DocumentCheckPrint" Codebehind="DocumentCheckPrint.aspx.cs" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Pluss | Document Check List </title>
    <link href="../../CSS/AutoLoanPrint.css" rel="stylesheet" type="text/css" />

    <script src="../../Scripts/JSLibs/prototype.js" type="text/javascript"></script>

    <script src="../../Scripts/JSLibs/util.js" type="text/javascript"></script>

    <script src="../../Scripts/AutoLoan/DocumentCheckPrint.js" type="text/javascript"></script>

    <script src="../../Scripts/JSLibs/effects.js" type="text/javascript"></script>

    <style type="text/css">
        .style1
        {
            text-align: Center;
        }
        .style2
        {
            width: 181px;
        }
        .style5
        {
            width: 234px;
            text-align: Center;
            background-color: #CCCCCC;
        }
        .style6
        {
            width: 268px;
            background-color: #CCCCCC;
        }
        .style7
        {
            font-size: x-small;
        }
        .style50
        {
            font-size: small;
            width: 382px;
        }
        .style53
        {
            font-size: small;
        }
        .style54
        {
            width: 219px;
            font-size: small;
        }
        .style55
        {
            width: 73px;
        }
        .style56
        {
            width: 59px;
            background-color: #999999;
        }
        .style57
        {
            width: 59px;
            text-align: center;
            background-color: #CCCCCC;
        }
        .style58
        {
            width: 253px;
            background-color: #CCCCCC;
        }
        .style59
        {
            width: 215px;
            background-color: #999999;
        }
        .style60
        {
            width: 215px;
            text-align: Center;
            background-color: #CCCCCC;
        }
        .style61
        {
            font-size: small;
            width: 188px;
        }
        .style62
        {
            width: 146px;
            font-size: small;
        }
        .style67
        {
            width: 188px;
        }
        .style68
        {
            font-size: small;
            height: 23px;
        }
        .style69
        {
            width: 73px;
            height: 23px;
            background-color: #CCCCCC;
        }
        .style70
        {
            width: 73px;
            background-color: #CCCCCC;
        }
        .style71
        {
            background-color: #999999;
        }
        .style72
        {
            width: 59px;
            text-align: center;
            height: 23px;
            background-color: #CCCCCC;
        }
        .style73
        {
            width: 253px;
            height: 23px;
            background-color: #CCCCCC;
        }
        .style74
        {
            width: 215px;
            text-align: Center;
            height: 23px;
            background-color: #CCCCCC;
        }
        .style75
        {
            height: 23px;
            background-color: #CCCCCC;
        }
        .style76
        {
            background-color: #CCCCCC;
        }
    </style>
    <link href="../../CSS/Default.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <%--<input type="button" id="btnPrint" value="Print" />--%>
    <asp:Panel ID="mainPanel" runat="server">
    <div style="width: 6.5in; height: auto; text-align: left;">
        <div style="width: auto; height: 20px;">
            <asp:Image ID="Image1" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                Width="624px" />
        </div>
        <div style="text-align: center">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Larger">DOCUMENT CHECK LIST</asp:Label>
            <asp:Label ID="lblProductId" runat="server" Font-Bold="True" Font-Size="Larger" Visible =false ></asp:Label>
        </div>
        <br />
        <b style="text-align: left">FOR CREDIT RELATED `DOCUMENTS</b>
        <div style="border: 3px solid #000000">
            <table style="width: 100%;">
                <tr>
                    <td class="style2">
                        LLID
                    </td>
                    <td>
                        <asp:Label ID="lLLID" runat="server" Style="background-color: #999999"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        UNDERWRITING LEVEL
                    </td>
                    <td>
                        <asp:Label ID="lUnderWritingLevel" runat="server" Style="background-color: #999999"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        APPLICANT NAME
                    </td>
                    <td>
                        <asp:Label ID="lApplicantName" runat="server" Style="background-color: #999999"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        A/C NUMBER
                    </td>
                    <td>
                        <asp:Label ID="lAccountNumber" runat="server" Style="background-color: #999999"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        SOURCE
                    </td>
                    <td>
                        <asp:Label ID="lSource" runat="server" Style="background-color: #999999"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        SEGMENT
                    </td>
                    <td>
                        <asp:Label ID="lSegment" runat="server" Style="background-color: #999999"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="clear: both;">
        </div>
        <br />
        <div style="width: 50%; float: left; border: 0;">
            <b>APPLICANT GIVEN DOCS</b>
        </div>
        <div style="width: 50%; float: left; border: 0;">
            <b>APPLICANT GIVEN DOCS</b>
        </div>
        <div style="xborder: 3px solid #000000">
            <div style="float: left; width: 46%; margin-right: 5px; margin-left: 5px; border: 3px solid #000000">
                <table class="reference" style="width: 100%;">
                    <tr>
                        <td class="style68">
                            Application Form filled up properly
                        </td>
                        <td style="text-align: center" class="style69">
                            <asp:Label ID="lApplicationForm" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Photograph of Customer(s)
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lPhoto" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Latest TIN Certificate
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lTin" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Photo ID
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lPhotoID" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Letter of Introduction/Salary Certificate
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lSalaryCertificate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Payslips/CASH Vouchers
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lPaySlip" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Trade Lisence
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lTradeLisence" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            MOA &amp; AOA
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lMOA" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Partnership Deed
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lPartnership" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Form-X/Form-12/Form-1
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lFormX" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Board Resolution
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lBoardRes" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Diploma Certificate
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lDipCert" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Certificate from Professional Association
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lCertFromPof" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Copy of Municipal Tax recipt
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lTaxRecipt" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Title Deeds
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lTitleDeed" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Rental Deeds/Slips
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lRentalDeed" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Utility Bills Copy
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lUtilityBill" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            OD or Loan Offer Letter(s)
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lLoanOfferLetter" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Global Consolidation
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lGlobalConso" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Signed IID
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lSignedIID" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53">
                            Used Car docs
                        </td>
                        <td style="text-align: center" class="style70">
                            <asp:Label ID="lUsedCarDoc" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="float: left; width: 46%; margin-left: 5px; margin-right: 5px; border: 3px solid #000000">
                <table class="reference" style="width: 100%;">
                    <tr>
                        <td class="style54">
                            Credit Checklist
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lCreditCheck" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            Income Assesment Sheet
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lIncomeAssSheet" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            Limit Loading Instruction
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lLimitLoadingIns" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            BASEL II Data Checklist
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lBASEL2Data" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            De-Duplication
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lDeDup" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            Norkom
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lNorkom" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            CIB Report
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lCIBReport" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            CPV Report
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lCPVReport" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            Car Price Verification Report
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lPriceVerf" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            Credit Card Repayment History
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lCreditCardRep" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            Bank Statement Verification
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lBankStatVeri" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            Credit Report(s)
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lCreditReport" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            Car Verification Report
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lCarVerifReport" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style54">
                            USED CAR Valuation Report
                        </td>
                        <td style="text-align: center" class="style76">
                            <asp:Label ID="lUsedCarValuation" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <b>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div style="clear: both;">
            </div>
            CIB REQUEST LOANS</b>
        <div style="border: 3px solid #000000">
            <table class="referenceWhite" style="width: 100%;">
                <tr>
                    <td style="text-align: center;" class="style56">
                        SL. NO
                    </td>
                    <td style="text-align: center" class="style59">
                        LOANS FACILITY
                    </td>
                    <td style="text-align: center" class="style71">
                        AMOUNT
                    </td>
                    <td style="text-align: center" class="style71">
                        TENOR&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style57">
                        1
                    </td>
                    <td class="style58">
                        <asp:Label ID="lFacility1" runat="server"></asp:Label>
                    </td>
                    <td class="style60">
                        <asp:Label ID="lAmount1" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lTenor1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style72">
                        2
                    </td>
                    <td class="style73">
                        <asp:Label ID="lFacility2" runat="server"></asp:Label>
                    </td>
                    <td class="style74">
                        <asp:Label ID="lAmount2" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style75">
                        <asp:Label ID="lTenor2" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style57">
                        3
                    </td>
                    <td class="style58">
                        <asp:Label ID="lFacility3" runat="server"></asp:Label>
                    </td>
                    <td class="style60">
                        <asp:Label ID="lAmount3" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lTenor3" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style57">
                        4
                    </td>
                    <td class="style58">
                        <asp:Label ID="lFacility4" runat="server"></asp:Label>
                    </td>
                    <td class="style60">
                        <asp:Label ID="lAmount4" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lTenor4" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style57">
                        5
                    </td>
                    <td class="style58">
                        <asp:Label ID="lFacility5" runat="server"></asp:Label>
                    </td>
                    <td class="style60">
                        <asp:Label ID="lAmount5" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lTenor5" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <b>BANK STATEMENT OF FOLLOWING ACCOUNTS</b>
        <div style="border: 3px solid #000000">
            <table class="referenceWhite" style="width: 100%;">
                <tr>
                    <td style="text-align: center;" class="style71">
                        OTHER BANKS
                    </td>
                    <td style="text-align: center" class="style71">
                        ACCOUNT NUMBER
                    </td>
                    <td style="text-align: center" class="style71">
                        TYPE
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        <asp:Label ID="lOtherBank1" runat="server"></asp:Label>
                    </td>
                    <td class="style5">
                        <asp:Label ID="lAccNO1" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lType1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        <asp:Label ID="lOtherBank2" runat="server"></asp:Label>
                    </td>
                    <td class="style5">
                        <asp:Label ID="lAccNO2" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lType2" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        <asp:Label ID="lOtherBank3" runat="server"></asp:Label>
                    </td>
                    <td class="style5">
                        <asp:Label ID="lAccNO3" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lType3" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        <asp:Label ID="lOtherBank4" runat="server"></asp:Label>
                    </td>
                    <td class="style5">
                        <asp:Label ID="lAccNO4" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lType4" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        <asp:Label ID="lOtherBank5" runat="server"></asp:Label>
                    </td>
                    <td class="style5">
                        <asp:Label ID="lAccNO5" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: center" class="style76">
                        <asp:Label ID="lType5" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <span class="style7">** Asset Ops to ensure that all docs marked in &quot;Yes&quot;
            column are attached with application before loan drawdown</span><br />
        <br />
        <table style="width: 100%; border: 0px;">
            <tr>
                <td class="style50">
                    APPRAISER
                </td>
                <td>
                    SUPPORTED BY
                </td>
            </tr>
            <tr>
                <td class="style50">
                    <asp:Label ID="lAppRaiseBy" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lSupportedBy" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <div style="width: auto; height: 20px;">
            <asp:Image ID="Image2" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                Width="624px" />
        </div>
    </div>
    <div class="modal" id="requiredInfoPopupDiv" style="xtop: 125px; height: 350px; _height: 350px;
        width: 530px; *width: 530px; _width: 530px; display: none;">
        <h3>
            Required Information</h3>
        <div style="padding: 8px; padding-top: 0px;">
            <div style="width: 5.3in; height: auto; text-align: left;">
                <%--<b style="text-align: left">FOR CREDIT RELATED DOCUMENTS</b>
                <table class="reference" style="width: 100%;">
                    <tr>
                        <td class="style67">
                            UNDERWRITING LEVEL
                        </td>
                        <td>
                            <asp:DropDownList ID="txtUnderWritingLevel" runat="server" Width="98%">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>--%>
                <div style="width: 50%; float: left;">
                    <b>APPLICANT GIVEN DOCS</b>
                </div>
                <table class="reference" style="width: 100%;">
                    <tr>
                        <td class="style61">
                            Application Form filled up properly
                        </td>
                        <td style="text-align: center" class="style55">
                            <%--<asp:TextBox ID="txtApplicationForm" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtApplicationForm" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style62">
                            Credit Checklist
                        </td>
                        <td style="text-align: center">
                            <%--<asp:TextBox ID="txtCreditCheck" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtCreditCheck" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style61">
                            Photograph of Customer(s)
                        </td>
                        <td style="text-align: center" class="style55">
                            <%--<asp:TextBox ID="txtPhoto" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtPhoto" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style62">
                            Income Assesment Sheet
                        </td>
                        <td style="text-align: center">
                            <%--<asp:TextBox ID="txtIncomeAssSheet" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtIncomeAssSheet" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style61">
                            Latest TIN Certificate
                        </td>
                        <td style="text-align: center" class="style55">
                            <%--<asp:TextBox ID="txtTin" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtTin" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style62">
                            Limit Loading Instruction
                        </td>
                        <td style="text-align: center">
                            <%--<asp:TextBox ID="txtLimitLoadingIns" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtLimitLoadingIns" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style61">
                            Photo ID
                        </td>
                        <td style="text-align: center" class="style55">
                            <%--<asp:TextBox ID="txtPhotoID" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtPhotoID" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style62">
                            BASEL II Data Checklist
                        </td>
                        <td style="text-align: center">
                            <%--<asp:TextBox ID="txtBASEL2Data" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtBASEL2Data" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style61">
                            Global Consolidation
                        </td>
                        <td style="text-align: center" class="style55">
                            <%--<asp:TextBox ID="txtGlobalConso" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtGlobalConso" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style62">
                            De-Duplication
                        </td>
                        <td style="text-align: center">
                            <%--<asp:TextBox ID="txtDeDup" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtDeDup" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style61">
                            Signed IID
                        </td>
                        <td style="text-align: center" class="style55">
                            <%--<asp:TextBox ID="txtSignedIID" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtSignedIID" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style62">
                            Norkom
                        </td>
                        <td style="text-align: center">
                            <%--<asp:TextBox ID="txtNorkom" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtNorkom" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style53" colspan="2">
                            &nbsp;
                        </td>
                        <td class="style62">
                            CIB Report
                        </td>
                        <td style="text-align: center">
                            <%--<asp:TextBox ID="txtCIBReport" runat="server" Width="70px" Text="Yes"></asp:TextBox>--%>
                            <asp:DropDownList ID="txtCIBReport" runat="server" Width="70px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <%--<b>CIB REQUEST LOANS</b><table class ="reference" style="width: 100%;">
            <tr>
                <td style=" text-align:center;" class="style68">
                    SL.#</td>
                <td style=" text-align:center;" class="style72">
                    FACILITY</td>
                <td style ="text-align:center" class="style70">
                    AMOUNT</td>
                <td style="text-align: center" class="style71">
                    TENOR</td>
            </tr>
            <tr>
                <td class="style69">
                    1</td>
                <td class="style63">
                    <asp:TextBox ID="txtFacility1" runat="server" Width="98%"></asp:TextBox>
                </td>
                <td class="style65">
                    <asp:TextBox ID="txtAmount1" runat="server" Width="98%"></asp:TextBox>
                </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtToner1" runat="server" Width="98%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style69">
                    2</td>
                <td class="style63">
                    <asp:TextBox ID="txtFacility2" runat="server" Width="98%"></asp:TextBox>
                </td>
                <td class="style65">
                    <asp:TextBox ID="txtAmount2" runat="server" Width="98%"></asp:TextBox>
                </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtToner2" runat="server" Width="98%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style69">
                    3</td>
                <td class="style63">
                    <asp:TextBox ID="txtFacility3" runat="server" Width="98%"></asp:TextBox>
                </td>
                <td class="style65">
                    <asp:TextBox ID="txtAmount3" runat="server" Width="98%"></asp:TextBox>
                </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtToner3" runat="server" Width="98%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style69">
                    4</td>
                <td class="style63">
                    <asp:TextBox ID="txtFacility4" runat="server" Width="98%"></asp:TextBox>
                                    </td>
                <td class="style65">
                    <asp:TextBox ID="txtAmount4" runat="server" Width="98%"></asp:TextBox>
                                    </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtToner4" runat="server" Width="98%"></asp:TextBox>
                                    </td>
            </tr>
            <tr>
                <td class="style69">
                    5</td>
                <td class="style63">
                    <asp:TextBox ID="txtFacility5" runat="server" Width="98%"></asp:TextBox>
                                    </td>
                <td class="style65">
                    <asp:TextBox ID="txtAmount5" runat="server" Width="98%"></asp:TextBox>
                                    </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtToner5" runat="server" Width="98%"></asp:TextBox>
                                    </td>
            </tr>
            </table>--%>
            </div>
        </div>
        <div class="btns" style="vertical-align: bottom;">
            <hr style="width: 98%; left: 0px; display: block;" />
            <br />
            <input id="btnClose" type="button" value="Close" class="btnRight" style="height: 30px;
                width: 50px; font-weight: bold;" />
            <asp:Button ID="btnSave" CssClass="btnRight" runat="server" Text="Save" OnClick="btnSave_Click"
                Width="50px" Height="30px" Font-Bold="True" />
        </div>
    </div>
    </asp:Panel>
    
    <asp:Panel ID="crystalPanel" runat="server" Visible="false">
    
        <%--<CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" />--%>
        <%--<CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" />--%>
        
        <CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" />
        
    </asp:Panel>
    
    </form>
</body>
</html>
