﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.InsuranceAndBANCASettings" Title="SCB | Insurence and Banka" Codebehind="InsuranceAndBANCASettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">

    <script src="../../Scripts/AutoLoan/InsuranceAndBANCASettings.js" type="text/javascript"></script>
    
    
        <style type="text/css">
        #nav_InsuranceAndBANCASettings a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    <div class="pageHeaderDiv">Insurance & BANCA Settings</div>
    <%--All Aspx code will goes on Content div--%>
    <div id="Content">
    <%--For Pager --%>
    <div class="SearchwithPager">
    <div class="search">
    <input type="text" id="txtSearch" class="txt txtSearch widthSize35_per" onkeypress="InsuranceAndBANCASettingsHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search"/>
    </div> 
    <div id="Pager" class="pager"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
    </div>
    <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					<col width="50px" />
					<col width="200px" />
					<col width="100px" />
					<col width="70px" />
					<%--<col width="100px" />--%>
					<col width="100px" />
					<%--<col width="100px" />--%>
					<col width="150px" />
					<col width="150px" />
				</colgroup>
				<thead>
					<tr>
						<th>
							SL
						</th>
						<th>
						    Company Name
						</th>
						<th>
						    Type
						</th>
						<th>
						    Age Limit
						</th>
						<%--<th>
						    Vehicle CC
						</th>--%>
						<th>
						    Insurence %
						</th>
						<th>
						    ARTA Rate %
						</th>
						<%--<th>
						    Tenor %
						</th>--%>
						<th> <a id="lnkAddInsuranceBanka" class="iconlink iconlinkAdd" href="javascript:;">New Insurance & BANCA </a>
						</th>
					</tr>
				</thead>
				<tbody id="insurenceList">
				</tbody>
			</table>
    
    </div>
    
    <div class="modal" id="insurencePopupDiv" style="display:none; height: 300px;">
		<h3>Insurence/Banca Details</h3>
		<label for="txtpostedDate">Company Name*</label> <input class="txt" id="txtCompanyName" title="Company Name"/><br/>
		<label for="txtHeadline">Insurance Type*</label>
		<select id="cmbInsurenceType" class="txt" title="Insurance Type">
		    <option value="1" selected="selected">General</option>
		    <option value="2">Life</option>
		    <option value="3">General & Life</option>
		</select>
		 <br/>
		 <label for="txtAgeLimit">Age limit</label> <input class="txt" id="txtAgeLimit" title="Age limit"/><br/>
		 <%--<label for="txtVehicleCC">Vehicle CC</label> <input class="txt" id="txtVehicleCC" title="Vehicle CC"/><br/>--%>
		 <label for="txtInsurancePer">Insurance %</label> <input class="txt" id="txtInsurancePer" title="Insurance %"/><br/>
		 <label for="txtArtaRate">Arta Rate %</label> <input class="txt" id="txtArtaRate" title="Arta Rate %"/><br/>
		 <%--<label for="txtTenor">Tenor %</label> <input class="txt" id="txtTenor" title="Tenor %"/><br/>--%>
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
    
</asp:Content>

