﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class VendorNegativeListing : System.Web.UI.Page
    {

        [AjaxNamespace("VendorNegativeListing")]
        protected void Page_Load(object sender, EventArgs e)
        {

            Utility.RegisterTypeForAjax(typeof(VendorNegativeListing));
        }

        
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVendorNegativeListing> GetAutoVendorNegativeList(int pageNo, int pageSize, string search)
        {
            pageNo = pageNo * pageSize;

            IVendorNegativeListingRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var vendorNegativeListingService = new VendorNegativeListingService(repository);

            
            List<AutoVendorNegativeListing> objVendorNegativeList =
                vendorNegativeListingService.GetAutoVendorNegativeList(pageNo, pageSize, search);
            return objVendorNegativeList;
        }


         [AjaxMethod(HttpSessionStateRequirement.Read)]
            public string VendorNegativeById(int vendornegativeID)
            {
                //var userID = Convert.ToInt32(Session["Id"].ToString());
                
                 IVendorNegativeListingRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var vendorNegativeListingService = new VendorNegativeListingService(repository);


                var res = vendorNegativeListingService.VendorNegativeById(vendornegativeID);
                return res;



            }





         [AjaxMethod(HttpSessionStateRequirement.Read)]
         public List<AutoVendor> GetAutoVendor()
         {
             IAutoVendorRepository repository = new AutoLoanDataService.AutoLoanDataService();
             var autoVendorService = new AutoVendorService(repository);


             List<AutoVendor> objAutoManufacturer =
                 autoVendorService.GetAutoVendor();
             return objAutoManufacturer;


         }



        //SaveAuto_NegativeVendor(negativeVen)

         [AjaxMethod(HttpSessionStateRequirement.Read)]
         public string SaveAuto_NegativeVendor(int manufacturerId)
         {
             //var userID = Convert.ToInt32(Session["Id"].ToString());
             IVendorNegativeListingRepository repository = new AutoLoanDataService.AutoLoanDataService();
             var autoLoanDataService = new VendorNegativeListingService(repository);
             //tenor.UserId = userID;

             var res = autoLoanDataService.SaveAuto_NegativeVendor(manufacturerId);
             return res;

         }

         [AjaxMethod(HttpSessionStateRequirement.Read)]
         public string SaveAuto_NegativeVendorNew(AutoVendorMOU autoVendorMOU)
         {
             AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
             var autoVendorMOUService = new AutoVendorMOUService(repository);
             var res = autoVendorMOUService.SaveAuto_NegativeVendorNew(autoVendorMOU);
             return res;
         }
         [AjaxMethod(HttpSessionStateRequirement.Read)]
         public List<AutoStatus> GetStatus(int statusID)
         {
             AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
             var autoVendorMOUService = new AutoVendorMOUService(repository);


             List<AutoStatus> objList_AutoStatus =
                 autoVendorMOUService.GetStatus(statusID);
             return objList_AutoStatus;
         }
        
    }
}
