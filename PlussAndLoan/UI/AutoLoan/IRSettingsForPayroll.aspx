<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.IRSettingsForPayroll" Title="IRSettings For Payroll" Codebehind="IRSettingsForPayroll.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <script src="../../Scripts/AutoLoan/IRSettingsForPayroll.js" type="text/javascript"></script>
    
    <div class="pageHeaderDiv">IRSettings For Payroll</div>
    
    <div id="Content">
    <%--For Pager --%>
    <div class="SearchwithPager">
    <div class="search searchwithUpload">
    <input type="text" id="txtSearch" class="txt txtSearch widthSize25_per" onkeypress="iRSettingsforPayrollHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search" onclick="iRSettingsforPayrollManager.render();"/>
<%--    <input type="button" id="btnUploadDoc" class="searchButton" value="Upload"/>
--%>    <%--<form id="frmUploader" class="uploaderForm" method="post" enctype="multipart/form-data" action="IRSettingsForPayroll.aspx">
    <input id="fileIRSetiingsForPayroll" class="txt txtUpload" type="file" title="IRSetiings For Payroll xls document" name="fileIRSetiingsForPayroll">
    <input type="button" id="btnUpload" class="searchButton" value="Upload"/>
    </form>--%>
        <style type="text/css">
        #nav_IRSettingsForPayroll a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
		
		<asp:FileUpload ID="fileDocumentUpload" class="txt txtUpload" runat="server" />
		
		<span class="btnUpload"><asp:Button ID="btnUploadSave" runat="server" Text="Upload" 
             onclick="btnUploadSave_Click" UseSubmitBehavior="False" /></span>
        

    </div> 
    <div id="Pager" class="pager floatRight"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
    </div>
    <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					<col width="50px" />
					<col width="300px" />
					<col width="100px" />
					<col width="100px" />
					<col width="150px" />
					<col width="150px" />
				</colgroup>
				<thead>
					<tr>
						<th>
							SL
						</th>
						<th>
						    Company Name
						</th>
						<th>
						    Category
						</th>
						<th>
						    Interest Rate
						</th>
						<th>
						    Processing Fee
						</th>
						<th> <a id="lnkAddNewCompany" class="iconlink iconlinkAdd" href="javascript:;">New Company </a>
						</th>
					</tr>
				</thead>
				<tbody id="companyListList">
				</tbody>
			</table>
    
    </div>
    
    <div class="modal" id="companyPopupDiv" style="display:none; *height:260px;">
		<h3>Payroll Rate Details</h3>
		<label for="txtpostedDate">Company Name*</label> <input class="txt" id="txtCompanyName" title="Company Name"/><br/>
		<label for="txtCompanyCode">Company Code*</label> <input class="txt" id="txtCompanyCode" title="Company Code" maxlength="3" /><br/>
		<label for="txtHeadline">Category*</label>
		<select id="cmbCategory" class="txt" title="Category">
		    <option value="-1">Select From List</option>
		    <option value="A">Cat A</option>
		    <option value="B">Cat B</option>
		    <option value="C">No Cat</option>
		</select>
		 <br/>
		 <label for="txtInterestRate">Interest Rate*</label> <input class="txt" id="txtInterestRate" title="Interest Rate"/><br/>
		 <label for="txtProcessingFee">Processing Fee</label> <input class="txt" id="txtProcessingFee" title="Processing Fee"/><br/>
		 <label for="tarearemarks">Remarks</label> <input class="tareaForAutoloan" id="tareaRemarks" title="Remarks"/><br/>
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
    
</asp:Content>

