﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoSupport.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.AutoLoanApplicationCISupport"
    Title="AutoLoan Application CI Support" Codebehind="AutoLoanApplicationCISupport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    
    <link href="../../CSS/Default.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/admin.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/TableLayoutForTabControls.css" rel="stylesheet" type="text/css" />

    <script src="../../Scripts/AutoLoan/LabelChange.js" type="text/javascript"></script>

    <script src="../../Scripts/AutoLoan/AutoLoanApplicationCISupport.js" type="text/javascript"></script>


<script>

//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateTermLoan1', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });



//    
//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateTermLoan2', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });
//    
//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateTermLoan3', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });
//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateTermLoan4', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });
//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateCreditCard1', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });
//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateCreditCard2', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });
//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateOverDraft1', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });
//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateOverDraft2', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });
//    
//    Event.observe(window, 'load', function() {
//    Calendar.setup({ inputField: 'txtSecurityIssuingDateOverDraft3', ifFormat: '%d/%m/%Y',
//            weekNumbers: false, button: 'newsDate'
//        });
//    });
        
    
</script>




    <style type="text/css">
        #nav_AutoLoanApplicationCISupport a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>


    <div id="divAutoloanSummary">
    
    <div class="pageHeaderDiv">Auto Loan Summary</div>

             <div id="div2" class="fieldDiv divPadding formBackColor">
                <div class="fieldDiv">
                    <label class="lbl rightAlign">
                        LLID<span class="starColor">*</span>:</label>
                        <input type="text" id="txtLLIDMain" class="txtField widthSize25_per" onkeypress="analyzeLoanApplicationHelper.searchKeypress(event)"/>
                        <input type="button" id="btnLLIdSearch" class="" value="Search"/>
                </div>
                
            </div>
         
             <div class="divLeftNormalClass widthSize97p9_per">
             
           <table  cellpadding="0" cellspacing="0" frame="box" border="0" class="summary centerAlign widthSize100_per">
				        <colgroup class="sortable ">
					        <col width="80px" />
					        <col width="150px" />
					        <col width="120px" />
					        <col width="90px" />
					        <col width="100px" />
					        <col width="110px" />
					        <col width="90px" />
					        <col width="110px" />
					        <col width="110px" />
        					
				        </colgroup>
				        <thead >
					        <tr class="theadTdTextAlignCenter">
						        <th>
							        LLID
						        </th>
						        <th>
						            CUSTOMER
						        </th>
						        <th>
						            PRODUCT
						        </th>
        						<th>
						            BRAND
						        </th>
						        <th>
						            MODEL
						        </th>
						        <th>
						            LOAN AMT
						        </th>
						        <th>
						            TENOR
						        </th>
						        <th>
						            STATUS
						        </th>				
						        <th>
						            ACTION 
						        </th>
					        </tr>
				        </thead>
				        <tbody id="GridLoanSummery">
				        </tbody>
			        </table>
			        <div class="clear"></div>
			<div id="Pager" class="pager pagerFooter" > 
                            <label for="txtPageNO" class="pagerlabel">page</label>                                           
                            <input id="txtPageNo" class="txtPageNo" value="1" maxlength="7"></input>
                            <span id="spntotalPage" class="spntotalPage">of 1</span> 
                            <a id="lnkPrev" title="Previous Page" href="#" class="prevPage">prev</a> 
                            <a id="lnkNext" title="Next Page" href="#" class="nextPage">next</a>
                    </div>
            <div class="clear"></div>
            
            
            
        </div>    
            
            
                    </div>
















<div id="LoanAppUI" style="display:none;">

    <div style="width: 100%; *width: 100%; _width: 100%; border: solid 1px black; margin-bottom: 5px;">
        <div id="divLoanAppMasterData" class="fieldDiv divPadding formBackColor" style="margin-bottom: 0;">
            <div class="fieldDiv">
                <label class="lblBig" style="font-size: medium; font-weight: bold; width: 160px;
                    _width: 160px; *width: 160px;">
                    CUSTOMER DETAILS</label>
                <label class="lblBig" style="font-size: medium; font-weight: bold; text-align: right;">
                    LLID:</label>
                <input type="text" id="txtLLID" class="txtField" disabled="disabled" />
            </div>
        </div>
    </div>
    
        <input id="txtAutoLoanMasterId" type="hidden" value="0"/>
        <input id="txtIsJoint" type="hidden" value="0" />
        <input id="txtAnalystMasterID" type="hidden" value="0" />
        <input id="txtloanAnalystApplicationID" type="hidden" value="0" />
        <input id="txtVehicleDetailID" type="hidden" value="0" />
        <input id="txtAutoLoanVehicleId" type="hidden" value="0" />
        <input id="txtApplicantSCBAccountID" type="hidden" value="0" />
        <input id="txtPRAccount_detailsID" type="hidden" value="0" />
        <input id="txtLoanFacilityID" type="hidden" value="0" />
        <input id="txtLoanSecurityID" type="hidden" value="0" />
        <input id="txtLoanFacilityOFFSCBID" type="hidden" value="0" />
    
    
    <div class="clear">
    </div>
    <div id="divLeft" class="divLeftBig" style="margin-left: 5px;">
        <div class="fieldDiv">
            <label class="lbl">
                Primary Applicant:</label>
            <input id="txtPrimaryApplicant" type="text" class="txtFieldBig" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Date of Birth:</label>
            <input id="txtDateOfBirth" type="text" class="txtFieldBig" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Current Age:</label>
            <input id="txtCurrenAge" type="text" class="txtFieldBig" disabled="disabled" /><br />
            <label id="lblFieldrequiredforPrAge" style="color:red; font-weigth:bold;"></label>
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Primary Profession:</label>
            <input id="txtPrimaryProfession" type="text" class="txtFieldBig" disabled="disabled" />
            <input id="txtPrProfessionId" type="hidden" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Other Profession:</label>
            <input id="txtOtherProfession" type="text" class="txtFieldBig" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Declared Income:</label>
            <input id="txtDeclaredIncome" type="text" class="txtFieldBig" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lbl" style="height: 50px; *height: 50px; _height: 50px; text-align: left;
                vertical-align: top;">
                Verified Address:</label>
            <textarea id="txtVerifiedAddress" class="txtFieldBig" cols="1" rows="3" style="height: 58px;
                _height: 58px; *height: 58px;" disabled="disabled"></textarea>
        </div>
        <div class="clear">
        </div>
    </div>
    <div id="divRightForJointMain" class="divRightBig" style="margin-left: 5px;">
        <div class="fieldDiv">
            <label class="lbl">
                Joint Applicant:</label>
            <input id="txtJointApplicant" type="text" class="txtFieldBig" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Date of Birth:</label>
            <input id="txtJtDateOfBirth" type="text" class="txtFieldBig" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Current Age:</label>
            <input id="txtJtCurrntAge" type="text" class="txtFieldBig" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Profession:</label>
            <input id="txtJtProfession" type="text" class="txtFieldBig" disabled="disabled" />
            <input id="txtJtProfessionId" type="hidden" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Declared Income:</label>
            <input id="txtJtDeclaredIncome" type="text" class="txtFieldBig" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lbl">
                Reason of Joint Applicant:</label>
            <select id="cmbReasonofJointApplicant" class="txtFieldBig comboSizeActive">
                <option value="-1">Select a reason</option>
                <option value="1">Income Consideration</option>
                <option value="2">Joint Registration</option>
                <option value="3">Business is in Joint Name</option>
                <option value="4">Customer is Mariner</option>
                <option value="5">AC is in Joint Name</option>
                <option value="6">Others</option>
            </select>
        </div>
        <div class="clear">
        </div>
        <!-- -----------------------------Tab Segment ----------------------------------------------------------------------------- -->
    </div>
    <div class="content" id="tabGeneral" style="padding-left: 0px;">
        <!--Application Details start -->
        <div id="mainDiv" style="width: 49%; margin-left: 2px; float: left; position: relative;">
            <div class="divHeadLine">
                <span>Application Detail</span></div>
            <div class="groupContainer" style="padding-left: 0px;">
                <div id="div1" class="divLeft" style="width: 170px; border:0px;">
                    <div class="divLbl">
                        <label class="lblSize">
                            Source (1):</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Branch Name:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Product Name (2):</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Value Pack:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Borrowing Relationship:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Customer Status:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Asking Repayment Method:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            ARTA:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            ARTA Preference:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            General Insurance:</label></div>
                            <div class="divLbl">
                        <label class="lblSize">
                            General Insurance Preference</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            100% Cash Secured:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            CC Bundle:</label></div>
                             <div id="divlblSequrityValue" class="divLbl" style="display:none;">
                        <label class="lblSize">
                            Security Value:</label></div>
                </div>
                <div id="JApplicantControldiv" class="divRight" style="border:0px;">
                    <div class="fieldDivContaint">
                        <select id="cmbSource1" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                        *height: 25px;">
                        <div style="width: 100%;">
                            <div class="divLeft" style="width: 60%; *width: 60%; _width: 60%; padding: 0px; border: none;">
                                <select id="cmbBranch" class="comboSize comboSizeActive">
                                    <option value="-1">Select a source</option>
                                </select></div>
                            <div class="divMiddile" style="width: 36%; _width: 36%; *_width: 34%; padding: 0px;
                                border: none;">
                                <input id="txtBranchCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                        </div>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbProductName" class="comboSize" disabled="disabled">
                            <option value="-1">Select a Status</option>
                            <option value="1">Staff Auto</option>
                            <option value="2">Conventional Auto</option>
                            <option value="3">Saadiq Auto</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbValuePack" class="comboSize" disabled="disabled">
                            <option value="-1">Select a Status</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                        *height: 25px;">
                        <div style="width: 100%;">
                            <div class="divLeft" style="width: 60%; *width: 60%; _width: 60%; padding: 0px; border: none;">
                                <select id="cmbBrowingRelationship" class="comboSize comboSizeActive">
                                    <option value="-1">Select a source</option>
                                    <option value="FTMB">First Time Borrower</option>
                                    <option value="NFTB">NON FTB</option>
                                    <option value="TMOB">12 MOB</option>
                                    <option value="TFMB">24 MOB</option>
                                </select></div>
                            <div class="divMiddile" style="width: 36%; *width: 36%; *_width: 34%; padding: 0px;
                                border: none;">
                                <input id="txtRelationshipCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                        </div>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbCustomerStatus" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <option value="1">Regular</option>
                            <option value="2">Preferred</option>
                            <option value="3">Priority</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbAskingRepaymentmethod" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <option value="1">PDC</option>
                            <option value="2">SI</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbARTA" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                            <option value="3">Cash</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbARTAReference" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <%--<option value="1">Alico</option>
                            <option value="2">Progoti Insurance</option>--%>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbGeneralInsurence" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                            <option value="3">Cash</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbGeneralInsurencePreference" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <%--<option value="1">Alico</option>
                            <option value="2">Progoti Insurance</option>--%>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbCashSecured" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbCCBundle" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint" id="divSequrityValueTextbox" style="display:none;">
                        <input type="text" id="txtSequrityValue" class="txtBoxSize comboSizeActive" />
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <br />
            <br />
        </div>
        <!--End of Application Detailsd -->
        <!--Vehicle Details start -->
        <div id="Div10" style="width: 47%; _width: 47%; *width: 47%; float: right;">
            <div class="divHeadLine">
                <span>Vehicle Details</span></div>
            <div class="groupContainer">
                <div id="div12" class="divLeft" style="width: 125px; _width: 125px; *width: 125px; border:0px;">
                    <div class="divLbl">
                        <label class="lblSize">
                            Vendor:</label></div>
                             <div class="divLbl">
                        <label class="lblSize">
                            New Vendor:</label></div>
                            <div class="divLbl">
                        <label class="lblSize">
                            Manufacturer/Brand:</label></div>
                            <div class="divLbl">
                        <label class="lblSize">
                            Vehicle Model:</label></div>
                            <div class="divLbl">
                        <label class="lblSize">
                            Trim Lebel:</label></div>
                            <div class="divLbl">
                        <label class="lblSize">
                            Engine Capacity:</label></div>
                            <div class="divLbl">
                        <label class="lblSize">
                            Vehicle Type:</label></div>
                          <div class="divLbl">
                        <label class="lblSize">
                            Vehicle Status:</label></div>  
                            <div class="divLbl">
                        <label class="lblSize">
                            Manufacturing Year:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Registration Year:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Value Pack Allowed:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Seating Capacity:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            MOU Vendor (3):</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Quoted Price:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Considered Price:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Price Considered on:</label></div>
                    <div class="divLbl">
                        <label class="lblSize">
                            Car Verification:</label></div>
                    <div class="divLbl" id="divLblDeliveryStatus">
                        <label class="lblSize">
                            Delivery Status:</label></div>
                </div>
                
                
                <div id="Div15" class="divRight" style="width: 235px; _width: 235px; *width: 235px; border:0px;">
                    <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                        *height: 25px;">
                        <div style="width: 100%;">
                            <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                <select id="cmbVendor" class="comboSize comboSizeActive">
                                    <option value="0">Select a Vendore</option>
                                </select>
                            </div>
                            <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                border: none;">
                                <input id="txtVendoreCode" type="text" class="txtBoxSize" disabled="disabled" />
                                </div>
                        </div>
                    </div>
                    <div class="fieldDivContaint">
                        <input id="txtVendorName" type ="text" class="txtBoxSize"/>
                    </div>
                    <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                        *height: 25px;">
                        <div style="width: 100%;">
                            <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                <select id="cmbManufacturer" class="comboSize comboSizeActive">
                                    <option value="0">Select a manufacturer</option>
                                </select>
                            </div>
                            <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                border: none;">
                                <input id="txtManufacturarCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                        </div>
                    </div>
                    <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                        *height: 25px;">
                        <div style="width: 100%;">
                            <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                <select id="cmbModel" class="comboSize comboSizeActive" onchange="analyzeLoanApplicationHelper.changeModel()" >
                                <option value="0">Select Model</option>
                                </select>  
                                <input type="text" id="txtModelName" class="txtBoxSize comboSizeActive" style="display:none;" />
                                <input id="hdnModel" type="hidden" /> 
                            </div>
                            <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                border: none;">
                                <input id="txtVehicleModelCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                        </div>
                    </div>
                     <div class="fieldDivContaint">
                        <input type="text" id="txtTrimLevel" class="txtBoxSize" disabled="disabled" />                                                                         
                    </div>
                    <div class="fieldDivContaint">
                        <input id="txtEngineCapacityCC" type="text" class="txtBoxSize" disabled="disabled" />
                    </div>
                    <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                        *height: 25px;">
                        <div style="width: 100%;">
                            <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                <input type="text" id="txtVehicleType" class="txtBoxSize" disabled="disabled" />
                                <select id="cmbVehicleType" class="comboSize comboSizeActive" title="Vehicle Type" style="display:none;">
		                            <option value="-1">Select From List</option>
		                            <option value="S1">Sedan</option>
		                            <option value="S2">Station wagon</option>
		                            <option value="S3">SUV</option>
		                            <option value="S4">Pick-Ups</option>
		                            <option value="S5">Microbus</option>
		                            <option value="S6">MPV</option>
		                        </select>
                            </div>
                            <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                border: none;">
                                <input id="txtVehicletypeCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                        </div>
                    </div>
                    <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                        *height: 25px;">
                        <div style="width: 100%;">
                            <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                <select id="cmdVehicleStatus" class="comboSize comboSizeActive">
                                    <option value="0">Select Vehicle Status</option>
                                </select>
                            </div>
                            <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                border: none;">
                                <input id="txtVehicleStatusCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                        </div>
                    </div>
                    <div class="fieldDivContaint">
                        <input type="text" id="txtManufacturaryear" class="txtBoxSize comboSizeActive" />
                    </div>
                    <div class="fieldDivContaint">
                        <input type="text" id="txtregistrationYear" class="txtBoxSize" disabled="disabled" />
                    </div> 
                    <div class="fieldDivContaint">
                        <select id="cmbvaluePackAllowed" class="comboSize" disabled="disabled">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    
                    <div class="fieldDivContaint">
                        <select id="cmsSeatingCapacity" class="comboSize comboSizeActive">
                            <option value="-1">Select Seating Capacity</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                        </select>
                    </div>
                    
                    
                    <div class="fieldDivContaint">
                        <select id="cmbVendoreRelationShip" class="comboSize" disabled="disabled">
                            <option value="1">MOU</option>
                            <option value="0">NON-MOU</option>
                            <option value="2">Negative Listed</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <input type="text" id="txtQuotedPrice" class="txtBoxSize comboSizeActive" />
                    </div>
                    <div class="fieldDivContaint">
                        <input type="text" id="txtConsideredprice" class="txtBoxSize comboSizeActive" />
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbPriceConsideredOn" class="comboSize comboSizeActive">
                            <option value="-1">Select a Price Considered</option>
                            <option value="1">Index Value</option>
                            <option value="2">Phone</option>
                            <option value="3">Price Verify</option>
                            <option value="2">Quotation</option>
                            <option value="3">NA</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint">
                        <select id="cmbCarVerification" class="comboSize comboSizeActive">
                            <option value="0">Select Verification Type</option>
                            <option value="1">Done</option>
                            <option value="2">Valued & Verified</option>
                            <option value="3">Not Required</option>
                        </select>
                    </div>
                    <div class="fieldDivContaint" id="divDeliveryStatus" style="display: none;">
                        <select id="cmbDeliveryStatus" class="comboSize comboSizeActive">
                            <option value="-1">Select a Status</option>
                            <option value="1">Car Delivered</option>
                            <option value="2">Car At Port</option>
                            <option value="3">Car At Vendor</option>
                        </select>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <br />
            <br />
        </div>
        <!--End of Vehicle Details -->
        <div class="clear">
        </div>
        <br />
        <!--Account Relationship start -->
        <div id="Div16" style="width: 100%; _width: 100%; *width: 100%; float: left;">
            <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%;">
                <span>Account Relationship</span></div>
                
                
            <div class="groupContainer" style="width: 98%; *width: 98%; _width: 98%;">
                <div class="divLeftBig" style="width: 315px; *width: 315px; _width: 315px;">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <div style="text-align: left; font-size: medium; font-weight: bold;">
                                    WITH SCB
                                    <div style="border: none;">
                                        <table class="tableStyle" id="tblApplicantAccountSCB">
                                            <tr class="theader">
                                                <td style="width: 35%;">
                                                    A/C NUMBER(S)
                                                </td>
                                                <td style="width: 35%;">
                                                    ACCOUNT STATUS
                                                </td>
                                                <td style="width: 30%;">
                                                    PRITORY A/C HOLDER
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="txtAccountNumberForSCB1" class="comboSizeActive" type="text" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatusForSCB1" class="comboSizeActive">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbPriorityAcHolderForSCB1" class="comboSizeActive">
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="txtAccountNumberForSCB2" type="text" class="comboSizeActive" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatusForSCB2" class="comboSizeActive">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbPriorityAcHolderForSCB2" class="comboSizeActive">
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="txtAccountNumberForSCB3" type="text" class="comboSizeActive" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatusForSCB3" class="comboSizeActive">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbPriorityAcHolderForSCB3" class="comboSizeActive">
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="txtAccountNumberForSCB4" type="text" class="comboSizeActive" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatusForSCB4" class="comboSizeActive">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbPriorityAcHolderForSCB4" class="comboSizeActive">
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="txtAccountNumberForSCB5" type="text" class="comboSizeActive" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatusForSCB5" class="comboSizeActive">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbPriorityAcHolderForSCB5" class="comboSizeActive">
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="divRightBig" style="width: 500px; *width: 500px; _width: 500px;">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <div style="text-align: left; font-size: medium; font-weight: bold;">
                                    OTHER BANK
                                    <div style="border: none;">
                                        <table class="tableStyle" id="tblApplicantAccountOther">
                                            <tr class="theader">
                                                <td>
                                                    BANK
                                                </td>
                                                <td>
                                                    BRANCH
                                                </td>
                                                <td>
                                                    A/C NUMBER(S)
                                                </td>
                                                <td>
                                                    A/C STATUS
                                                </td>
                                                <td>
                                                    A/C TYPE
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbBankNameForOther1" disabled="disabled" onchange="analyzeLoanApplicationManager.GetBranch('cmbBankNameForOther1')">
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbBranchNameOther1" disabled="disabled">
                                                        <option value="-1">Select a Branch</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="txtAccountNumberother1" type="text" disabled="disabled" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatus1" disabled="disabled">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbAccountType1" disabled="disabled">
                                                        <option value="0">Select from List</option>
                                                        <option value="1">Current</option>
                                                        <option value="2">Savings</option>
                                                        <option value="3">Salary</option>
                                                        <option value="4">Others</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbBankNameForOther2" onchange="analyzeLoanApplicationManager.GetBranch('cmbBankNameForOther2')" disabled="disabled">
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbBranchNameOther2" disabled="disabled">
                                                        <option value="-1">Select a Branch</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="txtAccountNumberother2" type="text" disabled="disabled" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatus2" disabled="disabled">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbAccountType2" disabled="disabled">
                                                        <option value="0">Select from List</option>
                                                        <option value="1">Current</option>
                                                        <option value="2">Savings</option>
                                                        <option value="3">Salary</option>
                                                        <option value="4">Others</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbBankNameForOther3" onchange="analyzeLoanApplicationManager.GetBranch('cmbBankNameForOther3')" disabled="disabled">
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbBranchNameOther3" disabled="disabled">
                                                        <option value="-1">Select a Branch</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="txtAccountNumberother3" type="text" disabled="disabled" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatus3" disabled="disabled">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbAccountType3" disabled="disabled">
                                                        <option value="0">Select from List</option>
                                                        <option value="1">Current</option>
                                                        <option value="2">Savings</option>
                                                        <option value="3">Salary</option>
                                                        <option value="4">Others</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbBankNameForOther4" onchange="analyzeLoanApplicationManager.GetBranch('cmbBankNameForOther4')" disabled="disabled">
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbBranchNameOther4" disabled="disabled">
                                                        <option value="-1">Select a Branch</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="txtAccountNumberother4" type="text" disabled="disabled" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatus4" disabled="disabled">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbAccountType4" disabled="disabled">
                                                        <option value="0">Select from List</option>
                                                        <option value="1">Current</option>
                                                        <option value="2">Savings</option>
                                                        <option value="3">Salary</option>
                                                        <option value="4">Others</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbBankNameForOther5" onchange="analyzeLoanApplicationManager.GetBranch('cmbBankNameForOther5')" disabled="disabled">
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbBranchNameOther5" disabled="disabled">
                                                        <option value="-1">Select a Branch</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="txtAccountNumberother5" type="text" disabled="disabled" />
                                                </td>
                                                <td>
                                                    <select id="cmbAccountStatus5" disabled="disabled">
                                                        <option value="0">Select Status</option>
                                                        <option value="1">Company</option>
                                                        <option value="2">Personal</option>
                                                        <option value="3">Joint</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="cmbAccountType5" disabled="disabled">
                                                        <option value="0">Select from List</option>
                                                        <option value="1">Current</option>
                                                        <option value="2">Savings</option>
                                                        <option value="3">Salary</option>
                                                        <option value="4">Others</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clear">
                </div>
                <div class="clear">
                </div>
            </div>
            <br />
            <br />
        </div>
        <!--End of Account Relationship -->
        <div class="clear">
        </div>
        <!--Exposures start -->
        <div id="Div17" style="width: 100%; _width: 100%; *width: 100%; float: left;">
            <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%;">
                <span>Exposures</span></div>
                
                
                
            <div class="groupContainer" style="width: 98%; *width: 98%; _width: 98%;">
                <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%; background-color: white;
                    text-align: left; border: none;">
                    <span style="color: black;">ON SCB</span></div>
                <div class="divLeftBig" style="width: 98%;">
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        TERM
                        <label id="lblLoanterms">
                            LOANS</label>
                        <div style="border: none; width: 98%; overflow-x:none;overflow-y:auto;">
                            <table class="tableStyleSmall" id="tblFacilityTermLoan" style="width:140%;">
                                <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                    <td style="width: 10%;">
                                        TYPE
                                    </td>
                                    <td style="width: 10%;">
                                        FLAG
                                    </td>
                                    <td style="width: 10%;">
                                        RATE
                                    </td>
                                    <td style="width: 10%;">
                                        LIMIT
                                    </td>
                                    <td style="width: 10%;">
                                        OUTSTANDING
                                    </td>
                                    <td id="tdEM1" style="width: 10%;">
                                        EMI
                                    </td>
                                    <td id="tdEM2" style="width: 10%;">
                                        EMI %
                                    </td>
                                    <td id="tdEM3" style="width: 10%;">
                                        EMI SHARE
                                    </td>
                                    <td style="width: 10%;">
                                        STATUS
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY TYPE
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY FV
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt1" style="width: 10%;">
                                        LTV
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans1" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTypeForExposer('cmbTypeForTermLoans1')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate1" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit1" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.calculateEMIShare('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.calculateEMIShare('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities1" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV1" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity1" type="text" style="width:40px;" disabled="disabled" />
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans2" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTypeForExposer('cmbTypeForTermLoans2')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate2" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit2" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding2" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateEMIShare('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateEMIShare('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities2" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity2" type="text" style="width:40px;" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans3" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTypeForExposer('cmbTypeForTermLoans3')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag3" type="text" disabled="disabled"  />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate3" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding3" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI3" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateEMIShare('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer3" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateEMIShare('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities3" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities3" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV3" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('3')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity3" style="width:40px;" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans4" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTypeForExposer('cmbTypeForTermLoans4')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate4" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtLimit4" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding4" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI4" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateEMIShare('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer4" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateEMIShare('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities4" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities4" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV4" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV4" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('4')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity4" type="text" style="width:40px;" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divColleterization">
									<label class="lblColleterization">COLLETERIZATION</label>
									<select id="cmbColleterization" class="comboSize widthSize22_per comboSizeActive">
									</select>
									<label class="lblColleterizationWith">With</label>
									<select id="cmbColleterizationWith" class="comboSize widthSize22_per comboSizeActive"></select>
								</div>
                        <div style="text-align: center; font-size: medium; font-weight: bold;">
                            CREDIT CARD
                            <div style="border: none;">
                                <table class="tableStyleSmall" id="tblFacilityCreditCard">
                                    <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                        <td style="width: 14%;">
                                            TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            FLAG
                                        </td>
                                        <td style="width: 11%;">
                                            ORIGINAL LIMIT
                                        </td>
                                        <%--<td style="width: 20%;" id="tdInterestRateCreditCard">
                                            INTEREST RATE
                                        </td>--%>
                                        <td style="width: 10%;" id="tdinterest">
                                            INTEREST
                                        </td>
                                        <td style="width: 12%;">
                                            STATUS
                                        </td>
                                        <td style="width: 15%;">
                                            SECURITY TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY FV
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY PV
                                        </td>
                                        <td id="tdlt2" style="width: 12%;">
                                            LTV
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="cmbTypeForCreditCard1" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTypeForExposer('cmbTypeForCreditCard1')">
                                            </select>
                                        </td>
                                        <td>
                                            <input id="txtFlagForcreditCard1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtOriginalLimitForCreditCard1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForCreditCardSequrity('1')" />
                                        </td>
                                        <%--<td>
                                            <input id="txtInterestRateCreditCard1" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForCreditCardSequrity('1')" />
                                        </td>--%>
                                        <td>
                                            <input id="txtInterestForCreditCard1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard1" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForcreditCard1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard1" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForCreditCardSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard1" type="text" disabled="disabled" />
                                    </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="cmbTypeForCreditCard2" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTypeForExposer('cmbTypeForCreditCard2')">
                                            </select>
                                        </td>
                                        <td>
                                            <input id="txtFlagForcreditCard2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtOriginalLimitForCreditCard2" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForCreditCardSequrity('2')" />
                                        </td>
                                        <%--<td>
                                            <input id="txtInterestRateCreditCard2" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForCreditCardSequrity('2')" />
                                        </td>--%>
                                        <td>
                                            <input id="txtInterestForCreditCard2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard2" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForcreditCard2" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForCreditCardSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard2" type="text" disabled="disabled" />
                                    </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="text-align: center; font-size: medium; font-weight: bold;">
                            OVERDRAFT
                            <div style="border: none;">
                                <table class="tableStyleSmall" id="tblFacilityOverDraft">
                                    <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                        <td style="width: 10%;">
                                            ORIGINAL LIMIT
                                        </td>
                                        <td style="width: 10%;">
                                            FLAG
                                        </td>
                                        <td style="width: 10%;" id="tdinteresRate">
                                            INTEREST RATE
                                        </td>
                                        <td style="width: 10%;" id="tdinteres1">
                                            INTEREST
                                        </td>
                                        <td style="width: 15%;" >
                                            STATUS
                                        </td>
                                        <td style="width: 15%;">
                                            SECURITY TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY FV
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY PV
                                        </td>
                                        <td id="tdlt3" style="width: 10%;">
                                            LTV
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeoverDraftInterest('1')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag1" class="comboSizeActive" type="text" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeoverDraftInterest('1')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft1" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft1" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForOverDraftSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft1" type="text" disabled="disabled" />
                                    </td>
                                    
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeoverDraftInterest('2')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag2" type="text" class="comboSizeActive" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeoverDraftInterest('2')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft2" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.calculateLTVForOverDraftSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft2" type="text" disabled="disabled" />
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit3" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeoverDraftInterest('3')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag3" type="text" class="comboSizeActive" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest3" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeoverDraftInterest('3')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres3" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft3" class="comboSizeActive">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft3" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft3" type="text" disabled="disabled" />
                                    </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div style="height:20px;"></div>
                <%--<div class="divLeftBig" style="width: 98%; *width: 98%; _width: 98%;">
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        SECURITIES
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOnSCBSecurityTermLoan">
                                <tr class="theader">
                                    <td>
                                        STATUS
                                    </td>
                                    <td>
                                        TYPE
                                    </td>
                                    <td>
                                        ISSUING OFFICE
                                    </td>
                                    <td>
                                        ISSUING DATE
                                    </td>
                                    <td>
                                        SECURITY FV
                                    </td>
                                    <td>
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt1">
                                        LTV
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecurities1">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities1">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeTermLoan1"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateTermLoan1"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV1" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV1" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity1" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecurities2">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities2">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeTermLoan2"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateTermLoan2"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV2" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV2" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity2" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecurities3">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities3">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeTermLoan3"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateTermLoan3"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV3" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV3" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('3')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity3" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecurities4">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities4">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeTermLoan4"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateTermLoan4"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV4" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV4" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForSequrity('4')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity4" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        <div style="border: none;">
                            <table class="tableStyleSmall" style="margin-top: 18px;" id="tblOnSCBSecurityCreditCard">
                                <tr class="theader">
                                    <td>
                                        STATUS
                                    </td>
                                    <td>
                                        TYPE
                                    </td>
                                    <td>
                                        ISSUING OFFICE
                                    </td>
                                    <td>
                                        ISSUING DATE
                                    </td>
                                    <td>
                                        SECURITY FV
                                    </td>
                                    <td>
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt2">
                                        LTV
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard1">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard1">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeCreditCard1"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateCreditCard1"/>
                                    </td>
                                    
                                    <td>
                                        <input id="txtSecurityFVForcreditCard1" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard1" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForCreditCardSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard1" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard2">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard2">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeCreditCard2"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateCreditCard2"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForcreditCard2" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard2" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForCreditCardSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard2" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        <div style="border: none;">
                            <table class="tableStyleSmall" style="margin-top: 18px;" id="tblOnSCBSecurityOverDraft">
                                <tr class="theader">
                                    <td>
                                        STATUS
                                    </td>
                                    <td>
                                        TYPE
                                    </td>
                                    <td>
                                        ISSUING OFFICE
                                    </td>
                                    <td>
                                        ISSUING DATE
                                    </td>
                                    <td>
                                        SECURITY FV
                                    </td>
                                    <td>
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt3">
                                        LTV
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft1">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft1">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeOverDraft1"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateOverDraft1"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft1" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft1" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForOverDraftSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft1" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft2">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft2">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeOverDraft2"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateOverDraft2"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft2" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft2" type="text" onchange="analyzeLoanApplicationHelper.calculateLTVForOverDraftSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft2" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft3">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft3">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeOverDraft3"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateOverDraft3"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft3" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft3" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft3" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>--%>
                <div class="clear">
                </div>
                <!-- OFF SCB start -->
                <br />
                <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%; background-color: white;
                    text-align: left; border: none;">
                    <span style="color: black;">OFF SCB</span></div>
                <div class="divRightBig" style="width: 98%; *width: 98%; _width: 98%; float: left;
                    margin-left: 2px;">
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        TERM
                        <label id="lblloanTerms1">
                            LOANS</label>
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityTermLoan">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <td id="tdEM4">
                                        EMI
                                    </td>
                                    <td id="tdEM5">
                                        EMI %
                                    </td>
                                    <td id="tdEM6">
                                        EMI SHARE
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB1" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB1" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb1" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB2" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb2" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB3" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB3" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB3" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb3" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB4" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb4" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB4" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB4" type="text" class="comboSizeActive" onchange="analyzeLoanApplicationHelper.changeTurnOverShareForOffSCB('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb4" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        CREDIT CARD
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityCreditCard">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <%--<td>
                                        INTEREST RATE
                                    </td>--%>
                                    <td id="tdinterest2">
                                        INTEREST
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForCreditCardOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForcreditCardOffScb1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeCreditCardInterestForOffSCB(1)" />
                                    </td>
                                    <%--<td>
                                        <input id="txtInterestRateForCreditCardForOffScb1" type="text" onchange="analyzeLoanApplicationHelper.changeCreditCardInterestForOffSCB(1)"  />
                                    </td>--%>
                                    <td>
                                        <input id="txtInterestForCreditCardForOffScb1" type="text"  disabled="disabled"  />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForCreditCardOffScb1" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForCreditCardOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForcreditCardOffScb2" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeCreditCardInterestForOffSCB(2)" />
                                    </td>
                                    <%--<td>
                                        <input id="txtInterestRateForCreditCardForOffScb2" type="text" onchange="analyzeLoanApplicationHelper.changeCreditCardInterestForOffSCB(2)" />
                                    </td>--%>
                                    <td>
                                        <input id="txtInterestForCreditCardForOffScb2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForCreditCardOffScb2" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        OVERDRAFT
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityOverDraft">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <td id="tdInterestrate1">
                                        INTEREST RATE
                                    </td>
                                    <td id="tdinterest3">
                                        INTEREST
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeOverDraftInterstForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB1" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeOverDraftInterstForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb1" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb2" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeOverDraftInterstForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB2" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeOverDraftInterstForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb2" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB3" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb3" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeOverDraftInterstForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB3" class="comboSizeActive" type="text" onchange="analyzeLoanApplicationHelper.changeOverDraftInterstForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb3" class="comboSizeActive">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <!-- End of OFF SCB -->
            </div>
            <br />
            <br />
        </div>
        <!--End of Exposures -->
        <div class="clear">
        </div>
        <div class="btns" style="width: 100%; text-align: center; background-color: #fff;height: 50px; vertical-align:middle;">
            <a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a> 
            <a href="javascript:;" id="lnkForward" class="iconlink iconlinkforward">Forward</a> 
            <a href="javascript:;" id="lnkBackward" class="iconlink iconlinkDefer">Backward</a>
            <a href="javascript:;" id="lnkDefer" class="iconlink iconlinkDefer">On Hold</a> 
            <a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a> 
            <%--<a href="javascript:;" id="lnkPrint" class="iconlink iconlinkClose">Print</a>--%>
        </div>
    </div>
    </div>
 
 
 
</asp:Content>
