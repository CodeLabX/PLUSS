﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI.AutoLoan
{

    public partial class AutoApprover : System.Web.UI.MasterPage
    {
        public UserManager userManagerObj = null;
        private UserAccessLogManager userAccessLogManagerObj = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            userAccessLogManagerObj = new UserAccessLogManager();
            userManagerObj = new UserManager();
            userIdHiddenField.Value = (string)Session["UserId"];
            if (Session.Count == 0)
            {
                Response.Redirect("~/ErrorPage/Timout.htm");
            }
            if (!IsPostBack)
            {
                userNameLabel.Text = (string)Session["UserName"];

                short userType = (short)Session["UserType"];
                //if (userType.Equals(5))
                //{
                //    MainMenu.Items[5].Enabled = true;
                //}
                //else
                //{
                //    MainMenu.Items[5].Enabled = false;
                //}
            }
            Session["MasterPageUrlForQueryTracker"] = "~/UI/AutoLoan/AutoApprover.master";
            Session["MasterPageUrlForAutoApprover"] = "~/UI/AutoLoan/AutoApprover.master";
        }

        #region

        private void removeCookie()
        {
            HttpCookie myCookie = new HttpCookie("LOGIN");
            Response.Cookies["LOGIN"].Expires = DateTime.Now.AddDays(-365);

            string currentCookieValue = "";
            if (Response.Cookies["LOGIN"].Value == "" && Response.Cookies["LOGIN"].Expires == DateTime.MinValue)
            {
                currentCookieValue = Request.Cookies["LOGIN"].Value;
                Response.Cookies.Remove("LOGIN");
            }
            else
            {
                myCookie = new HttpCookie("LOGIN");
                myCookie.Values.Add("LoginId", "");
                myCookie.Values.Add("status", "0");
                currentCookieValue = Response.Cookies["LOGIN"].Value;
            }
        }

        #endregion

        protected void logout_Click(object sender, EventArgs e)
        {
            SaveAccessLog(userIdHiddenField.Value.ToString(), 0, 0, 0);
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            removeCookie(); //tarek
            Response.Redirect("~/LoginUI.aspx");


        }

        private void SaveAccessLog(string loginId, int successStatus, int unSuccessStatus, int userLoginStatus)
        {
            int insertAccessLog = 0;
            UserAccessLog userAccessLogObj = new UserAccessLog();
            userAccessLogObj.UserLogInId = loginId.ToString();
            userAccessLogObj.IP = Request.ServerVariables["REMOTE_ADDR"].ToString();
            userAccessLogObj.LogInCount = 0;
            userAccessLogObj.LogInDateTime = DateTime.Now;
            userAccessLogObj.LogOutDateTime = DateTime.Now;
            userAccessLogObj.userLoginSuccessStatus = 0;
            userAccessLogObj.userLoginUnSuccessStatus = 0;
            userAccessLogObj.userLoginStatus = 0;
            userAccessLogObj.AccessFor = "LOGOUT";
            insertAccessLog = userAccessLogManagerObj.SendDataInToDB(userAccessLogObj);
        }

        protected void MainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            string s = e.Item.Text;
        }
    }
}
