﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class AnalystSummary : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //string callingFrom = Request.QueryString["CallFrom"];
            if (Session["MasterPageUrlForAnalystSummary"] != null)
            {
                this.MasterPageFile = Session["MasterPageUrlForAnalystSummary"].ToString();
            }

        }

        [AjaxNamespace("AnalystSummary")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(AnalystSummary));
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanSummary> GetLoanSummary(int pageNo, int pageSize, string searchKey)
        {
            List<AutoLoanSummary> objLoanSummaryList = new List<AutoLoanSummary>();
            pageNo = pageNo * pageSize;
            var userType = Session["AutoUserType"];
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objLoanSummaryList;
            }
            else
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                objLoanSummaryList =
                    autoLoanAssessmentService.getAutoLoanSummaryAllForAnalyst(pageNo, pageSize, searchKey, statePermission);
            }
            return objLoanSummaryList;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoUserTypeVsWorkflow> loadState() {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);
            var userType = Convert.ToInt32(Session["AutoUserType"]);

            List<AutoUserTypeVsWorkflow> objList_AutoStatus = autoVehicleStatusIRService.LoadState(userType);
            return objList_AutoStatus;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string AppealedByAutoLoanMasterId(int appiledOnAutoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                if (UserType == 10)
                {
                    objAutoStatusHistory.StatusID = 43; //received by auto ops maker
                }
                else if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 20; //received by auto ops Checker
                }
                else if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 23; //received by auto CI Support
                }
                else if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 26; //received by auto CI Analyst
                }
                else if (UserType == 13)
                {
                    objAutoStatusHistory.StatusID = 26; //received by auto CI Analyst
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }
    }
}
