﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class AutoLoanSummaryForSales : System.Web.UI.Page
    {
        [AjaxNamespace("AutoLoanSummaryForSales")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(AutoLoanSummaryForSales));
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanSummary> GetLoanSummary(int pageNo, int pageSize, Int32 searchKey)
        {
            pageNo = pageNo * pageSize;
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            List<AutoLoanSummary> objLoanSummaryList =
                autoLoanAssessmentService.getAutoLoanSummaryAll(pageNo, pageSize, searchKey);
            return objLoanSummaryList;
        }
        
        

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string AppealedByAutoLoanMasterId(int appiledOnAutoLoanMasterId)
        {
            var res = "";
            try
            {
               var userID = Convert.ToInt32(Session["Id"].ToString());
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                objAutoStatusHistory.StatusID = 17; //Forword To CI
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string appealForThisLLID(int hdllid, int newLLId, int appiledOnAutoLoanMasterId, int llType)
        {
            var res = "";
            try
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);


                if (llType == 1)
                {
                    var rejectData = autoLoanAssessmentService.GetRejectInformation(appiledOnAutoLoanMasterId);
                    var todaysDate = DateTime.Now;

                    TimeSpan diff = todaysDate - rejectData.ActionDate;
                    int days = diff.Days;

                    if (days <= 10)
                    {
                        var userID = Convert.ToInt32(Session["Id"].ToString());
                        AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                        objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                        objAutoStatusHistory.StatusID = 17; //Forword To CI
                        objAutoStatusHistory.UserID = userID;
                        res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
                    }
                    else
                    {
                        res = "10 Days Expired";
                    }


                }
                else
                {

                    var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];
                    ILoanApplicationRepository repository1 = new AutoLoanDataService.AutoLoanDataService();
                    var loanApplicationService = new LoanApplicationService(repository1);
                    var objPrimaryLLID = loanApplicationService.getLoanLocetorInfoByLLIDForSales(newLLId, statePermission);
                    if (objPrimaryLLID != "No Data")
                    {
                        if (((AutoLoanService.Entity.LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).LLID == newLLId)
                        {

                            var objAutoLoanApplicationAll = new Object();

                            if (statePermission.Count == 0)
                            {
                                objAutoLoanApplicationAll = null;
                            }
                            else
                            {
                                objAutoLoanApplicationAll = autoLoanAssessmentService.getLoanLocetorInfoByLLID(hdllid);
                            }
                            if (objAutoLoanApplicationAll != null)
                            {
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster.ParentLLID = hdllid;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster.LLID = newLLId;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster.Auto_LoanMasterId = 0;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster.StatusID = 17;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).AutoLoanAnalystMaster.AnalystMasterId = 0;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).AutoLoanAnalystMaster.AutoLoanMasterId = 0;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).AutoLoanAnalystMaster.LlId = newLLId;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRAccount.AccountNumberForSCB = string.IsNullOrEmpty(((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).AccountNo) ? ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRAccount.AccountNumberForSCB : ((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).AccountNo;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.PrName = string.IsNullOrEmpty(((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).ApplicantName) ? ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.PrName : ((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).ApplicantName;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.DateofBirth = string.IsNullOrEmpty(((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).DateOfBirth) ? ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.DateofBirth : Convert.ToDateTime(((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).DateOfBirth);
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster.AppliedAmount = ((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).LoanApplyAmount==0 ? ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster.AppliedAmount : ((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).LoanApplyAmount;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster.AppliedAmount = ((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).LoanApplyAmount == 0 ? ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster.AppliedAmount : ((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).LoanApplyAmount;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.SpouseName = string.IsNullOrEmpty(((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).SourcePersonName) ? ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.SpouseName : ((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).SourcePersonName;
                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.TINNumber = string.IsNullOrEmpty(((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).TinNo) ? ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.TINNumber : ((LoanLocatorApplicantInfoByLLID)(objPrimaryLLID)).TinNo;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant.PrApplicantId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJTApplicant.JtApplicantId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRProfession.PrProfessionId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJTProfession.JtProfessionId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanVehicle.AutoLoanVehicleId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanVehicle.AutoLoanVehicleId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRAccount.PrAccountId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJTAccount.jtAccountId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRFinance.PrFinanceId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJTFinance.JtFinanceId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanReference.ReferenceId = 0;

                                ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoApplicationInsurance.ApplicationInsuranceId = 0;




                                res = loanApplicationService.SaveAutoLoanApplication(((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanMaster, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRApplicant, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJTApplicant, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRProfession,
                                    ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJTProfession, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanVehicle, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRAccount, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJTAccount, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRAccountDetailList, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJtAccountDetailList,
                                    ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoPRFinance, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoJTFinance, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanReference, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanFacilityList, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoLoanSecurityList,
                                    ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoApplicationInsurance, ((AutoLoanApplicationAll)(objAutoLoanApplicationAll)).objAutoVendor);

                                if (res == "Success") {
                                    var userID = Convert.ToInt32(Session["Id"].ToString());
                                    //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                                    AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                                    objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                                    objAutoStatusHistory.StatusID = 41;
                                    objAutoStatusHistory.UserID = userID;


                                    res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
                                }
                            }
                        }
                    }
                    else {
                        res = objPrimaryLLID.ToString();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBank> GetBank(int status)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBank(status);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVendor> GetVendor()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetVendor();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoProfession> GetProfession()
        {
            AutoSegmentSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoSegmentSettingsService = new AutoSegmentSettingsService(repository);


            List<AutoProfession> objList_AutoProfession =
                autoSegmentSettingsService.GetProfession();
            return objList_AutoProfession;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoCompany> getNameOfCompany()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getNameOfCompany();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> getVehicleStatus(int statusID)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVehicleStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<Auto_InsuranceBanca> getGeneralInsurance()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getGeneralInsurance();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanApplicationSource> GetAllSource()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAllSource();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoFacility> getFacility()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetFacility();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public Object getLoanLocetorInfoByLLID(int llid)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getLoanLocetorInfoByLLIDForSalesAndOps(llid);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> getModelbyManufacturerID(int manufacturerID)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            List<AutoVehicle> objList_AutoModel = tanorAndLTVService.GetModelList(manufacturerID);
            return objList_AutoModel;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranch> GetBranch(int bankID)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBranch(bankID);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPrice> GetPriceByVehicleId(int vehicleId, int manufacturingYear, int vehicleStatus)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetPriceByVehicleId(vehicleId, manufacturingYear, vehicleStatus);
            return res;
        }

    }
}