﻿<%@ Page Title="SCB | Tanor and LTV Settings" Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.TenorAndLTVSettings" Codebehind="TenorAndLTVSettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<script src="../../Scripts/AutoLoan/TanorAndLTVSettings.js" type="text/javascript"></script>



    <style type="text/css">
        #nav_TenorAndLTVSettings a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    <div class ="pageHeaderDiv ">Tenor & LTV Settings</div>
<div id="Content">
<div class="SearchwithPager">
    <div class="search searchwithUpload">
    <input type="text" id="txtSearch" class="txt txtSearch widthSize35_per" onkeypress="tanorHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search"/>
    <asp:FileUpload ID="fileDocumentUpload" class="txt txtUpload" runat="server" />
        <asp:Button ID="btnUploadSave" runat="server" Text="Upload" 
             onclick="btnUploadSave_Click" UseSubmitBehavior="False" />
    </div> 
    <div id="Pager" class="pager"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
    
    
    </div>
 
 <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					<col width="50px" />
					<col width="150px" />
					<col width="100px" />
					<col width="70px" />
					<col width="100px" />
					<col width="100px" />
					<col width="70px" />
					<col width="150px" />
				</colgroup>
				<thead>
					<tr>
						<th>
							SL
						</th>
						<th>
						    Manufacturer
						</th>
						<th>
						    Country
						</th>
						<th>
						    Model
						</th>
						<th>
						    Vehicle Status
						</th>
						<th>
						    Tenor
						</th>
						
						<th>
						    LTV %
						</th>
						<th> <a id="lnkAddTanor" class="iconlink iconlinkAdd" href="javascript:;">New Tenor & LTV</a>
						</th>
					</tr>
				</thead>
				<tbody id="tanorlist">
				</tbody>
			</table>
 
</div>


<div class="modal" id="tanorAndLTVPopupDiv" style="height:370px; display:none; text-align: left;">
		<h3>Add/Edit Tenor & LTV Settings</h3>
		<label for="txtHeadline">Manufacturer*</label>
		<select id="cboManufacturer" class="txt" title="Manufacturer">
		    <%--<option value="-1" selected="selected">Select from List</option>--%>
		</select>
		 
		<label for="txtManufacturingCountry">Manufacturing Country*</label>
		<input class="txt" id="txtManufacturingCountry" title="Manufacturing Country" disabled="disabled" /><br/>
		
		 <label for="txtHeadline">Model:</label> 
		 <select id="cmbModel" class="txt" title="Model">
		 <%--<option value="-1" selected="selected">Select from List</option>--%>
		 </select>
		
		 <label for="txtTrimLevel">Trim Level:</label>
		 <input class="txt" id="txtTrimLevel" title="Trim Level" disabled="disabled" /><br/>
		 
		 <label for="txtCC">CC:</label>
		 <input class="txt" id="txtCC" title="CC" disabled="disabled" /><br/>
		 <label for="cmbVehicleType">Vehicle Type</label>
		 <select id="cmbVehicleType" class="txt" title="Vehicle Type">
		    <option value="-1">Select From List</option>
		    <option value="S1">Sedan</option>
		    <option value="S2">Station wagon</option>
		    <option value="S3">SUV</option>
		    <option value="S4">Pick-Ups</option>
		    <option value="S5">Microbus</option>
		    <option value="S6">MPV</option>
		</select><br/>
		 <label for="txtHeadline">Vehicle Status:</label> 
		 <select id="cmbVehicleStatus" class="txt" title="Vehicle Status">
		 </select>
		 
		 <label for="txtTenor">Tenor:</label>
		 <input class="txt" id="txtTenor" title="Tenor"/><br/>
		 
		 <label for="txtLTV">LTV %:</label>
		 <input class="txt" id="txtLTV" title="LTV %"/><br/>
		<label for="txtLTV">Value Pack Allowed:</label>
		 <select id="cmbValuePackAllowed" class="txt" title="Value Pack Allowed">
		    <option value="1" selected="selected">Yes</option>
		    <option value="0">No</option>
		 </select>
		
		
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
</asp:Content>

