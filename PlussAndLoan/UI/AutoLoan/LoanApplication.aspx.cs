﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class LoanApplication : System.Web.UI.Page
    {

        private string m_SpreadSheet;
        private string llid = string.Empty;

        [AjaxNamespace("LoanApplication")]
        protected void Page_Load(object sender, EventArgs e)
        {
            ////page load event
            Utility.RegisterTypeForAjax(typeof(LoanApplication));

            #region bank statement
            //try
            //{
            //    string stype = string.Empty;
            //    string updatedBy = string.Empty;
            //    string strOperation = string.Empty;
            //    strOperation = Request.QueryString["operation"];
            //    llid = Request.QueryString["llid"];
            //    string accountType = Request.QueryString["applicantType"];

            //    if (strOperation == "save")
            //    {
            //        var streamReader = new System.IO.StreamReader(Request.InputStream);
            //        var spreadsheetdata = streamReader.ReadToEnd(); //'' this needs to be updated in teh database

            //        SaveTemplate(spreadsheetdata, accountType);

            //        //Response.Write("Data Saved");
            //        Response.End();
            //    }
            //    else if (strOperation == "loadTemplate")
            //    {
            //        Response.Write(GetTemplate(llid,accountType));
            //      //  Response.End();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            #endregion bank statement

        }

#region bank statement
        //for save bank statement

        private string GetTemplate(string llid, string accountType)
        {
            // show bank statement
            
            string sReturnData = string.Empty;
            //ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            //var loanApplicationService = new LoanApplicationService(repository);

            //if (accountType == "primary")
            //{
            //    sReturnData = loanApplicationService.GetPRBankStatementData(llid);
            //}
            //else if (accountType == "joint")
            //{
            //    sReturnData = loanApplicationService.GetJTBankStatementData(llid);

            //}

            return sReturnData;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoGetBankStatement> GetBankStatement(string llid)
        {
            // show bank statement
            var sReturnData = new List<AutoGetBankStatement>();
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);

            sReturnData = loanApplicationService.getBankStatement(llid);
            

            return sReturnData;
        }

        private void SaveTemplate(string spreadsheetdata, string SavePRTemplate)
        {
            //save xml to session
            if (SavePRTemplate == "primary")
            {
                Session["PRspreadsheetData"] = spreadsheetdata;
            }
            else if (SavePRTemplate == "joint")
            {
                Session["JTspreadsheetData"] = spreadsheetdata;
            }

        }

        // for save bank statement end
#endregion bank statement



        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveLoanApplication(AutoLoanMaster objAutoLoanMaster, 
            AutoPRApplicant objAutoPRApplicant, AutoJTApplicant objAutoJTApplicant, 
            AutoPRProfession objAutoPRProfession, AutoJTProfession objAutoJTProfession, 
            AutoLoanVehicle objAutoLoanVehicle, AutoPRAccount objAutoPRAccount, AutoJTAccount objAutoJTAccount, 
            List<AutoPRAccountDetail> objAutoPRAccountDetailList, List<AutoJTAccountDetail> objAutoJtAccountDetailList, 
            AutoPRFinance objAutoPRFinance, AutoJTFinance objAutoJTFinance, AutoLoanReference objAutoLoanReference, 
            List<AutoLoanFacility> objAutoLoanFacilityList, List<AutoLoanSecurity> objAutoLoanSecurityList, 
            AutoApplicationInsurance objAutoApplicationInsurance, AutoVendor objAutoVendor)//,AutoPRBankStatement objPRBankStatement,AutoJTBankStatement objJTBankStatement)
        //public string SaveLoanApplication(AutoPRBankStatement objPRBankStatement)
        {
            //loan application save method
            //var PRspreadsheet = Session["PRspreadsheetData"];
            //objPRBankStatement.Statement = PRspreadsheet.ToString();
            //objPRBankStatement.LastUpdateDate = DateTime.Now;

            //var JTspreadsheet = Session["JTspreadsheetData"];
            //objJTBankStatement.Statement = JTspreadsheet.ToString();
            //objJTBankStatement.LastUpdateDate = DateTime.Now;


            objAutoLoanMaster.UserID = Convert.ToInt32(Session["Id"].ToString());
            //objAutoLoanMaster.StatusID = 17;
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.SaveAutoLoanApplication(objAutoLoanMaster, objAutoPRApplicant, objAutoJTApplicant, objAutoPRProfession,
                objAutoJTProfession, objAutoLoanVehicle, objAutoPRAccount, objAutoJTAccount, objAutoPRAccountDetailList, objAutoJtAccountDetailList,
                objAutoPRFinance, objAutoJTFinance, objAutoLoanReference, objAutoLoanFacilityList, objAutoLoanSecurityList,
                objAutoApplicationInsurance, objAutoVendor);
            return res;
           // return "Success";
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> getModelbyManufacturerID(int manufacturerID)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            List<AutoVehicle> objList_AutoModel = tanorAndLTVService.GetModelList(manufacturerID);
            return objList_AutoModel;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string GetServerDateTime() {
            var res = DateTime.Now.ToString();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public Object getLoanLocetorInfoByLLID(int llid)
        {
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getLoanLocetorInfoByLLIDForSales(llid, statePermission);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBank> GetBank(int status)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBank(status);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranch> GetBranch(int bankID)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBranch(bankID);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVendor> GetVendor()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetVendor();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoFacility> getFacility()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetFacility();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoProfession> GetProfession()
        {
            AutoSegmentSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoSegmentSettingsService = new AutoSegmentSettingsService(repository);


            List<AutoProfession> objList_AutoProfession =
                autoSegmentSettingsService.GetProfession();
            return objList_AutoProfession;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetAutoModelByManufacturerID(int manufacturerID)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAutoModelByManufacturerID(manufacturerID);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetAutoTrimLevelByModel(int manufacturerID, string model)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAutoTrimLevelByModel(manufacturerID, model);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetAutoEngineCCByModel_TrimLevel(int manufacturerID, string model, string trimLevel)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAutoEngineCCByModel_TrimLevel(manufacturerID, model, trimLevel);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetAutoVehicleType(int manufacturerID, string model, string engineCC, string TrimLevel)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAutoVehicleType(manufacturerID, model, engineCC, TrimLevel);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoCompany> getNameOfCompany()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getNameOfCompany();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> getVehicleStatus(int statusID)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVehicleStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPrice> getManufacturingYear(int manufacturerID, string model, string engineCC, string TrimLevel)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getManufacturingYear(manufacturerID, model, engineCC, TrimLevel);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPrice> GetPrice(int manufacturerID, string model, string engineCC, string TrimLevel, string manufacturingYear)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetPrice(manufacturerID, model, engineCC, TrimLevel, manufacturingYear);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPrice> GetPriceByVehicleId(int vehicleId, int manufacturingYear, int vehicleStatus)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetPriceByVehicleId(vehicleId, manufacturingYear, vehicleStatus);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<Auto_InsuranceBanca> getGeneralInsurance()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getGeneralInsurance();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanApplicationSource> GetAllSource()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAllSource();
            return res;
        }






    }
}
