﻿using System;
using System.Collections.Generic;
using AutoLoanService.Entity.CheckListPrint;
using CrystalDecisions.CrystalReports.Engine;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class UI_AutoLoan_BALetterPrintCrystal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BAlatterPrintEntity ba = new BAlatterPrintEntity();
            List<BAlatterPrintEntity> baList = new List<BAlatterPrintEntity>();
            ba = (BAlatterPrintEntity)Session["BALetter"];
            baList.Add(ba);
            string path = "";
            ReportDocument rd = new ReportDocument();
            if (ba.reportType == "3")
            {
                path = Server.MapPath("PrintCheckList/BALetterSaadiq.rpt");
            }
            else
            {
                path = Server.MapPath("PrintCheckList/BALetter.rpt");
            }
            rd.Load(path);
            rd.SetDataSource(baList);
            crViewer.ReportSource = rd;
            //rd.PrintToPrinter(1, true, 0, 0);
        }
    }
}
