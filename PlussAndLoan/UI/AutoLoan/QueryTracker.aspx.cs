﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{

    public partial class QueryTracker : System.Web.UI.Page
    {
        protected  void Page_PreInit(object sender, EventArgs e)
        {
            //string callingFrom = Request.QueryString["CallFrom"];
            if (Session["MasterPageUrlForQueryTracker"] != null)
            {
                this.MasterPageFile = Session["MasterPageUrlForQueryTracker"].ToString();
            }

        }
        [AjaxNamespace("QueryTracker")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(QueryTracker));
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<Department_Entity> GetQueryTo()
        {
            IQueryRepository repository = new AutoLoanDataService.AutoQueryDataService();
            var autoATDepartment = new AutoQueryService(repository);


            List<Department_Entity> objDepartment =
                autoATDepartment.GetAll_AT_Department();
            return objDepartment;


        }

        /// <summary>
        ///  if readUnreadStatus =0 then getAllUnread Query
        /// else
        /// readUnreadStatus =1 then getAllRead Query
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="readUnreadStatus"></param>
        /// <returns></returns>
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<QueryTracker_Entity> GetAllReadUnreadQueryDetails(int pageNo, int pageSize, int readUnreadStatus)
        {
            pageNo = pageNo * pageSize;
            IQueryRepository repository = new AutoLoanDataService.AutoQueryDataService();
            var getQueryTrackerList = new AutoQueryService(repository);

            int userId = Convert.ToInt32(Session["Id"].ToString());
            int usLeId = Convert.ToInt32(Session["AutoUserType"].ToString());
            List<QueryTracker_Entity> objGetQueryTrackerList =
                getQueryTrackerList.GetQueryTrackerListByReadUnreadStatus(userId, usLeId, pageNo, pageSize, readUnreadStatus);
            return objGetQueryTrackerList;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public QueryTrackerDetails_Entity searchDetailsByLlId(int pageNo, int pageSize, int searchKey)
        {
            pageNo = pageNo * pageSize;

            var a = Session["Id"].ToString();
            var a1 = Session["UserId"].ToString();
            var a2 = Session["UserName"].ToString();
            var a3 = Session["Designation"].ToString();
            var a4 = Session["AutoUserType"].ToString();
            IQueryRepository repository = new AutoLoanDataService.AutoQueryDataService();
            var searchQueryDetails = new AutoQueryService(repository);


            QueryTrackerDetails_Entity objsearchQueryDetails =
                searchQueryDetails.SearchQueryDetailsByLLID(pageNo,pageSize,searchKey);
            if (objsearchQueryDetails != null)
            {
                objsearchQueryDetails.LoginUserName = Session["UserName"].ToString();
            }
            return objsearchQueryDetails;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string GetLoginUserName()
        {
            string LoginUserName = Session["UserName"].ToString();
            return LoginUserName;
        }



        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<QueryTracker_Entity> GetQueryTrackerListByLLID(int pageNo, int pageSize, int searchKey)
        {
            pageNo = pageNo * pageSize;

            IQueryRepository repository = new AutoLoanDataService.AutoQueryDataService();
            var getQueryTrackerList = new AutoQueryService(repository);

            int userId = Convert.ToInt32(Session["Id"].ToString());
            int usLeId = Convert.ToInt32(Session["AutoUserType"].ToString());
            List<QueryTracker_Entity> objGetQueryTrackerList =
                getQueryTrackerList.GetQueryTrackerListByLLID(userId, usLeId, pageNo, pageSize, searchKey);
      

            return objGetQueryTrackerList;
        }




        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatusHistory> SearchApplicationStatusByLLID(int searchKey)
        {
            IQueryRepository repository = new AutoLoanDataService.AutoQueryDataService();
            var applicationStatus = new AutoQueryService(repository);


            List<AutoStatusHistory> objAutoStatusHistoryList =
                applicationStatus.SearchApplicationStatusByLLID(searchKey);
            return objAutoStatusHistoryList;
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveNewQueryByLLID(QueryTracker_Entity objNewQuery)
        {
            var res="";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                IQueryRepository repository = new AutoLoanDataService.AutoQueryDataService();
                var saveNewQueryByLLID = new AutoQueryService(repository);
                objNewQuery.PostingById = userID;
                objNewQuery.Comments.Replace("'", "^")
                    .Replace("&","~");

              res = saveNewQueryByLLID.SaveNewQuery(objNewQuery);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string ReplyOnQueryByLLID(QueryTracker_Entity objReplayOnQuery)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                IQueryRepository repository = new AutoLoanDataService.AutoQueryDataService();
                var replyQueryByLLID = new AutoQueryService(repository);
                objReplayOnQuery.PostingById = userID;
                objReplayOnQuery.Comments.Replace("'", "^")
                    .Replace("&", "~");

                res = replyQueryByLLID.ReplyOnQuery(objReplayOnQuery);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string MakeAsReadQueryByTrackerId(int trackerId)
        {
            var res = "";
            try
            {
            
                IQueryRepository repository = new AutoLoanDataService.AutoQueryDataService();
                var makeAsReadQueryByTrackerId = new AutoQueryService(repository);
                res = makeAsReadQueryByTrackerId.UpdateStatusAsReadQueryByTrackerId(trackerId);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        
    }
}
