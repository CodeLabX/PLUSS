﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class IRSettingsForVehicleStatus : System.Web.UI.Page
    {
        [AjaxNamespace("IRSettingsForVehicleStatus")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(IRSettingsForVehicleStatus));
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicleStatusIR> GetAutoVehicleStatusIRSummary(int pageNo, int pageSize)
        {
            pageNo = pageNo * pageSize;

            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoVehicleStatusIR> objList_AutoVehicleStatusIR =
                autoVehicleStatusIRService.GetAutoVehicleStatusIRSummary(pageNo, pageSize);
            return objList_AutoVehicleStatusIR;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAuto_IRVehicleStatus(AutoVehicleStatusIR insurance)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new AutoVehicleStatusIRService(repository);
            var res = autoLoanDataService.SaveAuto_IRVehicleStatus(insurance);
            return res;

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string IRVehicleStatusDeleteById(int vehicleStatusIRId)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new AutoVehicleStatusIRService(repository);
            var res = autoLoanDataService.IRVehicleStatusDeleteById(vehicleStatusIRId);
            return res;

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> GetStatus(int statusID)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVehicleStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }

    }
}
