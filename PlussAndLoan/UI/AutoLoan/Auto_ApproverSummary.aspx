﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoApprover.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.Auto_ApproverSummary" Title="Auto Approver Summary" Codebehind="Auto_ApproverSummary.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<link href="../../CSS/Default.css" rel="stylesheet" type="text/css" />

 <link href="../../CSS/admin.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/TableLayoutForTabControls.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Scripts/AutoLoan/AutoLoanApprover.js"></script>
       <style type="text/css">
        #nav_Auto_ApproverSummary a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    <div id="divAutoloanSummary">
    
    <div class="pageHeaderDiv">LOAN SUMMARY</div>

             <div id="divLoanAppMasterData" class="fieldDiv divPadding formBackColor centerAlign">
                <div class="fieldDiv">
                    <label class="lbl rightAlign">
                        LLID<span class="starColor">*</span>:</label>
                        <input type="text" id="txtLLIDMain" class="txtField widthSize25_per" onkeypress="ApproverHelper.searchKeypress(event)"/>
                        <input type="button" id="btnLLIdSearch" class="" value="Search"/>
                </div>
                
            </div>
         
             <div class="divLeftNormalClass widthSize97p9_per">
             
           <table  cellpadding="0" cellspacing="0" frame="box" border="0" class="summary centerAlign widthSize100_per">
				        <colgroup class="sortable ">
					        <col width="80px" />
					        <col width="450px" />
					        <col width="150px" />
					        <col width="90px" />
					        <col width="100px" />
					        <col width="110px" />
					        <col width="60px" />
					        <col width="110px" />
					        <col width="110px" />
					        <col width="80px" />
        					
				        </colgroup>
				        <thead >
					        <tr class="theadTdTextAlignCenter">
						        <th>
							        LLID
						        </th>
						        <th>
						            CUSTOMER
						        </th>
						        <th>
						            PRODUCT
						        </th>
        						<th>
						            BRAND
						        </th>
						        <th>
						            MODEL
						        </th>
						        <th>
						            TOTAL LOAN
						        </th>
<%--						        <th>
						            EMI
						        </th>--%>
						        <th>
						            TENOR
						        </th>	
						        <th>
						            STATUS
						        </th>			
						        <th>
						            ACTION 
						        </th>
					        </tr>
				        </thead>
				        <tbody id="GridLoanSummery">
				        </tbody>
			        </table>
			        <div class="clear"></div>
			<div id="Pager" class="pager pagerFooter" > 
                            <label for="txtPageNO" class="pagerlabel">page</label>                                           
                            <input id="txtPageNo" class="txtPageNo" value="1" maxlength="7"></input>
                            <span id="spntotalPage" class="spntotalPage">of 1</span> 
                            <a id="lnkPrev" title="Previous Page" href="#" class="prevPage">prev</a> 
                            <a id="lnkNext" title="Next Page" href="#" class="nextPage">next</a>
                    </div>
            <div class="clear"></div>
            
            
            
        </div>    
            
            
                    </div>
     
     <div class="clear"></div>
     
     <div id="divLoanDetails" style="display:none;">
    
    <input id="txtAutoLoanMasterId" type="hidden" value="0"/>
        
            
           <div  class="divHeadLineWhiteBg" style="text-align: center; margin: 0px; width:84%; margin-bottom:5px; background:#005d9a; xmargin:auto; color:White;"> <span style="font-weight: bold; font-family: verdana; font-size: medium">CUSTOMER INFORMATION</span> </div>
           
           <div class="divLeftBig" style="width :750px; margin: 0px; border:0px; ">
           <div class="content" id="Div4" style="border: none; padding: 0;">
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">LLID:</label>
                            <input id="txtLLID" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CUSTOMER SEGMENT:</label>
                            <select id="cmbEmployeeSegment" class="comboSizeH_24 widthSize30_per" disabled="disabled">
                            </select>
                            
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">DATE OF BIRTH:</label>
                            <input id="txtDateOfBirth" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CATAGORY:</label>
                            <select id="cmbEmployeeCategory" class="comboSizeH_24 widthSize22_per" disabled="disabled">
                            </select>
                            <input id="txtEmployerCategoryCode" class="txtFieldBig widthSize7_per " type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">AGE:</label>
                            <input id="txtAge" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">ASSESMENT METHOD:</label>
                            <select id="cmbAssessmentMethod" class="comboSizeH_24 widthSize22_per" disabled="disabled">
                            </select>
                            <input id="txtAssesmentMethodCode" class="txtFieldBig widthSize7_per " type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">DECLARED INCOME:</label>
                            <input id="txtDeclaredIncomePr" class="txtFieldBig widthSize15_per" type="text" disabled="disabled" />
                            <input id="txtDeclaredIncomeJt" class="txtFieldBig widthSize14_per" type="text" disabled="disabled" />
                            <label class="lbl rightAlign" style ="width: 20%;">APPROPIATE INCOME:</label>
                            <input id="txtAppropiateIncome" class="txtFieldBig widthSize22_per" type="text" disabled="disabled" />
                            <input id="Text13" class="txtFieldBig widthSize7_per " type="text" disabled="disabled" />
                        </div>
                        
                        <div class="fieldDiv">
                             <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">ASSESSED INCOME:</label>
                            <input id="txtAssesedIncome" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl rightAlign" style ="width: 20%;">TOTAL INCOME:</label>
                            <input id="txtTotalIncome" class="txtFieldBig" style="width:223px;" type="text" disabled="disabled" />
                            <%--<label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">ASSESSED INCOME:</label>
                            <input id="txtAssesedIncome" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">TOTAL INCOME:</label>
                            <input id="txtTotalIncome" class="txtFieldBig widthSize30_per " type="text" disabled="disabled" />--%>
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">JOINT APP INCOME:</label>
                            <input id="txuJointAppIncome" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CUSTOMER:</label>
                            <input id="txtCustomarName" class="txtFieldBig" style="width:223px;" type="text" disabled="disabled" />
                        </div>
                        
                       
                        
                        
                        
                        
                    
                    </div>
       </div>
       
       
       <div class="clear"></div>
       <br/>
       
       <div  class="divHeadLineWhiteBg" style="text-align: center; margin: 0px;width:84%; margin-bottom:5px;background:#005d9a; xmargin:auto; color:White;"> <span style="font-weight: bold; font-family: verdana; font-size: medium">VENDOR &amp; VEHICLE INFORMATION</span> </div>
           
           <div class="divLeftBig" style="width :750px; margin: 0px; border:0px; ">
           <div class="content" id="Div1" style="border: none; padding: 0;">
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">VENDOR:</label>
                            <input id="tVendor" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">MOU:</label>
                            <select id="cmbVendoreRelationShip" class="comboSize widthSize30_per" disabled="disabled">
                            <option value="1">MOU</option>
                            <option value="0">NON-MOU</option>
                            <option value="2">Negative Listed</option>
                        </select>
                           
                        </div>
                        <div class="fieldDiv">
                         <label class="lbl" style ="width: 135px;">VENDOR CATAGORY:</label>
                            <select id="cmbVendorCategory" class="comboSize widthSize30_per" style="width:228px;" disabled="disabled">
                            
                            </select>
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">PRODUCT:</label>
                             <select id="cmbProductName" class="comboSizeH_24 widthSize30_per" style="width:228px;" disabled="disabled">
            <option value="1">Staff Auto</option>
            <option value="2">Conventional Auto</option>
            <option value="3">Saadiq Auto</option>
        </select>
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">BRAND:</label>
                            <select id="cmbBrand" class="comboSizeH_24 widthSize30_per" disabled="disabled"></select>
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">MODEL:</label>
                            <input id="tModel" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">ENGINE(CC):</label>
                            <input id="tEngineCC" class="txtFieldBig widthSize30_per " style="width:223px;" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">VEHICLE STATUS:</label>
                            <select id="cmbVehicleStatue" class="comboSizeH_24 widthSize30_per" style="width:228px;" disabled="disabled"></select>
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">VEHICLE TYPE:</label>
                            <input id="tVehicleType" class="txtFieldBig widthSize30_per " style="width:223px;" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">MANUFACTURE YEAR:</label>
                            <input id="tManufacturingYear" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">SEATING CAPACITY:</label>
                            <input id="tSeatingCapacity" class="txtFieldBig widthSize30_per " style="width:223px;" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">QUOTED PRICE:</label>
                            <input id="tquotedPrice" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CONSIDERED PRICE:</label>
                            <input id="tconsideredPrice" class="txtFieldBig widthSize30_per " style="width:223px;" type="text" disabled="disabled" />
                        </div>
                        
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">CONSIDERATION ON:</label>
                             <select id="cmbPriceConsideredOn" class="comboSize widthSize30_per" style="width:228px;" disabled="disabled">
                            <option value="-1">Select a Price Considered</option>
                            <option value="1">Index Value</option>
                            <option value="2">Phone</option>
                            <option value="3">Price Verify</option>
                            <option value="2">Quotation</option>
                            <option value="3">NA</option>
                        </select>
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CAR VERIFICATION:</label>
                            <select id="cmbCarVerification" class="comboSize widthSize30_per" disabled="disabled">
                            <option value="0">Select Verification Type</option>
                            <option value="1">Done</option>
                            <option value="2">Valued & Verified</option>
                            <option value="3">Not Required</option>
                        </select>
                        </div>
                        
                       
                        
                        
                        
                        
                    
                    </div>
       </div>
       <div class = "clear"></div>
       
       <br/>
       
       <div  class="divHeadLineWhiteBg" style="text-align: center; margin: 0px;width:84%; margin-bottom:5px; background:#005d9a; xmargin:auto; color:White;"> <span style="font-weight: bold; font-family: verdana; font-size: medium">LOAN INFORMATION</span> </div>
           
           <div class="divLeftBig" style="width :750px; margin: 0px; border:0px; background-color:darkgray; ">
           <div class="content" id="Div2" style="border: none; padding: 0;">
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">PO AMOUNT:</label>
                            <input id="tPOAmount" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">GEN INSURANCE:</label>
                            <input id="tGeneralInsurance" class="txtFieldBig widthSize11_per " type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 50px;*width: 50px;_width: 50px;">ARTA:</label>
                            <input id="tARTA" class="txtFieldBig widthSize11_per " style="width:80px;" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">TOTAL LOAN AMT:</label>
                            <input id="tTotalLoanAmount" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">ARTA FROM:</label>
                            <input id="tARTAFrom" class="txtFieldBig widthSize30_per" style="width:223px;" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">INTEREST RATE:</label>
                            <input id="tInterestRate" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">TENOR:</label>
                            <input id="tTenor" class="txtFieldBig widthSize30_per" style="width:223px;" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">EMI:</label>
                            <input id="tEMI" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">PROFIT MARGIN:</label>
                            <input id="tProfitMargine" class="txtFieldBig widthSize30_per" style="width:223px;" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">LTV:</label>
                            <input id="tLTV" class="txtFieldBig widthSize12_per" type="text" disabled="disabled" />
                            <label class="lbl rightAlign" style ="width: 5%;">DBR:</label>
                            <input id="tDBR" class="txtFieldBig widthSize12_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 148px;">LOAN EXPIRY YEAR:</label>
                            <input id="tLoanExpiryYear" class="txtFieldBig widthSize30_per" style="width:223px;" type="text" disabled="disabled" />
                        </div>
                       
                    </div>
       </div>
       <div class = "clear"></div>
        <br/>
        
      
      
      
      <div class="divLeftBig" style="width :370px; margin: 0px; ">
               <div  class="divHeadLineWhiteBg widthSize100_per" style="text-align: center; margin: 0px; border: none;"> <span style="font-weight: bold; font-family: verdana; font-size: medium"><u>DEVIATION</u></span> </div>
           <div class="content" id="Div5" style="border: none; padding: 0;">
               
                        <div style="border:none;">
                                        <table class="tableStyle" id="Table3" >
                                               <tr class="theader">
                                                   <td style=" width: 15%;*width: 15%;_width: 15%;">Level</td>
                                                   <td style=" width: 65%;*width: 65%;_width: 65%;">Description</td>
                                                   <td style=" width: 15%;*width: 15%;_width: 15%;">Code</td>
                                                   
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbDeviationLevel1" disabled="disabled" onchange="">
                                                        </select>
                                                        </td>
                                                    <td><select id="cmbDeviationDescription1" disabled="disabled"/></td>
                                                    <td><input id="tDeviationCode1" type ="text" disabled="disabled" /></td>
                                                </tr>
                                               
                                               <tr>
                                                    <td><select id="cmbDeviationLevel2" disabled="disabled" onchange="">
                                                        </select>
                                                        </td>
                                                    <td><select id="cmbDeviationDescription2" disabled="disabled"/></td>
                                                    <td><input id="tDeviationCode2" type ="text" disabled="disabled" /></td>
                                                </tr>
                                                
                                               <tr>
                                                    <td><select id="cmbDeviationLevel3" disabled="disabled" onchange="">
                                                        </select>
                                                        </td>
                                                    <td><select id="cmbDeviationDescription3" disabled="disabled"/></td>
                                                    <td><input id="tDeviationCode3" type ="text" disabled="disabled" /></td>
                                                </tr>
                                                
                                              
                                              

                                        </table>
                       </div>
             </div>
          </div>
       
       
       
       <div class="divLeftBig centerAlign" style="margin-left: 5px; margin-bottom: 0px; width: 365px; background-color:darkgray;" >
                                            <span style="font-weight: bold; font-family: verdana;">DEVIATION FOUND</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;">AGE</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="tDevFoundAge" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;" id="tdDbr6">DBR</td>
                                                   <td style ="padding-left: 2px;padding-right: 2px;"><input id="tDevFoundDBR" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;">SEATING CAPACITY</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="tDevFoundSeatingCapacity" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;">SHARE PORTION</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="tDevFoundSharePortion" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>




                                        </table>
                                        <div class="clear"></div>
                                         </div>
                                        <div class="clear"></div>
                                        <br/>
       
       
       
       
     
      
        
           
      <div class="divLeftBig" style="width :370px;*width :370px;_width :370px; margin: 0px; ">
               <div  class="divHeadLineWhiteBg widthSize100_per" style="text-align: center; margin: 0px; border: none;"> <span style="font-weight: bold; font-family: verdana; font-size: medium"><u>EXPOSURES</u></span> </div>
           
               
                        <div style="text-align:left; font-size:medium;font-weight:bold;padding: 2px;">ON SCB                                
                                    <div style="border:none;">
                                        <table class="tableStyle" id="Table1" >
                                               <tr class="theader">
                                                   <td >LIMIT</td>
                                                   <td id="td1">INTEREST</td>
                                                   <td >CONSIDERED</td>
                                                   
                                               </tr>
                                               <tr>
                                                    <td><input id="tLimit1" type ="text"disabled="disabled"/></td>
                                                    <td><input id="tInterest1" type ="text" disabled="disabled"/></td>
                                                    <td><input id="tConsidered1" type ="text" disabled="disabled"/></td>
                                                </tr>
                                               <tr>
                                                    <td><input id="tLimit2" type ="text"disabled="disabled"/></td>
                                                    <td><input id="tInterest2" type ="text" disabled="disabled"/></td>
                                                    <td><input id="tConsidered2" type ="text" disabled="disabled"/></td>
                                                </tr>
                                                 <tr>
                                                    <td><input id="tLimit3" type ="text"disabled="disabled"/></td>
                                                    <td><input id="tInterest3" type ="text" disabled="disabled"/></td>
                                                    <td><input id="tConsidered3" type ="text" disabled="disabled"/></td>
                                                </tr>
                                              

                                        </table>
                                    </div>
                                </div>
                                

                                <div class="clear"></div>
                                
                                <div style="text-align:left; font-size:medium;font-weight:bold;padding: 2px; margin-top: 5px;">OFF-SCB                                
                                    <div style="border:none;">
                                        <table class="tableStyle" id="Table2" >
                                               <tr class="theader">
                                                   <td >BANK/FI</td>
                                                   <td >LIMIT</td>
                                                   <td id="td2">INTEREST</td>
                                                   <td >CONSIDERED</td>
                                                   
                                               </tr>
                                               <tr>
                                                    <td>
                                                        <select id="cmbBank1" disabled="disabled"></select>
                                                    </td>
                                                    <td><input id="tOffLimit1" type ="text"disabled="disabled"/></td>
                                                    <td><input id="tOffInterest1" type ="text" disabled="disabled"/></td>
                                                    <td><input id="tOffConsidered1" type ="text" disabled="disabled"/></td>
                                                </tr>
                                               <tr>
                                                    <td>
                                                        <select id="cmbBank2" disabled="disabled"></select>
                                                    </td>
                                                    <td><input id="tOffLimit2" type ="text"disabled="disabled"/></td>
                                                    <td><input id="tOffInterest2" type ="text" disabled="disabled"/></td>
                                                    <td><input id="tOffConsidered2" type ="text" disabled="disabled"/></td>
                                                </tr>
                                               <tr>
                                                    <td>
                                                        <select id="cmbBank3" disabled="disabled"></select>
                                                    </td>
                                                    <td><input id="tOffLimit3" type ="text"disabled="disabled"/></td>
                                                    <td><input id="tOffInterest3" type ="text" disabled="disabled"/></td>
                                                    <td><input id="tOffConsidered3" type ="text" disabled="disabled"/></td>
                                                </tr>
                                               <tr>
                                                    <td>
                                                        <select id="cmbBank4" disabled="disabled"></select>
                                                    </td>
                                                    <td><input id="tOffLimit4" type ="text"disabled="disabled"/></td>
                                                    <td><input id="tOffInterest4" type ="text" disabled="disabled"/></td>
                                                    <td><input id="tOffConsidered4" type ="text" disabled="disabled"/></td>
                                                </tr>
                                        </table>
                                        <input type="hidden" id="Hidden1" />
<input type="hidden" id="Hidden2" />
                                    </div>
                                </div>
                               
         
          
       </div>
       <div class="divLeftBig centerAlign" style="margin-left: 5px; margin-bottom: 0px; width: 365px" >
           
           
           
           
                                            <span style="font-weight: bold; font-family: verdana;">CONDITIONS [+]</span> 
                                            
                                            <table class="tableStyle" id="Table4" >
                                               <tr>
                                                    <td class="widthSize90_per" style="width: 90%;">
                                                        <select id="cmbCondition1" disabled="disabled"></select>
                                                    </td>
                                                    <%--<td class="widthSize10_per"> <input id="chkCondition1" type="checkbox" onclick="ApproverHelper.disableApprovedButton('chkCondition1');" /></td>--%>
                                                   
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <select id="cmbCondition2" disabled="disabled" ></select>
                                                    </td>
                                                    <%--<td><input id="chkCondition2" type="checkbox"  onclick="ApproverHelper.disableApprovedButton('chkCondition1');"/></td>--%>
                                                   
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <select id="cmbCondition3" disabled="disabled" ></select>
                                                    </td>
                                                    <%--<td><input id="chkCondition3" type="checkbox"  onclick="ApproverHelper.disableApprovedButton('chkCondition1');"/></td>--%>
                                                   
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <select id="cmbCondition4" disabled="disabled" ></select>
                                                    </td>
                                                    <%--<td><input id="chkCondition4" type="checkbox"  onclick="ApproverHelper.disableApprovedButton('chkCondition1');"/></td>--%>
                                                   
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <select id="cmbCondition5" disabled="disabled" ></select>
                                                    </td>
                                                    <%--<td><input id="chkCondition5" type="checkbox"  onclick="ApproverHelper.disableApprovedButton('chkCondition1');"/></td>--%>
                                                   
                                                </tr>

                                        </table>
                                            <div class="fieldDiv">
                            <textarea id="txtCondition1" class="txtFieldBig widthSize100_per" cols="1" rows="3"style="height: 55px;" disabled="disabled" ></textarea>
                        </div>
                        <div class="fieldDiv">
                            <textarea id="txtCondition2" class="txtFieldBig widthSize100_per" cols="1" rows="3"style="height: 55px;" disabled="disabled"></textarea>
                        </div>
                                            
                                            
                                        
          <div class="clear"></div>
                                      
          </div>  
          
          <br/>
        <div class="divLeftBig centerAlign" style="margin-left: 5px; margin-bottom: 0px; margin-top: 10px; width: 365px" >
           
             
           
                                            
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;">ANLYST REMARKS</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;">
                                                        <textarea id="tAnalystRemark" disabled="disabled" cols="20" class ="txtBoxSize_H22 widthSize100_per" rows="10" style="height: 105px;*height: 105px;_height: 105px;"></textarea></td>
                                               </tr>
                                           </table>
                                        <div class="clear"></div>
       </div>
                                         
          <div class="clear"></div>
                                        <br/>
                                        <div class="divLeftBig" style="width :370px;*width :370px;_width :370px; margin: 0px; ">
                                            <span style="font-weight: bold; font-family: verdana;">STRENGTH [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="cmbStrength1" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="cmbStrength2" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="cmbStrength3" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="cmbStrength4" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="cmbStrength5" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                        </table>
                                        <div class="fieldDiv">
                            <textarea id="txtStrength1" class="txtFieldBig widthSize100_per" cols="1" rows="3"style="height: 55px;" disabled="disabled" ></textarea>
                        </div>
                        <div class="fieldDiv">
                            <textarea id="txtStrength2" class="txtFieldBig widthSize100_per" cols="1" rows="3"style="height: 55px;" disabled="disabled"></textarea>
                        </div>
                                        <div class="clear"></div>
                                         </div>

                                        <div class="divLeftBig" style="width :370px;*width :370px;_width :365px; margin: 0px;margin-left: 5px; ">
                                            <span style="font-weight: bold; font-family: verdana;">WEAKNESS [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="cmbWeakness1" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="cmbWeakness2" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="cmbWeakness3" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="cmbWeakness4" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                                <tr>
                                                   <td><select id="cmbWeakness5" disabled="disabled" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                        </table>
                                        <div class="fieldDiv">
                            <textarea id="txtWeakness1" class="txtFieldBig widthSize100_per" cols="1" rows="3"style="height: 55px;" disabled="disabled" ></textarea>
                        </div>
                        <div class="fieldDiv">
                            <textarea id="txtWeakness2" class="txtFieldBig widthSize100_per" cols="1" rows="3"style="height: 55px;" disabled="disabled"></textarea>
                        </div>
                                        <div class="clear"></div>
                                         </div>
                                         <div class="clear"></div>
                                         <br/>
        
        
        
        <div class="divLeftBig leftAlign" style="margin-left: 5px; margin-bottom: 0px; width: 750px;*width: 750px;_width: 750px;" >
           
           
           
           
                                            <span style="font-weight: bold; font-family: verdana;" >APPROVER COMMENTS:</span> 
                                            
                                        <textarea id="tApproverRemark" cols="20" class ="txtBoxSize_H22 widthSize100_per" rows="20" style="height: 150px;*height: 150px;_height: 150px;"></textarea>
          <div class="clear"></div>
                                      
          </div> 
        
        <div class="divLeftBig centerAlign" style="margin-left: 5px; margin-bottom: 0px; width: 750px;*width: 750px;_width: 750px;" >
            <input id="btnBackToAnalyst" type="button" value="Back To Analyst" class="btnApp" />&nbsp;&nbsp;
            <input id="btnApprove" type="button" value="Approve" class="btnApp" />&nbsp;&nbsp;
            <input id="btnApproveWithCondition" type="button" class="btnApp" value="Approve With Condition" />&nbsp;&nbsp;
            <input id="btnReject" type="button" value="Reject" class="btnApp" style="display:none;" />
            <input id="btnDecline" type="button" value="Decline" class="btnApp" style="display:none;" />
            <input id="btnCancel" type="button" class="btnApp" value="Cancel" />
           <div class="clear"></div>
                                      
          </div> 
            
     
            
            
    </div>
    <div class="clear"></div>
</asp:Content>

