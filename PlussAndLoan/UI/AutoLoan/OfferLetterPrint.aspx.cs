﻿using System;
using System.Collections.Generic;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Entity.CheckListPrint;
using AutoLoanService.Interface;
using CrystalDecisions.CrystalReports.Engine;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class OfferLetterPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int llid = 0;

            llid = Convert.ToInt32(Request.QueryString["llid"]);

            if (llid > 0)
            {
                ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
                var loanApplicationService = new LoanApplicationService(repository);
                AutoLoanApplicationAll res = new AutoLoanApplicationAll();
                var res1 = loanApplicationService.getLoanLocetorInfoByLLID(llid);
                res = (AutoLoanApplicationAll)res1;
                setValueToControl(res);
                ConfigureCReport();
            }
        }

        private void setValueToControl(AutoLoanApplicationAll res)
        {
            if (res.objAutoLoanMaster.JointApplication)
            {
                lName.Text = res.objAutoPRApplicant.PrName + " & " + res.objAutoJTApplicant.JtName;
            }
            else {
                lName.Text = res.objAutoPRApplicant.PrName;
            }

            lProductId.Text = res.objAutoLoanMaster.ProductId.ToString();

            lAddress.Text = res.objAutoPRApplicant.MailingAddress;
            lTotalLaon.Text = "BDT " + res.objAuto_An_LoanCalculationTotal.TotalLoanAmount.ToString();
            lOfferedLoanAmount.Text = "BDT " + res.objAuto_An_LoanCalculationTotal.TotalLoanAmount.ToString();

            lCoreLoanAmount.Text =
                Convert.ToString(res.objAuto_An_LoanCalculationTotal.TotalLoanAmount -
                                 res.objAuto_An_LoanCalculationInsurance.ARTA -
                                 res.objAuto_An_LoanCalculationInsurance.GeneralInsurance);
            lLifeInsurance.Text = res.objAuto_An_LoanCalculationInsurance.ARTA.ToString();
            lVehicleInsurance.Text = res.objAuto_An_LoanCalculationInsurance.GeneralInsurance.ToString();
            lTotalLoanAmount.Text = "BDT " + res.objAuto_An_LoanCalculationTotal.TotalLoanAmount.ToString();
            lTenor.Text = res.objAuto_An_LoanCalculationTenor.ConsideredTenor.ToString();
            lInterestRate.Text = res.objAuto_An_LoanCalculationInterest.ConsideredRate.ToString();

            var a = (res.objAuto_An_LoanCalculationInterest.ConsideredRate / 100) *
                    res.objAuto_An_LoanCalculationTenor.ConsideredTenor * 12;
            var b = res.objAuto_An_LoanCalculationTotal.TotalLoanAmount / a;

            lMonthlyInstalment.Text = b.ToString("#.##");
            //if (res.AutoPRBankStatementList != null && res.AutoPRBankStatementList.Count > 0)
            //{
            //    lBankStatementName.Text = res.AutoPRBankStatementList[0].BANK_NM;
            //}

            if (res.objAuto_An_FinalizationRepaymentMethod.Instrument == 2)
            {
                lSIFrom.Text = "SI";
                lSIFromAccountNo.Text = res.objAuto_An_FinalizationRepaymentMethod.SCBAccount;
                lSIFromBankName.Text = "SCB";
            }
            else
            {
                lSIFrom.Text = "PDC";
                lSIFromAccountNo.Text = res.objAuto_An_FinalizationRepaymentMethod.AccountNumber;
                lSIFromBankName.Text = res.objAuto_An_FinalizationRepaymentMethod.BankName;
            }
            Label2.Text = res.objAutoLoanMaster.LLID.ToString();

            string condition = "";
//            for (var i = 0; i < res.Auto_An_FinalizationConditionList.Count; i++)
//            {
//                condition += string.Format(@"<tr><td class='style7'><table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse;width: 26pt' width='35'>
//                        <colgroup><col style='mso-width-source: userset; mso-width-alt: 1280; width: 26pt' width='35' /></colgroup><tr height='22' style='height: 16.5pt'>
//                            <td class='style6' height='22' width='35'>q</td></tr></table></td><td>{0}</td></tr>", res.Auto_An_FinalizationConditionList[i].Condition);
//            }
            //hdnCondition.Text = res.Auto_An_FinalizationConditionList.ToString();

            Session["OriginalCondition"] = res.Auto_An_FinalizationConditionList;


        }

        protected void bPrint_Click(object sender, EventArgs e)
        {
            if (bPrint.Text == "Print")
            {
                bPrint.Text = "Back";
                ConfigureCReport();
                //CrystalPanel.Visible = true;
                mainPanel.Visible = false;
            }
            else
            {
                bPrint.Text = "Print";
                //CrystalPanel.Visible = false;
                mainPanel.Visible = true;
            }
        }

        private void ConfigureCReport()
        {
            OfferLatterPrint olp = new OfferLatterPrint();
            List<OfferLatterPrint> olpList = new List<OfferLatterPrint>();
            olp.PDate = Label1.Text;
            olp.lProductId = lProductId.Text;
            olp.Ref = Label2.Text;
            olp.lName = lName.Text;
            olp.lAddress = lAddress.Text;
            olp.lTotalLaon = lTotalLaon.Text;
            olp.lOfferedLoanAmount = lOfferedLoanAmount.Text;
            olp.lCoreLoanAmount = lCoreLoanAmount.Text;
            olp.lLifeInsurance = lLifeInsurance.Text;
            olp.lVehicleInsurance = lVehicleInsurance.Text;
            olp.lTotalLoanAmount = lTotalLoanAmount.Text;
            olp.lTenor = lTenor.Text;
            olp.lInterestRate = lInterestRate.Text;
            olp.lMonthlyInstalment = lMonthlyInstalment.Text;
            olp.lSIFrom = lSIFrom.Text;
            olp.lSIFromAccountNo = lSIFromAccountNo.Text;
            olp.lSIFromBankName = lSIFromBankName.Text;
            olp.lBankStatementName = lBankStatementName.Text;
            olp.Label10 = Label10.Text;
            olp.Label11 = Label11.Text;
            olp.Label12 = Label12.Text;
            olp.PDate = DateTime.Now.ToString("dd/MM/yyyy");
            olp.Ref = Label2.Text;

            //olpList.Add(olp);


            ReportDocument rd = new ReportDocument();
            string path = Server.MapPath("PrintCheckList/OfferLatter.rpt");
            rd.Load(path);
            rd.SetDataSource(olpList);
            //rd.SetParameterValue("TruckSerialNumber", "Arifuzzaman");
           // crViewer.ReportSource = rd;
            

            //rd.PrintToPrinter(1, true, 0, 0);
            //ReportDocument rd = new ReportDocument();
            //string path = Server.MapPath("PrintCheckList/OfferLatter.rpt");
            //rd.Load(path);
            //rd.SetDataSource(olpList);
            ////rd.SetParameterValue("TruckSerialNumber", "Arifuzzaman");
            //crViewer.ReportSource = rd;
            ////rd.PrintToPrinter(1, true, 0, 0);

            Session["OfferLatterObject"] = olp;
            //Response.Redirect("OfferLetterPrintCrystal.aspx");

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            OfferLatterPrint b2 = new OfferLatterPrint();
            b2 = (OfferLatterPrint)Session["OfferLatterObject"];

            List<Auto_An_FinalizationCondition> originalCondition = (List<Auto_An_FinalizationCondition>)Session["OriginalCondition"];

            var orgcon = "";
            if(originalCondition.Count>0){
                for(var i=0; i<originalCondition.Count; i++){
                    if(orgcon == ""){
                        orgcon += originalCondition[i].Condition;
                    }
                    else{
                        orgcon += "\n" + originalCondition[i].Condition;
                    }
                }
            }
            
            b2.DynamicCondition = orgcon;

            b2.SatisfactoryScbBank = txtSatisFactory.Value;
            b2.Label10 = txtCompanyName.Value;
            b2.Label11 = txtSettlementOf.Value;
            b2.Label12 = txtSettlementWith.Value;

            var givenCon = "";
            if (tareaCondition1.Value != "") {
                givenCon = tareaCondition1.Value;
            }
            if (tareaCondition2.Value != "")
            {
                if (givenCon == "")
                {
                    givenCon = tareaCondition2.Value;
                }
                else {
                    givenCon += "\n" + tareaCondition2.Value;
                }
            }
            if (tareaCondition3.Value != "")
            {
                if (givenCon == "")
                {
                    givenCon = tareaCondition3.Value;
                }
                else
                {
                    givenCon += "\n" + tareaCondition3.Value;
                }
            }
            if (tareaCondition4.Value != "")
            {
                if (givenCon == "")
                {
                    givenCon = tareaCondition4.Value;
                }
                else
                {
                    givenCon += "\n" + tareaCondition4.Value;
                }
            }
            if (tareaCondition5.Value != "")
            {
                if (givenCon == "")
                {
                    givenCon = tareaCondition5.Value;
                }
                else
                {
                    givenCon += "\n" + tareaCondition5.Value;
                }
            }

            b2.NewGivenCondition = givenCon;
            Session["OfferLatterObject"] = b2;

            Response.Redirect("OfferLetterPrintCrystal.aspx");
        }
    }
}
