﻿using System;
using System.Collections.Generic;
using System.IO;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{

    public partial class VendorMOUSettings : System.Web.UI.Page
    {
        [AjaxNamespace("VendorMOUSettings")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(VendorMOUSettings));
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVendorMOU> GetAutoVendorMOUSummary(int pageNo, int pageSize, string vendorName)
        {
            pageNo = pageNo * pageSize;

            AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new AutoVendorMOUService(repository);


            List<AutoVendorMOU> objList_AutoVendorMOU =
                autoVendorMOUService.GetAutoVendorMOUSummary(pageNo, pageSize, vendorName);
            return objList_AutoVendorMOU;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAuto_VendorMOU(AutoVendorMOU autoVendorMOU)
        {
            AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new AutoVendorMOUService(repository);
            var res = autoVendorMOUService.SaveAuto_VendorMOU(autoVendorMOU);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveVendorMOU()
        {
            //string returnValue = autoVendorMOU.VendorName;
            return "Success";
        }



        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string AutoVendorMOUDeleteById(int VendorMouId)
        {
            AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new AutoVendorMOUService(repository);
            var res = autoVendorMOUService.AutoVendorMOUInactiveById(VendorMouId);
            return res;

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> GetStatus(int statusID)
        {
            AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new AutoVendorMOUService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVendorMOUService.GetStatus(statusID);
            return objList_AutoStatus;
        }



        protected void btnUploadSave_Click(object sender, EventArgs e)
        {
            var res = uploadFile();
            if (res == "Upload success" || res == "Success")
            {
                string msg = "Operation Successfull.";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");
            }
            else
            {
                string msg = "Operation partially completed. Found Error with follwoing rows (" + res + ")";

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");

            }
        }

        public string uploadFile()
        {
            string name = "";
            string returnValue = "";
            if (!fileDocumentUpload.FileContent.CanRead)
            {

                returnValue = "Please select Valid file";
            }
            else
            {
                try
                {
                    string serverPath = Request.PhysicalApplicationPath;
                    var directory = Server.MapPath(@"../Uploads/Auto");

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    DateTime date = DateTime.Now;
                    name = date.ToString("yyyyMMddHHmmss") + "_" +
                             fileDocumentUpload.FileName;
                    fileDocumentUpload.SaveAs(Path.Combine(directory, name));


                    returnValue = "Upload success";
                    returnValue = DumpExcelFile(Convert.ToString(Path.Combine(directory, name)));

                }
                catch (Exception ex)
                {
                    returnValue = "Upload Failed";
                }

            }
            return returnValue;
        }


        public string DumpExcelFile(string excelFilePath)
        {
            AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new AutoVendorMOUService(repository);
            var res = autoVendorMOUService.DumpExcelFile_VendorMOU(excelFilePath);
            return res;

        }




    }
}
