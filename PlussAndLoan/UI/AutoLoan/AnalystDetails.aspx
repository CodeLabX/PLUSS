<%@ Page Title="Pluss | Analyst Details" Language="C#" MasterPageFile="~/UI/AutoLoan/AutoAnalystMasterPage.master"
    AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.AnalystDetails" Codebehind="AnalystDetails.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <link href="../../CSS/Default.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/admin.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/TableLayoutForTabControls.css" rel="stylesheet" type="text/css" />
    
    <script src="../../Scripts/AutoLoan/AnalystSave.js" type="text/javascript"></script>

    <script src="../../Scripts/AutoLoan/SaveObject.js" type="text/javascript"></script>

    <script src="../../Scripts/AutoLoan/AnalystLoanCalculation.js" type="text/javascript"></script>
    <script src="../../Scripts/AutoLoan/AnalystFinalizationTabSave.js" type="text/javascript"></script>

    <script src="../../Scripts/AutoLoan/AnalystDetails.js" type="text/javascript"></script>

    <script src="../../Scripts/AutoLoan/AnalystDetailsCalculation.js" type="text/javascript"></script>

    <script src="../../Scripts/activatables.js"></script>

    <script src="../../Scripts/AutoLoan/AnalystHtmlHelper.js" type="text/javascript"></script>

    
    

    <%--<script src="../../Scripts/AutoLoan/LabelChange.js" type="text/javascript"></script>--%>
    <style type="text/css">
        #nav_AnalyzeLoanApplication a
        {
            background-color: #F2F6F6;
            color: #3DAE38;
        }
    </style>
    <script>
        Event.observe(window, 'load', function() {
        Calendar.setup({ inputField: 'txtReworkDate', ifFormat: '%d/%m/%Y',
                weekNumbers: false, button: 'newsDate'
            });
        });
        </script>
    
    <div id="maindivContent">
        <div class="divHeaderTitel">
            <h1 style="padding-top: 5px;*padding-top: 0px; *padding-bottom:10px; color: white;">
                <label id="lblLoan">
                    Loan</label>
                Application Assessment</h1>
            <input id="analystMasterIdHiddenField" type="hidden" />
        </div>
        <div style="width: 100%; *width: 100%; _width: 100%; border: solid 1px black; margin-bottom: 5px;">
            <div id="divLoanAppMasterData" class="fieldDiv divPadding formBackColor" style="margin-bottom: 0;">
                <div class="fieldDiv">
                    <label class="lblBig" style="font-size: medium; font-weight: bold; width: 200px;color: #333;">
                        CUSTOMER DETAILS</label>
                    <label class="lblBig" style="font-size: medium; font-weight: bold; text-align: right;">
                        LLID:</label>
                    <input type="text" id="txtLLID" class="txtField" runat="server" disabled="disabled" />
                    <label id="spnWait" runat="server" style="color: red; font-weight: bold;">
                    </label>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="divLeft" class="divLeftBig" style="margin-left: 5px;">
            <div class="fieldDiv">
                <label class="lbl">
                    Primary Applicant:</label>
                <input id="txtPrimaryApplicant" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Date of Birth:</label>
                <input id="txtDateOfBirth" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Current Age:</label>
                <input id="txtCurrenAge" type="text" class="txtFieldBig" disabled="disabled" /><br />
                <label id="lblFieldrequiredforPrAge" style="color: Red; font-weight: bold;">
                </label>
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Primary Profession:</label>
                <input id="txtPrimaryProfession" type="text" class="txtFieldBig" disabled="disabled" />
                <input id="txtPrProfessionId" type="hidden" />
                <input id="txtPrOtherProfessionId" type="hidden" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Other Profession:</label>
                <input id="txtOtherProfession" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Declared Income:</label>
                <input id="txtDeclaredIncome" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl" style="height: 50px; *height: 50px; _height: 50px; text-align: left;
                    vertical-align: top;">
                    Verified Address:</label>
                <textarea id="txtVerifiedAddress" class="txtFieldBig" cols="1" rows="3" style="height: 58px;
                    _height: 58px; *height: 58px;" disabled="disabled"></textarea>
            </div>
            <div class="clear">
            </div>
        </div>
        <div id="divForJointApplicant" class="divRightBig" style="margin-left: 5px;">
            <div class="fieldDiv">
                <label class="lbl">
                    Joint Applicant:</label>
                <input id="txtJointApplicant" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Date of Birth:</label>
                <input id="txtJtDateOfBirth" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Current Age:</label>
                <input id="txtJtCurrntAge" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Profession:</label>
                <input id="txtJtProfession" type="text" class="txtFieldBig" disabled="disabled" />
                <input id="txtJtProfessionId" type="hidden" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Declared Income:</label>
                <input id="txtJtDeclaredIncome" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Reason of Joint Applicant:</label>
                <select id="cmbReasonofJointApplicant" class="txtFieldBig comboSizeActive">
                    <option value="-1">Select a reason</option>
                    <option value="1">Income Consideration</option>
                    <option value="2">Joint Registration</option>
                    <option value="3">Business is in Joint Name</option>
                    <option value="4">Customer is Mariner</option>
                    <option value="5">AC is in Joint Name</option>
                    <option value="6">Others</option>
                </select>
            </div>
            <div class="clear">
            </div>
            <!-- -----------------------------Tab Segment ----------------------------------------------------------------------------- -->
        </div>
        <div id="Div67" class="divRightBig" style="margin-left: 5px; margin-top: 10px; border: 0px;">
        </div>
        <div class="clear">
        </div>
        <div id="mainTabForAnalyse" style="xdisplay: none;">
            <ol id="toc" class="toc">
                <li><a href="#tabGeneral"><span>General</span></a></li>
                <li><a href="#tabIncomeGenerator"><span>Income Generator</span></a></li>
                <li><a href="#tabLoanCalculator"><span>
                    <label id="lblLoanMaintab">
                        Loan</label>
                    Calculator</span></a></li>
                <li><a href="#tabFinalization"><span>Finalization</span></a></li>
            </ol>
            <!-- -----------------  TAB (a.1) � General (Application & Vehicle): Start ------------------------------ -->
            <div class="content" id="tabGeneral" style="padding-left: 0px; width: 99%; *border:0px;">
                <!--Application Details start -->
                <div id="mainDiv" style="width: 49%; margin-left: 2px; float: left; position: relative;">
                    <div class="divHeadLine">
                        <span>Application Detail</span></div>
                    <div class="groupContainer" style="padding-left: 0px;">
                        <input id="txtAutoLoanMasterId" type="hidden" value="0" />
                        <input id="txtIsJoint" type="hidden" value="0" />
                        <input id="txtAnalystMasterID" type="hidden" value="0" />
                        <input id="txtloanAnalystApplicationID" type="hidden" value="0" />
                        <input id="txtVehicleDetailID" type="hidden" value="0" />
                        <input id="txtAutoLoanVehicleId" type="hidden" value="0" />
                        <input id="txtApplicantSCBAccountID" type="hidden" value="0" />
                        <input id="txtPRAccount_detailsID" type="hidden" value="0" />
                        <input id="txtLoanFacilityID" type="hidden" value="0" />
                        <input id="txtLoanSecurityID" type="hidden" value="0" />
                        <input id="txtLoanFacilityOFFSCBID" type="hidden" value="0" />
                        <input type="hidden" id="txtVendorwithArta" />
                        <input type="hidden" id="txtVendorwithoutArta" />
                        <input type="hidden" id="txtaccidentOrTheft" />
                        <input type="hidden" id="txtCommision" />
                        <input type="hidden" id="txtVat" />
                        <input type="hidden" id="txtPassenger" />
                        <input type="hidden" id="txtAmountofDriver" />
                        <input type="hidden" id="txtStampCharge" />
                        <div id="div1" class="divLeft" style="width: 170px; border:0px;">
                            <div class="divLbl">
                                <label class="lblSize">
                                    Source (1):</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Branch Name:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Product Name (2):</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Value Pack:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Borrowing Relationship:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Customer Status:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Asking Interest:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Asking Tenor(Month):</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Asking Repayment Method:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    ARTA:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    ARTA Preference:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    General Insurance:</label></div>
                                    <div class="divLbl">
                        <label class="lblSize">
                            General Insurance Preference</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    100% Cash Secured:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    CC Bundle:</label></div>
                            <div class="divLbl" id="divSecurityValue" style="display: none;">
                                <label class="lblSize">
                                    Security Value:</label></div>
                        </div>
                        <div id="JApplicantControldiv" class="divRight" style="border:0px;">
                            <div class="fieldDivContaint">
                                <select id="cmbSource1" class="comboSize comboSizeActive">
                                    <option value="-1">Select a Status</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                                *height: 25px;">
                                <div style="width: 100%;">
                                    <div class="divLeft" style="width: 60%; *width: 60%; _width: 60%; padding: 0px; border: none;">
                                        <select id="cmbBranch" class="comboSize comboSizeActive">
                                            <option value="-1">Select a source</option>
                                        </select></div>
                                    <div class="divMiddile" style="width: 36%; *width: 36%; _width: 34%; padding: 0px;
                                        border: none;">
                                        <input id="txtBranchCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                                </div>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbProductName" class="comboSize" disabled="disabled">
                                    <option value="-1">Select a Status</option>
                                    <option value="1">Staff Auto</option>
                                    <option value="2">Conventional Auto</option>
                                    <option value="3">Saadiq Auto</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbValuePack" class="comboSize" disabled="disabled">
                                    <option value="-1">Select a Status</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                                *height: 25px;">
                                <div style="width: 100%;">
                                    <div class="divLeft" style="width: 60%; *width: 60%; _width: 60%; padding: 0px; border: none;">
                                        <select id="cmbBrowingRelationship" class="comboSize comboSizeActive">
                                            <option value="-1">Select a source</option>
                                            <option value="FTMB">First Time Borrower</option>
                                            <option value="NFTB">NON FTB</option>
                                            <option value="TMOB">12 MOB</option>
                                            <option value="TFMB">24 MOB</option>
                                        </select></div>
                                    <div class="divMiddile" style="width: 36%; *width: 36%; _width: 34%; padding: 0px;
                                        border: none;">
                                        <input id="txtRelationshipCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                                </div>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbCustomerStatus" class="comboSize comboSizeActive">
                                    <option value="-1">Select a Status</option>
                                    <option value="1">Regular</option>
                                    <option value="2">Preferred</option>
                                    <option value="3">Priority</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <input type="text" id="txtAskingInterest" class="txtBoxSize comboSizeActive" />
                            </div>
                            <div class="fieldDivContaint">
                                <input type="text" id="txtAskingtenor" class="txtBoxSize comboSizeActive" />
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbAskingRepaymentmethod" class="comboSize comboSizeActive">
                                    <option value="-1">Select a Status</option>
                                    <option value="1">PDC</option>
                                    <option value="2">SI</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbARTA" class="comboSize comboSizeActive">
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                    <option value="3">Cash</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbARTAReference" class="comboSize comboSizeActive">
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbGeneralInsurence" class="comboSize comboSizeActive">
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                    <option value="3">Cash</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbGeneralInsurencePreference" class="comboSize comboSizeActive">
                                    <option value="-1">Select a Status</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbCashSecured" class="comboSize comboSizeActive">
                                    <option value="-1">Select a Status</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbCCBundle" class="comboSize comboSizeActive" >
                                    <option value="-1">Select a Status</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint" id="divSequrityvaluetextbox" style="display: none;">
                                <input type="text" id="txtSecurityValue" class="comboSize comboSizeActive" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <br />
                    <br />
                </div>
                <!--End of Application Detailsd -->
                <!--Vehicle Details start -->
                <div id="Div10" style="width: 47%; _width: 47%; *width: 47%; float: right;">
                    <div class="divHeadLine">
                        <span>Vehicle Details</span></div>
                    <div class="groupContainer">
                        <div id="div12" class="divLeft" style="width: 125px;border:0px;">
                            <div class="divLbl">
                                <label class="lblSize">
                                    Vendor:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    New Vendor:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Manufacturer/Brand:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Vehicle Model:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Trim Lebel:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Engine Capacity:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Vehicle Type:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Vehicle Status:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Manufacturing Year:</label></div>
                            <div class="divLbl" id="divmanufacalertlbl" style="display: none;">
                                <label class="lblSize">
                                </label>
                            </div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Registration Year:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Value Pack Allowed:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Seating Capacity:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    MOU Vendor (3):</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Quoted Price:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Considered Price:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Price Considered on:</label></div>
                            <div class="divLbl">
                                <label class="lblSize">
                                    Car Verification:</label></div>
                            <div class="divLbl" id="divLblDeliveryStatus">
                                <label class="lblSize">
                                    Delivery Status:</label></div>
                        </div>
                        <div id="Div15" class="divRight" style="width: 235px; border:0px;">
                            <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                                *height: 25px;">
                                <div style="width: 100%;">
                                    <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                        <select id="cmbVendor" class="comboSize comboSizeActive">
                                            <option value="0">Select a Vendore</option>
                                        </select>
                                    </div>
                                    <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                        border: none;">
                                        <input id="txtVendoreCode" type="text" class="txtBoxSize" disabled="disabled" />
                                    </div>
                                </div>
                            </div>
                            <div class="fieldDivContaint">
                                <input id="txtVendorName" type="text" class="txtBoxSize" />
                            </div>
                            <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                                *height: 25px;">
                                <div style="width: 100%;">
                                    <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                        <select id="cmbManufacturer" class="comboSize comboSizeActive">
                                            <option value="0">Select a manufacturer</option>
                                        </select>
                                    </div>
                                    <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                        border: none;">
                                        <input id="txtManufacturarCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                                </div>
                            </div>
                            <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                                *height: 25px;">
                                <div style="width: 100%;">
                                    <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                        <select id="cmbModel" class="comboSize comboSizeActive">
                                            <option value="0">Select Model</option>
                                        </select>
                                        <input type="text" id="txtModelName" class="txtBoxSize" style="display:none;" />
                                        <input id="hdnModel" type="hidden" />
                                    </div>
                                    <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                        border: none;">
                                        <input id="txtVehicleModelCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                                </div>
                            </div>
                            <div class="fieldDivContaint">
                                <input type="text" id="txtTrimLevel" class="txtBoxSize" disabled="disabled" />
                            </div>
                            <div class="fieldDivContaint">
                                <input id="txtEngineCapacityCC" type="text" class="txtBoxSize" disabled="disabled" />
                            </div>
                            <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                                *height: 25px;">
                                <div style="width: 100%;">
                                    <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                        <input type="text" id="txtVehicleType" class="txtBoxSize" disabled="disabled" />
                                        <select id="cmbVehicleType" class="comboSize" title="Vehicle Type" style="display:none;">
		                                    <option value="-1">Select From List</option>
		                                    <option value="S1">Sedan</option>
		                                    <option value="S2">Station wagon</option>
		                                    <option value="S3">SUV</option>
		                                    <option value="S4">Pick-Ups</option>
		                                    <option value="S5">Microbus</option>
		                                    <option value="S6">MPV</option>
		                                </select>
                                    </div>
                                    <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                        border: none;">
                                        <input id="txtVehicletypeCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                                </div>
                            </div>
                            <div class="fieldDivContaint" style="margin-bottom: 5px; height: 25px; _height: 25px;
                                *height: 25px;">
                                <div style="width: 100%;">
                                    <div class="divLeft" style="width: 73%; *width: 73%; _width: 73%; padding: 0px; border: none;">
                                        <select id="cmdVehicleStatus" class="comboSize comboSizeActive">
                                            <option value="0">Select Vehicle Status</option>
                                        </select>
                                    </div>
                                    <div class="divMiddile" style="width: 23%; *width: 23%; _width: 22%; padding: 0px;
                                        border: none;">
                                        <input id="txtVehicleStatusCode" type="text" class="txtBoxSize" disabled="disabled" /></div>
                                </div>
                            </div>
                            <div class="fieldDivContaint">
                                <input type="text" id="txtManufacturaryear" class="txtBoxSize comboSizeActive" /><br />
                            </div>
                            <div class="fieldDivContaint" id="divmanuyearalerttext" style="display: none;">
                                <label id="lblManufactureYearAlert" style="color: red; font-weight: bold;">
                                </label>
                            </div>
                            <div class="fieldDivContaint">
                                <input type="text" id="txtregistrationYear" class="txtBoxSize" disabled="disabled" />
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbvaluePackAllowed" class="comboSize" disabled="disabled">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmsSeatingCapacity" class="comboSize comboSizeActive">
                                    <option value="-1">Select Seating Capacity</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbVendoreRelationShip" class="comboSize" disabled="disabled">
                                    <option value="1">MOU</option>
                                    <option value="0">NON-MOU</option>
                                    <option value="2">Negative Listed</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <input type="text" id="txtQuotedPrice" class="txtBoxSize comboSizeActive" />
                            </div>
                            <div class="fieldDivContaint">
                                <input type="text" id="txtConsideredprice" class="txtBoxSize comboSizeActive" />
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbPriceConsideredOn" class="comboSize comboSizeActive">
                                    <option value="-1">Select a Price Considered</option>
                                    <option value="1">Index Value</option>
                                    <option value="2">Phone</option>
                                    <option value="3">Price Verify</option>
                                    <option value="4">Quotation</option>
                                    <option value="5">NA</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint">
                                <select id="cmbCarVerification" class="comboSize comboSizeActive">
                                    <option value="0">Select Verification Type</option>
                                    <option value="1">Done</option>
                                    <option value="2">Valued & Verified</option>
                                    <option value="3">Not Required</option>
                                </select>
                            </div>
                            <div class="fieldDivContaint" id="divDeliveryStatus" style="display: none;">
                                <select id="cmbDeliveryStatus" class="comboSize comboSizeActive">
                                    <option value="-1">Select a Status</option>
                                    <option value="1">Car Delivered</option>
                                    <option value="2">Car At Port</option>
                                    <option value="3">Car At Vendor</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <br />
                    <br />
                </div>
                <!--End of Vehicle Details -->
                <div class="clear">
                </div>
                <br />
                <!--Account Relationship start -->
                <div id="Div16" style="width: 100%; _width: 100%; *width: 100%; float: left;">
                    <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%;">
                        <span>Account Relationship</span></div>
                    <div class="groupContainer" style="width: 98%; *width: 98%; _width: 98%;">
                        <div class="divLeftBig" style="width: 315px; *width: 315px; _width: 315px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <div style="text-align: left; font-size: medium; font-weight: bold;">
                                            WITH SCB
                                            <div style="border: none;">
                                                <table class="tableStyle" id="tblApplicantAccountSCB">
                                                    <tr class="theader">
                                                        <td style="width: 35%;">
                                                            A/C NUMBER(S)
                                                        </td>
                                                        <td style="width: 35%;">
                                                            ACCOUNT STATUS
                                                        </td>
                                                        <td style="width: 30%;">
                                                            PRITORY A/C HOLDER
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="txtAccountNumberForSCB1" type="text" class="comboSizeActive" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatusForSCB1" class="comboSizeActive">
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbPriorityAcHolderForSCB1" class="comboSizeActive">
                                                                <option value="1">Yes</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="txtAccountNumberForSCB2" type="text" class="comboSizeActive" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatusForSCB2" class="comboSizeActive">
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbPriorityAcHolderForSCB2" class="comboSizeActive">
                                                                <option value="1">Yes</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="txtAccountNumberForSCB3" type="text" class="comboSizeActive" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatusForSCB3" class="comboSizeActive">
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbPriorityAcHolderForSCB3" class="comboSizeActive">
                                                                <option value="1">Yes</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="txtAccountNumberForSCB4" type="text" class="comboSizeActive" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatusForSCB4" class="comboSizeActive" >
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbPriorityAcHolderForSCB4" class="comboSizeActive" >
                                                                <option value="1">Yes</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="txtAccountNumberForSCB5" type="text" class="comboSizeActive" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatusForSCB5" class="comboSizeActive" >
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbPriorityAcHolderForSCB5" class="comboSizeActive" >
                                                                <option value="1">Yes</option>
                                                                <option value="0">No</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="divRightBig" style="width: 500px; *width: 500px; _width: 500px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <div style="text-align: left; font-size: medium; font-weight: bold;">
                                            OTHER BANK
                                            <div style="border: none;">
                                                <table class="tableStyle" id="tblApplicantAccountOther">
                                                    <tr class="theader">
                                                        <td>
                                                            BANK
                                                        </td>
                                                        <td>
                                                            BRANCH
                                                        </td>
                                                        <td>
                                                            A/C NUMBER(S)
                                                        </td>
                                                        <td>
                                                            A/C STATUS
                                                        </td>
                                                        <td>
                                                            A/C TYPE
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select id="cmbBankNameForOther1" onchange="analystDetailsManager.GetBranch('cmbBankNameForOther1')" disabled="disabled">
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbBranchNameOther1" disabled="disabled">
                                                                <option value="-1">Select a Branch</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input id="txtAccountNumberother1" type="text" disabled="disabled" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatus1" disabled="disabled">
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountType1" disabled="disabled">
                                                                <option value="0">Select from List</option>
                                                                <option value="1">Current</option>
                                                                <option value="2">Savings</option>
                                                                <option value="3">Salary</option>
                                                                <option value="4">Others</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select id="cmbBankNameForOther2" onchange="analystDetailsManager.GetBranch('cmbBankNameForOther2')" disabled="disabled">
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbBranchNameOther2" disabled="disabled">
                                                                <option value="-1">Select a Branch</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input id="txtAccountNumberother2" type="text" disabled="disabled" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatus2" disabled="disabled">
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountType2" disabled="disabled">
                                                                <option value="0">Select from List</option>
                                                                <option value="1">Current</option>
                                                                <option value="2">Savings</option>
                                                                <option value="3">Salary</option>
                                                                <option value="4">Others</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select id="cmbBankNameForOther3" onchange="analystDetailsManager.GetBranch('cmbBankNameForOther3')" disabled="disabled">
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbBranchNameOther3" disabled="disabled">
                                                                <option value="-1">Select a Branch</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input id="txtAccountNumberother3" type="text" disabled="disabled" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatus3" disabled="disabled">
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountType3" disabled="disabled">
                                                                <option value="0">Select from List</option>
                                                                <option value="1">Current</option>
                                                                <option value="2">Savings</option>
                                                                <option value="3">Salary</option>
                                                                <option value="4">Others</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select id="cmbBankNameForOther4" onchange="analystDetailsManager.GetBranch('cmbBankNameForOther4')" disabled="disabled">
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbBranchNameOther4" disabled="disabled">
                                                                <option value="-1">Select a Branch</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input id="txtAccountNumberother4" type="text" disabled="disabled" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatus4" disabled="disabled">
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountType4" disabled="disabled">
                                                                <option value="0">Select from List</option>
                                                                <option value="1">Current</option>
                                                                <option value="2">Savings</option>
                                                                <option value="3">Salary</option>
                                                                <option value="4">Others</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <select id="cmbBankNameForOther5" onchange="analystDetailsManager.GetBranch('cmbBankNameForOther5')" disabled="disabled">
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbBranchNameOther5" disabled="disabled">
                                                                <option value="-1">Select a Branch</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input id="txtAccountNumberother5" type="text" disabled="disabled" />
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountStatus5" disabled="disabled">
                                                                <option value="0">Select Status</option>
                                                                <option value="1">Company</option>
                                                                <option value="2">Personal</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select id="cmbAccountType5" disabled="disabled">
                                                                <option value="0">Select from List</option>
                                                                <option value="1">Current</option>
                                                                <option value="2">Savings</option>
                                                                <option value="3">Salary</option>
                                                                <option value="4">Others</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <br />
                    <br />
                </div>
                <!--End of Account Relationship -->
                <div class="clear">
                </div>
                <!--Exposures start -->
                <%--<div id="Div17" style="width: 100%; _width: 100%; *width: 100%; float: left;">
                    <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%;">
                        <span>Exposures</span></div>
                    <div class="groupContainer" style="width: 98%; *width: 98%; _width: 98%;">
                        <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%; background-color: white;
                            text-align: left; border: none;">
                            <span style="color: black;">ON SCB</span></div>
                        <div class="divLeftBig" style="width: 500px; *width: 500px; _width: 500px;">
                            <div style="text-align: center; font-size: medium; font-weight: bold;">
                                TERM
                                <label id="lblLoanterms">
                                    LOANS</label>
                                <div style="border: none;">
                                    <table class="tableStyleSmall" id="tblFacilityTermLoan">
                                        <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                            <td style="width: 15%;">
                                                TYPE
                                            </td>
                                            <td>
                                                FLAG
                                            </td>
                                            <td>
                                                RATE
                                            </td>
                                            <td>
                                                LIMIT
                                            </td>
                                            <td>
                                                OUTSTANDING
                                            </td>
                                            <td id="tdEM1">
                                                EMI
                                            </td>
                                            <td id="tdEM2">
                                                EMI %
                                            </td>
                                            <td id="tdEM3">
                                                EMI SHARE
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbTypeForTermLoans1" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans1')">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtflag1" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <input id="txtInterestRate1" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtLimit1" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtoutStanding1" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('1')" />
                                            </td>
                                            <td>
                                                <input id="txtEMI1" type="text" onchange="analystDetailsHelper.calculateEMIShare('1')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIPer1" type="text" onchange="analystDetailsHelper.calculateEMIShare('1')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIShare1" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbTypeForTermLoans2" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans2')">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtflag2" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <input id="txtInterestRate2" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtLimit2" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtoutStanding2" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('2')" />
                                            </td>
                                            <td>
                                                <input id="txtEMI2" type="text" onchange="analystDetailsHelper.calculateEMIShare('2')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIPer2" type="text" onchange="analystDetailsHelper.calculateEMIShare('2')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIShare2" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbTypeForTermLoans3" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans3')">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtflag3" type="text" disable="disable" />
                                            </td>
                                            <td>
                                                <input id="txtInterestRate3" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtLimit3" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtoutStanding3" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('3')" />
                                            </td>
                                            <td>
                                                <input id="txtEMI3" type="text" onchange="analystDetailsHelper.calculateEMIShare('3')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIPer3" type="text" onchange="analystDetailsHelper.calculateEMIShare('3')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIShare3" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbTypeForTermLoans4" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans4')">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtflag4" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <input id="txtInterestRate4" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtLimit4" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtoutStanding4" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('4')" />
                                            </td>
                                            <td>
                                                <input id="txtEMI4" type="text" onchange="analystDetailsHelper.calculateEMIShare('4')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIPer4" type="text" onchange="analystDetailsHelper.calculateEMIShare('4')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIShare4" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="divColleterization">
                                    <label class="lblColleterization">
                                        COLLETERIZATION</label>
                                    <select id="cmbColleterization" class="comboSize widthSize22_per">
                                    </select>
                                    <label class="lblColleterizationWith">
                                        With</label>
                                    <select id="cmbColleterizationWith" class="comboSize widthSize22_per">
                                    </select>
                                </div>
                                <div style="text-align: center; font-size: medium; font-weight: bold;">
                                    CREDIT CARD
                                    <div style="border: none;">
                                        <table class="tableStyleSmall" id="tblFacilityCreditCard">
                                            <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                                <td style="width: 20%;">
                                                    TYPE
                                                </td>
                                                <td style="width: 20%;">
                                                    FLAG
                                                </td>
                                                <td style="width: 26%;">
                                                    ORIGINAL LIMIT
                                                </td>
                                                <td style="width: 34%;" id="tdinterest">
                                                    INTEREST
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbTypeForCreditCard1" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForCreditCard1')">
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="txtFlagForcreditCard1" type="text" disabled="disabled" />
                                                </td>
                                                <td>
                                                    <input id="txtOriginalLimitForCreditCard1" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                                </td>
                                                <td>
                                                    <input id="txtInterestForCreditCard1" type="text" disabled="disabled" /><input id="txtInterestRateCreditCard1"
                                                        type="hidden" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select id="cmbTypeForCreditCard2" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForCreditCard2')">
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="txtFlagForcreditCard2" type="text" disabled="disabled" />
                                                </td>
                                                <td>
                                                    <input id="txtOriginalLimitForCreditCard2" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                                </td>
                                                <td>
                                                    <input id="txtInterestForCreditCard2" type="text" disabled="disabled" /><input id="txtInterestRateCreditCard2"
                                                        type="hidden" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style="text-align: center; font-size: medium; font-weight: bold;">
                                    OVERDRAFT
                                    <div style="border: none;">
                                        <table class="tableStyleSmall" id="tblFacilityOverDraft">
                                            <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                                <td style="width: 20%;">
                                                    ORIGINAL LIMIT
                                                </td>
                                                <td style="width: 20%;">
                                                    FLAG
                                                </td>
                                                <td style="width: 26%;" id="tdinteresRate">
                                                    INTEREST RATE
                                                </td>
                                                <td style="width: 34%;" id="tdinteres1">
                                                    INTEREST
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="txtOverdraftlimit1" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('1')" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftFlag1" type="text" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftInterest1" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('1')" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftInteres1" type="text" disabled="disabled" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="txtOverdraftlimit2" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('2')" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftFlag2" type="text" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftInterest2" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('2')" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftInteres2" type="text" disabled="disabled" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="txtOverdraftlimit3" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('3')" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftFlag3" type="text" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftInterest3" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('3')" />
                                                </td>
                                                <td>
                                                    <input id="txtOverdraftInteres3" type="text" disabled="disabled" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="divRightBig" style="width: 320px; *width: 320px; _width: 320px;">
                            <div style="text-align: center; font-size: medium; font-weight: bold;">
                                SECURITIES
                                <div style="border: none;">
                                    <table class="tableStyle" id="tblOnSCBSecurityTermLoan">
                                        <tr class="theader">
                                            <td>
                                                STATUS
                                            </td>
                                            <td>
                                                TYPE
                                            </td>
                                            <td>
                                                SECURITY FV
                                            </td>
                                            <td>
                                                SECURITY PV
                                            </td>
                                            <td id="tdlt1">
                                                LTV
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txttermloanIssuingOffice1" />
                                                <input type="hidden" id="txttermloanIssuingDate1" />
                                                <input type="hidden" id="txttermloanInterestRate1" />
                                                <select id="cmbStatusForSecurities1" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequrities1">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFV1" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPV1" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('1')" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurity1" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txttermloanIssuingOffice2" />
                                                <input type="hidden" id="txttermloanIssuingDate2" />
                                                <input type="hidden" id="txttermloanInterestRate2" />
                                                <select id="cmbStatusForSecurities2" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequrities2">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFV2" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPV2" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('2')" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurity2" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txttermloanIssuingOffice3" />
                                                <input type="hidden" id="txttermloanIssuingDate3" />
                                                <input type="hidden" id="txttermloanInterestRate3" />
                                                <select id="cmbStatusForSecurities3" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequrities3">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFV3" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPV3" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('3')" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurity3" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txttermloanIssuingOffice4" />
                                                <input type="hidden" id="txttermloanIssuingDate4" />
                                                <input type="hidden" id="txttermloanInterestRate4" />
                                                <select id="cmbStatusForSecurities4" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequrities4">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFV4" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPV4" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('4')" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurity4" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="text-align: center; font-size: medium; font-weight: bold;">
                                <div style="border: none;">
                                    <table class="tableStyle" style="margin-top: 18px;" id="tblOnSCBSecurityCreditCard">
                                        <tr class="theader">
                                            <td>
                                                STATUS
                                            </td>
                                            <td>
                                                TYPE
                                            </td>
                                            <td>
                                                SECURITY FV
                                            </td>
                                            <td>
                                                SECURITY PV
                                            </td>
                                            <td id="tdlt2">
                                                LTV
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txtCreditCardIssuingOffice1" />
                                                <input type="hidden" id="txtCreditCardIssuingDate1" />
                                                <input type="hidden" id="txtCreditCardInterestRate1" />
                                                <select id="cmbStatusForSecuritiesForCreditCard1" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequritiesForcreditCard1">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFVForcreditCard1" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPVForCreditCard1" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurityForCreditCard1" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txtCreditCardIssuingOffice2" />
                                                <input type="hidden" id="txtCreditCardIssuingDate2" />
                                                <input type="hidden" id="txtCreditCardInterestRate2" />
                                                <select id="cmbStatusForSecuritiesForCreditCard2" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequritiesForcreditCard2">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFVForcreditCard2" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPVForCreditCard2" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurityForCreditCard2" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="text-align: center; font-size: medium; font-weight: bold;">
                                <div style="border: none;">
                                    <table class="tableStyle" style="margin-top: 18px;" id="tblOnSCBSecurityOverDraft">
                                        <tr class="theader">
                                            <td>
                                                STATUS
                                            </td>
                                            <td>
                                                TYPE
                                            </td>
                                            <td>
                                                SECURITY FV
                                            </td>
                                            <td>
                                                SECURITY PV
                                            </td>
                                            <td id="tdlt3">
                                                LTV
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txtOverDraftIssuingOffice1" />
                                                <input type="hidden" id="txtOverDraftIssuingDate1" />
                                                <input type="hidden" id="txtOverDraftInterestRate1" />
                                                <select id="cmbStatusForSecuritiesForoverDraft1">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequritiesForoverDraft1">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFVForoverDraft1" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPVForoverDraft1" type="text" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('1')" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurityForoverDraft1" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txtOverDraftIssuingOffice2" />
                                                <input type="hidden" id="txtOverDraftIssuingDate2" />
                                                <input type="hidden" id="txtOverDraftInterestRate2" />
                                                <select id="cmbStatusForSecuritiesForoverDraft2">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequritiesForoverDraft2">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFVForoverDraft2" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPVForoverDraft2" type="text" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('2')" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurityForoverDraft2" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" id="txtOverDraftIssuingOffice3" />
                                                <input type="hidden" id="txtOverDraftIssuingDate3" />
                                                <input type="hidden" id="txtOverDraftInterestRate3" />
                                                <select id="cmbStatusForSecuritiesForoverDraft3">
                                                    <option value="-1">Select a Status</option>
                                                    <option value="1">ACTIVE</option>
                                                    <option value="2">SETTLED</option>
                                                    <option value="3">TO BE CLOSED</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="cmbTypeForSequritiesForoverDraft3">
                                                    <option value="-1">Select a Security type</option>
                                                    <option value="1">FD (SCB)</option>
                                                    <option value="2">Ezee FD</option>
                                                    <option value="3">MSS (SCB)</option>
                                                    <option value="4">CESS (SCB)</option>
                                                    <option value="5">RFCD</option>
                                                    <option value="6">WEDB-SCB</option>
                                                    <option value="7">WEDB-OTHER</option>
                                                    <option value="8">ICB Unit</option>
                                                    <option value="9">USD Bond DPB</option>
                                                    <option value="10">USD Bond DIB</option>
                                                    <option value="11">MORTGAGE SECURITY</option>
                                                    <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtSecurityFVForoverDraft3" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtSecurityPVForoverDraft3" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtLTVForSecurityForoverDraft3" type="text" disabled="disabled" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <!-- OFF SCB start -->
                        <br />
                        <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%; background-color: white;
                            text-align: left; border: none;">
                            <span style="color: black;">OFF SCB</span></div>
                        <div class="divRightBig" style="width: 98%; *width: 98%; _width: 98%; float: left;
                            margin-left: 2px;">
                            <div style="text-align: center; font-size: medium; font-weight: bold;">
                                TERM
                                <label id="lblloanTerms1">
                                    LOANS</label>
                                <div style="border: none;">
                                    <table class="tableStyleSmall" id="tblOFFSCBFacilityTermLoan">
                                        <tr class="theader">
                                            <td>
                                                FINANCIAL INSTITUTION
                                            </td>
                                            <td>
                                                ORIGINAL LIMIT
                                            </td>
                                            <td id="tdEM4">
                                                EMI
                                            </td>
                                            <td id="tdEM5">
                                                EMI %
                                            </td>
                                            <td id="tdEM6">
                                                EMI SHARE
                                            </td>
                                            <td>
                                                STATUS
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForOffSCB1">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForOffScb1" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtEMIforOffSCB1" type="text" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('1')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIPerForOffSCB1" type="text" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('1')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIShareForOffSCB1" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForOffScb1" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForOffSCB2">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForOffScb2" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtEMIforOffSCB2" type="text" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('2')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIPerForOffSCB2" type="text" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('2')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIShareForOffSCB2" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForOffScb2" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForOffSCB3">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForOffScb3" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtEMIforOffSCB3" type="text" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('3')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIPerForOffSCB3" type="text" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('3')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIShareForOffSCB3" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForOffScb3" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForOffSCB4">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForOffScb4" type="text" />
                                            </td>
                                            <td>
                                                <input id="txtEMIforOffSCB4" type="text" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('4')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIPerForOffSCB4" type="text" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('4')" />
                                            </td>
                                            <td>
                                                <input id="txtEMIShareForOffSCB4" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForOffScb4" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="text-align: center; font-size: medium; font-weight: bold;">
                                CREDIT CARD
                                <div style="border: none;">
                                    <table class="tableStyleSmall" id="tblOFFSCBFacilityCreditCard">
                                        <tr class="theader">
                                            <td>
                                                FINANCIAL INSTITUTION
                                            </td>
                                            <td>
                                                ORIGINAL LIMIT
                                            </td>
                                            <td>
                                                INTEREST RATE
                                            </td>
                                            <td id="tdinterest2">
                                                INTEREST
                                            </td>
                                            <td>
                                                STATUS
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForCreditCardOffSCB1">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForcreditCardOffScb1" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(1)" />
                                            </td>
                                            <td>
                                                <input id="txtInterestRateForCreditCardForOffScb1" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(1)" />
                                            </td>
                                            <td>
                                                <input id="txtInterestForCreditCardForOffScb1" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForCreditCardOffScb1">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForCreditCardOffSCB2">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForcreditCardOffScb2" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(2)" />
                                            </td>
                                            <td>
                                                <input id="txtInterestRateForCreditCardForOffScb2" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(2)" />
                                            </td>
                                            <td>
                                                <input id="txtInterestForCreditCardForOffScb2" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForCreditCardOffScb2">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="text-align: center; font-size: medium; font-weight: bold;">
                                OVERDRAFT
                                <div style="border: none;">
                                    <table class="tableStyleSmall" id="tblOFFSCBFacilityOverDraft">
                                        <tr class="theader">
                                            <td>
                                                FINANCIAL INSTITUTION
                                            </td>
                                            <td>
                                                ORIGINAL LIMIT
                                            </td>
                                            <td id="tdInterestrate1">
                                                INTEREST RATE
                                            </td>
                                            <td id="tdinterest3">
                                                INTEREST
                                            </td>
                                            <td>
                                                STATUS
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForOverDraftOffSCB1">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForOverDraftOffScb1" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('1')" />
                                            </td>
                                            <td>
                                                <input id="txtinterestRateForOverDraftOffSCB1" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('1')" />
                                            </td>
                                            <td>
                                                <input id="txtInterestForOverDraftInterest1" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForOverDraftOffScb1">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForOverDraftOffSCB2">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForOverDraftOffScb2" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('2')" />
                                            </td>
                                            <td>
                                                <input id="txtinterestRateForOverDraftOffSCB2" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('2')" />
                                            </td>
                                            <td>
                                                <input id="txtInterestForOverDraftInterest2" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForOverDraftOffScb2">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="cmbFinancialInstitutionForOverDraftOffSCB3">
                                                </select>
                                            </td>
                                            <td>
                                                <input id="txtOriginalLimitForOverDraftOffScb3" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('3')" />
                                            </td>
                                            <td>
                                                <input id="txtinterestRateForOverDraftOffSCB3" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('3')" />
                                            </td>
                                            <td>
                                                <input id="txtInterestForOverDraftInterest3" type="text" disabled="disabled" />
                                            </td>
                                            <td>
                                                <select id="cmbStatusForOverDraftOffScb3">
                                                    <option value="0">Select Status</option>
                                                    <option value="1">Active</option>
                                                    <option value="2">Settled</option>
                                                    <option value="3">To Be Closed</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <!-- End of OFF SCB -->
                    </div>
                    <br />
                    <br />
                </div>--%>
                
                <div id="Div17" style="width: 100%; _width: 100%; *width: 100%; float: left;">
            <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%;">
                <span>Exposures</span></div>
                
                
                
            <div class="groupContainer" style="width: 98%; *width: 98%; _width: 98%;">
                <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%; background-color: white;
                    text-align: left; border: none;">
                    <span style="color: black;">ON SCB</span></div>
                <div class="divLeftBig" style="width: 98%;">
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        TERM
                        <label id="lblLoanterms">
                            LOANS</label>
                        <div style="border: none; width: 98%; overflow-y:none; overflow-x:scroll; ">
                            <table class="tableStyleSmall" id="tblFacilityTermLoan" style="width:140%;">
                                <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                    <td style="width: 10%;">
                                        TYPE
                                    </td>
                                    <td style="width: 10%;">
                                        FLAG
                                    </td>
                                    <td style="width: 10%;">
                                        RATE
                                    </td>
                                    <td style="width: 10%;">
                                        LIMIT
                                    </td>
                                    <td style="width: 10%;">
                                        OUTSTANDING
                                    </td>
                                    <td id="tdEM1" style="width: 10%;">
                                        EMI
                                    </td>
                                    <td id="tdEM2" style="width: 10%;">
                                        EMI %
                                    </td>
                                    <td id="tdEM3" style="width: 10%;">
                                        EMI SHARE
                                    </td>
                                    <td style="width: 10%;">
                                        STATUS
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY TYPE
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY FV
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt1" style="width: 10%;">
                                        LTV %
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans1" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans1')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate1" type="text" class="comboSizeActive"/>
                                    </td>
                                    <td>
                                        <input id="txtLimit1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities1" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(1,1)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity1" style="width:40px;" type="text" disabled="disabled" />
                                        <input type="hidden" id="txttermloanInterestRate1" />
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans2" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans2')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate2" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit2" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities2" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(1,2)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity2" style="width:40px;" type="text" disabled="disabled" />
                                        <input type="hidden" id="txttermloanInterestRate2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans3" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans3')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag3" type="text" disabled="disabled"  />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate3" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit3" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding3" class="comboSizeActive" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities3" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(1,3)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities3" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('3')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity3" style="width:40px;" type="text" disabled="disabled" />
                                        
                                        <input type="hidden" id="txttermloanInterestRate3" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans4" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans4')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate4" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit4" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding4" class="comboSizeActive" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities4" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(1,4)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities4" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV4" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('4')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity4" style="width:40px;" type="text" disabled="disabled" />
                                        <input type="hidden" id="txttermloanInterestRate4" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divColleterization">
									<label class="lblColleterization">COLLETERIZATION</label>
									<select id="cmbColleterization" class="comboSize widthSize22_per comboSizeActive">
									</select>
									<label class="lblColleterizationWith">With</label>
									<select id="cmbColleterizationWith" class="comboSize widthSize22_per comboSizeActive"></select>
								</div>
                        <div style="text-align: center; font-size: medium; font-weight: bold;">
                            CREDIT CARD
                            <div style="border: none;">
                                <table class="tableStyleSmall" id="tblFacilityCreditCard">
                                    <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                        <td style="width: 14%;">
                                            TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            FLAG
                                        </td>
                                        <td style="width: 11%;">
                                            ORIGINAL LIMIT
                                        </td>
                                        <%--<td style="width: 20%;" id="tdInterestRateCreditCard">
                                            INTEREST RATE
                                        </td>--%>
                                        <td style="width: 10%;" id="tdinterest">
                                            INTEREST
                                        </td>
                                        <td style="width: 12%;">
                                            STATUS
                                        </td>
                                        <td style="width: 15%;">
                                            SECURITY TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY FV
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY PV
                                        </td>
                                        <td id="tdlt2" style="width: 12%;">
                                            LTV %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="cmbTypeForCreditCard1" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForCreditCard1')">
                                            </select>
                                        </td>
                                        <td>
                                            <input id="txtFlagForcreditCard1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtOriginalLimitForCreditCard1" class="comboSizeActive comboSizeActive"  type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                        </td>
                                        <%--<td>
                                            <input id="txtInterestRateCreditCard1" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                        </td>--%>
                                        <td>
                                            <input id="txtInterestForCreditCard1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard1" class="comboSizeActive" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForcreditCard1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard1" type="text" disabled="disabled" />
                                    </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="cmbTypeForCreditCard2" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForCreditCard2')">
                                            </select>
                                        </td>
                                        <td>
                                            <input id="txtFlagForcreditCard2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtOriginalLimitForCreditCard2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                        </td>
                                        <%--<td>
                                            <input id="txtInterestRateCreditCard2" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                        </td>--%>
                                        <td>
                                            <input id="txtInterestForCreditCard2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard2" class="comboSizeActive" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForcreditCard2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard2" type="text" disabled="disabled" />
                                    </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="text-align: center; font-size: medium; font-weight: bold;">
                            OVERDRAFT
                            <div style="border: none;">
                                <table class="tableStyleSmall" id="tblFacilityOverDraft">
                                    <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                        <td style="width: 10%;">
                                            ORIGINAL LIMIT
                                        </td>
                                        <td style="width: 10%;">
                                            FLAG
                                        </td>
                                        <td style="width: 10%;" id="tdinteresRate">
                                            INTEREST RATE
                                        </td>
                                        <td style="width: 10%;" id="tdinteres1">
                                            INTEREST
                                        </td>
                                        <td style="width: 15%;" >
                                            STATUS
                                        </td>
                                        <td style="width: 15%;">
                                            SECURITY TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY FV
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY PV
                                        </td>
                                        <td id="tdlt3" style="width: 10%;">
                                            LTV %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeoverDraftInterest('1')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag1" type="text" class="comboSizeActive" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeoverDraftInterest('1')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft1" class="comboSizeActive" onchange="analystDetailsHelper.createOverDraftStatemntForBank('1',1)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft1" type="text" disabled="disabled" />
                                        <input type="hidden" id="txtOverDraftInterestRate1" />
                                    </td>
                                    
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeoverDraftInterest('2')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag2" type="text" class="comboSizeActive" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeoverDraftInterest('2')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft2" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('1', 2)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft2" type="text" disabled="disabled" />
                                        <input type="hidden" id="txtOverDraftInterestRate2" />
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit3" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('3')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag3" class="comboSizeActive" type="text" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest3" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('3')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres3" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft3" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('1',3)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft3" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft3" type="text" disabled="disabled" />
                                        <input type="hidden" id="txtOverDraftInterestRate3" />
                                    </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div style="height:20px;"></div>
                <%--<div class="divLeftBig" style="width: 98%; *width: 98%; _width: 98%;">
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        SECURITIES
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOnSCBSecurityTermLoan">
                                <tr class="theader">
                                    <td>
                                        STATUS
                                    </td>
                                    <td>
                                        TYPE
                                    </td>
                                    <td>
                                        ISSUING OFFICE
                                    </td>
                                    <td>
                                        ISSUING DATE
                                    </td>
                                    <td>
                                        SECURITY FV
                                    </td>
                                    <td>
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt1">
                                        LTV
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecurities1">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities1">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeTermLoan1"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateTermLoan1"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV1" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV1" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity1" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecurities2">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities2">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeTermLoan2"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateTermLoan2"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV2" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV2" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity2" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecurities3">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities3">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeTermLoan3"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateTermLoan3"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV3" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV3" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('3')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity3" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecurities4">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities4">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeTermLoan4"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateTermLoan4"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV4" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV4" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('4')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity4" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        <div style="border: none;">
                            <table class="tableStyleSmall" style="margin-top: 18px;" id="tblOnSCBSecurityCreditCard">
                                <tr class="theader">
                                    <td>
                                        STATUS
                                    </td>
                                    <td>
                                        TYPE
                                    </td>
                                    <td>
                                        ISSUING OFFICE
                                    </td>
                                    <td>
                                        ISSUING DATE
                                    </td>
                                    <td>
                                        SECURITY FV
                                    </td>
                                    <td>
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt2">
                                        LTV
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard1">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard1">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeCreditCard1"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateCreditCard1"/>
                                    </td>
                                    
                                    <td>
                                        <input id="txtSecurityFVForcreditCard1" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard1" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard1" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard2">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard2">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeCreditCard2"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateCreditCard2"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForcreditCard2" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard2" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard2" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        <div style="border: none;">
                            <table class="tableStyleSmall" style="margin-top: 18px;" id="tblOnSCBSecurityOverDraft">
                                <tr class="theader">
                                    <td>
                                        STATUS
                                    </td>
                                    <td>
                                        TYPE
                                    </td>
                                    <td>
                                        ISSUING OFFICE
                                    </td>
                                    <td>
                                        ISSUING DATE
                                    </td>
                                    <td>
                                        SECURITY FV
                                    </td>
                                    <td>
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt3">
                                        LTV
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft1">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft1">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeOverDraft1"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateOverDraft1"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft1" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft1" type="text" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft1" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft2">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft2">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeOverDraft2"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateOverDraft2"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft2" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft2" type="text" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft2" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft3">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft3">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input type="text" id="txtSecurityIssuingOfficeOverDraft3"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSecurityIssuingDateOverDraft3"/>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft3" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft3" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft3" type="text" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>--%>
                <div class="clear">
                </div>
                <!-- OFF SCB start -->
                <br />
                <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%; background-color: white;
                    text-align: left; border: none;">
                    <span style="color: black;">OFF SCB</span></div>
                <div class="divRightBig" style="width: 98%; *width: 98%; _width: 98%; float: left;
                    margin-left: 2px;">
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        TERM
                        <label id="lblloanTerms1">
                            LOANS</label>
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityTermLoan">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <td id="tdEM4">
                                        EMI
                                    </td>
                                    <td id="tdEM5">
                                        EMI %
                                    </td>
                                    <td id="tdEM6">
                                        EMI SHARE
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB1" type="text" disabled="disabled" onchange="analystDetailsHelper.createExistingrepaymentGrid()" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb1" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(2,1)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb2" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(2,2)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB3" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb3" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(2,3)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB4" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb4" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb4" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(2,4)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        CREDIT CARD
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityCreditCard">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <%--<td>
                                        INTEREST RATE
                                    </td>--%>
                                    <td id="tdinterest2">
                                        INTEREST
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForCreditCardOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForcreditCardOffScb1" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(1)" />
                                    </td>
                                    <%--<td>
                                        <input id="txtInterestRateForCreditCardForOffScb1" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(1)"  />
                                    </td>--%>
                                    <td>
                                        <input id="txtInterestForCreditCardForOffScb1" type="text"  disabled="disabled"  />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForCreditCardOffScb1" class="comboSizeActive" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForCreditCardOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForcreditCardOffScb2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(2)" />
                                    </td>
                                    <%--<td>
                                        <input id="txtInterestRateForCreditCardForOffScb2" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(2)" />
                                    </td>--%>
                                    <td>
                                        <input id="txtInterestForCreditCardForOffScb2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForCreditCardOffScb2" class="comboSizeActive" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        OVERDRAFT
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityOverDraft">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <td id="tdInterestrate1">
                                        INTEREST RATE
                                    </td>
                                    <td id="tdinterest3">
                                        INTEREST
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb1" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB1" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb1" class="comboSizeActive" onchange="analystDetailsHelper.createOverDraftStatemntForBank('2',1)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb2" class="comboSizeActive" onchange="analystDetailsHelper.createOverDraftStatemntForBank('2',2)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB3" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb3" class="comboSizeActive" onchange="analystDetailsHelper.createOverDraftStatemntForBank('2',3)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <!-- End of OFF SCB -->
            </div>
            <br />
            <br />
        </div>
                <!--End of Exposures -->
                <div class="clear">
                </div>
            </div>
            <!-- -----------------End of  TAB (a.1) � General (Application & Vehicle): ------------------------------ -->
            <!-- -----------------------------Main Income Generator tab(TAB (b.1) � Income Generator) Start-------------------------------------- -->
            <div class="content" id="tabIncomeGenerator" style="*border:0px;">
            </div>
            <!-- -----------------------------End of Main tab Income Generator tab(TAB (b.1) � Income Generator) -------------------------------------- -->
            <!-- -----------------------------TAB (c) � Loan Calculator: Start-------------------------------------- -->
            <div class="content" id="tabLoanCalculator" style="*border:0px;">
                <div class="divHeadLineWhiteBg" style="background:#005d9a; xmargin:auto; color:White;">
                    <span style="font-size: large; font-weight: bold; font-family: verdana;">INCOME & SEGMENTATION</span></div>
                <div class="divRightBigNormalClass" style="width: 98%; *width: 98%; _width: 98%;
                    float: left; margin-left: 2px;">
                    <div style="text-align: left; font-size: small; font-weight: bold;" id="divLoanCalculatorForPrimaryApplicant">
                        PRIMARY APPLICANTS INCOME
                        <br />
                        <br />
                        <div style="border: none;">
                            <table class="tableStyleTiny" style="border: 1px solid gray;" id="tblPrLoanCalculator">
                                <tr class="theader">
                                    <td class="widthSize15_per">
                                        CALCULATION SOURCE
                                    </td>
                                    <td class="widthSize15_per">
                                        INCOME(S)
                                    </td>
                                    <td class="widthSize30_per" colspan="2">
                                        INCOME ASSESMENT METHOD(S)
                                    </td>
                                    <td class="widthSize30_per " colspan="2">
                                        EMPLOYER CATEGORY(S)
                                    </td>
                                    <td class="widthSize10_per">
                                        CONSIDER
                                    </td>
                                </tr>
                                <tr id="trBank1ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank1ForLCPr">
                                        BANK - 1
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank1">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank1">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank1">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank1">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank1" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank1" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank1" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank2ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank2ForLCPr">
                                        BANK - 2
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank2">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank2">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank2">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank2">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank2" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank2" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank2" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank3ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank3ForLCPr">
                                        BANK - 3
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank3">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank3">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank3">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank3">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank3" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank3" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank3" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank4ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank4ForLCPr">
                                        BANK - 4
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank4">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank4">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank4">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank4">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank4" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank4" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank4" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank5ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank5ForLCPr">
                                        BANK - 5
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank5">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank5">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank5">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank5">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank5" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank5" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank5" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank6ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank6ForLCPr">
                                        BANK - 6
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank6">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank6">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank6">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank6">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank6" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank6" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank6" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trSalariedForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;">
                                        SALARY
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrSalary">
                                        <input type="hidden" id="txtAssMethodIdForLcPrSalary">
                                        <input type="hidden" id="txtAssCatIdForLcPrSalary">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrSalary" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrSalary" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrSalary" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trLandlordForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;">
                                        RENT
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrRent">
                                        <input type="hidden" id="txtAssMethodIdForLcPrrent">
                                        <input type="hidden" id="txtAssCatIdForLcPrRent">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrRent" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrRent" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrRent" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trDoctorForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;">
                                        PRACTICING DOCTOR
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrDoc">
                                        <input type="hidden" id="txtAssMethodIdForLcPrDoc">
                                        <input type="hidden" id="txtAssCatIdForLcPrDoc">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrDoctor" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrDoctor" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrDoctor" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trTeacherForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;">
                                        TUTION INCOME
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrTeacher" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrTec">
                                        <input type="hidden" id="txtAssMethodIdForLcPrTec">
                                        <input type="hidden" id="txtAssCatIdForLcPrTec">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrTeacher" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrTeacher" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrTeacher" type="text" class="txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrTeacher" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrTeacher" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div style="text-align: left; font-size: small; font-weight: bold; display: none;"
                        id="divLoanCalculatorForJointApplicant">
                        <br />
                        JOINT APPLICANTS INCOME
                        <br />
                        <br />
                        <div style="border: none;">
                            <table class="tableStyleTiny" style="border: 1px solid gray;" id="tblJtLoanCalculator">
                                <tr class="theader">
                                    <td class="widthSize15_per">
                                        CALCULATION SOURCE
                                    </td>
                                    <td class="widthSize15_per">
                                        INCOME(S)
                                    </td>
                                    <td class="widthSize30_per" colspan="2">
                                        INCOME ASSESMENT METHOD(S)
                                    </td>
                                    <td class="widthSize30_per " colspan="2">
                                        EMPLOYER CATEGORY(S)
                                    </td>
                                    <td class="widthSize10_per">
                                        CONSIDER
                                    </td>
                                </tr>
                                <tr id="trBank1ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank1ForLCJt">
                                        BANK - 1
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank1">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank1">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank1">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank1">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank1" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank1" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank1" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank2ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank2ForLCJt">
                                        BANK - 2
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank2">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank2">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank2">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank2">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank2" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank2" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank2" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank3ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank3ForLCJt">
                                        BANK - 3
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank3">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank3">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank3">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank3">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank3" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank3" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank3" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank4ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank4ForLCJt">
                                        BANK - 4
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank4">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank4">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank4">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank4">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank4" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank4" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank4" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank5ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank5ForLCJt">
                                        BANK - 5
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank5">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank5">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank5">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank5">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank5" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank5" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank5" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank6ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank6ForLCJt">
                                        BANK - 6
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank6">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank6">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank6">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank6">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank6" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank6" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank6" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trSalariedForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;">
                                        SALARY
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtSalary">
                                        <input type="hidden" id="txtAssMethodIdForLcJtSalary">
                                        <input type="hidden" id="txtAssCatIdForLcJtSalary">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtSalary" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtSalary" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtSalary" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trLandlordForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;">
                                        RENT
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtRent">
                                        <input type="hidden" id="txtAssMethodIdForLcJtrent">
                                        <input type="hidden" id="txtAssCatIdForLcJtRent">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtRent" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtRent" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtRent" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trDoctorForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;">
                                        PRACTICING DOCTOR
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtDoc">
                                        <input type="hidden" id="txtAssMethodIdForLcJtDoc">
                                        <input type="hidden" id="txtAssCatIdForLcJtDoc">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtDoctor" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtDoctor" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtDoctor" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trTeacherForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;">
                                        TUTION INCOME
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtTeacher" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtTec">
                                        <input type="hidden" id="txtAssMethodIdForLcJtTec">
                                        <input type="hidden" id="txtAssCatIdForLcJtTec">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtTeacher" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtTeacher" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtTeacher" type="text" class="txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtTeacher" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtTeacher" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="divLeftBig" style="width: 50%; border: none;">
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                    Total Income:</label></span>
                            <div class="divLeft" style="width: 62%; *width: 62%; _width: 62%; padding: 0px; border: none;">
                                <input id="txtTotalInComeForLcAll" class="txtBoxSize_H22" disabled="disabled" />
                                <input type="hidden" id="txtTotalIncomeForLcJointAll" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Declared Income:</label></span>
                            <div class="divLeft" style="width: 62%; *width: 62%; _width: 62%; padding: 0px; border: none;">
                                <input id="txtDeclaredIncomeForLcAll" class="txtBoxSize_H22" disabled="disabled" />
                                <input type="hidden" id="txtDeclaredIncomeForLcJointAll" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Appropriate Income:</label></span>
                            <div class="divLeft" style="width: 62%; *width: 62%; _width: 62%; padding: 0px; border: none;">
                                <input id="txtAppropriteIncomeForLcAll" class="txtBoxSize_H22" disabled="disabled" />
                                <input id="txtAppropriteIncomeForLcJointAll" type="hidden" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label id="lblTotaljointIncome" class="lbl" style="width: 140px; *width: 140px; width: 140px; font-weight: bold">
                                    TOTAL JOINT INCOME:</label></span>
                            <div class="divLeft" style="width: 62%; *width: 62%; _width: 62%; padding: 0px; border: none;">
                                <input id="txtTotalJointInComeForLcWithCal" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divRight" style="width: 45%; padding: 0px; border: none; float: right;
                        padding-top: 5px;">
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;">
                                    Employee Segment:</label></span>
                            <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                <select id="cmbEmployeeSegmentLc" class="comboSizeH_24 comboSizeActive">
                                </select>
                            </div>
                            <div class="divMiddile widthSize15_per" style="padding: 0px; border: none;">
                                <input id="txtSegmentCodeLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div id="divCashForLc" style="display: none;">
                            <div class="div3in1" style="display:none;">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 120px;">
                                        Assessment Method:</label></span>
                                <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                    <select id="cmbAssessmentMethodForLcCash" class="comboSizeH_24">
                                    </select>
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 120px;">
                                        Category:</label></span>
                                <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                    <select id="cmbCategoryForLcCash" class="comboSizeH_24">
                                    </select>
                                </div>
                            </div>
                            <div class="div3in1" id="divSubCategoryForLcCashCovered">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 120px;">
                                        Sub Category:</label></span>
                                <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                    <select id="cmbSubCategoryForLcCash" class="comboSizeH_24">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;">
                                    Assessment Method:</label></span>
                            <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                <input id="txtAssessmentMethodLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                                <input type="hidden" id="txtAssessmentMethodIdLc" />
                            </div>
                            <div class="divMiddile widthSize15_per" style="padding: 0px; border: none;">
                                <input id="txtAssessmentCodeLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;">
                                    Category:</label></span>
                            <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                <input id="txtEmployeeCategoryForLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                                <input type="hidden" id="txtEmployeeCategoryIdForLc" />
                                <input type="hidden" id="txtProfessionIdForLcIncomeAndSegment" />
                            </div>
                            <div class="divMiddile widthSize15_per" style="padding: 0px; border: none;">
                                <input id="txtEmployeeCategoryCodeForLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <!-- End of OFF SCB -->
                </div>
                <div class="clear">
                </div>
                <div class="divHeadLineWhiteBg" style="background:#005d9a; xmargin:auto; color:White;">
                    <span style="font-size: large; font-weight: bold; font-family: verdana;">REPAYMENT CAPABILITY</span></div>
                <div class="divRightBigNormalClass" style="width: 98%; *width: 98%; _width: 98%;
                    float: left; margin-left: 2px;">
                    <div style="text-align: left; font-size: small; font-weight: bold;">
                        <div class="divLeftBig" style="overflow: hidden; padding: 0px;">
                            <div class="divHeadLineWhiteBg" style="text-align: left; border: none; padding-left: 5px;">
                                <span style="font-size: small; font-weight: bold; font-family: verdana;">EXISTING REPAYMENTS</span></div>
                            <div class="divLeftBig" style="border: none; border-radius: 0px; margin-bottom: 0px;
                                margin-left: 0px; padding: 0px;">
                                <table class="tableStyleTiny" style="border: 1px solid gray;">
                                    <tr class="theader">
                                        <td class="widthSize50_per" colspan="2">
                                            <table class="widthSize100_per">
                                                <tr>
                                                    <td colspan="2">
                                                        ON US
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        TYPE
                                                    </td>
                                                    <td id="tdEM80">
                                                        EMI
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="widthSize50_per " colspan="2">
                                            <table class="widthSize100_per">
                                                <tr>
                                                    <td colspan="2">
                                                        OFF US
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        FI/NBFI
                                                    </td>
                                                    <td id="tdEM81">
                                                        EMI
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtTypeForLcExistingPayment1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPayment1" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtBankForLcExistingPaymentOffScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                            <input id="txtBankIdForLcExistingPaymentOffScb1" type="text" class="txtBoxSize_H22"
                                                style="display: none;" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPaymentOffScb1" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtTypeForLcExistingPayment2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPayment2" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtBankForLcExistingPaymentOffScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                            <input id="txtBankIdForLcExistingPaymentOffScb2" type="text" class="txtBoxSize_H22"
                                                style="display: none;" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPaymentOffScb2" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtTypeForLcExistingPayment3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPayment3" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtBankForLcExistingPaymentOffScb3" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                            <input id="txtBankIdForLcExistingPaymentOffScb3" type="text" class="txtBoxSize_H22"
                                                style="display: none;" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPaymentOffScb3" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtTypeForLcExistingPayment4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPayment4" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtBankForLcExistingPaymentOffScb4" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                            <input id="txtBankIdForLcExistingPaymentOffScb4" type="text" class="txtBoxSize_H22"
                                                style="display: none;" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPaymentOffScb4" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Total=
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtTotalEmiForLcExistingPaymentOnScb" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            Total=
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtTotalEmiForLcExistingPaymentOffScb" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="divHeadLineWhiteBg" style="text-align: left; border: none; padding-left: 5px;">
                                <span style="font-size: small; font-weight: bold; font-family: verdana;">CREDIT CARDS</span></div>
                            <div class="divLeftBig" style="border: none; border-radius: 0px; margin-bottom: 0px;
                                margin-left: 0px; padding: 0px;">
                                <table class="tableStyleTiny" style="border: 1px solid gray;">
                                    <tr class="theader">
                                        <td>
                                            LIMIT
                                        </td>
                                        <td id="tdinterest52">
                                            INTEREST
                                        </td>
                                        <td>
                                            LIMIT
                                        </td>
                                        <td id="tdinterest53">
                                            INTEREST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtLimitCarditcardForLcExistingPaymentForScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtInterestcreditcardForLcExistingPaymentForScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtLimitCarditcardForLcExistingPaymentForOffScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtInterestcreditcardForLcExistingPaymentOnScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtLimitCarditcardForLcExistingPaymentForScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtInterestcreditcardForLcExistingPaymentForScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtLimitCarditcardForLcExistingPaymentForOffScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtInterestcreditcardForLcExistingPaymentOnScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            Total=
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtTotalinterestForOnScbLcExistingrepayment" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="text-align: right;">
                                            Total=
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtTotalinterestForOffScbLcExistingrepayment" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="divRight" style="width: 45%; padding: 0px; border: none; float: right;
                        padding-top: 5px;">
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdDbr1">
                                    DBR Level:</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <select id="cmbDBRLevel" class="comboSizeH_24 comboSizeActive">
                                    <option value="1">Level-1</option>
                                    <option value="2">Level -2 ( 5% Breach)</option>
                                    <option value="3">Level -2 ( 10% Breach)</option>
                                </select>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdDbr2">
                                    Appropriate DBR %:</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtAppropriateDbr" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdDbr3">
                                    Considered DBR %:</label></span>
                            <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                <input id="txtConsideredDbr" class="txtBoxSize_H22 comboSizeActive" type="text" />
                            </div>
                            <div class="divMiddile widthSize40_per" style="padding: 0px; border: none;">
                                <span style="color: red; display: none;" id="tdDbr4">*Level-3 DBR Required</span>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdEM82">
                                    Maximum EMI Capability:</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtMaximumEmiCapability" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdEM83">
                                    Existing EMI(s):</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtExistingEmi" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdEM84">
                                    Current EMI Capability</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtCurrentEMICapability" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;">
                                    Balance Supported:</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtBalanceSupported" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px; font-weight: bold;" id="tdEM85">
                                    Maximum EMI:</label></span>
                            <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                <input id="txtMaximumEmi" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                            <div class="divMiddile widthSize40_per" style="padding: 0px; border: none;">
                                <span style="color: red; display: none;" id="tdDbrEx5">*DBR EXHAUSTED</span>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="divHeadLineWhiteBg" style="background:#005d9a; xmargin:auto; color:White;">
                    <span style="font-size: large; font-weight: bold; font-family: verdana;">
                        <label id="lblLoanCalculation">
                            LOAN</label>
                        CALCULATION</span></div>
                <div class="divRightBigNormalClass widthSize100_per" style="float: left; margin-left: 0px;
                    margin-bottom: 0px; padding: 2px;">
                    <div class="divLeftBig widthSize23_per" style="margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;">TENOR (Years)</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                    ARTA Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" id="txtArtaAllowedFoLc" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Vehicle Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" id="txtvehicleAllowedForLc" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Level:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <select id="cmbLabelForTonerLc" class="txtBoxSize_H22 comboSizeActive" onchange="analystDetailsHelper.calculateTenorSettings()">
                                    <option value="1">Level-1</option>
                                    <option value="2">Level-2</option>
                                </select>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Age Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" id="txtAgeAllowedForLc" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Asking Tenor:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAskingTenorForLc" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Appropriate Tenor:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAppropriteTenorForLc" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Considered Tenor:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22 comboSizeActive" id="txtCosideredtenorForLc" onchange="analystDetailsHelper.calculateTenorSettings()" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Considered in Months:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtConsideredMarginForLc" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;" id="spnInterestPersent">INTEREST
                            (%)</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                    Vendor Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtInterestLCVendoreAllowed" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    ARTA Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtInterestLCArtaAllowed" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Segment Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtInterestLCSegmentAllowed" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Asking Rate:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtInterestLCAskingRate" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Appropriate Rate:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAppropriteRate" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Considered Rate:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22 comboSizeActive" id="txtInterestLCCosideredRate" onchange="analystDetailsHelper.calculateInterestRate();" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1 widthSize100_per" style="color: red; margin-bottom: 10px;">
                                *Check if Special Rate Prevails</span>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana; font-size: 90%;">
                            <label id="lblLoanAmount">
                                LOAN</label>
                            AMOUNT (BDT)</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;" id="tdlt4">
                                    LTV Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtLtvAllowedForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Income Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtIncoomeAllowedForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    BB & CAD Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtbdCadAllowedForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" id="lblAskingLoan" style="width: 140px; *width: 140px; width: 140px;">
                                    Asking Loan:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAskingLoanAmountForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Existing Facility Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtExistingFacilitiesAllowedForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" id="lblAppropriteLoan" style="width: 140px; *width: 140px; width: 140px;">
                                    Appropriate Loan:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAppropriteloanAmountForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Approved
                                    <label id="lblApprovedloan">
                                        Loan</label>
                                    :</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22 comboSizeActive" id="txtApprovedLoanAmountForLcLoanAmount" onchange="analystDetailsHelper.calculateInterestRate();" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;">INSURANCE (BDT)</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                    General Insurance:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtgeneralInsurenceForLcIns" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    ARTA:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtArtaForLCInsurenceAmount" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Total Insurance:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtTotalInsurenceForLcInsurence" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <br />
                    <div style="margin: 0px; border: solid 1px green; border-radius: 15px;">
                        <div class="divLeftBig widthSize73_per" style="margin-bottom: 0px; border: none;">
                            <span id="spnLoanSummary" style="font-weight: bold; font-family: verdana;">LOAN SUMMARY</span>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" id="lblTotalloanAmount" style="width: 140px; _width: 140px; *width: 140px;
                                        text-align: right; font-weight: bold;">
                                        Total Loan Amount:</label></span>
                                <div class="divLeft widthSize50_per" style="padding: 0px; border: none;">
                                    <input id="txtloanAmountForLcLoanSummary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;" id="tdlt5">
                                        LTV Excluding Insurance:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtLtvExclusdingInsurence" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px; margin-left: 25px;"
                                        id="tdlt6">
                                        LTV Including Insurance:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtltvIncludingLtvForLcLoanSummary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;" id="tdlt7">
                                        DBR Excluding Insurance:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtDBRExcludingInsurence" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px; margin-left: 25px;"
                                        id="tdlt8">
                                        DBR Including Insurance:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtDBRIncludingInsurence" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px;
                            border: none;">
                            <br />
                            <div class="div3in1">
                                <span class="spnLeftLabel1 widthSize30_per rightAlign" style="font-weight: bold;">EMR:</span>
                                <div class="divLeft widthSize65_per" style="padding: 0px; border: none;">
                                    <input id="txtEmrForlcloanSummary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1 widthSize30_per leftAlign rightAlign" id="tdlt9">Extra LTV:</span>
                                <div class="divLeft widthSize65_per" style="padding: 0px; border: none;">
                                    <input id="txtExtraLtvForLcloanSumary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1 widthSize30_per leftAlign rightAlign" id="tdDbr5">Extra DBR:</span>
                                <div class="divLeft widthSize65_per" style="padding: 0px; border: none;">
                                    <input id="txtExtraDBRForLcloanSummary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <!-- -----------------------------End of TAB (c) � Loan Calculator: -------------------------------------- -->
            <!-- -----------------------------TAB (d) � Loan Finalization:  Start-------------------------------------- -->
            <div class="content" id="tabFinalization" style="*border:0px;">
                <div class="divRightBigNormalClass widthSize100_per" style="float: left; margin-left: 0px;
                    margin-bottom: 0px; padding: 2px;">
                    <div class="divLeftBig widthSize48_per centerAlign" style="margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;">DEVIATION</span>
                        <table id="tblFinalizationDeviation" class="tableStyleTiny" style="border: 1px solid gray;">
                            <tr class="theader">
                                <td class="widthSize20_per">
                                    Level
                                </td>
                                <td class="widthSize60_per">
                                    Description
                                </td>
                                <td class="widthSize20_per">
                                    Code
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmdDeviationLevel1" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="analystDetailsManager.getDeviationDescriptionByLevel('cmdDeviationLevel1')">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="cmdDeviationDescription1" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="analystDetailsHelper.getDeviationCodeByID('cmdDeviationDescription1')">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;">
                                    <input id="txtDeviationCode1" type="text" class="txtBoxSize_H22 widthSize100_per"
                                        disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmdDeviationLevel2" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="analystDetailsManager.getDeviationDescriptionByLevel('cmdDeviationLevel2')">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="cmdDeviationDescription2" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="analystDetailsHelper.getDeviationCodeByID('cmdDeviationDescription2')">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;">
                                    <input id="txtDeviationCode2" type="text" class="txtBoxSize_H22 widthSize100_per"
                                        disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmdDeviationLevel3" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="analystDetailsManager.getDeviationDescriptionByLevel('cmdDeviationLevel3')">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="cmdDeviationDescription3" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="analystDetailsHelper.getDeviationCodeByID('cmdDeviationDescription3')">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;">
                                    <input id="txtDeviationCode3" type="text" class="txtBoxSize_H22 widthSize100_per"
                                        disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmdDeviationLevel4" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="analystDetailsManager.getDeviationDescriptionByLevel('cmdDeviationLevel4')">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="cmdDeviationDescription4" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="analystDetailsHelper.getDeviationCodeByID('cmdDeviationDescription4')">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;">
                                    <input id="txtDeviationCode4" type="text" class="txtBoxSize_H22 widthSize100_per"
                                        disabled="disabled" />
                                </td>
                            </tr>
                        </table>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;
                        float: right;">
                        <span style="font-weight: bold; font-family: verdana;">DEVIATION FOUND</span>
                        <table id="tblFinalizationDeviationFound" class="tableStyleTiny" style="border: 1px solid gray;">
                            <tr>
                                <td class="leftAlign widthSize30_per" style="padding-left: 10px;">
                                    AGE
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;">
                                    <input id="txtDeviationAgeForfinalizeTab" type="text" class="txtBoxSize_H22 widthSize100_per"
                                        disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td class="leftAlign widthSize30_per" style="padding-left: 10px;" id="tdDbr6">
                                    DBR
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;">
                                    <input id="txtDeviationDbrForfinalizeTab" type="text" class="txtBoxSize_H22 widthSize100_per"
                                        disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td class="leftAlign widthSize30_per" style="padding-left: 10px;">
                                    SEATING CAPACITY
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;">
                                    <input id="txtSeacitingCapacityDeviationForfinalizeTab" type="text" class="txtBoxSize_H22 widthSize100_per"
                                        disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td class="leftAlign widthSize30_per" style="padding-left: 10px;">
                                    SHARE PORTION
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;">
                                    <input id="txtSharePortionForFinalizeTab" type="text" class="txtBoxSize_H22 widthSize100_per"
                                        disabled="disabled" />
                                </td>
                            </tr>
                        </table>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <br />
                    <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;">STRENGTH [+]</span>
                        <table id="tblFinalizationStrength" class="tableStyleTiny" style="border: 1px solid gray;">
                            <tr>
                                <td>
                                    <select id="cmbStrength1" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbStrength2" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbStrength3" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbStrength4" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbStrength5" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div class="fieldDiv">
                            <textarea id="tareaStrength1" class="txtFieldBig widthSize100_per comboSizeActive" cols="1" rows="2"
                                style="height: 55px;"></textarea>
                        </div>
                        <div class="fieldDiv">
                            <textarea id="tareaStrength2" class="txtFieldBig widthSize100_per comboSizeActive" cols="1" rows="2"
                                style="height: 55px;"></textarea>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;
                        float: right;">
                        <span style="font-weight: bold; font-family: verdana;">WEAKNESS [+]</span>
                        <table id="tblFinalizationWeakness" class="tableStyleTiny" style="border: 1px solid gray;">
                            <tr>
                                <td>
                                    <select id="cmbWeakness1" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbWeakness2" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbWeakness3" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbWeakness4" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbWeakness5" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div class="fieldDiv">
                            <textarea id="tareaWeakness1" class="txtFieldBig widthSize100_per comboSizeActive" cols="1" rows="2"
                                style="height: 55px;"></textarea>
                        </div>
                        <div class="fieldDiv">
                            <textarea id="tareaWeakness2" class="txtFieldBig widthSize100_per comboSizeActive" cols="1" rows="2"
                                style="height: 55px;"></textarea>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <br />
                    <div class="divLeftNormalClass widthSize48_per" style="margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;">REPAYMENT METHOD</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1 widthSize18_per">Asking:</span>
                            <div class="divLeft widthSize80_per" style="padding: 0px; border: none;">
                                <input id="cmbAskingRepaymentmethodForFinalize" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1 widthSize18_per">Instrument:</span>
                            <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                <select id="txtInstrumentForFinalizetab" class="comboSizeH_24 comboSizeActive">
                                    <option value="-1">Select</option>
                                    <option value="1">PDC</option>
                                    <option value="2">SI</option>
                                </select>
                            </div>
                            <span id="spnAccountFinalizeRepayment" class="spnLeftLabel1 widthSize18_per" style="margin-left: 10px;">SCB Account:</span>
                            <div id="divScbAccountFinalizeRepayment" class="divLeft widthSize35_per" style="padding: 0px; border: none;">
                                <select id="cmbAccountNumberForSCBForFinalizetab" class="comboSizeH_24 comboSizeActive">
                                    <option value="-1">Account Number</option>
                                </select>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1 widthSize18_per">Security PDC:</span>
                            <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                <select id="cmbSequrityPdcForFinalizetab" class="comboSizeH_24 comboSizeActive" onchange="analystDetailsHelper.changeSequrityPdc();">
                                    <option value="-1">Select</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            <span class="spnLeftLabel1 widthSize18_per" style="margin-left: 10px;">No of PDC(s):</span>
                            <div id="divNoofPdcForFinalizetab" class="divLeft widthSize35_per" style="padding: 0px;
                                border: none;">
                                <input id="txtNoOfPdcForFinalizeTab" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div id="divrepaymentDetailsForFinalizeTab">
                            <div class="div3in1">
                                <span class="spnLeftLabel1 widthSize18_per">PDC Amount:</span>
                                <div class="divLeft widthSize80_per" style="padding: 0px; border: none;">
                                    <input id="txtPdcAmountForFinalizeTab" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1 widthSize18_per">Bank Name:</span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <select id="cmbBankNameForFinalizeTabofrepaymentMethod" class="comboSizeH_24 comboSizeActive" onchange="analystDetailsHelper.changeBankNameForRepaymentForFinalizeTab();">
                                        <option value="-1">Select a Bank</option>
                                    </select>
                                </div>
                                <span class="spnLeftLabel1 widthSize18_per" style="margin-left: 10px;">Account No:</span>
                                <div class="divLeft widthSize35_per" style="padding: 0px; border: none;">
                                    <input id="txtAccountnumberForaBankFinalizeTab" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;
                        float: right;">
                        <span style="font-weight: bold; font-family: verdana;">CONDITIONS [+]</span>
                        <table id="tblFinalizationCondition" class="tableStyleTiny" style="border: 1px solid gray;">
                            <tr>
                                <td>
                                    <select id="cmbCondition1" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="-1">
                                            <section>All Off Us Exposured to be Matched with CIB</section>
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbCondition2" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbCondition3" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbCondition4" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbCondition5" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            
                        </table>
                        <div class="fieldDiv">
                            <textarea id="tareaCondition1" class="txtFieldBig widthSize100_per comboSizeActive" cols="1" rows="2"
                                style="height: 55px;"></textarea>
                        </div>
                        <div class="fieldDiv">
                            <textarea id="tareaCondition2" class="txtFieldBig widthSize100_per comboSizeActive" cols="1" rows="2"
                                style="height: 55px;"></textarea>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <br />
                    <div class="divLeftBig widthSize98_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;
                        float: right;">
                        <span style="font-weight: bold; font-family: verdana;">REMARKS [+]</span>
                        <table id="tblfinalizationRemark" class="tableStyleTiny" style="border: 1px solid gray;">
                            <tr>
                                <td>
                                    <select id="cmbRemark1" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="-1">
                                            <section>All Off Us Exposured to be Matched with CIB</section>
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbRemark2" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="cmbRemark3" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div class="fieldDiv">
                            <textarea id="txtRemark4" class="txtFieldBig widthSize100_per comboSizeActive" cols="1" rows="3"
                                style="height: 84px; _height: 84px; *height: 84px;"></textarea>
                        </div>
                        <div class="fieldDiv">
                            <textarea id="txtRemark5" class="txtFieldBig widthSize100_per comboSizeActive" cols="1" rows="3"
                                style="height: 84px; _height: 84px; *height: 84px;"></textarea>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <br />
                    <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;">REQUIRED DOCUMENT</span>
                        <table class="tableStyleTiny" style="border: 1px solid gray;">
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    LETTER OF INTRODUCTION
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtLetterOfIntroduction" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbLetterOfIntroduction" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    PAYSLIPS/CASH VOUCHERS
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtPayslips" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbPayslip" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    TRADE LICENSE
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtTradeLicence" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbtradelicence" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    MOA & AOA
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtMoaAoa" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbMoaAoa" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    PARTNERSHIP DEED
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtPartnershipDeed" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbPartnershipDeed" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    FORM X/FORM XII/FORM I
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtFrom" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbFrom" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    BOARD RESOULATION
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtboardresolution" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbBoardresolution" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    DIPLOMA CERTIFICATE
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtDiplomarcertificate" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbDiplomacertificate" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    PROFESSIONAL CERTIFICATE
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtProfessionalcertificate" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbProfessinoalCertificate" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    MUNICIPAL TAX RECIPT
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtMunicipalTax" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbunicipalTax" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    TITLE DEED
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtTitleDeed" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbTitleDeed" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    RENTAL DEED
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtRentalDeed" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbentalDeed" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    UTILITY BILLS
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtUtilityBills" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbtilityBills" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    <label id="lblLoanOfferLater">
                                        LOAN</label>
                                    OFFER LETTER
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtLoanOfferLetter" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbLoanOfferLetter" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize48_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;
                        float: right;">
                        <span style="font-weight: bold; font-family: verdana;">VERIFICATION REPORTS</span>
                        <table class="tableStyleTiny" style="border: 1px solid gray;">
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    CPV REPORT
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtCpvreport" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbCpvreport" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    CAR PRICE VERIFCATION REPORT
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtCarPriceVerificationReport" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbCarPriceVerificationReport" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    STATEMENT AUTHENTICATION
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtStatementAutheticationreport" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbStatementAutheticationreport" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    CREDIT REPORT
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtcreditReport" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbcreditReport" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    CAR VERIFICATION REPORT
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtCarVerificationreport" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbCarVerificationreport" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    USED CAR VALUATION REPORT
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtUsedCarVerificationreport" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbUsedCarVerificationreport" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    USED CAR DOCS
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtUsedcarDocs" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbUsedcarDocs" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="rightAlign widthSize10_per">
                                    CARD REPAYMENT HISTORY
                                </td>
                                <td class="widthSize10_per">
                                    <input id="txtCardrepaymentHistory" class="txtBoxSize_H22" disabled="disabled" />
                                </td>
                                <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                    <select id="cmbCardrepaymentHistory" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftNormalClass widthSize48_per" style="margin-bottom: 0px; float: right;
                        margin-top: 5px;">
                        <span style="font-weight: bold; font-family: verdana;">APPRAISER & APPROVER</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1 widthSize18_per">Appraised By:</span>
                            <div class="divLeft widthSize64_per" style="padding: 0px; border: none;">
                                <select id="cmbAppraisedby" class="comboSizeH_24" disabled="disabled" onchange="analystDetailsHelper.changeUser(this.id)">
                                    <option value="-1">Select a System User</option>
                                </select>
                            </div>
                            <div class="divLeft widthSize15_per" style="padding: 0px; border: none; margin-left: 5px;">
                                <input id="txtAppraiserCode" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1 widthSize18_per">Supported By:</span>
                            <div class="divLeft widthSize64_per" style="padding: 0px; border: none;">
                                <select id="cmbSupportedby" class="comboSizeH_24" disabled="disabled" onchange="analystDetailsHelper.changeUser(this.id)">
                                    <option value="-1">Select a System User</option>
                                </select>
                            </div>
                            <div class="divLeft widthSize15_per" style="padding: 0px; border: none; margin-left: 5px;">
                                <input id="txtSupportedbyCode" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1 widthSize18_per">Approved By:</span>
                            <div class="divLeft widthSize64_per" style="padding: 0px; border: none;">
                                <select id="cmbApprovedby" class="comboSizeH_24 comboSizeActive" onchange="analystDetailsHelper.changeUser(this.id)">
                                    <option value="-1">Select a System User</option>
                                </select>
                            </div>
                            <div class="divLeft widthSize15_per" style="padding: 0px; border: none; margin-left: 5px;">
                                <input id="txtApprovedbyCode" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <select id="cmbLabelForDlaLimit" style="display: none;">
                                <option value="1">Label 1</option>
                                <option value="2">Label 2</option>
                                <option value="3">Label 3</option>
                            </select>
                            <span class="spnLeftLabel1 widthSize20_per">Auto DLA Limit:</span>
                            <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                <input id="txtAutoDLALimit" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                            <span class="spnLeftLabel1 widthSize25_per" style="margin-left: 10px; display:none;">Total Secured
                                Limit:</span>
                            <div class="divLeft widthSize25_per" style="padding: 0px; border: none; display:none;">
                                <input id="txtTotalSecuredlimit" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="divLeftNormalClass widthSize100_per" style="margin-bottom: 0px; margin-top: 5px;
                        padding: 0px;">
                        <div class="divHeadLine widthSize100_per" style="text-align: center; margin: 0px;">
                            <span style="font-weight: bold; font-family: verdana; font-size: medium">ALERTS</span>
                        </div>
                        <div class="divLeftBig widthSize45_per centerAlign" style="margin-bottom: 0px; margin-top: 5px;">
                            <span style="font-weight: bold; font-family: verdana;">VERIFICATION REPORTS</span>
                            <table class="tableStyleTiny" style="border: 1px solid gray;">
                                <tr class="theader">
                                    <td class="widthSize5_per">
                                        DEMOGRAPHIC
                                    </td>
                                    <td>
                                        ALLOWED
                                    </td>
                                    <td>
                                        FOUND
                                    </td>
                                    <td>
                                        FLAG
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        MAX AGE
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMaxAgeAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMaxAgeFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMaxAgeFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        MIN AGE
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMinAgeAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMinAgeFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMinAgeFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        MIN INCOME
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMinincomeAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMinIncomeFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtMinIncomeFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        LOCATION
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;" colspan="2">
                                        <input id="txtLocationForLc" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                        <select id="cmbLocationForLcFlag" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                            <option value="-1">Select</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        NATIONALITY
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;" colspan="2">
                                        <input id="txtNatinalityForLc" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                        <select id="cmbNationalityForLcFlag" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                            <option value="-1">Select</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <div class="clear">
                            </div>
                            <br />
                            <table id="tblJointAlertVerification" class="tableStyleTiny" style=" display:none; border: 1px solid gray;">
                                <tr class="theader">
                                    <td class="widthSize5_per">
                                        JOINT APPLICANT
                                    </td>
                                    <td>
                                        ALLOWED
                                    </td>
                                    <td>
                                        FOUND
                                    </td>
                                    <td>
                                        FLAG
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        MIN AGE
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointMinAgeAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointMinAgeFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointminAgeFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        MAX AGE
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointMaxAgeAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointMaxAgeFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointMaxAgeFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        MIN INCOME
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointMinIncomeAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointMinIncomeFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize25_per" style="padding-right: 3px;">
                                        <input id="txtJointMinIncomeFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                            <div class="clear">
                            </div>
                            <br />
                            <table class="tableStyleTiny" style="border: 1px solid gray;">
                                <tr class="theader">
                                    <td class="widthSize5_per">
                                        VEHICLE & VENDOR
                                    </td>
                                    <td>
                                        ALLOWED
                                    </td>
                                    <td>
                                        FOUND
                                    </td>
                                    <td class="widthSize10_per">
                                        FLAG
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        CAR AGE AT TENOR END
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtCarAgeAtTenorEndAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtCarAgeAtTenorEndFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtCarAgeAtTenorEndFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        SEATING CAPACITY
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtSeatingCapacityAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtSeatingCapacityFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtSeatingCapacityFlagForvehicleAndVendoreForFinalizeTab" class="txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        CAR VERIFCATION
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;" colspan="2">
                                        <input id="txtCarVerificationForFinTab" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtCarVerificationFlagForFinTab" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        HYPOTHECATION CHECK
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;" colspan="2">
                                        <input id="txthypothecationForFinalizeTab" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txthypothecationFlagForFinalizeTab" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        VENDOR STATUS
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;" colspan="2">
                                        <input id="txtVendoreStatusForFnlTab" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;" class=" widthSize10_per">
                                        <select id="cmbVendoreStatusFlagForFnlTab" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                            <option value="-1">Select</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="divLeftBig widthSize45_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;
                            float: right; margin-top: 5px;">
                            <table class="tableStyleTiny" style="border: 1px solid gray;">
                                <tr class="theader">
                                    <td class="widthSize5_per" id="tdLoan">
                                        LOAN
                                    </td>
                                    <td>
                                        ALLOWED
                                    </td>
                                    <td>
                                        FOUND
                                    </td>
                                    <td class="widthSize10_per">
                                        FLAG
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per" id="tdloanAmount">
                                        LOAN AMOUNT
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtLoanAmountAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtLoanAmountFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtLoanAmountFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        BB MAX LIMIT
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtBBMaxlimitAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtBBMaxlimitFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtBBMaxlimitFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per" id="tdinterest54">
                                        INTEREST RATES
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtInterestRatesAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtInterestRatesFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtInterestRatesFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        TENOR
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtTenorAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtTenorFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtTenorFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        ARTA ENROLLMENT
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtArtaEnrollmentAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtArtaEnrollmentFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtArtaEnrollmentFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per" id="tdlt10">
                                        LTV INC. BANCA
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtLtvIncludingBankaAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtLtvIncludingBankaFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtLtvIncludingBankaFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per" id="tdlt11">
                                        MAX LTV SPREAD
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtMaxLtvSpreadAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtMaxLtvSpreadFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtMaxLtvSpreadFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per" id="tdDbr7">
                                        DBR
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtDbrForFtAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtDbrForFtFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtDbrForFtFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per" id="td1">
                                        DBR SPREAD
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtDbrSpreadForFtAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtDbrSpreadForFtFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtDbrSpreadForFtFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per" id="tdTotalAutoloan">
                                        TOTAL AUTO LOAN
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtTotalAutoLoanForFtAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtTotalAutoLoanForFtFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtTotalAutoLoanForFtFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        APPROVERS DLA
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <input id="txtApproversDlaAllowed" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtApproversDlaFound" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtApproversDlaFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize25_per">
                                        CIB OBTAINED
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;" colspan="2">
                                        <input id="txtCibObtainedForFt" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtCibObtainedForFtFlag" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                </tr>
                            </table>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="divLeftBig widthSize45_per centerAlign" style="margin-left: 5px; margin-bottom: 0px;
                            float: right; margin-top: 5px;">
                            <span style="font-weight: bold; font-family: verdana;">CIB REQUEST LOANS</span>
                            <table class="tableStyleTiny" id="tblCibrequestLoans" style="border: 1px solid gray;">
                                <tr class="theader">
                                    <td class="widthSize10_per" id="tdLoan">
                                        SL NO
                                    </td>
                                    <td>
                                        FACILITY
                                    </td>
                                    <td>
                                        AMOUNT
                                    </td>
                                    <td>
                                        TENOR
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize10_per">
                                        1
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <select id="cmbFacilityForCibRequest1" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                            <option value="-1">Select from List</option>
                                            <option value="1">TERM LOAN</option>
                                            <option value="2">REVOLVING</option>
                                            <option value="3">OTHER</option>
                                        </select>
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtLoanAmountCibrequest1" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtTenorForCibRequest1" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize10_per">
                                        2
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <select id="cmbFacilityForCibRequest2" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                            <option value="-1">Select from List</option>
                                            <option value="1">TERM LOAN</option>
                                            <option value="2">REVOLVING</option>
                                            <option value="3">OTHER</option>
                                        </select>
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtLoanAmountCibrequest2" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtTenorForCibRequest2" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize10_per">
                                        3
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <select id="cmbFacilityForCibRequest3" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                            <option value="-1">Select from List</option>
                                            <option value="1">TERM LOAN</option>
                                            <option value="2">REVOLVING</option>
                                            <option value="3">OTHER</option>
                                        </select>
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtLoanAmountCibrequest3" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtTenorForCibRequest3" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize10_per">
                                        4
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <select id="cmbFacilityForCibRequest4" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                            <option value="-1">Select from List</option>
                                            <option value="1">TERM LOAN</option>
                                            <option value="2">REVOLVING</option>
                                            <option value="3">OTHER</option>
                                        </select>
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtLoanAmountCibrequest4" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtTenorForCibRequest4" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rightAlign widthSize10_per">
                                        5
                                    </td>
                                    <td class="widthSize15_per" style="padding-right: 3px;">
                                        <select id="cmbFacilityForCibRequest5" class="widthSize100_per comboSizeH_24 comboSizeActive">
                                            <option value="-1">Select from List</option>
                                            <option value="1">TERM LOAN</option>
                                            <option value="2">REVOLVING</option>
                                            <option value="3">OTHER</option>
                                        </select>
                                    </td>
                                    <td class="widthSize20_per" style="padding-right: 3px;">
                                        <input id="txtLoanAmountCibrequest5" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                    <td class="widthSize10_per" style="padding-right: 3px;">
                                        <input id="txtTenorForCibRequest5" class="txtBoxSize_H22 comboSizeActive" />
                                    </td>
                                </tr>
                            </table>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <br />
                    <div style="margin: 0px; border: solid 1px green; border-radius: 15px;">
                        <div class="divHeadLine widthSize100_per" style="margin: 0px">
                            <span style="font-weight: bold; font-family: verdana;">REWORK</span>
                        </div>
                        <div class="divLeftBig widthSize100_per" style="margin-bottom: 0px; border: none;">
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                        Rework Done:</label></span>
                                <div class="divLeft widthSize15_per" style="padding: 0px; border: none;">
                                    <select id="cmbReworkDone" class="comboSizeH_24 widthSize100_per comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                
                                <div id="divReworkDet" style="display:none;">
                                
                                <span class="spnLeftLabel1" style="margin-left: 20px;">Rework Date:</span>
                                <div class="divLeft widthSize20_per" style="padding: 0px; border: none;">
                                    <input id="txtReworkDate" class="txtBoxSize_H22 comboSizeActive" />
                                </div>
                                <span class="spnLeftLabel1" style="margin-left: 65px;">Rework Count:</span>
                                <div class="divLeft widthSize20_per" style="padding: 0px; border: none;">
                                    <select id="cmbReworkCount" class="comboSizeH_24 widthSize100_per comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="1">1st</option>
                                        <option value="2">2nd</option>
                                    </select>
                                </div>
                            
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                        Rework Reason:</label></span>
                                <div class="divLeft widthSize81_per" style="padding: 0px; border: none;">
                                    <%--<input id="txtreworkReason" class="txtBoxSize_H22" disabled="disabled" />--%>
                                    <select id="cmbReworkReason" class="comboSizeH_24 widthSize100_per comboSizeActive">
                                        <option value="-1">Select</option>
                                        <option value="Interest Rate Change">Interest Rate Change</option>
                                        <option value="Interest Rate Change">Tenor Change</option>
                                        <option value="ARTA waiver">ARTA waiver</option>
                                        <option value="ARTA by Cash">ARTA by Cash</option>
                                        <option value="General Insurance by Cash">General Insurance by Cash</option>
                                        <option value="General Insurance by Cash">General Insurance waiver</option>
                                        <option value="Loan amount increase">Loan amount increase</option>
                                        <option value="Loan Amount Decrease">Loan Amount Decrease</option>
                                        <option value="Quotation Change">Quotation Change</option>
                                        <option value="OTHER">OTHER</option>
                                    </select>
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                        Last Approval Date:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtLastApprovalDate" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                                <span class="spnLeftLabel1" style="margin-left: 157px;">Last Approval Amount:</span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtLastApprovalAmount" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <br />
                    <div style="margin: 0px; border: solid 1px green; border-radius: 15px;">
                        <div class="divHeadLine widthSize100_per" style="margin: 0px">
                            <span style="font-weight: bold; font-family: verdana;">CIB INFO</span>
                        </div>
                        <div class="divLeftBig widthSize100_per" style="margin-bottom: 0px; border: none;">
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                        BUREAU HISTORY:</label></span>
                                <div class="divLeft widthSize15_per" style="padding: 0px; border: none;">
                                    <select id="cmbBureauHistory" class="comboSizeH_24 widthSize100_per comboSizeActive">
                                    </select>
                                </div>
                                <span class="spnLeftLabel1" style="margin-left: 20px;"></span>
                                <div class="divLeft widthSize20_per" style="padding: 0px; border: none;">
                                    <input id="txtCibInfoDesc" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                                <span class="spnLeftLabel1" style="margin-left: 65px;"></span>
                                <div class="divLeft widthSize20_per" style="padding: 0px; border: none;">
                                    <input id="txtCibInfoCode" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <!-- -----------------------------End of TAB (d) � Loan Finalization:-------------------------------------- -->

            <script src="../../Scripts/activatables.js" type="text/javascript"></script>

            <script type="text/javascript">
    activatables('page', ['tabGeneral', 'tabIncomeGenerator', 'tabLoanCalculator', 'tabFinalization']);
            </script>

            <br />
            <div class="clear">
            </div>
            <div class="modal" id="bankStatementPopupDiv" style="display: none; width: 850px;
                height: 350px;">
                <h3>
                    Bank Statement For <span id="spnBankStatementName"></span>
                </h3>
                <div>
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <object classid="clsid:0002E559-0000-0000-C000-000000000046" id="BankStatementAVG"
                                    width="100%" style="height: 250px">
                                    <param name="XMLData" value="'<?xml version='1.0'?><ss:Workbook xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:c='urn:schemas-microsoft-com:office:component:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'><x:ExcelWorkbook><x:ProtectStructure>False</x:ProtectStructure><x:ActiveSheet>0</x:ActiveSheet></x:ExcelWorkbook><ss:Worksheet ss:Name='Sheet0'><x:WorksheetOptions><x:ViewableRange>R1:R262144</x:ViewableRange><x:Selection>R1C1</x:Selection><x:TopRowVisible>0</x:TopRowVisible><x:LeftColumnVisible>0</x:LeftColumnVisible><x:ProtectContents>False</x:ProtectContents></x:WorksheetOptions><c:WorksheetOptions></c:WorksheetOptions></ss:Worksheet></ss:Workbook>'" />
                                </object>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="btns">
                    <a href="javascript:;" id="btnClosePopup" class="iconlink iconlinkClose">Close</a>
                </div>
            </div>
            <div class="divAnalystComments" style="width:99%; margin:auto;">
                <label>
                    Analyst Comment</label><br />
                <textarea id="tAnalystRemark" cols="20" class="txtBoxSize_H22 widthSize100_per comboSizeActive" rows="20"
                    style="height: 150px; *height: 150px; _height: 150px;"></textarea>
            </div>
            <div class="btns" style="width: 100%; text-align: center; background-color: #fff;
                height: 50px; vertical-align: middle;">
                <div style="float: left; width: 50%">
                    
                    <a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a> 
                    <a href="javascript:;"id="lnkForward" class="iconlink iconlinkforward">Forward</a> 
                        <a href="javascript:;"
                            id="lnkBackward" class="iconlink iconlinkBackword">Backward</a> <a href="javascript:;"
                                id="lnkOnHold" class="iconlink iconlinkDefer">On Hold</a> <a href="javascript:;"
                                    id="lnkReject" class="iconlink iconlinkReject">Reject</a> <a href="javascript:;"
                                        id="lnkDecline" class="iconlink iconlinkDecline">Decline</a>
                </div>
                <div style="float: left; width: 49%; text-align: left;">
                    <a href="javascript:;" id="lnkDocumentChecklist" class="iconlink iconlinkDocumentCheckList">
                        Document Checklist</a> <a href="javascript:;" id="lnkLoanChecklist" class="iconlink iconlinkCloseLoanCheckList">
                            Loan Checklist</a> <a href="javascript:;" id="lnkBasel" class="iconlink iconlinkCloseBasel">
                                BASEL II</a> <a href="javascript:;" id="lnkLLI" class="iconlink iconlinkCloseLLI">LLI</a>
                    <a href="javascript:;" id="lnkOfferLetter" class="iconlink iconlinkCloseOfferLeter">
                        Offer Letter</a> <a href="javascript:;" id="lnkBookLet" class="iconlink iconlinkCloseBookLet">
                            Booklet</a><a href="javascript:;" id="lnkIncomeAssment" class="iconlink iconlinkCloseOfferLeter">
                        Income Assessment</a> <a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
