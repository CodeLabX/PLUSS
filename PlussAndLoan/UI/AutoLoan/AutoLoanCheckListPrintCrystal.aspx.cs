﻿using System;
using System.Collections.Generic;
using AutoLoanService.Entity.CheckListPrint;
using CrystalDecisions.CrystalReports.Engine;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class UI_AutoLoan_AutoLoanCheckListPrintCrystal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoanCheckListPrintEntity lc = new LoanCheckListPrintEntity();
                List<LoanCheckListPrintEntity> lcList = new List<LoanCheckListPrintEntity>();
                lc = (LoanCheckListPrintEntity)Session["LoanCheckListObject"];
                lcList.Add(lc);

                ReportDocument rd = new ReportDocument();
                string path = "";
                if (lcList[0].lblProductId == "3") {
                    path = Server.MapPath("PrintCheckList/LoanCheckListSadiqu.rpt");
                }
                else
                {
                    path = Server.MapPath("PrintCheckList/LoanCheckList.rpt");
                }
                rd.Load(path);
                rd.SetDataSource(lcList);
                crViewer.ReportSource = rd;
                //rd.PrintToPrinter(1, true, 0, 0);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
