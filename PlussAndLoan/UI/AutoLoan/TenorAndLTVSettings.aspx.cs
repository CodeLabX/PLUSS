﻿using System;
using System.Collections.Generic;
using System.IO;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{

    public partial class TenorAndLTVSettings : System.Web.UI.Page
    {
        [AjaxNamespace("TenorAndLTVSettings")]
        protected void Page_Load(object sender, EventArgs e)
        {

            Utility.RegisterTypeForAjax(typeof(TenorAndLTVSettings));
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBrandTenorLTV> GetTenorList(int pageNo, int pageSize, string search)
        {
            pageNo = pageNo * pageSize;

            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            List<AutoBrandTenorLTV> objAutoBrandTenorLTV =
                tanorAndLTVService.GetTenorList(pageNo, pageSize, search);
            return objAutoBrandTenorLTV;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;


        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> GetStatus(int statusID)
        {
            AutoVendorMOURepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new AutoVendorMOUService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVendorMOUService.GetStatus(statusID);
            return objList_AutoStatus;
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> getModelbyManufacturerID(int manufacturerID)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            List<AutoVehicle> objList_AutoModel = tanorAndLTVService.GetModelList(manufacturerID);
            return objList_AutoModel;
        }



        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAuto_TenorAndLTV(AutoBrandTenorLTV tenor)
        {
            var userID = Convert.ToInt32(Session["Id"].ToString());
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new TanorAndLTVService(repository);
            tenor.UserId = userID;

            var res = autoLoanDataService.SaveAuto_TenorAndLTV(tenor);
            return res;

        }



        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string TanorLtvDeleteById(int tanorLtvID)
        {
            //var userID = Convert.ToInt32(Session["Id"].ToString());

            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new TanorAndLTVService(repository);
            //tenor.UserId = userID;

            var res = autoLoanDataService.TanorLtvDeleteById(tanorLtvID);
            return res;



        }

        protected void btnUploadSave_Click(object sender, EventArgs e)
        {
            if (fileDocumentUpload.FileName == "") {
                string msg = "Please select a xls file for upload.";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");
                return;
            }
            
            var res = uploadFile();
            if (res == "Upload success" || res == "Success")
            {
                string msg = "Operation Successfull.";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");
            }
            else
            {
                string msg = "Operation partially completed. Found Error with follwoing rows (" + res + ")";

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");

            }
        }

        public string uploadFile()
        {
            string name = "";
            string returnValue = "";
            if (!fileDocumentUpload.FileContent.CanRead)
            {

                returnValue = "Please select Valid file";
            }
            else
            {
                try
                {
                    string serverPath = Request.PhysicalApplicationPath;
                    var directory = Server.MapPath(@"../Uploads/Auto");

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    DateTime date = DateTime.Now;
                    name = date.ToString("yyyyMMddHHmmss") + "_" +
                             fileDocumentUpload.FileName;
                    fileDocumentUpload.SaveAs(Path.Combine(directory, name));


                    returnValue = "Upload success";
                    returnValue = DumpExcelFile(Convert.ToString(Path.Combine(directory, name)));

                }
                catch (Exception ex)
                {
                    returnValue = "Upload Failed";
                }

            }
            return returnValue;
        }


        public string DumpExcelFile(string excelFilePath)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVendorMOUService = new TanorAndLTVService(repository);
            var res = autoVendorMOUService.DumpExcelFile_ForTenorAndLTV(excelFilePath);
            return res;

        }




    }
}
