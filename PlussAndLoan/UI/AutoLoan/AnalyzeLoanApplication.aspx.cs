﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class AnalyzeLoanApplication : System.Web.UI.Page
    {
        [AjaxNamespace("AnalyzeLoanApplication")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(AnalyzeLoanApplication));
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanSummary> GetLoanSummary(int pageNo, int pageSize, Int32 searchKey)
        {
            List<AutoLoanSummary> objLoanSummaryList = new List<AutoLoanSummary>();
            pageNo = pageNo * pageSize;
            var userType = Session["UserType"];
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objLoanSummaryList;
            }
            else
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                objLoanSummaryList =
                    autoLoanAssessmentService.getAutoLoanSummaryAll(pageNo, pageSize, searchKey, statePermission);
            }
            return objLoanSummaryList;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string AppealedByAutoLoanMasterId(int appiledOnAutoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                if (UserType == 10)
                {
                    objAutoStatusHistory.StatusID = 18; //received by auto ops maker
                }
                else if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 20; //received by auto ops Checker
                }
                else if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 23; //received by auto CI Support
                }
                else if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 26; //received by auto CI Analyst
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public Object GetDataByLLID(int llId)
        {
            //ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            //var autoLoanDataService = new LoanApplicationService(repository);
            //var objAutoLoanApplicationAll = autoLoanDataService.getLoanLocetorInfoByLLID(llId);
            //return objAutoLoanApplicationAll;

            var objAutoLoanApplicationAll = new Object();
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objAutoLoanApplicationAll;
            }
            else
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                objAutoLoanApplicationAll = autoLoanAssessmentService.getLoanLocetorInfoByLLID(llId, statePermission);
            }

            return objAutoLoanApplicationAll;
        }

        #region Rimpo Other Tab


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> getVehicleStatus(int statusID)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVehicleStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVendor> GetVendor()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetVendor();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranchWithSource> GetAllAutoBranch()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetAllbranchName();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranchWithSource> GetAllAutoSourceByBranchId(int branchId)
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetAllAutoSourceByBranchId(branchId);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBank> GetBank(int status)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBank(status);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranch> GetBranch(int bankID)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBranch(bankID);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoFacility> getFacility()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetFacility();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussSector> GetSector()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetAllPlussSector();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussSubSector> GetSubSector(int sectorId)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetPlussSubSectorBySectorID(sectorId);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussIncomeAcesmentMethod> GetIncomeAssement()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetPlussIncomeAcesmentMethod();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPayrollRate> GetEmployerForSalaried()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetEmployerForSalaried();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoSegmentSettings> GetEmployeSegment()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetEmployeSegment();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<auto_CibInfo> getCibInfo()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getCibInfo();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<Auto_InsuranceBanca> getGeneralInsurance()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getGeneralInsurance();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoOwnDamage> GetOwnDamage()
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);


            List<AutoOwnDamage> objAutoOwnDamageList = autoLoanDataService.GetOwnDamage();
            return objAutoOwnDamageList;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoOthercharge GetOtherDamage()
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);


            var objAutoOthercharge = autoLoanDataService.GetOtherDamage();
            return objAutoOthercharge;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAnalystLoanApplication(Auto_AnalystAll objAnalystAll, int stateId)
        {
            
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var userID = Convert.ToInt32(Session["Id"].ToString());

            var res = autoLoanAssessmentService.SaveAnalystLoanApplication(objAnalystAll, stateId, userID);
            
            return res;
            //return "Failed";
        }

        #endregion Rimpo Other Tab


        #region Finalization Tab Arif

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<DaviationSettingsEntity> getDeviationByLevel(int deviationFor)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getDeviationByLevel(deviationFor);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<DaviationSettingsEntity> getDeviationLevel()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getDeviationLevel();
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string getDeviationCodeByID(int deviationID)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getDeviationCodeByID(deviationID);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<t_Pluss_Remark> getRemarkStrangthConditionWeaknessAll()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getRemarkStrangthConditionWeaknessAll();
            return res;

        }

        //[AjaxMethod(HttpSessionStateRequirement.Read)]
        //public List<t_pluss_user> gett_pluss_userAll()
        //{
        //    AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
        //    var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
        //    var res = autoLoanAssessmentService.get_pluss_userAll();
        //    return res;

        //}
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public t_pluss_userapproverlimit GetApproverLimitByUserId(int userId)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetApproverLimitByUserId(userId);
            return res;

        }


        #endregion Finalization Tab

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string ChangeStateForAnalyst(int autoLoanMasterId, string remark, int stateId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;
                objAutoStatusHistory.StatusID = stateId;
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.ChangeStateForAnalyst(objAutoStatusHistory, remark);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> getModelbyManufacturerID(int manufacturerID)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            List<AutoVehicle> objList_AutoModel = tanorAndLTVService.GetModelList(manufacturerID);
            return objList_AutoModel;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoBrandTenorLTV getValuePackAllowed(int manufacturerID, int vehicleId, int vehicleStatus)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            AutoBrandTenorLTV objAutoBrandTenorLTV = tanorAndLTVService.getvaluepackallowed(manufacturerID, vehicleId, vehicleStatus);
            return objAutoBrandTenorLTV;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoLoanVehicle CheckMouByVendoreId(int vendoreId)
        {

            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var userID = Convert.ToInt32(Session["Id"].ToString());

            AutoLoanVehicle objAutoLoanVehicle = autoLoanAssessmentService.CheckMouByVendoreId(vendoreId);

            return objAutoLoanVehicle;
        }


    }
}