﻿<%@ Page Title="Pluss | Analyst Summary" Language="C#" MasterPageFile="~/UI/AutoLoan/AutoAnalystMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.AnalystSummary" Codebehind="AnalystSummary.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<link href="../../CSS/Default.css" rel="stylesheet" type="text/css" />

 <link href="../../CSS/admin.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/AutoLoan/AnalystSummary.js" type="text/javascript"></script>
    <style type="text/css">
        #nav_AnalyzeLoanApplication a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    
    <div id="divAutoloanSummary">
    
    <div class="pageHeaderDiv">Auto Loan Summary</div>

             <div id="div84" class="fieldDiv divPadding formBackColor">
                <div class="fieldDiv">
                    <label class="lbl rightAlign">
                        LLID<span class="starColor">*</span>:</label>
                        <input type="text" id="txtLLIDMain" class="txtField widthSize25_per" onkeypress="analystSummaryManager.searchKeypress(event)"/>
                        <select class="combo" id="cmbStatus">
                            <option value="-1">Default</option>
                        </select>
                        <input type="button" id="btnLLIdSearch" class="" value="Search"/>
                </div>
                
            </div>
         
             <div class="divLeftNormalClass widthSize97p9_per">
             
           <table  cellpadding="0" cellspacing="0" frame="box" border="0" class="summary centerAlign widthSize100_per">
				        <colgroup class="sortable ">
					        <col width="80px" />
					        <col width="150px" />
					        <col width="120px" />
					        <col width="90px" />
					        <col width="100px" />
					        <col width="110px" />
					        <col width="90px" />
					        <col width="110px" />
					        <col width="110px" />
        					
				        </colgroup>
				        <thead >
					        <tr class="theadTdTextAlignCenter">
						        <th>
							        LLID
						        </th>
						        <th>
						            CUSTOMER
						        </th>
						        <th>
						            PRODUCT
						        </th>
        						<th>
						            BRAND
						        </th>
						        <th>
						            MODEL
						        </th>
						        <th>
						            LOAN AMT
						        </th>
						        <th>
						            TENOR
						        </th>
						        <th>
						            STATUS
						        </th>				
						        <th>
						            ACTION 
						        </th>
					        </tr>
				        </thead>
				        <tbody id="GridLoanSummery">
				        </tbody>
			        </table>
			        <div class="clear"></div>
			<div id="Pager" class="pager pagerFooter" > 
                            <label for="txtPageNO" class="pagerlabel">page</label>                                           
                            <input id="txtPageNo" class="txtPageNo" value="1" maxlength="7"></input>
                            <span id="spntotalPage" class="spntotalPage">of 1</span> 
                            <a id="lnkPrev" title="Previous Page" href="#" class="prevPage">prev</a> 
                            <a id="lnkNext" title="Next Page" href="#" class="nextPage">next</a>
                    </div>
            <div class="clear"></div>
            
            
            
        </div>    
            
            
                    </div>
    
</asp:Content>

