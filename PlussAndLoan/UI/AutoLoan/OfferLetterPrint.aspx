﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PlussAndLoan.UI.AutoLoan.OfferLetterPrint" Codebehind="OfferLetterPrint.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Offer Letter Print</title>
    <link href="../../CSS/AutoLoanPrint.css" rel="stylesheet" type="text/css" />

    <script src="../../Scripts/JSLibs/prototype.js" type="text/javascript"></script>

    <script src="../../Scripts/JSLibs/util.js" type="text/javascript"></script>

    <script src="../../Scripts/AutoLoan/OfferLetter.js" type="text/javascript"></script>

    <script src="../../Scripts/JSLibs/effects.js" type="text/javascript"></script>
    
    <link href="../../CSS/Default.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    
    
    
    <style type="text/css">
        table.reference
        {
            background-color: #FFFFFF;
            border: 1px solid #C3C3C3;
            border-collapse: collapse;
            width: 100%;
        }
        table.reference th
        {
            background-color: #E5EECC;
            border: 1px solid #C3C3C3;
            padding: 0px;
            vertical-align: top;
        }
        table.reference td
        {
            border: 1px solid #C3C3C3;
            padding: 0px;
            vertical-align: top;
            font-size: 12px;
        }
        table.referenceBorderNone
        {
            background-color: #FFFFFF;
            border-collapse: collapse;
            width: 100%;
        }
        table.referenceBorderNone th
        {
            background-color: #E5EECC;
            padding: 0px;
            vertical-align: top;
        }
        table.referenceBorderNone td
        {
            padding: 0px;
            vertical-align: top;
            font-size: 12px;
        }
        .style1
        {
        }
        .style2
        {
            text-align: right;
            width: 375px;
        }
        .style3
        {
            text-align: left;
        }
        .style4
        {
            text-align: left;
            width: 60px;
        }
        .style6
        {
            height: 16.5pt;
            width: 26pt;
            color: black;
            font-size: 12.0pt;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: Wingdings;
            text-align: right;
            vertical-align: top;
            white-space: normal;
            border-style: none;
            border-color: inherit;
            border-width: medium;
            padding: 0px;
        }
        .style7
        {
            vertical-align: top;
        }
        .style8
        {
            vertical-align: top;
        }
        .style9
        {
            text-align: justify;
        }
        .style11
        {
            text-align: left;
            font-weight: normal;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="margin: 0px; width: 622px;">
    <asp:Button ID="bPrint" runat="server" Text="Print" onclick="bPrint_Click" />
    <asp:Panel ID="mainPanel" runat="server">
    <div style="width: 6.5in; height: auto; text-align: left;">
        <div>
            <div style="width: auto; height: 20px;">
                <asp:Image ID="Image1" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                    Width="624px" />
            </div>
            <br />
            <br />
            Date:
            <asp:Label ID="Label1" runat="server"></asp:Label>
            <asp:Label ID="lProductId" runat="server" Visible="false"></asp:Label>
            <br />
            Ref#:
            <asp:Label ID="Label2" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="lName" runat="server" Style="font-weight: 700"></asp:Label>
            <br />
            <asp:Label ID="lAddress" runat="server"></asp:Label>
            <br />
            <br />
        </div>
        <div style="text-align: center">
            <b>Indicative offer:
                <asp:Label ID="lTotalLaon" runat="server"></asp:Label>
            </b>
            <br />
        </div>
        <div class="style1">
            Dear Sir/Madam,<br />
            <br />
            Thank you for choosing Standard Chartered Bank, Bangladesh for your vehicle financing
            requirement. With reference to your Auto Loan application Standard Chartered Bank
            is pleased to inform you that after reviewing your application,Standard Chartered
            Bank has agreed in principle to grant you an Auto Loan of&nbsp;&nbsp;
            <asp:Label ID="lOfferedLoanAmount" runat="server" Style="font-weight: 700"></asp:Label>
            &nbsp;. The details of the loan are as follows,<br />
        </div>
        <div style="text-align: center; padding-left: 40px; padding-right: 40px; border-right-style: none;
            border-bottom-style: none; border-left-style: none; border-right-width: 0px;
            border-bottom-width: 0px; border-left-width: 0px;">
            <table class="reference" style="width: 100%; font-weight: 700;">
                <tr>
                    <td class="style2">
                        Core Loan Amount
                    </td>
                    <td class="style3" colspan="2">
                        &nbsp;
                        <asp:Label ID="lCoreLoanAmount" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        Insurance Premium for Credit Life Insurance*
                    </td>
                    <td class="style3" colspan="2">
                        &nbsp;
                        <asp:Label ID="lLifeInsurance" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        Insurance Premium for Vehicle Insurance*
                    </td>
                    <td class="style3" colspan="2">
                        &nbsp;
                        <asp:Label ID="lVehicleInsurance" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        Total Loan Amount**
                    </td>
                    <td class="style3" colspan="2">
                        &nbsp;
                        <asp:Label ID="lTotalLoanAmount" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        Loan Tenor
                    </td>
                    <td class="style3" colspan="2">
                        &nbsp;
                        <asp:Label ID="lTenor" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        Interest Rate
                    </td>
                    <td class="style3" colspan="2">
                        &nbsp;
                        <asp:Label ID="lInterestRate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        Monthly Instalment for the Total Loan
                    </td>
                    <td class="style3" colspan="2">
                        &nbsp;
                        <asp:Label ID="lMonthlyInstalment" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="border-width: 0px; border-style: none; vertical-align: middle; text-align: right;"
                        class="style2" rowspan="2">
                        Repayment Method
                    </td>
                    <td class="style4">
                        &nbsp;&nbsp;
                        <asp:Label ID="lSIFrom" runat="server"></asp:Label>
                    </td>
                    <td class="style3">
                        <asp:Label ID="lSIFromAccountNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style4">
                        &nbsp;&nbsp;
                        <asp:Label ID="lSIFromBankName" runat="server"></asp:Label>
                    </td>
                    <td class="style3">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table class="referenceBorderNone" style="width: 100%; font-weight: 700;" frame="void">
                <tr>
                    <td class="style11">
                        * Choosing to avail the Insurance Premium finance affs interest rate for the total
                        loan
                    </td>
                </tr>
                <tr>
                    <td class="style11">
                        * Credit Life Insurance covers Life Insurance only
                    </td>
                </tr>
                <tr>
                    <td class="style11">
                        ** Total loan amount will be 50% of the reasonable value of the acceptable vehicle.
                    </td>
                </tr>
            </table>
        </div>
        <div class="style9">
            Standard Chartered Bank, however, will issue purchase order for your acceptance
            and availing of the loan subject to your fulfilment of the following conditions:
            <br />
        </div>
        <table style="width: 100%;">
            <tr>
                <td style="vertical-align: top" class="style7">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="style9">
                    Submission of reasonable (reasonable in the opinion of the Bank) Vehicle Price Quotation
                    once you have finalized the vehicle that you wish to purchase within 15 days of
                    issue of this letter. Please note that your final selection of vehicle brand, model,
                    manufacturing year &amp; car dealer may or may not impact Interest Rate of the loan,
                    Tenor of the loan and the Loan Amount itself that was agreed in principle. Your
                    final vehicle price quotation must contain accurate details regarding the vehicle
                    you wish to purchase.
                </td>
            </tr>
            <tr>
                <td class="style7">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: justify">
                    Satisfactory (satisfactory in the opinion of Standard Chartered Bank) Bank Statement
                    authentication report from&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lBankStatementName" runat="server" BackColor="#CCCCCC" Style="font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style7">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35" class="style9">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35"
                                class="style9" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="style9">
                    Satisfactory (satisfactory in the opinion of Standard Chartered Bank) CIB report
                    from Bangladesh Bank.
                </td>
            </tr>
            <tr>
                <td class="style7">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    Submission of your company&#39;s
                    <asp:Label ID="Label10" runat="server" Width="232px"></asp:Label>
                    &nbsp;Latest/Renewed Trade License
                </td>
            </tr>
            <tr>
                <td class="style7">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    Submission of your Tax Certificate
                </td>
            </tr>
            <tr>
                <td class="style7">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: justify">
                    Settlement of
                    <asp:Label ID="Label11" runat="server" BackColor="#999999" Width="122px"></asp:Label>
                    &nbsp;With
                    <asp:Label ID="Label12" runat="server" BackColor="#999999" Width="308px"></asp:Label>
                    &nbsp;<br />
                    A certificate issued from that Bank has to be submitted confirming full settlement
                    &amp; closure. This certificate can be submitted prior to availing the loan from
                    Standard Chartered Bank
                </td>
            </tr>
            <asp:Literal ID="hdnCondition" runat="server"></asp:Literal>
        </table>
        <br />
        <br />
        <br />
        <br />
        <br />
        <div style="text-align: justify">
            Please note that this invitation to offer from Standard Chartered Bank is only valid
            for 15 days from&nbsp;&nbsp; the date of issue of this letter after which if you
            are not able to fulfil the above conditions, your Auto Loan application in total
            will be deemed by Standard Chartered Bank as cancelled.
            <br />
            <br />
        </div>
        <table style="width: 100%;">
            <tr>
                <td style="vertical-align: top; text-align: justify;" class="style7" colspan="2">
                    However you will receive the following letters subject to the fulfilment of above
                    conditions
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    Formal approval letter addressed to you
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    Letter addressed to your Auto Dealer
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    Letter addressed to BRTA Mirpur, for joint registration
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td class="style8" colspan="2">
                    To avail an Auto Loan from Standard Chartered Bank, the precondition to finance
                    are:
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    The selection of vehicle has to be acceptable in the opinion of Standard Chartered
                    Bank
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    There may be a need to verify the vehicle and its value. This verfication and valuation
                    will be done by Standard Chartered Bank&#39;s Appointed verfication agency and valuator.
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    Registration of the vehicle jointly with Standard Chartered Bank and the loan applicant.
                    Registration to be done with Bangladesh Road Transport Authority-Mirpur, Dhaka.
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    The vehicle to be comprehensively insured in the joint names of Standard Chartered
                    Bank and the loan applicant from Standard Chartered Bank’s listed insurer.
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        width: 26pt" width="35">
                        <colgroup>
                            <col style="mso-width-source: userset; mso-width-alt: 1280; width: 26pt" width="35" />
                        </colgroup>
                        <tr height="22" style="height: 16.5pt">
                            <td class="style6" height="22" width="35">
                                q
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    The vehicle financed by Standard Chartered Bank is for personal usage by the loan
                    applicant.
                </td>
            </tr>
        </table>
        <br />
        Please note that this is only an invitation to offer and not an offer on the part
        of Standard Chartered Bank and by virtue of this letter Standard Chartered Bank
        is in no way obligated to grant you this loan.
        <br />
        <br />
        <br />
        <br />
        <br />
        <div style="border-top: 3px solid #000001; width: 299px; border-left-style: none;
            border-right-style: none; border-bottom-style: none;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; AUTHORISED
            SIGNATORY</div>
        <div style="width: auto; height: 20px;">
            <asp:Image ID="Image2" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                Width="624px" />
        </div>
    </div>
    
    
    <div class="modal" id="requiredInfoPopupDiv" style="xtop: 125px; height: 500px;
        width: 530px; *width: 530px; _width: 530px; display: none;">
        <h3>Required Information</h3>
        <div style="padding: 8px; padding-top: 0px;">
            <div style="width: 5.3in; height: auto; text-align: left;">
               
                <table class="reference" style="width: 100%;">
                <tr>
                    <td style="text-align: justify">
                        1).  Satisfactory (satisfactory in the opinion of Standard Chartered Bank) Bank Statement
                        authentication report from&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="text" class="textbox" id="txtSatisFactory" runat="server" style="width:250px; background-color:Green; color:White;" /><br /><br />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        2). Submission of your company&#39;s
                        <input type="text" class="textbox" id="txtCompanyName" runat="server" style="width:250px; background-color:Green; color:White;" />
                        &nbsp;Latest/Renewed Trade License<br /><br />
                    </td>
                </tr>
            
            
            
            <tr>
                
                <td style="text-align: justify">
                    Settlement of
                    <input type="text" class="textbox" id="txtSettlementOf" runat="server" style="width:250px; background-color:Green; color:White;" />
                    &nbsp;With
                    <input type="text" class="textbox" id="txtSettlementWith" runat="server" style="width:250px; background-color:Green; color:White;" />
                    &nbsp;<br />
                    A certificate issued from that Bank has to be submitted confirming full settlement
                    &amp; closure. This certificate can be submitted prior to availing the loan from
                    Standard Chartered Bank<br /><br />
                </td>
            </tr>
            <tr>
                <td>Additional Condition</td>
            </tr>
            <tr>
                <td>
                    <label class="lbl">Condition 1</label>
                    <textarea id="tareaCondition1" rows="2" style="width:350px; background-color:Green; color:White;" runat="server"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="lbl">Condition 2</label>
                    <textarea id="tareaCondition2" rows="2" style="width:350px; background-color:Green; color:White;" runat="server"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="lbl">Condition 3</label>
                    <textarea id="tareaCondition3" rows="2" style="width:350px; background-color:Green; color:White;" runat="server"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="lbl">Condition 4</label>
                    <textarea id="tareaCondition4" rows="2" style="width:350px; background-color:Green; color:White;" runat="server"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="lbl">Condition 5</label>
                    <textarea id="tareaCondition5" rows="2" style="width:350px; background-color:Green; color:White;" runat="server"></textarea>
                </td>
            </tr>
                </table>
                
            </div>
        </div>
        <div class="btns" style="vertical-align: bottom;">
            <hr style="width: 98%; left: 0px; display: block;" />
            <br />
            <input id="btnClose" type="button" value="Close" class="btnRight" style="height: 30px;
                width: 50px; font-weight: bold;" />
            <asp:Button ID="btnSave" CssClass="btnRight" runat="server" Text="Save" OnClick="btnSave_Click"
                Width="50px" Height="30px" Font-Bold="True" />
        </div>
    </div>
    
    </asp:Panel>
    
    </form>
</body>
</html>
