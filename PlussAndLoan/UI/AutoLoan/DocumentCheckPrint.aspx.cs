﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Entity.CheckListPrint;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class DocumentCheckPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //int llid = 10001;

            int llid = Convert.ToInt32(Request.QueryString["llid"]);

            if (llid > 0)
            {
                ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
                var loanApplicationService = new LoanApplicationService(repository);

                AutoLoanAssessmentRrepository repository1 = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository1);



                AutoLoanApplicationAll res = new AutoLoanApplicationAll();
                var res1 = autoLoanAssessmentService.getLoanLocetorInfoByLLID(llid);
                res = (AutoLoanApplicationAll)res1;


                var segment = autoLoanAssessmentService.GetEmployeSegment();


                var bank = loanApplicationService.GetBank(1);

                setValueToControl(res, segment, bank);
            }
        }

        private void setValueToControl(AutoLoanApplicationAll res, List<AutoSegmentSettings> segment, List<AutoBank> bank)
        {
            #region Heading
            lblProductId.Text = res.objAutoLoanMaster.ProductId.ToString();
            lLLID.Text = res.objAutoLoanMaster.LLID.ToString();
            var deviationLevel = "";

            if (res.Auto_An_FinalizationDeviationList.Count > 0)
            {
                var label = res.Auto_An_FinalizationDeviationList[0].Level;
                if (label == 2)
                {
                    deviationLevel = "LEVEL-2";
                }
                else if (label == 3)
                {
                    deviationLevel = "LEVEL-3";
                }
                else
                {
                    deviationLevel = "LEVEL-1";
                }

            }
            else {
                deviationLevel = "LEVEL-1";
            }


            lUnderWritingLevel.Text = deviationLevel;
            //lApplicantName.Text = res.objAutoPRApplicant.PrName;
            lApplicantName.Text = res.objAutoLoanMaster.JointApplication == true
                                  ? res.objAutoPRApplicant.PrName + ", " + res.objAutoJTApplicant.JtName
                                  : res.objAutoPRApplicant.PrName;

            if (res.objAutoPRAccountDetailList.Count > 0)
            {
                lAccountNumber.Text = res.objAutoPRAccountDetailList[0].AccountNumber;
            }
            else {
                lAccountNumber.Text = res.objAutoPRAccount.AccountNumberForSCB;
            }
            lSource.Text = res.AutoLoanAnalystApplication.BranchName;

            var segmentId = res.objAuto_An_IncomeSegmentIncome.EmployeeSegment;
            for (var i = 0; i < segment.Count; i++)
            {
                if (segmentId == segment[i].SegmentID)
                {
                    lSegment.Text = segment[i].SegmentName.ToString();
                    break;
                }
            }

            #endregion Heading

            #region

            lApplicationForm.Text = "Yes";
            lCreditCheck.Text = "Yes";
            lPhoto.Text = "Yes";
            lIncomeAssSheet.Text = "Yes";
            lTin.Text = "Yes";
            lLimitLoadingIns.Text = "Yes";
            lPhotoID.Text = "Yes";
            lBASEL2Data.Text = "Yes";
            lSalaryCertificate.Text = res.objAuto_An_FinalizationReqDoc == null?"No": res.objAuto_An_FinalizationReqDoc.LetterOfIntroduction == 1 ? "Yes" : "No";
            lDeDup.Text = "Yes";
            lPaySlip.Text = res.objAuto_An_FinalizationReqDoc==null?"No": res.objAuto_An_FinalizationReqDoc.Payslips == 1 ? "Yes" : "No";
            lNorkom.Text = "Yes";
            lTradeLisence.Text = res.objAuto_An_FinalizationReqDoc==null?"No": res.objAuto_An_FinalizationReqDoc.TradeLicence == 1 ? "Yes" : "No";
            lCIBReport.Text = "Yes";//res.objAuto_An_AlertVerificationReport.CIBOptained == 1 ? "Yes" : "No";
            lMOA.Text = res.objAuto_An_FinalizationReqDoc==null?"No": res.objAuto_An_FinalizationReqDoc.MOA_AOA == 1 ? "Yes" : "No";
            lCPVReport.Text = res.objAuto_An_FinalizationVarificationReport == null?"No":res.objAuto_An_FinalizationVarificationReport.CPVReport == 1 ? "Yes" : "No";
            lPartnership.Text = res.objAuto_An_FinalizationReqDoc == null? "No" : res.objAuto_An_FinalizationReqDoc.PartnershipDeed == 1 ? "Yes" : "No";
            lPriceVerf.Text = res.objAuto_An_FinalizationVarificationReport == null? "No" : res.objAuto_An_FinalizationVarificationReport.CarPriceVarificationReport == 1 ? "Yes" : "No";
            lFormX.Text = res.objAuto_An_FinalizationReqDoc==null?"No" : res.objAuto_An_FinalizationReqDoc.FormX == 1 ? "Yes" : "No";
            lCreditCardRep.Text = res.objAuto_An_FinalizationVarificationReport==null?"No" : res.objAuto_An_FinalizationVarificationReport.CardRepaymentHistory == 1 ? "Yes" : "No";
            lBoardRes.Text = res.objAuto_An_FinalizationReqDoc==null?"No" : res.objAuto_An_FinalizationReqDoc.BoardResolution == 1 ? "Yes" : "No";
            lBankStatVeri.Text = res.objAuto_An_FinalizationVarificationReport == null?"No" : res.objAuto_An_FinalizationVarificationReport.StatementAuthentication == 1 ? "Yes" : "No";
            lDipCert.Text = res.objAuto_An_FinalizationReqDoc== null? "No" : res.objAuto_An_FinalizationReqDoc.DiplomaCertificate == 1 ? "Yes" : "No";
            lCreditReport.Text = res.objAuto_An_FinalizationVarificationReport == null? "No" : res.objAuto_An_FinalizationVarificationReport.CreditReport == 1 ? "Yes" : "No";
            lCertFromPof.Text = res.objAuto_An_FinalizationReqDoc == null? "No" : res.objAuto_An_FinalizationReqDoc.ProfessionalCertificate == 1 ? "Yes" : "No";
            lCarVerifReport.Text = res.objAuto_An_FinalizationVarificationReport == null? "No" : res.objAuto_An_FinalizationVarificationReport.CarVerificationReport == 1 ? "Yes" : "No";
            lTaxRecipt.Text = res.objAuto_An_FinalizationReqDoc == null? "No" : res.objAuto_An_FinalizationReqDoc.MunicipalTaxRecipt == 1 ? "Yes" : "No";
            lUsedCarValuation.Text = res.objAuto_An_FinalizationVarificationReport== null? "No" : res.objAuto_An_FinalizationVarificationReport.UsedCarValuationReport == 1 ? "Yes" : "No";
            lTitleDeed.Text = res.objAuto_An_FinalizationReqDoc == null? "No" : res.objAuto_An_FinalizationReqDoc.TitleDeed == 1 ? "Yes" : "No";
            lRentalDeed.Text = res.objAuto_An_FinalizationReqDoc == null? "No" : res.objAuto_An_FinalizationReqDoc.RentalDeed == 1 ? "Yes" : "No";
            lUtilityBill.Text = res.objAuto_An_FinalizationReqDoc == null? "No" : res.objAuto_An_FinalizationReqDoc.UtilityBill == 1 ? "Yes" : "No";
            lLoanOfferLetter.Text = res.objAuto_An_FinalizationReqDoc == null?"No" : res.objAuto_An_FinalizationReqDoc.LoanOfferLatter == 1 ? "Yes" : "No";
            lGlobalConso.Text = "Yes";
            lSignedIID.Text = "Yes";
            lUsedCarDoc.Text = res.objAuto_An_FinalizationVarificationReport==null? "No" : res.objAuto_An_FinalizationVarificationReport.UsedCarDocs == 1 ? "Yes" : "No";

            #endregion

            #region Bank Account

            for (int i = 1; i <= res.objAutoPRAccountDetailList.Count; i++)
            {
                Label lOtherBank = this.FindControl("lOtherBank" + i) as Label;
                //lOtherBank.Text = res.objAutoPRAccountDetailList[i - 1].BankId.ToString();
                var bankId = res.objAutoPRAccountDetailList[i - 1].BankId;
                for (var j = 0; j < bank.Count; j++)
                {
                    if (bankId == bank[j].BankID)
                    {
                        lOtherBank.Text = bank[j].BankName;
                        break;
                    }
                }


                Label lAccNO = this.FindControl("lAccNO" + i) as Label;
                lAccNO.Text = res.objAutoPRAccountDetailList[i - 1].AccountNumber;

                Label lType = this.FindControl("lType" + i) as Label;
                if (res.objAutoPRAccountDetailList[i - 1].AccountType == "1")
                {
                    lType.Text = "Corporate";
                }
                else
                {
                    lType.Text = "Individual";
                }

                //lType.Text = res.objAutoPRAccountDetailList[i - 1].AccountType;

            }

            #endregion Bank Account

            #region CIB Request Loan
            for (var i = 0; i < res.objAuto_An_CibRequestLoan.Count; i++)
            {
                var identityField = i + 1;
                var facility = "";
                switch (res.objAuto_An_CibRequestLoan[i].CibFacilityId)
                {
                    case 1:
                        facility = "TERM LOAN";
                        break;
                    case 2:
                        facility = "REVOLVING";
                        break;
                    case 3:
                        facility = "OTHER";
                        break;
                }
                Label lfacility = this.FindControl("lFacility" + identityField) as Label;
                lfacility.Text = facility;

                Label lAmount = this.FindControl("lAmount" + identityField) as Label;
                lAmount.Text = res.objAuto_An_CibRequestLoan[i].CibAmount.ToString();

                Label lTenor = this.FindControl("lTenor" + identityField) as Label;
                lTenor.Text = res.objAuto_An_CibRequestLoan[i].CibTenor.ToString();
            }

            lAppRaiseBy.Text = res.objAutoLoanMaster.SourceName;
            lSupportedBy.Text = res.objAuto_App_Step_ExecutionByUser.AppSupportByName;

            #endregion

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //lUnderWritingLevel.Text = txtUnderWritingLevel.Text;
            lApplicationForm.Text = txtApplicationForm.Text;
            lPhoto.Text = txtPhoto.Text;
            lTin.Text = txtTin.Text;
            lPhotoID.Text = txtPhotoID.Text;
            lGlobalConso.Text = txtGlobalConso.Text;
            lSignedIID.Text = txtSignedIID.Text;
            lCreditCheck.Text = txtCreditCheck.Text;
            lIncomeAssSheet.Text = txtIncomeAssSheet.Text;
            lLimitLoadingIns.Text = txtLimitLoadingIns.Text;
            lBASEL2Data.Text = txtBASEL2Data.Text;
            lDeDup.Text = txtDeDup.Text;
            lNorkom.Text = txtNorkom.Text;
            lCIBReport.Text = txtCIBReport.Text;

            //for (int i = 1; i <=5; i++)
            //{

            //    var lblFacil =(Label) FindControl("lFacility" + i);
            //    var txtFacil = (TextBox) FindControl("txtFacility" + i);
            //    lblFacil.Text = txtFacil.Text;

            //    var lblAmount = (Label)FindControl("lAmount" + i);
            //    var txtAmount = (TextBox)FindControl("txtAmount" + i);
            //    lblAmount.Text = txtAmount.Text;

            //    var lblTenor = (Label)FindControl("lTenor" + i);
            //    var txtTenor = (TextBox)FindControl("txtToner" + i);
            //    lblTenor.Text = txtTenor.Text;

            //}

            RegisterClientScriptBlock("", "<script> docCheckListManager.PopupOpened(1);</script>");
            ConfigureCReport();
        }


        private void ConfigureCReport()
        {
            DocumentCheckListEntity dc = new DocumentCheckListEntity();

            #region
            dc.lblProductId = lblProductId.Text.Trim();
            dc.Label1 = Label1.Text.Trim();
            dc.lLLID = lLLID.Text.Trim();
            dc.lUnderWritingLevel = lUnderWritingLevel.Text.Trim();
            dc.lApplicantName = lApplicantName.Text.Trim();
            dc.lAccountNumber = lAccountNumber.Text.Trim();
            dc.lSource = lSource.Text.Trim();
            dc.lSegment = lSegment.Text.Trim();
            dc.lApplicationForm = lApplicationForm.Text.Trim();
            dc.lPhoto = lPhoto.Text.Trim();
            dc.lTin = lTin.Text.Trim();
            dc.lPhotoID = lPhotoID.Text.Trim();
            dc.lSalaryCertificate = lSalaryCertificate.Text.Trim();
            dc.lPaySlip = lPaySlip.Text.Trim();
            dc.lTradeLisence = lTradeLisence.Text.Trim();
            dc.lMOA = lMOA.Text.Trim();
            dc.lPartnership = lPartnership.Text.Trim();
            dc.lFormX = lFormX.Text.Trim();
            dc.lBoardRes = lBoardRes.Text.Trim();
            dc.lDipCert = lDipCert.Text.Trim();
            dc.lCertFromPof = lCertFromPof.Text.Trim();
            dc.lTaxRecipt = lTaxRecipt.Text.Trim();
            dc.lTitleDeed = lTitleDeed.Text.Trim();
            dc.lRentalDeed = lRentalDeed.Text.Trim();
            dc.lUtilityBill = lUtilityBill.Text.Trim();
            dc.lLoanOfferLetter = lLoanOfferLetter.Text.Trim();
            dc.lGlobalConso = lGlobalConso.Text.Trim();
            dc.lSignedIID = lSignedIID.Text.Trim();
            dc.lUsedCarDoc = lUsedCarDoc.Text.Trim();
            dc.lCreditCheck = lCreditCheck.Text.Trim();
            dc.lIncomeAssSheet = lIncomeAssSheet.Text.Trim();
            dc.lLimitLoadingIns = lLimitLoadingIns.Text.Trim();
            dc.lBASEL2Data = lBASEL2Data.Text.Trim();
            dc.lDeDup = lDeDup.Text.Trim();
            dc.lNorkom = lNorkom.Text.Trim();
            dc.lCIBReport = lCIBReport.Text.Trim();
            dc.lCPVReport = lCPVReport.Text.Trim();
            dc.lPriceVerf = lPriceVerf.Text.Trim();
            dc.lCreditCardRep = lCreditCardRep.Text.Trim();
            dc.lBankStatVeri = lBankStatVeri.Text.Trim();
            dc.lCreditReport = lCreditReport.Text.Trim();
            dc.lCarVerifReport = lCarVerifReport.Text.Trim();
            dc.lUsedCarValuation = lUsedCarValuation.Text.Trim();
            dc.lFacility1 = lFacility1.Text.Trim();
            dc.lAmount1 = lAmount1.Text.Trim();
            dc.lTenor1 = lTenor1.Text.Trim();
            dc.lFacility2 = lFacility2.Text.Trim();
            dc.lAmount2 = lAmount2.Text.Trim();
            dc.lTenor2 = lTenor2.Text.Trim();
            dc.lFacility3 = lFacility3.Text.Trim();
            dc.lAmount3 = lAmount3.Text.Trim();
            dc.lTenor3 = lTenor3.Text.Trim();
            dc.lFacility4 = lFacility4.Text.Trim();
            dc.lAmount4 = lAmount4.Text.Trim();
            dc.lTenor4 = lTenor4.Text.Trim();
            dc.lFacility5 = lFacility5.Text.Trim();
            dc.lAmount5 = lAmount5.Text.Trim();
            dc.lTenor5 = lTenor5.Text.Trim();
            dc.lOtherBank1 = lOtherBank1.Text.Trim();
            dc.lAccNO1 = lAccNO1.Text.Trim();
            dc.lType1 = lType1.Text.Trim();
            dc.lOtherBank2 = lOtherBank2.Text.Trim();
            dc.lAccNO2 = lAccNO2.Text.Trim();
            dc.lType2 = lType2.Text.Trim();
            dc.lOtherBank3 = lOtherBank3.Text.Trim();
            dc.lAccNO3 = lAccNO3.Text.Trim();
            dc.lType3 = lType3.Text.Trim();
            dc.lOtherBank4 = lOtherBank4.Text.Trim();
            dc.lAccNO4 = lAccNO4.Text.Trim();
            dc.lType4 = lType4.Text.Trim();
            dc.lOtherBank5 = lOtherBank5.Text.Trim();
            dc.lAccNO5 = lAccNO5.Text.Trim();
            dc.lType5 = lType5.Text.Trim();
            dc.lAppRaiseBy = lAppRaiseBy.Text.Trim();
            dc.lSupportedBy = lSupportedBy.Text.Trim();

            #endregion

            Session["DocumentCheckListPrintObject"] = dc;
            Response.Redirect("DocumentCheckPrintCrystal.aspx");
        }

    }
}
