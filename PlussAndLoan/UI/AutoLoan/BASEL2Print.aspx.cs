﻿using System;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Entity.CheckListPrint;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class BASEL2Print : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int llid = 0;

            llid = Convert.ToInt32(Request.QueryString["llid"]);

            if (llid > 0)
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var loanAssesmentService = new AutoLoanAssessmentService(repository);
                AutoLoanApplicationAll res = new AutoLoanApplicationAll();
                var res1 = loanAssesmentService.getLoanLocetorInfoByLLID(llid);
                res = (AutoLoanApplicationAll)res1;
                setValueToControl(res);
                configureReprot();
            }
        }

        private void configureReprot()
        {
            Basal2Entity b2 = new Basal2Entity();

            #region
            b2.lCustomarName = lCustomarName.Text.Trim();
            b2.lCustomerMaster = lCustomerMaster.Text.Trim();
            b2.lHighestEducation = lHighestEducation.Text.Trim();
            b2.lEmploymentStatusCode = lEmploymentStatusCode.Text.Trim();
            b2.lBusinessEstablishDate = lBusinessEstablishDate.Text.Trim();
            b2.lMonthOfTotalWorkExperience = lMonthOfTotalWorkExperience.Text.Trim();
            b2.lMonthInCurrentJob = lMonthInCurrentJob.Text.Trim();
            b2.lDateOfJoining = lDateOfJoining.Text.Trim();
            b2.sdfsadfsd = sdfsadfsd.Text.Trim();
            b2.lOtherMonthlyIncome = lOtherMonthlyIncome.Text.Trim();
            b2.lMonthlyLoanRepayment = lMonthlyLoanRepayment.Text.Trim();
            b2.lOutstandingAmount = lOutstandingAmount.Text.Trim();
            b2.lTypeOfAccount = lTypeOfAccount.Text.Trim();
            b2.lLoanAccountNumber = lLoanAccountNumber.Text.Trim();
            b2.lDateLoanBooked = lDateLoanBooked.Text.Trim();
            b2.lDesignation = lDesignation.Text.Trim();
            b2.lDepartment = lDepartment.Text.Trim();
            b2.lDateOfBirth = lDateOfBirth.Text.Trim();
            b2.lWithdrawalDate = lWithdrawalDate.Text.Trim();
            b2.lWithdrawalReason = lWithdrawalReason.Text.Trim();
            b2.lCreditPolicy = lCreditPolicy.Text.Trim();
            b2.lSCBUnsecuredExposure = lSCBUnsecuredExposure.Text.Trim();
            b2.lSCBSecuredExposure = lSCBSecuredExposure.Text.Trim();
            b2.lLTV = lLTV.Text.Trim();
            b2.lFTV = lFTV.Text.Trim();
            b2.lTotalLTV = lTotalLTV.Text.Trim();
            b2.lTotalFTV = lTotalFTV.Text.Trim();
            b2.lDeviationCodeAssetOps = lDeviationCodeAssetOps.Text.Trim();
            b2.lDIRatio = lDIRatio.Text.Trim();
            b2.lInternalBacklistReason = lInternalBacklistReason.Text.Trim();
            b2.lYearsOfResidence = lYearsOfResidence.Text.Trim();
            b2.lRentalPerMonth = lRentalPerMonth.Text.Trim();
            b2.lNoOfDependents = lNoOfDependents.Text.Trim();
            b2.lHomeTelNo = lHomeTelNo.Text.Trim();
            b2.lOfficeTelNo = lOfficeTelNo.Text.Trim();
            b2.lMobileNo = lMobileNo.Text.Trim();
            b2.lNumbreOfCars = lNumbreOfCars.Text.Trim();
            b2.lPackagePromoCode = lPackagePromoCode.Text.Trim();
            b2.lCoBorrower1Relationship = lCoBorrower1Relationship.Text.Trim();
            b2.lCoBorrower1SCBFlag = lCoBorrower1SCBFlag.Text.Trim();
            b2.lPurchaseDate = lPurchaseDate.Text.Trim();
            b2.lApplicationDate = lApplicationDate.Text.Trim();
            b2.lTopUpAmount = lTopUpAmount.Text.Trim();
            b2.lNumberOfTopUp = lNumberOfTopUp.Text.Trim();
            b2.lTopUp1Amount = lTopUp1Amount.Text.Trim();
            b2.lTopUp1Date = lTopUp1Date.Text.Trim();
            b2.lTopUp2Amount = lTopUp2Amount.Text.Trim();
            b2.lTopUp2Date = lTopUp2Date.Text.Trim();
            b2.lTopUp3Amount = lTopUp3Amount.Text.Trim();
            b2.lTopUp3Date = lTopUp3Date.Text.Trim();
            b2.lPriceOfCar = lPriceOfCar.Text.Trim();
            b2.lCurrentMarketValue = lCurrentMarketValue.Text.Trim();
            b2.lCarModel = lCarModel.Text.Trim();
            b2.lCarMadeYear = lCarMadeYear.Text.Trim();
            b2.lEngineCC = lEngineCC.Text.Trim();
            b2.lCarUsage = lCarUsage.Text.Trim();
            b2.lDealerCode = lDealerCode.Text.Trim();
            b2.lNameOfFriend = lNameOfFriend.Text.Trim();
            b2.lFriendRelativeContact = lFriendRelativeContact.Text.Trim();
            #endregion

            Session["Basel2Object"] = b2;
            Response.Redirect("BASEL2PrintCrystal.aspx");
        }

        private void setValueToControl(AutoLoanApplicationAll res)
        {

            #region ACCEPTABLE VALUE / SOURCE

            if (res.objAutoLoanMaster.JointApplication)
            {
                lCustomarName.Text = res.objAutoPRApplicant.PrName + " & " + res.objAutoJTApplicant.JtName;
            }
            else {
                lCustomarName.Text = res.objAutoPRApplicant.PrName;
            }
            lCustomerMaster.Text = res.AutoApplicantSCBAccountList[0].AccountNumberSCB;
            //lGender.Text = "";
            //lMaritalStatus.Text = "";

            var prgender = res.objAutoPRApplicant.Gender == 1 ? "Male" : "Female";

            //lblPrimaryApplicantGender.Text = prgender.ToString();
            //lblPrMaritialStatus.Text = res.objAutoPRApplicant.MaritalStatus;
            //if (res.objAutoLoanMaster.JointApplication) {
            //    var jtGender = res.objAutoJTApplicant.Gender == 1 ? "Male" : "Female";
            //    lblJointApplicanGender.Text = jtGender.ToString();
            //    lblJtMaritialStatus.Text = res.objAutoJTApplicant.MaritalStatus;
            //}

            


            lHighestEducation.Text = res.objAutoPRApplicant.HighestEducation;
            lEmploymentStatusCode.Text = res.objAuto_An_IncomeSegmentIncome.EmployeeSegmentCode;
            lBusinessEstablishDate.Text = "";//Convert.ToString(DateTime.Now.AddYears(res.objAutoPRProfession.YearsInBussiness).ToString("dd/MM/yyyy"));
            lMonthOfTotalWorkExperience.Text = "";
            lMonthInCurrentJob.Text = res.objAutoPRProfession.MonthinCurrentProfession;
            lDateOfJoining.Text = "";
            //lMonthlyIncome.Text = "";
            lOtherMonthlyIncome.Text = res.objAuto_An_IncomeSegmentIncome.TotalJointIncome.ToString();
            lMonthlyLoanRepayment.Text = "";
            lOutstandingAmount.Text = "";
            lTypeOfAccount.Text = "";
            lLoanAccountNumber.Text = "";
            lDateLoanBooked.Text = "";
            lDesignation.Text = "";
            lDepartment.Text = "";
            lDateOfBirth.Text = res.objAutoPRApplicant.DateofBirth.ToString("dd/MM/yyyy");

            #endregion ACCEPTABLE VALUE / SOURCE

            #region CREDIT INITIATION SHEET

            lWithdrawalDate.Text = "";
            lWithdrawalReason.Text = "";
            //lOverrideFlag.Text = "";
            //lPreApprovalFlag.Text = "";
            //lIncomeVerificationFlag.Text = "";
            //lEmploymentStatusVerification.Text = "";
            lCreditPolicy.Text = "";
            lSCBUnsecuredExposure.Text = "";
            lSCBSecuredExposure.Text = "";
            lLTV.Text = "";
            lFTV.Text = "";
            lTotalLTV.Text = "";
            lTotalFTV.Text = "";
            //if (res.objAutoPRApplicant.ResidenceAddress == "")
            //{
            //    lResidenceAddressVerification.Text = "No";
            //}
            //else {
            //    lResidenceAddressVerification.Text = "Yes";
            //}

            //if (res.objAutoPRApplicant.PhoneResidence == "")
            //{
            //    lHomeTelephoneVerification.Text = "No";
            //}
            //else {
            //    lHomeTelephoneVerification.Text = "Yes";
            //}
            //if (res.objAutoPRProfession.OfficePhone == "")
            //{
            //    lOfficeTelephoneVerificationFlag.Text = "No";
            //}
            //else {
            //    lOfficeTelephoneVerificationFlag.Text = "Yes"; 
            //}
            //if (res.objAutoPRApplicant.Mobile == "")
            //{
            //    lMobileTelephoneVerificationFlag.Text = "No";
            //}
            //else {
            //    lMobileTelephoneVerificationFlag.Text = "Yes";
            //}
            lDeviationCodeAssetOps.Text = "";
            lDIRatio.Text = Convert.ToString(Math.Ceiling(res.objAuto_An_LoanCalculationTotal.FBRIncludingInsurance));
            //lInternalBlacklistFlag.Text = "";
            lInternalBacklistReason.Text = "";
            //lResidenceState.Text = res.objAutoPRApplicant.ResidenceAddress.ToString();
            //lResidenceType.Text = res.objAutoPRApplicant.ResidenceStatus == "-1" ? "" : res.objAutoPRApplicant.ResidenceStatus.ToString();

            #endregion CREDIT INITIATION SHEET

            #region 2nd Page Continue

            lYearsOfResidence.Text = res.objAutoPRApplicant.ResidenceStatusYear;
            lRentalPerMonth.Text = "";
            lNoOfDependents.Text = res.objAutoPRApplicant.NumberofDependents;
            lHomeTelNo.Text = res.objAutoPRApplicant.PhoneResidence;
            lOfficeTelNo.Text = res.objAutoPRProfession.OfficePhone;
            lMobileNo.Text = res.objAutoPRApplicant.Mobile;
            lNumbreOfCars.Text = "";
            lPackagePromoCode.Text = "";
            //lGuarantor1ScbFlag.Text = "";
            lCoBorrower1Relationship.Text = "";
            lCoBorrower1SCBFlag.Text = "";
            lPurchaseDate.Text = "";
            lApplicationDate.Text = res.objAutoLoanMaster.ReceiveDate.ToString();
            //Label83.Text = "";
            //Label80.Text = "";
            //Label81.Text = "";
            //Label82.Text = "";
            //Label84.Text = "";
            //Label85.Text = "";
            lTopUpAmount.Text = "";
            lNumberOfTopUp.Text = "";
            lTopUp1Amount.Text = "";
            lTopUp1Date.Text = "";
            lTopUp2Amount.Text = "";
            lTopUp2Date.Text = "";
            lTopUp3Amount.Text = "";
            lTopUp3Date.Text = "";
            lPriceOfCar.Text = "";
            lCurrentMarketValue.Text = "";
            lCarModel.Text = res.objAutoLoanVehicle.Model;
            lCarMadeYear.Text = res.objAutoLoanVehicle.ManufacturingYear.ToString();
            lEngineCC.Text = res.objAutoLoanVehicle.EngineCC;
            lCarUsage.Text = "";
            lDealerCode.Text = "";
            //lAccountCloseReason.Text = "";
            //lChargeOfReasonCode.Text = "";
            lNameOfFriend.Text = "";
            lFriendRelativeContact.Text = "";

            #endregion 2nd page continue

        }









    }
}
