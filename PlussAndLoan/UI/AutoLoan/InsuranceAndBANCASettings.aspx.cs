﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class InsuranceAndBANCASettings : System.Web.UI.Page
    {
        [AjaxNamespace("InsuranceAndBANCASettings")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof (InsuranceAndBANCASettings));
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<Auto_InsuranceBanca> GetInsuranceAndBancaSettingsSummary(int pageNo, int pageSize, string search)
        {
            pageNo = pageNo * pageSize;

            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);
            

            List<Auto_InsuranceBanca> objListAuto_InsuranceBanca =
                autoLoanDataService.GetInsuranceAndBancaSettingsSummary(pageNo, pageSize, search);
            return objListAuto_InsuranceBanca;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAuto_InsuranceBanca(Auto_InsuranceBanca insurance)
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);
            var res = autoLoanDataService.SaveAuto_InsuranceBanca(insurance);
            return res;

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string InsuranceDeleteById(int insurenceId)
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);
            var res = autoLoanDataService.InsuranceDeleteById(insurenceId);
            return res;

            
        }
        
    }
}
