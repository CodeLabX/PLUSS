﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PlussAndLoan.UI.AutoLoan.AutoLoanCheckListPrint" Codebehind="AutoLoanCheckListPrint.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pluss | Auto Loan Check List </title>
    <style type="text/css">
        .style53
        {
            width: 119px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style54
        {
            width: 88px;
            height: 21px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style56
        {
            width: 88px;
            height: 14px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style57
        {
            height: 16px;
            background-color: #CCCCCC;
        }
        .style58
        {
            width: 147px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style61
        {
            height: 16px;
            width: 94px;
            background-color: #CCCCCC;
        }
        .style62
        {
            height: 21px;
            width: 94px;
            background-color: #CCCCCC;
        }
        .style65
        {
            height: 14px;
            width: 158px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style66
        {
            height: 8px;
            width: 158px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style68
        {
            height: 8px;
            text-align: right;
            font-family: "Arial Narrow";
            width: 116px;
        }
        .style69
        {
            height: 14px;
            text-align: right;
            width: 116px;
            font-family: "Arial Narrow";
        }
        .style71
        {
            width: 88px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style72
        {
            width: 116px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style74
        {
            width: 89px;
            font-size: x-small;
            height: 16px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style75
        {
            width: 89px;
            font-size: x-small;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style77
        {
            height: 16px;
            text-align: right;
            width: 102px;
            font-family: "Arial Narrow";
        }
        .style79
        {
            width: 102px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style80
        {
            width: 124px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style81
        {
            height: 16px;
            width: 124px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style83
        {
            width: 64px;
            background-color: #CCCCCC;
        }
        .style84
        {
            height: 16px;
            width: 64px;
            background-color: #CCCCCC;
        }
        .style00
        {
            width: 87px;
            font-size: 11px;
        }
        .style95
        {
            width: 31px;
        }
        .style105
        {
        }
        .style106
        {
            width: 74px;
        }
        .style108
        {
            width: 68px;
        }
        .style109
        {
            width: 66px;
        }
        .style110
        {
            text-align: center;
        }
        .style111
        {
            width: 66px;
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
        .style112
        {
            width: 68px;
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
        .style113
        {
            width: 58px;
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
        .style115
        {
            width: 74px;
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
        .style116
        {
            width: 110px;
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
        .style117
        {
            width: 31px;
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
        .style118
        {
            width: 81px;
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
        .style119
        {
            text-align: center;
            font-weight: bold;
            background-color: #999999;
        }
        .style120
        {
            font-weight: bold;
        }
        .style121
        {
            font-size: small;
            font-weight: bold;
        }
        .style122
        {
            font-size: small;
        }
        .style123
        {
            width: 598px;
            font-weight: bold;
        }
        .style125
        {
            width: 479px;
            text-align: center;
            background-color: #CCCCCC;
        }
        .style128
        {
        }
        .style129
        {
            width: 331px;
        }
        .style131
        {
            width: 310px;
            text-align: center;
            background-color: #CCCCCC;
        }
        .style132
        {
            font-family: "Arial Narrow";
        }
        .style133
        {
            width: 174px;
            text-align: right;
            font-family: "Arial Narrow";
            font-size: 10px;
        }
        table.reference
        {
            background-color: #FFFFFF;
            border: 1px solid #C3C3C3;
            border-collapse: collapse;
            width: 100%;
        }
        table.reference th
        {
            background-color: #E5EECC;
            border: 1px solid #C3C3C3;
            padding: 0px;
            vertical-align: top;
        }
        table.reference td
        {
            border: 1px solid #C3C3C3;
            padding: 0px;
            vertical-align: top;
            font-size: 12px;
        }
        .style136
        {
            width: 119px;
            text-align: right;
            font-family: "Arial Narrow";
            height: 10px;
        }
        .style137
        {
            width: 169px;
            height: 10px;
            background-color: #CCCCCC;
        }
        .style138
        {
            width: 147px;
            text-align: right;
            font-family: "Arial Narrow";
            height: 10px;
        }
        .style139
        {
            height: 10px;
        }
        .style140
        {
            width: 119px;
            text-align: right;
            font-family: "Arial Narrow";
            height: 2px;
        }
        .style141
        {
            width: 169px;
            height: 2px;
            background-color: #CCCCCC;
        }
        .style142
        {
            width: 147px;
            text-align: right;
            font-family: "Arial Narrow";
            height: 2px;
        }
        .style143
        {
            height: 2px;
        }
        .style144
        {
            width: 110px;
        }
        .style145
        {
            width: 36px;
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
        .style146
        {
            width: 36px;
        }
        .style147
        {
            width: 58px;
        }
        .style150
        {
            width: 174px;
            text-align: right;
            font-family: "Arial Narrow";
            font-size: 10px;
            height: 6px;
        }
        .style151
        {
            text-align: center;
            height: 6px;
            background-color: #CCCCCC;
        }
        .style153
        {
            width: 226px;
        }
        .style155
        {
            height: 19px;
            background-color: #CCCCCC;
        }
        .style156
        {
            width: 310px;
            height: 19px;
            background-color: #CCCCCC;
        }
        .style160
        {
            width: 91px;
        }
        .style161
        {
            width: 91px;
            text-align: center;
            background-color: #999999;
        }
        .style162
        {
            text-align: center;
            width: 396px;
            background-color: #999999;
        }
        .style163
        {
            width: 396px;
            background-color: #CCCCCC;
        }
        .style164
        {
            width: 396px;
            text-align: right;
        }
        .style165
        {
        }
        .style166
        {
            width: 80px;
        }
        .style167
        {
            width: 51px;
            text-align: center;
        }
        .style168
        {
            width: 404px;
            height: 15px;
        }
        .style169
        {
            width: 51px;
            text-align: center;
            height: 15px;
        }
        .style170
        {
            width: 80px;
            height: 15px;
        }
        .style171
        {
            height: 15px;
        }
        .style172
        {
            font-size: x-small;
        }
        .style173
        {
            width: 270px;
        }
        .style174
        {
            width: 70px;
        }
        .style175
        {
            text-align: right;
            width: 106px;
        }
        .style176
        {
            text-align: right;
        }
        .style181
        {
            width: 73px;
        }
        .style183
        {
            width: 163px;
            background-color: #CCCCCC;
        }
        .style185
        {
            width: 30px;
            background-color: #CCCCCC;
        }
        .style186
        {
            width: 109px;
            background-color: #CCCCCC;
        }
        .style187
        {
            width: 205px;
        }
        .style188
        {
            width: 208px;
        }
        .style191
        {
            width: 88px;
            height: 8px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style192
        {
            height: 8px;
            width: 94px;
            background-color: #CCCCCC;
        }
        .style194
        {
            width: 89px;
            font-size: x-small;
            height: 9px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style195
        {
            height: 9px;
            width: 94px;
            background-color: #CCCCCC;
        }
        .style196
        {
            height: 9px;
            width: 124px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style197
        {
            height: 9px;
            width: 64px;
            background-color: #CCCCCC;
        }
        .style198
        {
            height: 9px;
            text-align: right;
            font-family: "Arial Narrow";
            width: 102px;
        }
        .style199
        {
            height: 9px;
            background-color: #CCCCCC;
        }
        .style200
        {
            width: 89px;
            font-size: x-small;
            height: 13px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style201
        {
            height: 13px;
            width: 94px;
            background-color: #CCCCCC;
        }
        .style202
        {
            height: 13px;
            width: 124px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style203
        {
            height: 13px;
            width: 64px;
            background-color: #CCCCCC;
        }
        .style204
        {
            height: 13px;
            text-align: right;
            font-family: "Arial Narrow";
            width: 102px;
        }
        .style205
        {
            height: 13px;
            background-color: #CCCCCC;
        }
        .style206
        {
            width: 89px;
            font-size: x-small;
            height: 14px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style208
        {
            height: 14px;
            width: 124px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style209
        {
            height: 14px;
            width: 64px;
            background-color: #CCCCCC;
        }
        .style210
        {
            height: 14px;
            text-align: right;
            font-family: "Arial Narrow";
            width: 102px;
        }
        .style211
        {
            width: 89px;
            font-size: x-small;
            height: 15px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style212
        {
            height: 15px;
            width: 94px;
            background-color: #CCCCCC;
        }
        .style213
        {
            height: 15px;
            width: 124px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style214
        {
            height: 15px;
            width: 64px;
            background-color: #CCCCCC;
        }
        .style215
        {
            height: 15px;
            text-align: right;
            font-family: "Arial Narrow";
            width: 102px;
        }
        .style216
        {
            width: 173px;
        }
        .style217
        {
            width: 178px;
        }
        .style218
        {
            width: 169px;
            background-color: #CCCCCC;
        }
        .style219
        {
            width: 158px;
            text-align: right;
            font-family: "Arial Narrow";
        }
        .style220
        {
            height: 14px;
            width: 94px;
            background-color: #CCCCCC;
        }
        .style221
        {
            width: 94px;
            background-color: #CCCCCC;
        }
        .style222
        {
            height: 8px;
            background-color: #CCCCCC;
        }
        .style223
        {
            height: 14px;
            background-color: #CCCCCC;
        }
        .style224
        {
            background-color: #CCCCCC;
        }
        .style225
        {
            height: 15px;
            background-color: #CCCCCC;
        }
        .style226
        {
            text-align: center;
            background-color: #CCCCCC;
        }
        .style227
        {
            width: 66px;
            background-color: #CCCCCC;
        }
        .style228
        {
            width: 68px;
            background-color: #CCCCCC;
        }
        .style229
        {
            width: 58px;
            background-color: #CCCCCC;
        }
        .style230
        {
            width: 36px;
            background-color: #CCCCCC;
        }
        .style231
        {
            width: 74px;
            background-color: #CCCCCC;
        }
        .style232
        {
            width: 110px;
            background-color: #CCCCCC;
        }
        .style233
        {
            width: 31px;
            background-color: #CCCCCC;
        }
        .style235
        {
            background-color: #999999;
        }
        .style236
        {
            width: 310px;
            background-color: #CCCCCC;
        }
        .style237
        {
            height: 16px;
        }
        .style238
        {
            text-align: center;
            background-color: #999999;
        }
        .style239
        {
            width: 91px;
            background-color: #CCCCCC;
        }
        .style240
        {
            text-align: center;
            background-color: #CCCCCC;
            height: 15px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 6.5in; height: auto; text-align: left;">
        <div style="width: auto; height: 20px;">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Image/headBorder.JPG" Height="20px"
                Width="624px" />
        </div>
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Larger" Style="font-size: large">AUTO LOAN CHECKLIST</asp:Label>
        <b style="text-align: left"><span class="style122">APPLICATION & CUSTOMER DETAILS</span></b><table
            class="reference" style="width: 100%; font-size: small;">
            <tr>
                <td class="style140">
                    LLID
                </td>
                <td class="style141">
                    <asp:Label ID="lLLID" runat="server"></asp:Label>
                </td>
                <td class="style142">
                    APPLICANT(S)
                </td>
                <td class="style143">
                    <asp:Label ID="lApplicant" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style53">
                    PRODUCT
                </td>
                <td class="style218">
                    <asp:Label ID="lProduct" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                    <asp:Label ID="lblProductId" runat="server" Style="background-color: #CCCCCC" Visible="false"></asp:Label>
                    
                </td>
                <td class="style58">
                    AGE
                </td>
                <td>
                    <asp:Label ID="lAge" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style53">
                    SOURCE
                </td>
                <td class="style218">
                    <asp:Label ID="lSource" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
                <td class="style58">
                    INCOME
                </td>
                <td>
                    <asp:Label ID="lIncome" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style136">
                    SUBMISSION DATE
                </td>
                <td class="style137">
                    <asp:Label ID="lSubmissionDate" runat="server"></asp:Label>
                </td>
                <td class="style138">
                    BORROWING RELATIONSHIP
                </td>
                <td class="style139">
                    <asp:Label ID="lBorrowingRelationship" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style53">
                    APPRAISAL DATE
                </td>
                <td class="style218">
                    <asp:Label ID="lAppraisalDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style53">
                    APPLICATION TYPE
                </td>
                <td class="style218">
                    <asp:Label ID="lApplicationType" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style53">
                    PURPOSE
                </td>
                <td class="style218">
                    <asp:Label ID="lPurpose" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <span class="style121">VEHICLE & VENDOR DETAILS</span><br />
        <table class="reference" style="width: 100%; font-size: small;">
            <tr>
                <td class="style71">
                    BRAND
                </td>
                <td style="text-align: center" class="style221">
                    <asp:Label ID="lBrand" runat="server"></asp:Label>
                </td>
                <td class="style219">
                    ENGINE
                </td>
                <td style="text-align: center" class="style224">
                    <asp:Label ID="lEngine" runat="server"></asp:Label>
                </td>
                <td class="style72">
                    QUOTED PRICE
                </td>
                <td style="text-align: center" class="style224">
                    <asp:Label ID="lQuotedPrice" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style71">
                    MODEL
                </td>
                <td style="text-align: center" class="style221">
                    <asp:Label ID="lModel" runat="server"></asp:Label>
                </td>
                <td class="style219">
                    MANUFACTURING YEAR
                </td>
                <td style="text-align: center" class="style224">
                    <asp:Label ID="lManufacturingYear" runat="server"></asp:Label>
                </td>
                <td class="style72">
                    CONSIDERED PRICE
                </td>
                <td style="text-align: center" class="style224">
                    <asp:Label ID="lConsideredPrice" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style56">
                    VEHICLE TYPE
                </td>
                <td style="text-align: center" class="style220">
                    <asp:Label ID="lVehicleType" runat="server"></asp:Label>
                </td>
                <td class="style65">
                    SEATING CAPACITY (TOTAL)
                </td>
                <td style="text-align: center" class="style223">
                    <asp:Label ID="lSeatingCapacity" runat="server"></asp:Label>
                </td>
                <td class="style69">
                    CONSIDERATION ON
                </td>
                <td style="text-align: center" class="style223">
                    <asp:Label ID="lConsiderationON" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style56">
                    VEHICLE STATUS
                </td>
                <td style="text-align: center" class="style220">
                    <asp:Label ID="lVehicleStatus" runat="server"></asp:Label>
                </td>
                <td class="style65">
                    AGE AT LOAN EXPIRY
                </td>
                <td style="text-align: center" class="style223">
                    <asp:Label ID="lAgeAtLoanExpory" runat="server"></asp:Label>
                </td>
                <td class="style69">
                    CAR VERIFICATION
                </td>
                <td style="text-align: center" class="style223">
                    <asp:Label ID="lCarVerification" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style191">
                    VENDOR
                </td>
                <td style="text-align: center" class="style192">
                    <asp:Label ID="lVendor" runat="server"></asp:Label>
                </td>
                <td class="style66">
                    VENDOR CATEGORY
                </td>
                <td style="text-align: center" class="style222">
                    <asp:Label ID="lVendorCategory" runat="server"></asp:Label>
                </td>
                <td class="style68">
                    VERIFICATION STATUS
                </td>
                <td style="text-align: center" class="style222">
                    <asp:Label ID="lVerificationStatus" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style54">
                    MOU
                </td>
                <td style="text-align: center" class="style62">
                    <asp:Label ID="lMou" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <span class="style121">LOAN INFORMATION</span>
        <br />
        <table class="reference" style="width: 100%; font-size: small;">
            <tr>
                <td class="style75">
                    PO AMOUNT
                </td>
                <td style="text-align: center" class="style221">
                    <asp:Label ID="lPOAmount" runat="server"></asp:Label>
                </td>
                <td class="style80">
                    LTV
                </td>
                <td style="text-align: center" class="style83">
                    <asp:Label ID="lLTV" runat="server"></asp:Label>
                </td>
                <td class="style79">
                    REPAYMENT
                </td>
                <td style="text-align: center" class="style224">
                    <asp:Label ID="lRepayment" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style75">
                    GEN INSURANCE
                </td>
                <td style="text-align: center" class="style221">
                    <asp:Label ID="lGenInsurance" runat="server"></asp:Label>
                </td>
                <td class="style80">
                    LTV ( WITH INSURANCE)
                </td>
                <td style="text-align: center" class="style83">
                    <asp:Label ID="lLTVWithInsurance" runat="server"></asp:Label>
                </td>
                <td class="style79">
                    BANK NAME
                </td>
                <td style="text-align: center" class="style224">
                    <asp:Label ID="lBankName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style74">
                    ARTA
                </td>
                <td style="text-align: center" class="style61">
                    <asp:Label ID="lARTA" runat="server"></asp:Label>
                </td>
                <td class="style81">
                    EXTRA LTV
                </td>
                <td style="text-align: center" class="style84">
                    <asp:Label ID="lExtraLTV" runat="server"></asp:Label>
                </td>
                <td class="style77">
                    ACCOUNT NUMBER
                </td>
                <td style="text-align: center" class="style57">
                    <asp:Label ID="lAccountNumber" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style211">
                    TOTAL LOAN
                </td>
                <td style="text-align: center" class="style212">
                    <asp:Label ID="lTotalLoan" runat="server"></asp:Label>
                </td>
                <td class="style213">
                    ALLOWED DBR
                </td>
                <td style="text-align: center" class="style214">
                    <asp:Label ID="lAllowedDBR" runat="server"></asp:Label>
                </td>
                <td class="style215">
                    SECURITY PDC
                </td>
                <td style="text-align: center" class="style225">
                    <asp:Label ID="lSecurityPDC" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style206">
                    EMI
                </td>
                <td style="text-align: center" class="style220">
                    <asp:Label ID="lEMI" runat="server"></asp:Label>
                </td>
                <td class="style208">
                    GIVEN DBR
                </td>
                <td style="text-align: center" class="style209">
                    <asp:Label ID="lGivenDBR" runat="server"></asp:Label>
                </td>
                <td class="style210">
                    SEC. PDC AMOUNT
                </td>
                <td style="text-align: center" class="style223">
                    <asp:Label ID="lSecPDCAmount" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style200">
                    TENOR
                </td>
                <td style="text-align: center" class="style201">
                    <asp:Label ID="lTenor" runat="server"></asp:Label>
                </td>
                <td class="style202">
                    DBR ( WITH INSURANCE)
                </td>
                <td style="text-align: center" class="style203">
                    <asp:Label ID="lDBRWithInsurance" runat="server"></asp:Label>
                </td>
                <td class="style204">
                    NO of SEC. PDC
                </td>
                <td style="text-align: center" class="style205">
                    <asp:Label ID="lNoOfSecPDC" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style194">
                    INTEREST RATE
                </td>
                <td style="text-align: center" class="style195">
                    <asp:Label ID="lInterestRate" runat="server"></asp:Label>
                </td>
                <td class="style196">
                    EXTRA DBR
                </td>
                <td style="text-align: center" class="style197">
                    <asp:Label ID="lExtraDBR" runat="server"></asp:Label>
                </td>
                <td class="style198" style="display: none;">
                    SEC. PDC BANK
                </td>
                <td style="text-align: center; display:none;" class="style199">
                    <asp:Label ID="lSecPDCBank" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style206">
                    ARTA FROM
                </td>
                <td style="text-align: center" class="style220">
                    <asp:Label ID="lARTAFrom" runat="server"></asp:Label>
                </td>
                <td class="style208">
                    LOAN EXPIRY
                </td>
                <td style="text-align: center" class="style209">
                    <asp:Label ID="lLoanExpiry" runat="server"></asp:Label>
                </td>
                <td class="style210">
                    AC NUMBER
                </td>
                <td style="text-align: center" class="style223">
                    <asp:Label ID="lACNumber" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <span class="style121">EXPOSURES</span><table style="border-style: solid; border-width: 1px;
            width: 100%;">
            <tr>
                <td>
                    <table class="reference" style="width: 100%;">
                        <tr>
                            <td class="style119" colspan="9">
                                ON US
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px" class="style118">
                                TYPE
                            </td>
                            <td style="font-size: 10px" class="style117">
                                FLAG
                            </td>
                            <td style="font-size: 10px" class="style116">
                                ORIGINAL AMOUNT
                            </td>
                            <td style="font-size: 10px" class="style115">
                                OUTSTANDING
                            </td>
                            <td style="font-size: 10px" class="style145">
                                EMI
                            </td>
                            <td style="font-size: 10px" class="style113">
                                SEC. TYPE
                            </td>
                            <td style="font-size: 10px" class="style112">
                                SECURITY FV
                            </td>
                            <td style="font-size: 10px" class="style111">
                                SECURITY PV
                            </td>
                            <td style="font-size: 10px" class="style240">
                                LTV
                            </td>
                        </tr>
                        <tr>
                            <td class="style120" colspan="9">
                                TERM LOANS
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lTermLoanType1" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lTermLoanFlag1" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lTermLoanOriginalAmount1" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lTermLoanOutstanding1" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lTermLoanEMI1" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lTermLoanSecurityType1" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lTermLoanSecurityFV1" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lTermLoanSecurityPV1" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lTermLoanLTV1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lTermLoanType2" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lTermLoanFlag2" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lTermLoanOriginalAmount2" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lTermLoanOutstanding2" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lTermLoanEMI2" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lTermLoanSecurityType2" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lTermLoanSecurityFV2" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lTermLoanSecurityPV2" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lTermLoanLTV2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lTermLoanType3" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lTermLoanFlag3" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lTermLoanOriginalAmount3" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lTermLoanOutstanding3" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lTermLoanEMI3" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lTermLoanSecurityType3" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lTermLoanSecurityFV3" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lTermLoanSecurityPV3" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lTermLoanLTV3" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lTermLoanType4" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lTermLoanFlag4" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lTermLoanOriginalAmount4" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lTermLoanOutstanding4" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lTermLoanEMI4" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lTermLoanSecurityType4" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lTermLoanSecurityFV4" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lTermLoanSecurityPV4" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lTermLoanLTV4" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lCreditCardType1" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lCreditCardFlag1" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lCreditCardOriginalAmount1" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lCreditCardOutstanding1" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lCreditCardEMI1" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lCreditCardSecurityType1" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lCreditCardSecurityFV1" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lCreditCardSecurityPV1" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lCreditCardLTV1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lCreditCardType2" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lCreditCardFlag2" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lCreditCardOriginalAmount2" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lCreditCardOutstanding2" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lCreditCardEMI2" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lCreditCardSecurityType2" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lCreditCardSecurityFV2" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lCreditCardSecurityPV2" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lCreditCardLTV2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style120" colspan="9">
                                THIS LOAN
                            </td>
                        </tr>
                        <tr>
                            <td class="style105">
                                Auto Loan
                            </td>
                            <td class="style95">
                                <asp:Label ID="lThisLoanFlag" runat="server"></asp:Label>
                            </td>
                            <td class="style144">
                                <asp:Label ID="lThisLoanOgiginalAmount" runat="server"></asp:Label>
                            </td>
                            <td class="style106">
                                <asp:Label ID="lThisLoanOutstanding" runat="server"></asp:Label>
                            </td>
                            <td class="style146">
                                <asp:Label ID="lThisLoanEMI" runat="server"></asp:Label>
                            </td>
                            <td class="style147">
                                <asp:Label ID="lThisLoanSecType" runat="server"></asp:Label>
                            </td>
                            <td class="style108">
                                <asp:Label ID="lThisLoanSecurityFV" runat="server"></asp:Label>
                            </td>
                            <td class="style109">
                                <asp:Label ID="lThisLoanSecurityPV" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lThisLoanLTV" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style120" colspan="9">
                                OVERDRAFT
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lOverDraftType1" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lOverDraftFlag1" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lOverDraftOriginalAmount1" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lOverDraftOutstanding1" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lOverDraftEMI1" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lOverDraftSecurityType1" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lOverDraftSecurityFV1" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lOverDraftSecurityPV1" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lOverDraftLTV1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lOverDraftType2" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lOverDraftFlag2" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lOverDraftOriginalAmount2" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lOverDraftOutstanding2" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lOverDraftEMI2" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lOverDraftSecurityType2" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lOverDraftSecurityFV2" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lOverDraftSecurityPV2" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lOverDraftLTV2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style224">
                                <asp:Label ID="lOverDraftType3" runat="server"></asp:Label>
                            </td>
                            <td class="style233">
                                <asp:Label ID="lOverDraftFlag3" runat="server"></asp:Label>
                            </td>
                            <td class="style232">
                                <asp:Label ID="lOverDraftOriginalAmount3" runat="server"></asp:Label>
                            </td>
                            <td class="style231">
                                <asp:Label ID="lOverDraftOutstanding3" runat="server"></asp:Label>
                            </td>
                            <td class="style230">
                                <asp:Label ID="lOverDraftEMI3" runat="server"></asp:Label>
                            </td>
                            <td class="style229">
                                <asp:Label ID="lOverDraftSecurityType3" runat="server"></asp:Label>
                            </td>
                            <td class="style228">
                                <asp:Label ID="lOverDraftSecurityFV3" runat="server"></asp:Label>
                            </td>
                            <td class="style227">
                                <asp:Label ID="lOverDraftSecurityPV3" runat="server"></asp:Label>
                            </td>
                            <td class="style224">
                                <asp:Label ID="lOverDraftLTV3" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table style="width: 100%;">
                        <tr>
                            <td class="style129" style="vertical-align: top;">
                                <table class="reference" style="width: 100%">
                                    <tr>
                                        <td style="text-align: center; font-weight: bold;" colspan="3" class="style235">
                                            OFF US
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style125">
                                            BANK/NBFI
                                        </td>
                                        <td class="style131">
                                            ORIGINAL LIMIT
                                        </td>
                                        <td class="style226">
                                            EMI/INTEREST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style123" colspan="3">
                                            TERM LOANS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBTermLoanBank1" runat="server"></asp:Label>
                                        </td>
                                        <td class="style236">
                                            <asp:Label ID="lOffSCBTermLoanOriginalLimit1" runat="server"></asp:Label>
                                        </td>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBTermLoanEMI1" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBTermLoanBank2" runat="server"></asp:Label>
                                        </td>
                                        <td class="style236">
                                            <asp:Label ID="lOffSCBTermLoanOriginalLimit2" runat="server"></asp:Label>
                                        </td>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBTermLoanEMI2" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBTermLoanBank3" runat="server"></asp:Label>
                                        </td>
                                        <td class="style236">
                                            <asp:Label ID="lOffSCBTermLoanOriginalLimit3" runat="server"></asp:Label>
                                        </td>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBTermLoanEMI3" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBTermLoanBank4" runat="server"></asp:Label>
                                        </td>
                                        <td class="style236">
                                            <asp:Label ID="lOffSCBTermLoanOriginalLimit4" runat="server"></asp:Label>
                                        </td>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBTermLoanEMI4" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBCreditCardBank1" runat="server"></asp:Label>
                                        </td>
                                        <td class="style236">
                                            <asp:Label ID="lOffSCBCreditCardOriginalLimit1" runat="server"></asp:Label>
                                        </td>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBCreditCardEMI1" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBCreditCardBank2" runat="server"></asp:Label>
                                        </td>
                                        <td class="style236">
                                            <asp:Label ID="lOffSCBCreditCardOriginalLimit2" runat="server"></asp:Label>
                                        </td>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBCreditCardEMI2" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style128" colspan="3" style="font-weight: bold">
                                            OVER DRAFTS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBOverDraftBank1" runat="server"></asp:Label>
                                        </td>
                                        <td class="style236">
                                            <asp:Label ID="lOffSCBOverDraftOriginalLimit1" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style224">
                                            <asp:Label ID="lOffSCBOverDraftBank2" runat="server"></asp:Label>
                                        </td>
                                        <td class="style236">
                                            <asp:Label ID="lOffSCBOverDraftOriginalLimit2" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style155">
                                            <asp:Label ID="lOffSCBOverDraftBank3" runat="server"></asp:Label>
                                        </td>
                                        <td class="style156">
                                            <asp:Label ID="lOffSCBOverDraftOriginalLimit3" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="text-align: left; vertical-align: top;">
                                <table class="reference" style="width: 100%;">
                                    <tr>
                                        <td colspan="2" style="text-align: center; font-weight: bold;" class="style132">
                                            AGGREGATED CALCULCATIONS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                            TOTAL SCB EXPOSURES
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lTotalSCBExposures" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style150">
                                            TOTAL LTV
                                        </td>
                                        <td class="style151">
                                            <asp:Label ID="lTotalLTV" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                            AGGREGATE SECURITY LOAN RATIO
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lTotalAggregateSecutityLoanRatio" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                            SURPLUS
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lSurplus" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                            TOTAL AUTO LOAN
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lTotalAutoLoan" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                            TOTAL MONTHY COMMITMENTS
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lTotalMonthCommitments" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                            TOTAL SCB SECURED LOANS
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lTotalSCBSecuredLoans" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                            TOTAL SCB UNSECURED LOANS
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lTotalSCBUnsecuredLoans" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                        </td>
                                        <td class="style226">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                            COLLERTERIZATION
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lCollerterization" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="Label163" runat="server">With</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px" class="style133">
                                        </td>
                                        <td class="style226">
                                            <asp:Label ID="lWithCollerterization" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 100%; font-size: 10px;">
            <tr>
                <td class="style153">
                    APPPRAISER
                </td>
                <td class="style217">
                    &nbsp;
                </td>
                <td>
                    SUPPORTED BY
                </td>
            </tr>
            <tr>
                <td class="style153">
                    <asp:Label ID="lAppRaisedBy1" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
                <td class="style217">
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="lAppSupportedBy1" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
            </tr>
        </table>
        <div style="background-image: url('../../Image/headBorder.JPG'); background-repeat: no-repeat;
            width: auto; height: 20px;">
        </div>
        <div style="background-image: url('../../Image/headBorder.JPG'); background-repeat: no-repeat;
            width: auto; height: 20px;">
            <b>AUTO LOAN CHECKLIST (CONT.)</b></div>
        <span class="style121">SRENGTH & WEAKNESS</span>
        <table style="border: 2px solid #000001; width: 100%;">
            <tr>
                <td style="border-style: solid; border-width: 1px; width: 50%; vertical-align: top;">
                    <table class="reference" style="width: 100%;">
                        <tr>
                            <td>
                                STRENGTH
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lStrength1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lStrength2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style237">
                                <asp:Label ID="lStrength3" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lStrength4" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lStrength5" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lStrength6" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lStrength7" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="border-style: solid; border-width: 1px; width: 50%; vertical-align: top;">
                    <table style="width: 100%;" class="reference">
                        <tr>
                            <td>
                                WEAKNESS
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lWeakness1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lWeakness2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lWeakness3" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lWeakness4" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lWeakness5" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lWeakness6" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lWeakness7" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <span class="style121">DEVIATIONS</span><br />
        <table class="reference" style="border: 2px solid #000001; width: 100%;">
            <tr>
                <td class="style161">
                    DEVIATION LEVEL
                </td>
                <td class="style162">
                    DESCRIPTION
                </td>
                <td class="style238">
                    CODES
                </td>
            </tr>
            <tr>
                <td class="style239">
                    <asp:Label ID="lDeviationLevel1" runat="server"></asp:Label>
                </td>
                <td class="style163">
                    <asp:Label ID="lDeviationDescription1" runat="server"></asp:Label>
                </td>
                <td class="style224">
                    <asp:Label ID="lDeviationCode1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style239">
                    <asp:Label ID="lDeviationLevel2" runat="server"></asp:Label>
                </td>
                <td class="style163">
                    <asp:Label ID="lDeviationDescription2" runat="server"></asp:Label>
                </td>
                <td class="style224">
                    <asp:Label ID="lDeviationCode2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style239">
                    <asp:Label ID="lDeviationLevel3" runat="server"></asp:Label>
                </td>
                <td class="style163">
                    <asp:Label ID="lDeviationDescription3" runat="server"></asp:Label>
                </td>
                <td class="style224">
                    <asp:Label ID="lDeviationCode3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style239">
                    <asp:Label ID="lDeviationLevel4" runat="server"></asp:Label>
                </td>
                <td class="style163">
                    <asp:Label ID="lDeviationDescription4" runat="server"></asp:Label>
                </td>
                <td class="style224">
                    <asp:Label ID="lDeviationCode4" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style160">
                    &nbsp;
                </td>
                <td class="style164">
                    UNDERWRITING LEVEL
                </td>
                <td>
                    <asp:Label ID="lUnderWritingLevel" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <span class="style121">REMARKS &amp; CONDITIONS</span><br />
        <table class="reference" style="border: 2px solid #000001; width: 100%;">
            <tr>
                <td style="font-size: 14px">
                    <b>Remarks</b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lRemark1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lRemark2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lRemark3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lRemark4" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lRemark5" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <table class="reference" style="border: 2px solid #000001; width: 100%;">
            <tr>
                <td class="style165" style="font-weight: bold">
                    CONDITIONS
                </td>
                <b>
                    <td class="style167">
                        CON. MET
                    </td>
                    <td class="style166">
                        VERIFIER SIGN.
                    </td>
                    <td>
                        DATE
                    </td>
            </tr>
            <tr>
                <td class="style168">
                    <asp:Label ID="lCondition1" runat="server" Font-Bold="False"></asp:Label>
                </td>
                <td class="style169">
                    <asp:CheckBox ID="CheckBox1" runat="server" Text="." />
                </td>
                <td class="style170">
                    <asp:Label ID="Label265" runat="server"></asp:Label>
                </td>
                <td class="style171">
                    <asp:Label ID="Label266" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style165">
                    <b>
                        <asp:Label ID="lCondition2" runat="server" Font-Bold="False"></asp:Label>
                    </b>
                </td>
                <td class="style167">
                    <b>
                        <asp:CheckBox ID="CheckBox2" runat="server" Text="." />
                    </b>
                </td>
                <td class="style166">
                    <asp:Label ID="Label268" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label267" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style165">
                    <b>
                        <asp:Label ID="lCondition3" runat="server" Font-Bold="False"></asp:Label>
                    </b>
                </td>
                <td class="style167">
                    <b>
                        <asp:CheckBox ID="CheckBox3" runat="server" Text="." />
                    </b>
                </td>
                <td class="style166">
                    <asp:Label ID="Label269" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label270" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style165">
                    <b>
                        <asp:Label ID="lCondition4" runat="server" Font-Bold="False"></asp:Label>
                    </b>
                </td>
                <td class="style167">
                    <b>
                        <asp:CheckBox ID="CheckBox4" runat="server" Text="." />
                    </b>
                </td>
                <td class="style166">
                    <asp:Label ID="Label272" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label271" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style165">
                    <b>
                        <asp:Label ID="lCondition5" runat="server" Font-Bold="False"></asp:Label>
                    </b>
                </td>
                <td class="style167">
                    <b>
                        <asp:CheckBox ID="CheckBox5" runat="server" Text="." />
                    </b>
                </td>
                <td class="style166">
                    <asp:Label ID="Label273" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label274" runat="server"></asp:Label>
                </td>
            </tr>
            </b>
            <tr>
                <td class="style165">
                    <b>
                        <asp:Label ID="lCondition6" runat="server" Font-Bold="False"></asp:Label>
                    </b>
                </td>
                <td class="style167">
                    <b>
                        <asp:CheckBox ID="CheckBox6" runat="server" Text="." />
                    </b>
                </td>
                <td class="style166">
                    <asp:Label ID="Label2" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style165">
                    <b>
                        <asp:Label ID="lCondition7" runat="server" Font-Bold="False"></asp:Label>
                    </b>
                </td>
                <td class="style167">
                    <b>
                        <asp:CheckBox ID="CheckBox7" runat="server" Text="." />
                    </b>
                </td>
                <td class="style166">
                    <asp:Label ID="Label4" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style172" colspan="4">
                    * CONDITIONS TO BE MET PRIOR ISSUING PURCHASE ORDER
                    <br />
                    * ALL APPROVALS ARE IN PRINCIPAL AND SUBJECT TO MEETING THE ASSET OPERATION CRITERIA
                    <br />
                    * PLEASE MARK &quot;√&quot; IN &quot;CON.MET&quot; FIELD IF CONDITIONS ARE MET
                </td>
            </tr>
        </table>
        <span class="style121">REWORK &amp; VALIDITY</span>
        <table class="reference" style="border: 2px solid #000001; width: 100%;">
            <tr>
                <td class="style173">
                    <table class="reference" style="width: 100%;">
                        <tr>
                            <td class="style175">
                                &nbsp;
                            </td>
                            <td>
                                REWORK
                            </td>
                        </tr>
                        <tr>
                            <td class="style175">
                                REWORK DONE
                            </td>
                            <td>
                                <asp:Label ID="lReworkDone" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style175">
                                REWORK COUNT
                            </td>
                            <td>
                                <asp:Label ID="lReworkCount" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style175">
                                REWORK REASON
                            </td>
                            <td>
                                <asp:Label ID="lReworkReason" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="style174">
                    &nbsp;
                </td>
                <td>
                    <table class="reference" style="width: 100%;">
                        <tr>
                            <td class="style176">
                                &nbsp;
                            </td>
                            <td>
                                VAIDITY
                            </td>
                        </tr>
                        <tr>
                            <td class="style176">
                                UNDERWRITING DATE
                            </td>
                            <td class="style110">
                                <asp:Label ID="lUnderWritingDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style176">
                                VALIDITY UPTO
                            </td>
                            <td class="style110">
                                <asp:Label ID="lValidityUpTo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style176" colspan="2">
                                * <span class="style172">VALIDTY IS 45 DAYS FROM APPROVAL</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <span class="style121">PDD PARAMETERS</span><br />
        <table class="reference" style="border: 2px solid #000001; width: 100%;">
            <tr>
                <td class="style181">
                    MAX LOAN
                </td>
                <td class="style183">
                    <asp:Label ID="lMaxLoan" runat="server"></asp:Label>
                </td>
                <td class="style233">
                    <asp:Label ID="lMaxLoanFlag" runat="server"></asp:Label>
                </td>
                <td class="style216">
                    EXPERIENCE
                </td>
                <td class="style186">
                    <asp:Label ID="lExperience" runat="server"></asp:Label>
                </td>
                <td class="style185">
                    <asp:Label ID="lExperienceFlag" runat="server"></asp:Label>
                </td>
                <td rowspan="7">
                    2 YEARS @ LEVEL 2 CASH SALARIED: 1 YEAR GOVT EMPLOYEE : BDT 20,000
                </td>
            </tr>
            <tr>
                <td class="style181">
                    MIN LOAN
                </td>
                <td class="style183">
                    <asp:Label ID="lMinLoan" runat="server"></asp:Label>
                </td>
                <td class="style233">
                    <asp:Label ID="lMinLoanFlag" runat="server"></asp:Label>
                </td>
                <td class="style216">
                    A/C RELATIONSHIP
                </td>
                <td class="style186">
                    <asp:Label ID="lAccountRelationshipe" runat="server"></asp:Label>
                </td>
                <td class="style185">
                    <asp:Label ID="lAccountRelationshipeFlag" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style181">
                    PURPOSE
                </td>
                <td class="style183">
                    <asp:Label ID="lLoanPurpose" runat="server"></asp:Label>
                </td>
                <td class="style233">
                    <asp:Label ID="lLoanPurposeFlag" runat="server"></asp:Label>
                </td>
                <td class="style216">
                    MIN INCOME
                </td>
                <td class="style186">
                    <asp:Label ID="lMinimumIncome" runat="server"></asp:Label>
                </td>
                <td class="style185">
                    <asp:Label ID="lMinimumIncomeFlag" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style181">
                    SEGMENT
                </td>
                <td class="style183">
                    <asp:Label ID="lSigment" runat="server"></asp:Label>
                </td>
                <td class="style233">
                    <asp:Label ID="lSigmentFlag" runat="server"></asp:Label>
                </td>
                <td class="style216">
                    PHYSICAL VERIFICATION
                </td>
                <td class="style186">
                    <asp:Label ID="lPhisicalVarification" runat="server"></asp:Label>
                </td>
                <td class="style185">
                    <asp:Label ID="lPhisicalVarificationFlag" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style181">
                    MIN AGE
                </td>
                <td class="style183">
                    <asp:Label ID="lMinAge" runat="server"></asp:Label>
                </td>
                <td class="style233">
                    <asp:Label ID="lMinAgeFlag" runat="server"></asp:Label>
                </td>
                <td class="style216">
                    TENOR
                </td>
                <td class="style186">
                    <asp:Label ID="lPadTenor" runat="server"></asp:Label>
                </td>
                <td class="style185">
                    <asp:Label ID="lPadTenorFlag" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style181">
                    MAX AGE
                </td>
                <td class="style183">
                    <asp:Label ID="lMaxAge" runat="server"></asp:Label>
                </td>
                <td class="style233">
                    <asp:Label ID="lMaxAgeFlag" runat="server"></asp:Label>
                </td>
                <td class="style216">
                    NATIONALITY
                </td>
                <td class="style186">
                    <asp:Label ID="lNationality" runat="server"></asp:Label>
                </td>
                <td class="style185">
                    <asp:Label ID="lNationalityFlag" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style181">
                    TELEPHONE
                </td>
                <td class="style183">
                    <asp:Label ID="lTelephone" runat="server"></asp:Label>
                </td>
                <td class="style233">
                    <asp:Label ID="lTelephoneFlag" runat="server"></asp:Label>
                </td>
                <td class="style216">
                    LOCATION
                </td>
                <td class="style186">
                    <asp:Label ID="lLocation" runat="server"></asp:Label>
                </td>
                <td class="style185">
                    <asp:Label ID="lLocationFlag" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <table class="reference" style="width: 100%;">
            <tr>
                <td class="style187">
                    APPRAISED BY
                </td>
                <td class="style188">
                    SUPPORTED BY
                </td>
                <td>
                    APPROVED BY
                </td>
            </tr>
            <tr>
                <td class="style187">
                    <asp:Label ID="lAppRaisedBy" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
                <td class="style188">
                    <asp:Label ID="lAppSupportedBy" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lAppApprovedBy" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="reference" style="border: 2px solid #000001; width: 100%;">
            <tr>
                <td>
                    APPROVER&#39;S COMMENTS ( IF ANY)
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lApproverComments" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <div style="width: auto; height: 20px;">
            <asp:Image ID="Image2" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                Width="624px" />
        </div>
    </div>
    </form>
</body>
</html>
