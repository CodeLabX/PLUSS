﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class Auto_LoanSummary : System.Web.UI.Page
    {


        protected void Page_PreInit(object sender, EventArgs e)
        {
            //string callingFrom = Request.QueryString["CallFrom"];
            if (Session["MasterPageUrlForAutoLoanSummary"] != null)
            {
                this.MasterPageFile = Session["MasterPageUrlForAutoLoanSummary"].ToString();
            }

        }


        [AjaxNamespace("Auto_LoanSummary")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(Auto_LoanSummary));
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanSummary> GetLoanSummary(int pageNo, int pageSize, Int32 searchKey)
        {
            List<AutoLoanSummary> objLoanSummaryList = new List<AutoLoanSummary>();
            pageNo = pageNo*pageSize;
            var userType = Session["AutoUserType"];
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objLoanSummaryList;
            }
            else
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                objLoanSummaryList =
                    autoLoanAssessmentService.getAutoLoanSummaryAll(pageNo, pageSize, searchKey, statePermission);
            }
            return objLoanSummaryList;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> getModelbyManufacturerID(int manufacturerID)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            List<AutoVehicle> objList_AutoModel = tanorAndLTVService.GetModelList(manufacturerID);
            return objList_AutoModel;
        }

        //[AjaxMethod(HttpSessionStateRequirement.Read)]
        //public List<AutoLoanSummary> GetLoanSummaryByLLID(int pageNo, int pageSize, Int32 searchKey)
        //{
        //    AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
        //    var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
        //    List<AutoLoanSummary> objLoanSummaryList =
        //        autoLoanAssessmentService.getAutoLoanSummaryAll(pageNo,pageSize);
        //    return objLoanSummaryList;
        //}

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string AppealedByAutoLoanMasterId(int appiledOnAutoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                if (UserType == 10)
                {
                    objAutoStatusHistory.StatusID = 43; //received by auto ops maker
                }
                else if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 20; //received by auto ops Checker
                }
                else if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 23; //received by auto CI Support
                }
                else if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 26; //received by auto CI Analyst
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string Disbursed(int masterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = masterId;
                objAutoStatusHistory.StatusID = 7;
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string ForwardLoanAppForSales(AutoLoanMaster objAutoLoanMaster,
            AutoPRApplicant objAutoPRApplicant, AutoJTApplicant objAutoJTApplicant,
            AutoPRProfession objAutoPRProfession, AutoJTProfession objAutoJTProfession,
            AutoLoanVehicle objAutoLoanVehicle, AutoPRAccount objAutoPRAccount, AutoJTAccount objAutoJTAccount,
            List<AutoPRAccountDetail> objAutoPRAccountDetailList, List<AutoJTAccountDetail> objAutoJtAccountDetailList,
            AutoPRFinance objAutoPRFinance, AutoJTFinance objAutoJTFinance, AutoLoanReference objAutoLoanReference,
            List<AutoLoanFacility> objAutoLoanFacilityList, List<AutoLoanSecurity> objAutoLoanSecurityList,
            AutoApplicationInsurance objAutoApplicationInsurance, AutoVendor objAutoVendor)
        {
            var res = "";
            try
            {
                objAutoLoanMaster.UserID = Convert.ToInt32(Session["Id"].ToString());
                
                ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
                var loanApplicationService = new LoanApplicationService(repository);
                res = loanApplicationService.SaveAutoLoanApplication(objAutoLoanMaster, objAutoPRApplicant, objAutoJTApplicant, objAutoPRProfession,
                    objAutoJTProfession, objAutoLoanVehicle, objAutoPRAccount, objAutoJTAccount, objAutoPRAccountDetailList, objAutoJtAccountDetailList,
                    objAutoPRFinance, objAutoJTFinance, objAutoLoanReference, objAutoLoanFacilityList, objAutoLoanSecurityList,
                    objAutoApplicationInsurance, objAutoVendor);
                
                if (res == "Success")
                {
                    var userID = Convert.ToInt32(Session["Id"].ToString());
                    var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                    AutoLoanAssessmentRrepository repository1 = new AutoLoanDataService.AutoLoanAssessmentDataService();
                    var autoLoanAssessmentService = new AutoLoanAssessmentService(repository1);

                    AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                    objAutoStatusHistory.AutoLoanMasterId = objAutoLoanMaster.Auto_LoanMasterId;
                    if (UserType == 10)
                    {
                        objAutoStatusHistory.StatusID = 44; //forwareded to ops checker
                    }
                    if (UserType == 12)
                    {
                        objAutoStatusHistory.StatusID = 21; //forwareded to CI
                    }
                    if (UserType == 9)
                    {
                        objAutoStatusHistory.StatusID = 24; //forwareded to CI Analyst
                    }
                    if (UserType == 8)
                    {
                        objAutoStatusHistory.StatusID = 27; //forwareded to CI Approver
                    }
                    objAutoStatusHistory.UserID = userID;


                    res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }
        //if (UserType == 10)
        //{
        //    objAutoStatusHistory.StatusID = 19; //forwareded to ops checker
        //}
        //if (UserType == 12)
        //{
        //    objAutoStatusHistory.StatusID = 21; //forwareded to CI
        //}
        //if (UserType == 9)
        //{
        //    objAutoStatusHistory.StatusID = 24; //forwareded to CI Analyst
        //}
        //if (UserType == 8)
        //{
        //    objAutoStatusHistory.StatusID = 27; //forwareded to CI Approver
        //}


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string HoldLoanApp(int autoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;
                if (UserType == 10)
                {
                    objAutoStatusHistory.StatusID = 39; //Hold to ops checker
                }
                if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 22; //hold by ops checker
                }
                if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 25; //hold by CI Support
                }
                if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 28; //hold by CI Analyst
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string BackwardLoanApp(int autoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;

                if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 35; //BackWard From Ops Cheker and Set to Opes Maker
                }
                if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 34; //Backward From CI Support and Set to Opes Checker
                }
                if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 33; //Backward From Analyst and Set to CI Support
                }
                if (UserType == 10)
                {
                    objAutoStatusHistory.StatusID = 17; //BackWard From Ops Maker and Set to Sales
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public int GetUserType()
        {
            var userType = Convert.ToInt32(Session["AutoUserType"].ToString());
            return userType;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPrice> GetPriceByVehicleId(int vehicleId, int manufacturingYear, int vehicleStatus)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetPriceByVehicleId(vehicleId, manufacturingYear, vehicleStatus);
            return res;
        }











        #region Loan Application

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveLoanApplication(AutoLoanMaster objAutoLoanMaster,
            AutoPRApplicant objAutoPRApplicant, AutoJTApplicant objAutoJTApplicant,
            AutoPRProfession objAutoPRProfession, AutoJTProfession objAutoJTProfession,
            AutoLoanVehicle objAutoLoanVehicle, AutoPRAccount objAutoPRAccount, AutoJTAccount objAutoJTAccount,
            List<AutoPRAccountDetail> objAutoPRAccountDetailList, List<AutoJTAccountDetail> objAutoJtAccountDetailList,
            AutoPRFinance objAutoPRFinance, AutoJTFinance objAutoJTFinance, AutoLoanReference objAutoLoanReference,
            List<AutoLoanFacility> objAutoLoanFacilityList, List<AutoLoanSecurity> objAutoLoanSecurityList,
            AutoApplicationInsurance objAutoApplicationInsurance, AutoVendor objAutoVendor)//,AutoPRBankStatement objPRBankStatement,AutoJTBankStatement objJTBankStatement)
        //public string SaveLoanApplication(AutoPRBankStatement objPRBankStatement)
        {
            //loan application save method
            //var PRspreadsheet = Session["PRspreadsheetData"];
            //objPRBankStatement.Statement = PRspreadsheet.ToString();
            //objPRBankStatement.LastUpdateDate = DateTime.Now;

            //var JTspreadsheet = Session["JTspreadsheetData"];
            //objJTBankStatement.Statement = JTspreadsheet.ToString();
            //objJTBankStatement.LastUpdateDate = DateTime.Now;


            objAutoLoanMaster.UserID = Convert.ToInt32(Session["Id"].ToString());
            //objAutoLoanMaster.StatusID = 1;
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.SaveAutoLoanApplication(objAutoLoanMaster, objAutoPRApplicant, objAutoJTApplicant, objAutoPRProfession,
                objAutoJTProfession, objAutoLoanVehicle, objAutoPRAccount, objAutoJTAccount, objAutoPRAccountDetailList, objAutoJtAccountDetailList,
                objAutoPRFinance, objAutoJTFinance, objAutoLoanReference, objAutoLoanFacilityList, objAutoLoanSecurityList,
                objAutoApplicationInsurance, objAutoVendor);//, objPRBankStatement, objJTBankStatement);

            return res;
            //return "Success";
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public Object getLoanLocetorInfoByLLID(int llid)
        {
            int userType = Convert.ToInt32(Session["AutoUserType"].ToString());
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getLoanLocetorInfoByLLIDForOps(llid, userType);


            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBank> GetBank(int status)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBank(status);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranch> GetBranch(int bankID)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBranch(bankID);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVendor> GetVendor()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetVendor();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoFacility> getFacility()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetFacility();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoProfession> GetProfession()
        {
            AutoSegmentSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoSegmentSettingsService = new AutoSegmentSettingsService(repository);


            List<AutoProfession> objList_AutoProfession =
                autoSegmentSettingsService.GetProfession();
            return objList_AutoProfession;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetAutoModelByManufacturerID(int manufacturerID)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAutoModelByManufacturerID(manufacturerID);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetAutoTrimLevelByModel(int manufacturerID, string model)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAutoTrimLevelByModel(manufacturerID, model);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetAutoEngineCCByModel_TrimLevel(int manufacturerID, string model, string trimLevel)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAutoEngineCCByModel_TrimLevel(manufacturerID, model, trimLevel);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> GetAutoVehicleType(int manufacturerID, string model, string engineCC, string TrimLevel)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAutoVehicleType(manufacturerID, model, engineCC, TrimLevel);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoCompany> getNameOfCompany()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getNameOfCompany();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> getVehicleStatus(int statusID)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVehicleStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPrice> getManufacturingYear(int manufacturerID, string model, string engineCC, string TrimLevel)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getManufacturingYear(manufacturerID, model, engineCC, TrimLevel);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPrice> GetPrice(int manufacturerID, string model, string engineCC, string TrimLevel, string manufacturingYear)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetPrice(manufacturerID, model, engineCC, TrimLevel, manufacturingYear);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<Auto_InsuranceBanca> getGeneralInsurance()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getGeneralInsurance();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanApplicationSource> GetAllSource()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetAllSource();
            return res;
        }

        #endregion
    }
}
