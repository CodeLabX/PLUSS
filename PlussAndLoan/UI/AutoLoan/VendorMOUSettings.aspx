﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.VendorMOUSettings" Title="Vendor MOU Settings" Codebehind="VendorMOUSettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <script src="../../Scripts/AutoLoan/AutoVendorMOUSettings.js" type="text/javascript"></script>
<script>
    Event.observe(window,'load',function(){
    Calendar.setup({ inputField: 'txtValidFrom', ifFormat: '%d/%m/%Y',
    weekNumbers: false, button: 'newsDate'
});  
        });
        </script>
        
        
            <style type="text/css">
        #nav_VendorMOUSettings a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    <div class ="pageHeaderDiv ">Vendor MOU Settings</div>
    
     <%--All Aspx code will goes on Content div--%>
    <div id="Content">
    <%--For Pager --%>
    <div class="SearchwithPager">
<%--    <div class="search">
    <input type="text" id="txtSearch" class="txt txtSearch" onkeypress="InsuranceAndBANCASettingsHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search" />
    </div> 
--%>    

    <div class="search searchwithUpload">
    <input type="text" id="txtSearch" class="txt txtSearch widthSize25_per" onkeypress="VendorMOUSettingsHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search" onclick="VendorMOUSettingsManager.render();"/>
<%--    <input type="button" id="btnUploadDoc" class="searchButton" value="Upload"/>
--%>    <%--<form id="frmUploader" class="uploaderForm" method="post" enctype="multipart/form-data" action="IRSettingsForPayroll.aspx">
    <input id="fileIRSetiingsForPayroll" class="txt txtUpload" type="file" title="IRSetiings For Payroll xls document" name="fileIRSetiingsForPayroll">
    <input type="button" id="btnUpload" class="searchButton" value="Upload"/>
    </form>--%>
    
		
		<asp:FileUpload ID="fileDocumentUpload" class="txt txtUpload" runat="server" />
        <asp:Button ID="btnUploadSave" runat="server" Text="Upload" 
             onclick="btnUploadSave_Click" UseSubmitBehavior="False" />

    </div> 


<div id="Pager" class="pager"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
    </div>
    <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					<col width="50px" />
					<col width="200px" />
					<col width="100px" />
					<col width="130px" />
					<col width="120px" />
					<col width="120px" />
					<col width="150px" />
				</colgroup>
				<thead>
					<tr>
						<th>
							SL
						</th>
						<th>
						    Vendor Name
						</th>
						<th>
						    Phone
						</th>
						<th>
						    Dealer Type
						</th>
						<th>
						    IR With ARTA
						</th>
						<th>
						    IR Without ARTA
						</th>
						<th> <a id="lnkAddNewIRSettingsForVehicle" class="iconlink iconlinkAdd" href="javascript:;">New Vendor MOU Settings </a>
						</th>
					</tr>
				</thead>
				<tbody id="IRSettingsList">
				</tbody>
			</table>
    
    </div>
    
    <div class="modal" id="VendorMOUSettingsPopupDiv" style="display:none; height:auto;">
		<h3>Vebdor MOU Settings</h3>
		<label for="txtVendorName">Vendor Name</label> <input class="txt" id="txtVendorName" title="Vendor Name"/><br/>
		<label for="txtVendorCode">Vendor Code</label> <input class="txt" id="txtVendorCode" title="Vendor Code"/><br/>
		<label for="cmbVehicleStatus">Dealer Status</label>
		<select id="cmbVehicleStatus" class="txt" title="Insurance Type" >
		    <%--<option value="1" selected="selected">General</option>
		    <option value="2">Life</option>
		    <option value="3">General & Life</option>--%>
		</select>
		 <br/>
		 <label for="cmbIsMou">Is Mou</label>
		<select id="cmbIsMou" class="txt" title="Is Mou" >
		    <option value="1" selected="selected">Yes</option>
		    <option value="0">No</option>
		</select>
		 <br/>
		<label for="txtAddress">Address</label> <input class="txt" id="txtAddress" title="Address"/><br/>
		<label for="txtPhone">Phone</label> <input class="txt" id="txtPhone" title="Phone"/><br/>
		<label for="txtWebsite">Website</label> <input class="txt" id="txtWebsite" title="Website"/><br/>
		<label for="txtIRWithARTA">IR With ARTA</label> <input class="txt" id="txtIRWithARTA" title="IR With ARTA"/><br/>
		<label for="txtIRWithoutARTA">IR Without ARTA</label> <input class="txt" id="txtIRWithoutARTA" title="IR Without ARTA"/><br/>
		<label for="txtProcessingFee">Processing Fee</label> <input class="txt" id="txtProcessingFee" title="Processing Fee"/><br/>
		<label for="txtValidFrom">Valid From</label> <input class="txt" id="txtValidFrom" title="Valid From"/><br/>
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
   
</asp:Content>

