﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class InsuranceCalculationSettings : System.Web.UI.Page
    {
        [AjaxNamespace("InsuranceCalculationSettings")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(InsuranceCalculationSettings));
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAutoInsurenceCalculationsettings(List<AutoOwnDamage> objOwnDamageList, AutoOthercharge objOtherCharge)
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);
            var res = autoLoanDataService.SaveAutoInsurenceCalculationsettings(objOwnDamageList, objOtherCharge);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoOwnDamage> GetOwnDamage()
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);


            List<AutoOwnDamage> objAutoOwnDamageList =autoLoanDataService.GetOwnDamage();
            return objAutoOwnDamageList;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoOthercharge GetOtherDamage()
        {
            Auto_InsuranceBancaRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new InsurenceService(repository);


            var objAutoOthercharge =autoLoanDataService.GetOtherDamage();
            return objAutoOthercharge;
        }
    }
}
