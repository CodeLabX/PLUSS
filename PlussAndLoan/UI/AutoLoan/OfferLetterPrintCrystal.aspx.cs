﻿using System;
using System.Collections.Generic;
using AutoLoanService.Entity.CheckListPrint;
using CrystalDecisions.CrystalReports.Engine;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class UI_AutoLoan_OfferLetterPrintCrystal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                OfferLatterPrint b2 = new OfferLatterPrint();
                List<OfferLatterPrint> b2List = new List<OfferLatterPrint>();
                b2 = (OfferLatterPrint)Session["OfferLatterObject"];
                b2List.Add(b2);

                ReportDocument rd = new ReportDocument();
                string path = "";
                if (b2List[0].lProductId == "3") {
                    path = Server.MapPath("PrintCheckList/OfferLatterSaadiq.rpt");
                }
                else
                {
                    path = Server.MapPath("PrintCheckList/OfferLatter.rpt");
                }
                rd.Load(path);
                rd.SetDataSource(b2List);
                crViewer.ReportSource = rd;
                //rd.PrintToPrinter(1, true, 0, 0);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
