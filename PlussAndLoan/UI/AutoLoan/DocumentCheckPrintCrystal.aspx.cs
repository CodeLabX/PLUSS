﻿using System;
using System.Collections.Generic;
using AutoLoanService.Entity.CheckListPrint;
using CrystalDecisions.CrystalReports.Engine;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class UI_AutoLoan_DocumentCheckPrintCrystal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DocumentCheckListEntity dc = new DocumentCheckListEntity();
            List<DocumentCheckListEntity> dcList = new List<DocumentCheckListEntity>();
            dc = (DocumentCheckListEntity) Session["DocumentCheckListPrintObject"];
            dcList.Add(dc);

            ReportDocument rd = new ReportDocument();
            string path = Server.MapPath("PrintCheckList/DocumentCheckPrintReport.rpt");
            rd.Load(path);
            rd.SetDataSource(dcList);
            crViewer.ReportSource = rd;
            //rd.PrintToPrinter(1, true, 0, 0);

        }
    }
}
