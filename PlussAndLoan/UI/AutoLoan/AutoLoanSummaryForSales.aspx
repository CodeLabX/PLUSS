<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoSales.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.AutoLoanSummaryForSales" Title="Auto Loan Summary For Sales" Codebehind="AutoLoanSummaryForSales.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 60px;
        }
        .style3
        {
        }
        .style4
        {
            width: 241px;
        }
        .style5
        {
            width: 179px;
        }
        .style7
        {
            font-weight: bold;
            font-size: large;
            height: 13px;
        }
        .style8
        {
            width: 100%;
        }
        .style11
        {
            text-align: center;
        }
        table.reference {
    background-color: #FFFFFF;
    border: 1px solid #C3C3C3;
    border-collapse: collapse;
    width: 100%;
}
table.reference th {
    background-color: #E5EECC;
    border: 1px solid #C3C3C3;
    padding-left: 5px;
    vertical-align: top;
}
table.reference td {
    border: 1px solid #C3C3C3;
    padding: 0px;
    vertical-align: top;
    font-size:12px;
} 
        
        .style21
        {
            width: 12px;
        }
                
        .style32
        {
            font-weight: bold;
        }
                
        .style35
        {
        }
        .style37
        {
            width: 4px;
        }
        .style38
        {
            width: 50px;
        }
        .style40
        {
            text-align: center;
            font-weight: bold;
        }
        
        .style41
        {
            width: 511px;
        }
        .style42
        {
            width: 119px;
        }
        .style43
        {
            width: 119px;
            font-weight: bold;
        }
        .style44
        {
            width: 50px;
            font-weight: bold;
        }
        .style45
        {
            text-align: center;
            font-weight: bold;
            font-size: medium;
            text-decoration: underline;
            font-family: Arial;
        }
        
        p.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	}
                
        .style68
        {
            width: 100%;
            border-color: #808080;
        }
        .style69
        {
            font-size: x-small;
            font-weight: normal;
        }
        
        .style71
        {
            height: 33px;
            font-weight: bold;
        }
        
        .style73
        {
            height: 7px;
        }
        .style74
        {
            width: 15px;
        }
        
        .style75
        {
            width: 12px;
            height: 25px;
        }
        .style76
        {
            font-weight: bold;
            font-size: 13px;
            height: 25px;
            }
        .style77
        {
            height: 25px;
        }
        .style78
        {
        }
        .style79
        {
            width: 81px;
        }
        .style80
        {
            width: 116px;
        }
        
        .style82
        {
            width: 274px;
        }
        .style83
        {
            width: 120px;
        }
        
        .style84
        {
        }
        
        .style85
        {
            width: 12px;
            height: 6px;
        }
        .style86
        {
            font-weight: bold;
            font-size: 13px;
            height: 6px;
        }
        .style87
        {
            height: 7px;
            text-align: justify;
            }
        .style88
        {
            width: 114px;
        }
        
        #nav_AutoLoanSummaryForSales a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    
    <%--<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>--%>
    
    <link href="../../CSS/Default.css" rel="stylesheet" type="text/css" />

 <link href="../../CSS/admin.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/TableLayoutForTabControls.css" rel="stylesheet" type="text/css" />
    
    <script src="../../Scripts/AutoLoan/AutoLoanSummaryForSales.js" type="text/javascript"></script>
    <script>
        Event.observe(window, 'load', function() {
            Calendar.setup({ inputField: 'txtAppliedDate', ifFormat: '%d/%m/%Y',
                weekNumbers: false, button: 'newsDate'
            });
        });
        Event.observe(window, 'load', function() {
            Calendar.setup({
                inputField: 'txtReceiveDate',
                ifFormat: '%d/%m/%Y',
                weekNumbers: false,
                button: 'newsDate'
            });
        });



        Event.observe(window, 'load', function() {
            Calendar.setup({ inputField: 'txtPApplicantDateOfBirth', ifFormat: '%d/%m/%Y',
                weekNumbers: false, button: 'newsDate'
            });

        });

        Event.observe(window, 'load', function() {
            Calendar.setup({ inputField: 'txtJApplicantDateOfBirth', ifFormat: '%d/%m/%Y',
                weekNumbers: false, button: 'newsDate'
            });
        });

//        Event.observe(window, 'load', function() {
//            Calendar.setup({ inputField: 'txtSecurityScheduleIssueDate1', ifFormat: '%d/%m/%Y',
//                weekNumbers: false, button: 'newsDate'
//            });
//        });

//        Event.observe(window, 'load', function() {
//            Calendar.setup({ inputField: 'txtSecurityScheduleIssueDate2', ifFormat: '%d/%m/%Y',
//                weekNumbers: false, button: 'newsDate'
//            });
//        });

//        Event.observe(window, 'load', function() {
//            Calendar.setup({ inputField: 'txtSecurityScheduleIssueDate3', ifFormat: '%d/%m/%Y',
//                weekNumbers: false, button: 'newsDate'
//            });
//        });

//        Event.observe(window, 'load', function() {
//            Calendar.setup({ inputField: 'txtSecurityScheduleIssueDate4', ifFormat: '%d/%m/%Y',
//                weekNumbers: false, button: 'newsDate'
//            });
//        });

//        Event.observe(window, 'load', function() {
//            Calendar.setup({ inputField: 'txtSecurityScheduleIssueDate5', ifFormat: '%d/%m/%Y',
//                weekNumbers: false, button: 'newsDate'
//            });
//        });
</script>

<style type="text/css">
       #nav_AutoLoanSummaryForSales a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    
    <div id="divLoanSummaryForSales">
    <div class="pageHeaderDiv ">Auto Loan Summary For Sales</div>

             <div id="divLoanAppMasterData" class="fieldDiv divPadding formBackColor">
                <div class="fieldDiv">
                    <label class="lbl rightAlign">LLID<span class="starColor">*</span>:</label>
                        <input type="text" id="txtLLIDSearch" class="txtField widthSize25_per" onkeypress="loanSummeryForSalesHelper.searchKeypress(event)"/>
                        <input type="button" id="btnLLIdSearch" class="" value="Search"/>
                </div>
                
            </div>
         
             <div class="divLeftNormalClass widthSize97p9_per">
             
           <table  cellpadding="0" cellspacing="0" frame="box" border="0" class="summary centerAlign widthSize100_per">
				        <colgroup class="sortable ">
					        <col width="80px" />
					        <col width="150px" />
					        <col width="120px" />
					        <col width="90px" />
					        <col width="100px" />
					        <col width="110px" />
					        <col width="90px" />
					        <col width="110px" />
					        <col width="110px" />
        					
				        </colgroup>
				        <thead >
					        <tr class="theadTdTextAlignCenter">
						        <th>
							        LLID
						        </th>
						        <th>
						            CUSTOMER
						        </th>
						        <th>
						            PRODUCT
						        </th>
        						<th>
						            BRAND
						        </th>
						        <th>
						            MODEL
						        </th>
						        <th>
						            LOAN AMT
						        </th>
						        <th>
						            TENOR
						        </th>
						        <th>
						            STATUS
						        </th>				
						        <th>
						            ACTION 
						        </th>
					        </tr>
				        </thead>
				        <tbody id="GridLoanSummery">
				        </tbody>
		</table>
			        <div class="clear"></div>
			<div id="Pager" class="pager pagerFooter" > 
                            <label for="txtPageNO" class="pagerlabel">page</label>                                           
                            <input id="txtPageNo" class="txtPageNo" value="1" maxlength="7"></input>
                            <span id="spntotalPage" class="spntotalPage">of 1</span> 
                            <a id="lnkPrev" title="Previous Page" href="#" class="prevPage">prev</a> 
                            <a id="lnkNext" title="Next Page" href="#" class="nextPage">next</a>
                    </div>
            <div class="clear">
                 </div>
             </div>




<br/>
 </div>
 <div id="loanDetailsForSales" style="display:none;">
    <div class="pageHeaderDiv fieldDiv">
        Loan Application</div>
    <div class="fieldDiv divPaddingLeftRight">
        <label>
            LLID:</label>
        <input type="text" id="txtLLID" class="txtField" />
        <input type="hidden" id="txtAutoLoanMasterId" value="0"/>
                <input type="hidden" id="txtPApplicantID" value="0"/>
                <input type="hidden" id="txtJApplicantID" value="0"/>
                <input type="hidden" id="txtPrProfessionId" value="0"/>
                <input type="hidden" id="txtJtProfessionId" value="0"/>
                <input type="hidden" id="txtAutoLoanVehicleId" value="0"/>
                <input type="hidden" id="txtPrAccountId" value="0"/>
                <input type="hidden" id="txtJtAccountId" value="0"/>
                <input type="hidden" id="txtPrAccountDetailsId1" value="0"/>
                <input type="hidden" id="txtPrAccountDetailsId2" value="0"/>
                <input type="hidden" id="txtPrAccountDetailsId3" value="0"/>
                <input type="hidden" id="txtPrAccountDetailsId4" value="0"/>
                <input type="hidden" id="txtPrAccountDetailsId5" value="0"/>
                <input type="hidden" id="txtJtAccountDetailsId1" value="0"/>
                <input type="hidden" id="txtJtAccountDetailsId2" value="0"/>
                <input type="hidden" id="txtJtAccountDetailsId3" value="0"/>
                <input type="hidden" id="txtJtAccountDetailsId4" value="0"/>
                <input type="hidden" id="txtJtAccountDetailsId5" value="0"/>
                <input type="hidden" id="txtPrFinanceId" value="0"/>
                <input type="hidden" id="txtJtFinanceId" value="0"/>
                <input type="hidden" id="txtReferenceId" value="0"/>
                <input type="hidden" id="txtFacilityId1" value="0"/>
                <input type="hidden" id="txtFacilityId2" value="0"/>
                <input type="hidden" id="txtFacilityId3" value="0"/>
                <input type="hidden" id="txtFacilityId4" value="0"/>
                <input type="hidden" id="txtFacilityId5" value="0"/>
                <input type="hidden" id="txtSecurityId1" value="0"/>
                <input type="hidden" id="txtSecurityId2" value="0"/>
                <input type="hidden" id="txtSecurityId3" value="0"/>
                <input type="hidden" id="txtSecurityId4" value="0"/>
                <input type="hidden" id="txtSecurityId5" value="0"/>
        <label class="lblBlank">
        </label>
        <label>
            Product Name:</label>
        <select id="cmbProductName" class="comboWidth">
            <option value="1">Staff Auto</option>
            <option value="2">Conventional Auto</option>
            <option value="3">Saadiq Auto</option>
        </select>
    </div>
    <div id="div1" class="fieldDiv divPadding formBackColor">
        <div class="fieldDiv">
            <label class="lbl lblLoan">
                Source<span class="starColor">*</span>:</label>
            <select id="cmbSource" class="comboWidth">
                <option value="0">Select a source</option>
            </select>
            <label class="lbl">
            </label>
            <label class="lbl lblLoan">
                Source Code:</label>
            <input type="text" id="txtSourceCode" class="txtField" />
        </div>
        <div class="fieldDiv">
            <label class="lbl lblLoan">
                Source Name:</label>
            <input type="text" id="txtSourceNAme" class="txtField" />
            <label class="lbl">
            </label>
            <label class="lbl lblLoan">
                Applied Date<span class="starColor">*</span>:</label>
            <input type="text" id="txtAppliedDate" class="txtField" />
        </div>
        <div class="fieldDiv">
            <label class="lbl lblLoan">
                Applied Amount<span class="starColor">*</span>:</label>
            <input type="text" id="txtAppliedAmount" class="txtField" onchange="LoanApplicationHealper.checkInputIsNumber('txtAppliedAmount')"/>
            <label class="lbl">
            </label>
            <label class="lbl lblLoan">
                Joint Application <span class="starColor">*</span>:</label>
            <select id="cmbJointApplication" class="comboWidth">
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
            <label class="lbl lblLoan" style="display:none;">
                Receive Date<span class="starColor">*</span>:</label>
            <input type="text" id="txtReceiveDate" class="txtField" style="display:none;" />
        </div>
        <div class="fieldDiv">
            <label class="lbl lblLoan">
                Asking Tenor <span class="starColor">*</span>:</label>
            <select id="cmbAskingTenor" class="comboWidth">
                <option value="12">12 Months</option>
                <option value="24">24 Months</option>
                <option value="36">36 Months</option>
                <option value="48">48 Months</option>
                <option value="60">60 Months</option>
            </select>
            <label class="lbl">
            </label>
            
           <%-- <label class="lbl lblLoan">
                Joint Application <span class="starColor">*</span>:</label>
            <select id="cmbJointApplication" class="comboWidth">
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>--%>
        </div>
        
        <div class="fieldDiv">
            <label class="lbl lblLoan">
                Asking Rate<span class="starColor">*</span>:</label>
            <input type="text" id="txtAskingRate" class="txtField" onchange="LoanApplicationHealper.checkInputIsNumber('txtAskingRate')"/>
            <label class="lbl">
            </label>
        </div>
        
    </div>
 
    









<ol id="toc" class ="toc">
    <li><a href="#page-1"><span>Applicant & Profession</span></a></li>
    <li><a href="#page-2"><span>Vehicle & Insurance</span></a></li>
    <li><a href="#page-3"><span>Account & Financial</span></a></li>
    <li><a href="#page-4"><span>References ,Facility & Security</span></a></li>
<%--    <li><a href="#page-5"><span>Bank Statement</span></a></li>
--%>
</ol>



<div class="content" id="page-1">


<%--Applicant segment start--%>

                <div id="mainDiv" style=" width:98%; margin-left:5px;">
        <div id ="div2" class ="divHeaderLoan">Applicant</div>
        
        <div class ="clear"></div>
        
        
        <div id ="div10" class ="divLeftSubHead" style="border:0; margin-left:2px;"></div>
        
        <div id ="div12" class ="divMiddileSubHead">Primary Applicant</div>
        
        <div id ="JApplicantdivHeader" class ="divRightSubHead">Joint Applicant</div>
        
        <div class="clear"></div>
        
        <div id ="divLeft" class ="divLeft">
            <div class ="divLbl"><label class="lblSize">Name<span class="starColor">*</span>:</label></div>
            <div class ="divLbl"><label class="lblSize">Gender<span class="starColor">*</span>:</label></div>
            <div class ="divLbl"><label class="lblSize">Relationship Number:</label></div>
            <div class ="divLbl"><label class="lblSize">TIN Number <span class="starColor">*</span>:</label></div>
            <div class ="divLbl"><label class="lblSize">Father�s Name:</label></div>
            <div class ="divLbl"><label class="lblSize">Mother�s Name:</label></div>
            <div class ="divLbl"><label class="lblSize">Date of Birth (MM-DD-YYYY)<span class="starColor">*</span>:</label></div>
            <div class ="divLbl"><label class="lblSize">Marital Status:</label></div>
            <div class ="divLbl"><label class="lblSize">Spouse Name:</label></div>
            <div class ="divLbl"><label class="lblSize">Spouse Profession:</label></div>
            <div class ="divLbl"><label class="lblSize">Spouse Work Address:</label></div>
            <div class ="divLbl"><label class="lblSize">Spouse Contact Number:</label></div>
            <div class ="divLbl"><label class="lblSize">Relationship with Primary/Joint:</label></div>
            <div class ="divLbl"><label class="lblSize">Nationality:</label></div>
            <div class ="divLbl"><label class="lblSize">Identification Number:</label></div>
            <div class ="divLbl"><label class="lblSize">Identification�Document:</label></div>
            <div class ="divLbl"><label class="lblSize">Number of Dependents:</label></div>
            <div class ="divLbl"><label class="lblSize">Highest Education Level<span class="starColor">*</span>:</label></div>
            <div class ="divLbl" style ="height:50px;*height:50px;_height:50px;"><label class="lblSize" >Residence Address:</label></div>
            <div class ="divLbl" style ="height:50px;*height:50px;_height:50px;"><label class="lblSize">Mailing Address:</label></div>
            <div class ="divLbl" style ="height:50px;*height:50px;_height:50px;"><label class="lblSize">Permanent Address:</label></div>
            <div class ="divLbl"><label class="lblSize">Residence Status:</label></div>
            <div class ="divLbl"><label class="lblSize">Ownership�Document:</label></div>
            <div class ="divLbl"><label class="lblSize">Years�in�Current�Address:</label></div>
            <div class ="divLbl"><label class="lblSize">Phone (Residence):</label></div>
            <div class ="divLbl"><label class="lblSize">Phone (Mobile):</label></div>
            <div class ="divLbl"><label class="lblSize">Email:</label></div>
            <div class ="divLbl"><label class="lblSize">Directorship with any Bank:</label></div>
            
        </div>
        
        <div id ="divMiddile" class ="divMiddile">
                <div class="fieldDivContaint">
                <input type="text" id="txtPApplicantName" class="txtBoxSize" />
                </div>
                <div class="fieldDivContaint">
                <select id="cmbGenderForPrimary" class="comboSize"><option value="1">MALE</option><option value="2">FEMALE</option></select>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantRelationshipNumber" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantTINNumber" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantFatherName" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantMotherName" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantDateOfBirth" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbPApplicantMaritalStatus" class="comboSize"><option value="-1">Select a Status</option>
                                                                                                              <option value="Married">Married</option>
                                                                                                              <option value="Single">Single</option>
                                                                                                              <option value="Separated">Separated</option>
                                                                                                              <option value="Others">Others</option>
                                                                                                                </select>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantSpouseName" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantSpouseProfession" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantSpouseWorkAddress" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantSpouseContactNumber" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantRelationshipWithPrimary" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantNationality" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint" style ="margin-bottom:5px; height:25px; _height:25px;*height:25px;" >
                    <div style ="width:100%;">
                    <div class ="divLeft" style ="width:42%;*width:42%;_width:42%;padding:0px; border:none;"><select id="cmbPApplicantIdentificationNumber" class="comboSize"><option value="-1">Select a source</option>
                                                                             <option value="ID">ID</option>
                                                                             <option value="License">License</option>
                                                                             <option value="Passport">Passport</option>
                                                                             </select></div>
                    <div class ="divMiddile" style ="width:55%;_width:52%; padding:0px; border:none;"><input id="txtPApplicantIdentificationNumber" type ="text" class="txtBoxSize"/></div>
                    
                        
                    </div>
                
                </div>
               
                <div class="fieldDivContaint">
                <select id="cmbPApplicantIdentificationDocument" class="comboSize"><option value="-1">Select from List</option>
                                                                                                                        <option value="Customer�Declaration">Customer�Declaration</option>
                                                                                                                        <option value="Certificate">Certificate</option>
                                                                                                                        <option value="Other�Doc">Other�Doc</option>
                                                                                                                        
                                                                                                                       </select>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantNumberOfDependents" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                 <select id="cmbPApplicantHighestEducationLevel" class="comboSize"><option value="-1">Select an Education Level</option>
                                                                                                                        <option value="SSC">SSC</option>
                                                                                                                        <option value="HSC">HSC</option>
                                                                                                                        <option value="DIP">DIP</option>
                                                                                                                        <option value="Graduate">Graduate</option>
                                                                                                                        <option value="P-Graduate">P-Graduate</option>
                                                                                                                        <option value="Others">Others</option>
                                                                                                                        
                                                                                                                       </select>
                </div>
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtPApplicantResidenceAddress" class ="txtAreaSize"></textarea>
                </div>
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtPApplicantMailingAddress" class ="txtAreaSize"></textarea>
                </div>
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtPApplicantPermanentAddress" class ="txtAreaSize"></textarea>
                </div>
                <div class="fieldDivContaint" >
                <select id="cmbPApplicantResidenceStatus" class="comboSize"><option value="-1">Select a Satatus</option>
                                                                                                                 <option value="Own">Own</option>
                                                                                                                 <option value="Rented">Rented</option>
                                                                                                                 <option value="Fam-Owned">Fam-Owned</option>
                                                                                                                 <option value="Com-Provided">Com-Provided</option>
                                                                                                                 <option value="Others">Others</option>
                                                                                                                 </select>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbPApplicantOwnershipDocument" class="comboSize"><option value="-1">Select Ownership Document</option>
                                                                                                                     <option value="Dead">Dead</option>
                                                                                                                     <option value="Mutation">Mutation</option>
                                                                                                                     <option value="Electric�Bill">Electric�Bill</option>
                                                                                                                     <option value="Water�Bill">Water�Bill</option>
                                                                                                                     <option value="Others">>Others</option>
                                                                                                                     </select>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbPApplicantResidenceStatusYear" class="comboSize"><option value="-1">Select year</option>
                                                                                                                     <option value="1">1 Year</option>
                                                                                                                     <option value="2">1-3 years</option>
                                                                                                                     <option value="3">3-5 years</option>
                                                                                                                     <option value="4">5-10 years</option>
                                                                                                                     <option value="5">>10 year</option>
                                                                                                                     </select>
                </div>
                <div class="fieldDivContaint">
               <input id="txtPApplicantPhoneRescident" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtpApplicantPhoneMobile" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantEmail" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbPAllicantDirectorShipWithAnyBank" class="comboSize">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option></select>
                </div>
                
        </div>
        
        <div id ="JApplicantControldiv" class ="divRight">
                <div class="fieldDivContaint">
                <input type="text" id="txtJApplicantName" class="txtBoxSize" />
                </div>
                <div class="fieldDivContaint">
                    <select id="cmbJointGender" class="comboSize"><option value="1">MALE</option><option value="2">FEMALE</option></select>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantRelationshipNumber" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantTINNumber" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantFatherName" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantMotherName" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantDateOfBirth" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbJApplicantMaritalStatus" class="comboSize"><option value="-1">Select a Status</option>
                                                                                                              <option value="Married">Married</option>
                                                                                                              <option value="Single">Single</option>
                                                                                                              <option value="Separated">Separated</option>
                                                                                                              <option value="Others">Others</option>
                                                                                                                </select>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantSpouseName" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantSpouseProfession" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantSpouseWorkAddress" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantSpouseContactNumber" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantRelationshipWithPrimary" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantNationality" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint" style ="margin-bottom:5px; height:25px; _height:25px;*height:25px;" >
                    <div style ="width:100%;">
                    <div class ="divLeft" style ="width:42%;*width:42%;_width:42%;padding:0px; border:none;"><select id="cmbJApplicantIdentificationNumber" class="comboSize"><option value="-1">Select a source</option>
                                                                             <option value="ID">ID</option>
                                                                             <option value="License">License</option>
                                                                             <option value="Passport">Passport</option>
                                                                             </select></div>
                    <div class ="divMiddile" style ="width:55%;_width:52%; padding:0px; border:none;"><input id="txtJApplicantIdentificationNumber" type ="text" class="txtBoxSize"/></div>
                    
                        
                    </div>
                
                </div>
               
                <div class="fieldDivContaint">
                <select id="cmbJApplicantIdentificationDocument" class="comboSize"><option value="-1">Select from List</option>
                                                                                                                        <option value="Customer�Declaration">Customer�Declaration</option>
                                                                                                                        <option value="Certificate">Certificate</option>
                                                                                                                        <option value="Other�Doc">Other�Doc</option>
                                                                                                                        
                                                                                                                       </select>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantNumberOfDependents" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                 <select id="cmbJApplicantHighestEducationLevel" class="comboSize"><option value="-1">Select an Education Level</option>
                                                                                                                        <option value="SSC">SSC</option>
                                                                                                                        <option value="HSC">HSC</option>
                                                                                                                        <option value="DIP">DIP</option>
                                                                                                                        <option value="Graduate">Graduate</option>
                                                                                                                        <option value="P-Graduate">P-Graduate</option>
                                                                                                                        <option value="Others">Others</option>
                                                                                                                        
                                                                                                                       </select>
                </div>
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtJApplicantResidenceAddress" class ="txtAreaSize"></textarea>
                </div>
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtJApplicantMailingAddress" class ="txtAreaSize"></textarea>
                </div>
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtJApplicantPermanentAddress" class ="txtAreaSize"></textarea>
                </div>
                <div class="fieldDivContaint" >
                <select id="cmbJApplicantResidenceStatus" class="comboSize"><option value="-1">Select a Satatus</option>
                                                                                                                 <option value="Own">Own</option>
                                                                                                                 <option value="Rented">Rented</option>
                                                                                                                 <option value="Fam-Owned">Fam-Owned</option>
                                                                                                                 <option value="Com-Provided">Com-Provided</option>
                                                                                                                 <option value="Others">Others</option>
                                                                                                                 </select>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbJApplicantOwnershipDocument" class="comboSize"><option value="-1">Select Ownership Document</option>
                                                                                                                     <option value="Dead">Dead</option>
                                                                                                                     <option value="Mutation">Mutation</option>
                                                                                                                     <option value="Electric�Bill">Electric�Bill</option>
                                                                                                                     <option value="Water�Bill">Water�Bill</option>
                                                                                                                     <option value="Others">>Others</option>
                                                                                                                     </select>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbJApplicantResidenceStatusYear" class="comboSize">
                <option value="-1">Select year</option>
                         <option value="1">1 Year</option>
                         <option value="2">1-3 years</option>
                         <option value="3">3-5 years</option>
                         <option value="4">5-10 years</option>
                         <option value="5">>10 year</option>
                         </select>
                </div>
                <div class="fieldDivContaint">
               <input id="txtJApplicantPhoneRescident" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantPhoneMobile" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantEmail" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbJAllicantDirectorShipWithAnyBank" class="comboSize">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option></select>
                </div>
                
             </div>
         

          <div class="clear"></div>
          
         
          <br />
          <br />
 </div>

<%--Applicant segment end--%>


<%--profession segment start--%>

                <div id="Div15" style=" width:98%; margin-left:5px;">
                <div id ="div16" class ="divHeaderLoan">Profession</div>

                <div class ="clear"></div>


                <div id ="div17" class ="divLeftSubHead" style="border:0; margin-left:2px;"></div>

                <div id ="div18" class ="divMiddileSubHead">Primary Applicant</div>

                <div id ="JProfessionHeaderDiv" class ="divRightSubHead">Joint Applicant</div>

                <div class="clear"></div>

                <div id ="div20" class ="divLeft">
                   	<div class ="divLbl">Primary Profession<span class="starColor">*</span>:<label class="lblSize"></label></div>
                    <div class ="divLbl"><label class="lblSize">Other Profession:</label></div>
                    <div class ="divLbl"><label class="lblSize">Month in Current Profession<span class="starColor">*</span>:</label></div>
                    <div class ="divLbl"><label class="lblSize">Name of Company (<span class="starColor">salaried</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Name of Organization (<span class="starColor">unlisted</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Designation (<span class="starColor">salaried/self-emp</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Nature of Business (<span class="starColor">business</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize"></label>Years in Business (<span class="starColor">business</span>):</div>
                    <div class ="divLbl"><label class="lblSize">Office Status (<span class="starColor">business</span>):</label></div>
                    <div class ="divLbl" style ="height:50px;*height:50px;_height:50px;">Address:<label class="lblSize" ></label></div>
                    <div class ="divLbl"><label class="lblSize">Office Phone (<span class="starColor">sal/self/business</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Ownership Type (<span class="starColor">business</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Employment Status (<span class="starColor">salaried</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Total Professional Experience:</label></div>
                    <div class ="divLbl"><label class="lblSize">Number of Employees (<span class="starColor">business</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Equity/Share % (<span class="starColor">business</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Primary Income Source (<span class="starColor">self-emp</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Other Income Source (<span class="starColor">self-emp</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">No. of Floors Rented (<span class="starColor">landlord</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Nature of Rented Floors (<span class="starColor">landlord</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Rented Ares in SFT (<span class="starColor">landlord</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Construction Completing Year (<span class="starColor">landlord</span>):</label></div>

                    
                   
                   
                    
                    
                </div>

                <div id ="div21" class ="divMiddile">
                        <div class="fieldDivContaint">
                                <select id="cmbPApplicantPrimaryProfession" class="comboSize"><option value="-1">Select Primary Profession</option>
                                                                                             </select>
                        </div>
                        <div class="fieldDivContaint">
                                <select id="cmbPApplicantOtherProfession" class="comboSize"><option value="-1">Select Other Profession</option>
                                                                                             </select>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantMonthInCurrentProfession" type ="text" class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <select id="cmbPApplicantNameOfCompany" class="comboSize"><option value="0">Select Company</option></select>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantNameofOrganization" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantDesignation" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantNatureOfBusiness" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantYearsInBusiness" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <select id="cmdPApplicantOfficeStatus" class="comboSize"><option value="0">Select Office Status</option>
                                                                                             <option value="1">Own</option>
                                                                                             <option value="2">Rented</option>
                                                                                             </select>
                        </div>
                        
                        <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                        
                        <textarea cols="1" rows="3" id="txtPApplicantAddress" class ="txtAreaSize"></textarea>
                        </div>
            
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantOfficePhone" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                                <select id="cmbPApplicantOwnershipType" class="comboSize"><option value="0">Select Ownership Type</option>
                                                                                             <option value="1">Proprietary</option>
                                                                                             <option value="2">Partnership</option>
                                                                                             <option value="3">Private Ltd</option>
                                                                                             <option value="4">Public Ltd</option>
                                                                                             </select>
                        </div>
                        <div class="fieldDivContaint">
                            <select id="cmbPApplicantEmploymentStatus" class="comboSize"><option value="0">Select Employment Status</option>
                                                                                             <option value="1">Contractual</option>
                                                                                             <option value="2">Temporary</option>
                                                                                             <option value="3">Permanent</option>
                                                                                             </select>
                        </div>
                                              
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantTotalProfessionalExperience" type ="text"class="txtBoxSize"/>

                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantNumberOfEmployees" type ="text"class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantNumberOfEmployees')"/>

                        </div>
                        <div class="fieldDivContaint">
                            <input id="txtPApplicantEquity_Share" type ="text"class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantEquity_Share')"/>
                        </div>
                        <div class="fieldDivContaint">
                            <input id="txtPApplicantPrimaryIncomeSource" type ="text"class="txtBoxSize"/>
						</div>
                        <div class="fieldDivContaint" >
                        <input id="txtPApplicantOtherIncomeSource" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtPApplicantNoOfFloorsRented" type ="text"class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantNoOfFloorsRented')"/>
						</div>
                        <div class="fieldDivContaint">
                                <select id="cmbPApplicantNatureOfRentedFloors" class="comboSize"><option value="0">Select Nature Of Rented Floors</option>
                                                                                             <option value="1">Commercial</option>
                                                                                             <option value="2">Residential</option>
                                                                                             </select>
						</div>
                        <div class="fieldDivContaint">
                                <input id="txtPApplicantRentedAresInSFT" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                                <input id="txtPApplicantConstructionCompletingYear" type ="text"class="txtBoxSize"/>
                        </div>
                       
                        
                </div>

                <div id ="JProfessionControlDiv" class ="divRight">
                        <div class="fieldDivContaint">
                                <select id="cmbJApplicantPrimaryProfession" class="comboSize"><option value="0">Select Primary Profession</option>
                                                                                             </select>
                        </div>
                        <div class="fieldDivContaint">
                                <select id="cmbJApplicantOtherProfession" class="comboSize"><option value="0">Select Other Profession</option>
                                                                                             </select>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantMonthInCurrentProfession" type ="text" class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <select id="cmbJApplicantNameOfCompany" class="comboSize"><option value="0">Select Company</option></select>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantNameofOrganization" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantDesignation" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantNatureOfBusiness" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantYearsInBusiness" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <select id="cmdJApplicantOfficeStatus" class="comboSize"><option value="0">Select Office Status</option>
                                                                                             <option value="1">Own</option>
                                                                                             <option value="2">Rented</option>
                                                                                             </select>
                        </div>
                        
                        <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                        
                        <textarea cols="1" rows="3" id="txtJApplicantAddress" class ="txtAreaSize"></textarea>
                        </div>
            
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantOfficePhone" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                                <select id="cmbJApplicantOwnershipType" class="comboSize"><option value="0">Select Ownership Type</option>
                                                                                             <option value="1">Proprietary</option>
                                                                                             <option value="2">Partnership</option>
                                                                                             <option value="3">Private Ltd</option>
                                                                                             <option value="4">Public Ltd</option>
                                                                                             </select>
                        </div>
                        <div class="fieldDivContaint">
                            <select id="cmbJApplicantEmploymentStatus" class="comboSize"><option value="0">Select Employment Status</option>
                                                                                             <option value="1">Contractual</option>
                                                                                             <option value="2">Temporary</option>
                                                                                             <option value="3">Permanent</option>
                                                                                             </select>
                        </div>
                                              
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantTotalProfessionalExperience" type ="text"class="txtBoxSize"/>

                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantNumberOfEmployees" type ="text"class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantNumberOfEmployees')"/>

                        </div>
                        <div class="fieldDivContaint">
                            <input id="txtJApplicantEquity_Share" type ="text"class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantEquity_Share')"/>
                        </div>
                        <div class="fieldDivContaint">
                            <input id="txtJApplicantPrimaryIncomeSource" type ="text"class="txtBoxSize"/>
						</div>
                        <div class="fieldDivContaint" >
                        <input id="txtJApplicantOtherIncomeSource" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                        <input id="txtJApplicantNoOfFloorsRented" type ="text"class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantNoOfFloorsRented')"/>
						</div>
                        <div class="fieldDivContaint">
                                <select id="cmbJApplicantNatureOfRentedFloors" class="comboSize"><option value="0">Select Nature Of Rented Floors</option>
                                                                                             <option value="1">Commercial</option>
                                                                                             <option value="2">Residential</option>
                                                                                             </select>
						</div>
                        <div class="fieldDivContaint">
                                <input id="txtJApplicantRentedAresInSFT" type ="text"class="txtBoxSize"/>
                        </div>
                        <div class="fieldDivContaint">
                                <input id="txtJApplicantConstructionCompletingYear" type ="text"class="txtBoxSize"/>
                        </div>       
                        
                 </div>
                 
                  <div class="clear"></div>
                  
                  
                  <br />
                  <br />
          
           </div>


<%--profession segment end--%>


    
</div>



<div class="content" id="page-2">






<%--Vehicle tab start--%>


<div id="Div2" style=" width:98%; margin-left:5px;">
        <div id ="div5" class ="divHeader" style ="height:1px;*height:1px;_height:1px;"></div>
        
        <div class ="clear"></div>
        <div id ="div3" class ="divLeft">
            		<div class ="divLbl"><label class="lblSize">Vendor:</label></div>
                    <div class ="divLbl"><label class="lblSize">Vendor Name (if not in the list):</label></div>
                    <div class ="divLbl"><label class="lblSize">Manufacturer <span class="starColor">*</span>):</label></div>
                    <div class ="divLbl"><label class="lblSize">Model <span class="starColor">*</span>:</label></div>
                    <div class ="divLbl"><label class="lblSize">Trim Level:</label></div>
                    <div class ="divLbl"><label class="lblSize">Engine CC:</label></div>
                    <div class ="divLbl"><label class="lblSize">Vehicle Type <span class="starColor">*</span>:</label></div>
                    <div class ="divLbl"><label class="lblSize">Vehicle Status <span class="starColor">*</span>:</label></div>
                    <div class ="divLbl"><label class="lblSize">Manufacturing Year <span class="starColor">*</span>:</label></div>
                    <div class ="divLbl"><label class="lblSize">Quoted Price <span class="starColor">*</span>:</label></div>
                  
        </div>
        
        <div id ="div4" class ="divMiddile">
                <div class="fieldDivContaint">
                        <select id="cmbVendor" class="comboSize"><option value="0">Select Vendor</option></select>                                                                                      
                </div>
                <div class="fieldDivContaint" style ="margin-bottom:5px; height:25px; _height:25px;*height:25px;" >
                    <div style ="width:100%;">
                    <div class ="divLeft" style ="width:55%;_width:52%; padding:0px; border:none;"><input id="txtVendorName" type ="text" class="txtBoxSize"/></div>
                    
                    <div class ="divMiddile" style ="width:42%;*width:42%;_width:42%;padding:0px; border:none;">
                    <select id="cmbDealerType" class="comboSize"><option value="0">Select Dealer Type</option></select>
                    
                        
                    </div>
                
                 </div>
                 </div>
                 <div class="fieldDivContaint" >
                        <select id="cmbManufacturer" class="comboSize"  onchange="LoanApplicationManager.changeManufacture()" ><option value="0" >Select Manufacturer</option></select>
                        
                    
                        
                 </div>
                 
                <div class="fieldDivContaint">
                      <select id="cmbModel" class="comboSize" onchange="LoanApplicationManager.changeModel()" ><option value="0">Select Model</option>
                                                                                             </select>  
                      <input type="text" id="txtModelName" class="txtBoxSize" style="display:none;" />
                      <input id="hdnModel" type="hidden" />                                                                           
                </div>
                <div class="fieldDivContaint">
                        <%--<select id="cmbTrimLevel" class="comboSize" onchange ="LoanApplicationManager.changeTrimLevel()"><option value="0">Select Trim Level</option>
                                                                                             </select>    --%>
                            <input type="text" id="txtTrimLevel" class="txtBoxSize" disabled="disabled" />                                                                         
                </div>
                <div class="fieldDivContaint">
                      <%--<select id="cmbEngineCC" class="comboSize" onchange ="LoanApplicationManager.changeEngineCC()"><option value="0">Select Engine CC</option>
                                                                                             </select>--%>
                      <input type="text" id="txtEngineCC" class="txtBoxSize" disabled="disabled" />                                                                                                                 
                </div>
                
                <div class="fieldDivContaint">
                <select id="cmbVehicleType" class="comboSize" title="Vehicle Type" style="display:none;">
		            <option value="-1">Select From List</option>
		            <option value="S1">Sedan</option>
		            <option value="S2">Station wagon</option>
		            <option value="S3">SUV</option>
		            <option value="S4">Pick-Ups</option>
		            <option value="S5">Microbus</option>
		            <option value="S6">MPV</option>
		        </select>
                <input id="txtVehicleType" type ="text" disabled="disabled" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                        <select id="cmdVehicleStatus" class="comboSize" onchange="LoanApplicationManager.changeVehicleStatus()"><option value="0">Select Vehicle Status</option>
                                                                                             </select>
                                                                                                                                
                </div>
                <div class="fieldDivContaint">
                        <%--<select id="cmbManufacturingYear" class="comboSize" onchange="LoanApplicationManager.changeManufacturingYear()"><option value="0">Select Manufacturing Year</option>
                                                                                             </select>--%>
                        <input type="text" id="txtManufacturingyear" class="txtBoxSize" onchange="LoanApplicationManager.changeManufacturingYear()" />                                                                                      
                </div>
                <div class="fieldDivContaint">
                 <input id="txtQuotedPrice" type ="text" class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtQuotedPrice')"/>
                </div>
                                

        </div>
        <div class="clear"></div>
           
          <br />
          
         <div id ="div7" class ="divLeft" style="display:none;">
                    <div class ="divLbl"><label class="lblSize">Verified Price:</label></div>
                    <div class ="divLbl"><label class="lblSize">Price Considered Based on:</label></div>
                    <div class ="divLbl"><label class="lblSize">Seating Capacity:</label></div>
                    <div class ="divLbl"><label class="lblSize">Car Verification:</label></div>                  
        </div>
         <div id ="div55" class ="divMiddile" style="display:none;">
         <div class="fieldDivContaint">
                <input id="txtVerifiedPrice" type ="text" class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtVerifiedPrice')"/>
                </div>
                <div class="fieldDivContaint">
                       <select id="cmbPriceConsideredBasedOn" class="comboSize"><option value="0">Select Price Option</option>
                                                                                             <option value="1">Index Value</option>
                                                                                             <option value="2">Phone</option>
                                                                                             <option value="3">Price Verify</option>
                                                                                             <option value="4">Quotation</option>
                                                                                             <option value="5">NA</option>
                                                                                             </select>                                                                                        
                </div>
                <div class="fieldDivContaint">
                <input id="txtSeatingCapacity" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                       <select id="cmbCarVerification" class="comboSize">
                       <option value="0">Select Verification Type</option>
					 <option value="1">Done</option>
					 <option value="2">Valued & Verified</option>
					 <option value="3">Not Required</option>
                     </select>
                </div>
                
                                
        </div>
        
        <div class="clear"></div>
           
          <br />
          
          <div id ="div8" class ="divLeft">
                    <div class ="divLbl"><label class="lblSize">General�Insurance:</label></div>
                    <div class ="divLbl"><label class="lblSize">Life�Insurance:</label></div>
                    <div class ="divLbl"><label class="lblSize">Insurance Finance�with�Loan(General):</label></div>
                    <div class ="divLbl"><label class="lblSize">Insurance Finance�with�Loan(Life):</label></div>
                   
        </div>
          
          <div id ="div6" class ="divMiddile">
                <div class="fieldDivContaint">
                        <select id="cmbGeneralInsurance" class="comboSize"><option value="-1">Select General Insurance</option>
                                                                                             </select>                                                                                
                </div>
                <div class="fieldDivContaint">
                        <select id="cmbLifeInsurance" class="comboSize"><option value="-1">Select Life Insurance</option>
                                                                                             </select>                                                                                      
                </div>
                <div class="fieldDivContaint">
                        <select id="cmbInsuranceFinancewithLoan" class="comboSize" disabled="disabled">
                                                                                             <option value="0">NO</option>
                                                                                             <option value="1">Yes</option>
                                                                                             </select>                                                                                       
                </div>
                <div class="fieldDivContaint">
                        <select id="cmbInsuranceFinancewithLoanLife" class="comboSize" disabled="disabled">
                                                                                             <option value="0">NO</option>
                                                                                             <option value="1">Yes</option>
                                                                                             </select>                                                                                       
                </div>
               
                                
        </div>
        
        <div class="clear"></div>
           
          <br />
          
          
          
 </div>


<%--Vehicle tab end--%>









    
</div>



<div class="content" id="page-3">









          <!-- ---------------------Accounts And Finance Tab 3---------------------------------------------- -->
        <div id="Div9" style=" width:98%; margin-left:5px;">
        <div id ="div11" class ="divHeaderLoan">Account</div>
        
        <div class ="clear"></div>
        
        
        <div id ="div23" class ="divLeftSubHead" style="border:0; margin-left:2px;"></div>
        
        <div id ="div24" class ="divMiddileSubHead">Primary Applicant</div>
        
        <div id ="JAccountHeaderDiv" class ="divRightSubHead">Joint Applicant</div>
        
        <div class="clear"></div>
        
        <div id ="div26" class ="divLeft">
            <div class ="divLbl"><label class="lblSize">Account Number for SCB:</label></div>
            <div class ="divLbl"><label class="lblSize">Credit Card Number for SCB:</label></div>
         </div>
        
        <div id ="div27" class ="divMiddile">
                <div class="fieldDivContaint">
                <input type="text" id="Text54" class="txtBoxSize" />
                </div>
                <div class="fieldDivContaint">
                <input id="Text95" type ="text" class="txtBoxSize"/>
                </div>
        </div>
        
        <div id ="JAccountControlDiv" class ="divRight">
                <div class="fieldDivContaint">
                <input type="text" id="Text55" class="txtBoxSize" />
                </div>
                <div class="fieldDivContaint">
                <input id="Text59" type ="text" class="txtBoxSize"/>
                </div>
                
                
        </div>
         

          <div class="clear"></div>
          
         
          <br />
          
          
          <div> 
              <table style="width:99%;">
                        <tr><td>
                                <div style="text-align:center; font-size:medium;font-weight:bold;">Primary Applicant Other Bank Account
                                
                                    <br />
                                    <br />
                                    <div style="border:none;">
                                        <table id="primaryBankAccTable" >
                                               <tr id="theader">
                                                   <td>Bank</td>
                                                   <td>Branch</td>
                                                   <td>Account Number</td>
                                                   <td>A/C Category</td>
                                                   <td>A/C Type</td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbPApplicantBank1" onchange="LoanApplicationManager.GetBranch('cmbPApplicantBank1')" ><option value="-1">Select a Bank</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantBranch1" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtPApplicantAccountNumber1" type ="text"/></td>
                                                    <td><select id="cmbPApplicantAccountCategory1" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantAccountType1" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbPApplicantBank2" onchange="LoanApplicationManager.GetBranch('cmbPApplicantBank2')" ><option value="-1">Select a Bank</option>
                                                                                                                                                                                         
                                                                                           </select></td>
                                                    <td><select id="cmbPApplicantBranch2" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtPApplicantAccountNumber2" type ="text"/></td>
                                                    <td><select id="cmbPApplicantAccountCategory2" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantAccountType2" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbPApplicantBank3" onchange="LoanApplicationManager.GetBranch('cmbPApplicantBank3')"><option value="-1">Select a Bank</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantBranch3" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtPApplicantAccountNumber3" type ="text"/></td>
                                                    <td><select id="cmbPApplicantAccountCategory3" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantAccountType3" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbPApplicantBank4" onchange="LoanApplicationManager.GetBranch('cmbPApplicantBank4')"><option value="-1">Select a Bank</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantBranch4" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtPApplicantAccountNumber4" type ="text"/></td>
                                                    <td><select id="cmbPApplicantAccountCategory4" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantAccountType4" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbPApplicantBank5" onchange="LoanApplicationManager.GetBranch('cmbPApplicantBank5')"><option value="-1">Select a Bank</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantBranch5" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtPApplicantAccountNumber5" type ="text"/></td>
                                                    <td><select id="cmbPApplicantAccountCategory5" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbPApplicantAccountType5" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>

                                        </table>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        
                    </table>
          </div >
          <div class ="clear"></div>
          <br />
          <div> 
                    <table style="width:99%;">
                        <tr><td>
                               <div id="Account_JointApplicantOtherBankAccount" style="text-align:center; font-size:medium;font-weight:bold;">Joint Applicant Other Bank Account
                                
                                    <br />
                                    <br />
                                    <div style="border:none;">
                                        <table id="jointBankAccTable" >
                                               <tr id="Tr1" class="theader">
                                                   <td>Bank</td>
                                                   <td>Branch</td>
                                                   <td>Account Number</td>
                                                   <td>A/C Category</td>
                                                   <td>A/C Type</td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbJApplicantBank1" onchange="LoanApplicationManager.GetBranch('cmbJApplicantBank1')" ><option value="-1">Select a Bank</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantBranch1" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtJApplicantAccountNumber1" type ="text"/></td>
                                                    <td><select id="cmbJApplicantAccountCategory1" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantAccountType1" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbJApplicantBank2" onchange="LoanApplicationManager.GetBranch('cmbJApplicantBank2')" ><option value="-1">Select a Bank</option>
                                                                                                                                                                                         
                                                                                           </select></td>
                                                    <td><select id="cmbJApplicantBranch2" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtJApplicantAccountNumber2" type ="text"/></td>
                                                    <td><select id="cmbJApplicantAccountCategory2" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantAccountType2" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbJApplicantBank3" onchange="LoanApplicationManager.GetBranch('cmbJApplicantBank3')"><option value="-1">Select a Bank</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantBranch3" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtJApplicantAccountNumber3" type ="text"/></td>
                                                    <td><select id="cmbJApplicantAccountCategory3" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantAccountType3" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbJApplicantBank4" onchange="LoanApplicationManager.GetBranch('cmbJApplicantBank4')"><option value="-1">Select a Bank</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantBranch4" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtJApplicantAccountNumber4" type ="text"/></td>
                                                    <td><select id="cmbJApplicantAccountCategory4" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantAccountType4" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                    <td><select id="cmbJApplicantBank5" onchange="LoanApplicationManager.GetBranch('cmbJApplicantBank5')"><option value="-1">Select a Bank</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantBranch5" ><option value="-1">Select a Branch</option>
                                                                                             </select></td>
                                                    <td><input id="txtJApplicantAccountNumber5" type ="text"/></td>
                                                    <td><select id="cmbJApplicantAccountCategory5" ><option value="0">Select from List</option>
                                                                                             <option value="1">Corporate</option>
                                                                                             <option value="2">Individual</option>
                                                                                             </select></td>
                                                    <td><select id="cmbJApplicantAccountType5" ><option value="0">Select from List</option>
                                                                                             <option value="1">Current</option>
                                                                                             <option value="2">Savings</option>
                                                                                             <option value="3">Salary</option>
                                                                                             <option value="4">Others</option>
                                                                                             </select></td>
                                               </tr>

                                        </table>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        
                    </table>
                </div>
           
            <div class ="clear"></div>
            <br />
            <br />
            
           
           
           
   <!-- ------------Financial Segment ------------------------------ -->         
       <div id ="div29" class ="divHeaderLoan">Financial</div>
        
        <div class ="clear"></div>
        
        
        <div id ="div30" class ="divLeftSubHead" style ="width:150px; *width:160px; _width:165px; border:0; margin-left:2px;"></div>
        
        <div id ="div31" class ="divMiddileSubHead" style ="width:250px;*width:250px;_width:250px;">Primary Applicant</div>
        
        <div id ="JFinanceHeaderDiv" class ="divRightSubHead" style ="width:250px;*width:250px;_width:250px;">Joint Applicant</div>
        
        <div class="clear"></div>
        
        <div id ="div33" class ="divLeft" style ="width:150px;*width:150px;_width:150px;">
            <div class ="divLbl"><label class="lblSize">Declared Primary Income <span class="starColor">*</span>:</label></div>
            <div class ="divLbl"><label class="lblSize">Declared Other Income:</label></div>
            <div class ="divLbl" style="height:152px;"><label class="lblSize" >Expenditure Details:</label></div>
            <div class ="divLbl"><label class="lblSize">Repayment to:</label></div>
            <div class ="divLbl"><label class="lblSize">Repayment for:</label></div>
         </div>
        
        <div id ="div34" class ="divMiddile" style ="width:250px;*width:250px;_width:250px;">
                
                 <div class="fieldDivContaint" style ="margin-bottom:5px; height:25px; _height:25px;*height:25px;" >
                    <div style ="width:100%;">
                    <div class ="divLeft" style ="xwidth:42%; width:0%;padding:0px; border:none;">
                    <input id="txtPApplicantDeclaredPrimaryIncomeSourch" type ="text" class="txtBoxSize" style="display:none;"/></div>
                    <div class ="divMiddile" style ="xwidth:55%;_xwidth:52%;width:100%;*width:98%; padding:0px; border:none;">
                    <input id="txtPApplicantDeclaredPrimaryIncomeAmount" type ="text" class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantDeclaredPrimaryIncomeAmount')"/></div>
                    
                        
                    </div>
                
                </div>
                <div class="fieldDivContaint" style ="margin-bottom:5px; height:25px; _height:25px;*height:25px;" >
                    <div style ="width:100%;">
                    <div class ="divLeft" style ="xwidth:42%;width:0%;padding:0px; border:none;">
                    <input id="txtPApplicantDeclaredOtherIncomeSourch" type ="text" class="txtBoxSize" style="display:none;"/></div>
                    <div class ="divMiddile" style ="xwidth:55%;_xwidth:52%;width:100%;*width:98%; padding:0px; border:none;">
                    <input id="txtPApplicantDeclaredOtherIncomeAmount" type ="text" class="txtBoxSize"onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantDeclaredOtherIncomeAmount')"/></div>
                        
                    </div>
                
                </div>
                
                <div class="fieldDivContaint" style="height:160px;*height:160px;_height:160px; border:solid 1px black; margin-bottom:5px; ">

                    <div class ="divLeft" style =" margin-top:5px;width:42%;*width:42%;_width:42%;padding:0px; border:none;">
                    
                            <div class ="divLbl"><label class="lblSize">Rent & Utilities:</label></div>
                            <div class ="divLbl"><label class="lblSize">Food & Clothing:</label></div>
                            <div class ="divLbl"><label class="lblSize" >Education:</label></div>
                            <div class ="divLbl"><label class="lblSize">Loan Repayment:</label></div>
                            <div class ="divLbl"><label class="lblSize">Others:</label></div>
                    </div>

                   <div class ="divMiddile" style ="margin-top:1px; width :50%;*width :50%;_width:50%; padding:2px;">
                   
                            <div class="fieldDivContaint" >
                            <input id="txtPApplicantRentUtilities" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantRentUtilities')"/>
                            </div>
                            <div class="fieldDivContaint" >
                            <input id="txtPApplicantFoodClothing" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantFoodClothing')"/>
                            </div>
                            <div class="fieldDivContaint" >
                            <input id="txtPApplicantEducation" type="text"  class="txtBoxSize"onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantEducation')" />
                            </div>
                            <div class="fieldDivContaint" >
                            <input id="txtPApplicantLoanRepayment" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantLoanRepayment')"/>
                            </div>    
                            <div class="fieldDivContaint" >
                            <input id="txtPApplicantOthers" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtPApplicantOthers')"/>
                            </div>
                    
                    </div>
                
                
                </div>
                
                <div class="fieldDivContaint" >
                <input id="txtPApplicantRepaymentto" type="text"  class="txtBoxSize" />
                </div>
                <div class="fieldDivContaint">
                <input id="txtPApplicantRepaymentfor" type ="text" class="txtBoxSize"/>
                </div>
        </div>
        
        <div id ="JFinanceControlDiv" class ="divRight" style ="width:250px;*width:250px;_width:250px;">
                
                
                 <div class="fieldDivContaint" style ="margin-bottom:5px; height:25px; _height:25px;*height:25px;" >
                    <div style ="width:100%;">
                    <div class ="divLeft" style ="xwidth:42%;width:0%;padding:0px; border:none;">
                    <input id="txtJApplicantDeclaredPrimaryIncomeSourch" type ="text" class="txtBoxSize" style="display:none;"/></div>
                    <div class ="divMiddile" style ="xwidth:55%;_xwidth:52%;width:100%; *width:98%;   padding:0px; border:none;">
                    <input id="txtJApplicantDeclaredPrimaryIncomeAmount" type ="text" class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantDeclaredPrimaryIncomeAmount')" /></div>
                    
                        
                    </div>
                
                </div>
                <div class="fieldDivContaint" style ="margin-bottom:5px; height:25px; _height:25px;*height:25px;" >
                    <div style ="width:100%;">
                    <div class ="divLeft" style ="xwidth:42%;width:0%;padding:0px; border:none;">
                    <input id="txtJApplicantDeclaredOtherIncomeSourch" type ="text" class="txtBoxSize" style="display:none;"/></div>
                    <div class ="divMiddile" style ="xwidth:55%;_xwidth:52%;width:100%;width:98%; padding:0px; border:none;"><input id="txtJApplicantDeclaredOtherIncomeAmount" type ="text" class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantDeclaredOtherIncomeAmount')" /></div>
                        
                    </div>
                
                </div>
                
                <div class="fieldDivContaint" style="height:160px;*height:160px;_height:160px; border:solid 1px black; margin-bottom:5px; ">

                    <div class ="divLeft" style =" margin-top:5px;width:42%;*width:42%;_width:42%;padding:0px; border:none;">
                    
                            <div class ="divLbl"><label class="lblSize">Rent & Utilities:</label></div>
                            <div class ="divLbl"><label class="lblSize">Food & Clothing:</label></div>
                            <div class ="divLbl"><label class="lblSize" >Education:</label></div>
                            <div class ="divLbl"><label class="lblSize">Loan Repayment:</label></div>
                            <div class ="divLbl"><label class="lblSize">Others:</label></div>
                    </div>

                   <div class ="divMiddile" style ="margin-top:1px; width :50%;_width:50%;*width :50%; padding:2px;">
                   
                            <div class="fieldDivContaint" >
                            <input id="txtJApplicantRentUtilities" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantRentUtilities')" />
                            </div>
                            <div class="fieldDivContaint" >
                            <input id="txtJApplicantFoodClothing" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantFoodClothing')" />
                            </div>
                            <div class="fieldDivContaint" >
                            <input id="txtJApplicantEducation" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantEducation')"/>
                            </div>
                            <div class="fieldDivContaint" >
                            <input id="txtJApplicantLoanRepayment" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantLoanRepayment')"/>
                            </div>    
                            <div class="fieldDivContaint" >
                            <input id="txtJApplicantOthers" type="text"  class="txtBoxSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtJApplicantOthers')"/>
                            </div>
                    
                    </div>
                
                
                </div>
                
                <div class="fieldDivContaint" >
                <input id="txtJApplicantRepaymentto" type="text"  class="txtBoxSize" />
                </div>
                <div class="fieldDivContaint">
                <input id="txtJApplicantRepaymentfor" type ="text" class="txtBoxSize"/>
                </div>

                
                
        </div>
         

          <div class="clear"></div>
          
 <!-- ------------End of Financial Segment ------------------------------ -->           
   
            

 </div>
          <!-- ---------------------End of Accounts And Finance---------------------------------------------- -->






    
</div>




<div class="content" id="page-4">









        <!--  ---------------------------- Reference Tab 4    ----------------------- -->
            <div id="Div13" style=" width:98%; margin-left:5px;">
        <div id ="div36" class ="divHeaderLoan">References</div>
        
        <div class ="clear"></div>
        
        
        <div id ="div37" class ="divLeftSubHead" style="border:0; margin-left:2px;"></div>
        
        <div id ="div38" class ="divMiddileSubHead">Reference 1</div>
        
        <div id ="div39" class ="divRightSubHead">Reference 2</div>
        
        <div class="clear"></div>
        
        <div id ="div40" class ="divLeft">
            <div class ="divLbl"><label class="lblSize">Name:</label></div>
            <div class ="divLbl"><label class="lblSize">Relationship:</label></div>
            <div class ="divLbl"><label class="lblSize">Occupation:</label></div>
            <div class ="divLbl"><label class="lblSize">Name of the Organization:</label></div>
            <div class ="divLbl"><label class="lblSize">Designation:</label></div>
            <div class ="divLbl" style ="height:50px;*height:50px;_height:50px;"><label class="lblSize" >Work Address:</label></div>
            <div class ="divLbl" style ="height:50px;*height:50px;_height:50px;"><label class="lblSize">Residence Address:</label></div>
            <div class ="divLbl"><label class="lblSize">Phone Number:</label></div>
            <div class ="divLbl"><label class="lblSize">Mobile Number:</label></div>
            
        </div>
        
        <div id ="div41" class ="divMiddile">
                <div class="fieldDivContaint">
                <input type="text" id="txtReferenceName1" class="txtBoxSize" />
                </div>
                <div class="fieldDivContaint">
                <input id="txtReferenceRelationship1" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbReferenceOccupation1" class="comboSize"><option value="0">Select a source</option></select>
                </div>
                <div class="fieldDivContaint">
                <input id="txtReferenceNameOfTheOrganization1" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtReferenceDesignation1" type ="text" class="txtBoxSize"/>
                </div>
                
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtReferenceWorkAddress1" class ="txtAreaSize"></textarea>
                </div>
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtReferenceResidenceAddress1" class ="txtAreaSize"></textarea>
                </div>
               
                
                <div class="fieldDivContaint">
                <input id="txtReferencePhoneNumber1" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtReferenceMobileNumber1" type ="text" class="txtBoxSize"/>
                </div>
                
                
        </div>
        
        <div id ="div42" class ="divRight">
                <div class="fieldDivContaint">
                <input type="text" id="txtReferenceName2" class="txtBoxSize" />
                </div>
                <div class="fieldDivContaint">
                <input id="txtReferenceRelationship2" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <select id="cmbReferenceOccupation2" class="comboSize"><option value="-1">Select a Status</option>
                                                                                                              <option value="Married">Married</option>
                                                                                                              <option value="Single">Single</option>
                                                                                                              <option value="Separated">Separated</option>
                                                                                                              <option value="Others">Others</option>
                                                                                                                </select>
                </div>
                <div class="fieldDivContaint">
                <input id="txtReferenceNameOfTheOrganization2" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtReferenceDesignation2" type ="text" class="txtBoxSize"/>
                </div>
                
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtReferenceWorkAddress2" class ="txtAreaSize"></textarea>
                </div>
                <div class="fieldDivContaint"style="margin-bottom:5px; height:60px;*height:60px;_height:60px;">
                <textarea cols="1" rows="3" id="txtReferenceResidenceAddress2" class ="txtAreaSize"></textarea>
                </div>
               
                
                <div class="fieldDivContaint">
                <input id="txtReferencePhoneNumber2" type ="text" class="txtBoxSize"/>
                </div>
                <div class="fieldDivContaint">
                <input id="txtReferenceMobileNumber2" type ="text" class="txtBoxSize"/>
                </div>
                
        </div>
         

          <div class="clear"></div>
          
         
          <br />


<div id="Div43" class="mainDivOfTabContent" style="width:50px;">
   <table width="100%">
   <tr><td colspan="3" style="height:50px;"><div style="text-align:center; font-size:2em;_font-size:2em;font-weight:bold;">Facility & Security Schedule</div></td></tr>
        <tr>
            <td colspan="3"><div> 
                    <table>
                        <tr><td>
                                <div style="text-align:center; font-size:medium;font-weight:bold; width:870px; height:200px; overflow-y:auto;">
                                    <div style="border:none;">
                                        <table id="tlbFacility" >
                                               <tr id="Tr2" class="theader">
                                                   <td style="width:10%;">Facility Type</td>
                                                   <td style="width:10%;">Interest Rate</td>
                                                   <td style="width:10%;">Present Balance</td>
                                                   <td style="width:10%;">Present EMI</td>
                                                   <td style="width:10%;">Present Limit</td>
                                                   <td style="width:10%;">Proposed Limit</td>
                                                   <td style="width:10%;">Repayment Agreement</td>
                                                   <td style="width:10%;">Status</td>
                                                   <td style="width:10%;">Nature of Security</td>
                                                   <td style="width:10%;">Face Value (BDT)</td>
                                                   <td style="width:10%;">XTV Rate (BDT)</td>
                                                   <td style="width:10%;">Interest Rate</td>
                                               </tr>
                                               <tr>
                                                    <td>
                                                        <select id="cmbFacilityScheduleFacilityType1" >
                                                        </select>
                                                    </td>
                                                    <td><input id="txtFacilityScheduleInterestRate1" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleInterestRate1')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentBalance1" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentBalance1')"td>
                                                    <td><input id="txtFacilitySchedulePresentEMI1" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentEMI1')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentLimit1" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentLimit1')"/></td>
                                                    <td><input id="txtFacilityScheduleProposedLimit1" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleProposedLimit1')"/></td>
                                                    <td><input id="txtFacilityScheduleRepaymentAgreement1" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleRepaymentAgreement1')"/></td>
                                                    <td>
                                                        <select id="cmbStatusForSecurities1">
                                                            <option value="-1">Select a Status</option>
                                                            <option value="1">ACTIVE</option>
                                                            <option value="2">SETTLED</option>
                                                            <option value="3">TO BE CLOSED</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity1" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                     <td><input id="txtSecurityScheduleFaceValue1" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue1')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate1" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate1')"/></td>
                                                    <td><input id="txtSecurityScheduleInterestRate1" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate1')"/></td>
                                               </tr>
                                                <tr>
                                                    <td>
                                                        <select id="cmbFacilityScheduleFacilityType2" >
                                                        </select>
                                                    </td>
                                                    <td><input id="txtFacilityScheduleInterestRate2" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleInterestRate2')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentBalance2" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentBalance2')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentEMI2" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentEMI2')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentLimit2" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentLimit2')"/></td>
                                                    <td><input id="txtFacilityScheduleProposedLimit2" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleProposedLimit2')"/></td>
                                                    <td><input id="txtFacilityScheduleRepaymentAgreement2" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleRepaymentAgreement2')"/></td>
                                                    <td>
                                                        <select id="cmbStatusForSecurities2">
                                                            <option value="-1">Select a Status</option>
                                                            <option value="1">ACTIVE</option>
                                                            <option value="2">SETTLED</option>
                                                            <option value="3">TO BE CLOSED</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity2" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                     <td><input id="txtSecurityScheduleFaceValue2" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue2')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate2" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate2')"/></td>
                                                    <td><input id="txtSecurityScheduleInterestRate2" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate2')"/></td>
                                               </tr>
											   
											   <tr>
                                                    <td>
                                                        <select id="cmbFacilityScheduleFacilityType3" >
                                                        </select>
                                                    </td>
                                                    <td><input id="txtFacilityScheduleInterestRate3" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleInterestRate3')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentBalance3" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentBalance3')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentEMI3" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentEMI3')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentLimit3" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentLimit3')"/></td>
                                                    <td><input id="txtFacilityScheduleProposedLimit3" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleProposedLimit3')"/></td>
                                                    <td><input id="txtFacilityScheduleRepaymentAgreement3" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleRepaymentAgreement3')"/></td>
                                                    <td>
                                                        <select id="cmbStatusForSecurities3">
                                                            <option value="-1">Select a Status</option>
                                                            <option value="1">ACTIVE</option>
                                                            <option value="2">SETTLED</option>
                                                            <option value="3">TO BE CLOSED</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity3" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                     <td><input id="txtSecurityScheduleFaceValue3" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue3')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate3" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate3')"/></td>
                                                    <td><input id="txtSecurityScheduleInterestRate3" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate3')"/></td>
                                               </tr>
											   
											   <tr>
                                                    <td>
                                                        <select id="cmbFacilityScheduleFacilityType4" >
                                                        </select>
                                                    </td>
                                                    <td><input id="txtFacilityScheduleInterestRate4" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleInterestRate4')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentBalance4" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentBalance4')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentEMI4" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentEMI4')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentLimit4" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentLimit4')"/></td>
                                                    <td><input id="txtFacilityScheduleProposedLimit4" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleProposedLimit4')"/></td>
                                                    <td><input id="txtFacilityScheduleRepaymentAgreement4" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleRepaymentAgreement4')"/></td>
                                                    <td>
                                                        <select id="cmbStatusForSecurities4">
                                                            <option value="-1">Select a Status</option>
                                                            <option value="1">ACTIVE</option>
                                                            <option value="2">SETTLED</option>
                                                            <option value="3">TO BE CLOSED</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity4" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                     <td><input id="txtSecurityScheduleFaceValue4" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue4')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate4" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate4')"/></td>
                                                    <td><input id="txtSecurityScheduleInterestRate4" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate4')"/></td>
                                               </tr>
											   <tr>
                                                    <td>
                                                        <select id="cmbFacilityScheduleFacilityType5" >
                                                        </select>
                                                    </td>
                                                    <td><input id="txtFacilityScheduleInterestRate5" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleInterestRate5')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentBalance5" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentBalance5')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentEMI5" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentEMI5')"/></td>
                                                    <td><input id="txtFacilitySchedulePresentLimit5" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilitySchedulePresentLimit5')"/></td>
                                                    <td><input id="txtFacilityScheduleProposedLimit5" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleProposedLimit5')"/></td>
                                                    <td><input id="txtFacilityScheduleRepaymentAgreement5" type ="text" class ="textBixSize" onchange="LoanApplicationHealper.checkInputIsNumber('txtFacilityScheduleRepaymentAgreement5')"/></td>
                                                    <td>
                                                        <select id="cmbStatusForSecurities5">
                                                            <option value="-1">Select a Status</option>
                                                            <option value="1">ACTIVE</option>
                                                            <option value="2">SETTLED</option>
                                                            <option value="3">TO BE CLOSED</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity5" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                     <td><input id="txtSecurityScheduleFaceValue5" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue5')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate5" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate5')"/></td>
                                                    <td><input id="txtSecurityScheduleInterestRate5" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate5')"/></td>
                                               </tr>
                                               <tr style="background-color:Gray;">
                                                    <td><label><b>Total BDT </b></label></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                               </tr>

                                        </table>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        
                    </table>
                </div>
            
            </td>
        </tr>
   </table>
   </div>
   <br />
   <%--<div id="Div44" class="mainDivOfTabContent" style="width:50px;">
   <table width="100%">
   <tr><td colspan="3" style="height:50px;"><div style="text-align:center; font-size:2em;_font-size:2em;font-weight:bold;">Security Schedule</div></td></tr>
        <tr>
            <td colspan="3"><div> 
                    <table>
                        <tr><td>
                                <div style="text-align:center; font-size:medium;font-weight:bold;">
                                    <div style="border:none;">
                                        <table id="tlbSecurity" >
                                               <tr id="Tr3" class="theader">
                                                   <td style="width:10%;">Facility Type</td>
                                                   <td style="width:10%;">Nature of Security</td>
                                                   <td style="width:10%;">Issuing Office</td>
                                                   <td style="width:10%;">Face Value (BDT)</td>
                                                   <td style="width:10%;">XTV Rate (BDT)</td>
                                                   <td style="width:10%;">Issue Date</td>
                                                   <td style="width:10%;">Interest Rate</td>
                                                   
                                               </tr>
                                               
                                               <tr>
                                                     <td>
                                                        <select id="cmbfacilityTypeForSequrity1" ></select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity1" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                    <td><input id="txtSecurityScheduleIssuingOffice1" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleFaceValue1" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue1')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate1" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate1')"/></td>
                                                    <td><input id="txtSecurityScheduleIssueDate1" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleInterestRate1" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate1')"/></td>
                                               </tr>
											   <tr>
											        <td>
                                                        <select id="cmbfacilityTypeForSequrity2" ></select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity2" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                    <td><input id="txtSecurityScheduleIssuingOffice2" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleFaceValue2" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue2')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate2" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate2')"/></td>
                                                    <td><input id="txtSecurityScheduleIssueDate2" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleInterestRate2" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate2')"/></td>
                                               </tr>
											   <tr>
                                                    <td>
                                                        <select id="cmbfacilityTypeForSequrity3" ></select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity3" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                    <td><input id="txtSecurityScheduleIssuingOffice3" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleFaceValue3" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue3')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate3" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate3')"/></td>
                                                    <td><input id="txtSecurityScheduleIssueDate3" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleInterestRate3" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate3')"/></td>
                                               </tr>
											   <tr>
											        <td>
                                                        <select id="cmbfacilityTypeForSequrity4" ></select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity4" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                    <td><input id="txtSecurityScheduleIssuingOffice4" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleFaceValue4" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue4')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate4" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate4')"/></td>
                                                    <td><input id="txtSecurityScheduleIssueDate4" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleInterestRate4" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate4')"/></td>
                                               </tr>
											   <tr>
											        <td>
                                                        <select id="cmbfacilityTypeForSequrity5" ></select>
                                                    </td>
                                                    <td>
                                                        <select id="txtSecurityScheduleNatureOfSecurity5" style="width: 150px;" >
                                                        <option value="1">FD (SCB)</option>
                                                        <option value="2">Ezee FD</option>
                                                        <option value="3">MSS (SCB)</option>
                                                        <option value="4">CESS (SCB)</option>
                                                        <option value="5">RFCD</option>
                                                        <option value="6">WEDB-SCB</option>
                                                        <option value="7">WEDB-OTHER</option>
                                                        <option value="8">ICB Unit</option>
                                                        <option value="9">USD Bond DPB</option>
                                                        <option value="10">USD Bond DIB</option>
                                                        <option value="11">MORTGAGE SECURITY</option>
                                                        <option value="12">CAR REGISTRATION+INSURANCE</option>
                                                        </select>
                                                    </td>
                                                    <td><input id="txtSecurityScheduleIssuingOffice5" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleFaceValue5" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleFaceValue5')"/></td>
                                                    <td><input id="txtSecurityScheduleXTVRate5" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleXTVRate5')"/></td>
                                                    <td><input id="txtSecurityScheduleIssueDate5" type ="text" class ="textBixSize2" /></td>
                                                    <td><input id="txtSecurityScheduleInterestRate5" type ="text" class ="textBixSize2" onchange="LoanApplicationHealper.checkInputIsNumber('txtSecurityScheduleInterestRate5')"/></td>
                                               </tr>


                                        </table>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        
                    </table>
                </div>
            
            </td>
        </tr>
   </table>
   </div>--%>



 </div>
      <!--  ----------------------------End of Reference Tab 4    ----------------------- -->
  
</div>

<script src="../../Scripts/activatables.js" type="text/javascript"></script>
<script type="text/javascript">
    activatables('page', ['page-1', 'page-2', 'page-3', 'page-4']);
</script>
<br />

   <div class ="clear"></div>

<div class="btns" style="width:100%;text-align:center; background-color:#fff; height:50px; vertical-align:middle; ">

			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
			<a href="javascript:;" id="lnkPrint" class="iconlink iconlinkDocumentCheckList">Print</a>
</div>
</div>

 <div class="modal" id="newLLPopupDiv" style="display:none; height:160px;">
		<h3>Appeal</h3>
		<label for="txtpostedDate">LLID*</label>
		<input type="radio" name="group1" value="1" onclick="loanSummeryForSalesHelper.clickRadio(1);" checked> Same LLID
        <input type="radio" name="group1" value="2" onclick="loanSummeryForSalesHelper.clickRadio(2);"> New LLID<br>
        <div id="divLLID" style="display:none;">
        <label for="txtLLID">New LLID*</label> <input class="txt" id="txtNewLLID" title="New LLID"/><br/>
        <input type="hidden" id="hdnLLID" />
        <input type="hidden" id="hdnMasterId" />
        </div>
		
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Appeal</a>
			<a href="javascript:;" id="lnkClosePopup" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
 
 <div class="clear"></div>
     

</asp:Content>

