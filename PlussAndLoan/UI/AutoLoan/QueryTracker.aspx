﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.QueryTracker" Title="Query Tracker" Codebehind="QueryTracker.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    
    <style type="text/css">
        #nav_QueryTracker a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    
    <div id="divQueryTracker" class="divQueryTrackerContainer widthSize98_per">
          <script type="text/javascript" src="../../Scripts/AutoLoan/QueryTracker.js"></script>
            <div class="pageHeaderDiv ">Query Tracker</div>

             <div id="divLoanAppMasterData" class="fieldDiv divPadding formBackColor">
                <div class="fieldDiv">
                    <label class="lbl rightAlign">
                        LLID<span class="starColor">*</span>:</label>
                        <input type="text" id="txtLLID" class="txtField widthSize25_per" onkeypress="queryTrackerHelper.searchKeypress(event)"/>
                        <input type="button" id="btnLLIdSearch" class="" value="Search"/>
                        <span><label class="lbl rightAlign" style="margin-left: 55px;*margin-left: 40px; width: 100px;">
                        Application Date:</label></span>
                    <input type="text" id="txtApplicationDate" class="txtField widthSize25_per" disabled="disabled" />
                </div>
                <div class="fieldDiv">
                    <label class="lbl">
                        Applicant Name:</label>
                    <input type="text" id="txtApplicantName" class="txtField widthSize25_per" disabled="disabled" />
                    <label class="lbl">
                    </label>
                    <label class="lbl rightAlign">
                        Date of Birth:</label>
                    <input type="text" id="txtDateOfBirth" class="txtField widthSize25_per" disabled="disabled" />
                </div>
                <div class="fieldDiv">
                    <label class="lbl">
                        Applied Amount:</label>
                    <input type="text" id="txtAppliedAmount" class="txtField widthSize25_per" disabled="disabled" onchange="LoanApplicationHealper.checkInputIsNumber('txtAppliedAmount')"/>
                    <label class="lbl">
                    </label>
                    <label class="lbl rightAlign">
                        Asking Tenor:</label>
                    <input type="text" id="txtAskingTenor" class="txtField widthSize25_per" disabled="disabled" />
                </div>
                <div class="fieldDiv" id="divCurrentStatus" style="display:none;">
                <label class="lbl" style="font-weight:bold; font-size:13px;">
                        Current Status:</label>
                        <span id="spnCurrentStatus" style="font-weight:bold; font-size:14px;"></span>
                </div>
                
            </div>
         
             <div class="divLeftNormalClass widthSize56_per">
             
            <div class="SearchwithPager">
            
            <div class="fieldDiv">
                    <label class="lbl" style="float: left;">
                        <h1><u>Query Details</u></h1></label>
                         
                    <span class="rightAlign" style="margin-right: 0;padding: 0;text-align: right; float: right;">
                        <input type="button" id="btnAddNewQuery" class="searchButton iconlinkAddNew iconlinkBlack widthSize100_per" value="Post New Query" onclick="javascript:;" style="display: none;"/>
                        <%--<div id="pagerForStatus" class="pager pagerFooter" > 
             <label for="txtPageNOStatus" class="pagerlabel">page</label>                                           
             <input id="txtPageNOStatus" class="txtPageNo" value="1" maxlength="7"></input>
             <span id="SpnTotalPageStatus" class="spntotalPage">of 1</span> <a id="A1"
                    title="Previous Page" href="#" class="prevPage">prev</a> <a id="A2" title="Next Page"
                        href="#" class="nextPage">next</a>
            </div>--%>
                    </span>
                    
                </div>
           
         </div>
         <div class="clear"></div>
            <table  cellpadding="0" cellspacing="0" frame="box" border="0" class="summary centerAlign">
				        <colgroup class="sortable ">
					        <col width="30px" />
					        <col width="150px" />
					        <col width="200px" />
					        <col width="50px" />
        					
				        </colgroup>
				        <thead >
					        <tr class="theadTdTextAlignCenter">
						        <th>
							        Date
						        </th>
						        <th>
						            User & Departmetn
						        </th>
						        <th>
						            Queries
						        </th>
        											
						        <th> #
						        </th>
					        </tr>
				        </thead>
				        <tbody id="GridQueryDetails">
				        </tbody>
			        </table>
			        <div class="clear"></div>
			<div id="Pager" class="pager pagerFooter" > 
                            <label for="txtPageNO" class="pagerlabel">page</label>                                           
                            <input id="txtPageNo" class="txtPageNo" value="1" maxlength="7"></input>
                            <span id="spntotalPage" class="spntotalPage">of 1</span> 
                            <a id="lnkPrev" title="Previous Page" href="#" class="prevPage">prev</a> 
                            <a id="lnkNext" title="Next Page" href="#" class="nextPage">next</a>
                    </div>
            <div class="clear"></div>
             </div>
                
            <div class="divRightBigNormalClass widthSize35_per margineRight_0px">
             
        <div class="SearchwithPager">
            
            <div class="fieldDiv  centerAlign">
                    
                        <h1><u>Application History</u></h1>
                </div>
          
            </div>
         
         <table  cellpadding="0" cellspacing="0" frame="box" border="0" class="summary centerAlign">
				        <colgroup class="sortable ">
					        <col width="50px" />
					        <col width="300px" />
        					
        					
				        </colgroup>
				        <thead >
					        <tr class="theadTdTextAlignCenter">
						        <th>
							        Date
						        </th>
						        <th>
						            Status Name(Department Name)
						        </th>
					        </tr>
				        </thead>
				        <tbody id="GridApplicationStatus">
				        </tbody>
			        </table>
			        <div class="clear"></div>
			         <%--<div id="pagerForStatus" class="pager pagerFooter" > 
             <label for="txtPageNOStatus" class="pagerlabel">page</label>                                           
             <input id="txtPageNOStatus" class="txtPageNo" value="1" maxlength="7"></input>
             <span id="SpnTotalPageStatus" class="spntotalPage">of 1</span> <a id="A1"
                    title="Previous Page" href="#" class="prevPage">prev</a> <a id="A2" title="Next Page"
                        href="#" class="nextPage">next</a>
            </div>--%>
         </div>

            <div class="modal" id="newQueryPopupDiv" style="height:310px;width: 435px; display:none;">
		        <h3>New Query Entry</h3>
		        <div class="fieldDiv">
                    <label class="lblquaryTracker">
                        LLID:</label>
                    <input type="text" id="txtNewQueryLLID" class="txtField widthSize32_per" disabled="disabled" />
                    <label class="lbl lblquaryTracker widthSize1_per">
                    </label>
                    <label class="lbl lblquaryTracker rightAlign" style="margin-left:12px;">
                        Posted By:</label>
                    <input type="text" id="txtPostedBy" class="txtField widthSize33_per" disabled="disabled" />
                </div>
                <div class="fieldDiv">
                    <label class="lblquaryTracker widthSize30_per">
                        Comments/Remarks:</label>
                    
                    <label class="lbl lblquaryTracker widthSize14_per rightAlign" style="margin-left:83px;" >
                        Query To:</label>
                        <select id="cmbQueryTo" class="comboSizeH_22 widthSize34_per">
                            <option value="-1">Select</option>
                        </select>
                </div>
                
                <div class="fieldDiv">
                    <textarea id="txtNewQuery" class="txtAreaNewQuerySize widthSize96_per" rows="10" cols="10"></textarea>
                </div>
        		 			
		        <div class="btns">
			        <a href="javascript:;" id="btnPostNewQuery" class="iconlink iconlinkSave">Post</a>
			        <a href="javascript:;" id="btnClose" class="iconlink iconlinkClose">Cancel</a>
		        </div>
            </div>
         
            <%--<input type="button" id="lnkReplay" class="searchButton iconlinkAddNew iconlinkBlack widthSize38_per" value="Replay" onclick="javascript:;"/>--%>
            <div class="modal" id="divQueryReplayPopup" style="height:380px;width: 600px; display:none;">
		<h3>Replay on Query </h3>
		<div class="fieldDiv">
            <label class="lblquaryTracker">
                LLID:</label>
            <input type="text" id="txtReplyOnLLID" class="txtField widthSize15_per" disabled="disabled" />
            <label class="lbl lblquaryTracker rightAlign">
                Queried By:</label>
            <input type="text" id="txtQueriedBy" class="txtField widthSize25_per" disabled="disabled" />
            <label class="lbl lblquaryTracker rightAlign">
                Reply By:</label>
            <input type="text" id="txtReply_ReplyBy" class="txtField widthSize25_per" disabled="disabled" />
        </div>
        <div class="fieldDiv">
            <label class="lblquaryTracker" style="vertical-align: top;">
                Query Details:</label>
                <textarea id="txtQueryDetails" class="txtAreaHight_55px widthSize81_per" cols="5" rows="5" disabled="disabled"></textarea>
        </div>
        
        <div class="fieldDiv">
            <label class="lblquaryTracker widthSize30_per">
                Replay Details:</label>
        </div>
        <div class="fieldDiv">
            <textarea id="txtReplayQueryDetails" class="txtAreaNewQuerySize widthSize96_per" cols="8" rows="8" ></textarea>
        </div>
		 			
		<div class="btns">
			<a href="javascript:;" id="btnReplay" class="iconlink iconlinkSave">Replay</a>
			<a href="javascript:;" id="btnCloseReplay" class="iconlink iconlinkClose">Cancel</a>
		</div>
    </div>
    </div>
    
</asp:Content>

