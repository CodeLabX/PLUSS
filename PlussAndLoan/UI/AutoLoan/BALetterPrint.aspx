﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.BALetterPrint" Codebehind="BALetterPrint.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BA Letter Print</title>
    <link rel="stylesheet" type="text/css" href="../../CSS/TableLayoutForTabControls.css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 60px;
        }
        .style3
        {
        }
        .style4
        {
            width: 205px;
        }
        .style5
        {
            width: 179px;
        }
        .style7
        {
            font-weight: bold;
            font-size: large;
            height: 13px;
        }
        .style8
        {
            width: 100%;
            padding: 0;
            font-size: x-small;
        }
        .style11
        {
            text-align: center;
        }
        table.reference
        {
            background-color: #FFFFFF;
            border: 1px solid #C3C3C3;
            border-collapse: collapse;
            width: 100%;
        }
        table.reference th
        {
            background-color: #E5EECC;
            border: 1px solid #C3C3C3;
            padding-left: 5px;
            vertical-align: top;
        }
        table.reference td
        {
            border: 1px solid #C3C3C3;
            padding: 3px;
            vertical-align: top;
            font-size: x-small;
        }
        table.referenceBoirderNull
        {
            background-color: #FFFFFF;
            border-collapse: collapse;
            width: 100%;
        }
        table.referenceBoirderNull th
        {
            background-color: #E5EECC;
            padding-left: 5px;
            vertical-align: top;
        }
        table.referenceBoirderNull td
        {
            padding: 3px;
            vertical-align: top;
            font-size: 12px;
        }
        .style21
        {
            width: 12px;
        }
        .style32
        {
            font-weight: bold;
            height: 10px;
        }
        .style35
        {
        }
        .style37
        {
            width: 4px;
        }
        .style40
        {
            text-align: center;
            font-weight: bold;
        }
        .style41
        {
            width: 511px;
        }
        .style42
        {
            width: 119px;
        }
        .style43
        {
            width: 119px;
            font-weight: bold;
        }
        .style44
        {
            width: 50px;
            font-weight: bold;
        }
        .style45
        {
            text-align: center;
            font-weight: bold;
            font-size: small;
            text-decoration: underline;
            font-family: Arial;
        }
        p.MsoNormal
        {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 10.0pt;
            margin-left: 0in;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri" , "sans-serif";
        }
        .style68
        {
            width: 100%;
            border-color: #808080;
        }
        .style69
        {
            font-size: larger;
            font-weight: normal;
        }
        .style71
        {
            height: 33px;
            font-weight: bold;
        }
        .style73
        {
            height: 7px;
        }
        .style74
        {
            width: 15px;
        }
        .style75
        {
            width: 12px;
            height: 25px;
        }
        .style76
        {
            font-weight: bold;
            font-size: 13px;
            height: 25px;
        }
        .style77
        {
            height: 25px;
        }
        .style78
        {
        }
        .style83
        {
            width: 78px;
        }
        .style84
        {
        }
        .style85
        {
            width: 12px;
            height: 6px;
        }
        .style86
        {
            font-weight: bold;
            font-size: 5px;
            height: 2px;
        }
        .style87
        {
            height: 7px;
            text-align: justify;
        }
        .style88
        {
            width: 114px;
        }
        .style90
        {
            width: 80px;
            font-weight: bold;
        }
        .style91
        {
            width: 80px;
        }
        .style97
        {
            width: 190px;
        }
        .style98
        {
            width: 79px;
        }
        .style99
        {
            width: 5px;
        }
        .style100
        {
            width: 195px;
        }
        .style104
        {
            height: 10px;
        }
        .style105
        {
            font-size: small;
        }
        .style106
        {
            font-size: larger;
        }
        .style107
        {
            font-weight: bold;
            height: 10px;
            font-size: larger;
        }
        .style108
        {
            width: 58px;
        }
        .style109
        {
            width: 325px;
        }
        .style110
        {
            height: 17px;
            font-weight: bold;
        }
    </style>

    <script type="text/javascript">

    function printPreview() {
      document.getElementById("actionButton").style.display = 'none';
    window.print();

    document.getElementById("actionButton").style.display = 'block';
  }

    </script>

</head>
<body style="font-size: x-small; margin: 0; padding: 0; border: 0;">
    <form id="form1" runat="server">
    <div>
        <div id="divPrintBALetterPage1" class="A4Page" style="xpadding-top: .45in; padding-left: .45in"
            runat="server">
            <table class="style1">
                <tr>
                    <td class="style108">
                        &nbsp;
                    </td>
                    <td class="style109">
                    </td>
                    <td class="style3" rowspan="3">
                        <table class="style1" style="line-height: 30px">
                            <tr>
                                <td class="style7 rightAlign">
                                    Standard
                                    <br />
                                    Chartered
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td rowspan="5">
                        <asp:Image ID="Image1" runat="server" Height="66px" ImageUrl="~/Images/scb_logo.jpg"
                            Width="90%" CssClass="floatLeft" />
                    </td>
                </tr>
                <tr>
                    <td class="style108">
                        &nbsp;
                    </td>
                    <td class="style109">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style108">
                        &nbsp;
                    </td>
                    <td class="style109">
                        &nbsp;
                        <asp:Label ID="reportType" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
            <table class="style8">
                <tr>
                    <td class="style35" colspan="3">
                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style91">
                        &nbsp;
                    </td>
                    <td class="style11">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style44">
                        Branch
                    </td>
                    <td class="style37">
                        :
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblBranch" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style90">
                        A/C No
                    </td>
                    <td class="style40">
                        :
                    </td>
                    <td class="rightAlign">
                        <asp:Label ID="lblACNo" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style44">
                        Date
                    </td>
                    <td class="style37">
                        :
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style91">
                        &nbsp;
                    </td>
                    <td class="style11">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <table class="style8 referenceBoirderNull" style="line-height: 13px">
                <tr>
                    <td class="style45">
                        BANKING ARRANGEMENT LETTER
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" class="style105">
                        Dear Sir/Madam,
                        <br />
                        We are pleased to advise that the following facillity has been granted to you on
                        the basis of your application dated&nbsp;&nbsp;
                        <asp:Label ID="lblApplicationDate" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class=" reference">
                <tr bgcolor="Gray">
                    <td bgcolor="Gray" colspan="4" class="style106">
                        <b>FACILITY DETAILS</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chkStaffAuto" runat="server" Font-Bold="True" Text="Staff Auto"
                            TextAlign="Left" /><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>
                        <asp:CheckBox ID="chkConventionalAuto" runat="server" CssClass="style32" Text="Conventional Auto"
                            TextAlign="Left" /><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>
                        <asp:CheckBox ID="chkSaadiqAuto" runat="server" CssClass="style32" Text="Saadiq Auto"
                            TextAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td class="style107">
                        LOAN AMOUNT
                    </td>
                    <td class="style107">
                        <asp:Label ID="Label12" runat="server" Text="BDT"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblLoanAmount" runat="server"></asp:Label>
                    </td>
                    <td class="style107">
                        INTEREST RATE
                    </td>
                    <td class="style106">
                        <asp:Label ID="lblInterestRate" runat="server" Text=""
                            CssClass="floatLeft" Font-Bold="True"></asp:Label>
                        <asp:Label ID="Label17" runat="server" Text="% p.a" CssClass="floatRight" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style107">
                        EMI AMOUNT
                    </td>
                    <td class="style107">
                        <asp:Label ID="Label38" runat="server" Text="BDT"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblEMIAmount" runat="server"></asp:Label>
                    </td>
                    <td class="style107">
                        NO. OF INSTALLMENTS
                    </td>
                    <td class="style107">
                        <asp:Label ID="lblNoOfInstallemt" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style107">
                        TENOR
                    </td>
                    <td class="style107">
                        <asp:Label ID="lblTenor" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style107">
                        EXPIRY DATE
                    </td>
                    <td class="style107">
                        <asp:Label ID="lblExpiryDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style107">
                        BRAND
                    </td>
                    <td class="style107">
                        <asp:Label ID="lblBrand" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style107">
                        MODEL
                    </td>
                    <td class="style107">
                        <asp:Label ID="lblModel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style107">
                        PROCESSING FEE
                    </td>
                    <td class="style107">
                        <asp:Label ID="Label39" runat="server" Text="BDT"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblProcessingFee" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style69" colspan="2">
                        Note: Last EMI amount may vary from the regular EMI amount
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class="style8 referenceBoirderNull" style="line-height: 14px">
                <tr>
                    <td class="style71" colspan="2" style="font-size: small; font-family: AriaL;">
                        DOCUMENTATION:
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td class="style104" style="line-height: 2px">
                        DP Note &amp; Letter of Continuation for BDT&nbsp;&nbsp;
                        <asp:Label ID="lblDPNoteLetter" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Letter of Lien and Set-off over Deposit A/C No
                        <asp:Label ID="lblDPNoteLetter0" runat="server" Text=""></asp:Label>
                        &nbsp;for BDT
                        <asp:Label ID="lblDPNoteLetter1" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Post-dated Cheques from
                        <asp:Label ID="lblDPNoteLetter2" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Undated Cheque
                        <asp:Label ID="lblDPNoteLetter3" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Letter of Hypothecation&nbsp;&nbsp;
                        <asp:Label ID="lblLetterOfHypothecate" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Letter of Authority to Sell Hypothecate Prorerty(Notarized)
                        <asp:Label ID="lblDPNoteLetter5" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Letter of Authorisation for Encashment of Securities&nbsp;
                        <asp:Label ID="lblDPNoteLetter6" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Ownership Transfer From
                        <asp:Label ID="lblDPNoteLetter7" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Security(ies):&nbsp;
                        <asp:Label ID="lblSecurity" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td style="line-height: 1px">
                        Other(s)&nbsp;
                        <asp:Label ID="lblDPNoteLetter9" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style73">
                    </td>
                    <td class="style87">
                        Your borrowing is to be secured by the following securities in the manner prescribed
                        hereinafter together with any other security which now or hereinafter may be held
                        with the Bank, all of which securities shall be available to it as security for
                        all liabilities of the borrower(s) at any time. All clauses, terms, conditions coinvenience
                        etc expressed in those securities will apply unless expressly modefied or varied
                        therein.
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class="style8 referenceBoirderNull">
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style74">
                        1.
                    </td>
                    <td>
                        DP note convering the value of entire loan
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style74">
                        2.
                    </td>
                    <td>
                        Securities mentioned in the Memorandum of Deposit
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style74">
                        3.
                    </td>
                    <td style="line-height: 2px">
                        Hypothecation over Vehicle
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class="style8 referenceBoirderNull">
                <tr>
                    <td class="style85">
                    </td>
                    <td class="style86" colspan="8" style="border-bottom-style: solid; border-bottom-width: 2px;
                        border-bottom-color: #333333">
                    </td>
                </tr>
                <tr>
                    <td class="style75">
                    </td>
                    <td class="style76" colspan="4">
                        Debit Authority
                    </td>
                    <td class="style77" colspan="4">
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style78" colspan="4">
                        In consideration fo granting me/us Auto Loan Facility, Loan A/C no:
                    </td>
                    <td colspan="4">
                        <asp:Label ID="lblLoanAccNo" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style78" colspan="4">
                        I/we hareby authorise the Bank to debit my/our account no
                    </td>
                    <td colspan="4">
                        <asp:Label ID="lblDebitAuthorityAcc" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td colspan="8" style="line-height: 12px">
                        for an amount not exceeding BDT&nbsp;&nbsp;<asp:Label ID="Label40" runat="server"
                            Text=""></asp:Label>&nbsp;&nbsp;
                        <asp:Label ID="Label41" runat="server" Text=""></asp:Label>
                        &nbsp; in&nbsp;
                        <asp:Label ID="Label42" runat="server" Text=""></asp:Label>
                        &nbsp; consecutive monthly installments as on&nbsp;
                        <asp:Label ID="Label43" runat="server" Text=""></asp:Label>
                        &nbsp;day or following working day of each month commencing from the month&nbsp;
                        of
                        <asp:Label ID="lblDPNoteLetter12" runat="server" Text=""></asp:Label>
                        &nbsp;,
                        <asp:Label ID="lblDPNoteLetter13" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style21" style="line-height: 12px">
                        &nbsp;
                    </td>
                    <td class="style99">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="2">
                        month
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; year
                    </td>
                    <td class="style78">
                        &nbsp;
                    </td>
                    <td class="style78">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class="style8 referenceBoirderNull">
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style88">
                        Signature<br />
                        <br />
                        (Primary Applicant)
                    </td>
                    <td class="style97">
                        <div style="border: 1px solid #000000; height: 50px;" class="widthSize80_per">
                        </div>
                    </td>
                    <td class="style98">
                        Signature<br />
                        <br />
                        (Co-Applicant)
                    </td>
                    <td>
                        <div style="border: 1px solid #000000; height: 50px;" class="widthSize99_per">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style88">
                        Date
                    </td>
                    <td class="style97">
                        <div style="border: 1px solid #000000; height: 20px;" class="widthSize80_per">
                        </div>
                    </td>
                    <td class="style98">
                        Date
                    </td>
                    <td>
                        <div style="border: 1px solid #000000; height: 20px;" class="widthSize99_per">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style84" colspan="4" style="border-bottom-style: solid; border-bottom-width: 2px;
                        border-bottom-color: #333333">
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
        </div>
        <br />
        <br />
        <div id="divBALetterPage2" class="A4Page" style="padding-top: .45in; padding-left: .45in"
            runat="server">
            <table class="style1">
                <tr>
                    <td class="style5">
                        &nbsp;
                    </td>
                    <td class="style4">
                        &nbsp;
                    </td>
                    <td class="style3" rowspan="3">
                        <table class="style1">
                            <tr>
                                <td class="style7 rightAlign">
                                    Standard Chartered
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td rowspan="5">
                        <asp:Image ID="Image2" runat="server" Height="66px" ImageUrl="~/Images/scb_logo.jpg"
                            Width="90%" CssClass="floatLeft" />
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        &nbsp;
                    </td>
                    <td class="style4">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        &nbsp;
                    </td>
                    <td class="style4">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table class="style8">
                <tr>
                    <td class="style35" colspan="3">
                        <asp:Label ID="Label46" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style42">
                        &nbsp;
                    </td>
                    <td class="style11">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style44">
                        Branch
                    </td>
                    <td class="style37">
                        :
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblBranch0" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style43">
                        A/C No
                    </td>
                    <td class="style40">
                        :
                    </td>
                    <td class="rightAlign">
                        <asp:Label ID="lblACNo0" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style44">
                        Date
                    </td>
                    <td class="style37">
                        :
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblDate0" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style42">
                        &nbsp;
                    </td>
                    <td class="style11">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <table class="style8 referenceBoirderNull">
                <tr>
                    <td class="style45">
                        BANKING ARRANGEMENT LETTER
                    </td>
                </tr>
                <tr>
                    <td>
                        Dear Sir / Madam,<br />
                        We are pleased to advise that the following facillity has been granted to you on
                        the basis of your application dated&nbsp;&nbsp;
                        <asp:Label ID="lblApplicationDate0" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class=" reference style68">
                <tr bgcolor="Gray">
                    <td bgcolor="Gray" colspan="4">
                        <b>FACILITY DETAILS</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp; <b></b>
                        <asp:CheckBox ID="chkStaffAuto0" runat="server" Font-Bold="True" Text="Staff Auto"
                            TextAlign="Left" /><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>
                        <asp:CheckBox ID="chkConventionalAuto0" runat="server" CssClass="style32" Text="Conventional Auto"
                            TextAlign="Left" /><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>
                        <asp:CheckBox ID="chkSaadiqAuto0" runat="server" CssClass="style32" Text="Saadiq Auto"
                            TextAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        LOAN AMOUNT
                    </td>
                    <td class="style32">
                        <asp:Label ID="Label9" runat="server" Text="BDT"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblLoanAmount0" runat="server"></asp:Label>
                    </td>
                    <td class="style32">
                        INTEREST RATE
                    </td>
                    <td>
                        <asp:Label ID="lblInterestRate0" runat="server" Text=""
                            CssClass="floatLeft" Font-Bold="True"></asp:Label>
                        <asp:Label ID="Label11" runat="server" Text="% p.a" CssClass="floatRight" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        EMI AMOUNT
                    </td>
                    <td class="style32">
                        <asp:Label ID="Label13" runat="server" Text="BDT"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblEMIAmount0" runat="server"></asp:Label>
                    </td>
                    <td class="style32">
                        NO. OF INSTALLMENTS
                    </td>
                    <td class="style32">
                        <asp:Label ID="lblNoOfInstallemt0" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        TENOR
                    </td>
                    <td class="style32">
                        <asp:Label ID="lblTenor0" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style32">
                        EXPIRY DATE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lblExpiryDate0" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        BRAND
                    </td>
                    <td class="style32">
                        <asp:Label ID="lblBrand0" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style32">
                        MODEL
                    </td>
                    <td class="style32">
                        <asp:Label ID="lblModel0" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        PROCESSING FEE
                    </td>
                    <td class="style32" colspan="3">
                        <asp:Label ID="Label16" runat="server" Text="BDT"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblProcessingFee0" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class="style8 referenceBoirderNull">
                <tr>
                    <td class="style110" colspan="2" style="font-size: small; font-family: AriaL;">
                        DOCUMENTATION:
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        DP Note &amp; Letter of Continuation for BDT&nbsp;&nbsp;
                        <asp:Label ID="lblDPNoteLetter_0" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Memorandum of Deposit over&nbsp;
                        <asp:Label ID="Label44" runat="server" Text=""></asp:Label>
                        &nbsp;for BDT
                        <asp:Label ID="Label45" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Letter of Lien and Set-off over Deposit A/C No
                        <asp:Label ID="Label19" runat="server" Text=""></asp:Label>
                        &nbsp;for BDT
                        <asp:Label ID="Label20" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Post-dated Cheques from
                        <asp:Label ID="Label21" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Undated Cheque
                        <asp:Label ID="Label22" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Letter of Hypothecation&nbsp;&nbsp;
                        <asp:Label ID="lblLetterOfHypothecate0" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Letter of Authority to Sell Hypothecate Prorerty(Notarized)
                        <asp:Label ID="Label24" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Letter of Authorisation for Encashment of Securities&nbsp;
                        <asp:Label ID="Label25" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Ownership Transfer From
                        <asp:Label ID="Label26" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Security(ies):&nbsp;
                        <asp:Label ID="lblSecurity0" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style32">
                        <img alt="" src="../Images/box.PNG" style="height: 8px; width: 8px" />
                    </td>
                    <td>
                        Other(s)&nbsp;
                        <asp:Label ID="Label28" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style73">
                    </td>
                    <td class="style87">
                        Your borrowing is to be secured by the following securities in the manner prescribed
                        hereinafter together with any other security which now or hereinafter may be held
                        with the Bank, all of which securities shall be available to it as security for
                        all liabilities of the borrower(s) at any time. All clauses, terms, conditions coinvenience
                        etc expressed in those securities will apply unless expressly modefied or varied
                        therein.
                    </td>
                </tr>
            </table>
            <table cellpadding="4" class="style8 referenceBoirderNull">
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style74">
                        1.
                    </td>
                    <td>
                        DP note convering the value of entire loan
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style74">
                        2.
                    </td>
                    <td>
                        Securities mentioned in the Memorandum of Deposit
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style74">
                        3.
                    </td>
                    <td>
                        Hypothecation over Car Vehicle
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class="referenceBoirderNull">
                <tr>
                    <td class="style85">
                    </td>
                    <td class="style86" colspan="2" style="border-bottom-style: solid; border-bottom-width: 2px;
                        border-bottom-color: #333333; line-height: -4px;">
                    </td>
                </tr>
                <tr>
                    <td class="style75">
                    </td>
                    <td class="style76">
                        Debit Authority
                    </td>
                    <td class="style77">
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style78">
                        In consideration fo granting me/us Auto Loan Facility, Loan A/C no:
                    </td>
                    <td>
                        <asp:Label ID="lblLoanAccNo0" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style78">
                        I/we hareby authorise the Bank to debit my/our account no
                    </td>
                    <td>
                        <asp:Label ID="lblDebitAuthorityAcc0" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td colspan="2">
                        for an amount not exceeding BDT&nbsp;&nbsp;<asp:Label ID="Label31" runat="server"
                            Text=""></asp:Label>&nbsp;&nbsp;
                        <asp:Label ID="Label32" runat="server" Text=""></asp:Label>
                        &nbsp; in&nbsp;
                        <asp:Label ID="Label33" runat="server" Text=""></asp:Label>
                        &nbsp; consecutive monthly installments as on&nbsp;
                        <asp:Label ID="Label34" runat="server" Text=""></asp:Label>
                        &nbsp;day or following working day of each month commencing from the month&nbsp;
                        of
                        <asp:Label ID="Label35" runat="server" Text=""></asp:Label>
                        &nbsp;,
                        <asp:Label ID="Label36" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style78" colspan="2">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;month&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        year
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="4" class="style8 referenceBoirderNull">
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style88">
                        Signature<br />
                        (Primary Applicant)
                    </td>
                    <td class="style100">
                        <div style="border: 1px solid #000000; height: 33px;" class="widthSize80_per">
                        </div>
                    </td>
                    <td class="style83">
                        Signature<br />
                        (Co-Applicant)
                    </td>
                    <td>
                        <div style="border: 1px solid #000000; height: 35px;" class="widthSize99_per">
                        </div>
                    </td>
                </tr>
                <tr style="padding: 3px 0px 0px 0px; line-height: 12px">
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style88">
                        Date
                    </td>
                    <td class="style100">
                        <div style="border: 1px solid #000000; height: 18px;" class="widthSize80_per">
                        </div>
                    </td>
                    <td class="style83">
                        Date
                    </td>
                    <td>
                        <div style="border: 1px solid #000000; height: 18px;" class="widthSize99_per">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="style21">
                        &nbsp;
                    </td>
                    <td class="style84" colspan="4" style="border-bottom-style: solid; border-bottom-width: 2px;
                        border-bottom-color: #333333">
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <div id="divBALetterPage3" style="margin-top: .45in;" runat="server">
            <table cellpadding="4" class="referenceBoirderNull">
                <tr>
                    <td style="padding: 3%; border: 2px solid #000000; font-family: Verdana; font-size: 9px;
                        text-align: justify">
                        Any tax/fees/levy imposed by the Government and/or appropriate authorities from
                        time to time either on the principal or on the interest portion of Sanchaya Patras
                        and/or any other Government securities shall be in the account of the customer.
                        The customer, in this connection, will ensure timely payment of such tax/fees/levy
                        and indemnify the Bank against all possible losses/penalties/expenses incurred there
                        from due to his/her failure in doing so. The Bank will not take any responsibility
                        for monitoring the maturity dates of a customer&#39;s securities &amp; as such will
                        not be responsible for any interest loss as a result of the customer&#39;s failure
                        to renew such securities upon maturity.<br />
                        <br />
                        The above facility is granted in accordance with the terms and conditions contained
                        in the Auto Loan Application signed by you. While the above facility is subject
                        to review on or before the date of expiry specified above, it is at all time available
                        solely at the Bank&#39;s discretion and is, therefore, subject to repayment on demand.<br />
                        <br />
                        For pre-payment or cancellation of the facility ahead of agreed terms, a settlement
                        fee as decided by Bank will be charged. For delayed payment, a penal interest at
                        24% will be charged. Any legal fees and other costs incurred by the Bank in connection
                        with the facilities will be due to your account.<br />
                        <br />
                        Interest rate mentioned in this letter is floating and Bank shall have the discretion
                        to change and determine the interest rate from time to time. Due to change of the
                        interest rate, the Bank shall also have the right to change the EMI amount as well
                        as the initial loan tenor. You hereby acknowledge such right of the Bank and give
                        your consent unconditionally to the Bank to make such changes.<br />
                        <br />
                        Interest on the facility shall accrue at the rate mentioned in this letter or at
                        such other rate as determined by the Bank from time to time at its sole discretion.
                        Any repayment, whether in part or full, of facility will be attributable first to
                        interest which has accrued on the facility and then to principal. The rate of interest
                        determined by the Bank from time to time shall remain in full force and effect as
                        if the facilities granted to the borrower were still in force, even though the account/accounts
                        in the customer&#39;s name with the bank are closed, become dormant or are subject
                        to litigation until full settlement of alt the customer&#39;s liabilities to the
                        bank.<br />
                        <br />
                        The Bank may cancel the facility and demand immediate repayment of the loan by a
                        letter posted to your address recorded with the Bank if any of your Financial Indebtedness
                        (a) is not paid when due or within any originally applicable grace period and the
                        same is declared to be or otherwise becomes due and payable or is placed on demand
                        prior to its specified maturity; (b) becomes due and payable prior to its specified
                        maturity following an event of default or any provision having a similar effect
                        (however described). For the purpose of this clause &quot;Financial Indebtedness&quot;
                        means any indebtedness in respect of any credit facilities, financial assistance,
                        banking accommodation or borrowing availed or enjoyed by you or from any financial
                        institution including but is not limited to the Bank; or any guarantee or indemnity
                        given by you to any financial institution including but is not limited to the Bank
                        in relation to any service, performance of any obligation, credit facilities, financial
                        assistance, banking accommodation or borrowing (whether actual or contingent) of
                        any third party.<br />
                        <br />
                        Without prejudice to the conditions set out in preceding paragraph, the Bank may
                        cancel the facility at its absolute discretion &amp; demand immediate repayment
                        of the loan by a letter posted to your address recorded with the Bank. Bank shall
                        have a first priority fixed charge over the vehicle by way of hypothecation and
                        you shall take all measures to register the name of the Bank with the registration
                        authority and record name of the Bank in the Blue Book of the Car. In the event
                        of your failure to perform any obligation under this letter r security granted by
                        you, Bank may sell the car as your constituted attorney appointed through the notarized
                        irrevocable letter of authority you have granted to the Bank.
                        <br />
                        <br />
                        You hereby authorize the Bank, for ease of monitoring and accounting, to route all
                        payments of the loan through a special account which will be opened in your name
                        in the Bank&#39;s book. Furthermore, you hereby authorize the Bank to re-fix the
                        quantum of monthly installments at it&#39;s sole discretion. You understand and
                        agree that the aggregate of any debit balance in the said special account and your
                        Auto loan account is the total outstanding on account of the Auto Loan scheme and
                        you will always remain liable to repay this outstanding and any interest and other
                        charges thereon.<br />
                        <br />
                        By accepting this offer, you hereby authorise the Bank to disclose the information
                        of your loan to any regulatory authority, any office in the Bank, any assignee of
                        the Bank, agent of the Bank or any subsidiary company of the Bank or any company
                        within the Standard Chartered group. You also agree and authorize bank to either
                        destroy or cancel any unused undated or post-dated cheque(s) pertaining to the loan
                        facility upon its closure or full settlement.<br />
                        <br />
                        You hereby agree that Bank may at any time or times hereinafter without notice to
                        you apply the right of set off of all or any of the monies from time to time standing
                        to the credit of your any account with the Bank in whichever form they are and whatever
                        name they may be called, in or toward the discharge and satisfaction of all sums
                        of money which now are or at any time or times hereinafter may become due or owing
                        to the Bank by you either alone or jointly with any other person or persons on any
                        account or in respect of any liability whatsoever whether actual or contingent and
                        whether in the capacity of principal debtor or guarantor surety or principal obligor
                        or otherwise. You also agree or authorize the Bank to restrict withdrawal from any
                        of your accounts as designated by the Bank after notice to you until all of your
                        liability with the Bank is fully adjusted and settled.<br />
                        <br />
                        Upon acceptance, this offer shall be construed as a valid contract between the Bank
                        and yourself and valid and binding upon yourself and your successors and shall be
                        valid notwithstanding any change in the document of incorporation of the Bank or
                        any merger, acquisition or amalgamation of the Bank with any other body corporate.<br />
                        <br />
                        This letter is subject to the laws of Bangladesh and courts of Bangladesh shall
                        have jurisdiction to resolve any dispute.<br />
                        <br />
                        <br />
                        Yours faithfully
                        <br />
                        <br />
                        <br />
                        <br />
                        -----------------------&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        ---------------------------
                        <br />
                        Authorized signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Authorized signature<br />
                        <br />
                        <br />
                        <br />
                        <b>Acceptance:<br />
                        </b>I / We have carefully read and understood the above terms and conditions and
                        agree with them.<br />
                        <br />
                        <br />
                        <br />
                        <br />
                        ------------------------------------------------&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        -------------------------------------<br />
                        Signature of primary applicant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature
                        of joint applicant<br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
        </div>
        <div id="actionButton" style="text-align: center;">
            <input type="button" id="btnPrintBaletter" value="Print" onclick="printPreview();" />
        </div>
    </div>
    </form>
</body>
</html>
