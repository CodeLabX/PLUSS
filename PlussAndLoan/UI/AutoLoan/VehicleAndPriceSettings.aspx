﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.VehicleAndPriceSettings" Title="SCB | Vehicle and Price Settings" Codebehind="VehicleAndPriceSettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    
   <%-- <script src="../../Scripts/AutoLoan/VehicleAndPriceSettings.js" type="text/javascript"></script>--%>
     <script src="../../Scripts/AutoLoan/VehicleAndPriceSettings.js" type="text/javascript"></script>
    <script>
        function uploadCallback(msg) {
            vehiclesHelper.uploadCallback(msg);
        }
    </script>
    
    <style type="text/css">
        #nav_VehicleAndPriceSettings a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    
    <div class="pageHeaderDiv">Vehicle & Price Settings</div>
    <div id="Content">
        <div id="vehicleSummaryDiv">
        <div class="SearchwithPager">
        <div>
    <input type="text" id="txtSearch" class="txt txtSearch txtsearchupload widthSize20_per" onkeypress="vehiclesHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search"/>
		<asp:FileUpload ID="fileDocumentUpload" class="txt txtUpload widthSize25_per" runat="server" />
        <asp:DropDownList runat="server" ID="cmbUploadType" 
                    CssClass="comboSizeH_22 widthSize15_per margineLeft_5per">
            <asp:ListItem Value="-1">Select From List</asp:ListItem>
            <asp:ListItem Value="1">New</asp:ListItem>
            <asp:ListItem Value="2">Recondition</asp:ListItem>
            <asp:ListItem Value="3">Used</asp:ListItem>
                </asp:DropDownList>
        <asp:Button ID="btnUploadSave" runat="server" Text="Upload" 
             onclick="btnUploadSave_Click" UseSubmitBehavior="False" />

    </div> 
    </div>
    <div class="clear"></div>
    <br />
            <div id="Pager" class="pager pagerTop_7per" style="margin-top:30px;"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
            <h2 class="centerHeader">Vehicle Summary (V-S)</h2>
            <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				    <colgroup class="sortable">
					    <col width="50px" />
					    <col width="150px" />
					    <col width="150px" />
					    <col width="100px" />
					    <col width="100px" />
					    <col width="100px" />
					    <col width="100px" />
					    <col width="120px" />
				    </colgroup>
				    <thead>
					    <tr>
						    <th>
							    SL
						    </th>
						    <th>
						        Manufacturer
						    </th>
						    <th>
						         Model
						    </th>
						    <th>
						        Trim Level
						    </th>
						    <th>
						        Vehicle CC
						    </th>
						    <th>
						        Chassis Code
						    </th>
						    <th>
						        Vehicle Type
						    </th>
						    <th> <a id="lnkAddVehicle" class="iconlink iconlinkAdd" href="javascript:;">Add Vehicle </a>
						    </th>
					    </tr>
				    </thead>
				    <tbody id="tblvehicleSummary">
				    </tbody>
			    </table>
	</div>
	
	    <div id="priceSummaryDiv" style="display: none;">
	        <h2 class="centerHeader">Price Summary (P-S) for <span id="spnVehicleName" ></span></h2>
	        <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				    <colgroup class="sortable">
					    <col width="50px" />
					    <col width="200px" />
					    <col width="200px" />
					    <col width="120px" />
					    <col width="250px" />
					    
				    </colgroup>
				    <thead>
					    <tr>
						    <th>
							    SL
						    </th>
						    <th>
						        Vehicle Status
						    </th>
						    <th>
						        Manufacturing Year
						    </th>
						    <th>
						        Minimum Price
						    </th>
						    <th>
						        Maximum Price <a id="lnkAddPrice" class="iconlink iconlinkAdd" href="javascript:;" style="margin-left: 75px;">Add Price </a>
						    </th>
						    
					    </tr>
				    </thead>
				    <tbody id="tblPriceSummary">
				    </tbody>
			    </table>
			    
			    <div class="actionButtons">
			        <input type="button" id="btnClose" value="Close"/>
			    </div>
	    </div>
    </div>
    
    <div class="modal" id="vehiclePopupDiv" style="display:none; height:350px;">
		<h3>Vehicle Details</h3>
		<label for="cmbMenufacture">Manufacturer*</label>
		<select id="cmbMenufacture" class="txt" title="Manufacturer">
		</select>
		<br/>
		<label for="cmbMenufacture">Manufacturer Code*</label>
		<input class="txt" id="txtManufacturingCode" title="Manufacturing Code" maxlength = "3" />	
		<br/>
		<label for="txtManufacturingCountry">Manufacturing Country</label>
		<input class="txt" id="txtManufacturingCountry" title="Manufacturing Country" /><br/>
		 <label for="txtModel">Model*</label> <input class="txt" id="txtModel" title="Model"/><br/>
		 <label for="txtModelCode">ModelCode*</label> <input class="txt" id="txtModelCode" title="Model" maxlength="3"/><br/>
		 <label for="txtTrimLevel">Trim Level</label> <input class="txt" id="txtTrimLevel" title="Trim Level"/><br/>
		 <label for="txtCC">Vehicle CC*</label> <input class="txt" id="txtCC" title="CC"/><br/>
		 <label for="cmbVehicleType">Vehicle Type</label>
		 <select id="cmbVehicleType" class="txt" title="Vehicle Type">
		    <option value="-1">Select From List</option>
		    <option value="S1">Sedan</option>
		    <option value="S2">Station wagon</option>
		    <option value="S3">SUV</option>
		    <option value="S4">Pick-Ups</option>
		    <option value="S5">Microbus</option>
		    <option value="S6">MPV</option>
		</select><br/>
		<label for="txtChassisCode">Chassis Code</label> <input class="txt" id="txtChassisCode" title="Chassis Code"/><br/>
		 
		<div class="btns">
			<a href="javascript:;" id="lnkSaveVehicle" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkCloseVehicle" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
    
    <div class="modal" id="priceSummaryPopupDiv" style="display:none">
		<h3>Price Details</h3>
		<label for="txtVehiclereference">Vehicle reference*</label> <input class="txt" id="txtVehiclereference" title="Vehicle reference" disabled="disabled"/><br/>
		<label for="cmbVehicleStatusForPriceSummary">Vehicle Status*</label>
		<select id="cmbVehicleStatusForPriceSummary" class="txt" title="Vehicle Status">
		    <option value="-1" selected="selected">Select from list</option>
		</select>
		 <br/>
		 <label for="txtManufacturingYear">Manufacturing Year</label> <input class="txt" id="txtManufacturingYear" title="Manufacturing Year"/><br/>
		 <label for="txtMinimumPrice">Minimum Price</label> <input class="txt" id="txtMinimumPrice" title="Minimum Price"/><br/>
		 <label for="txtMaximumPrice">Maximum Price</label> <input class="txt" id="txtMaximumPrice" title="Maximum Price"/><br/>
		 <label for="cmbActiveStatus">Active</label>
		 <select id="cmbActiveStatus" class="txt" title="Active">
		    <option value="1">Yes</option>
		    <option value="2">No</option>
		</select>
		 
		<div class="btns">
			<a href="javascript:;" id="lnkSavePrice" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkClosePrice" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
    
</asp:Content>

