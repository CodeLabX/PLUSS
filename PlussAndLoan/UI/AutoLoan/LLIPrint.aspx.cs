﻿using System;
using System.Web.UI.WebControls;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Entity.CheckListPrint;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class LLIPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            int llid = 0;

            llid = Convert.ToInt32(Request.QueryString["llid"]);

            if (llid > 0)
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var loanAssesmentService = new AutoLoanAssessmentService(repository);
                AutoLoanApplicationAll res = new AutoLoanApplicationAll();
                var res1 = loanAssesmentService.getLoanLocetorInfoByLLID(llid);
                res = (AutoLoanApplicationAll)res1;
                setValueToControl(res);
                ConfigureCReport();
            }
        }

        private void setValueToControl(AutoLoanApplicationAll res)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

            ILoanApplicationRepository repository1 = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository1);

            var vendorList = loanApplicationService.GetVendor();

            var userlist = autoLoanAssessmentService.get_pluss_userAll();

            string[,] vehicleArray = new string[,]
	                                    {
	                                        {"S1", "Sedan"},
	                                        {"S2", "Station wagon"},
	                                        {"S3", "SUV"},
	                                        {"S4", "Pick-Ups"},
	                                        {"S5", "Microbus"},
                                            {"S6", "MPV"}
	                                    };

            #region Heading

            lDate.Text = res.objAutoLoanMaster.AppliedDate.ToShortDateString();

            string[] productNameArray = {
                                        "Staff Auto","Conventional Auto","Saadiq Auto"
                                    };
            

            lProductType.Text = productNameArray[res.objAutoLoanMaster.ProductId -1];//res.objAutoLoanMaster.ProductId.ToString();
            if (res.objAutoLoanMaster.JointApplication)
            {
                lApplicantName.Text = res.objAutoPRApplicant.PrName + " & " + res.objAutoJTApplicant.JtName;
            }
            else {
                lApplicantName.Text = res.objAutoPRApplicant.PrName;
            }

            lblProductId.Text = res.objAutoLoanMaster.ProductId.ToString();

            #endregion Heading

            #region LOAN INFORMATION

            lIncome.Text = res.objAuto_An_IncomeSegmentIncome.AppropriateIncome.ToString();
            lLoanAmount.Text = res.objAuto_An_LoanCalculationTotal.TotalLoanAmount.ToString();
            lTenor.Text = res.objAuto_An_LoanCalculationTenor.ConsideredTenor.ToString();
            lInterestRate.Text = res.objAuto_An_LoanCalculationInterest.ConsideredRate.ToString();
            lEMI.Text = res.objAuto_An_LoanCalculationTotal.EMR.ToString();

            #endregion LOAN INFORMATION

            #region MIS MODULE

            lLoanType.Text = res.objAuto_An_IncomeSegmentIncome.EmployeeSegmentCode;
            lLoneTypeRemark.Text = "--";
            lEmployerCat.Text = res.objAuto_An_IncomeSegmentIncome.CategoryCode;

            //foreach (Auto_An_IncomeSegmentApplicantIncome a in res.Auto_An_IncomeSegmentApplicantIncomeList)
            //{
            //    if (a.ApplicantType == 1)
            //    {
            //        lEmployerCat.Text = a.EmployerCategoryCode;

            //    }
            //}
            lEmployerCatRemark.Text = "--";

            var analysisCode = "";
            var approverCode = "";
            var isfoundApp = false;
            var isfoundAnl = false;

            for (var j = 0; j < userlist.Count; j++) {
                if (isfoundAnl && isfoundApp) {
                    break;
                }
                if (userlist[j].USER_ID == res.objAuto_App_Step_ExecutionByUser.AppAnalysisBy) {
                    analysisCode = userlist[j].USER_CODE.ToString();
                    isfoundAnl = true;
                }
                if (userlist[j].USER_ID == res.objAuto_App_Step_ExecutionByUser.AppApprovedBy)
                {
                    approverCode = userlist[j].USER_CODE.ToString();
                    isfoundApp = true;
                }
            }


            lAnalystCode.Text = analysisCode.ToString();
            lAnalystCodeRemark.Text = "--";
            lApproverCode.Text = approverCode.ToString();
            lApproverCodeRemark.Text = "--";

            lIncomeAssesmentCode.Text = res.objAuto_An_IncomeSegmentIncome.AssesmentMethodCode;
            lIncomeAssesmentCodeRemark.Text = "";

            int i = 1;
            foreach (Auto_An_FinalizationDeviation a in res.Auto_An_FinalizationDeviationList)
            {
                if (i < 4)
                {
                    Label lDeviationCode = this.FindControl("lDeviationCode" + i) as Label;
                    lDeviationCode.Text = res.Auto_An_FinalizationDeviationList[i - 1].Code;
                }

                i += 1;
            }
            lDeviationCodeRemark1.Text = "--";

            //lDeviationCode2.Text = "";
            lDeviationCodeRemark2.Text = "--";
            //lDeviationCode3.Text = "";
            lDeviationCodeRemark3.Text = "--";


            if (res.objAuto_An_IncomeSegmentIncome.AssesmentMethodCode == "FN")
            {
                lCollaterals.Text = "ZH";
            }
            else {
                lCollaterals.Text = "--";
            }
            lCollateralsRemark.Text = "--";

            if (res.AutoLoanAnalystVehicleDetail.valuePackAllowed == true)
            {
                lCRD.Text = "ALPLVP";
                lCRDRemark.Text = "--";
            }
            else
            {
                lCRD.Text = "--";
                lCRDRemark.Text = "--";
            }
            lBorrowerCode.Text = res.AutoLoanAnalystApplication.BorrowingRelationShipCode;
            lBorrowerCodeRemark.Text = "--";
            lApplicationSourceCode.Text = res.AutoLoanAnalystApplication.BranchCode;
            lApplicationSourceCodeRemark.Text = "--";

            lDiscretion.Text = "";
            lDiscretionRemark.Text = "";
            lInprft.Text = "";
            lInprftRemark.Text = "";

            if (res.objAuto_An_FinalizationRepaymentMethod.Instrument == 2)
            {
                lBTF.Text = "";
                lBTFRemark.Text = "";
            }
            else {
                var bankList = loanApplicationService.GetBank(1);
                var bankcode = "";
                for (var b = 0; b < bankList.Count; b++) {
                    if (bankList[b].BankID == res.objAuto_An_FinalizationRepaymentMethod.BankID) {
                        bankcode = string.IsNullOrEmpty(bankList[b].BankCode) ? "" : bankList[b].BankCode.ToString();
                        break;
                    }
                }
                lBTF.Text = bankcode;
                lBTFRemark.Text = "";
            }
            

            if (res.objAutoLoanVehicle.VehicleStatus == 1)
            {
                lVehicleStatus.Text = "N";
            }
            else if (res.objAutoLoanVehicle.VehicleStatus == 2)
            {
                lVehicleStatus.Text = "R";
            }
            else if (res.objAutoLoanVehicle.VehicleStatus == 3)
            {
                lVehicleStatus.Text = "U";
            }
            lVehicleStatusRemark.Text = "--";
            var vehicleTypeCode = "";
            for (var v = 0; v < vehicleArray.Length; v++) {
                
                if (vehicleArray[v,1].ToString() == res.objAutoLoanVehicle.VehicleType) {
                    vehicleTypeCode = vehicleArray[v, 0];
                    break;
                }
            }

            lVehicleType.Text = vehicleTypeCode;
            lVehicleTypeRemark.Text = "--";
            //lVehicleSize.Text = res.objAutoLoanVehicle.SeatingCapacity.ToString();
            lVehicleSize.Text = "--";
            lVehicleSizeRemark.Text = res.objAutoLoanVehicle.EngineCC.ToString();

            lVehicleManufacturingYear.Text = "--";
            lVehicleManufacturingYearRemark.Text = res.objAutoLoanVehicle.ManufacturingYear.ToString();


            var vendorType = "";
            var vendorCode = "";
            for (var vn = 0; vn < vendorList.Count; vn++) {
                if (vendorList[vn].AutoVendorId == res.objAutoLoanVehicle.VendorId) {
                    if (vendorList[vn].IsMou == 0) {
                        vendorType = "N";
                    }
                    if (vendorList[vn].IsMou == 1)
                    {
                        
                        vendorType = "Y";
                    }
                    vendorCode = vendorList[vn].VendorCode;
                    break;
                }
            }

            lVendorCat.Text = vendorType;
            lVendorCatRemark.Text = "";
            lLoanPurposeCode.Text = "VHPS";
            lLoanPurposeCodeRemark.Text = "";
            lBancaInfo.Text = "--";
            lBancaInfoRemark.Text = res.objAuto_An_LoanCalculationInsurance.ARTA.ToString();
            lLoanClass.Text = "LN";
            lLoanClassRemark.Text = "";
            lDBR.Text = "--";
            lDBRRemark.Text = res.objAuto_An_LoanCalculationTotal.FBRIncludingInsurance.ToString();
            lLTV.Text = "--";
            lLTVRemark.Text = res.objAuto_An_LoanCalculationTotal.LTVExcludingInsurance.ToString();
            lSAL.Text = "--";
            lSALRemark.Text = res.objAuto_An_IncomeSegmentIncome.TotalJointIncome.ToString();

            double totalDbt = 0;

            for (var e = 0; e < res.objAutoLoanFacilityList.Count; e++) {
                if (res.objAutoLoanFacilityList[e].Status == 1)
                {
                    if (res.objAutoLoanFacilityList[e].FacilityType == 18 || res.objAutoLoanFacilityList[e].FacilityType == 22)
                    {
                        totalDbt += res.objAutoLoanFacilityList[e].PresentLimit;
                    }
                    else
                    {
                        totalDbt += res.objAutoLoanFacilityList[e].PresentBalance;
                    }
                }
            }
            totalDbt += res.objAuto_An_LoanCalculationAmount.ApprovedLoan;

            lDBT.Text = "--";
            lDBTRemark.Text = totalDbt.ToString();
            lMIS4.Text = "--";
            lMIS4Remark.Text = res.objAutoLoanMaster.LLID.ToString();

            #endregion MIS MODULE

            #region VEHICLE MODULE

            lVehicleBrand.Text = res.objAutoLoanVehicle.ManufacturerCode;
            lVehicleBrandRemark.Text = "--";
            lVehicleModel.Text = res.objAutoLoanVehicle.ModelCode;
            lVehicleModelRemark.Text = "--";
            lVehicleDeler.Text = vendorCode;
            lVehicleDelerRemark.Text = "--";
            lVehicleManufacYear.Text = "--";
            lVehicleManufacYearRemark.Text = res.objAutoLoanVehicle.ManufacturingYear.ToString();
            lVehiclceSizee.Text = "--";
            lVehiclceSizeeRemark.Text = res.objAutoLoanVehicle.EngineCC + " CC";
            lVehiclePrice.Text = "--";
            lVehiclePriceRemark.Text = "BDT " + res.AutoLoanAnalystVehicleDetail.ConsideredPrice.ToString();

            #endregion VEHICLE MODULE

            #region INSURANCE MODULE

            var insCode = "";
            var arta = res.AutoLoanAnalystApplication.ARTA;
            if (arta == 2)
            {
                insCode = "";
            }
            else {
                if (res.AutoLoanAnalystApplication.ARTAName.ToUpper() == "PROGOTI") {
                    insCode = "0716";
                }
                if (res.AutoLoanAnalystApplication.ARTAName.ToUpper() == "ALICO")
                {
                    insCode = "1000";
                }

            }




            lInsurenceCode.Text = insCode.ToString();
            lInsurenceCodeRemark.Text = "--";
            lGeneralInsurance.Text = "--";
            lGeneralInsuranceRemark.Text = res.objAuto_An_LoanCalculationInsurance.GeneralInsurance.ToString();

            #endregion INSURANCE MODULE

        }

        protected void bPrint_Click(object sender, EventArgs e)
        {
            if (bPrint.Text == "Print")
            {
                bPrint.Text = "Back";
                ConfigureCReport();
                CrystalPanel.Visible = true;
                mainPanel.Visible = false;
            }
            else
            {
                bPrint.Text = "Print";
                CrystalPanel.Visible = false;
                mainPanel.Visible = true;
            }
        }

        private void ConfigureCReport()
        {
            LLIPrintEntity lli = new LLIPrintEntity();

            #region value assinging
            lli.lDate = lDate.Text;
            lli.lProductType = lProductType.Text;
            lli.lblProductId = lblProductId.Text;
            lli.lApplicantName = lApplicantName.Text;
            lli.lIncome = lIncome.Text;
            lli.lLoanAmount = lLoanAmount.Text;
            lli.lTenor = lTenor.Text;
            lli.lInterestRate = lInterestRate.Text;
            lli.lEMI = lEMI.Text;
            lli.lLoanType = lLoanType.Text;
            lli.lLoneTypeRemark = lLoneTypeRemark.Text;
            lli.lEmployerCat = lEmployerCat.Text;
            lli.lEmployerCatRemark = lEmployerCatRemark.Text;
            lli.lAnalystCode = lAnalystCode.Text;
            lli.lAnalystCodeRemark = lAnalystCodeRemark.Text;
            lli.lApproverCode = lApproverCode.Text;
            lli.lApproverCodeRemark = lApproverCodeRemark.Text;
            lli.lIncomeAssesmentCode = lIncomeAssesmentCode.Text;
            lli.lIncomeAssesmentCodeRemark = lIncomeAssesmentCodeRemark.Text;
            lli.lDeviationCode1 = lDeviationCode1.Text;
            lli.lDeviationCodeRemark1 = lDeviationCodeRemark1.Text;
            lli.lDeviationCode2 = lDeviationCode2.Text;
            lli.lDeviationCodeRemark2 = lDeviationCodeRemark2.Text;
            lli.lDeviationCode3 = lDeviationCode3.Text;
            lli.lDeviationCodeRemark3 = lDeviationCodeRemark3.Text;
            lli.lCollaterals = lCollaterals.Text;
            lli.lCollateralsRemark = lCollateralsRemark.Text;
            lli.lCRD = lCRD.Text;
            lli.lCRDRemark = lCRDRemark.Text;
            lli.lBorrowerCode = lBorrowerCode.Text;
            lli.lBorrowerCodeRemark = lBorrowerCodeRemark.Text;
            lli.lApplicationSourceCode = lApplicationSourceCode.Text;
            lli.lApplicationSourceCodeRemark = lApplicationSourceCodeRemark.Text;
            lli.lDiscretion = lDiscretion.Text;
            lli.lDiscretionRemark = lDiscretionRemark.Text;
            lli.lInprft = lInprft.Text;
            lli.lInprftRemark = lInprftRemark.Text;
            lli.lBTF = lBTF.Text;
            lli.lBTFRemark = lBTFRemark.Text;
            lli.lVehicleStatus = lVehicleStatus.Text;
            lli.lVehicleStatusRemark = lVehicleStatusRemark.Text;
            lli.lVehicleType = lVehicleType.Text;
            lli.lVehicleTypeRemark = lVehicleTypeRemark.Text;
            lli.lVehicleSize = lVehicleSize.Text;
            lli.lVehicleSizeRemark = lVehicleSizeRemark.Text;
            lli.lVehicleManufacturingYear = lVehicleManufacturingYear.Text;
            lli.lVehicleManufacturingYearRemark = lVehicleManufacturingYearRemark.Text;
            lli.lVendorCat = lVendorCat.Text;
            lli.lVendorCatRemark = lVendorCatRemark.Text;
            lli.lLoanPurposeCode = lLoanPurposeCode.Text;
            lli.lLoanPurposeCodeRemark = lLoanPurposeCodeRemark.Text;
            lli.lBancaInfo = lBancaInfo.Text;
            lli.lBancaInfoRemark = lBancaInfoRemark.Text;
            lli.lLoanClass = lLoanClass.Text;
            lli.lLoanClassRemark = lLoanClassRemark.Text;
            lli.lDBR = lDBR.Text;
            lli.lDBRRemark = lDBRRemark.Text;
            lli.lLTV = lLTV.Text;
            lli.lLTVRemark = lLTVRemark.Text;
            lli.lSAL = lSAL.Text;
            lli.lSALRemark = lSALRemark.Text;
            lli.lDBT = lDBT.Text;
            lli.lDBTRemark = lDBTRemark.Text;
            lli.lMIS4 = lMIS4.Text;
            lli.lMIS4Remark = lMIS4Remark.Text;
            lli.lVehicleBrand = lVehicleBrand.Text;
            lli.lVehicleBrandRemark = lVehicleBrandRemark.Text;
            lli.lVehicleModel = lVehicleModel.Text;
            lli.lVehicleModelRemark = lVehicleModelRemark.Text;
            lli.lVehicleDeler = lVehicleDeler.Text;
            lli.lVehicleDelerRemark = lVehicleDelerRemark.Text;
            lli.lVehicleManufacYear = lVehicleManufacYear.Text;
            lli.lVehicleManufacYearRemark = lVehicleManufacYearRemark.Text;
            lli.lVehiclceSizee = lVehiclceSizee.Text;
            lli.lVehiclceSizeeRemark = lVehiclceSizeeRemark.Text;
            lli.lVehiclePrice = lVehiclePrice.Text;
            lli.lVehiclePriceRemark = lVehiclePriceRemark.Text;
            lli.lInsurenceCode = lInsurenceCode.Text;
            lli.lInsurenceCodeRemark = lInsurenceCodeRemark.Text;
            lli.lGeneralInsurance = lGeneralInsurance.Text;
            lli.lGeneralInsuranceRemark = lGeneralInsuranceRemark.Text;
            lli.Label83 = Label83.Text;
            lli.Label85 = Label85.Text;
            lli.Label87 = Label87.Text;
            lli.Label89 = Label89.Text;
            lli.Label84 = Label84.Text;
            lli.Label86 = Label86.Text;
            lli.Label88 = Label88.Text;
            lli.Label90 = Label90.Text;
            #endregion


            Session["LLI"] = lli;
            Response.Redirect("LLIPrintCrystal.aspx");


            //List<LLIPrintEntity> lliList = new List<LLIPrintEntity>();
            //lliList.Add(lli);

            //ReportDocument rd = new ReportDocument();
            //string path = Server.MapPath("PrintCheckList/LLIPrint.rpt");
            //rd.Load(path);
            //rd.SetDataSource(lliList);
            ////rd.SetParameterValue("TruckSerialNumber", "Arifuzzaman");
            //crViewer.ReportSource = rd;
            ////rd.PrintToPrinter(1, true, 0, 0);


        }
    }
}
