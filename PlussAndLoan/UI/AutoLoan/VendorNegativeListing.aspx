﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.VendorNegativeListing" Title="SCB | Vendor Negative List" Codebehind="VendorNegativeListing.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">

    <script src="../../Scripts/AutoLoan/VendorNegativeListingSettings.js" type="text/javascript"></script>
    <script>
        Event.observe(window, 'load', function() {
            Calendar.setup({ inputField: 'txtValidFrom', ifFormat: '%d/%m/%Y',
                weekNumbers: false, button: 'newsDate'
            });
        });
        </script>
    
    
        <style type="text/css">
        #nav_VendorNegativeListing a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    
    <div class="pageHeaderDiv ">Vendor Negative Listing</div>
    
    <div id="Content">
<div class="SearchwithPager">
  <%--  <div class="search">
    <input type="text" id="txtSearch" class="txt txtSearch" onkeypress="tanorHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search"/>
    </div> --%>
  
 <div id="Pager" class="pager" style="left:337px;*left:352px;"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
  
    <div align="center">
         
   
    </div>
    
    
    </div>
 
 <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					<col width="50px" />
					<col width="200px" />
					<col width="100px" />
					<col width="100px" />
					
				</colgroup>
				<thead>
					<tr>
						<th>
							SL
						</th>
						<th>
						    Vendor Name
						</th>
						<th>
						    Listing Date
						</th>
											
						<th> <a id="lnkAddnegativeVendor" class="iconlink iconlinkAdd" href="javascript:;">New Vendor Negative</a>
						</th>
					</tr>
				</thead>
				<tbody id="vendornegativelisting">
				</tbody>
			</table>
 
</div>


<div class="modal" id="Vendor_Negative_ListingPopupDiv" style="height:150px; display:none; width:500px;">
		<h3>Negative Listing</h3>
		<div id="vendoreList">
		<label for="txtHeadline">Vendor:*</label>
		<select id="cmbVendor" class="txt" title="VendorList">
		    <%--<option value="-1" selected="selected">Select from List</option>--%>
		</select>
		 <a href="javascript:;" id="lnkNewVendore" class="iconlink iconlinkAdd" style="color:#0680CB;">Add New</a>
		 </div>	
		 <div id="newVendore" style="display:none;">
		    <label for="txtVendorName">Vendor Name</label> <input class="txt" id="txtVendorName" title="IR With ARTA"/><br/>
		<label for="cmbVehicleStatus">Dealer Status</label>
		<select id="cmbVehicleStatus" class="txt" title="Insurance Type" >
		    <%--<option value="1" selected="selected">General</option>
		    <option value="2">Life</option>
		    <option value="3">General & Life</option>--%>
		</select>
		 <br/>
		 <label for="cmbIsMou">Is Mou</label>
		<select id="cmbIsMou" class="txt" title="Is Mou" >
		    <option value="1" selected="selected">Yes</option>
		    <option value="0">No</option>
		</select>
		 <br/>
		<label for="txtAddress">Address</label> <input class="txt" id="txtAddress" title="Address"/><br/>
		<label for="txtPhone">Phone</label> <input class="txt" id="txtPhone" title="Phone"/><br/>
		<label for="txtWebsite">Website</label> <input class="txt" id="txtWebsite" title="Website"/><br/>
		<label for="txtIRWithARTA">IR With ARTA</label> <input class="txt" id="txtIRWithARTA" title="IR With ARTA"/><br/>
		<label for="txtIRWithoutARTA">IR Without ARTA</label> <input class="txt" id="txtIRWithoutARTA" title="IR Without ARTA"/><br/>
		<label for="txtProcessingFee">Processing Fee</label> <input class="txt" id="txtProcessingFee" title="Processing Fee"/><br/>
		<label for="txtValidFrom">Valid From</label> <input class="txt" id="txtValidFrom" title="Valid From"/><br/>
		<a href="javascript:;" id="lnkHide" class="iconlink" style="color:#0680CB; float:right;">Hide</a>
		<div class="clear"></div>
		 </div>
		 		
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Add to Negative List</a>
			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Cancel</a>
		</div>
    </div>
</asp:Content>

