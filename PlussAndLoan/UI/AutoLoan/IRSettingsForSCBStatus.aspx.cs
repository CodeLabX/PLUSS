﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class IRSettingsForSCBStatus : System.Web.UI.Page
    {
        [AjaxNamespace("IRSettingsForSCBStatus")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(IRSettingsForSCBStatus));
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoSCBStatusIR> GetAutoSCBStatusIRSummary(int pageNo, int pageSize)
        {
            pageNo = pageNo * pageSize;

            AutoSCBStatusIRrepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoSCBStatusIRService = new AutoSCBStatusIRService(repository);


            List<AutoSCBStatusIR> objList_AutoSCBStatusIR =
                autoSCBStatusIRService.GetAutoSCBStatusIRSummary(pageNo, pageSize);
            return objList_AutoSCBStatusIR;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveAuto_IRSCBStatus(AutoSCBStatusIR SCBStatusIR)
        {
            AutoSCBStatusIRrepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new AutoSCBStatusIRService(repository);
            var res = autoLoanDataService.SaveAuto_IRSCBStatus(SCBStatusIR);
            return res;

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string IRSCBStatusDeleteById(int scbStatusIRId)
        {
            AutoSCBStatusIRrepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new AutoSCBStatusIRService(repository);
            var res = autoLoanDataService.IRSCBStatusDeleteById(scbStatusIRId);
            return res;

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> GetStatus(int statusID)
        {
            AutoSCBStatusIRrepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoSCBStatusIRService = new AutoSCBStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoSCBStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }

    }
}
