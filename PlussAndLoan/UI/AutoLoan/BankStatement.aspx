﻿<%@ Page Title="Pluss | Bank Statement" Language="C#" MasterPageFile="~/UI/AutoLoan/AutoSales.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.BankStatement" Codebehind="BankStatement.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    

    <script src="../../Scripts/AutoLoan/BankStatement.js" type="text/javascript"></script>
    
    
        <style type="text/css">
        #nav_BankStatement a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    
    <div style ="width: 100%;*width: 100%;_width: 100%; border:solid 1px black">
    <div id="divLoanAppMasterData" class="fieldDiv divPadding formBackColor">
        
        <div class="fieldDiv">
            <label class="lblBig">LLID:</label>
                <input type="text" id="txtLLID" class="txtField" />
                <input type="hidden" id="txtAutoLoanMasterId" class="txtField" value="0" />
                <input type="button" id="btnLoanSearch" class="" value="Search"/>
        <label class="lblBlank">
        </label>
           
        </div>
        
        
        

        <div class="fieldDiv">
            <label class="lblBig">Primary Applicant:</label>
            <input type="text" id="txtPrimaryApplicant" class="txtFieldBig" disabled="disabled"/>
            <input id="oldBankID" type="hidden" value="0"/>
            <input id="oldBankIDjT" type="hidden" value="0"/>
            <input id="oldAccountNO" type="hidden" value="0"/>
            <input id="oldAccountNOjT" type="hidden" value="0"/>
            <label class="lbl">
            </label>
            <label class="lblBig">Profession:</label>
            <input type="text" id="txtProfession" class="txtFieldBig" disabled="disabled"/>
        </div>
        <div class="fieldDiv">
            <label class="lblBig">Date of Birth:</label>
            <input type="text" id="txtDateofBirth" class="txtFieldBig" disabled="disabled"/>
            <label class="lbl">
            </label>
            <label class="lblBig">Declared Income:</label>
            <input type="text" id="txtDeclaredIncome" class="txtFieldBig" disabled="disabled"/>
        </div>

        <div class="fieldDiv">
            <label class="lblBig">Verified Address:</label>
            <input type="text" id="txtVerifiedAddress" class="txtFieldBig" disabled="disabled" style="width: 744px;*width: 719px;_width: 737px;"/>
            
        </div>
        
        <div id="divJointApplicantDetails" style="display:none;">
        
        <div class="fieldDiv">
            <label class="lblBig">Secondary Applicant:</label>
            <input type="text" id="txtSecondaryApplicant" class="txtFieldBig" disabled="disabled"/>
            <label class="lbl">
            </label>
            <label class="lblBig">Profession:</label>
            <input type="text" id="txtSecondaryProfession" class="txtFieldBig" disabled="disabled"/>
        </div>

        <div class="fieldDiv">
            <label class="lblBig">Date of Birth:</label>
            <input type="text" id="txtSecondaryDateobBirth" class="txtFieldBig" disabled="disabled"/>
            <label class="lbl">
            </label>
            <label class="lblBig">Declared Income:</label>
            <input type="text" id="txtSecondaryDeclaredIncome" class="txtFieldBig" disabled="disabled"/>
        </div>
  
        </div>
    </div>
        
        
        
        
        
        
        
        


    </div>
    <div class="divLeftSubHead"  style="width: 100%;*width: 100%;_width: 100%; border: none;" >
        <div class="fieldDiv">
            
        </div>
    </div>
    <%--<div class ="divMiddileSubHead"></div>
    <div class ="divRightSubHead"></div>
    <div class ="divLeft"></div>--%>
    <div class ="clear"></div>

    <div class="content" id="page-5">





<div  style=" width:100%; margin-left:5px;">


        <div style="width:100%; text-align:center; padding-top:10px;"><span style="font-size:large; font-weight:bold;">Primary Applicant</span></div>
        <div id="divbankstatemntDetails" class="mainDivBankStatemntLeft">
             <div id="div14" class="fieldDiv divPadding">
                    <div class="fieldDiv">
            <label class="lblSmall" >
                Bank Name:</label>
                <select id="cmbPApplicantBankName"  onchange="bankstatementManager.GetBranch('cmbPApplicantBankName')" class="comboWidth">
                <option value="-1">Select Bank</option>
                </select>
                <input type="hidden" id="PrBankStatementId" class="txtField" value="0" />
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">
                Branch Name:</label>
                <select id="cmbPApplicantBranchName" class="comboWidth">
                <option value="-1">Select Branch</option>
                </select>
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">
                A/C Name:</label>
                <input type="text" id="txtPApplicantAccountName" class="txtField" />
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">
                A/C Number:</label>
                <input type="text" id="txtPApplicantAccountNumber" class="txtField"/>
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">
                A/C Category:</label>
                <select id="cmbPApplicantAccountCategory" class="comboWidth">
                <option value="-1">Select A/C Catagory</option>
                <option value="1">Corporate</option>
                <option value="2">Individual</option>
                </select>
        </div>
                    <div class="fieldDiv">
                        <label class="lblSmall">A/C Type:</label>
                        <select id="cmbPApplicantAccountType" class="comboWidth">
                            <option value="-1">Select A/C Type</option>
                            <option value="1">Current</option>
                            <option value="2">Savings</option>
                            <option value="3">STD</option>
                            <option value="4">Others</option>
                        </select>
                    </div>
        <div class="actionButtons">
            <input type="button" id="btnSavePrimaryBankStatement" value="Save Primary Bank Details"/>
            <input type="button" id="btnPrimaryClear" value="Clear"/>
        </div>
        
        
            </div>
        
   </div>
        <div id="divBankStatementSummery" class="mainDivBankStatemntRight">
            
            <div id="div19" class="fieldDiv divPadding">
                 
                         
                         <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				                        <colgroup class="sortable">
					                        <col width="30px" />
					                        <col width="150px" />
					                        <col width="100px" />
					                        <col width="70px" />
					                        <col width="100px" />
					                        <col width="90px" />
					                        <col width="30px" />
					                        
				                        </colgroup>
				                        <thead>
					                        <tr>
						                        <th>
							                        SL
						                        </th>
						                        <th>
						                            Bank Name
						                        </th>
						                        <th>
						                            Branch Name
						                        </th>
						                        <th>
						                            A/C No
						                        </th>
						                        <th>
						                            From
						                        </th>
						                        <th>
						                            To
						                        </th>
						                        <th>
						                            #
						                        </th>
                        					
					                        </tr>
				                        </thead>
				                        <tbody id="bankStatementSummerylist">
				                        </tbody>
			                        </table>
                         
                     
            </div>
        
   </div>
          <div class ="clear"></div>
        <br />
        <br />
            <div id="divPrBankStatement" style="display:none;">
             <div>
             <div style="width:100%; text-align:center; padding-top:10px;"><span style="font-size:large; font-weight:bold;">Bank Statement</span></div>
              <div style="width:100%; text-align:left; padding-top:10px;"><span style="font-size:medium; font-weight:bold;">BANK - 1 (AVERAGE BALANCE)</span></div>
                 <table width="100%">
                        <tr>
                            <td align="left">
                                <object classid="clsid:0002E559-0000-0000-C000-000000000046" id="PBankStatementAVG" width="100%"
                                    style="height: 250px">
                                    <param name="XMLData" value="'<?xml version='1.0'?><ss:Workbook xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:c='urn:schemas-microsoft-com:office:component:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'><x:ExcelWorkbook><x:ProtectStructure>False</x:ProtectStructure><x:ActiveSheet>0</x:ActiveSheet></x:ExcelWorkbook><ss:Worksheet ss:Name='Sheet0'><x:WorksheetOptions><x:ViewableRange>R1:R262144</x:ViewableRange><x:Selection>R1C1</x:Selection><x:TopRowVisible>0</x:TopRowVisible><x:LeftColumnVisible>0</x:LeftColumnVisible><x:ProtectContents>False</x:ProtectContents></x:WorksheetOptions><c:WorksheetOptions></c:WorksheetOptions></ss:Worksheet></ss:Workbook>'" />
                                </object>
                            </td>
                        </tr>
                 </table>
             </div>
             
             <%--<div>
             <div style="width:20PX; text-align:left; padding-top:10px; cursor: pointer; background-color: #3DAE38;"><span style="font-size:medium; font-weight:bold;">ASSESMENT</span></div>
              <div style="width:100%; text-align:left; padding-top:10px;"><span style="font-size:medium; font-weight:bold;">BANK - 1 (CREDIT TURNOVER)</span></div>
                 <table width="100%">
                        <tr>
                            <td align="left">
                                <object classid="clsid:0002E559-0000-0000-C000-000000000046" id="PBankStatementCRE" width="100%"
                                    style="height: 250px">
                                    <param name="XMLData" value="'<?xml version='1.0'?><ss:Workbook xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:c='urn:schemas-microsoft-com:office:component:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'><x:ExcelWorkbook><x:ProtectStructure>False</x:ProtectStructure><x:ActiveSheet>0</x:ActiveSheet></x:ExcelWorkbook><ss:Worksheet ss:Name='Sheet0'><x:WorksheetOptions><x:ViewableRange>R1:R262144</x:ViewableRange><x:Selection>R1C1</x:Selection><x:TopRowVisible>0</x:TopRowVisible><x:LeftColumnVisible>0</x:LeftColumnVisible><x:ProtectContents>False</x:ProtectContents></x:WorksheetOptions><c:WorksheetOptions></c:WorksheetOptions></ss:Worksheet></ss:Workbook>'" />
                                </object>
                            </td>
                        </tr>
                 </table>
             </div>--%>
    
             <div class="actionButtons">
                <input type="button" id="btnAddPrimaryApplicant" class="xbtn" value="Save Primary Bank Statement"/>
             </div>
    
    </div>
    <div style="width:100%;"></div>
    <hr  style="left: 30px;position: static;width: 725px;*width: 725px; _width: 725px;"/>
    <div id="JBankStatementDiv" style="display: none;">
    <div class ="clear"></div>
        <br />
        <br />
    <div style="width:100%; text-align:center; padding-top:10px;"><span style="font-size:large; font-weight:bold;">Joint Applicant</span></div>
        <div id="divbankstatemntDetails2" class="mainDivBankStatemntLeft">
             <div id="div22" class="fieldDiv divPadding">
                
                <div class="fieldDiv">
            <label class="lblSmall">
                Bank Name:</label>
                <select id="cmbJApplicantBankName" onchange="bankstatementManager.GetBranch('cmbJApplicantBankName')" class="comboWidth">
                <option value="-1">Select Bank</option>
                </select>
                <input type="hidden" id="txtjtBankStatementId" class="txtField" value="0" />
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">
                Branch Name:</label>
                <select id="cmbJApplicantBranchName" class="comboWidth">
                <option value="-1">Select Branch</option>
                </select>
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">
                A/C Name:</label>
                <input type="text" id="txtJApplicantAccountName" class="txtField" />
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">
                A/C Number:</label>
                <input type="text" id="txtJApplicantAccountNumber" class="txtField" />
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">A/C Category:</label>
                <select id="cmbJApplicantAccountCategory" class="comboWidth">
                <option value="-1">Select A/C Catagory</option>
                <option value="1">Corporate</option>
                <option value="2">Individual</option>
                </select>
        </div>
                    <div class="fieldDiv">
            <label class="lblSmall">A/C Type:</label>
                <select id="cmbJApplicantAccountType" class="comboWidth">
                <option value="-1">Select A/C Type</option>
                <option value="1">Current</option>
                <option value="2">Savings</option>
                <option value="3">STD</option>
                <option value="4">Others</option>
                </select>
        </div>
        <div class="actionButtons">
            <input type="button" id="btnSaveJointBankStatement" value="Save Joint Bank Statement"/>
            <input type="button" id="btnJointClear" value="Clear"/>
        </div>
        
        
    </div>
        
   </div>
        <div id="divBankStatementSummery2" class="mainDivBankStatemntRight">
            <div id="div25" class="fieldDiv divPadding">
                 
                         
                         <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				                        <colgroup class="sortable">
					                        <col width="30px" />
					                        <col width="150px" />
					                        <col width="100px" />
					                        <col width="70px" />
					                        <col width="100px" />
					                        <col width="90px" />
					                        <col width="30px" />
					                        
				                        </colgroup>
				                        <thead>
					                        <tr>
						                        <th>
							                        SL
						                        </th>
						                        <th>
						                            Bank Name
						                        </th>
						                        <th>
						                            Branch Name
						                        </th>
						                        <th>
						                            A/C No
						                        </th>
						                        <th>
						                            From
						                        </th>
						                        <th>
						                            To
						                        </th>
						                        <th>
						                            #
						                        </th>
                        					
					                        </tr>
				                        </thead>
				                        <tbody id="bankStatementSummerylistforJointApplicant">
				                        </tbody>
			                        </table>
                         
                     
            </div>
        
   </div>
   
<div class ="clear"></div>
        <br />
        <br />
     
       
             <div id="divJointBankStatement" style="display:none;">
             
               <div>
             <div style="width:100%; text-align:center; padding-top:10px;"><span style="font-size:large; font-weight:bold;">Bank Statement</span></div>
              <div style="width:100%; text-align:left; padding-top:10px;"><span style="font-size:medium; font-weight:bold;">BANK - 1 (AVERAGE BALANCE)</span></div>
                 <table width="100%">
                        <tr>
                            <td align="left">
                                <object classid="clsid:0002E559-0000-0000-C000-000000000046" id="JBankStatementAVG" width="100%"
                                    style="height: 250px">
                                    <param name="XMLData" value="'<?xml version='1.0'?><ss:Workbook xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:c='urn:schemas-microsoft-com:office:component:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'><x:ExcelWorkbook><x:ProtectStructure>False</x:ProtectStructure><x:ActiveSheet>0</x:ActiveSheet></x:ExcelWorkbook><ss:Worksheet ss:Name='Sheet0'><x:WorksheetOptions><x:ViewableRange>R1:R262144</x:ViewableRange><x:Selection>R1C1</x:Selection><x:TopRowVisible>0</x:TopRowVisible><x:LeftColumnVisible>0</x:LeftColumnVisible><x:ProtectContents>False</x:ProtectContents></x:WorksheetOptions><c:WorksheetOptions></c:WorksheetOptions></ss:Worksheet></ss:Workbook>'" />
                                </object>
                            </td>
                        </tr>
                 </table>
             </div>
             
             <%--<div>
             <div style="width:20PX; text-align:left; padding-top:10px; cursor: pointer; background-color: #3DAE38;"><span style="font-size:medium; font-weight:bold;">ASSESMENT</span></div>
              <div style="width:100%; text-align:left; padding-top:10px;"><span style="font-size:medium; font-weight:bold;">BANK - 1 (CREDIT TURNOVER)</span></div>
                 <table width="100%">
                        <tr>
                            <td align="left">
                                <object classid="clsid:0002E559-0000-0000-C000-000000000046" id="Object3" width="100%"
                                    style="height: 250px">
                                    <param name="XMLData" value="'<?xml version='1.0'?><ss:Workbook xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:c='urn:schemas-microsoft-com:office:component:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'><x:ExcelWorkbook><x:ProtectStructure>False</x:ProtectStructure><x:ActiveSheet>0</x:ActiveSheet></x:ExcelWorkbook><ss:Worksheet ss:Name='Sheet0'><x:WorksheetOptions><x:ViewableRange>R1:R262144</x:ViewableRange><x:Selection>R1C1</x:Selection><x:TopRowVisible>0</x:TopRowVisible><x:LeftColumnVisible>0</x:LeftColumnVisible><x:ProtectContents>False</x:ProtectContents></x:WorksheetOptions><c:WorksheetOptions></c:WorksheetOptions></ss:Worksheet></ss:Workbook>'" />
                                </object>
                            </td>
                        </tr>
                 </table>
             </div>--%>
    
            <div class="actionButtons">
                <input type="button" id="btnAddJointApplicant" class="xbtn" value="Save Joint Bank Statement"/>
             </div>
    
    </div>

</div>











    
</div>
</div>
</asp:Content>

