﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class Auto_ApproverSummary : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //string callingFrom = Request.QueryString["CallFrom"];
            if (Session["MasterPageUrlForAutoApprover"] != null)
            {
                this.MasterPageFile = Session["MasterPageUrlForAutoApprover"].ToString();
            }

        }
        
        [AjaxNamespace("Auto_ApproverSummary")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(Auto_ApproverSummary));
        }

        #region Get Method
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanSummary> GetLoanSummary(int pageNo, int pageSize, Int32 searchKey)
        {
            List<AutoLoanSummary> objLoanSummaryList = new List<AutoLoanSummary>();
            pageNo = pageNo * pageSize;
            var userType = Session["AutoUserType"];
            var userID = Convert.ToInt32(Session["Id"].ToString());
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objLoanSummaryList;
            }
            else
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                objLoanSummaryList =
                    autoLoanAssessmentService.getAutoLoanSummaryAllForApprover(pageNo, pageSize, searchKey, statePermission, userID);
            }
            return objLoanSummaryList;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public Object GetAllLoanInfoByLLID(int llid)
        {
            var objAutoLoanApplicationAll = new Object();
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objAutoLoanApplicationAll;
            }
            else
            {

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                objAutoLoanApplicationAll = autoLoanAssessmentService.getLoanLocetorInfoByLLID(llid, statePermission);


                //ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
                //var autoLoanDataService = new LoanApplicationService(repository);
                //objAutoLoanApplicationAll = autoLoanDataService.getLoanLocetorInfoByLLID(llid, statePermission);
            }

            return objAutoLoanApplicationAll;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<t_Pluss_Remark> getRemarkStrangthConditionWeaknessAll()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getRemarkStrangthConditionWeaknessAll();
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string AppealedByAutoLoanMasterId(int appiledOnAutoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                if (UserType == 10)
                {
                    objAutoStatusHistory.StatusID = 18; //received by auto ops maker
                }
                else if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 20; //received by auto ops Checker
                }
                else if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 23; //received by auto CI Support
                }
                else if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 26; //received by auto CI Analyst
                }
                else if (UserType == 7)
                {
                    objAutoStatusHistory.StatusID = 29; //received by auto Approver
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<DaviationSettingsEntity> getDeviationLevel()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getDeviationLevel();
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<DaviationSettingsEntity> getDeviationByLevel(int deviationFor)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.getDeviationByLevel(deviationFor);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBank> GetBank(int status)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBank(status);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoSegmentSettings> GetEmployeSegment()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetEmployeSegment();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussSector> GetSector()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetAllPlussSector();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussIncomeAcesmentMethod> GetIncomeAssement()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetPlussIncomeAcesmentMethod();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> getVehicleStatus(int statusID)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVehicleStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }


        #endregion Get Method


        #region Save Data
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string ApprovedAutoLoan(int autoLoanMasterId,string remark)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;


                objAutoStatusHistory.StatusID = 30; // Approved by Approver.
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory, remark);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string ApproveWithCondition(int autoLoanMasterId, string remark)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;


                objAutoStatusHistory.StatusID = 31; // Approved with condition by Approver.
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory,remark);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string ConditionallyApproved(int autoLoanMasterId,string remark)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;


                objAutoStatusHistory.StatusID = 32; // Conditionally Approved by Approver.
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory, remark);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string Reject(int autoLoanMasterId, string remark)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;


                objAutoStatusHistory.StatusID = 37; // Rejected by Approver.
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory,remark);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string Decline(int autoLoanMasterId, string remark)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;


                objAutoStatusHistory.StatusID = 38; // Decline by Approver.
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory, remark);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string BackToAnalyst(int autoLoanMasterId, string remark)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                //var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;


                objAutoStatusHistory.StatusID = 26; // Back To Analyst By Approver
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory, remark);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        #endregion Save Data


    }
}
