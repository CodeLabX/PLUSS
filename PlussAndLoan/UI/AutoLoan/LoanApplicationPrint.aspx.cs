﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class LoanApplicationPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int llid = 0;
            try
            {
                llid = Convert.ToInt32(Request.QueryString["llid"]);

                //ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
                //var loanApplicationService = new LoanApplicationService(repository);
                //AutoLoanApplicationAll res = new AutoLoanApplicationAll();
                //var res1 = loanApplicationService.getLoanLocetorInfoByLLID(llid);
                //res = (AutoLoanApplicationAll) res1;

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var loanAssesmentService = new AutoLoanAssessmentService(repository);
                AutoLoanApplicationAll res = new AutoLoanApplicationAll();
                var res1 = loanAssesmentService.getLoanLocetorInfoByLLID(llid);
                res = (AutoLoanApplicationAll)res1;

                //generateInsuranceCheckBox();
                setValueToControl(res);
            }
            catch (Exception ex)
            {

            }

        }

        private List<Auto_InsuranceBanca> getGeneralInsurance()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getGeneralInsurance();
            return res;
        }

        private void generateInsuranceCheckBox(AutoLoanApplicationAll res)
        {
            CheckBoxList cbListLifeInsurance = new CheckBoxList();
            CheckBoxList cbListGeneralInsurance = new CheckBoxList();
            var allInsurance = getGeneralInsurance();
            List<Auto_InsuranceBanca> LifeInsurance = new List<Auto_InsuranceBanca>();
            List<Auto_InsuranceBanca> GeneralInsurance = new List<Auto_InsuranceBanca>();

            foreach (Auto_InsuranceBanca a in allInsurance)
            {
                if (a.InsType == 2 || a.InsType == 3)
                {
                    LifeInsurance.Add(a);
                }
                else
                {
                    GeneralInsurance.Add(a);
                }
            }

            for (int i = 0; i < LifeInsurance.Count; i++)
            {
                //cbList.Items.Add(new ListItem("Checkbox " + i.ToString(), i.ToString()));
                cbListLifeInsurance.Items.Add(new ListItem(LifeInsurance[i].CompanyName, LifeInsurance[i].InsCompanyId.ToString()));
            }

            for (int i = 0; i < GeneralInsurance.Count; i++)
            {
                cbListGeneralInsurance.Items.Add(new ListItem(GeneralInsurance[i].CompanyName, GeneralInsurance[i].InsCompanyId.ToString()));
            }

            PlaceHolder1.Controls.Add(cbListLifeInsurance);
            PlaceHolder2.Controls.Add(cbListGeneralInsurance);

            foreach (ListItem li in cbListLifeInsurance.Items)
            {
                //var value = li.Value;
                //var text = li.Text;
                //bool isChecked = li.Selected;

                var lifeIns = res.objAutoApplicationInsurance;

                if (li.Value == lifeIns.LifeInsurance.ToString())
                {
                    li.Selected = true;
                }
            }

            foreach (ListItem li in cbListGeneralInsurance.Items)
            {
                var generalIns = res.objAutoApplicationInsurance;

                if (li.Value == generalIns.GeneralInsurance.ToString())
                {
                    li.Selected = true;
                }
            }


        }


        private void setValueToControl(AutoLoanApplicationAll res)
        {
            AutoSegmentSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoSegmentSettingsService = new AutoSegmentSettingsService(repository);


            List<AutoProfession> objList_AutoProfession = autoSegmentSettingsService.GetProfession();


            #region Heading

            txtSource.Text = res.AutoLoanAnalystApplication.BranchName;
            txtSourceCode.Text = res.AutoLoanAnalystApplication.BranchCode;//res.objAutoLoanMaster.SourceCode;
            txtDate.Text = res.objAutoLoanMaster.ReceiveDate.ToShortDateString();
            txtAppliedAmount.Text = res.objAutoLoanMaster.AppliedAmount.ToString();
            switch (res.objAutoLoanMaster.AskingTenor)
            {
                case "12":
                    {
                        chkTenor12.Checked = true;
                        break;
                    }

                case "24":
                    {
                        chkTenor24.Checked = true;
                        break;
                    }

                case "36":
                    {
                        chkTenor36.Checked = true;
                        break;
                    }
                case "48":
                    {
                        chkTenor48.Checked = true;
                        break;
                    }
                case "60":
                    {
                        chkTenor60.Checked = true;
                        break;
                    }
            }

            switch (res.objAutoLoanVehicle.VehicleType)
            {
                case "Sedan":
                    {
                        chkSedan.Checked = true;
                        break;
                    }
                case "Pick-Ups":
                    {
                        chkPickUp.Checked = true;
                        break;
                    }
                case "Station Wagon":
                    {
                        chkStationWagon.Checked = true;
                        break;
                    }
                case "Microbus":
                    {
                        chkMicrobus.Checked = true;
                        break;
                    }
                case "SUV":
                    {
                        chkSUV.Checked = true;
                        break;
                    }
                case "MPV":
                    {
                        chkMPV.Checked = true;
                        break;
                    }
            }

            //if (res.objAutoApplicationInsurance.LifeInsurance > 0)
            //{
            //    chkInsuranceTypeLife.Checked = true;
            //}
            if (res.AutoLoanAnalystApplication.ARTA == 1)
            {
                chkInsuranceTypeLife.Checked = true;
            }

            //if (res.objAutoApplicationInsurance.GeneralInsurance > 0)
            if (res.AutoLoanAnalystApplication.GeneralInsurance == 1)
            {
                chkInsuranceTypeGenerel.Checked = true;
            }

            if (res.objAutoApplicationInsurance.InsuranceFinancewithLoan)
            {
                chkInsuranceFinanceWithLoanYes.Checked = true;
            }
            else
            {
                chkInsuranceFinanceWithLoanNo.Checked = true;
            }

            #endregion heading

            #region Insurance

            generateInsuranceCheckBox(res);

            #endregion 

            #region Primary Applicant

            // Primary Applicant

            lblPApplicantName.Text = res.objAutoPRApplicant.PrName;
            lblPApplicantRelationshipNumber.Text = res.objAutoPRApplicant.RelationShipNumber.ToString();
            lblPApplicantTinNumber.Text = res.objAutoPRApplicant.TINNumber;
            lblPApplicantFathersName.Text = res.objAutoPRApplicant.FathersName;
            lblPApplicantMothersName.Text = res.objAutoPRApplicant.MothersName;
            lblPApplicantBirthDateDay.Text = res.objAutoPRApplicant.DateofBirth.Day.ToString();
            lblPApplicantBirthDateMonth.Text = res.objAutoPRApplicant.DateofBirth.Month.ToString();
            lblPApplicantBirthDateYear.Text = res.objAutoPRApplicant.DateofBirth.Year.ToString();
            switch (res.objAutoPRApplicant.MaritalStatus)
            {
                case "Married":
                    {
                        chkPApplicantMaritalStatusMarried.Checked = true;
                        break;
                    }
                case "Single":
                    {
                        chkPApplicantMaritalStatusSingle.Checked = true;
                        break;
                    }
                case "Separated":
                    {
                        chkPApplicantMaritalStatusSeparated.Checked = true;
                        break;
                    }
                case "Others":
                    {
                        chkPApplicantMaritalStatusOthers.Checked = true;
                        break;
                    }
            }

            lblPApplicantSpouseName.Text = res.objAutoPRApplicant.SpouseName;
            lblPApplicantSpouseProfession.Text = res.objAutoPRApplicant.SpouseProfession;
            lblPApplicantSpouseWorkaddress.Text = res.objAutoPRApplicant.SpouseWorkAddress;
            lblPApplicantSpouseContact.Text = res.objAutoPRApplicant.SpouseContactNumber;
            lblPApplicantNationality.Text = res.objAutoPRApplicant.Nationality;
            lblPApplicantDependance.Text = res.objAutoPRApplicant.NumberofDependents;
            lblPApplicantPassportIDTypeAndNo.Text = string.IsNullOrEmpty(res.objAutoPRApplicant.IdentificationNumber)? "" : res.objAutoPRApplicant.IdentificationNumberType + " : " +
                                                    res.objAutoPRApplicant.IdentificationNumber;
            switch (res.objAutoPRApplicant.HighestEducation)
            {
                case "SSC":
                    {
                        chkPApplicantSSC.Checked = true;
                        break;
                    }
                case "HSC":
                    {
                        chkPApplicantHSC.Checked = true;
                        break;
                    }
                case "DIP":
                    {

                        break;
                    }
                case "Graduate":
                    {
                        chkPApplicantGraduate.Checked = true;
                        break;
                    }
                case "P-Graduate":
                    {
                        chkPApplicantPostGraduate.Checked = true;
                        break;
                    }
                case "Others":
                    {
                        chkPApplicantOthers.Checked = true;
                        break;
                    }
            }

            lblPApplicantResidenceAddress.Text = res.objAutoPRApplicant.ResidenceAddress;
            lblPApplicantMailingAddress.Text = res.objAutoPRApplicant.MailingAddress;
            lblPApplicantParmanentAddress.Text = res.objAutoPRApplicant.PermanentAddress;
            lblPApplicantResidenceStatus.Text = res.objAutoPRApplicant.ResidenceStatus == "-1" ? "" : res.objAutoPRApplicant.ResidenceStatus;
            lblPApplicantYearsInCurrentAddress.Text = res.objAutoPRApplicant.ResidenceStatusYear == "-1" ? "" : res.objAutoPRApplicant.ResidenceStatusYear;
            lblPApplicantPhoneRes.Text = res.objAutoPRApplicant.PhoneResidence;
            lblPApplicantPhoneMobile.Text = res.objAutoPRApplicant.Mobile;
            lblPApplicantEmail.Text = res.objAutoPRApplicant.Email;

            #endregion Primary Applicant

            #region Joint Applicant

            // Joint applicant

            if (res.objAutoLoanMaster.JointApplication)
            {
                lblJApplicantName.Text = res.objAutoJTApplicant.JtName;
                lblJApplicantRelationshipNumber.Text = string.IsNullOrEmpty(res.objAutoJTApplicant.RelationShipNumber) ? "" : res.objAutoJTApplicant.RelationShipNumber.ToString();
                lblJApplicantTinNumber.Text = res.objAutoJTApplicant.TINNumber;
                lblJApplicantFathersName.Text = res.objAutoJTApplicant.FathersName;
                lblJApplicantMothersName.Text = res.objAutoJTApplicant.MothersName;
                lblJApplicantBirthDateDay.Text = res.objAutoJTApplicant.DateofBirth.Day.ToString();
                lblJApplicantBirthDateMonth.Text = res.objAutoJTApplicant.DateofBirth.Month.ToString();
                lblJApplicantBirthDateYear.Text = res.objAutoJTApplicant.DateofBirth.Year.ToString();
                switch (res.objAutoJTApplicant.MaritalStatus)
                {
                    case "Married":
                        {
                            chkJApplicantMaritalStatusMarried.Checked = true;
                            break;
                        }
                    case "Single":
                        {
                            chkJApplicantMaritalStatusSingle.Checked = true;
                            break;
                        }
                    case "Separated":
                        {
                            chkJApplicantMaritalStatusSeparated.Checked = true;
                            break;
                        }
                    case "Others":
                        {
                            chkJApplicantMaritalStatusOthers.Checked = true;
                            break;
                        }
                }

                lblJApplicantSpouseName.Text = res.objAutoJTApplicant.SpouseName;
                lblJApplicantSpouseProfession.Text = res.objAutoJTApplicant.SpouseProfession;
                lblJApplicantSpouseWorkaddress.Text = res.objAutoJTApplicant.SpouseWorkAddress;
                lblJApplicantSpouseContact.Text = res.objAutoJTApplicant.SpouseContactNumber;
                lblJApplicantNationality.Text = res.objAutoJTApplicant.Nationality;
                lblJApplicantDependance.Text = res.objAutoJTApplicant.NumberofDependents;
                lblJApplicantPassportIDTypeAndNo.Text = res.objAutoJTApplicant.IdentificationNumberType + " : " +
                                                        res.objAutoJTApplicant.IdentificationNumber;
                switch (res.objAutoJTApplicant.HighestEducation)
                {
                    case "SSC":
                        {
                            chkPApplicantSSC.Checked = true;
                            break;
                        }
                    case "HSC":
                        {
                            chkPApplicantHSC.Checked = true;
                            break;
                        }
                    case "DIP":
                        {

                            break;
                        }
                    case "Graduate":
                        {
                            chkPApplicantGraduate.Checked = true;
                            break;
                        }
                    case "P-Graduate":
                        {
                            chkPApplicantPostGraduate.Checked = true;
                            break;
                        }
                    case "Others":
                        {
                            chkPApplicantOthers.Checked = true;
                            break;
                        }
                }

                lblJApplicantResidenceAddress.Text = res.objAutoJTApplicant.ResidenceAddress;
                lblJApplicantMailingAddress.Text = res.objAutoJTApplicant.MailingAddress;
                lblJApplicantParmanentAddress.Text = res.objAutoJTApplicant.PermanentAddress;
                lblJApplicantResidenceStatus.Text = res.objAutoJTApplicant.ResidenceStatus;
                lblJApplicantYearsInCurrentAddress.Text = res.objAutoJTApplicant.ResidenceStatusYear;
                lblJApplicantPhoneRes.Text = res.objAutoJTApplicant.PhoneResidence;
                lblJApplicantPhoneMobile.Text = res.objAutoJTApplicant.Mobile;
                lblJApplicantEmail.Text = res.objAutoJTApplicant.Email;
            }

            #endregion Joint Applicant

            #region Profession

            #region PrimaryProfession

            //primary profession

            //switch (res.objAutoPRProfession.PrimaryProfession.ToString())
            //{
              //  case "1":
                //    {
                        //Business

                        
                        

            var primaryProfessionName = "";
            
            for (var i = 0; i < objList_AutoProfession.Count;i++ )
            {
                if(objList_AutoProfession[i].ProfID == res.objAutoPRProfession.PrimaryProfession)
                {
                    primaryProfessionName = objList_AutoProfession[i].ProfName;
                    break;
                }
            }
            lblPApplicantProfessionBusinessPrimaryProfession.Text = primaryProfessionName;
            var otherProfessionName = "";
            for (var i = 0; i < objList_AutoProfession.Count; i++)
            {
                if (objList_AutoProfession[i].ProfID == res.objAutoPRProfession.OtherProfession)
                {
                    otherProfessionName = objList_AutoProfession[i].ProfName;
                    break;
                }
            }
            lblPApplicantProfessionBusinessOtherProfession.Text = otherProfessionName;


                lblPApplicantProfessionBusinessMonthInCurrentJob.Text =
                    res.objAutoPRProfession.MonthinCurrentProfession;
                        lblPApplicantProfessionBusinessNameOfOrganization.Text =
                            res.objAutoPRProfession.NameofOrganization.ToString();
                        lblPApplicantProfessionBusinessDesignation.Text = res.objAutoPRProfession.Designation;
                        lblPApplicantProfessionBusinessNatureOfBusiness.Text = res.objAutoPRProfession.NatureOfBussiness;
                        lblPApplicantProfessionBusinessYearsInBusiness.Text = res.objAutoPRProfession.YearsInBussiness;

            var officeStatus = "";
            switch(res.objAutoPRProfession.OfficeStatus)
            {
                case "1":
                    officeStatus = "Own";
                    break;
                case "2":
                    officeStatus = "Rented";
                    break;
            }
            lblPApplicantProfessionBusinessOfficeStatus.Text = officeStatus;
                        lblPApplicantProfessionBusinessOfficePhone.Text = res.objAutoPRProfession.OfficePhone;
            var ownershipType = "";
            switch(res.objAutoPRProfession.OwnerShipType)
            {
                case "1":
                    ownershipType = "Proprietary";
                    break;
                case "2":
                    ownershipType = "Partnership";
                    break;
                case "3":
                    ownershipType = "Private Ltd";
                    break;
                case "4":
                    ownershipType = "Public Ltd";
                    break;
            }
            lblPApplicantProfessionBusinessOwnershipType.Text = ownershipType;
                        lblPApplicantProfessionBusinessTotalExperience.Text =
                            res.objAutoPRProfession.TotalProfessionalExperience;
                        lblPApplicantProfessionBusinessNumberOfEmployee.Text =
                            res.objAutoPRProfession.NumberOfEmployees.ToString();
                        lblPApplicantProfessionBusinessEquity.Text = res.objAutoPRProfession.EquityShare.ToString();

                      //  break;
                    //}
                //case "2":
                  //  {
                        //Salaried
            lblPApplicantProfessionSalariedPrimaryProfession.Text = primaryProfessionName;
            lblPApplicantProfessionSalariedOtherProfession.Text = otherProfessionName;


                        
                        
                        lblPApplicantProfessionSalariedMonthInCurrentJob.Text =
                            res.objAutoPRProfession.MonthinCurrentProfession.ToString();
                        lblPApplicantProfessionSalariedNameOfCompany.Text =
                            res.objAutoPRProfession.NameofCompany.ToString();
                        lblPApplicantProfessionSalariedDesignation.Text = res.objAutoPRProfession.Designation;
                        lblPApplicantProfessionSalariedAddress.Text = res.objAutoPRProfession.Address;
                        lblPApplicantProfessionSalariedOfficePhone.Text = res.objAutoPRProfession.OfficePhone;

            var employssStatus = "";
            switch(res.objAutoPRProfession.EmploymentStatus)
            {
                case "1":
                    employssStatus = "Contractual";
                    break;
                case "2":
                    employssStatus = "Temporary";
                    break;
                case "3":
                    employssStatus = "Permanent";
                    break;
            }
            lblPApplicantProfessionSalariedEmployementStatus.Text = employssStatus;
                        lblPApplicantProfessionSalariedTotalExperience.Text =
                            res.objAutoPRProfession.TotalProfessionalExperience;
                //        break;
                //    }
                //case "3":
                //    {
                        //Self Employed

            lblPApplicantProfessionSelfPrimaryProfession.Text = primaryProfessionName;
                        lblPApplicantProfessionSelfOtherProfession.Text =otherProfessionName;
                        lblPApplicantProfessionSelfMonthInCurrentJob.Text =
                            res.objAutoPRProfession.MonthinCurrentProfession;
                        lblPApplicantProfessionSelfNameOfOrganization.Text = res.objAutoPRProfession.NameofOrganization;
                        lblPApplicantProfessionSelfDesignation.Text = res.objAutoPRProfession.Designation;
                        lblPApplicantProfessionSelfAddress.Text = res.objAutoPRProfession.Address;
                        lblPApplicantProfessionSelfOfficePhone.Text = res.objAutoPRProfession.OfficePhone;
                        lblPApplicantProfessionSelfTotalExperience.Text =
                            res.objAutoPRProfession.TotalProfessionalExperience;
                        lblPApplicantProfessionSelfPrimaryIncomeSource.Text = res.objAutoPRProfession.PrIncomeSource;
                        lblPApplicantProfessionSelfOtherIncomeSource.Text = res.objAutoPRProfession.OtherIncomeSource;

                //        break;
                //    }
                //case "4":
                //    {
                //        //Doctor
                //        break;
                //    }
                //case "5":
                //    {
                        //Landlord

                        lblPApplicantProfessionLandPrimaryProfession.Text =primaryProfessionName;
                        lblPApplicantProfessionLandOtherProfession.Text =otherProfessionName;
                        lblPApplicantProfessionLandMonthInCurrentJob.Text =
                            res.objAutoPRProfession.MonthinCurrentProfession;
                        lblPApplicantProfessionLandNameOfOrganization.Text = res.objAutoPRProfession.NameofOrganization;
                        lblPApplicantProfessionLandAddress.Text = res.objAutoPRProfession.Address;
                        lblPApplicantProfessionLandTotalExperience.Text =
                            res.objAutoPRProfession.TotalProfessionalExperience;
                        lblPApplicantProfessionLandNumberOfFloors.Text =
                            res.objAutoPRProfession.NoOfFloorsRented.ToString();
                        lblPApplicantProfessionLandNatureOfRentedFloor.Text =
                            res.objAutoPRProfession.NatureOfrentedFloors;
                        lblPApplicantProfessionLandRentedAreaInSFT.Text = res.objAutoPRProfession.RentedAresInSFT;
                        lblPApplicantProfessionLandConstructionCompletingYear.Text =
                            res.objAutoPRProfession.ConstructionCompletingYear;


                //        break;
                //    }
                //case "6":
                //    {
                //        //Others
                //        break;
                //    }
            //}

            #endregion PrimaryProfession

            #region JointProfession

            //switch (res.objAutoJTProfession.PrimaryProfession.ToString())
            //{
            //    case "1":
            //        {
                        //Business

                        if (res.objAutoLoanMaster.JointApplication)
                        {

                            var JointPrProfessionName = "";

                            for (var i = 0; i < objList_AutoProfession.Count; i++)
                            {
                                if (objList_AutoProfession[i].ProfID == res.objAutoJTProfession.PrimaryProfession)
                                {
                                    JointPrProfessionName = objList_AutoProfession[i].ProfName;
                                    break;
                                }
                            }
                            var JointotherProfessionName = "";
                            for (var i = 0; i < objList_AutoProfession.Count; i++)
                            {
                                if (objList_AutoProfession[i].ProfID == res.objAutoJTProfession.OtherProfession)
                                {
                                    JointotherProfessionName = objList_AutoProfession[i].ProfName;
                                    break;
                                }
                            }

                            lblJApplicantProfessionBusinessPrimaryProfession.Text = JointPrProfessionName;
                            lblJApplicantProfessionBusinessOtherProfession.Text = JointotherProfessionName;
                            lblJApplicantProfessionBusinessMonthInCurrentJob.Text =
                                res.objAutoJTProfession.MonthinCurrentProfession;
                            lblJApplicantProfessionBusinessNameOfOrganization.Text =
                                res.objAutoJTProfession.NameofOrganization;
                            lblJApplicantProfessionBusinessDesignation.Text = res.objAutoJTProfession.Designation;
                            lblJApplicantProfessionBusinessNatureOfBusiness.Text = res.objAutoJTProfession.NatureOfBussiness;
                            lblJApplicantProfessionBusinessYearsInBusiness.Text = res.objAutoJTProfession.YearsInBussiness;
                            officeStatus = "";
                            switch (res.objAutoJTProfession.OfficeStatus)
                            {
                                case "1":
                                    officeStatus = "Own";
                                    break;
                                case "2":
                                    officeStatus = "Rented";
                                    break;
                            }
                            lblJApplicantProfessionBusinessOfficeStatus.Text = officeStatus;
                            lblJApplicantProfessionBusinessOfficePhone.Text = res.objAutoJTProfession.OfficePhone;
                            ownershipType = "";
                            switch (res.objAutoPRProfession.OwnerShipType)
                            {
                                case "1":
                                    ownershipType = "Proprietary";
                                    break;
                                case "2":
                                    ownershipType = "Partnership";
                                    break;
                                case "3":
                                    ownershipType = "Private Ltd";
                                    break;
                                case "4":
                                    ownershipType = "Public Ltd";
                                    break;
                            }
                            lblJApplicantProfessionBusinessOwnershipType.Text = ownershipType;
                            lblJApplicantProfessionBusinessTotalExperience.Text =
                                res.objAutoJTProfession.TotalProfessionalExperience;
                            lblJApplicantProfessionBusinessNumberOfEmployee.Text =
                                res.objAutoJTProfession.NumberOfEmployees.ToString();
                            lblJApplicantProfessionBusinessEquity.Text = res.objAutoJTProfession.EquityShare.ToString();

                            //        break;
                            //    }
                            //case "2":
                            //    {
                            //Salaried
                            lblJApplicantProfessionSalariedPrimaryProfession.Text = JointPrProfessionName;
                            lblJApplicantProfessionSalariedOtherProfession.Text = JointotherProfessionName;
                            lblJApplicantProfessionSalariedMonthInCurrentJob.Text =
                                res.objAutoJTProfession.MonthinCurrentProfession;
                            if (res.objAutoJTProfession.NameofCompany == null)
                            {
                                lblJApplicantProfessionSalariedNameOfCompany.Text = "";
                            }
                            else
                            {
                                lblJApplicantProfessionSalariedNameOfCompany.Text =
                                    res.objAutoJTProfession.NameofCompany.ToString();
                            }
                            lblJApplicantProfessionSalariedDesignation.Text = res.objAutoJTProfession.Designation;
                            lblJApplicantProfessionSalariedAddress.Text = res.objAutoJTProfession.Address;
                            lblJApplicantProfessionSalariedOfficePhone.Text = res.objAutoJTProfession.OfficePhone;

                            var employssStatusJt = "";
                            switch (res.objAutoJTProfession.EmploymentStatus)
                            {
                                case "1":
                                    employssStatusJt = "Contractual";
                                    break;
                                case "2":
                                    employssStatusJt = "Temporary";
                                    break;
                                case "3":
                                    employssStatusJt = "Permanent";
                                    break;
                            }
                            lblJApplicantProfessionSalariedEmployementStatus.Text = employssStatusJt;
                            lblJApplicantProfessionSalariedTotalExperience.Text =
                                res.objAutoJTProfession.TotalProfessionalExperience;
                            //        break;
                            //    }
                            //case "3":
                            //    {
                            //Self Employed

                            lblJApplicantProfessionSelfPrimaryProfession.Text = JointPrProfessionName;
                            lblJApplicantProfessionSelfOtherProfession.Text = JointotherProfessionName;
                            lblJApplicantProfessionSelfMonthInCurrentJob.Text =
                                res.objAutoJTProfession.MonthinCurrentProfession;
                            lblJApplicantProfessionSelfNameOfOrganization.Text = res.objAutoJTProfession.NameofOrganization;
                            lblJApplicantProfessionSelfDesignation.Text = res.objAutoJTProfession.Designation;
                            lblJApplicantProfessionSelfAddress.Text = res.objAutoJTProfession.Address;
                            lblJApplicantProfessionSelfOfficePhone.Text = res.objAutoJTProfession.OfficePhone;
                            lblJApplicantProfessionSelfTotalExperience.Text =
                                res.objAutoJTProfession.TotalProfessionalExperience;
                            lblJApplicantProfessionSelfPrimaryIncomeSource.Text = res.objAutoJTProfession.JtIncomeSource;
                            lblJApplicantProfessionSelfOtherIncomeSource.Text = res.objAutoJTProfession.OtherIncomeSource;

                            //        break;
                            //    }
                            //case "4":
                            //    {
                            //        //Doctor
                            //        break;
                            //    }
                            //case "5":
                            //    {
                            //Landlord

                            lblJApplicantProfessionLandPrimaryProfession.Text = JointPrProfessionName;
                            lblJApplicantProfessionLandOtherProfession.Text = JointotherProfessionName;
                            lblJApplicantProfessionLandMonthInCurrentJob.Text =
                                res.objAutoJTProfession.MonthinCurrentProfession;
                            lblJApplicantProfessionLandNameOfOrganization.Text = res.objAutoJTProfession.NameofOrganization;
                            lblJApplicantProfessionLandAddress.Text = res.objAutoJTProfession.Address;
                            lblJApplicantProfessionLandTotalExperience.Text =
                                res.objAutoJTProfession.TotalProfessionalExperience;
                            lblJApplicantProfessionLandNumberOfFloors.Text =
                                res.objAutoJTProfession.NoOfFloorsRented.ToString();
                            lblJApplicantProfessionLandNatureOfRentedFloor.Text =
                                res.objAutoJTProfession.NatureOfrentedFloors;
                            lblJApplicantProfessionLandRentedAreaInSFT.Text = res.objAutoJTProfession.RentedAresInSFT;
                            lblJApplicantProfessionLandConstructionCompletingYear.Text =
                                res.objAutoJTProfession.ConstructionCompletingYear;


                            //            break;
                            //        }
                            //    case "6":
                            //        {
                            //            //Others
                            //            break;
                            //        }
                            //}
                        }

            #endregion JointProfession

            #endregion profession


            #region Financial Information

            lblPDeclaredPrimaryIncomeSource.Text = res.objAutoPRFinance.DPISource;
            lblPDeclaredPrimaryIncomeAmount.Text = res.objAutoPRFinance.DPIAmount.ToString();
            lblPDeclaredOtherIncomeSource.Text = res.objAutoPRFinance.DOISource;
            lblPDeclaredOtherIncomeAmount.Text = res.objAutoPRFinance.DOIAmount.ToString();
            lblPExpanceTypeRentUtilities.Text = res.objAutoPRFinance.EDRentAndUtilities.ToString();
            lblPExpanceTypeFoodClothing.Text = res.objAutoPRFinance.EDFoodAndClothing.ToString();
            lblPExpanceTypeEducation.Text = res.objAutoPRFinance.EDEducation.ToString();
            lblPExpanceTypeLoanRepayment.Text = res.objAutoPRFinance.EDLoanRePayment.ToString();
            lblPExpanceTypeOther.Text = res.objAutoPRFinance.EDOther.ToString();

            if (res.objAutoLoanMaster.JointApplication)
            {
                lblJDeclaredPrimaryIncomeSource.Text = res.objAutoJTFinance.DPISource;
                lblJDeclaredPrimaryIncomeAmount.Text = res.objAutoJTFinance.DPIAmount.ToString();
                lblJDeclaredOtherIncomeSource.Text = res.objAutoJTFinance.DOISource;
                lblJDeclaredOtherIncomeAmount.Text = res.objAutoJTFinance.DOIAmount.ToString();
                lblJExpanceTypeRentUtilities.Text = res.objAutoJTFinance.EDRentAndUtilities.ToString();
                lblJExpanceTypeFoodClothing.Text = res.objAutoJTFinance.EDFoodAndClothing.ToString();
                lblJExpanceTypeEducation.Text = res.objAutoJTFinance.EDEducation.ToString();
                lblJExpanceTypeLoanRepayment.Text = res.objAutoJTFinance.EDLoanRePayment.ToString();
                lblJExpanceTypeOther.Text = res.objAutoJTFinance.EDOther.ToString();
            }


            #endregion Financial Information


            #region Vehicle information

            ITanorAndLTV repositorym = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repositorym);


            List<AutoMenufacturer> objAutoManufacturer = autoManufactuerService.GetAutoManufacturer();
            var manufactureName = "";

            for (var i = 0; i < objAutoManufacturer.Count;i++ )
            {
                if(objAutoManufacturer[i].ManufacturerId == res.objAutoLoanVehicle.Auto_ManufactureId)
                {
                    manufactureName = objAutoManufacturer[i].ManufacturerName;
                    break;
                }
            }

            lblVehicleManufacturar.Text = manufactureName;

            switch (res.objAutoLoanVehicle.VehicleStatus.ToString())
            {
                case "1":
                    {
                        chkVehicleStatusNew.Checked = true;
                        break;
                    }
                case "2":
                    {
                        chkVehicleStatusRecondition.Checked = true;
                        break;
                    }
                case "3":
                    {
                        chkVehicleStatusUsed.Checked = true;
                        break;
                    }
            }
            lblVehicleCarNameTrim.Text = res.objAutoLoanVehicle.Model + " : " + res.objAutoLoanVehicle.TrimLevel;
            lblVehicleMfgYear.Text = res.objAutoLoanVehicle.ManufacturingYear.ToString();
            lblVehicleCCAndMileage.Text = res.objAutoLoanVehicle.EngineCC;
            lblVehicleCurrentMktValue.Text = res.objAutoLoanVehicle.VerifiedPrice.ToString();
            

            ILoanApplicationRepository repositoryv = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repositoryv);
            List<AutoVendor> objAutoVendorList = loanApplicationService.GetVendor();
            var vendoreName = "";
            var vendoreAddress = "";
            var vendoreMobile = "";
            for (var i = 0; i < objAutoVendorList.Count; i++ )
            {
                if(objAutoVendorList[i].AutoVendorId == res.objAutoLoanVehicle.VendorId)
                {
                    vendoreName = objAutoVendorList[i].VendorName;
                    vendoreAddress = objAutoVendorList[i].Address;
                    vendoreMobile = objAutoVendorList[i].Phone;
                    break;
                }
            }
            lblVehicleVendorDealerCode.Text = vendoreName;
            lblVehicleVendorContactPhone.Text = res.objAutoVendor.Phone;
            lblVehicleVendorAddress.Text = vendoreAddress;
            lblVehicleVendorContactMobile.Text = vendoreMobile;
            lblVehicleVendorContactPhone.Text = vendoreMobile;


            #endregion vehicle information


                #region referance

            lblReference1Name.Text = res.objAutoLoanReference.ReferenceName1 == "null" ? "" : res.objAutoLoanReference.ReferenceName1;
            lblReference2Name.Text = res.objAutoLoanReference.ReferenceName2 == "null" ? "" : res.objAutoLoanReference.ReferenceName2;
            lblReference1Relationship.Text = res.objAutoLoanReference.Relationship1 == "null" ? "" : res.objAutoLoanReference.Relationship1;
            lblReference2Relationship.Text = res.objAutoLoanReference.Relationship2 == "null" ? "" : res.objAutoLoanReference.Relationship2;

            
            
            var occupation1 = "";

            for (var i = 0; i < objList_AutoProfession.Count; i++)
            {
                if(objList_AutoProfession[i].ProfID.ToString() == res.objAutoLoanReference.Occupation1)
                {
                    occupation1 = objList_AutoProfession[i].ProfName;
                    break;
                }

            }
            lblReference1Occupation.Text = occupation1.ToString() == "null" ? "" : occupation1;

            

            var occupation2 = "";

            for (var i = 0; i < objList_AutoProfession.Count; i++)
            {
                if (objList_AutoProfession[i].ProfID.ToString() == res.objAutoLoanReference.Occupation2)
                {
                    occupation2 = objList_AutoProfession[i].ProfName;
                    break;
                }

            }
            lblReference2Occupation.Text = occupation2.ToString() == "null" ? "" : occupation2;

            lblReference1NameOfOrganization.Text = res.objAutoLoanReference.OrganizationName1 == "null" ? "" : res.objAutoLoanReference.OrganizationName1;
            lblReference2NameOfOrganization.Text = res.objAutoLoanReference.OrganizationName2 == "null" ? "" : res.objAutoLoanReference.OrganizationName2;
            lblReference1Designation.Text = res.objAutoLoanReference.Designation1 == "null" ? "" : res.objAutoLoanReference.Designation1;
            lblReference2Designation.Text = res.objAutoLoanReference.Designation2 == "null" ? "" : res.objAutoLoanReference.Designation2;
            lblReference1WorkAddress.Text = res.objAutoLoanReference.WorkAddress1 == "null" ? "" : res.objAutoLoanReference.WorkAddress1;
            lblReference2WorkAddress.Text = res.objAutoLoanReference.WorkAddress2 == "null" ? "" : res.objAutoLoanReference.WorkAddress2;
            lblReference1ResidentAddress.Text = res.objAutoLoanReference.ResidenceAddress1 == "null" ? "" : res.objAutoLoanReference.ResidenceAddress1;
            lblReference2ResidentAddress.Text = res.objAutoLoanReference.ResidenceAddress2 == "null" ? "" : res.objAutoLoanReference.ResidenceAddress2;
            lblReference1PhoneResidence.Text = res.objAutoLoanReference.PhoneNumber1 == "null" ? "" : res.objAutoLoanReference.PhoneNumber1;
            lblReference2PhoneResidence.Text = res.objAutoLoanReference.PhoneNumber2 == "null" ? "" : res.objAutoLoanReference.PhoneNumber2;
            lblReference1PhoneMobile.Text = res.objAutoLoanReference.MobileNumber1 == "null" ? "" : res.objAutoLoanReference.MobileNumber1;
            lblReference2PhoneMobile.Text = res.objAutoLoanReference.MobileNumber2 == "null" ? "" : res.objAutoLoanReference.MobileNumber2;


            #endregion referance



            #region start FacilityType

            //var a = res.objAutoLoanFacilityList[0].FacilityTypeName;

            double PresentBalanceTotal = 0;
            int facilityLableID = 1;

            for (int f = 0; f < res.objAutoLoanFacilityList.Count; f++)
            {
                #region Old Code comment
                //if (f == 0)
                //{
                //    lblFacilityScheduleFacilityType1.Text = res.objAutoLoanFacilityList[f].FacilityTypeName;
                //    lblFacilityScheduleInterestRate1.Text = res.objAutoLoanFacilityList[f].InterestRate.ToString();
                //    lblFacilitySchedulePresentBalance1.Text = res.objAutoLoanFacilityList[f].PresentBalance.ToString();
                //    lblFacilitySchedulePresentEMI1.Text = res.objAutoLoanFacilityList[f].PresentEMI.ToString();
                //    lblFacilitySchedulePresentLimit1.Text = res.objAutoLoanFacilityList[f].PresentLimit.ToString();
                //    lblFacilityScheduleProposedLimit1.Text = res.objAutoLoanFacilityList[f].ProposedLimit.ToString();
                //    lblFacilityScheduleRepaymentArrangement1.Text =
                //        res.objAutoLoanFacilityList[f].RepaymentAgrement.ToString();
                //}
                //else if (f == 1)
                //{
                //    lblFacilityScheduleFacilityType2.Text = res.objAutoLoanFacilityList[f].FacilityTypeName;
                //    lblFacilityScheduleInterestRate2.Text = res.objAutoLoanFacilityList[f].InterestRate.ToString();
                //    lblFacilitySchedulePresentBalance2.Text = res.objAutoLoanFacilityList[f].PresentBalance.ToString();
                //    lblFacilitySchedulePresentEMI2.Text = res.objAutoLoanFacilityList[f].PresentEMI.ToString();
                //    lblFacilitySchedulePresentLimit2.Text = res.objAutoLoanFacilityList[f].PresentLimit.ToString();
                //    lblFacilityScheduleProposedLimit2.Text = res.objAutoLoanFacilityList[f].ProposedLimit.ToString();
                //    lblFacilityScheduleRepaymentArrangement2.Text =
                //        res.objAutoLoanFacilityList[f].RepaymentAgrement.ToString();
                //}
                //else if (f == 2)
                //{
                //    lblFacilityScheduleFacilityType3.Text = res.objAutoLoanFacilityList[f].FacilityTypeName;
                //    lblFacilityScheduleInterestRate3.Text = res.objAutoLoanFacilityList[f].InterestRate.ToString();
                //    lblFacilitySchedulePresentBalance3.Text = res.objAutoLoanFacilityList[f].PresentBalance.ToString();
                //    lblFacilitySchedulePresentEMI3.Text = res.objAutoLoanFacilityList[f].PresentEMI.ToString();
                //    lblFacilitySchedulePresentLimit3.Text = res.objAutoLoanFacilityList[f].PresentLimit.ToString();
                //    lblFacilityScheduleProposedLimit3.Text = res.objAutoLoanFacilityList[f].ProposedLimit.ToString();
                //    lblFacilityScheduleRepaymentArrangement3.Text =
                //        res.objAutoLoanFacilityList[f].RepaymentAgrement.ToString();
                //}
                //else if (f == 3)
                //{
                //    lblFacilityScheduleFacilityType4.Text = res.objAutoLoanFacilityList[f].FacilityTypeName;
                //    lblFacilityScheduleInterestRate4.Text = res.objAutoLoanFacilityList[f].InterestRate.ToString();
                //    lblFacilitySchedulePresentBalance4.Text = res.objAutoLoanFacilityList[f].PresentBalance.ToString();
                //    lblFacilitySchedulePresentEMI4.Text = res.objAutoLoanFacilityList[f].PresentEMI.ToString();
                //    lblFacilitySchedulePresentLimit4.Text = res.objAutoLoanFacilityList[f].PresentLimit.ToString();
                //    lblFacilityScheduleProposedLimit4.Text = res.objAutoLoanFacilityList[f].ProposedLimit.ToString();
                //    lblFacilityScheduleRepaymentArrangement4.Text =
                //        res.objAutoLoanFacilityList[f].RepaymentAgrement.ToString();
                //}
                //else if (f == 4)
                //{
                //    lblFacilityScheduleFacilityType5.Text = res.objAutoLoanFacilityList[f].FacilityTypeName;
                //    lblFacilityScheduleInterestRate5.Text = res.objAutoLoanFacilityList[f].InterestRate.ToString();
                //    lblFacilitySchedulePresentBalance5.Text = res.objAutoLoanFacilityList[f].PresentBalance.ToString();
                //    lblFacilitySchedulePresentEMI5.Text = res.objAutoLoanFacilityList[f].PresentEMI.ToString();
                //    lblFacilitySchedulePresentLimit5.Text = res.objAutoLoanFacilityList[f].PresentLimit.ToString();
                //    lblFacilityScheduleProposedLimit5.Text = res.objAutoLoanFacilityList[f].ProposedLimit.ToString();
                //    lblFacilityScheduleRepaymentArrangement5.Text =
                //        res.objAutoLoanFacilityList[f].RepaymentAgrement.ToString();
                //}
                #endregion

                if (res.objAutoLoanFacilityList[f].Status == 1)
                {
                    Label FacilityScheduleFacilityType =
                        this.FindControl("lblFacilityScheduleFacilityType" + facilityLableID.ToString()) as Label;
                    FacilityScheduleFacilityType.Text = res.objAutoLoanFacilityList[f].FacilityTypeName;

                    Label FacilityScheduleInterestRate =
                        this.FindControl("lblFacilityScheduleInterestRate" + facilityLableID.ToString()) as Label;
                    FacilityScheduleInterestRate.Text = res.objAutoLoanFacilityList[f].InterestRate.ToString();

                    Label FacilitySchedulePresentBalance =
                        this.FindControl("lblFacilitySchedulePresentBalance" + facilityLableID.ToString()) as Label;
                    FacilitySchedulePresentBalance.Text = res.objAutoLoanFacilityList[f].PresentBalance.ToString();

                    Label FacilitySchedulePresentEMI =
                        this.FindControl("lblFacilitySchedulePresentEMI" + facilityLableID.ToString()) as Label;
                    FacilitySchedulePresentEMI.Text = res.objAutoLoanFacilityList[f].PresentEMI.ToString();

                    Label FacilitySchedulePresentLimit =
                        this.FindControl("lblFacilitySchedulePresentLimit" + facilityLableID.ToString()) as Label;
                    FacilitySchedulePresentLimit.Text = res.objAutoLoanFacilityList[f].PresentLimit.ToString();

                    Label FacilityScheduleProposedLimit =
                        this.FindControl("lblFacilityScheduleProposedLimit" + facilityLableID.ToString()) as Label;
                    FacilityScheduleProposedLimit.Text = res.objAutoLoanFacilityList[f].ProposedLimit.ToString();

                    Label FacilityScheduleRepaymentArrangement =
                        this.FindControl("lblFacilityScheduleRepaymentArrangement" + facilityLableID.ToString()) as Label;
                    FacilityScheduleRepaymentArrangement.Text =
                        res.objAutoLoanFacilityList[f].RepaymentAgrement.ToString();
                    facilityLableID += 1;
                }
            }
            //Inserting Blank row
            while (facilityLableID <= 9)
            {
                Label FacilityScheduleFacilityType =
                    this.FindControl("lblFacilityScheduleFacilityType" + facilityLableID.ToString()) as Label;
                if (string.IsNullOrEmpty(FacilityScheduleFacilityType.Text))
                {
                    FacilityScheduleFacilityType.Text = "-";
                }
                facilityLableID += 1;
            }

            #endregion FacilityType


            #region start Security Schedule

            string[] securityName =
                {
                    "Select", "FD (SCB)", "Ezee FD", "MSS (SCB)", "CESS (SCB)", "RFCD", "WEDB-SCB",
                    "WEDB-OTHER", "ICB Unit", "USD Bond DPB", "USD Bond DIB", "MORTGAGE SECURITY",
                    "CAR REGISTRATION+INSURANCE"
                };
            int SecurityLabelID = 1;

            for (int s = 0; s < res.objAutoLoanFacilityList.Count; s++)
            {
                #region Old code comment
                //if (s == 0)
                //{
                //    if (res.objAutoLoanSecurityList[s].NatureOfSecurity != -1)
                //    {
                //        lblSecurityScheduleNatureOfSecurity1.Text =
                //            securityName[res.objAutoLoanSecurityList[s].NatureOfSecurity];
                //    }
                //    lblSecurityScheduleIssuingOffice1.Text = res.objAutoLoanSecurityList[s].IssuingOffice;
                //    lblSecurityScheduleFaceValue1.Text = res.objAutoLoanSecurityList[s].FaceValue.ToString();
                //    lblSecurityScheduleXTV1.Text = res.objAutoLoanSecurityList[s].XTVRate.ToString();
                //    lblSecurityScheduleIssueDate1.Text = res.objAutoLoanSecurityList[s].IssueDate.ToString("MMM-dd-yyyy");
                //    lblSecurityScheduleInterest1.Text = res.objAutoLoanSecurityList[s].InterestRate.ToString();
                //}

                //else if (s == 1)
                //{
                //    if (res.objAutoLoanSecurityList[s].NatureOfSecurity != -1)
                //    {
                //        lblSecurityScheduleNatureOfSecurity2.Text =
                //            securityName[res.objAutoLoanSecurityList[s].NatureOfSecurity == -1 ? 0 : res.objAutoLoanSecurityList[s].NatureOfSecurity];
                //    }
                //    lblSecurityScheduleIssuingOffice2.Text = res.objAutoLoanSecurityList[s].IssuingOffice;
                //    lblSecurityScheduleFaceValue2.Text = res.objAutoLoanSecurityList[s].FaceValue.ToString();
                //    lblSecurityScheduleXTV2.Text = res.objAutoLoanSecurityList[s].XTVRate.ToString();
                //    lblSecurityScheduleIssueDate2.Text = res.objAutoLoanSecurityList[s].IssueDate.ToString("MMM-dd-yyyy");
                //    lblSecurityScheduleInterest2.Text = res.objAutoLoanSecurityList[s].InterestRate.ToString();
                //}

                //else if (s == 2)
                //{
                //    if (res.objAutoLoanSecurityList[s].NatureOfSecurity != -1)
                //    {
                //        lblSecurityScheduleNatureOfSecurity3.Text =
                //            securityName[res.objAutoLoanSecurityList[s].NatureOfSecurity];
                //    }
                //    lblSecurityScheduleIssuingOffice3.Text = res.objAutoLoanSecurityList[s].IssuingOffice;
                //    lblSecurityScheduleFaceValue3.Text = res.objAutoLoanSecurityList[s].FaceValue.ToString();
                //    lblSecurityScheduleXTV3.Text = res.objAutoLoanSecurityList[s].XTVRate.ToString();
                //    lblSecurityScheduleIssueDate3.Text = res.objAutoLoanSecurityList[s].IssueDate.ToString("MMM-dd-yyyy");
                //    lblSecurityScheduleInterest3.Text = res.objAutoLoanSecurityList[s].InterestRate.ToString();
                //}

                //else if (s == 3)
                //{
                //    if (res.objAutoLoanSecurityList[s].NatureOfSecurity != -1)
                //    {
                //        lblSecurityScheduleNatureOfSecurity4.Text =
                //            securityName[res.objAutoLoanSecurityList[s].NatureOfSecurity];
                //    }
                //    lblSecurityScheduleIssuingOffice4.Text = res.objAutoLoanSecurityList[s].IssuingOffice;
                //    lblSecurityScheduleFaceValue4.Text = res.objAutoLoanSecurityList[s].FaceValue.ToString();
                //    lblSecurityScheduleXTV4.Text = res.objAutoLoanSecurityList[s].XTVRate.ToString();
                //    lblSecurityScheduleIssueDate4.Text = res.objAutoLoanSecurityList[s].IssueDate.ToString("MMM-dd-yyyy");
                //    lblSecurityScheduleInterest4.Text = res.objAutoLoanSecurityList[s].InterestRate.ToString();
                //}
                #endregion

                if (res.objAutoLoanFacilityList[s].Status == 1)
                {
                    if (res.objAutoLoanFacilityList[s].NatureOfSecurity != -1)
                    {
                        Label SecurityScheduleNatureOfSecurity =
                            this.FindControl("lblSecurityScheduleNatureOfSecurity" + SecurityLabelID.ToString()) as
                            Label;
                        SecurityScheduleNatureOfSecurity.Text =
                            securityName[res.objAutoLoanFacilityList[s].NatureOfSecurity];
                    }

                    Label SecurityScheduleIssuingOffice =
                        this.FindControl("lblSecurityScheduleIssuingOffice" + SecurityLabelID.ToString()) as
                        Label;
                    SecurityScheduleIssuingOffice.Text = "-";

                    Label SecurityScheduleFaceValue =
                        this.FindControl("lblSecurityScheduleFaceValue" + SecurityLabelID.ToString()) as
                        Label;
                    SecurityScheduleFaceValue.Text = res.objAutoLoanFacilityList[s].FaceValue.ToString();

                    Label SecurityScheduleXTV =
                        this.FindControl("lblSecurityScheduleXTV" + SecurityLabelID.ToString()) as
                        Label;
                    SecurityScheduleXTV.Text = res.objAutoLoanFacilityList[s].XTVRate.ToString();

                    Label SecurityScheduleIssueDate =
                        this.FindControl("lblSecurityScheduleIssueDate" + SecurityLabelID.ToString()) as
                        Label;
                    SecurityScheduleIssueDate.Text = "-";

                    Label SecurityScheduleInterest =
                        this.FindControl("lblSecurityScheduleInterest" + SecurityLabelID.ToString()) as
                        Label;
                    SecurityScheduleInterest.Text = res.objAutoLoanFacilityList[s].InterestRate.ToString();

                    SecurityLabelID += 1;
                }
            }

            #endregion Security Schedule

        }








    }
}
