﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Xml;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class BankStatement : System.Web.UI.Page
    {

        private string m_SpreadSheet;
        private string llid = string.Empty;
        private XmlDocument xmlDoc = new XmlDocument();


        protected void Page_PreInit(object sender, EventArgs e)
        {
            //string callingFrom = Request.QueryString["CallFrom"];
            if (Session["MasterPageUrlForBankStatement"] != null)
            {
                this.MasterPageFile = Session["MasterPageUrlForBankStatement"].ToString();
            }

        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public int GetUserType()
        {
            var userType = Convert.ToInt32(Session["AutoUserType"].ToString());
            return userType;
        }

        [AjaxNamespace("BankStatement")]
        protected void Page_Load(object sender, EventArgs e)
        {
            
            Utility.RegisterTypeForAjax(typeof (BankStatement));


            #region bank statement

            AutoPRBankStatement autoPRBankStatement = new AutoPRBankStatement();
            AutoJTBankStatement autoJTBankStatement = new AutoJTBankStatement();
            try
            {
                string stype = string.Empty;
                string updatedBy = string.Empty;
                string strOperation = string.Empty;
                strOperation = Request.QueryString["operation"];
                llid = Request.QueryString["llid"];
                string accountType = Request.QueryString["applicantType"];
                string prBID = Request.QueryString["prBID"];
                int oldBankID = Convert.ToInt32(Request.QueryString["oldBankID"]);
                string oldAccountNO = Request.QueryString["oldAccountNO"];

                string jsonAllObject = Request.QueryString["jsonObj"];

                if (jsonAllObject == null || jsonAllObject == "")
                {
                }
                else
                {
                    if (accountType == "JointAVG")
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer serializer =
                            new System.Web.Script.Serialization.JavaScriptSerializer();
                        autoJTBankStatement = serializer.Deserialize<AutoJTBankStatement>(jsonAllObject);
                    }
                    else
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer serializer =
                            new System.Web.Script.Serialization.JavaScriptSerializer();
                        autoPRBankStatement = serializer.Deserialize<AutoPRBankStatement>(jsonAllObject);
                    }
                }


                if (strOperation == "save")
                {
                    var streamReader = new System.IO.StreamReader(Request.InputStream);
                    var spreadsheetdata = streamReader.ReadToEnd(); //'' this needs to be updated in teh database

                    if (accountType == "JointAVG")
                    {
                        string result = SaveTemplate(spreadsheetdata, accountType, autoJTBankStatement, oldBankID, oldAccountNO);
                        //Response.End();
                        //Response.Write("<script language=\"javascript\">");
                        //Response.Write("alert('Hello World!');");
                        //Response.Write("return false;");
                        //Response.Write("</script>");
                        Response.End();
                    }
                    else
                    {
                        string result = SaveTemplate(spreadsheetdata, accountType, autoPRBankStatement, oldBankID, oldAccountNO);
                        this.Page.ClientScript.RegisterClientScriptBlock(typeof (Page), "test Title", "alert('test fsdfsdfs');", false);
                        //Response.End();
                        //Response.Write("<script language=\"javascript\">");
                        //Response.Write("alert('Hello World!');");
                        //Response.Write("return false;");
                        //Response.Write("</script>");
                        //Response.End();
                    }
                }
                else if (strOperation == "loadTemplate")
                {
//                    Response.Write(GetTemplate(autoPRBankStatement, accountType));
                    //  Response.End();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            #endregion bank statement


        }

        private string SaveTemplate(string spreadsheetdata, string accountType, AutoJTBankStatement autoJTBankStatement, int oldBankID, string oldAccountNO)
        {
            string res = "";
            List<Auto_BankStatementAvgTotal> auto_BankStatementAvgTotalList = new List<Auto_BankStatementAvgTotal>();
            if (accountType == "JointAVG")
            {
                try
                {
                    //Session["PRspreadsheetDataAVG"] = spreadsheetdata;

                    XmlNodeList msgDataNodeList;
                    string parseData = ParseValue(spreadsheetdata, "ss:Table");
                    parseData = parseData.Replace("ss:table", "Table").Replace("ss:", "");
                    xmlDoc.LoadXml(parseData);

                    //DataSet ds = new DataSet();
                    ////ds.ReadXml(parseData);
                    //StringReader reader = new StringReader(parseData);
                    //ds.ReadXml(reader);

                    msgDataNodeList = xmlDoc.SelectNodes("//Row");


                    #region geting data about the AVGTotal/TurnOver Total

                    try
                    {
                        int rowIndex = 4;

                        #region row index = 4

                        if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 4;
                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 2;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }

                        }
                        #endregion row index = 4

                        #region if row index = 5

                        else if (msgDataNodeList.Item(rowIndex + 1).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 1).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 1).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 5;
                            #region set the column index
                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 2;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }
                        #endregion if row index = 5

                        #region if row idex = 6

                        else if (msgDataNodeList.Item(rowIndex + 2).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 2).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 2).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 6;

                            #region set the column index

                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 2;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }
                        #endregion if row index = 6

                        #region if row index = 7

                        else if (msgDataNodeList.Item(rowIndex + 3).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 3).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 3).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 7;

                            #region set the column index

                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 2;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }
                        #endregion if row index = 7

                        #region if row index = 8

                        else if (msgDataNodeList.Item(rowIndex + 4).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 4).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 4).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 8;

                            #region set the column index

                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 2;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }

                        #endregion if row index = 8

                        #region if row index = 9
                        else if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 9;

                            #region set the column index

                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 2;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }
                        #endregion if row index = 9

                        //var a = msgDataNodeList.Item(11).NextSibling.ChildNodes.Item(7).InnerText;
                    }
                    catch (Exception ex)
                    {
                        // need to code
                    }

                    #endregion AVGTotal/TurnOver Total

                    try
                    {
                        autoJTBankStatement.Statement = spreadsheetdata;

                        if (auto_BankStatementAvgTotalList.Count == 12)
                        {
                            //string data = msgDataNodeList[1].SelectNodes("//Data")[12].InnerText;
                            string data = auto_BankStatementAvgTotalList[11].Month.ToString();
                            string dateFrom = auto_BankStatementAvgTotalList[0].Month.ToString();
                            string dateTo = auto_BankStatementAvgTotalList[11].Month.ToString();

                            autoJTBankStatement.StatementDateFrom = Convert.ToDateTime(dateFrom);
                            autoJTBankStatement.StatementDateTo = Convert.ToDateTime(dateTo);
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    IBankStatementRepository repository = new AutoLoanDataService.AutoLoanDataService();
                    var bankStatementService = new BankStatementService(repository);
                    res = bankStatementService.SaveJointBankStatement(autoJTBankStatement, auto_BankStatementAvgTotalList, oldBankID, oldAccountNO);
                }
                catch (Exception ex)
                {

                }

            }
            return res;
        }

        private object JavaScriptDeserializer(string jsonAllObject)
        {
            throw new NotImplementedException();
        }

        #region bank statement
        //for save bank statement

        //private string GetTemplate(AutoPRBankStatement autoPrBankStatement, string accountType)
        //{
        //    // show bank statement

        //    string sReturnData = string.Empty;
        //    ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
        //    var loanApplicationService = new LoanApplicationService(repository);

        //    if (accountType == "primary")
        //    {
        //        sReturnData = loanApplicationService.GetPRBankStatementData(llid);
        //    }
        //    else if (accountType == "joint")
        //    {
        //        sReturnData = loanApplicationService.GetJTBankStatementData(llid);

        //    }

        //    return sReturnData;
        //}

        private string SaveTemplate(string spreadsheetdata, string SavePRTemplate, AutoPRBankStatement autoPrBankStatement, int oldBankID, string oldAccountNO)
        {
            string res = "";
            List<Auto_BankStatementAvgTotal> auto_BankStatementAvgTotalList = new List<Auto_BankStatementAvgTotal>();

            if (SavePRTemplate == "primaryAVG")
            {
                try
                {
                    //Session["PRspreadsheetDataAVG"] = spreadsheetdata;

                    XmlNodeList msgDataNodeList;
                    string parseData = ParseValue(spreadsheetdata, "ss:Table");
                    parseData = parseData.Replace("ss:table", "Table").Replace("ss:", "");
                    xmlDoc.LoadXml(parseData);
                    msgDataNodeList = xmlDoc.SelectNodes("//Row");



                    #region geting data about the AVGTotal/TurnOver Total

                    try
                    {
                        int rowIndex = 4;

                        #region row index = 4

                        if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 4;
                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 1;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }

                        }
                        #endregion row index = 4

                        #region if row index = 5

                        else if (msgDataNodeList.Item(rowIndex + 1).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 1).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 1).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 5;
                            #region set the column index
                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 1;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }
                        #endregion if row index = 5

                        #region if row idex = 6

                        else if (msgDataNodeList.Item(rowIndex + 2).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 2).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 2).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 6;

                            #region set the column index

                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 1;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }
                        #endregion if row index = 6

                        #region if row index = 7

                        else if (msgDataNodeList.Item(rowIndex + 3).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 3).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 3).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 7;

                            #region set the column index

                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 1;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }
                        #endregion if row index = 7

                        #region if row index = 8

                        else if (msgDataNodeList.Item(rowIndex + 4).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 4).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 4).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 8;

                            #region set the column index

                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 1;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }

                        #endregion if row index = 8

                        #region if row index = 9
                        else if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL" ||
                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                        {
                            rowIndex = 9;

                            #region set the column index

                            int colIndex = 0;
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(0).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 0;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(1).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 1;
                            }
                            if (msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(2).InnerText.ToUpper() == "TOTAL")
                            {
                                colIndex = 2;
                            }

                            #endregion set the column index

                            for (int i = colIndex + 1; i < msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Count; i++)
                            {
                                Auto_BankStatementAvgTotal auto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();



                                #region geting month/date
                                if (msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(0).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(1).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "INPUT")
                                {
                                    auto_BankStatementAvgTotal.Month = Convert.ToDateTime(msgDataNodeList.Item(2).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    //need to do something to show a message.
                                }
                                #endregion geting month/date

                                auto_BankStatementAvgTotal.Type = 1;

                                auto_BankStatementAvgTotal.AVGTotal =
                                    string.IsNullOrEmpty(
                                        msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText)
                                        ? 0
                                        : Convert.ToInt32(
                                            msgDataNodeList.Item(rowIndex).NextSibling.ChildNodes.Item(i).InnerText);

                                #region geting Credit TurnOver Total
                                if (msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    //auto_BankStatementAvgTotal.TrunOverTotal = Convert.ToInt32(msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(colIndex).InnerText);
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 5).NextSibling.ChildNodes.Item(i).InnerText);

                                }

                                else if (msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 6).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else if (msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(colIndex).InnerText.ToUpper() == "TOTAL")
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = string.IsNullOrEmpty(
                                                                        msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText)
                                                                        ? 0
                                                                        : Convert.ToInt32(
                                                                            msgDataNodeList.Item(rowIndex + 7).NextSibling.ChildNodes.Item(i).InnerText);
                                }
                                else
                                {
                                    auto_BankStatementAvgTotal.TrunOverTotal = 0;
                                }
                                #endregion Credit TurnOver Total


                                auto_BankStatementAvgTotalList.Add(auto_BankStatementAvgTotal);

                                if (auto_BankStatementAvgTotalList.Count == 12)
                                {
                                    break;
                                }
                            }
                        }
                        #endregion if row index = 9

                        //var a = msgDataNodeList.Item(11).NextSibling.ChildNodes.Item(7).InnerText;
                    }
                    catch (Exception ex)
                    {
                        // need to code
                    }

                    #endregion AVGTotal/TurnOver Total





                    try
                    {
                        autoPrBankStatement.Statement = spreadsheetdata;

                        //string data = msgDataNodeList[1].SelectNodes("//Data")[12].InnerText;
                        //string dateFrom = msgDataNodeList[1].SelectNodes("//Data")[1].InnerText;
                        //string dateTo = msgDataNodeList[1].SelectNodes("//Data")[12].InnerText;
                        if (auto_BankStatementAvgTotalList.Count == 12)
                        {
                            string data = auto_BankStatementAvgTotalList[11].Month.Date.ToString();
                            string dateFrom = auto_BankStatementAvgTotalList[0].Month.Date.ToString();
                            string dateTo = auto_BankStatementAvgTotalList[11].Month.Date.ToString();

                            autoPrBankStatement.StatementDateFrom = Convert.ToDateTime(dateFrom);
                            autoPrBankStatement.StatementDateTo = Convert.ToDateTime(dateTo);
                        }
                    }
                    catch (Exception)
                    {

                    }


                    IBankStatementRepository repository = new AutoLoanDataService.AutoLoanDataService();
                    var bankStatementService = new BankStatementService(repository);
                    res = bankStatementService.SavePrimaryBankStatement(autoPrBankStatement, auto_BankStatementAvgTotalList, oldBankID, oldAccountNO);
                }
                catch (Exception ex)
                {

                }

            }
            return res;
        }

        public string ParseValue(string xmlString, string xmlTag)
        {
            string TagValue = null;
            string StringXML = xmlString;
            string TagXML = xmlTag;
            xmlString = xmlString.ToUpper();
            string StartTag = xmlTag.ToUpper();
            string EndTag = "/" + xmlTag.ToUpper();
            int j = 0;
            int i = xmlString.IndexOf(StartTag, 0);
            if (i >= 0)
                j = xmlString.IndexOf(EndTag, 0);
            if (i >= 0 && j >= i)
            {
                i = i + StartTag.Length;
                TagValue = "<" + StartTag.ToLower() + " " + StringXML.Substring(i, j - i) + EndTag.ToLower() + ">";
            }
            else
            {
                TagValue = "";
            }
            return TagValue;
        }



        // for save bank statement end
        #endregion bank statement

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoGetBankStatement> GetBankStatement(string llid)
        {
            // show bank statement
            var sReturnData = new List<AutoGetBankStatement>();
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);

            sReturnData = loanApplicationService.getBankStatement(llid);


            return sReturnData;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBank> GetBank(int status)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBank(status);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranch> GetBranch(int bankId)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBranch(bankId);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SavePrimaryBankStatement(AutoPRBankStatement autoPRBankStatement)
        {
            IBankStatementRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var bankStatementService = new BankStatementService(repository);
            var res = bankStatementService.SavePrimaryBankStatement(autoPRBankStatement);
            return res;
            //return "Success";

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveJointBankStatement(AutoJTBankStatement AutojtBankStatement)
        {
            IBankStatementRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var bankStatementService = new BankStatementService(repository);
            var res = bankStatementService.SaveJointBankStatement(AutojtBankStatement);
            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPRBankStatement> GetPRBankStatementData(string llid)
        {
            IBankStatementRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var bankStatementService = new BankStatementService(repository);
            var res = bankStatementService.GetPRBankStatementData(llid);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoJTBankStatement> GetJTBankStatementData(string llid)
        {
            IBankStatementRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var bankStatementService = new BankStatementService(repository);
            var res = bankStatementService.GetJTBankStatementData(llid);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<SearchParantBankStatementInformation> SearchParantBankStatementInformationByLLID(int llid)
        {
            List<SearchParantBankStatementInformation> res = new List<SearchParantBankStatementInformation>();

            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                //return res;
            }
            else
            {
                IBankStatementRepository repository = new AutoLoanDataService.AutoLoanDataService();
                var bankStatementService = new BankStatementService(repository);
                res = bankStatementService.SearchParantBankStatementInformationByLLID(llid, statePermission);
            }
            return res;
        }

    }
}