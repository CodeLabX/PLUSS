﻿<%@ Page Title="SCB | Auto | Source Settings" Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.BranchNameWithSource" Codebehind="BranchNameWithSource.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <script src="../../Scripts/AutoLoan/AutoBranceAndSource.js" type="text/javascript"></script>
    
    
        <style type="text/css">
        #nav_BranchNameWithSource a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    <div class ="pageHeaderDiv ">Source Settings</div>
    <div id="Content">
    <div class="SearchwithPager">
  

    <div class="search searchwithUpload">
    <input type="text" id="txtSearch" class="txt txtSearch widthSize30_per"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search"/>
		
		<asp:FileUpload ID="fileDocumentUpload" class="txt txtUpload" runat="server" />
        <asp:Button ID="btnUploadSave" runat="server" Text="Upload" 
             onclick="btnUploadSave_Click" UseSubmitBehavior="False" />

    </div> 


<div id="Pager" class="pager"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
    </div>
    <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					
					<col width="200px" />
					<col width="350px" />
					<col width="100px" />
					<col width="150px" />
				</colgroup>
				<thead>
					<tr>
						
						<th>
						    Branch Name
						</th>
						<th>
						    Source Name
						</th>
						<th>
						    Source Code
						</th>
						<th> <a id="lnkAddNewSource" class="iconlink iconlinkAdd" href="javascript:;">New Source </a>
						</th>
					</tr>
				</thead>
				<tbody id="branchAndSourceList">
				</tbody>
			</table>
			
    </div>
    
    <div class="modal" id="SourcePopupDiv" style="display:none; height:auto; width: 522px;">
		<h3>Source Settings</h3>
		<div id="branchNameWithCombo">
		    <label for="labelForbranchName">Branch Name</label> 
		    <select id="cmbbranchName" class="txt" title="Branch Name" >
		    </select>
		    <a id="lnkAddNewbranch" class="iconlink iconlinkAdd" href="javascript:;" style="color: #0680CB;">New Branch </a>
		</div>
		<div id="branchNameWithtextbox">
		    <label for="labelForbranchName">Branch Name</label> 
		    <input class="txt" id="txtbranchName" title="Branch Name" disabled="disabled"/><br/>
		</div>
		
		<label for="labelForSourceName">Source Name</label>
		<input class="txt" id="txtSourceName" title="Source Name"/>
		 <br/>
		 <label for="labelForSourceCode">Source Code</label>
		 <input class="txt" id="txtSourceCode" title="Source Code" maxlength="3"/>
		 <br/>
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
</asp:Content>

