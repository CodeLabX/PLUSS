﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.IncomeAssementPrint" Codebehind="IncomeAssementPrint.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pluss | Income Assement</title>
    <link href="../../CSS/Default.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/admin.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/TableLayoutForTabControls.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/JSLibs/prototype.js" type="text/javascript"></script>
    <script src="../../Scripts/JSLibs/effects.js" type="text/javascript"></script>
    <script src="../../Scripts/JSLibs/util.js" type="text/javascript"></script>
    <script src="../../Scripts/JSLibs/Pager.js" type="text/javascript"></script>
    <script src="../../Scripts/JSLibs/scriptaculous.js" type="text/javascript"></script>
    <script src="../../Scripts/JSLibs/calendar.js" type="text/javascript"></script>
    <script src="../../Scripts/JSLibs/calendar-en.js" type="text/javascript"></script>
    <script src="../../Scripts/JSLibs/calendar-setup.js" type="text/javascript"></script>

    <script src="../../Scripts/JSLibs/json2.js" type="text/javascript"></script>
    <script src="../../Scripts/AutoLoan/AnalystHtmlHelper.js" type="text/javascript"></script>
    <script src="../../Scripts/AutoLoan/IncomeAssementPrint.js" type="text/javascript"></script>
    
    <link rel="stylesheet" type="text/css" href="../../CSS/TableLayoutForTabControls.css" />
   
    <link href="../../CSS/LoanApplicationTabContent.css" rel="stylesheet" type="text/css" />
    
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="maindivContent">
        <div style="width: auto; height: 20px;">
                <asp:Image ID="Image1" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                    Width="912px" />
            </div>
        <div class="divHeaderTitel">
            <h1 style="padding-top: 5px;*padding-top: 0px; *padding-bottom:10px; color: white;">
                <label id="lblLoan">
                    Loan</label>
                Application Assessment</h1>
            <input id="analystMasterIdHiddenField" type="hidden" />
        </div>
        <div style="width: 100%; *width: 100%; _width: 100%; border: solid 1px black; margin-bottom: 5px;">
            <div id="divLoanAppMasterData" class="fieldDiv divPadding formBackColor" style="margin-bottom: 0;">
                <div class="fieldDiv">
                    <label class="lblBig" style="font-size: medium; font-weight: bold; width: 200px;color: #333;">
                        CUSTOMER DETAILS</label>
                    <label class="lblBig" style="font-size: medium; font-weight: bold; text-align: right;">
                        LLID:</label>
                    <input type="text" id="txtLLID" class="txtField" runat="server" disabled="disabled" />
                    <%--<asp:TextBox ID="txtLLID" runat="server" CssClass="txtField" Enabled=false></asp:TextBox>--%>
                    <label id="spnWait" runat="server" style="color: red; font-weight: bold;">
                    </label>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="divLeft" class="divLeftBig" style="margin-left: 5px;">
            <div class="fieldDiv">
                <label class="lbl">
                    Primary Applicant:</label>
                <input id="txtPrimaryApplicant" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Date of Birth:</label>
                <input id="txtDateOfBirth" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Current Age:</label>
                <input id="txtCurrenAge" type="text" class="txtFieldBig" disabled="disabled" /><br />
                <label id="lblFieldrequiredforPrAge" style="color: Red; font-weight: bold;">
                </label>
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Primary Profession:</label>
                <input id="txtPrimaryProfession" type="text" class="txtFieldBig" disabled="disabled" />
                <input id="txtPrProfessionId" type="hidden" />
                <input id="txtPrOtherProfessionId" type="hidden" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Other Profession:</label>
                <input id="txtOtherProfession" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Declared Income:</label>
                <input id="txtDeclaredIncome" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl" style="height: 50px; *height: 50px; _height: 50px; text-align: left;
                    vertical-align: top;">
                    Verified Address:</label>
                <textarea id="txtVerifiedAddress" class="txtFieldBig" cols="1" rows="3" style="height: 58px;
                    _height: 58px; *height: 58px;" disabled="disabled"></textarea>
            </div>
            <div class="clear">
            </div>
        </div>
        <div id="divForJointApplicant" class="divRightBig" style="margin-left: 5px;">
            <div class="fieldDiv">
                <label class="lbl">
                    Joint Applicant:</label>
                <input id="txtJointApplicant" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Date of Birth:</label>
                <input id="txtJtDateOfBirth" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Current Age:</label>
                <input id="txtJtCurrntAge" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Profession:</label>
                <input id="txtJtProfession" type="text" class="txtFieldBig" disabled="disabled" />
                <input id="txtJtProfessionId" type="hidden" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Declared Income:</label>
                <input id="txtJtDeclaredIncome" type="text" class="txtFieldBig" disabled="disabled" />
            </div>
            <div class="fieldDiv">
                <label class="lbl">
                    Reason of Joint Applicant:</label>
                <select id="cmbReasonofJointApplicant" class="txtFieldBig comboSizeActive">
                    <option value="-1">Select a reason</option>
                    <option value="1">Income Consideration</option>
                    <option value="2">Joint Registration</option>
                    <option value="3">Business is in Joint Name</option>
                    <option value="4">Customer is Mariner</option>
                    <option value="5">AC is in Joint Name</option>
                    <option value="6">Others</option>
                </select>
            </div>
            
            <div class="fieldDivContaint" style="display:none;">
                                <select id="cmbCashSecured" class="comboSize comboSizeActive">
                                    <option value="-1">Select a Status</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
            
            <div class="clear">
            </div>
            <!-- -----------------------------Tab Segment ----------------------------------------------------------------------------- -->
        </div>
        
        <div id="Div17" style="width: 100%; _width: 100%; *width: 100%; float: left; display:none;">
            <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%;">
                <span>Exposures</span></div>
                
                
                
            <div class="groupContainer" style="width: 98%; *width: 98%; _width: 98%;">
                <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%; background-color: white;
                    text-align: left; border: none;">
                    <span style="color: black;">ON SCB</span></div>
                <div class="divLeftBig" style="width: 98%;">
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        TERM
                        <label id="lblLoanterms">
                            LOANS</label>
                        <div style="border: none; width: 98%; overflow-y:none; overflow-x:scroll; ">
                            <table class="tableStyleSmall" id="tblFacilityTermLoan" style="width:140%;">
                                <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                    <td style="width: 10%;">
                                        TYPE
                                    </td>
                                    <td style="width: 10%;">
                                        FLAG
                                    </td>
                                    <td style="width: 10%;">
                                        RATE
                                    </td>
                                    <td style="width: 10%;">
                                        LIMIT
                                    </td>
                                    <td style="width: 10%;">
                                        OUTSTANDING
                                    </td>
                                    <td id="tdEM1" style="width: 10%;">
                                        EMI
                                    </td>
                                    <td id="tdEM2" style="width: 10%;">
                                        EMI %
                                    </td>
                                    <td id="tdEM3" style="width: 10%;">
                                        EMI SHARE
                                    </td>
                                    <td style="width: 10%;">
                                        STATUS
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY TYPE
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY FV
                                    </td>
                                    <td style="width: 10%;">
                                        SECURITY PV
                                    </td>
                                    <td id="tdlt1" style="width: 10%;">
                                        LTV %
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans1" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans1')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate1" type="text" class="comboSizeActive"/>
                                    </td>
                                    <td>
                                        <input id="txtLimit1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities1" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(1,1)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity1" style="width:40px;" type="text" disabled="disabled" />
                                        <input type="hidden" id="txttermloanInterestRate1" />
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans2" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans2')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate2" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit2" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities2" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(1,2)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity2" style="width:40px;" type="text" disabled="disabled" />
                                        <input type="hidden" id="txttermloanInterestRate2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans3" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans3')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag3" type="text" disabled="disabled"  />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate3" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit3" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding3" class="comboSizeActive" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities3" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(1,3)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities3" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('3')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity3" style="width:40px;" type="text" disabled="disabled" />
                                        
                                        <input type="hidden" id="txttermloanInterestRate3" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbTypeForTermLoans4" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForTermLoans4')">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtflag4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtInterestRate4" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtLimit4" class="comboSizeActive" type="text" />
                                    </td>
                                    <td>
                                        <input id="txtoutStanding4" class="comboSizeActive" type="text" onchange="analystDetailsHelper.calculateLTVForSequrity('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMI4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPer4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateEMIShare('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShare4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForSecurities4" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(1,4)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequrities4" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFV4" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPV4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForSequrity('4')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurity4" style="width:40px;" type="text" disabled="disabled" />
                                        <input type="hidden" id="txttermloanInterestRate4" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <div style="text-align: center; font-size: medium; font-weight: bold;">
                            CREDIT CARD
                            <div style="border: none;">
                                <table class="tableStyleSmall" id="tblFacilityCreditCard">
                                    <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                        <td style="width: 14%;">
                                            TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            FLAG
                                        </td>
                                        <td style="width: 11%;">
                                            ORIGINAL LIMIT
                                        </td>
                                        <%--<td style="width: 20%;" id="tdInterestRateCreditCard">
                                            INTEREST RATE
                                        </td>--%>
                                        <td style="width: 10%;" id="tdinterest">
                                            INTEREST
                                        </td>
                                        <td style="width: 12%;">
                                            STATUS
                                        </td>
                                        <td style="width: 15%;">
                                            SECURITY TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY FV
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY PV
                                        </td>
                                        <td id="tdlt2" style="width: 12%;">
                                            LTV %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="cmbTypeForCreditCard1" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForCreditCard1')">
                                            </select>
                                        </td>
                                        <td>
                                            <input id="txtFlagForcreditCard1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtOriginalLimitForCreditCard1" class="comboSizeActive comboSizeActive"  type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                        </td>
                                        <%--<td>
                                            <input id="txtInterestRateCreditCard1" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                        </td>--%>
                                        <td>
                                            <input id="txtInterestForCreditCard1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard1" class="comboSizeActive" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForcreditCard1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard1" type="text" disabled="disabled" />
                                    </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="cmbTypeForCreditCard2" class="comboSizeActive" onchange="analystDetailsHelper.changeTypeForExposer('cmbTypeForCreditCard2')">
                                            </select>
                                        </td>
                                        <td>
                                            <input id="txtFlagForcreditCard2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtOriginalLimitForCreditCard2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                        </td>
                                        <%--<td>
                                            <input id="txtInterestRateCreditCard2" type="text" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                        </td>--%>
                                        <td>
                                            <input id="txtInterestForCreditCard2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForCreditCard2" class="comboSizeActive" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForcreditCard2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForcreditCard2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForCreditCard2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForCreditCardSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForCreditCard2" type="text" disabled="disabled" />
                                    </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="text-align: center; font-size: medium; font-weight: bold;">
                            OVERDRAFT
                            <div style="border: none;">
                                <table class="tableStyleSmall" id="tblFacilityOverDraft">
                                    <tr class="theader" style="height: 33px; *height: 33px; _height: 33px;">
                                        <td style="width: 10%;">
                                            ORIGINAL LIMIT
                                        </td>
                                        <td style="width: 10%;">
                                            FLAG
                                        </td>
                                        <td style="width: 10%;" id="tdinteresRate">
                                            INTEREST RATE
                                        </td>
                                        <td style="width: 10%;" id="tdinteres1">
                                            INTEREST
                                        </td>
                                        <td style="width: 15%;" >
                                            STATUS
                                        </td>
                                        <td style="width: 15%;">
                                            SECURITY TYPE
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY FV
                                        </td>
                                        <td style="width: 10%;">
                                            SECURITY PV
                                        </td>
                                        <td id="tdlt3" style="width: 10%;">
                                            LTV %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeoverDraftInterest('1')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag1" type="text" class="comboSizeActive" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeoverDraftInterest('1')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres1" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft1" class="comboSizeActive" onchange="analystDetailsHelper.createOverDraftStatemntForBank('1',1)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft1" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('1')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft1" type="text" disabled="disabled" />
                                        <input type="hidden" id="txtOverDraftInterestRate1" />
                                    </td>
                                    
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeoverDraftInterest('2')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag2" type="text" class="comboSizeActive" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeoverDraftInterest('2')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres2" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft2" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('1', 2)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft2" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('2')" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft2" type="text" disabled="disabled" />
                                        <input type="hidden" id="txtOverDraftInterestRate2" />
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtOverdraftlimit3" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('3')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftFlag3" class="comboSizeActive" type="text" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInterest3" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeoverDraftInterest('3')" />
                                        </td>
                                        <td>
                                            <input id="txtOverdraftInteres3" type="text" disabled="disabled" />
                                        </td>
                                        <td>
                                        <select id="cmbStatusForSecuritiesForoverDraft3" class="comboSizeActive" onchange="analystDetailsHelper.calculateLTVForOverDraftSequrity('1',3)">
                                            <option value="-1">Select a Status</option>
                                            <option value="1">ACTIVE</option>
                                            <option value="2">SETTLED</option>
                                            <option value="3">TO BE CLOSED</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="cmbTypeForSequritiesForoverDraft3" class="comboSizeActive">
                                            <option value="-1">Select a Security type</option>
                                            <option value="1">FD (SCB)</option>
                                            <option value="2">Ezee FD</option>
                                            <option value="3">MSS (SCB)</option>
                                            <option value="4">CESS (SCB)</option>
                                            <option value="5">RFCD</option>
                                            <option value="6">WEDB-SCB</option>
                                            <option value="7">WEDB-OTHER</option>
                                            <option value="8">ICB Unit</option>
                                            <option value="9">USD Bond DPB</option>
                                            <option value="10">USD Bond DIB</option>
                                            <option value="11">MORTGAGE SECURITY</option>
                                            <option value="12">CAR REGISTRATION+INSURANCE</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtSecurityFVForoverDraft3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtSecurityPVForoverDraft3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtLTVForSecurityForoverDraft3" type="text" disabled="disabled" />
                                        <input type="hidden" id="txtOverDraftInterestRate3" />
                                    </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div style="height:20px;"></div>
                
                <div class="clear">
                </div>
                <!-- OFF SCB start -->
                <br />
                <div class="divHeadLine" style="width: 98%; *width: 98%; _width: 98%; background-color: white;
                    text-align: left; border: none;">
                    <span style="color: black;">OFF SCB</span></div>
                <div class="divRightBig" style="width: 98%; *width: 98%; _width: 98%; float: left;
                    margin-left: 2px;">
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        TERM
                        <label id="lblloanTerms1">
                            LOANS</label>
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityTermLoan">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <td id="tdEM4">
                                        EMI
                                    </td>
                                    <td id="tdEM5">
                                        EMI %
                                    </td>
                                    <td id="tdEM6">
                                        EMI SHARE
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb1" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB1" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB1" type="text" disabled="disabled" onchange="analystDetailsHelper.createExistingrepaymentGrid()" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb1" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(2,1)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb2" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB2" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb2" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(2,2)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB3" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb3" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb3" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(2,3)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOffSCB4" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOffScb4" type="text" class="comboSizeActive" />
                                    </td>
                                    <td>
                                        <input id="txtEMIforOffSCB4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIPerForOffSCB4" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeTurnOverShareForOffSCB('4')" />
                                    </td>
                                    <td>
                                        <input id="txtEMIShareForOffSCB4" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOffScb4" class="comboSizeActive" onchange="analystDetailsHelper.changeStatusForTermLoans(2,4)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        CREDIT CARD
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityCreditCard">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <%--<td>
                                        INTEREST RATE
                                    </td>--%>
                                    <td id="tdinterest2">
                                        INTEREST
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForCreditCardOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForcreditCardOffScb1" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(1)" />
                                    </td>
                                    <%--<td>
                                        <input id="txtInterestRateForCreditCardForOffScb1" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(1)"  />
                                    </td>--%>
                                    <td>
                                        <input id="txtInterestForCreditCardForOffScb1" type="text"  disabled="disabled"  />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForCreditCardOffScb1" class="comboSizeActive" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForCreditCardOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForcreditCardOffScb2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(2)" />
                                    </td>
                                    <%--<td>
                                        <input id="txtInterestRateForCreditCardForOffScb2" type="text" onchange="analystDetailsHelper.changeCreditCardInterestForOffSCB(2)" />
                                    </td>--%>
                                    <td>
                                        <input id="txtInterestForCreditCardForOffScb2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForCreditCardOffScb2" class="comboSizeActive" onchange="analystDetailsHelper.createExistingrepaymentGrid()">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="text-align: center; font-size: medium; font-weight: bold;">
                        OVERDRAFT
                        <div style="border: none;">
                            <table class="tableStyleSmall" id="tblOFFSCBFacilityOverDraft">
                                <tr class="theader">
                                    <td>
                                        FINANCIAL INSTITUTION
                                    </td>
                                    <td>
                                        ORIGINAL LIMIT
                                    </td>
                                    <td id="tdInterestrate1">
                                        INTEREST RATE
                                    </td>
                                    <td id="tdinterest3">
                                        INTEREST
                                    </td>
                                    <td>
                                        STATUS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB1" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb1" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB1" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('1')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest1" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb1" class="comboSizeActive" onchange="analystDetailsHelper.createOverDraftStatemntForBank('2',1)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB2" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB2" class="comboSizeActive" type="text" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('2')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest2" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb2" class="comboSizeActive" onchange="analystDetailsHelper.createOverDraftStatemntForBank('2',2)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="cmbFinancialInstitutionForOverDraftOffSCB3" class="comboSizeActive">
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtOriginalLimitForOverDraftOffScb3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtinterestRateForOverDraftOffSCB3" type="text" class="comboSizeActive" onchange="analystDetailsHelper.changeOverDraftInterstForOffSCB('3')" />
                                    </td>
                                    <td>
                                        <input id="txtInterestForOverDraftInterest3" type="text" disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbStatusForOverDraftOffScb3" class="comboSizeActive" onchange="analystDetailsHelper.createOverDraftStatemntForBank('2',3)">
                                            <option value="0">Select Status</option>
                                            <option value="1">Active</option>
                                            <option value="2">Settled</option>
                                            <option value="3">To Be Closed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <!-- End of OFF SCB -->
            </div>
            <br />
            <br />
        </div>
        
        <div id="Div67" class="divRightBig" style="margin-left: 5px; margin-top: 10px; border: 0px;">
        </div>
        <div class="clear">
        </div>
        
            
            <!-- -----------------End of  TAB (a.1) – General (Application & Vehicle): ------------------------------ -->
            <!-- -----------------------------Main Income Generator tab(TAB (b.1) – Income Generator) Start-------------------------------------- -->
            <div class="content" id="tabIncomeGenerator" style="*border:0px;">
            </div>
            <!-- -----------------------------End of Main tab Income Generator tab(TAB (b.1) – Income Generator) -------------------------------------- -->
            <!-- -----------------------------TAB (c) – Loan Calculator: Start-------------------------------------- -->
            <div class="content" id="tabLoanCalculator" style="*border:0px;">
                <div class="divHeadLineWhiteBg" style="background:#005d9a; xmargin:auto; color:White;">
                    <span style="font-size: large; font-weight: bold; font-family: verdana;">INCOME & SEGMENTATION</span></div>
                <div class="divRightBigNormalClass" style="width: 98%; *width: 98%; _width: 98%;
                    float: left; margin-left: 2px;">
                    <div style="text-align: left; font-size: small; font-weight: bold;" id="divLoanCalculatorForPrimaryApplicant">
                        PRIMARY APPLICANTS INCOME
                        <br />
                        <br />
                        <div style="border: none;">
                            <table class="tableStyleTiny" style="border: 1px solid gray;" id="tblPrLoanCalculator">
                                <tr class="theader">
                                    <td class="widthSize15_per">
                                        CALCULATION SOURCE
                                    </td>
                                    <td class="widthSize15_per">
                                        INCOME(S)
                                    </td>
                                    <td class="widthSize30_per" colspan="2">
                                        INCOME ASSESMENT METHOD(S)
                                    </td>
                                    <td class="widthSize30_per " colspan="2">
                                        EMPLOYER CATEGORY(S)
                                    </td>
                                    <td class="widthSize10_per">
                                        CONSIDER
                                    </td>
                                </tr>
                                <tr id="trBank1ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank1ForLCPr">
                                        BANK - 1
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank1">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank1">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank1">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank1">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank1" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank1" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank1" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank2ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank2ForLCPr">
                                        BANK - 2
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank2">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank2">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank2">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank2">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank2" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank2" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank2" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank3ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank3ForLCPr">
                                        BANK - 3
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank3">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank3">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank3">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank3">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank3" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank3" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank3" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank4ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank4ForLCPr">
                                        BANK - 4
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank4">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank4">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank4">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank4">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank4" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank4" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank4" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank5ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank5ForLCPr">
                                        BANK - 5
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank5">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank5">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank5">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank5">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank5" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank5" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank5" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank6ForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;" id="tdBank6ForLCPr">
                                        BANK - 6
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrBank6">
                                        <input type="hidden" id="txtAssMethodIdForLcPrBank6">
                                        <input type="hidden" id="txtAssCatIdForLcPrBank6">
                                        <input type="hidden" id="txtAverageBalanceIdForLcPrBank6">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrBank6" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrBank6" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrBank6" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trSalariedForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;">
                                        SALARY
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrSalary">
                                        <input type="hidden" id="txtAssMethodIdForLcPrSalary">
                                        <input type="hidden" id="txtAssCatIdForLcPrSalary">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrSalary" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrSalary" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrSalary" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trLandlordForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;">
                                        RENT
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrRent">
                                        <input type="hidden" id="txtAssMethodIdForLcPrrent">
                                        <input type="hidden" id="txtAssCatIdForLcPrRent">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrRent" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrRent" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrRent" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trDoctorForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;">
                                        PRACTICING DOCTOR
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrDoc">
                                        <input type="hidden" id="txtAssMethodIdForLcPrDoc">
                                        <input type="hidden" id="txtAssCatIdForLcPrDoc">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrDoctor" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrDoctor" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrDoctor" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trTeacherForLoanCalculatorPr" style="display: none;">
                                    <td style="text-align: right;">
                                        TUTION INCOME
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCPrTeacher" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcPrTec">
                                        <input type="hidden" id="txtAssMethodIdForLcPrTec">
                                        <input type="hidden" id="txtAssCatIdForLcPrTec">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCPrTeacher" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCPrTeacher" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCPrTeacher" type="text" class="txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCPrTeacher" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCPrTeacher" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Pr')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div style="text-align: left; font-size: small; font-weight: bold; display: none;"
                        id="divLoanCalculatorForJointApplicant">
                        <br />
                        JOINT APPLICANTS INCOME
                        <br />
                        <br />
                        <div style="border: none;">
                            <table class="tableStyleTiny" style="border: 1px solid gray;" id="tblJtLoanCalculator">
                                <tr class="theader">
                                    <td class="widthSize15_per">
                                        CALCULATION SOURCE
                                    </td>
                                    <td class="widthSize15_per">
                                        INCOME(S)
                                    </td>
                                    <td class="widthSize30_per" colspan="2">
                                        INCOME ASSESMENT METHOD(S)
                                    </td>
                                    <td class="widthSize30_per " colspan="2">
                                        EMPLOYER CATEGORY(S)
                                    </td>
                                    <td class="widthSize10_per">
                                        CONSIDER
                                    </td>
                                </tr>
                                <tr id="trBank1ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank1ForLCJt">
                                        BANK - 1
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank1">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank1">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank1">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank1">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank1" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank1" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank1" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank2ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank2ForLCJt">
                                        BANK - 2
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank2">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank2">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank2">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank2">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank2" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank2" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank2" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank3ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank3ForLCJt">
                                        BANK - 3
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank3">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank3">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank3">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank3">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank3" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank3" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank3" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank4ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank4ForLCJt">
                                        BANK - 4
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank4">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank4">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank4">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank4">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank4" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank4" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank4" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank5ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank5ForLCJt">
                                        BANK - 5
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank5">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank5">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank5">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank5">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank5" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank5" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank5" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank5" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trBank6ForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;" id="tdBank6ForLCJt">
                                        BANK - 6
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtBank6">
                                        <input type="hidden" id="txtAssMethodIdForLcJtBank6">
                                        <input type="hidden" id="txtAssCatIdForLcJtBank6">
                                        <input type="hidden" id="txtAverageBalanceIdForLcJtBank6">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtBank6" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtBank6" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtBank6" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtBank6" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trSalariedForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;">
                                        SALARY
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtSalary">
                                        <input type="hidden" id="txtAssMethodIdForLcJtSalary">
                                        <input type="hidden" id="txtAssCatIdForLcJtSalary">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtSalary" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtSalary" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtSalary" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtSalary" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trLandlordForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;">
                                        RENT
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtRent">
                                        <input type="hidden" id="txtAssMethodIdForLcJtrent">
                                        <input type="hidden" id="txtAssCatIdForLcJtRent">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtRent" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtRent" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtRent" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtRent" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trDoctorForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;">
                                        PRACTICING DOCTOR
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtDoc">
                                        <input type="hidden" id="txtAssMethodIdForLcJtDoc">
                                        <input type="hidden" id="txtAssCatIdForLcJtDoc">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtDoctor" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtDoctor" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtDoctor" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtDoctor" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trTeacherForLoanCalculatorJt" style="display: none;">
                                    <td style="text-align: right;">
                                        TUTION INCOME
                                    </td>
                                    <td style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtIncomeBForLCJtTeacher" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        <input type="hidden" id="txtbankIdForLcJtTec">
                                        <input type="hidden" id="txtAssMethodIdForLcJtTec">
                                        <input type="hidden" id="txtAssCatIdForLcJtTec">
                                    </td>
                                    <td>
                                        <input id="txtInComeAssMethLCJtTeacher" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtAssCodeForLCJtTeacher" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <input id="txtEmployerCategoryForLCJtTeacher" type="text" class="txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td class="widthSize5_per" style="padding-left: 2px; padding-right: 2px;">
                                        <input id="txtEmployerCatCodeForLCJtTeacher" type="text" class="widthSize100_per txtBoxSize_H22"
                                            disabled="disabled" />
                                    </td>
                                    <td>
                                        <select id="cmbConsiderForLCJtTeacher" class="widthSize100_per comboSizeH_24 comboSizeActive" onchange="AnalystDetailsCalculationHelper.CalculateLoanCalculator('Jt')">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="divLeftBig" style="width: 50%; border: none;">
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                    Total Income:</label></span>
                            <div class="divLeft" style="width: 62%; *width: 62%; _width: 62%; padding: 0px; border: none;">
                                <input id="txtTotalInComeForLcAll" class="txtBoxSize_H22" disabled="disabled" />
                                <input type="hidden" id="txtTotalIncomeForLcJointAll" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Declared Income:</label></span>
                            <div class="divLeft" style="width: 62%; *width: 62%; _width: 62%; padding: 0px; border: none;">
                                <input id="txtDeclaredIncomeForLcAll" class="txtBoxSize_H22" disabled="disabled" />
                                <input type="hidden" id="txtDeclaredIncomeForLcJointAll" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Appropriate Income:</label></span>
                            <div class="divLeft" style="width: 62%; *width: 62%; _width: 62%; padding: 0px; border: none;">
                                <input id="txtAppropriteIncomeForLcAll" class="txtBoxSize_H22" disabled="disabled" />
                                <input id="txtAppropriteIncomeForLcJointAll" type="hidden" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label id="lblTotaljointIncome" class="lbl" style="width: 140px; *width: 140px; width: 140px; font-weight: bold">
                                    TOTAL JOINT INCOME:</label></span>
                            <div class="divLeft" style="width: 62%; *width: 62%; _width: 62%; padding: 0px; border: none;">
                                <input id="txtTotalJointInComeForLcWithCal" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divRight" style="width: 45%; padding: 0px; border: none; float: right;
                        padding-top: 5px;">
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;">
                                    Employee Segment:</label></span>
                            <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                <select id="cmbEmployeeSegmentLc" class="comboSizeH_24 comboSizeActive">
                                </select>
                            </div>
                            <div class="divMiddile widthSize15_per" style="padding: 0px; border: none;">
                                <input id="txtSegmentCodeLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div id="divCashForLc" style="display: none;">
                            <div class="div3in1" style="display:none;">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 120px;">
                                        Assessment Method:</label></span>
                                <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                    <select id="cmbAssessmentMethodForLcCash" class="comboSizeH_24">
                                    </select>
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 120px;">
                                        Category:</label></span>
                                <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                    <select id="cmbCategoryForLcCash" class="comboSizeH_24">
                                    </select>
                                </div>
                            </div>
                            <div class="div3in1" id="divSubCategoryForLcCashCovered">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 120px;">
                                        Sub Category:</label></span>
                                <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                    <select id="cmbSubCategoryForLcCash" class="comboSizeH_24">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;">
                                    Assessment Method:</label></span>
                            <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                <input id="txtAssessmentMethodLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                                <input type="hidden" id="txtAssessmentMethodIdLc" />
                            </div>
                            <div class="divMiddile widthSize15_per" style="padding: 0px; border: none;">
                                <input id="txtAssessmentCodeLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;">
                                    Category:</label></span>
                            <div class="divLeft widthSize51_per" style="padding: 0px; border: none;">
                                <input id="txtEmployeeCategoryForLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                                <input type="hidden" id="txtEmployeeCategoryIdForLc" />
                                <input type="hidden" id="txtProfessionIdForLcIncomeAndSegment" />
                            </div>
                            <div class="divMiddile widthSize15_per" style="padding: 0px; border: none;">
                                <input id="txtEmployeeCategoryCodeForLc" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <!-- End of OFF SCB -->
                </div>
                <div class="clear">
                </div>
                <div class="divHeadLineWhiteBg" style="background:#005d9a; xmargin:auto; color:White;">
                    <span style="font-size: large; font-weight: bold; font-family: verdana;">REPAYMENT CAPABILITY</span></div>
                <div class="divRightBigNormalClass" style="width: 98%; *width: 98%; _width: 98%;
                    float: left; margin-left: 2px;">
                    <div style="text-align: left; font-size: small; font-weight: bold;">
                        <div class="divLeftBig" style="overflow: hidden; padding: 0px;">
                            <div class="divHeadLineWhiteBg" style="text-align: left; border: none; padding-left: 5px;">
                                <span style="font-size: small; font-weight: bold; font-family: verdana;">EXISTING REPAYMENTS</span></div>
                            <div class="divLeftBig" style="border: none; border-radius: 0px; margin-bottom: 0px;
                                margin-left: 0px; padding: 0px;">
                                <table class="tableStyleTiny" style="border: 1px solid gray;">
                                    <tr class="theader">
                                        <td class="widthSize50_per" colspan="2">
                                            <table class="widthSize100_per">
                                                <tr>
                                                    <td colspan="2">
                                                        ON US
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        TYPE
                                                    </td>
                                                    <td id="tdEM80">
                                                        EMI
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="widthSize50_per " colspan="2">
                                            <table class="widthSize100_per">
                                                <tr>
                                                    <td colspan="2">
                                                        OFF US
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        FI/NBFI
                                                    </td>
                                                    <td id="tdEM81">
                                                        EMI
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtTypeForLcExistingPayment1" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPayment1" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtBankForLcExistingPaymentOffScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                            <input id="txtBankIdForLcExistingPaymentOffScb1" type="text" class="txtBoxSize_H22"
                                                style="display: none;" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPaymentOffScb1" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtTypeForLcExistingPayment2" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPayment2" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtBankForLcExistingPaymentOffScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                            <input id="txtBankIdForLcExistingPaymentOffScb2" type="text" class="txtBoxSize_H22"
                                                style="display: none;" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPaymentOffScb2" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtTypeForLcExistingPayment3" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPayment3" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtBankForLcExistingPaymentOffScb3" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                            <input id="txtBankIdForLcExistingPaymentOffScb3" type="text" class="txtBoxSize_H22"
                                                style="display: none;" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPaymentOffScb3" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtTypeForLcExistingPayment4" type="text" class="txtBoxSize_H22" disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPayment4" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtBankForLcExistingPaymentOffScb4" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                            <input id="txtBankIdForLcExistingPaymentOffScb4" type="text" class="txtBoxSize_H22"
                                                style="display: none;" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtEmiForLcExistingPaymentOffScb4" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Total=
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtTotalEmiForLcExistingPaymentOnScb" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            Total=
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtTotalEmiForLcExistingPaymentOffScb" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="divHeadLineWhiteBg" style="text-align: left; border: none; padding-left: 5px;">
                                <span style="font-size: small; font-weight: bold; font-family: verdana;">CREDIT CARDS</span></div>
                            <div class="divLeftBig" style="border: none; border-radius: 0px; margin-bottom: 0px;
                                margin-left: 0px; padding: 0px;">
                                <table class="tableStyleTiny" style="border: 1px solid gray;">
                                    <tr class="theader">
                                        <td>
                                            LIMIT
                                        </td>
                                        <td id="tdinterest52">
                                            INTEREST
                                        </td>
                                        <td>
                                            LIMIT
                                        </td>
                                        <td id="tdinterest53">
                                            INTEREST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtLimitCarditcardForLcExistingPaymentForScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtInterestcreditcardForLcExistingPaymentForScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtLimitCarditcardForLcExistingPaymentForOffScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtInterestcreditcardForLcExistingPaymentOnScb1" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="txtLimitCarditcardForLcExistingPaymentForScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtInterestcreditcardForLcExistingPaymentForScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td>
                                            <input id="txtLimitCarditcardForLcExistingPaymentForOffScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtInterestcreditcardForLcExistingPaymentOnScb2" type="text" class="txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            Total=
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtTotalinterestForOnScbLcExistingrepayment" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                        <td style="text-align: right;">
                                            Total=
                                        </td>
                                        <td style="padding-left: 2px; padding-right: 2px;">
                                            <input id="txtTotalinterestForOffScbLcExistingrepayment" type="text" class="widthSize100_per txtBoxSize_H22"
                                                disabled="disabled" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="divRight" style="width: 45%; padding: 0px; border: none; float: right;
                        padding-top: 5px;">
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdDbr1">
                                    DBR Level:</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <select id="cmbDBRLevel" class="comboSizeH_24 comboSizeActive">
                                    <option value="1">Level-1</option>
                                    <option value="2">Level -2 ( 5% Breach)</option>
                                    <option value="3">Level -2 ( 10% Breach)</option>
                                </select>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdDbr2">
                                    Appropriate DBR %:</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtAppropriateDbr" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdDbr3">
                                    Considered DBR %:</label></span>
                            <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                <input id="txtConsideredDbr" class="txtBoxSize_H22 comboSizeActive" type="text" />
                            </div>
                            <div class="divMiddile widthSize40_per" style="padding: 0px; border: none;">
                                <span style="color: red; display: none;" id="tdDbr4">*Level-3 DBR Required</span>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdEM82">
                                    Maximum EMI Capability:</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtMaximumEmiCapability" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdEM83">
                                    Existing EMI(s):</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtExistingEmi" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;" id="tdEM84">
                                    Current EMI Capability</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtCurrentEMICapability" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px;">
                                    Balance Supported:</label></span>
                            <div class="divLeft widthSize66_per" style="padding: 0px; border: none;">
                                <input id="txtBalanceSupported" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 120px; font-weight: bold;" id="tdEM85">
                                    Maximum EMI:</label></span>
                            <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                <input id="txtMaximumEmi" class="txtBoxSize_H22" type="text" disabled="disabled" />
                            </div>
                            <div class="divMiddile widthSize40_per" style="padding: 0px; border: none;">
                                <span style="color: red; display: none;" id="tdDbrEx5">*DBR EXHAUSTED</span>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="divHeadLineWhiteBg" style="background:#005d9a; xmargin:auto; color:White;">
                    <span style="font-size: large; font-weight: bold; font-family: verdana;">
                        <label id="lblLoanCalculation">
                            LOAN</label>
                        CALCULATION</span></div>
                <div class="divRightBigNormalClass widthSize100_per" style="float: left; margin-left: 0px;
                    margin-bottom: 0px; padding: 2px;">
                    <div class="divLeftBig widthSize23_per" style="margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;">TENOR (Years)</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                    ARTA Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" id="txtArtaAllowedFoLc" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Vehicle Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" id="txtvehicleAllowedForLc" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Level:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <select id="cmbLabelForTonerLc" class="txtBoxSize_H22 comboSizeActive" onchange="analystDetailsHelper.calculateTenorSettings()">
                                    <option value="1">Level-1</option>
                                    <option value="2">Level-2</option>
                                </select>
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Age Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" id="txtAgeAllowedForLc" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Asking Tenor:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAskingTenorForLc" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Appropriate Tenor:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAppropriteTenorForLc" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Considered Tenor:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22 comboSizeActive" id="txtCosideredtenorForLc" onchange="analystDetailsHelper.calculateTenorSettings()" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Considered in Months:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtConsideredMarginForLc" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;" id="spnInterestPersent">INTEREST
                            (%)</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                    Vendor Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtInterestLCVendoreAllowed" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    ARTA Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtInterestLCArtaAllowed" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Segment Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtInterestLCSegmentAllowed" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Asking Rate:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtInterestLCAskingRate" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Appropriate Rate:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAppropriteRate" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Considered Rate:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22 comboSizeActive" id="txtInterestLCCosideredRate" onchange="analystDetailsHelper.calculateInterestRate();" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1 widthSize100_per" style="color: red; margin-bottom: 10px;">
                                *Check if Special Rate Prevails</span>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana; font-size: 90%;">
                            <label id="lblLoanAmount">
                                LOAN</label>
                            AMOUNT (BDT)</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;" id="tdlt4">
                                    LTV Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtLtvAllowedForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Income Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtIncoomeAllowedForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    BB & CAD Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtbdCadAllowedForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" id="lblAskingLoan" style="width: 140px; *width: 140px; width: 140px;">
                                    Asking Loan:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAskingLoanAmountForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Existing Facility Allowed:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtExistingFacilitiesAllowedForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" id="lblAppropriteLoan" style="width: 140px; *width: 140px; width: 140px;">
                                    Appropriate Loan:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22" disabled="disabled" id="txtAppropriteloanAmountForLcLoanAmount" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; width: 140px;">
                                    Approved
                                    <label id="lblApprovedloan">
                                        Loan</label>
                                    :</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input class="txtBoxSize_H22 comboSizeActive" id="txtApprovedLoanAmountForLcLoanAmount" onchange="analystDetailsHelper.calculateInterestRate();" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px;">
                        <span style="font-weight: bold; font-family: verdana;">INSURANCE (BDT)</span>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; _width: 140px; *width: 140px;">
                                    General Insurance:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtgeneralInsurenceForLcIns" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    ARTA:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtArtaForLCInsurenceAmount" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="div3in1">
                            <span class="spnLeftLabel1">
                                <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;">
                                    Total Insurance:</label></span>
                            <div class="divLeft widthSize30_per" style="padding: 0px; border: none;">
                                <input id="txtTotalInsurenceForLcInsurence" class="txtBoxSize_H22" disabled="disabled" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <br />
                    <div style="margin: 0px; border: solid 1px green; border-radius: 15px;">
                        <div class="divLeftBig widthSize73_per" style="margin-bottom: 0px; border: none;">
                            <span id="spnLoanSummary" style="font-weight: bold; font-family: verdana;">LOAN SUMMARY</span>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" id="lblTotalloanAmount" style="width: 140px; _width: 140px; *width: 140px;
                                        text-align: right; font-weight: bold;">
                                        Total Loan Amount:</label></span>
                                <div class="divLeft widthSize50_per" style="padding: 0px; border: none;">
                                    <input id="txtloanAmountForLcLoanSummary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;" id="tdlt5">
                                        LTV Excluding Insurance:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtLtvExclusdingInsurence" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px; margin-left: 25px;"
                                        id="tdlt6">
                                        LTV Including Insurance:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtltvIncludingLtvForLcLoanSummary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px;" id="tdlt7">
                                        DBR Excluding Insurance:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtDBRExcludingInsurence" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                                <span class="spnLeftLabel1">
                                    <label class="lbl" style="width: 140px; *width: 140px; _width: 140px; margin-left: 25px;"
                                        id="tdlt8">
                                        DBR Including Insurance:</label></span>
                                <div class="divLeft widthSize25_per" style="padding: 0px; border: none;">
                                    <input id="txtDBRIncludingInsurence" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="divLeftBig widthSize23_per" style="margin-left: 5px; margin-bottom: 0px;
                            border: none;">
                            <br />
                            <div class="div3in1">
                                <span class="spnLeftLabel1 widthSize30_per rightAlign" style="font-weight: bold;">EMR:</span>
                                <div class="divLeft widthSize65_per" style="padding: 0px; border: none;">
                                    <input id="txtEmrForlcloanSummary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1 widthSize30_per leftAlign rightAlign" id="tdlt9">Extra LTV:</span>
                                <div class="divLeft widthSize65_per" style="padding: 0px; border: none;">
                                    <input id="txtExtraLtvForLcloanSumary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="div3in1">
                                <span class="spnLeftLabel1 widthSize30_per leftAlign rightAlign" id="tdDbr5">Extra DBR:</span>
                                <div class="divLeft widthSize65_per" style="padding: 0px; border: none;">
                                    <input id="txtExtraDBRForLcloanSummary" class="txtBoxSize_H22" disabled="disabled" />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            
           </div> 
    
    </div>
    </form>
</body>
</html>
