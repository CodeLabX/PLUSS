﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PlussAndLoan.UI.AutoLoan.LoanApplicationPrint" Codebehind="LoanApplicationPrint.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pluss | Booklet</title>
    <link href="../../CSS/AutoLoanPrint.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" style="text-align: center">
    <%--Main content Div--%>
    <div style="width: 7.0in; height: auto; text-align: left">
        <%--Header section--%>
        <div style="text-align: center">
            <div style="width: auto; height: 20px;">
                <asp:Image ID="Image1" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                    Width="624px" />
            </div>
            <span style="font-size: 26px; font-weight: bold">Saadiq Auto Finance application form</span><br />
            Please complete in BLOCK latters</div>
        <div style="text-align: left">
            <table style="width: 100%; empty-cells: show; text-align: center; vertical-align: middle;">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Source:" Font-Bold="True"></asp:Label>
                    </td>
                    <td class="style1">
                        <asp:TextBox ID="txtSource" runat="server" Width="169px" BorderStyle="Solid" BorderWidth="1px"
                            Height="22px"></asp:TextBox>
                    </td>
                    <td class="style17">
                        <asp:Label ID="Label4" runat="server" Text="Source Code" Font-Bold="True"></asp:Label>
                    </td>
                    <td class="style8">
                        <asp:TextBox ID="txtSourceCode" runat="server" Width="100px" BorderStyle="Solid"
                            BorderWidth="1px" Height="22px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Date" Font-Bold="True"></asp:Label>
                    </td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtDate" runat="server" Width="127px" BorderStyle="Solid" BorderWidth="1px"
                            Height="22px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
        <div style="border-style: solid; border-width: 1px; vertical-align: middle; text-align: left;">
            <table style="width: 100%; vertical-align: middle; text-align: center;">
                <tr>
                    <td class="style9">
                        Amount Applied For:
                    </td>
                    <td class="style10" style="text-align: left">
                        <asp:TextBox ID="txtAppliedAmount" runat="server" Width="140px" BorderStyle="Solid"
                            BorderWidth="1px" Height="22px"></asp:TextBox>
                    </td>
                    <td class="style11" style="text-align: left">
                        Tenor (Months)&nbsp;:
                    </td>
                    <td>
                        <div style="border-style: solid; border-width: 1px; text-align: left; vertical-align: middle;
                            height: 22px;">
                            <asp:CheckBox ID="chkTenor12" runat="server" Text="12" TextAlign="Left" />
                            &nbsp;<asp:CheckBox ID="chkTenor24" runat="server" Text="24" TextAlign="Left" />
                            &nbsp;<asp:CheckBox ID="chkTenor36" runat="server" Text="36" TextAlign="Left" />
                            &nbsp;<asp:CheckBox ID="chkTenor48" runat="server" Text="48" TextAlign="Left" />
                            &nbsp;<asp:CheckBox ID="chkTenor60" runat="server" Text="60" TextAlign="Left" />
                        </div>
                    </td>
                </tr>
            </table>
            Vhicle Type (Please Tick):&nbsp;<asp:CheckBox ID="chkSedan" runat="server" Text="Sedan" TextAlign="Left" />
            &nbsp;
            <asp:CheckBox ID="chkPickUp" runat="server" Text="Pick-Ups" 
                TextAlign="Left" />
            &nbsp;
            <asp:CheckBox ID="chkStationWagon" runat="server" Text="Station Wagon" 
                TextAlign="Left" />
            &nbsp;
            <asp:CheckBox ID="chkMicrobus" runat="server" Text="Microbus" TextAlign="Left" />
            &nbsp;
            <asp:CheckBox ID="chkSUV" runat="server" Text="SUV" TextAlign="Left" />
        &nbsp;&nbsp;
            <asp:CheckBox ID="chkMPV" runat="server" Text="MPV" TextAlign="Left" />
        </div>
        <%--Header section Close--%>
        <%--Personal Information--%>
        <div style="border-style: solid; border-width: 1px; margin-top: 5px">
            <table class="reference">
                <tr style="width: 100%">
                    <td class="style12" colspan="3">
                        <div style="float: left; font-size: 14px;">
                            Personal Information
                        </div>
                        <div class="style14">
                            <span class="style15">Insurance Coverage: </span>
                            <asp:CheckBox ID="chkInsuranceTypeLife" runat="server" Font-Bold="False" Font-Size="Medium"
                                Text="Life" TextAlign="Left" />
                            &nbsp;<asp:CheckBox ID="chkInsuranceTypeGenerel" runat="server" Font-Bold="False"
                                Font-Size="Medium" Text="Generel" TextAlign="Left" />
                        </div>
                        <div class="style16">
                            <span class="style15">Insurance Finance With Loan ?: </span>
                            <asp:CheckBox ID="chkInsuranceFinanceWithLoanYes" runat="server" Font-Bold="False"
                                Font-Size="Medium" Text="Y" TextAlign="Left" />
                            <asp:CheckBox ID="chkInsuranceFinanceWithLoanNo" runat="server" Font-Bold="False"
                                Font-Size="Medium" Text="N" TextAlign="Left" />
                        </div>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        &nbsp;
                    </td>
                    <td class="style19">
                        <div style="float:left">Life Insurance:</div>
                        <div>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </div>
                        <br />
                    </td>
                    <td>
                        <div style="float:left">General Insurance:</div>
                        <div>
                            <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
                        </div>
                        
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        &nbsp;
                    </td>
                    <td class="style19">
                        <div style="text-align: center; font-weight: bold;">
                            Primary Applicant
                        </div>
                    </td>
                    <td>
                        <div style="text-align: center; font-weight: bold;">
                            Joint Applicant
                        </div>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Name
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Relationship Number
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantRelationshipNumber" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantRelationshipNumber" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Tin Number
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantTinNumber" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantTinNumber" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Provious facilities from Standard chartered
                    </td>
                    <td class="style19">
                        <div>
                            <table class="reference">
                                <tr>
                                    <td class="style21">
                                        &nbsp; Facility Type(s)
                                    </td>
                                    <td>
                                        &nbsp;
                                        <asp:Label ID="lblPApplicantFacilityType" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style21">
                                        &nbsp; Amount(s)
                                    </td>
                                    <td>
                                        &nbsp;
                                        <asp:Label ID="lblPApplicantFacilityAmount" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div>
                            <table class="reference">
                                <tr>
                                    <td class="style21">
                                        &nbsp; Facility Type(s)
                                    </td>
                                    <td>
                                        &nbsp;
                                        <asp:Label ID="lblJApplicantFacilityType" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style21">
                                        &nbsp; Amount(s)
                                    </td>
                                    <td>
                                        <asp:Label ID="lblJApplicantFacilityAmount" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        SCB Credit card NO.
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantSCBCreditCardNo" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantSCBCreditCardNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Father&#39;s Name
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantFathersName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantFathersName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Mother&#39;s Name
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantMothersName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantMothersName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Date of Birth
                    </td>
                    <td class="style19">
                        <div>
                            <table style="width: 100%;">
                                <tr>
                                    <td class="style23">
                                        Day:<asp:Label ID="lblPApplicantBirthDateDay" runat="server"></asp:Label>
                                    </td>
                                    <td class="style22">
                                        Month:<asp:Label ID="lblPApplicantBirthDateMonth" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        Year:<asp:Label ID="lblPApplicantBirthDateYear" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div>
                            <table style="width: 100%;">
                                <tr>
                                    <td class="style23">
                                        Day:<asp:Label ID="lblJApplicantBirthDateDay" runat="server"></asp:Label>
                                    </td>
                                    <td class="style22">
                                        Month:<asp:Label ID="lblJApplicantBirthDateMonth" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        Year:<asp:Label ID="lblJApplicantBirthDateYear" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Marital Status
                    </td>
                    <td class="style19">
                        <asp:CheckBox ID="chkPApplicantMaritalStatusSingle" runat="server" Text="Single" />
                        <asp:CheckBox ID="chkPApplicantMaritalStatusMarried" runat="server" Text="Married" />
                        <asp:CheckBox ID="chkPApplicantMaritalStatusSeparated" runat="server" Text="separated" />
                        <asp:CheckBox ID="chkPApplicantMaritalStatusOthers" runat="server" Text="Others" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkJApplicantMaritalStatusSingle" runat="server" Text="Single" />
                        <asp:CheckBox ID="chkJApplicantMaritalStatusMarried" runat="server" Text="Married" />
                        <asp:CheckBox ID="chkJApplicantMaritalStatusSeparated" runat="server" Text="separated" />
                        <asp:CheckBox ID="chkJApplicantMaritalStatusOthers" runat="server" Text="Others" />
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Spouse Name
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantSpouseName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantSpouseName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Spouse Profession
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantSpouseProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantSpouseProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Spouse work address
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantSpouseWorkaddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantSpouseWorkaddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Spouse Contact NO.
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantSpouseContact" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantSpouseContact" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Relationship with Primary Applicant.
                    </td>
                    <td style="background-color: #808080" class="style19">
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantRelationWithPrimaryApplicant" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Nationality
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantNationality" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantNationality" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        No. of Dependants
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantDependance" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantDependance" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Passport/ID type &amp; No.
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantPassportIDTypeAndNo" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantPassportIDTypeAndNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Highest Education level Please tick
                    </td>
                    <td class="style19">
                        <asp:CheckBox ID="chkPApplicantSSC" runat="server" Text="SSC" />
                        <asp:CheckBox ID="chkPApplicantHSC" runat="server" Text="HSC" />
                        <asp:CheckBox ID="chkPApplicantGraduate" runat="server" Text="Graduate" />
                        <br />
                        <asp:CheckBox ID="chkPApplicantPostGraduate" runat="server" Text="Post Graduate" />
                        <asp:CheckBox ID="chkPApplicantOthers" runat="server" Text="Others" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkJApplicantSSC" runat="server" Text="SSC" />
                        <asp:CheckBox ID="chkJApplicantHSC" runat="server" Text="HSC" />
                        <asp:CheckBox ID="chkJApplicantGraduate" runat="server" Text="Graduate" />
                        <br />
                        <asp:CheckBox ID="chkJApplicantPostGraduate" runat="server" Text="Post Graduate" />
                        <asp:CheckBox ID="chkJApplicantOthers" runat="server" Text="Others" />
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Residence address
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantResidenceAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantResidenceAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Mailing address
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantMailingAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantMailingAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Parmanent address
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantParmanentAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantParmanentAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Residence Status
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantResidenceStatus" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantResidenceStatus" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Years in current address
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantYearsInCurrentAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantYearsInCurrentAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Phone (Res)
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantPhoneRes" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantPhoneRes" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Phone (Mobile)
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantPhoneMobile" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantPhoneMobile" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Email
                    </td>
                    <td class="style19">
                        <asp:Label ID="lblPApplicantEmail" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantEmail" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="border-style: solid; border-width: 1px">
                    <td class="style18">
                        Directorship with any private bank Please tick.
                    </td>
                    <td class="style19">
                        <table style="width: 100%;">
                            <tr>
                                <td style="text-align: center">
                                    &nbsp;<asp:CheckBox ID="chkPApplicantDirectorshipWithOtherbankYes" runat="server"
                                        Text="Yes" />
                                </td>
                                <td style="text-align: center">
                                    &nbsp;<asp:CheckBox ID="chkPApplicantDirectorshipWithOtherbankNo" runat="server"
                                        Text="No" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    If Yes, Bank Name:
                                    <asp:Label ID="lblPApplicantDirectorshipWithOtherbank" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td style="text-align: center">
                                    <asp:CheckBox ID="chkJApplicantDirectorshipWithOtherbankYes" runat="server" Text="Yes" />
                                </td>
                                <td style="text-align: center">
                                    <asp:CheckBox ID="chkJApplicantDirectorshipWithOtherbankNo" runat="server" Text="No" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    If Yes, Bank Name:
                                    <asp:Label ID="lblJApplicantDirectorshipWithOtherbank" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <%--Personal Information Close--%>
        <br />
        <br />
        <br />
        <br />
        <br />
        <%--Professionl information--%>
        <%--salarid--%>
        <div>
            <table class="reference">
                <tr>
                    <td colspan="3" style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Professional Information (Salaried person only).
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        &nbsp;
                    </td>
                    <td class="style25">
                        &nbsp; Primay Applicant
                    </td>
                    <td>
                        &nbsp; Joint Applicant
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Primary Profession
                    </td>
                    <td class="style25">
                        &nbsp;
                        <asp:Label ID="lblPApplicantProfessionSalariedPrimaryProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                        <asp:Label ID="lblJApplicantProfessionSalariedPrimaryProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Other Profession
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSalariedOtherProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSalariedOtherProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Months in current Job.
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSalariedMonthInCurrentJob" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSalariedMonthInCurrentJob" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Name of company
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSalariedNameOfCompany" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSalariedNameOfCompany" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Designation
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSalariedDesignation" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSalariedDesignation" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Address
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSalariedAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSalariedAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Office Phone
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSalariedOfficePhone" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSalariedOfficePhone" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Employment Status
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSalariedEmployementStatus" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSalariedEmployementStatus" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Total Professional Experience
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSalariedTotalExperience" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSalariedTotalExperience" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <%--business--%>
        <div>
            <table class="reference">
                <tr>
                    <td colspan="3" style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Professional Information (Business person only).
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        &nbsp;
                    </td>
                    <td class="style25">
                        &nbsp; Primay Applicant
                    </td>
                    <td>
                        &nbsp; Joint Applicant
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Primary Profession
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessPrimaryProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessPrimaryProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Other Profession
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessOtherProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessOtherProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Months in current Job.
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessMonthInCurrentJob" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessMonthInCurrentJob" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Name of Organization
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessNameOfOrganization" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessNameOfOrganization" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Designation
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessDesignation" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessDesignation" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Nature of Business
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessNatureOfBusiness" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessNatureOfBusiness" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Years in business
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessYearsInBusiness" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessYearsInBusiness" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Offoce status
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessOfficeStatus" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessOfficeStatus" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Office Phone
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessOfficePhone" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessOfficePhone" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Ownarship type
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessOwnershipType" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessOwnershipType" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Total Professional Experience
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessTotalExperience" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessTotalExperience" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Number of Employees
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessNumberOfEmployee" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessNumberOfEmployee" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Equity/Share %
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionBusinessEquity" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionBusinessEquity" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <%--Self-employee--%>
        <div>
            <table class="reference">
                <tr>
                    <td colspan="3" style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Professional Information (Self-Employment Detail).
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        &nbsp;
                    </td>
                    <td class="style25">
                        &nbsp; Primay Applicant
                    </td>
                    <td>
                        &nbsp; Joint Applicant
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Primary Profession
                    </td>
                    <td class="style25">
                        &nbsp;
                        <asp:Label ID="lblPApplicantProfessionSelfPrimaryProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                        <asp:Label ID="lblJApplicantProfessionSelfPrimaryProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Other Profession
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfOtherProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfOtherProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Month in Current Job.
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfMonthInCurrentJob" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfMonthInCurrentJob" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Name of Organization
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfNameOfOrganization" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfNameOfOrganization" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Designation
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfDesignation" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfDesignation" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Address
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Office Phone
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfOfficePhone" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfOfficePhone" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Total Professional Experience
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfTotalExperience" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfTotalExperience" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Primary Income Source
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfPrimaryIncomeSource" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfPrimaryIncomeSource" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Other Income Source
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionSelfOtherIncomeSource" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionSelfOtherIncomeSource" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <%--Land Lord--%><br />
        <br />
        <br />
        <br />
        &nbsp;<div>
            <table class="reference">
                <tr>
                    <td colspan="3" style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Professional Information (Landlord/Landlady).
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        &nbsp;
                    </td>
                    <td class="style25">
                        &nbsp; Primay Applicant
                    </td>
                    <td>
                        &nbsp; Joint Applicant
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Primary Profession
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandPrimaryProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandPrimaryProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Other Profession
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandOtherProfession" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandOtherProfession" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Month in Current Job.
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandMonthInCurrentJob" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandMonthInCurrentJob" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Name of Organization
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandNameOfOrganization" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandNameOfOrganization" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Address
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Total Professional Experience
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandTotalExperience" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandTotalExperience" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        No. of Floors Rented
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandNumberOfFloors" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandNumberOfFloors" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Nature of Rented Floors
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandNatureOfRentedFloor" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandNatureOfRentedFloor" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Rented Ares in SFT
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandRentedAreaInSFT" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandRentedAreaInSFT" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Construction Completing Year
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPApplicantProfessionLandConstructionCompletingYear" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJApplicantProfessionLandConstructionCompletingYear" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <%--Professionl information end--%>
        <br />
        <%--Financial Information--%>
        <div>
            <table class="reference">
                <tr>
                    <td colspan="3" style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Financial Information
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        &nbsp;
                    </td>
                    <td class="style25">
                        Primay Applicant
                    </td>
                    <td>
                        Joint Applicant
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Declared Primary Income Source
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPDeclaredPrimaryIncomeSource" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJDeclaredPrimaryIncomeSource" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Declared Primary Income Amount
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPDeclaredPrimaryIncomeAmount" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJDeclaredPrimaryIncomeAmount" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Declared Other Income source
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPDeclaredOtherIncomeSource" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJDeclaredOtherIncomeSource" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Declared Other Income Amount
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPDeclaredOtherIncomeAmount" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJDeclaredOtherIncomeAmount" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Type of Expance
                    </td>
                    <td class="style25">
                        <table class="reference">
                            <tr>
                                <td class="style21">
                                    Rent &amp; Utilities
                                </td>
                                <td>
                                    <asp:Label ID="lblPExpanceTypeRentUtilities" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style21">
                                    Food &amp; Clothing
                                </td>
                                <td>
                                    <asp:Label ID="lblPExpanceTypeFoodClothing" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style21">
                                    Education
                                </td>
                                <td>
                                    <asp:Label ID="lblPExpanceTypeEducation" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style21">
                                    Loan Repayment
                                </td>
                                <td>
                                    <asp:Label ID="lblPExpanceTypeLoanRepayment" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style21">
                                    Others
                                </td>
                                <td>
                                    <asp:Label ID="lblPExpanceTypeOther" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="reference">
                            <tr>
                                <td class="style21">
                                    Rent &amp; Utilities
                                </td>
                                <td>
                                    <asp:Label ID="lblJExpanceTypeRentUtilities" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style21">
                                    Food &amp; Clothing
                                </td>
                                <td>
                                    <asp:Label ID="lblJExpanceTypeFoodClothing" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style21">
                                    Education
                                </td>
                                <td>
                                    <asp:Label ID="lblJExpanceTypeEducation" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style21">
                                    Loan Repayment
                                </td>
                                <td>
                                    <asp:Label ID="lblJExpanceTypeLoanRepayment" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style21">
                                    Others
                                </td>
                                <td>
                                    <asp:Label ID="lblJExpanceTypeOther" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Account(s) with Other Bank
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblPExpanceTypeOtherBankAccounts" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblJExpanceTypeOtherBankAccounts" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Finance Facilities/Card Details
                    </td>
                    <td class="style25">
                        <table class="reference">
                            <tr>
                                <td style="text-align: center" class="style47">
                                    Type
                                </td>
                                <td style="text-align: center">
                                    Monthly repanyment
                                </td>
                            </tr>
                            <tr>
                                <td class="style47">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="style47">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="reference">
                            <tr>
                                <td style="text-align: center" class="style48">
                                    Type
                                </td>
                                <td style="text-align: center">
                                    Monthly repanyment
                                </td>
                            </tr>
                            <tr>
                                <td class="style48">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="style48">
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <%--Financial information end--%>
        <br />
        <%--Vehicle Information--%>
        <div>
            <table class="reference">
                <tr>
                    <td colspan="2" style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Vehicle Information
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Manufacturar
                    </td>
                    <td class="style25">
                        <table style="border: 0;">
                            <tr>
                                <td class="style49" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    <asp:Label ID="lblVehicleManufacturar" runat="server"></asp:Label>
                                </td>
                                <td style="border: 0;">
                                    Status:&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkVehicleStatusNew" runat="server" Text="New" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkVehicleStatusRecondition" runat="server" Text="Recondition" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkVehicleStatusUsed" runat="server" Text="Used" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Car name/Trim
                    </td>
                    <td class="style25">
                        <table style="border: 0;">
                            <tr>
                                <td class="style27" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    <asp:Label ID="lblVehicleCarNameTrim" runat="server"></asp:Label>
                                </td>
                                <td style="border: 0;">
                                    Mfg. year:&nbsp;
                                    <asp:Label ID="lblVehicleMfgYear" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Engine No.
                    </td>
                    <td class="style25">
                        <table style="border: 0;">
                            <tr>
                                <td class="style28" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    <asp:Label ID="lblVehicleEngineNo" runat="server"></asp:Label>
                                </td>
                                <td class="style29" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    Chassis No.
                                </td>
                                <td style="border: 0;">
                                    <asp:Label ID="lblVehicleChassisNo" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        CC and Mileage (KM)
                    </td>
                    <td class="style25">
                        <table style="border: 0;">
                            <tr>
                                <td class="style28" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    <asp:Label ID="lblVehicleCCAndMileage" runat="server"></asp:Label>
                                </td>
                                <td class="style29" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    Current Mkt. value(for used car only)
                                </td>
                                <td style="border: 0;">
                                    <asp:Label ID="lblVehicleCurrentMktValue" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Vendor/Dealer Code
                    </td>
                    <td class="style25">
                        <table style="border: 0;">
                            <tr>
                                <td class="style28" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    <asp:Label ID="lblVehicleVendorDealerCode" runat="server"></asp:Label>
                                </td>
                                <td class="style18" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    Contact person(vendor)
                                </td>
                                <td style="border: 0;">
                                    <asp:Label ID="lblVehicleVendorContactPerson" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Vendor address
                    </td>
                    <td class="style25">
                        <asp:Label ID="lblVehicleVendorAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Vendor contact No.
                    </td>
                    <td class="style25">
                        <table style="border: 0;">
                            <tr>
                                <td class="style27" style="border-style: none solid none none; border-width: 0 1 0 0;
                                    border-top-color: inherit; border-right-color: #808080; border-bottom-color: inherit;
                                    border-left-color: inherit;">
                                    Phone:&nbsp;
                                    <asp:Label ID="lblVehicleVendorContactPhone" runat="server"></asp:Label>
                                </td>
                                <td style="border: 0;">
                                    Mobile:&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="lblVehicleVendorContactMobile" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <%--Vehicle information end--%>
        <br />
        <br />
        <br />
        <%--referance start--%>
        <div>
            <table class="reference">
                <tr>
                    <td style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        References
                    </td>
                    <td style="background-color: #808080; font-size: 18px; font-weight: bold;" class="style30">
                        Reference 1
                    </td>
                    <td style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Reference 2
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Name
                    </td>
                    <td class="style30">
                        <asp:Label ID="lblReference1Name" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReference2Name" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Relationship
                    </td>
                    <td class="style30">
                        <asp:Label ID="lblReference1Relationship" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReference2Relationship" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Occupation
                    </td>
                    <td class="style30">
                        <asp:Label ID="lblReference1Occupation" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReference2Occupation" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Name of organization
                    </td>
                    <td class="style30">
                        <asp:Label ID="lblReference1NameOfOrganization" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReference2NameOfOrganization" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Designation
                    </td>
                    <td class="style30">
                        <asp:Label ID="lblReference1Designation" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReference2Designation" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Work Address
                    </td>
                    <td class="style30">
                        <asp:Label ID="lblReference1WorkAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReference2WorkAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Resident address
                    </td>
                    <td class="style30">
                        <asp:Label ID="lblReference1ResidentAddress" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblReference2ResidentAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: middle;">
                        Telephpone
                    </td>
                    <td class="style30">
                        <table class="reference">
                            <tr>
                                <td class="style31">
                                    Residence
                                </td>
                                <td>
                                    <asp:Label ID="lblReference1PhoneResidence" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style31">
                                    Office
                                </td>
                                <td>
                                    <asp:Label ID="lblReference1PhoneOffice" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style31">
                                    Mobile
                                </td>
                                <td>
                                    <asp:Label ID="lblReference1PhoneMobile" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="reference">
                            <tr>
                                <td class="style31">
                                    Residence
                                </td>
                                <td>
                                    <asp:Label ID="lblReference2PhoneResidence" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style31">
                                    Office
                                </td>
                                <td>
                                    <asp:Label ID="lblReference2PhoneOffice" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style31">
                                    Mobile
                                </td>
                                <td>
                                    <asp:Label ID="lblReference2PhoneMobile" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <table class="reference">
                <tr>
                    <td style="background-color: #808080; font-size: 18px; font-weight: bold;" colspan="4">
                        BANK USE ONLY
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Relationship
                    </td>
                    <td class="style30" colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style24">
                        Account number
                    </td>
                    <td class="style32">
                        &nbsp;
                    </td>
                    <td class="style33">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <br />
            <table class="reference">
                <tr>
                    <td colspan="7" style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Facility Schedule
                    </td>
                </tr>
                <tr>
                    <td class="style34">
                        Facility<br />
                        Type
                    </td>
                    <td class="style40">
                        Interest/Rent<br />
                        rate %
                    </td>
                    <td class="style41">
                        Present<br />
                        Balance
                    </td>
                    <td class="style40">
                        Present
                        <br />
                        EMI
                    </td>
                    <td class="style37">
                        Present
                        <br />
                        Limit
                        <td class="style39">
                            Proposed
                            <br />
                            Limit
                        </td>
                        <td class="style35">
                            Repayment<br />
                    Arrangement
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType1" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate1" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance1" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI1" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit1" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit1" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement1" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType2" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate2" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance2" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI2" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit2" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit2" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement2" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType3" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate3" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance3" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI3" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit3" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit3" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement3" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType4" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate4" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance4" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI4" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit4" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit4" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement4" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType5" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate5" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance5" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI5" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit5" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit5" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement5" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType6" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate6" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance6" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI6" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit6" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit6" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement6" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType7" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate7" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance7" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI7" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit7" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit7" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement7" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType8" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate8" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance8" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI8" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit8" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit8" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement8" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style34">
                        <asp:Label ID="lblFacilityScheduleFacilityType9" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilityScheduleInterestRate9" runat="server"></asp:Label>
                    </td>
                    <td class="style41">
                        <asp:Label ID="lblFacilitySchedulePresentBalance9" runat="server"></asp:Label>
                    </td>
                    <td class="style40">
                        <asp:Label ID="lblFacilitySchedulePresentEMI9" runat="server"></asp:Label>
                    </td>
                    <td class="style37">
                        <asp:Label ID="lblFacilitySchedulePresentLimit9" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblFacilityScheduleProposedLimit9" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblFacilityScheduleRepaymentArrangement9" runat="server"></asp:Label>
                </tr>
                </table>
            <br />
            <table class="reference">
                <tr>
                    <td colspan="6" style="background-color: #808080; font-size: 18px; font-weight: bold;">
                        Security Schedule
                    </td>
                </tr>
                <tr>
                    <td class="style45">
                        Nature of<br />
                        Securiry
                    </td>
                    <td class="style43">
                        Issuing
                        <br />
                        Office
                    </td>
                    <td class="style43">
                        Face value<br />
                        BDT
                    </td>
                    <td class="style46">
                        XTV BDT<br />
                        rate
                        <td class="style39">
                            Issue date
                        </td>
                        <td class="style35">
                    Interest</tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity1" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice1" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue1" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV1" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate1" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest1" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity2" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice2" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue2" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV2" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate2" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest2" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity3" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice3" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue3" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV3" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate3" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest3" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity4" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice4" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue4" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV4" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate4" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest4" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity5" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice5" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue5" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV5" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate5" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest5" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity6" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice6" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue6" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV6" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate6" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest6" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity7" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice7" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue7" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV7" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate7" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest7" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity8" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice8" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue8" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV8" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate8" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest8" runat="server"></asp:Label>
                </tr>
                <tr>
                    <td class="style45">
                        <asp:Label ID="lblSecurityScheduleNatureOfSecurity9" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleIssuingOffice9" runat="server"></asp:Label>
                    </td>
                    <td class="style43">
                        <asp:Label ID="lblSecurityScheduleFaceValue9" runat="server"></asp:Label>
                    </td>
                    <td class="style46">
                        <asp:Label ID="lblSecurityScheduleXTV9" runat="server"></asp:Label>
                        <td class="style39">
                            <asp:Label ID="lblSecurityScheduleIssueDate9" runat="server"></asp:Label>
                        </td>
                        <td class="style35">
                            <asp:Label ID="lblSecurityScheduleInterest9" runat="server"></asp:Label>
                </tr>
                </table>
            <div style="width: auto; height: 20px;">
                <asp:Image ID="Image2" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                    Width="643px" />
            </div>
        </div>
        <%--referance end--%>
        <%--<input id="Button1" type="button" value="Print" onclick="javascript:printPage();" />--%>

        <script type="text/javascript">
            function printPage() {
                this.visible = false;
                window.print();
            }
        </script>

    </div>
    <%--Main content div close--%>
    </form>
</body>
</html>
