﻿using System;
using System.Collections.Generic;
using System.IO;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class IRSettingsForPayroll : System.Web.UI.Page
    {
        [AjaxNamespace("IRSettingsForPayroll")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(IRSettingsForPayroll));
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoPayrollRate> GetPayRollSettingsSummary(int pageNo, int pageSize, string search)
        {
            pageNo = pageNo * pageSize;

            IRSettingsForPayrollRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new IRSettingsForPayrollService(repository);


            List<AutoPayrollRate> objListAutoPayrollRate =
                autoLoanDataService.GetPayRollSettingsSummary(pageNo, pageSize, search);
            return objListAutoPayrollRate;
        }




        //[AjaxMethod(HttpSessionStateRequirement.Read)]
        //public string Upload()
        //{
        //    //string script = "<script>parent.iRSettingsforPayrollHelper.uploadCallback({0})</script>";
        //    string returnValue = "";
        //    //if (Request.Files["fileDocumentUpload"].ContentLength <= 0)
        //    if (!fileDocumentUpload.FileContent.CanRead)
        //    {
        //        //script = string.Format(script, "{Error:'Please select a file.'}");
        //        returnValue = "Please select Valid file";
        //    }
        //    else
        //    {

        //        try
        //        {
        //            string serverPath = Request.PhysicalApplicationPath;
        //            var directory = Server.MapPath(serverPath + @"/Temp");

        //            if (!Directory.Exists(directory))
        //            {
        //                Directory.CreateDirectory(directory);
        //            }
        //            if (Request.Files["fileDocumentUpload"].ContentLength > 0)
        //            {
        //                var name = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" +
        //                     Path.GetFileName(Request.Files["fileDocumentUpload"].FileName);
        //                //Request.Files["fileDocumentUpload"].SaveAs(Path.Combine(directory, name));
        //                fileDocumentUpload.SaveAs(Path.Combine(directory, name));

        //            }

        //            //script = string.Format(script, string.Format("{{}}"));
        //            returnValue = "Upload success";

        //        }
        //        catch (Exception ex)
        //        {
        //            //script = string.Format(script,
        //            //        string.Format("{{Error:{0}}}", JavaScriptSerializer.Serialize(ex.Message)));

        //            returnValue = "Upload Failed";
        //        }


        //    }
        //    //Response.Write(script);
        //    //Response.End();
        //    return returnValue;
        //}


        protected void btnUploadSave_Click(object sender, EventArgs e)
        {
            var res = uploadFile();
            if (res == "Upload success" || res == "Success")
            {
                string msg = "Operation Successfull.";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");
            }
            else
            {
                string msg = "Operation partially completed. Found Error with follwoing rows (" + res + ")";

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "uploadCallback(" + msg + ");", true);
                RegisterClientScriptBlock("clntdate", "<script>alert('" + msg + "');</script>");

            }
        }



        public string uploadFile()
        {
            string name = "";
            string returnValue = "";
            if (!fileDocumentUpload.FileContent.CanRead)
            {
                
                returnValue = "Please select Valid file";
            }
            else
            {
                try
                {
                    string serverPath = Request.PhysicalApplicationPath;
                    var directory = Server.MapPath(@"../Uploads/Auto");

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    DateTime date = DateTime.Now;
                    name = date.ToString("yyyyMMddHHmmss") + "_" +
                             fileDocumentUpload.FileName;
                    fileDocumentUpload.SaveAs(Path.Combine(directory, name));


                    returnValue = "Upload success";
                    returnValue = DumpExcelFile(Convert.ToString(Path.Combine(directory, name)));

                }
                catch (Exception ex)
                {
                    returnValue = "Upload Failed";
                }
                
            }
            return returnValue;
        }



        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string savePayroll(AutoPayrollRate autoPayrollRate)
        {
            IRSettingsForPayrollRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoLoanDataService = new IRSettingsForPayrollService(repository);
            var res = autoLoanDataService.savePayroll(autoPayrollRate);
            return res;

        }



        public string DumpExcelFile(string excelFilePath)
        {
            IRSettingsForPayrollRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var iRSettingsForPayrollService = new IRSettingsForPayrollService(repository);
            var res = iRSettingsForPayrollService.DumpExcelFile(excelFilePath);
            return res;

        }



    }
    
}
