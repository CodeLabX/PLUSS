﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class SegmentSettings : System.Web.UI.Page
    {
        [AjaxNamespace("SegmentSettings")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(SegmentSettings));
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoSegmentSettings> GetSegmentSettingsSummary(int pageNo, int pageSize)
        {
            pageNo = pageNo * pageSize;

            List<AutoSegmentSettings> autoSegmentSettingsList = new List<AutoSegmentSettings>();
            AutoSegmentSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoSegmentSettingsService = new AutoSegmentSettingsService(repository);


            List<AutoSegmentSettings> objList_AutoSegmentSettings =
                autoSegmentSettingsService.GetSegmentSettingsSummary(pageNo, pageSize);

            foreach (var obj in objList_AutoSegmentSettings)
            {
                if (obj.MinLoanAmt < 0)
                {
                    obj.MinLoanAmt = 0;
                }
                if (obj.MaxLoanAmt < 0)
                {
                    obj.MaxLoanAmt = 0;
                }
                if (obj.MinTanor < 0)
                {
                    obj.MinTanor = 0;
                }
                if (obj.MaxTanor < 0)
                {
                    obj.MaxTanor = 0;
                }
                if (obj.IrWithARTA < 0)
                {
                    obj.IrWithARTA = 0;
                }
                if (obj.IrWithoutARTA < 0)
                {
                    obj.IrWithoutARTA = 0;
                }
                if (obj.MinAgeL1 < 0)
                {
                    obj.MinAgeL1 = 0;
                }
                if (obj.MinAgeL2 < 0)
                {
                    obj.MinAgeL2 = 0;
                }
                if (obj.MaxAgeL1 < 0)
                {
                    obj.MaxAgeL1 = 0;
                }
                if (obj.MaxAgeL2 < 0)
                {
                    obj.MaxAgeL2 = 0;
                }
                if (obj.LTVL1 < 0)
                {
                    obj.LTVL1 = 0;
                }
                if (obj.LTVL2 < 0)
                {
                    obj.LTVL2 = 0;
                }
                if (obj.ExperienceL1 < 0)
                {
                    obj.ExperienceL1 = 0;
                }
                if (obj.ExperienceL2 < 0)
                {
                    obj.ExperienceL2 = 0;
                }
                if (obj.AcRelationshipL1 < 0)
                {
                    obj.AcRelationshipL1 = 0;
                }
                if (obj.AcRelationshipL2 < 0)
                {
                    obj.AcRelationshipL2 = 0;
                }
                if (obj.MinDBRL1 < 0)
                {
                    obj.MinDBRL1 = 0;
                }
                if (obj.MinDBRL2 < 0)
                {
                    obj.MinDBRL2 = 0;
                }
                if (obj.MaxDBRL2 < 0)
                {
                    obj.MaxDBRL2 = 0;
                }
                if (obj.DBRL3 < 0)
                {
                    obj.DBRL3 = 0;
                }
                if (obj.PrimaryMinIncomeL1 < 0)
                {
                    obj.PrimaryMinIncomeL1 = 0;
                }
                if (obj.PrimaryMinIncomeL2 < 0)
                {
                    obj.PrimaryMinIncomeL2 = 0;
                }
                if (obj.JointMinIncomeL1 < 0)
                {
                    obj.JointMinIncomeL1 = 0;
                }
                if (obj.JointMinIncomeL2 < 0)
                {
                    obj.JointMinIncomeL2 = 0;
                }

                autoSegmentSettingsList.Add(obj);

            }

            return autoSegmentSettingsList;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string saveSegment(AutoSegmentSettings autoSegmentSettings, int segmentID)
        {
            AutoSegmentSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            autoSegmentSettings.UserId = Convert.ToInt32(Session["Id"]);
            var autoSegmentSettingsService = new AutoSegmentSettingsService(repository);
            var res = autoSegmentSettingsService.saveSegment(autoSegmentSettings, segmentID);
            return res.ToString();

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoProfession> GetProfession()
        {
            AutoSegmentSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoSegmentSettingsService = new AutoSegmentSettingsService(repository);


            List<AutoProfession> objList_AutoProfession =
                autoSegmentSettingsService.GetProfession();
            return objList_AutoProfession;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string InactiveSegmentById(int segmentId)
        {
            AutoSegmentSettingsRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var userId = Convert.ToInt32(Session["Id"]);
            var autoSegmentSettingsService = new AutoSegmentSettingsService(repository);
            var res = autoSegmentSettingsService.InactiveSegmentById(segmentId, userId);
            return res.ToString();

        }





    }
}
