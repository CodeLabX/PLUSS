﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{

    public partial class AutoLoanApplicationCISupport : System.Web.UI.Page
    {

        [AjaxNamespace("AutoLoanApplicationCISupport")]
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Utility.RegisterTypeForAjax(typeof(AutoLoanApplicationCISupport));
            }
            catch (Exception ex)
            {

            }
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public Object GetDataByLLID(int llId)
        {
            var objAutoLoanApplicationAll = new Object();
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objAutoLoanApplicationAll;
            }
            else
            {

                ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
                var autoLoanDataService = new LoanApplicationService(repository);
                objAutoLoanApplicationAll = autoLoanDataService.getLoanLocetorInfoByLLIDForCISupport(llId, statePermission);
            }

            return objAutoLoanApplicationAll;
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string SaveCISupportLoanApp(Auto_LoanAnalystMaster autoAnalistMaster,
            Auto_LoanAnalystApplication autoLoanAnalystApplication, Auto_LoanAnalystVehicleDetail autoLoanAnalystVehicleDetail,
            List<AutoApplicantSCBAccount> autoApplicantAccountSCBList, List<AutoPRAccountDetail> autoApplicantAccountOtherList,
            List<AutoLoanFacility> autoFacilityList, List<AutoLoanSecurity> autoSecurityList, List<AutoLoanFacilityOffSCB> oFFSCBFacilityList, 
            AutoLoanVehicle objAutoLoanVehicle, AutoVendor objAutoVendor)
        {
            int userID = Convert.ToInt32(Session["Id"].ToString());

            var res = "";
            AutoLoanCISupportRepository repository = new AutoLoanDataService.AutoLoanCISupportDataService();
            var autoLoanCISupportService = new AutoLoanCISupportService(repository);

            res = autoLoanCISupportService.SaveCISupportLoanApp(autoAnalistMaster, autoLoanAnalystApplication, 
                autoLoanAnalystVehicleDetail, autoApplicantAccountSCBList, autoApplicantAccountOtherList,
                autoFacilityList, autoSecurityList, oFFSCBFacilityList, userID, objAutoLoanVehicle, objAutoVendor);

            return res;

        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanSummary> GetLoanSummary(int pageNo, int pageSize, Int32 searchKey)
        {
            List<AutoLoanSummary> objLoanSummaryList = new List<AutoLoanSummary>();
            pageNo = pageNo * pageSize;
            var userType = Session["AutoUserType"];
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            if (statePermission.Count == 0)
            {
                return objLoanSummaryList;
            }
            else
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                objLoanSummaryList =
                    autoLoanAssessmentService.getAutoLoanSummaryAll(pageNo, pageSize, searchKey, statePermission);
            }
            return objLoanSummaryList;
        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string AppealedByAutoLoanMasterId(int appiledOnAutoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                if (UserType == 10)
                {
                    objAutoStatusHistory.StatusID = 18; //received by auto ops maker
                }
                else if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 20; //received by auto ops Checker
                }
                else if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 23; //received by auto CI Support
                }
                else if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 26; //received by auto CI Analyst
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string ForwardLoanApp(Auto_LoanAnalystMaster autoAnalistMaster,
            Auto_LoanAnalystApplication autoLoanAnalystApplication, Auto_LoanAnalystVehicleDetail autoLoanAnalystVehicleDetail,
            List<AutoApplicantSCBAccount> autoApplicantAccountSCBList, List<AutoPRAccountDetail> autoApplicantAccountOtherList,
            List<AutoLoanFacility> autoFacilityList, List<AutoLoanSecurity> autoSecurityList, List<AutoLoanFacilityOffSCB> oFFSCBFacilityList,
            AutoLoanVehicle objAutoLoanVehicle, AutoVendor objAutoVendor)
        {
            var res = "";
            try
            {
                int userID = Convert.ToInt32(Session["Id"].ToString());

                AutoLoanCISupportRepository repository = new AutoLoanDataService.AutoLoanCISupportDataService();
                var autoLoanCISupportService = new AutoLoanCISupportService(repository);

                res = autoLoanCISupportService.SaveCISupportLoanApp(autoAnalistMaster, autoLoanAnalystApplication,
                    autoLoanAnalystVehicleDetail, autoApplicantAccountSCBList, autoApplicantAccountOtherList,
                    autoFacilityList, autoSecurityList, oFFSCBFacilityList, userID, objAutoLoanVehicle, objAutoVendor);








                if (res == "Success")
                {

                    var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                    AutoLoanAssessmentRrepository repository1 = new AutoLoanDataService.AutoLoanAssessmentDataService();
                    var autoLoanAssessmentService = new AutoLoanAssessmentService(repository1);

                    AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                    objAutoStatusHistory.AutoLoanMasterId = autoAnalistMaster.AutoLoanMasterId;
                    if (UserType == 10)
                    {
                        objAutoStatusHistory.StatusID = 19; //forwareded to ops checker
                    }
                    if (UserType == 12)
                    {
                        objAutoStatusHistory.StatusID = 21; //forwareded to CI
                    }
                    if (UserType == 9)
                    {
                        objAutoStatusHistory.StatusID = 24; //forwareded to CI Analyst
                    }
                    if (UserType == 8)
                    {
                        objAutoStatusHistory.StatusID = 27; //forwareded to CI Approver
                    }
                    objAutoStatusHistory.UserID = userID;


                    res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }


        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string HoldLoanApp(int autoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;
                if (UserType == 10)
                {
                    //objAutoStatusHistory.StatusID = 3; //forwareded to ops checker
                }
                if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 22; //hold by ops checker
                }
                if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 25; //hold by CI Support
                }
                if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 28; //hold by CI Analyst
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string BackwardLoanApp(int autoLoanMasterId)
        {
            var res = "";
            try
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["AutoUserType"].ToString());

                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = autoLoanMasterId;

                if (UserType == 12)
                {
                    objAutoStatusHistory.StatusID = 35; //BackWard From Ops Cheker and Set to Opes Maker
                }
                if (UserType == 9)
                {
                    objAutoStatusHistory.StatusID = 34; //Backward From CI Support and Set to Opes Checker
                }
                if (UserType == 8)
                {
                    objAutoStatusHistory.StatusID = 33; //Backward From Analyst and Set to CI Support
                }
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }












        #region getting information
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoStatus> getVehicleStatus(int statusID)
        {
            AutoVehicleStatusIRRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var autoVehicleStatusIRService = new AutoVehicleStatusIRService(repository);


            List<AutoStatus> objList_AutoStatus =
                autoVehicleStatusIRService.GetStatus(statusID);
            return objList_AutoStatus;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoMenufacturer> GetAutoManufacturer()
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository);


            List<AutoMenufacturer> objAutoManufacturer =
                autoManufactuerService.GetAutoManufacturer();
            return objAutoManufacturer;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVendor> GetVendor()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetVendor();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranchWithSource> GetAllAutoBranch()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetAllbranchName();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranchWithSource> GetAllAutoSourceByBranchId(int branchId)
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetAllAutoSourceByBranchId(branchId);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBank> GetBank(int status)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBank(status);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoBranch> GetBranch(int bankID)
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.GetBranch(bankID);
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoFacility> getFacility()
        {
            IAutobranchAndSource repository = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository);
            var res = autoBranchAndSourceService.GetFacility();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussSector> GetSector()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetAllPlussSector();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussSubSector> GetSubSector(int sectorId)
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetPlussSubSectorBySectorID(sectorId);
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<PlussIncomeAcesmentMethod> GetIncomeAssement()
        {
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var res = autoLoanAssessmentService.GetPlussIncomeAcesmentMethod();
            return res;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<Auto_InsuranceBanca> getGeneralInsurance()
        {
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            var res = loanApplicationService.getGeneralInsurance();
            return res;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoVehicle> getModelbyManufacturerID(int manufacturerID)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            List<AutoVehicle> objList_AutoModel = tanorAndLTVService.GetModelList(manufacturerID);
            return objList_AutoModel;
        }

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoBrandTenorLTV getValuePackAllowed(int manufacturerID, int vehicleId, int vehicleStatus)
        {
            ITanorAndLTV repository = new AutoLoanDataService.AutoLoanDataService();
            var tanorAndLTVService = new TanorAndLTVService(repository);


            AutoBrandTenorLTV objAutoBrandTenorLTV = tanorAndLTVService.getvaluepackallowed(manufacturerID, vehicleId, vehicleStatus);
            return objAutoBrandTenorLTV;
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public AutoLoanVehicle CheckMouByVendoreId(int vendoreId)
        {

            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var userID = Convert.ToInt32(Session["Id"].ToString());

            AutoLoanVehicle objAutoLoanVehicle = autoLoanAssessmentService.CheckMouByVendoreId(vendoreId);

            return objAutoLoanVehicle;
        }

        #endregion


    }
}
