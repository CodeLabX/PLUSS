﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.LLIPrint" Codebehind="LLIPrint.aspx.cs" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
--%>    
<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LIMIT LOADING INSTRUCTIONS</title>
    <style type="text/css">
        table.reference
        {
            background-color: #FFFFFF;
            border: 1px solid #C3C3C3;
            border-collapse: collapse;
            width: 100%;
        }
        table.reference th
        {
            background-color: #E5EECC;
            border: 1px solid #C3C3C3;
            padding: 0px;
            vertical-align: top;
        }
        table.reference td
        {
            border: 1px solid #C3C3C3;
            padding: 0px;
            vertical-align: top;
            font-size: 12px;
        }
        .style3
        {
            background-color: #CCCCCC;
        }
        .style4
        {
            width: 141px;
        }
        .style5
        {
            width: 102px;
            font-family: "Arial Narrow";
            font-size: small;
            text-align: right;
        }
        .style6
        {
            width: 141px;
            font-family: "Arial Narrow";
            font-size: small;
        }
        .style7
        {
            width: 200px;
            font-family: "Arial Narrow";
            font-size: small;
            background-color: #CCCCCC;
        }
        .style8
        {
            width: 102px;
            text-align: right;
        }
        .style13
        {
            width: 289px;
        }
        .style19
        {
            width: 84px;
            text-align: center;
            height: 16px;
            background-color: #CCCCCC;
        }
        .style20
        {
            width: 289px;
            height: 16px;
        }
        .style21
        {
            width: 115px;
            text-align: center;
            height: 16px;
            background-color: #CCCCCC;
        }
        .style22
        {
            text-align: center;
            height: 16px;
            background-color: #CCCCCC;
        }
        .style23
        {
            width: 223px;
        }
        .style24
        {
            width: 134px;
        }
        .style25
        {
            width: 84px;
            height: 16px;
        }
        .style26
        {
            height: 16px;
        }
        .style27
        {
            height: 16px;
            width: 126px;
        }
        .style28
        {
            background-color: #999999;
        }
        .style29
        {
            width: 289px;
            background-color: #999999;
        }
        .style30
        {
            width: 84px;
            background-color: #999999;
        }
        .style31
        {
            width: 84px;
            text-align: center;
            background-color: #CCCCCC;
        }
        .style32
        {
            width: 115px;
            text-align: center;
            background-color: #CCCCCC;
        }
        .style33
        {
            text-align: center;
            background-color: #CCCCCC;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="mainPanel" runat="server">
        <asp:Button ID="bPrint" runat="server" Text="Print" OnClick="bPrint_Click" />
        <div style="width: 6.5in; height: auto; text-align: left;">
            <div style="width: auto; height: 20px;">
                <asp:Image ID="Image1" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                    Width="624px" />
            </div>
            <div>
                LIMIT LOADING INSTRUCTIONS
            </div>
            <table style="border: 3px solid #000001; width: 100%;">
                <tr>
                    <td class="style5">
                        PRODUCT
                    </td>
                    <td class="style7">
                        AUTO LOAN
                    </td>
                    <td class="style6">
                        DATE
                    </td>
                    <td>
                        <asp:Label ID="lDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        PRODUCT TYPE
                    </td>
                    <td class="style3">
                        <asp:Label ID="lProductType" runat="server"></asp:Label>
                        <asp:Label ID="lblProductId" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td class="style4">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        APPLICANT NAME
                    </td>
                    <td class="style3" colspan="3">
                        <asp:Label ID="lApplicantName" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            LOAN INFORMATION<br />
            <table style="border: 3px solid #000001; width: 100%; font-family: 'Arial Narrow';
                font-size: small;">
                <tr>
                    <td class="style8">
                        INCOME
                    </td>
                    <td>
                        <asp:Label ID="lIncome" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style8">
                        LOAN AMOUNT
                    </td>
                    <td>
                        <asp:Label ID="lLoanAmount" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style8">
                        TENOR
                    </td>
                    <td>
                        <asp:Label ID="lTenor" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style8">
                        INTEREST RATE
                    </td>
                    <td>
                        <asp:Label ID="lInterestRate" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style8">
                        EMI
                    </td>
                    <td>
                        <asp:Label ID="lEMI" runat="server" Style="background-color: #CCCCCC"></asp:Label>
                    </td>
                </tr>
            </table>
            MIS MODULE&nbsp;
            <table style="border: 1px solid #000001; width: 100%;" class="reference">
                <tr>
                    <td class="style30">
                        MIS TYPE
                    </td>
                    <td class="style29">
                        MIS TYPE DESCRIPTION
                    </td>
                    <td class="style28">
                        MIS CODE
                    </td>
                    <td class="style28">
                        REMARKS
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        05
                    </td>
                    <td class="style13">
                        LOANTYPE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lLoanType" runat="server"></asp:Label>
                    </td>
                    <td class="style32">
                        <asp:Label ID="lLoneTypeRemark" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        EC
                    </td>
                    <td class="style13">
                        EMPLOYERCATEGORY
                    </td>
                    <td class="style32">
                        <asp:Label ID="lEmployerCat" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lEmployerCatRemark" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        15
                    </td>
                    <td class="style13">
                        ANALYSTCODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lAnalystCode" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lAnalystCodeRemark" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        16
                    </td>
                    <td class="style13">
                        APPROVERCODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lApproverCode" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lApproverCodeRemark" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        IN
                    </td>
                    <td class="style13">
                        INCOMEASSESSMENTCODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lIncomeAssesmentCode" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lIncomeAssesmentCodeRemark" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        01
                    </td>
                    <td class="style13">
                        DEVIATIONCODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lDeviationCode1" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lDeviationCodeRemark1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        01
                    </td>
                    <td class="style13">
                        DEVIATIONCODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lDeviationCode2" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lDeviationCodeRemark2" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        01
                    </td>
                    <td class="style13">
                        DEVIATIONCODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lDeviationCode3" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lDeviationCodeRemark3" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style19">
                        CL
                    </td>
                    <td class="style20">
                        COLLATERALS
                    </td>
                    <td class="style21">
                        <asp:Label ID="lCollaterals" runat="server"></asp:Label>
                    </td>
                    <td class="style22">
                        <asp:Label ID="lCollateralsRemark" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        CD
                    </td>
                    <td class="style13">
                        CRD
                    </td>
                    <td class="style32">
                        <asp:Label ID="lCRD" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lCRDRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        BC
                    </td>
                    <td class="style13">
                        BORROWERCODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lBorrowerCode" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lBorrowerCodeRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        AP
                    </td>
                    <td class="style13">
                        APPLICATIONSOURCECODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lApplicationSourceCode" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lApplicationSourceCodeRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        06
                    </td>
                    <td class="style13">
                        DISCRETION
                    </td>
                    <td class="style32">
                        <asp:Label ID="lDiscretion" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lDiscretionRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        18
                    </td>
                    <td class="style13">
                        INPRFT
                    </td>
                    <td class="style32">
                        <asp:Label ID="lInprft" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lInprftRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        11
                    </td>
                    <td class="style13">
                        BTF
                    </td>
                    <td class="style32">
                        <asp:Label ID="lBTF" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lBTFRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        VS
                    </td>
                    <td class="style13">
                        VEHICLESTATUS
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleStatus" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehicleStatusRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        VT
                    </td>
                    <td class="style13">
                        VEHICLETYPE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleType" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehicleTypeRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        VZ
                    </td>
                    <td class="style13">
                        VEHICLESIZE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleSize" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehicleSizeRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        VM
                    </td>
                    <td class="style13">
                        VEHICLEMANUFACTURINGYEAR
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleManufacturingYear" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehicleManufacturingYearRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        DC
                    </td>
                    <td class="style13">
                        DEVELOPER/VENDOR CATEGORY
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVendorCat" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVendorCatRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        07
                    </td>
                    <td class="style13">
                        LOANPURPOSECODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lLoanPurposeCode" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lLoanPurposeCodeRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        M1
                    </td>
                    <td class="style13">
                        BANCAINFO
                    </td>
                    <td class="style32">
                        <asp:Label ID="lBancaInfo" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lBancaInfoRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        LC
                    </td>
                    <td class="style13">
                        LOANCLASS
                    </td>
                    <td class="style32">
                        <asp:Label ID="lLoanClass" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lLoanClassRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        18
                    </td>
                    <td class="style13">
                        DBR
                    </td>
                    <td class="style32">
                        <asp:Label ID="lDBR" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lDBRRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        18
                    </td>
                    <td class="style13">
                        LTV
                    </td>
                    <td class="style32">
                        <asp:Label ID="lLTV" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lLTVRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        18
                    </td>
                    <td class="style13">
                        SAL
                    </td>
                    <td class="style32">
                        <asp:Label ID="lSAL" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lSALRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        18
                    </td>
                    <td class="style13">
                        DBT
                    </td>
                    <td class="style32">
                        <asp:Label ID="lDBT" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lDBTRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        M4
                    </td>
                    <td class="style13">
                        MIS-4
                    </td>
                    <td class="style32">
                        <asp:Label ID="lMIS4" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lMIS4Remark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            VEHICLE MODULE<br />
            <table style="border: 1px solid #000001; width: 100%;" class="reference">
                <tr>
                    <td class="style30">
                        MIS TYPE
                    </td>
                    <td class="style29">
                        MIS TYPE DESCRIPTION
                    </td>
                    <td class="style28">
                        MIS CODE
                    </td>
                    <td class="style28">
                        REMARKS
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        UC33
                    </td>
                    <td class="style13">
                        VEHICLE BRAND
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleBrand" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleBrandRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        UC34
                    </td>
                    <td class="style13">
                        VEHICLE MODEL
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleModel" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehicleModelRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        UC35
                    </td>
                    <td class="style13">
                        VEHICLE DEALER
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleDeler" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehicleDelerRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        VM
                    </td>
                    <td class="style13">
                        VEHICLE MANUFACTURING YEAR
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehicleManufacYear" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehicleManufacYearRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        VZ
                    </td>
                    <td class="style13">
                        VEHICLE SIZE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehiclceSizee" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehiclceSizeeRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        &nbsp;
                    </td>
                    <td class="style13">
                        VEHICLE PRICE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lVehiclePrice" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lVehiclePriceRemark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            INSURANCE MODULE<br />
            <table style="border: 1px solid #000001; width: 100%;" class="reference">
                <tr>
                    <td class="style30">
                        MIS TYPE
                    </td>
                    <td class="style29">
                        MIS TYPE DESCRIPTION
                    </td>
                    <td class="style28">
                        MIS CODE
                    </td>
                    <td class="style28">
                        REMARKS
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        UC71
                    </td>
                    <td class="style13">
                        INSURERECODE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lInsurenceCode" runat="server"></asp:Label>
                    </td>
                    <td class="style32">
                        <asp:Label ID="lInsurenceCodeRemark" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style31">
                        &nbsp;
                    </td>
                    <td class="style13">
                        GENERAL INSURANCE
                    </td>
                    <td class="style32">
                        <asp:Label ID="lGeneralInsurance" runat="server"></asp:Label>
                    </td>
                    <td class="style33">
                        <asp:Label ID="lGeneralInsuranceRemark" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            CIB INFO<table style="border: 1px solid #000001; width: 100%;" class="reference">
                <tr>
                    <td class="style25">
                        <asp:Label ID="Label83" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style20">
                        <asp:Label ID="Label85" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style27">
                        <asp:Label ID="Label87" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style26">
                        <asp:Label ID="Label89" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 84px; text-align: left;">
                        <asp:Label ID="Label84" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style13">
                        <asp:Label ID="Label86" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style27">
                        <asp:Label ID="Label88" runat="server" Text=""></asp:Label>
                    </td>
                    <td class="style26">
                        <asp:Label ID="Label90" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <div style="border-top: 2px solid #000001; width: 250px;">
                AUTHORIZED SIGNATURE (CCU)</div>
            <br />
            <br />
            <table style="width: 100%;">
                <tr>
                    <td class="style23">
                        <div style="border-top: 2px solid #000001; width: 202px;">
                            AUTHORIZED SIGNATURE</div>
                    </td>
                    <td class="style24">
                        &nbsp;
                    </td>
                    <td>
                        <div style="border-top: 2px solid #000001; width: 202px;">
                            AUTHORIZED SIGNATURE</div>
                    </td>
                </tr>
            </table>
            <div style="width: auto; height: 20px;">
                <asp:Image ID="Image2" runat="server" ImageUrl="../../Image/headBorder.JPG" Height="20px"
                    Width="624px" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="CrystalPanel" runat="server">
<%--        <CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" />--%>
       <%-- <CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" />--%>
        <CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" />

    </asp:Panel>
    </form>
</body>
</html>
