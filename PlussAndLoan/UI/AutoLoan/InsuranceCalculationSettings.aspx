﻿<%@ Page Title="SCB | Auto Loan | Insurance Calculation Settings" Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.InsuranceCalculationSettings" Codebehind="InsuranceCalculationSettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <script src="../../Scripts/AutoLoan/InsuranceCalculationSettings.js" type="text/javascript"></script>
    
    
        <style type="text/css">
        #nav_InsuranceCalculationSettings a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    <div class="pageHeaderDiv">Insurance Calculation Settings</div>
<div id="Content">
    <div class="ownDamage">
        <h3>OWN DAMAGE</h3>
        <table id="ownDamagedTable" cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
            <colgroup class="sortable">
					<col width="130px" />
					<col width="130px" />
					<col width="130px" />
					<col width="130px" />
					<col width="50px" />
				</colgroup>
				<thead>
					<tr>
						<th>
							CC From
						</th>
						<th>
						    CC To
						</th>
						<th>
						    Own Damage Charge (BDT)
						</th>
						<th>
						    ACT liablity (BDT)
						</th>
						<th> <a id="lnkAddInsurance" class="iconlink iconlinkAdd" href="javascript:;">New</a>
						</th>
					</tr>
				</thead>
				<tbody id="insurenceList">
				</tbody>
        </table>
    </div>
    <div class="OtherCharges widthSize80_per">
        <h3>Other Charges</h3>
        <%--<div class="divLeft" style="width: auto;">--%>
            <div class="fieldDiv">
                <label class="lbl lblOtherCharges rightAlign">Accident or Theft(% of vehicle value):</label>
                <input type="text" id="txtaccidentOrTheft" class="txtField"/>
                <input type="hidden" id="txtOtherChargeId" value="0"/>
            </div>
            <div class="fieldDiv">
                <label class="lbl lblOtherCharges rightAlign">Commission(% of Total Amount before VAT):</label>
                <input type="text" id="txtCommision" class="txtField"/>
            </div>
            <div class="fieldDiv">
                <label class="lbl lblOtherCharges rightAlign">VAT(% of Total Insurence Amount):</label>
                <input type="text" id="txtVat" class="txtField"/>
            </div>
            <div class="fieldDiv">
                <label class="lbl lblOtherCharges rightAlign">Passenger per Seating Capacity(BDT):</label>
                <input type="text" id="txtPassenger" class="txtField"/>
            </div>
            <div class="fieldDiv">
                <label class="lbl lblOtherCharges rightAlign">Amount of Driver Seat(BDT):</label>
                <input type="text" id="txtAmountofDriver" class="txtField"/>
            </div>
            <div class="fieldDiv">
                <label class="lbl lblOtherCharges rightAlign">Drivers seat Charge(BDT):</label>
                <input type="text" id="txtDriverSeatCharge" class="txtField"/>
            </div>
            <div class="fieldDiv">
                <label class="lbl lblOtherCharges rightAlign">Stamp Charge(BDT):</label>
                <input type="text" id="txtStampCharge" class="txtField"/>
            </div>
            <br/><br/>
            <div class="btns actionButtons">
			    <a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			    <a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		    </div>
        <%--</div>--%>
        <div class="clear"></div>
    </div>
</div>

</asp:Content>

