﻿//using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Configuration;
using AutoLoanService.Entity.CheckListPrint;
using CrystalDecisions.CrystalReports.Engine;


namespace PlussAndLoan.UI.AutoLoan
{
    public partial class UI_AutoLoan_BASEL2PrintCrystal : System.Web.UI.Page
    {
        private string userId = "";
        private string password = "";
        private string database = "";
        private string server = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Basal2Entity b2 = new Basal2Entity();
                List<Basal2Entity> b2List = new List<Basal2Entity>();

                b2 = (Basal2Entity)Session["Basel2Object"];
                b2List.Add(b2);


                ReportDocument rd = new ReportDocument();
                string path = Server.MapPath("PrintCheckList/basal2.rpt");
                rd.Load(path);
                rd.SetDataSource(b2List);
                crViewer.ReportSource = rd;
                //rd.PrintToPrinter(1, true, 0, 0);


                userId = ConfigurationSettings.AppSettings["userid"];
                password = ConfigurationSettings.AppSettings["password"];
                database = ConfigurationSettings.AppSettings["database"];
                server = ConfigurationSettings.AppSettings["server"];

                SetConnection(rd, database, server, userId, password);

            }
            catch (Exception ex)
            {

            }
        }


        private void SetConnection(ReportDocument report, string databaseName, string serverName, string userName, string password)
        {
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in report.Database.Tables)
            {
                var logOnInfo = table.LogOnInfo;
                var connectionInfo = logOnInfo.ConnectionInfo;

                // Set the Connection parameters.
                connectionInfo.DatabaseName = databaseName;
                connectionInfo.ServerName = serverName;
                connectionInfo.Password = password;
                connectionInfo.UserID = userName;
                table.ApplyLogOnInfo(logOnInfo);
            }
        }







    }
}
