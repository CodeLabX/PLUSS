﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoLoan.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.AutoLoan.IRSettingsForVehicleStatus" Title="IR Settings for Vehicle Status" Codebehind="IRSettingsForVehicleStatus.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    
    <script src="../../Scripts/AutoLoan/IRSettingsForVehicle.js" type="text/javascript"></script>
    
        <style type="text/css">
        #nav_IRSettingsForVehicleStatus a {
                background-color: #F2F6F6;
                color: #3DAE38;
        }
    </style>
    
    
    <div class="pageHeaderDiv">IR Settings for Vehicle Status</div>
    <%--All Aspx code will goes on Content div--%>
    <div id="Content">
    <%--For Pager --%>
    <div class="SearchwithPager">
<%--    <div class="search">
    <input type="text" id="txtSearch" class="txt txtSearch" onkeypress="InsuranceAndBANCASettingsHelper.searchKeypress(event);"/>
    <input type="button" id="btnSearch" class="searchButton" value="Search" />
    </div> 
--%>    <div id="Pager" class="pager"> 
     <label for="txtPageNO" class="pagerlabel">page</label>                                           
     <input id="txtPageNO" class="txtPageNo" value="1" maxlength="7"></input>
     <span id="SpnTotalPage" class="spntotalPage">of 1</span> <a id="lnkPrev"
            title="Previous Page" href="#" class="prevPage">prev</a> <a id="lnkNext" title="Next Page"
                href="#" class="nextPage">next</a>
    </div>
    </div>
    <table cellpadding="0" cellspacing="0" frame="box" border="0" class="summary">
				<colgroup class="sortable">
					<col width="50px" />
					<col width="200px" />
					<col width="150px" />
					<col width="150px" />
					<col width="170px" />
					<col width="150px" />
				</colgroup>
				<thead>
					<tr>
						<th>
							SL
						</th>
						<th>
						    Vehicle Status
						</th>
						<th>
						    IR with Bundle
						</th>
						<th>
						    IR Without Bundle
						</th>
						<th>
						    Valid From
						</th>
						<th> <a id="lnkAddNewIRSettingsForVehicle" class="iconlink iconlinkAdd" href="javascript:;">New IR Settings For Vehicle </a>
						</th>
					</tr>
				</thead>
				<tbody id="IRSettingsList">
				</tbody>
			</table>
    
    </div>
    
    <div class="modal" id="IRSettingsPopupDiv" style="display:none">
		<h3>IR Settings for Vehicle Status</h3>
		<label for="txtHeadline">Vehicle Status</label>
		<select id="cmbVehicleStatus" class="txt" title="Insurance Type" >
		    <%--<option value="1" selected="selected">General</option>
		    <option value="2">Life</option>
		    <option value="3">General & Life</option>--%>
		</select>
		 <br/>
		<label for="txtpostedDate">IR With Bundle</label> <input class="txt" id="txtIRWithARTA" title="IR With ARTA"/><br/>
		 <label for="txtAgeLimit">IR Without Bundle</label> <input class="txt" id="txtIRWithoutARTA" title="Age limit"/><br/>
		<div class="btns">
			<a href="javascript:;" id="lnkSave" class="iconlink iconlinkSave">Save</a>
			<a href="javascript:;" id="lnkClose" class="iconlink iconlinkClose">Close</a>
		</div>
    </div>
    
</asp:Content>

