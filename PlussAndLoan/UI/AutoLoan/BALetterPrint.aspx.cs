﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Entity.CheckListPrint;
using AutoLoanService.Interface;

namespace PlussAndLoan.UI.AutoLoan
{
    public partial class BALetterPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var llid = Request.QueryString["llid"];
            if (llid != "" && IsNumeric(llid))
            {

                divPrintBALetterPage1.Visible = false;
                divBALetterPage2.Visible = false;
                divBALetterPage3.Visible = false;

                LoadBALetterInfo(Convert.ToInt32(llid));

                ConfigureCReport();
            }
        }

        private void ConfigureCReport()
        {
            BAlatterPrintEntity ba = new BAlatterPrintEntity();

            #region value assinging
            ba.Label3 = Label3.Text.Trim();
            ba.lblBranch = lblBranch.Text.Trim();
            ba.lblACNo = lblACNo.Text.Trim();
            ba.lblDate = lblDate.Text.Trim();
            ba.lblApplicationDate = lblApplicationDate.Text.Trim();
            ba.Label12 = Label12.Text.Trim();
            ba.lblLoanAmount = lblLoanAmount.Text.Trim();
            ba.lblInterestRate = lblInterestRate.Text.Trim();
            ba.Label17 = Label17.Text.Trim();
            ba.Label38 = Label38.Text.Trim();
            ba.lblEMIAmount = lblEMIAmount.Text.Trim();
            ba.lblNoOfInstallemt = lblNoOfInstallemt.Text.Trim();
            ba.lblTenor = lblTenor.Text.Trim();
            ba.lblExpiryDate = lblExpiryDate.Text.Trim();
            ba.lblBrand = lblBrand.Text.Trim();
            ba.lblModel = lblModel.Text.Trim();
            ba.Label39 = Label39.Text.Trim();
            ba.lblProcessingFee = lblProcessingFee.Text.Trim();
            ba.lblDPNoteLetter = lblDPNoteLetter.Text.Trim();
            ba.lblDPNoteLetter0 = lblDPNoteLetter0.Text.Trim();
            ba.lblDPNoteLetter1 = lblDPNoteLetter1.Text.Trim();
            ba.lblDPNoteLetter2 = lblDPNoteLetter2.Text.Trim();
            ba.lblDPNoteLetter3 = lblDPNoteLetter3.Text.Trim();
            ba.lblLetterOfHypothecate = lblLetterOfHypothecate.Text.Trim();
            ba.lblDPNoteLetter5 = lblDPNoteLetter5.Text.Trim();
            ba.lblDPNoteLetter6 = lblDPNoteLetter6.Text.Trim();
            ba.lblDPNoteLetter7 = lblDPNoteLetter7.Text.Trim();
            ba.lblSecurity = lblSecurity.Text.Trim();
            ba.lblDPNoteLetter9 = lblDPNoteLetter9.Text.Trim();
            ba.lblLoanAccNo = lblLoanAccNo.Text.Trim();
            ba.lblDebitAuthorityAcc = lblDebitAuthorityAcc.Text.Trim();
            ba.Label40 = Label40.Text.Trim();
            ba.Label41 = Label41.Text.Trim();
            ba.Label42 = Label42.Text.Trim();
            ba.Label43 = Label43.Text.Trim();
            ba.lblDPNoteLetter12 = lblDPNoteLetter12.Text.Trim();
            ba.lblDPNoteLetter13 = lblDPNoteLetter13.Text.Trim();
            ba.Label46 = Label46.Text.Trim();
            ba.lblBranch0 = lblBranch0.Text.Trim();
            ba.lblACNo0 = lblACNo0.Text.Trim();
            ba.lblDate0 = lblDate0.Text.Trim();
            ba.lblApplicationDate0 = lblApplicationDate0.Text.Trim();
            ba.Label9 = Label9.Text.Trim();
            ba.lblLoanAmount0 = lblLoanAmount0.Text.Trim();
            ba.lblInterestRate0 = lblInterestRate0.Text.Trim();
            ba.Label11 = Label11.Text.Trim();
            ba.Label13 = Label13.Text.Trim();
            ba.lblEMIAmount0 = lblEMIAmount0.Text.Trim();
            ba.lblNoOfInstallemt0 = lblNoOfInstallemt0.Text.Trim();
            ba.lblTenor0 = lblTenor0.Text.Trim();
            ba.lblExpiryDate0 = lblExpiryDate0.Text.Trim();
            ba.lblBrand0 = lblBrand0.Text.Trim();
            ba.lblModel0 = lblModel0.Text.Trim();
            ba.Label16 = Label16.Text.Trim();
            ba.lblProcessingFee0 = lblProcessingFee0.Text.Trim();
            ba.lblDPNoteLetter_0 = lblDPNoteLetter_0.Text.Trim();
            ba.Label44 = Label44.Text.Trim();
            ba.Label45 = Label45.Text.Trim();
            ba.Label19 = Label19.Text.Trim();
            ba.Label20 = Label20.Text.Trim();
            ba.Label21 = Label21.Text.Trim();
            ba.Label22 = Label22.Text.Trim();
            ba.lblLetterOfHypothecate0 = lblLetterOfHypothecate0.Text.Trim();
            ba.Label24 = Label24.Text.Trim();
            ba.Label25 = Label25.Text.Trim();
            ba.Label26 = Label26.Text.Trim();
            ba.lblSecurity0 = lblSecurity0.Text.Trim();
            ba.Label28 = Label28.Text.Trim();
            ba.lblLoanAccNo0 = lblLoanAccNo0.Text.Trim();
            ba.lblDebitAuthorityAcc0 = lblDebitAuthorityAcc0.Text.Trim();
            ba.Label31 = Label31.Text.Trim();
            ba.Label32 = Label32.Text.Trim();
            ba.Label33 = Label33.Text.Trim();
            ba.Label34 = Label34.Text.Trim();
            ba.Label35 = Label35.Text.Trim();
            ba.Label36 = Label36.Text.Trim();
            ba.reportType = reportType.Text.Trim();
            #endregion


            Session["BALetter"] = ba;
            Response.Redirect("BALetterPrintCrystal.aspx");


        }

        public static bool IsNumeric(string text)
        {
            return string.IsNullOrEmpty(text) ? false :
                    Regex.IsMatch(text, @"^\s*\-?\d+(\.\d+)?\s*$");
        }
        public string LoadBALetterInfo(int llid)
        {

            #region All Data
            ILoanApplicationRepository repository = new AutoLoanDataService.AutoLoanDataService();
            var loanApplicationService = new LoanApplicationService(repository);
            AutoLoanApplicationAll res = new AutoLoanApplicationAll();
            var res1 = loanApplicationService.getLoanLocetorInfoByLLID(llid);

            res = (AutoLoanApplicationAll)res1;

            #endregion

            #region Source

            IAutobranchAndSource repository1 = new AutoLoanDataService.AutoBranchAndSourceDataService();
            var autoBranchAndSourceService = new AutoBranchAndSourceService(repository1);
            List<AutoBranchWithSource> objAutoBranchAndSourceServiceList = autoBranchAndSourceService.GetAllbranchName();

            #endregion

            #region Manufacture

            ITanorAndLTV repository2 = new AutoLoanDataService.AutoLoanDataService();
            var autoManufactuerService = new TanorAndLTVService(repository2);


            List<AutoMenufacturer> objAutoManufacturerList =
                autoManufactuerService.GetAutoManufacturer();

            #endregion

            #region BA Letter Page 1
            var sourceName = "N/A";
            for (var i = 0; i < objAutoBranchAndSourceServiceList.Count; i++)
            {
                if (objAutoBranchAndSourceServiceList[i].AutoSourceId == res.AutoLoanAnalystApplication.Source)
                {
                    sourceName = objAutoBranchAndSourceServiceList[i].SourceName;
                    break;
                }
            }

            lblBranch.Text = sourceName;
            lblDate.Text = System.DateTime.Now.ToShortDateString();
            lblACNo.Text = (res.objAutoPRAccount.AccountNumberForSCB == "") ? "N/A" : res.objAutoPRAccount.AccountNumberForSCB;
            lblApplicationDate.Text = res.objAutoLoanMaster.AppliedDate.ToShortDateString() == "" ? "N/A" : res.objAutoLoanMaster.AppliedDate.ToShortDateString();
            switch (res.objAutoLoanMaster.ProductId)
            {
                case 1:
                    chkStaffAuto.Checked = true;
                    break;
                case 2:
                    chkConventionalAuto.Checked = true;
                    break;
                case 3:
                    chkSaadiqAuto.Checked = true;
                    break;
                default:
                    chkStaffAuto.Checked = false;
                    chkConventionalAuto.Checked = false;
                    chkSaadiqAuto.Checked = false;
                    break;

            }

            lblLoanAmount.Text = Convert.ToString(res.objAuto_An_LoanCalculationTotal.TotalLoanAmount) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_LoanCalculationTotal.TotalLoanAmount);
            lblEMIAmount.Text = Convert.ToString(res.objAuto_An_RepaymentCapability.MaxEMI) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_RepaymentCapability.MaxEMI);
            lblTenor.Text = Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredTenor) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredTenor);

            var manufactureName = "N/A";

            for (var i = 0; i < objAutoManufacturerList.Count; i++)
            {
                if (res.objAutoLoanVehicle.Auto_ManufactureId == objAutoManufacturerList[i].ManufacturerId)
                {
                    manufactureName = objAutoManufacturerList[i].ManufacturerName;
                    break;
                }
            }


            lblBrand.Text = manufactureName;
            lblInterestRate.Text = Convert.ToString(res.objAuto_An_LoanCalculationInterest.ConsideredRate) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_LoanCalculationInterest.ConsideredRate);
            lblNoOfInstallemt.Text = Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredInMonth) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredInMonth);
            lblExpiryDate.Text = "";
            lblModel.Text = res.objAutoLoanVehicle.Model == "" ? "N/A" : res.objAutoLoanVehicle.Model;
            lblProcessingFee.Text = "Not Found";

            // lblDPNoteLetter.Text = "Not Found";
            lblLetterOfHypothecate.Text = res.objAuto_An_AlertVerificationReport.HypothecationCheck == "" ? "N/A" : res.objAuto_An_AlertVerificationReport.HypothecationCheck;
            lblSecurity.Text = res.objAuto_An_FinalizationRepaymentMethod.SecurityPDC == 1 ? "Yes" : "No";

            lblLoanAccNo.Text = res.objAuto_An_FinalizationRepaymentMethod.AccountNumber == "" ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.AccountNumber;
            lblDebitAuthorityAcc.Text = res.objAuto_An_FinalizationRepaymentMethod.SCBAccount == "" ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.SCBAccount;
            #endregion

            #region BA Letter Page-2
            lblBranch0.Text = sourceName;
            lblDate0.Text = System.DateTime.Now.ToShortDateString();
            lblACNo0.Text = (res.objAutoPRAccount.AccountNumberForSCB == "") ? "N/A" : res.objAutoPRAccount.AccountNumberForSCB;
            lblApplicationDate0.Text = res.objAutoLoanMaster.AppliedDate.ToShortDateString() == "" ? "N/A" : res.objAutoLoanMaster.AppliedDate.ToShortDateString();
            switch (res.objAutoLoanMaster.ProductId)
            {
                case 1:
                    chkStaffAuto0.Checked = true;
                    break;
                case 2:
                    chkConventionalAuto0.Checked = true;
                    break;
                case 3:
                    chkSaadiqAuto0.Checked = true;
                    break;
                default:
                    chkStaffAuto0.Checked = false;
                    chkConventionalAuto0.Checked = false;
                    chkSaadiqAuto0.Checked = false;
                    break;

            }

            lblLoanAmount0.Text = Convert.ToString(res.objAuto_An_LoanCalculationTotal.TotalLoanAmount) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_LoanCalculationTotal.TotalLoanAmount);
            lblEMIAmount0.Text = Convert.ToString(res.objAuto_An_RepaymentCapability.MaxEMI) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_RepaymentCapability.MaxEMI);
            lblTenor0.Text = Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredTenor) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredTenor);

            lblBrand0.Text = manufactureName;
            lblInterestRate0.Text = Convert.ToString(res.objAuto_An_LoanCalculationInterest.ConsideredRate) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_LoanCalculationInterest.ConsideredRate);
            lblNoOfInstallemt0.Text = Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredInMonth) == "0" ? "N/A" : Convert.ToString(res.objAuto_An_LoanCalculationTenor.ConsideredInMonth);

            lblExpiryDate.Text = "";
            lblModel0.Text = res.objAutoLoanVehicle.Model == "" ? "N/A" : res.objAutoLoanVehicle.Model;
            //lblProcessingFee.Text = "Not Found";

            //// lblDPNoteLetter.Text = "Not Found";
            lblLetterOfHypothecate0.Text = res.objAuto_An_AlertVerificationReport.HypothecationCheck == "" ? "N/A" : res.objAuto_An_AlertVerificationReport.HypothecationCheck;
            lblSecurity0.Text = res.objAuto_An_FinalizationRepaymentMethod.SecurityPDC == 1 ? "Yes" : "No";

            lblLoanAccNo0.Text = res.objAuto_An_FinalizationRepaymentMethod.AccountNumber == "" ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.AccountNumber;
            lblDebitAuthorityAcc0.Text = res.objAuto_An_FinalizationRepaymentMethod.SCBAccount == "" ? "N/A" : res.objAuto_An_FinalizationRepaymentMethod.SCBAccount;
            #endregion

            if (res.objAutoLoanMaster.ProductId == 3)
            {
                divPrintBALetterPage1.Visible = true;
                reportType.Text = "3";
            }
            else
            {
                divBALetterPage2.Visible = true;
                divBALetterPage3.Visible = true;
            }

            return "Success";
        }

    }
}
