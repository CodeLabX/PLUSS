﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.LoanLocatorReports;

namespace PlussAndLoan.UI
{
    public partial class IndividualTatByDate : System.Web.UI.Page
    {
        private readonly LocAdminTatReportManager _locAdminManager = new LocAdminTatReportManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            string product = (string)Session["prod"];
            string fromDate = (string)Session["d1"];
            string toDate = (string)Session["d2"];

            productDiv.InnerText = product;
            formDateDiv.InnerText = fromDate;
            toDateDiv.InnerText = toDate;

            DataTable tatDt = (DataTable)Session["inTAT"];
            if (tatDt != null)
            {


                for (int j = 0; j < tatDt.Rows.Count; j++)
                {
                    var loanId = Convert.ToInt32(tatDt.Rows[j]["LOAN_APPL_ID"]);
                    var applyDate = tatDt.Rows[j]["APPL_DATE"].ToString();

                    DataTable dt = GetReportData(loanId);



                    var TAT_SOURCE1 = "N/A";
                    var TAT_SOURCE2 = "N/A";
                    var TAT_SOURCE3 = "N/A";
                    var TAT_CREDIT1 = "N/A";
                    var TAT_CREDIT2 = "N/A";
                    var TAT_CREDIT3 = "N/A";
                    var TAT_CREDIT_VALUATION = "N/A";
                    var TAT_CREDIT_LEGAL = "N/A";
                    var TAT_CREDIT_LEGAL_ON_PROCESS = "N/A";
                    var TAT_CREDIT_ONHOLD_ON_PROCESS = "N/A";
                    var TAT_CREDIT_ONHOLD = "N/A";
                    var TAT_ASSET1 = "N/A";
                    var TAT_ASSET2 = "N/A";
                    var TAT_ASSET3 = "N/A";
                    var TAT_ASSET_ONHOLD = "N/A";
                    var TAT_CREDIT_VERIFICATION = "N/A";
                    var TAT_VERIF = "N/A";
                    var TAT_APPROVED = "N/A";
                    var TAT_DISBURSED = "N/A";
                    var TAT_CREDIT_COND_APPROVED = "N/A";

                    var TAT_CREDIT1_ON_PROCESS = "N/A";
                    var TAT_CREDIT2_ON_PROCESS = "N/A";
                    var TAT_CREDIT3_ON_PROCESS = "N/A";
                    var TAT_CREDIT_VALUATION_ON_PROCESS = "N/A";

                    var TAT_ASSET1_ON_PROCESS = "N/A";
                    var TAT_ASSET2_ON_PROCESS = "N/A";
                    var TAT_ASSET3_ON_PROCESS = "N/A";
                    var TAT_ASSET_ONHOLD_ON_PROCESS = "N/A";
                    var TAT_CREDIT_VERIFICATION_ON_PROCESS = "N/A";
                    var date = "NULL";

                    var s = 1;
                    var c = 1;
                    var a = 1;

                    if (dt != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var id = Convert.ToInt32(dt.Rows[i]["ID"]);

                            if (dt.Rows[i]["TrType"].ToString() == "1Source")
                            {
                                if (s == 1)
                                {
                                    TAT_SOURCE1 = dt.Rows[i]["DATE_TIME"].ToString();
                                }
                                else if (s == 2)
                                {
                                    TAT_SOURCE2 = dt.Rows[i]["DATE_TIME"].ToString();
                                }
                                else if (s == 3)
                                {
                                    TAT_SOURCE3 = dt.Rows[i]["DATE_TIME"].ToString();
                                }
                                s = s + 1;
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "3Credit")
                            {
                                if (c == 1)
                                {
                                    TAT_CREDIT1 = dt.Rows[i]["DATE_TIME"].ToString();
                                    date = getDateFromDetails(loanId, id, "ascDate");
                                    if (date != null)
                                    {
                                        TAT_CREDIT1_ON_PROCESS = date;
                                    }
                                }
                                else if (c == 2)
                                {
                                    TAT_CREDIT2 = dt.Rows[i]["DATE_TIME"].ToString();
                                    date = getDateFromDetails(loanId, id, "ascDate");
                                    if (date != null)
                                    {
                                        TAT_CREDIT2_ON_PROCESS = date;
                                    }
                                }
                                else if (c == 3)
                                {
                                    TAT_CREDIT3 = dt.Rows[i]["DATE_TIME"].ToString();
                                    date = getDateFromDetails(loanId, id, "ascDate");
                                    if (date != null)
                                    {
                                        TAT_CREDIT3_ON_PROCESS = date;
                                    }
                                }
                                c = c + 1;
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_valuation")
                            {
                                TAT_CREDIT_VALUATION = dt.Rows[i]["DATE_TIME"].ToString();
                                date = getDateFromDetails(loanId, id, "descDate");
                                if (date != null)
                                {
                                    TAT_CREDIT_VALUATION_ON_PROCESS = date;
                                }
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_legal")
                            {
                                TAT_CREDIT_LEGAL = dt.Rows[i]["DATE_TIME"].ToString();
                                date = getDateFromDetails(loanId, id, "descDate");
                                if (date != null)
                                {
                                    TAT_CREDIT_LEGAL_ON_PROCESS = date;
                                }
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_onhold")
                            {
                                TAT_CREDIT_ONHOLD = dt.Rows[i]["DATE_TIME"].ToString();
                                date = getDateFromDetails(loanId, id, "descDate");
                                if (date != null)
                                {
                                    TAT_CREDIT_ONHOLD_ON_PROCESS = date;
                                }
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_Cond_approved")
                            {
                                TAT_CREDIT_COND_APPROVED = dt.Rows[i]["DATE_TIME"].ToString();
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "6Asset ops")
                            {
                                if (a == 1)
                                {
                                    TAT_ASSET1 = dt.Rows[i]["DATE_TIME"].ToString();
                                    date = getDateFromDetails(loanId, id, "ascDate");
                                    if (date != null)
                                    {
                                        TAT_ASSET1_ON_PROCESS = date;
                                    }
                                }
                                else if (a == 2)
                                {
                                    TAT_ASSET2 = dt.Rows[i]["DATE_TIME"].ToString();
                                    date = getDateFromDetails(loanId, id, "ascDate");
                                    if (date != null)
                                    {
                                        TAT_ASSET2_ON_PROCESS = date;
                                    }
                                }
                                else if (a == 3)
                                {
                                    TAT_ASSET3 = dt.Rows[i]["DATE_TIME"].ToString();
                                    date = getDateFromDetails(loanId, id, "ascDate");
                                    if (date != null)
                                    {
                                        TAT_ASSET3_ON_PROCESS = date;
                                    }
                                }
                                a = a + 1;
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "6Assetops_onhold")
                            {
                                TAT_ASSET_ONHOLD = dt.Rows[i]["DATE_TIME"].ToString();
                                date = getDateFromDetails(loanId, id, "descDate");
                                if (date != null)
                                {
                                    TAT_ASSET_ONHOLD_ON_PROCESS = date;
                                }
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_approved")
                            {
                                TAT_APPROVED = dt.Rows[i]["DATE_TIME"].ToString();
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "6Disbursed")
                            {
                                TAT_DISBURSED = dt.Rows[i]["DATE_TIME"].ToString();
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_verification")
                            {
                                TAT_CREDIT_VERIFICATION = dt.Rows[i]["DATE_TIME"].ToString();
                                date = getDateFromDetails(loanId, id, "descDate");
                                if (date != null)
                                {
                                    TAT_CREDIT_VERIFICATION_ON_PROCESS = date;
                                }
                            }

                        }

                        TableRow row = new TableRow();
                        row.Cells.Add(new TableCell { Text = loanId.ToString() });
                        row.Cells.Add(new TableCell { Text = product });
                        row.Cells.Add(new TableCell { Text = applyDate });
                        row.Cells.Add(new TableCell { Text = TAT_SOURCE1.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_SOURCE2.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_SOURCE3.ToString() });

                        row.Cells.Add(new TableCell { Text = TAT_CREDIT1_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT1.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT2_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT2.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT3_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT3.ToString() });

                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_ONHOLD.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_ONHOLD_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_VALUATION.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_VALUATION_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_LEGAL.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_LEGAL_ON_PROCESS.ToString() });

                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_VERIFICATION.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_VERIFICATION_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_COND_APPROVED.ToString() });

                        row.Cells.Add(new TableCell { Text = TAT_ASSET1_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET1.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET2_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET2.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET3_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET3.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET_ONHOLD.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET_ONHOLD_ON_PROCESS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_APPROVED.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_DISBURSED.ToString() });
                        dynamicTable.Rows.Add(row);


                    }

                }
            }
        }


        public DataTable GetReportData(int loanId)
        {
            var results = _locAdminManager.GetReportData(loanId);
            return results;
        }

        public string getDateFromDetails(int loanId, int id, string order)
        {
            var date = _locAdminManager.getDateFromDetails(loanId, id, order);
            return date;

        }

    }
}