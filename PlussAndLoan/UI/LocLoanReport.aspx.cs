﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Configuration;
using System.Web.Services.Description;
using BLL;
using BLL.LoanLocatorReports;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_LocLoanReport : System.Web.UI.Page
    {
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        public readonly LocLoanReportManager _loanReportManager = new LocLoanReportManager();
        string m1 = "";
        private readonly LocAdminTatReportManager _locAdminManager = new LocAdminTatReportManager();

        protected void Page_Load(object sender, EventArgs e)
        {
        

            if (!IsPostBack)
            {
                productDropDownList.DataSource = AllProductList();
                productDropDownList.DataTextField = "ProductName";
                productDropDownList.DataValueField = "ProductId";
                productDropDownList.DataBind();

                DayDropDownList.DataSource = AllDayList();
                DayDropDownList.DataBind();
                DayDropDownList.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;

                MonthDropDownList.DataSource = AllMonthList();
                MonthDropDownList.DataTextField = "Value";
                MonthDropDownList.DataValueField = "Key";
                MonthDropDownList.DataBind();
                MonthDropDownList.SelectedIndex = DateTime.Now.Month - 1;

                YearDropDownList.DataSource = AllyearList();
                YearDropDownList.DataBind();
                YearDropDownList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;

                ToDateDayList.DataSource = AllDayList();
                ToDateDayList.DataBind();
                ToDateDayList.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;

                ToDateMonthList.DataSource = AllMonthList();
                ToDateMonthList.DataTextField = "Value";
                ToDateMonthList.DataValueField = "Key";
                ToDateMonthList.DataBind();
                ToDateMonthList.SelectedIndex = DateTime.Now.Month - 1;
                ToDateYearList.DataSource = AllyearList();
                ToDateYearList.DataBind();
                ToDateYearList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
                DropDownYearList.DataSource = AllyearList();
                DropDownYearList.DataBind();
                DropDownYearList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
                DropDownMonthList.DataSource = AllMonthList();
                DropDownMonthList.DataTextField = "Value";
                DropDownMonthList.DataValueField = "Key";
                DropDownMonthList.DataBind();
                DropDownMonthList.SelectedIndex = DateTime.Now.Month - 1;

                DropDownProductList.DataSource = AllProductList();
                DropDownProductList.DataTextField = "ProductName";
                DropDownProductList.DataValueField = "ProductId";
                DropDownProductList.DataBind();

                DropDownaPerfometerMonth.DataSource = AllMonthList();
                DropDownaPerfometerMonth.DataTextField = "Value";
                DropDownaPerfometerMonth.DataValueField = "Key";
                DropDownaPerfometerMonth.DataBind();
                DropDownaPerfometerMonth.SelectedIndex = DateTime.Now.Month - 1;
                DropDownaPerfometerYear.DataSource = AllyearList();
                DropDownaPerfometerYear.DataBind();
                DropDownaPerfometerYear.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
                DropDownaPerfometerProduct.DataSource = AllProductList();
                DropDownaPerfometerProduct.DataTextField = "ProductName";
                DropDownaPerfometerProduct.DataValueField = "ProductId";
                DropDownaPerfometerProduct.DataBind();

                CappingDayDropDown.DataSource = AllDayList();
                CappingDayDropDown.DataBind();
                CappingDayDropDown.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;
                CappingMonthDropDown.DataSource = AllMonthList();
                CappingMonthDropDown.DataTextField = "Value";
                CappingMonthDropDown.DataValueField = "Key";
                CappingMonthDropDown.DataBind();
                CappingMonthDropDown.SelectedIndex = DateTime.Now.Month - 1;
                CappingYearDropDown.DataSource = AllyearList();
                CappingYearDropDown.DataBind();
                CappingYearDropDown.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
                CappingToDayDropDown.DataSource = AllDayList();
                CappingToDayDropDown.DataBind();
                CappingToDayDropDown.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;
                CappingToMonthDropDown.DataSource = AllMonthList();
                CappingToMonthDropDown.DataTextField = "Value";
                CappingToMonthDropDown.DataValueField = "Key";
                CappingToMonthDropDown.DataBind();
                CappingToMonthDropDown.SelectedIndex = DateTime.Now.Month - 1;
                CappingToYearDropDown.DataSource = AllyearList();
                CappingToYearDropDown.DataBind();
                CappingToYearDropDown.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;

                ProductListDropDown.DataSource = AllProductList();
                ProductListDropDown.DataTextField = "ProductName";
                ProductListDropDown.DataValueField = "ProductId";
                ProductListDropDown.DataBind();
                AppMonthDropDown.DataSource = AllMonthList();
                AppMonthDropDown.DataTextField = "Value";
                AppMonthDropDown.DataValueField = "Key";
                AppMonthDropDown.DataBind();
                AppMonthDropDown.SelectedIndex = DateTime.Now.Month - 1;
                AppYearDropDown.DataSource = AllyearList();
                AppYearDropDown.DataBind();
                AppYearDropDown.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;

                m1 = CappingMonthDropDown.SelectedValue;

            }

        }



        #region DropDown Value List

        public List<int> AllyearList()
        {
            List<int> yearList = new List<int>();
            for (int i = 2004; i < 2051; i++)
            {
                yearList.Add(i);
            }
            return yearList;

        }

        public List<int> AllDayList()
        {
            List<int> dayList = new List<int>();
            for (int i = 1; i < 32; i++)
            {
                dayList.Add(i);
            }
            return dayList;

        }

        public Dictionary<int, string> AllMonthList()
        {
            IDictionary<int, string> months = new Dictionary<int, string>();
            months.Add(1, "January");
            months.Add(2, "February");
            months.Add(3, "March");
            months.Add(4, "April");
            months.Add(5, "May");
            months.Add(6, "June");
            months.Add(7, "July");
            months.Add(8, "August");
            months.Add(9, "September");
            months.Add(10, "October");
            months.Add(11, "November");
            months.Add(12, "December");

            return (Dictionary<int, string>) months;
        }


        public List<Product> AllProductList()
        {
            List<Product> productListObj = _loanApplicationManagerObj.GetAllProduct();
            var productObj = new Product {ProductId = 0, ProductName = "Select Product"};
            productListObj.Insert(0, productObj);
            return productListObj;
        }

        #endregion


        #region Application Status Wise Reprot

        protected void btnShow_Click(object sender, EventArgs e)
        {

            int y1 = Convert.ToInt32(YearDropDownList.SelectedValue);
            string m1 = MonthDropDownList.SelectedValue;
            string d1 = DayDropDownList.SelectedValue;
            if (m1.Length==1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;

            Session["dateOne"] = date1;
            int y2 = Convert.ToInt32(ToDateYearList.SelectedValue);
            string m2 = ToDateMonthList.SelectedValue;
            string d2 = ToDateDayList.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;
            Session["dateTwo"] = date2;
            int productId = Convert.ToInt32(productDropDownList.SelectedValue);
            string productName = productDropDownList.SelectedItem.ToString();
            Session["product"] = productName;
            int status = Convert.ToInt32(appr_disb.SelectedValue);
            string statusName = appr_disb.SelectedItem.ToString();
            Session["status"] = statusName;

            var roleCutDate = WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"];

            var ss = ApplicationStatusWiseReprot(date1, date2, productId, status, roleCutDate);

           Session["results"] = ss;
           Response.Redirect("ApplicationStatusWiseReprot.aspx");

        }

        public DataTable ApplicationStatusWiseReprot(string date1, string date2, int productId, int status, string roleCutDate)
        {
            var results = _loanReportManager.ApplicationStatusWiseReprot(date1, date2, productId, status, roleCutDate);
            
            return results;
        }

        #endregion



        #region List Of Application Deferred/Declined- According To Reasons

        protected void btnAppShow_Click(object sender, EventArgs e)
        {
            int producId = Convert.ToInt32(ProductListDropDown.SelectedValue);
            string productName = ProductListDropDown.SelectedItem.ToString();
            Session["proName"] = productName;
            int status = Convert.ToInt32(statDropDownList.SelectedValue);
           string stat=statDropDownList.SelectedItem.ToString();
            Session["state"] = stat;
            string month = AppMonthDropDown.SelectedValue;
            string m = AppMonthDropDown.SelectedItem.ToString();
            Session["month"] = m; 
            if (month.Length ==1)
            {
                month = "0" + month;
            }
            int year = Convert.ToInt32(AppYearDropDown.SelectedValue);
            Session["year"] = year;
            string date = "%"+year + "-" + month+"%";
            
           var app= ApplicationDeferredOrDeclinedReprot(producId, status, date);
            Session["appReport"] = app;
            Response.Redirect("ApplicationDeferredOrDeclined.aspx");

        }

        public DataTable ApplicationDeferredOrDeclinedReprot(int producId, int status, string date) 
        {
            var results = _loanReportManager.ApplicationDeferredOrDeclinedReprot(producId, status, date);

            return results;
        }

        #endregion


        #region View Quick  Report
        protected void btnQuickReportMonthly_Click(object sender, EventArgs e)
        {

            int productId = Convert.ToInt32(DropDownProductList.SelectedValue);
            Session["proId"] = productId;
            string productName = DropDownProductList.SelectedItem.ToString();
            Session["ProName"] = productName;
            string month = DropDownMonthList.SelectedValue;
            Session["monthNo"] = month;
            string monthName = DropDownMonthList.SelectedItem.ToString();
            Session["monthName"] = monthName;
            int year = Convert.ToInt32(DropDownYearList.SelectedValue);

            if (month.Length==1)
            {
                month = "0" + month;
            }
            Session["year"] = year;
            int buttonId = 1;
            Session["button"] = buttonId;
            var res = GetQuickReport(year, month, productId, buttonId);
            Session["res"] = res;
            var resData = GetCalculatingVol(year, month, productId, buttonId);
            Session["resData"] = resData;
            Response.Redirect("QuickReport.aspx"); 

        }
        protected void btnQuickReportYearly_Click(object sender, EventArgs e)
        {
            int productId = Convert.ToInt32(DropDownProductList.SelectedValue);
            Session["proId"] = productId;
            string productName = DropDownProductList.SelectedItem.ToString();
            Session["ProName"] = productName;
            string month = DropDownMonthList.SelectedValue;
            Session["monthNo"] = month;
            string monthName = DropDownMonthList.SelectedItem.ToString();
            Session["monthName"] = monthName;
            int year = Convert.ToInt32(DropDownYearList.SelectedValue);

            if (month.Length == 1)
            {
                month = "0" + month;
            }
            Session["year"] = year;
            int buttonId = 2;
            Session["button"] = buttonId;
            var res = GetQuickReport(year, month, productId, buttonId);
            Session["res"] = res;
            var resData = GetCalculatingVol(year, month, productId, buttonId);
            Session["resData"] = resData;
            Response.Redirect("QuickReport.aspx");
        }

        public DataTable GetQuickReport(int year, string month, int productId, int buttonId)
        {
            var res = _locAdminManager.GetQuickReport(year, month, productId, buttonId);
            return res;
        }
        public DataTable GetCalculatingVol(int year, string month, int productId, int buttonId)
        {
            var resData = _locAdminManager.GetCalculatingVol(year, month, productId, buttonId);
            return resData;
        }
        #endregion


        #region aPerfometer
        protected void btnaPerfometer_Click(object sender, EventArgs e)
        {
            int productId = Convert.ToInt32(DropDownaPerfometerProduct.SelectedValue);
            Session["ProdId"] = productId;
            string productName = DropDownaPerfometerProduct.SelectedItem.ToString();
            Session["ProdName"] = productName;

            string monthNo = DropDownaPerfometerMonth.SelectedValue;
            if (monthNo.Length == 1)
            {
                monthNo = "0" + monthNo;
            }
            Session["monthNo"] = monthNo; 
            string monthName = DropDownaPerfometerMonth.SelectedItem.ToString();
            Session["monthName"] = monthName;
            string year = DropDownaPerfometerYear.SelectedValue;
            Session["year"] = year;
            bool personWise = aPerfometerCheckBox.Checked;
            Session["cb1"] = personWise;

            Response.Redirect("PerformanceReport.aspx");

        }

        #endregion

        #region Capping Report
        //___________Product Wise Capping Report__________
        protected void btnprodcapp_Click(object sender, EventArgs e) 
        {

            int y1 = Convert.ToInt32(CappingYearDropDown.SelectedValue);
            string m1 = CappingMonthDropDown.SelectedValue;
            string d1 = CappingDayDropDown.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;


            int y2 = Convert.ToInt32(CappingToYearDropDown.SelectedValue);
            string m2 = CappingToMonthDropDown.SelectedValue;
            string d2 = CappingToDayDropDown.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;

            int buttonId = 1;
            int capptype = 1;

            Session["date1"] = date1;
            Session["date2"] = date2;
            var prodWiseCapp = CappingReport(date1, date2, buttonId, capptype);
            Session["prodWiseCapp"] = prodWiseCapp;
            Response.Redirect("ProductWiseCappingReport.aspx");
            
        }

        //___________Tenor Wise Capping Report__________
        protected void btntenocapp_Click(object sender, EventArgs e)
        {
            int y1 = Convert.ToInt32(CappingYearDropDown.SelectedValue);
            string m1 = CappingMonthDropDown.SelectedValue;
            string d1 = CappingDayDropDown.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;


            int y2 = Convert.ToInt32(CappingToYearDropDown.SelectedValue);
            string m2 = CappingToMonthDropDown.SelectedValue;
            string d2 = CappingToDayDropDown.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;

            int buttonId = 2;
            int capptype = 1;

            var tenorCap =CappingReport(date1, date2, buttonId, capptype);
            Session["tenorCap"] = tenorCap;
            Session["date1"] = date1;
            Session["date2"] = date2;
            Response.Redirect("TenorWiseCappingReport.aspx");
        }
        //___________Level Wise for SME__________
        protected void btnlevelcapp_Click(object sender, EventArgs e)
        {
            int y1 = Convert.ToInt32(CappingYearDropDown.SelectedValue);
            string m1 = CappingMonthDropDown.SelectedValue;
            string d1 = CappingDayDropDown.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;


            int y2 = Convert.ToInt32(CappingToYearDropDown.SelectedValue);
            string m2 = CappingToMonthDropDown.SelectedValue;
            string d2 = CappingToDayDropDown.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;

            int buttonId = 3;
            int capptype = 1;

            var smeCapp =CappingReport(date1, date2, buttonId, capptype);
            var level =GetLevelCap(capptype);
            Session["Capp"] = smeCapp;
            Session["level"] = level;
            Session["date1"] = date1;
            Session["date2"] = date2;
            //Session["button"] = 1;
            var total = GetTotal(date1, date2, capptype);
            Session["total"] = total;
            Response.Redirect("LevelWiseCapping.aspx");
           
        }
        //___________Level Wise for PL__________
        protected void btnlevelcapp2_Click(object sender, EventArgs e)
        {
            int y1 = Convert.ToInt32(CappingYearDropDown.SelectedValue);
            string m1 = CappingMonthDropDown.SelectedValue;
            string d1 = CappingDayDropDown.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;

            int y2 = Convert.ToInt32(CappingToYearDropDown.SelectedValue);
            string m2 = CappingToMonthDropDown.SelectedValue;
            string d2 = CappingToDayDropDown.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;

            int buttonId = 3;
            int capptype = 2;

            var plCapp=CappingReport(date1, date2, buttonId, capptype);
            var level=GetLevelCap(capptype);

            Session["Capp"] = plCapp;
            Session["level"] = level;
            Session["date1"] = date1;
            Session["date2"] = date2;
            //Session["button"] = 2;
            var total = GetTotal(date1, date2, capptype);
            Session["total"] = total;
            Response.Redirect("LevelWiseCapping.aspx");
            
        }
        //___________Level Wise for AL__________
        protected void btnlevelcapp3_Click(object sender, EventArgs e)
        {
            int y1 = Convert.ToInt32(CappingYearDropDown.SelectedValue);
            string m1 = CappingMonthDropDown.SelectedValue;
            string d1 = CappingDayDropDown.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;


            int y2 = Convert.ToInt32(CappingToYearDropDown.SelectedValue);
            string m2 = CappingToMonthDropDown.SelectedValue;
            string d2 = CappingToDayDropDown.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;

            int buttonId = 3;
            int capptype = 3;

            var alCapp=CappingReport(date1, date2, buttonId, capptype);
            var level=GetLevelCap(capptype);

            Session["Capp"] = alCapp;
            Session["level"] = level;
            Session["date1"] = date1;
            Session["date2"] = date2;
            //Session["button"] = 3;
            var total = GetTotal(date1, date2, capptype);
            Session["total"] = total;
            Response.Redirect("LevelWiseCapping.aspx");
           
        }
        //___________Level Wise for Flexi__________
        protected void btnlevelcapp45_Click(object sender, EventArgs e)
        {
            int y1 = Convert.ToInt32(CappingYearDropDown.SelectedValue);
            string m1 = CappingMonthDropDown.SelectedValue;
            string d1 = CappingDayDropDown.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;


            int y2 = Convert.ToInt32(CappingToYearDropDown.SelectedValue);
            string m2 = CappingToMonthDropDown.SelectedValue;
            string d2 = CappingToDayDropDown.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;

            int buttonId = 3;
            int capptype = 4;

            var flexiCapp=CappingReport(date1, date2, buttonId, capptype);
            var level=GetLevelCap(capptype);
            var total = GetTotal(date1, date2, capptype);
            Session["total"] = total;
            Session["Capp"] = flexiCapp;
            Session["level"] = level;
            
            Session["date1"] = date1;
            Session["date2"] = date2;
            //Session["button"] = 4;
            Response.Redirect("LevelWiseCapping.aspx");

        }

        public DataTable CappingReport(string date1, string date2, int buttonId, int capptype)
        {
            DataTable results = _loanReportManager.CappingReport(date1, date2, buttonId, capptype);
 
            return results;
        }

        public DataTable GetLevelCap(int capptype)
        {
            DataTable level = _loanReportManager.GetLevelCap(capptype);
            return level;
        }

        public DataTable GetTotal(string date1, string date2,  int capptype)
        {
            DataTable total = _loanReportManager.GetTotal(date1, date2, capptype);
            return total;
        }

        //___________Segment Wise for PL__________
        protected void btnSEGMENT_Click(object sender, EventArgs e)
        {
            
            
                m1 = CappingMonthDropDown.SelectedValue;
            
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }

            var pl =SegmentWiseCappingReport(m1);
            var segment= SegmentWiseCappingMonth(m1);
            Session["m1"] = m1;
            Session["pl"] = pl;
            Session["segmnt"] = segment;
            Response.Redirect("SegmentWiseCapping.aspx");
        }

        public DataTable SegmentWiseCappingReport(string m1)
        {
            var result = _loanReportManager.SegmentWiseCappingReport(m1);
            return result;
        }

        public DataTable SegmentWiseCappingMonth(string m1)
        {
            var res = _loanReportManager.SegmentWiseCappingMonth(m1);
            return res;
        }


        #endregion

       

    }
}