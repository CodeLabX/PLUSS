﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI.UserControls.UCs
{
    public partial class ModalUserControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void Show(string message, string title)
        {
            message = message.Replace("'", "\"").Replace("\r\n", " ");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + message + "','" + title + "');", true);
        }
    }
}