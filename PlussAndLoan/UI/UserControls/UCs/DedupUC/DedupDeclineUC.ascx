﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DedupDeclineUC.ascx.cs" Inherits="PlussAndLoan.UI.UserControls.UCs.DedupUC.DedupDeclineUC" %>

<%@ Register Src="~/UI/UserControls/css/spreadsheet.ascx" TagPrefix="uc1" TagName="spreadsheet" %>
<%@ Register Src="~/UI/UserControls/css/mydatepickercss.ascx" TagPrefix="uc1" TagName="mydatepickercss" %>
<%@ Register Src="~/UI/UserControls/js/jqueryLib1_3_2min.ascx" TagPrefix="uc1" TagName="jqueryLib1_3_2min" %>
<%@ Register Src="~/UI/UserControls/js/datepicker.ascx" TagPrefix="uc1" TagName="datepicker" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="ucModal" TagName="ModalUserControl" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Decline</title>
    
    <uc1:spreadsheet runat="server" ID="spreadsheet" />
    <uc1:mydatepickercss runat="server" ID="mydatepickercss" />

    <uc1:jqueryLib1_3_2min runat="server" ID="jqueryLib1_3_2min" />
    <uc1:datepicker runat="server" ID="datepicker" />

  

    <script type="text/javascript">
                    $(function(){                    
                    $('#declineDateTextBox').datepicker({showOn:'button',buttonImageOnly:false,
                changeMonth:this,changeYear:true,dateFormat:'dd-mm-yy' });
                });
    </script>

    <script type="text/javascript">
    function JS_FunctionCheckRequest()
    {
        if (confirm("Are you sure want to decline ?")) {
            return true;
        }
         else {
            return false;
        }
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
<ucModal:ModalUserControl runat="server" ID="modalPopup" />

    <div>
        <fieldset>
            <legend>Applicant Information</legend>
            <table border="0" cellpadding="1" cellspacing="1">
                <tr>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td style="width: 220px">
                        &nbsp;
                    </td>
                    <td align="right" valign="top">
                        &nbsp;
                    </td>
                    <td valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonDedupIdLabel" runat="server" Text="Loan Locator Id :"></asp:Label>
                    </td>
                    <td style="width: 220px">
                        <asp:TextBox ID="negativeListPersonLLIdTextBox" runat="server" MaxLength="9"></asp:TextBox>
                    </td>
                    <td align="right" valign="top">
                        &nbsp;
                    </td>
                    <td valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonApplicantNameLabel" runat="server" Text="Applicant Name :"></asp:Label>
                    </td>
                    <td style="width: 220px">
                        <asp:TextBox ID="negativeListPersonApplicantNameTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" style="width: 150px">
                        <asp:Label ID="negativeListPersonDateOfBirthLabel" runat="server" Text="Date of Birth :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonDobTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonFatherNameLabel" runat="server" Text="Father's Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonFatherNameTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="negativeListPersonMotherNameLabel" runat="server" Text="Mother's Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonMotherNameTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonEducationalLevelLabel" runat="server" Text="Educational Level :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="negativeListPersonEducationalDropDownList" runat="server" Width="220px"
                            ReadOnly="True">
                        </asp:DropDownList>
                    </td>
                    <td align="right">
                        <asp:Label ID="negativeListPersonMaritalStatusLabel" runat="server" Text="Marital Status :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="negativeListPersonMaritalStatusDropDownList" runat="server"
                            Width="200px" ReadOnly="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="negativeListPersonResidentialAddressLabel" runat="server" Text="Residential Address :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonResidentialAddressTextBox" runat="server" TextMode="MultiLine"
                            Width="220px" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right" valign="top">
                        <asp:Label ID="negativeListPersonResidentailStatusLabel" runat="server" Text="Residentail Status :"></asp:Label>
                    </td>
                    <td valign="top">
                        <asp:DropDownList ID="negativeListPersonResidentialStatusDropDownList" runat="server"
                            ReadOnly="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="negativeListPersonContactNoLabel" runat="server" Text="Contact No. :"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="contactGridView" runat="server" Width="268px" AutoGenerateColumns="False"
                            class="style1" PageSize="5" CssClass="GridviewBorder">
                            <Columns>
                                <asp:TemplateField HeaderText="Number">
                                    <ItemTemplate>
                                        <asp:TextBox ID="contactNoTextBox" runat="server" Width="260px" CssClass="ssTextBox"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                        </asp:GridView>
                    </td>
                    <td align="right" valign="top">
                        &nbsp;
                        <asp:Label ID="negativeListPersonIdLabel" runat="server" Text="ID :"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="idGridView" runat="server" Width="268px" AutoGenerateColumns="False"
                            class="style1" PageSize="5" CssClass="GridviewBorder">
                            <Columns>
                                <asp:TemplateField HeaderText="ID Type">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="idTypeDropDownList" runat="server" Width="140px">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ID No.">
                                    <ItemTemplate>
                                        <asp:TextBox ID="idNoTextBox" runat="server" Width="150px" CssClass="ssTextBox"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Label ID="negativeListPersonAcNoSCBLabel" runat="server" Text="A/C No. for SCB :"></asp:Label>
                    </td>
                    <td>
                        <asp:GridView ID="scbAccGridView" runat="server" Width="268px" AutoGenerateColumns="False"
                            PageSize="5" CssClass="GridviewBorder">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="scbPreTextBox" runat="server" Width="60px" CssClass="ssTextBox"
                                            MaxLength="2"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="A/C No.">
                                    <ItemTemplate>
                                        <asp:TextBox ID="scbMasterTextBox" runat="server" Width="148px" CssClass="ssTextBox"
                                            MaxLength="7"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="scbPostTextBox" runat="server" Width="60px" CssClass="ssTextBox"
                                            MaxLength="2"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="ssHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                        </asp:GridView>
                    </td>
                    <td align="right" valign="top">
                        <asp:Label ID="negativeListPersonTinNoLabel" runat="server" Text="TIN No. :"></asp:Label>
                        <br />
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="negativeListPersonAccountNameLabel" runat="server" Text="Product Name :"></asp:Label>
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="sourchLabel" runat="server" Text="Sourch Name :"></asp:Label>
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="negativeListPersonTinNoTextBox" runat="server" Width="50px"></asp:TextBox>
                        &nbsp;
                        <asp:TextBox ID="negativeListPersonTinNo2TextBox" runat="server" Width="50px"></asp:TextBox>
                        &nbsp;
                        <asp:TextBox ID="negativeListPersonTinNo3TextBox" runat="server" Width="50px"></asp:TextBox>
                        <br />
                        <br />
                        <br />
                        <asp:DropDownList ID="productDropDownList" runat="server" Width="200px">
                        </asp:DropDownList>
                        <br />
                        <br />
                        <asp:TextBox ID="sourchTextBox" runat="server" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonNameOfCompanyLabel" runat="server" Text="Name of Company :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonCompanyNameTextBox" runat="server" Width="220px"
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="bankBranchLabel" runat="server" Text="Bank/Branch :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="bankBranchTextBox" runat="server" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonOfficeAddressLabel" runat="server" Text="Office Address :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="negativeListPersonOfficeAddressTextBox" runat="server" TextMode="MultiLine"
                            Width="220px" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="remarksLabel" runat="server" Text="Remarks :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="remarksTextBox" runat="server" TextMode="MultiLine" Width="288px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonDeclineDateLabel" runat="server" Text="Decline Date :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="declineDateTextBox" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="negativeListPersonDeclineReasonLabel" runat="server" Text="Decline Reason :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="declineReasonTextBox" runat="server" TextMode="MultiLine" Width="289px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="negativeListPersonStatusLabel" runat="server" Text="Status :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="negativeListPersonStatusDropDownList" runat="server" Width="150px">
                            <asp:ListItem Value=""> </asp:ListItem>
                            <asp:ListItem Value="N">N</asp:ListItem>
                            <asp:ListItem Value="C">C</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td colspan="3">
                        <div class="FixedHeader" style="width: 625px; font-size: 12px; text-align: center;
                            font-weight: bold;">
                            Locator Remarks</div>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td colspan="3">
                        <div style="overflow-y: scroll; width: 625px; height: 130px; z-index: 2; border: solid 1px gray;"
                            visible="true">
                            <asp:GridView ID="remarksGridView" runat="server" Width="100px" AutoGenerateColumns="False"
                                AllowPaging="False" ShowHeader="False">
                                <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                                <Columns>
                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="IdCheckBox" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="20px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="580px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="remarksTextBox" runat="server" Text='<%# Eval("Name") %>' Width="580px"
                                                CssClass="ssTextBox"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle Width="580px"></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="FixedHeader" />
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td align="right" colspan="2">
                        <asp:Button ID="saveButtor" runat="server" Text="Decline" Width="100px" Height="27px"
                            OnClick="saveButtor_Click" />
                        <asp:Button ID="clearButtor" runat="server" Text="Clear" Width="100px" Height="27px"
                            OnClick="clearButtor_Click" />
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    </form>
</body>
</html>

