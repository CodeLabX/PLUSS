﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="azGridTableCss.ascx.cs" Inherits="PlussAndLoan.UI.UserControls.css.azGridTableCss" %>
<style type="text/css">
     table.az-gridTb th .part1 {
        background-color: #f49970;
    }

    table.az-gridTb th {
        border-width: 1px;
        padding: 3px;
        border-style: solid;
        border: #c7c7c7 1px solid;
        background-color: #9fbce3;
    }

    .az-gridTb {
        width: 100%;
        border-collapse: collapse;
    }

        .az-gridTb td {
            padding: 1px;
            border: #c7c7c7 1px solid;
        }

        .az-gridTb tr {
            background: #d3e3f9;
        }

            .az-gridTb tr:nth-child(odd) {
                background: #d3e3f9;
            }

            .az-gridTb tr:nth-child(even) {
                background: #e4eaf3;
            }
</style>