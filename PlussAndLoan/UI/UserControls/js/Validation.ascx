﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UserControls.js.UI_UserControls_js_Validation" Codebehind="Validation.ascx.cs" %>

<script type="text/javascript">
   
    $(function () {
        $('#ctl00_ContentPlaceHolder_LLIDTextBox').numberOnly();
        $('#ctl00_ContentPlaceHolder_appliedAmountTextBox,#ctl00_ContentPlaceHolder_declaredIncomeTextBox').numeric();
        $('#ctl00_ContentPlaceHolder_tinNoPreTextBox,#ctl00_ContentPlaceHolder_tinNoMasterTextBox,#ctl00_ContentPlaceHolder_tinNoPostTextBox').numberOnly();
        $('#ctl00_ContentPlaceHolder_officeAddressTextBox,#ctl00_ContentPlaceHolder_residentialAddressTextBox').maxLength(200);
    });
    $(function () {
        $('#ctl00_ContentPlaceHolder_scbAccGridView_ctl02_scbPreTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl02_scbMasterTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl02_scbPostTextBox').numberOnly();
        $('#ctl00_ContentPlaceHolder_scbAccGridView_ctl03_scbPreTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl03_scbMasterTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl03_scbPostTextBox').numberOnly();
        $('#ctl00_ContentPlaceHolder_scbAccGridView_ctl04_scbPreTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl04_scbMasterTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl04_scbPostTextBox').numberOnly();
        $('#ctl00_ContentPlaceHolder_scbAccGridView_ctl05_scbPreTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl05_scbMasterTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl05_scbPostTextBox').numberOnly();
        $('#ctl00_ContentPlaceHolder_scbAccGridView_ctl06_scbPreTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl06_scbMasterTextBox,#ctl00_ContentPlaceHolder_scbAccGridView_ctl06_scbPostTextBox').numberOnly();
    });
    $(function () {
        $('#ctl00_ContentPlaceHolder_creditCardNoGridView_ctl02_crdtCrdNoTextBox,#ctl00_ContentPlaceHolder_creditCardNoGridView_ctl03_crdtCrdNoTextBox,#ctl00_ContentPlaceHolder_creditCardNoGridView_ctl04_crdtCrdNoTextBox').numberOnly();
        $('#ctl00_ContentPlaceHolder_creditCardNoGridView_ctl05_crdtCrdNoTextBox,#ctl00_ContentPlaceHolder_creditCardNoGridView_ctl06_crdtCrdNoTextBox').numberOnly();
    });

    $(function () {
        $('#ctl00_ContentPlaceHolder_contactGridView_ctl02_contactNoTextBox,#ctl00_ContentPlaceHolder_contactGridView_ctl03_contactNoTextBox,#ctl00_ContentPlaceHolder_contactGridView_ctl04_contactNoTextBox').contactNumber();//numeric("+-");
        $('#ctl00_ContentPlaceHolder_contactGridView_ctl05_contactNoTextBox,#ctl00_ContentPlaceHolder_contactGridView_ctl06_contactNoTextBox').contactNumber();//numeric("+-");
    });
    $(function () {
        $('#ctl00_ContentPlaceHolder_workExperienceMonthTextBox').numeric();
        //            $('#ctl00_ContentPlaceHolder_otherBankGridView_ctl02_acNoTextBox,#ctl00_ContentPlaceHolder_otherBankGridView_ctl03_acNoTextBox,#ctl00_ContentPlaceHolder_otherBankGridView_ctl04_acNoTextBox').numberOnly();
        //            $('#ctl00_ContentPlaceHolder_otherBankGridView_ctl05_acNoTextBox,#ctl00_ContentPlaceHolder_otherBankGridView_ctl06_acNoTextBox').numberOnly();
    });
    // $(function(){           
    //            $('#ctl00_ContentPlaceHolder_idGridView_ctl02_idNoTextBox,#ctl00_ContentPlaceHolder_idGridView_ctl03_idNoTextBox,#ctl00_ContentPlaceHolder_idGridView_ctl04_idNoTextBox').numberOnly();
    //            $('#ctl00_ContentPlaceHolder_idGridView_ctl05_idNoTextBox,#ctl00_ContentPlaceHolder_idGridView_ctl06_idNoTextBox').numberOnly();
    //           });     
    $(function () {
        $('#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl02_jointAcNoSCBPreTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl02_jointAcNoSCBMasterTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl02_jointAcNoSCBPostTextBox1').numberOnly();
        $('#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl03_jointAcNoSCBPreTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl03_jointAcNoSCBMasterTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl03_jointAcNoSCBPostTextBox1').numberOnly();
        $('#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl04_jointAcNoSCBPreTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl04_jointAcNoSCBMasterTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl04_jointAcNoSCBPostTextBox1').numberOnly();
        $('#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl05_jointAcNoSCBPreTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl05_jointAcNoSCBMasterTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl05_jointAcNoSCBPostTextBox1').numberOnly();
        $('#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl06_jointAcNoSCBPreTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl06_jointAcNoSCBMasterTextBox1,#ctl00_ContentPlaceHolder_jointApplicantaccNoGridView1_ctl06_jointAcNoSCBPostTextBox1').numberOnly();
        $('#ctl00_ContentPlaceHolder_jTinPreTextBox1,#ctl00_ContentPlaceHolder_jTinMasterTextBox1,#ctl00_ContentPlaceHolder_jTinPostTextBox1').numberOnly();

        $('#ctl00_ContentPlaceHolder_jointCreditCardGridView1_ctl02_jointCardNoTextBox1,#ctl00_ContentPlaceHolder_jointCreditCardGridView1_ctl03_jointCardNoTextBox1,#ctl00_ContentPlaceHolder_jointCreditCardGridView1_ctl04_jointCardNoTextBox1').numberOnly();
        $('#ctl00_ContentPlaceHolder_jointCreditCardGridView1_ctl05_jointCardNoTextBox1,#ctl00_ContentPlaceHolder_jointCreditCardGridView1_ctl06_jointCardNoTextBox1').numberOnly();
        //jointcontactGridView1_ctl02_jointContactNoTextBox1
        $('#ctl00_ContentPlaceHolder_jointcontactGridView1_ctl02_jointContactNoTextBox1,#ctl00_ContentPlaceHolder_jointcontactGridView1_ctl03_jointContactNoTextBox1,#ctl00_ContentPlaceHolder_jointcontactGridView1_ctl04_jointContactNoTextBox1').contactNumber();
        $('#ctl00_ContentPlaceHolder_jointcontactGridView1_ctl05_jointContactNoTextBox1,#ctl00_ContentPlaceHolder_jointcontactGridView1_ctl06_jointContactNoTextBox1').contactNumber();

        //             $('#ctl00_ContentPlaceHolder_jointIdGridView1_ctl02_jointIdNoTextBox1,#ctl00_ContentPlaceHolder_jointIdGridView1_ctl03_jointIdNoTextBox1,#ctl00_ContentPlaceHolder_jointIdGridView1_ctl04_jointIdNoTextBox1').numeric();
        //             $('#ctl00_ContentPlaceHolder_jointIdGridView1_ctl05_jointIdNoTextBox1,#ctl00_ContentPlaceHolder_jointIdGridView1_ctl06_jointIdNoTextBox1').numeric();             
    });
    $(function () {
        $('#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl02_jAppSCBAccPreTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl02_jAppSCBAccMasterTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl02_jAppSCBAccPostTextBox2').numberOnly();
        $('#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl03_jAppSCBAccPreTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl03_jAppSCBAccMasterTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl03_jAppSCBAccPostTextBox2').numberOnly();
        $('#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl04_jAppSCBAccPreTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl04_jAppSCBAccMasterTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl04_jAppSCBAccPostTextBox2').numberOnly();
        $('#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl05_jAppSCBAccPreTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl05_jAppSCBAccMasterTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl05_jAppSCBAccPostTextBox2').numberOnly();
        $('#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl06_jAppSCBAccPreTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl06_jAppSCBAccMasterTextBox2,#ctl00_ContentPlaceHolder_jAppSCBAccGridView2_ctl06_jAppSCBAccPostTextBox2').numberOnly();
        $('#ctl00_ContentPlaceHolder_jAppTinPreTextBox2,#ctl00_ContentPlaceHolder_jAppTinMasterTextBox2,#ctl00_ContentPlaceHolder_jAppTinPostTextBox2').numberOnly();

        $('#ctl00_ContentPlaceHolder_jCrdtCrdGridView2_ctl02_jAppCrdtCrdTextBox2,#ctl00_ContentPlaceHolder_jCrdtCrdGridView2_ctl03_jAppCrdtCrdTextBox2,#ctl00_ContentPlaceHolder_jCrdtCrdGridView2_ctl04_jAppCrdtCrdTextBox2').numberOnly();
        $('#ctl00_ContentPlaceHolder_jCrdtCrdGridView2_ctl05_jAppCrdtCrdTextBox2,#ctl00_ContentPlaceHolder_jCrdtCrdGridView2_ctl06_jAppCrdtCrdTextBox2').numberOnly();

        $('#ctl00_ContentPlaceHolder_jAppContactGridView2_ctl02_jAppContactTextBox2,#ctl00_ContentPlaceHolder_jAppContactGridView2_ctl03_jAppContactTextBox2,#ctl00_ContentPlaceHolder_jAppContactGridView2_ctl04_jAppContactTextBox2').contactNumber();
        $('#ctl00_ContentPlaceHolder_jAppContactGridView2_ctl05_jAppContactTextBox2,#ctl00_ContentPlaceHolder_jAppContactGridView2_ctl06_jAppContactTextBox2').contactNumber();

        //             $('#ctl00_ContentPlaceHolder_jAppIdGridView2_ctl02_jAppIdNoTextBox2,#ctl00_ContentPlaceHolder_jAppIdGridView2_ctl03_jAppIdNoTextBox2,#ctl00_ContentPlaceHolder_jAppIdGridView2_ctl04_jAppIdNoTextBox2').numeric();
        //             $('#ctl00_ContentPlaceHolder_jAppIdGridView2_ctl05_jAppIdNoTextBox2,#ctl00_ContentPlaceHolder_jAppIdGridView2_ctl06_jAppIdNoTextBox2').numeric();             
    });

    $(function () {
        for (var i = 2 ; i <= 6 ; i++) {
            $('#ctl00_ContentPlaceHolder_jAppSCBAccGridView3_ctl0' + i + '_jAppSCBAccPreTextBox3,#ctl00_ContentPlaceHolder_jAppSCBAccGridView3_ctl0' + i + '_jAppSCBAccMasterTextBox3,#ctl00_ContentPlaceHolder_jAppSCBAccGridView3_ctl0' + i + '_jAppSCBAccPostTextBox3').numberOnly();

            $('#ctl00_ContentPlaceHolder_jAppCrdtCrdGridView3_ctl0' + i + '_jAppCrdtCrdTextBox3').numberOnly();
            $('#ctl00_ContentPlaceHolder_jAppCrdtCrdGridView4_ctl0' + i + '_jAppCrdtCrdTextBox4').numberOnly();
        }
        for (var i = 2 ; i <= 5 ; i++) {
            $('#ctl00_ContentPlaceHolder_jAppContactGridView3_ctl0' + i + '_jAppContactTextBox3').contactNumber();
            $('#ctl00_ContentPlaceHolder_jAppContactGridView4_ctl0' + i + '_jAppContactTextBox4').contactNumber();
        }
        $('#ctl00_ContentPlaceHolder_jAppTinPreTextBox3,#ctl00_ContentPlaceHolder_jAppTinMasterTextBox3,#ctl00_ContentPlaceHolder_jAppTinPostTextBox3').numberOnly();
        $('#ctl00_ContentPlaceHolder_jAppTinPreTextBox4, #ctl00_ContentPlaceHolder_jAppTinMasterTextBox4,#ctl00_ContentPlaceHolder_jAppTinPostTextBox4').numberOnly();
    });
</script>