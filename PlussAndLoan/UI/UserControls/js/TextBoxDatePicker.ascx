﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UserControls.js.UI_UserControls_js_TextBoxDatePicker" Codebehind="TextBoxDatePicker.ascx.cs" %>

<script type="text/javascript">
    //Main Loan Applicant
    $(function () {
        $('#ctl00_ContentPlaceHolder_ciForDate,#ctl00_ContentPlaceHolder_ciToDate,#ctl00_ContentPlaceHolder_misToDate,#ctl00_ContentPlaceHolder_misToDate,#ctl00_ContentPlaceHolder_lamsToDate,#ctl00_ContentPlaceHolder_lamsToDate,#ctl00_ContentPlaceHolder_loan_date,#ctl00_ContentPlaceHolder_dateTextBox,#ctl00_ContentPlaceHolder_dateTextBox2,#ctl00_ContentPlaceHolder_txt_startDate,#ctl00_ContentPlaceHolder_txt_toDate').datepicker({
            showOn: 'button', buttonImageOnly: false,
            changeMonth: this, changeYear: true, dateFormat: 'yy-mm-dd', yearRange: '1960:2030'
        });
    });

    function ValidteLoanApplicationForm() {
        var errCount = 0;
        if (!isNotSelect("ctl00_ContentPlaceHolder_productDropDownList", "Please select product")) {
            errCount++;
        }
        else if (!isNotSelect("ctl00_ContentPlaceHolder_sourceDropDownList", "Please select source")) {
            errCount++;
        }
        else if (!isEmpty("ctl00_ContentPlaceHolder_customerNameTextBox", "Please enter customer name")) {
            errCount++;
        }
        else if (!isEmpty("ctl00_ContentPlaceHolder_appliedAmountTextBox", "Please enter appled amount")) {
            errCount++;
        }
        else if (!MasterAccountCheck('ctl00_ContentPlaceHolder_scbAccGridView_ctl02_scbPreTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl02_scbMasterTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl02_scbPostTextBox')) {
            errCount++;
        }
        else if (!MasterAccountCheck('ctl00_ContentPlaceHolder_scbAccGridView_ctl03_scbPreTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl03_scbMasterTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl03_scbPostTextBox')) {
            errCount++;
        }
        else if (!MasterAccountCheck('ctl00_ContentPlaceHolder_scbAccGridView_ctl04_scbPreTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl04_scbMasterTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl04_scbPostTextBox')) {
            errCount++;
        }
        else if (!MasterAccountCheck('ctl00_ContentPlaceHolder_scbAccGridView_ctl05_scbPreTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl05_scbMasterTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl05_scbPostTextBox')) {
            errCount++;
        }
        else if (!MasterAccountCheck('ctl00_ContentPlaceHolder_scbAccGridView_ctl06_scbPreTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl06_scbMasterTextBox', 'ctl00_ContentPlaceHolder_scbAccGridView_ctl06_scbPostTextBox')) {
            errCount++;
        }
        else if (!isEmpty("ctl00_ContentPlaceHolder_dobTextBox", "Please enter date of birth")) {
            errCount++;
        }
        else if (!isEmpty("ctl00_ContentPlaceHolder_declaredIncomeTextBox", "Please enter declared income")) {
            errCount++;
        }
        else if (!isNotSelect("ctl00_ContentPlaceHolder_educationalDropDownList", "Please select educational level")) {
            errCount++;
        }
        else if (!IsProfessionSelect("ctl00_ContentPlaceHolder_professionDropdownList", "Please enter company name.")) {
            errCount++;
        }
        else if (!isEmpty("ctl00_ContentPlaceHolder_workExperienceMonthTextBox", "Please enter working experience")) {
            errCount++;
        }
        //else if (!isMarriedSelect("ctl00_ContentPlaceHolder_spousesOccupationDropdownList", "Please select spouses occupation")) {
        //    errCount++;
        //}

        if (errCount > 0)
            return false;
        else
            return true;
    }

    function isEmpty(targetId, msg) {
        obj = document.getElementById(targetId);
        if (obj.value == '' || obj.length == 0) {
            //document.getElementById('ctl00_ContentPlaceHolder_remarksLabel').innerHTML = msg;	
            alert(msg);
            obj.focus();
            return false;
        }
        else {
            document.getElementById("ctl00_ContentPlaceHolder_remarksLabel").innerHTML = "";
            return true;
        }
    }

    function isNotSelect(targetId, msg) {
        obj = document.getElementById(targetId);
        if (obj.selectedIndex == 0) {
            //document.getElementById("remarksLabel").innerHTML = msg;		        
            alert(msg);
            obj.focus();
            return false;
        }
        else {
            document.getElementById("ctl00_ContentPlaceHolder_remarksLabel").innerHTML = "";
        }
        return true;
    }
    function isMarriedSelect(targetId, msg) {
        obj = document.getElementById('ctl00_ContentPlaceHolder_maritalStatusDropDownList');
        obj2 = document.getElementById(targetId);

        if (obj.selectedIndex == 1 && obj2.selectedIndex == 0) {
            document.getElementById("remarksLabel").innerHTML = "";
            return true;
        }
        else if (obj.selectedIndex == 1 && obj2.selectedIndex != 0) {
            alert('You could not select spouse occupation');
            obj2.focus();
            return false;
        }
        else if (obj.selectedIndex == 0 && obj2.selectedIndex == 0) {
            alert(msg);
            obj2.focus();
            return false;
        }
        else {
            document.getElementById("remarksLabel").innerHTML = "";
            return true;
        }
    }

    function IsProfessionSelect(control, msg) {
        var ctrlValue = document.getElementById(control).value;
        var value2 = document.getElementById('ctl00_ContentPlaceHolder_companyNameTextBox').value;
        //alert(ctrlValue);
        if (ctrlValue == 'Salaried' && value2.length == 0) {
            alert(msg);
            document.getElementById('ctl00_ContentPlaceHolder_companyNameTextBox').focus();
            return false;
        }
        else {
            return true;
        }
    }

    function CursorAutoPosition1(val) {
        var keycode = window.event.keyCode;
        var obj1 = document.getElementById('ctl00_ContentPlaceHolder_tinNoPreTextBox').value;
        var obj2 = document.getElementById('ctl00_ContentPlaceHolder_tinNoMasterTextBox').value;
        var obj3 = document.getElementById('ctl00_ContentPlaceHolder_tinNoPostTextBox').value;
        if (obj1.length == 3 && obj2.length < 3 && val == 1) {
            document.getElementById('ctl00_ContentPlaceHolder_tinNoMasterTextBox').focus();
        }
        if (obj2.length == 3 && (obj3.length < 4) && val == 2) {
            document.getElementById('ctl00_ContentPlaceHolder_tinNoPostTextBox').focus();
        }

        if (val == 1 && (keycode == 39)) {
            document.getElementById('ctl00_ContentPlaceHolder_tinNoMasterTextBox').select();
        }
        if (val == 2 && keycode == 39) {
            document.getElementById('ctl00_ContentPlaceHolder_tinNoPostTextBox').select();
        }
        if (val == 3 && (keycode == 37)) {
            document.getElementById('ctl00_ContentPlaceHolder_tinNoMasterTextBox').select();
        }
        if (val == 2 && (keycode == 37)) {
            document.getElementById('ctl00_ContentPlaceHolder_tinNoPreTextBox').select();
        }
    }

    function CursorAutoPosition4SCBAcc(contrl1, contrl2, contrl3, val) {
        var keycode = window.event.keyCode;
        var obj1 = document.getElementById(contrl1).value;
        var obj2 = document.getElementById(contrl2).value;
        var obj3 = document.getElementById(contrl3).value;

        if (obj1.length == 2 && obj2.length < 7 && val == 1) {
            document.getElementById(contrl2).focus();
        }

        if (obj2.length == 7 && (obj3.length < 2) && val == 2) {
            document.getElementById(contrl3).focus();
        }
        if (val == 1 && (keycode == 39)) {
            document.getElementById(contrl2).select();
        }
        if (val == 2 && keycode == 39) {
            document.getElementById(contrl3).select();
        }
        if (val == 3 && (keycode == 37)) {
            document.getElementById(contrl2).select();
        }
        if (val == 2 && (keycode == 37)) {
            document.getElementById(contrl1).select();
        }
    }

    function FormatedDate(control) {
        //var obj1 = document.getElementById(control).value;
        //if (obj1 == null || obj1 == '') {
        //    return;
        //}
        //var temp1;
        //var temp2;
        //var temp3;
        //var result;
        //if (obj1.length == 0) {
        //    alert('Please,enter a date');
        //    document.getElementById(control).focus();
        //}
        //else if (obj1.length == 10) {
        //    var value10 = obj1.replace('-', '');

        //    temp1 = value10.substr(0, 2);
        //    temp2 = value10.substr(2, 2);
        //    temp3 = value10.substr(5, 4);
        //    if (parseInt(temp3) < 1900 || parseInt(temp3) > 2099) {
        //        alert('Please,enter year between (1900 to 2099)');
        //        document.getElementById(control).focus();
        //    }
        //    else {
        //        if (parseInt(temp1) > 31 && !isleap(temp3) && parseInt(temp2) != 2) {
        //            alert('Please,enter valid day');
        //            document.getElementById(control).focus();
        //        }
        //        else if (parseInt(temp1) > 28 && !isleap(temp3) && parseInt(temp2) == 2) {
        //            alert('Please,enter valid day.');
        //            document.getElementById(control).focus();
        //        }
        //        else if (parseInt(temp1) > 29 && isleap(temp3) && parseInt(temp2) == 2) {
        //            alert('Please,enter valid day.');
        //            document.getElementById(control).focus();
        //        }
        //        if (parseInt(temp2) > 12) {
        //            alert('Please,enter valid month');
        //            document.getElementById(control).focus();
        //        }

        //    }
        //    return false;
        //}
        //else {
        //    alert('Please,enter correct date format');
        //    document.getElementById(control).focus();
        //    return false;
        //}

    }

    function CreditCardValidation(control) {
        //var objValue = document.getElementById(control).value;
        //if (objValue.length != 0 && objValue.length >= 16) {
        //    alert('Credit card account no. must be less than 16 digit');
        //    document.getElementById(control).focus();
        //}
        //else if (objValue.length == 16) {
        //    if (objValue.indexOf('0') == 0) {
        //        alert('Credit card no. not start with 0');
        //        document.getElementById(control).focus();
        //    }
        //}
    }

    function IdDateFormate(control) {
        var obj1 = document.getElementById(control).value;
        var temp1;
        var temp2;
        var temp3;
        var result;
        if (obj1.length == 10 || obj1.length == 0) {
            var value10 = obj1.replace('-', '');

            temp1 = value10.substr(0, 2);
            temp2 = value10.substr(2, 2);
            temp3 = value10.substr(5, 4);

            if (parseInt(temp1) > 31 && !isleap(temp3) && parseInt(temp2) != 2) {
                alert('Please,enter valid day');
                document.getElementById(control).focus();
            }
            else if (parseInt(temp1) > 28 && !isleap(temp3) && parseInt(temp2) == 2) {
                alert('Please,enter valid day.');
                document.getElementById(control).focus();
            }
            else if (parseInt(temp1) > 29 && isleap(temp3) && parseInt(temp2) == 2) {
                alert('Please,enter valid day.');
                document.getElementById(control).focus();
            }
            if (parseInt(temp3) < 1900 || parseInt(temp3) > 2099) {
                alert('Please,enter year between (1900 to 2099)');
                document.getElementById(control).focus();
            }
            if (parseInt(temp2) > 12) {
                alert('Please,enter valid month');
                document.getElementById(control).focus();
            }
        }
        else {
            alert('Please,enter correct date format');
            document.getElementById(control).focus();
        }
    }

    function CursorAutoPosition(control1, control2, control3, val) {
        var keycode = window.event.keyCode;
        var obj1 = document.getElementById(control1).value;
        var obj2 = document.getElementById(control2).value;
        var obj3 = document.getElementById(control3).value;
        if (obj1.length == 3 && obj2.length < 3 && val == 1) {
            document.getElementById(control2).focus();
            //obj2.focus();
        }
        if (obj2.length == 3 && (obj3.length < 4) && val == 2) {
            document.getElementById(control3).focus();
            //obj3.focus();
        }
        if (val == 1 && (keycode == 39)) {
            document.getElementById(control2).select();
        }
        if (val == 2 && keycode == 39) {
            document.getElementById(control3).select();
        }
        if (val == 3 && (keycode == 37)) {
            document.getElementById(control2).select();
        }
        if (val == 2 && (keycode == 37)) {
            document.getElementById(control1).select();
        }
    }



    function isleap(val) {
        var yr = val;
        if ((parseInt(yr) % 4) == 0) {
            if (parseInt(yr) % 100 == 0) {
                if (parseInt(yr) % 400 != 0) {
                    return false;
                }
                if (parseInt(yr) % 400 == 0) {
                    return true;
                }
            }
            if (parseInt(yr) % 100 != 0) {
                return true;
            }
        }
        if ((parseInt(yr) % 4) != 0) {
            return false;
        }
    }



    function AutoFormatedDate(control) {
        var obj1 = document.getElementById(control).value;
        var temp1;
        var temp2;
        var temp3;
        var result;
        if (obj1.length == 0) {
            alert('Please,enter a date');
            document.getElementById(control).focus();
        }
        else //if(obj1.length == 10)
        {

            if (obj1.length == 2) {
                document.getElementById(control).value = obj1 + '-';
                return;

            }
            else if (obj1.length == 5) {
                document.getElementById(control).value = obj1 + '-';
                return;
            }
            else if (obj1.length == 10) {
                var value10 = obj1.replace('-', '');
                temp1 = value10.substr(0, 2);
                temp2 = value10.substr(2, 2);
                temp3 = value10.substr(5, 4);
                if (parseInt(temp3) < 1900 || parseInt(temp3) > 2099) {
                    alert('Please,enter year between (1900 to 2099)');
                    document.getElementById(control).focus();
                }
                else {
                    if (parseInt(temp1) > 31 && !isleap(temp3) && parseInt(temp2) != 2) {
                        alert('Please,enter valid day');
                        document.getElementById(control).focus();
                    }
                    else if (parseInt(temp1) > 28 && !isleap(temp3) && parseInt(temp2) == 2) {
                        alert('Please,enter valid day.');
                        document.getElementById(control).focus();
                    }
                    else if (parseInt(temp1) > 29 && isleap(temp3) && parseInt(temp2) == 2) {
                        alert('Please,enter valid day.');
                        document.getElementById(control).focus();
                    }
                    if (parseInt(temp2) > 12) {
                        alert('Please,enter valid month');
                        document.getElementById(control).focus();
                    }

                }
            }

        }
    }

    function AutoFormatDate(control) {
        var obj1 = document.getElementById(control).value;

        if (obj1.length == 2) {
            document.getElementById(control).value = obj1 + '-';
            return;

        }
        else if (obj1.length == 5) {
            document.getElementById(control).value = obj1 + '-';
            return;
        }
    }

    function MasterAccountCheck(preControl, masterControl, postControl) {
        var preAcc = document.getElementById(preControl).value;
        var mAcc = document.getElementById(masterControl).value;
        var postAcc = document.getElementById(postControl).value;
        var totalAcc = preAcc + mAcc + postAcc;
        var count = 0;
        //alert(totalAcc.length);
        if (totalAcc.length == 11 || totalAcc.length == 0) {
            document.getElementById(preControl).style.color = 'black';
            document.getElementById(masterControl).style.color = 'black';
            document.getElementById(postControl).style.color = 'black';
            return true;
        }
        else {
            alert('Account should be 11 digit');
            document.getElementById(preControl).style.color = 'red';
            document.getElementById(masterControl).style.color = 'red';
            document.getElementById(postControl).style.color = 'red';
            //document.getElementById(masterControl).focus();
            return false;
        }


    }
</script>