﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UserControls.js.UI_UserControls_js_PopupWindowsJs" Codebehind="PopupWindowsJs.ascx.cs" %>
<script>
    function ShowPopUpWindows(dlg, pg) {
        window.onerror = ErrorHandle;
        document.getElementById("blockUI").style.height = pg.scrollHeight;
        document.getElementById("blockUI").style.width = pg.scrollWidth;
        document.getElementById("blockUI").style.display = "block";

        document.getElementById(dlg).style.zIndex = 50002;
        document.getElementById(dlg).style.left = pg.scrollLeft + 600;
        document.getElementById(dlg).style.display = "block";
        document.getElementById(dlg).style.left = (pg.clientWidth - document.getElementById(dlg).clientWidth) / 2;
        document.getElementById(dlg).style.top = pg.scrollTop + 350;
        document.getElementById(dlg).style.top = (pg.clientHeight - document.getElementById(dlg).clientHeight) / 2;

    }
    function closeDialog(dlg) {
        window.onerror = ErrorHandle;
        document.getElementById(dlg).style.display = "none";
        document.getElementById("blockUI").style.display = "none";
    }
    function ErrorHandle(err, url, line) {
        alert('Oops, something went wrong.\n' +
             'Please do not wery go ahade \n' +
             'Error text: ' + err + '\n' +
             'Location  : ' + url + '\n' +
             'Line no   : ' + line + '\n');
        return true; // let the browser handle the error */
    }
</script>