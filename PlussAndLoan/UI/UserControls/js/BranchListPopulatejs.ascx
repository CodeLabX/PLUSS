﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UserControls.js.UI_UserControls_js_BranchListPopulatejs" Codebehind="BranchListPopulatejs.ascx.cs" %>
<script type="text/javascript">

    var request;
    var response;
    var branchDropDown;

    function populatebranchDropDown(valueObj) {
        var bankDropDownList;
        var bankObj = valueObj.id;
        var bankObjId = bankObj.substr(47, 1);

        if (bankObjId > 1 && bankObjId < 7) {
            branchDropDown = document.getElementById("ctl00_ContentPlaceHolder_otherBankGridView_ctl0" + bankObjId + "_branchDropDownList");
        }

        if (bankObj.value != '') {
            //return SendRequest(bankDropDownList.options[bankDropDownList.selectedIndex].value);					
            return SendRequest(valueObj.value);
        }
        else {
            clearSelect(branchDropDown);//Clear the branchDropDown dropdown
        }
    }
    function InitializeRequest() {
        try {
            request = new ActiveXObject("Microsoft.XMLHTTP");//Try creating an XMLHTTP Object
        }
        catch (Ex) {
            try {
                request = new ActiveXObject("Microsoft.XMLHTTP");//First failure, try again creating an XMLHTTP Object
            }
            catch (Ex) {
                request = null;//Else assign null to request
            }
        }

        if (!request && typeof XMLHttpRequest != 'undefined') {
            request = new XMLHttpRequest();
        }
    }

    function SendRequest(ID) {
        InitializeRequest();//Call InitializeRequest to set request object
        var url = "BranchListServerSideCode.aspx?BankID=" + ID;//Create the url to send the request to
        request.onreadystatechange = ProcessRequest;//Delegate ProcessRequest to onreadystatechange property so it gets called for every change in readyState value
        request.open("GET", url, true);//Open a GET request to the URL
        request.send(null);//Send the request with a null body.
    }

    function ProcessRequest() {
        if (request.readyState == 4)//If the readyState is in the "Ready" state
        {
            if (request.status == 200)//If the returned status code was 200. Everything was OK.
            {
                if (request.responseText != "")//If responseText is not blank
                {
                    populateList(request.responseText);//Call the populateList fucntion
                }
                else {
                    //status.innerText = "None Found";//Set the status to "None Found"
                    clearSelect(branchDropDown);//Call clearSelect function
                }
            }
        }
        return true;//return
    }

    function populateList(response) {
        var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");//Create the XMLDOM object
        xmlDoc.async = false;
        xmlDoc.loadXML(response);//Load the responseText into the XMLDOM document

        var opt;
        var TerritoriesElem = xmlDoc.getElementsByTagName("BankBranches");//Create the EmployeeTerritories element
        var branchDropDownElem = TerritoriesElem[0].getElementsByTagName("Branch");//Create the TERRITORIES element

        clearSelect(branchDropDown);//Clear the dropdown before filling it with new values
        if (TerritoriesElem.length > 0)//If there are one or more TERRITORIES nodes
        {
            for (var i = 0; i < branchDropDownElem.length; i++)//Loop through the XML TERRITORIES nodes
            {
                var textNode = document.createTextNode(branchDropDownElem[i].getAttribute("Name"));//Create a TextNode
                appendToSelect(branchDropDown, branchDropDownElem[i].getAttribute("Id"), textNode);//Call appendToSelect to append the text elements to the Territory dropdown
            }
        }
    }

    function appendToSelect(select, value, content) {
        var opt;
        opt = document.createElement("option");//Create an Element of type option
        opt.value = value;//Set the option's value
        opt.appendChild(content);//Attach the text content to the option
        select.appendChild(opt);//Append the option to the referenced [Territory] select box
    }

    function clearSelect(select) {
        select.options.length = 1;//Set the select box's length to 1 so only "--Select--" is availale in the selection on calling this function.
        //You may want to write your own clearSelect logic
    }

    function JS_FunctionValueAssign(valueObj) {
        var branchIdHidden;
        var bankObj = valueObj.id;
        var bankObjId = bankObj.substr(47, 1);

        if (valueObj.value > 0) {
            branchIdHidden = document.getElementById("ctl00_ContentPlaceHolder_otherBankGridView_ctl0" + bankObjId + "_branchId");
            branchIdHidden.value = "";
            branchIdHidden.value = valueObj.value;
        }
        else {
            branchIdHidden.value = "";
        }
    }
</script>
