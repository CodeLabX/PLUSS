﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoSales.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_Default" Title="Untitled Page" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 60px;
        }
        .style3
        {
        }
        .style4
        {
            width: 241px;
        }
        .style5
        {
            width: 179px;
        }
        .style7
        {
            font-weight: bold;
            font-size: large;
            height: 13px;
        }
        .style8
        {
            width: 100%;
        }
        .style11
        {
            text-align: center;
        }
        table.reference {
    background-color: #FFFFFF;
    border: 1px solid #C3C3C3;
    border-collapse: collapse;
    width: 100%;
}
table.reference th {
    background-color: #E5EECC;
    border: 1px solid #C3C3C3;
    padding: 0px;
    vertical-align: top;
}
table.reference td {
    border: 1px solid #C3C3C3;
    padding-left: 5px;
    vertical-align: top;
    font-size:12px;
} 
        
        .style21
        {
            width: 12px;
        }
                
        .style32
        {
            font-weight: bold;
        }
                
        .style35
        {
        }
        .style37
        {
            width: 4px;
        }
        .style38
        {
            width: 50px;
        }
        .style40
        {
            text-align: center;
            font-weight: bold;
        }
        
        .style41
        {
            width: 511px;
        }
        .style42
        {
            width: 119px;
        }
        .style43
        {
            width: 119px;
            font-weight: bold;
        }
        .style44
        {
            width: 50px;
            font-weight: bold;
        }
        .style45
        {
            text-align: center;
            font-weight: bold;
            font-size: medium;
            text-decoration: underline;
            font-family: Arial;
        }
        
        p.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	}
                
        .style68
        {
            width: 100%;
            border-color: #808080;
        }
        .style69
        {
            font-size: x-small;
            font-weight: normal;
        }
        
        .style71
        {
            height: 33px;
            font-weight: bold;
        }
        
        .style73
        {
            height: 7px;
        }
        .style74
        {
            width: 15px;
        }
        
        .style75
        {
            width: 12px;
            height: 25px;
        }
        .style76
        {
            font-weight: bold;
            font-size: 13px;
            height: 25px;
            }
        .style77
        {
            height: 25px;
        }
        .style78
        {
        }
        .style79
        {
            width: 81px;
        }
        .style80
        {
            width: 116px;
        }
        
        .style82
        {
            width: 274px;
        }
        .style83
        {
            width: 120px;
        }
        
        .style84
        {
        }
        
        .style85
        {
            width: 12px;
            height: 6px;
        }
        .style86
        {
            font-weight: bold;
            font-size: 13px;
            height: 6px;
        }
        .style87
        {
            height: 7px;
            text-align: justify;
            }
        .style88
        {
            width: 114px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <div id="divPrintBALetter" class="widthSize80_per paddingAllSide10_per">
    <div id="divPrintBALetterBody" class="widthSize100_per">
        
        <table class="style1">
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
            <td class="style3" rowspan="3">
                <table class="style1">
                    <tr>
                        <td class="style7 rightAlign">
                            Standard Chartered</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="floatRight" Font-Bold="True" 
                                Font-Size="Larger" ForeColor="#666666" style="font-weight: 700" Text="Saadiq"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="5">
                <asp:Image ID="Image1" runat="server" Height="66px" 
                    ImageUrl="~/Images/scb_logo.jpg" Width="90%"  CssClass="floatLeft"/>
            </td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
        </tr>

    </table> 
    
       <table class="style8">
            <tr>
                <td class="style35" colspan="3">
                    <asp:Label ID="Label2" runat="server" 
                        
                        Text="................................................................................................."></asp:Label>
                </td>
                <td class="style42">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style44">
                    Branch</td>
                <td class="style37">
                    :</td>
                <td class="style41">
                    <asp:Label ID="lblBranch" runat="server" 
                        Text="..............................................................................."></asp:Label>
                </td>
                <td class="style43">
                    A/C No</td>
                <td class="style40">
                    :</td>
                <td class="rightAlign">
                    <asp:Label ID="lblACNo" runat="server" 
                        Text="............................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style44">
                    Date</td>
                <td class="style37">
                    :</td>
                <td class="style41">
                    <asp:Label ID="lblDate" runat="server" 
                        Text="..............................................................................."></asp:Label>
                                        </td>
                <td class="style42">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style38">
                    &nbsp;</td>
                <td class="style37">
                    &nbsp;</td>
                <td class="style41">
                    &nbsp;</td>
                <td class="style42">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br/> 
          <table class="style8">
              <tr>
                  <td class="style45">
                      BANKING ARRANGEMENT LETTER</td>
              </tr>
              <tr>
                  <td>
                      Dear Sir/Madam,<br />
                      We are pleased to advise that the following facillity has been granted to 
                      you on the basis of your 
                      application dated&nbsp;&nbsp;
                      <asp:Label ID="lblApplicationDate" runat="server" 
                          
                          Text="........................................................................................................................................."></asp:Label>
                  </td>
              </tr>
        </table>
        <br/>
          <table cellpadding="4" class=" reference style68" style="padding-left: 5px">
                                        <tr bgcolor="Gray">
                                            <td bgcolor="Gray" colspan="4">
                                                <b>FACILITY DETAILS</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <b>Auto loan </b><input id="Checkbox1" type="checkbox" class="style32" /><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                Used car finance </b>
                      <input id="Checkbox2" type="checkbox" class="style32" /></td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      LOAN AMOUNT</td>
                                            <td class="style32">
                      <asp:Label ID="Label12" runat="server" Text="BDT"></asp:Label>
                                            </td>
                                            <td class="style32">
                      INTEREST RATE</td>
                                            <td>
                      <asp:Label ID="lblInterestRate" runat="server" Text=".........................................................." 
                          CssClass="floatLeft" Font-Bold="True"></asp:Label>
                      <asp:Label ID="Label17" runat="server" Text="% p.a" CssClass="floatRight" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      EMI AMOUNT</td>
                                            <td class="style32">
                      <asp:Label ID="Label38" runat="server" Text="BDT"></asp:Label>
                                            </td>
                                            <td class="style32">
                                                NO. OF INSTALLMENTS</td>
                                            <td class="style32">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      TENOR</td>
                                            <td class="style32">
                      <asp:Label ID="lblTenor" runat="server" 
                          Text="............................................."></asp:Label>
                                            </td>
                                            <td class="style32">
                                                EXPIRY DATE</td>
                                            <td class="style32">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      BRAND</td>
                                            <td class="style32">
                      <asp:Label ID="lblTenor0" runat="server" 
                          Text="............................................."></asp:Label>
                                            </td>
                                            <td class="style32">
                                                MODEL</td>
                                            <td class="style32">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      PROCESSING FEE</td>
                                            <td class="style32">
                      <asp:Label ID="Label39" runat="server" Text="BDT"></asp:Label>
                                            </td>
                                            <td class="style69" colspan="2">
                      Note: Last EMI amount may vary from the regular EMI amount</td>
                                        </tr>
                                    </table>
          <br />
                                                <table cellpadding="4" class="style8">
                                                    <tr>
                                                        <td class="style71" colspan="2" 
                                                            style="font-size: 14px; font-family: AriaL;">
                    DOCUMENTATION:</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            DP Note &amp; Letter of Continuation for BDT&nbsp;&nbsp;
                      <asp:Label ID="lblDPNoteLetter" runat="server" 
                          Text="............................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Letter of Lien and Set-off over Deposit A/C No
                      <asp:Label ID="lblDPNoteLetter0" runat="server" 
                          Text="............................................."></asp:Label>
                                            &nbsp;for BDT
                      <asp:Label ID="lblDPNoteLetter1" runat="server" 
                          Text="............................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Post-dated Cheques from
                      <asp:Label ID="lblDPNoteLetter2" runat="server" 
                          Text="................................................................................"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Undated Cheque
                      <asp:Label ID="lblDPNoteLetter3" runat="server" 
                          Text="................................................................................................"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Letter of Hypothecation&nbsp;&nbsp;
                      <asp:Label ID="lblDPNoteLetter4" runat="server" 
                          Text="...................................................................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Letter of Authority to Sell Hypothecate Prorerty(Notarized)
                      <asp:Label ID="lblDPNoteLetter5" runat="server" 
                          Text="........................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Letter of Authorisation for Encashment of Securities&nbsp;
                      <asp:Label ID="lblDPNoteLetter6" runat="server" 
                          Text="................................"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Ownership Transfer From
                      <asp:Label ID="lblDPNoteLetter7" runat="server" 
                          Text="..................................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Security(ies):&nbsp;
                      <asp:Label ID="lblDPNoteLetter8" runat="server" 
                          Text="........................................................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Other(s)&nbsp;
                      <asp:Label ID="lblDPNoteLetter9" runat="server" 
                          Text="................................................................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style73">
                                                            </td>
                                                        <td class="style87">
                                                            Your borrowing is to be secured by the following securities in the manner 
                                                            prescribed hereinafter together with any other security which now or hereinafter 
                                                            may be held with the Bank, all of which securities shall be available to it as 
                                                            security for all liabilities of the borrower(s) at any time. All clauses, terms, conditions 
                                                            coinvenience etc expressed in those securities will apply unless expressly 
                                                            modefied or varied therein.</td>
                                                    </tr>
                                                </table>
                                <table cellpadding="4" class="style8">
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style74">
                                            1.</td>
                                        <td>
                                            DP note convering the value of entire loan</td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style74">
                                            2.</td>
                                        <td>
                                            Securities mentioned in the Memorandum of Deposit
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style74">
                                            3.</td>
                                        <td>
                                            Hypothecation over Vehicle</td>
                                    </tr>
        </table>
                                <br />
                                <table cellpadding="4" class="style8">
                                    <tr>
                                        <td class="style85">
                                            </td>
                                        <td class="style86" colspan="8" 
                                            style="border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: #333333">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style75">
                                            </td>
                                        <td class="style76" colspan="4">
                                            Debit Authority</td>
                                        <td class="style77" colspan="4">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style78" colspan="4">
                                            In consideration fo granting me/us Auto Loan Facility, Loan A/C no:</td>
                                        <td colspan="4">
                      <asp:Label ID="lblDPNoteLetter10" runat="server" 
                          Text=".................................................................."></asp:Label>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style78" colspan="4">
                                            I/we hareby authorise the Bank to debit my/our account no</td>
                                        <td colspan="4">
                      <asp:Label ID="lblDPNoteLetter11" runat="server" 
                          Text=".................................................................."></asp:Label>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td colspan="8">
                                            for an amount not exceeding BDT&nbsp;&nbsp;<asp:Label ID="Label40" runat="server" 
                        Text=".................................."></asp:Label>&nbsp;&nbsp;
                    <asp:Label ID="Label41" runat="server" 
                        Text="............"></asp:Label>
                &nbsp; in&nbsp;
                    <asp:Label ID="Label42" runat="server" 
                        Text="............"></asp:Label>
                &nbsp; consecutive monthly installments as on&nbsp;
                    <asp:Label ID="Label43" runat="server" 
                        Text="............"></asp:Label>
                &nbsp;day or following working day of each month commencing from the month&nbsp; of
                      <asp:Label ID="lblDPNoteLetter12" runat="server" 
                          Text="..........................................."></asp:Label>
                                            &nbsp;,
                      <asp:Label ID="lblDPNoteLetter13" runat="server" 
                          Text="..........................................."></asp:Label>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style78">
                                            &nbsp;</td>
                                        <td class="style79">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td colspan="2">
                                            month</td>
                                        <td class="style80">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; year</td>
                                        <td class="style78">
                                            &nbsp;</td>
                                        <td class="style78">
                                            &nbsp;</td>
                                    </tr>
                                    </table>
                                <br />
                                <table cellpadding="4" class="style8">
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style88">
                    Signature<br />
                                            (Primary Applicant)</td>
                                        <td class="style82">
                    <div style="border: 1px solid #000000; height: 50px;" class="widthSize80_per">
                    </div>
                                        </td>
                                        <td class="style83">
                    Signature<br />
                                            (Co-Applicant)</td>
                                        <td>
                    <div style="border: 1px solid #000000; height: 50px;" class="widthSize99_per">
                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style88">
                                            &nbsp;</td>
                                        <td class="style82">
                                            &nbsp;</td>
                                        <td class="style83">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style88">
                                            Date</td>
                                        <td class="style82">
                    <div style="border: 1px solid #000000; height: 20px;" class="widthSize80_per">
                    </div>
                                        </td>
                                        <td class="style83">
                                            Date</td>
                                        <td>
                    <div style="border: 1px solid #000000; height: 20px;" class="widthSize99_per">
                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        
                                            <td class="style84" colspan="4" 
                                            
                                            style="border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: #333333">
                                            </td>
                                    </tr>
        </table>
    </div>
    </div>
    
    <br/>
    <br/>
    <div class="clear"></div>

           <div id="divBALetterPage2" class="widthSize80_per paddingAllSide10_per" 
        style="padding: 5%; width: 8.27in; height: 11.69in;"> 
    <div id="div2" class="widthSize100_per">
        
        <table class="style1">
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
            <td class="style3" rowspan="3">
                <table class="style1">
                    <tr>
                        <td class="style7 rightAlign">
                            Standard Chartered</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" CssClass="floatRight" Font-Bold="True" 
                                Font-Size="Larger" ForeColor="#666666" style="font-weight: 700" Text="Saadiq"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="5">
                <asp:Image ID="Image2" runat="server" Height="66px" 
                    ImageUrl="~/Images/scb_logo.jpg" Width="90%"  CssClass="floatLeft"/>
            </td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
        </tr>

    </table> 
    
       <table class="style8">
            <tr>
                <td class="style35" colspan="3">
                    <asp:Label ID="Label4" runat="server" 
                        
                        Text="................................................................................................."></asp:Label>
                </td>
                <td class="style42">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style44">
                    Branch</td>
                <td class="style37">
                    :</td>
                <td class="style41">
                    <asp:Label ID="Label5" runat="server" 
                        Text="..............................................................................."></asp:Label>
                </td>
                <td class="style43">
                    A/C No</td>
                <td class="style40">
                    :</td>
                <td class="rightAlign">
                    <asp:Label ID="Label6" runat="server" 
                        Text="............................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style44">
                    Date</td>
                <td class="style37">
                    :</td>
                <td class="style41">
                    <asp:Label ID="Label7" runat="server" 
                        Text="..............................................................................."></asp:Label>
                                        </td>
                <td class="style42">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style38">
                    &nbsp;</td>
                <td class="style37">
                    &nbsp;</td>
                <td class="style41">
                    &nbsp;</td>
                <td class="style42">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br/> 
          <table class="style8">
              <tr>
                  <td class="style45">
                      BANKING ARRANGEMENT LETTER</td>
              </tr>
              <tr>
                  <td>
                      Dear Sir / Madam,<br />
                      We are pleased to advise that the following facillity has been granted to 
                      you on the basis of your 
                      application dated&nbsp;&nbsp;
                      <asp:Label ID="Label8" runat="server" 
                          
                          Text="........................................................................................................................................."></asp:Label>
                  </td>
              </tr>
        </table>
        <br/>
          <table cellpadding="4" class=" reference style68">
                                        <tr bgcolor="Gray">
                                            <td bgcolor="Gray" colspan="4">
                                                <b>FACILITY DETAILS</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                                <b>Auto Loan </b><input id="Checkbox3" type="checkbox" class="style32" /><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lifestyle<input 
                                                    id="Checkbox5" type="checkbox" class="style32" />&nbsp;&nbsp;Auto Fixed Loan
                                                <input id="Checkbox6" type="checkbox" class="style32" />&nbsp;&nbsp;Takeover
                                                <input id="Checkbox7" type="checkbox" class="style32" />&nbsp;&nbsp;&nbsp;&nbsp; 
                                                Used car finance </b>
                      <input id="Checkbox4" type="checkbox" class="style32" /> </td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      LOAN AMOUNT</td>
                                            <td class="style32">
                      <asp:Label ID="Label9" runat="server" Text="BDT"></asp:Label>
                                            </td>
                                            <td class="style32">
                      INTEREST RATE</td>
                                            <td>
                      <asp:Label ID="Label10" runat="server" Text=".........................................................." 
                          CssClass="floatLeft" Font-Bold="True"></asp:Label>
                      <asp:Label ID="Label11" runat="server" Text="% p.a" CssClass="floatRight" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      EMI AMOUNT</td>
                                            <td class="style32">
                      <asp:Label ID="Label13" runat="server" Text="BDT"></asp:Label>
                                            </td>
                                            <td class="style32">
                                                NO. OF INSTALLMENTS</td>
                                            <td class="style32">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      TENOR</td>
                                            <td class="style32">
                      <asp:Label ID="Label14" runat="server" 
                          Text="............................................."></asp:Label>
                                            </td>
                                            <td class="style32">
                                                EXPIRY DATE</td>
                                            <td class="style32">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      BRAND</td>
                                            <td class="style32">
                      <asp:Label ID="Label15" runat="server" 
                          Text="............................................."></asp:Label>
                                            </td>
                                            <td class="style32">
                                                MODEL</td>
                                            <td class="style32">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="style32">
                      PROCESSING FEE</td>
                                            <td class="style32" colspan="3">
                      <asp:Label ID="Label16" runat="server" Text="BDT"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
          <br />
                                                <table cellpadding="4" class="style8">
                                                    <tr>
                                                        <td class="style71" colspan="2" 
                                                            style="font-size: 14px; font-family: AriaL;">
                    DOCUMENTATION:</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            DP Note &amp; Letter of Continuation for BDT&nbsp;&nbsp;
                      <asp:Label ID="Label18" runat="server" 
                          Text="............................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Memorandum of Deposit over&nbsp;
                      <asp:Label ID="Label44" runat="server" 
                          Text="........................................................................"></asp:Label>
                                            &nbsp;for BDT
                      <asp:Label ID="Label45" runat="server" 
                          Text="............................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Letter of Lien and Set-off over Deposit A/C No
                      <asp:Label ID="Label19" runat="server" 
                          Text="............................................."></asp:Label>
                                            &nbsp;for BDT
                      <asp:Label ID="Label20" runat="server" 
                          Text="............................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Post-dated Cheques from
                      <asp:Label ID="Label21" runat="server" 
                          Text="................................................................................"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Undated Cheque
                      <asp:Label ID="Label22" runat="server" 
                          Text="................................................................................................"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Letter of Hypothecation&nbsp;&nbsp;
                      <asp:Label ID="Label23" runat="server" 
                          Text="...................................................................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Letter of Authority to Sell Hypothecate Prorerty(Notarized)
                      <asp:Label ID="Label24" runat="server" 
                          Text="........................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Letter of Authorisation for Encashment of Securities&nbsp;
                      <asp:Label ID="Label25" runat="server" 
                          Text="................................"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Ownership Transfer From
                      <asp:Label ID="Label26" runat="server" 
                          Text="..................................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Security(ies):&nbsp;
                      <asp:Label ID="Label27" runat="server" 
                          Text="........................................................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            <img alt="" src="../Images/box.PNG" /></td>
                                                        <td>
                                                            Other(s)&nbsp;
                      <asp:Label ID="Label28" runat="server" 
                          Text="................................................................................."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style73">
                                                            </td>
                                                        <td class="style87">
                                                            Your borrowing is to be secured by the following securities in the manner 
                                                            prescribed hereinafter together with any other security which now or hereinafter 
                                                            may be held with the Bank, all of which securities shall be available to it as 
                                                            security for all liabilities of the borrower(s) at any time. All clauses, terms, conditions 
                                                            coinvenience etc expressed in those securities will apply unless expressly 
                                                            modefied or varied therein.</td>
                                                    </tr>
                                                </table>
                                <table cellpadding="4" class="style8">
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style74">
                                            1.</td>
                                        <td>
                                            DP note convering the value of entire loan</td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style74">
                                            2.</td>
                                        <td>
                                            Securities mentioned in the Memorandum of Deposit
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style74">
                                            3.</td>
                                        <td>
                                            Hypothecation over Car Vehicle</td>
                                    </tr>
        </table>
                                <br />
                                <table cellpadding="4" class="style8">
                                    <tr>
                                        <td class="style85">
                                            </td>
                                        <td class="style86" colspan="8" 
                                            style="border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: #333333">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style75">
                                            </td>
                                        <td class="style76" colspan="4">
                                            Debit Authority</td>
                                        <td class="style77" colspan="4">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style78" colspan="4">
                                            In consideration fo granting me/us Auto Loan Facility, Loan A/C no:</td>
                                        <td colspan="4">
                      <asp:Label ID="Label29" runat="server" 
                          Text=".................................................................."></asp:Label>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style78" colspan="4">
                                            I/we hareby authorise the Bank to debit my/our account no</td>
                                        <td colspan="4">
                      <asp:Label ID="Label30" runat="server" 
                          Text=".................................................................."></asp:Label>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td colspan="8">
                                            for an amount not exceeding BDT&nbsp;&nbsp;<asp:Label ID="Label31" runat="server" 
                        Text=".................................."></asp:Label>&nbsp;&nbsp;
                    <asp:Label ID="Label32" runat="server" 
                        Text="............"></asp:Label>
                &nbsp; in&nbsp;
                    <asp:Label ID="Label33" runat="server" 
                        Text="............"></asp:Label>
                &nbsp; consecutive monthly installments as on&nbsp;
                    <asp:Label ID="Label34" runat="server" 
                        Text="............"></asp:Label>
                &nbsp;day or following working day of each month commencing from the month&nbsp; of
                      <asp:Label ID="Label35" runat="server" 
                          Text="..........................................."></asp:Label>
                                            &nbsp;,
                      <asp:Label ID="Label36" runat="server" 
                          Text="..........................................."></asp:Label>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style78">
                                            &nbsp;</td>
                                        <td class="style79">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td colspan="2">
                                            month</td>
                                        <td class="style80">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; year</td>
                                        <td class="style78">
                                            &nbsp;</td>
                                        <td class="style78">
                                            &nbsp;</td>
                                    </tr>
                                    </table>
                                <br />
                                <table cellpadding="4" class="style8">
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style88">
                    Signature<br />
                                            (Primary Applicant)</td>
                                        <td class="style82">
                    <div style="border: 1px solid #000000; height: 50px;" class="widthSize80_per">
                    </div>
                                        </td>
                                        <td class="style83">
                    Signature<br />
                                            (Co-Applicant)</td>
                                        <td>
                    <div style="border: 1px solid #000000; height: 50px;" class="widthSize99_per">
                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style88">
                                            &nbsp;</td>
                                        <td class="style82">
                                            &nbsp;</td>
                                        <td class="style83">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        <td class="style88">
                                            Date</td>
                                        <td class="style82">
                    <div style="border: 1px solid #000000; height: 20px;" class="widthSize80_per">
                    </div>
                                        </td>
                                        <td class="style83">
                                            Date</td>
                                        <td>
                    <div style="border: 1px solid #000000; height: 20px;" class="widthSize99_per">
                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style21">
                                            &nbsp;</td>
                                        
                                            <td class="style84" colspan="4" 
                                            
                                            style="border-bottom-style: solid; border-bottom-width: 2px; border-bottom-color: #333333">
                                            </td>
                                    </tr>
        </table>
    </div>
    </div>
    
    <br/>
    <br/>
    <div class="clear"></div>
    
    <div id="divBALetterPage3" class="widthSize80_per paddingAllSide10_per" 
        style="padding: 0px; width: 8.27in; height: 11.69in;">
    <div id="div3" class="widthSize100_per">
       
        <table cellpadding="4" class="style8">
            <tr>
                <td style="padding: 5%; border: 2px solid #000000; font-family: Verdana; font-size: 9px;">
                    Any tax/fees/levy imposed by the Government and/or appropriate authorities from 
                    time to time either on the principal or on the interest portion of Sanchaya 
                    Patras and/or any other Government securities shall be in the account of the 
                    customer. The customer, in this connection, will ensure timely payment of such 
                    tax/fees/levy and indemnify the Bank against all possible 
                    losses/penalties/expenses incurred there from due to his/her failure in doing 
                    so. The Bank will not take any responsibility for monitoring the maturity dates 
                    of a customer&#39;s securities &amp; as such will not be responsible for any interest 
                    loss as a result of the customer&#39;s failure to renew such securities upon 
                    maturity.<br />
                    <br />
                    The above facility is granted in accordance with the terms and conditions 
                    contained in the Auto Loan Application signed by you. While the above facility 
                    is subject to review on or before the date of expiry specified above, it is at 
                    all time available solely at the Bank&#39;s discretion and is, therefore, subject to 
                    repayment on demand.<br />
                    <br />
                    For pre-payment or cancellation of the facility ahead of agreed terms, a 
                    settlement fee as decided by Bank will be charged. For delayed payment, a penal 
                    interest at 24% will be charged. Any legal fees and other costs incurred by the 
                    Bank in connection with the facilities will be due to your account.<br />
                    <br />
                    Interest rate mentioned in this letter is floating and Bank shall have the 
                    discretion to change and determine the interest rate from time to time. Due to 
                    change of the interest rate, the Bank shall also have the right to change the 
                    EMI amount as well as the initial loan tenor. You hereby acknowledge such right 
                    of the Bank and give your consent unconditionally to the Bank to make such 
                    changes.<br />
                    <br />
                    Interest on the facility shall accrue at the rate mentioned in this letter or at 
                    such other rate as determined by the Bank from time to time at its sole 
                    discretion. Any repayment, whether in part or full, of facility will be 
                    attributable first to interest which has accrued on the facility and then to 
                    principal. The rate of interest determined by the Bank from time to time shall 
                    remain in full force and effect as if the facilities granted to the borrower 
                    were still in force, even though the account/accounts in the customer&#39;s name 
                    with the bank are closed, become dormant or are subject to litigation until full 
                    settlement of alt the customer&#39;s liabilities to the bank.<br />
                    <br />
                    The Bank may cancel the facility and demand immediate repayment of the loan by a 
                    letter posted to your address recorded with the Bank if any of your Financial 
                    Indebtedness (a) is not paid when due or within any originally applicable grace 
                    period and the same is declared to be or otherwise becomes due and payable or is 
                    placed on demand prior to its specified maturity; (b) becomes due and payable 
                    prior to its specified maturity following an event of default or any provision 
                    having a similar effect (however described). For the purpose of this clause 
                    &quot;Financial Indebtedness&quot; means any indebtedness in respect of any credit 
                    facilities, financial assistance, banking accommodation or borrowing availed or 
                    enjoyed by you or from any financial institution including but is not limited to 
                    the Bank; or any guarantee or indemnity given by you to any financial 
                    institution including but is not limited to the Bank in relation to any service, 
                    performance of any obligation, credit facilities, financial assistance, banking 
                    accommodation or borrowing (whether actual or contingent) of any third party.<br />
                    <br />
                    Without prejudice to the conditions set out in preceding paragraph, the Bank may 
                    cancel the facility at its absolute discretion &amp; demand immediate repayment of 
                    the loan by a letter posted to your address recorded with the Bank. Bank shall 
                    have a first priority fixed charge over the vehicle by way of hypothecation and 
                    you shall take all measures to register the name of the Bank with the 
                    registration authority and record name of the Bank in the Blue Book of the Car. 
                    In the event of your failure to perform any obligation under this letter r 
                    security granted by you, Bank may sell the car as your constituted attorney 
                    appointed through the notarized irrevocable letter of authority you have granted 
                    to the Bank.
                    <br />
                    <br />
                    You hereby authorize the Bank, for ease of monitoring and accounting, to route 
                    all payments of the loan through a special account which will be opened in your 
                    name in the Bank&#39;s book. Furthermore, you hereby authorize the Bank to re-fix 
                    the quantum of monthly installments at it&#39;s sole discretion. You understand and 
                    agree that the aggregate of any debit balance in the said special account and 
                    your Auto loan account is the total outstanding on account of the Auto Loan 
                    scheme and you will always remain liable to repay this outstanding and any 
                    interest and other charges thereon.<br />
                    <br />
                    By accepting this offer, you hereby authorise the Bank to disclose the 
                    information of your loan to any regulatory authority, any office in the Bank, 
                    any assignee of the Bank, agent of the Bank or any subsidiary company of the 
                    Bank or any company within the Standard Chartered group. You also agree and 
                    authorize bank to either destroy or cancel any unused undated or post-dated 
                    cheque(s) pertaining to the loan facility upon its closure or full settlement.<br />
                    <br />
                    You hereby agree that Bank may at any time or times hereinafter without notice 
                    to you apply the right of set off of all or any of the monies from time to time 
                    standing to the credit of your any account with the Bank in whichever form they 
                    are and whatever name they may be called, in or toward the discharge and 
                    satisfaction of all sums of money which now are or at any time or times 
                    hereinafter may become due or owing to the Bank by you either alone or jointly 
                    with any other person or persons on any account or in respect of any liability 
                    whatsoever whether actual or contingent and whether in the capacity of principal 
                    debtor or guarantor surety or principal obligor or otherwise. You also agree or 
                    authorize the Bank to restrict withdrawal from any of your accounts as 
                    designated by the Bank after notice to you until all of your liability with the 
                    Bank is fully adjusted and settled.<br />
                    <br />
                    Upon acceptance, this offer shall be construed as a valid contract between the 
                    Bank and yourself and valid and binding upon yourself and your successors and 
                    shall be valid notwithstanding any change in the document of incorporation of 
                    the Bank or any merger, acquisition or amalgamation of the Bank with any other 
                    body corporate.<br />
                    <br />
                    This letter is subject to the laws of Bangladesh and courts of Bangladesh shall 
                    have jurisdiction to resolve any dispute.<br />
                    <br />
                    <br />
                    Yours faithfully
                    <br />
                    <br />
                    <br />
                    <br />
                    -----------------------&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    ---------------------------
                    <br />
                    Authorized signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Authorized signature<br />
                    <br />
                    <br />
                    <br />
                    <b>Acceptance:<br />
                    </b>I / We have carefully read and understood the above terms and conditions and 
                    agree with them.<br />
                    <br />
                    <br />
                    <br />
                    <br />
                    ------------------------------------------------&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    ---------------------------------------<br />
                    Signature of primary applicant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Signature of joint applicant<br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
       
    </div>
    </div>
    
    <br/>
    <br/>
    <div class="clear"></div>
</asp:Content>

