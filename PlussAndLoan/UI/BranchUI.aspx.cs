﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BranchUI : System.Web.UI.Page
    {

        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2
        }

        BranchInformationManager branchInformationManagerObj=null;
        BankInformationManager bankInformationManagerObj=null;
        List<BankInformation> bankInformationListObj=null;
        List<LocationInformation> locationInformationListObj=null;
        protected void Page_Load(object sender, EventArgs e)
        {
            branchInformationManagerObj = new BranchInformationManager();
            bankInformationManagerObj = new BankInformationManager();
            locationInformationListObj = branchInformationManagerObj.GetAllLocationInformation();
            bankInformationListObj = bankInformationManagerObj.GetAllBankInfo();
            bankInformationListObj = bankInformationListObj.FindAll(findInactiveBank => findInactiveBank.Status != 0);
            if (!IsPostBack)
            {
                PopulateLocationDropDownList();
                locationDropDownList.SelectedValue = "0";
                PupolateBankDropDownList();
                bankNameDropDownList.SelectedValue = "0";
                FillBranchInfoGridView();
                clearingStatusDropdownList.SelectedValue = "1";
            }        
        }

        private void FillBranchInfoGridView()
        {
            SearchedBranchInfo("");
        
        }

        private void PupolateBankDropDownList()
        {
            BankInformation bankInformationObj = new BankInformation();
            bankInformationObj.Id = 0;
            bankInformationObj.Name = "Select";
            bankInformationListObj.Insert(0, bankInformationObj);
            bankNameDropDownList.DataSource = bankInformationListObj;
            bankNameDropDownList.DataTextField = "Name";
            bankNameDropDownList.DataValueField = "Id";
            bankNameDropDownList.DataBind();
        }

        private void PopulateLocationDropDownList()
        {
            LocationInformation locationInformationObj = new LocationInformation();
            locationInformationObj.Id = 0;
            locationInformationObj.Name = "Select";
            locationInformationListObj.Insert(0, locationInformationObj);
            locationDropDownList.DataSource = locationInformationListObj;
            locationDropDownList.DataTextField = "Name";
            locationDropDownList.DataValueField = "Id";
            locationDropDownList.DataBind();
        }


        protected void uploadButton_Click(object sender, EventArgs e)
        {
            string path = Request.PhysicalApplicationPath;
            path += @"Uploads\";
            errorMsgLabel.Text = "";
            string fileName = "";
            int insertOrUpdateRow = -1;
            if (FileUpload.FileName != "")
            {
                fileName = Server.HtmlEncode(FileUpload.FileName);
                path += fileName;
                FileUpload.SaveAs(path);
                List<BranchInformation> branchInformationListObj = ImportDataFromExcel(path);
                insertOrUpdateRow = branchInformationManagerObj.InsertORUpdateBranchInfo(branchInformationListObj);
                if (insertOrUpdateRow > 0)
                {
                    errorMsgLabel.Text = "Data upload sucessfully.";
                    addButton.Text = "Save";
                    ClearField();
                    FillBranchInfoGridView();
                }
                else
                {
                    errorMsgLabel.Text = "Data not upload sucessfully.";
                }
            }
            else
            {
                errorMsgLabel.Text = "Please Select file name.";
            }
        }

        public List<BranchInformation> ImportDataFromExcel(string fileName)
        {
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            string conString = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source="
                                                                       + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));

            System.Data.OleDb.OleDbConnection contoexcel = new System.Data.OleDb.OleDbConnection(conString);
            System.Data.DataTable branchInoDataTableObj = new System.Data.DataTable();

            try
            {
                contoexcel.Open();
                branchInoDataTableObj = contoexcel.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);

                string sheetName = branchInoDataTableObj.Rows[0]["TABLE_NAME"].ToString();
                branchInoDataTableObj.Clear();
                branchInoDataTableObj.Columns.Clear();
                string commandtext = "SELECT [Bank Name] AS BankName, [Branch Name] AS BranchName, Location, Address, [Clearing Status] AS ClearingStatus FROM [" + sheetName + "]";
                System.Data.OleDb.OleDbDataAdapter AdapterforExcel = new System.Data.OleDb.OleDbDataAdapter(commandtext, contoexcel);
                AdapterforExcel.Fill(branchInoDataTableObj);
                if (branchInoDataTableObj != null)
                {
                    DataTableReader branchDataTableReaderObj = branchInoDataTableObj.CreateDataReader();
                    if (branchDataTableReaderObj != null)
                    {
                        while (branchDataTableReaderObj.Read())
                        {
                            BranchInformation branchInformationObj = new BranchInformation();
                            branchInformationObj.Bank.Name = EscapeCharacter(Convert.ToString(branchDataTableReaderObj["BankName"]).Trim());
                            Int32 bankId = bankInformationManagerObj.GetBankId(branchInformationObj.Bank.Name);
                            if (bankId > 0)
                            {
                                branchInformationObj.Bank.Id = bankId;
                            }
                            else
                            {
                                BankInformation bankInformationObj = new BankInformation();
                                bankInformationObj.Code = "Undefined";
                                bankInformationObj.Name = branchInformationObj.Bank.Name;
                                bankInformationObj.Status = 1;
                                int insertBankName = bankInformationManagerObj.InsertORUpdateBankInfo(bankInformationObj);
                                if (insertBankName > 0)
                                {
                                    bankId = bankInformationManagerObj.GetBankId(branchInformationObj.Bank.Name);
                                    branchInformationObj.Bank.Id = bankId;
                                }
                                else
                                {
                                    branchInformationObj.Bank.Id = 0;
                                }
                            }
                            branchInformationObj.Name = EscapeCharacter(Convert.ToString(branchDataTableReaderObj["BranchName"]).Trim());
                            branchInformationObj.Location.Name = EscapeCharacter(Convert.ToString(branchDataTableReaderObj["Location"]).Trim());

                            Int32 locationId = branchInformationManagerObj.GetLocationId(branchInformationObj.Location.Name);
                            if (locationId > 0)
                            {
                                branchInformationObj.Location.Id = locationId;
                            }
                            else
                            {

                                int insertLocationName = branchInformationManagerObj.InsertLocationName(branchInformationObj.Location.Name);
                                if (insertLocationName > 0)
                                {
                                    locationId = branchInformationManagerObj.GetLocationId(branchInformationObj.Location.Name);
                                    branchInformationObj.Location.Id = locationId;
                                }
                                else
                                {
                                    branchInformationObj.Location.Id = 0;
                                }
                            }
                            branchInformationObj.Address = EscapeCharacter(Convert.ToString(branchDataTableReaderObj["Address"]).Trim());
                            branchInformationObj.Status = 1;
                            if (branchDataTableReaderObj["ClearingStatus"] != null)
                            {
                                branchInformationObj.ClearingStatus = Convert.ToInt32('0' + branchDataTableReaderObj["ClearingStatus"].ToString());
                            }
                            else
                            {
                                branchInformationObj.ClearingStatus = 0;
                            }
                            branchInformationObj.InsertOrUploadFlag = 2;
                            branchInformationListObj.Add(branchInformationObj);
                        }
                        branchDataTableReaderObj.Close();
                    }
                    branchInoDataTableObj.Clear();
                }

            }
            catch (System.Data.OleDb.OleDbException exp)
            {
                return null;
            }
            finally
            {
                contoexcel.Close();
            }
            return branchInformationListObj;
        }
        public string EscapeCharacter(string fieldString)
        {
            string returnMessage = "";
            if (fieldString.Length > 0)
            {
                fieldString = fieldString.Replace("'", "''");
                fieldString = fieldString.Replace('"', '\"');
                fieldString = fieldString.Replace(@"\", @"\\");
                returnMessage = fieldString;
            }
            return returnMessage;
        }
        protected void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                errorMsgLabel.Text = "";
                List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
                BranchInformation branchInformationObj = new BranchInformation();
                int insertOrUpdateRow = -1;
                branchNameMF.Text = "";
                if (!String.IsNullOrEmpty(branchIdHiddenField.Value))
                {
                    branchInformationObj.Id = Convert.ToInt32(branchIdHiddenField.Value);
                }
                else
                {
                    branchInformationObj.Id = 0;
                }
                branchInformationObj.Bank.Id = Convert.ToInt32(bankNameDropDownList.SelectedValue);
                if (branchInformationObj.Bank.Id == 0)
                {
                    bankNameMF.Text = "Please select bank name.";
                    return;
                }
                bankNameMF.Text = "";
                if (String.IsNullOrEmpty(branchNameTextBox.Text))
                {
                    branchNameMF.Text = "Please enter branch name.";
                    branchNameTextBox.Focus();
                    return;
                }
                branchNameMF.Text = "";
                branchInformationObj.Name = branchNameTextBox.Text.Trim();
                if (branchInformationManagerObj.IsExistsBranchName(branchInformationObj.Name, branchInformationObj.Id, branchInformationObj.Bank.Id))
                {
                    errorMsgLabel.Text = "Branch name already exists.";
                    return;
                }
                errorMsgLabel.Text = "";

                branchInformationObj.Location.Id = Convert.ToInt32(locationDropDownList.SelectedValue);
                if (branchInformationObj.Location.Id == 0)
                {
                    locationMF.Text = "Please select location name.";
                    return;
                }
                locationMF.Text = "";
                branchInformationObj.Address = addressTextBox.Text.Trim();
                if (String.IsNullOrEmpty(branchInformationObj.Address))
                {
                    addressMF.Text = "Please enter address";
                    return;
                }
                addressMF.Text = "";
                branchInformationObj.Status = Convert.ToInt32(statusDropdownList.SelectedValue);
                branchInformationObj.ClearingStatus = Convert.ToInt32(clearingStatusDropdownList.SelectedValue);
                if (addButton.Text == "Update")
                {
                    branchInformationObj.InsertOrUploadFlag = 0;
                }
                else
                {
                    branchInformationObj.InsertOrUploadFlag = 1;
                }
                branchInformationListObj.Add(branchInformationObj);

                insertOrUpdateRow = branchInformationManagerObj.InsertORUpdateBranchInfo(branchInformationListObj);

                if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT))
                {
                    errorMsgLabel.Text = "Data save sucessfully.";
                    addButton.Text = "Save";
                    ClearField();
                    FillBranchInfoGridView();

                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERTERROR))
                {
                    errorMsgLabel.Text = "Data not saved sucessfully.";
                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                {
                    errorMsgLabel.Text = "Data update sucessfully.";
                    addButton.Text = "Save";
                    ClearField();
                    FillBranchInfoGridView();
                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                {
                    errorMsgLabel.Text = "Data not update sucessfully.";
                }
                new AT(this).AuditAndTraial("Bank Information", errorMsgLabel.Text);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Branch");  
            }
        }

        public bool IsEmptyOrBlank( Control controlObj)
        {
            TextBox textBox = (TextBox)controlObj;
            if (String.IsNullOrEmpty(textBox.Text))
            {
                if (String.IsNullOrEmpty(textBox.Text))
                {
                    branchNameMF.Text = "Please enter branch name.";
                    textBox.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public void ClearField()
        {
            branchIdHiddenField.Value = "";
            branchNameTextBox.Text = "";
            bankNameDropDownList.SelectedValue  = "0";
            locationDropDownList.SelectedValue = "0";
            addressTextBox.Text = "";
            statusDropdownList.SelectedValue = "1";
            clearingStatusDropdownList.SelectedValue = "1";
        }
        protected void searchbutton_Click(object sender, EventArgs e)
        {
            try
            {
                errorMsgLabel.Text = "";
                string searchKey = "";
                if (String.IsNullOrEmpty(searchTextBox.Text))
                {
                    searchKey = "";
                }
                else
                {
                    switch (searchTextBox.Text.ToUpper().Replace(" ", "").Trim())
                    {
                        case "ACTIVE":
                            searchKey = "1";
                            break;
                        case "INACTIVE":
                            searchKey = "0";
                            break;
                        case "OFFLINECLEAR":
                            searchKey = "0";
                            break;
                        case "ONLINECLEAR":
                            searchKey = "1";
                            break;
                        case "OFFLINENOTCLEAR":
                            searchKey = "2";
                            break;
                        default:
                            searchKey = searchTextBox.Text.Trim();
                            break;
                    }         
                }
                SearchedBranchInfo(searchKey);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Search Brach");  
            }
        }

        private void SearchedBranchInfo(string searchKey)
        {
            //FileInfo BranchXML = new FileInfo(Server.MapPath("../includes/BranchList.xml"));
            //StreamWriter stremWriter = null;
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            //stremWriter = BranchXML.CreateText();
            if (searchKey == "")
            {
                branchInformationListObj = branchInformationManagerObj.GetAllBranchInformation();
            }
            else
            {
                branchInformationListObj = branchInformationManagerObj.GetAllBranchInformation(searchKey);
            }
            try
            {
                var xml="<xml id='xml_data'><rows>";
               // xml += "rows";
                //stremWriter.WriteLine("<rows>");
                foreach (BranchInformation branchInformationObj in branchInformationListObj)
                {
                    xml+="<row id='" + branchInformationObj.Id.ToString() + "'>";
                    xml+="<cell>";
                    xml+=branchInformationObj.Id.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=branchInformationObj.Bank.Id.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(branchInformationObj.Bank.Name.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(branchInformationObj.Name.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=branchInformationObj.Location.Id.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=branchInformationObj.Location.Name.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=branchInformationObj.Address.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=branchInformationObj.Status.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=branchInformationObj.StatusMsg.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=branchInformationObj.ClearingStatus.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=branchInformationObj.ClearingStatusMsg.ToString();
                    xml+="</cell>";
                    xml+="</row>";
                }
                xml += "</rows></xml>";

               // gvBranch.DataSource = branchInformationListObj;
                //gvBranch.DataBind();
                dvXml.InnerHtml = xml;
            }
            finally
            {
             //   stremWriter.Close();
            }
           // stremWriter.Close();
        }

        public void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            //GridViewRow row = gvBranch.SelectedRow;
            //var id = row.Cells[1].Text;

            //var lbBankId = (Label)gvBranch.SelectedRow.Cells[2].FindControl("bankId");
            //var bankId = lbBankId.Text;
            
            //var lbBankName = (Label)gvBranch.SelectedRow.Cells[3].FindControl("bankName");
            //var bankName = lbBankName.Text;

            //var branchName = row.Cells[4].Text;
            //var brLbl = (Label)gvBranch.SelectedRow.Cells[5].FindControl("BranchId");
            //var branchId = brLbl.Text;
            
            //var lblocationId = (Label)gvBranch.SelectedRow.Cells[6].FindControl("locationId");
            //var locationId = lblocationId.Text;

            //var lblocation = (Label)gvBranch.SelectedRow.Cells[7].FindControl("location");
            //var location = lblocation.Text;

            //var address = row.Cells[8].Text;

            //var lbStatus = (Label)gvBranch.SelectedRow.Cells[9].FindControl("Status");
            //var status = lbStatus.Text;

            //var lbClearStatus = (Label)gvBranch.SelectedRow.Cells[11].FindControl("ClearingStatus");
            //var clearingStatus = lbClearStatus.Text;

            //addButton.Text = "Update";
            //branchIdHiddenField.Value = id;
            //bankNameDropDownList.SelectedValue = bankId;
            //branchNameTextBox.Text = branchName;
            //locationDropDownList.SelectedValue = locationId;
            //addressTextBox.Text = address;
            //clearingStatusDropdownList.SelectedValue = clearingStatus;
            //statusDropdownList.SelectedValue = status;
        }

        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearField();
        }
    }
}
