﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_RemarksTemplateUI : System.Web.UI.Page
    {
        public RemarksManager remarksManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            remarksManagerObj = new RemarksManager();
            if (!Page.IsPostBack)
            {
                LoadRemarksForList();
                LoadData("");
            }
        }
        public void LoadRemarksForList()
        {
            List<RemarksFor> remarksForObjList = new List<RemarksFor>();
            RemarksFor remarksForObj = new RemarksFor();
            remarksForObjList = remarksManagerObj.SelectRemarksForList();
            remarksForObj.Id = 0;
            remarksForObj.Name = "Select remarks for";
            remarksForObjList.Insert(0,remarksForObj);
            remarksForDropDownList.DataSource = remarksForObjList;
            remarksForDropDownList.DataTextField = "Name";
            remarksForDropDownList.DataValueField = "Id";
            remarksForDropDownList.DataBind();
        }
        private void LoadData(string searchKey)
        {
           
            List<Remarks> remarksObjList = new List<Remarks>();
            if (searchKey == "")
            {
                remarksObjList = remarksManagerObj.SelectRemarksList();
            }
            else
            {
                remarksObjList = remarksManagerObj.SelectRemarksList(searchKey);
            }
            try
            {
                var xml = "<xml id='xml_data'><rows>";
                foreach (Remarks remarksObj in remarksObjList)
                {
                    xml+="<row id='" + remarksObj.Id.ToString() + "'>";

                    xml+="<cell>";
                    xml+=remarksObj.Id.ToString();
                    xml+="</cell>";
                    if (remarksObj.Dtype == 1)
                    {
                        xml+="<cell>";
                        xml+="Remarks";
                        xml+="</cell>";
                    }
                    else
                    {
                        xml+="<cell>";
                        xml+="Condition";
                        xml+="</cell>";
                    }
                    xml+="<cell>";
                    xml+=remarksObj.RemarksForName.Id.ToString();
                    xml+="</cell>";

                    xml+="<cell>";
                    xml+=remarksObj.RemarksForName.Name.ToString();
                    xml+="</cell>";

                    xml+="<cell>";
                    xml+=AddSlash(remarksObj.Name);
                    xml+="</cell>";

                    if (remarksObj.Status == 1)
                    {
                        xml+="<cell>";
                        xml+="Active";
                        xml+="</cell>";
                    }
                    else
                    {
                        xml+="<cell>";
                        xml+="Inactive";
                        xml+="</cell>";
                    }
                    xml += "<cell>";
                    xml += remarksObj.Dtype.ToString();
                    xml += "</cell>";
                    xml+="</row>";
                }
                xml += "</rows></xml>";
                remarksDiv.InnerHtml = xml;
            }
            finally
            {
            }
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                int remarks = 0;
                Remarks remarksObj = new Remarks();
                if (IsBlank())
                {
                    remarksObj.Id = Convert.ToInt32( remarksIdHiddenField.Value);
                    remarksObj.ProductId = Convert.ToInt32("1");
                    //remarksObj.Dtype = Convert.ToInt32("1");
                    remarksObj.Dtype = Convert.ToInt32(cmbTempleteType.SelectedValue);
                    remarksObj.Name = Convert.ToString(remarksTextBox.Text.Trim());
                    remarksObj.RemarksNo = Convert.ToInt32("1");
                    remarksObj.RemarksFor = Convert.ToInt32(remarksForDropDownList.SelectedIndex);

                    if (statusDropDowonList.Text == "Active")
                    {
                        remarksObj.Status = 1;
                    }
                    else
                    {
                        remarksObj.Status = 0;
                    }
                    remarks = remarksManagerObj.SendRemarksInToDB(remarksObj);
                    if (remarks == INSERTE)
                    {
                        Alert.Show("Save successfully");
                        new AT(this).AuditAndTraial("Remarks Template", "Save successfully");
                        LoadData("");
                        ClearField();
                    }
                    else if (remarks == UPDATE)
                    {
                        Alert.Show("Update successfully");
                        new AT(this).AuditAndTraial("Remarks Template", "Update successfully");
                        LoadData("");
                        ClearField();
                    }
                    else if (remarks == ERROR)
                    {
                        Alert.Show("Error in data!");
                        new AT(this).AuditAndTraial("Remarks Template", "Error in data!");
                    }

                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Remarks Template");
            }

        }
        private bool IsBlank()
        {
            if (remarksTextBox.Text == "")
            {
                Alert.Show("Please input Remarks");
                remarksTextBox.Focus();
                return false;
            }
            else if (remarksForDropDownList.SelectedIndex == 0)
            {
                Alert.Show("Please select remarks for");
                remarksForDropDownList.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearField();
        }
        private void ClearField()
        {
            remarksIdHiddenField.Value = remarksManagerObj.SelectRemarksMaxId().ToString();
            remarksTextBox.Text = "";
            remarksForDropDownList.SelectedIndex = 0;
            statusDropDowonList.SelectedIndex = 0;
        }

        protected void searchbutton_Click(object sender, EventArgs e)
        {
            try
            {
                string searchKey = null;
                if (searchTextBox.Text != "")
                {
                    searchKey = searchTextBox.Text;
                    if (searchTextBox.Text == "Active")
                    {
                        searchKey = "1";
                    }
                    else if (searchTextBox.Text == "Inactive")
                    {
                        searchKey = "0";
                    }
                    LoadData(searchKey);
                }
                else
                {
                    LoadData("");
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Search Remarks Template");
            }
        }
    }
}
