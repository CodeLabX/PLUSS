﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_test2" Codebehind="test2.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
           <link href="../CSS/Default.css" rel="stylesheet" type="text/css" />

 <link href="../CSS/admin.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/tabs.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/AutoLoanCommon.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/TableLayoutForTabControls.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
     <div class="divLeftNormalClass widthSize100_per" style="margin-bottom: 0px;  margin-top: 5px; padding: 0px;" >
           <div  class="divHeadLine widthSize100_per" style="text-align: center; margin: 0px;"> <span style="font-weight: bold; font-family: verdana; font-size: medium">LOAN DETAILS</span> </div>
           <div  class="divHeadLineWhiteBg widthSize100_per" style="text-align: center; margin: 0px;"> <span style="font-weight: bold; font-family: verdana; font-size: medium">CUSTOMER INFORMATION</span> </div>
           
           <div class="divLeftBig" style="width :750px; margin: 0px; ">
           <div class="content" id="Div4" style="border: none; padding: 0;">
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">LLID:</label>
                            <input id="Text369" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CUSTOMER SEGMENT:</label>
                            <input id="Text370" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">DATE OF BIRTH:</label>
                            <input id="Text1" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">EMPLOYER CATAGORY:</label>
                            <input id="Text2" class="txtFieldBig widthSize22_per" type="text" disabled="disabled" />
                            <input id="Text12" class="txtFieldBig widthSize7_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">AGE:</label>
                            <input id="Text3" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">ASSESMENT METHOD:</label>
                            <input id="Text4" class="txtFieldBig widthSize22_per" type="text" disabled="disabled" />
                            <input id="Text14" class="txtFieldBig widthSize7_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">DECLARED INCOME:</label>
                            <input id="Text5" class="txtFieldBig widthSize15_per" type="text" disabled="disabled" />
                            <input id="Text11" class="txtFieldBig widthSize14_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">APPROPIATE INCOME:</label>
                            <input id="Text6" class="txtFieldBig widthSize22_per" type="text" disabled="disabled" />
                            <input id="Text13" class="txtFieldBig widthSize7_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">ASSESSED INCOME:</label>
                            <input id="Text7" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">TOTAL INCOME:</label>
                            <input id="Text8" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">JOINT APP INCOME:</label>
                            <input id="Text9" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CUSTOMER:</label>
                            <input id="Text10" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        
                       
                        
                        
                        
                        
                    
                    </div>
       </div>
       
       
       <div class="clear"></div>
       <br/>
       
       <div  class="divHeadLineWhiteBg widthSize100_per" style="text-align: center; margin: 0px;"> <span style="font-weight: bold; font-family: verdana; font-size: medium">VENDOR &amp; VEHICLE INFORMATION</span> </div>
           
           <div class="divLeftBig" style="width :750px; margin: 0px; ">
           <div class="content" id="Div1" style="border: none; padding: 0;">
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">VENDOR:</label>
                            <input id="Text15" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">MOU:</label>
                            <input id="Text16" class="txtFieldBig widthSize5_per " type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 19%;*width: 19%;_width: 19%;">VENDOR CATAGORY:</label>
                            <input id="Text33" class="txtFieldBig widthSize5_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">PRODUCT:</label>
                            <input id="Text17" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">BRAND:</label>
                            <input id="Text18" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">MODEL:</label>
                            <input id="Text20" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">ENGINE(CC):</label>
                            <input id="Text19" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">VEHICLE STATUS:</label>
                            <input id="Text22" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">VEHICLE TYPE:</label>
                            <input id="Text21" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">MANUFACTURE YEAR:</label>
                            <input id="Text27" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">SEATING CAPACITY:</label>
                            <input id="Text28" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">QUOTED PRICE:</label>
                            <input id="Text29" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CONSIDERED PRICE:</label>
                            <input id="Text30" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">CONSIDERATION ON:</label>
                            <input id="Text31" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">CAR VERIFICATION:</label>
                            <input id="Text32" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        
                       
                        
                        
                        
                        
                    
                    </div>
       </div>
       <div class = "clear"></div>
       
       <br/>
       
       <div  class="divHeadLineWhiteBg widthSize100_per" style="text-align: center; margin: 0px;"> <span style="font-weight: bold; font-family: verdana; font-size: medium">LOAN INFORMATION</span> </div>
           
           <div class="divLeftBig" style="width :750px; margin: 0px; ">
           <div class="content" id="Div2" style="border: none; padding: 0;">
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">PO AMOUNT:</label>
                            <input id="Text23" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">GEN INSURANCE:</label>
                            <input id="Text24" class="txtFieldBig widthSize11_per " type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 6%;*width: 6%;_width: 6%;">ARTA:</label>
                            <input id="Text25" class="txtFieldBig widthSize11_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">TOTAL LOAN AMT:</label>
                            <input id="Text26" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">ARTA FROM:</label>
                            <input id="Text34" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">INTEREST RATE:</label>
                            <input id="Text35" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">TENOR:</label>
                            <input id="Text36" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                           <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">EMI:</label>
                            <input id="Text37" class="txtFieldBig widthSize30_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">PROFIT MARGIN:</label>
                            <input id="Text38" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                        <div class="fieldDiv">
                            <label class="lbl" style ="width: 18%;*width: 18%;_width: 18%;">LTV:</label>
                            <input id="Text39" class="txtFieldBig widthSize12_per" type="text" disabled="disabled" />
                            <label class="lbl rightAlign" style ="width: 5%;*width: 5%;_width: 5%;">DBR:</label>
                            <input id="Text41" class="txtFieldBig widthSize12_per" type="text" disabled="disabled" />
                            <label class="lbl  rightAlign " style ="width: 20%;*width: 20%;_width: 20%;">LOAN EXPIRY YEAR:</label>
                            <input id="Text40" class="txtFieldBig widthSize30_per floatRight" type="text" disabled="disabled" />
                        </div>
                       
                    </div>
       </div>
       <div class = "clear"></div>
        <br/>
        
      
      
      
      <div class="divLeftBig" style="width :370px; margin: 0px; ">
               <div  class="divHeadLineWhiteBg widthSize100_per" style="text-align: center; margin: 0px; border: none;"> <span style="font-weight: bold; font-family: verdana; font-size: medium"><u>DEVIATION</u></span> </div>
           <div class="content" id="Div5" style="border: none; padding: 0;">
               
                        <div style="border:none;">
                                        <table class="tableStyle" id="Table3" >
                                               <tr class="theader">
                                                   <td style=" width: 15%;*width: 15%;_width: 15%;">Level</td>
                                                   <td style=" width: 65%;*width: 65%;_width: 65%;">Description</td>
                                                   <td style=" width: 15%;*width: 15%;_width: 15%;">Code</td>
                                                   
                                               </tr>
                                               <tr>
                                                    <td><select id="Select13" onchange="analyzeLoanApplicationHelper.changeConsiderForOverDraftWithScb(1,'Pr', 'BM')">
                                                        </select>
                                                        </td>
                                                    <td><input id="Text59" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text66" type ="text" disabled="disabled" /></td>
                                                </tr>
                                               
                                               <tr>
                                                    <td><select id="Select14" onchange="analyzeLoanApplicationHelper.changeConsiderForOverDraftWithScb(1,'Pr', 'BM')">
                                                        </select>
                                                        </td>
                                                    <td><input id="Text58" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text60" type ="text" disabled="disabled" /></td>
                                                </tr>
                                                
                                               <tr>
                                                    <td><select id="Select15" onchange="analyzeLoanApplicationHelper.changeConsiderForOverDraftWithScb(1,'Pr', 'BM')">
                                                        </select>
                                                        </td>
                                                    <td><input id="Text61" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text62" type ="text" disabled="disabled" /></td>
                                                </tr>
                                                
                                              
                                              

                                        </table>
                       </div>
             </div>
          </div>
       
       
       
       <div class="divLeftBig centerAlign" style="margin-left: 5px; margin-bottom: 0px; width: 365px" >
                                            <span style="font-weight: bold; font-family: verdana;">DEVIATION FOUND</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;">AGE</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text1489" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;" id="tdDbr6">DBR</td>
                                                   <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text1490" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;">SEATING CAPACITY</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text1491" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;">SHARE PORTION</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;"><input id="Text1492" type ="text" class ="txtBoxSize_H22 widthSize100_per" disabled="disabled"/></td>
                                               </tr>




                                        </table>
                                        <div class="clear"></div>
                                         </div>
                                        <div class="clear"></div>
                                        <br/>
       
       
       
       
     
      
        
           
      <div class="divLeftBig" style="width :370px;*width :370px;_width :370px; margin: 0px; ">
               <div  class="divHeadLineWhiteBg widthSize100_per" style="text-align: center; margin: 0px; border: none;"> <span style="font-weight: bold; font-family: verdana; font-size: medium"><u>EXPOSURES</u></span> </div>
           
               
                        <div style="text-align:left; font-size:medium;font-weight:bold;padding: 2px;">ON SCB                                
                                    <div style="border:none;">
                                        <table class="tableStyle" id="Table1" >
                                               <tr class="theader">
                                                   <td >LIMIT</td>
                                                   <td id="td1">INTEREST</td>
                                                   <td >CONSIDERED</td>
                                                   
                                               </tr>
                                               <tr>
                                                    <td><input id="Text42" type ="text"disabled="disabled"/></td>
                                                    <td><input id="Text43" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text63" type ="text" disabled="disabled"/></td>
                                                </tr>
                                               <tr>
                                                    <td><input id="Text44" type ="text"disabled="disabled"/></td>
                                                    <td><input id="Text45" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text46" type ="text" disabled="disabled"/></td>
                                                </tr>
                                                 <tr>
                                                    <td><input id="Text47" type ="text"disabled="disabled"/></td>
                                                    <td><input id="Text48" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text49" type ="text" disabled="disabled"/></td>
                                                </tr>
                                              

                                        </table>
                                    </div>
                                </div>
                                

                                <div class="clear"></div>
                                
                                <div style="text-align:left; font-size:medium;font-weight:bold;padding: 2px; margin-top: 5px;">OFF-SCB                                
                                    <div style="border:none;">
                                        <table class="tableStyle" id="Table2" >
                                               <tr class="theader">
                                                   <td >BANK/FI</td>
                                                   <td >LIMIT</td>
                                                   <td id="td2">INTEREST</td>
                                                   <td >CONSIDERED</td>
                                                   
                                               </tr>
                                               <tr>
                                                    <td>
                                                        <select id="Select5" disabled="disabled"></select>
                                                    </td>
                                                    <td><input id="Text50" type ="text"disabled="disabled"/></td>
                                                    <td><input id="Text51" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text64" type ="text" disabled="disabled"/></td>
                                                </tr>
                                               <tr>
                                                    <td>
                                                        <select id="Select1" disabled="disabled"></select>
                                                    </td>
                                                    <td><input id="Text52" type ="text"disabled="disabled"/></td>
                                                    <td><input id="Text53" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text54" type ="text" disabled="disabled"/></td>
                                                </tr>
                                               <tr>
                                                    <td>
                                                        <select id="Select2" disabled="disabled"></select>
                                                    </td>
                                                    <td><input id="Text55" type ="text"disabled="disabled"/></td>
                                                    <td><input id="Text56" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text57" type ="text" disabled="disabled"/></td>
                                                </tr>
                                               <tr>
                                                    <td>
                                                        <select id="Select3" disabled="disabled"></select>
                                                    </td>
                                                    <td><input id="Text65" type ="text"disabled="disabled"/></td>
                                                    <td><input id="Text67" type ="text" disabled="disabled"/></td>
                                                    <td><input id="Text68" type ="text" disabled="disabled"/></td>
                                                </tr>
                                        </table>
                                        <input type="hidden" id="Hidden1" />
<input type="hidden" id="Hidden2" />
                                    </div>
                                </div>
                               
         
          
       </div>
       <div class="divLeftBig centerAlign" style="margin-left: 5px; margin-bottom: 0px; width: 365px" >
           
           
           
           
                                            <span style="font-weight: bold; font-family: verdana;">CONDITIONS [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="Select126" class="widthSize100_per comboSizeH_24" >
                                                                                                <option value="-1">Select</option>
                                                                                                <option value="-1"><section>All Off Us Exposured to be Matched with CIB</section></option>
                                                                                             </select></td>
                                                    <td>
                                                        <input id="Checkbox1" type="checkbox" />
                                                    </td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select4" class="widthSize100_per comboSizeH_24" >
                                                                                                <option value="-1">Select</option>
                                                                                                <option value="-1"><section>All Off Us Exposured to be Matched with CIB</section></option>
                                                                                             </select></td>
                                                    <td>
                                                        <input id="Checkbox2" type="checkbox" />
                                                    </td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select6" class="widthSize100_per comboSizeH_24" >
                                                                                                <option value="-1">Select</option>
                                                                                                <option value="-1"><section>All Off Us Exposured to be Matched with CIB</section></option>
                                                                                             </select></td>
                                                    <td>
                                                        <input id="Checkbox3" type="checkbox" />
                                                    </td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select7" class="widthSize100_per comboSizeH_24" >
                                                                                                <option value="-1">Select</option>
                                                                                                <option value="-1"><section>All Off Us Exposured to be Matched with CIB</section></option>
                                                                                             </select></td>
                                                    <td>
                                                        <input id="Checkbox4" type="checkbox" />
                                                    </td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select8" class="widthSize100_per comboSizeH_24" >
                                                                                                <option value="-1">Select</option>
                                                                                                <option value="-1"><section>All Off Us Exposured to be Matched with CIB</section></option>
                                                                                             </select></td>
                                                    <td>
                                                        <input id="Checkbox5" type="checkbox" />
                                                    </td>
                                               </tr>
                                               
                                        </table>
                                        
          <div class="clear"></div>
                                      
          </div>  
          
          <br/>
        <div class="divLeftBig centerAlign" style="margin-left: 5px; margin-bottom: 0px; margin-top: 10px; width: 365px" >
           
             
           
                                            
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td class="leftAlign widthSize35_per" style="padding-left: 10px;">ANLYST REMARKS</td>
                                                    <td style ="padding-left: 2px;padding-right: 2px;">
                                                        <textarea id="TextArea1" cols="20" class ="txtBoxSize_H22 widthSize100_per" rows="10" style="height: 105px;*height: 105px;_height: 105px;"></textarea></td>
                                               </tr>
                                           </table>
                                        <div class="clear"></div>
       </div>
                                         
          <div class="clear"></div>
                                        <br/>
                                        <div class="divLeftBig" style="width :370px;*width :370px;_width :370px; margin: 0px; ">
                                            <span style="font-weight: bold; font-family: verdana;">STRENGTH [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="Select116" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select117" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select118" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select119" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select120" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                        </table>
                                        <div class="clear"></div>
                                         </div>

                                        <div class="divLeftBig" style="width :370px;*width :370px;_width :365px; margin: 0px;margin-left: 5px; ">
                                            <span style="font-weight: bold; font-family: verdana;">WEAKNESS [+]</span> 
                                            <table class="tableStyleTiny" style=" border: 1px solid gray;" >
                                               <tr>
                                                   <td><select id="Select121" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select122" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select123" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                               <tr>
                                                   <td><select id="Select124" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                                <tr>
                                                   <td><select id="Select125" class="widthSize100_per comboSizeH_24" ><option value="-1">Select</option>
                                                                                             </select></td>
                                               </tr>
                                        </table>
                                        <div class="clear"></div>
                                         </div>
                                         <div class="clear"></div>
                                         <br/>
        
        
        
        <div class="divLeftBig leftAlign" style="margin-left: 5px; margin-bottom: 0px; width: 750px;*width: 750px;_width: 750px;" >
           
           
           
           
                                            <span style="font-weight: bold; font-family: verdana;" >APPROVER COMMENTS:</span> 
                                            
                                        <textarea id="TextArea2" cols="20" class ="txtBoxSize_H22 widthSize100_per" rows="20" style="height: 150px;*height: 150px;_height: 150px;"></textarea>
          <div class="clear"></div>
                                      
          </div> 
        
        <div class="divLeftBig centerAlign" style="margin-left: 5px; margin-bottom: 0px; width: 750px;*width: 750px;_width: 750px;" >
            <input id="btnApprove" type="button" value="Approver" />&nbsp;&nbsp;<input id="btnApproveWithCondition" type="button" value="Approve With Condition" />&nbsp;&nbsp;<input id="btnReject" type="button" value="Reject" />&nbsp;&nbsp;<input id="btnDecline" type="button" value="Decline" />&nbsp;&nbsp;<input id="btnCancel" type="button" value="Cancel" />
           <div class="clear"></div>
                                      
          </div> 
        
     </div>
     
       <div class = "clear"></div>

    </form>
</body>
</html>
