﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{

 public partial class UpdateLoanStatus : System.Web.UI.Page
    {
        private UpdateLoanStatusManager updateLoanStatusManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            updateButton.Visible = false;
            updateLoanStatusManager=new UpdateLoanStatusManager();
            if (!IsPostBack)
            {
                LoadUpdateStatus();
            }
        }

        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2,
            DELETE = 4,
            DELETEERROR = -4
        }
        #endregion

        private void LoadUpdateStatus()
        {
            var checkListObj = updateLoanStatusManager.GetAllStatus();
            StatusGridView.DataSource = checkListObj;
            StatusGridView.DataBind();
        }
        protected void updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                int updateRow = -1;
                BusinessEntities.UpdateStatus updateStatus = new BusinessEntities.UpdateStatus();
                if (String.IsNullOrEmpty(statusTextBox.Text.Trim()))
                {
                    errorMsgLabel.Text = "You Must Enter Status Name";
                }
                else
                {
                    updateStatus.LOAN_STATUS_ID = Convert.ToInt32(idHiddenField.Value);
                    updateStatus.LOAN_STATUS_NAME = statusTextBox.Text.Trim();
                    updateStatus.UserId = Convert.ToInt32(Session["Id"].ToString());
                    updateRow = updateLoanStatusManager.UpdateStatus(updateStatus);

                    if (updateRow.Equals((int)InsertUpdateFlag.UPDATE))
                    {
                        errorMsgLabel.Text = "Data update sucessfully.";
                        ClearField();
                        LoadUpdateStatus();
                        updateButton.Visible = false;
                    }
                    else if (updateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                    {
                        errorMsgLabel.Text = "Data not update sucessfully.";
                    }
                }
                new AT(this).AuditAndTraial("Update Status Information", errorMsgLabel.Text);

            }
            catch (Exception exception)
            {

                CustomException.Save(exception, "Save Update Status");
            }
        }

        public void ClearField()
        {
            idHiddenField.Value = "0";
            statusTextBox.Text = "";
            errorMsgLabel.Text = "";

        }

        protected void StatusGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateButton.Visible = true;
            int index = StatusGridView.SelectedIndex;
            if (index > -1)
            {
                int rowIndex = Convert.ToInt32(StatusGridView.SelectedRow.RowIndex);
                idHiddenField.Value = (StatusGridView.Rows[rowIndex].Cells[2].FindControl("Id") as Label).Text;
                statusTextBox.Text = (StatusGridView.Rows[rowIndex].Cells[1].FindControl("statusLabel") as Label).Text;
                
            }
        }
    }
}