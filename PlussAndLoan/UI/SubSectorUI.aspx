﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_SubSectorUI" Title="Sub Sector" Codebehind="SubSectorUI.aspx.cs" %>
<asp:Content ID="content" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <div>    
        <table>
            <tr>
                <td colspan="3" align="left" style="padding-left:270px;">
                    <asp:Label ID="Label1" runat="server" Text="Sub-Sector Information" Font-Bold="true" Font-Size="18px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label11" runat="server" Text="Sub-Sector Id:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="subSectorIdTextBox" runat="server" Width="170px" ReadOnly="true"></asp:TextBox>
                    <asp:HiddenField ID="subSectorIdHidden" runat="server" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label2" runat="server" Text="Sector Name:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="sectorNameDropDownList" runat="server" Width="300px">
                    </asp:DropDownList>
                    <asp:Label ID="Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label3" runat="server" Text="Sub Sector Name:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="subSetorNameTextBox" runat="server" Width="300px">
                    </asp:TextBox>
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label4" runat="server" Text="Sector Code:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="sectorCodeTextBox" runat="server" Width="170px">
                    </asp:TextBox>
                    <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label5" runat="server" Text="Profit Margin:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="interestTextBox" runat="server" Width="170px"
                        TextMode="SingleLine"></asp:TextBox>
                        <asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                     <asp:Label ID="codeLabel" runat="server" Text="Code: "></asp:Label>
                    </td>
                <td>
                    <asp:TextBox id="codeTextBox" runat="server" Width="95px">
                    </asp:TextBox>
                    <asp:Label ID="Label12" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                     <asp:Label ID="Label7" runat="server" Text="Color: "></asp:Label>
                    </td>
                <td>
                    <asp:DropDownList id="colorDownList" runat="server" Width="95px">
                        <asp:ListItem>Green</asp:ListItem>
                        <asp:ListItem>Indigo</asp:ListItem>
                        <asp:ListItem>Amber</asp:ListItem>
                        <asp:ListItem>Red</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                     <asp:Label ID="Label9" runat="server" Text="Status: "></asp:Label>
                    </td>
                <td>
                    <asp:DropDownList id="statusDropDownList" runat="server" Width="95px">
                        <asp:ListItem >Active</asp:ListItem>
                        <asp:ListItem>Inactive</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                <asp:Button ID="saveButton" runat="server" Text="Save" Width="100px" Height="27px" 
                        onclick="saveButton_Click" />
                <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" 
                        Height="27px" onclick="clearButton_Click" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
               <td style="padding-left:15px">
                 <asp:TextBox ID="searchTextBox" runat="server" Width="150px"></asp:TextBox>
                    <asp:Button ID="searchbutton" runat="server" Text="Search" Width="100px" 
                       onclick="searchbutton_Click" />
                    </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td style="padding-left:90px;">
                                <asp:FileUpload ID="FileUpload" runat="server" Height="20px" />
                                <asp:Button ID="uploadButton" runat="server" Text="Upload" Width="100px" 
                                    Height="20px" onclick="uploadButton_Click" />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="center" style="padding-left:5px">
                    <div id="gridbox"  style="background-color:white; width:795px; height:220px;"></div>
                </td>
            </tr>
        </table>    
         <div style="display: none" runat="server" id="subSectorDiv"></div>
    </div>
        <script type="text/javascript">
	    var mygrid;
	    mygrid = new dhtmlXGridObject('gridbox');
	    mygrid.setImagePath("../codebase/imgs/");
	    mygrid.setHeader("Id,'',Sector Name,Sub Sector Name,Sector Code,Profit Margin,Code,Color,Status");
	    mygrid.setInitWidths("28,0,170,170,110,90,80,80,50");
	    mygrid.setColAlign("right,left,left,left,left,left,left,left,left");
	    mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro");
	    mygrid.setColSorting("int,str,str,str,str,str,str,str,str");
	    //mygrid.setSkin("modern");
	    mygrid.setSkin("light");	 
	    mygrid.init();
	    mygrid.enableSmartRendering(true,20);
            //mygrid.loadXML("../includes/SubSectorList.xml");
	    mygrid.parse(document.getElementById("xml_data"));
	    mygrid.attachEvent("onRowSelect",doOnRowSelected);
        function doOnRowSelected(id)
        {
            if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0)
            {
              document.getElementById('ctl00_ContentPlaceHolder_saveButton').value="Update";
              document.getElementById('ctl00_ContentPlaceHolder_subSectorIdHidden').value = mygrid.cells(mygrid.getSelectedId(),0).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_subSectorIdTextBox').value = mygrid.cells(mygrid.getSelectedId(),0).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_sectorNameDropDownList').value = parseInt(mygrid.cells(mygrid.getSelectedId(),1).getValue());
              document.getElementById('ctl00_ContentPlaceHolder_subSetorNameTextBox').value=mygrid.cells(mygrid.getSelectedId(),3).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_sectorCodeTextBox').value=mygrid.cells(mygrid.getSelectedId(),4).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_interestTextBox').value=mygrid.cells(mygrid.getSelectedId(),5).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_codeTextBox').value=mygrid.cells(mygrid.getSelectedId(),6).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_colorDownList').value=mygrid.cells(mygrid.getSelectedId(),7).getValue();
              document.getElementById('ctl00_ContentPlaceHolder_statusDropDownList').value=mygrid.cells(mygrid.getSelectedId(),8).getValue();
             }
             else
             {
              document.getElementById('ctl00_ContentPlaceHolder_saveButton').value="Save";
             }
        }
       function LoadExcelData()
        {
            mygrid.clearAll();
            mygrid.loadXML("../includes/SubSectorList.xml");
        }
	</script>
</asp:Content>

