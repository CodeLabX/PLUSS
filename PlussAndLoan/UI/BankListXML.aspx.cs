﻿using System;
using System.Collections.Generic;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_BankList : System.Web.UI.Page
    {
        public BankStatementManager bankStatementManagerObj = null;
        public BankStatement bankStatementObj = null;
        public ApplicantOtherBankAccount otherBankAccountObj = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            otherBankAccountObj=new ApplicantOtherBankAccount();
            bankStatementManagerObj = new BankStatementManager();
            bankStatementObj = new BankStatement();
            if (!Page.IsPostBack)
            {

                if (Request.QueryString.Count != 0)
                {
                    Int64 LLId = Convert.ToInt64(Request.QueryString["LLId"]);
                    LoadBankStatement(LLId);
                }
            }

        }
        private void LoadBankStatement(Int64 LLId)
        {
            List<BankNameList> otherBankAccountObjList = new List<BankNameList>();
            otherBankAccountObj = bankStatementManagerObj.SelectLoanApplicationOthreBankInfo(LLId);
            if (otherBankAccountObj != null)
            {
                if (otherBankAccountObj.OtherBankId1 > 0)
                {
                    BankNameList bankNameList1Obj = new BankNameList();
                    bankNameList1Obj.OtherBankId = otherBankAccountObj.OtherBankId1;
                    bankNameList1Obj.OtherBank = otherBankAccountObj.OtherBank1.ToString();
                    bankNameList1Obj.OtherBranchId = otherBankAccountObj.OtherBranchId1;
                    bankNameList1Obj.OtherBranch = otherBankAccountObj.OtherBranch1;
                    bankNameList1Obj.OtherAccNo = otherBankAccountObj.OtherAccNo1.ToString();
                    bankNameList1Obj.OtherAccType = otherBankAccountObj.OtherAccType1.ToString();
                    otherBankAccountObjList.Add(bankNameList1Obj);
                }
                if (otherBankAccountObj.OtherBankId2 > 0)
                {
                    BankNameList bankNameList2Obj = new BankNameList();
                    bankNameList2Obj.OtherBankId = otherBankAccountObj.OtherBankId2;
                    bankNameList2Obj.OtherBank = otherBankAccountObj.OtherBank2.ToString();
                    bankNameList2Obj.OtherBranchId = otherBankAccountObj.OtherBranchId2;
                    bankNameList2Obj.OtherBranch = otherBankAccountObj.OtherBranch2;
                    bankNameList2Obj.OtherAccNo = otherBankAccountObj.OtherAccNo2.ToString();
                    bankNameList2Obj.OtherAccType = otherBankAccountObj.OtherAccType2.ToString();
                    otherBankAccountObjList.Add(bankNameList2Obj);
                }
                if (otherBankAccountObj.OtherBankId3 > 0)
                {
                    BankNameList bankNameList3Obj = new BankNameList();
                    bankNameList3Obj.OtherBankId = otherBankAccountObj.OtherBankId3;
                    bankNameList3Obj.OtherBank = otherBankAccountObj.OtherBank3.ToString();
                    bankNameList3Obj.OtherBranchId = otherBankAccountObj.OtherBranchId3;
                    bankNameList3Obj.OtherBranch = otherBankAccountObj.OtherBranch3;
                    bankNameList3Obj.OtherAccNo = otherBankAccountObj.OtherAccNo3.ToString();
                    bankNameList3Obj.OtherAccType = otherBankAccountObj.OtherAccType3.ToString();
                    otherBankAccountObjList.Add(bankNameList3Obj);
                }

                if (otherBankAccountObj.OtherBankId4 > 0)
                {
                    BankNameList bankNameList4Obj = new BankNameList();
                    bankNameList4Obj.OtherBankId = otherBankAccountObj.OtherBankId4;
                    bankNameList4Obj.OtherBank = otherBankAccountObj.OtherBank4.ToString();
                    bankNameList4Obj.OtherBranchId = otherBankAccountObj.OtherBranchId4;
                    bankNameList4Obj.OtherBranch = otherBankAccountObj.OtherBranch4;
                    bankNameList4Obj.OtherAccNo = otherBankAccountObj.OtherAccNo4.ToString();
                    bankNameList4Obj.OtherAccType = otherBankAccountObj.OtherAccType4.ToString();
                    otherBankAccountObjList.Add(bankNameList4Obj);
                }
                if (otherBankAccountObj.OtherBankId5 > 0)
                {
                    BankNameList bankNameList5Obj = new BankNameList();
                    bankNameList5Obj.OtherBankId = otherBankAccountObj.OtherBankId5;
                    bankNameList5Obj.OtherBank = otherBankAccountObj.OtherBank5.ToString();
                    bankNameList5Obj.OtherBranchId = otherBankAccountObj.OtherBranchId5;
                    bankNameList5Obj.OtherBranch = otherBankAccountObj.OtherBranch5;
                    bankNameList5Obj.OtherAccNo = otherBankAccountObj.OtherAccNo5.ToString();
                    bankNameList5Obj.OtherAccType = otherBankAccountObj.OtherAccType5.ToString();
                    otherBankAccountObjList.Add(bankNameList5Obj);
                }

            }
            int rowIndex = 1;
            Response.ContentType = "text/xml";
            Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>");
            Response.Write("<rows>");
            if (otherBankAccountObjList.Count > 0)
            {
                try
                {
                    foreach (BankNameList bankNameObj in otherBankAccountObjList)
                    {
                        Response.Write("<row id='" + rowIndex.ToString() + "'>");
                        Response.Write("<cell>");
                        Response.Write(rowIndex.ToString());
                        Response.Write("</cell>");
                        Response.Write("<cell>");
                        Response.Write(bankNameObj.OtherBankId.ToString());
                        Response.Write("</cell>");
                        Response.Write("<cell>");
                        Response.Write(bankNameObj.OtherBank.ToString());
                        Response.Write("</cell>");
                        Response.Write("<cell>");
                        Response.Write(bankNameObj.OtherBranchId.ToString());
                        Response.Write("</cell>");
                        Response.Write("<cell>");
                        Response.Write(bankNameObj.OtherBranch.ToString());
                        Response.Write("</cell>");
                        Response.Write("<cell>");
                        Response.Write(bankNameObj.OtherAccNo.ToString());
                        Response.Write("</cell>");                    
                        Response.Write("</row>");
                        rowIndex = rowIndex + 1;
                    }
                }
                catch(Exception ex)
                {
                }
            }
            else
            {
            }
            Response.Write("</rows>");
        }

        public string EndMonth(string startMonth)
        {
            string endMonth = null;
            DateTime getDate = DateTime.Parse("01-" + startMonth);
            int month = getDate.Month;
            for (int i = 0; i < 12; i++)
            {
                endMonth = getDate.AddMonths(i).ToString("MMMM-yyyy");
            }
            return endMonth;
        }
    }
}
