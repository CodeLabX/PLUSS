﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BLL;
using BusinessEntities;
using BusinessEntities.Report;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BSVERBankList : System.Web.UI.Page
    {
        public ApplicantOtherBankAccount otherBankAccountObj = null;
        public BankStatementManager bankStatementManagerObj = null;
        public BSVERManager bSVERManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                otherBankAccountObj = new ApplicantOtherBankAccount();
                bankStatementManagerObj = new BankStatementManager();
                bSVERManagerObj = new BSVERManager();
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString.Count != 0)
                    {
                        Int64 LLId = Convert.ToInt64(Request.QueryString["BsvrLLId"]);
                        LoadBankNameList(LLId);
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Load Bank Name List");  
            }
        }
        private void LoadBankNameList(Int64 LLId)
        {
            int ApplicationStatus = 0;
            string entryUserName = "";
            List<BankNameList> otherBankAccountObjList = new List<BankNameList>();
            otherBankAccountObj = bankStatementManagerObj.SelectLoanApplicationOthreBankInfo(LLId);
            if (otherBankAccountObj != null)
            {
                if (otherBankAccountObj.OtherBankId1 > 0)
                {
                    BankNameList bankNameList1Obj = new BankNameList();
                    bankNameList1Obj.OtherBankId = otherBankAccountObj.OtherBankId1;
                    bankNameList1Obj.OtherBank = otherBankAccountObj.OtherBank1.ToString();
                    bankNameList1Obj.OtherBranchId = otherBankAccountObj.OtherBranchId1;
                    bankNameList1Obj.OtherBranch = otherBankAccountObj.OtherBranch1;
                    bankNameList1Obj.OtherAccNo = otherBankAccountObj.OtherAccNo1.ToString();
                    bankNameList1Obj.OtherAccType = otherBankAccountObj.OtherAccType1.ToString();
                    otherBankAccountObjList.Add(bankNameList1Obj);
                }
                if (otherBankAccountObj.OtherBankId2 > 0)
                {
                    BankNameList bankNameList2Obj = new BankNameList();
                    bankNameList2Obj.OtherBankId = otherBankAccountObj.OtherBankId2;
                    bankNameList2Obj.OtherBank = otherBankAccountObj.OtherBank2.ToString();
                    bankNameList2Obj.OtherBranchId = otherBankAccountObj.OtherBranchId2;
                    bankNameList2Obj.OtherBranch = otherBankAccountObj.OtherBranch2;
                    bankNameList2Obj.OtherAccNo = otherBankAccountObj.OtherAccNo2.ToString();
                    bankNameList2Obj.OtherAccType = otherBankAccountObj.OtherAccType2.ToString();
                    otherBankAccountObjList.Add(bankNameList2Obj);
                }
                if (otherBankAccountObj.OtherBankId3 > 0)
                {
                    BankNameList bankNameList3Obj = new BankNameList();
                    bankNameList3Obj.OtherBankId = otherBankAccountObj.OtherBankId3;
                    bankNameList3Obj.OtherBank = otherBankAccountObj.OtherBank3.ToString();
                    bankNameList3Obj.OtherBranchId = otherBankAccountObj.OtherBranchId3;
                    bankNameList3Obj.OtherBranch = otherBankAccountObj.OtherBranch3;
                    bankNameList3Obj.OtherAccNo = otherBankAccountObj.OtherAccNo3.ToString();
                    bankNameList3Obj.OtherAccType = otherBankAccountObj.OtherAccType3.ToString();
                    otherBankAccountObjList.Add(bankNameList3Obj);
                }

                if (otherBankAccountObj.OtherBankId4 > 0)
                {
                    BankNameList bankNameList4Obj = new BankNameList();
                    bankNameList4Obj.OtherBankId = otherBankAccountObj.OtherBankId4;
                    bankNameList4Obj.OtherBank = otherBankAccountObj.OtherBank4.ToString();
                    bankNameList4Obj.OtherBranchId = otherBankAccountObj.OtherBranchId4;
                    bankNameList4Obj.OtherBranch = otherBankAccountObj.OtherBranch4;
                    bankNameList4Obj.OtherAccNo = otherBankAccountObj.OtherAccNo4.ToString();
                    bankNameList4Obj.OtherAccType = otherBankAccountObj.OtherAccType4.ToString();
                    otherBankAccountObjList.Add(bankNameList4Obj);
                }
                if (otherBankAccountObj.OtherBankId5 > 0)
                {
                    BankNameList bankNameList5Obj = new BankNameList();
                    bankNameList5Obj.OtherBankId = otherBankAccountObj.OtherBankId5;
                    bankNameList5Obj.OtherBank = otherBankAccountObj.OtherBank5.ToString();
                    bankNameList5Obj.OtherBranchId = otherBankAccountObj.OtherBranchId5;
                    bankNameList5Obj.OtherBranch = otherBankAccountObj.OtherBranch5;
                    bankNameList5Obj.OtherAccNo = otherBankAccountObj.OtherAccNo5.ToString();
                    bankNameList5Obj.OtherAccType = otherBankAccountObj.OtherAccType5.ToString();
                    otherBankAccountObjList.Add(bankNameList5Obj);
                }
                ApplicationStatus = otherBankAccountObj.ApplicationStatus;
                entryUserName = bankStatementManagerObj.GetLLIdEntryUserId(LLId);
            }
            Response.Write(otherBankAccountObjList.Count.ToString() + "," + ApplicationStatus + "," + entryUserName);

           
            int rowIndex = 1;
            //Response.ContentType = "text/xml";
            //StringBuilder xml = new StringBuilder();
            //xml.AppendLine("<?xml version='1.0' encoding='UTF-8'?>");
            //xml.AppendLine("<rows>");
            //if (otherBankAccountObjList.Count > 0)
            //{
            //    try
            //    {
            //        foreach (BankNameList bankNameObj in otherBankAccountObjList)
            //        {
            //             xml.AppendLine("<row id='" + rowIndex.ToString() + "'>") ;
            //             xml.AppendLine("<cell>");
            //            xml.AppendLine(rowIndex.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(bankNameObj.OtherBankId.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(bankNameObj.OtherBank.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(bankNameObj.OtherBranchId.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(bankNameObj.OtherBranch.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(bankNameObj.OtherAccNo.ToString());
            //            xml.AppendLine("</cell>");
            //            BASVERReportInfo bASVERReportInfoObj = new BASVERReportInfo();
            //            string statementTypeId = "";
            //            string statementType = "";
            //            string subjectId = "";
            //            string subjectName = "";

            //            bASVERReportInfoObj = bSVERManagerObj.SelectBASVERReportInfo(LLId,(Int16)bankNameObj.OtherBankId, (Int16)bankNameObj.OtherBranchId, bankNameObj.OtherAccNo);
            //            if (bASVERReportInfoObj != null)
            //            {
            //                statementTypeId = bASVERReportInfoObj.StatementType_Id.ToString();
            //                switch(statementTypeId)
            //                {
            //                    case "1":
            //                        statementType = "Document";
            //                        break;
            //                    case "2":
            //                        statementType = "Email";
            //                        break;
            //                }
            //                subjectId = bASVERReportInfoObj.Subject_Id.ToString();
            //                subjectName = bASVERReportInfoObj.SubjectName.ToString();
            //            }
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(statementTypeId.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(subjectId.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(AddSlash(subjectName.ToString()));
            //            xml.AppendLine("</cell>");
            //            int AccountTypeId = 0;
            //            switch (bankNameObj.OtherAccType.Trim().ToUpper().Replace(" ", ""))
            //            {
            //                case "CURRENT":
            //                    AccountTypeId = 1;
            //                    break;
            //                case "SAVING":
            //                    AccountTypeId = 2;
            //                    break;
            //                case "CALL":
            //                    AccountTypeId = 3;
            //                    break;
            //                case "OD":
            //                    AccountTypeId = 4;
            //                    break;
            //                case "SOD":
            //                    AccountTypeId = 5;
            //                    break;
            //                case "SALARY":
            //                    AccountTypeId = 7;
            //                    break;
            //                case "Others":
            //                    AccountTypeId = 6;
            //                    break;
            //                default:
            //                    AccountTypeId = 6;
            //                    break;
            //            }
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(AccountTypeId.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("<cell>");
            //            xml.AppendLine(LLId.ToString());
            //            xml.AppendLine("</cell>");
            //            xml.AppendLine("</row>");
            //            rowIndex = rowIndex + 1;
            //        }
            //        xml.AppendLine("</rows>");
            //    }
            //    finally
            //    {
            //    }
            //}
            //else
            //{
            //    xml.AppendLine("</rows>");
            //}
            //Response.Write(xml.ToString());

            var xml = "<xml id='xml_data'><rows>";
            if (otherBankAccountObjList.Count > 0)
            {
                try
                {
                    foreach (BankNameList bankNameObj in otherBankAccountObjList)
                    {
                        xml += "<row id='" + rowIndex.ToString() + "'>";
                        xml += "<cell>";
                        xml += rowIndex.ToString();
                        xml += "</cell>";
                        xml += "<cell>";
                        xml += bankNameObj.OtherBankId.ToString();
                        xml += "</cell>";
                        xml += "<cell>";
                        xml += bankNameObj.OtherBank.ToString();
                        xml += "</cell>";
                        xml += "<cell>";
                        xml += bankNameObj.OtherBranchId.ToString();
                        xml += "</cell>";
                        xml += "<cell>";
                        xml += bankNameObj.OtherBranch.ToString();
                        xml += "</cell>";
                        xml += "<cell>";
                        xml += bankNameObj.OtherAccNo.ToString();
                        xml += "</cell>";
                        BASVERReportInfo bASVERReportInfoObj = new BASVERReportInfo();
                        string statementTypeId = "";
                        string statementType = "";
                        string subjectId = "";
                        string subjectName = "";

                        bASVERReportInfoObj = bSVERManagerObj.SelectBASVERReportInfo(LLId, (Int16)bankNameObj.OtherBankId, (Int16)bankNameObj.OtherBranchId, bankNameObj.OtherAccNo);
                        if (bASVERReportInfoObj != null)
                        {
                            statementTypeId = bASVERReportInfoObj.StatementType_Id.ToString();
                            switch (statementTypeId)
                            {
                                case "1":
                                    statementType = "Document";
                                    break;
                                case "2":
                                    statementType = "Email";
                                    break;
                            }
                            subjectId = bASVERReportInfoObj.Subject_Id.ToString();
                            subjectName = bASVERReportInfoObj.SubjectName.ToString();
                        }
                        xml += "<cell>";
                        xml += statementTypeId.ToString();
                        xml += "</cell>";
                        xml += "<cell>";
                        xml += subjectId.ToString();
                        xml += "</cell>";
                        xml += "<cell>";
                        xml += AddSlash(subjectName.ToString());
                        xml += "</cell>";
                        int AccountTypeId = 0;
                        switch (bankNameObj.OtherAccType.Trim().ToUpper().Replace(" ", ""))
                        {
                            case "CURRENT":
                                AccountTypeId = 1;
                                break;
                            case "SAVING":
                                AccountTypeId = 2;
                                break;
                            case "CALL":
                                AccountTypeId = 3;
                                break;
                            case "OD":
                                AccountTypeId = 4;
                                break;
                            case "SOD":
                                AccountTypeId = 5;
                                break;
                            case "SALARY":
                                AccountTypeId = 7;
                                break;
                            case "Others":
                                AccountTypeId = 6;
                                break;
                            default:
                                AccountTypeId = 6;
                                break;
                        }
                        xml += "<cell>";
                        xml += AccountTypeId.ToString();
                        xml += "</cell>";
                        xml += "<cell>";
                        xml += LLId.ToString();
                        xml += "</cell>";
                        xml += "</row>";
                        rowIndex = rowIndex + 1;
                    }
                    xml += "</rows></xml>";
                    //div.InnerHtml = xml;
                    Response.Write(xml.ToString());
                }
                finally
                {
                }
            }
            else
            {
                xml += "</rows></xml>";
            }
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
