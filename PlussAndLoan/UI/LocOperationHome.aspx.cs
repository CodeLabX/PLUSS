﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI
{

    public partial class Home : System.Web.UI.Page
    {
        readonly LocatorApplicationService _service = new LocatorApplicationService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadProduct();
                LoadLoanState();
                LoadGrid("");
            }
        }

        private void LoadLoanState()
        {
            var loanApplicationManagerObj = new LoanApplicationManager();
            var dt = loanApplicationManagerObj.GetLoanStates();
            var loanStates = DataTableToList.GetList<Loan>(dt);
            var state = new Loan() { LoanState = 0, StateName = "" };
            loanStates.Insert(0, state);
            sel_loanStatus.DataTextField = "StateName";
            sel_loanStatus.DataValueField = "LoanState";
            sel_loanStatus.DataSource = loanStates;
            sel_loanStatus.DataBind();
        }

        private void LoadProduct()
        {
            List<Product> productListObj = new LoanApplicationManager().GetAllProduct();
            var productObj = new Product { ProductId = 0, ProductName = "" };
            productListObj.Insert(0, productObj);
            findByProduct.DataTextField = "ProductName";
            findByProduct.DataValueField = "ProductId";
            findByProduct.DataSource = productListObj;
            findByProduct.DataBind();
        }

        private void LoadGrid(string filter)
        {

            DateTime dateTime = Convert.ToDateTime(WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"]);
            var dtTmpList = _service.GetLoanApplications(dateTime, 0,filter);
            var total = _service.GetCountOfLoans();
            totalLoan.Text = string.Format("Total {0} Loan Applications", total);
            gvData.DataSource = dtTmpList;
            gvData.DataBind();
        }


        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvData.PageIndex = e.NewPageIndex;
            LoadGrid(GridFilterBy());
        }

        protected void Edit(object sender, EventArgs e)
        {
            ////
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGrid(GridFilterBy());
        }
        private string GridFilterBy()
        {
            var filterBy = "";
            var llid = findById.Text;
            var productId = findByProduct.SelectedValue;
            var acc = findAcBy.Text;
            var customer = findByCustomer.Text;
            var state = sel_loanStatus.SelectedValue;
            if (!string.IsNullOrEmpty(llid))
            {
                filterBy += string.Format(" where l.LOAN_APPL_ID={0}", llid);
            }
            if (productId != "0")
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.PROD_ID={0}", productId);
                }
                else
                {
                    filterBy += string.Format(" and l.PROD_ID={0}", productId);
                }

            }
            if (!string.IsNullOrEmpty(acc))
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.ACC_NUMB like '%{0}%'", acc);
                }
                else
                {
                    filterBy += string.Format(" and l.ACC_NUMB like '%{0}%'", acc);
                }

            }
            if (!string.IsNullOrEmpty(customer))
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.CUST_NAME like '%{0}%'", customer);
                }
                else
                {
                    filterBy += string.Format(" and l.CUST_NAME like '%{0}%'", customer);
                }

            }
            if (state != "0")
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.LOAN_STATUS_ID='{0}'", state);
                }
                else
                {
                    filterBy += string.Format(" and l.LOAN_STATUS_ID='{0}'", state);
                }

            }
            return filterBy;
        }
        protected void refresh_Click(object sender, EventArgs e)
        {
            findById.Text = "";
            findByProduct.SelectedValue = "0";
            findAcBy.Text = "";
            findByCustomer.Text = "";
            LoadGrid("");
        }
    }
}