﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LevelWiseCapping.aspx.cs" Inherits="PlussAndLoan.UI.LevelWiseCapping" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="ShowReportDiv" runat="server">
            <div id="viewDiv">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="100%" align="center" valign="top">
                            <table width="780" border="0" cellspacing="0" cellpadding="0">
                                <tr align="left" valign="top">
                                    <td height="47" background="image/blueline.gif">
                                        <img src="images/blueline.gif" width="554" height="47"></td>
                                    <td width="226" height="47">
                                        <img src="images/logo.gif" width="226" height="47"></td>
                                </tr>
                            </table>
                            <table width="780" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="780" height="47" align="left" valign="middle">
                                        <div align="left">
                                            <font color="#333333" size="2" face="Verdana, Arial, Helvetica, sans-serif"><img src="images/ltopbg.jpg" width="450" height="47"></font>
                                        </div>
                                        <div align="left"></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="0">
                    <tr>
                        <td width="31%" height="20" align="left"><font size="4" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font>
                        </td>
                        <td width="36%" align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Level 
      wise Capping</font></strong>
                            <br/>
                            <asp:Label runat="server" ID="ProdName"></asp:Label>
                            <br>
                            <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Period :<asp:Label runat="server" ID="divDate"></asp:Label>
      </font></td>
                        <td width="33%" height="20" align="left" valign="middle" bgcolor="#FFFFFF">
                            <asp:Button runat="server" Text=" BACK"  style='border: 1px solid #83b67a; background-color: #BDD9A5' OnClick="Back_Click" />
                            <asp:Button runat="server" text=" CLOSE"  style='border: 1px solid #83b67a; background-color: #BDD9A5' OnClick="Close_Click"/>
                            <asp:Button runat="server" text=" PRINT " style='border: 1px solid #83b67a; background-color: #BDD9A5' OnClientClick="javascript:window.print();"/>
                            <%--<input type="button" name="Submit1" value=" BACK" onclick="history.go(-1)" style='border: 1px solid #83b67a; background-color: #BDD9A5'>--%>
                            <%--<input type="button" name="Submit1" value=" CLOSE" onclick="window.close();" style='border: 1px solid #83b67a; background-color: #BDD9A5'>
                            <input type="button" name="Submit1" value=" PRINT " onclick="window.print();" style='border: 1px solid #83b67a; background-color: #BDD9A5'>--%>
                        </td>
                    </tr>
                </table>
                <br />
                <div id="divProdWiseCappingReport" style="overflow-y: scroll; height: 380px; width: 100%;" class="GridviewBorder">
                    <asp:Table runat="server" width="80%" border="1" align="center" cellpadding="0" cellspacing="0" bgcolor="#ccffcc" ID="LevelReport">
                         <asp:TableRow Height="43">
                              <asp:TableCell height="43" width="80" >&nbsp;</asp:TableCell>
                              <asp:TableCell height="43" width="80" >Amount</asp:TableCell>
                              <asp:TableCell height="43" width="100" >% of Total</asp:TableCell>
                              <asp:TableCell height="43" width="100" >Capping %</asp:TableCell>
                              <asp:TableCell height="43" width="150" >Space Avialable</asp:TableCell>
                         </asp:TableRow>
                         
                    </asp:Table>
                </div>
                 
            </div>
        </div>
    </div>
    </form>
</body>
</html>
