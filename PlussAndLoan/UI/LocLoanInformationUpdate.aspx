﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorAdmin.master" AutoEventWireup="true" CodeBehind="LocLoanInformationUpdate.aspx.cs" Inherits="PlussAndLoan.UI.LocLoanInformationUpdate" %>

<%@ Register Src="~/UI/UserControls/js/TextBoxDatePicker.ascx" TagPrefix="uc1" TagName="TextBoxDatePicker" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <uc1:TextBoxDatePicker runat="server" ID="TextBoxDatePicker" />
    
    <script type="text/javascript">
        function autotab(original, destination) {
            if (original.getAttribute && original.value.length == original.getAttribute("maxlength")) {
                document.getElementById('ctl00_ContentPlaceHolder_' + destination).focus();
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="LoanAppInfoDiv" runat="server">
        <table >
            <tr>
                <td colspan="3" align="left" style="padding-left: 270px">
                    <asp:Label ID="Label4" runat="server" Text="Update Loan Application Information" Font-Bold="true" Font-Size="18px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="Label5" runat="server" Text="Enter Loan Application Id"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="loanAppIdTextBox" runat="server" Width="231px"></asp:TextBox>
                    <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="GoButton" runat="server" Text="Go" Width="50px" Height="27px" text-align="center" OnClick="GoButton_Click"/>&nbsp;
               
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div id="LoanAppViewDiv" runat="server">
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr>
                <td style="font-size: 13px; font-weight: bold; background-color: Gray; color: White;"
                    colspan="4">LOAN APPLICATION INFORMATION
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="productNameLabel" Text="Product" runat="server"></asp:Label>
                    
                </td>
                <td>
                    <asp:DropDownList ID="productDropDownList" runat="server" Width="235px" OnSelectedIndexChanged="productDropDownList_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    <asp:Label ID="productNameMandatoryLabel" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2">&nbsp;
                </td>
            </tr>
            <tr id="dealersDiv" runat="server">
                <td style="font-weight: bold">
                    <asp:Label ID="Label7" Text="AUTO DEALER'S NAME" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="DealearsTextBox" runat="server" Width="230px"></asp:TextBox>
                    &nbsp;
                </td>
                <td colspan="2">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="dateLabel" Text="APPLICATION DATE" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="dateTextBox" runat="server" Width="230px" MaxLength="12" readonly class="required datePicCal"></asp:TextBox>
                    &nbsp;
                    <asp:Label ID="dateMandatoryLabel" Text="*" runat="server" ForeColor="Red"></asp:Label>
                    &nbsp; (dd-mm-yyyy)
                </td>
                <td colspan="2">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="sourceLabel" Text="ENTRY FROM SOURCE" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="sourceDropDownList" runat="server" Width="235px"></asp:DropDownList>
                    &nbsp;
                    <asp:Label ID="mandatory1" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2" style="font-weight: bold">
                    <%--  <asp:Label ID="sourcePersonNameLabel" Text="Source Person Name" runat="server"></asp:Label>
                    <asp:TextBox ID="sourcePersonTextBox" runat="server"></asp:TextBox>--%>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="Label1" Text="ACCOUNT NUMBER" runat="server"></asp:Label>
                </td>
                <td>
                     <asp:TextBox runat="server" ID="txtPreAcc" Width="60px" MaxLength="2" onKeyup="autotab(this, 'txtMidAcc')"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtMidAcc" Width="100px" MaxLength="7" onKeyup="autotab(this, 'txtPostAcc')"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtPostAcc" Width="60px" MaxLength="2"></asp:TextBox>

                    <%-- <input type="text" id="txtAccPre" style="width: 60px;" runat="server" onkeyup="autoTabMaxLen(this, ctl00_ContentPlaceHolder_txtAccMid)" maxlength="2" min="0" />
                    <input type="text" id="txtAccMid" style="width: 100px;" runat="server" onkeyup="autoTabMaxLen(this, ctl00_ContentPlaceHolder_txtAccLast)" maxlength="7" min="0" />
                    <input type="text" id="txtAccLast" style="width: 60px;" runat="server" min="0" maxlength="2" />--%>
                    &nbsp;
                    <asp:Label ID="Label2" Text="*" runat="server" ForeColor="Red"></asp:Label>
                    <input type="checkbox" id="chkCpf" runat="server" />
                    CPF
                    <input type="checkbox" id="chkPriority" runat="server" />
                    Priority
                </td>
                <td colspan="2" style="font-weight: bold">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;" width="200">
                    <asp:Label ID="customerNameLabel" Text="CUSTOMER NAME" runat="server"></asp:Label>
                </td>
                <td valign="top">
                    <asp:TextBox ID="customerNameTextBox" runat="server" Width="230px"></asp:TextBox>
                    <span style="color: Red;">*</span>
                </td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="appliedAmountLabel" Text="AMOUNT OF LOAN APPLIED" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="appliedAmountTextBox" runat="server" Width="230px"></asp:TextBox>
                    <asp:Label ID="appliedAmountMandatoryLabel" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td colspan="2">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="professionLabel" runat="server" Text="APPLICANT PROFESSION"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="professionDropdownList" runat="server" Width="235px">
                    </asp:DropDownList>
                </td>
                <td>&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="companyNameLabel" Text="ORGANIZATION" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="companyNameTextBox" runat="server" Width="230px"></asp:TextBox>
                </td>
                <td>&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="Decliend" Text="If Previously Decliend ( APP. ID)" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDecliendLoan" Width="230px"></asp:TextBox>
                </td>
                <td>&nbsp;
                </td>
                <td>&nbsp;
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td style="width: 400px" align="center">
                    <asp:Label runat="server" ID="msgLabel"></asp:Label><br />
                    <asp:Button runat="server" Text="Update" ID="btnUpdateLoanApplication" Width="123px" Height="27px" OnClick="btnUpdateLoanApplication_Click"/>
                    &nbsp;
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>
