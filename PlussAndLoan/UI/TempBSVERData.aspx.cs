﻿using System;
using System.Collections.Generic;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_TempBSVERData : System.Web.UI.Page
    {
        public BSVERManager bSVERManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            bSVERManagerObj = new BSVERManager();
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count != 0)
                {
                    Int64 LLId = Convert.ToInt64(Request.QueryString["LLId"]);
                    Int16 bankId = Convert.ToInt16(Request.QueryString["bankId"]);
                    Int16 branchId = Convert.ToInt16(Request.QueryString["branchId"]);
                    string aCNo = Convert.ToString(Request.QueryString["aCNo"]);
                    Int16 subjectId = Convert.ToInt16(Request.QueryString["subjectId"]);
                    Int16 statementTypeId = Convert.ToInt16(Request.QueryString["statemenTypeId"]);

                    LoadBSVERDetails(LLId,statementTypeId, bankId, branchId, aCNo,subjectId);
                }
            }

        }
        private void LoadBSVERDetails(Int64 LLId, Int16 statementTypeId,Int16 bankId, Int16 branchId, string aCNo, Int16 subjectId)
        {
            Int64 bsVerMasterId = 0;
            List<BASVERReportInfo> bASVERDetailsObjList = new List<BASVERReportInfo>();
            if (LLId > 0 && bankId > 0 && branchId > 0 && subjectId > 0 && aCNo != "")
            {
                bsVerMasterId = bSVERManagerObj.SelectBSVerMasterId(LLId, statementTypeId, bankId, branchId,subjectId ,aCNo);
                Session["bsverMasterId"] = bsVerMasterId;
                bASVERDetailsObjList = bSVERManagerObj.SelectBASVERReportInfoList(bsVerMasterId);
            }
            if (bASVERDetailsObjList.Count > 0)
            {
                int i = 0;
                foreach (BASVERReportInfo bSVERDetailsObj in bASVERDetailsObjList)
                {
                    Response.Write(bSVERDetailsObj.Date.ToString("dd-MM-yyyy") + ",");
                    Response.Write(bSVERDetailsObj.Description.ToString() + ",");
                    Response.Write(bSVERDetailsObj.Withdrawl.ToString() + ",");
                    Response.Write(bSVERDetailsObj.Diposit.ToString() + ",");
                    Response.Write(bSVERDetailsObj.Balance.ToString() + ",");
                    Response.Write(bSVERDetailsObj.BasVerDetailsId.ToString() + "#");
                    i = i + 1;
                    if (i == 4)
                    {
                        Response.Write("*");
                        Response.Write(bSVERDetailsObj.SCBEmailAddress.ToString() + ",");
                        Response.Write(bSVERDetailsObj.Approved_Id.ToString() + ",");
                        Response.Write(bSVERDetailsObj.ApproverDesignation.ToString() + ",");
                        Response.Write(bSVERDetailsObj.CustomerAddress.ToString() + ",");
                        Response.Write(bSVERDetailsObj.Dear.ToString() + ",");
                        Response.Write(bSVERDetailsObj.EmailAddress.ToString());
                        break;
                    }
                }
            }
        }
    }
}
