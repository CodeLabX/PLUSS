﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_CIBResultUploadUI : System.Web.UI.Page
    {
        public CIBResultManager cIBResultManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        Int32 rowsPerPage = 500;
        Int32 pageNum = 1;
        Int32 offset = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            cIBResultManagerObj = new CIBResultManager();
            cIBResultManagerObj = new CIBResultManager();
            if (Request.QueryString.Count != 0)
            {
                pageNum = Convert.ToInt32(Request.QueryString["pageIndex"]);
                offset = (pageNum - 1) * rowsPerPage;
            }
            BindData(offset, rowsPerPage);
        }
        private void BindData(Int32 offset, Int32 rowsPerPage)
        {
            List<CIBResult> cIBResultObjList = new List<CIBResult>();
            cIBResultObjList = cIBResultManagerObj.GetCIBResult(offset, rowsPerPage);
            StringBuilder headTable = new StringBuilder();
            StringBuilder viewTable = new StringBuilder();
            StringBuilder pagingTable = new StringBuilder();
            if (cIBResultObjList != null)
            {

                headTable.Append("<table id='headTable' width='100%'>");
                headTable.Append("<tr>");
                headTable.Append("<td><b>SL.</b></td>");
                headTable.Append("<td><b>LLId</b></td>");
                headTable.Append("<td><b>Customer Name</b></td>");
                headTable.Append("<td><b>Status</b></td>");
                headTable.Append("<td><b>Remarks</b></td>");
                headTable.Append("<td><b>Father's Name</b></td>");
                headTable.Append("<td><b>Mother's Name</b></td>");
                headTable.Append("<td><b>Ref Number</b></td>");
                headTable.Append("<td><b>Type</b></td>");
                headTable.Append("</tr>");
                headTable.Append("</table>");
                tableHeadDiv.InnerHtml = headTable.ToString();
                viewTable.Append("<table id='dataTable' class='tableWidth' width='100%'>");
                foreach (CIBResult cIBResultObj in cIBResultObjList)
                {
                    viewTable.Append("<tr>");
                    viewTable.Append("<td>" + cIBResultObj.ID.ToString() + "</td>");
                    viewTable.Append("<td>" + cIBResultObj.LLID.ToString() + "</td>");
                    viewTable.Append("<td>" + cIBResultObj.CustomerName.ToString() + "</td>");
                    viewTable.Append("<td>" + cIBResultObj.Status.ToString() + "</td>");
                    viewTable.Append("<td>" + cIBResultObj.Remarks.ToString() + "</td>");
                    viewTable.Append("<td>" + cIBResultObj.FatherName.ToString() + "</td>");
                    viewTable.Append("<td>" + cIBResultObj.MotherName.ToString() + "</td>");
                    viewTable.Append("<td>" + cIBResultObj.ReferenceNumber.ToString() + "</td>");
                    viewTable.Append("<td>" + cIBResultObj.Type.ToString() + "</td>");
                    viewTable.Append("</tr>");
                }
                viewTable.Append("</table>");
                Int32 numrows = cIBResultManagerObj.GetTolalRecord();
                Int32 maxPage =numrows / rowsPerPage;
                maxPage = maxPage + 1;
                string nav = "";
                string prev = "";
                string first = "";
                string next = "";
                string last = "";
                Int32 page = 0;
                for (page = 1; page <= maxPage; page++)
                {
                    if (page == pageNum)
                    {
                        nav = page.ToString();
                    }
                    else
                    {
                        nav += " <a href=../UI/CIBResultUploadUI.aspx?pageIndex=" + page.ToString() + ">" + page.ToString() + "</a> ";
                    }
                }
                if (pageNum > 1)
                {
                    page = pageNum - 1;
                    prev = " <a href=../UI/CIBResultUploadUI.aspx?pageIndex=" + page.ToString() + ">Prev</a> ";
                    first = " <a href=../UI/CIBResultUploadUI.aspx?pageIndex=1>First Page</a>";
                }
                else
                {
                    prev = "&nbsp;";
                    first = "&nbsp;";
                }

                if (pageNum < maxPage)
                {
                    page = pageNum + 1;
                    next = " <a href=../UI/CIBResultUploadUI.aspx?pageIndex=" + page + ">Next</a> ";

                    last = " <a href=../UI/CIBResultUploadUI.aspx?pageIndex=" + maxPage + ">Last Page</a> ";
                }
                else
                {
                    next = "&nbsp;";
                    last = "&nbsp;";
                }
                pagingTable.Append("<table id='pagingTable' width='100%'>");
                pagingTable.Append("<tr>");
                pagingTable.Append("<td>" + first.ToString() + prev.ToString() + nav.ToString() + next.ToString() + last.ToString() + "</td>");
                pagingTable.Append("</tr>");
                pagingTable.Append("</table>");            
                recordViewDiv.InnerHtml = viewTable.ToString();
                pagingDiv.InnerHtml = pagingTable.ToString();
            

            }
        }
        protected void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string path = Request.PhysicalApplicationPath;
                path += @"Uploads\";
                if (FileUpload.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload.FileName);
                    path += fileName;
                    FileUpload.SaveAs(path);
                    ReadExcleFile(path);
                    new AT(this).AuditAndTraial("CIB Result", "Upload");
                }
                else
                {
                    Alert.Show("Please select Excel file");
                    FileUpload.Focus();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "CIB Result Upload");
            }
        }
        private void ReadExcleFile(string fileName)
        {
            long slNo = 1;
            List<CIBResult> cIBResultObjList = new List<CIBResult>();
            if (fileName != "")
            {
                string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source="
                                                                        + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));

                DataTable dataTableObj = new DataTable();
                OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);
                FileInfo CIBResultListExcel = new FileInfo(Server.MapPath("../includes/CIBResultList.xml"));
                StreamWriter stremWriter = null;
                stremWriter = CIBResultListExcel.CreateText();
                try
                {
                    dataAdapterObj.Fill(dataTableObj);

                    //stremWriter.WriteLine("<?xml version='1.0' encoding='iso-8859-1'?>");
                    //stremWriter.WriteLine("<rows>");
                    foreach (DataRow dataRow in dataTableObj.Rows)
                    {
                        CIBResult cIBResultObj = new CIBResult();
                        //stremWriter.WriteLine("<row id='" + slNo + "'>");
                        //stremWriter.WriteLine("<cell>");
                        //stremWriter.WriteLine(slNo);
                        // stremWriter.WriteLine("</cell>");
                        if (dataRow[0].ToString() != "")
                        {
                            cIBResultObj.LLID = dataRow[0].ToString();
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine(cIBResultObj.LLID.ToString());
                            //stremWriter.WriteLine("</cell>");
                        }
                        else
                        {
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine("");
                            //stremWriter.WriteLine("</cell>");
                            cIBResultObj.LLID = "";
                        }
                        if (dataRow[1].ToString() != "")
                        {
                            cIBResultObj.CustomerName = AddSlash(dataRow[1].ToString());
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine(cIBResultObj.CustomerName.ToString());
                            //stremWriter.WriteLine("</cell>");
                        }
                        else
                        {
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine("");
                            //stremWriter.WriteLine("</cell>");
                            cIBResultObj.CustomerName = "";
                        }
                        if (dataRow[2].ToString() != "")
                        {
                            cIBResultObj.Status = AddSlash(dataRow[2].ToString());
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine(cIBResultObj.Status.ToString());
                            //stremWriter.WriteLine("</cell>");
                        }
                        else
                        {
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine("");
                            //stremWriter.WriteLine("</cell>");
                            cIBResultObj.Status = "";
                        }
                        if (dataRow[3].ToString() != "")
                        {
                            cIBResultObj.Remarks = AddSlash(dataRow[3].ToString());
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine(cIBResultObj.Remarks.ToString());
                            //stremWriter.WriteLine("</cell>");
                        }
                        else
                        {
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine("");
                            //stremWriter.WriteLine("</cell>");
                            cIBResultObj.Remarks = "";
                        }
                        if (dataRow[4].ToString() != "")
                        {
                            cIBResultObj.FatherName = AddSlash(dataRow[4].ToString());
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine(cIBResultObj.FatherName.ToString());
                            //stremWriter.WriteLine("</cell>");
                        }
                        else
                        {
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine("");
                            //stremWriter.WriteLine("</cell>");
                            cIBResultObj.FatherName = "";
                        }
                        if (dataRow[5].ToString() != "")
                        {
                            cIBResultObj.MotherName = AddSlash(dataRow[5].ToString());
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine(cIBResultObj.MotherName.ToString());
                            //stremWriter.WriteLine("</cell>");
                        }
                        else
                        {
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine("");
                            //stremWriter.WriteLine("</cell>");
                            cIBResultObj.MotherName = "";
                        }
                        if (dataRow[6].ToString() != "")
                        {
                            cIBResultObj.ReferenceNumber = AddSlash(dataRow[6].ToString());
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine(cIBResultObj.ReferenceNumber.ToString());
                            //stremWriter.WriteLine("</cell>");
                        
                        }
                        else
                        {
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine("");
                            //stremWriter.WriteLine("</cell>");
                            cIBResultObj.ReferenceNumber = "";
                        }
                        if (dataRow[7].ToString() != "")
                        {
                            cIBResultObj.Type = AddSlash(dataRow[7].ToString());
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine(cIBResultObj.Type.ToString());
                            //stremWriter.WriteLine("</cell>");
                        
                        }
                        else
                        {
                            //stremWriter.WriteLine("<cell>");
                            //stremWriter.WriteLine("");
                            //stremWriter.WriteLine("</cell>");
                            cIBResultObj.Type = "";
                        }
                        cIBResultObj.EntryDate = DateTime.Now;
                        cIBResultObj.UserId = Convert.ToInt32(Session["Id"].ToString());
                        slNo = slNo + 1;
                        //stremWriter.WriteLine("</row>");
                        cIBResultObjList.Add(cIBResultObj);
                    }
                    //stremWriter.WriteLine("</rows>");

                }
                catch (Exception ex)
                {
                    Alert.Show(ex.Message);
                    //stremWriter.Close();
                }
                //stremWriter.Close();
            }
            if (cIBResultObjList.Count > 0)
            {
                UploadCIBResultList(cIBResultObjList);
            }
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0 && sMessage != "")
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private void UploadCIBResultList(List<CIBResult> cIBResultObjList)
        {
            int ListInsert = 0;
            if (cIBResultObjList.Count > 0)
            {
                ListInsert = cIBResultManagerObj.SendCIBResultInToDB(cIBResultObjList);
            }
            if (ListInsert == INSERTE || ListInsert == UPDATE)
            {
                Alert.Show("Upload Successfully");
            }
            else
            {
                Alert.Show("Error in data ! \n Please check excel file");
            }
        }
    }
}
