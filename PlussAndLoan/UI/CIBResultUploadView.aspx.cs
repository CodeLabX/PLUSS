﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_CIBResultUploadView : System.Web.UI.Page
    {
        public CIBResultManager cIBResultManagerObj = null;
        Int32 rowsPerPage = 50;
        Int32 pageNum = 1;
        Int32 offset = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            cIBResultManagerObj=new CIBResultManager();
            if (Request.QueryString.Count != 0)
            {
                pageNum = Convert.ToInt32(Request.QueryString["pageIndex"]);
                offset = (pageNum - 1) * rowsPerPage;
            }
            BindData(offset, rowsPerPage);

        }
        private void BindData(Int32 offset, Int32 rowsPerPage)
        {
            List<CIBResult> cIBResultObjList = new List<CIBResult>();
            cIBResultObjList = cIBResultManagerObj.GetCIBResult(offset,rowsPerPage);
            if (cIBResultObjList != null)
            {
                Response.Write("<table width='100%'>");
                Response.Write("<tr>");
                Response.Write("<td><b>SL.</b></td>");
                Response.Write("<td><b>LLId</b></td>");
                Response.Write("<td><b>Customer Name</b></td>");
                Response.Write("<td><b>Status</b></td>");
                Response.Write("<td><b>Remarks</b></td>");
                Response.Write("<td><b>Father's Name</b></td>");
                Response.Write("<td><b>Mother's Name</b></td>");
                Response.Write("<td><b>Ref Number</b></td>");
                Response.Write("<td><b>Type</b></td>");
                Response.Write("</tr>");
                foreach (CIBResult cIBResultObj in cIBResultObjList)
                {
                    Response.Write("<tr>");
                    Response.Write("<td>" + cIBResultObj.ID.ToString() + "</td>");
                    Response.Write("<td>" + cIBResultObj.LLID.ToString() + "</td>");
                    Response.Write("<td>" + cIBResultObj.CustomerName.ToString() + "</td>");
                    Response.Write("<td>" + cIBResultObj.Status.ToString() + "</td>");
                    Response.Write("<td>" + cIBResultObj.Remarks.ToString() + "</td>");
                    Response.Write("<td>" + cIBResultObj.FatherName.ToString() + "</td>");
                    Response.Write("<td>" + cIBResultObj.MotherName.ToString() + "</td>");
                    Response.Write("<td>" + cIBResultObj.ReferenceNumber.ToString() + "</td>");
                    Response.Write("<td>" + cIBResultObj.Type.ToString() + "</td>");
                    Response.Write("</tr>");
                }
                Response.Write("</table>");
                Int32 numrows = cIBResultManagerObj.GetTolalRecord();
                Int32 maxPage = numrows/rowsPerPage;
                string nav = "";
                string prev = "";
                string first = "";
                string next = "";
                string last = "";
                Int32 page = 0;
                for( page = 1; page <= maxPage; page++)
                {
                    if (page == pageNum)
                    {
                        nav = page.ToString();
                    }
                    else
                    {
                        nav += " <a href=../UI/CIBResultUploadView.aspx?pageIndex=" + page.ToString() + ">" + page.ToString() + "</a> ";
                    }		
                }
                if (pageNum > 1)
                {
                    page = pageNum - 1;
                    prev = " <a href=../UI/CIBResultUploadView.aspx?pageIndex=" + page.ToString() + ">Prev</a> ";
                    first = " <a href=../UI/CIBResultUploadView.aspx?pageIndex=1>First Page</a>";
                } 
                else
                {
                    prev  = "&nbsp;"; 
                    first = "&nbsp;"; 
                }

                if (pageNum < maxPage)
                {
                    page = pageNum + 1;
                    next = " <a href=../UI/CIBResultUploadView.aspx?pageIndex=" + page + ">Next</a> ";

                    last = " <a href=../UI/CIBResultUploadView.aspx?pageIndex=" + maxPage + ">Last Page</a> ";
                } 
                else
                {
                    next = "&nbsp;"; 
                    last = "&nbsp;"; 
                }
                Response.Write("<table width='100%'>");
                Response.Write("<tr>");
                Response.Write("<td>"+ first.ToString()+ prev.ToString()+ nav.ToString()+ next.ToString() + last.ToString() +"</td>");
                Response.Write("</tr>");

            }
        }
        private string GetTextFromXMLFile(string file)
        {
            StreamReader reader = new StreamReader(file);
            string ret = reader.ReadToEnd();
            reader.Close();
            return ret;
        }
    
    }
}
