﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BLL.LoanLocatorReports;

namespace PlussAndLoan.UI
{
    public partial class IndividualTatReport : System.Web.UI.Page
    {
        private readonly LocAdminTatReportManager _locAdminManager = new LocAdminTatReportManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            string product = (string)Session["pro"];
            string fromDate = (string)Session["dateOne"];
            string toDate = (string)Session["dateTwo"];

            productDiv.InnerText = product;
            formDateDiv.InnerText = fromDate;
            toDateDiv.InnerText = toDate;


            DataTable dtApplications = (DataTable)Session["tatReport"];
            if (dtApplications != null)
            {
                for (int j = 0; j < dtApplications.Rows.Count; j++)
                {

                    var loanId = Convert.ToInt32(dtApplications.Rows[j]["LOAN_APPL_ID"]);

                    DataTable dt = GetData(loanId);


                    var TAT_SOURCE1 = 0;
                    var TAT_SOURCE2 = 0;
                    var TAT_SOURCE3 = 0;
                    var TAT_CREDIT1 = 0;
                    var TAT_CREDIT2 = 0;
                    var TAT_CREDIT3 = 0;
                    var TAT_CREDIT_VALUATION = 0;
                    var TAT_CREDIT_LEGAL = 0;
                    var TAT_CREDIT_ONHOLD = 0;
                    var TAT_ASSET1 = 0;
                    var TAT_ASSET2 = 0;
                    var TAT_ASSET3 = 0;
                    var TAT_ASSET_ONHOLD = 0;

                    var TAT_VERIF = 0;
                    var TAT_UNACKN = 0;
                    var TAT_OTHERS = 0;
                    var TAT_TOTAL = 0;

                    var s = 1;
                    var c = 1;
                    var a = 1;


                    if (dt != null)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["TrType"].ToString() == "1Source")
                            {
                                if (s == 1)
                                    TAT_SOURCE1 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else if (s == 2)
                                    TAT_SOURCE2 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else if (s == 3)
                                    TAT_SOURCE2 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else
                                    TAT_OTHERS = TAT_OTHERS + Convert.ToInt32(dt.Rows[i]["TAT"]);
                                s = s + 1;
                            }
                            if (dt.Rows[i]["TrType"].ToString() == "3Credit")
                            {

                                if (c == 1)
                                    TAT_CREDIT1 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else if (c == 2)
                                    TAT_CREDIT2 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else if (c == 3)
                                    TAT_CREDIT3 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else
                                    TAT_OTHERS = TAT_OTHERS + Convert.ToInt32(dt.Rows[i]["TAT"]);
                                c = c + 1;
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_valuation")
                            {
                                TAT_CREDIT_VALUATION = TAT_CREDIT_VALUATION + Convert.ToInt32(dt.Rows[i]["TAT"]);
                            }
                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_legal")
                            {
                                TAT_CREDIT_LEGAL = TAT_CREDIT_LEGAL + Convert.ToInt32(dt.Rows[i]["TAT"]);
                            }


                            if (dt.Rows[i]["TrType"].ToString() == "3Credit_onhold")
                            {
                                TAT_CREDIT_ONHOLD = TAT_CREDIT_ONHOLD + Convert.ToInt32(dt.Rows[i]["TAT"]);
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "6Asset ops")
                            {

                                if (a == 1)
                                    TAT_ASSET1 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else if (a == 2)
                                    TAT_ASSET2 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else if (a == 3)
                                    TAT_ASSET3 = Convert.ToInt32(dt.Rows[i]["TAT"]);
                                else
                                    TAT_OTHERS = TAT_OTHERS + Convert.ToInt32(dt.Rows[i]["TAT"]);
                                a = a + 1;
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "6Assetops_onhold")
                            {
                                TAT_ASSET_ONHOLD = TAT_ASSET_ONHOLD + Convert.ToInt32(dt.Rows[i]["TAT"]);
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "4Varification")
                            {
                                TAT_VERIF = TAT_VERIF + Convert.ToInt32(dt.Rows[i]["TAT"]);
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "2Transition")
                            {
                                TAT_UNACKN = TAT_UNACKN + Convert.ToInt32(dt.Rows[i]["TAT"]);
                            }

                            if (dt.Rows[i]["TrType"].ToString() == "5Transition")
                            {
                                TAT_UNACKN = TAT_UNACKN + Convert.ToInt32(dt.Rows[i]["TAT"]);
                            }
                        }

                        TAT_TOTAL = TAT_SOURCE1 + TAT_SOURCE2 + TAT_SOURCE3 +
                                    TAT_CREDIT1 + TAT_CREDIT2 + TAT_CREDIT3 + TAT_CREDIT_ONHOLD + TAT_CREDIT_VALUATION +
                                    TAT_CREDIT_LEGAL + TAT_VERIF + TAT_ASSET1 + TAT_ASSET2 + TAT_ASSET3 + TAT_ASSET_ONHOLD +
                                    TAT_UNACKN + TAT_OTHERS;



                        TableRow row = new TableRow();
                        row.Cells.Add(new TableCell { Text = loanId.ToString() });
                        row.Cells.Add(new TableCell { Text = product });
                        row.Cells.Add(new TableCell { Text = TAT_SOURCE1.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_SOURCE2.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_SOURCE3.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT1.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT2.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT3.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_ONHOLD.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_VALUATION.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_CREDIT_LEGAL.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_VERIF.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET1.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET2.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET3.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_ASSET_ONHOLD.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_UNACKN.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_OTHERS.ToString() });
                        row.Cells.Add(new TableCell { Text = TAT_TOTAL.ToString() });
                        myTable.Rows.Add(row);

                    }

                }
            }

        }


        public DataTable GetData(int loanId)
        {
            var results = _locAdminManager.GetData(loanId);
            return results;
        }


    }
}