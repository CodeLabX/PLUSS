﻿using System;
using System.Web.Services;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_CheckTopupLoanAmount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        [WebMethod]
        public static string GetLoanAmount(string LLId, double outStanding)
        {
            try
            {
                string returnvalue = "";
                double loanAmount = 0;
                double compareLoanAmount = 0;
                PLManager pLManagerObj = new PLManager();
                if (LLId.Length > 0)
                {
                    PL plObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(LLId.Trim()));
                    if (plObj != null)
                    {
                        if (plObj.PlLoantypeId == 3)
                        {
                            if (plObj.PlSafetyplusId == 1)
                            {
                                loanAmount = plObj.PlBancaLoanAmount;
                            }
                            else
                            {
                                loanAmount = plObj.PlProposedloanamount;
                            }
                        }
                    }
                    compareLoanAmount = loanAmount - outStanding;
                    if (compareLoanAmount < 50000)
                    {
                        returnvalue = "Current loan and new loan limit difference is less than 50 K";
                    }
                }
                return returnvalue;
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Check Topup Loan Amount");
                throw new Exception("");
            }

        }
    }
}
