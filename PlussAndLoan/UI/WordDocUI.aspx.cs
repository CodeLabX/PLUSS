﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_WordDocUI : System.Web.UI.Page
    {
        public SubjectManager subjectManagerObj = null;
        public WordDocumentManager wordDocumentManagerObj=null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            subjectManagerObj = new SubjectManager();
            wordDocumentManagerObj = new WordDocumentManager(); 
            if (!Page.IsPostBack)
            {
                LoadData();
                LoadSubjectList();
            }
        }

        private void LoadData()
        {
            
            List<WordDocument> wordDocumentObjList = new List<WordDocument>();
           
            wordDocumentObjList = wordDocumentManagerObj.SelectWordDocumentList();
            try
            {
                var xml = "<xml id='xml_data'><rows>";
                foreach (WordDocument wordDocumentObj in wordDocumentObjList)
                {
                    xml+="<row id='" + wordDocumentObj.Id.ToString() + "'>";

                    xml+="<cell>";
                    xml+=wordDocumentObj.Id.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.To.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=wordDocumentObj.SubjectId.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.SubjectName.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.Body.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.Body1.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.Body2.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.Body3.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.Body4.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.Body5.ToString());
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=AddSlash(wordDocumentObj.Body6.ToString());
                    xml+="</cell>";

                    xml+="</row>";
                }
                xml += "</rows></xml>";
                docDiv.InnerHtml = xml;
            }
            finally
            {
            }
            
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private void LoadSubjectList()
        {
            List<Subject> subjectObjList = subjectManagerObj.SelectSubjectList();
            Subject subjectObj = new Subject();
            subjectObj.Id = 0;
            subjectObj.Name = "Select subject name";
            subjectObjList.Insert(0, subjectObj);
            subjectDropDownList.DataSource = subjectObjList;
            subjectDropDownList.DataValueField = "Id";
            subjectDropDownList.DataTextField = "Name";
            subjectDropDownList.DataBind();
        }
        protected void updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                int wordDocument = 0;
                WordDocument wordDocumentObj = new WordDocument();
                if (IsBlank())
                {
                    if (idHiddenField.Value != "")
                    {
                        wordDocumentObj.Id = Convert.ToInt32(idHiddenField.Value);
                    }
                    wordDocumentObj.To = Convert.ToString(toTextBox.Text.Trim());
                    wordDocumentObj.SubjectId = Convert.ToInt32(subjectDropDownList.SelectedValue.ToString());
                    wordDocumentObj.Body = Convert.ToString(starLable.Text + bodyPartTextBox.Text.Trim() + hashLabel.Text + bodyTextBox.Text.Trim());
                    wordDocumentObj.Body1 = Convert.ToString(body1TextBox.Text.Trim());
                    wordDocumentObj.Body2 = Convert.ToString(body2TextBox.Text.Trim());
                    wordDocumentObj.Body3 = Convert.ToString(body3TextBox.Text.Trim());
                    wordDocumentObj.Body4 = Convert.ToString(body4TextBox.Text.Trim());
                    wordDocumentObj.Body5 = Convert.ToString(body5TextBox.Text.Trim());
                    wordDocumentObj.Body6 = Convert.ToString(body6TextBox.Text.Trim());
                    wordDocument = wordDocumentManagerObj.SendWordDocumentInToDB(wordDocumentObj);
                    var msg = "";
                    if (wordDocument == INSERTE)
                    {
                        msg = "Save successfully";
                        LoadData();
                        ClearField();
                    }
                    else if (wordDocument == UPDATE)
                    {
                        msg = "Update successfully";
                        LoadData();
                        ClearField();
                    }
                    else if (wordDocument == ERROR)
                    {
                        msg = "Error in data!";
                    }

                    new AT(this).AuditAndTraial("Bank Information", msg);
                    Alert.Show(msg);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Upload Word Doc");
            }

        }
        private bool IsBlank()
        {
            if (Convert.ToInt32(subjectDropDownList.SelectedValue.ToString())==0)
            {
                Alert.Show("Please select subject name");
                subjectDropDownList.Focus();
                return false;
            }
            else if (bodyTextBox.Text == "")
            {
                Alert.Show("Please input New Sourcing");
                bodyTextBox.Focus();
                return false;
            }
            else if (bodyTextBox.Text == "")
            {
                Alert.Show("Please input Industry Profit Margin");
                bodyTextBox.Focus();
                return false;
            }
            else if (bodyTextBox.Text == "")
            {
                Alert.Show("Please input Deviation");
                bodyTextBox.Focus();
                return false;
            }
            else if (bodyTextBox.Text == "")
            {
                Alert.Show("Please input Top-up");
                bodyTextBox.Focus();
                return false;
            }
            else if (bodyTextBox.Text == "")
            {
                Alert.Show("Please input Bureau Policy");
                bodyTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearField();
        }
        private void ClearField()
        {
            idHiddenField.Value = wordDocumentManagerObj.SelectMaxId().ToString();
            toTextBox.Text = "";
            bodyPartTextBox.Text = "";
            bodyTextBox.Text = "";
            body1TextBox.Text = "";
            body2TextBox.Text = "";
            body3TextBox.Text = "";
            body4TextBox.Text = "";
            body5TextBox.Text = "";
            body6TextBox.Text = "";
            subjectDropDownList.SelectedIndex = 0;
        }
    }
}
