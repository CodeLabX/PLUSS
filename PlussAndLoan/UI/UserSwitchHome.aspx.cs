﻿using System;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Bat.Common;
using BLL;
using BusinessEntities;
using DAL;
using UserManager = BLL.UserManager;

namespace PlussAndLoan.UI
{
    public partial class UserSwitchHome : System.Web.UI.Page
    {
        enum USERTYPE : short
        {
            Admin = 1,
            Analyst = 2,
            SupportUser = 3,
            Approver = 4,
            ScoreAdmin = 5,

            //-----------Added by Mazhar-------------
            AutoAdmin = 6,
            AutoApprover = 7,
            AutoAnalyst = 8,
            AutoSupport = 9,
            AutoOperations = 10,
            AutoSales = 11,
            AutoOperationChecker = 12,
            AutoAnalystApprover = 13,

            AdminMaker=17,
            AdminChecker=18,
            LoanLocatorOperation=19,
            LoanLocatorAdmin=20
        }
        public Int32 userType = 0;
        public Int32 autoUserType = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            int both = 0;
            btnPluss.Enabled = false;
            btnLoanLocator.Enabled = false;

            try
            {
                if (Session["UserType"] != null)
                {
                    userType = Convert.ToInt32(Session["UserType"].ToString());
                    both = Convert.ToInt32(Session["both"]);
                }
                if (Session["AutoUserType"] != null)
                {
                    autoUserType = Convert.ToInt32(Session["AutoUserType"].ToString());
                }

                if (userType == 0 && autoUserType == 0)
                {
                    btnPluss.Enabled = false;
                    // btnAuto.Enabled = false;

                }
                else if (userType == 0 && autoUserType != 0)
                {
                    btnPluss.Enabled = false;
                    //    btnAuto.Enabled = true;
                }
                else if (userType != 0 && autoUserType == 0)
                {
                    btnLoanLocator.Enabled = both == 1;
                    if (userType != 19)
                    {
                        btnPluss.Enabled = true;  
                    }
                    //    btnAuto.Enabled = false;
                }
                else if (userType != 0 && autoUserType != 0)
                {
                    btnPluss.Enabled = true;
                    btnLoanLocator.Enabled = both == 1;
                    //    btnAuto.Enabled = true;
                }

                btnLoanLocator.Enabled = (userType == 19) || (userType == 20);
                if (both == 1)
                {
                    btnPluss.Enabled = true;
                    btnLoanLocator.Enabled = true;
                }
                

                var log = (List<UserAccessLog>)Session["loginStatus"];
                var dt = new UserAccessLogManager().GetUnSuccessfullLoginHistory(Session["UserId"].ToString());

                if (log != null && log.Any())
                {
                    loginStatus.ForeColor = Color.Green;
                    loginStatus.Font.Bold=true;
                    loginStatus.Text = "Last Successfull Login is " + log[0].LogInDateTime;
                    if (Convert.ToInt32(dt.Rows[0]["Total"]) > 0)
                    {
                        loginError.Font.Bold = true;
                        loginError.ForeColor = Color.Red;
                        loginError.Text = "There were " + Convert.ToInt32(dt.Rows[0]["Total"]) + " unsuccessful login attempt using your User ID";
                    }
                    else
                    {
                        loginError.Text = "";
                    }
                    
                }
            }
            catch (Exception exception)
            {
                new AT(this).AuditAndTraial("switch user", "SW UI -" + exception.Message);
            }
       
 
        }
        protected void btnPluss_Click(object sender, EventArgs e)
        {

            if (userType.Equals((short)USERTYPE.Admin) || userType.Equals((short)USERTYPE.ScoreAdmin))
            {
                Response.Redirect("~/UI/AdminHomeUI.aspx");
            }
            else if (userType.Equals((short)USERTYPE.Analyst) || userType.Equals((short)USERTYPE.Admin) || userType.Equals((short)USERTYPE.Approver) || userType.Equals((short)USERTYPE.ScoreAdmin))
            {
                Response.Redirect("~/UI/AnalystHomeUI.aspx");
            }
            else if (userType.Equals((short)USERTYPE.SupportUser) || userType.Equals((short)USERTYPE.Admin) || userType.Equals((short)USERTYPE.Analyst))
            {
                Response.Redirect("~/UI/BlankPage.aspx");
            }
            else if (userType.Equals((short)USERTYPE.AdminMaker) || userType.Equals((short)USERTYPE.AdminChecker) || userType.Equals(7))
            {
                Response.Redirect("~/UI/ITAdminHome.aspx");
            }
        }
        protected void btnAuto_Click(object sender, EventArgs e)
        {
            if (autoUserType.Equals((short)USERTYPE.AutoAdmin))
            {
                Response.Redirect("~/UI/AutoLoan/AutoLoanHome.aspx");
            }
            else if (autoUserType.Equals((short)USERTYPE.AutoApprover))
            {
                Response.Redirect("~/UI/AutoLoan/AutoApproverHome.aspx");
            }
            else if (autoUserType.Equals((short)USERTYPE.AutoAnalyst))
            {
                Response.Redirect("~/UI/AutoLoan/AutoAnalystHome.aspx");
            }

            else if (autoUserType.Equals((short)USERTYPE.AutoSupport))
            {
                Response.Redirect("~/UI/AutoLoan/AutoSupportHome.aspx");
            }
            else if (autoUserType.Equals((short)USERTYPE.AutoOperations))
            {
                Response.Redirect("~/UI/AutoLoan/AutoOperationsHome.aspx");
            }
            else if (autoUserType.Equals((short)USERTYPE.AutoSales))
            {
                Response.Redirect("~/UI/AutoLoan/AutoSalesHome.aspx");
            }
            else if (autoUserType.Equals((short)USERTYPE.AutoOperationChecker))
            {
                Response.Redirect("~/UI/AutoLoan/AutoOperationCheckerHome.aspx");
            }
            else if (autoUserType.Equals((short)USERTYPE.AutoAnalystApprover))
            {
                Response.Redirect("~/UI/AutoLoan/AutoAnalystApproverHome.aspx");
            }
        }
        protected void btnLoanLocator_Click(object sender, EventArgs e)
        {
            var userManagerObj = new UserManager();
            var loanUser = userManagerObj.GetLoanLocatorUser(Session["UserId"].ToString());
            //loanUser.ID_LEARNER = Convert.ToInt32(Session["Id"]);
            loanUser.logedintime = DateTime.Now;
            Session["loanUser"] = loanUser;


            if (loanUser.LOAN_DEPT_ID == 1 || loanUser.LOAN_DEPT_ID == 2 || loanUser.LOAN_DEPT_ID == 3)
            {
                Response.Redirect("~/UI/LocLoanHome.aspx"); // Submission 
            }
            else if (loanUser.LOAN_DEPT_ID == 4)
            {
                Response.Redirect("~/UI/LocOperationHome.aspx");
            }
            else if (loanUser.LOAN_DEPT_ID == 5 || loanUser.LOAN_DEPT_ID == 7)
            {
                Response.Redirect("~/UI/LocAssetHome.aspx");
            }
            else if (loanUser.LOAN_DEPT_ID == 6)
            {
                Response.Redirect("~/UI/LocLoanAdminHome.aspx"); 
            }
            else if (loanUser.LOAN_DEPT_ID == 8 || loanUser.LOAN_DEPT_ID == 9)
            {
                Response.Redirect("~/UI/LocLoanViewer.aspx");
            }
            else
            {
                Response.Write("You are not properlly configured to access Loan Locator Module ! Contact to administrator to configure again.");
            }
        }
    }
}
