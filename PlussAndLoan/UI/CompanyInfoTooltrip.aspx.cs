﻿using System;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_CompanyInfoTooltrip : System.Web.UI.Page
    {
        public PayRollManager payRollManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                payRollManagerObj = new PayRollManager();
                if (Request.QueryString.Count != 0)
                {
                    Int32 companyId = Convert.ToInt32(Request.QueryString["companyId"]);
                    LoadCompanyInfo(companyId);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Load Company Info");
            }
        }
        private void LoadCompanyInfo(Int32 companyId)
        {
            if (companyId > 0)
            {
                PayRoll payRollObj = new PayRoll();
                payRollObj = payRollManagerObj.SelectCompanyInfo(companyId,"");
                if (payRollObj != null)
                {
                    Response.Write("<table width='100%'>");

                    Response.Write("<tr>");
                    Response.Write("<td><b>Company Name :</b></td>");
                    Response.Write("<td>" + payRollObj.CompName.ToString() + "</td>");
                    Response.Write("<td><b>Segment Code :</b></td>");
                    Response.Write("<td>" + payRollObj.CompSegmentID.ToString() + "</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr>");
                    Response.Write("<td><b>Company Type :</b></td>");
                    Response.Write("<td>" + payRollObj.CompStructure.ToString() + "</td>");
                    Response.Write("<td><b>Cat Status :</b></td>");
                    Response.Write("<td>" + payRollObj.CompCatstatus.ToString() + "</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr>");
                    Response.Write("<td><b>Do Categorized :</b></td>");
                    Response.Write("<td>" + payRollObj.CompDocategorized.ToString() + "</td>");
                    Response.Write("<td><b>MUE :</b></td>");
                    Response.Write("<td>" + payRollObj.CompMue.ToString() + "</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr>");
                    Response.Write("<td><b>IR with EOSB :</b></td>");
                    Response.Write("<td>" + payRollObj.CompIrwitheosb.ToString() + "</td>");
                    Response.Write("<td><b>IR without EOSB :</b></td>");
                    Response.Write("<td>" + payRollObj.CompIrwithouteosb.ToString() + "</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr>");
                    Response.Write("<td><b>CCU Contition :</b></td>");
                    Response.Write("<td>" + payRollObj.CompCcucondition.ToString() + "</td>");
                    Response.Write("<td><b>CCU Remarks :</b></td>");
                    Response.Write("<td>" + payRollObj.CompCcuremarks.ToString() + "</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr>");
                    Response.Write("<td><b>Extra Benefit :</b></td>");
                    Response.Write("<td>" + payRollObj.ExtraBenefit.ToString() + "</td>");
                    Response.Write("<td></td>");
                    Response.Write("<td></td>");
                    Response.Write("</tr>");
                    Response.Write("</table>");
                }
            }
        }
    }
}
