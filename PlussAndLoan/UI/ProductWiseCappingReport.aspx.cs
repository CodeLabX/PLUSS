﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI
{
    public partial class ProductWiseCappingReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                date.Text=Session["date1"].ToString();
                date3.Text = Session["date2"].ToString();
                date2.Text = " To ";
                ProdWiseCappingGridView.DataSource = Session["prodWiseCapp"];
                ProdWiseCappingGridView.DataBind();
            }

        }

        int sumFooterValue = 0;
        Label total;
        protected void ProdWiseCappingGridView_RowDataBound(object sender, GridViewRowEventArgs e)
       {
            //IsAccessBoth
           Label percentTotal=null;
           decimal digit=0;

           Label percentSpace = null;
           decimal digitSpace = 0;
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // view total ammount in the footer
                total = (Label)e.Row.FindControl("hdlabelTotal");
                sumFooterValue = Convert.ToInt32(total.Text);

                //for two decimal place of total
                percentTotal = (Label)e.Row.FindControl("total");
                decimal digitTotal =Convert.ToDecimal(String.Format("{0:0.00}", Convert.ToDecimal(percentTotal.Text)));
                e.Row.Cells[2].Text = digitTotal.ToString();
                digit +=digitTotal ;
                //for round decimal number
               // Math.Round(digitTotal, 2);

                // for two decimal place of space

                percentSpace = (Label)e.Row.FindControl("space");
                decimal space = Convert.ToDecimal(String.Format("{0:0.00}", Convert.ToDecimal(percentSpace.Text)));
                e.Row.Cells[4].Text = space.ToString();
                digitSpace += space;
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbl = (Label)e.Row.FindControl("labelTotal");
                lbl.Text = sumFooterValue.ToString();
            }

        }

        protected void Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
            Session.Clear();
        }

        protected void Close_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
            Session.Clear();
        }
    }
}