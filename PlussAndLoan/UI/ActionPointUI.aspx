﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_ActionPointUI" Title="Action Point" Codebehind="ActionPointUI.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="content" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <div>
    
        <table>
            <tr>
                <td colspan="3" align="left" style="padding-left:270px">
                    <asp:Label ID="Label1" runat="server" Text="Action Point Information" Font-Bold="true" Font-Size="18px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:HiddenField ID="idHiddenField" runat="server" /></td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label11" runat="server" Text="Color Name:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="colorNameTextBox" runat="server" Width="150px"></asp:TextBox>
                    <asp:Label ID="Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label2" runat="server" Text="New Sourcing:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="newSourceTextBox" runat="server" Width="250px">
                    </asp:TextBox>
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label3" runat="server" Text="Industry Profit Margin:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="indstryPrfMrgTextBox" runat="server" Width="250px">
                    </asp:TextBox>
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label4" runat="server" Text="Deviation:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="deviationTextBox" runat="server" Width="250px">
                    </asp:TextBox>
                    <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label5" runat="server" Text="Top-up:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="topupTextBox" runat="server" Width="250px"
                        TextMode="SingleLine"></asp:TextBox>
                        <asp:Label ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                     <asp:Label ID="codeLabel" runat="server" Text="Bureau Policy: "></asp:Label>
                    </td>
                <td>
                    <asp:TextBox id="bureauPolicyTextBox" runat="server" Width="250px">
                    </asp:TextBox>
                    <asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                     <asp:Label ID="statusLabel" runat="server" Text="Status"></asp:Label>
                     </td>
                <td>
                    <asp:DropDownList ID="statusDropDowonList" runat="server" Width="100px">
                    <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>Inactive</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                <asp:Button ID="saveButton" runat="server" Text="Save" Width="100px" Height="27px" 
                        onclick="saveButton_Click" />&nbsp;
                <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" 
                        Height="27px" onclick="clearButton_Click" />&nbsp;
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="center" style="padding-left:2px">
                 <div id="gridbox"  style="background-color:white; width:785px; height:285px;"></div>   
                </td>
            </tr>
        </table>    
         <div style="display: none" runat="server" id="actionDiv"></div>
    </div>
    <script type="text/javascript">
        var mygrid;
        mygrid = new dhtmlXGridObject('gridbox');
        mygrid.setImagePath("../codebase/imgs/");
        mygrid.setHeader("SL.,Coler Name,New Sourching,Industry Profit Margin,Deviation,Top-up,Bureau Policy,Status");
        mygrid.setInitWidths("30,95,100,140,120,120,120,60");
        mygrid.setColAlign("right,left,left,left,left,left,left,left");
        mygrid.setColTypes("ro,ed,ro,ro,ro,ro,ro,ro");
        mygrid.setColSorting("int,str,str,str,str,str,str,str");
        //mygrid.setSkin("modern");	 
        mygrid.setSkin("light");
        mygrid.init();
        mygrid.enableSmartRendering(true, 20);
       // mygrid.loadXML("../includes/ActionPointsList.xml");
        mygrid.parse(document.getElementById("xml_data"));
        function DoOnRowSelected(id) {
            if (mygrid.cells(mygrid.getSelectedId(), 0).getValue() > 0) {
               // document.getElementById('ctl00_ContentPlaceHolder1_saveButton').value = "Update";
                document.getElementById('ctl00_ContentPlaceHolder_idHiddenField').value = mygrid.cells(mygrid.getSelectedId(), 0).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_colorNameTextBox').value = mygrid.cells(mygrid.getSelectedId(), 1).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_newSourceTextBox').value = mygrid.cells(mygrid.getSelectedId(), 2).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_indstryPrfMrgTextBox').value = mygrid.cells(mygrid.getSelectedId(), 3).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_deviationTextBox').value = mygrid.cells(mygrid.getSelectedId(), 4).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_topupTextBox').value = mygrid.cells(mygrid.getSelectedId(), 5).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_bureauPolicyTextBox').value = mygrid.cells(mygrid.getSelectedId(), 6).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_statusDropDowonList').value = mygrid.cells(mygrid.getSelectedId(), 7).getValue();
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder1_saveButton').value = "Save";
            }
        };
        mygrid.attachEvent("onRowSelect", DoOnRowSelected);
	</script>
</asp:Content>


