﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI
{
    public partial class GuestTatReport : System.Web.UI.Page
    {
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        private readonly LoanReportService _service = new LoanReportService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadProduct();
                LoadLoanState();
                txt_startDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
                txt_toDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        private void LoadProduct()
        {
            List<Product> productListObj = _loanApplicationManagerObj.GetAllProduct();
            var productObj = new Product { ProductId = 0, ProductName = "" };
            productListObj.Insert(0, productObj);
            sel_product.DataTextField = "ProductName";
            sel_product.DataValueField = "ProductId";
            sel_product.DataSource = productListObj;
            sel_product.DataBind();
        }

        private void LoadLoanState()
        {
            var dt = _loanApplicationManagerObj.GetLoanStates();
            var loanStates = DataTableToList.GetList<Loan>(dt);
            var state = new Loan() { LoanState = 0, StateName = "" };
            loanStates.Insert(0, state);
            sel_loanStatus.DataTextField = "StateName";
            sel_loanStatus.DataValueField = "LoanState";
            sel_loanStatus.DataSource = loanStates;
            sel_loanStatus.DataBind();
        }

        protected void DuplicateSearch(object o, EventArgs e)
        {
            var start = txt_startDate.Value;
            var to = txt_toDate.Value;
            var productId = sel_product.Value;
            var stateId = sel_loanStatus.Value;
            var dt = _service.GetDuplicateSearch(start, to, productId, stateId);
            var data = _service.GetDuplicateRawData(start, to, productId);

            div_processResultDisplay.InnerHtml = "";

            var noofField = dt != null ? dt.Columns.Count : 100;
            var colWith = noofField != 0 ? 100 / noofField : 0;
            if (colWith * noofField > 100)
            {
                colWith = colWith - 1;
            }
            var successCount = 0;
            var chckIndex = 0;
            var tableBodyData = "";
            var tabelHeadRowName = "";
            var fieldData = "";
            var index = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                fieldData = "";
                for (index = 0; index < noofField; index++)
                {
                    string name = dt.Columns[index].ColumnName;
                    if (chckIndex == 0)
                    {
                        tabelHeadRowName += "<td width='" + colWith + "%'><label class='LabelHead'>" + name + "</label></td>";
                    }
                    fieldData += "<td><label>" + dt.Rows[i][index] + "</label></td>	";
                }
                tableBodyData += "<tr align='center'>" + fieldData + "</tr>";
                successCount = successCount + 1;
                chckIndex++;

            }

            var idArray = data.AsEnumerable().Select(r => r.Field<object>("ID")).ToList();
            var loanApplIdArray = data.AsEnumerable().Select(r => r.Field<object>("LOAN_APPL_ID")).ToList();
            var persIdArray = data.AsEnumerable().Select(r => r.Field<object>("PERS_ID")).ToList();
            var deptIdArray = data.AsEnumerable().Select(r => r.Field<object>("DEPT_ID")).ToList();
            var dateTimeArray = data.AsEnumerable().Select(r => r.Field<object>("DATE_TIME")).ToList();
            var processStatusIdArray = data.AsEnumerable().Select(r => r.Field<object>("PROCESS_STATUS_ID")).ToList();
            var remarksArray = data.AsEnumerable().Select(r => r.Field<object>("REMARKS")).ToList();
            var countArray = data.AsEnumerable().Select(r => r.Field<object>("noofCount")).ToList();

            var chckIndex1 = 0;
            index = 0;
            var tabelHeadRowName2 = "";
            for (int i = 0; i < data.Rows.Count; i++)
            {
                for (var index1 = 0; index1 < noofField; index1++)
                {
                    string name = dt.Columns[index1].ColumnName;
                    if ((chckIndex1 == 0) && (!string.IsNullOrEmpty(tabelHeadRowName)))
                    {
                        tabelHeadRowName2 += "<td width='" + colWith + "%'><label class='LabelHead'>" + name + "</label></td>";
                    }
                }
                index++;
                chckIndex1++;
            }

            var duplicateIdArray = "";

            for (index = 0; index < idArray.Count; index++)
            {
                if (index + 1 < idArray.Count)
                {
                    if ((loanApplIdArray[index] == loanApplIdArray[index + 1]) &&
                        (processStatusIdArray[index] == processStatusIdArray[index + 1]) &&
                        (deptIdArray[index] == deptIdArray[index + 1]))
                    {
                        fieldData += "<td><label>" + idArray[index] + "</label></td>";
                        fieldData += "<td><label>" + loanApplIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + persIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + deptIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + dateTimeArray[index] + "</label></td>";
                        fieldData += "<td><label>" + processStatusIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + remarksArray[index] + "</label></td>";
                        fieldData += "<td><label>" + countArray[index] + "</label></td>";
                        duplicateIdArray += idArray[index + 1] + ",";
                        successCount = successCount + 1;
                        tableBodyData += "<tr align='center'>" + fieldData + "</tr>";
                    }
                    if ((Convert.ToInt32(processStatusIdArray[index]) == 5) &&
                        (Convert.ToInt32(processStatusIdArray[index + 1]) == 4) &&
                        (Convert.ToInt32(deptIdArray[index]) == 4) && (Convert.ToInt32(deptIdArray[index + 1]) == 4))
                    {
                        fieldData += "<td><label>" + idArray[index] + "</label></td>";
                        fieldData += "<td><label>" + loanApplIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + persIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + deptIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + dateTimeArray[index] + "</label></td>";
                        fieldData += "<td><label>" + processStatusIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + remarksArray[index] + "</label></td>";
                        fieldData += "<td><label>" + countArray[index] + "</label></td>";
                        duplicateIdArray += idArray[index + 1] + ",";
                        successCount = successCount + 1;
                        tableBodyData += "<tr align='center'>" + fieldData + "</tr>";
                    }
                    if ((Convert.ToInt32(processStatusIdArray[index]) == 10) &&
                        (Convert.ToInt32(processStatusIdArray[index + 1]) == 4) &&
                        (Convert.ToInt32(deptIdArray[index]) == 4) && (Convert.ToInt32(deptIdArray[index + 1]) == 4))
                    {
                        fieldData += "<td><label>" + idArray[index] + "</label></td>";
                        fieldData += "<td><label>" + loanApplIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + persIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + deptIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + dateTimeArray[index] + "</label></td>";
                        fieldData += "<td><label>" + processStatusIdArray[index] + "</label></td>";
                        fieldData += "<td><label>" + remarksArray[index] + "</label></td>";
                        fieldData += "<td><label>" + countArray[index] + "</label></td>";
                        duplicateIdArray += idArray[index + 1] + ",";
                        successCount = successCount + 1;
                        tableBodyData += "<tr align='center'>" + fieldData + "</tr>";
                    }
                    fieldData = "";
                }
            }
            if (!string.IsNullOrEmpty(tabelHeadRowName))
            {
                tabelHeadRowName = tabelHeadRowName2;
            }

            var tableHeadData = "<tr align='center'  bgcolor='#CCCCCC'><td colspan='" + noofField + "' > <label class='LabelHead' >" + successCount + " Records found </td></tr><tr align='center'  bgcolor='#CCCCCC'>" + tabelHeadRowName + "</tr>";
            Session["DuplicateId"] = duplicateIdArray.Trim();
            div_processResultDisplay.InnerHtml = "<table border='1' cellpadding='0' cellspacing='0' width='100%' class='tableWidth'>" + tableHeadData + tableBodyData + "</table>";

            //            div_processResultDisplay.InnerHtml = "<table border='1' cellpadding='0' cellspacing='0' width='100%' class='tableWidth'><tr align='center'  bgcolor='#CCCCCC'>" + tabelHeadRowName + "</tr>" + tableBodyData + "</table>";
        }

        protected void ExportCsv(object o, EventArgs e)
        {

            var filename = "TATReportExport_" + DateTime.Now.Ticks + ".csv";
            var csv = string.Empty;

            var start = txt_startDate.Value;
            var to = txt_toDate.Value;
            var productId = sel_product.Value;
            var stateId = sel_loanStatus.Value;
            var dt = _service.GetDuplicateSearch(start, to, productId, stateId);

            csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ','));
            csv += "\r\n";

            foreach (DataRow row in dt.Rows)
            {
                csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", ";") + ','));
                csv += "\r\n";
            }

            //Download the CSV file.
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(csv);
            Response.Flush();
            Response.End();


        }
    }
}