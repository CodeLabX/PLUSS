﻿using System;
using System.Collections.Generic;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_SelectCompanyList : System.Web.UI.Page
    {
        public PayRollManager payRollManagerObj=null;
        protected void Page_Load(object sender, EventArgs e)
        {
            payRollManagerObj=new PayRollManager();

            string letters = "";
            if (Request.QueryString.Count != 0)
            {
                letters = Convert.ToString(Request.QueryString["letters"].ToString());
            }
            if (letters.Length > 0)
            {
                LoadList(letters);
            }
        }
        private void LoadList(string letters)
        {
            List<PayRoll> payRollObjList=payRollManagerObj.SarchByLetters(letters);
            if(payRollObjList!=null)
            {
                foreach(PayRoll PayRollObj in payRollObjList)
                {
                    Response.Write(PayRollObj.CompID.ToString()+"###"+PayRollObj.CompName.ToString()+"|");
                }
            }
        }
    }
}
