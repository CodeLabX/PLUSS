﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI
{

    public partial class ProductInfo : System.Web.UI.Page
    {
        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2
        }
        #endregion

        private LoanApplicationManager loanApplicationManager;
        readonly LocatorApplicationService _service = new LocatorApplicationService();
        protected void Page_Load(object sender, EventArgs e)
        {
            loanApplicationManager = new LoanApplicationManager();
            if (!IsPostBack)
            {
                LoadProduct();
            }
            productEditDiv.Visible = false;

            productViewDiv.Visible = true;
        }

        //protected void productInfoGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {

        //        e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(ProductGrid, "Select$" + e.Row.RowIndex.ToString()));
        //        e.Row.Attributes["onmouseover"] = "this.style.cursor = 'pointer'";
        //        e.Row.Focus();

        //    }
        //}
        private void LoadProduct()
        {
            List<Product> productListObj = new LoanApplicationManager().GetAllProductByLearner();
            ProductGrid.DataSource = productListObj;
            ProductGrid.DataBind();
        }

        protected void ProductGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            productEditDiv.Visible = true;

            productViewDiv.Visible = false;
            GridViewRow row = ProductGrid.SelectedRow;
            prodName.Text = ((Label)row.FindControl("productNameLabel")).Text;
            if (((Label)row.FindControl("learnerId")).Text == 0.ToString())
            {
                learnerName.Text = "";
            }
            else
            {
                learnerName.Text = ((Label)row.FindControl("learnerId")).Text;
            }
            idHiddenField.Value = ((Label)row.FindControl("ProdIdLabel")).Text;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            errorMsgLabel.Text = "";
            productEditDiv.Visible = false;

            productViewDiv.Visible = true;
            try
            {

                int insertOrUpdateRow = -1;
                Product product = new Product();
                if (String.IsNullOrEmpty(learnerName.Text.Trim()))
                {
                    errorMsgLabel.Text = "Staff id is not available. Please enter correct staff id";
                }
                else
                {
                    errorMsgLabel.Text = "";
                    product.ProductId = Convert.ToInt32(idHiddenField.Value);
                    if (!String.IsNullOrEmpty(learnerName.Text.Trim()))
                    {
                        product.IdLearner = Convert.ToInt32(learnerName.Text.Trim());

                        //updateFlag = true;
                    }
                    else
                    {
                        product.IdLearner = 0;
                    }

                    product.UserId = Convert.ToInt32(Session["Id"].ToString());

                    insertOrUpdateRow = loanApplicationManager.InsertORUpdateProductInfo(product);

                    if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT))
                    {
                        errorMsgLabel.Text = "Data save sucessfully.";
                        ClearField();
                        LoadProduct();


                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERTERROR))
                    {
                        errorMsgLabel.Text = "Data not saved sucessfully.";
                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                    {
                        errorMsgLabel.Text = "Data update sucessfully.";
                        //addButton.Text = "Save";
                        ClearField();
                        LoadProduct();
                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                    {
                        errorMsgLabel.Text = "Data not update sucessfully.";
                    }
                }
                new AT(this).AuditAndTraial("Bank Information", errorMsgLabel.Text);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Bank");
            }

        }

        public void ClearField()
        {
            idHiddenField.Value = "";
            learnerName.Text = "";
            prodName.Text = "";
            errorMsgLabel.Text = "";
        }


    }
}