﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.LoanLocatorReports;

namespace PlussAndLoan.UI
{
    public partial class PerformanceReport : Page
    {
        public readonly LocLoanReportManager _loanReportManager = new LocLoanReportManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            int productId = (int)Session["ProdId"];
            string productName = Session["ProdName"].ToString();
            string monthNo = Session["monthNo"].ToString();
            string monthName = Session["monthName"].ToString();
            string year = Session["year"].ToString();
            int y = Convert.ToInt32(Session["year"]);
            bool cb1 = (bool)Session["cb1"];
            string title = "For the month " + monthName + " of " + year;
            titleDiv.InnerText = title;
            product.InnerText = productName;



            // Start #############


            var regi_name = "";
            var TMR_NO_APPL = 0;
            var TMR_APPL_AMT = 0;
            var TMA_NO_APPL = 0;
            var TMA_APRV_AMT = 0; //month
            double t_rec_qty = 0;
            double t_rec_amt = 0;
            double t_appr_qty = 0;
            double t_appr_amt = 0;
            var t_mtq = 0;
            var t_mtv = 0;
            var t_ytq = 0;
            var t_ytv = 0;
            var t_mtqv = 0;
            var t_mtvv = 0;
            var t_ytqv = 0;
            var t_ytvv = 0;

            var SMR_NO_APPL = 0;
            var SMR_APPL_AMT = 0;
            var SMA_NO_APPL = 0;
            var SMA_APPL_AMT = 0;
            var SMA_APRV_AMT = 0; //month
            double s_rec_qty = 0;
            double s_rec_amt = 0;
            double s_appr_qty = 0;
            double s_appr_amt = 0;
            var s_mtq = 0;
            var s_mtv = 0;
            var s_ytq = 0;
            var s_ytv = 0;
            var s_mtqv = 0;
            var s_mtvv = 0;
            var s_ytqv = 0;
            var s_ytvv = 0;
            DataTable row1 = GetRowOne(monthNo, productId);
            for (int i = 0; i <100; i++)
            {

                DataTable row2 = GetRowTwo(year, productId);
                DataTable row3 = GetRowThree(monthNo, productId);
                DataTable row4 = GetRowFour(year, productId);

                if (regi_name != row1.Rows[i]["REGI_NAME"].ToString())
                {
                    TableRow ro = new TableRow();

                    ro.Cells.Add(new TableCell { Text = "TOTAL" });
                    ro.Cells.Add(new TableCell { Text = SMR_NO_APPL.ToString() });
                    ro.Cells.Add(new TableCell { Text = SMR_APPL_AMT.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_rec_qty.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_rec_amt.ToString() });
                    ro.Cells.Add(new TableCell { Text = SMA_NO_APPL.ToString() });
                    ro.Cells.Add(new TableCell { Text = SMA_APPL_AMT.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_appr_qty.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_appr_amt.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_mtq.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_mtv.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_ytq.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_ytv.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_mtqv.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_mtvv.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_ytqv.ToString() });
                    ro.Cells.Add(new TableCell { Text = s_ytvv.ToString() });
                    performTable.Rows.Add(ro);

                    TMR_NO_APPL += SMR_NO_APPL;
                    TMR_APPL_AMT += SMR_APPL_AMT;
                    TMA_NO_APPL += SMA_NO_APPL;
                    TMA_APRV_AMT += SMA_APRV_AMT; //month
                    t_rec_qty += s_rec_qty;
                    t_rec_amt += s_rec_amt;
                    t_appr_qty += s_appr_qty;
                    t_appr_amt += s_appr_amt;
                    t_mtq += s_mtq;
                    t_mtv += s_mtv;
                    t_ytq += s_ytq;
                    t_ytv += s_ytv;
                    t_mtqv += s_mtqv;
                    t_mtvv += s_mtvv;
                    t_ytqv += s_ytqv;
                    t_ytvv += s_ytvv;

                    SMR_NO_APPL = 0;
                    SMR_APPL_AMT = 0;
                    SMA_NO_APPL = 0;
                    SMA_APRV_AMT = 0; //month
                    s_rec_qty = 0;
                    s_rec_amt = 0;
                    s_appr_qty = 0;
                    s_appr_amt = 0;
                    s_mtq = 0;
                    s_mtv = 0;
                    s_ytq = 0;
                    s_ytv = 0;
                    s_mtqv = 0;
                    s_mtvv = 0;
                    s_ytqv = 0;
                    s_ytvv = 0;

                    TableRow ro1 = new TableRow();
                    regi_name = row1.Rows[i]["REGI_NAME"].ToString();
                    ro1.Cells.Add(new TableCell { Text = SMR_NO_APPL.ToString() });

                }

                TableRow ro2 = new TableRow();
                string branch = row1.Rows[i]["BR_NAME"].ToString();
                ro2.Cells.Add(new TableCell { Text = branch });
                performTable.Rows.Add(ro2);

                var rec_qty = Convert.ToInt64(row2.Rows[i]["NO_APPL"]);
                var rec_amt = Convert.ToInt64(row2.Rows[i]["APPL_AMT"]); //kkkk
                var appr_qty = Convert.ToInt64(row4.Rows[i]["NO_APPL"]);
                var appr_amt = Convert.ToInt64(row4.Rows[i]["APRV_AMT"]);

                int branchId = (int)row3.Rows[i]["BR_ID"];
                if (y == 2004)
                {
                    DataTable dt = GetOldValue(productId, branchId);
                    rec_qty = rec_qty + (int)dt.Rows[0]["RECE_QTY"];
                    rec_amt = rec_amt + (int)dt.Rows[0]["RECE_VAL"];
                    appr_qty = appr_qty + (int)dt.Rows[0]["APPR_QTY"];
                    appr_amt = appr_amt + (int)dt.Rows[0]["APPR_VAL"];
                }

                var mtq = 0; var mtv = 0; var ytq = 0; var ytv = 0;
                decimal[] getTargetData = GetTargetData(year, monthNo, productId, branchId);

                var mtqv = getTargetData[0] -Convert.ToDecimal(row3.Rows[i]["NO_APPL"]);
                var mtvv = getTargetData[1] -Convert.ToDecimal(row3.Rows[i]["APRV_AMT"]);
                var ytqv = getTargetData[2] - Convert.ToDecimal(appr_qty);
                var ytvv = getTargetData[3] - Convert.ToDecimal(appr_amt);

                decimal row1App =Convert.ToInt32(row1.Rows[i]["NO_APPL"]);
                decimal row1Amt = Convert.ToInt32(row1.Rows[i]["APPL_AMT"]);
                decimal row3App = Convert.ToInt32(row3.Rows[i]["NO_APPL"]);
                decimal row3Amt = Convert.ToInt32(row3.Rows[i]["APRV_AMT"]);

                SMR_NO_APPL += Convert.ToInt32(row1App);
                SMR_APPL_AMT += Convert.ToInt32(row1Amt);
                SMA_NO_APPL += Convert.ToInt32(row3App);
                SMA_APRV_AMT += Convert.ToInt32(row3Amt);

                s_rec_qty += rec_qty;
                s_rec_amt += rec_amt;
                s_appr_qty += appr_qty;
                s_appr_amt += appr_amt;

                s_mtq += mtq;
                s_mtv += mtv;
                s_ytq += ytq;
                s_ytv += ytv;

                s_mtqv += Convert.ToInt32(mtqv);
                s_mtvv += Convert.ToInt32(mtvv);
                s_ytqv += Convert.ToInt32(ytqv);
                s_ytvv += Convert.ToInt32(ytvv);

                ro2.Cells.Add(new TableCell { Text = row1App.ToString() });
                ro2.Cells.Add(new TableCell { Text = row1Amt.ToString() });
                ro2.Cells.Add(new TableCell { Text = rec_qty.ToString() });
                ro2.Cells.Add(new TableCell { Text = rec_amt.ToString() });
                ro2.Cells.Add(new TableCell { Text = row3App.ToString() });
                ro2.Cells.Add(new TableCell { Text = row3Amt.ToString() });
                ro2.Cells.Add(new TableCell { Text = appr_qty.ToString() });
                ro2.Cells.Add(new TableCell { Text = appr_amt.ToString() });
                ro2.Cells.Add(new TableCell { Text = mtq.ToString() });
                ro2.Cells.Add(new TableCell { Text = mtv.ToString() });
                ro2.Cells.Add(new TableCell { Text = ytq.ToString() });
                ro2.Cells.Add(new TableCell { Text = ytv.ToString() });
                ro2.Cells.Add(new TableCell { Text = mtqv.ToString() });
                ro2.Cells.Add(new TableCell { Text = mtvv.ToString() });
                ro2.Cells.Add(new TableCell { Text = ytqv.ToString() });
                ro2.Cells.Add(new TableCell { Text = ytvv.ToString() });
                performTable.Rows.Add(ro2);


                //if ($cb1)  getpersondata($y1,$m1,$ProdId,$row3->BR_ID);


                TableRow ro3 = new TableRow();
                ro3.Cells.Add(new TableCell { Text = "TOTAL" });
                ro3.Cells.Add(new TableCell { Text = SMR_NO_APPL.ToString() });
                ro3.Cells.Add(new TableCell { Text = SMR_APPL_AMT.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_rec_qty.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_rec_amt.ToString() });
                ro3.Cells.Add(new TableCell { Text = SMA_NO_APPL.ToString() });
                ro3.Cells.Add(new TableCell { Text = SMA_APPL_AMT.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_appr_qty.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_appr_amt.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_mtq.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_mtv.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_ytq.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_ytv.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_mtqv.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_mtvv.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_ytqv.ToString() });
                ro3.Cells.Add(new TableCell { Text = s_ytvv.ToString() });
                performTable.Rows.Add(ro3);


                TMR_NO_APPL += SMR_NO_APPL;
                TMR_APPL_AMT += SMR_APPL_AMT;
                TMA_NO_APPL += SMA_NO_APPL;

                TMA_APRV_AMT += SMA_APRV_AMT;
                t_rec_qty += s_rec_qty;
                t_rec_amt += s_rec_amt;
                t_appr_qty += s_appr_qty;
                t_appr_amt += s_appr_amt;
                t_mtq += s_mtq;
                t_mtv += s_mtv;
                t_ytq += s_ytq;
                t_ytv += s_ytv;
                t_mtqv += s_mtqv;
                t_mtvv += s_mtvv;
                t_ytqv += s_ytqv;
                t_ytvv += s_ytvv;
                TableRow ro4 = new TableRow();
                ro4.Cells.Add(new TableCell { Text = "TOTAL" });
                ro4.Cells.Add(new TableCell { Text = TMR_NO_APPL.ToString() });
                ro4.Cells.Add(new TableCell { Text = TMR_APPL_AMT.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_rec_qty.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_rec_amt.ToString() });
                ro4.Cells.Add(new TableCell { Text = TMA_NO_APPL.ToString() });
                ro4.Cells.Add(new TableCell { Text = TMA_APRV_AMT.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_appr_qty.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_appr_amt.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_mtq.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_mtv.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_ytq.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_ytv.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_mtqv.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_mtvv.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_ytqv.ToString() });
                ro4.Cells.Add(new TableCell { Text = t_ytvv.ToString() });
                performTable.Rows.Add(ro3);


            }

        }


        public decimal[] GetTargetData(string year, string monthNo, int productId, int branchId)
        {
            decimal[] getTargetData = new decimal[4];
            DataTable resultM = _loanReportManager.GetResultM(year, monthNo, productId, branchId);
            DataTable resultY = _loanReportManager.GetResultY(year, productId, branchId);

            if (resultM != null)
            {
                for (int i = 0; i < resultM.Rows.Count; i++)
                {
                    getTargetData[0] = Convert.ToDecimal(resultM.Rows[i]["TARG_QTY"]); // $mtq
                    getTargetData[1] = Convert.ToDecimal(resultM.Rows[i]["TARG_AMOU"]); //$mtv
                }

            }


            if (resultY != null)
            {
                for (int i = 0; i < resultY.Rows.Count; i++)
                {
                    getTargetData[2] = Convert.ToDecimal(resultY.Rows[i]["TARG_QTY"]); // $ytq
                    getTargetData[3] = Convert.ToDecimal(resultY.Rows[i]["TARG_AMOU"]); // $ytv
                }

            }
            return getTargetData;
        }



        public DataTable GetRowOne(string monthNo, int productId)
        {
            var res = _loanReportManager.GetRowOne(monthNo, productId);
            return res;
        }

        public DataTable GetRowTwo(string year, int productId)
        {
            var res = _loanReportManager.GetRowTwo(year, productId);
            return res;
        }

        public DataTable GetRowThree(string monthNo, int productId)
        {
            var res = _loanReportManager.GetRowThree(monthNo, productId);
            return res;
        }

        public DataTable GetRowFour(string year, int productId)
        {
            var res = _loanReportManager.GetRowFour(year, productId);
            return res;
        }


        public DataTable GetOldValue(int productId, int branchId)
        {
            var res = _loanReportManager.GetOldValue(productId, branchId);
            return res;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
        }



    }
}