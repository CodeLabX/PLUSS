﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BILMUE : System.Web.UI.Page
    {
        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2
        }
        #endregion

        private BILMUEManager bilMUEManagerObj;
        public PLManager pLManagerObj = null;
        public CustomerManager customerManagerObj = null;
        #region Page_Load(object sender, EventArgs e)
        protected void Page_Load(object sender, EventArgs e)
        {

            Page.Form.DefaultButton = searchButton.UniqueID;
            Page.Form.DefaultFocus = llIdTextBox.ClientID;

            bilMUEManagerObj = new BILMUEManager();
            pLManagerObj = new PLManager();
            customerManagerObj = new CustomerManager();

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);

            //mob12DropDownList_SelectedIndexChanged(sender, e);
            if (!Page.IsPostBack)
            {
                //mob12DropDownList_SelectedIndexChanged(sender, e);
                saveButton.Enabled = false;
            }
            CallJavaScriptFunction();
            //netExpTextBox.Enabled = false;
            //netSecrtyTextBox.Enabled = false;
            //netNarTextBox.Enabled = false;
            //bilGrdMueTextBox.Enabled = false;
            //netAllowedTextBox.Enabled = false;
            //availableTextBox.Enabled = false;
            mob12DropDownList.Attributes.Add("onchange", "MobDropDownSelect();");
        }
        #endregion

        #region CallJavaScriptFunction()
        /// <summary>
        /// This method provide calling the javascript funtion.
        /// </summary>
        private void CallJavaScriptFunction()
        {
            bilGrdMueTextBox.Attributes.Add("onkeyup", "CalculateNetAllowed()");
            creditCrdTextBox.Attributes.Add("onkeyup", "CalculateNetAllowed()");
            unsecuredBillTextBox.Attributes.Add("onkeyup", "CalculateNetUnsecured()");
            unsecuredOtherTextBox.Attributes.Add("onkeyup", "CalculateNetUnsecured()");
            creditCrd2TextBox.Attributes.Add("onkeyup", "CalculateNetUnsecured()");
            availableTextBox.Attributes.Add("onkeyup", "CalculateNetUnsecured()");

            //mob12DropDownList.Attributes.Add("onchange", "CalculateNetAllowed();");
            //mob12DropDownList.Attributes.Add("onchange", "CalculateNetAllowed();");

            //string ctrlName1;
            //TextBox ctrl1;
            //string ctrlName2;
            //TextBox ctrl2;
            //string ctrlName3;
            //TextBox ctrl3;
            //for (int i = 1; i <= 6; i++)
            //{
            //    ctrlName1 = "facExpsure" + i + "TextBox";
            //    ctrlName2 = "facSecrty" + i + "TextBox";
            //    ctrlName3 = "facNar" + i + "TextBox";


            facExpsure1TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure1TextBox.ClientID + "','"+facSecrty1TextBox.ClientID+"','"+facNar1TextBox.ClientID+"')");
            facSecrty1TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure1TextBox.ClientID + "','" + facSecrty1TextBox.ClientID + "','" + facNar1TextBox.ClientID + "')");

            facExpsure2TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure2TextBox.ClientID + "','" + facSecrty2TextBox.ClientID + "','" + facNar2TextBox.ClientID + "')");
            facSecrty2TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure2TextBox.ClientID + "','" + facSecrty2TextBox.ClientID + "','" + facNar2TextBox.ClientID + "')");

            facExpsure3TextBox.Attributes.Add("onkeyup", "CalculateNar('"+facExpsure3TextBox.ClientID+"','"+facSecrty3TextBox.ClientID+"','"+facNar3TextBox.ClientID+"')");
            facSecrty3TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure3TextBox.ClientID + "','" + facSecrty3TextBox.ClientID + "','" + facNar3TextBox.ClientID + "')");

            facExpsure4TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure4TextBox.ClientID + "','" + facSecrty4TextBox.ClientID + "','" + facNar4TextBox.ClientID + "')");
            facSecrty4TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure4TextBox.ClientID + "','" + facSecrty4TextBox.ClientID + "','" + facNar4TextBox.ClientID + "')");

            facExpsure5TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure5TextBox.ClientID + "','" + facSecrty5TextBox.ClientID + "','" + facNar5TextBox.ClientID + "')");
            facSecrty5TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure5TextBox.ClientID + "','" + facSecrty5TextBox.ClientID + "','" + facNar5TextBox.ClientID + "')");

            facExpsure6TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure6TextBox.ClientID + "','" + facSecrty6TextBox.ClientID + "','" + facNar6TextBox.ClientID + "')");
            facSecrty6TextBox.Attributes.Add("onkeyup", "CalculateNar('" + facExpsure6TextBox.ClientID + "','" + facSecrty6TextBox.ClientID + "','" + facNar6TextBox.ClientID + "')");
            //facExpsure1TextBox.Attributes.Add("onkeyup", "CalculateNar('ctl00_ContentPlaceHolder_" + ctrlName1 + "','ctl00_ContentPlaceHolder_" + ctrlName2 + "','ctl00_ContentPlaceHolder_" + ctrlName3 + "')");

            //facSecrty1TextBox.Attributes.Add("onkeyup", "CalculateNar('ctl00_ContentPlaceHolder_" + ctrlName1 + "','ctl00_ContentPlaceHolder_" + ctrlName2 + "','ctl00_ContentPlaceHolder_" + ctrlName3 + "')");

            //form1.Attributes.Add("onload", "CalculateNar('" + ctrl1.ClientID + "','" + ctrl2.ClientID + "','" + ctrl3.ClientID + "')");
            // }
            plExprTextBox.Attributes.Add("onkeyup", "CalculateNar('" + plExprTextBox.ClientID + "','" + plSecrtyTextBox.ClientID + "','" + plNarTextBox.ClientID + "')");
            plSecrtyTextBox.Attributes.Add("onkeyup", "CalculateNar('" + plExprTextBox.ClientID + "','" + plSecrtyTextBox.ClientID + "','" + plNarTextBox.ClientID + "')");

            crdtCrdExpTextBox.Attributes.Add("onkeyup", "CalculateNar('" + crdtCrdExpTextBox.ClientID + "','" + crdtCrdSecrtyTextBox.ClientID + "','" + crdtCrdNarTextBox.ClientID + "')");
            crdtCrdSecrtyTextBox.Attributes.Add("onkeyup", "CalculateNar('" + crdtCrdExpTextBox.ClientID + "','" + crdtCrdSecrtyTextBox.ClientID + "','" + crdtCrdNarTextBox.ClientID + "')");

            bilExpTextBox.Attributes.Add("onkeyup", "CalculateNar('" + bilExpTextBox.ClientID + "','" + bilSecrtyTextBox.ClientID + "','" + bilNarTextBox.ClientID + "')");
            bilSecrtyTextBox.Attributes.Add("onkeyup", "CalculateNar('" + bilExpTextBox.ClientID + "','" + bilSecrtyTextBox.ClientID + "','" + bilNarTextBox.ClientID + "')");

            flexiExpTextBox.Attributes.Add("onkeyup", "CalculateNar('" + flexiExpTextBox.ClientID + "','" + flexiSecrtyTextBox.ClientID + "','" + flexiNarTextBox.ClientID + "')");
            flexiSecrtyTextBox.Attributes.Add("onkeyup", "CalculateNar('" + flexiExpTextBox.ClientID + "','" + flexiSecrtyTextBox.ClientID + "','" + flexiNarTextBox.ClientID + "')");

            otherDirCrditcrdExpTextBox.Attributes.Add("onkeyup", "CalculateNar('" + otherDirCrditcrdExpTextBox.ClientID + "','" + otherDirCrditcrdSecrtyTextBox.ClientID + "','" + otherDirCrditcrdNarTextBox.ClientID + "'),CalculateNetExposure(),CalculateNetNar();");
            otherDirCrditcrdSecrtyTextBox.Attributes.Add("onkeyup", "CalculateNar('" + otherDirCrditcrdExpTextBox.ClientID + "','" + otherDirCrditcrdSecrtyTextBox.ClientID + "','" + otherDirCrditcrdNarTextBox.ClientID + "'),CalculateNetNar();");

       
        }
        #endregion

        #region searchButton_Click(object sender, EventArgs e)
        /// <summary>
        /// Search button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void searchButton_Click(object sender, EventArgs e)
        {
            string llId = Convert.ToString(llIdTextBox.Text.Trim());
            List<CustomerDetail> customerDetailListObj = new List<CustomerDetail>();
            double BilOS = 0;
            double fDValue = 0;
            double UnSecureValue = 0;
            if (String.IsNullOrEmpty(llId) || (llId.Length == 0))
            {
                //Alert.Show("Please select LLId");
                msgLabel.Text = "Please enter the LLId";
                llIdTextBox.Focus();
                return;
            }
            if (!LoadExistingBILMUE(llId))
            {
                BILMUE customerDetailDataListObj = bilMUEManagerObj.GetCustomerDetailData(llId);
                List<BILMUE> exposureListObj = bilMUEManagerObj.GetCustomerExposureData(llId);
                PL plObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llIdTextBox.Text.Trim()));
                customerDetailListObj = customerManagerObj.GetCustomerDetailInfo(llId);
                if (plObj != null)
                {
                    creditCrd2TextBox.Text = plObj.PlScbcreditcardlimit.ToString();
                    plExprTextBox.Text = plObj.PlProposedloanamount.ToString();
                    crdtCrdExpTextBox.Text = plObj.PlScbcreditcardlimit.ToString();
                    creditCardHiddenField.Value = plObj.PlScbcreditcardlimit.ToString();
                }
                if (customerDetailDataListObj != null)
                {
                    saveButton.Enabled = true;
                    msgLabel.Text = "";
                    saveButton.Text = "Save";
                    mob12DropDownList.SelectedValue = customerDetailDataListObj.Mob12.ToString();
                    mob12DropDownList_SelectedIndexChanged(sender, e);
                    if (customerDetailListObj != null)
                    {
                        foreach (CustomerDetail customerDetailObj in customerDetailListObj)
                        {
                            if (customerDetailObj.Facility.Contains("BIL [30%]") || customerDetailObj.Facility.Contains("BIL [50%]"))
                            {
                                BilOS = BilOS + customerDetailObj.OutStanding;
                                fDValue = fDValue + customerDetailObj.CashSecurity;
                            }
                            else if (!customerDetailObj.Facility.Contains("BIL [30%]") || !customerDetailObj.Facility.Contains("BIL [50%]") && customerDetailObj.Flag.Equals("U"))
                            {
                                UnSecureValue = UnSecureValue + customerDetailObj.OutStanding;
                            }
                        }
                    }
                    existingBilOSTextBox.Text = BilOS.ToString();
                    bilOSHiddenField.Value = BilOS.ToString();
                    fdValueTextBox.Text = fDValue.ToString();
                    unsecuredBillTextBox.Text = (BilOS - fDValue).ToString();
                    unsecuredOtherTextBox.Text = UnSecureValue.ToString();
                    unsecureOtherHiddenField.Value = UnSecureValue.ToString();
                    if (exposureListObj.Count > 0)
                    {
                        switch (exposureListObj.Count)
                        {
                            case 1:
                                facility1TextBox.Text = Convert.ToString("" + exposureListObj[0].Facility[0].ToString());
                                facExpsure1TextBox.Text = Convert.ToString("" + exposureListObj[0].OutStanding[0].ToString());
                                facSecrty1TextBox.Text = Convert.ToString("" + exposureListObj[0].CashSecurity[0].ToString());
                                break;
                            case 2:
                                facility1TextBox.Text = Convert.ToString("" + exposureListObj[0].Facility[0].ToString());
                                facExpsure1TextBox.Text = Convert.ToString("" + exposureListObj[0].OutStanding[0].ToString());
                                facSecrty1TextBox.Text = Convert.ToString("" + exposureListObj[0].CashSecurity[0].ToString());
                                facility2TextBox.Text = Convert.ToString("" + exposureListObj[1].Facility[0].ToString());
                                facExpsure2TextBox.Text = Convert.ToString("" + exposureListObj[1].OutStanding[0].ToString());
                                facSecrty2TextBox.Text = Convert.ToString("" + exposureListObj[1].CashSecurity[0].ToString());
                                break;
                            case 3:
                                facility1TextBox.Text = Convert.ToString("" + exposureListObj[0].Facility[0].ToString());
                                facExpsure1TextBox.Text = Convert.ToString("" + exposureListObj[0].OutStanding[0].ToString());
                                facSecrty1TextBox.Text = Convert.ToString("" + exposureListObj[0].CashSecurity[0].ToString());
                                facility2TextBox.Text = Convert.ToString("" + exposureListObj[1].Facility[0].ToString());
                                facExpsure2TextBox.Text = Convert.ToString("" + exposureListObj[1].OutStanding[0].ToString());
                                facSecrty2TextBox.Text = Convert.ToString("" + exposureListObj[1].CashSecurity[0].ToString());
                                facility3TextBox.Text = Convert.ToString("" + exposureListObj[2].Facility[0].ToString());
                                facExpsure3TextBox.Text = Convert.ToString("" + exposureListObj[2].OutStanding[0].ToString());
                                facSecrty3TextBox.Text = Convert.ToString("" + exposureListObj[2].CashSecurity[0].ToString());
                                break;
                            case 4:
                                facility1TextBox.Text = Convert.ToString("" + exposureListObj[0].Facility[0].ToString());
                                facExpsure1TextBox.Text = Convert.ToString("" + exposureListObj[0].OutStanding[0].ToString());
                                facSecrty1TextBox.Text = Convert.ToString("" + exposureListObj[0].CashSecurity[0].ToString());
                                facility2TextBox.Text = Convert.ToString("" + exposureListObj[1].Facility[0].ToString());
                                facExpsure2TextBox.Text = Convert.ToString("" + exposureListObj[1].OutStanding[0].ToString());
                                facSecrty2TextBox.Text = Convert.ToString("" + exposureListObj[1].CashSecurity[0].ToString());
                                facility3TextBox.Text = Convert.ToString("" + exposureListObj[2].Facility[0].ToString());
                                facExpsure3TextBox.Text = Convert.ToString("" + exposureListObj[2].OutStanding[0].ToString());
                                facSecrty3TextBox.Text = Convert.ToString("" + exposureListObj[2].CashSecurity[0].ToString());
                                facility4TextBox.Text = Convert.ToString("" + exposureListObj[3].Facility[0].ToString());
                                facExpsure4TextBox.Text = Convert.ToString("" + exposureListObj[3].OutStanding[0].ToString());
                                facSecrty4TextBox.Text = Convert.ToString("" + exposureListObj[3].CashSecurity[0].ToString());
                                break;
                            case 5:
                                facility1TextBox.Text = Convert.ToString("" + exposureListObj[0].Facility[0].ToString());
                                facExpsure1TextBox.Text = Convert.ToString("" + exposureListObj[0].OutStanding[0].ToString());
                                facSecrty1TextBox.Text = Convert.ToString("" + exposureListObj[0].CashSecurity[0].ToString());
                                facility2TextBox.Text = Convert.ToString("" + exposureListObj[1].Facility[0].ToString());
                                facExpsure2TextBox.Text = Convert.ToString("" + exposureListObj[1].OutStanding[0].ToString());
                                facSecrty2TextBox.Text = Convert.ToString("" + exposureListObj[1].CashSecurity[0].ToString());
                                facility3TextBox.Text = Convert.ToString("" + exposureListObj[2].Facility[0].ToString());
                                facExpsure3TextBox.Text = Convert.ToString("" + exposureListObj[2].OutStanding[0].ToString());
                                facSecrty3TextBox.Text = Convert.ToString("" + exposureListObj[2].CashSecurity[0].ToString());
                                facility4TextBox.Text = Convert.ToString("" + exposureListObj[3].Facility[0].ToString());
                                facExpsure4TextBox.Text = Convert.ToString("" + exposureListObj[3].OutStanding[0].ToString());
                                facSecrty4TextBox.Text = Convert.ToString("" + exposureListObj[3].CashSecurity[0].ToString());
                                facility5TextBox.Text = Convert.ToString("" + exposureListObj[4].Facility[0].ToString());
                                facExpsure5TextBox.Text = Convert.ToString("" + exposureListObj[4].OutStanding[0].ToString());
                                facSecrty5TextBox.Text = Convert.ToString("" + exposureListObj[4].CashSecurity[0].ToString());
                                break;
                        }
                    }
                }
                else
                {
                    //Alert.Show("Data not found");
                    msgLabel.Text = "Data not found";
                    saveButton.Enabled = true;
                    saveButton.Text = "Save";
                    //ClearField();
                    ClearTextBoxs(this);
                    llIdTextBox.Text = llId;
                    llIdTextBox.Focus();
                    //mob12DropDownList_SelectedIndexChanged(sender, e);
                    return;
                }
            }
        }
        #endregion
        private bool LoadExistingBILMUE(string llId)
        {
            BILMUE bilMueObj = bilMUEManagerObj.GetBILMUEData(llId);
            if (bilMueObj == null)
            {
                return false;
            }
            else
            {
                saveButton.Enabled = true;
                msgLabel.Text = "";
                saveButton.Text = "Update";
                idHiddenField.Value = bilMueObj.Id.ToString();
                mob12DropDownList.SelectedValue = bilMueObj.Mob12.ToString();
                bilGrdMueTextBox.Text = bilMueObj.BilGuardedMue.ToString();
                creditCrdTextBox.Text = bilMueObj.CreditCard.ToString();
                netAllowedTextBox.Text = bilMueObj.NetAllowed.ToString();
                existingBilOSTextBox.Text = bilMueObj.ExistingBilOs.ToString();
                fdValueTextBox.Text = bilMueObj.FdValue.ToString();
                unsecuredBillTextBox.Text = bilMueObj.UnsecuredBil.ToString();
                unsecuredOtherTextBox.Text = bilMueObj.UnsecuredOther.ToString();
                creditCrd2TextBox.Text = bilMueObj.CreditCard2.ToString();
                netUnsecuredTextBox.Text = bilMueObj.NetUnsecured.ToString();
                availableTextBox.Text = bilMueObj.Available.ToString();

                facility1TextBox.Text = bilMueObj.Facility1;
                facExpsure1TextBox.Text = bilMueObj.Exposure1.ToString();
                facSecrty1TextBox.Text = bilMueObj.Security1.ToString();
                facNar1TextBox.Text = bilMueObj.Nar1.ToString();

                facility2TextBox.Text = bilMueObj.Facility2;
                facExpsure2TextBox.Text = bilMueObj.Exposure2.ToString();
                facSecrty2TextBox.Text = bilMueObj.Security2.ToString();
                facNar2TextBox.Text = bilMueObj.Nar2.ToString();

                facility3TextBox.Text = bilMueObj.Facility3;
                facExpsure3TextBox.Text = bilMueObj.Exposure3.ToString();
                facSecrty3TextBox.Text = bilMueObj.Security3.ToString();
                facNar3TextBox.Text = bilMueObj.Nar3.ToString();

                facility4TextBox.Text = bilMueObj.Facility4;
                facExpsure4TextBox.Text = bilMueObj.Exposure4.ToString();
                facSecrty4TextBox.Text = bilMueObj.Security4.ToString();
                facNar4TextBox.Text = bilMueObj.Nar4.ToString();

                facility5TextBox.Text = bilMueObj.Facility5;
                facExpsure5TextBox.Text = bilMueObj.Exposure5.ToString();
                facSecrty5TextBox.Text = bilMueObj.Security5.ToString();
                facNar5TextBox.Text = bilMueObj.Nar5.ToString();

                facility6TextBox.Text = bilMueObj.Facility6;
                facExpsure6TextBox.Text = bilMueObj.Exposure6.ToString();
                facSecrty6TextBox.Text = bilMueObj.Security6.ToString();
                facNar6TextBox.Text = bilMueObj.Nar6.ToString();

                plExprTextBox.Text = bilMueObj.PLExposure.ToString();
                plSecrtyTextBox.Text = bilMueObj.PLSecurity.ToString();
                plNarTextBox.Text = bilMueObj.PLNar.ToString();
                crdtCrdExpTextBox.Text = bilMueObj.CCExposure.ToString();
                crdtCrdSecrtyTextBox.Text = bilMueObj.CCSecurity.ToString();
                crdtCrdNarTextBox.Text = bilMueObj.CCNar.ToString();

                bilExpTextBox.Text = bilMueObj.OtherBilExposure.ToString();
                bilSecrtyTextBox.Text = bilMueObj.OtherBilSecurity.ToString();
                bilNarTextBox.Text = bilMueObj.OtherBilNar.ToString();
                flexiExpTextBox.Text = bilMueObj.OtherPLIPEFLexiExposure.ToString();
                flexiSecrtyTextBox.Text = bilMueObj.OtherPLIPEFLexiSecurity.ToString();
                flexiNarTextBox.Text = bilMueObj.OtherPLIPEFLexiNar.ToString();
                otherDirCrditcrdExpTextBox.Text = bilMueObj.OtherCCExposure.ToString();
                otherDirCrditcrdSecrtyTextBox.Text = bilMueObj.OtherCCSecurity.ToString();
                otherDirCrditcrdNarTextBox.Text = bilMueObj.OtherCCNar.ToString();

                netExpTextBox.Text = bilMueObj.NetExposure.ToString();
                netSecrtyTextBox.Text = bilMueObj.NetSecurity.ToString();
                netNarTextBox.Text = bilMueObj.NetNar.ToString();
                PL plObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llIdTextBox.Text.Trim()));
                if (plObj != null)
                {
                    //&& plObj.UserId.ToString() == Session["Id"].ToString()
                    if (plObj.Status == 0)
                    {
                        saveButton.Enabled = true;
                    }
                    else
                    {
                        saveButton.Enabled = false;
                    }
                }
                return true;
            }
        }
        #region mob12DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        /// <summary>
        /// Mob12 SelectedIndexChanged event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void mob12DropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (mob12DropDownList.SelectedValue == "1")
            {
                bilGrdMueTextBox.Text = "4900000";
            }
            else if (mob12DropDownList.SelectedValue == "0")
            {
                bilGrdMueTextBox.Text = "3500000";
            }
            //ScriptManager.RegisterStartupScript(mob12DropDownList, this.GetType(), "mobDropDown", "CalculateNetAllowed();", false);
        }
        #endregion

        #region saveButton_Click(object sender, EventArgs e)
        /// <summary>
        /// Save button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                BILMUE bilMueObj = new BILMUE();
                int insertOrUpdateRow = -1;
                string llId = llIdTextBox.Text.Trim();
                if (String.IsNullOrEmpty(llId))
                {
                    msgLabel.Text = "Please select LLId";
                    llIdTextBox.Focus();
                    return;
                }
                msgLabel.Text = "";
                Int32 id = Convert.ToInt32("0" + idHiddenField.Value);
                bilMueObj.Id = id;
                bilMueObj.LLId = llId;
                bilMueObj.Mob12 = Convert.ToInt32("0" + mob12DropDownList.SelectedValue.ToString());
                bilMueObj.BilGuardedMue = Convert.ToDouble("0" + bilGrdMueTextBox.Text.Trim());
                bilMueObj.CreditCard = Convert.ToDouble("0" + creditCrdTextBox.Text.Trim());
                bilMueObj.NetAllowed = Convert.ToDouble("0" + netAllowedTextBox.Text);
                if (!String.IsNullOrEmpty(existingBilOSTextBox.Text.Trim()))
                {
                    bilMueObj.ExistingBilOs = Convert.ToDouble(existingBilOSTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.ExistingBilOs = Convert.ToDouble("0" + existingBilOSTextBox.Text.Trim());
                }
                if (!String.IsNullOrEmpty(fdValueTextBox.Text.Trim()))
                {
                    bilMueObj.FdValue = Convert.ToDouble(fdValueTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.FdValue = Convert.ToDouble("0" + fdValueTextBox.Text.Trim());
                }
                if (!String.IsNullOrEmpty(unsecuredBillTextBox.Text.Trim()))
                {
                    bilMueObj.UnsecuredBil = Convert.ToDouble(unsecuredBillTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.UnsecuredBil = Convert.ToDouble("0" + unsecuredBillTextBox.Text.Trim());
                }
                if (!String.IsNullOrEmpty(unsecuredOtherTextBox.Text.Trim()))
                {
                    bilMueObj.UnsecuredOther = Convert.ToDouble(unsecuredOtherTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.UnsecuredOther = Convert.ToDouble("0" + unsecuredOtherTextBox.Text.Trim());
                }
                if (!String.IsNullOrEmpty(creditCrd2TextBox.Text.Trim()))
                {
                    bilMueObj.CreditCard2 = Convert.ToDouble(creditCrd2TextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.CreditCard2 = Convert.ToDouble("0" + creditCrd2TextBox.Text.Trim());
                }
                if (!String.IsNullOrEmpty(netUnsecuredTextBox.Text))
                {
                    bilMueObj.NetUnsecured = Convert.ToDouble(netUnsecuredTextBox.Text);
                }
                else
                {
                    bilMueObj.NetUnsecured = Convert.ToDouble("0" + netUnsecuredTextBox.Text);
                }
                if (!String.IsNullOrEmpty(availableTextBox.Text))
                {
                    bilMueObj.Available = Convert.ToDouble(availableTextBox.Text);
                }
                else
                {
                    bilMueObj.Available = Convert.ToDouble("0" + availableTextBox.Text);
                }
                bilMueObj.Facility1 = Convert.ToString(facility1TextBox.Text.Trim());
                bilMueObj.Exposure1 = Convert.ToDouble("0" + facExpsure1TextBox.Text.Trim());
                if (!String.IsNullOrEmpty(facSecrty1TextBox.Text.Trim()))
                {
                }
                else
                {
                    bilMueObj.Security1 = Convert.ToDouble("0" + facSecrty1TextBox.Text.Trim());
                }
                if(!String.IsNullOrEmpty(facNar1TextBox.Text.Trim())) 
                {
                    bilMueObj.Nar1 = Convert.ToDouble(facNar1TextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.Nar1 = Convert.ToDouble("0" + facNar1TextBox.Text.Trim());
                }

                bilMueObj.Facility2 = Convert.ToString(facility2TextBox.Text.Trim());
                bilMueObj.Exposure2 = Convert.ToDouble("0" + facExpsure2TextBox.Text.Trim());
                bilMueObj.Security2 = Convert.ToDouble("0" + facSecrty2TextBox.Text.Trim());
                if (!String.IsNullOrEmpty(facNar2TextBox.Text.Trim()))
                {
                    bilMueObj.Nar2 = Convert.ToDouble(facNar2TextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.Nar2 = Convert.ToDouble("0" + facNar2TextBox.Text.Trim());
                }
                //bilMueObj.Nar2 = Convert.ToDouble("0" + facNar2TextBox.Text.Trim());

                bilMueObj.Facility3 = Convert.ToString(facility3TextBox.Text.Trim());
                bilMueObj.Exposure3 = Convert.ToDouble("0" + facExpsure3TextBox.Text.Trim());
                bilMueObj.Security3 = Convert.ToDouble("0" + facSecrty3TextBox.Text.Trim());
                if (!String.IsNullOrEmpty(facNar3TextBox.Text.Trim()))
                {
                    bilMueObj.Nar3 = Convert.ToDouble(facNar3TextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.Nar3 = Convert.ToDouble("0" + facNar3TextBox.Text.Trim());
                }
                //bilMueObj.Nar3 = Convert.ToDouble("0" + facNar3TextBox.Text.Trim());

                bilMueObj.Facility4 = Convert.ToString(facility4TextBox.Text.Trim());
                bilMueObj.Exposure4 = Convert.ToDouble("0" + facExpsure4TextBox.Text.Trim());
                bilMueObj.Security4 = Convert.ToDouble("0" + facSecrty4TextBox.Text.Trim());
                if (!String.IsNullOrEmpty(facNar4TextBox.Text.Trim()))
                {
                    bilMueObj.Nar4 = Convert.ToDouble(facNar4TextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.Nar4 = Convert.ToDouble("0" + facNar4TextBox.Text.Trim());
                }
                //bilMueObj.Nar4 = Convert.ToDouble("0" + facNar4TextBox.Text.Trim());

                bilMueObj.Facility5 = Convert.ToString(facility5TextBox.Text.Trim());
                bilMueObj.Exposure5 = Convert.ToDouble("0" + facExpsure5TextBox.Text.Trim());
                bilMueObj.Security5 = Convert.ToDouble("0" + facSecrty5TextBox.Text.Trim());
                if (!String.IsNullOrEmpty(facNar5TextBox.Text.Trim()))
                {
                    bilMueObj.Nar5 = Convert.ToDouble(facNar5TextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.Nar5 = Convert.ToDouble("0" + facNar5TextBox.Text.Trim());
                }
                //bilMueObj.Nar5 = Convert.ToDouble("0" + facNar5TextBox.Text.Trim());

                bilMueObj.Facility6 = Convert.ToString(facility6TextBox.Text.Trim());
                bilMueObj.Exposure6 = Convert.ToDouble("0" + facExpsure6TextBox.Text.Trim());
                bilMueObj.Security6 = Convert.ToDouble("0" + facSecrty6TextBox.Text.Trim());
                if (!String.IsNullOrEmpty(facNar6TextBox.Text.Trim()))
                {
                    bilMueObj.Nar6 = Convert.ToDouble(facNar6TextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.Nar6 = Convert.ToDouble("0" + facNar6TextBox.Text.Trim());
                }
                //bilMueObj.Nar6 = Convert.ToDouble("0" + facNar6TextBox.Text.Trim());

                bilMueObj.PLExposure = Convert.ToDouble("0" + plExprTextBox.Text.Trim());
                bilMueObj.PLSecurity = Convert.ToDouble("0" + plSecrtyTextBox.Text.Trim());
                if (!String.IsNullOrEmpty(plNarTextBox.Text.Trim()))
                {
                    bilMueObj.PLNar = Convert.ToDouble(plNarTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.PLNar = Convert.ToDouble("0" + plNarTextBox.Text.Trim());
                }
                //bilMueObj.PLNar = Convert.ToDouble("0" + plNarTextBox.Text.Trim());

                bilMueObj.CCExposure = Convert.ToDouble("0" + crdtCrdExpTextBox.Text.Trim());
                bilMueObj.CCSecurity = Convert.ToDouble("0" + crdtCrdSecrtyTextBox.Text.Trim());
                if (!String.IsNullOrEmpty(crdtCrdNarTextBox.Text.Trim()))
                {
                    bilMueObj.CCNar = Convert.ToDouble(crdtCrdNarTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.CCNar = Convert.ToDouble("0" + crdtCrdNarTextBox.Text.Trim());
                }
                //bilMueObj.CCNar = Convert.ToDouble("0" + crdtCrdNarTextBox.Text.Trim());

                bilMueObj.OtherBilExposure = Convert.ToDouble("0" + bilExpTextBox.Text.Trim());
                bilMueObj.OtherBilSecurity = Convert.ToDouble("0" + bilSecrtyTextBox.Text.Trim());
                if (!String.IsNullOrEmpty(bilNarTextBox.Text.Trim()))
                {
                    bilMueObj.OtherBilNar = Convert.ToDouble(bilNarTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.OtherBilNar = Convert.ToDouble("0" + bilNarTextBox.Text.Trim());
                }
                //bilMueObj.OtherBilNar = Convert.ToDouble("0" + bilNarTextBox.Text.Trim());

                bilMueObj.OtherPLIPEFLexiExposure = Convert.ToDouble("0" + flexiExpTextBox.Text.Trim());
                bilMueObj.OtherPLIPEFLexiSecurity = Convert.ToDouble("0" + flexiSecrtyTextBox.Text.Trim());
                if (!String.IsNullOrEmpty(flexiNarTextBox.Text.Trim()))
                {
                    bilMueObj.OtherPLIPEFLexiNar = Convert.ToDouble(flexiNarTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.OtherPLIPEFLexiNar = Convert.ToDouble("0" + flexiNarTextBox.Text.Trim());
                }
                //bilMueObj.OtherPLIPEFLexiNar = Convert.ToDouble("0" + flexiNarTextBox.Text.Trim());

                bilMueObj.OtherCCExposure = Convert.ToDouble("0" + otherDirCrditcrdExpTextBox.Text.Trim());
                bilMueObj.OtherCCSecurity = Convert.ToDouble("0" + otherDirCrditcrdSecrtyTextBox.Text.Trim());
                if (!String.IsNullOrEmpty(otherDirCrditcrdNarTextBox.Text.Trim()))
                {
                    bilMueObj.OtherCCNar = Convert.ToDouble(otherDirCrditcrdNarTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.OtherCCNar = Convert.ToDouble("0" + otherDirCrditcrdNarTextBox.Text.Trim());
                }
                //bilMueObj.OtherCCNar = Convert.ToDouble("0" + otherDirCrditcrdNarTextBox.Text.Trim());

                bilMueObj.NetExposure = Convert.ToDouble("0" + netExpTextBox.Text.Trim());
                bilMueObj.NetSecurity = Convert.ToDouble("0" + netSecrtyTextBox.Text.Trim());
                if (!String.IsNullOrEmpty(netNarTextBox.Text.Trim()))
                {
                    bilMueObj.NetNar = Convert.ToDouble(netNarTextBox.Text.Trim());
                }
                else
                {
                    bilMueObj.NetNar = Convert.ToDouble("0" + netNarTextBox.Text.Trim());
                }
                //bilMueObj.NetNar = Convert.ToDouble("0" + netNarTextBox.Text);

                insertOrUpdateRow = bilMUEManagerObj.InsertORUpdateBilMueInfo(bilMueObj);

                if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT))
                {
                    msgLabel.Text = "Save sucessfully.";
                    saveButton.Text = "Save";
                    //ClearField();
                    ClearTextBoxs(this);
                    llIdTextBox.Focus();
                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERTERROR))
                {
                    msgLabel.Text = "Data not saved sucessfully.";
                    llIdTextBox.Focus();
                    new AT(this).AuditAndTraial("Maximum Unsecured Exposure", msgLabel.Text);
                    return;
                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                {
                    msgLabel.Text = "Update sucessfully.";
                    saveButton.Text = "Save";
                    //ClearField();
                    ClearTextBoxs(this);
                    llIdTextBox.Focus();
                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                {
                    msgLabel.Text = "Data not update sucessfully.";
                    llIdTextBox.Focus();
                    new AT(this).AuditAndTraial("Maximum Unsecured Exposure", msgLabel.Text);
                    return;
                }
                new AT(this).AuditAndTraial("Maximum Unsecured Exposure", msgLabel.Text);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Maximum Unsecured Exposure for BIL Customers ");  
            }
        }
        #endregion 

        #region ClearField()
        private void ClearField()
        {
            /*  idHiddenField.Value = "";
          llIdTextBox.Text = "";
          mob12DropDownList.SelectedValue = "1";
          bilGrdMueTextBox.Text = "4900000";
          creditCrdTextBox.Text = "";
          netAllowedTextBox.Text = "";
          existingBilOSTextBox.Text = "";
          fdValueTextBox.Text = "";
          unsecuredBillTextBox.Text = "";
          unsecuredOtherTextBox.Text = "";
          creditCrd2TextBox.Text = "";
          netUnsecuredTextBox.Text = "";
          availableTextBox.Text = "";

          facility1TextBox.Text = "";
          facility2TextBox.Text = "";
          facility3TextBox.Text = "";
          facility4TextBox.Text = "";
          facility5TextBox.Text = "";
          facility6TextBox.Text = "";
          string ctrlName;
          TextBox ctrl;
          for (int i = 1; i <= 6; i++)
          {
              ctrlName = "facExpsure" + i + "TextBox";
              ctrl = (TextBox)FindControl(ctrlName);
              ctrl.Text = "";
          }*/
            //facExpsure1TextBox.Text = "";
            //facExpsure2TextBox.Text = "";
            //facExpsure3TextBox.Text = "";
            //facExpsure4TextBox.Text = "";
            //facExpsure5TextBox.Text = "";
            //facExpsure6TextBox.Text = "";
        }
        #endregion

        #region clearButton_Click(object sender, EventArgs e)
        /// <summary>
        /// Clear button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearTextBoxs(this);
            msgLabel.Text = "";
            llIdTextBox.Focus();
            saveButton.Enabled = true;
        }
        #endregion

        #region ClearTextBoxs(Control ctrl)
        /// <summary>
        /// This method provide clear all textbox.
        /// </summary>
        /// <param name="ctrl">Gets control</param>
        private void ClearTextBoxs(Control ctrl)
        {
            foreach (Control eachCtrl in ctrl.Controls)
            {
                if (eachCtrl.HasControls())
                {
                    ClearTextBoxs(eachCtrl);
                }
                else if (eachCtrl is TextBox)
                {
                    ((TextBox)eachCtrl).Text = "";
                }
            }
            mob12DropDownList.SelectedValue = "1";
            bilGrdMueTextBox.Text = "4900000";
        }
        #endregion
    }
}
