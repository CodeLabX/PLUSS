﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_SectorUI : System.Web.UI.Page
    {
        public SectorManager sectorManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            sectorManagerObj = new SectorManager();
            if (!Page.IsPostBack)
            {
                LoadData("");
                sectorIdTextBox.Text = sectorManagerObj.SelectMaxScetorId().ToString();
                sectorIdHidden.Value = sectorIdTextBox.Text;
            }
        }
        private void LoadData(string searchKey)
        {
           
            List<Sector> sectorObjList = new List<Sector>();
           
            if (searchKey == "")
            {
                sectorObjList = sectorManagerObj.SelectSectorList();
            }
            else
            {
                sectorObjList = sectorManagerObj.SelectSectorList(searchKey);
            }
            try
            {
                var xml = "<xml id='xml_data'><rows>";
                foreach (Sector sectorObj in sectorObjList)
                {
                    xml+="<row id='" + sectorObj.Id.ToString() + "'>";

                    xml+="<cell>";
                    xml+=sectorObj.Id.ToString();
                    xml+="</cell>";

                    xml+="<cell>";
                    xml+=AddSlash(sectorObj.Name.ToString());
                    xml+="</cell>";
                    if (sectorObj.Status == 1)
                    {
                        xml+="<cell>";
                        xml+="Active";
                        xml+="</cell>";
                    }
                    else
                    {
                        xml+="<cell>";
                        xml+="Inactive";
                        xml+="</cell>";
                    }
                    xml+="</row>";
                }

                xml += "</rows></xml>";
                dvXml.InnerHtml = xml;
            }
            finally
            {
              
            }
          
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                int sectorInfo = 0;
                Sector sectorObj = new Sector();
                if (IsBlank())
                {
                    sectorObj.Id = Convert.ToInt32(sectorIdHidden.Value);
                    sectorObj.Name = Convert.ToString(sectorNameTextBox.Text);
                    if (sectrorStatusDropDownList.SelectedValue == "Active")
                    {
                        sectorObj.Status = 1;
                    }
                    else
                    {
                        sectorObj.Status = 0;
                    }
                    sectorInfo = sectorManagerObj.SendSectorInfoInToDB(sectorObj);
                    var msg = "";
                    if (sectorInfo == INSERTE)
                    {
                        msg = "Save successfully";
                        LoadData("");
                        FieldClear();
                    }
                    else if (sectorInfo == UPDATE)
                    {
                        msg = "Update successfully";
                        LoadData("");
                        FieldClear();
                    }
                    else
                    {
                        msg = "Error in data!";
                    }
                    Alert.Show(msg);
                    new AT(this).AuditAndTraial("Bank Information", msg);
                }
                else
                {
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Sector");
            }
        }
        private bool IsBlank()
        {
            if (sectorIdTextBox.Text == "")
            {
                Alert.Show("Please insert sector Id");
                sectorIdTextBox.Focus();
                return false;
            }
            else if (sectorNameTextBox.Text == "")
            {
                Alert.Show("Please insert sector Name");
                sectorNameTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            FieldClear();
        }
        private void FieldClear()
        {
            sectorIdTextBox.Text = sectorManagerObj.SelectMaxScetorId().ToString();
            sectorIdHidden.Value = sectorIdTextBox.Text;
            sectorNameTextBox.Text = "";
            sectrorStatusDropDownList.SelectedIndex = 0;
        }
        protected void searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                string searchKey = null;
                if (searchTextBox.Text != "")
                {
                    if (searchTextBox.Text == "Active")
                    {
                        searchKey = "1";
                    }
                    else if (searchTextBox.Text == "Inactive")
                    {
                        searchKey = "0";
                    }
                    searchKey = searchTextBox.Text;
                    LoadData(searchKey);
                }
                else
                {
                    LoadData("");
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Search Sector");
            }
        }
    }
}
