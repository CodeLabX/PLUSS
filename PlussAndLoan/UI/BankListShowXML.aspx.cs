﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_BankListShowXML : System.Web.UI.Page
    {
        public ApplicantOtherBankAccount otherBankAccountObj = null;
        public BankStatementManager bankStatementManagerObj = null;
        public BankStatement bankStatementObj = null;
        public SCBBankNameListManager SCBBNListManagerObj = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            otherBankAccountObj = new ApplicantOtherBankAccount();
            bankStatementManagerObj = new BankStatementManager();
            SCBBNListManagerObj = new SCBBankNameListManager();
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count != 0)
                {
                    Int64 LLId = Convert.ToInt64(Request.QueryString["numLLId"]);
                    LoadBankNameList(LLId);
                }
            }

        }
        private void LoadBankNameList(Int64 LLId)
        {
            int ApplicationStatus = 0;
            string entryUserName = "";
            List<BankNameList> otherBankAccountObjList = new List<BankNameList>();
            otherBankAccountObj = bankStatementManagerObj.SelectLoanApplicationOthreBankInfo(LLId);
            List<BankNameList> SCBBNListObjList = new List<BankNameList>();
            // problem
            SCBBNListObjList = SCBBNListManagerObj.GetSCBBankNameList(LLId.ToString());
            if (otherBankAccountObj != null)
            {
                if (otherBankAccountObj.OtherBankId1 > 0)
                {
                    BankNameList bankNameList1Obj = new BankNameList();
                    bankNameList1Obj.OtherBankId = otherBankAccountObj.OtherBankId1;
                    bankNameList1Obj.OtherBank = otherBankAccountObj.OtherBank1.ToString();
                    bankNameList1Obj.OtherBranchId = otherBankAccountObj.OtherBranchId1;
                    bankNameList1Obj.OtherBranch = otherBankAccountObj.OtherBranch1;
                    bankNameList1Obj.OtherAccNo = otherBankAccountObj.OtherAccNo1.ToString();
                    bankNameList1Obj.OtherAccType = otherBankAccountObj.OtherAccType1.ToString();
                    otherBankAccountObjList.Add(bankNameList1Obj);
                }
                if (otherBankAccountObj.OtherBankId2 > 0)
                {
                    BankNameList bankNameList2Obj = new BankNameList();
                    bankNameList2Obj.OtherBankId = otherBankAccountObj.OtherBankId2;
                    bankNameList2Obj.OtherBank = otherBankAccountObj.OtherBank2.ToString();
                    bankNameList2Obj.OtherBranchId = otherBankAccountObj.OtherBranchId2;
                    bankNameList2Obj.OtherBranch = otherBankAccountObj.OtherBranch2;
                    bankNameList2Obj.OtherAccNo = otherBankAccountObj.OtherAccNo2.ToString();
                    bankNameList2Obj.OtherAccType = otherBankAccountObj.OtherAccType2.ToString();
                    otherBankAccountObjList.Add(bankNameList2Obj);
                }
                if (otherBankAccountObj.OtherBankId3 > 0)
                {
                    BankNameList bankNameList3Obj = new BankNameList();
                    bankNameList3Obj.OtherBankId = otherBankAccountObj.OtherBankId3;
                    bankNameList3Obj.OtherBank = otherBankAccountObj.OtherBank3.ToString();
                    bankNameList3Obj.OtherBranchId = otherBankAccountObj.OtherBranchId3;
                    bankNameList3Obj.OtherBranch = otherBankAccountObj.OtherBranch3;
                    bankNameList3Obj.OtherAccNo = otherBankAccountObj.OtherAccNo3.ToString();
                    bankNameList3Obj.OtherAccType = otherBankAccountObj.OtherAccType3.ToString();
                    otherBankAccountObjList.Add(bankNameList3Obj);
                }

                if (otherBankAccountObj.OtherBankId4 > 0)
                {
                    BankNameList bankNameList4Obj = new BankNameList();
                    bankNameList4Obj.OtherBankId = otherBankAccountObj.OtherBankId4;
                    bankNameList4Obj.OtherBank = otherBankAccountObj.OtherBank4.ToString();
                    bankNameList4Obj.OtherBranchId = otherBankAccountObj.OtherBranchId4;
                    bankNameList4Obj.OtherBranch = otherBankAccountObj.OtherBranch4;
                    bankNameList4Obj.OtherAccNo = otherBankAccountObj.OtherAccNo4.ToString();
                    bankNameList4Obj.OtherAccType = otherBankAccountObj.OtherAccType4.ToString();
                    otherBankAccountObjList.Add(bankNameList4Obj);
                }
                if (otherBankAccountObj.OtherBankId5 > 0)
                {
                    BankNameList bankNameList5Obj = new BankNameList();
                    bankNameList5Obj.OtherBankId = otherBankAccountObj.OtherBankId5;
                    bankNameList5Obj.OtherBank = otherBankAccountObj.OtherBank5.ToString();
                    bankNameList5Obj.OtherBranchId = otherBankAccountObj.OtherBranchId5;
                    bankNameList5Obj.OtherBranch = otherBankAccountObj.OtherBranch5;
                    bankNameList5Obj.OtherAccNo = otherBankAccountObj.OtherAccNo5.ToString();
                    bankNameList5Obj.OtherAccType = otherBankAccountObj.OtherAccType5.ToString();
                    otherBankAccountObjList.Add(bankNameList5Obj);
                }

                ApplicationStatus = otherBankAccountObj.ApplicationStatus;
                entryUserName = bankStatementManagerObj.GetLLIdEntryUserId(LLId);
            }
            if (SCBBNListObjList != null)
            {
                if (SCBBNListObjList.Count > 0)
                {
                    otherBankAccountObjList.InsertRange(otherBankAccountObjList.Count, SCBBNListObjList);
                }
            }
            //Response.Write(otherBankAccountObjList.Count.ToString() + "," + ApplicationStatus + "," + entryUserName);
            int rowIndex = 1;
            Response.ContentType = "text/xml";
            StringBuilder xmlDoc = new StringBuilder();
            xmlDoc.AppendLine("<?xml version='1.0' encoding='UTF-8'?>");
            xmlDoc.AppendLine("<rows>");
            if (otherBankAccountObjList.Count > 0)
            {
                try
                {
                    foreach (BankNameList bankNameObj in otherBankAccountObjList)
                    {
                        xmlDoc.AppendLine("<row id='" + rowIndex.ToString() + "'>"); ;
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(rowIndex.ToString());
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(bankNameObj.OtherBankId.ToString());
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(bankNameObj.OtherBank.ToString());
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(bankNameObj.OtherBranchId.ToString());
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(bankNameObj.OtherBranch.ToString());
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(bankNameObj.OtherAccNo.ToString());
                        xmlDoc.AppendLine("</cell>");
                        int AccountTypeId = 0;
                        switch (bankNameObj.OtherAccType.Trim().ToUpper().Replace(" ", ""))
                        {
                            case "CURRENT":
                                AccountTypeId = 1;
                                break;
                            case "SAVING":
                                AccountTypeId = 2;
                                break;
                            case "CALL":
                                AccountTypeId = 3;
                                break;
                            case "OD":
                                AccountTypeId = 4;
                                break;
                            case "SOD":
                                AccountTypeId = 5;
                                break;
                            case "SALARY":
                                AccountTypeId = 7;
                                break;
                            case "Others":
                                AccountTypeId = 6;
                                break;
                            default:
                                AccountTypeId = 6;
                                break;
                        }
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(AccountTypeId.ToString());
                        xmlDoc.AppendLine("</cell>");
 
                        string AcName = "";
                        string startMonth = "";
                        string endMonth = "";
                        if (bankNameObj.OtherBank.Equals("SCB"))
                        {
                            AcName = bankNameObj.OtherAcName.ToString();
                            startMonth = Convert.ToDateTime(bankNameObj.EndMonth).ToString("MMM-yy");
                            endMonth = Convert.ToDateTime(bankNameObj.StartMonth).ToString("MMM-yy");
                        }
                        else
                        {
                            BankStatement bankSMEObj = new BankStatement();
                            bankSMEObj = bankStatementManagerObj.GetBankStatement(LLId, (Int16)bankNameObj.OtherBankId, (Int16)bankNameObj.OtherBranchId, bankNameObj.OtherAccNo);
                            if (bankSMEObj != null)
                            {
                                AcName = bankSMEObj.ACName.ToString();

                                if (bankSMEObj.StartMonth != "" & bankSMEObj.EndMonth != "")
                                {
                                    startMonth = bankSMEObj.StartMonth;
                                    endMonth = bankSMEObj.EndMonth;
                                    //DateTime startMonth = Convert.ToDateTime(bankSMEObj.StartMonth);
                                    //endMonth = Convert.ToDateTime(bankSMEObj.EndMonth);
                                }
                            }
                        }
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(AcName);
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(startMonth);
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(endMonth);
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(startMonth);
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(LLId.ToString());
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(bankNameObj.OtherAccType.ToString());
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine("Load^javascript:JS_FunctionLoadCalculationData()^_self");
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("<cell>");
                        xmlDoc.AppendLine(bankNameObj.MasterId.ToString());
                        xmlDoc.AppendLine("</cell>");
                        xmlDoc.AppendLine("</row>");
                        rowIndex = rowIndex + 1;
                    }
                    xmlDoc.AppendLine("</rows>");
                }
                finally
                {
                }
            }
            else
            {
                xmlDoc.AppendLine("</rows>");
            }
            Response.Write(xmlDoc.ToString());
        }
        public string EndMonth(string startMonth)
        {
            string endMonth = null;
            if (startMonth.Length > 0)
            {
                DateTime getDate = DateTime.Parse("01-" + startMonth);
                int month = getDate.Month;
                for (int i = 0; i < 12; i++)
                {
                    endMonth = getDate.AddMonths(i).ToString("MMM-yy");
                }
            }
            return endMonth;
        }
   
    
    
    }
}
