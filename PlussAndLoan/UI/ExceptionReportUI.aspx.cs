﻿using System;

namespace PlussAndLoan.UI
{
    public partial class UI_ExceptionReportUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                startDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
                endDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }

        }
        protected void nextButton_Click(object sender, EventArgs e)
        {
            if (startDateTextBox.Text == null)
            {
                errMsgLabel.Text = "Please inser Start Date";
            }
            else if (endDateTextBox.Text == null)
            {
                errMsgLabel.Text = "Please inser End Date";
            }
            else
            {
                Session["ReportType"] = "ExceptionReport";
                Session["fromDate"] = startDateTextBox.Text;
                Session["toDate"] = endDateTextBox.Text;
                Response.Redirect("~/Reports/ReportViewer.aspx");
            }


        }
    }
}
