﻿using System;
using System.Collections.Generic;
using AjaxPro;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class AutoLoanSummaryForSales4444 : System.Web.UI.Page
    {
        [AjaxNamespace("AutoLoanSummaryForSales4444")]
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(AutoLoanSummaryForSales4444));
        }
        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public List<AutoLoanSummary> GetLoanSummary(int pageNo, int pageSize, Int32 searchKey)
        {
            try
            {
                pageNo = pageNo * pageSize;
                AutoLoanAssessmentRrepository repository = new AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                List<AutoLoanSummary> objLoanSummaryList =
                    autoLoanAssessmentService.getAutoLoanSummaryAll(pageNo, pageSize, searchKey);
                return objLoanSummaryList;
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Get Loan Summary");
                throw new Exception("");
            }
        }
        
        //[AjaxMethod(HttpSessionStateRequirement.Read)]
        //public List<AutoLoanSummary> GetLoanSummaryByLLID(int pageNo, int pageSize, Int32 searchKey)
        //{
        //    AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
        //    var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
        //    List<AutoLoanSummary> objLoanSummaryList =
        //        autoLoanAssessmentService.getAutoLoanSummaryAll(pageNo,pageSize);
        //    return objLoanSummaryList;
        //}

        [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string AppealedByAutoLoanMasterId(int appiledOnAutoLoanMasterId)
        {
            var res = "";
            try
            {
               var userID = Convert.ToInt32(Session["Id"].ToString());
                AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = appiledOnAutoLoanMasterId;
                objAutoStatusHistory.StatusID = 2; //Forword To CI
                objAutoStatusHistory.UserID = userID;


                res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return res;

        }


            [AjaxMethod(HttpSessionStateRequirement.Read)]
        public string LoadBALetterInfo(int appiledOnAutoLoanMasterId)
        {
            
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            BALetter_Entity objBALetter =
                autoLoanAssessmentService.getBaLetter(appiledOnAutoLoanMasterId);

            #region BA Letter Page 1

                

                Label46.Text= "dfgdgdgfgd";
                lBranch.Text = "uytutuytu";
                lblDate.Text = System.DateTime.Now.ToShortDateString();
                lblACNo.Text = objBALetter.AccountNumber;
                lblApplicationDate.Text = objBALetter.AppliedDate.ToShortDateString();
                switch (objBALetter.ProductId)
                {
                    case 1:
                        chkStaffAuto.Checked = true;
                        break;
                    case 2:
                        chkConventionalAuto.Checked = true;
                        break;
                    case 3:
                        chkSaadiqAuto.Checked = true;
                        break;
                    default:
                        chkStaffAuto.Checked = false;
                        chkConventionalAuto.Checked = false;
                        chkSaadiqAuto.Checked = false;
                        break;

                }

                lblLoanAmount.Text = Convert.ToString(objBALetter.TotalLoanAmount);
                lblEMIAmount.Text = Convert.ToString(objBALetter.MaxEMI);
                lblTenor.Text = Convert.ToString(objBALetter.ConsideredTenor); 

                lblBrand.Text = objBALetter.ManufacturerName;
                lblInterestRate.Text = Convert.ToString(objBALetter.ConsideredRate);
                lblNoOfInstallemt.Text = Convert.ToString(objBALetter.ConsideredInMonth);

                //lblExpiryDate.Text = "Not Found";
                lblModel.Text = objBALetter.Model;
                //lblProcessingFee.Text = "Not Found";

                //lblDPNoteLetter.Text = "Not Found";
                lblLetterOfHypothecate.Text = objBALetter.HypothecationCheck;
                lblSecurity.Text = Convert.ToString(objBALetter.SecurityPDC); 

                lblLoanAccNo.Text = objBALetter.AccountNumber;
                lblDebitAuthorityAcc.Text = objBALetter.SCBAccount;
            #endregion

                #region BA Letter Page-2
                lblBranch0.Text = objBALetter.AutoBranchName;
                lblDate0.Text = System.DateTime.Now.ToShortDateString();
                lblACNo0.Text = objBALetter.AccountNumber;
                lblApplicationDate0.Text = objBALetter.AppliedDate.ToShortDateString();
                switch (objBALetter.ProductId)
                {
                    case 1:
                        chkStaffAuto0.Checked = true;
                        break;
                    case 2:
                        chkConventionalAuto0.Checked = true;
                        break;
                    case 3:
                        chkSaadiqAuto0.Checked = true;
                        break;
                    default:
                        chkStaffAuto0.Checked = false;
                        chkConventionalAuto0.Checked = false;
                        chkSaadiqAuto0.Checked = false;
                        break;

                }

                lblLoanAmount0.Text = Convert.ToString(objBALetter.TotalLoanAmount);
                lblEMIAmount0.Text = Convert.ToString(objBALetter.MaxEMI);
                lblTenor0.Text = Convert.ToString(objBALetter.ConsideredTenor);

                lblBrand0.Text = objBALetter.ManufacturerName;
                lblInterestRate0.Text = Convert.ToString(objBALetter.ConsideredRate);
                lblNoOfInstallemt0.Text = Convert.ToString(objBALetter.ConsideredInMonth);

                //lblExpiryDate.Text = "Not Found";
                lblModel.Text = objBALetter.Model;
                //lblProcessingFee.Text = "Not Found";

                //lblDPNoteLetter.Text = "Not Found";
                lblLetterOfHypothecate0.Text = objBALetter.HypothecationCheck;
                lblSecurity0.Text = Convert.ToString(objBALetter.SecurityPDC);

                lblLoanAccNo0.Text = objBALetter.AccountNumber;
                lblDebitAuthorityAcc0.Text = objBALetter.SCBAccount;
                #endregion

                return "Success";
        }
    }
}