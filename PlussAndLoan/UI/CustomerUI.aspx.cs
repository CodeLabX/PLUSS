﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_CustomerUI : System.Web.UI.Page
    {
        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2
        }
        #endregion

        private CustomerManager customerManagerObj;
        private Customer customerLoanAppObj;
        private Customer customerBankStCalObj;
        private Customer customerCustomerObj;
        private List<CustomerDetail> customerDetailListObj;
        public PLManager pLManagerObj = null;
        public ApplicantOtherBankAccount otherBankAccountObj = null;
        public BankStatementManager bankStatementManagerObj = null;
        public BankStatement bankStatementObj = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            customerManagerObj = new CustomerManager();
            customerLoanAppObj = new Customer();
            customerBankStCalObj = new Customer();
            customerCustomerObj = new Customer();
            customerDetailListObj = new List<CustomerDetail>();
            pLManagerObj = new PLManager();
            otherBankAccountObj = new ApplicantOtherBankAccount();
            bankStatementManagerObj = new BankStatementManager();

            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetAllowResponseInBrowserHistory(false);
            if (!IsPostBack)
            {
                FillFirstPaymentDropDownList();
                InitializeData();
                FillFacilityDropDownList();
                foreach (GridViewRow gvr in customerGridView.Rows)
                {
                    (gvr.Cells[0].FindControl("taskfacilityDropDownList") as DropDownList).Attributes.Add("onchange", "CalculateCashSecurity()");
                    (gvr.Cells[1].FindControl("taskFlagTextBox") as TextBox).Text = (gvr.Cells[0].FindControl("taskfacilityDropDownList") as DropDownList).SelectedValue;
                    (gvr.Cells[3].FindControl("taskOrginalLimitTextBox") as TextBox).Attributes.Add("onkeyup", "CalculateCashSecurity()");
                }
                saveButton.Enabled = false;
                //searchButton.Attributes.Add("onclick", "JS_GridClear();JS_FunctionLoadBankNameList()");
            }
        
            topUpDate1TextBox.MaxLength = 10;
            topUpDate2TextBox.MaxLength = 10;
            topUpDate3TextBox.MaxLength = 10;

            topUpDate1TextBox.Attributes.Add("onkeyup", "AutoFormatDate('"+topUpDate1TextBox.ClientID+"');");
            topUpDate1TextBox.Attributes.Add("onblur", "DateFormate('" + topUpDate1TextBox.ClientID + "');");
            topUpDate2TextBox.Attributes.Add("onkeyup", "AutoFormatDate('" + topUpDate2TextBox.ClientID + "');");
            topUpDate2TextBox.Attributes.Add("onblur", "DateFormate('" + topUpDate2TextBox.ClientID + "');");
            topUpDate3TextBox.Attributes.Add("onkeyup", "AutoFormatDate('" + topUpDate3TextBox.ClientID + "');");
            topUpDate3TextBox.Attributes.Add("onblur", "DateFormate('" + topUpDate3TextBox.ClientID + "');");

            topupAmount1TextBox.Attributes.Add("onblur", "CheckTopupLoanAmount();");

            topUpCountDropDownList.Attributes.Add("onchange", "TopUpFieldShowHide()");
        
            foreach(GridViewRow gvr in customerGridView.Rows)
            {
                (gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).MaxLength = 10;
                (gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).MaxLength = 10;
                (gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('"+(gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).ClientID+"');");
                (gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).Attributes.Add("onblur", "DateFormate('"+(gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).ClientID+"');");

                (gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).ClientID + "');");
                (gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).Attributes.Add("onblur", "DateFormate('" + (gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).ClientID + "');");

            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();", true);
            llIdTextBox.Text = Session["LLID"] == null ? llIdTextBox.Text : Convert.ToString(Session["LLID"]);

            if (llIdTextBox.Text!="")
            {
                Int64 LLId = Convert.ToInt64(llIdTextBox.Text);
                LoadBankStatement(LLId);
            }
        }
        private void TopUpTextFieldShowHide(int topUpcount)
        {
            topUpCountHiddenField.Value = topUpcount.ToString();
            switch (topUpcount.ToString())
            {
                case "0":                
                    topupAmount1Label.Style.Add("visibility", "hidden");
                    topupAmount2TextBox.Style.Add("visibility", "hidden");
                    date2Label.Style.Add("visibility", "hidden");
                    topUpDate2TextBox.Style.Add("visibility", "hidden");
                    topupAmount3Label.Style.Add("visibility", "hidden");
                    topupAmount3TextBox.Style.Add("visibility", "hidden");
                    date3Label.Style.Add("visibility", "hidden");
                    topUpDate3TextBox.Style.Add("visibility ", "hidden");
                    break;
                case "1":
                    topupAmount1Label.Style.Add("visibility", "visible");
                    topupAmount2TextBox.Style.Add("visibility", "visible");
                    date2Label.Style.Add("visibility", "visible");
                    topUpDate2TextBox.Style.Add("visibility", "visible");
                    topupAmount3Label.Style.Add("visibility", "hidden");
                    topupAmount3TextBox.Style.Add("visibility", "hidden");
                    date3Label.Style.Add("visibility", "hidden");
                    topUpDate3TextBox.Style.Add("visibility ", "hidden");
                    break;
                case "2":
                    topupAmount1Label.Style.Add("visibility", "visible");
                    topupAmount2TextBox.Style.Add("visibility", "visible");
                    date2Label.Style.Add("visibility", "visible");
                    topUpDate2TextBox.Style.Add("visibility", "visible");
                    topupAmount3Label.Style.Add("visibility", "visible");
                    topupAmount3TextBox.Style.Add("visibility", "visible");
                    date3Label.Style.Add("visibility", "visible");
                    topUpDate3TextBox.Style.Add("visibility ", "visible");
                    break;
            }
        }

        #region InitialData
        private void FillFirstPaymentDropDownList()
        {
            firstPaymentDropDownList.Items.Insert(0, "Optional");
            for (int i = 1; i <= 28; i++)
            {
                firstPaymentDropDownList.Items.Insert(i,i.ToString());
            }
            firstPaymentDropDownList.SelectedIndex = 0;
        }

        private void InitializeData()
        {
            string a = "";
            List<string> stringList = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                a = "";
                stringList.Add(a);
            }
            customerGridView.DataSource = stringList;
            customerGridView.DataBind();
            topUpDate1TextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            topUpDate2TextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            topUpDate3TextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
        }
        public void FillFacilityDropDownList()
        {
            List<Product> productListObj;//= new List<Product>();
            productListObj = customerManagerObj.GetAllProduct();
            Product productObj = new Product();
            productObj.ProductName = "";
            productObj.ProdType = "";
            productListObj.Insert(0, productObj);

            foreach (GridViewRow gvr in customerGridView.Rows)
            {
                (gvr.FindControl("taskfacilityDropDownList") as DropDownList).DataSource = productListObj;
                (gvr.FindControl("taskfacilityDropDownList") as DropDownList).DataTextField = "ProductName";
                (gvr.FindControl("taskfacilityDropDownList") as DropDownList).DataValueField = "ProdType";
                (gvr.FindControl("taskfacilityDropDownList") as DropDownList).DataBind();

            }
        }
        #endregion

        #region Search Button Click Event
        protected void searchButton_Click(object sender, EventArgs e)
        {
            string llId = Convert.ToString(llIdTextBox.Text.Trim());

            int topUpId = 0;
            if (String.IsNullOrEmpty(llId))
            {
                msgLabel.Text = "Please enter LLId";
                llIdTextBox.Focus();
                return;
            }
            if (ExistingCustomerInfo(llId) == false)
            {
                msgLabel.Text = "";
                customerLoanAppObj = customerManagerObj.GetCustomerLoanAppInfo(llId);
                customerBankStCalObj = customerManagerObj.GetCustomerBankStatementCalculation(llId);
                if (customerLoanAppObj != null)
                {
                    topUpId = customerManagerObj.GetTopUpId(Convert.ToInt32(llId));
                    if (topUpId.Equals(3))
                    {
                        topUpDiv.Style.Add("display", "block");
                        TopUpTextFieldShowHide(0);
                    }
                    else
                    {
                        topUpDiv.Style.Add("display", "none");
                    }
                    customerNameTextBox.Text = customerLoanAppObj.Name;
                    if (customerLoanAppObj.MasterNo != null || customerLoanAppObj.MasterNo != "")
                    {
                        if (customerLoanAppObj.MasterNo.Length > 9)
                        {
                            masterTextBox.Text = customerLoanAppObj.MasterNo.Substring(2, 7).ToString();
                        }
                        else
                        {
                            masterTextBox.Text = customerLoanAppObj.MasterNo;
                        }
                    }
                    if (customerBankStCalObj != null)
                    {
                        pdcTextBox.Text = customerBankStCalObj.PDC;
                        pdcAccNoTextBox.Text = customerBankStCalObj.PDCAccount;
                        otherStBank1TextBox.Text = customerBankStCalObj.OtherStatementBank1;
                        otherStBank2TextBox.Text = customerBankStCalObj.OtherStatementBank2;
                        otherStBank3TextBox.Text = customerBankStCalObj.OtherStatementBank3;
                        otherStBank4TextBox.Text = customerBankStCalObj.OtherStatementBank4;
                        otherStBank5TextBox.Text = customerBankStCalObj.OtherStatementBank5;

                        otherStAcc1TextBox.Text = customerBankStCalObj.OtherStatementAcc1;
                        otherStAcc2TextBox.Text = customerBankStCalObj.OtherStatementAcc2;
                        otherStAcc3TextBox.Text = customerBankStCalObj.OtherStatementAcc3;
                        otherStAcc4TextBox.Text = customerBankStCalObj.OtherStatementAcc4;
                        otherStAcc5TextBox.Text = customerBankStCalObj.OtherStatementAcc5;

                    }
                    saveButton.Enabled = true;

                }
                else
                {
                    msgLabel.Text = "Data not found.";
                    saveButton.Enabled = true;
                    saveButton.Text = "Save";
                    llIdTextBox.Focus();
                }
            }
        }
        #endregion

        private bool ExistingCustomerInfo(string llId)
        {
            int topUpId = 0;
            customerCustomerObj = customerManagerObj.GetCustomerInfo(llId);
            if (customerCustomerObj != null)
            {
                saveButton.Text = "Update";
                idHiddenField.Value = customerCustomerObj.Id.ToString();
                customerNameTextBox.Text = customerCustomerObj.Name;
                masterTextBox.Text = customerCustomerObj.MasterNo;
                pdcTextBox.Text = customerCustomerObj.PDC;
                pdcAccNoTextBox.Text = customerCustomerObj.PDCAccount;
                ccplBundleOfferDropDownList.SelectedValue = customerCustomerObj.CCPLBundleOffer.ToString();
                otherDirLoanDropDownList.SelectedValue = customerCustomerObj.OtherDirLoan.ToString();
                try
                {
                    firstPaymentDropDownList.SelectedIndex = customerCustomerObj.FirstPayment;
                }
                catch (Exception)
                {

                    firstPaymentDropDownList.SelectedIndex = 0;
                }
            
                otherStBank1TextBox.Text = customerCustomerObj.OtherStatementBank1;
                otherStBank2TextBox.Text = customerCustomerObj.OtherStatementBank2;
                otherStBank3TextBox.Text = customerCustomerObj.OtherStatementBank3;
                otherStBank4TextBox.Text = customerCustomerObj.OtherStatementBank4;
                otherStBank5TextBox.Text = customerCustomerObj.OtherStatementBank5;

                otherStAcc1TextBox.Text = customerCustomerObj.OtherStatementAcc1;
                otherStAcc2TextBox.Text = customerCustomerObj.OtherStatementAcc2;
                otherStAcc3TextBox.Text = customerCustomerObj.OtherStatementAcc3;
                otherStAcc4TextBox.Text = customerCustomerObj.OtherStatementAcc4;
                otherStAcc5TextBox.Text = customerCustomerObj.OtherStatementAcc5;
                mob12DropDownList.SelectedValue = customerCustomerObj.Mob12.ToString();

                topUpId = customerManagerObj.GetTopUpId(Convert.ToInt32(llId));
                TopUpTextFieldShowHide(customerCustomerObj.TopUpCount);

                if (topUpId.Equals(3))
                {
                    topUpCountDropDownList.SelectedValue = customerCustomerObj.TopUpCount.ToString();
                    topUpDiv.Style.Add("display", "block");
                    topupAmount1TextBox.Text = customerCustomerObj.TopUpAmount1.ToString();
                    outStandingTextBox.Text = customerCustomerObj.OutStanding.ToString();
                    if (DateFormat.IsDate(customerCustomerObj.TopUpDate1))
                    {
                        topUpDate1TextBox.Text = customerCustomerObj.TopUpDate1.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        topUpDate1TextBox.Text = "";
                    }

                    topupAmount2TextBox.Text = customerCustomerObj.TopUpAmount2.ToString();
                    if (DateFormat.IsDate(customerCustomerObj.TopUpDate2))
                    {
                        topUpDate2TextBox.Text = customerCustomerObj.TopUpDate2.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        topUpDate2TextBox.Text = "";
                    }
                    topupAmount3TextBox.Text = customerCustomerObj.TopUpAmount3.ToString();
                    if (DateFormat.IsDate(customerCustomerObj.TopUpDate3))
                    {
                        topUpDate3TextBox.Text = customerCustomerObj.TopUpDate3.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        topUpDate3TextBox.Text = "";
                    }
                }
                else
                {
                    topUpDiv.Style.Add("display", "none");
                }
                customerDetailListObj = customerManagerObj.GetCustomerDetailInfo(llId);
                if (customerDetailListObj.Count > 0)
                {
                    saveButton.Text = "Update";
                    for (int i = 0; i < customerDetailListObj.Count; i++)
                    {
                        (customerGridView.Rows[i].Cells[0].FindControl("idTextBox") as Label).Text = customerDetailListObj[i].Id.ToString();
                        (customerGridView.Rows[i].Cells[1].FindControl("taskfacilityDropDownList") as DropDownList).SelectedValue = customerDetailListObj[i].Facility.Trim().ToString() + "#" + customerDetailListObj[i].Flag.ToString();
                        (customerGridView.Rows[i].Cells[2].FindControl("taskFlagTextBox") as TextBox).Text = customerDetailListObj[i].Flag.ToString();
                        (customerGridView.Rows[i].Cells[3].FindControl("taskOutstandingTextBox") as TextBox).Text = customerDetailListObj[i].OutStanding.ToString();
                        (customerGridView.Rows[i].Cells[4].FindControl("taskOrginalLimitTextBox") as TextBox).Text = customerDetailListObj[i].OrginalLimit.ToString();
                        (customerGridView.Rows[i].Cells[5].FindControl("taskRepmtTextBox") as TextBox).Text = customerDetailListObj[i].RePayment.ToString();
                        if (DateFormat.IsDate(customerDetailListObj[i].DisbursDate))
                        {
                            (customerGridView.Rows[i].Cells[6].FindControl("taskDisDateTextBox") as TextBox).Text = customerDetailListObj[i].DisbursDate.ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            (customerGridView.Rows[i].Cells[6].FindControl("taskDisDateTextBox") as TextBox).Text = "";
                        }
                        if (DateFormat.IsDate(customerDetailListObj[i].ExposureDate))
                        {
                            (customerGridView.Rows[i].Cells[7].FindControl("taskExpDateTextBox") as TextBox).Text = customerDetailListObj[i].ExposureDate.ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            (customerGridView.Rows[i].Cells[7].FindControl("taskExpDateTextBox") as TextBox).Text = "";
                        }
                        (customerGridView.Rows[i].Cells[8].FindControl("taskCashsecurityTextBox") as TextBox).Text = customerDetailListObj[i].CashSecurity.ToString();
                    }
                }
                PL plObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llIdTextBox.Text.Trim()));
                if (plObj != null)
                {
                    if (plObj.Status == 0)
                    {
                        saveButton.Enabled = true;
                    }
                    else
                    {
                        saveButton.Enabled = false;
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "JS_FunctionLoadBankNameList();", true);
                return true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "JS_FunctionLoadBankNameList();", true);
                return false;
            }       
        
        }
        //private void LoadEntryBankList(Int64 LLId)
        //{
        //    StringBuilder bankListSB = new StringBuilder();
        //    List<BankNameList> otherBankAccountObjList = new List<BankNameList>();
        //    otherBankAccountObj = bankStatementManagerObj.SelectLoanApplicationOthreBankInfo(LLId);
        //    if (otherBankAccountObj != null)
        //    {
        //        if (otherBankAccountObj.OtherBankId1 > 0)
        //        {
        //            BankNameList bankNameList1Obj = new BankNameList();
        //            bankNameList1Obj.OtherBankId = otherBankAccountObj.OtherBankId1;
        //            bankNameList1Obj.OtherBank = otherBankAccountObj.OtherBank1.ToString();
        //            bankNameList1Obj.OtherBranchId = otherBankAccountObj.OtherBranchId1;
        //            bankNameList1Obj.OtherBranch = otherBankAccountObj.OtherBranch1;
        //            bankNameList1Obj.OtherAccNo = otherBankAccountObj.OtherAccNo1.ToString();
        //            bankNameList1Obj.OtherAccType = otherBankAccountObj.OtherAccType1.ToString();
        //            otherBankAccountObjList.Add(bankNameList1Obj);
        //        }
        //        if (otherBankAccountObj.OtherBankId2 > 0)
        //        {
        //            BankNameList bankNameList2Obj = new BankNameList();
        //            bankNameList2Obj.OtherBankId = otherBankAccountObj.OtherBankId2;
        //            bankNameList2Obj.OtherBank = otherBankAccountObj.OtherBank2.ToString();
        //            bankNameList2Obj.OtherBranchId = otherBankAccountObj.OtherBranchId2;
        //            bankNameList2Obj.OtherBranch = otherBankAccountObj.OtherBranch2;
        //            bankNameList2Obj.OtherAccNo = otherBankAccountObj.OtherAccNo2.ToString();
        //            bankNameList2Obj.OtherAccType = otherBankAccountObj.OtherAccType2.ToString();
        //            otherBankAccountObjList.Add(bankNameList2Obj);
        //        }
        //        if (otherBankAccountObj.OtherBankId3 > 0)
        //        {
        //            BankNameList bankNameList3Obj = new BankNameList();
        //            bankNameList3Obj.OtherBankId = otherBankAccountObj.OtherBankId3;
        //            bankNameList3Obj.OtherBank = otherBankAccountObj.OtherBank3.ToString();
        //            bankNameList3Obj.OtherBranchId = otherBankAccountObj.OtherBranchId3;
        //            bankNameList3Obj.OtherBranch = otherBankAccountObj.OtherBranch3;
        //            bankNameList3Obj.OtherAccNo = otherBankAccountObj.OtherAccNo3.ToString();
        //            bankNameList3Obj.OtherAccType = otherBankAccountObj.OtherAccType3.ToString();
        //            otherBankAccountObjList.Add(bankNameList3Obj);
        //        }

        //        if (otherBankAccountObj.OtherBankId4 > 0)
        //        {
        //            BankNameList bankNameList4Obj = new BankNameList();
        //            bankNameList4Obj.OtherBankId = otherBankAccountObj.OtherBankId4;
        //            bankNameList4Obj.OtherBank = otherBankAccountObj.OtherBank4.ToString();
        //            bankNameList4Obj.OtherBranchId = otherBankAccountObj.OtherBranchId4;
        //            bankNameList4Obj.OtherBranch = otherBankAccountObj.OtherBranch4;
        //            bankNameList4Obj.OtherAccNo = otherBankAccountObj.OtherAccNo4.ToString();
        //            bankNameList4Obj.OtherAccType = otherBankAccountObj.OtherAccType4.ToString();
        //            otherBankAccountObjList.Add(bankNameList4Obj);
        //        }
        //        if (otherBankAccountObj.OtherBankId5 > 0)
        //        {
        //            BankNameList bankNameList5Obj = new BankNameList();
        //            bankNameList5Obj.OtherBankId = otherBankAccountObj.OtherBankId5;
        //            bankNameList5Obj.OtherBank = otherBankAccountObj.OtherBank5.ToString();
        //            bankNameList5Obj.OtherBranchId = otherBankAccountObj.OtherBranchId5;
        //            bankNameList5Obj.OtherBranch = otherBankAccountObj.OtherBranch5;
        //            bankNameList5Obj.OtherAccNo = otherBankAccountObj.OtherAccNo5.ToString();
        //            bankNameList5Obj.OtherAccType = otherBankAccountObj.OtherAccType5.ToString();
        //            otherBankAccountObjList.Add(bankNameList5Obj);
        //        }

        //    }
        //    int rowIndex = 1;
        //    if (otherBankAccountObjList.Count > 0)
        //    {
        //        try
        //        {
        //            bankListSB.Append("<rows>");
        //            foreach (BankNameList bankNameObj in otherBankAccountObjList)
        //            {
        //                bankListSB.Append("<row id='" + rowIndex.ToString() + "'>");
        //                bankListSB.Append("<cell>");
        //                bankListSB.Append(rowIndex.ToString());
        //                bankListSB.Append("</cell>");
        //                bankListSB.Append("<cell>");
        //                bankListSB.Append(bankNameObj.OtherBankId.ToString());
        //                bankListSB.Append("</cell>");
        //                bankListSB.Append("<cell>");
        //                bankListSB.Append(bankNameObj.OtherBank.ToString());
        //                bankListSB.Append("</cell>");
        //                bankListSB.Append("<cell>");
        //                bankListSB.Append(bankNameObj.OtherBranchId.ToString());
        //                bankListSB.Append("</cell>");
        //                bankListSB.Append("<cell>");
        //                bankListSB.Append(bankNameObj.OtherBranch.ToString());
        //                bankListSB.Append("</cell>");
        //                bankListSB.Append("<cell>");
        //                bankListSB.Append(bankNameObj.OtherAccNo.ToString());
        //                bankListSB.Append("</cell>");
        //                bankListSB.Append("</row>");
        //                rowIndex = rowIndex + 1;
        //            }
        //            bankListSB.Append("</rows>");
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //    }
        //    else
        //    {
        //        bankListSB.Append("<rows><row id='1'><cell></cell><cell></cell><cell></cell><cell></cell><cell></cell><cell></cell></row></rows>");
        //    }
        //    bankListNameHiddenField.Value = "";
        //    bankListNameHiddenField.Value = bankListSB.ToString();
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "JS_FunctionLoadBankNameList();", true);
        //}

        #region taskfacilityDropDownList_SelectedIndexChanged
        protected void taskfacilityDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedValue = "";        
            foreach (GridViewRow gvr in customerGridView.Rows)
            {
                selectedValue = Convert.ToString((gvr.Cells[0].FindControl("taskfacilityDropDownList") as DropDownList).SelectedValue);
                string[] selectArr = selectedValue.Split('#');           
            
                if (selectedValue != "")
                {
                    (gvr.Cells[1].FindControl("taskFlagTextBox") as TextBox).Text = selectArr[1].Trim().ToString();//(gvr.Cells[0].FindControl("taskfacilityDropDownList") as DropDownList).SelectedValue;
                }
                else
                {
                    (gvr.Cells[1].FindControl("taskFlagTextBox") as TextBox).Text = "";
                }
            }
        }
        #endregion
        private int DeleteCUDEIDExposer(List<int>cudeIDList)
        {
            //throw new NotImplementedException();
            int count = 0;
            foreach (int id in cudeIDList)
            {
                int customerId4Detail = customerManagerObj.GetCustomerId(id.ToString());
                string queryString = "DELETE FROM t_pluss_customerdetails WHERE CUDE_ID = '" + id.ToString() + "'";// " AND CUDE_CUSTOMER_LLID = '" + customerId4Detail + "' ";
                count=count+ DbQueryManager.ExecuteNonQuery(queryString); 
            }
            return count;
        }


        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (CashSecuirirtyCheck())
                {
                    ExceedLimitLbl.Text = "";
                    ExceedLimitLbl.Text = "Could Not Exceed The Limit";
                    return;
                }
                ExceedLimitLbl.Text = "";
                int insertOrUpdateRow = -1;
                int insertOrUpdateRow2 = -1;
                Customer customerObj = new Customer();
                List<CustomerDetail> cutomerDetailListObj;
                List<int> getIdForDeletionList = new List<int>();
                int deletedRows = -1;
                string llId = llIdTextBox.Text.Trim();


                if (String.IsNullOrEmpty(llId))
                {
                    Alert.Show("Please enter LLID");
                    llIdTextBox.Focus();
                    return;
                }
                else if (pdcTextBox.Text=="")
                {
                    Alert.Show("Please enter PDC/SI Bank name");
                    pdcTextBox.Focus();
                    return;
                }
                else if (pdcAccNoTextBox.Text=="")
                {
                    Alert.Show("Please enter valid  PDC/SI AC No.");
                    pdcAccNoTextBox.Focus();
                    return;
                }

                else if (EMIExposerCheck() == true)
                {
                    Alert.Show("You must enter exposure");
                    return;
                }
                else
                {
                    msgLabel.Text = "";
                    int customerId = Convert.ToInt32("0" + idHiddenField.Value);
                    if (customerId > 0)
                    {
                        customerObj.Id = customerId;
                    }
                    else
                    {
                        customerObj.Id = 0;
                    }
                    customerObj.LLId = llId;
                    customerObj.Name = customerNameTextBox.Text.Trim();
                    customerObj.MasterNo = masterTextBox.Text.Trim();
                    customerObj.PDC = pdcTextBox.Text.Trim();
                    customerObj.PDCAccount = pdcAccNoTextBox.Text.Trim();
                    customerObj.FirstPayment = firstPaymentDropDownList.SelectedIndex;
                    customerObj.CCPLBundleOffer = Convert.ToInt32(ccplBundleOfferDropDownList.SelectedValue);
                    customerObj.OtherDirLoan = Convert.ToInt32(otherDirLoanDropDownList.SelectedValue);
                    customerObj.OtherStatementBank1 = otherStBank1TextBox.Text.Trim();
                    customerObj.OtherStatementAcc1 = otherStAcc1TextBox.Text.Trim();
                    customerObj.OtherStatementBank2 = otherStBank2TextBox.Text.Trim();
                    customerObj.OtherStatementAcc2 = otherStAcc2TextBox.Text.Trim();
                    customerObj.OtherStatementBank3 = otherStBank3TextBox.Text.Trim();
                    customerObj.OtherStatementAcc3 = otherStAcc3TextBox.Text.Trim();
                    customerObj.OtherStatementBank4 = otherStBank4TextBox.Text.Trim();
                    customerObj.OtherStatementAcc4 = otherStAcc4TextBox.Text.Trim();
                    customerObj.OtherStatementBank5 = otherStBank5TextBox.Text.Trim();
                    customerObj.OtherStatementAcc5 = otherStAcc5TextBox.Text.Trim();
                    customerObj.Mob12 = Convert.ToInt32(mob12DropDownList.SelectedValue);
                    customerObj.TopUpAmount1 = Convert.ToDouble("0" + topupAmount1TextBox.Text.Trim());
                    customerObj.OutStanding = Convert.ToDouble("0" + outStandingTextBox.Text.Trim());
                    customerObj.TopUpCount = Convert.ToInt32("0" + topUpCountHiddenField.Value);
                    if (!String.IsNullOrEmpty(topUpDate1TextBox.Text.Trim()))
                    {
                        customerObj.TopUpDate1 = Convert.ToDateTime(topUpDate1TextBox.Text.Trim(), new CultureInfo("ru-RU"));
                    }
                    customerObj.TopUpAmount2 = Convert.ToDouble("0" + topupAmount2TextBox.Text.Trim());
                    if (!String.IsNullOrEmpty(topUpDate2TextBox.Text.Trim()))
                    {
                        customerObj.TopUpDate2 = Convert.ToDateTime(topUpDate2TextBox.Text.Trim(), new CultureInfo("ru-RU"));
                    }
                    customerObj.TopUpAmount3 = Convert.ToDouble("0" + topupAmount3TextBox.Text.Trim());
                    if (!String.IsNullOrEmpty(topUpDate3TextBox.Text.Trim()))
                    {
                        customerObj.TopUpDate3 = Convert.ToDateTime(topUpDate3TextBox.Text.Trim(), new CultureInfo("ru-RU"));
                    }

                    insertOrUpdateRow = customerManagerObj.InsertORUpdateCustomerInfo(customerObj);

                }
                if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT) || insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                {
                    msgLabel.Text = "";
                    cutomerDetailListObj = new List<CustomerDetail>();
                    CustomerDetail customerDetailObj = null;
                    int customerId4Detail = customerManagerObj.GetCustomerId(llId);
                    int gridNumberCount = 0;
                    foreach (GridViewRow gvr in customerGridView.Rows)
                    {
                        gridNumberCount++;
                        if (!((gvr.Cells[1].FindControl("taskfacilityDropDownList") as DropDownList).Text == ""))
                        {
                    
                            string facility = Convert.ToString((gvr.Cells[1].FindControl("taskfacilityDropDownList") as DropDownList).Text);
                            string[] facilityArr = facility.Split('#');
                            customerDetailObj = new CustomerDetail();
                            customerDetailObj.Id = Convert.ToInt32("0" + (gvr.Cells[0].FindControl("idTextBox") as Label).Text.Trim());
                            customerDetailObj.Facility = facilityArr[0].Trim().ToString();
                            customerDetailObj.Flag = Convert.ToChar("" + (gvr.Cells[2].FindControl("taskFlagTextBox") as TextBox).Text.Trim());
                            customerDetailObj.OutStanding = Convert.ToDouble("0" + (gvr.Cells[3].FindControl("taskOutstandingTextBox") as TextBox).Text.Trim());
                            customerDetailObj.OrginalLimit = Convert.ToDouble("0" + (gvr.Cells[4].FindControl("taskOrginalLimitTextBox") as TextBox).Text.Trim());
                            customerDetailObj.RePayment = Convert.ToDouble("0" + (gvr.Cells[5].FindControl("taskRepmtTextBox") as TextBox).Text.Trim());
                            if (!(String.IsNullOrEmpty((gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).Text.Trim())))
                            {
                                customerDetailObj.DisbursDate = Convert.ToDateTime((gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                            }
                            if (!(String.IsNullOrEmpty((gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).Text.Trim())))
                            {
                                customerDetailObj.ExposureDate = Convert.ToDateTime((gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                            }
                            customerDetailObj.CashSecurity = Convert.ToDouble("0" + (gvr.Cells[8].FindControl("taskCashsecurityTextBox") as TextBox).Text.Trim());
                            customerDetailObj.CustomerId = customerId4Detail;
                            customerDetailObj.CustomerLLId = llId;
                            cutomerDetailListObj.Add(customerDetailObj);

                            //BIL [30%]#U

                        }
                        else
                        {
                    
                    
                            List<CustomerDetail> customerDetailListObj_FromDB=new List<CustomerDetail>();
                            customerManagerObj=new CustomerManager();
                            customerDetailListObj_FromDB = customerManagerObj.GetCustomerDetailInfo(llId);
                            int dataExistAtPosition = 0;
                            try
                            {
                                dataExistAtPosition = customerDetailListObj_FromDB[gridNumberCount-1].Id;
                                getIdForDeletionList.Add(dataExistAtPosition);
                            }
                            catch (Exception)
                            {
                                dataExistAtPosition = 0;
                            }
                   
                        }
                    }
                    if (cutomerDetailListObj.Count > 0)
                    {
                        insertOrUpdateRow2 = customerManagerObj.InsertORUpdateCustomerDetailsInfo(cutomerDetailListObj);
                        deletedRows = DeleteCUDEIDExposer(getIdForDeletionList);
                    }
                }
        
                if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT))
                {
                    Alert.Show("Save sucessfully.");
                    saveButton.Text = "Save";
                    new AT(this).AuditAndTraial("Add Customer", "Save sucessfully");
                    llIdTextBox.Focus();
                    ClearField();
                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERTERROR))
                {
                    Alert.Show("Data not saved sucessfully.");
                    new AT(this).AuditAndTraial("Add Customer", "Data not saved sucessfully");
                    llIdTextBox.Focus();
                    return;
                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                {
                    Alert.Show("Update sucessfully.");
                    new AT(this).AuditAndTraial("Add Customer", "Update sucessfully");
                    saveButton.Text = "Save";
                    llIdTextBox.Focus();
                    ClearField();
                }
                else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                {
                    Alert.Show("Data not update sucessfully.");
                    new AT(this).AuditAndTraial("Add Customer", "Data not update sucessfully");
                    llIdTextBox.Focus();
                    return;
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Customer");
            }
        }

        private bool EMIExposerCheck()
        {
            bool returnValue = false;
            PL plObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llIdTextBox.Text.Trim()));
            if (plObj != null)
            {
                if (plObj.PlEmiwithscb > 0)
                {
                    foreach (GridViewRow gvr in customerGridView.Rows)
                    {
                        if ((gvr.Cells[1].FindControl("taskfacilityDropDownList") as DropDownList).SelectedValue != "" || (gvr.Cells[3].FindControl("taskOutstandingTextBox") as TextBox).Text != "" || (gvr.Cells[4].FindControl("taskOrginalLimitTextBox") as TextBox).Text != "")
                        {
                            returnValue = false;
                            break;
                        }
                        else
                        {
                            returnValue = true;
                        }
                    }

                }
            }
            return returnValue;
        }
        public void ClearField()
        {
            idHiddenField.Value = "";
            customerNameTextBox.Text = "";
            ccplBundleOfferDropDownList.SelectedValue = "1";
            masterTextBox.Text = "";
            otherDirLoanDropDownList.SelectedValue = "1";
            pdcTextBox.Text = "";
            pdcAccNoTextBox.Text = "";
            firstPaymentDropDownList.SelectedIndex = 1;
            otherStBank1TextBox.Text = "";
            otherStAcc1TextBox.Text = "";
            otherStBank2TextBox.Text = "";
            otherStAcc2TextBox.Text = "";
            otherStBank3TextBox.Text = "";
            otherStAcc3TextBox.Text = "";
            otherStBank4TextBox.Text = "";
            otherStAcc4TextBox.Text = "";
            otherStBank5TextBox.Text = "";
            otherStAcc5TextBox.Text = "";
            mob12DropDownList.SelectedValue = "1";

            foreach (GridViewRow gvr in customerGridView.Rows)
            {
                (gvr.Cells[0].FindControl("idTextBox") as Label).Text = "";
                (gvr.Cells[1].FindControl("taskfacilityDropDownList") as DropDownList).SelectedValue = "";
                (gvr.Cells[2].FindControl("taskFlagTextBox") as TextBox).Text = "";
                (gvr.Cells[3].FindControl("taskOutstandingTextBox") as TextBox).Text = "";
                (gvr.Cells[4].FindControl("taskOrginalLimitTextBox") as TextBox).Text = "";
                (gvr.Cells[5].FindControl("taskRepmtTextBox") as TextBox).Text = "";
                (gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).Text = "";
                (gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).Text = "";
                (gvr.Cells[8].FindControl("taskCashsecurityTextBox") as TextBox).Text = "";
            }

            topupAmount1TextBox.Text = "";
            outStandingTextBox.Text = "";
            topUpDate1TextBox.Text = "";
            topupAmount2TextBox.Text = "";
            topUpDate2TextBox.Text = "";
            topupAmount3TextBox.Text = "";
            topUpDate3TextBox.Text = "";
            topUpDate1TextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            topUpDate2TextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            topUpDate3TextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            llIdTextBox.Text = "";
            msgLabel.Text = "";
            ClearField();
            llIdTextBox.Focus();
            saveButton.Enabled = true;
        }    
        protected void  bankListNameHiddenField_ValueChanged(object sender, EventArgs e)
        {

        }

        //tarek 07062011
        private bool CashSecuirirtyCheck()
        {
            bool returnValue = false;
            PL plObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llIdTextBox.Text.Trim()));
            if (plObj != null)
            {
                if (plObj.PlProposedloanamount > 0)
                {
                    foreach (GridViewRow gvr in customerGridView.Rows)
                    {
                        if ((gvr.Cells[1].FindControl("taskfacilityDropDownList") as DropDownList).SelectedValue == "BIL [30%]#U")
                        {
                            double outStanding = Convert.ToDouble("0" + (gvr.Cells[4].FindControl("taskOutstandingTextBox") as TextBox).Text.Trim());
                            double cashSecuirity = Convert.ToDouble("0" + (gvr.Cells[8].FindControl("taskCashsecurityTextBox") as TextBox).Text.Trim());
                            double difference = outStanding - cashSecuirity + plObj.PlProposedloanamount;
                            if (mob12DropDownList.SelectedIndex == 1)//12+ 4.9
                            {
                     
                                if (difference >= 4900000)
                                {
                                    //block to save
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }

                            }
                            else //less than 12 check with 3.5
                            {
                                if (difference >= 3500000)
                                {
                                    //block to save
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }

                            }
                   
                            //break;
                        }
                        //else
                        //{
                        //    returnValue = true;
                        //}
                    }

                }
            }
            return returnValue;
        }
        //
        protected void DeleteFromGrid_Click(object sender, EventArgs e)
        {
            int gridNumberCount = 0;
            int counts = 0;
            foreach (GridViewRow gvr in customerGridView.Rows)
            {
                gridNumberCount++;
                if (!((gvr.Cells[1].FindControl("taskfacilityDropDownList") as DropDownList).Text == ""))
                {

                    (gvr.Cells[1].FindControl("taskfacilityDropDownList") as DropDownList).Text = "";
                    int id2 =  Convert.ToInt32("0" + (gvr.Cells[0].FindControl("idTextBox") as Label).Text.Trim());
                    (gvr.Cells[0].FindControl("idTextBox") as Label).Text="";
                    (gvr.Cells[2].FindControl("taskFlagTextBox") as TextBox).Text = "";
                    (gvr.Cells[3].FindControl("taskOutstandingTextBox") as TextBox).Text = "";
                    (gvr.Cells[4].FindControl("taskOrginalLimitTextBox") as TextBox).Text = "";
                    (gvr.Cells[5].FindControl("taskRepmtTextBox") as TextBox).Text = "";
                    (gvr.Cells[6].FindControl("taskDisDateTextBox") as TextBox).Text = "";
                    (gvr.Cells[7].FindControl("taskExpDateTextBox") as TextBox).Text = "";
                    (gvr.Cells[8].FindControl("taskCashsecurityTextBox") as TextBox).Text = "";
                    string queryString = "DELETE FROM t_pluss_customerdetails WHERE CUDE_ID = '" + id2.ToString() + "'";// " AND CUDE_CUSTOMER_LLID = '" + customerId4Detail + "' ";
                    counts =DbQueryManager.ExecuteNonQuery(queryString);

                }
                else
                {
                }
            }
        }


        private void LoadBankStatement(Int64 LLId)
        {
            List<BankNameList> otherBankAccountObjList = new List<BankNameList>();
            otherBankAccountObj = bankStatementManagerObj.SelectLoanApplicationOthreBankInfo(LLId);
            if (otherBankAccountObj != null)
            {
                if (otherBankAccountObj.OtherBankId1 > 0)
                {
                    BankNameList bankNameList1Obj = new BankNameList();
                    bankNameList1Obj.OtherBankId = otherBankAccountObj.OtherBankId1;
                    bankNameList1Obj.OtherBank = otherBankAccountObj.OtherBank1.ToString();
                    bankNameList1Obj.OtherBranchId = otherBankAccountObj.OtherBranchId1;
                    bankNameList1Obj.OtherBranch = otherBankAccountObj.OtherBranch1;
                    bankNameList1Obj.OtherAccNo = otherBankAccountObj.OtherAccNo1.ToString();
                    bankNameList1Obj.OtherAccType = otherBankAccountObj.OtherAccType1.ToString();
                    otherBankAccountObjList.Add(bankNameList1Obj);
                }
                if (otherBankAccountObj.OtherBankId2 > 0)
                {
                    BankNameList bankNameList2Obj = new BankNameList();
                    bankNameList2Obj.OtherBankId = otherBankAccountObj.OtherBankId2;
                    bankNameList2Obj.OtherBank = otherBankAccountObj.OtherBank2.ToString();
                    bankNameList2Obj.OtherBranchId = otherBankAccountObj.OtherBranchId2;
                    bankNameList2Obj.OtherBranch = otherBankAccountObj.OtherBranch2;
                    bankNameList2Obj.OtherAccNo = otherBankAccountObj.OtherAccNo2.ToString();
                    bankNameList2Obj.OtherAccType = otherBankAccountObj.OtherAccType2.ToString();
                    otherBankAccountObjList.Add(bankNameList2Obj);
                }
                if (otherBankAccountObj.OtherBankId3 > 0)
                {
                    BankNameList bankNameList3Obj = new BankNameList();
                    bankNameList3Obj.OtherBankId = otherBankAccountObj.OtherBankId3;
                    bankNameList3Obj.OtherBank = otherBankAccountObj.OtherBank3.ToString();
                    bankNameList3Obj.OtherBranchId = otherBankAccountObj.OtherBranchId3;
                    bankNameList3Obj.OtherBranch = otherBankAccountObj.OtherBranch3;
                    bankNameList3Obj.OtherAccNo = otherBankAccountObj.OtherAccNo3.ToString();
                    bankNameList3Obj.OtherAccType = otherBankAccountObj.OtherAccType3.ToString();
                    otherBankAccountObjList.Add(bankNameList3Obj);
                }

                if (otherBankAccountObj.OtherBankId4 > 0)
                {
                    BankNameList bankNameList4Obj = new BankNameList();
                    bankNameList4Obj.OtherBankId = otherBankAccountObj.OtherBankId4;
                    bankNameList4Obj.OtherBank = otherBankAccountObj.OtherBank4.ToString();
                    bankNameList4Obj.OtherBranchId = otherBankAccountObj.OtherBranchId4;
                    bankNameList4Obj.OtherBranch = otherBankAccountObj.OtherBranch4;
                    bankNameList4Obj.OtherAccNo = otherBankAccountObj.OtherAccNo4.ToString();
                    bankNameList4Obj.OtherAccType = otherBankAccountObj.OtherAccType4.ToString();
                    otherBankAccountObjList.Add(bankNameList4Obj);
                }
                if (otherBankAccountObj.OtherBankId5 > 0)
                {
                    BankNameList bankNameList5Obj = new BankNameList();
                    bankNameList5Obj.OtherBankId = otherBankAccountObj.OtherBankId5;
                    bankNameList5Obj.OtherBank = otherBankAccountObj.OtherBank5.ToString();
                    bankNameList5Obj.OtherBranchId = otherBankAccountObj.OtherBranchId5;
                    bankNameList5Obj.OtherBranch = otherBankAccountObj.OtherBranch5;
                    bankNameList5Obj.OtherAccNo = otherBankAccountObj.OtherAccNo5.ToString();
                    bankNameList5Obj.OtherAccType = otherBankAccountObj.OtherAccType5.ToString();
                    otherBankAccountObjList.Add(bankNameList5Obj);
                }

            }
            int rowIndex = 1;
            //Response.ContentType = "text/xml";
            //Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>");
            //Response.Write("<rows>");

            var xml = "<xml id='myGridxml_data'><rows>";
            if (otherBankAccountObjList.Count > 0)
            {
                try
                {
                    foreach (BankNameList bankNameObj in otherBankAccountObjList)
                    {
                        xml+="<row id='" + rowIndex.ToString() + "'>";
                        xml+="<cell>";
                        xml+=rowIndex.ToString();
                        xml+="</cell>";
                        xml+="<cell>";
                        xml+=bankNameObj.OtherBankId.ToString();
                        xml+="</cell>";
                        xml+="<cell>";
                        xml+=bankNameObj.OtherBank.ToString();
                        xml+="</cell>";
                        xml+="<cell>";
                        xml+=bankNameObj.OtherBranchId.ToString();
                        xml+="</cell>";
                        xml+="<cell>";
                        xml+=bankNameObj.OtherBranch.ToString();
                        xml+="</cell>";
                        xml+="<cell>";
                        xml+=bankNameObj.OtherAccNo.ToString();
                        xml+="</cell>";

                        xml+="</row>";
                        rowIndex = rowIndex + 1;
                    }
                }
                catch (Exception ex)
                {
                }
            }

            xml += "</rows></xml>";
            dvXml.InnerHtml = xml;
        }

        public string EndMonth(string startMonth)
        {
            string endMonth = null;
            DateTime getDate = DateTime.Parse("01-" + startMonth);
            int month = getDate.Month;
            for (int i = 0; i < 12; i++)
            {
                endMonth = getDate.AddMonths(i).ToString("MMMM-yyyy");
            }
            return endMonth;
        }
    }
}
