﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_BILMUE" MasterPageFile="~/UI/AnalystMasterPage.master" Codebehind="BILMUEUI.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tableFacilityWidth
        {
            width: 390px;
        }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<%--<body onload="CalculateNetAllowed();CalculateNetUnsecured();CalculateNarLoad();MobDropDownSelect();">--%>    
    <div>
        <table>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="center" style="font-weight: bold; font-size: 15px">
                    Maximum Unsecured Exposure for BIL Customers
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="center" style="padding-left: 80px;">
                    <div style="border:solid 1px gray ">
                    <table width="100%">
                        <tr>
                            <td align="left" width="135">
                                &nbsp;
                            </td>
                            <td align="left">
                                <asp:HiddenField ID="idHiddenField" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="135">
                                LLId
                            </td>
                            <td align="left">
                                <asp:TextBox ID="llIdTextBox" runat="server" MaxLength="9"></asp:TextBox>
                                <asp:Button ID="searchButton" runat="server" Height="20px" Text="..." OnClick="searchButton_Click" />
                                &nbsp;<asp:Label ID="msgLabel" runat="server" ForeColor="#FF3300"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="135">
                                Mob 12
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="mob12DropDownList" runat="server" Width="55px" AutoPostBack="false"
                                    ><%--OnSelectedIndexChanged="mob12DropDownList_SelectedIndexChanged"--%>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="135">
                                BIL guarded MUE
                            </td>
                            <td align="left">
                                <asp:TextBox ID="bilGrdMueTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Credit Card
                            </td>
                            <td align="left">
                                <asp:TextBox ID="creditCrdTextBox" runat="server" Text="500000" ReadOnly="True">500000</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Net allowed
                            </td>
                            <td align="left">
                                <asp:TextBox ID="netAllowedTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        </table>
                     </div>
                     <div style="border:solid 1px gray ">
                        <table width="100%">
                        <tr>
                            <td align="left">
                                Existing BIL O/S
                            </td>
                            <td align="left">
                                <asp:TextBox ID="existingBilOSTextBox" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="bilOSHiddenField" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                FD value
                            </td>
                            <td align="left">
                                <asp:TextBox ID="fdValueTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Unsecured (BIL)
                            </td>
                            <td align="left">
                                <asp:TextBox ID="unsecuredBillTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Unsecured (other)
                            </td>
                            <td align="left">
                                <asp:TextBox ID="unsecuredOtherTextBox" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="unsecureOtherHiddenField" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Credit Card
                            </td>
                            <td align="left">
                                <asp:TextBox ID="creditCrd2TextBox" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="creditCardHiddenField" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Net unsecured
                            </td>
                            <td align="left">
                                <asp:TextBox ID="netUnsecuredTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Available
                            </td>
                            <td align="left">                               
                                        <asp:TextBox ID="availableTextBox" runat="server"></asp:TextBox>
                                   
                            </td>
                        </tr>
                    </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <div id="facilityDivContainer" style="padding-left: 80px;">
                        <div id="facilityDiv" class="DivBorder">
                            <table cellpadding="1" cellspacing="0">
                                <tr>
                                    <td width="50">
                                        &nbsp;
                                    </td>
                                    <td width="100" class="GvTableHeader">
                                        Facility
                                    </td>
                                    <td width="80" class="GvTableHeader">
                                        Exposure
                                    </td>
                                    <td width="80" class="GvTableHeader">
                                        Security
                                    </td>
                                    <td width="80" class="GvTableHeader">
                                        NAR
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel" width="30">
                                        1
                                    </td>
                                    <td width="100">
                                        <asp:TextBox ID="facility1TextBox" runat="server" Width="100px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facExpsure1TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facSecrty1TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facNar1TextBox" runat="server" Width="80px" Height="16px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        2
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facility2TextBox" runat="server" Width="100px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facExpsure2TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facSecrty2TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facNar2TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        3
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facility3TextBox" runat="server" Width="100px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facExpsure3TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facSecrty3TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facNar3TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        4
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facility4TextBox" runat="server" Width="100px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facExpsure4TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facSecrty4TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facNar4TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        5
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facility5TextBox" runat="server" Width="100px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facExpsure5TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facSecrty5TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facNar5TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        6
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facility6TextBox" runat="server" Width="100px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facExpsure6TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facSecrty6TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="facNar6TextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        7
                                    </td>
                                    <td>
                                        PL (Proposed)
                                    </td>
                                    <td>
                                        <asp:TextBox ID="plExprTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="plSecrtyTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="plNarTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        8
                                    </td>
                                    <td width="100">
                                        Credit Card
                                    </td>
                                    <td>
                                        <asp:TextBox ID="crdtCrdExpTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="crdtCrdSecrtyTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="crdtCrdNarTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="otherDirDivContainer" style="padding-left: 80px;">
                        <div id="otherDirDiv" class="DivBorder">
                            <table cellpadding="1" cellspacing="0">
                                <tr>
                                    <td width="50">
                                        &nbsp;
                                    </td>
                                    <td class="GvTableHeader" colspan="4">
                                        Other Director&#39;s Facility Aggregate
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        1
                                    </td>
                                    <td width="100">
                                        BIL
                                    </td>
                                    <td>
                                        <asp:TextBox ID="bilExpTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="bilSecrtyTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="bilNarTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        2
                                    </td>
                                    <td>
                                        PL/IPF/FLEXI
                                    </td>
                                    <td>
                                        <asp:TextBox ID="flexiExpTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="flexiSecrtyTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="flexiNarTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td class="ssRowLabel">
                                        3
                                    </td>
                                    <td>
                                        Credit Card
                                    </td>
                                    <td>
                                        <asp:TextBox ID="otherDirCrditcrdExpTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="otherDirCrditcrdSecrtyTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="otherDirCrditcrdNarTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 20px;">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="font-weight: bold">
                                        Net
                                    </td>
                                    <td>
                                        <asp:TextBox ID="netExpTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="netSecrtyTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="netNarTextBox" runat="server" Width="80px" Height="15px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 200px;">
                    &nbsp;
                </td>
                <td style="padding-left: 170px;">
                    <asp:Button ID="saveButton" Text="Save" runat="server" Width="100px" Height="25px"
                        OnClick="saveButton_Click" />
                    <asp:Button ID="clearButton" Text="Clear" runat="server" Width="100px" Height="25px"
                        OnClick="clearButton_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
