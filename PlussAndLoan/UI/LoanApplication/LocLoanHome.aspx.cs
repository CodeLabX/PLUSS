﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI.LoanApplication
{
    public partial class UI_LocLoanHome : System.Web.UI.Page
    {
        readonly LocatorApplicationService _service = new LocatorApplicationService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string strLlid = Request.QueryString["llid"];
                string strAcc = Request.QueryString["acc"];
                string strArm = Request.QueryString["arm"];
                string strName = Request.QueryString["name"];

                if (!string.IsNullOrEmpty(strLlid))
                {
                    findById.Text = strLlid;
                    btnSearch_Click(this, null);
                }

                LoadProduct();
                LoadLoanState();
                LoadGrid(GridFilterBy(strLlid, strAcc, strArm, strName));
            }
        }

        private void LoadLoanState()
        {
            var loanApplicationManagerObj = new LoanApplicationManager();
            var dt = loanApplicationManagerObj.GetLoanStates();
            var loanStates = DataTableToList.GetList<Loan>(dt);
            var state = new Loan() { LoanState = 0, StateName = "" };
            loanStates.Insert(0, state);
            sel_loanStatus.DataTextField = "StateName";
            sel_loanStatus.DataValueField = "LoanState";
            sel_loanStatus.DataSource = loanStates;
            sel_loanStatus.DataBind();
        }

        private void LoadProduct()
        {
            List<Product> productListObj = new LoanApplicationManager().GetAllProduct();
            var productObj = new Product { ProductId = 0, ProductName = "" };
            productListObj.Insert(0, productObj);
            findByProduct.DataTextField = "ProductName";
            findByProduct.DataValueField = "ProductId";
            findByProduct.DataSource = productListObj;
            findByProduct.DataBind();
        }

        private void LoadGrid(string filter)
        {
            DateTime dateTime = Convert.ToDateTime(WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"]);

            var dtTmpList = _service.GetLoanApplications(dateTime, 0, filter);
            var total = _service.GetCountOfLoans();
            string filterCount = "";
            if (dtTmpList != null && dtTmpList.Rows.Count > 0 && dtTmpList.Rows.Count != total)
            {
                var filterTotal  = dtTmpList.Rows.Count;
                filterCount = string.Format("  -  Total {0} Applications found by filter",filterTotal);
            }
            totalLoan.Text = string.Format("Total {0} Loan Applications {1}", total, filterCount);
            gvData.DataSource = dtTmpList;
            gvData.DataBind();
        }


        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvData.PageIndex = e.NewPageIndex;
            LoadGrid(GridFilterBy());
        }


        protected void Edit(object sender, EventArgs e)
        {
            ////
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            LoadGrid(GridFilterBy());
        }

        private string GridFilterBy(string pLlid = "", string pAcc = "", string pArmCode = "", string pName = "")
        {
            var filterBy = "";
            var llid = pLlid ?? findById.Text;
            var productId = findByProduct.SelectedValue;
            var acc = pAcc ?? findAcBy.Text;
            var customer = pName ?? findByCustomer.Text;
            var armCode = pArmCode ?? txtARMCode.Text;
            var state = sel_loanStatus.SelectedValue;
            if (!string.IsNullOrEmpty(llid))
            {
                filterBy += string.Format(" where l.LOAN_APPL_ID={0}", llid);
            }
            if (productId != "0")
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.PROD_ID={0}", productId);
                }
                else
                {
                    filterBy += string.Format(" and l.PROD_ID={0}", productId);
                }

            }
            if (!string.IsNullOrEmpty(acc))
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.ACC_NUMB like '%{0}%'", acc);
                }
                else
                {
                    filterBy += string.Format(" and l.ACC_NUMB like '%{0}%'", acc);
                }

            }
            if (!string.IsNullOrEmpty(armCode))
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.ARM_Code like '%{0}%'", txtARMCode.Text);
                }
                else
                {
                    filterBy += string.Format(" and l.ARM_Code like '%{0}%'", txtARMCode.Text);
                }

            }
            if (!string.IsNullOrEmpty(customer))
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.CUST_NAME like '%{0}%'", customer);
                }
                else
                {
                    filterBy += string.Format(" and l.CUST_NAME like '%{0}%'", customer);
                }

            }
            if (state != "0")
            {
                if (string.IsNullOrEmpty(filterBy))
                {
                    filterBy += string.Format(" where l.LOAN_STATUS_ID='{0}'", state);
                }
                else
                {
                    filterBy += string.Format(" and l.LOAN_STATUS_ID='{0}'", state);
                }

            }
            return filterBy;
        }
        protected void refresh_Click(object sender, EventArgs e)
        {
            findById.Text = "";
            findByProduct.SelectedValue = "0";
            findAcBy.Text = "";
            findByCustomer.Text = "";
            LoadGrid("");
        }

    }
}