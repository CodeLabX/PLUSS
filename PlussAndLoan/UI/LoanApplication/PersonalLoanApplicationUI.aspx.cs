﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using BLL;
using System.Data;
using BLL.Report;
using BusinessEntities;
using BusinessEntities.Report;
using DAL;
using AzUtilities;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI.LoanApplication
{
    public partial class UI_PersonalLoanApplicationUI : System.Web.UI.Page
    {
        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            STATUSUPDATE = 3,
            INSERTERROR = -1,
            UPDATEERROR = -2,
            STATUSUPDATEERROR = -3
        }
        #endregion

        enum ForwardOrDeferFlag : int
        {
            FORWARDID = 4,
            DEFERID = 6
        }
        #region Global variable
        private LoanApplicationManager loanApplicationManagerObj;
        List<AccountType> accountTypeListObj;
        List<IdType> idTypeListObj;
        List<EducationLevel> educationLevelListObj;
        LoanInformation loanInformationObj = null;
        private const Int32 SCBACCLENGTH = 11;
        public PayRollManager payRollManagerObj = null;
        #endregion

        #region PageLoad
        public LoanApplicationQueueManager loanApplicationQueueManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            tinNoMasterTextBox.Visible = false;
            tinNoPostTextBox.Visible = false;
            Page.Form.DefaultFocus = LLIDTextBox.ClientID;
            Page.Form.DefaultButton = findButtonTextBox.UniqueID;

            loanApplicationManagerObj = new LoanApplicationManager();
            loanApplicationQueueManagerObj = new LoanApplicationQueueManager();
            payRollManagerObj = new PayRollManager();
            accountTypeListObj = loanApplicationManagerObj.GetAllAccountType();
            educationLevelListObj = loanApplicationManagerObj.GetAllEducationLevel();
            idTypeListObj = loanApplicationManagerObj.GetAllIdTypes();

            if (!IsPostBack)
            {
                InitializedData();
                ProductDropDownListItem();
                SourceDropDownListItem();
                BankDropDownListItem();
                BranchDropDownListInitailItem();

                AccountTypeDropDownListItem();
                IdTypeDropDownListItem();
                ProfessionDropDownListItems();
                EducationLevelDropDownListItems();
                SpouseDropDownListItems();
                FillRemarksGridView();
                saveButton.Enabled = true;
                deferButton.Enabled = true;
                forwardButton.Enabled = true;
            }
            foreach (GridViewRow gvr in otherBankGridView.Rows)
            {
                (gvr.FindControl("bankDropDownList") as DropDownList).Attributes.Add("OnChange", "return populatebranchDropDown(this);");
                (gvr.FindControl("branchDropDownList") as DropDownList).Attributes.Add("OnChange", "return JS_FunctionValueAssign(this);");
            }
            companyNameTextBox.Attributes.Add("onkeyup", "ajax_showOptions(this,'getItemNameByLetters',event);");
            tinNoPreTextBox.Attributes.Add("onkeyup", "CursorAutoPosition1(1);");
            tinNoMasterTextBox.Attributes.Add("onkeyup", "CursorAutoPosition1(2);");
            tinNoPostTextBox.Attributes.Add("onkeyup", "CursorAutoPosition1(3);");
            dobTextBox.Attributes.Add("onblur", "FormatedDate('" + dobTextBox.ClientID + "');");
            dobTextBox.Attributes.Add("onkeyup", "AutoFormatDate('" + dobTextBox.ClientID + "');");
            maritalStatusDropDownList.Attributes.Add("onchange", "MaritalStatusChange();");
            professionDropdownList.Attributes.Add("onchange", "IsProfessionSelect('" + professionDropdownList.ClientID + "', 'Please enter company name.');");
            (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',1);");
            (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',2);");
            (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',3);");

            (scbAccGridView.Rows[1].Cells[0].FindControl("scbPreTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[1].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[1].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[1].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',1);");
            (scbAccGridView.Rows[1].Cells[1].FindControl("scbMasterTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[1].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[1].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[1].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',2);");
            (scbAccGridView.Rows[1].Cells[2].FindControl("scbPostTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[1].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[1].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[1].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',3);");

            (scbAccGridView.Rows[2].Cells[0].FindControl("scbPreTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[2].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[2].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[2].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',1);");
            (scbAccGridView.Rows[2].Cells[1].FindControl("scbMasterTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[2].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[2].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[2].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',2);");
            (scbAccGridView.Rows[2].Cells[2].FindControl("scbPostTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[2].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[2].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[2].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',3);");

            (scbAccGridView.Rows[3].Cells[0].FindControl("scbPreTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[3].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[3].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[3].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',1);");
            (scbAccGridView.Rows[3].Cells[1].FindControl("scbMasterTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[3].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[3].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[3].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',2);");
            (scbAccGridView.Rows[3].Cells[2].FindControl("scbPostTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[3].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[3].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[3].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',3);");

            (scbAccGridView.Rows[4].Cells[0].FindControl("scbPreTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[4].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[4].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[4].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',1);");
            (scbAccGridView.Rows[4].Cells[1].FindControl("scbMasterTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[4].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[4].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[4].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',2);");
            (scbAccGridView.Rows[4].Cells[2].FindControl("scbPostTextBox") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (scbAccGridView.Rows[4].Cells[0].FindControl("scbPreTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[4].Cells[1].FindControl("scbMasterTextBox") as TextBox).ClientID + "','" + (scbAccGridView.Rows[4].Cells[2].FindControl("scbPostTextBox") as TextBox).ClientID + "',2);");

            (creditCardNoGridView.Rows[0].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (creditCardNoGridView.Rows[0].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).ClientID + "');");
            (creditCardNoGridView.Rows[1].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (creditCardNoGridView.Rows[1].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).ClientID + "');");
            (creditCardNoGridView.Rows[2].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (creditCardNoGridView.Rows[2].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).ClientID + "');");
            (creditCardNoGridView.Rows[3].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (creditCardNoGridView.Rows[3].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).ClientID + "');");
            (creditCardNoGridView.Rows[4].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (creditCardNoGridView.Rows[4].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).ClientID + "');");

            (idGridView.Rows[0].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (idGridView.Rows[0].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
            (idGridView.Rows[1].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (idGridView.Rows[1].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
            (idGridView.Rows[2].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (idGridView.Rows[2].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
            (idGridView.Rows[3].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (idGridView.Rows[3].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
            (idGridView.Rows[4].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (idGridView.Rows[4].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");

            (idGridView.Rows[0].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (idGridView.Rows[0].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
            (idGridView.Rows[1].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (idGridView.Rows[1].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
            (idGridView.Rows[2].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (idGridView.Rows[2].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
            (idGridView.Rows[3].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (idGridView.Rows[3].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
            (idGridView.Rows[4].Cells[2].FindControl("expiryDateTextBox") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (idGridView.Rows[4].Cells[2].FindControl("expiryDateTextBox") as TextBox).ClientID + "');");
        }

        private void LoadData()
        {
            int i = 1;
            List<LoanApplicationQueue> loanApplicationQueue4PLObjList = new List<LoanApplicationQueue>();
            List<LoanApplicationQueue> loanApplicationQueue4LLObjList = new List<LoanApplicationQueue>();
            List<LoanApplicationQueue> loanApplicationQueueObjList = new List<LoanApplicationQueue>();

            FileInfo LoanApplicationListXML = new FileInfo(Server.MapPath("../includes/LoanApplicationList.xml"));
            StreamWriter stremWriter = null;
            stremWriter = LoanApplicationListXML.CreateText();
            loanApplicationQueue4LLObjList = loanApplicationQueueManagerObj.GetLoanApplicationQueueFromLL();
            loanApplicationQueue4PLObjList = loanApplicationQueueManagerObj.GetLoanApplicationQueueFromPL();
            if (loanApplicationQueue4LLObjList.Count > 0)
            {
                loanApplicationQueueObjList.AddRange(loanApplicationQueue4LLObjList);
            }
            if (loanApplicationQueue4PLObjList.Count > 0)
            {
                loanApplicationQueueObjList.AddRange(loanApplicationQueue4PLObjList);
            }
            try
            {
                stremWriter.WriteLine("<?xml version='1.0' encoding='iso-8859-1'?>");
                stremWriter.WriteLine("<rows>");
                foreach (LoanApplicationQueue loanApplicationQueueObj in loanApplicationQueueObjList)
                {
                    stremWriter.WriteLine("<row id='" + i.ToString() + "'>"); ;
                    stremWriter.WriteLine("<cell>");
                    stremWriter.WriteLine(loanApplicationQueueObj.LLId.ToString());
                    stremWriter.WriteLine("</cell>");
                    stremWriter.WriteLine("<cell>");
                    stremWriter.WriteLine(loanApplicationQueueObj.CustomerName.ToString());
                    stremWriter.WriteLine("</cell>");
                    stremWriter.WriteLine("<cell>");
                    stremWriter.WriteLine(loanApplicationQueueObj.Status.ToString());
                    stremWriter.WriteLine("</cell>");
                    stremWriter.WriteLine("<cell>");
                    stremWriter.WriteLine(loanApplicationQueueObj.Remarks.ToString());
                    stremWriter.WriteLine("</cell>");
                    stremWriter.WriteLine("</row>");
                    i = i + 1;
                }
                stremWriter.WriteLine("</rows>");
            }
            finally
            {
                stremWriter.Close();
            }
            stremWriter.Close();
        }
        #endregion

        #region Initialized Data
        /// <summary>
        /// This method populate intialize data.
        /// </summary>
        private void InitializedData()
        {
            dateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            string a = "";
            List<string> stringList = new List<string>();
            List<string> contactNoListObj = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                a = "";
                stringList.Add(a);
            }
            for (int j = 0; j < 4; j++)
            {
                a = "";
                contactNoListObj.Add(a);
            }
            List<string> maritalStatus = new List<string>();
            maritalStatus.Insert(0, "Married");
            maritalStatus.Insert(1, "Single");
            maritalStatusDropDownList.DataSource = maritalStatus;
            maritalStatusDropDownList.DataBind();

            List<string> residentialStatus = new List<string>();
            residentialStatus.Insert(0, "Own");
            residentialStatus.Insert(1, "Family Owned");
            residentialStatus.Insert(2, "Others");
            residentialStatusDropDownList.DataSource = residentialStatus;
            residentialStatusDropDownList.DataBind();

            List<Vechile> vechileListObj = new List<Vechile>();
            Vechile vechileObj = new Vechile();
            vechileObj.VechileId = 0;
            vechileObj.VechileType = "Yes";
            vechileListObj.Add(vechileObj);
            Vechile vechileObj2 = new Vechile();
            vechileObj2.VechileId = 1;
            vechileObj2.VechileType = "No";
            vechileListObj.Add(vechileObj2);
            vechicleDropDownList.DataSource = vechileListObj;
            vechicleDropDownList.DataTextField = "VechileType";
            vechicleDropDownList.DataValueField = "VechileId";
            vechicleDropDownList.DataBind();
            vechicleDropDownList.SelectedValue = "1";

            scbAccGridView.DataSource = stringList;
            scbAccGridView.DataBind();

            creditCardNoGridView.DataSource = stringList;
            creditCardNoGridView.DataBind();

            otherBankGridView.DataSource = stringList;
            otherBankGridView.DataBind();

            idGridView.DataSource = stringList;
            idGridView.DataBind();

            contactGridView.DataSource = stringList;
            contactGridView.DataBind();

            /*
        jointApplicantaccNoGridView1.DataSource = stringList;
        jointApplicantaccNoGridView1.DataBind();

        jointCreditCardGridView1.DataSource = stringList;
        jointCreditCardGridView1.DataBind();

        jointcontactGridView1.DataSource = contactNoListObj;
        jointcontactGridView1.DataBind();

        jointIdGridView1.DataSource = stringList;
        jointIdGridView1.DataBind();

        jAppSCBAccGridView2.DataSource = stringList;
        jAppSCBAccGridView2.DataBind();

        jCrdtCrdGridView2.DataSource = stringList;
        jCrdtCrdGridView2.DataBind();

        jAppContactGridView2.DataSource = contactNoListObj;
        jAppContactGridView2.DataBind();

        jAppIdGridView2.DataSource = stringList;
        jAppIdGridView2.DataBind();

        jAppSCBAccGridView3.DataSource = stringList;
        jAppSCBAccGridView3.DataBind();

        jAppCrdtCrdGridView3.DataSource = stringList;
        jAppCrdtCrdGridView3.DataBind();

        jAppContactGridView3.DataSource = contactNoListObj;
        jAppContactGridView3.DataBind();

        jAppIdGridView3.DataSource = stringList;
        jAppIdGridView3.DataBind();

        jSCBAccGridView4.DataSource = stringList;
        jSCBAccGridView4.DataBind();

        jAppCrdtCrdGridView4.DataSource = stringList;
        jAppCrdtCrdGridView4.DataBind();

        jAppContactGridView4.DataSource = contactNoListObj;
        jAppContactGridView4.DataBind();

        jAppIdGridView4.DataSource = stringList;
        jAppIdGridView4.DataBind();
         */

            productDropDownList.SelectedValue = "0";
            sourceDropDownList.SelectedValue = "0";
            educationalDropDownList.Text = "";
            maritalStatusDropDownList.SelectedIndex = 1;
            residentialStatusDropDownList.SelectedIndex = 2;
            professionDropdownList.Text = "";
            spousesOccupationDropdownList.Text = "";
            foreach (GridViewRow gvr in otherBankGridView.Rows)
            {
                (gvr.FindControl("bankDropDownList") as DropDownList).SelectedValue = "0";
                (gvr.FindControl("branchDropDownList") as DropDownList).SelectedValue = "0";
                (gvr.FindControl("acTypeDropDownList") as DropDownList).SelectedValue = "";
            }
            foreach (GridViewRow gvr in idGridView.Rows)
            {
                (gvr.FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "";
            }
            /*jointEducationalDropDownList1.SelectedValue = "";
        jAppEducationDropDownList2.SelectedValue = "";
        foreach (GridViewRow gvr in jointIdGridView1.Rows)
        {
            (gvr.FindControl("jointIdTypeDropDownList1") as DropDownList).SelectedValue = "";
        }
        foreach (GridViewRow gvr in jAppIdGridView2.Rows)
        {
            (gvr.FindControl("jAppIdTypeDropDownList2") as DropDownList).SelectedValue = "";
        }
         */
            contactGridView.Rows[0].Cells[0].Text = "RS Tel";
            contactGridView.Rows[1].Cells[0].Text = "RS Cell";
            contactGridView.Rows[2].Cells[0].Text = "Off Tel";
            contactGridView.Rows[3].Cells[0].Text = "Off Cell";
            contactGridView.Rows[4].Cells[0].Text = "Others";
            //(contactGridView.Rows[0].Cells[1].FindControl("contactNoTextBox") as TextBox).Text = "";

            /*
        jointcontactGridView1.Rows[0].Cells[0].Text = "RS Tel";
         jointcontactGridView1.Rows[1].Cells[0].Text = "RS Cell";
         jointcontactGridView1.Rows[2].Cells[0].Text = "Off Tel";
         jointcontactGridView1.Rows[3].Cells[0].Text = "Off Cell";


         jAppContactGridView2.Rows[0].Cells[0].Text = "RS Tel";
         jAppContactGridView2.Rows[1].Cells[0].Text = "RS Cell";
         jAppContactGridView2.Rows[2].Cells[0].Text = "Off Tel";
         jAppContactGridView2.Rows[3].Cells[0].Text = "Off Cell";


         jAppContactGridView3.Rows[0].Cells[0].Text = "RS Tel";
         jAppContactGridView3.Rows[1].Cells[0].Text = "RS Cell";
         jAppContactGridView3.Rows[2].Cells[0].Text = "Off Tel";
         jAppContactGridView3.Rows[3].Cells[0].Text = "Off Cell";

         jAppContactGridView4.Rows[0].Cells[0].Text = "RS Tel";
         jAppContactGridView4.Rows[1].Cells[0].Text = "RS Cell";
         jAppContactGridView4.Rows[2].Cells[0].Text = "Off Tel";
         jAppContactGridView4.Rows[3].Cells[0].Text = "Off Cell";
         */

            genderDropDownList.Items.Insert(0, new ListItem("Male"));
            genderDropDownList.Items.Insert(1, new ListItem("Female"));
            genderDropDownList.SelectedIndex = 0;
            multipleLoanDropDownList.Items.Insert(0, new ListItem("Yes"));
            multipleLoanDropDownList.Items.Insert(1, new ListItem("No"));
            multipleLoanDropDownList.SelectedIndex = 1;
            earlierLoanWScbDropDownList.Items.Insert(0, new ListItem("Yes"));
            earlierLoanWScbDropDownList.Items.Insert(1, new ListItem("No"));
            earlierLoanWScbDropDownList.SelectedIndex = 1;
            availedAutoLoanDropDownList.Items.Insert(0, new ListItem("Yes"));
            availedAutoLoanDropDownList.Items.Insert(1, new ListItem("No"));
            availedAutoLoanDropDownList.SelectedIndex = 1;
            mortageLoanDropDownList.Items.Insert(0, new ListItem("Yes"));
            mortageLoanDropDownList.Items.Insert(1, new ListItem("No"));
            mortageLoanDropDownList.SelectedIndex = 1;


        }
        #endregion

        #region Product DropDown Items
        /// <summary>
        /// This method gives product dropdown items.
        /// </summary>
        private void ProductDropDownListItem()
        {
            List<Product> productListObj = loanApplicationManagerObj.GetAllProduct();
            Product productObj = new Product();
            productObj.ProductId = 0;
            productObj.ProductName = "";
            //productListObj.Add(productObj);
            productListObj.Insert(0, productObj);
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "ProductId";
            productDropDownList.DataSource = productListObj;
            productDropDownList.DataBind();
        }
        #endregion

        #region Source DropDown Items
        /// <summary>
        /// This method gives source dropdown items.
        /// </summary>
        private void SourceDropDownListItem()
        {
            List<Source> sourceLisObj = loanApplicationManagerObj.GetAllSource();
            Source sourceObj = new Source();
            sourceObj.SourceId = 0;
            sourceObj.SourceName = "";
            sourceLisObj.Insert(0, sourceObj);
            sourceDropDownList.DataTextField = "SourceName";
            sourceDropDownList.DataValueField = "SourceId";
            sourceDropDownList.DataSource = sourceLisObj;
            sourceDropDownList.DataBind();
        }
        #endregion

        #region Vechile DropDown Items
        /// <summary>
        /// This method gives source dropdown items.
        /// </summary>
        private void VechileDropDownListItem()
        {
            List<Vechile> vechileLisObj = loanApplicationManagerObj.GetAllVechile();
            vechicleDropDownList.DataTextField = "VechileType";
            vechicleDropDownList.DataValueField = "VechileId";
            vechicleDropDownList.DataSource = vechileLisObj;
            vechicleDropDownList.DataBind();
        }
        #endregion

        #region Bank DropDown Items
        /// <summary>
        /// This method gives bank dropdown items.
        /// </summary>
        private void BankDropDownListItem()
        {
            List<BankInformation> bankInformationListObj = loanApplicationManagerObj.GetAllBanks();
            BankInformation bankInformationObj = new BankInformation();
            bankInformationObj.Name = "";
            bankInformationObj.Id = 0;
            bankInformationListObj.Insert(0, bankInformationObj);
            foreach (GridViewRow gvr in otherBankGridView.Rows)
            {
                (gvr.FindControl("bankDropDownList") as DropDownList).DataSource = bankInformationListObj;
                (gvr.FindControl("bankDropDownList") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("bankDropDownList") as DropDownList).DataValueField = "Id";
                (gvr.FindControl("bankDropDownList") as DropDownList).DataBind();
            }
        }
        #endregion

        #region Branch DropDown Items
        /// <summary>
        /// This method gives branch dropdown items.
        /// </summary>
        private void BranchDropDownListItem()
        {
            List<BranchInformation> branchInformationListObj = loanApplicationManagerObj.GetAllBranchs();
            BranchInformation branchInformationObj = new BranchInformation();
            branchInformationObj.Id = 0;
            branchInformationObj.Name = "";
            branchInformationListObj.Insert(0, branchInformationObj);
            //branchInformationListObj.Add(branchInformationObj);
            foreach (GridViewRow gvr in otherBankGridView.Rows)
            {
                (gvr.FindControl("branchDropDownList") as DropDownList).DataSource = branchInformationListObj;
                (gvr.FindControl("branchDropDownList") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("branchDropDownList") as DropDownList).DataValueField = "Id";
                (gvr.FindControl("branchDropDownList") as DropDownList).DataBind();
            }
        }
        #endregion

        #region Branch DropDown Items
        /// <summary>
        /// This method gives branch dropdown items.
        /// </summary>
        private void BranchDropDownListItem(int rowIndex, Int32 bankId)
        {
            List<BranchInformation> branchInformationListObj = loanApplicationManagerObj.GetAllBranchs(bankId);
            BranchInformation branchInformationObj = new BranchInformation();
            branchInformationObj.Id = 0;
            branchInformationObj.Name = "";
            branchInformationListObj.Insert(0, branchInformationObj);
            //branchInformationListObj.Add(branchInformationObj);
            //foreach (GridViewRow gvr in otherBankGridView.Rows)
            //{
            (otherBankGridView.Rows[rowIndex].FindControl("branchDropDownList") as DropDownList).DataSource = branchInformationListObj;
            (otherBankGridView.Rows[rowIndex].FindControl("branchDropDownList") as DropDownList).DataTextField = "Name";
            (otherBankGridView.Rows[rowIndex].FindControl("branchDropDownList") as DropDownList).DataValueField = "Id";
            (otherBankGridView.Rows[rowIndex].FindControl("branchDropDownList") as DropDownList).DataBind();

            //}
        }
        #endregion

        #region Branch DropDown Initial Items
        /// <summary>
        /// This method gives branch dropdown items.
        /// </summary>
        private void BranchDropDownListInitailItem()
        {
            List<BranchInformation> branchInformationListObj = new List<BranchInformation>();
            BranchInformation branchInformationObj = new BranchInformation();
            branchInformationObj.Id = 0;
            branchInformationObj.Name = "";
            branchInformationListObj.Insert(0, branchInformationObj);
            foreach (GridViewRow gvr in otherBankGridView.Rows)
            {
                (gvr.FindControl("branchDropDownList") as DropDownList).DataSource = branchInformationListObj;
                (gvr.FindControl("branchDropDownList") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("branchDropDownList") as DropDownList).DataValueField = "Id";
                (gvr.FindControl("branchDropDownList") as DropDownList).DataBind();
            }
        }
        #endregion

        #region Account Type DropDown Items
        /// <summary>
        /// This method gives account type dropdown items.
        /// </summary>
        private void AccountTypeDropDownListItem()
        {
            AccountType accountTypeObj = new AccountType();
            accountTypeObj.Id = 0;
            accountTypeObj.Name = "";
            accountTypeListObj.Insert(0, accountTypeObj);
            foreach (GridViewRow gvr in otherBankGridView.Rows)
            {
                (gvr.FindControl("acTypeDropDownList") as DropDownList).DataSource = accountTypeListObj;
                (gvr.FindControl("acTypeDropDownList") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("acTypeDropDownList") as DropDownList).DataValueField = "Name";
                (gvr.FindControl("acTypeDropDownList") as DropDownList).DataBind();
            }
        }
        #endregion

        #region Id Type DropDown Items
        /// <summary>
        /// This method gives account type dropdown items.
        /// </summary>
        private void IdTypeDropDownListItem()
        {
            IdType idTypeObj = new IdType();
            idTypeObj.Id = 0;
            idTypeObj.Name = "";
            idTypeListObj.Insert(0, idTypeObj);
            foreach (GridViewRow gvr in idGridView.Rows)
            {
                (gvr.FindControl("idTypeDropDownList") as DropDownList).DataSource = idTypeListObj;
                (gvr.FindControl("idTypeDropDownList") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("idTypeDropDownList") as DropDownList).DataValueField = "Name";
                (gvr.FindControl("idTypeDropDownList") as DropDownList).DataBind();
            }
        }
        #endregion

        #region Profession DropDown List Items
        /// <summary>
        /// This method populate all professions.
        /// </summary>
        private void ProfessionDropDownListItems()
        {
            List<Profession> professionListObj = loanApplicationManagerObj.GetAllProfession();
            Profession professionObj = new Profession();
            professionObj.Id = 0;
            professionObj.Name = "";
            professionListObj.Insert(0, professionObj);
            professionDropdownList.DataSource = professionListObj;
            professionDropdownList.DataTextField = "Name";
            professionDropdownList.DataValueField = "Name";
            professionDropdownList.DataBind();
        }
        #endregion

        #region Education Level DropDown List Items
        /// <summary>
        /// This method populate all education levels.
        /// </summary>
        private void EducationLevelDropDownListItems()
        {
            EducationLevel educationLevelObj = new EducationLevel();
            educationLevelObj.Id = 0;
            educationLevelObj.Name = "";
            educationLevelListObj.Insert(0, educationLevelObj);
            educationalDropDownList.DataSource = educationLevelListObj;
            educationalDropDownList.DataTextField = "Name";
            educationalDropDownList.DataValueField = "Name";
            educationalDropDownList.DataBind();
        }
        #endregion

        #region Sopuse Profession DropDown List Items
        /// <summary>
        /// This method populate all spouse professions.
        /// </summary>
        private void SpouseDropDownListItems()
        {
            List<Profession> professionListObj = loanApplicationManagerObj.GetAllProfession();
            Profession professionObj = new Profession();
            professionObj.Id = 0;
            professionObj.Name = "";
            professionListObj.Insert(0, professionObj);
            spousesOccupationDropdownList.DataSource = professionListObj;
            spousesOccupationDropdownList.DataTextField = "Name";
            spousesOccupationDropdownList.DataValueField = "Name";
            spousesOccupationDropdownList.DataBind();

        }
        #endregion




        #region FillRemarksGridView
        /// <summary>
        /// This method fill remarks gridview.
        /// </summary>
        private void FillRemarksGridView()
        {
            List<Remarks> remarksListObj = loanApplicationManagerObj.GetAllRemarks();
            remarksGridView.DataSource = remarksListObj;
            remarksGridView.DataBind();
        }
        #endregion

        #region findButton click event
        /// <summary>
        /// Find button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void findButtonTextBox_Click(object sender, EventArgs e)
        {
            Int32 llid = 0;
            try
            {
                if (String.IsNullOrEmpty(LLIDTextBox.Text))
                {
                    remarksLabel.Text = "Data Not Found! Please Check Status in the Loan Locator System";
                    saveButton.Text = "Save";
                    LLIDTextBox.Focus();
                    ClearLoanLocatorData();
                    ClearLoanApplicationData();

                    return;
                }
                llid = Convert.ToInt32(LLIDTextBox.Text);
            }
            catch (Exception ex)
            {
                remarksLabel.Text = "Error in converting to 'int'";
                saveButton.Text = "Save";
                LLIDTextBox.Focus();
                ClearLoanLocatorData();
                ClearLoanApplicationData();

                string mess = ex.Message;
                modalPopup.ShowError(mess, Title);
                CustomException.Save(ex, "findButtonTextBox_Click - 1st Part");
                return;
            }
            if (IsExistDataInPluss(llid) == false)
            {
                try
                {
                    LoanLocatorLoanApplicant loanLocatorLoanApplicantObj = loanApplicationManagerObj.GetLoanAppLoanLocatorInfo(llid);
                    if (loanLocatorLoanApplicantObj != null)
                    {
                        errorMsgLabel.Text = "";
                        remarksLabel.Text = "";
                        try
                        {
                            productDropDownList.SelectedValue = Convert.ToString(loanLocatorLoanApplicantObj.ProductId);
                            sourceDropDownList.SelectedValue = Convert.ToString(loanLocatorLoanApplicantObj.BranchId);
                        }
                        catch (Exception ex)
                        {
                            string mess = ex.Message;
                            modalPopup.ShowError(mess, Title);
                            CustomException.Save(ex, "findButtonTextBox_Click - productDropDownList");
                        }
                        dateTextBox.ReadOnly = true;
                        dateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
                        customerNameTextBox.Text = Convert.ToString(loanLocatorLoanApplicantObj.ApplicantName);
                        appliedAmountTextBox.Text = Convert.ToString(loanLocatorLoanApplicantObj.LoanApplyAmount);
                        appliedAmountTextBox.ReadOnly = false;
                        string scbAccNo = loanLocatorLoanApplicantObj.AccountNo.ToString();
                        string scbAcc = scbAccNo.Replace("-", "");
                        if (scbAcc.Length.Equals(11))
                        {
                            (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = scbAcc.Substring(0, 2).ToString();
                            (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = scbAcc.Substring(2, 7).ToString();
                            (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = scbAcc.Substring(9, 2).ToString();
                        }
                        else
                        {
                            (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = scbAcc;
                        }

                        if (!string.IsNullOrEmpty(loanLocatorLoanApplicantObj.Organization))
                        {
                            companyNameTextBox.Text = loanLocatorLoanApplicantObj.Organization;
                        }
                        else
                        {
                            companyNameTextBox.Text = loanLocatorLoanApplicantObj.CustBulkData.OrganisationName;
                        }
                            switch (loanLocatorLoanApplicantObj.EmployeType.Trim())
                        {
                            case "Business":
                                professionDropdownList.Text = loanLocatorLoanApplicantObj.EmployeType;
                                break;
                            case "Salaried":
                                professionDropdownList.Text = loanLocatorLoanApplicantObj.EmployeType;
                                break;
                            case "Self Employed":
                                professionDropdownList.Text = loanLocatorLoanApplicantObj.EmployeType;
                                break;
                            case "Doctor":
                                professionDropdownList.Text = loanLocatorLoanApplicantObj.EmployeType;
                                break;
                            case "Landlord":
                                professionDropdownList.Text = loanLocatorLoanApplicantObj.EmployeType;
                                break;
                            case "Others":
                                professionDropdownList.Text = loanLocatorLoanApplicantObj.EmployeType;
                                break;
                            default:
                                professionDropdownList.Text = "";
                                break;
                        }
                        
                        sourcePersonTextBox.Text = loanLocatorLoanApplicantObj.SourcePersonName;
                        txtARMCode.Text = loanLocatorLoanApplicantObj.ARMCode;

                        mothersNameTextBox.Text = loanLocatorLoanApplicantObj.CustBulkData.MotherName;
                        fathersNameTextBox.Text = loanLocatorLoanApplicantObj.CustBulkData.FatherName;
                        if (!String.IsNullOrEmpty(loanLocatorLoanApplicantObj.CustBulkData.DOB))
                        {
                            dobTextBox.Text = Convert.ToDateTime(loanLocatorLoanApplicantObj.CustBulkData.DOB).ToString("dd-MM-yyyy");
                        }
                        documentTypeTextBox.Text = loanLocatorLoanApplicantObj.CustBulkData.SpouseName;
                        
                        tinNoPreTextBox.Text = loanLocatorLoanApplicantObj.CustBulkData.TIN;

                        residentialAddressTextBox.Text = loanLocatorLoanApplicantObj.CustBulkData.AddressRes + " " +
                                                         loanLocatorLoanApplicantObj.CustBulkData.AddressRes_PostalCode + " " +
                                                        loanLocatorLoanApplicantObj.CustBulkData.AddressRes_CityCode + " " +
                                                        loanLocatorLoanApplicantObj.CustBulkData.AddressRes_CityName + " " +
                                                        loanLocatorLoanApplicantObj.CustBulkData.AddressRes_CountryCode;

                        officeAddressTextBox.Text = loanLocatorLoanApplicantObj.CustBulkData.AddressOff + " " +
                                                loanLocatorLoanApplicantObj.CustBulkData.AddressOff_PostalCode + " " +
                                               loanLocatorLoanApplicantObj.CustBulkData.AddressOff_CityCode + " " +
                                               loanLocatorLoanApplicantObj.CustBulkData.AddressOff_CityName + " " +
                                               loanLocatorLoanApplicantObj.CustBulkData.AddressOff_CountryCode;

                        if (!string.IsNullOrEmpty(loanLocatorLoanApplicantObj.CustBulkData.Gender))
                        {
                            if (loanLocatorLoanApplicantObj.CustBulkData.Gender.Equals("F"))
                            {
                                genderDropDownList.SelectedValue = "Female";
                            }
                            else
                            {
                                genderDropDownList.SelectedValue = "Male";
                            }
                        }
                        else
                        {
                            genderDropDownList.SelectedValue = "Male";
                        }

                        if (!string.IsNullOrEmpty(loanLocatorLoanApplicantObj.CustBulkData.MaritalStatus))
                        {
                            if (loanLocatorLoanApplicantObj.CustBulkData.MaritalStatus.Equals("M"))
                            {
                                maritalStatusDropDownList.SelectedValue = "Married";
                            }
                            else if (loanLocatorLoanApplicantObj.CustBulkData.MaritalStatus.Equals("S"))
                            {
                                maritalStatusDropDownList.SelectedValue = "Single";
                            }
                            else
                            {
                                maritalStatusDropDownList.SelectedValue = "Single";
                            }
                        }
                        else
                        {
                            maritalStatusDropDownList.SelectedValue = "Single";
                        }

                        (contactGridView.Rows[0].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanLocatorLoanApplicantObj.CustBulkData.ContactNo1;
                        (contactGridView.Rows[1].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanLocatorLoanApplicantObj.CustBulkData.ContactNo2;

                        (creditCardNoGridView.Rows[0].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text = ""; // loanLocatorLoanApplicantObj.AccountNo;

                        CustomersBulkData customer = loanLocatorLoanApplicantObj.CustBulkData;
                        byte rowNo = 0;

                        deferButton.Enabled = true;
                        forwardButton.Enabled = true;
                        saveButton.Enabled = true;
                        if (!string.IsNullOrWhiteSpace(customer.PassportNo) && !string.IsNullOrEmpty(customer.PassportNo))
                        {
                            //idGridView.Rows[0]
                            (idGridView.Rows[rowNo].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "Passport";
                            (idGridView.Rows[rowNo].Cells[0].FindControl("idNoTextBox") as TextBox).Text = customer.PassportNo;
                            (idGridView.Rows[rowNo].Cells[0].FindControl("expiryDateTextBox") as TextBox).Text = customer.PassportExp;
                            rowNo++;
                        }

                        // New Bulk Insert
                        //loanLocatorLoanApplicantObj.CustBulkData.RefType = Convert.ToString(loanLocatorLoanAppDataReaderObj["RefType"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.RefrenceID = Convert.ToString(loanLocatorLoanAppDataReaderObj["RefrenceID"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.MasterNo = Convert.ToString(loanLocatorLoanAppDataReaderObj["MasterNo"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.RelationshipNo = Convert.ToString(loanLocatorLoanAppDataReaderObj["RelationshipNo"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.FullName = Convert.ToString(loanLocatorLoanAppDataReaderObj["FullName"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.FatherName = Convert.ToString(loanLocatorLoanAppDataReaderObj["FatherName"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.MotherName = Convert.ToString(loanLocatorLoanAppDataReaderObj["MotherName"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.DOB = Convert.ToString(loanLocatorLoanAppDataReaderObj["DOB"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.HighestEduLevel = Convert.ToString(loanLocatorLoanAppDataReaderObj["HighestEduLevel"]);


                        //loanLocatorLoanApplicantObj.CustBulkData.ProfessionCode = Convert.ToString(loanLocatorLoanAppDataReaderObj["ProfessionCode"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.OrganisationName = Convert.ToString(loanLocatorLoanAppDataReaderObj["OrganisationName"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.Department = Convert.ToString(loanLocatorLoanAppDataReaderObj["Department"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.TIN = Convert.ToString(loanLocatorLoanAppDataReaderObj["TIN"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.ContactNo1 = Convert.ToString(loanLocatorLoanAppDataReaderObj["ContactNo1"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.ContactNo2 = Convert.ToString(loanLocatorLoanAppDataReaderObj["ContactNo2"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.MaritalStatus = Convert.ToString(loanLocatorLoanAppDataReaderObj["MaritalStatus"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.Gemder = Convert.ToString(loanLocatorLoanAppDataReaderObj["Gemder"]);
                        //loanLocatorLoanApplicantObj.CustBulkData.SpouseName = Convert.ToString(loanLocatorLoanAppDataReaderObj["SpouseName"]);
                        // End of Bulk Insert

                        deferButton.Enabled = true;
                        forwardButton.Enabled = true;
                        saveButton.Enabled = true;
                    }
                    else
                    {
                        remarksLabel.Text = "Data Not Found! Please Check Status in the Loan Locator System";
                        saveButton.Text = "Save";
                        LLIDTextBox.Focus();
                        ClearLoanLocatorData();
                        ClearLoanApplicationData();
                    }

                }
                catch (Exception ex)
                {
                    string mess = ex.Message;
                    modalPopup.ShowError(mess, Title);
                    CustomException.Save(ex, "findButtonTextBox_Click - Final Part");
                }
            }
        }
        #endregion
        private bool IsExistDataInPluss(Int32 lLId)
        {
            try
            {
                loanInformationObj = loanApplicationManagerObj.GetLoanInformation(lLId);
                if (loanInformationObj != null)
                {
                    LoanApplicantPlussDatabase(lLId);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex){
                string mess = ex.Message;
                modalPopup.ShowError(mess, Title);
                CustomException.Save(ex, ": IsExistDataInPluss: ");
                return false;
            }
        }
        #region LoanApplicantPlussDatabase
        /// <summary>
        /// This method populate loan applicant data from pluss database.
        /// </summary>
        /// <param name="llid">Gets a LLID.</param>
        private void LoanApplicantPlussDatabase(Int32 llid)
        {
            LoanInformation loanInformationObj = loanApplicationManagerObj.GetLoanInformation(llid);
            if (loanInformationObj != null)
            {
                remarksLabel.Text = "";
                saveButton.Text = "Update";
                try
                {
                    productDropDownList.SelectedValue = Convert.ToString(loanInformationObj.ProductId);
                }
                catch
                {
                }
                try
                {
                    sourceDropDownList.SelectedValue = Convert.ToString(loanInformationObj.SourceId);
                }
                catch
                {
                }
                if (DateFormat.IsDate(loanInformationObj.RecieveDate))
                {
                    dateTextBox.Text = loanInformationObj.RecieveDate.ToString("dd-MM-yyyy");
                    dateTextBox.ReadOnly = true;
                }
                else
                {
                    dateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
                    dateTextBox.ReadOnly = false;
                }
                customerNameTextBox.Text = Convert.ToString(loanInformationObj.ApplicantName);
                appliedAmountTextBox.Text = Convert.ToString(loanInformationObj.AppliedAmount);
                sourcePersonTextBox.Text = loanInformationObj.SourcePersonName.ToString();
                txtARMCode.Text = loanInformationObj.ARMCode.ToString();

                (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo1;
                (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo1;
                (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo1;

                (scbAccGridView.Rows[1].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo2;
                (scbAccGridView.Rows[1].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo2;
                (scbAccGridView.Rows[1].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo2;

                (scbAccGridView.Rows[2].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo3;
                (scbAccGridView.Rows[2].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo3;
                (scbAccGridView.Rows[2].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo3;

                (scbAccGridView.Rows[3].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo4;
                (scbAccGridView.Rows[3].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo4;
                (scbAccGridView.Rows[3].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo4;

                (scbAccGridView.Rows[4].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo5;
                (scbAccGridView.Rows[4].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo5;
                (scbAccGridView.Rows[4].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo5;

                if (String.IsNullOrEmpty(loanInformationObj.TINNo.ToString()))
                {
                    tinNoPreTextBox.Text = "";
                    //tinNoMasterTextBox.Text = "";
                    //tinNoPostTextBox.Text = "";
                }
                else
                {
                    string tin = loanInformationObj.TINNo.ToString();
                    string tinNo = tin.Replace("-", "");
                    tinNoPreTextBox.Text = tinNo;//tinNo.Substring(0, 3);
                    //tinNoMasterTextBox.Text = tinNo.Substring(3, 3);
                    //tinNoPostTextBox.Text = tinNo.Substring(6);
                }
                declaredIncomeTextBox.Text = loanInformationObj.DeclaredAmount.ToString();
                (creditCardNoGridView.Rows[0].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text = loanInformationObj.CreditCardNo1;
                (creditCardNoGridView.Rows[1].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text = loanInformationObj.CreditCardNo2;
                (creditCardNoGridView.Rows[2].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text = loanInformationObj.CreditCardNo3;
                (creditCardNoGridView.Rows[3].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text = loanInformationObj.CreditCardNo4;
                (creditCardNoGridView.Rows[4].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text = loanInformationObj.CreditCardNo5;

                fathersNameTextBox.Text = loanInformationObj.FatherName;
                mothersNameTextBox.Text = loanInformationObj.MotherName;
                if (loanInformationObj.DOB != null)
                {
                    dobTextBox.Text = loanInformationObj.DOB.ToString("dd-MM-yyyy");
                }
                else
                {
                    dobTextBox.Text = "";
                }

                switch (loanInformationObj.EducationalLevel.Trim())
                {
                    case "SSC":
                        educationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "HSC":
                        educationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "DIP":
                        educationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "Graduate":
                        educationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "Post-Graduate":
                        educationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "Others":
                        educationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    default:
                        educationalDropDownList.Text = "";
                        break;
                }

                switch (loanInformationObj.MaritalStatus.Trim())
                {
                    case "Married":
                        maritalStatusDropDownList.Text = loanInformationObj.MaritalStatus;
                        break;
                    case "Single":
                        maritalStatusDropDownList.Text = loanInformationObj.MaritalStatus;
                        break;
                    default:
                        maritalStatusDropDownList.Text = "Single";
                        break;
                }

                switch (loanInformationObj.ResidentialStatus.Trim())
                {
                    case "Own":
                        residentialStatusDropDownList.Text = loanInformationObj.ResidentialStatus;
                        break;
                    case "Family Owned":
                        residentialStatusDropDownList.Text = loanInformationObj.ResidentialStatus;
                        break;
                    case "Others":
                        residentialStatusDropDownList.Text = loanInformationObj.ResidentialStatus;
                        break;
                    default:
                        residentialStatusDropDownList.Text = "Others";
                        break;
                }

                residentialAddressTextBox.Text = loanInformationObj.ResidentAddress;
                (contactGridView.Rows[0].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo1;
                (contactGridView.Rows[1].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo2;
                (contactGridView.Rows[2].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo3;
                (contactGridView.Rows[3].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo4;
                (contactGridView.Rows[4].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo5;

                switch (loanInformationObj.Profession.Trim())
                {
                    case "Business":
                        professionDropdownList.Text = loanInformationObj.Profession;
                        break;
                    case "Salaried":
                        professionDropdownList.Text = loanInformationObj.Profession;
                        break;
                    case "Self Employed":
                        professionDropdownList.Text = loanInformationObj.Profession;
                        break;
                    case "Doctor":
                        professionDropdownList.Text = loanInformationObj.Profession;
                        break;
                    case "Landlord":
                        professionDropdownList.Text = loanInformationObj.Profession;
                        break;
                    case "Others":
                        professionDropdownList.Text = loanInformationObj.Profession;
                        break;
                    default:
                        professionDropdownList.Text = "";
                        break;
                }
                companyNameTextBox.Text = loanInformationObj.Business;
                officeAddressTextBox.Text = loanInformationObj.OfficeAddress;
                workExperienceMonthTextBox.Text = Convert.ToString(loanInformationObj.JobDuration);
                switch (loanInformationObj.SpousOcupation.Trim())
                {
                    case "Business":
                        spousesOccupationDropdownList.Text = loanInformationObj.SpousOcupation;
                        break;
                    case "Salaried":
                        spousesOccupationDropdownList.Text = loanInformationObj.SpousOcupation;
                        break;
                    case "Self Employed":
                        spousesOccupationDropdownList.Text = loanInformationObj.SpousOcupation;
                        break;
                    case "Doctor":
                        spousesOccupationDropdownList.Text = loanInformationObj.SpousOcupation;
                        break;
                    case "Landlord":
                        spousesOccupationDropdownList.Text = loanInformationObj.SpousOcupation;
                        break;
                    case "Others":
                        spousesOccupationDropdownList.Text = loanInformationObj.SpousOcupation;
                        break;
                    default:
                        spousesOccupationDropdownList.Text = "";
                        break;
                }



                //if (!string.IsNullOrEmpty(loanInformationObj.OtherBankAccount.OtherBankId1.ToString()))
                //{
                  //  (otherBankGridView.Rows[0].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBankId1.ToString();
                   // BranchDropDownListItem(0, Convert.ToInt32(loanInformationObj.OtherBankAccount.OtherBankId1));
                //}
                //try
                //{
                  //  (otherBankGridView.Rows[0].Cells[1].FindControl("branchDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBranchId1.ToString();
                    //(otherBankGridView.Rows[0].Cells[1].FindControl("branchId") as HiddenField).Value = loanInformationObj.OtherBankAccount.OtherBranchId1.ToString();
               // }
                //catch
               // {
                //}
               // if (loanInformationObj.OtherBankAccount.OtherAccNo1 == "0")
               // {
                 //   (otherBankGridView.Rows[0].Cells[2].FindControl("acNoTextBox") as TextBox).Text = "";
                //}
                //else
                //{
                  //  (otherBankGridView.Rows[0].Cells[2].FindControl("acNoTextBox") as TextBox).Text = loanInformationObj.OtherBankAccount.OtherAccNo1;
                //}

                //bool accountTypeFlag1 = false;
                //for (int i = 0; i < accountTypeListObj.Count; i++)
                //{
                  //  if (loanInformationObj.OtherBankAccount.OtherAccType1.Trim() == accountTypeListObj[i].Name)
                   // {
                     //   accountTypeFlag1 = true;
                      //  break;
                   // }
               // }
                //if (accountTypeFlag1)
                //{
                  //  (otherBankGridView.Rows[0].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = loanInformationObj.OtherBankAccount.OtherAccType1;
                //}
                //else
                //{
                  //  (otherBankGridView.Rows[0].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = "";
                //}


                //if (!string.IsNullOrEmpty(loanInformationObj.OtherBankAccount.OtherBankId2.ToString()))
                //{
                //    (otherBankGridView.Rows[1].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBankId2.ToString();
                //    BranchDropDownListItem(1, Convert.ToInt32(loanInformationObj.OtherBankAccount.OtherBankId2));
                //}
                //try
                //{
                //    (otherBankGridView.Rows[1].Cells[1].FindControl("branchDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBranchId2.ToString();
                //    (otherBankGridView.Rows[1].Cells[1].FindControl("branchId") as HiddenField).Value = loanInformationObj.OtherBankAccount.OtherBranchId2.ToString();
                //}
                //catch
                //{
                //}
                //if (loanInformationObj.OtherBankAccount.OtherAccNo2 == "0")
                //{
                //    (otherBankGridView.Rows[1].Cells[2].FindControl("acNoTextBox") as TextBox).Text = "";
                //}
                //else
                //{
                //    (otherBankGridView.Rows[1].Cells[2].FindControl("acNoTextBox") as TextBox).Text = loanInformationObj.OtherBankAccount.OtherAccNo2;
                //}
                //bool accountTypeFlag2 = false;
                //for (int i = 0; i < accountTypeListObj.Count; i++)
                //{
                //    if (loanInformationObj.OtherBankAccount.OtherAccType2.Trim() == accountTypeListObj[i].Name)
                //    {
                //        accountTypeFlag2 = true;
                //        break;
                //    }
                //}
                //if (accountTypeFlag2)
                //{
                //    (otherBankGridView.Rows[1].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = loanInformationObj.OtherBankAccount.OtherAccType2;
                //}
                //else
                //{
                //    (otherBankGridView.Rows[1].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = "";
                //}
                //if (!string.IsNullOrEmpty(loanInformationObj.OtherBankAccount.OtherBankId3.ToString()))
                //{
                //    (otherBankGridView.Rows[2].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBankId3.ToString();
                //}


                //BranchDropDownListItem(2, Convert.ToInt32(loanInformationObj.OtherBankAccount.OtherBankId3));
                //try
                //{
                //    (otherBankGridView.Rows[2].Cells[1].FindControl("branchDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBranchId3.ToString();
                //    (otherBankGridView.Rows[2].Cells[1].FindControl("branchId") as HiddenField).Value = loanInformationObj.OtherBankAccount.OtherBranchId3.ToString();
                //}
                //catch
                //{
                //}
                //if (loanInformationObj.OtherBankAccount.OtherAccNo3 == "0")
                //{
                //    (otherBankGridView.Rows[2].Cells[2].FindControl("acNoTextBox") as TextBox).Text = "";
                //}
                //else
                //{
                //    (otherBankGridView.Rows[2].Cells[2].FindControl("acNoTextBox") as TextBox).Text = loanInformationObj.OtherBankAccount.OtherAccNo3;
                //}
                //bool accountTypeFlag3 = false;
                //for (int i = 0; i < accountTypeListObj.Count; i++)
                //{
                //    if (loanInformationObj.OtherBankAccount.OtherAccType3.Trim() == accountTypeListObj[i].Name)
                //    {
                //        accountTypeFlag3 = true;
                //        break;
                //    }
                //}
                //if (accountTypeFlag3)
                //{
                //    (otherBankGridView.Rows[2].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = loanInformationObj.OtherBankAccount.OtherAccType3;
                //}
                //else
                //{
                //    (otherBankGridView.Rows[2].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = "";
                //}


                //if (!string.IsNullOrEmpty(loanInformationObj.OtherBankAccount.OtherBankId4.ToString()))
                //{
                //    (otherBankGridView.Rows[3].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBankId4.ToString();
                //    BranchDropDownListItem(3, Convert.ToInt32(loanInformationObj.OtherBankAccount.OtherBankId4));
                //}
                //try
                //{
                //    (otherBankGridView.Rows[3].Cells[1].FindControl("branchDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBranchId4.ToString();
                //    (otherBankGridView.Rows[3].Cells[1].FindControl("branchId") as HiddenField).Value = loanInformationObj.OtherBankAccount.OtherBranchId4.ToString();
                //}
                //catch
                //{
                ////}
                //if (loanInformationObj.OtherBankAccount.OtherAccNo4 == "0")
                //{
                //    (otherBankGridView.Rows[3].Cells[2].FindControl("acNoTextBox") as TextBox).Text = "";
                //}
                //else
                //{
                //    (otherBankGridView.Rows[3].Cells[2].FindControl("acNoTextBox") as TextBox).Text = loanInformationObj.OtherBankAccount.OtherAccNo4;
                //}
                //bool accountTypeFlag4 = false;
                //for (int i = 0; i < accountTypeListObj.Count; i++)
                //{
                //    if (loanInformationObj.OtherBankAccount.OtherAccType4.Trim() == accountTypeListObj[i].Name)
                //    {
                //        accountTypeFlag4 = true;
                //        break;
                //    }
                ////}
                //if (accountTypeFlag4)
                //{
                //    (otherBankGridView.Rows[3].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = loanInformationObj.OtherBankAccount.OtherAccType4;
                //}
                //else
                //{
                //    (otherBankGridView.Rows[3].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = "";
                //}

                //(otherBankGridView.Rows[4].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBankId5.ToString();
                //BranchDropDownListItem(4, Convert.ToInt32(loanInformationObj.OtherBankAccount.OtherBankId5));
                //try
                //{
                //    (otherBankGridView.Rows[4].Cells[1].FindControl("branchDropDownList") as DropDownList).SelectedValue = loanInformationObj.OtherBankAccount.OtherBranchId5.ToString();
                //    (otherBankGridView.Rows[4].Cells[1].FindControl("branchId") as HiddenField).Value = loanInformationObj.OtherBankAccount.OtherBranchId5.ToString();
                //}
                //catch
                //{
                ////}
                //if (loanInformationObj.OtherBankAccount.OtherAccNo5 == "0")
                //{
                //    (otherBankGridView.Rows[4].Cells[2].FindControl("acNoTextBox") as TextBox).Text = "";
                //}
                //else
                //{
                //    (otherBankGridView.Rows[4].Cells[2].FindControl("acNoTextBox") as TextBox).Text = loanInformationObj.OtherBankAccount.OtherAccNo5;
                //}
                //bool accountTypeFlag5 = false;
                //for (int i = 0; i < accountTypeListObj.Count; i++)
                //{
                //    if (loanInformationObj.OtherBankAccount.OtherAccType5.Trim() == accountTypeListObj[i].Name)
                //    {
                //        accountTypeFlag5 = true;
                //        break;
                //    }
                //}
                //if (accountTypeFlag5)
                //{
                //    (otherBankGridView.Rows[4].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = loanInformationObj.OtherBankAccount.OtherAccType5;
                //}
                //else
                //{
                //    (otherBankGridView.Rows[4].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text = "";
                //}

                try
                {
                    bool idTyeFlag1 = false;
                    for (int i = 0; i < idTypeListObj.Count; i++)
                    {
                        if (loanInformationObj.ApplicantId.Type1.Trim() == idTypeListObj[i].Name)
                        {
                            idTyeFlag1 = true;
                            break;
                        }
                    }
                    if (idTyeFlag1)
                    {
                        (idGridView.Rows[0].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type1;
                    }
                    else
                    {
                        (idGridView.Rows[0].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "";
                    }

                    (idGridView.Rows[0].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id1;
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate1))
                    {
                        (idGridView.Rows[0].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate1.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idGridView.Rows[0].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = "";
                    }
                }
                catch
                {
                }
                try
                {
                    bool idTyeFlag2 = false;
                    for (int i = 0; i < idTypeListObj.Count; i++)
                    {
                        if (loanInformationObj.ApplicantId.Type2.Trim() == idTypeListObj[i].Name)
                        {
                            idTyeFlag2 = true;
                            break;
                        }
                    }
                    if (idTyeFlag2)
                    {
                        (idGridView.Rows[1].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type2;
                    }
                    else
                    {
                        (idGridView.Rows[1].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "";
                    }
                    //(idGridView.Rows[1].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type2;
                    (idGridView.Rows[1].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id2;
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate2))
                    {
                        (idGridView.Rows[1].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate2.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idGridView.Rows[1].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = "";
                    }
                }
                catch
                {
                }
                try
                {
                    bool idTyeFlag3 = false;
                    for (int i = 0; i < idTypeListObj.Count; i++)
                    {
                        if (loanInformationObj.ApplicantId.Type3.Trim() == idTypeListObj[i].Name)
                        {
                            idTyeFlag3 = true;
                            break;
                        }
                    }
                    if (idTyeFlag3)
                    {
                        (idGridView.Rows[2].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type3;
                    }
                    else
                    {
                        (idGridView.Rows[2].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "";
                    }
                    //(idGridView.Rows[2].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type3;
                    (idGridView.Rows[2].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id3;
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate3))
                    {
                        (idGridView.Rows[2].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate3.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idGridView.Rows[2].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = "";
                    }
                }
                catch
                {
                }
                try
                {
                    bool idTyeFlag4 = false;
                    for (int i = 0; i < idTypeListObj.Count; i++)
                    {
                        if (loanInformationObj.ApplicantId.Type4.Trim() == idTypeListObj[i].Name)
                        {
                            idTyeFlag4 = true;
                            break;
                        }
                    }
                    if (idTyeFlag4)
                    {
                        (idGridView.Rows[3].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type4;
                    }
                    else
                    {
                        (idGridView.Rows[3].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "";
                    }
                    //(idGridView.Rows[3].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type4;
                    (idGridView.Rows[3].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id4;
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate4))
                    {
                        (idGridView.Rows[3].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate4.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idGridView.Rows[3].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = "";
                    }
                }
                catch
                {
                }
                try
                {
                    bool idTyeFlag5 = false;
                    for (int i = 0; i < idTypeListObj.Count; i++)
                    {
                        if (loanInformationObj.ApplicantId.Type5.Trim() == idTypeListObj[i].Name)
                        {
                            idTyeFlag5 = true;
                            break;
                        }
                    }
                    if (idTyeFlag5)
                    {
                        (idGridView.Rows[4].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type5;
                    }
                    else
                    {
                        (idGridView.Rows[4].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "";
                    }
                    //(idGridView.Rows[4].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type5;
                    (idGridView.Rows[4].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id5;
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate5))
                    {
                        (idGridView.Rows[4].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate5.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idGridView.Rows[4].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text = "";
                    }
                }
                catch
                {
                }
                vechicleDropDownList.SelectedValue = loanInformationObj.VechileId.ToString();
                if (loanInformationObj.ExtraField1 != null)
                {
                    if (loanInformationObj.ExtraField1 == "Male" || loanInformationObj.ExtraField1 == "Female")
                    {
                        genderDropDownList.Text = loanInformationObj.ExtraField1;
                    }
                }
                dependentTextBox.Text = loanInformationObj.ExtraField2;
                documentTypeTextBox.Text = loanInformationObj.ExtraField3;
                if (loanInformationObj.ExtraField4 != null)
                {
                    if (loanInformationObj.ExtraField4 == "Yes" || loanInformationObj.ExtraField4 == "No")
                    {
                        multipleLoanDropDownList.Text = loanInformationObj.ExtraField4;
                    }
                }
                if (loanInformationObj.ExtraField5 != null)
                {
                    if (loanInformationObj.ExtraField5 == "Yes" || loanInformationObj.ExtraField5 == "No")
                    {
                        earlierLoanWScbDropDownList.Text = loanInformationObj.ExtraField5;
                    }
                }
                if (loanInformationObj.ExtraField6 != null)
                {
                    if (loanInformationObj.ExtraField6 == "Yes" || loanInformationObj.ExtraField6 == "No")
                    {
                        availedAutoLoanDropDownList.Text = loanInformationObj.ExtraField6;
                    }
                }
                if (loanInformationObj.ExtraField7 != null)
                {
                    if (loanInformationObj.ExtraField7 == "Yes" || loanInformationObj.ExtraField7 == "No")
                    {
                        mortageLoanDropDownList.Text = loanInformationObj.ExtraField7;
                    }
                }
                extraFieldTextBox8.Text = loanInformationObj.ExtraField8;
                extraFieldTextBox9.Text = loanInformationObj.ExtraField9;
                extraFieldTextBox10.Text = loanInformationObj.ExtraField10;

                if (loanInformationObj.ForwardOrDesferStatus.Equals((int)ForwardOrDeferFlag.DEFERID))
                {
                    remarksLabel.Text = "Already deferred.";
                    LLIDTextBox.Focus();

                    List<Int32> reasonIdListObj = loanApplicationManagerObj.GetAllDeferReasonId(llid);
                    if (reasonIdListObj != null)
                    {
                        List<Remarks> remarksListObj = loanApplicationManagerObj.GetAllRemarks();
                        for (int i = 0; i < reasonIdListObj.Count; i++)
                        {
                            for (int j = 0; j < remarksListObj.Count; j++)
                            {
                                if (remarksListObj[j].Id == reasonIdListObj[i])
                                {
                                    (remarksGridView.Rows[j].FindControl("selectCheckBox") as CheckBox).Checked = true;
                                    break;
                                }
                            }
                        }

                    }
                }
                else
                {
                    foreach (GridViewRow gvr in remarksGridView.Rows)
                    {
                        (gvr.FindControl("selectCheckBox") as CheckBox).Checked = false;
                    }
                }
                if (loanInformationObj.ForwardOrDesferStatus.Equals(3) || loanInformationObj.ForwardOrDesferStatus.Equals(6) || loanInformationObj.ForwardOrDesferStatus.Equals(4) || loanInformationObj.ForwardOrDesferStatus.Equals(8))
                {
                    deferButton.Enabled = true;
                    forwardButton.Enabled = true;
                    saveButton.Enabled = true;
                }
                else
                {
                    deferButton.Enabled = true;
                    forwardButton.Enabled = true;
                    saveButton.Enabled = true;
                }

                // PopulateJointApplcants(llid);
            }
            else
            {
                remarksLabel.Text = "No Record Found!";
                LLIDTextBox.Focus();
                saveButton.Text = "Save";
                ClearLoanApplicationData();
                //ClearJointApplicantInfo();
            }
        }
        #endregion

      
        #region Clear Loan Locator Data
        /// <summary>
        /// This method provide clear all loan locator data.
        /// </summary>
        private void ClearLoanLocatorData()
        {
            productDropDownList.SelectedValue = "0";
            sourceDropDownList.SelectedValue = "0";
            dateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            customerNameTextBox.Text = "";
            appliedAmountTextBox.Text = "";

            foreach (GridViewRow gvr in scbAccGridView.Rows)
            {
                (gvr.FindControl("scbPreTextBox") as TextBox).Text = "";
                (gvr.FindControl("scbMasterTextBox") as TextBox).Text = "";
                (gvr.FindControl("scbPostTextBox") as TextBox).Text = "";
            }
            professionDropdownList.Text = "";
            companyNameTextBox.Text = "";
        }
        #endregion

        #region Clear Loan Application Data
        /// <summary>
        /// This method provide clear all loan information.
        /// </summary>
        private void ClearLoanApplicationData()
        {
            sourcePersonTextBox.Text = "";
            txtARMCode.Text = "";
            declaredIncomeTextBox.Text = "";
            tinNoPreTextBox.Text = "";
            tinNoMasterTextBox.Text = "";
            tinNoPostTextBox.Text = "";

            foreach (GridViewRow gvr in creditCardNoGridView.Rows)
            {
                (gvr.FindControl("crdtCrdNoTextBox") as TextBox).Text = "";
            }
            fathersNameTextBox.Text = "";
            mothersNameTextBox.Text = "";
            dobTextBox.Text = "";
            educationalDropDownList.Text = "";
            maritalStatusDropDownList.SelectedIndex = 1;
            residentialStatusDropDownList.SelectedIndex = 2;
            residentialAddressTextBox.Text = "";
            foreach (GridViewRow gvr in contactGridView.Rows)
            {
                (gvr.FindControl("contactNoTextBox") as TextBox).Text = "";
            }
            officeAddressTextBox.Text = "";
            workExperienceMonthTextBox.Text = "";
            spousesOccupationDropdownList.Text = "";
            foreach (GridViewRow gvr in otherBankGridView.Rows)
            {
                (gvr.FindControl("bankDropDownList") as DropDownList).SelectedValue = "0";
                (gvr.FindControl("branchDropDownList") as DropDownList).SelectedValue = "0";
                (gvr.FindControl("acTypeDropDownList") as DropDownList).SelectedValue = "";
                (gvr.FindControl("acNoTextBox") as TextBox).Text = "";
            }
            foreach (GridViewRow gvr in idGridView.Rows)
            {
                (gvr.FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "";
                (gvr.FindControl("idNoTextBox") as TextBox).Text = "";
                (gvr.FindControl("expiryDateTextBox") as TextBox).Text = "";
            }
            vechicleDropDownList.SelectedValue = "1";
            dependentTextBox.Text = "";
            multipleLoanDropDownList.SelectedIndex = 1;
            documentTypeTextBox.Text = "";
            saveButton.Enabled = true;
            deferButton.Enabled = true;
            forwardButton.Enabled = true;
        }
        #endregion

        #region saveButton_Click
        /// <summary>
        /// Save button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void saveButton_Click(object sender, EventArgs e)
        {
            int insertOrUpdateRow = -1;
            LoanInformation loanInfo = new LoanInformation();

            remarksLabel.Text = "";
            loanInfo.LLId = Convert.ToInt32(LLIDTextBox.Text.Trim());
            loanInfo.ProductId = Convert.ToInt32("0" + productDropDownList.SelectedValue);
            loanInfo.SourceId = Convert.ToInt32("0" + sourceDropDownList.SelectedValue);
            loanInfo.SourcePersonName = sourcePersonTextBox.Text.Trim();
            loanInfo.ARMCode = txtARMCode.Text.Trim();

            if (!String.IsNullOrEmpty(dateTextBox.Text.Trim()))
            {
                loanInfo.RecieveDate = Convert.ToDateTime(dateTextBox.Text.Trim(), new System.Globalization.CultureInfo("ru-RU"));
            }
            loanInfo.ApplicantName = customerNameTextBox.Text.Trim();
            loanInfo.AppliedAmount = Convert.ToDecimal("0" + appliedAmountTextBox.Text.Trim());
            string accNo1 = (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).Text.Trim();
            accNo1 += (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text.Trim();
            accNo1 += (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).Text.Trim();
            loanInfo.AccountNo.MasterAccNo1 = accNo1;
            string accNo2 = (scbAccGridView.Rows[1].Cells[0].FindControl("scbPreTextBox") as TextBox).Text.Trim();
            accNo2 += (scbAccGridView.Rows[1].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text.Trim();
            accNo2 += (scbAccGridView.Rows[1].Cells[2].FindControl("scbPostTextBox") as TextBox).Text.Trim();
            loanInfo.AccountNo.MasterAccNo2 = accNo2;
            string accNo3 = (scbAccGridView.Rows[2].Cells[0].FindControl("scbPreTextBox") as TextBox).Text.Trim();
            accNo3 += (scbAccGridView.Rows[2].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text.Trim();
            accNo3 += (scbAccGridView.Rows[2].Cells[2].FindControl("scbPostTextBox") as TextBox).Text.Trim();
            loanInfo.AccountNo.MasterAccNo3 = accNo3;
            string accNo4 = (scbAccGridView.Rows[3].Cells[0].FindControl("scbPreTextBox") as TextBox).Text.Trim();
            accNo4 += (scbAccGridView.Rows[3].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text.Trim();
            accNo4 += (scbAccGridView.Rows[3].Cells[2].FindControl("scbPostTextBox") as TextBox).Text.Trim();
            loanInfo.AccountNo.MasterAccNo4 = accNo4;
            string accNo5 = (scbAccGridView.Rows[4].Cells[0].FindControl("scbPreTextBox") as TextBox).Text.Trim();
            accNo5 += (scbAccGridView.Rows[4].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text.Trim();
            accNo5 += (scbAccGridView.Rows[4].Cells[2].FindControl("scbPostTextBox") as TextBox).Text.Trim();
            loanInfo.AccountNo.MasterAccNo5 = accNo5;

            loanInfo.DeclaredAmount = Convert.ToDecimal("0" + declaredIncomeTextBox.Text.Trim());
            string tinNo = tinNoPreTextBox.Text.Trim();
            //tinNo += tinNoMasterTextBox.Text.Trim();
            //tinNo += tinNoPostTextBox.Text.Trim();
            loanInfo.TINNo = tinNo;

            loanInfo.CreditCardNo1 = (creditCardNoGridView.Rows[0].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text.Trim();
            loanInfo.CreditCardNo2 = (creditCardNoGridView.Rows[1].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text.Trim();
            loanInfo.CreditCardNo3 = (creditCardNoGridView.Rows[2].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text.Trim();
            loanInfo.CreditCardNo4 = (creditCardNoGridView.Rows[3].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text.Trim();
            loanInfo.CreditCardNo5 = (creditCardNoGridView.Rows[4].Cells[0].FindControl("crdtCrdNoTextBox") as TextBox).Text.Trim();
            loanInfo.FatherName = fathersNameTextBox.Text.Trim();
            loanInfo.MotherName = mothersNameTextBox.Text.Trim();
            if (!String.IsNullOrEmpty(dobTextBox.Text.Trim()))
            {
                loanInfo.DOB = Convert.ToDateTime(dobTextBox.Text.Trim(), new System.Globalization.CultureInfo("ru-RU"));
            }
            loanInfo.MaritalStatus = maritalStatusDropDownList.Text.Trim();
            loanInfo.EducationalLevel = educationalDropDownList.Text.Trim();
            loanInfo.ResidentialStatus = residentialStatusDropDownList.Text.Trim();
            loanInfo.ResidentAddress = residentialAddressTextBox.Text.Trim();
            loanInfo.ContactNumber.ContactNo1 = (contactGridView.Rows[0].Cells[0].FindControl("contactNoTextBox") as TextBox).Text.Trim();
            loanInfo.ContactNumber.ContactNo2 = (contactGridView.Rows[1].Cells[0].FindControl("contactNoTextBox") as TextBox).Text.Trim();
            loanInfo.ContactNumber.ContactNo3 = (contactGridView.Rows[2].Cells[0].FindControl("contactNoTextBox") as TextBox).Text.Trim();
            loanInfo.ContactNumber.ContactNo4 = (contactGridView.Rows[3].Cells[0].FindControl("contactNoTextBox") as TextBox).Text.Trim();
            loanInfo.ContactNumber.ContactNo5 = (contactGridView.Rows[4].Cells[0].FindControl("contactNoTextBox") as TextBox).Text.Trim();
            loanInfo.Profession = professionDropdownList.Text.Trim();
            loanInfo.Business = companyNameTextBox.Text.Trim();
            loanInfo.OfficeAddress = officeAddressTextBox.Text.Trim();
            loanInfo.JobDuration = Convert.ToInt32("0" + workExperienceMonthTextBox.Text.Trim());
            loanInfo.SpousOcupation = spousesOccupationDropdownList.Text.Trim();

            loanInfo.OtherBankAccount.OtherBankId1 = 0;  // Convert.ToInt32("0" + (otherBankGridView.Rows[0].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue.ToString());
            //if ((otherBankGridView.Rows[0].Cells[1].FindControl("branchId") as HiddenField).Value != "")
            //{
             //   loanInformationObj.OtherBankAccount.OtherBranchId1 = Convert.ToInt32((otherBankGridView.Rows[0].Cells[1].FindControl("branchId") as HiddenField).Value);
            //}
           // else
          //  {
                loanInfo.OtherBankAccount.OtherBranchId1 = 0;
           // }

           // if (String.IsNullOrEmpty((otherBankGridView.Rows[0].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim()))
           // {
                loanInfo.OtherBankAccount.OtherAccNo1 = "0";
            // }
            // else
            //  {
            //     loanInformationObj.OtherBankAccount.OtherAccNo1 = (otherBankGridView.Rows[0].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim();
            //  }
            loanInfo.OtherBankAccount.OtherAccType1 = "0";  //(otherBankGridView.Rows[0].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text.Trim();
            loanInfo.OtherBankAccount.OtherBankId2 = 0; //Convert.ToInt32("0" + (otherBankGridView.Rows[1].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue);
           // if ((otherBankGridView.Rows[1].Cells[1].FindControl("branchId") as HiddenField).Value != "")
           // {
          //      loanInformationObj.OtherBankAccount.OtherBranchId2 = Convert.ToInt32((otherBankGridView.Rows[1].Cells[1].FindControl("branchId") as HiddenField).Value);
           // }
            //else
           // {
                loanInfo.OtherBankAccount.OtherBranchId2 = 0;
           // }
           // if (String.IsNullOrEmpty((otherBankGridView.Rows[1].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim()))
           // {
                loanInfo.OtherBankAccount.OtherAccNo2 = "0";
            // }
            //  else
            //  {
            //      loanInformationObj.OtherBankAccount.OtherAccNo2 = (otherBankGridView.Rows[1].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim();
            //  }
            loanInfo.OtherBankAccount.OtherAccType2 = "0"; //(otherBankGridView.Rows[1].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text.Trim();
            loanInfo.OtherBankAccount.OtherBankId3 = 0; // Convert.ToInt32("0" + (otherBankGridView.Rows[2].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue);
            //if ((otherBankGridView.Rows[2].Cells[1].FindControl("branchId") as HiddenField).Value != "")
            //{
              //  loanInformationObj.OtherBankAccount.OtherBranchId3 = Convert.ToInt32((otherBankGridView.Rows[2].Cells[1].FindControl("branchId") as HiddenField).Value);
            //}
            //else
            //{
                loanInfo.OtherBankAccount.OtherBranchId3 = 0;
            //}

            //if (String.IsNullOrEmpty((otherBankGridView.Rows[2].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim()))
            //{
                loanInfo.OtherBankAccount.OtherAccNo3 = "0";
            //}
            //else
            //{
            //  loanInformationObj.OtherBankAccount.OtherAccNo3 = (otherBankGridView.Rows[2].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim();
            //}
            loanInfo.OtherBankAccount.OtherAccType3 = "0"; //(otherBankGridView.Rows[2].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text.Trim();
            loanInfo.OtherBankAccount.OtherBankId4 = 0; // Convert.ToInt32("0" + (otherBankGridView.Rows[3].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue);
            //if ((otherBankGridView.Rows[3].Cells[1].FindControl("branchId") as HiddenField).Value != "")
            //{
               // loanInformationObj.OtherBankAccount.OtherBranchId4 = Convert.ToInt32((otherBankGridView.Rows[3].Cells[1].FindControl("branchId") as HiddenField).Value);
            //}
            //else
            //{
                loanInfo.OtherBankAccount.OtherBranchId4 = 0;
            //}

            //if (String.IsNullOrEmpty((otherBankGridView.Rows[3].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim()))
            //{
                loanInfo.OtherBankAccount.OtherAccNo4 = "0";
            //}
            //else
            //{
            //  loanInformationObj.OtherBankAccount.OtherAccNo4 = (otherBankGridView.Rows[3].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim();
            //}
            loanInfo.OtherBankAccount.OtherAccType4 = "0";// (otherBankGridView.Rows[3].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text.Trim();
            loanInfo.OtherBankAccount.OtherBankId5 = 0; // Convert.ToInt32("0" + (otherBankGridView.Rows[4].Cells[0].FindControl("bankDropDownList") as DropDownList).SelectedValue);
            //if ((otherBankGridView.Rows[4].Cells[1].FindControl("branchId") as HiddenField).Value != "")
            //{
              //  loanInformationObj.OtherBankAccount.OtherBranchId5 = Convert.ToInt32((otherBankGridView.Rows[4].Cells[1].FindControl("branchId") as HiddenField).Value);
            //}
            //else
            //{
                loanInfo.OtherBankAccount.OtherBranchId5 = 0;
            //}
            //if (String.IsNullOrEmpty((otherBankGridView.Rows[4].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim()))
            //{
                loanInfo.OtherBankAccount.OtherAccNo5 = "0";
            //}
            //else
            //{
            //  loanInformationObj.OtherBankAccount.OtherAccNo5 = (otherBankGridView.Rows[4].Cells[2].FindControl("acNoTextBox") as TextBox).Text.Trim();
            //}

            loanInfo.OtherBankAccount.OtherAccType5 = "0"; //(otherBankGridView.Rows[4].Cells[3].FindControl("acTypeDropDownList") as DropDownList).Text.Trim();

            loanInfo.ApplicantId.Type1 = (idGridView.Rows[0].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
            loanInfo.ApplicantId.Id1 = (idGridView.Rows[0].Cells[1].FindControl("idNoTextBox") as TextBox).Text.Trim();
            if (!String.IsNullOrEmpty((idGridView.Rows[0].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim()))
            {
                loanInfo.ApplicantId.ExpiryDate1 = Convert.ToDateTime((idGridView.Rows[0].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim(), new System.Globalization.CultureInfo("ru-RU"));
            }
            loanInfo.ApplicantId.Type2 = (idGridView.Rows[1].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
            loanInfo.ApplicantId.Id2 = (idGridView.Rows[1].Cells[1].FindControl("idNoTextBox") as TextBox).Text.Trim();
            if (!String.IsNullOrEmpty((idGridView.Rows[1].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim()))
            {
                loanInfo.ApplicantId.ExpiryDate2 = Convert.ToDateTime((idGridView.Rows[1].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim(), new System.Globalization.CultureInfo("ru-RU"));
            }
            loanInfo.ApplicantId.Type3 = (idGridView.Rows[2].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
            loanInfo.ApplicantId.Id3 = (idGridView.Rows[2].Cells[1].FindControl("idNoTextBox") as TextBox).Text.Trim();
            if (!String.IsNullOrEmpty((idGridView.Rows[2].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim()))
            {
                loanInfo.ApplicantId.ExpiryDate3 = Convert.ToDateTime((idGridView.Rows[2].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim(), new System.Globalization.CultureInfo("ru-RU"));
            }
            loanInfo.ApplicantId.Type4 = (idGridView.Rows[3].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
            loanInfo.ApplicantId.Id4 = (idGridView.Rows[3].Cells[1].FindControl("idNoTextBox") as TextBox).Text.Trim();
            if (!String.IsNullOrEmpty((idGridView.Rows[3].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim()))
            {
                loanInfo.ApplicantId.ExpiryDate4 = Convert.ToDateTime((idGridView.Rows[3].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim(), new System.Globalization.CultureInfo("ru-RU"));
            }
            loanInfo.ApplicantId.Type5 = (idGridView.Rows[4].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
            loanInfo.ApplicantId.Id5 = (idGridView.Rows[4].Cells[1].FindControl("idNoTextBox") as TextBox).Text.Trim();
            if (!String.IsNullOrEmpty((idGridView.Rows[4].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim()))
            {
                loanInfo.ApplicantId.ExpiryDate5 = Convert.ToDateTime((idGridView.Rows[4].Cells[2].FindControl("expiryDateTextBox") as TextBox).Text.Trim(), new System.Globalization.CultureInfo("ru-RU"));
            }
            loanInfo.VechileId = Convert.ToInt32("0" + vechicleDropDownList.SelectedIndex);


            loanInfo.ExtraField1 = genderDropDownList.SelectedItem.Text.Trim();
            loanInfo.ExtraField2 = dependentTextBox.Text.Trim();
            loanInfo.ExtraField3 = documentTypeTextBox.Text.Trim();
            loanInfo.ExtraField4 = multipleLoanDropDownList.SelectedItem.Text.Trim();
            loanInfo.ExtraField5 = earlierLoanWScbDropDownList.SelectedItem.Text.Trim();
            loanInfo.ExtraField6 = availedAutoLoanDropDownList.SelectedItem.Text.Trim();
            loanInfo.ExtraField7 = mortageLoanDropDownList.SelectedItem.Text.Trim();
            loanInfo.ExtraField8 = extraFieldTextBox8.Text.Trim();
            loanInfo.ExtraField9 = extraFieldTextBox9.Text.Trim();
            loanInfo.ExtraField10 = extraFieldTextBox10.Text.Trim();

            loanInfo.EntryUserId = Convert.ToInt32(Session["Id"].ToString());
            var user = (User)Session["UserDetails"];

            DateTime loanStartDateTime = new LocatorApplicationService().GetStartTime(loanInfo.LLId.Value);
            loanInfo.LoanStatusId = 4;
            insertOrUpdateRow = loanApplicationManagerObj.InsertORUpdateLoanInfo(loanInfo, user, loanStartDateTime);
            int companyId = 0;
            if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT))
            {
                companyId = payRollManagerObj.SelectCompanyId(AddSlash(companyNameTextBox.Text.Trim()));
                if (companyId == 0)
                {
                    if (!productDropDownList.SelectedItem.Text.Contains("SME"))
                    {
                        if (professionDropdownList.Text.Trim().ToUpper().Contains("SALARIED"))
                        {
                            payRollManagerObj.InsertCompanyName(AddSlash(companyNameTextBox.Text.Trim()));
                        }
                    }
                }
                if (productDropDownList.SelectedValue == "1" || productDropDownList.SelectedValue == "4" || productDropDownList.SelectedValue == "12")
                {
                    SendRCValues();
                }
                remarksLabel.Text = "Data Saved Sucessfully.";
                LLIDTextBox.Focus();
                ClearLoanLocatorData();
                ClearLoanApplicationData();
                new AT(this).AuditAndTraial("Personal Loan Application", remarksLabel.Text);
            }
            else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERTERROR))
            {
                remarksLabel.Text = "Data Save Failed!";
                new AT(this).AuditAndTraial("Personal Loan Application", remarksLabel.Text);
                LLIDTextBox.Focus();
            }
            else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
            {
                companyId = payRollManagerObj.SelectCompanyId(AddSlash(companyNameTextBox.Text.Trim()));
                if (companyId == 0)
                {
                    if (!productDropDownList.SelectedItem.Text.Contains("SME"))
                    {
                        if (professionDropdownList.Text.Trim().ToUpper().Contains("SALARIED"))
                        {
                            payRollManagerObj.InsertCompanyName(AddSlash(companyNameTextBox.Text.Trim()));
                        }
                    }
                }
                remarksLabel.Text = "Data Updated Sucessfully.";
                new AT(this).AuditAndTraial("Personal Loan Application", remarksLabel.Text);
                LLIDTextBox.Focus();
                ClearLoanLocatorData();
                ClearLoanApplicationData();
            }
            else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
            {
                remarksLabel.Text = "Data Update Failed!";
                new AT(this).AuditAndTraial("Personal Loan Application", remarksLabel.Text);
                LLIDTextBox.Focus();
            }
            // }

            if (!insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
            {
                
            }
        }
        #endregion

        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("'", "''");
                sMessage = sMessage.Replace('"', '\"');
                sMessage = sMessage.Replace(@"\", @"\\");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private void UpdateLoanStatusAndDetails(int loanStatusId)
        {
            int llid = Convert.ToInt32(LLIDTextBox.Text);
            LoanApplicationDetails loanApplicationDetailsObj = new LoanApplicationDetails();
            List<Remarks> remarksListObj = new List<Remarks>();
            List<Int32> remarksIdListObj = new List<Int32>();
            foreach (GridViewRow gvr in remarksGridView.Rows)
            {
                if ((gvr.FindControl("selectCheckBox") as CheckBox).Checked)
                {
                    Remarks remarksObj = new Remarks();
                    Int32 remarksIdObj;
                    remarksIdObj = Convert.ToInt32((gvr.FindControl("remarksIdLabel") as Label).Text);
                    remarksIdListObj.Add(remarksIdObj);
                    remarksObj.Id = remarksIdObj;
                    remarksObj.Name = (gvr.FindControl("remarksTextBox") as TextBox).Text;
                    remarksListObj.Add(remarksObj);
                }
            }
            loanApplicationDetailsObj.LAId = llid;
            loanApplicationDetailsObj.PersonId = (string)Session["UserId"];

            var user = (User)Session["UserDetails"];

            var deptId = 0;

            if (user.Type == 1) deptId = 4;
            else deptId = 5;

            loanApplicationDetailsObj.DeptId = 5;
            loanApplicationDetailsObj.DeptId = deptId;
            loanApplicationDetailsObj.ProcessStatusId = loanStatusId;
            int insertLoanAppDetailsFlag = loanApplicationManagerObj.InsertDeferReasonDetails(loanApplicationDetailsObj, remarksListObj);

        }
        #region deferButton_Click
        /// <summary>
        /// Defer button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void deferButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(LLIDTextBox.Text.Trim()))
                {
                    remarksLabel.Text = "Please Enter LLID";
                    LLIDTextBox.Focus();
                }
                else
                {
                    int countCheckedCheckBox = 0;
                    remarksLabel.Text = "";
                    foreach (GridViewRow gvr in remarksGridView.Rows)
                    {
                        if ((gvr.FindControl("selectCheckBox") as CheckBox).Checked)
                        {
                            countCheckedCheckBox++;
                            break;
                        }
                    }
                    if (countCheckedCheckBox > 0)
                    {
                        remarksLabel.Text = "";
                        saveButton_Click(sender, e);
                        int llid = Convert.ToInt32(LLIDTextBox.Text);
                        int deferFlag = (int)ForwardOrDeferFlag.DEFERID;
                        int updateFlag = loanApplicationManagerObj.SetForwardStatus(llid, deferFlag, (string)Session["UserId"]);
                        if (updateFlag > -3)
                        {
                            int deleteFlag = loanApplicationManagerObj.DeleteDeferReason(llid);
                            if (!String.IsNullOrEmpty(deleteFlag.ToString()))
                            {
                                List<Remarks> remarksListObj = new List<Remarks>();
                                List<Int32> remarksIdListObj = new List<Int32>();
                                foreach (GridViewRow gvr in remarksGridView.Rows)
                                {
                                    if ((gvr.FindControl("selectCheckBox") as CheckBox).Checked)
                                    {
                                        Remarks remarksObj = new Remarks();
                                        Int32 remarksIdObj;
                                        remarksIdObj = Convert.ToInt32((gvr.FindControl("remarksIdLabel") as Label).Text);
                                        remarksIdListObj.Add(remarksIdObj);
                                        remarksObj.Id = remarksIdObj;
                                        remarksObj.Name = (gvr.FindControl("remarksTextBox") as TextBox).Text;
                                        remarksListObj.Add(remarksObj);
                                    }
                                }
                                int insertFlag = loanApplicationManagerObj.InsertDeferReason(llid, remarksIdListObj);
                                if (insertFlag > -1)
                                {
                                    LoanApplicationDetails loanApplicationDetailsObj = new LoanApplicationDetails();
                                    loanApplicationDetailsObj.LAId = llid;

                                    loanApplicationDetailsObj.PersonId = (string)Session["UserId"];
                                    loanApplicationDetailsObj.DeptId = 5;
                                    loanApplicationDetailsObj.ProcessStatusId = 6;

                                    int insertLoanAppDetailsFlag = loanApplicationManagerObj.InsertDeferReasonDetails(loanApplicationDetailsObj, remarksListObj);
                                    if (insertLoanAppDetailsFlag.Equals((int)InsertUpdateFlag.INSERT))
                                    {
                                        remarksLabel.Text = "Deferred Sucessfully.";
                                        LLIDTextBox.Focus();
                                        ClearLoanLocatorData();
                                        foreach (GridViewRow gvr in remarksGridView.Rows)
                                        {
                                            (gvr.FindControl("selectCheckBox") as CheckBox).Checked = false;
                                        }
                                        ClearLoanApplicationData();
                                    }
                                    else if (insertLoanAppDetailsFlag.Equals((int)InsertUpdateFlag.INSERTERROR))
                                    {
                                        remarksLabel.Text = "Deferred Failed!";
                                        LLIDTextBox.Focus();
                                    }
                                    new AT(this).AuditAndTraial("Perdonal Loan", remarksLabel.Text);
                                }
                            }
                        }
                        else
                        {
                            remarksLabel.Text = "Deferred Failed!";
                            LLIDTextBox.Focus();
                        }
                    }
                    else
                    {
                        //errorMsgLabel.Text = "Please select remarks.";
                        Alert.Show("Please select remarks.");
                        remarksGridView.Focus();
                        //remarksGridView.Visible = true;
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Defer Personal Loan Application");
            }
        }
        #endregion

        #region forwardButton_Click
        /// <summary>
        /// Forward button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void forwardButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(LLIDTextBox.Text))
            {
                remarksLabel.Text = "Please Enter LLID";
                LLIDTextBox.Focus();
            }
            else
            {
                int updateFlag = -3;
                remarksLabel.Text = "";
                saveButton_Click(sender, e);
                int llid = Convert.ToInt32(LLIDTextBox.Text);
                remarksLabel.Text = "";
                int statusId = (int)ForwardOrDeferFlag.FORWARDID;
                var user = (User)Session["UserDetails"];

                if(user.Type == 1)
                {
                    statusId = 45;
                }

                updateFlag = loanApplicationManagerObj.SetForwardStatus(llid, statusId, user.UserId);
                if (updateFlag.Equals((int)InsertUpdateFlag.STATUSUPDATE))
                {
                    UpdateLoanStatusAndDetails(statusId);
                    remarksLabel.Text = "Forwarded Sucessfully";
                    LLIDTextBox.Focus();
                    ClearLoanLocatorData();
                    ClearLoanApplicationData();
                    //ClearJointApplicantInfo();
                }
                else if (updateFlag.Equals((int)InsertUpdateFlag.STATUSUPDATEERROR))
                {
                    remarksLabel.Text = "Forward Failed! Please Check Status in the Loan Locator System";
                    LLIDTextBox.Focus();
                }
                new AT(this).AuditAndTraial("Personal Loan", remarksLabel.Text);
            }
        }
        #endregion
        protected void refreshButton_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            remarksLabel.Text = "";
            LLIDTextBox.Focus();
            ClearLoanApplicationData();
            ClearLoanLocatorData();
            foreach (GridViewRow gvr in remarksGridView.Rows)
            {
                (gvr.FindControl("selectCheckBox") as CheckBox).Checked = false;
            }
        }

        public void SendRCValues()
        {
            BusinessEntities.Report.ActivityReportObject activityReportObject = new ActivityReportObject();
            BLL.Report.ActivityReportManager activityReportManagerObj = new ActivityReportManager();
            if (productDropDownList.SelectedValue == "1" && professionDropdownList.SelectedValue == "Salaried")
            {
                activityReportObject.RcPLSAL = 1;
            }
            else if (productDropDownList.SelectedValue == "1" && professionDropdownList.SelectedValue != "Salaried")
            {
                activityReportObject.RcPLBIZ = 1;
            }
            else if (productDropDownList.SelectedValue == "4")
            {
                activityReportObject.RcFL = 1;
            }
            else if (productDropDownList.SelectedValue == "12")
            {
                activityReportObject.RcIPF = 1;
            }
            activityReportObject.Date = DateTime.Now;
            activityReportManagerObj.SendReceiveStatus(activityReportObject);
        }
    }
}
