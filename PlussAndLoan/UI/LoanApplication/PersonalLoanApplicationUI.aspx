﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PlussAndLoan.UI.LoanApplication.UI_PersonalLoanApplicationUI" EnableEventValidation="false"
    MasterPageFile="~/UI/ITAdminMasterPage.master" CodeBehind="PersonalLoanApplicationUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/js/TextBoxDatePicker.ascx" TagPrefix="uc1" TagName="TextBoxDatePicker" %>
<%@ Register Src="~/UI/UserControls/js/Validation.ascx" TagPrefix="uc1" TagName="Validation" %>
<%@ Register Src="~/UI/UserControls/js/BranchListPopulatejs.ascx" TagPrefix="uc1" TagName="BranchListPopulatejs" %>
<%@ Register Src="~/UI/UserControls/css/LoanApplicationTabContent.ascx" TagPrefix="uc1" TagName="LoanApplicationTabContent" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <modal:modalPopup runat="server" ID="modalPopup" />
    <style type="text/css">
        body {
        }

        .selectBoxArrow {
            margin-top: 1px;
            float: left;
            position: absolute;
            right: 1px;
        }

        .selectBoxInput {
            border: 0px;
            padding-left: 1px;
            height: 16px;
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .selectBox {
            border: 1px solid #7f9db9;
            height: 20px;
        }

        #ajax_listOfOptions {
            position: absolute; /* Never change this one */
            width: 254px; /* Width of box */
            height: 150px; /* Height of box */
            overflow: auto; /* Scrolling features */
            border: 1px solid #317082; /* Dark green border #317082*/
            background-color: #FFF; /* White background color */
            text-align: left;
            font-size: 10px;
            z-index: 100;
        }

            #ajax_listOfOptions div {
                /* General rule for both .optionDiv and .optionDivSelected */
                margin: 1px;
                padding: 1px;
                cursor: pointer;
                font-size: 10px;
            }

            #ajax_listOfOptions .optionDiv {
                /* Div for each item in list */
            }

            #ajax_listOfOptions .optionDivSelected {
                /* Selected item in the list */
                background-color: #317082;
                color: #FFF;
            }

        #ajax_listOfOptions_iframe {
            background-color: #F00;
            position: absolute;
            z-index: 5;
        }

        form {
            display: inline;
        }

        .auto-style1 {
            width: 330px;
        }

        .auto-style2 {
            width: 335px;
        }
    </style>

    <script type="text/javascript">	
        function LLIdIsEmpty() {
            var value1 = document.getElementById('ctl00_ContentPlaceHolder_LLIDTextBox').value;
            if (value1.length == 0 || value1 == '') {
                document.getElementById('ctl00_ContentPlaceHolder_errorMsgLabel').innerHTML = "Please enter LLID !";
                document.getElementById('ctl00_ContentPlaceHolder_LLIDTextBox').focus();
                return false;
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder_errorMsgLabel').innerHTML = '';
                return true;
            }
        }

        function showHideExtraField() {
            var hiddenFielsValue = parseInt(document.getElementById("ctl00_ContentPlaceHolder_extraFieldHiddenField").value);
            if (hiddenFielsValue == 0) {
                document.getElementById("ctl00_ContentPlaceHolder_extraFieldImg").src = "../images/minus.gif";
                document.getElementById("ctl00_ContentPlaceHolder_extraFieldHiddenField").value = 1;
                document.getElementById("extraFieldTR").style.display = '';

            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder_extraFieldImg").src = "../images/plus.gif";
                document.getElementById("ctl00_ContentPlaceHolder_extraFieldHiddenField").value = 0;
                document.getElementById("extraFieldTR").style.display = 'none';

            }
        }
        function MaritalStatusChange() {
            if (document.getElementById('ctl00_ContentPlaceHolder_maritalStatusDropDownList').value == "Married") {
                document.getElementById('ctl00_ContentPlaceHolder_documentTypeTextBox').disabled = false;
                document.getElementById('ctl00_ContentPlaceHolder_spousesOccupationDropdownList').disabled = false;
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder_documentTypeTextBox').disabled = true;
                document.getElementById('ctl00_ContentPlaceHolder_spousesOccupationDropdownList').disabled = true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="ContentPlaceHolder" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <uc1:TextBoxDatePicker runat="server" ID="TextBoxDatePicker" />
    <uc1:Validation runat="server" ID="Validation" />
    <uc1:BranchListPopulatejs runat="server" ID="BranchListPopulatejs" />
    <uc1:LoanApplicationTabContent runat="server" ID="LoanApplicationTabContent" />




    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Application Form</div>


        <table width="90%" border="0" cellspacing="6" cellpadding="2">
            <tr>
                <td style="font-weight: bold; width: 200px">
                    <asp:Label ID="LLIDLabel" Text="LLID" runat="server"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox Width="260px" CssClass="input-field" ID="LLIDTextBox" runat="server" MaxLength="9"></asp:TextBox>
                    &nbsp;
                    <asp:Button ID="findButtonTextBox" CssClass="btn primarybtn" Text=" Search " runat="server" OnClick="findButtonTextBox_Click" OnClientClick="return LLIdIsEmpty();" />
                    <asp:Label ID="remarksLabel" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="10.5pt"></asp:Label>
                    <asp:Label ID="errorMsgLabel" runat="server" ForeColor="#FF3300" Font-Bold="true"></asp:Label>
                </td>
            </tr>


            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="productNameLabel" Text="Product Name" runat="server"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:DropDownList CssClass="select-field" ID="productDropDownList" runat="server" Width="260px">
                    </asp:DropDownList>
                    <asp:Label ID="productNameMandatoryLabel" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
                <td style="font-weight: bold; text-align:right">
                    <asp:Label ID="dateLabel" Text="Receive Date" runat="server"></asp:Label>
                </td>
                <td >
                    <asp:TextBox CssClass="input-field" ID="dateTextBox" runat="server" Width="260px" ReadOnly="True"></asp:TextBox>
                    &nbsp
                    <asp:Label ID="dateMandatoryLabel" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="sourceLabel" Text="Entry from  Source" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList CssClass="select-field" ID="sourceDropDownList" runat="server" Width="260px">
                    </asp:DropDownList>
                    &nbsp
                    <asp:Label ID="mandatory1" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td style="font-weight: bold ; text-align:right">
                    <asp:Label ID="sourcePersonNameLabel" Text="Source Person Name" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" ID="sourcePersonTextBox" runat="server" Width="260px"></asp:TextBox>
                </td>
            </tr>
            <tr>  
                <td style="font-weight: bold; text-align:left" valign="middle">Loan Purpose
                </td>
                <td style="font-weight: bold" valign="top">
                    <asp:DropDownList CssClass="select-field" ID="extraFieldTextBox8" runat="server" Width="260px">
                        <asp:ListItem Value="" Text=""></asp:ListItem>
                        <asp:ListItem Value="House Renovation" Text="House Renovation">House Renovation</asp:ListItem>
                        <asp:ListItem Value="Medical" Text="Medical">Medical</asp:ListItem>
                        <asp:ListItem Value="Education" Text="Education">Education</asp:ListItem>
                        <asp:ListItem Value="Marriage" Text="Marriage">Marriage</asp:ListItem>
                        <asp:ListItem Value="Travel" Text="Travel">Travel</asp:ListItem>
                        <asp:ListItem Value="Office Renovation" Text="Office Renovation">Office Renovation</asp:ListItem>
                        <asp:ListItem Value="Home Appliances" Text="Home Appliances">Home Appliances</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="font-weight: bold; width: 0;" valign="middle" align="right">
                    <asp:Label ID="Label41" runat="server" Text="ARM Code"  Width="180px"></asp:Label>
                </td>
                <td style="font-weight: bold;" valign="top" class="auto-style2">
                    <asp:TextBox ID="txtARMCode" runat="server" Width="260px" MaxLength="3" CssClass="input-field"></asp:TextBox>
                    <asp:DropDownList CssClass="select-field" ID="mortageLoanDropDownList" runat="server" Width="260px" Visible="false">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">
                    <asp:Label ID="customerNameLabel" Text="Customer Name" runat="server"></asp:Label>
                </td>
                <td valign="top">
                    <asp:TextBox ID="customerNameTextBox" runat="server" Width="260px" CssClass="input-field"></asp:TextBox>
                    <span style="color: Red;">*</span>
                </td>
                <td style="font-weight: bold; text-align:right">
                    <asp:Label ID="appliedAmountLabel" Text="Applied Amount" runat="server"></asp:Label>
                </td>
                <td >
                    <asp:TextBox Width="260px" CssClass="input-field" ID="appliedAmountTextBox" runat="server"></asp:TextBox>
                    <asp:Label ID="appliedAmountMandatoryLabel" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
          
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="fathersNameLabel" Text="Father's Name" runat="server"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox CssClass="input-field" ID="fathersNameTextBox" runat="server" Width="260px"></asp:TextBox>
                </td>
                <td style="font-weight: bold; text-align:right; width: 200px;">
                    <asp:Label ID="mothersNameLabel" Text="Mother's Name" runat="server"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox CssClass="input-field" ID="mothersNameTextBox" runat="server" Width="260px"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="dateOfBirthLabel" Text="Date of Birth" runat="server"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox CssClass="input-field" ID="dobTextBox" runat="server" Width="240px" MaxLength="10"></asp:TextBox>
                    <asp:Label ID="mandatory3" Text="*" runat="server" ForeColor="Red"></asp:Label>
                    (dd-mm-yyyy)
                </td>
                <td style="font-weight: bold" align="right">
                    <asp:Label ID="maritalStatusLabel" Text="Marital Status" runat="server"></asp:Label>
                </td>
                <td style="font-weight: bold" align="left">
                    <asp:DropDownList CssClass="select-field" ID="maritalStatusDropDownList" runat="server" Width="260px">
                    </asp:DropDownList>
                </td>
            </tr>
           
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="declaredIncomeLabel" Text="Declared Income" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" ID="declaredIncomeTextBox" Width="260px" runat="server"></asp:TextBox>
                    &nbsp;<asp:Label ID="appliedAmountMandatoryLabel0" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td style="font-weight: bold;" align="right">
                    <asp:Label ID="tinNoLabel" Text="TIN No." runat="server"></asp:Label>
                </td>
                <td style="font-weight: bold;" align="left">
                    <asp:TextBox CssClass="input-field" ID="tinNoPreTextBox" runat="server" Width="260px" MaxLength="16"></asp:TextBox>
                    <asp:TextBox CssClass="input-field" ID="tinNoMasterTextBox" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                    <asp:TextBox CssClass="input-field" ID="tinNoPostTextBox" runat="server" Width="80px" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="acNoSCBLabel" Text="A/C No. for SCB" runat="server"></asp:Label>
                </td>
                <td valign="top">
                    <asp:GridView ID="scbAccGridView" runat="server" Width="288px" AutoGenerateColumns="False"   
                        PageSize="5" class="style1" CssClass="myGridStyle">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:TextBox CssClass="input-field has-width-100px" ID="scbPreTextBox" runat="server" Width="65px"
                                        MaxLength="2"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="A/C No.">
                                <ItemTemplate>
                                    <asp:TextBox CssClass="input-field has-width-100px" ID="scbMasterTextBox" runat="server" Width="148px"
                                        MaxLength="7"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:TextBox CssClass="input-field has-width-100px" ID="scbPostTextBox" runat="server" Width="65px" MaxLength="2"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="ssHeader" />
                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                    </asp:GridView>
                </td>
                <td style="font-weight: bold; text-align:right" valign="top">
                    <asp:Label ID="lblIDInfo" Text="ID Info." runat="server"></asp:Label>
                </td>
                <td valign="top" align="left" style="font-weight: bold">
                    <asp:GridView ID="idGridView" runat="server" Width="268px" AutoGenerateColumns="False"
                        class="style1" PageSize="5" CssClass="myGridStyle">
                        <Columns>
                            <asp:TemplateField HeaderText="ID Type">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="select-field" ID="idTypeDropDownList" runat="server" Width="140px">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID No.">
                                <ItemTemplate>
                                    <asp:TextBox CssClass="input-field" ID="idNoTextBox" runat="server" Width="150px"
                                        MaxLength="25"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Expiry Date">
                                <ItemTemplate>
                                    <asp:TextBox CssClass="input-field" ID="expiryDateTextBox" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="ssHeader" />
                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                    </asp:GridView>
                </td>
            </tr>

            <tr>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="creditCardNoLabel" Text="Card Account No." runat="server"></asp:Label>
                </td>

                <td>
                    <asp:GridView ID="creditCardNoGridView" runat="server" Width="350px" AutoGenerateColumns="False"
                        class="style1" PageSize="5" CssClass="myGridStyle">
                        <Columns>
                            <asp:TemplateField HeaderText="Card Account No.">
                                <ItemTemplate>
                                    <asp:TextBox CssClass="input-field" ID="crdtCrdNoTextBox" runat="server" Width="265px"
                                        MaxLength="15"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="ssHeader" />
                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                    </asp:GridView>
                </td>
                <td valign="top" align="right">
                    <asp:Label ID="contactNoLabel0" Text="Contact No." runat="server" Font-Bold="True"></asp:Label>
                </td>
                <td valign="top">
                    <asp:GridView ID="contactGridView" runat="server" Width="442px" AutoGenerateColumns="False"
                        class="style1" PageSize="5" CssClass="myGridStyle">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Number">
                                <ItemTemplate>
                                    <asp:TextBox CssClass="input-field" ID="contactNoTextBox" runat="server"
                                        MaxLength="15"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="ssHeader" />
                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                    </asp:GridView>
                </td>

            </tr>

            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="educationalLabel" Text="Highest Educational Level" runat="server"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:DropDownList CssClass="select-field" ID="educationalDropDownList" runat="server" Width="260px">
                    </asp:DropDownList>
                    <asp:Label ID="mandatory4" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td align="right">
                    <asp:Label ID="Label34" runat="server" Font-Bold="True" Text="Gender "></asp:Label>
                </td>
                <td>
                    <asp:DropDownList CssClass="select-field" ID="genderDropDownList" runat="server" Width="260px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="residentialAddressLabel" Text="Address" runat="server"></asp:Label>
                </td>
                <td >
                    <asp:TextBox CssClass="textarea-field" ID="residentialAddressTextBox" runat="server" Width="260px" TextMode="MultiLine"
                        MaxLength="200" Rows="5"></asp:TextBox>
                </td>
                <td style="font-weight: bold" valign="top" align="right">
                    <asp:Label ID="officeAddressLabel" Text="Office Address" runat="server"></asp:Label>
                </td>
                <td >
                    <asp:TextBox CssClass="textarea-field" ID="officeAddressTextBox" runat="server" Width="260px" TextMode="MultiLine"
                        MaxLength="200" Rows="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="professionLabel" runat="server" Text="Profession"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:DropDownList CssClass="select-field" ID="professionDropdownList" runat="server" Width="260px">
                    </asp:DropDownList>
                </td>
                <td style="font-weight: bold" valign="top" align="right">
                    <asp:Label ID="workExperienceLabel" Text="Months in current Job/Business" runat="server"></asp:Label>
                </td>
                <td style="font-weight: bold">
                    <asp:TextBox CssClass="input-field" ID="workExperienceMonthTextBox" runat="server" Width="260px"></asp:TextBox>

                    <asp:Label ID="mandatory5" Text="*" runat="server" ForeColor="Red"></asp:Label>
                    <asp:Label ID="workExperienceMonthLabel" Text="Months" runat="server" Width="45px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="companyNameLabel" Text="Name of Company" runat="server"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox CssClass="input-field" ID="companyNameTextBox" runat="server" Width="260px"></asp:TextBox>
                </td>
                <td align="right">
                    <asp:Label ID="Label36" runat="server" Font-Bold="True" Text="Spouse Name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" ID="documentTypeTextBox" Width="260px" runat="server" MaxLength="60"></asp:TextBox>
                </td>
            </tr>
            
            

            <tr >
                <td style="font-weight: bold; text-align:left" valign="top">Remarks List
                </td>
                <td valign="top" colspan="3">
                    <div id="remarksDiv"  class="GridviewBorder" style="overflow-y: scroll; width: 635px; height:300px;  z-index: -2; border: solid 1px gray;">
                        <asp:GridView ID="remarksGridView" CssClass="myGridStyle" runat="server" Width="302px" AutoGenerateColumns="False"
                            PageSize="2" >
                            <Columns>
                                <asp:TemplateField HeaderText="Id" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="remarksIdLabel" runat="server" Text='<%# Eval("Id") %>' Width="30px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="selectCheckBox" runat="server" Width="15px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remarks">
                                    <ItemTemplate>
                                        <asp:TextBox CssClass="input-field" ID="remarksTextBox" runat="server" Width="560px" Text='<%# Eval("Name") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="FixedHeader" />
                            <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>

            <tr style="visibility:hidden">
                <td style="font-weight: bold;" align="right">
                    <asp:Label ID="residentialStatusLabel" Text="Residential Status" runat="server"></asp:Label>
                </td>
                <td style="font-weight: bold" align="left">
                    <asp:DropDownList CssClass="select-field" ID="residentialStatusDropDownList" runat="server" Width="260px">
                    </asp:DropDownList>
                </td>
                
                <td align="right">
                    <asp:Label ID="Label37" runat="server" Font-Bold="True" Text="Multiple Loan Flag"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList CssClass="select-field" ID="multipleLoanDropDownList" runat="server" Width="260px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr style="visibility:hidden">
                <td align="right">
                    <asp:Label ID="Label35" runat="server" Font-Bold="True" Text="Dependent"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" ID="dependentTextBox" Width="260px" runat="server" MaxLength="60"></asp:TextBox>
                </td>
                
                <td align="right" style="font-weight: bold">
                    <asp:Label ID="spousesOccupationLabel" runat="server" Text="Spouse's Occupation"></asp:Label>
                </td>
                <td align="left" style="font-weight: bold">
                    <asp:DropDownList CssClass="select-field" ID="spousesOccupationDropdownList" runat="server" Width="260px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr style="visibility: hidden;">
                <td style="font-weight: bold;" valign="top">
                    <asp:Label ID="bankAccountLabel" Text="Account with Other Bank" runat="server"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:DropDownList CssClass="select-field" ID="vechicleDropDownList" Visible="false" runat="server" Height="0px" Width="260px">
                    </asp:DropDownList>
                    <asp:TextBox Visible="false" CssClass="input-field" ID="extraFieldTextBox9" runat="server"></asp:TextBox>
                    <asp:TextBox Visible="false" CssClass="input-field" ID="extraFieldTextBox10" runat="server"></asp:TextBox>
                    <asp:DropDownList Visible="false" CssClass="select-field" ID="earlierLoanWScbDropDownList" runat="server" Width="100px">
                    </asp:DropDownList>
                    <asp:DropDownList Visible="false" CssClass="select-field" ID="availedAutoLoanDropDownList" runat="server" Width="100px">
                    </asp:DropDownList>

                    <asp:GridView ID="otherBankGridView" runat="server" AutoGenerateColumns="false" class="style1"
                        PageSize="5" CssClass="GridviewBorder">
                        <%-- <Columns>
                            <asp:TemplateField HeaderText="Bank">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="select-field" ID="bankDropDownList" runat="server" Width="200px">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Branch">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="select-field" ID="branchDropDownList" runat="server" Width="200px">
                                    </asp:DropDownList><asp:HiddenField ID="branchId" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="A/C No.">
                                <ItemTemplate>
                                    <asp:TextBox CssClass="input-field" ID="acNoTextBox" runat="server" Width="150px"
                                        MaxLength="25"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="A/C Type">
                                <ItemTemplate>
                                    <asp:DropDownList CssClass="select-field" ID="acTypeDropDownList" runat="server" Width="130px">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="ssHeader" />
                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                    </asp:GridView>
                    <uc1:BranchListPopulatejs runat="server" ID="BranchListPopulatejs1" />
                </td>
            </tr>
            <%-- <tr style="visibility:hidden">
                <td style="font-weight: bold; visibility:hidden" valign="top">
                    <asp:Label ID="vehicleLabel" runat="server" Text="Auto Mobile "></asp:Label>
                </td>
                <td style="visibility:hidden" valign="top" class="auto-style2">--%>

            <%-- </td>
                <td style="visibility:hidden" valign="top" colspan="2">
                </td>
            </tr>--%>
            <%--<tr>
                <td style="font-weight: bold; width: 0;" valign="top" align="left">
                    <asp:Label ID="Label40" runat="server" Text="Earlier Loan With SCB" Width="180px"></asp:Label>
                </td>
                <td style="font-weight: bold;" valign="top" class="auto-style2">
                    <asp:DropDownList CssClass="select-field" ID="earlierLoanWScbDropDownList1" runat="server" Width="100px">
                    </asp:DropDownList>
                </td>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="Label39" runat="server" Text="Customer Availed Auto Loan" Width="200px"></asp:Label>
                </td>
                <td style="font-weight: bold" valign="top">
                    <asp:DropDownList CssClass="select-field" ID="availedAutoLoanDropDownList1" runat="server" Width="100px">
                    </asp:DropDownList>
                </td>
            </tr>--%>
            
            <%--  <tr>
                <td style="font-weight: bold; width: 0;" valign="top" align="left">
                    <asp:Label ID="Label42" runat="server" Font-Bold="True" Text="Extra Field 9" Width="100px"></asp:Label>
                </td>
                <td style="font-weight: bold;" valign="top" class="auto-style2">
                    <asp:TextBox CssClass="input-field" ID="extraFieldTextBox999" runat="server"></asp:TextBox>
                </td>
                <td style="font-weight: bold" valign="top">Extra Field 10
                </td>
                <td style="font-weight: bold" valign="top">
                    <asp:TextBox CssClass="input-field" ID="extraFieldTextBox1099" runat="server"></asp:TextBox>
                </td>
            </tr>--%>
            


        </table>
        <!-- Hide & Show -->
        <%--<table border="0px" cellpadding="1" cellspacing="1">
            <tr style="height: 20px">
                <td style="background-color: Gray; color: White;">
                    <a><span id="myDivSpan">
                        <img id="plus_minus" runat="server" src="~/images/plus.gif" onclick="showHideDetails()"
                            alt="" />Joint Applicant-1</span> </a>
                    <asp:HiddenField ID="showValue" runat="server" Value="0" />
                </td>
            </tr>
            <tr id="spanDetails" style="display: none; height: 200px">
                <td>
                    <table>
                        <tr>
                            <td style="font-weight: bold; width: 200px">
                                <asp:Label ID="jointApplicantsNameLabel" Text="Joint Applicant's Name" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jointApplicantsNameTextBox1" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="jointAcNoLabel" Text="A/C No. for SCB" runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr style="width: 280px">
                                        <td>
                                            <asp:GridView ID="jointApplicantaccNoGridView1" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jointAcNoSCBPreTextBox1" runat="server" Width="60px" CssClass="ssTextBox"
                                                                MaxLength="2"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="A/C No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jointAcNoSCBMasterTextBox1" runat="server" Width="140px" CssClass="ssTextBox"
                                                                MaxLength="7"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jointAcNoSCBPostTextBox1" runat="server" Width="60px" CssClass="ssTextBox"
                                                                MaxLength="2"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="jointTinNumberLabel" Text="TIN No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jTinPreTextBox1" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                <asp:TextBox CssClass="input-field" ID="jTinMasterTextBox1" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                <asp:TextBox CssClass="input-field" ID="jTinPostTextBox1" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="jointCreditCardLabel" Text="Card Account No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="jointCreditCardGridView1" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Card No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jointCardNoTextBox1" runat="server" Width="260px" CssClass="ssTextBox"
                                                                MaxLength="16"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="jointApplicantFatherLabel" Text="Father's Name" runat="server"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox CssClass="input-field" ID="jointApplicantFatherTextBox1" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="jointApplicantMotherLabel" Text="Mother's Name" runat="server"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox CssClass="input-field" ID="jointApplicantMotherTextBox1" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="dobLabel" Text="Date of Birth" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="JDOBTextBox1" runat="server" MaxLength="10"></asp:TextBox>
                                &nbsp;(dd-mm-yyyy)
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="jointEducationalLabel" Text="Highest Educational Level" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList CssClass="select-field" ID="jointEducationalDropDownList1" runat="server" Width="100px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="jointcontactLabel" Text="Contact No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="jointcontactGridView1" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="4" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Number">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jointContactNoTextBox1" runat="server" Width="200px" CssClass="ssTextBox"
                                                                MaxLength="15"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="jointCompanyNameLabel" Text="Name of Company" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jointCompanyNameTextBox1" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="jointIdLabel" Text="ID Info." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr style="width: 280px">
                                        <td>
                                            <asp:GridView ID="jointIdGridView1" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID Type">
                                                        <ItemTemplate>
                                                            <asp:DropDownList CssClass="select-field" ID="jointIdTypeDropDownList1" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ID No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jointIdNoTextBox1" runat="server" Width="150px" CssClass="ssTextBox"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expiry Date (dd-mm-yyyy)">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jointIdexpiryDateTextBox1" runat="server" Width="100px" CssClass="ssTextBox"
                                                                MaxLength="10"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp&nbsp&nbsp&nbsp
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 20px">
                <td style="background-color: Gray; color: White;">
                    <a><span id="span1">
                        <img id="img" runat="server" src="~/images/plus.gif" onclick="showHideDetails1()"
                            alt="" />Joint Applicant-2</span> </a>
                    <asp:HiddenField ID="hiddenField" runat="server" Value="0" />
                </td>
            </tr>
            <tr id="tr1" style="display: none;">
                <td>
                    <table>
                        <tr>
                            <td style="font-weight: bold; width: 200px">
                                <asp:Label ID="Label1" Text="Joint Applicant's Name" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppNameTextBox2" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label2" Text="A/C No. for SCB" runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr style="width: 280px">
                                        <td>
                                            <asp:GridView ID="jAppSCBAccGridView2" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccPreTextBox2" runat="server" Width="60px" CssClass="ssTextBox"
                                                                MaxLength="2"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="A/C No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccMasterTextBox2" runat="server" Width="140px" CssClass="ssTextBox"
                                                                MaxLength="7"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccPostTextBox2" runat="server" Width="60px" CssClass="ssTextBox"
                                                                MaxLength="2"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label3" Text="TIN No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppTinPreTextBox2" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                <asp:TextBox CssClass="input-field" ID="jAppTinMasterTextBox2" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                <asp:TextBox CssClass="input-field" ID="jAppTinPostTextBox2" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label4" Text="Card Account No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="jCrdtCrdGridView2" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Card No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppCrdtCrdTextBox2" runat="server" Width="260px" CssClass="ssTextBox"
                                                                MaxLength="16"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label5" Text="Father's Name" runat="server"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox CssClass="input-field" ID="jAppFatherNameTextBox2" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label6" Text="Mother's Name" runat="server"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox CssClass="input-field" ID="jAppMotherNameTextBox2" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label7" Text="Date of Birth" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppDOBTextBox2" runat="server" MaxLength="10"></asp:TextBox>
                                &nbsp;(dd-mm-yyyy)
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label8" Text="Highest Educational Level" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList CssClass="select-field" ID="jAppEducationDropDownList2" runat="server" Width="100px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label9" Text="Contact No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="jAppContactGridView2" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Number">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppContactTextBox2" runat="server" Width="200px" CssClass="ssTextBox"
                                                                MaxLength="15"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label10" Text="Name of Company" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppCompanyTextBox2" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label11" Text="ID Info." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr style="width: 280px">
                                        <td>
                                            <asp:GridView ID="jAppIdGridView2" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID Type">
                                                        <ItemTemplate>
                                                            <asp:DropDownList CssClass="select-field" ID="jAppIdTypeDropDownList2" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ID No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppIdNoTextBox2" runat="server" Width="150px" CssClass="ssTextBox"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expiry Date (dd-mm-yyyy)">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppIdExpiryDateTextBox2" runat="server" Width="100px" CssClass="ssTextBox"
                                                                MaxLength="10"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp&nbsp&nbsp&nbsp
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 20px">
                <td style="background-color: Gray; color: White;">
                    <a><span id="span2">
                        <img id="imgTwo" runat="server" src="~/images/plus.gif" onclick="showHideDetails2()"
                            alt="" />Joint Applicant-3</span> </a>
                    <asp:HiddenField ID="hiddenFieldTwo" runat="server" Value="0" />
                </td>
            </tr>
            <tr id="trTwo" style="display: none;">
                <td>
                    <table>
                        <tr>
                            <td style="font-weight: bold; width: 200px">
                                <asp:Label ID="Label12" Text="Joint Applicant's Name" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppNameTextBox3" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label13" Text="A/C No. for SCB" runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr style="width: 280px">
                                        <td>
                                            <asp:GridView ID="jAppSCBAccGridView3" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccPreTextBox3" runat="server" Width="60px" CssClass="ssTextBox"
                                                                MaxLength="2"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="A/C No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccMasterTextBox3" runat="server" Width="140px" CssClass="ssTextBox"
                                                                MaxLength="7"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccPostTextBox3" runat="server" Width="60px" CssClass="ssTextBox"
                                                                MaxLength="2"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label14" Text="TIN No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppTinPreTextBox3" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                <asp:TextBox CssClass="input-field" ID="jAppTinMasterTextBox3" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                <asp:TextBox CssClass="input-field" ID="jAppTinPostTextBox3" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label15" Text="Card Account No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="jAppCrdtCrdGridView3" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Card No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppCrdtCrdTextBox3" runat="server" Width="260px" CssClass="ssTextBox"
                                                                MaxLength="16"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label16" Text="Father's Name" runat="server"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox CssClass="input-field" ID="jAppFatherNameTextBox3" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label17" Text="Mother's Name" runat="server"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox CssClass="input-field" ID="jAppMotherNameTextBox3" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label18" Text="Date of Birth" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppDOBTextBox3" runat="server" MaxLength="10"></asp:TextBox>
                                &nbsp;(dd-mm-yyyy)
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label19" Text="Highest Educational Level" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList CssClass="select-field" ID="jAppEducationDropDownList3" runat="server" Width="100px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label20" Text="Contact No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="jAppContactGridView3" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Number">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppContactTextBox3" runat="server" Width="200px" CssClass="ssTextBox"
                                                                MaxLength="15"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label21" Text="Name of Company" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppCompanyTextBox3" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label22" Text="ID Info." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr style="width: 280px">
                                        <td>
                                            <asp:GridView ID="jAppIdGridView3" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID Type">
                                                        <ItemTemplate>
                                                            <asp:DropDownList CssClass="select-field" ID="jAppIdTypeTextBox3" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ID No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppIdTextBox3" runat="server" Width="100px" CssClass="ssTextBox"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expiry Date (dd-mm-yyyy)">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppIdExpiryDateTextBox3" runat="server" Width="100px" CssClass="ssTextBox"
                                                                MaxLength="10"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp&nbsp&nbsp&nbsp
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 20px">
                <td style="background-color: Gray; color: White;">
                    <a><span id="span3">
                        <img id="imgThree" runat="server" src="~/images/plus.gif" onclick="showHideDetails3()"
                            alt="" />Joint Applicant-4</span> </a>
                    <asp:HiddenField ID="hiddenFieldThree" runat="server" Value="0" />
                </td>
            </tr>
            <tr id="trThree" style="display: none;">
                <td>
                    <table>
                        <tr>
                            <td style="font-weight: bold; width: 200px">
                                <asp:Label ID="Label23" Text="Joint Applicant's Name" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppNameTextBox4" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label24" Text="A/C No. for SCB" runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr style="width: 280px">
                                        <td>
                                            <asp:GridView ID="jSCBAccGridView4" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccPreTextBox4" runat="server" Width="60px" CssClass="ssTextBox"
                                                                MaxLength="2"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="A/C No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccMasterTextBox4" runat="server" Width="140px" CssClass="ssTextBox"
                                                                MaxLength="7"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppSCBAccPostTextBox4" runat="server" Width="60px" CssClass="ssTextBox"
                                                                MaxLength="2"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label25" Text="TIN No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppTinPreTextBox4" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                <asp:TextBox CssClass="input-field" ID="jAppTinMasterTextBox4" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                                <asp:TextBox CssClass="input-field" ID="jAppTinPostTextBox4" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label26" Text="Card Account No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="jAppCrdtCrdGridView4" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Card No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppCrdtCrdTextBox4" runat="server" Width="260px" CssClass="ssTextBox"
                                                                MaxLength="16"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label27" Text="Father's Name" runat="server"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox CssClass="input-field" ID="jAppFatherNameTextBox4" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label28" Text="Mother's Name" runat="server"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox CssClass="input-field" ID="jAppMotherNameTextBox4" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label29" Text="Date of Birth" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppDOBTextBox4" runat="server" MaxLength="10"></asp:TextBox>
                                &nbsp;(dd-mm-yyyy)
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label30" Text="Highest Educational Level" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList CssClass="select-field" ID="jAppEducationDropDownList4" runat="server" Width="100px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label31" Text="Contact No." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="jAppContactGridView4" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Number">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppContactTextBox4" runat="server" Width="200px" CssClass="ssTextBox"
                                                                MaxLength="15"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold">
                                <asp:Label ID="Label32" Text="Name of Company" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox CssClass="input-field" ID="jAppCompanyTextBox4" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" valign="top">
                                <asp:Label ID="Label33" Text="ID Info." runat="server"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr style="width: 280px">
                                        <td>
                                            <asp:GridView ID="jAppIdGridView4" runat="server" Width="268px" AutoGenerateColumns="False"
                                                class="style1" PageSize="5" CssClass="GridviewBorder">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID Type">
                                                        <ItemTemplate>
                                                            <asp:DropDownList CssClass="select-field" ID="jAppIdTypeTextBox4" runat="server" Width="100px" Height="10px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ID No.">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppIdNoTextBox4" runat="server" Width="100px" CssClass="ssTextBox"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expiry Date (dd-mm-yyyy)">
                                                        <ItemTemplate>
                                                            <asp:TextBox CssClass="input-field" ID="jAppIdExpiryDateTextBox4" runat="server" Width="100px" CssClass="ssTextBox"
                                                                MaxLength="10"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="ssHeader" />
                                                <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="bottom">
                                            &nbsp&nbsp&nbsp&nbsp
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>--%>

        <table>
            <tr>
                <td style="width: 220px" align="center"></td>
                <td style="width: 400px" align="center">
                    <asp:Button ID="deferButton" runat="server" Text="Defer" Width="95px" Height="30px"
                        OnClick="deferButton_Click" />
                    <asp:Button ID="forwardButton" Text="Forward" runat="server" Width="95px" Height="30px"
                        OnClick="forwardButton_Click" OnClientClick="return ValidteLoanApplicationForm(); return LLIdIsEmpty();" />
                    <asp:Button CssClass="btn primarybtn" ID="saveButton" Text="Save" runat="server" OnClick="saveButton_Click" OnClientClick="return ValidteLoanApplicationForm(); return LLIdIsEmpty();" />
                    &nbsp;<asp:Button ID="clearButton" CssClass="btn clearbtn" Text="Clear" runat="server" Width="95px" Height="30px" OnClick="clearButton_Click" />
                </td>
                <td style="width: 280px; padding-left: 30px;" align="left">
                    <%--<asp:Label ID="errorMsgLabel" runat="server" ForeColor="#FF3300"></asp:Label>--%>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
