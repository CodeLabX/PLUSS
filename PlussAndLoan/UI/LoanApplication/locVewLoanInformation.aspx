﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanViewer.Master" AutoEventWireup="true" CodeBehind="locVewLoanInformation.aspx.cs" Inherits="PlussAndLoan.UI.LoanApplication.locVewLoanInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>::: LOAN LOCATOR :::</title>
    <style>
        table.az-gridTb th .part1 {
            background-color: #f49970;
        }

        table.az-gridTb th {
            border-width: 1px;
            padding: 3px;
            border-style: solid;
            border: #c7c7c7 1px solid;
            background-color: #9fbce3;
        }

        .az-gridTb {
            width: 100%;
            border-collapse: collapse;
        }

            .az-gridTb td {
                padding: 1px;
                border: #c7c7c7 1px solid;
            }

            .az-gridTb tr {
                background: #d3e3f9;
            }

                .az-gridTb tr:nth-child(odd) {
                    background: #d3e3f9;
                }

                .az-gridTb tr:nth-child(even) {
                    background: #e4eaf3;
                }

        .azBold {
            font-weight: bold;
        }

        .az-pageTitle {
            color: whitesmoke;
            margin-top: 15px;
            margin-bottom: 5px;
            font-weight: bold;
            font-size: 1em;
            background-color: #017AA7;
            padding: 2px 2px 2px 2px;
            box-shadow: 1px 1px black;
        }

        .leftDiv {
            float: left;
        }

        .rightDiv {
            float: right;
        }

        .centerAlign {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div id="divViewApplication" class="centerAlign" runat="server">
        <input id="status_update" type="hidden" value="100" runat="server" />
        <div style="width: 100%; background-color: rgb(5, 110, 132); color: white; margin-top: 1px">
            <h3>Loan Information</h3>
        </div>
        <table width="100%" class="az-gridTb">
            <tr>
                <td style="width: 60%">
                    <table width="100%">
                        <tr>
                            <td style="width: 40%" class="rightAlign azBold">Application ID:</td>
                            <td class="leftAlign">&nbsp;<span id="llid" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Product:</td>
                            <td class="leftAlign">&nbsp;<span id="spProduct" runat="server"></span></td>

                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Source Name:</td>
                            <td class="leftAlign">&nbsp;<span id="spSource" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Department Name:</td>
                            <td class="leftAlign">&nbsp;<span id="spDept" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Submission Date:</td>
                            <td class="leftAlign">&nbsp;<span id="spSubmissionDate" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Application Date:</td>
                            <td class="leftAlign">&nbsp;<span id="spApplicationDate" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">A/C No.:</td>
                            <td class="leftAlign">&nbsp;<span id="spAccNo" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Loan A/C No.:</td>
                            <td class="leftAlign">&nbsp;<span id="spLoanAcc" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">A/C Type:</td>
                            <td class="leftAlign">&nbsp;<span id="spAccType" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Submitted By:</td>
                            <td class="leftAlign">&nbsp;<span id="spSubmitBy" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Applied Loan Amount:</td>
                            <td class="leftAlign">&nbsp;<span id="spAmount" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Conditional Approved Amount:</td>
                            <td class="leftAlign">&nbsp;<span id="spConApproAmnt" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Approved Amount:</td>
                            <td class="leftAlign">&nbsp;<span id="spApproAmnt" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Customer Name:</td>
                            <td class="leftAlign">&nbsp;<span id="spCustomer" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Organization:</td>
                            <td class="leftAlign">&nbsp;<span id="spOrganization" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Profession:</td>
                            <td class="leftAlign">&nbsp;<span id="spProfession" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Loan Status:</td>
                            <td class="leftAlign">&nbsp;<span id="spLoanStatus" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Decision Status:</td>
                            <td class="leftAlign">&nbsp;<span id="spDecission" runat="server"></span></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <fieldset class="leftAlign">
                        <legend style="font-weight: normal; font-size: 1em">CIB Report Status<span runat="server" id="cibRemarks"></span></legend>
                        <input runat="server" type="checkbox" id="cibRec" onclick="checkcibRec()" />Report Received<br />
                        <input runat="server" type="checkbox" id="systemErr" onclick="checksystemErr()" />System Error<br />
                        <input runat="server" type="checkbox" id="underTaking" onclick="checkunderTaking()" />Undertaking Returned
                         <br />
                    </fieldset>
                </td>
            </tr>
        </table>
        <div class="az-pageTitle" style="padding: 0 0 0 0; box-shadow: none; margin-bottom: 0; margin-top: 0; color: black">
            <div class="leftDiv widthSize25_per leftAlign">Loan Request Update Details</div>
            <div class="rightDiv widthSize25_per rightAlign">TOTAL TAT#&nbsp;<span id="spTotatlTat" runat="server">0</span>&nbsp;(days)&nbsp;</div>
            <div class="clear"></div>
        </div>
        <div id="tdDetails" runat="server"></div>
      <%--  <button style="margin-top: 5px"><a href="/pluss/UI/LocOperationHome.aspx" style="color: black">Close</a></button>--%>
    </div>


    <script type="text/javascript">
        $("#toOtherVc").hide();
        function goBack() {
            window.history.back();
        }
        function checkcibRec() {
            if ($('input[id=cibRec]').is(':checked')) {
                $('input[id=cibRec]').attr('checked', true);
                $('input[id=systemErr]').attr('checked', false);
                $('input[id=underTaking]').attr('checked', false);
            }
            else {
                $('input[id=cibRec]').attr('checked', false);
                $('input[id=systemErr]').attr('checked', true);
                $('input[id=underTaking]').attr('checked', false);
            }
        }
        function checksystemErr() {

            if ($('input[id=systemErr]').is(':checked')) {
                $('input[id=cibRec]').attr('checked', false);
                $('input[id=systemErr]').attr('checked', true);
                $('input[id=underTaking]').attr('checked', false);
            }
            else {
                $('input[id=cibRec]').attr('checked', false);
                $('input[id=systemErr]').attr('checked', false);
                $('input[id=underTaking]').attr('checked', true);
            }

        }
        function checkunderTaking() {

            if ($('input[id=underTaking]').is(':checked')) {
                $('input[id=cibRec]').attr('checked', false);
                $('input[id=systemErr]').attr('checked', false);
                $('input[id=underTaking]').attr('checked', true);
            }
            else {
                $('input[id=cibRec]').attr('checked', true);
                $('input[id=systemErr]').attr('checked', false);
                $('input[id=underTaking]').attr('checked', false);
            }
        }
        function day() {
            var conent = "<select name=d1 id=d1>";
            var d = date("d");
            for (var i = 1; i < 32; i++) {
                if (d == i)
                    conent += " <option value='" + i + "' selected> $i </option>";
                else
                    conent += " <option value=i> i </option>";
            }
            $("#spDay").html(conent);
        }

    </script>
</asp:Content>
