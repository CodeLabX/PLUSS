﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.WebControls;
using AzUtilities;
using BLL;
using BusinessEntities;
using DAL;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI.LoanApplication
{
    public partial class UI_LocAddNewLoan : System.Web.UI.Page
    {
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        readonly LocatorApplicationService _service = new LocatorApplicationService();
        public RemarksManager remarksManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            remarksManagerObj = new RemarksManager();
            msgLabel.Text = "";
            if (!IsPostBack)
            {
                InitializedData();
                ProductDropDownListItem();
                SourceDropDownListItem();
                //BankDropDownListItem();
                //BranchDropDownListInitailItem();

                ProfessionDropDownListItems();
                //FillRemarksGridView();
            }
        }

        private void InitializedData()
        {
            try
            {
                txtSubmittedBy.Text = (string)Session["UserName"];
                var userId = (string)Session["UserId"];
                txtSubmissionDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                loan_date.Value = DateTime.Now.ToString("yyyy-MM-dd");

                var remarksListObj = _service.GetCheckList(2);
                remarksGridView.DataSource = remarksListObj;
                remarksGridView.DataBind();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("UI_LocAddNewLoan :: InitializedData");
                LogFile.WriteLog(ex);
            }

        }

        private void ProfessionDropDownListItems()
        {
            try
            {
                List<Profession> professionListObj = _loanApplicationManagerObj.GetAllProfession();
                Profession professionObj = new Profession();
                professionObj.Id = 0;
                professionObj.Name = "";
                professionListObj.Insert(0, professionObj);
                professionDropdownList.DataSource = professionListObj;
                professionDropdownList.DataTextField = "Name";
                professionDropdownList.DataValueField = "Id";
                professionDropdownList.DataBind();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("UI_LocAddNewLoan :: ProfessionDropDownListItems");
                LogFile.WriteLog(ex);
            }
        }

        private void SourceDropDownListItem()
        {
            try
            {
                //List<Source> sourceLisObj = _loanApplicationManagerObj.LocGetAllSource();

                List<Source> sourceLisObj = _loanApplicationManagerObj.GetAllSource();
                Source sourceObj = new Source();
                sourceObj.SourceId = 0;
                sourceObj.SourceName = "";
                sourceLisObj.Insert(0, sourceObj);
                sourceDropDownList.DataTextField = "SourceName";
                sourceDropDownList.DataValueField = "SourceId";
                sourceDropDownList.DataSource = sourceLisObj;
                sourceDropDownList.DataBind();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("UI_LocAddNewLoan :: SourceDropDownListItem");
                LogFile.WriteLog(ex);
            }
        }

        private void ProductDropDownListItem()
        {
            try
            {
                List<Product> productListObj = _loanApplicationManagerObj.GetAllProduct();
                var productObj = new Product { ProductId = 0, ProductName = "" };
                productListObj.Insert(0, productObj);
                productDropDownList.DataTextField = "ProductName";
                productDropDownList.DataValueField = "ProductId";
                productDropDownList.DataSource = productListObj;
                productDropDownList.DataBind();
                //productDropDownList.SelectedValue = 16.ToString();
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("UI_LocAddNewLoan :: ProductDropDownListItem");
                LogFile.WriteLog(ex);
            }
        }

        protected void btnSaveLoanApplication_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateLoanApp())
                {
                    var loan = GetLoanApplication();
                    var res = _service.SaveNewLoan(loan);
                    new AT(this).AuditAndTraial("Add New Loan", "New Loan");
                    if (res > 0)
                    {
                        //msgLabel.Text = "Application saved successfully ( LLID: "+res+" )";
                        Response.Redirect("~/UI/LoanApplication/loc_EditLoan.aspx?id=" + res);
                        msgLabel.ForeColor = Color.Green;
                        msgLabel.Font.Bold = true;
                        ClearForm();
                    }
                    else
                    {
                        msgLabel.Text = "Application not save. Please check for the loan ID";
                        msgLabel.ForeColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                msgLabel.Text = "Application not save";
                msgLabel.ForeColor = Color.Red;

                LogFile.WriteLine("UI_LocAddNewLoan :: btnSaveLoanApplication_Click");
                LogFile.WriteLog(ex);
            }
        }

        private void ClearForm()
        {
            txtPreAcc.Text="";
            txtMidAcc.Text="";
            txtPreAcc.Text="";
            customerNameTextBox.Text="";
            appliedAmountTextBox.Text = "";
            txtSourcePwId.Text="";
            txtDecliendLoan.Text="";
            chkCpf.Checked = false;
            chkPriority.Checked=false;
            professionDropdownList.SelectedValue = "0";
            companyNameTextBox.Text = "";
        }

        private Loan GetLoanApplication()
        {
            try
            {
                var obj = new Loan();
                obj.SubmissionDate = DateTime.Now;
                obj.ProductId = Convert.ToInt32(productDropDownList.SelectedValue);
                obj.PreAccNo = txtPreAcc.Text;
                obj.MidAccNo = txtMidAcc.Text;
                obj.PostAccNo = txtPostAcc.Text;
                obj.CustomerName = customerNameTextBox.Text;
                obj.AppliedAmount = Convert.ToInt32(appliedAmountTextBox.Text);
                var user = (User)Session["UserDetails"];
                obj.SubmitedBy = user.Id;
                obj.LoanSourceCode = txtSourcePwId.Text;
                obj.ARMCode = txtARMCode.Text;
                obj.LoanState = 1;
                obj.EntryBranch = Convert.ToInt32(sourceDropDownList.SelectedValue);
                obj.PreDeclined = txtDecliendLoan.Text;
                obj.ForCrd = 1;
                obj.ApplicationDate = Convert.ToDateTime(txtSubmissionDate.Text); ;
                obj.Cep = chkCpf.Checked;
                obj.Prio = chkPriority.Checked;
                if (obj.Cep && obj.Prio) { obj.AccType = 3; }
                else if (obj.Prio) { obj.AccType = 2; }
                else if (obj.Cep) { obj.AccType = 1; }
                else { obj.AccType = 4; }
                obj.EmployeeType = professionDropdownList.SelectedItem.Text;
                obj.Organization = companyNameTextBox.Text;
                obj.AutoDelarName = txtSubmittedBy.Text;
                obj.MakerID = (string)Session["UserId"];


                obj.CheckList = new List<int>();

                foreach (var id in from GridViewRow gvr in remarksGridView.Rows where (gvr.FindControl("IdCheckBox") as CheckBox).Checked select Convert.ToInt32((gvr.FindControl("idLabel") as Label).Text))
                {
                    obj.CheckList.Add(id);
                }
                return obj;
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("UI_LocAddNewLoan :: GetLoanApplication");
                LogFile.WriteLog(ex);
                throw;
            }
        }

        private bool ValidateLoanApp()
        {
            try
            {
                if (string.IsNullOrEmpty(loan_date.Value))
                {
                    //msgLabel.Text = "You Must Enter Application Date";
                    modalPopup.ShowSuccess("You Must Enter Application Date", "New Loan Application");
                    msgLabel.ForeColor = Color.Red;
                    return false;
                }
                else if (string.IsNullOrEmpty(customerNameTextBox.Text))
                {
                    //msgLabel.Text = "You Must Enter Customer name";
                    modalPopup.ShowSuccess("You Must Enter Customer name", "New Loan Application");
                    msgLabel.ForeColor = Color.Red;
                    return false;
                }
                else if (string.IsNullOrEmpty(appliedAmountTextBox.Text))
                {
                    //msgLabel.Text = "You Must Fill The Loan Amount";
                    modalPopup.ShowSuccess("You Must Fill The Loan Amount", "New Loan Application");
                    msgLabel.ForeColor = Color.Red;
                    return false;
                }
                //else if (string.IsNullOrEmpty(txtPreAcc.Text))
                //{
                //    msgLabel.Text = "You Must Enter Account Number";
                //    msgLabel.ForeColor = Color.Red;
                //    return false;
                //}
                //else if (string.IsNullOrEmpty(txtMidAcc.Text))
                //{
                //    msgLabel.Text = "You Must Enter Account Number";
                //    msgLabel.ForeColor = Color.Red;
                //    return false;
                //}
                //else if (string.IsNullOrEmpty(txtPostAcc.Text))
                //{
                //    msgLabel.Text = "You Must Enter Account Number";
                //    msgLabel.ForeColor = Color.Red;
                //    return false;
                //}
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("UI_LocAddNewLoan :: ValidateLoanApp");
                LogFile.WriteLog(ex);
                throw;
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        protected void txtPostAcc_TextChanged(object sender, EventArgs e)
        {
            string accNo = txtPreAcc.Text + "-" + txtMidAcc.Text + "-" + txtPostAcc.Text;

            if (accNo.Length != 13)
            {
                modalPopup.ShowWarning("Account No Must be of 11 characters", Title);
            }

            CustomerBulkDataManager _manager = new CustomerBulkDataManager();
            try
            {
                CustomersBulkData custData = _manager.GetCustomerDataWithAccountNo(accNo);

                if (custData != null)
                {
                    customerNameTextBox.Text = custData.FullName;
                    //do SomeThing with the Profession

                    companyNameTextBox.Text = custData.OrganisationName;
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("UI_LocAddNewLoan :: txtPostAcc_TextChanged");
                LogFile.WriteLog(ex);
            }

            customerNameTextBox.Focus();
        }
    }
}