﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using AzUtilities;
using Bat.Common;
using BLL;
using BusinessEntities;
using DAL;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI.LoanApplication
{
    public partial class UI_loc_EditLoan : System.Web.UI.Page
    {
        readonly LocatorApplicationService _service = new LocatorApplicationService();
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        private readonly DateTime learnerTableSplitTime = Convert.ToDateTime(WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"]);
        protected void Page_Load(object sender, EventArgs e)
        {

            var user = (User)Session["UserDetails"];
            if (user == null)
            {
                Response.Redirect("~/LoginUI.aspx");
            }

            if (!IsPostBack)
            {
                var a = 'a';
                HideAllActionButtons();
                int loanId = Convert.ToInt32(Request.QueryString["id"]);
                LoadLoanInformation(loanId);
                HideDivForm();
            }
        }

        private void HideDivForm()
        {
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            divCheckList.Visible = false;
            divSubmitToOnSource.Visible = false;
        }

        private void LoadLoanInformation(int id)
        {
            DateTime dateTime = Convert.ToDateTime(WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"]);
            var a = 'a';
            var loan = _service.GetLoanApplications(dateTime, id);
            if (loan == null)
            {
                return;
            }
            var appId = 0;
            decimal amount = 0;
            var accNo = "";


            var type = 0;
            var pers = 0;
            var deli_dept_id = 0;
            var deli_role_id = 0;
            var pers_id = 0;
            var deci = 0;
            var maker = 0;
            var gLoanStatusId = 0;
            var gProductId = 0;
            var gProductLoanType = "";
            decimal gAmount = 0;
            var cust = "";
            foreach (DataRow row in loan.Rows)
            {
                divSubmitToOnSource.Visible = false;
                llid.InnerText = row["LOAN_APPL_ID"].ToString();
                spProduct.InnerText = row["PROD_NAME"].ToString();
                spSource.InnerText = row["BR_NAME"].ToString();
                spARMCode.InnerText = row["ARM_Code"].ToString();
                spDept.InnerText = row["LOAN_DEPT_NAME"].ToString();
                spSubmissionDate.InnerText = Convert.ToDateTime(row["RECE_DATE"]).ToString("yyyy-MM-dd");
                spApplicationDate.InnerText = Convert.ToDateTime(row["APPL_DATE"]).ToString("yyyy-MM-dd");
                spAccNo.InnerText = row["ACC_NUMB"].ToString();
                spLoanAcc.InnerText = row["LOAN_AC_NO"].ToString();

                var accTypeId = Convert.ToInt32(row["ACC_TYPE"]);
                spAccType.InnerText = row["ACC_TYPE_NAME"].ToString();

                spSubmitBy.InnerText = row["NAME_LEARNER"].ToString();
                spAmount.InnerText = row["LOAN_APP_AMOU"].ToString();
                spConApproAmnt.InnerText = row["LOAN_COND_APRV_AMOU"].ToString();
                spApproAmnt.InnerText = row["LOAN_APRV_AMOU"].ToString();

                spCustomer.InnerText = row["CUST_NAME"].ToString();
                cust = row["CUST_NAME"].ToString();
                spOrganization.InnerText = row["ORGAN"].ToString();
                spProfession.InnerText = row["EMPL_TYPE"].ToString();
                spLoanStatus.InnerText = row["LOAN_STATUS_NAME"].ToString();
                spDecission.InnerText = row["LOAN_DECI_STATUS_Name"].ToString();



                appId = Convert.ToInt32(row["LOAN_APPL_ID"]);
                amount = Convert.ToDecimal(row["LOAN_APP_AMOU"]);
                accNo = row["ACC_NUMB"].ToString();
                //var user = (LoanLocatorUser)Session["loanUser"];

                //type = user.LOAN_DEPT_ID;
                //pers = user.ID_LEARNER;

                var user = (User)Session["UserDetails"];

                type = user.UserLevel;
                //pers = user.ID_LEARNER;

                deli_dept_id = Convert.ToInt32(row["DELI_DEPT_ID"]);
                pers_id = Convert.ToInt32(row["PERS_ID"]);
                if (!string.IsNullOrEmpty(row["MAKER"].ToString())) { maker = Convert.ToInt32(row["MAKER"]); }
                if (!string.IsNullOrEmpty(row["LOAN_DECI_STATUS_ID"].ToString())) { deci = Convert.ToInt32(row["LOAN_DECI_STATUS_ID"]); }
                gLoanStatusId = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                gProductId = Convert.ToInt32(row["PROD_ID"]);
                gProductLoanType = row["PROD_LOAN_TYPE"].ToString();
                gAmount = Convert.ToDecimal(row["LOAN_APP_AMOU"]);
            }


            #region Other Loan Details Information

            var otherInfo = _service.GetLoanRequestDetailsWithRole(Convert.ToInt32(llid.InnerText), learnerTableSplitTime);
            if (otherInfo != null)
            {
                var table =
                    "<table class='az-gridTb' style='margin-top: -1px'><thead><tr><th>Date Time</th><th>Person Name</th><th>Role/Dept Name</th><th>Process status</th><th>Remarks</th><th>Individual TAT</th></tr></thead>";
                int i = 0;
                foreach (DataRow row in otherInfo.Rows)
                {
                    if (i == 0)
                    {
                        var total = row["TAT"].ToString();
                        spTotatlTat.InnerText = string.IsNullOrEmpty(total) ? "0" : total;
                        i++;
                    }


                    table += "<tr>";
                    //spDate.InnerText = Convert.ToDateTime(row["DATE_TIME"]).ToString("yyyy-MM-dd");
                    //spPerson.InnerText = row["NAME_LEARNER"].ToString();
                    //spOtherDept.InnerText = row["LOAN_DEPT_NAME"].ToString();
                    table += "<td>" + Convert.ToDateTime(row["DATE_TIME"]) + "</td>";
                    table += "<td>" + row["NAME_LEARNER"].ToString() + "</td>";
                    table += "<td>" + row["LOAN_DEPT_NAME"].ToString() + "</td>";

                    var lastDd = 0;
                    if (Convert.ToInt32(row["PROCESS_STATUS_ID"]) == 2 && lastDd == 0)
                    {
                        lastDd = 1;
                        //spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                        //<a href=\"show_def_dec.php?id=$row->LOAN_APPL_ID&lst=2\"><font color=#0000cc>$row->LOAN_STATUS_NAME</font></a>
                    }
                    else if (Convert.ToInt32(row["PROCESS_STATUS_ID"]) == 6 && lastDd == 0)
                    {
                        lastDd = 1;
                        //spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                        //<a href=\"show_def_dec.php?id=$row->LOAN_APPL_ID&lst=6\"><font color=#0000cc>$row->LOAN_STATUS_NAME</font></a>
                    }
                    else
                    {
                        // spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                    }
                    // spRemarks.InnerText = row["REMARKS"].ToString();
                    // spTat.InnerText = row["TAT"].ToString();
                    table += "<td>" + row["REMARKS"].ToString() + "</td>";
                    table += "<td>" + row["TAT"].ToString() + "</td>";
                    table += "</tr>";
                }
                tdDetails.InnerHtml = table + "</table>";
            }

            #endregion

            #region CIB Report

            var cibStatus = _service.GetCibStatus(Convert.ToInt32(llid.InnerText));

            var status = DataTableToList.GetList<Cib>(cibStatus);
            Cib cib = null;
            if (status != null)
            {
                cib = new Cib();
                cib = status.FirstOrDefault();
            }
            var cibCurrentStatus = "";

            if (cib != null && cib.REMARKS == "Report Received")
            {
                cibRec.Checked = true;
                systemErr.Checked = false;
                underTaking.Checked = false;
                cibCurrentStatus = cib.REMARKS;
            }
            else if (cib != null && cib.REMARKS == "System Error")
            {
                cibRec.Checked = false;
                systemErr.Checked = true;
                underTaking.Checked = false;
                cibCurrentStatus = cib.REMARKS;
            }
            else if (cib != null && cib.REMARKS == "Undertaking Returned")
            {
                cibRec.Checked = false;
                systemErr.Checked = false;
                underTaking.Checked = true;
                cibCurrentStatus = cib.REMARKS;
            }
            if (cib != null && !string.IsNullOrEmpty(cib.REMARKS))
            {
                cibRemarks.InnerText = cib.REMARKS;
            }

            #endregion


            PopulateDynamicButtons(gProductId, deci, gLoanStatusId, appId, type, maker, amount, accNo, pers_id, gAmount, deli_dept_id, pers, cust, gProductLoanType);

        }

        private void PopulateDynamicButtons(
            int pProductId,
            int pDecision,
            int pLoanStatusId,
            int loanApplicationId,
            int userType,
            int maker,
            decimal amount,
            string accNo,
            int pers_id,
            decimal amou,
            int deli_dept_id,
            int pers,
            string cust, string pProdLoanType)
        {
            var loanLocatorUser = (LoanLocatorUser)Session["loanUser"];
            #region store action param

            var param = new ActionParam();
            param.CustomerName = cust;
            param.prodId = pProductId;
            param.deci = pDecision;
            param.LoanStateId = pLoanStatusId;
            param.cond = pLoanStatusId;
            param.appId = loanApplicationId;
            param.type = userType;
            param.maker = maker;
            param.amount = amount;
            param.AccountNumber = accNo;
            param.PersId = pers_id;
            param.Learner_Pers = pers;
            param.DeliDeptId = deli_dept_id;
            param.personId = pers;

            param.loanBtn = "Forward to Loan-Ops";
            param.crdBtn = "Forward to Credit";
            param.toSrcBtn = "Forward to Source";
            param.Button = "";
            param.Op = 0;
            param.oriId = userType;
            //var oriId = type;
            var oriId = 1;
            var deliDeptId = deli_dept_id;
            btnCibSubmit.Visible = oriId == 1;// oriId == 5;
            #endregion

            HideAllActionButtons();

            // if (IsPostBack) return;
            const string loanBtn = "Forward to Loan-Ops";
            const string crdBtn = "Forward to Credit";
            var loanStateId = 0;
            var jobState = 0;
            var loanUser = (User)Session["UserDetails"];
            var userLevel = loanUser.Type;
            userType = loanUser.Type;

            //if ((oriId == 1 || oriId == 2 || oriId == 3) && (cond == 8))
            #region Old Button Logic
            //if (pLoanStatusId == 8)
            //{
            //    btnOtherVc.Visible = false;//transfer_src_option.php
            //    btnUpdate.Visible = true; //admin_loan.php? //OnServerClick='OnClickUpdate(" + appId + "," + oriId + ")
            //}
            //if (oriId == deliDeptId)
            ////if (cond != 7)
            //{
            //    var loanDpt = _service.GetLoanDept(userType);
            //    userType = Convert.ToInt32(loanDpt.Rows[0]["LOAN_DEPT_TYPE"]);
            //    param.loan_status_type = userType;

            //    if (userType == 101)
            //    //&& loanUser.ID_LEARNER == pers_id)
            //    {
            //        status_update.Value = "101";
            //        btnOtherVc.Visible = false; //transfer_src_option.php
            //        if (pLoanStatusId == 13 || pLoanStatusId == 12)
            //        {
            //            if (pLoanStatusId == 13)
            //            {
            //                btnLoanOps.Visible = true; //remarks.php //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
            //                btnLoanOpsCr.Visible = true; //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")'
            //            }
            //            else
            //            {
            //                btnLoanOpsCr.Visible = true;//OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")
            //            }
            //        }
            //        else
            //        {
            //            btnLoanOpsCr.Visible = true; //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")
            //            btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
            //        }
            //        btnUpdate.Visible = true; //OnServerClick='OnClickUpdate(" + appId + "," + oriId + ")
            //    }
            //    if (userType == 201)
            //    { //credit unit


            //        status_update.Value = "201";
            //        if (pLoanStatusId == 8 || pLoanStatusId == 12 || pLoanStatusId == 9 || pLoanStatusId == 16 || pLoanStatusId == 17 || pLoanStatusId == 18)
            //        {
            //            btnOnProcess.Visible = true; // OnServerClick='OnClickOnProcess()
            //            param.Button = "CreaOnProcess";
            //        }
            //        else if (pLoanStatusId == 5)
            //        {
            //            btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
            //            btnUpdateProfile.Visible = true; //OnServerClick='OnClickUpdateProfile(' + appId + ') //edit_optional.php
            //        }
            //        else if (pLoanStatusId == 7)
            //        {

            //        }
            //        else //if (cond == 8)
            //        {

            //            var status = _service.GetLoanStatusByType(userType);
            //            foreach (DataRow row in status.Rows)
            //            {
            //                loanStateId = Convert.ToInt32(row["LOAN_STATUS_ID"]);
            //                var loanStateName = row["LOAN_STATUS_NAME"].ToString();
            //                jobState = Convert.ToInt32(row["JOB_STAT"]);
            //                //param.JobState = jobState;
            //                //param.LoanStateId = loanStateId;
            //                //param.LoanStateName = loanStateName;
            //                param.Backfrom = 1;
            //                //  }

            //                if (loanStateId == 5)
            //                {
            //                    btnOnStatus5.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
            //                    btnOnStatus5.InnerText = loanStateName;
            //                    b5.Value = loanStateId.ToString();

            //                }
            //                else if (loanStateId == 9 || ((loanStateId == 16 || loanStateId == 18) && param.prodId == 11 /* 11 = Mortgage Loan  */))
            //                {
            //                    if (loanStateName != "Verification")
            //                    {
            //                        //LogFile.WriteLine("asi line 358  :: " + loanStateId);
            //                        //btnOnStatus9_16_18_11.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
            //                        //btnOnStatus9_16_18_11.InnerText = loanStateName;
            //                        //b9161811.Value = loanStateId.ToString();

            //                        if (loanStateId == 9)
            //                        {
            //                            //LogFile.WriteLine("asi line 359  :: " + loanStateId);
            //                            btnOnStatus9.Visible = true;
            //                            btnOnStatus9.InnerText = loanStateName;
            //                            b9.Value = loanStateId.ToString();
            //                        }
            //                        if (loanStateId == 16)
            //                        {
            //                            //LogFile.WriteLine("asi line 366  :: " + loanStateId);
            //                            btnOnStatus16.Visible = true;
            //                            btnOnStatus16.InnerText = loanStateName;
            //                            b16.Value = loanStateId.ToString();
            //                        }
            //                        if (loanStateId == 18)
            //                        {
            //                            //LogFile.WriteLine("asi line 373  :: " + loanStateId);
            //                            btnOnStatus18.Visible = true;
            //                            btnOnStatus18.InnerText = loanStateName;
            //                            b18.Value = loanStateId.ToString();
            //                        }

            //                    }
            //                }
            //                else if (loanStateId == 2 || loanStateId == 6 || loanStateId == 15)//DECLINED-2 OR DEFFERED-6 OR CONDITIONALLY APPROVED		 
            //                {
            //                    if (loanStateId == 15)
            //                    {
            //                        //approver.php
            //                        //LogFile.WriteLine("asi line 386  :: " + loanStateId);
            //                        btnOnStatus_15.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
            //                        btnOnStatus_15.InnerText = loanStateName;
            //                        b15.Value = loanStateId.ToString();
            //                    }
            //                    else if (loanStateId == 6)
            //                    {
            //                        //def_dec.php
            //                        //LogFile.WriteLine("asi line 394  :: " + loanStateId);
            //                        btnOnStatus6.Visible = true;//OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
            //                        btnOnStatus6.InnerText = loanStateName;
            //                        b6.Value = loanStateId.ToString();
            //                    }
            //                    else
            //                    {
            //                        //def_dec.php
            //                        btnOnStatus2.Visible = true;//OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
            //                        btnOnStatus2.InnerText = loanStateName;
            //                        b2.Value = loanStateId.ToString();
            //                    }
            //                }
            //            }
            //            btnToSource.Visible = true;//OnServerClick='OnClickToSource(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
            //            srcJob.Value = "10";
            //            srcLst.Value = "3";
            //            srcBack.Value = "1";
            //            btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + ")
            //            btnUpdateProfile.Visible = true; //OnServerClick='OnClickUpdateProfile(" + appId + ")
            //            btnOnHold.Visible = true; //OnServerClick='OnClickOnHold(" + appId + "," + type + ")
            //        }

            //    }

            //    #region type=301

            //    if (userType == 301)
            //    {
            //        status_update.Value = "301";
            //        if (oriId == 5)
            //        {
            //            if (userLevel == 1 && maker != 1)
            //            {
            //                if (pLoanStatusId == 10 || pLoanStatusId == 17)
            //                {
            //                    LogFile.WriteLine("asi line no 431   :: " + pLoanStatusId);
            //                    btnOnProcess.Visible = true; //OnServerClick='OnClickOnProcess()
            //                    param.Button = "AssetOnProcess";
            //                }

            //                else
            //                {
            //                    btnOnHold.Visible = true; //OnServerClick='OnClickOnHold(" + appId + "," + type + ")
            //                    btnToSource.Visible = true;
            //                    //OnServerClick='OnClickToSource(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
            //                    srcJob.Value = "10";
            //                    srcLst.Value = "3";
            //                    srcBack.Value = "0";

            //                    btnLoanOpsCr.Visible = true;
            //                    //OnServerClick='OnClickToCredit(" + appId + "," + type + ")
            //                    crJob.Value = "10";
            //                    crLst.Value = "12";
            //                    crBack.Value = "0";
            //                    if (pDecision == 5) //deci_status 5 means approved..
            //                    {
            //                        btnDisbursed.Visible = true;
            //                        //OnServerClick='OnClickDisbursed(" + appId + "," + type + ")
            //                    }
            //                    btnUpdateProfile.Visible = true; //OnServerClick='OnClickDisbursed(" + appId + ")
            //                }
            //            }

            //            if (userLevel == 2 && maker == 1 && pLoanStatusId != 7)
            //            {
            //                btnLoanDisbursed.Visible = true;
            //                //OnServerClick='OnClickLoanDisbursed(" + appId + "," + type + "," + personId + ",10,4)
            //                btnToMaker.Visible = true; //OnServerClick='OnClickToMaker(" + appId + "," + type + ")
            //            }
            //        }
            //    }

            //    #endregion

            //}
            #endregion

            if (pLoanStatusId == 8)
            {
                btnOtherVc.Visible = false;//transfer_src_option.php
                btnUpdate.Visible = true; //admin_loan.php? //OnServerClick='OnClickUpdate(" + appId + "," + oriId + ")
            }
            if (oriId == deliDeptId || deliDeptId == 101 || deliDeptId == 5 || deliDeptId == 4 || deliDeptId == 2 || deliDeptId == 3)
            //if (cond != 7)
            {

                var loanDpt = _service.GetLoanUserRoleState(userType);
                if (loanDpt is null)
                {
                    return;
                }
                string[] userTypes = loanDpt.Rows[0]["LOAN_DEPT_TYPE"].ToString().Split(',');

                //userType = Convert.ToInt32(loanDpt.Rows[0]["LOAN_DEPT_TYPE"]);
                param.loan_status_type = Convert.ToInt32(userTypes[0]);

                /*
                 * UDR and Direct Sales Status Update = 101
                 * CI (Credit) = 201
                 * CI (Credit) = 301
                 */

                //Direct Sales and UDR
                //if (userType == 101)

                if ((userTypes.Contains("101") && userType == 1) || userType == 1 || userTypes.Contains("12"))
                {
                    status_update.Value = "101";
                    btnOtherVc.Visible = false; //transfer_src_option.php
                    btnUpdate.Visible = false;

                    if (userTypes[0] == "101" && pLoanStatusId == 10)
                    {
                        btnOnProcess.Visible = true; // OnServerClick='OnClickOnProcess()
                        param.Button = "CreaOnProcess";
                    }
                    else if (pLoanStatusId == 13 || pLoanStatusId == 12 || pLoanStatusId == 2)
                    {
                        if (pLoanStatusId == 13)
                        {
                            if (pLoanStatusId != 10 && pLoanStatusId != 8)
                                btnLoanOps.Visible = true;
                            //remarks.php //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
                            btnLoanOpsCr.Visible = true; //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")'
                        }
                        else
                        {
                            btnLoanOpsCr.Visible = true;//OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")
                        }
                    }
                    // Not for frontline and if loan is conditionally approved
                    else if ((userType != 9 && pLoanStatusId == 15) || pLoanStatusId == 4) 
                    {
                        btnOnProcess.Visible = true; // OnServerClick='OnClickOnProcess()
                        param.Button = "CreaOnProcess";
                    }
                    else
                    {
                        //if initiated = 1
                        if ((pLoanStatusId == 1) || pLoanStatusId == 45)
                        {
                            btnLoanOpsCr.Visible = true; //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")
                            btnOnHold.Visible = true;
                            if (pLoanStatusId != 10 && pLoanStatusId != 45)
                                btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ") 
                        }
                        if (userType != 9 && pLoanStatusId == 45)
                            btnToSource.Visible = true;
                    }
                    //if (userType != 9 || pLoanStatusId == 1 || pLoanStatusId == 13)                    
                    if (userType != 9 && (pLoanStatusId == 1 || pLoanStatusId == 13 || pLoanStatusId == 45))
                        btnUpdate.Visible = true; //OnServerClick='OnClickUpdate(" + appId + "," + oriId + ")
                }
                //if (userType == 201)
                if (userTypes.Contains("201") && pLoanStatusId != 10)
                { //credit unit


                    status_update.Value = "201";
                    btnUpdate.Visible = false;
                    //if (pLoanStatusId == 12 || pLoanStatusId == 9 || pLoanStatusId == 16 || pLoanStatusId == 17 || pLoanStatusId == 18)
                    if (deliDeptId == 4 && (pLoanStatusId == 8 || pLoanStatusId == 17))
                    {
                        btnOnProcess.Visible = true; // OnServerClick='OnClickOnProcess()
                        param.Button = "CreaOnProcess";
                    }
                    else if (pLoanStatusId == 5)
                    {
                        btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
                        btnUpdateProfile.Visible = true; //OnServerClick='OnClickUpdateProfile(' + appId + ') //edit_optional.php
                    }
                    else if (pLoanStatusId == 7 || pLoanStatusId == 15 || pLoanStatusId == 13)
                    {

                    }
                    else //if (cond == 8)
                    {

                        if (pLoanStatusId == 8 || pLoanStatusId == 4)
                        {
                            var status = _service.GetLoanStatusByType(Convert.ToInt32(userTypes[0]));
                            if (status != null)
                            {
                                foreach (DataRow row in status.Rows)
                                {
                                    loanStateId = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                                    var loanStateName = row["LOAN_STATUS_NAME"].ToString();
                                    jobState = Convert.ToInt32(row["JOB_STAT"]);
                                    //param.JobState = jobState;
                                    //param.LoanStateId = loanStateId;
                                    //param.LoanStateName = loanStateName;
                                    param.Backfrom = 1;
                                    //  }

                                    if (loanStateId == 5)
                                    {
                                        btnOnStatus5.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                        btnOnStatus5.InnerText = loanStateName;
                                        b5.Value = loanStateId.ToString();

                                    }
                                    else if (loanStateId == 9 || ((loanStateId == 16 || loanStateId == 18) && param.prodId == 11 /* 11 = Mortgage Loan  */))
                                    {
                                        if (loanStateName != "Verification")
                                        {
                                            //LogFile.WriteLine("asi line 358  :: " + loanStateId);
                                            //btnOnStatus9_16_18_11.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                            //btnOnStatus9_16_18_11.InnerText = loanStateName;
                                            //b9161811.Value = loanStateId.ToString();

                                            if (loanStateId == 9)
                                            {
                                                //LogFile.WriteLine("asi line 359  :: " + loanStateId);
                                                btnOnStatus9.Visible = true;
                                                btnOnStatus9.InnerText = loanStateName;
                                                b9.Value = loanStateId.ToString();
                                            }
                                            if (loanStateId == 16)
                                            {
                                                //LogFile.WriteLine("asi line 366  :: " + loanStateId);
                                                btnOnStatus16.Visible = true;
                                                btnOnStatus16.InnerText = loanStateName;
                                                b16.Value = loanStateId.ToString();
                                            }
                                            if (loanStateId == 18)
                                            {
                                                //LogFile.WriteLine("asi line 373  :: " + loanStateId);
                                                btnOnStatus18.Visible = true;
                                                btnOnStatus18.InnerText = loanStateName;
                                                b18.Value = loanStateId.ToString();
                                            }

                                        }
                                    }
                                    else if (loanStateId == 2 || loanStateId == 6 || loanStateId == 15)//DECLINED-2 OR DEFFERED-6 OR CONDITIONALLY APPROVED		 
                                    {
                                        if (loanStateId == 15)
                                        {
                                            //approver.php
                                            //LogFile.WriteLine("asi line 386  :: " + loanStateId);
                                            btnOnStatus_15.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                            btnOnStatus_15.InnerText = loanStateName;
                                            b15.Value = loanStateId.ToString();
                                        }
                                        else if (loanStateId == 6)
                                        {
                                            //def_dec.php
                                            //LogFile.WriteLine("asi line 394  :: " + loanStateId);
                                            btnOnStatus6.Visible = true;//OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                            btnOnStatus6.InnerText = loanStateName;
                                            b6.Value = loanStateId.ToString();
                                        }
                                        else
                                        {
                                            //def_dec.php
                                            btnOnStatus2.Visible = true;//OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                            btnOnStatus2.InnerText = loanStateName;
                                            b2.Value = loanStateId.ToString();
                                        }
                                    }
                                }

                            }
                            btnToSource.Visible = true;//OnServerClick='OnClickToSource(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                            srcJob.Value = "10";
                            srcLst.Value = "3";
                            srcBack.Value = "1";
                            btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + ")
                            btnUpdateProfile.Visible = false; //OnServerClick='OnClickUpdateProfile(" + appId + ")
                            btnOnHold.Visible = true; //OnServerClick='OnClickOnHold(" + appId + "," + type + ")
                        } 
                    }

                }

                #region type=301

                //if (userType == 301)
                if (userTypes.Contains("301"))
                {
                    status_update.Value = "301";
                    if (oriId == 5)
                    {
                        if (userLevel == 1 && maker != 1)
                        {
                            if (pLoanStatusId == 10 || pLoanStatusId == 17)
                            {
                                btnOnProcess.Visible = true; //OnServerClick='OnClickOnProcess()
                                param.Button = "AssetOnProcess";
                            }

                            else
                            {
                                btnOnHold.Visible = true; //OnServerClick='OnClickOnHold(" + appId + "," + type + ")
                                btnToSource.Visible = true;
                                //OnServerClick='OnClickToSource(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                srcJob.Value = "10";
                                srcLst.Value = "3";
                                srcBack.Value = "0";

                                btnLoanOpsCr.Visible = true;
                                //OnServerClick='OnClickToCredit(" + appId + "," + type + ")
                                crJob.Value = "10";
                                crLst.Value = "12";
                                crBack.Value = "0";
                                if (pDecision == 5) //deci_status 5 means approved..
                                {
                                    btnDisbursed.Visible = true;
                                    //OnServerClick='OnClickDisbursed(" + appId + "," + type + ")
                                }
                                btnUpdateProfile.Visible = true; //OnServerClick='OnClickDisbursed(" + appId + ")
                            }
                        }

                        if (userLevel == 2 && maker == 1 && pLoanStatusId != 7)
                        {
                            btnLoanDisbursed.Visible = true;
                            //OnServerClick='OnClickLoanDisbursed(" + appId + "," + type + "," + personId + ",10,4)
                            btnToMaker.Visible = true; //OnServerClick='OnClickToMaker(" + appId + "," + type + ")
                        }
                    }
                }

                #endregion

            }

            if (pLoanStatusId != 7)
            {
                btnCheckList.Visible = false; //loan_check_list.php
                param.Op = 1;
            }
            param.status_update = Convert.ToInt32(status_update.Value);
            Session["param"] = param;
        }

        #region Old

        private void LoadLoanInformationTest(int id)
        {
            DateTime dateTime = Convert.ToDateTime(WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"]);
            var loan = _service.GetLoanApplications(dateTime, id);

            //var sourceId = 0;
            var appId = 0;
            //var oriId = 0;
            //var deptType = 0;
            //var personId = 0;
            var amount = 0;
            // var deliDeptId = 0;
            var accNo = "";
            //var pers_id = 0;

            var type = 0;
            var pers = 0;
            var deli_dept_id = 0;
            var pers_id = 0;
            var deci = 0;
            var maker = 0;
            var cond = 0;
            var prod = 0;
            var amou = 0;



            foreach (DataRow row in loan.Rows)
            {
                llid.InnerText = row["LLId"].ToString();
                spProduct.InnerText = row["ProductName"].ToString();
                spSource.InnerText = row["SourceName"].ToString();
                spARMCode.InnerText = row["ARMCode"].ToString();
                spDept.InnerText = row["DepartmentName"].ToString();
                spSubmissionDate.InnerText = Convert.ToDateTime(row["RecieveDate"]).ToString("yyyy-MM-dd");
                spApplicationDate.InnerText = Convert.ToDateTime(row["AppliedDate"]).ToString("yyyy-MM-dd");
                spAccNo.InnerText = row["AccNo"].ToString();
                spLoanAcc.InnerText = row["LoanAccNo"].ToString();

                var accTypeId = Convert.ToInt32(row["AccTypeId"]);
                switch (accTypeId)
                {
                    case 1:
                        spAccType.InnerText = "CEP";
                        break;
                    case 2:
                        spAccType.InnerText = "Priority";
                        break;
                    case 3:
                        spAccType.InnerText = "CEP & Priority";
                        break;
                    default:
                        spAccType.InnerText = "";
                        break;
                }


                spSubmitBy.InnerText = row["SubmitedBy"].ToString();
                spAmount.InnerText = row["AppliedAmount"].ToString();
                spConApproAmnt.InnerText = row["ApprovedAmount"].ToString();
                spCustomer.InnerText = row["ApplicantName"].ToString();
                spOrganization.InnerText = row["Organization"].ToString();
                spProfession.InnerText = row["Profession"].ToString();
                spLoanStatus.InnerText = row["StateName"].ToString();
                spDecission.InnerText = row["DecisionStatus"].ToString();
                //spLoanAcc.InnerText = row["LoanAccNo"].ToString();
                //spLoanAcc.InnerText = row["LoanAccNo"].ToString();
                if (!string.IsNullOrEmpty(row["Maker"].ToString()))
                {
                    maker = Convert.ToInt32(row["Maker"]);
                }
                //prodId = Convert.ToInt32(row["ProductId"]);
                //  if (!string.IsNullOrEmpty(row["DecisionStatusId"].ToString())) { deci = Convert.ToInt32(row["DecisionStatusId"]); }
                // sourceId = Convert.ToInt32(row["SourceId"]);
                //cond = Convert.ToInt32(row["LoanStatusId"]);
                // appId = Convert.ToInt32(row["LLId"]);
                // deliDeptId = Convert.ToInt32(row["DeliDeptId"]);
                var user = (LoanLocatorUser)Session["loanUser"];
                type = user.LOAN_DEPT_ID;
                //oriId = user.LOAN_DEPT_ID;
                // deptType = Convert.ToInt32(row["DeptType"]);
                pers = user.ID_LEARNER; //Convert.ToInt32(row["PersonId"]);
                //   amount = Convert.ToInt32(row["AppliedAmount"]);
                accNo = row["AccNo"].ToString();
                // pers_id = Convert.ToInt32(row["PERS_ID"]);
                // if (!string.IsNullOrEmpty(row["Maker"].ToString()))
                // { maker = Convert.ToInt32(row["Maker"]); }

                deli_dept_id = Convert.ToInt32(row["DELI_DEPT_ID"]);
                pers_id = Convert.ToInt32(row["PERS_ID"]);
                deci = Convert.ToInt32(row["LOAN_DECI_STATUS_ID"]);
                maker = Convert.ToInt32(row["MAKER"]);
                cond = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                prod = Convert.ToInt32(row["PROD_ID"]);
                amou = Convert.ToInt32(row["LOAN_APP_AMOU"]);

            }

            //Other Loan Details Information
            var otherInfo = _service.GetLoanRequestDetails(Convert.ToInt32(llid.InnerText));
            if (otherInfo != null)
            {
                var table =
                    "<table class='az-gridTb' style='margin-top: -1px'><thead><tr><th>Date Time</th><th>Person Name</th><th>Dept Name</th><th>Process status</th><th>Remarks</th><th>Individual TAT</th></tr></thead>";
                int i = 0;
                foreach (DataRow row in otherInfo.Rows)
                {
                    if (i == 0)
                    {
                        var total = row["TAT"].ToString();
                        spTotatlTat.InnerText = string.IsNullOrEmpty(total) ? "0" : total;
                        i++;
                    }


                    table += "<tr>";
                    //spDate.InnerText = Convert.ToDateTime(row["DATE_TIME"]).ToString("yyyy-MM-dd");
                    //spPerson.InnerText = row["NAME_LEARNER"].ToString();
                    //spOtherDept.InnerText = row["LOAN_DEPT_NAME"].ToString();
                    table += "<td>" + Convert.ToDateTime(row["DATE_TIME"]).ToString("yyyy-MM-dd") + "</td>";
                    table += "<td>" + row["NAME_LEARNER"].ToString() + "</td>";
                    table += "<td>" + row["LOAN_DEPT_NAME"].ToString() + "</td>";

                    var lastDd = 0;
                    if (Convert.ToInt32(row["PROCESS_STATUS_ID"]) == 2 && lastDd == 0)
                    {
                        lastDd = 1;
                        //spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                        //<a href=\"show_def_dec.php?id=$row->LOAN_APPL_ID&lst=2\"><font color=#0000cc>$row->LOAN_STATUS_NAME</font></a>
                    }
                    else if (Convert.ToInt32(row["PROCESS_STATUS_ID"]) == 6 && lastDd == 0)
                    {
                        lastDd = 1;
                        //spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                        //<a href=\"show_def_dec.php?id=$row->LOAN_APPL_ID&lst=6\"><font color=#0000cc>$row->LOAN_STATUS_NAME</font></a>
                    }
                    else
                    {
                        // spProcess.InnerText = row["LOAN_STATUS_NAME"].ToString();
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                    }
                    // spRemarks.InnerText = row["REMARKS"].ToString();
                    // spTat.InnerText = row["TAT"].ToString();
                    table += "<td>" + row["REMARKS"].ToString() + "</td>";
                    table += "<td>" + row["TAT"].ToString() + "</td>";
                    table += "</tr>";
                }
                tdDetails.InnerHtml = table + "</table>";
            }

            //// CIB Report 
            var cibStatus = _service.GetCibStatus(Convert.ToInt32(llid.InnerText));

            var status = DataTableToList.GetList<Cib>(cibStatus);
            Cib cib = null;
            if (status != null)
            {
                cib = new Cib();
                cib = status.FirstOrDefault();
            }
            var cibCurrentStatus = "";

            if (cib != null && cib.REMARKS == "Report Received")
            {
                cibRec.Checked = true;
                systemErr.Checked = false;
                underTaking.Checked = false;
                cibCurrentStatus = cib.REMARKS;
            }
            else if (cib != null && cib.REMARKS == "System Error")
            {
                cibRec.Checked = false;
                systemErr.Checked = true;
                underTaking.Checked = false;
                cibCurrentStatus = cib.REMARKS;
            }
            else if (cib != null && cib.REMARKS == "Undertaking Returned")
            {
                cibRec.Checked = false;
                systemErr.Checked = false;
                underTaking.Checked = true;
                cibCurrentStatus = cib.REMARKS;
            }

            //   PopulateDynamicButtons(prodId, deci, sourceId, cond, appId, oriId, type, deptType, personId, maker, amount, accNo, deliDeptId, pers_id);

        }

        private void PopulateDynamicButtons(int prodId, int deci, int sourceId, int cond, int appId, int oriId, int type,
            int deptType, int personId, int maker, int amount, string accNo, int deliDeptId, int pers_id)
        {
            #region store action param

            var param = new ActionParam();
            param.prodId = prodId;
            param.deci = deci;
            param.sourceId = sourceId;
            param.cond = cond;
            param.appId = appId;
            param.oriId = oriId;
            param.type = type;
            param.deptType = deptType;
            param.personId = personId;
            param.maker = maker;
            param.amount = amount;
            param.loanBtn = "Forward to Loan-Ops";
            param.crdBtn = "Forward to Credit";
            param.AccountNumber = accNo;
            param.DeliDeptId = deliDeptId;
            param.PersId = pers_id;
            param.Op = 0;

            #endregion



            if (IsPostBack) return;
            const string loanBtn = "Forward to Loan-Ops";
            const string crdBtn = "Forward to Credit";
            var loanStateId = 0;
            var jobState = 0;
            var loanUser = (LoanLocatorUser)Session["loanUser"];
            var userLevel = loanUser.ST_USER_LEVEL;

            if ((oriId == 1 || oriId == 2 || oriId == 3) && (cond == 8))
            {
                //transfer_src_option.php
                btnOtherVc.Visible = false;
                //admin_loan.php? //OnServerClick='OnClickUpdate(" + appId + "," + oriId + ")
                btnUpdate.Visible = true;
            }
            if (oriId == deliDeptId)
            {
                var loanDpt = _service.GetLoanDept(type);
                type = Convert.ToInt32(loanDpt.Rows[0]["LOAN_DEPT_TYPE"]);
                // transfer to other sources.
                if (type == 101 && personId == pers_id)
                {
                    status_update.Value = "101";
                    //transfer_src_option.php
                    btnOtherVc.Visible = false;
                    if (cond == 13 || cond == 12)
                    {
                        if (cond == 13)
                        {
                            //remarks.php //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
                            btnLoanOps.Visible = true;

                            btnLoanOpsCr.Visible = true;
                            //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")'
                            //btnLoanOpsCr.InnerText = crdBtn;
                        }
                        else
                        {
                            btnLoanOpsCr.Visible = true;
                            //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")
                            //btnLoanOpsCr.InnerText = crdBtn;
                        }
                    }
                    else
                    {
                        btnLoanOpsCr.Visible = true;
                        //OnServerClick='OnClickToCredit(" + appId + "," + type + "," + crdBtn + ")
                        //btnLoanOpsCr.InnerText = crdBtn;

                        btnLoanOps.Visible = true;
                        //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
                    }
                    btnUpdate.Visible = true; //OnServerClick='OnClickUpdate(" + appId + "," + oriId + ")
                }
                if (type == 201)
                {
                    //credit unit
                    status_update.Value = "201";
                    if (cond == 8 || cond == 12 || cond == 9 || cond == 16 || cond == 17 || cond == 18)
                    {
                        btnOnProcess.Visible = true; // OnServerClick='OnClickOnProcess()
                    }
                    else if (cond == 5)
                    {
                        btnLoanOps.Visible = true;
                        //OnServerClick='OnClickLoanOps(" + appId + "," + type + "," + loanBtn + ")
                        btnUpdateProfile.Visible = true;
                        //OnServerClick='OnClickUpdateProfile(' + appId + ') //edit_optional.php
                    }
                    else if (cond == 7)
                    {
                    }
                    else
                    {
                        // cond=8
                        var status = _service.GetLoanStatusByType(type);

                        //  for (var i = 0; i < status.length; i++) {
                        foreach (DataRow row in status.Rows)
                        {
                            loanStateId = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                            var loanStateName = row["LOAN_STATUS_NAME"].ToString();
                            jobState = Convert.ToInt32(row["JOB_STAT"]);
                            param.JobState = jobState;
                            param.LoanStateId = loanStateId;
                            param.LoanStateName = loanStateName;
                            param.Backfrom = 1;
                            //  }

                            if (loanStateId == 5)
                            {
                                //btnOnStatus.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                btnOnStatus5.Visible = true;
                                btnOnStatus5.InnerText = loanStateName;

                            }
                            else if (loanStateId == 9 || ((loanStateId == 16 || loanStateId == 18) && prodId == 11 /* 11 = Mortgage Loan  */))
                            {
                                if (loanStateName != "Verification")
                                {
                                    // btnOnStatus9_16_18_11.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                    // btnOnStatus9_16_18_11.InnerText = loanStateName;
                                }
                            }
                            else if (loanStateId == 2 || loanStateId == 6 || loanStateId == 15) //DECLINED-2 OR DEFFERED-6 OR CONDITIONALLY APPROVED		 
                            {
                                if (loanStateId == 15)
                                {
                                    //approver.php
                                    btnOnStatus_15.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                    btnOnStatus_15.InnerText = loanStateName;
                                }
                                else
                                {
                                    //def_dec.php
                                    btnOnStatus6.Visible = true; //OnServerClick='OnClickOnStatus(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                    btnOnStatus6.InnerText = loanStateName;
                                }
                            }
                        }
                    }
                    //assetstatus.php
                    btnToSource.Visible = true;
                    //OnServerClick='OnClickToSource(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                    //remarks.php
                    btnLoanOps.Visible = true; //OnServerClick='OnClickLoanOps(" + appId + "," + type + ")

                    //edit_optional.php
                    btnUpdateProfile.Visible = true; //OnServerClick='OnClickUpdateProfile(" + appId + ")
                    //remarks.php
                    btnOnHold.Visible = true; //OnServerClick='OnClickOnHold(" + appId + "," + type + ")

                }
                if (type == 301)
                {
                    status_update.Value = "301";
                    if (oriId == 5)
                    {
                        if (userLevel == 1 && maker != 1)
                        {
                            if (cond == 10 || cond == 17)
                            {
                                btnOnProcess.Visible = true; //OnServerClick='OnClickOnProcess()
                            }

                            else
                            {
                                //echo'<input name=loanid type=hidden value=$id>';
                                btnOnHold.Visible = true; //OnServerClick='OnClickOnHold(" + appId + "," + type + ")
                                btnToSource.Visible = true;
                                //OnServerClick='OnClickToSource(" + appId + "," + type + "," + personId + "," + jobState + "," + loanStateId + "," + prodId + "," + amount + ")
                                btnLoanOpsCr.Visible = true;
                                //OnServerClick='OnClickToCredit(" + appId + "," + type + ")
                                //btnLoanOpsCr.InnerText = crdBtn;
                                if (deci == 5) //deci_status 5 means approved..
                                {
                                    //remarks.php
                                    btnDisbursed.Visible = true;
                                    //OnServerClick='OnClickDisbursed(" + appId + "," + type + ")
                                }
                                //update_disburseinfo.php
                                btnUpdateProfile.Visible = true; //OnServerClick='OnClickDisbursed(" + appId + ")
                            }
                        }

                        if (userLevel == 2 && maker == 1 && cond != 7)
                        {
                            //approve.php
                            btnLoanDisbursed.Visible = true;
                            //OnServerClick='OnClickLoanDisbursed(" + appId + "," + type + "," + personId + ",10,4)
                            //remarks.php
                            btnToMaker.Visible = true; //OnServerClick='OnClickToMaker(" + appId + "," + type + ")
                        }
                    }
                }
            }

            if (cond != 7)
            {
                btnCheckList.Visible = false; //loan_check_list.php
                param.Op = 1;
            }
            param.status_update = Convert.ToInt32(status_update.Value);
            Session["param"] = param;
        }

        #endregion

        private void HideAllActionButtons()
        {
            btnOtherVc.Visible = false;
            btnUpdate.Visible = false;
            btnLoanOps.Visible = false;
            btnLoanOpsCr.Visible = false;
            btnOnProcess.Visible = false;
            btnUpdateProfile.Visible = false;
            //btnOnStatus.Visible = false;
            btnToSource.Visible = false;
            btnOnHold.Visible = false;
            btnDisbursed.Visible = false;
            btnLoanDisbursed.Visible = false;
            btnToMaker.Visible = false;
            btnCheckList.Visible = false;

            btnOnStatus5.Visible = false;

            btnOnStatus9.Visible = false;
            btnOnStatus16.Visible = false;
            btnOnStatus18.Visible = false;

            btnOnStatus_15.Visible = false;
            btnOnStatus6.Visible = false;
            btnOnStatus2.Visible = false;
        }

        #region Other VC

        protected void OnClickOtherVc(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];

            otherVcAppId.InnerText = param.appId.ToString();

            divViewApplication.Visible = false;
            toOtherVc.Visible = true;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;

        }

        public void OnClickOtherVcOk(object sender, EventArgs e)
        {
            var user = otherUserNumber.Text;
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            var res = _service.SaveOtherVc(param, user);
            //Response.Write("<script>alert('" + res + "')</script>");
            CloseOtherVc(new object(), new EventArgs());
            new AT(this).AuditAndTraial("Other VC", "Other VC");
            LoadLoanInformation(param.appId);

        }

        public void CloseOtherVc(object sender, EventArgs e)
        {
            divViewApplication.Visible = true;
            toOtherVc.Visible = false;
        }

        #endregion

        #region Update Loan
        protected void OnClickUpdate(object sender, EventArgs e)
        {
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = true;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            ProductDropDownListItem();
            SourceDropDownListItem();
            ProfessionDropDownListItems();
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];

            LoadApplication(param);
        }
        private void LoadApplication(ActionParam param)
        {
            var loan = _service.GetLoanApplication(param.appId);
            //var obj = DataTableToList.GetList<LoanInformation>(loan).FirstOrDefault();

            adProduct.SelectedValue = loan.Rows[0]["ProductId"].ToString();
            adApplicationDate.Value = Convert.ToDateTime(loan.Rows[0]["AppliedDate"]).ToString("yyyy - MM - dd");
            adSource.SelectedValue = loan.Rows[0]["SourceId"].ToString();
            if (loan.Rows[0]["AccTypeId"] != DBNull.Value)
            {
                switch (Convert.ToInt32(loan.Rows[0]["AccTypeId"]))
                {
                    case 1:
                        chkAdCpf.Checked = true;
                        chkAdPriority.Checked = false;
                        break;
                    case 2:
                        chkAdPriority.Checked = true;
                        chkAdCpf.Checked = false;
                        break;
                    case 3:
                        chkAdCpf.Checked = true;
                        chkAdPriority.Checked = true;
                        break;
                    default: spAccType.InnerText = ""; break;
                }
            }

            if (loan.Rows[0]["AccNo"] != DBNull.Value)
            {
                var acc = loan.Rows[0]["AccNo"].ToString().Split('-');
                adAccPre.Value = acc[0];
                adAccMid.Value = acc[1];
                adAccLast.Value = acc[2];
            }

            txtAdCust.Value = loan.Rows[0]["ApplicantName"].ToString();
            txtAdAmount.Value = loan.Rows[0]["AppliedAmount"].ToString();

            if (loan.Rows[0]["ProfessionId"] != DBNull.Value) { adProfession.SelectedValue = loan.Rows[0]["ProfessionId"].ToString(); }
            if (loan.Rows[0]["Organization"] != DBNull.Value) { txtAdOrganization.Value = loan.Rows[0]["Organization"].ToString(); }
            if (loan.Rows[0]["PreDeclined"] != DBNull.Value) { adDeclined.Value = loan.Rows[0]["PreDeclined"].ToString(); }
            if (loan.Rows[0]["ARMCode"] != DBNull.Value) { txtARMCodeUpdate.Value = loan.Rows[0]["ARMCode"].ToString(); }

        }
        private void ProfessionDropDownListItems()
        {
            List<Profession> professionListObj = _loanApplicationManagerObj.GetAllProfession();
            Profession professionObj = new Profession();
            professionObj.Id = 0;
            professionObj.Name = "";
            professionListObj.Insert(0, professionObj);
            adProfession.DataSource = professionListObj;
            adProfession.DataTextField = "Name";
            adProfession.DataValueField = "Id";
            adProfession.DataBind();
        }
        private void SourceDropDownListItem()
        {
            List<Source> sourceLisObj = _loanApplicationManagerObj.LocGetAllSource();
            Source sourceObj = new Source();
            sourceObj.SourceId = 0;
            sourceObj.SourceName = "";
            sourceLisObj.Insert(0, sourceObj);
            adSource.DataTextField = "SourceName";
            adSource.DataValueField = "SourceId";
            adSource.DataSource = sourceLisObj;
            adSource.DataBind();
        }
        private void ProductDropDownListItem()
        {
            List<Product> productListObj = _loanApplicationManagerObj.GetAllProduct();
            var productObj = new Product { ProductId = 0, ProductName = "" };
            productListObj.Insert(0, productObj);
            adProduct.DataTextField = "ProductName";
            adProduct.DataValueField = "ProductId";
            adProduct.DataSource = productListObj;
            adProduct.DataBind();
        }
        public void CloseUpdate(object sender, EventArgs e)
        {
            divViewApplication.Visible = true;
            divUpdate.Visible = false;

        }
        public void UpdateLoanInfo(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            var loan = GetLoanObj();
            var selectedLoan = _service.GetLoanApplication(param.appId);
            var obj = new LoanInformation();//DataTableToList.GetList<>(selectedLoan).FirstOrDefault();
            obj.LLId = param.appId;
            obj.PersonId = param.PersId;
            obj.DeliDeptId = param.DeliDeptId;
            obj.LoanStatusId = param.LoanStateId;
            var res = _service.UpdateLoanInfo(loan, obj);
            new AT(this).AuditAndTraial("Update Loan", "Update Loan");
            LoadLoanInformation(param.appId);
            if (res == "1")
            {
                //Response.Write("<script>alert('Loan Updated Successfully')</script>");
                //LoadLoanInformation(param.appId);
                CloseUpdate(new object(), new EventArgs());
            }
        }
        private Loan GetLoanObj()
        {
            try
            {
                var obj = new Loan();
                obj.SubmissionDate = DateTime.Now;
                obj.ProductId = Convert.ToInt32(adProduct.SelectedValue);
                obj.PreAccNo = adAccPre.Value;
                obj.MidAccNo = adAccMid.Value;
                obj.PostAccNo = adAccLast.Value;
                obj.CustomerName = txtAdCust.Value;
                obj.AppliedAmount = Convert.ToInt32(txtAdAmount.Value);
                obj.SubmitedBy = Convert.ToInt32(Session["Id"].ToString());
                obj.EntryBranch = Convert.ToInt32(adSource.SelectedValue);
                obj.PreDeclined = adDeclined.Value;
                obj.ARMCode = txtARMCodeUpdate.Value;
                obj.ApplicationDate = Convert.ToDateTime(adApplicationDate.Value); ;
                obj.Cep = chkAdCpf.Checked;
                obj.Prio = chkAdPriority.Checked;
                if (obj.Cep && obj.Prio) { obj.AccType = 3; }
                else if (obj.Prio) { obj.AccType = 2; }
                else if (obj.Cep) { obj.AccType = 1; }
                else { obj.AccType = 4; }
                obj.EmployeeType = adProfession.SelectedItem.Text;
                obj.Organization = txtAdOrganization.Value;
                return obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Loan Ops

        protected void OnClickLoanOps(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];

            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = true;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            btnLoanOpsRe.Value = param.loanBtn;
        }



        public void OnClickLoanOpsPost(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];

            var remarks = lopsRemarks.Value;
            param.Button = "ForwLoanDocs";
            var res = _service.UpdateLoanOps(param, remarks, false, "ForwLoanDocs");
            //UpdateStatusOption(param);
            new AT(this).AuditAndTraial("Loan Ops", "Loan Ops");
            LoadLoanInformation(param.appId);
            if (res == "1")
            {
                //Response.Write("<script>alert('Loan Updated Successfully')</script>");
                OnClickLoanOpsClose(new object(), new EventArgs());
            }
            else if (res == "0")
            {
                Response.Write("<script>alert('Loan not update, Try again!')</script>");
            }
            else
            {
                Response.Write("<script>alert('" + res + "')</script>");
            }
        }

        public void OnClickLoanOpsClose(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = true;
            divLoanOps.Visible = false;
            LoadLoanInformation(param.appId);
        }

        protected void OnClickToSourceClose(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = true;
            divLoanOps.Visible = false;
            LoadLoanInformation(param.appId);
        }
        #endregion


        #region Loan Credit

        protected void OnClickToCredit(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = true;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            btnToCrSave.Value = param.crdBtn;
            if (!string.IsNullOrEmpty(crJob.Value))
            {
                param.job = Convert.ToInt32(crJob.Value);
            }
            if (!string.IsNullOrEmpty(crLst.Value))
            {
                param.lst = Convert.ToInt32(crLst.Value);
            }
            if (!string.IsNullOrEmpty(crBack.Value))
            {
                param.backfrom = Convert.ToInt32(crBack.Value);
            }

        }

        public void OnClickToCreditPost(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            var remarks = toCrRemarks.Value;
            param.Button = "ForwCrea";
            var res = "";
            if (param.lst == 0)
            {
                res = _service.UpdateLoanOps(param, remarks, false, "ForwCrea");
            }
            else
            {
                res = _service.SubmitAssetStatus(param, remarks);
            }
            new AT(this).AuditAndTraial("ToCredit", "ToCredit");
            //UpdateStatusOption(param);
            OnClickToCreditClose(new object(), new EventArgs());
        }

        public void OnClickToCreditClose(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = true;
            divToCredit.Visible = false;
            LoadLoanInformation(param.appId);

        }

        #endregion


        protected void OnClickOnProcess(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = true;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            UpdateStatusOption(param);

            divViewApplication.Visible = true;
            divOnProcess.Visible = false;
            LoadLoanInformation(param.appId);
        }

        private void UpdateStatusOption(ActionParam param)
        {
            var user = (User)Session["UserDetails"];

            var dt = DateTime.Now;
            var ldep =user.Type == 1 ? 5 : 0;
            var pers = user.Id; //param.Learner_Pers;
            var remarks = string.Empty;
            var pro_status = 0;
            var deli_dept = 0;
            var DATE_TIME = new LocatorApplicationService().GetStartTime(param.appId);
            var tat = (dt - DATE_TIME).Days;
            var inSql = string.Empty;
            var upSql = string.Empty;

            param.oriId = user.Type;

            if (param.status_update == 101)  //Update Req from Source Unit
            {
                if (param.Button.Equals("ForwLoanDocs") || param.Button.Equals("ForwCrea") || param.Button.Equals("CreaOnProcess"))
                {
                    if (param.Button.Equals("ForwLoanDocs"))
                    {
                        pro_status = 10;
                        deli_dept = 5;
                    }
                    else if (param.Button.Equals("CreaOnProcess"))
                    {
                        pro_status = 45;
                        deli_dept = 5;
                    }
                    else
                    {
                        pro_status = 8; deli_dept = 4;
                    }
                    inSql = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT,MakerId) values({0},{1},{2},'{3}',{4},'{5}',{6},'{7}')",
                        param.appId, pers, ldep, dt, pro_status, remarks, tat, param.makerID);


                    if (param.Button.Equals("ForwCrea"))
                        upSql = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1} where LOAN_APPL_ID={2}", pro_status, deli_dept, param.appId);
                    else
                        upSql = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, MAKER=0 where LOAN_APPL_ID={2}", pro_status, deli_dept, param.appId);
                }
            }




            else if (param.status_update == 201)  //Update Req from Creadit Unit
            {
                if (param.Button.Equals("ForwLoanDocs") || param.Button.Equals("CreaOnProcess") || param.Button.Equals("On_Hold"))
                {
                    if (param.Button.Equals("ForwLoanDocs"))
                    {
                        //TAT function call
                        pro_status = 10; deli_dept = 5;

                    }
                    else if (param.Button.Equals("On_Hold"))
                    {
                        pro_status = 17; deli_dept = 4;
                    }

                    else
                    {
                        //TAT function call
                        pro_status = 4; deli_dept = 4;

                    }
                    inSql = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT, MakerId) values
					  ({0},{1},{2},'{3}',{4},'{5}',{6},'{7}')", param.appId, pers, ldep, dt, pro_status, remarks, tat, param.makerID);


                    if (param.Button.Equals("CreaOnProcess"))
                        upSql = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1} where LOAN_APPL_ID={2}", pro_status, deli_dept, param.appId);
                    else
                    {
                        var op_tat_credit = new LocatorApplicationService().GetOperationalTat(param.appId, 0);//operational_tat($id, $type=0);
                        upSql = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, MAKER=0, OP_TAT_CREDIT={2} where LOAN_APPL_ID={3}",
                            pro_status, deli_dept, op_tat_credit, param.appId);

                    }

                }
            }
            else if (param.status_update == 301)  //Update Req from Creadit Unit
            {

                if (param.Button.Equals("AssetReadyDis") || param.Button.Equals("AssetOnProcess") || param.Button.Equals("AssetRetCheck") || param.Button.Equals("QuickDis") || param.Button.Equals("On_Hold"))
                {
                    if (param.Button.Equals("AssetReadyDis"))//if($HTTP_POST_VARS['QuickDis'])
                    {
                        pro_status = 7;
                    }
                    else if (param.Button.Equals("On_Hold"))
                    {
                        pro_status = 17;
                    }
                    else
                    {
                        pro_status = 4;
                    }
                    deli_dept = 5;

                    inSql = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT, MakerId) values
					  ({0},{1},{2},'{3}',{4},'{5}',{6})", param.appId, pers, ldep, dt, pro_status, remarks, tat);


                    if (param.Button.Equals("AssetReadyDis"))

                        upSql = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, MAKER=1 where LOAN_APPL_ID={2}", pro_status, deli_dept, param.appId);

                    else if (param.Button.Equals("QuickDis"))
                    {

                        var op_tat_asset = new LocatorApplicationService().GetOperationalTat(param.appId, 1);
                        upSql = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID=7,LOAN_DISB_DATE='{0}',OP_TAT_ASSET={1}, DELI_DEPT_ID= 5 where LOAN_APPL_ID={2}", dt, op_tat_asset, param.appId);

                    }
                    else

                    { upSql = string.Format(@"update loc_loan_app_info set LOAN_STATUS_ID={0}, DELI_DEPT_ID={1}, MAKER=0 where LOAN_APPL_ID={2}", pro_status, deli_dept, param.appId); }


                }
            }

            //if (param.Button.Equals("cib_status") && param.oriId == 5)
            if (param.Button.Equals("cib_status") && param.oriId == 1)
            {
                pro_status = 19;
                remarks = "";
                var rec = cibRec.Checked;
                var syserr = systemErr.Checked;
                var under = underTaking.Checked;

                if (rec) { remarks = "Report Received"; }
                else if (syserr) { remarks = "System Error"; }
                else if (under) { remarks = "Undertaking Returned"; }

                if (!string.IsNullOrEmpty(remarks))
                {
                    inSql = string.Format(@"insert into loan_app_details(LOAN_APPL_ID,PERS_ID,DEPT_ID,DATE_TIME,PROCESS_STATUS_ID,REMARKS,TAT) values
                                      ({0},'{1}','{2}','{3}',{4},'{5}',{6})", param.appId, pers, ldep, dt, pro_status, remarks, tat);
                }
            }

            var res = _service.UpdateStatusOption(inSql, upSql);
            Response.Write("<script>alert(" + res + ")</script>");
            new AT(this).AuditAndTraial("OnProcess", "OnProcess");
            LoadLoanInformation(param.appId);
        }

        #region Update Personal Profile

        protected void OnClickUpdateProfile(object sender, EventArgs e)
        {
            var errror = 0;
            try
            {
                var param = (ActionParam)Session["param"];
                param.makerID = (string)Session["UserId"];

                divViewApplication.Visible = false;
                toOtherVc.Visible = false;
                divUpdate.Visible = false;
                divLoanOps.Visible = false;
                divToCredit.Visible = false;
                divOnProcess.Visible = false;
                divUpdateProfile.Visible = true;
                divOnStatus.Visible = false;
                divToSource.Visible = false;
                divOnHold.Visible = false;
                divisbursed.Visible = false;
                divLoanDisbursed.Visible = false;
                divToMaker.Visible = false;
                divSubmitToOnSource.Visible = false;
                var loan = _service.GetLoanApplication(param.appId);
                errror = 1;
                //var obj = DataTableToList.GetList<LoanInformation>(loan).FirstOrDefault();
                errror = 2;
                errror = 3;

                foreach (DataRow row in loan.Rows)
                {
                    cust.InnerText = " of " + row["ApplicantName"].ToString() + "( Loan ID:" + row["LLId"].ToString() + " )";
                    if (!string.IsNullOrEmpty(row["Owner"].ToString())) { owner.Value = row["Owner"].ToString(); }
                    if (!string.IsNullOrEmpty(row["MotherName"].ToString())) { owner.Value = row["MotherName"].ToString(); }
                    if (!string.IsNullOrEmpty(row["Tenor"].ToString())) { owner.Value = row["Tenor"].ToString(); }
                    if (!string.IsNullOrEmpty(row["UserDefine"].ToString())) { owner.Value = row["UserDefine"].ToString(); }
                    if (!string.IsNullOrEmpty(row["UserDefine2"].ToString())) { owner.Value = row["UserDefine2"].ToString(); }
                    if (!string.IsNullOrEmpty(row["UserDefine3"].ToString())) { owner.Value = row["UserDefine3"].ToString(); }
                    if (!string.IsNullOrEmpty(row["UserDefine4"].ToString())) { owner.Value = row["UserDefine4"].ToString(); }
                }

                //owner.Value = obj.Owner;
                //mother.Value = obj.MotherName;
                //tenor.Value = obj.Tenor.ToString();
                //ud.SelectedValue = obj.UserDefine;
                //ud2.Value = obj.UserDefine2;
                //ud3.Value = obj.UserDefine3;
                //ud4.Value = obj.UserDefine4;
            }
            catch (Exception exception)
            {
                throw new Exception(errror.ToString());
            }
        }

        public void UpdateProfile(object o, EventArgs e)
        {
            var res = "";
            try
            {
                var param = (ActionParam)Session["param"];
                param.makerID = (string)Session["UserId"];
                var own = owner.Value;
                var motherName = mother.Value;
                var teno = tenor.Value;
                var userdef = ud.SelectedValue;
                var userDef2 = ud2.Value;
                var userDef3 = ud3.Value;
                var userDef4 = ud4.Value;
                res = _service.UpdateProfile(param, own, motherName, teno, userdef, userDef2, userDef3, userDef4);
                CloseProfile(new object(), new EventArgs());
                new AT(this).AuditAndTraial("UpdateLoanProfile", "UpdateLoanProfile");
                LoadLoanInformation(param.appId);
            }
            catch (Exception exception)
            {
                res = "Optional  Status not Updated";
            }
            //Response.Write("<script>alert('" + res + "')</script>");

        }

        public void CloseProfile(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = true;
            divUpdateProfile.Visible = false;
            LoadLoanInformation(param.appId);
        }

        #endregion

        #region On Status

        private void ShowStatusDiv()
        {
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = true;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
        }
        protected void OnClickOnStatus5(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            ShowStatusDiv();
            var status = _service.GetLoanStatusByType(param.loan_status_type);
            var ls = Convert.ToInt32(b5.Value);
            var lst = 0;
            var loanStateName = "";
            var job = 0;
            foreach (DataRow row in status.Rows)
            {
                if (ls == Convert.ToInt32(row["LOAN_STATUS_ID"]))
                {
                    lst = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                    loanStateName = row["LOAN_STATUS_NAME"].ToString();
                    job = Convert.ToInt32(row["JOB_STAT"]);
                }

            }
            PopulateView(param, lst, loanStateName, job);

        }
        //protected void OnClickOnStatus9_16_18_11(object sender, EventArgs e)
        //{
        //    var param = (ActionParam)Session["param"];
        //    ShowStatusDiv();
        //    var status = _service.GetLoanStatusByType(param.loan_status_type);
        //    var ls = Convert.ToInt32(b9161811.Value);
        //    var lst = 0;
        //    var loanStateName = "";
        //    var job = 0;
        //    foreach (DataRow row in status.Rows)
        //    {
        //        if (ls == Convert.ToInt32(row["LOAN_STATUS_ID"]))
        //        {
        //            lst = Convert.ToInt32(row["LOAN_STATUS_ID"]);
        //            loanStateName = row["LOAN_STATUS_NAME"].ToString();
        //            job = Convert.ToInt32(row["JOB_STAT"]);
        //        }

        //    }
        //    PopulateView(param, lst, loanStateName, job);
        //}

        protected void OnClickOnStatus9(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            ShowStatusDiv();
            var status = _service.GetLoanStatusByType(param.loan_status_type);
            var ls = Convert.ToInt32(b9.Value);
            var lst = 0;
            var loanStateName = "";
            var job = 0;
            foreach (DataRow row in status.Rows)
            {
                if (ls == Convert.ToInt32(row["LOAN_STATUS_ID"]))
                {
                    lst = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                    loanStateName = row["LOAN_STATUS_NAME"].ToString();
                    job = Convert.ToInt32(row["JOB_STAT"]);
                }

            }
            PopulateView(param, lst, loanStateName, job);
        }
        protected void OnClickOnStatus16(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];

            ShowStatusDiv();
            var status = _service.GetLoanStatusByType(param.loan_status_type);
            var ls = Convert.ToInt32(b16.Value);
            var lst = 0;
            var loanStateName = "";
            var job = 0;
            foreach (DataRow row in status.Rows)
            {
                if (ls == Convert.ToInt32(row["LOAN_STATUS_ID"]))
                {
                    lst = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                    loanStateName = row["LOAN_STATUS_NAME"].ToString();
                    job = Convert.ToInt32(row["JOB_STAT"]);
                }

            }
            PopulateView(param, lst, loanStateName, job);
        }
        protected void OnClickOnStatus18(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];

            ShowStatusDiv();
            var status = _service.GetLoanStatusByType(param.loan_status_type);
            var ls = Convert.ToInt32(b18.Value);
            var lst = 0;
            var loanStateName = "";
            var job = 0;
            foreach (DataRow row in status.Rows)
            {
                if (ls == Convert.ToInt32(row["LOAN_STATUS_ID"]))
                {
                    lst = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                    loanStateName = row["LOAN_STATUS_NAME"].ToString();
                    job = Convert.ToInt32(row["JOB_STAT"]);
                }

            }
            PopulateView(param, lst, loanStateName, job);
        }

        protected void OnClickOnStatus15(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            ShowStatusDiv();
            var status = _service.GetLoanStatusByType(param.loan_status_type);
            var ls = Convert.ToInt32(b15.Value);
            var lst = 0;
            var loanStateName = "";
            var job = 0;
            foreach (DataRow row in status.Rows)
            {
                if (ls == Convert.ToInt32(row["LOAN_STATUS_ID"]))
                {
                    lst = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                    loanStateName = row["LOAN_STATUS_NAME"].ToString();
                    job = Convert.ToInt32(row["JOB_STAT"]);
                }

            }
            PopulateView(param, lst, loanStateName, job);
        }
        protected void OnClickOnStatus2(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            ShowStatusDiv();
            var status = _service.GetLoanStatusByType(param.loan_status_type);
            var ls = Convert.ToInt32(b2.Value);
            var lst = 0;
            var loanStateName = "";
            var job = 0;
            foreach (DataRow row in status.Rows)
            {
                if (ls == Convert.ToInt32(row["LOAN_STATUS_ID"]))
                {
                    lst = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                    loanStateName = row["LOAN_STATUS_NAME"].ToString();
                    job = Convert.ToInt32(row["JOB_STAT"]);
                }

            }
            PopulateView(param, lst, loanStateName, job);
        }
        protected void OnClickOnStatus6(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            ShowStatusDiv();
            var status = _service.GetLoanStatusByType(param.loan_status_type);
            var ls = Convert.ToInt32(b6.Value);
            var lst = 0;
            var loanStateName = "";
            var job = 0;
            foreach (DataRow row in status.Rows)
            {
                if (ls == Convert.ToInt32(row["LOAN_STATUS_ID"]))
                {
                    lst = Convert.ToInt32(row["LOAN_STATUS_ID"]);
                    loanStateName = row["LOAN_STATUS_NAME"].ToString();
                    job = Convert.ToInt32(row["JOB_STAT"]);
                }

            }
            PopulateView(param, lst, loanStateName, job);
        }

        public void SaveOnStatus(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            //$id=$HTTP_POST_VARS['id'];
            //$lst=$HTTP_POST_VARS['lst'];
            //$type=$HTTP_POST_VARS['type'];
            //$pers=$HTTP_POST_VARS['pers'];
            //$job=$HTTP_POST_VARS['job'];
            //$ldep=$_SESSION['ori_id'];

            ////echo $id.$type.$pers."<BR>".$job."<BR>".$lst;



            decimal approvedAmount = 0;
            if (!string.IsNullOrEmpty(ap_amou.Value))
            {
                approvedAmount = Convert.ToDecimal(ap_amou.Value);
            }
            decimal condapprovamou = 0;
            if (!string.IsNullOrEmpty(cond_approv_amou.Value))
            {
                condapprovamou = Convert.ToDecimal(cond_approv_amou.Value);
            }
            decimal disbursedAmo = 0;
            if (!string.IsNullOrEmpty(disb_amou.Value))
            {
                disbursedAmo = Convert.ToDecimal(disb_amou.Value);
            }
            var objTenor = tenor.Value;
            var objRemarks = remarks.Value;
            var year = y1.Value;
            var month = m1.Value;
            var day = d1.Value;
            var disDate = year + '-' + month + '-' + day;
            var submit = hdnSubmit.Value;
            var preAcc = pre_acc.Value;
            var midAcc = mid_acc.Value;
            var postAcc = post_acc.Value;
            var loanac = preAcc + "-" + midAcc + "-" + postAcc;
            var loanAccExist = loan_ac.Value;
            //var defdec = def_dec.Value;
            var apprlvl = appr_lvl.Value;
            var dt = DateTime.Now;
            //$dt1=date("Y:m:d");
            var chk = Request.Form["reasonChk"];
            string reasonCheckList = chk ?? "";
            var res = "";
            if (param.LoanStateId == 2 || param.LoanStateId == 6)
            {
                res = _service.UpdateLoanToDeclinedOrAssessment(param, reasonCheckList, objRemarks);
            }
            else
            {
                res = _service.SaveOnStatus(param, approvedAmount, condapprovamou, objTenor, objRemarks, year, month,
                day, disDate, submit, preAcc, midAcc, postAcc, loanac, loanAccExist, reasonCheckList, apprlvl, disbursedAmo);
            }

            //var msg = res.Split('\\');
            //if (msg.Length > 0)
            //{
            //    Response.Write("<script>alert(" + msg[0] + ")</script>");

            //}
            //else
            //{
            //    Response.Write("<script>alert(" + res + ")</script>");
            //}
            new AT(this).AuditAndTraial("OnStatus", "OnStatus");
            CloseOnStatus(new object(), new EventArgs());
            LoadLoanInformation(param.appId);

        }

        public void CloseOnStatus(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = true;
            divOnStatus.Visible = false;
            LoadLoanInformation(param.appId);
        }

        private void PopulateView(ActionParam param, int lst, string loanStateName, int job)
        {
            var type = param.type;
            var name = "";
            var name1 = loanStateName;
            if (param.DeliDeptId == 5 && lst == 4)
            {
                name1 = "Disburse";
            }
            param.JobState = job;
            param.LoanStateId = lst;
            dvStateName.InnerText = name1;
            rowStatus.Visible = true;
            r210.Visible = false;
            r211.Visible = false;
            r212.Visible = false;
            r220.Visible = false;

            if (job == 21)
            {
                r210.Visible = true;
                r211.Visible = true;
                r212.Visible = true;
            }
            if (job == 22)
            {
                r220.Visible = true;
            }

            r40.Visible = false;
            r41.Visible = false;
            r42.Visible = false;
            tblAcc.Visible = false;
            loan_ac.Visible = false;
            if (lst == 4)
            {

                r40.Visible = true;
                r41.Visible = true;
                r42.Visible = true;
                if (string.IsNullOrEmpty(param.AccountNumber))
                {
                    tblAcc.Visible = true;
                }
                else
                {
                    loan_ac.Visible = true;
                    loan_ac.Value = param.AccountNumber;
                }
            }
            r260.Visible = false;
            if (lst == 2 || lst == 6)
            {
                rowStatus.Visible = false;
                r260.Visible = true;
                if (lst == 2)
                {
                    name = "Declined";
                    type = 1;
                }
                else
                {
                    name = "Deferred";
                    type = 2;
                }
                //r260Text.InnerText = name;
                spLlid.InnerText = param.appId.ToString();
                spCase.InnerText = name;
                spCust.InnerText = param.CustomerName;

                var reasonInfo = _service.GetLoanDecDefReasonInfo(param.appId);
                var rowArray = new List<int>();
                var rowcount = 0;
                if (reasonInfo != null)
                {
                    rowcount = reasonInfo.Rows.Count;
                    for (int i = 0; i < rowcount; i++)
                    {
                        rowArray.Add(Convert.ToInt32(reasonInfo.Rows[i]["REAS_ID"]));
                    }
                }
                var reason = _service.GetDeferredDeclineReason(param.prodId, type); ;
                if (reason != null)
                {
                    var done = 0;
                    var reasonTable = "<table width='90%' border='0' align='left'>";
                    for (int i = 0; i < reason.Rows.Count; i++)
                    {
                        reasonTable += "<tr bgcolor=E7F7C6>";
                        done = 0;
                        var num = reason.Rows[i]["num"];
                        var reasName = reason.Rows[i]["REAS_NAME"];
                        if (rowcount <= 0)
                        {
                            reasonTable += string.Format("<td><input type=checkbox runat='server'  name='reasonChk'  value={0} > {1}</input></td>", num, reasName);
                        }
                        else
                        {
                            for (var j = 0; j < rowArray.Count; j++)
                            {
                                if (j == Convert.ToInt32(reason.Rows[i]["num"]))
                                {
                                    reasonTable += string.Format("<td><input type=checkbox runat='server'  name='reasonChk'  value={0} checked> {1}</input></td>", num, reasName);
                                    done = 1;
                                }
                            }

                            if (done == 0)
                                reasonTable += string.Format("<td><input type=checkbox runat='server' name='reasonChk' value={0} > {1}</input></td>", num, reasName);
                        }
                        reasonTable += "</tr>";
                    }
                    divReason.InnerHtml = reasonTable + "</table>";
                }


            }
            if (job == 10)
            {
                hdnSubmit.Value = name1;
            }

            else if (param.JobState == 22)
            {
                hdnSubmit.Value = name1;
            }
            else
            {
                //html += "<input name='Submit'   id='submit3' runat='server' type='submit' onServerClick='SaveOnStatus' style='border:1px solid #83b67a; background-color:#D8EED5'  value='" + name1 + "'>";
            }

            submit4.Value = name1;
        }

        private void GetDeferredDeclineReason(int prodId, int type)
        {

            var data = _service.GetDeferredDeclineReason(prodId, type);
            //def_dec.DataTextField = "REAS_NAME";
            //def_dec.DataValueField = "REAS_ID";
            //def_dec.DataSource = data;
            //def_dec.DataBind();

        }

        private string YearDropDwon()
        {
            var htm = " <select name=y1 id='y1'  runat='server'>";
            var y = DateTime.Now.Year;
            for (var i = 2004; i < 2021; i++)
            {
                htm += y == i
                    ? " <option value=" + i + " selected> " + i + " </option>"
                    : " <option value=" + i + "> " + i + " </option>";
            }
            return htm + "</select>";
        }

        private string MonthDropDwon()
        {
            var htm =
                "<select name=m1 id='m1'  runat='server' style='border:1px solid #4179C0; background-color:#F5F8FC'>";
            var m = DateTime.Now.Month;
            var j = 0;
            var month1 = new string[]
            {
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
                "November", "December"
            };
            for (var i = 0; i < 12; i++)
            {
                j = i + 1;
                htm += m == j
                    ? " <option value=" + j + " selected>" + month1[i] + "</option>"
                    : " <option value=" + j + ">" + month1[i] + "</option>";
            }
            htm += "</select>";
            return htm;
        }

        private string DayDropDwon()
        {
            var conent = "<select name=d1 id=d1  runat='server'>";
            var d = DateTime.Now.Day;
            for (var i = 1; i < 32; i++)
            {
                if (d == i)
                    conent += " <option value=" + i + " selected> " + i + " </option>";
                else
                    conent += " <option value=i> " + i + " </option>";
            }
            return conent + "</select>";
        }

        #endregion


        #region To Source

        //protected void OnClickToSource2(object sender, EventArgs e)
        //{
        //    var param = (ActionParam)Session["param"];
        //    divViewApplication.Visible = false;
        //    toOtherVc.Visible = false;
        //    divUpdate.Visible = false;
        //    divLoanOps.Visible = false;
        //    divToCredit.Visible = false;
        //    divOnProcess.Visible = false;
        //    divUpdateProfile.Visible = false;
        //    divOnStatus.Visible = false;
        //    divToSource.Visible = true;
        //    divOnHold.Visible = false;
        //    divisbursed.Visible = false;
        //    divLoanDisbursed.Visible = false;
        //    divToMaker.Visible = false;

        //    param.job = Convert.ToInt32(srcJob.Value);
        //    param.lst = Convert.ToInt32(srcLst.Value);
        //    param.backfrom = Convert.ToInt32(srcBack.Value);

        //    if (param.lst == 11)
        //        status.Text = "Forward to Loan admin( LLID:" + param.appId + ")";
        //    if (param.lst == 12)
        //        status.Text = "Send Back to Credit( LLID:" + param.appId + ")";
        //    if (param.lst == 3)
        //        status.Text = "Send Back to Source( LLID:" + param.appId + ")";

        //}
        protected void OnToSourceClick(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = true;
            btnSubmitToOnSource.Value = param.toSrcBtn;
        }

        protected void SubmitToOnSource(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            var remarks = toSourceRemarks.Value;
            param.lst = 3;
            var res = _service.SubmitAssetStatus(param, remarks);
            //Response.Write("<script>alert('" + res + "')</script>");
            new AT(this).AuditAndTraial("ToSource", "ToSource");
            CloseToOnSource(new object(), new EventArgs());
            LoadLoanInformation(param.appId);
        }

        protected void CloseToOnSource(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = true;
            divToSource.Visible = false;
            LoadLoanInformation(param.appId);
        }

        #endregion


        #region On Hold

        protected void OnClickOnHold(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];

            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = true;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            sel_onhold.DataValueField = "ONHOLD_NAME";
            sel_onhold.DataTextField = "ONHOLD_NAME";
            sel_onhold.DataSource = _service.GetOnHolds();
            sel_onhold.DataBind();
        }

        protected void OnHoldSubmit(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            var remarks = onHoldRemarks.Value;
            param.Button = "On_Hold";
            var res = _service.UpdateLoanOps(param, remarks, false, "On_Hold");
            //UpdateStatusOption(param);
            if (res == "1")
            {
                // Response.Write("<script>alert('Loan Updated Successfully')</script>");
                OnHodlClose(new object(), new EventArgs());
            }
            else if (res == "0")
            {
                Response.Write("<script>alert('Loan not update, Try again!')</script>");
            }
            else
            {
                Response.Write("<script>alert('" + res + "')</script>");
            }
            new AT(this).AuditAndTraial("ToHold", "ToHold");
            LoadLoanInformation(param.appId);
        }

        protected void OnHodlClose(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = true;
            divOnHold.Visible = false;
            LoadLoanInformation(param.appId);
        }

        #endregion


        protected void OnClickDisbursed(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            param.Button = "AssetReadyDis";
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = true;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            UpdateStatusOption(param);

            divViewApplication.Visible = true;
            divisbursed.Visible = false;
            LoadLoanInformation(param.appId);
        }
        protected void OnClickLoanDisbursed(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = true;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            LoadLoanInformation(param.appId);
        }
        protected void OnClickToMaker(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            param.Button = "AssetRetCheck";
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            UpdateStatusOption(param);
            divViewApplication.Visible = true;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            LoadLoanInformation(param.appId);
        }

        #region Update Check List

        protected void OnClickToCheckList(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = false;
            toOtherVc.Visible = false;
            divUpdate.Visible = false;
            divLoanOps.Visible = false;
            divToCredit.Visible = false;
            divOnProcess.Visible = false;
            divUpdateProfile.Visible = false;
            divOnStatus.Visible = false;
            divToSource.Visible = false;
            divOnHold.Visible = false;
            divisbursed.Visible = false;
            divLoanDisbursed.Visible = false;
            divToMaker.Visible = false;
            divSubmitToOnSource.Visible = false;
            divCheckList.Visible = true;

            PopulateCheckList();

        }

        private void PopulateCheckList()
        {
            var erroPos = 0;
            try
            {
                var param = (ActionParam)Session["param"];
                param.makerID = (string)Session["UserId"];
                var loan = _service.GetLoanApplication(param.appId);
                //var obj = DataTableToList.GetList<LoanInformation>(loan).FirstOrDefault();
                spAppId.InnerText = param.appId.ToString();
                spCustomerName.InnerText = loan.Rows[0][9].ToString();//obj.ApplicantName;

                var loanCheckList = _service.GetLoanCheckListInfo(param.appId);
                var chkList = _service.GetCheckList(param.prodId);
                var chkTable = "<table width='100%' border='0' align='center'>";

                if (chkList != null)
                {
                    for (int i = 0; i < chkList.Rows.Count; i++)
                    {
                        var tr = "<tr style='background-color:#E7F7C6'>";
                        var chkId = 0;
                        if (chkList.Rows[i]["Id"] != null)
                        {
                            chkId = Convert.ToInt32(chkList.Rows[i]["Id"]);
                        }
                        var title = chkList.Rows[i]["Name"].ToString();
                        var check = "";

                        if (loanCheckList != null)
                        {
                            for (int j = 0; j < loanCheckList.Rows.Count; j++)
                            {
                                if (loanCheckList.Rows[j]["CHECK_LIST_ID"] != null)
                                {
                                    if (chkId == Convert.ToInt32(loanCheckList.Rows[j]["CHECK_LIST_ID"]))
                                    {
                                        check = "checked";
                                        break;
                                    }
                                }

                            }
                        }

                        var td = string.Format(@"<td><input type=checkbox runat='server' name='check_list' value={0} {1}> {2}</input></td>", chkId, check, title);
                        chkTable += tr + td + "</tr>";
                    }
                    checkListDetails.InnerHtml = chkTable + "</table>";
                }
                else
                {
                    checkListDetails.InnerHtml = chkTable + "<tr bgcolor='#E7F7C6'><td bgcolor='#BDD3A5' align='center'><font color='#FF0000'>Product Have No Check List.</font></td></tr></table>";
                }
            }
            catch (Exception e)
            {
                throw new Exception(erroPos.ToString());
            }
        }

        protected void OnClickUpdateCheckList(object o, EventArgs e)
        {
            var selectedValues = Request.Form["check_list"];
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            var res = _service.OnClickUpdateCheckList(param.appId, selectedValues);
            new AT(this).AuditAndTraial("UpdateCheckList", "UpdateCheckList");
            LoadLoanInformation(param.appId);
        }

        protected void OnClickCloseCheckList(object o, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            divViewApplication.Visible = true;
            divCheckList.Visible = false;
            LoadLoanInformation(param.appId);
        }

        #endregion
        protected void btnCibSubmit_Click(object sender, EventArgs e)
        {
            var param = (ActionParam)Session["param"];
            param.makerID = (string)Session["UserId"];
            param.oriId = 5;
            param.Button = "cib_status";
            UpdateStatusOption(param);
            new AT(this).AuditAndTraial("Update CIB", "Update CIB");
            LoadLoanInformation(param.appId);
        }

    }

    public class Cib
    {
        public int PROCESS_STATUS_ID { get; set; }
        public string REMARKS { get; set; }
    }
}