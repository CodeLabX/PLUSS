﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI.LoanApplication
{
    public partial class UI_JointApplicationUI : System.Web.UI.Page
    {
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            STATUSUPDATE = 3,
            INSERTERROR = -1,
            UPDATEERROR = -2,
            STATUSUPDATEERROR = -3
        }
    
        private LoanApplicationManager loanApplicationManagerObj;
        private JointApplicationManager jointApplicationManagerObj;
        List<EducationLevel> educationLevelListObj;
        List<IdType> idTypeListObj;
        private const Int32 SCBACCLENGTH = 11;
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.DefaultFocus = LLIDTextBox.ClientID;
            Page.Form.DefaultButton = findButtonTextBox.UniqueID;

            loanApplicationManagerObj = new LoanApplicationManager();
            jointApplicationManagerObj = new JointApplicationManager();
            idTypeListObj = loanApplicationManagerObj.GetAllIdTypes();
            educationLevelListObj = loanApplicationManagerObj.GetAllEducationLevel();
            if (!Page.IsPostBack)
            {
                InitializedData();
                JoinApplicantIdTypeDropDownListItem();
                EducationLevelDropDownListItems();
            }
            //ScriptManager.RegisterClientScriptInclude(this.Page, typeof(Page), "JointAppValidation",
            //    Page.ResolveClientUrl("~/Scripts/JointApplicationValidation.js"));
            //ScriptManager.RegisterClientScriptInclude(this.Page, typeof(Page), "JointAppValidation2",
            //    Page.ResolveClientUrl("~/Scripts/TextBoxDatePicker.js"));
            LoadJavaScriptFuntion();
        }

        private void LoadJavaScriptFuntion()
        {
            //jTinPreTextBox1.Attributes.Add("onkeyup", "CursorAutoPosition('" + jTinPreTextBox1.ClientID + "','" + jTinMasterTextBox1.ClientID + "','" + jTinPostTextBox1.ClientID + "',1);");
            //jTinMasterTextBox1.Attributes.Add("onkeyup", "CursorAutoPosition('" + jTinPreTextBox1.ClientID + "','" + jTinMasterTextBox1.ClientID + "','" + jTinPostTextBox1.ClientID + "',2);");
            //jTinPostTextBox1.Attributes.Add("onkeyup", "CursorAutoPosition('" + jTinPreTextBox1.ClientID + "','" + jTinMasterTextBox1.ClientID + "','" + jTinPostTextBox1.ClientID + "',3);");

            //jAppTinPreTextBox2.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox2.ClientID + "','" + jAppTinMasterTextBox2.ClientID + "','" + jAppTinPostTextBox2.ClientID + "',1);");
            //jAppTinMasterTextBox2.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox2.ClientID + "','" + jAppTinMasterTextBox2.ClientID + "','" + jAppTinPostTextBox2.ClientID + "',2);");
            //jAppTinPostTextBox2.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox2.ClientID + "','" + jAppTinMasterTextBox2.ClientID + "','" + jAppTinPostTextBox2.ClientID + "',3);");

            //jAppTinPreTextBox3.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox3.ClientID + "','" + jAppTinMasterTextBox3.ClientID + "','" + jAppTinPostTextBox3.ClientID + "',1);");
            //jAppTinMasterTextBox3.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox3.ClientID + "','" + jAppTinMasterTextBox3.ClientID + "','" + jAppTinPostTextBox3.ClientID + "',2);");
            //jAppTinPostTextBox3.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox3.ClientID + "','" + jAppTinMasterTextBox3.ClientID + "','" + jAppTinPostTextBox3.ClientID + "',3);");

            jAppTinPreTextBox4.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox4.ClientID + "','" + jAppTinMasterTextBox4.ClientID + "','" + jAppTinPostTextBox4.ClientID + "',1);");
            jAppTinMasterTextBox4.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox4.ClientID + "','" + jAppTinMasterTextBox4.ClientID + "','" + jAppTinPostTextBox4.ClientID + "',2);");
            jAppTinPostTextBox4.Attributes.Add("onkeyup", "CursorAutoPosition('" + jAppTinPreTextBox4.ClientID + "','" + jAppTinMasterTextBox4.ClientID + "','" + jAppTinPostTextBox4.ClientID + "',3);");

            (jointCreditCardGridView1.Rows[0].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jointCreditCardGridView1.Rows[0].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).ClientID + "');");
            (jointCreditCardGridView1.Rows[1].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jointCreditCardGridView1.Rows[1].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).ClientID + "');");
            (jointCreditCardGridView1.Rows[2].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jointCreditCardGridView1.Rows[2].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).ClientID + "');");
            (jointCreditCardGridView1.Rows[3].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jointCreditCardGridView1.Rows[3].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).ClientID + "');");
            (jointCreditCardGridView1.Rows[4].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jointCreditCardGridView1.Rows[4].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).ClientID + "');");

            (jCrdtCrdGridView2.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jCrdtCrdGridView2.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).ClientID + "');");
            (jCrdtCrdGridView2.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jCrdtCrdGridView2.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).ClientID + "');");
            (jCrdtCrdGridView2.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jCrdtCrdGridView2.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).ClientID + "');");
            (jCrdtCrdGridView2.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jCrdtCrdGridView2.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).ClientID + "');");
            (jCrdtCrdGridView2.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jCrdtCrdGridView2.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).ClientID + "');");

            (jAppCrdtCrdGridView3.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView3.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).ClientID + "');");
            (jAppCrdtCrdGridView3.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView3.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).ClientID + "');");
            (jAppCrdtCrdGridView3.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView3.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).ClientID + "');");
            (jAppCrdtCrdGridView3.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView3.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).ClientID + "');");
            (jAppCrdtCrdGridView3.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView3.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).ClientID + "');");

            (jAppCrdtCrdGridView4.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView4.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).ClientID + "');");
            (jAppCrdtCrdGridView4.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView4.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).ClientID + "');");
            (jAppCrdtCrdGridView4.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView4.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).ClientID + "');");
            (jAppCrdtCrdGridView4.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView4.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).ClientID + "');");
            (jAppCrdtCrdGridView4.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Attributes.Add("onblur", "CreditCardValidation('" + (jAppCrdtCrdGridView4.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).ClientID + "');");

            JDOBTextBox1.Attributes.Add("onblur", "IdDateFormate('" + JDOBTextBox1.ClientID + "');");
            JDOBTextBox1.Attributes.Add("onkeyup", "AutoFormatDate('" + JDOBTextBox1.ClientID + "');");
            (jointIdGridView1.Rows[0].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jointIdGridView1.Rows[0].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[1].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jointIdGridView1.Rows[1].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[2].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jointIdGridView1.Rows[2].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[3].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jointIdGridView1.Rows[3].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[4].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jointIdGridView1.Rows[4].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[0].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jointIdGridView1.Rows[0].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[1].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jointIdGridView1.Rows[1].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[2].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jointIdGridView1.Rows[2].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[3].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jointIdGridView1.Rows[3].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");
            (jointIdGridView1.Rows[4].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jointIdGridView1.Rows[4].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).ClientID + "');");

            jAppDOBTextBox2.Attributes.Add("onblur", "IdDateFormate('" + jAppDOBTextBox2.ClientID + "');");
            jAppDOBTextBox2.Attributes.Add("onkeyup", "AutoFormatDate('" + jAppDOBTextBox2.ClientID + "');");
            (jAppIdGridView2.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView2.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView2.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView2.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView2.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView2.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView2.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView2.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView2.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView2.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");
            (jAppIdGridView2.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView2.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).ClientID + "');");

            jAppDOBTextBox3.Attributes.Add("onblur", "IdDateFormate('" + jAppDOBTextBox3.ClientID + "');");
            jAppDOBTextBox3.Attributes.Add("onkeyup", "AutoFormatDate('" + jAppDOBTextBox3.ClientID + "');");
            (jAppIdGridView3.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView3.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView3.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView3.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView3.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView3.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView3.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView3.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView3.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView3.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");
            (jAppIdGridView3.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView3.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).ClientID + "');");

            jAppDOBTextBox4.Attributes.Add("onblur", "IdDateFormate('" + jAppDOBTextBox4.ClientID + "');");
            jAppDOBTextBox4.Attributes.Add("onkeyup", "AutoFormatDate('" + jAppDOBTextBox4.ClientID + "');");
            (jAppIdGridView4.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView4.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView4.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView4.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView4.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (jAppIdGridView4.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView4.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView4.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView4.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView4.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");
            (jAppIdGridView4.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Attributes.Add("onkeyup", "AutoFormatDate('" + (jAppIdGridView4.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).ClientID + "');");

            (jointApplicantaccNoGridView1.Rows[0].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[0].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[0].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[0].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',1);");
            (jointApplicantaccNoGridView1.Rows[0].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[0].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[0].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[0].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',2);");
            (jointApplicantaccNoGridView1.Rows[0].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[0].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[0].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[0].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',3);");
            (jointApplicantaccNoGridView1.Rows[1].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[1].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[1].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[1].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',1);");
            (jointApplicantaccNoGridView1.Rows[1].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[1].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[1].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[1].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',2);");
            (jointApplicantaccNoGridView1.Rows[1].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[1].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[1].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[1].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',3);");
            (jointApplicantaccNoGridView1.Rows[2].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[2].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[2].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[2].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',1);");
            (jointApplicantaccNoGridView1.Rows[2].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[2].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[2].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[2].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',2);");
            (jointApplicantaccNoGridView1.Rows[2].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[2].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[2].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[2].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',3);");
            (jointApplicantaccNoGridView1.Rows[3].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[3].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[3].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[3].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',1);");
            (jointApplicantaccNoGridView1.Rows[3].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[3].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[3].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[3].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',2);");
            (jointApplicantaccNoGridView1.Rows[3].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[3].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[3].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[3].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',3);");
            (jointApplicantaccNoGridView1.Rows[4].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[4].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[4].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[4].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',1);");
            (jointApplicantaccNoGridView1.Rows[4].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[4].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[4].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[4].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',2);");
            (jointApplicantaccNoGridView1.Rows[4].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jointApplicantaccNoGridView1.Rows[4].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[4].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).ClientID + "','" + (jointApplicantaccNoGridView1.Rows[4].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).ClientID + "',3);");

            (jAppSCBAccGridView2.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView2.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView2.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',3);");
            (jAppSCBAccGridView2.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView2.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView2.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',3);");
            (jAppSCBAccGridView2.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView2.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView2.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',3);");
            (jAppSCBAccGridView2.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView2.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView2.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',3);");
            (jAppSCBAccGridView2.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView2.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView2.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView2.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).ClientID + "','" + (jAppSCBAccGridView2.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).ClientID + "',3);");

            (jAppSCBAccGridView3.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView3.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView3.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',3);");
            (jAppSCBAccGridView3.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView3.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView3.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',3);");
            (jAppSCBAccGridView3.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView3.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView3.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',3);");
            (jAppSCBAccGridView3.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView3.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView3.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',3);");
            (jAppSCBAccGridView3.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',1);");
            (jAppSCBAccGridView3.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',2);");
            (jAppSCBAccGridView3.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jAppSCBAccGridView3.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).ClientID + "','" + (jAppSCBAccGridView3.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).ClientID + "',3);");

            (jSCBAccGridView4.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',1);");
            (jSCBAccGridView4.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',2);");
            (jSCBAccGridView4.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',3);");
            (jSCBAccGridView4.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',1);");
            (jSCBAccGridView4.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',2);");
            (jSCBAccGridView4.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',3);");
            (jSCBAccGridView4.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',1);");
            (jSCBAccGridView4.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',2);");
            (jSCBAccGridView4.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',3);");
            (jSCBAccGridView4.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',1);");
            (jSCBAccGridView4.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',2);");
            (jSCBAccGridView4.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',3);");
            (jSCBAccGridView4.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',1);");
            (jSCBAccGridView4.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',2);");
            (jSCBAccGridView4.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Attributes.Add("onkeyup", "CursorAutoPosition4SCBAcc('" + (jSCBAccGridView4.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).ClientID + "','" + (jSCBAccGridView4.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).ClientID + "',3);");
        }
        private void InitializedData()
        {
            string a = "";
            List<string> stringList = new List<string>();
            List<string> contactNoListObj = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                a = "";
                stringList.Add(a);
            }
            for (int j = 0; j < 4; j++)
            {
                a = "";
                contactNoListObj.Add(a);
            }
            List<string> maritalStatus = new List<string>();
            maritalStatus.Insert(0, "Married");
            maritalStatus.Insert(1, "Single");


            List<string> residentialStatus = new List<string>();
            residentialStatus.Insert(0, "Own");
            residentialStatus.Insert(1, "Family Owned");
            residentialStatus.Insert(2, "Others");


            jointApplicantaccNoGridView1.DataSource = stringList;
            jointApplicantaccNoGridView1.DataBind();

            jointCreditCardGridView1.DataSource = stringList;
            jointCreditCardGridView1.DataBind();

            jointcontactGridView1.DataSource = contactNoListObj;
            jointcontactGridView1.DataBind();

            jointIdGridView1.DataSource = stringList;
            jointIdGridView1.DataBind();

            jAppSCBAccGridView2.DataSource = stringList;
            jAppSCBAccGridView2.DataBind();

            jCrdtCrdGridView2.DataSource = stringList;
            jCrdtCrdGridView2.DataBind();

            jAppContactGridView2.DataSource = contactNoListObj;
            jAppContactGridView2.DataBind();

            jAppIdGridView2.DataSource = stringList;
            jAppIdGridView2.DataBind();

            jAppSCBAccGridView3.DataSource = stringList;
            jAppSCBAccGridView3.DataBind();

            jAppCrdtCrdGridView3.DataSource = stringList;
            jAppCrdtCrdGridView3.DataBind();

            jAppContactGridView3.DataSource = contactNoListObj;
            jAppContactGridView3.DataBind();

            jAppIdGridView3.DataSource = stringList;
            jAppIdGridView3.DataBind();

            jSCBAccGridView4.DataSource = stringList;
            jSCBAccGridView4.DataBind();

            jAppCrdtCrdGridView4.DataSource = stringList;
            jAppCrdtCrdGridView4.DataBind();

            jAppContactGridView4.DataSource = contactNoListObj;
            jAppContactGridView4.DataBind();

            jAppIdGridView4.DataSource = stringList;
            jAppIdGridView4.DataBind();

            jointEducationalDropDownList1.SelectedValue = "";
            jAppEducationDropDownList2.SelectedValue = "";
            foreach (GridViewRow gvr in jointIdGridView1.Rows)
            {
                (gvr.FindControl("jointIdTypeDropDownList1") as DropDownList).SelectedValue = "";
            }
            foreach (GridViewRow gvr in jAppIdGridView2.Rows)
            {
                (gvr.FindControl("jAppIdTypeDropDownList2") as DropDownList).SelectedValue = "";
            }

            jointcontactGridView1.Rows[0].Cells[0].Text = "RS Tel";
            jointcontactGridView1.Rows[1].Cells[0].Text = "RS Cell";
            jointcontactGridView1.Rows[2].Cells[0].Text = "Off Tel";
            jointcontactGridView1.Rows[3].Cells[0].Text = "Off Cell";


            jAppContactGridView2.Rows[0].Cells[0].Text = "RS Tel";
            jAppContactGridView2.Rows[1].Cells[0].Text = "RS Cell";
            jAppContactGridView2.Rows[2].Cells[0].Text = "Off Tel";
            jAppContactGridView2.Rows[3].Cells[0].Text = "Off Cell";


            jAppContactGridView3.Rows[0].Cells[0].Text = "RS Tel";
            jAppContactGridView3.Rows[1].Cells[0].Text = "RS Cell";
            jAppContactGridView3.Rows[2].Cells[0].Text = "Off Tel";
            jAppContactGridView3.Rows[3].Cells[0].Text = "Off Cell";

            jAppContactGridView4.Rows[0].Cells[0].Text = "RS Tel";
            jAppContactGridView4.Rows[1].Cells[0].Text = "RS Cell";
            jAppContactGridView4.Rows[2].Cells[0].Text = "Off Tel";
            jAppContactGridView4.Rows[3].Cells[0].Text = "Off Cell";

        }

        #region Joint Applicant Id Type DropDown Items
        /// <summary>
        /// This method gives account type dropdown items.
        /// </summary>
        private void JoinApplicantIdTypeDropDownListItem()
        {
            //List<IdType> idTypeListObj = loanApplicationManagerObj.GetAllIdTypes();
            //Joint Applicant 1
            IdType idTypeObj = new IdType();
            idTypeObj.Id = 0;
            idTypeObj.Name = "";
            idTypeListObj.Insert(0, idTypeObj);
            //idTypeListObj.Add(idTypeObj);
            foreach (GridViewRow gvr in jointIdGridView1.Rows)
            {
                (gvr.FindControl("jointIdTypeDropDownList1") as DropDownList).DataSource = idTypeListObj;
                (gvr.FindControl("jointIdTypeDropDownList1") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("jointIdTypeDropDownList1") as DropDownList).DataValueField = "Name";
                (gvr.FindControl("jointIdTypeDropDownList1") as DropDownList).DataBind();
            }
            //End Joint Applicant 1

            //Joint Applicant 2
            foreach (GridViewRow gvr in jAppIdGridView2.Rows)
            {
                (gvr.FindControl("jAppIdTypeDropDownList2") as DropDownList).DataSource = idTypeListObj;
                (gvr.FindControl("jAppIdTypeDropDownList2") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("jAppIdTypeDropDownList2") as DropDownList).DataValueField = "Name";
                (gvr.FindControl("jAppIdTypeDropDownList2") as DropDownList).DataBind();
            }
            //End Joint Applicant 2

            //Joint Applicant 3
            foreach (GridViewRow gvr in jAppIdGridView3.Rows)
            {
                (gvr.FindControl("jAppIdTypeTextBox3") as DropDownList).DataSource = idTypeListObj;
                (gvr.FindControl("jAppIdTypeTextBox3") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("jAppIdTypeTextBox3") as DropDownList).DataValueField = "Name";
                (gvr.FindControl("jAppIdTypeTextBox3") as DropDownList).DataBind();
            }
            //End Joint Applicant 3

            //Joint Applicant 4
            foreach (GridViewRow gvr in jAppIdGridView4.Rows)
            {
                (gvr.FindControl("jAppIdTypeTextBox4") as DropDownList).DataSource = idTypeListObj;
                (gvr.FindControl("jAppIdTypeTextBox4") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("jAppIdTypeTextBox4") as DropDownList).DataValueField = "Name";
                (gvr.FindControl("jAppIdTypeTextBox4") as DropDownList).DataBind();
            }
            //End Joint Applicant 4
        }
        #endregion

        #region Education Level DropDown List Items
        /// <summary>
        /// This method populate all education levels.
        /// </summary>
        private void EducationLevelDropDownListItems()
        {
            //educationLevelListObj = loanApplicationManagerObj.GetAllEducationLevel();
            //educationLevelListObj = loanApplicationManagerObj.GetAllEducationLevel();
            EducationLevel educationLevelObj = new EducationLevel();
            educationLevelObj.Id = 0;
            educationLevelObj.Name = "";
            //educationLevelListObj.Add(educationLevelObj);
            educationLevelListObj.Insert(0, educationLevelObj);
            jointEducationalDropDownList1.DataSource = educationLevelListObj;
            jointEducationalDropDownList1.DataTextField = "Name";
            jointEducationalDropDownList1.DataValueField = "Name";
            jointEducationalDropDownList1.DataBind();

            jAppEducationDropDownList2.DataSource = educationLevelListObj;
            jAppEducationDropDownList2.DataTextField = "Name";
            jAppEducationDropDownList2.DataValueField = "Name";
            jAppEducationDropDownList2.DataBind();

            jAppEducationDropDownList3.DataSource = educationLevelListObj;
            jAppEducationDropDownList3.DataTextField = "Name";
            jAppEducationDropDownList3.DataValueField = "Name";
            jAppEducationDropDownList3.DataBind();

            jAppEducationDropDownList4.DataSource = educationLevelListObj;
            jAppEducationDropDownList4.DataTextField = "Name";
            jAppEducationDropDownList4.DataValueField = "Name";
            jAppEducationDropDownList4.DataBind();
        }
        #endregion

        protected void findButtonTextBox_Click(object sender, EventArgs e)
        {
            try
            {
                int llid = Convert.ToInt32(LLIDTextBox.Text.Trim());
                if (!jointApplicationManagerObj.IsExistsLoanInfo(llid))
                {
                    errorMsgLabel.Text = "Data not found!";
                    saveButton.Enabled = false;
                    ClearJointApplicantInfo();
                    return;
                }
                errorMsgLabel.Text = "";
                saveButton.Enabled = true;
                PopulateJointApplcants(llid);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Finding Joint Application");
            }
        }
        #region PopulateJointApplcants
        /// <summary>
        /// This method populate joint applicants information
        /// </summary>
        /// <param name="llid">Gets llid</param>
        public void PopulateJointApplcants(Int32 llid)
        {
            JoinApplicantInformation joinApplicantInformationObj = jointApplicationManagerObj.GetAllJointApplicantInformation(llid);
            if (joinApplicantInformationObj != null)
            {
                jointApplicantsNameTextBox1.Text = joinApplicantInformationObj.JointApp1Name;
                if (joinApplicantInformationObj.J1SCBAccNo1.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jointApplicantaccNoGridView1.Rows[0].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo1.ToString().Substring(0, 2);
                    (jointApplicantaccNoGridView1.Rows[0].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo1.ToString().Substring(2, 7);
                    (jointApplicantaccNoGridView1.Rows[0].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo1.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J1SCBAccNo1 == 0)
                    {
                        (jointApplicantaccNoGridView1.Rows[0].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = "";
                    }
                    else
                    {
                        (jointApplicantaccNoGridView1.Rows[0].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo1.ToString();
                    }
                }
                if (joinApplicantInformationObj.J1SCBAccNo2.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jointApplicantaccNoGridView1.Rows[1].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo2.ToString().Substring(0, 2);
                    (jointApplicantaccNoGridView1.Rows[1].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo2.ToString().Substring(2, 7);
                    (jointApplicantaccNoGridView1.Rows[1].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo2.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J1SCBAccNo2 == 0)
                    {
                        (jointApplicantaccNoGridView1.Rows[1].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = "";
                    }
                    else
                    {
                        (jointApplicantaccNoGridView1.Rows[1].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo2.ToString();
                    }
                }
                if (joinApplicantInformationObj.J1SCBAccNo3.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jointApplicantaccNoGridView1.Rows[2].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo3.ToString().Substring(0, 2);
                    (jointApplicantaccNoGridView1.Rows[2].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo3.ToString().Substring(2, 7);
                    (jointApplicantaccNoGridView1.Rows[2].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo3.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J1SCBAccNo3 == 0)
                    {
                        (jointApplicantaccNoGridView1.Rows[2].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = "";
                    }
                    else
                    {
                        (jointApplicantaccNoGridView1.Rows[2].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo3.ToString();
                    }
                }
                if (joinApplicantInformationObj.J1SCBAccNo4.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jointApplicantaccNoGridView1.Rows[3].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo4.ToString().Substring(0, 2);
                    (jointApplicantaccNoGridView1.Rows[3].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo4.ToString().Substring(2, 7);
                    (jointApplicantaccNoGridView1.Rows[3].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo4.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J1SCBAccNo4 == 0)
                    {
                        (jointApplicantaccNoGridView1.Rows[3].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = "";
                    }
                    else
                    {
                        (jointApplicantaccNoGridView1.Rows[3].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo4.ToString();
                    }
                }
                if (joinApplicantInformationObj.J1SCBAccNo5.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jointApplicantaccNoGridView1.Rows[4].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo5.ToString().Substring(0, 2);
                    (jointApplicantaccNoGridView1.Rows[4].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo5.ToString().Substring(2, 7);
                    (jointApplicantaccNoGridView1.Rows[4].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo5.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J1SCBAccNo5 == 0)
                    {
                        (jointApplicantaccNoGridView1.Rows[4].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = "";
                    }
                    else
                    {
                        (jointApplicantaccNoGridView1.Rows[4].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = joinApplicantInformationObj.J1SCBAccNo5.ToString();
                    }
                }
                if (String.IsNullOrEmpty(joinApplicantInformationObj.J1TinNo))
                {
                    jTinPreTextBox1.Text = "";
                    jTinMasterTextBox1.Text = "";
                    jTinPostTextBox1.Text = "";
                }
                //else if (joinApplicantInformationObj.J1TinNo.Replace("-", "").Length <= 3)
                //{
                //    jTinPreTextBox1.Text = joinApplicantInformationObj.J1TinNo;
                //    jTinMasterTextBox1.Text = "";
                //    jTinPostTextBox1.Text = "";
                //}
                //else if (joinApplicantInformationObj.J1TinNo.Replace("-", "").Length <= 6 && joinApplicantInformationObj.J1TinNo.Replace("-", "").Length > 3)
                //{
                //    jTinPreTextBox1.Text = joinApplicantInformationObj.J1TinNo.Replace("-", "").Substring(0, 3);
                //    jTinMasterTextBox1.Text = joinApplicantInformationObj.J1TinNo.Replace("-", "").Substring(3);
                //    jTinPostTextBox1.Text = "";
                //}
                else
                {
                    jTinPreTextBox1.Text = joinApplicantInformationObj.J1TinNo.Replace("-", "");//.Substring(0, 3);
                    jTinMasterTextBox1.Text = "";// joinApplicantInformationObj.J1TinNo.Replace("-", "").Substring(3, 3);
                    jTinPostTextBox1.Text = "";// joinApplicantInformationObj.J1TinNo.Replace("-", "").Substring(6);
                }
                (jointCreditCardGridView1.Rows[0].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text = joinApplicantInformationObj.J1CrdtCrdNo1;
                (jointCreditCardGridView1.Rows[1].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text = joinApplicantInformationObj.J1CrdtCrdNo2;
                (jointCreditCardGridView1.Rows[2].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text = joinApplicantInformationObj.J1CrdtCrdNo3;
                (jointCreditCardGridView1.Rows[3].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text = joinApplicantInformationObj.J1CrdtCrdNo4;
                (jointCreditCardGridView1.Rows[4].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text = joinApplicantInformationObj.J1CrdtCrdNo5;

                jointApplicantFatherTextBox1.Text = joinApplicantInformationObj.J1FatherName;
                jointApplicantMotherTextBox1.Text = joinApplicantInformationObj.J1MotherName;
                if (DateFormat.IsDate(joinApplicantInformationObj.J1DOB))
                {
                    JDOBTextBox1.Text = joinApplicantInformationObj.J1DOB.ToString("dd-MM-yyyy");
                }
                else
                {
                    JDOBTextBox1.Text = "";
                }

                bool joint1EducationLevelFlag = false;
                for (int i = 0; i < educationLevelListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp1EducationLevel.Trim() == educationLevelListObj[i].Name)
                    {
                        joint1EducationLevelFlag = true;
                        break;
                    }
                }
                if (joint1EducationLevelFlag)
                {
                    jointEducationalDropDownList1.Text = joinApplicantInformationObj.JApp1EducationLevel.Trim();
                }
                else
                {
                    jointEducationalDropDownList1.Text = "";
                }

                (jointcontactGridView1.Rows[0].Cells[0].FindControl("jointContactNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1ContactNo1;
                (jointcontactGridView1.Rows[1].Cells[0].FindControl("jointContactNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1ContactNo2;
                (jointcontactGridView1.Rows[2].Cells[0].FindControl("jointContactNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1ContactNo3;
                (jointcontactGridView1.Rows[3].Cells[0].FindControl("jointContactNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1ContactNo4;

                jointCompanyNameTextBox1.Text = joinApplicantInformationObj.JApp1Company;
                bool joint1IdTyeFlag1 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp1IdType1.Trim() == idTypeListObj[i].Name)
                    {
                        joint1IdTyeFlag1 = true;
                        break;
                    }
                }
                if (joint1IdTyeFlag1)
                {
                    (jointIdGridView1.Rows[0].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = joinApplicantInformationObj.JApp1IdType1.Trim();
                }
                else
                {
                    (jointIdGridView1.Rows[0].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = "";
                }
                (jointIdGridView1.Rows[0].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1Id1;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp1IdExpire1))
                {
                    (jointIdGridView1.Rows[0].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1IdExpire1.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jointIdGridView1.Rows[0].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = "";
                }
                bool joint1IdTyeFlag2 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp1IdType2.Trim() == idTypeListObj[i].Name)
                    {
                        joint1IdTyeFlag2 = true;
                        break;
                    }
                }
                if (joint1IdTyeFlag2)
                {
                    (jointIdGridView1.Rows[1].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = joinApplicantInformationObj.JApp1IdType2.Trim();
                }
                else
                {
                    (jointIdGridView1.Rows[1].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = "";
                }
                (jointIdGridView1.Rows[1].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1Id2;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp1IdExpire2))
                {
                    (jointIdGridView1.Rows[1].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1IdExpire2.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jointIdGridView1.Rows[1].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = "";
                }
                bool joint1IdTyeFlag3 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp1IdType3.Trim() == idTypeListObj[i].Name)
                    {
                        joint1IdTyeFlag3 = true;
                        break;
                    }
                }
                if (joint1IdTyeFlag3)
                {
                    (jointIdGridView1.Rows[2].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = joinApplicantInformationObj.JApp1IdType3.Trim();
                }
                else
                {
                    (jointIdGridView1.Rows[2].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = "";
                }
                (jointIdGridView1.Rows[2].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1Id3;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp1IdExpire3))
                {
                    (jointIdGridView1.Rows[2].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1IdExpire3.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jointIdGridView1.Rows[2].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = "";
                }
                bool joint1IdTyeFlag4 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp1IdType4.Trim() == idTypeListObj[i].Name)
                    {
                        joint1IdTyeFlag4 = true;
                        break;
                    }
                }
                if (joint1IdTyeFlag4)
                {
                    (jointIdGridView1.Rows[3].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = joinApplicantInformationObj.JApp1IdType4.Trim();
                }
                else
                {
                    (jointIdGridView1.Rows[3].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = "";
                }
                (jointIdGridView1.Rows[3].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1Id4;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp1IdExpire4))
                {
                    (jointIdGridView1.Rows[3].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1IdExpire4.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jointIdGridView1.Rows[3].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = "";
                }

                bool joint1IdTyeFlag5 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp1IdType5.Trim() == idTypeListObj[i].Name)
                    {
                        joint1IdTyeFlag5 = true;
                        break;
                    }
                }
                if (joint1IdTyeFlag5)
                {
                    (jointIdGridView1.Rows[4].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = joinApplicantInformationObj.JApp1IdType5.Trim();
                }
                else
                {
                    (jointIdGridView1.Rows[4].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text = "";
                }
                (jointIdGridView1.Rows[4].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1Id5;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp1IdExpire5))
                {
                    (jointIdGridView1.Rows[4].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = joinApplicantInformationObj.JApp1IdExpire5.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jointIdGridView1.Rows[4].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = "";
                }
                jAppNameTextBox2.Text = joinApplicantInformationObj.JointApp2Name;
                if (joinApplicantInformationObj.J2SCBAccNo1.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView2.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo1.ToString().Substring(0, 2);
                    (jAppSCBAccGridView2.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo1.ToString().Substring(2, 7);
                    (jAppSCBAccGridView2.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo1.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J2SCBAccNo1 == 0)
                    {
                        (jAppSCBAccGridView2.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView2.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo1.ToString();
                    }
                }
                if (joinApplicantInformationObj.J2SCBAccNo2.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView2.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo2.ToString().Substring(0, 2);
                    (jAppSCBAccGridView2.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo2.ToString().Substring(2, 7);
                    (jAppSCBAccGridView2.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo2.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J2SCBAccNo2 == 0)
                    {
                        (jAppSCBAccGridView2.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView2.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo2.ToString();
                    }
                }
                if (joinApplicantInformationObj.J2SCBAccNo3.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView2.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo3.ToString().Substring(0, 2);
                    (jAppSCBAccGridView2.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo3.ToString().Substring(2, 7);
                    (jAppSCBAccGridView2.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo3.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J2SCBAccNo3 == 0)
                    {
                        (jAppSCBAccGridView2.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView2.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo3.ToString();
                    }
                }
                if (joinApplicantInformationObj.J2SCBAccNo4.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView2.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo4.ToString().Substring(0, 2);
                    (jAppSCBAccGridView2.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo4.ToString().Substring(2, 7);
                    (jAppSCBAccGridView2.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo4.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J2SCBAccNo4 == 0)
                    {
                        (jAppSCBAccGridView2.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView2.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo4.ToString();
                    }
                }
                if (joinApplicantInformationObj.J2SCBAccNo5.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView2.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo5.ToString().Substring(0, 2);
                    (jAppSCBAccGridView2.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo5.ToString().Substring(2, 7);
                    (jAppSCBAccGridView2.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo5.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J2SCBAccNo5 == 0)
                    {
                        (jAppSCBAccGridView2.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView2.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo5.ToString();
                    }
                }
                if (String.IsNullOrEmpty(joinApplicantInformationObj.J2TinNo))
                {
                    jAppTinPreTextBox2.Text = "";
                    jAppTinMasterTextBox2.Text = "";
                    jAppTinPostTextBox2.Text = "";
                }
                //else if (joinApplicantInformationObj.J2TinNo.Replace("-", "").Length <= 3)
                //{
                //    jAppTinPreTextBox2.Text = joinApplicantInformationObj.J2TinNo;
                //    jAppTinMasterTextBox2.Text = "";
                //    jAppTinPostTextBox2.Text = "";
                //}
                //else if (joinApplicantInformationObj.J2TinNo.Replace("-", "").Length <= 6 && joinApplicantInformationObj.J2TinNo.Replace("-", "").Length > 3)
                //{
                //    jAppTinPreTextBox2.Text = joinApplicantInformationObj.J2TinNo.Replace("-", "").Substring(0, 3);
                //    jAppTinMasterTextBox2.Text = joinApplicantInformationObj.J2TinNo.Replace("-", "").Substring(3);
                //    jAppTinPostTextBox2.Text = "";
                //}
                else
                {
                    jAppTinPreTextBox2.Text = joinApplicantInformationObj.J2TinNo.Replace("-", "");//.Substring(0, 3);
                    jAppTinMasterTextBox2.Text = "";// joinApplicantInformationObj.J2TinNo.Replace("-", "");//.Substring(3, 3);
                    jAppTinPostTextBox2.Text = "";// joinApplicantInformationObj.J2TinNo.Replace("-", "");//.Substring(6);
                }
                (jCrdtCrdGridView2.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text = joinApplicantInformationObj.J2CrdtCrdNo1;
                (jCrdtCrdGridView2.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text = joinApplicantInformationObj.J2CrdtCrdNo2;
                (jCrdtCrdGridView2.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text = joinApplicantInformationObj.J2CrdtCrdNo3;
                (jCrdtCrdGridView2.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text = joinApplicantInformationObj.J2CrdtCrdNo4;
                (jCrdtCrdGridView2.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text = joinApplicantInformationObj.J2CrdtCrdNo5;

                jAppFatherNameTextBox2.Text = joinApplicantInformationObj.J2FatherName;
                jAppMotherNameTextBox2.Text = joinApplicantInformationObj.J2MotherName;
                if (DateFormat.IsDate(joinApplicantInformationObj.J2DOB))
                {
                    jAppDOBTextBox2.Text = joinApplicantInformationObj.J2DOB.ToString("dd-MM-yyyy");
                }
                else
                {
                    jAppDOBTextBox2.Text = "";
                }
                bool joint2EducationLevelFlag = false;
                for (int i = 0; i < educationLevelListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp2EducationLevel.Trim() == educationLevelListObj[i].Name)
                    {
                        joint2EducationLevelFlag = true;
                        break;
                    }
                }
                if (joint2EducationLevelFlag)
                {
                    jAppEducationDropDownList2.Text = joinApplicantInformationObj.JApp2EducationLevel.Trim();
                }
                else
                {
                    jAppEducationDropDownList2.Text = "";
                }

                (jAppContactGridView2.Rows[0].Cells[0].FindControl("jAppContactTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2ContactNo1;
                (jAppContactGridView2.Rows[1].Cells[0].FindControl("jAppContactTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2ContactNo2;
                (jAppContactGridView2.Rows[2].Cells[0].FindControl("jAppContactTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2ContactNo3;
                (jAppContactGridView2.Rows[3].Cells[0].FindControl("jAppContactTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2ContactNo4;

                jAppCompanyTextBox2.Text = joinApplicantInformationObj.JApp2Company;

                bool joint2IdTyeFlag1 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp2IdType1.Trim() == idTypeListObj[i].Name)
                    {
                        joint2IdTyeFlag1 = true;
                        break;
                    }
                }
                if (joint2IdTyeFlag1)
                {
                    (jAppIdGridView2.Rows[0].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = joinApplicantInformationObj.JApp2IdType1.Trim();
                }
                else
                {
                    (jAppIdGridView2.Rows[0].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = "";
                }
                (jAppIdGridView2.Rows[0].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2Id1;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp2IdExpire1))
                {
                    (jAppIdGridView2.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2IdExpire1.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView2.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = "";
                }
                bool joint2IdTyeFlag2 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp2IdType2.Trim() == idTypeListObj[i].Name)
                    {
                        joint2IdTyeFlag2 = true;
                        break;
                    }
                }
                if (joint2IdTyeFlag2)
                {
                    (jAppIdGridView2.Rows[1].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = joinApplicantInformationObj.JApp2IdType2.Trim();
                }
                else
                {
                    (jAppIdGridView2.Rows[1].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = "";
                }
                (jAppIdGridView2.Rows[1].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2Id2;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp2IdExpire2))
                {
                    (jAppIdGridView2.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2IdExpire2.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView2.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = "";
                }
                bool joint2IdTyeFlag3 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp2IdType3.Trim() == idTypeListObj[i].Name)
                    {
                        joint2IdTyeFlag3 = true;
                        break;
                    }
                }
                if (joint2IdTyeFlag3)
                {
                    (jAppIdGridView2.Rows[2].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = joinApplicantInformationObj.JApp2IdType3.Trim();
                }
                else
                {
                    (jAppIdGridView2.Rows[2].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = "";
                }
                (jAppIdGridView2.Rows[2].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2Id3;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp2IdExpire3))
                {
                    (jAppIdGridView2.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2IdExpire3.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView2.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = "";
                }
                bool joint2IdTyeFlag4 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp2IdType4.Trim() == idTypeListObj[i].Name)
                    {
                        joint2IdTyeFlag4 = true;
                        break;
                    }
                }
                if (joint2IdTyeFlag4)
                {
                    (jAppIdGridView2.Rows[3].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = joinApplicantInformationObj.JApp2IdType4.Trim();
                }
                else
                {
                    (jAppIdGridView2.Rows[3].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = "";
                }
                (jAppIdGridView2.Rows[3].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2Id4;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp2IdExpire4))
                {
                    (jAppIdGridView2.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2IdExpire4.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView2.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = "";
                }
                bool joint2IdTyeFlag5 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp2IdType5.Trim() == idTypeListObj[i].Name)
                    {
                        joint2IdTyeFlag5 = true;
                        break;
                    }
                }
                if (joint2IdTyeFlag5)
                {
                    (jAppIdGridView2.Rows[4].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = joinApplicantInformationObj.JApp2IdType5.Trim();
                }
                else
                {
                    (jAppIdGridView2.Rows[4].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text = "";
                }
                (jAppIdGridView2.Rows[4].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2Id5;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp2IdExpire5))
                {
                    (jAppIdGridView2.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = joinApplicantInformationObj.JApp2IdExpire5.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView2.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = "";
                }
                jAppNameTextBox3.Text = joinApplicantInformationObj.JointApp3Name;
                if (joinApplicantInformationObj.J3SCBAccNo1.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView3.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo1.ToString().Substring(0, 2);
                    (jAppSCBAccGridView3.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo1.ToString().Substring(2, 7);
                    (jAppSCBAccGridView3.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo1.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J3SCBAccNo1 == 0)
                    {
                        (jAppSCBAccGridView3.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView3.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo1.ToString();
                    }
                }
                if (joinApplicantInformationObj.J3SCBAccNo2.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView3.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo2.ToString().Substring(0, 2);
                    (jAppSCBAccGridView3.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo2.ToString().Substring(2, 7);
                    (jAppSCBAccGridView3.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo2.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J3SCBAccNo2 == 0)
                    {
                        (jAppSCBAccGridView3.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView3.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J2SCBAccNo2.ToString();
                    }
                }
                if (joinApplicantInformationObj.J3SCBAccNo3.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView3.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo3.ToString().Substring(0, 2);
                    (jAppSCBAccGridView3.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo3.ToString().Substring(2, 7);
                    (jAppSCBAccGridView3.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo3.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J3SCBAccNo3 == 0)
                    {
                        (jAppSCBAccGridView3.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView3.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo3.ToString();
                    }
                }
                if (joinApplicantInformationObj.J3SCBAccNo4.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView3.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo4.ToString().Substring(0, 2);
                    (jAppSCBAccGridView3.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo4.ToString().Substring(2, 7);
                    (jAppSCBAccGridView3.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo4.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J3SCBAccNo4 == 0)
                    {
                        (jAppSCBAccGridView3.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView3.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo4.ToString();
                    }
                }
                if (joinApplicantInformationObj.J3SCBAccNo5.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jAppSCBAccGridView3.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo5.ToString().Substring(0, 2);
                    (jAppSCBAccGridView3.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo5.ToString().Substring(2, 7);
                    (jAppSCBAccGridView3.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo5.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J3SCBAccNo5 == 0)
                    {
                        (jAppSCBAccGridView3.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = "";
                    }
                    else
                    {
                        (jAppSCBAccGridView3.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = joinApplicantInformationObj.J3SCBAccNo5.ToString();
                    }
                }
                if (String.IsNullOrEmpty(joinApplicantInformationObj.J3TinNo))
                {
                    jAppTinPreTextBox3.Text = "";
                    jAppTinMasterTextBox3.Text = "";
                    jAppTinPostTextBox3.Text = "";
                }
                //else if (joinApplicantInformationObj.J3TinNo.Replace("-", "").Length <= 3)
                //{
                //    jAppTinPreTextBox3.Text = joinApplicantInformationObj.J3TinNo;
                //    jAppTinMasterTextBox3.Text = "";
                //    jAppTinPostTextBox3.Text = "";
                //}
                //else if (joinApplicantInformationObj.J3TinNo.Replace("-", "").Length <= 6 && joinApplicantInformationObj.J3TinNo.Replace("-", "").Length > 3)
                //{
                //    jAppTinPreTextBox3.Text = joinApplicantInformationObj.J3TinNo.Replace("-", "").Substring(0, 3);
                //    jAppTinMasterTextBox3.Text = joinApplicantInformationObj.J3TinNo.Replace("-", "").Substring(3);
                //    jAppTinPostTextBox3.Text = "";
                //}
                //else
                //{
                //    jAppTinPreTextBox3.Text = joinApplicantInformationObj.J3TinNo.Replace("-", "").Substring(0, 3);
                //    jAppTinMasterTextBox3.Text = joinApplicantInformationObj.J3TinNo.Replace("-", "").Substring(3, 3);
                //    jAppTinPostTextBox3.Text = joinApplicantInformationObj.J3TinNo.Replace("-", "").Substring(6);
                //}
                jAppTinPreTextBox3.Text = joinApplicantInformationObj.J3TinNo;

                (jAppCrdtCrdGridView3.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text = joinApplicantInformationObj.J3CrdtCrdNo1;
                (jAppCrdtCrdGridView3.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text = joinApplicantInformationObj.J3CrdtCrdNo2;
                (jAppCrdtCrdGridView3.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text = joinApplicantInformationObj.J3CrdtCrdNo3;
                (jAppCrdtCrdGridView3.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text = joinApplicantInformationObj.J3CrdtCrdNo4;
                (jAppCrdtCrdGridView3.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text = joinApplicantInformationObj.J3CrdtCrdNo5;

                jAppFatherNameTextBox3.Text = joinApplicantInformationObj.J3FatherName;
                jAppMotherNameTextBox3.Text = joinApplicantInformationObj.J3MotherName;
                if (DateFormat.IsDate(joinApplicantInformationObj.J3DOB))
                {
                    jAppDOBTextBox3.Text = joinApplicantInformationObj.J3DOB.ToString("dd-MM-yyyy");
                }
                else
                {
                    jAppDOBTextBox3.Text = "";
                }
                bool joint3EducationLevelFlag = false;
                for (int i = 0; i < educationLevelListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp3EducationLevel.Trim() == educationLevelListObj[i].Name)
                    {
                        joint3EducationLevelFlag = true;
                        break;
                    }
                }
                if (joint3EducationLevelFlag)
                {
                    jAppEducationDropDownList3.Text = joinApplicantInformationObj.JApp3EducationLevel.Trim();
                }
                else
                {
                    jAppEducationDropDownList3.Text = "";
                }
                (jAppContactGridView3.Rows[0].Cells[0].FindControl("jAppContactTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3ContactNo1;
                (jAppContactGridView3.Rows[1].Cells[0].FindControl("jAppContactTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3ContactNo2;
                (jAppContactGridView3.Rows[2].Cells[0].FindControl("jAppContactTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3ContactNo3;
                (jAppContactGridView3.Rows[3].Cells[0].FindControl("jAppContactTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3ContactNo4;

                jAppCompanyTextBox3.Text = joinApplicantInformationObj.JApp3Company;

                bool joint3IdTyeFlag1 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp3IdType1.Trim() == idTypeListObj[i].Name)
                    {
                        joint3IdTyeFlag1 = true;
                        break;
                    }
                }
                if (joint3IdTyeFlag1)
                {
                    (jAppIdGridView3.Rows[0].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = joinApplicantInformationObj.JApp3IdType1.Trim();
                }
                else
                {
                    (jAppIdGridView3.Rows[0].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = "";
                }
                (jAppIdGridView3.Rows[0].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3Id1;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp3IdExpire1))
                {
                    (jAppIdGridView3.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3IdExpire1.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView3.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = "";
                }
                bool joint3IdTyeFlag2 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp3IdType2.Trim() == idTypeListObj[i].Name)
                    {
                        joint3IdTyeFlag2 = true;
                        break;
                    }
                }
                if (joint3IdTyeFlag2)
                {
                    (jAppIdGridView3.Rows[1].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = joinApplicantInformationObj.JApp3IdType2.Trim();
                }
                else
                {
                    (jAppIdGridView3.Rows[1].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = "";
                }
                (jAppIdGridView3.Rows[1].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3Id2;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp3IdExpire2))
                {
                    (jAppIdGridView3.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3IdExpire2.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView3.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = "";
                }
                bool joint3IdTyeFlag3 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp3IdType3.Trim() == idTypeListObj[i].Name)
                    {
                        joint3IdTyeFlag3 = true;
                        break;
                    }
                }
                if (joint3IdTyeFlag3)
                {
                    (jAppIdGridView3.Rows[2].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = joinApplicantInformationObj.JApp3IdType3.Trim();
                }
                else
                {
                    (jAppIdGridView3.Rows[2].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = "";
                }
                (jAppIdGridView3.Rows[2].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3Id3;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp3IdExpire3))
                {
                    (jAppIdGridView3.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3IdExpire3.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView3.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = "";
                }
                bool joint3IdTyeFlag4 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp3IdType4.Trim() == idTypeListObj[i].Name)
                    {
                        joint3IdTyeFlag4 = true;
                        break;
                    }
                }
                if (joint3IdTyeFlag4)
                {
                    (jAppIdGridView3.Rows[3].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = joinApplicantInformationObj.JApp3IdType4.Trim();
                }
                else
                {
                    (jAppIdGridView3.Rows[3].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = "";
                }
                (jAppIdGridView3.Rows[3].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3Id4;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp3IdExpire4))
                {
                    (jAppIdGridView3.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3IdExpire4.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView3.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = "";
                }
                bool joint3IdTyeFlag5 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp3IdType5.Trim() == idTypeListObj[i].Name)
                    {
                        joint3IdTyeFlag5 = true;
                        break;
                    }
                }
                if (joint3IdTyeFlag5)
                {
                    (jAppIdGridView3.Rows[4].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = joinApplicantInformationObj.JApp3IdType5.Trim();
                }
                else
                {
                    (jAppIdGridView3.Rows[4].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text = "";
                }
                (jAppIdGridView3.Rows[4].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3Id5;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp3IdExpire5))
                {
                    (jAppIdGridView3.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = joinApplicantInformationObj.JApp3IdExpire5.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView3.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = "";
                }

                jAppNameTextBox4.Text = joinApplicantInformationObj.JointApp4Name;
                if (joinApplicantInformationObj.J4SCBAccNo1.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jSCBAccGridView4.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo1.ToString().Substring(0, 2);
                    (jSCBAccGridView4.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo1.ToString().Substring(2, 7);
                    (jSCBAccGridView4.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo1.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J4SCBAccNo1 == 0)
                    {
                        (jSCBAccGridView4.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = "";
                    }
                    else
                    {
                        (jSCBAccGridView4.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo1.ToString();
                    }
                }
                if (joinApplicantInformationObj.J4SCBAccNo2.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jSCBAccGridView4.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo2.ToString().Substring(0, 2);
                    (jSCBAccGridView4.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo2.ToString().Substring(2, 7);
                    (jSCBAccGridView4.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo2.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J4SCBAccNo2 == 0)
                    {
                        (jSCBAccGridView4.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = "";
                    }
                    else
                    {
                        (jSCBAccGridView4.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo2.ToString();
                    }
                }
                if (joinApplicantInformationObj.J4SCBAccNo3.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jSCBAccGridView4.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo3.ToString().Substring(0, 2);
                    (jSCBAccGridView4.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo3.ToString().Substring(2, 7);
                    (jSCBAccGridView4.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo3.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J4SCBAccNo3 == 0)
                    {
                        (jSCBAccGridView4.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = "";
                    }
                    else
                    {
                        (jSCBAccGridView4.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo3.ToString();
                    }
                }
                if (joinApplicantInformationObj.J4SCBAccNo4.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jSCBAccGridView4.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo4.ToString().Substring(0, 2);
                    (jSCBAccGridView4.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo4.ToString().Substring(2, 7);
                    (jSCBAccGridView4.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo4.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J4SCBAccNo4 == 0)
                    {
                        (jSCBAccGridView4.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = "";
                    }
                    else
                    {
                        (jSCBAccGridView4.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo4.ToString();
                    }
                }
                if (joinApplicantInformationObj.J4SCBAccNo5.ToString().Length.Equals(SCBACCLENGTH))
                {
                    (jSCBAccGridView4.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo5.ToString().Substring(0, 2);
                    (jSCBAccGridView4.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo5.ToString().Substring(2, 7);
                    (jSCBAccGridView4.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo5.ToString().Substring(9, 2);
                }
                else
                {
                    if (joinApplicantInformationObj.J4SCBAccNo5 == 0)
                    {
                        (jSCBAccGridView4.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = "";
                    }
                    else
                    {
                        (jSCBAccGridView4.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = joinApplicantInformationObj.J4SCBAccNo5.ToString();
                    }
                }
                if (String.IsNullOrEmpty(joinApplicantInformationObj.J4TinNo))
                {
                    jAppTinPreTextBox4.Text = "";
                    jAppTinMasterTextBox4.Text = "";
                    jAppTinPostTextBox4.Text = "";
                }
                //else if (joinApplicantInformationObj.J4TinNo.Replace("-", "").Length <= 3)
                //{
                //    jAppTinPreTextBox4.Text = joinApplicantInformationObj.J4TinNo;
                //    jAppTinMasterTextBox4.Text = "";
                //    jAppTinPostTextBox4.Text = "";
                //}
                //else if (joinApplicantInformationObj.J4TinNo.Replace("-", "").Length <= 6 && joinApplicantInformationObj.J4TinNo.Replace("-", "").Length > 3)
                //{
                //    jAppTinPreTextBox4.Text = joinApplicantInformationObj.J4TinNo.Replace("-", "").Substring(0, 3);
                //    jAppTinMasterTextBox4.Text = joinApplicantInformationObj.J4TinNo.Replace("-", "").Substring(3);
                //    jAppTinPostTextBox4.Text = "";
                //}
                //else
                //{
                //    jAppTinPreTextBox4.Text = joinApplicantInformationObj.J4TinNo.Replace("-", "").Substring(0, 3);
                //    jAppTinMasterTextBox4.Text = joinApplicantInformationObj.J4TinNo.Replace("-", "").Substring(3, 3);
                //    jAppTinPostTextBox4.Text = joinApplicantInformationObj.J4TinNo.Replace("-", "").Substring(6);
                //}
                jAppTinPreTextBox4.Text = joinApplicantInformationObj.J4TinNo;

                (jAppCrdtCrdGridView4.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text = joinApplicantInformationObj.J4CrdtCrdNo1;
                (jAppCrdtCrdGridView4.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text = joinApplicantInformationObj.J4CrdtCrdNo2;
                (jAppCrdtCrdGridView4.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text = joinApplicantInformationObj.J4CrdtCrdNo3;
                (jAppCrdtCrdGridView4.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text = joinApplicantInformationObj.J4CrdtCrdNo4;
                (jAppCrdtCrdGridView4.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text = joinApplicantInformationObj.J4CrdtCrdNo5;

                jAppFatherNameTextBox4.Text = joinApplicantInformationObj.J4FatherName;
                jAppMotherNameTextBox4.Text = joinApplicantInformationObj.J4MotherName;
                if (DateFormat.IsDate(joinApplicantInformationObj.J4DOB))
                {
                    jAppDOBTextBox4.Text = joinApplicantInformationObj.J4DOB.ToString("dd-MM-yyyy");
                }
                else
                {
                    jAppDOBTextBox4.Text = "";
                }
                bool joint4EducationLevelFlag = false;
                for (int i = 0; i < educationLevelListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp4EducationLevel.Trim() == educationLevelListObj[i].Name)
                    {
                        joint4EducationLevelFlag = true;
                        break;
                    }
                }
                if (joint4EducationLevelFlag)
                {
                    jAppEducationDropDownList4.Text = joinApplicantInformationObj.JApp4EducationLevel.Trim();
                }
                else
                {
                    jAppEducationDropDownList4.Text = "";
                }

                (jAppContactGridView4.Rows[0].Cells[0].FindControl("jAppContactTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4ContactNo1;
                (jAppContactGridView4.Rows[1].Cells[0].FindControl("jAppContactTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4ContactNo2;
                (jAppContactGridView4.Rows[2].Cells[0].FindControl("jAppContactTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4ContactNo3;
                (jAppContactGridView4.Rows[3].Cells[0].FindControl("jAppContactTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4ContactNo4;

                jAppCompanyTextBox4.Text = joinApplicantInformationObj.JApp4Company;

                bool joint4IdTyeFlag1 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp4IdType1.Trim() == idTypeListObj[i].Name)
                    {
                        joint4IdTyeFlag1 = true;
                        break;
                    }
                }
                if (joint4IdTyeFlag1)
                {
                    (jAppIdGridView4.Rows[0].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = joinApplicantInformationObj.JApp4IdType1.Trim();
                }
                else
                {
                    (jAppIdGridView4.Rows[0].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = "";
                }
                (jAppIdGridView4.Rows[0].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4Id1;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp4IdExpire1))
                {
                    (jAppIdGridView4.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4IdExpire1.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView4.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = "";
                }
                bool joint4IdTyeFlag2 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp4IdType2.Trim() == idTypeListObj[i].Name)
                    {
                        joint4IdTyeFlag2 = true;
                        break;
                    }
                }
                if (joint4IdTyeFlag2)
                {
                    (jAppIdGridView4.Rows[1].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = joinApplicantInformationObj.JApp4IdType2.Trim();
                }
                else
                {
                    (jAppIdGridView4.Rows[1].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = "";
                }
                (jAppIdGridView4.Rows[1].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4Id2;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp4IdExpire2))
                {
                    (jAppIdGridView4.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4IdExpire2.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView4.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = "";
                }
                bool joint4IdTyeFlag3 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp4IdType4.Trim() == idTypeListObj[i].Name)
                    {
                        joint4IdTyeFlag3 = true;
                        break;
                    }
                }
                if (joint4IdTyeFlag3)
                {
                    (jAppIdGridView4.Rows[2].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = joinApplicantInformationObj.JApp4IdType3.Trim();
                }
                else
                {
                    (jAppIdGridView4.Rows[2].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = "";
                }
                (jAppIdGridView4.Rows[2].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4Id3;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp4IdExpire3))
                {
                    (jAppIdGridView4.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4IdExpire3.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView4.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = "";
                }
                bool joint4IdTyeFlag4 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp4IdType4.Trim() == idTypeListObj[i].Name)
                    {
                        joint4IdTyeFlag4 = true;
                        break;
                    }
                }
                if (joint4IdTyeFlag4)
                {
                    (jAppIdGridView4.Rows[3].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = joinApplicantInformationObj.JApp4IdType4.Trim();
                }
                else
                {
                    (jAppIdGridView4.Rows[3].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = "";
                }
                (jAppIdGridView4.Rows[3].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4Id4;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp4IdExpire4))
                {
                    (jAppIdGridView4.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4IdExpire4.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView4.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = "";
                }
                bool joint4IdTyeFlag5 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (joinApplicantInformationObj.JApp4IdType5.Trim() == idTypeListObj[i].Name)
                    {
                        joint4IdTyeFlag5 = true;
                        break;
                    }
                }
                if (joint4IdTyeFlag5)
                {
                    (jAppIdGridView4.Rows[4].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = joinApplicantInformationObj.JApp4IdType5.Trim();
                }
                else
                {
                    (jAppIdGridView4.Rows[4].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text = "";
                }
                (jAppIdGridView4.Rows[4].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4Id5;
                if (DateFormat.IsDate(joinApplicantInformationObj.JApp4IdExpire5))
                {
                    (jAppIdGridView4.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = joinApplicantInformationObj.JApp4IdExpire5.ToString("dd-MM-yyyy");
                }
                else
                {
                    (jAppIdGridView4.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = "";
                }
            }
        }
        #endregion

        #region ClearJointApplicantInfo
        /// <summary>
        /// This method provide clear all joint applicant information.
        /// </summary>
        private void ClearJointApplicantInfo()
        {
            //Applicant 1
            jointApplicantsNameTextBox1.Text = "";
            foreach (GridViewRow gvr in jointApplicantaccNoGridView1.Rows)
            {
                (gvr.FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text = "";
                (gvr.FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text = "";
                (gvr.FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text = "";
            }
            jTinPreTextBox1.Text = "";
            jTinMasterTextBox1.Text = "";
            jTinPostTextBox1.Text = "";
            foreach (GridViewRow gvr in jointCreditCardGridView1.Rows)
            {
                (gvr.FindControl("jointCardNoTextBox1") as TextBox).Text = "";
            }
            jointApplicantFatherTextBox1.Text = "";
            jointApplicantMotherTextBox1.Text = "";
            JDOBTextBox1.Text = "";
            jointEducationalDropDownList1.SelectedValue = "";
            foreach (GridViewRow gvr in jointcontactGridView1.Rows)
            {
                (gvr.FindControl("jointContactNoTextBox1") as TextBox).Text = "";
            }
            jointCompanyNameTextBox1.Text = "";
            foreach (GridViewRow gvr in jointIdGridView1.Rows)
            {
                (gvr.FindControl("jointIdTypeDropDownList1") as DropDownList).SelectedValue = "";
                (gvr.FindControl("jointIdNoTextBox1") as TextBox).Text = "";
                (gvr.FindControl("jointIdexpiryDateTextBox1") as TextBox).Text = "";
            }
            //End Applicant 1

            //Applicant 2
            jAppNameTextBox2.Text = "";
            foreach (GridViewRow gvr in jAppSCBAccGridView2.Rows)
            {
                (gvr.FindControl("jAppSCBAccPreTextBox2") as TextBox).Text = "";
                (gvr.FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text = "";
                (gvr.FindControl("jAppSCBAccPostTextBox2") as TextBox).Text = "";
            }
            jAppTinPreTextBox2.Text = "";
            jAppTinMasterTextBox2.Text = "";
            jAppTinPostTextBox2.Text = "";
            foreach (GridViewRow gvr in jCrdtCrdGridView2.Rows)
            {
                (gvr.FindControl("jAppCrdtCrdTextBox2") as TextBox).Text = "";
            }
            jAppFatherNameTextBox2.Text = "";
            jAppMotherNameTextBox2.Text = "";
            jAppDOBTextBox2.Text = "";
            jAppEducationDropDownList2.SelectedValue = "";
            foreach (GridViewRow gvr in jAppContactGridView2.Rows)
            {
                (gvr.FindControl("jAppContactTextBox2") as TextBox).Text = "";
            }
            jAppCompanyTextBox2.Text = "";
            foreach (GridViewRow gvr in jAppIdGridView2.Rows)
            {
                (gvr.FindControl("jAppIdTypeDropDownList2") as DropDownList).SelectedValue = "";
                (gvr.FindControl("jAppIdNoTextBox2") as TextBox).Text = "";
                (gvr.FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text = "";
            }
            //End Applicant 2

            //Applicant 3
            jAppNameTextBox3.Text = "";
            foreach (GridViewRow gvr in jAppSCBAccGridView3.Rows)
            {
                (gvr.FindControl("jAppSCBAccPreTextBox3") as TextBox).Text = "";
                (gvr.FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text = "";
                (gvr.FindControl("jAppSCBAccPostTextBox3") as TextBox).Text = "";
            }
            jAppTinPreTextBox3.Text = "";
            jAppTinMasterTextBox3.Text = "";
            jAppTinPostTextBox3.Text = "";
            foreach (GridViewRow gvr in jAppCrdtCrdGridView3.Rows)
            {
                (gvr.FindControl("jAppCrdtCrdTextBox3") as TextBox).Text = "";
            }
            jAppFatherNameTextBox3.Text = "";
            jAppMotherNameTextBox3.Text = "";
            jAppDOBTextBox3.Text = "";
            jAppEducationDropDownList3.SelectedValue = "";
            foreach (GridViewRow gvr in jAppContactGridView3.Rows)
            {
                (gvr.FindControl("jAppContactTextBox3") as TextBox).Text = "";
            }
            jAppCompanyTextBox3.Text = "";
            foreach (GridViewRow gvr in jAppIdGridView3.Rows)
            {
                (gvr.FindControl("jAppIdTypeTextBox3") as DropDownList).SelectedValue = "";
                (gvr.FindControl("jAppIdTextBox3") as TextBox).Text = "";
                (gvr.FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text = "";
            }
            //End Applicant 3
            //Applicant 4
            jAppNameTextBox4.Text = "";
            foreach (GridViewRow gvr in jSCBAccGridView4.Rows)
            {
                (gvr.FindControl("jAppSCBAccPreTextBox4") as TextBox).Text = "";
                (gvr.FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text = "";
                (gvr.FindControl("jAppSCBAccPostTextBox4") as TextBox).Text = "";
            }
            jAppTinPreTextBox4.Text = "";
            jAppTinMasterTextBox4.Text = "";
            jAppTinPostTextBox4.Text = "";
            foreach (GridViewRow gvr in jAppCrdtCrdGridView4.Rows)
            {
                (gvr.FindControl("jAppCrdtCrdTextBox4") as TextBox).Text = "";
            }
            jAppFatherNameTextBox4.Text = "";
            jAppMotherNameTextBox4.Text = "";
            jAppDOBTextBox4.Text = "";
            jAppEducationDropDownList4.SelectedValue = "";
            foreach (GridViewRow gvr in jAppContactGridView4.Rows)
            {
                (gvr.FindControl("jAppContactTextBox4") as TextBox).Text = "";
            }
            jAppCompanyTextBox4.Text = "";
            foreach (GridViewRow gvr in jAppIdGridView4.Rows)
            {
                (gvr.FindControl("jAppIdTypeTextBox4") as DropDownList).SelectedValue = "";
                (gvr.FindControl("jAppIdNoTextBox4") as TextBox).Text = "";
                (gvr.FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text = "";
            }
            //End Applicant 4
        }
        #endregion    

        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearJointApplicantInfo();
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                int insertOrUpdateRow = -1;
                JoinApplicantInformation joinApplicantInformationObj = new JoinApplicantInformation();

                joinApplicantInformationObj.LlId = Convert.ToInt32(LLIDTextBox.Text.Trim());
                joinApplicantInformationObj.JointApp1Name = jointApplicantsNameTextBox1.Text.Trim();

                string jAccNo1 = (jointApplicantaccNoGridView1.Rows[0].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text.Trim();
                jAccNo1 += (jointApplicantaccNoGridView1.Rows[0].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text.Trim();
                jAccNo1 += (jointApplicantaccNoGridView1.Rows[0].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1SCBAccNo1 = Convert.ToInt64("0" + jAccNo1);
                string jAccNo2 = (jointApplicantaccNoGridView1.Rows[1].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text.Trim();
                jAccNo2 += (jointApplicantaccNoGridView1.Rows[1].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text.Trim();
                jAccNo2 += (jointApplicantaccNoGridView1.Rows[1].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1SCBAccNo2 = Convert.ToInt64("0" + jAccNo2);
                string jAccNo3 = (jointApplicantaccNoGridView1.Rows[2].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text.Trim();
                jAccNo3 += (jointApplicantaccNoGridView1.Rows[2].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text.Trim();
                jAccNo3 += (jointApplicantaccNoGridView1.Rows[2].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1SCBAccNo3 = Convert.ToInt64("0" + jAccNo3);
                string jAccNo4 = (jointApplicantaccNoGridView1.Rows[3].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text.Trim();
                jAccNo4 += (jointApplicantaccNoGridView1.Rows[3].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text.Trim();
                jAccNo4 += (jointApplicantaccNoGridView1.Rows[3].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1SCBAccNo4 = Convert.ToInt64("0" + jAccNo4);
                string jAccNo5 = (jointApplicantaccNoGridView1.Rows[4].Cells[0].FindControl("jointAcNoSCBPreTextBox1") as TextBox).Text.Trim();
                jAccNo5 += (jointApplicantaccNoGridView1.Rows[4].Cells[1].FindControl("jointAcNoSCBMasterTextBox1") as TextBox).Text.Trim();
                jAccNo5 += (jointApplicantaccNoGridView1.Rows[4].Cells[2].FindControl("jointAcNoSCBPostTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1SCBAccNo5 = Convert.ToInt64("0" + jAccNo5);
                string j1tinNo = jTinPreTextBox1.Text.Trim();
                j1tinNo += jTinMasterTextBox1.Text.Trim();
                j1tinNo += jTinPostTextBox1.Text.Trim();
                joinApplicantInformationObj.J1TinNo = j1tinNo;

                joinApplicantInformationObj.J1CrdtCrdNo1 = (jointCreditCardGridView1.Rows[0].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1CrdtCrdNo2 = (jointCreditCardGridView1.Rows[1].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1CrdtCrdNo3 = (jointCreditCardGridView1.Rows[2].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1CrdtCrdNo4 = (jointCreditCardGridView1.Rows[3].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.J1CrdtCrdNo5 = (jointCreditCardGridView1.Rows[4].Cells[0].FindControl("jointCardNoTextBox1") as TextBox).Text.Trim();

                joinApplicantInformationObj.J1FatherName = jointApplicantFatherTextBox1.Text.Trim();
                joinApplicantInformationObj.J1MotherName = jointApplicantMotherTextBox1.Text.Trim();
                if (!String.IsNullOrEmpty(JDOBTextBox1.Text.Trim()))
                {
                    joinApplicantInformationObj.J1DOB = Convert.ToDateTime(JDOBTextBox1.Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp1EducationLevel = jointEducationalDropDownList1.Text.Trim();
                joinApplicantInformationObj.JApp1ContactNo1 = (jointcontactGridView1.Rows[0].Cells[0].FindControl("jointContactNoTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp1ContactNo2 = (jointcontactGridView1.Rows[1].Cells[0].FindControl("jointContactNoTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp1ContactNo3 = (jointcontactGridView1.Rows[2].Cells[0].FindControl("jointContactNoTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp1ContactNo4 = (jointcontactGridView1.Rows[3].Cells[0].FindControl("jointContactNoTextBox1") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp1Company = jointCompanyNameTextBox1.Text.Trim();

                joinApplicantInformationObj.JApp1IdType1 = (jointIdGridView1.Rows[0].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp1Id1 = (jointIdGridView1.Rows[0].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jointIdGridView1.Rows[0].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp1IdExpire1 = Convert.ToDateTime((jointIdGridView1.Rows[0].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text, new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp1IdType2 = (jointIdGridView1.Rows[1].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp1Id2 = (jointIdGridView1.Rows[1].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jointIdGridView1.Rows[1].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp1IdExpire2 = Convert.ToDateTime((jointIdGridView1.Rows[1].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp1IdType3 = (jointIdGridView1.Rows[2].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp1Id3 = (jointIdGridView1.Rows[2].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jointIdGridView1.Rows[2].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp1IdExpire3 = Convert.ToDateTime((jointIdGridView1.Rows[2].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp1IdType4 = (jointIdGridView1.Rows[3].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp1Id4 = (jointIdGridView1.Rows[3].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jointIdGridView1.Rows[3].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp1IdExpire4 = Convert.ToDateTime((jointIdGridView1.Rows[3].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp1IdType5 = (jointIdGridView1.Rows[4].Cells[0].FindControl("jointIdTypeDropDownList1") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp1Id5 = (jointIdGridView1.Rows[4].Cells[1].FindControl("jointIdNoTextBox1") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jointIdGridView1.Rows[4].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp1IdExpire5 = Convert.ToDateTime((jointIdGridView1.Rows[4].Cells[2].FindControl("jointIdexpiryDateTextBox1") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }

                joinApplicantInformationObj.JointApp2Name = jAppNameTextBox2.Text.Trim();

                string j2AccNo1 = (jAppSCBAccGridView2.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text.Trim();
                j2AccNo1 += (jAppSCBAccGridView2.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text.Trim();
                j2AccNo1 += (jAppSCBAccGridView2.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2SCBAccNo1 = Convert.ToInt64("0" + j2AccNo1);
                string j2AccNo2 = (jAppSCBAccGridView2.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text.Trim();
                j2AccNo2 += (jAppSCBAccGridView2.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text.Trim();
                j2AccNo2 += (jAppSCBAccGridView2.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2SCBAccNo2 = Convert.ToInt64("0" + j2AccNo2);
                string j2AccNo3 = (jAppSCBAccGridView2.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text.Trim();
                j2AccNo3 += (jAppSCBAccGridView2.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text.Trim();
                j2AccNo3 += (jAppSCBAccGridView2.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2SCBAccNo3 = Convert.ToInt64("0" + j2AccNo3);
                string j2AccNo4 = (jAppSCBAccGridView2.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text.Trim();
                j2AccNo4 += (jAppSCBAccGridView2.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text.Trim();
                j2AccNo4 += (jAppSCBAccGridView2.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2SCBAccNo4 = Convert.ToInt64("0" + j2AccNo4);
                string j2AccNo5 = (jAppSCBAccGridView2.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox2") as TextBox).Text.Trim();
                j2AccNo5 += (jAppSCBAccGridView2.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox2") as TextBox).Text.Trim();
                j2AccNo5 += (jAppSCBAccGridView2.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2SCBAccNo5 = Convert.ToInt64("0" + j2AccNo5);
                string j2tinNo = jAppTinPreTextBox2.Text.Trim();
                j2tinNo += jAppTinMasterTextBox2.Text.Trim();
                j2tinNo += jAppTinPostTextBox2.Text.Trim();
                joinApplicantInformationObj.J2TinNo = j2tinNo;

                joinApplicantInformationObj.J2CrdtCrdNo1 = (jCrdtCrdGridView2.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2CrdtCrdNo2 = (jCrdtCrdGridView2.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2CrdtCrdNo3 = (jCrdtCrdGridView2.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2CrdtCrdNo4 = (jCrdtCrdGridView2.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.J2CrdtCrdNo5 = (jCrdtCrdGridView2.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox2") as TextBox).Text.Trim();

                joinApplicantInformationObj.J2FatherName = jAppFatherNameTextBox2.Text.Trim();
                joinApplicantInformationObj.J2MotherName = jAppMotherNameTextBox2.Text.Trim();
                if (!String.IsNullOrEmpty(jAppDOBTextBox2.Text.Trim()))
                {
                    joinApplicantInformationObj.J2DOB = Convert.ToDateTime(jAppDOBTextBox2.Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp2EducationLevel = jAppEducationDropDownList2.Text.Trim();
                joinApplicantInformationObj.JApp2ContactNo1 = (jAppContactGridView2.Rows[0].Cells[0].FindControl("jAppContactTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp2ContactNo2 = (jAppContactGridView2.Rows[1].Cells[0].FindControl("jAppContactTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp2ContactNo3 = (jAppContactGridView2.Rows[2].Cells[0].FindControl("jAppContactTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp2ContactNo4 = (jAppContactGridView2.Rows[3].Cells[0].FindControl("jAppContactTextBox2") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp2Company = jAppCompanyTextBox2.Text.Trim();

                joinApplicantInformationObj.JApp2IdType1 = (jAppIdGridView2.Rows[0].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp2Id1 = (jAppIdGridView2.Rows[0].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView2.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp2IdExpire1 = Convert.ToDateTime((jAppIdGridView2.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp2IdType2 = (jAppIdGridView2.Rows[1].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp2Id2 = (jAppIdGridView2.Rows[1].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView2.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp2IdExpire2 = Convert.ToDateTime((jAppIdGridView2.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp2IdType3 = (jAppIdGridView2.Rows[2].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp2Id3 = (jAppIdGridView2.Rows[2].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView2.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp2IdExpire3 = Convert.ToDateTime((jAppIdGridView2.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp2IdType4 = (jAppIdGridView2.Rows[3].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp2Id4 = (jAppIdGridView2.Rows[3].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView2.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp2IdExpire4 = Convert.ToDateTime((jAppIdGridView2.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp2IdType5 = (jAppIdGridView2.Rows[4].Cells[0].FindControl("jAppIdTypeDropDownList2") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp2Id5 = (jAppIdGridView2.Rows[4].Cells[1].FindControl("jAppIdNoTextBox2") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView2.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp2IdExpire5 = Convert.ToDateTime((jAppIdGridView2.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox2") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }

                joinApplicantInformationObj.JointApp3Name = jAppNameTextBox3.Text.Trim();

                string j3AccNo1 = (jAppSCBAccGridView3.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text.Trim();
                j3AccNo1 += (jAppSCBAccGridView3.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text.Trim();
                j3AccNo1 += (jAppSCBAccGridView3.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3SCBAccNo1 = Convert.ToInt64("0" + j3AccNo1);
                string j3AccNo2 = (jAppSCBAccGridView3.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text.Trim();
                j3AccNo2 += (jAppSCBAccGridView3.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text.Trim();
                j3AccNo2 += (jAppSCBAccGridView3.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3SCBAccNo2 = Convert.ToInt64("0" + j3AccNo2);
                string j3AccNo3 = (jAppSCBAccGridView3.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text.Trim();
                j3AccNo3 += (jAppSCBAccGridView3.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text.Trim();
                j3AccNo3 += (jAppSCBAccGridView3.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3SCBAccNo3 = Convert.ToInt64("0" + j3AccNo3);
                string j3AccNo4 = (jAppSCBAccGridView3.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text.Trim();
                j3AccNo4 += (jAppSCBAccGridView3.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text.Trim();
                j3AccNo4 += (jAppSCBAccGridView3.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3SCBAccNo4 = Convert.ToInt64("0" + j3AccNo4);
                string j3AccNo5 = (jAppSCBAccGridView3.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox3") as TextBox).Text.Trim();
                j3AccNo5 += (jAppSCBAccGridView3.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox3") as TextBox).Text.Trim();
                j3AccNo5 += (jAppSCBAccGridView3.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3SCBAccNo5 = Convert.ToInt64("0" + j3AccNo5);
                string j3tinNo = jAppTinPreTextBox3.Text.Trim();
                j3tinNo += jAppTinMasterTextBox3.Text.Trim();
                j3tinNo += jAppTinPostTextBox3.Text.Trim();
                joinApplicantInformationObj.J3TinNo = j3tinNo;

                joinApplicantInformationObj.J3CrdtCrdNo1 = (jAppCrdtCrdGridView3.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3CrdtCrdNo2 = (jAppCrdtCrdGridView3.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3CrdtCrdNo3 = (jAppCrdtCrdGridView3.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3CrdtCrdNo4 = (jAppCrdtCrdGridView3.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.J3CrdtCrdNo5 = (jAppCrdtCrdGridView3.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox3") as TextBox).Text.Trim();

                joinApplicantInformationObj.J3FatherName = jAppFatherNameTextBox3.Text.Trim();
                joinApplicantInformationObj.J3MotherName = jAppMotherNameTextBox3.Text.Trim();
                if (!String.IsNullOrEmpty(jAppDOBTextBox3.Text.Trim()))
                {
                    joinApplicantInformationObj.J3DOB = Convert.ToDateTime(jAppDOBTextBox3.Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp3EducationLevel = jAppEducationDropDownList3.Text.Trim();
                joinApplicantInformationObj.JApp3ContactNo1 = (jAppContactGridView3.Rows[0].Cells[0].FindControl("jAppContactTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp3ContactNo2 = (jAppContactGridView3.Rows[1].Cells[0].FindControl("jAppContactTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp3ContactNo3 = (jAppContactGridView3.Rows[2].Cells[0].FindControl("jAppContactTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp3ContactNo4 = (jAppContactGridView3.Rows[3].Cells[0].FindControl("jAppContactTextBox3") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp3Company = jAppCompanyTextBox3.Text.Trim();

                joinApplicantInformationObj.JApp3IdType1 = (jAppIdGridView3.Rows[0].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp3Id1 = (jAppIdGridView3.Rows[0].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView3.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp3IdExpire1 = Convert.ToDateTime((jAppIdGridView3.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp3IdType2 = (jAppIdGridView3.Rows[1].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp3Id2 = (jAppIdGridView3.Rows[1].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView3.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp3IdExpire2 = Convert.ToDateTime((jAppIdGridView3.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp3IdType3 = (jAppIdGridView3.Rows[2].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp3Id3 = (jAppIdGridView3.Rows[2].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView3.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp3IdExpire3 = Convert.ToDateTime((jAppIdGridView3.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp3IdType4 = (jAppIdGridView3.Rows[3].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp3Id4 = (jAppIdGridView3.Rows[3].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView3.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp3IdExpire4 = Convert.ToDateTime((jAppIdGridView3.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp3IdType5 = (jAppIdGridView3.Rows[4].Cells[0].FindControl("jAppIdTypeTextBox3") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp3Id5 = (jAppIdGridView3.Rows[4].Cells[1].FindControl("jAppIdTextBox3") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView3.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp3IdExpire5 = Convert.ToDateTime((jAppIdGridView3.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox3") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }

                joinApplicantInformationObj.JointApp4Name = jAppNameTextBox4.Text.Trim();

                string j4AccNo1 = (jSCBAccGridView4.Rows[0].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text.Trim();
                j4AccNo1 += (jSCBAccGridView4.Rows[0].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text.Trim();
                j4AccNo1 += (jSCBAccGridView4.Rows[0].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4SCBAccNo1 = Convert.ToInt64("0" + j4AccNo1);
                string j4AccNo2 = (jSCBAccGridView4.Rows[1].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text.Trim();
                j4AccNo2 += (jSCBAccGridView4.Rows[1].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text.Trim();
                j4AccNo2 += (jSCBAccGridView4.Rows[1].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4SCBAccNo2 = Convert.ToInt64("0" + j4AccNo2);
                string j4AccNo3 = (jSCBAccGridView4.Rows[2].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text.Trim();
                j4AccNo3 += (jSCBAccGridView4.Rows[2].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text.Trim();
                j4AccNo3 += (jSCBAccGridView4.Rows[2].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4SCBAccNo3 = Convert.ToInt64("0" + j4AccNo3);
                string j4AccNo4 = (jSCBAccGridView4.Rows[3].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text.Trim();
                j4AccNo4 += (jSCBAccGridView4.Rows[3].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text.Trim();
                j4AccNo4 += (jSCBAccGridView4.Rows[3].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4SCBAccNo4 = Convert.ToInt64("0" + j4AccNo4);
                string j4AccNo5 = (jSCBAccGridView4.Rows[4].Cells[0].FindControl("jAppSCBAccPreTextBox4") as TextBox).Text.Trim();
                j4AccNo5 += (jSCBAccGridView4.Rows[4].Cells[1].FindControl("jAppSCBAccMasterTextBox4") as TextBox).Text.Trim();
                j4AccNo5 += (jSCBAccGridView4.Rows[4].Cells[2].FindControl("jAppSCBAccPostTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4SCBAccNo5 = Convert.ToInt64("0" + j4AccNo5);
                string j4tinNo = jAppTinPreTextBox4.Text.Trim();
                j4tinNo += jAppTinMasterTextBox4.Text.Trim();
                j4tinNo += jAppTinPostTextBox4.Text.Trim();
                joinApplicantInformationObj.J4TinNo = j4tinNo;

                joinApplicantInformationObj.J4CrdtCrdNo1 = (jAppCrdtCrdGridView4.Rows[0].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4CrdtCrdNo2 = (jAppCrdtCrdGridView4.Rows[1].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4CrdtCrdNo3 = (jAppCrdtCrdGridView4.Rows[2].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4CrdtCrdNo4 = (jAppCrdtCrdGridView4.Rows[3].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.J4CrdtCrdNo5 = (jAppCrdtCrdGridView4.Rows[4].Cells[0].FindControl("jAppCrdtCrdTextBox4") as TextBox).Text.Trim();

                joinApplicantInformationObj.J4FatherName = jAppFatherNameTextBox4.Text.Trim();
                joinApplicantInformationObj.J4MotherName = jAppMotherNameTextBox4.Text.Trim();
                if (!String.IsNullOrEmpty(jAppDOBTextBox4.Text.Trim()))
                {
                    joinApplicantInformationObj.J4DOB = Convert.ToDateTime(jAppDOBTextBox4.Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp4EducationLevel = jAppEducationDropDownList4.Text.Trim();
                joinApplicantInformationObj.JApp4ContactNo1 = (jAppContactGridView4.Rows[0].Cells[0].FindControl("jAppContactTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp4ContactNo2 = (jAppContactGridView4.Rows[1].Cells[0].FindControl("jAppContactTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp4ContactNo3 = (jAppContactGridView4.Rows[2].Cells[0].FindControl("jAppContactTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp4ContactNo4 = (jAppContactGridView4.Rows[3].Cells[0].FindControl("jAppContactTextBox4") as TextBox).Text.Trim();
                joinApplicantInformationObj.JApp4Company = jAppCompanyTextBox4.Text.Trim();

                joinApplicantInformationObj.JApp4IdType1 = (jAppIdGridView4.Rows[0].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp4Id1 = (jAppIdGridView4.Rows[0].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView4.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp4IdExpire1 = Convert.ToDateTime((jAppIdGridView4.Rows[0].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp4IdType2 = (jAppIdGridView4.Rows[1].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp4Id2 = (jAppIdGridView4.Rows[1].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView4.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp4IdExpire2 = Convert.ToDateTime((jAppIdGridView4.Rows[1].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp4IdType3 = (jAppIdGridView4.Rows[2].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp4Id3 = (jAppIdGridView4.Rows[2].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView4.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp4IdExpire3 = Convert.ToDateTime((jAppIdGridView4.Rows[2].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp4IdType4 = (jAppIdGridView4.Rows[3].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp4Id4 = (jAppIdGridView4.Rows[3].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView4.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp4IdExpire4 = Convert.ToDateTime((jAppIdGridView4.Rows[3].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }
                joinApplicantInformationObj.JApp4IdType5 = (jAppIdGridView4.Rows[4].Cells[0].FindControl("jAppIdTypeTextBox4") as DropDownList).Text.Trim();
                joinApplicantInformationObj.JApp4Id5 = (jAppIdGridView4.Rows[4].Cells[1].FindControl("jAppIdNoTextBox4") as TextBox).Text.Trim();
                if (!String.IsNullOrEmpty((jAppIdGridView4.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim()))
                {
                    joinApplicantInformationObj.JApp4IdExpire5 = Convert.ToDateTime((jAppIdGridView4.Rows[4].Cells[2].FindControl("jAppIdExpiryDateTextBox4") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                }

                insertOrUpdateRow = jointApplicationManagerObj.InsertORUpdateJointApplicationInfo(joinApplicantInformationObj);

                if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                {
                    errorMsgLabel.Text = "Data save sucessfully.";
                    LLIDTextBox.Focus();
                    ClearJointApplicantInfo();
                }
                else
                {
                    errorMsgLabel.Text = "Data not save sucessfully.";
                    LLIDTextBox.Focus();
                }
                new AT(this).AuditAndTraial("Joint Application", errorMsgLabel.Text);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Join Application");
            }
        }
    }
}
