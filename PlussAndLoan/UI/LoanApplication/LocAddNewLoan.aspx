﻿<%@ Page Title="Add New Loan Application" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.LoanApplication.UI_LocAddNewLoan" CodeBehind="LocAddNewLoan.aspx.cs" %>


<%@ Register Src="~/UI/UserControls/js/TextBoxDatePicker.ascx" TagPrefix="uc1" TagName="TextBoxDatePicker" %>
<%@ Register Src="~/UI/UserControls/js/Validation.ascx" TagPrefix="uc1" TagName="Validation" %>
<%@ Register Src="~/UI/UserControls/js/BranchListPopulatejs.ascx" TagPrefix="uc1" TagName="BranchListPopulatejs" %>
<%@ Register Src="~/UI/UserControls/js/jqueryLib1_3_0.ascx" TagPrefix="uc1" TagName="jqueryLib1_3_0" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <uc1:jqueryLib1_3_0 runat="server" ID="jqueryLib1_3_0" />
    <uc1:TextBoxDatePicker runat="server" ID="TextBoxDatePicker" />
    <uc1:Validation runat="server" ID="Validation" />
    <uc1:BranchListPopulatejs runat="server" ID="BranchListPopulatejs" />
    <style type="text/css">
        body {
        }

        .selectBoxArrow {
            margin-top: 1px;
            float: left;
            position: absolute;
            right: 1px;
        }

        .selectBoxInput {
            border: 0px;
            padding-left: 1px;
            height: 16px;
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .selectBox {
            border: 1px solid #7f9db9;
            height: 20px;
        }

        #ajax_listOfOptions {
            position: absolute; /* Never change this one */
            width: 254px; /* Width of box */
            height: 150px; /* Height of box */
            overflow: auto; /* Scrolling features */
            border: 1px solid #317082; /* Dark green border #317082*/
            background-color: #FFF; /* White background color */
            text-align: left;
            font-size: 10px;
            z-index: 100;
        }

            #ajax_listOfOptions div {
                /* General rule for both .optionDiv and .optionDivSelected */
                margin: 1px;
                padding: 1px;
                cursor: pointer;
                font-size: 10px;
            }

            #ajax_listOfOptions .optionDiv {
                /* Div for each item in list */
            }

            #ajax_listOfOptions .optionDivSelected {
                /* Selected item in the list */
                background-color: #317082;
                color: #FFF;
            }

        #ajax_listOfOptions_iframe {
            background-color: #F00;
            position: absolute;
            z-index: 5;
        }

        form {
            display: inline;
        }
    </style>

    <script type="text/javascript">
        function autoTabMaxLen(original, destination) {
            debugger;
            if (original.getAttribute && original.value.length == original.getAttribute("maxlength"))
                $("#" + destination.getAttribute("id")).siblings('input:visible').focus();
        }

        function showHideExtraField() {
            var hiddenFielsValue = parseInt(document.getElementById("ctl00_ContentPlaceHolder_extraFieldHiddenField").value);
            if (hiddenFielsValue == 0) {
                document.getElementById("ctl00_ContentPlaceHolder_extraFieldImg").src = "../images/minus.gif";
                document.getElementById("ctl00_ContentPlaceHolder_extraFieldHiddenField").value = 1;
                document.getElementById("extraFieldTR").style.display = '';

            }
            else {
                document.getElementById("ctl00_ContentPlaceHolder_extraFieldImg").src = "../images/plus.gif";
                document.getElementById("ctl00_ContentPlaceHolder_extraFieldHiddenField").value = 0;
                document.getElementById("extraFieldTR").style.display = 'none';

            }
        }
        function MaritalStatusChange() {
            if (document.getElementById('ctl00_ContentPlaceHolder_maritalStatusDropDownList').value == "Married") {
                document.getElementById('ctl00_ContentPlaceHolder_documentTypeTextBox').disabled = false;
                document.getElementById('ctl00_ContentPlaceHolder_spousesOccupationDropdownList').disabled = false;
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder_documentTypeTextBox').disabled = true;
                document.getElementById('ctl00_ContentPlaceHolder_spousesOccupationDropdownList').disabled = true;
            }
        }
        function autotab(original, destination) {
            debugger;
            if (original.getAttribute && original.value.length == original.getAttribute("maxlength")) {
                // $("#" + destination).focus();
                document.getElementById('ctl00_ContentPlaceHolder_' + destination).focus();
            }
            //   destination.focus();

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <modal:modalPopup runat="server" ID="modalPopup" />

    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Add New Loan Application</div>



        <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="productNameLabel" Text="PRODUCT" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList CssClass="select-field" ID="productDropDownList" runat="server" Width="231px"></asp:DropDownList>
                    <asp:Label ID="productNameMandatoryLabel" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="dateLabel" Text="APPLICATION DATE" runat="server"></asp:Label>
                </td>
                <td>
                    <input name="loan_date" runat="server" type="text" id="loan_date" style="width: 230px" maxlength="12" readonly class="required datePicCal input-field" />
                    &nbsp;
                    <asp:Label ID="dateMandatoryLabel" Text="*" runat="server" ForeColor="Red"></asp:Label>
                    &nbsp; (yyyy-MM-dd)
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="sourceLabel" Text="ENTRY FROM SOURCE" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList CssClass="select-field" ID="sourceDropDownList" runat="server" Width="231px"></asp:DropDownList>
                    &nbsp;
                    <asp:Label ID="mandatory1" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="Label1" Text="ACCOUNT NUMBER" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" runat="server" type="number" ID="txtPreAcc" Width="60px" MaxLength="2" onKeyup="autotab(this, 'txtMidAcc')"></asp:TextBox>
                    <asp:TextBox CssClass="input-field" runat="server" type="number" ID="txtMidAcc" Width="100px" MaxLength="7" onKeyup="autotab(this, 'txtPostAcc')"></asp:TextBox>
                    <asp:TextBox CssClass="input-field" runat="server" type="number" ID="txtPostAcc" Width="60px" MaxLength="2"
                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        onblur="__doPostBack('<%= txtPostAcc.ClientID  %>', '');"
                        onKeyup="autotab(this, 'chkCpf')" OnTextChanged="txtPostAcc_TextChanged"></asp:TextBox>

                    <%-- <input type="text" id="txtAccPre" style="width: 60px;" runat="server" onkeyup="autoTabMaxLen(this, ctl00_ContentPlaceHolder_txtAccMid)" maxlength="2" min="0" />
                    <input type="text" id="txtAccMid" style="width: 100px;" runat="server" onkeyup="autoTabMaxLen(this, ctl00_ContentPlaceHolder_txtAccLast)" maxlength="7" min="0" />
                    <input type="text" id="txtAccLast" style="width: 60px;" runat="server" min="0" maxlength="2" />--%>

                    <input type="checkbox" id="chkCpf" runat="server" style="visibility: hidden" />
                    <%--CPF --%>
                    <input type="checkbox" id="chkPriority" runat="server" style="visibility: hidden" />
                    <%--Priority --%>
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold;" width="200">
                    <asp:Label ID="customerNameLabel" Text="CUSTOMER NAME" runat="server"></asp:Label>
                </td>
                <td valign="top">
                    <asp:TextBox CssClass="input-field" ID="customerNameTextBox" runat="server" Width="230px"></asp:TextBox>
                    <span style="color: Red;">*</span>
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="appliedAmountLabel" Text="AMOUNT OF LOAN APPLIED" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" ID="appliedAmountTextBox" runat="server" Width="230px"></asp:TextBox>
                    <asp:Label ID="appliedAmountMandatoryLabel" Text="*" runat="server" ForeColor="Red"></asp:Label>
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="professionLabel" runat="server" Text="APPLICANT PROFESSION"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList CssClass="select-field" ID="professionDropdownList" runat="server" Width="231px">
                    </asp:DropDownList>
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="companyNameLabel" Text="ORGANIZATION" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" ID="companyNameTextBox" runat="server" Width="230px"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="Decliend" Text="If Previously Decliend ( APP. ID)" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" runat="server" type="number" ID="txtDecliendLoan" Width="230px"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="Label3" Text="SOURCE CODE (PW ID)" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" runat="server" ID="txtSourcePwId" Width="230px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="Label2" Text="ARM CODE" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" MaxLength="3" runat="server" ID="txtARMCode" Width="230px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold" valign="top">
                    <asp:Label ID="Label4" Text="SUBMITTED BY" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" runat="server" ID="txtSubmittedBy" Width="230px" ReadOnly="True"></asp:TextBox>
                </td>

            </tr>
            <tr>
                <td style="font-weight: bold">
                    <asp:Label ID="Label5" Text="SUBMISSION DATE" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="input-field" ID="txtSubmissionDate" runat="server" ReadOnly="True" Width="230px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; width: 0;" valign="top" align="left">&nbsp;</td>
                <td style="font-weight: bold;" valign="top" colspan="3">
                    <%--<div id="remarksDiv" style="overflow-y: scroll; height: 180px; width: 620px; display: none;" class="GridviewBorder">
                        <asp:GridView ID="remarksGridView" runat="server" Width="602px" AutoGenerateColumns="False" PageSize="2">
                            <Columns>
                                <asp:TemplateField HeaderText="Id" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="remarksIdLabel" runat="server" Text='<%# Eval("Id") %>' Width="30px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="selectCheckBox" runat="server" Width="15px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Check List">
                                    <ItemTemplate>
                                        <asp:TextBox CssClass="input-field" ID="remarksTextBox" runat="server" Width="590px" Text='<%# Eval("Name") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="FixedHeader" />
                        </asp:GridView>
                    </div>--%>

                    <div style="overflow-y: scroll; width: 625px; z-index: 2; border: solid 1px gray; visibility: hidden">
                        <asp:GridView ID="remarksGridView" runat="server" Width="100px" AutoGenerateColumns="False"
                            AllowPaging="False" ShowHeader="False">
                            <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />

                            <Columns>
                                <%--<asp:TemplateField HeaderText="" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="IdCheckBox" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id" HeaderStyle-Width="20px" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="580px">
                                        <ItemTemplate>
                                            <asp:TextBox ID="remarksTextBox" runat="server" Text='<%# Eval("Name") %>' Width="580px"
                                                CssClass="ssTextBox"></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle Width="580px"></HeaderStyle>
                                    </asp:TemplateField>--%>
                            </Columns>

                            <HeaderStyle CssClass="FixedHeader" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td style="width: 220px" align="center"></td>
                <td style="width: 400px" align="center">
                    <asp:Label runat="server" ID="msgLabel"></asp:Label><br />
                    <asp:Button CssClass="btn primarybtn" runat="server" Text="Save" ID="btnSaveLoanApplication" OnClick="btnSaveLoanApplication_Click" Width="123px" />
                    &nbsp;
                    <asp:Button CssClass="btn clearbtn" runat="server" Text="Clear" ID="btnClear" Width="123px" OnClick="btnClear_Click" />
                </td>
                <td style="width: 280px; padding-left: 30px;" align="left"></td>
            </tr>
        </table>
    </div>


</asp:Content>

