﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using DAL;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI.LoanApplication
{
    public partial class locVewLoanInformation : System.Web.UI.Page
    {
        readonly LocatorApplicationService _service = new LocatorApplicationService();
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int loanId = Convert.ToInt32(Request.QueryString["id"]);
                LoadLoanInformation(loanId);
            }
        }
        private void LoadLoanInformation(int id)
        {
            DateTime dateTime = Convert.ToDateTime(WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"]);
            var loan = _service.GetLoanApplications(dateTime, id);
            foreach (DataRow row in loan.Rows)
            {
                llid.InnerText = row["LOAN_APPL_ID"].ToString();
                spProduct.InnerText = row["PROD_NAME"].ToString();
                spSource.InnerText = row["BR_NAME"].ToString();
                spDept.InnerText = row["LOAN_DEPT_NAME"].ToString();
                spSubmissionDate.InnerText = Convert.ToDateTime(row["RECE_DATE"]).ToString("yyyy-MM-dd");
                spApplicationDate.InnerText = Convert.ToDateTime(row["APPL_DATE"]).ToString("yyyy-MM-dd");
                spAccNo.InnerText = row["ACC_NUMB"].ToString();
                spLoanAcc.InnerText = row["LOAN_AC_NO"].ToString();

                var accTypeId = Convert.ToInt32(row["ACC_TYPE"]);
                spAccType.InnerText = row["ACC_TYPE_NAME"].ToString();

                spSubmitBy.InnerText = row["NAME_LEARNER"].ToString();
                spAmount.InnerText = row["LOAN_APP_AMOU"].ToString();
                spConApproAmnt.InnerText = row["LOAN_COND_APRV_AMOU"].ToString();
                spApproAmnt.InnerText = row["LOAN_APRV_AMOU"].ToString();

                spCustomer.InnerText = row["CUST_NAME"].ToString();
                spOrganization.InnerText = row["ORGAN"].ToString();
                spProfession.InnerText = row["EMPL_TYPE"].ToString();
                spLoanStatus.InnerText = row["LOAN_STATUS_NAME"].ToString();
                spDecission.InnerText = row["LOAN_DECI_STATUS_Name"].ToString();

            }


            #region Other Loan Details Information

            var otherInfo = _service.GetLoanRequestDetails(Convert.ToInt32(llid.InnerText));
            if (otherInfo != null)
            {
                var table =
                    "<table class='az-gridTb' style='margin-top: -1px'><thead><tr><th>Date Time</th><th>Person Name</th><th>Dept Name</th><th>Process status</th><th>Remarks</th><th>Individual TAT</th></tr></thead>";
                int i = 0;
                foreach (DataRow row in otherInfo.Rows)
                {
                    if (i == 0)
                    {
                        var total = row["TAT"].ToString();
                        spTotatlTat.InnerText = string.IsNullOrEmpty(total) ? "0" : total;
                        i++;
                    }


                    table += "<tr>";
                    table += "<td>" + Convert.ToDateTime(row["DATE_TIME"]) + "</td>";
                    table += "<td>" + row["NAME_LEARNER"].ToString() + "</td>";
                    table += "<td>" + row["LOAN_DEPT_NAME"].ToString() + "</td>";

                    var lastDd = 0;
                    if (Convert.ToInt32(row["PROCESS_STATUS_ID"]) == 2 && lastDd == 0)
                    {
                        lastDd = 1;
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                    }
                    else if (Convert.ToInt32(row["PROCESS_STATUS_ID"]) == 6 && lastDd == 0)
                    {
                        lastDd = 1;
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                    }
                    else
                    {
                        table += "<td>" + row["LOAN_STATUS_NAME"].ToString() + "</td>";
                    }
                    table += "<td>" + row["REMARKS"].ToString() + "</td>";
                    table += "<td>" + row["TAT"].ToString() + "</td>";
                    table += "</tr>";
                }
                tdDetails.InnerHtml = table + "</table>";
            }

            #endregion

            #region CIB Report

            var cibStatus = _service.GetCibStatus(Convert.ToInt32(llid.InnerText));

            var status = DataTableToList.GetList<Cib>(cibStatus);
            Cib cib = null;
            if (status != null)
            {
                cib = new Cib();
                cib = status.FirstOrDefault();
            }
            var cibCurrentStatus = "";

            if (cib != null && cib.REMARKS == "Report Received")
            {
                cibRec.Checked = true;
                systemErr.Checked = false;
                underTaking.Checked = false;
                cibCurrentStatus = cib.REMARKS;
            }
            else if (cib != null && cib.REMARKS == "System Error")
            {
                cibRec.Checked = false;
                systemErr.Checked = true;
                underTaking.Checked = false;
                cibCurrentStatus = cib.REMARKS;
            }
            else if (cib != null && cib.REMARKS == "Undertaking Returned")
            {
                cibRec.Checked = false;
                systemErr.Checked = false;
                underTaking.Checked = true;
                cibCurrentStatus = cib.REMARKS;
            }
            if (cib != null && !string.IsNullOrEmpty(cib.REMARKS))
            {
                cibRemarks.InnerText = cib.REMARKS;
            }

            #endregion



        }

        
    }
}