﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.LoanLocatorReports;

namespace PlussAndLoan
{
    public partial class AccessLoginReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DayDropDownList.DataSource = AllDayList();
                DayDropDownList.DataBind();
                DayDropDownList.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;

                MonthDropDownList.DataSource = AllMonthList();
                MonthDropDownList.DataTextField = "Value";
                MonthDropDownList.DataValueField = "Key";
                MonthDropDownList.DataBind();
                MonthDropDownList.SelectedIndex = DateTime.Now.Month - 1;

                YearDropDownList.DataSource = AllyearList();
                YearDropDownList.DataBind();
                YearDropDownList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;

                ToDateDayList.DataSource = AllDayList();
                ToDateDayList.DataBind();
                ToDateDayList.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;

                ToDateMonthList.DataSource = AllMonthList();
                ToDateMonthList.DataTextField = "Value";
                ToDateMonthList.DataValueField = "Key";
                ToDateMonthList.DataBind();
                ToDateMonthList.SelectedIndex = DateTime.Now.Month - 1;
                ToDateYearList.DataSource = AllyearList();
                ToDateYearList.DataBind();
                ToDateYearList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            ShowReportDiv.Visible = false;
            insertDiv.Visible = true;
        }


        #region DropDown Value List

        public List<int> AllyearList()
        {
            List<int> yearList = new List<int>();
            for (int i = 2004; i < 2051; i++)
            {
                yearList.Add(i);
            }
            return yearList;

        }

        public List<int> AllDayList()
        {
            List<int> dayList = new List<int>();
            for (int i = 1; i < 32; i++)
            {
                dayList.Add(i);
            }
            return dayList;

        }

        public Dictionary<int, string> AllMonthList()
        {
            IDictionary<int, string> months = new Dictionary<int, string>();
            months.Add(1, "January");
            months.Add(2, "February");
            months.Add(3, "March");
            months.Add(4, "April");
            months.Add(5, "May");
            months.Add(6, "June");
            months.Add(7, "July");
            months.Add(8, "August");
            months.Add(9, "September");
            months.Add(10, "October");
            months.Add(11, "November");
            months.Add(12, "December");

            return (Dictionary<int, string>)months;
        }

        #endregion
        LocLoanReportManager locLoanReport = new LocLoanReportManager();
        protected void btnShow_OnClick(object sender, EventArgs e)
        {
            ShowReportDiv.Visible = true;
            insertDiv.Visible = false;

            int y1 = Convert.ToInt32(YearDropDownList.SelectedValue);
            string m1 = MonthDropDownList.SelectedValue;
            string d1 = DayDropDownList.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string fromDate = y1 + "-" + m1 + "-" + d1;


            int y2 = Convert.ToInt32(ToDateYearList.SelectedValue);
            string m2 = ToDateMonthList.SelectedValue;
            string d2 = ToDateDayList.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string toDate = y2 + "-" + m2 + "-" + d2;

            string userCode = staffIdTextBox.Text;

            var accessLoginReports =locLoanReport.AccessLoginReports(fromDate,toDate,userCode);
            AccessGridView.DataSource = accessLoginReports;
            AccessGridView.DataBind();

            fromDiv.InnerText = fromDate;
            toDiv.InnerText = toDate;
        }
    }
}