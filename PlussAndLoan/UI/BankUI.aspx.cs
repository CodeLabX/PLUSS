﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BankUI : System.Web.UI.Page
    {
        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,        
            INSERTERROR = -1,
            UPDATEERROR = -2       
        }
        #endregion
        BankInformationManager bankInformationManagerObj;
   
        protected void Page_Load(object sender, EventArgs e)
        {
            bankInformationManagerObj = new BankInformationManager();
            //DataSet ds = new DataSet();
            //ds.ReadXml(Server.MapPath("~/App_Code/BankInfo.xml"));
            //bankInfoGridView.DataSource = ds;
            //bankInfoGridView.DataBind();
            if (!IsPostBack)
            {
                FillBankInfoGridView();            
            }
        }
        public void FillBankInfoGridView()
        {
            List<BankInformation> bankInformationListObj = bankInformationManagerObj.GetAllBankInfo();
            bankInfoGridView.DataSource = bankInformationListObj;        
            bankInfoGridView.DataBind();
        }
        protected void bankInfoGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(bankInfoGridView.SelectedIndex);
            if (index > -1)
            {
                errorMsgLabel.Text = "";
                addButton.Text = "Update";
                int rowIndex = Convert.ToInt32(bankInfoGridView.SelectedRow.RowIndex);
                idLabel.Text = (bankInfoGridView.Rows[rowIndex].Cells[1].FindControl("idLabel") as Label).Text;
                //bankCodeTextBox.Text = (bankInfoGridView.Rows[rowIndex].Cells[2].FindControl("codeLabel") as Label).Text;
                bankNameTextBox.Text = (bankInfoGridView.Rows[rowIndex].Cells[3].FindControl("nameLabel") as Label).Text;
                statusDropdownList.SelectedValue = (bankInfoGridView.Rows[rowIndex].Cells[3].FindControl("statusLabel") as Label).Text;
                bankInfoGridView.Rows[rowIndex].Focus();
            }
        }   
 
        protected void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                int insertOrUpdateRow = -1;
                bool updateFlag = false;
                BankInformation bankInformationObj = new BankInformation();
                if (String.IsNullOrEmpty(bankNameTextBox.Text.Trim()))
                {
                    errorMsgLabel.Text = "Please input bank information.";
                }
                else
                {
                    errorMsgLabel.Text = "";
                    if (!String.IsNullOrEmpty(idLabel.Text.Trim()))
                    {
                        bankInformationObj.Id = Convert.ToInt32(idLabel.Text.Trim());
                        //updateFlag = true;
                    }
                    else
                    {
                        bankInformationObj.Id = 0;
                    }
            
                    bankInformationObj.Name = bankNameTextBox.Text.Trim();
                    if (bankInformationManagerObj.IsExistsBankName(bankInformationObj.Name, bankInformationObj.Id))
                    {
                        errorMsgLabel.Text = "Bank name allready exists.";
                        return;
                    }
                    errorMsgLabel.Text = "";
                    bankInformationObj.Status = Convert.ToInt32(statusDropdownList.SelectedValue);

                    insertOrUpdateRow = bankInformationManagerObj.InsertORUpdateBankInfo(bankInformationObj);

                    if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT))
                    {
                        errorMsgLabel.Text = "Data save sucessfully.";
                        addButton.Text = "Save";
                        ClearField();
                        FillBankInfoGridView();

                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERTERROR))
                    {
                        errorMsgLabel.Text = "Data not saved sucessfully.";
                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                    {
                        errorMsgLabel.Text = "Data update sucessfully.";
                        addButton.Text = "Save";
                        ClearField();
                        FillBankInfoGridView();
                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                    {
                        errorMsgLabel.Text = "Data not update sucessfully.";
                    }
                }
                new AT(this).AuditAndTraial("Bank Information", errorMsgLabel.Text);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Bank");  
            }
        }

        public void ClearField()
        {
            idLabel.Text = "";
            //bankCodeTextBox.Text = "";
            bankNameTextBox.Text = "";
            statusDropdownList.SelectedValue = "1";
        }
        protected void ClearButton_Click(object sender, EventArgs e)
        {
            ClearField();
            errorMsgLabel.Text = "";
            addButton.Text = "Save";
        }
        protected void bankInfoGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(bankInfoGridView, "Select$" + e.Row.RowIndex.ToString()));
                // e.Row.Attributes["onmouseover"] = "this.style.color = 'Black'";
                e.Row.Attributes["onmouseover"] = "this.style.cursor = 'pointer'";
                e.Row.Focus();
           
            }
        }
        protected override void Render(HtmlTextWriter writer)
        {
            for (int i = 0; i < bankInfoGridView.Rows.Count; i++)
            {
                Page.ClientScript.RegisterForEventValidation(new PostBackOptions(bankInfoGridView, "Select$" + i.ToString()));
            }
            base.Render(writer);
        }
    }
}
