<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_AuditTrailReport" Title="Audit Trail Report" CodeBehind="AuditTrailReport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">


    <script type="text/javascript">
        function pageLoad() {
            $('#ctl00_ContentPlaceHolder_startDateTextBox').unbind();
            $('#ctl00_ContentPlaceHolder_endDateTextBox').unbind();
            $('#ctl00_ContentPlaceHolder_startDateTextBox').datepicker({
                showOn: 'button',
                buttonImage: '../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            $('#ctl00_ContentPlaceHolder_endDateTextBox').datepicker({
                showOn: 'button',
                buttonImage: '../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
        }
    </script>
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Audit Trail Report</div>


        <table width="35%" border="0" cellspacing="10" cellpadding="1" cellpadding="5px">
            <tr>
                <td colspan="2">
                    <asp:Label runat="server" ID="errMsgLabel"></asp:Label>
                </td>
            </tr>
            <tr>
                
                <td style="font-weight: bold; width: 150px; text-align:right">Start Date
                </td>
                <td>
                    <asp:TextBox ID="startDateTextBox" runat="server" TabIndex="4" Width="120px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="startDateRequiredFieldValidator" runat="server" ControlToValidate="startDateTextBox"
                        ErrorMessage="Please select a date" SetFocusOnError="true" EnableClientScript="false"
                        CssClass="validation"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="sdateRegularExpressionValidator" runat="server"
                        ErrorMessage="Please select a valid date" ControlToValidate="startDateTextBox"
                        ValidationExpression="^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$"
                        SetFocusOnError="true" EnableClientScript="false" CssClass="validation"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr id="endDateRow" runat="server">
                <td style="font-weight: bold; width: 100px ; text-align:right">End Date
                </td>
                <td>
                    <asp:TextBox ID="endDateTextBox" runat="server" TabIndex="4" Width="120px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="endDateRequiredFieldValidator" runat="server"
                        ErrorMessage="Please select a valid date" ControlToValidate="endDateTextBox"
                        ValidationExpression="^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$"
                        SetFocusOnError="true" EnableClientScript="false" CssClass="validation"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 15px">&nbsp;
                </td>
            </tr>
            <tr>
               
                <td colspan="2" align="center">
                    <asp:LinkButton ID="nextButton" runat="server" Text="Audit Trail Report (PDF)" CssClass="btn primarybtn"
                        OnClick="showAuditTrailReport_Click" OnClientClick="aspnetForm.target ='_blank';"  />

                    <asp:Button ID="cancelButton" CssClass="btn primarybtn" runat="server" Text="Reset" CausesValidation="false"
                        OnClick="cancelButton_Click" />

                    <asp:Button runat="server" ID="btnReportCSV" CssClass="btn primarybtn"
                        Text="Audit Trail Report (CSV)" OnClick="btnReportCSV_Click" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>
