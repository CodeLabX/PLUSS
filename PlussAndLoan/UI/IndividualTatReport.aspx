﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="IndividualTatReport.aspx.cs" Inherits="PlussAndLoan.UI.IndividualTatReport" %>

<html runat="server">
<head>
    <title></title>
    <script language="javascript" type="text/javascript">
        function PrintDiv() {
            //debugger;
            var myContentToPrint = document.getElementById("printDiv");
            var myWindowToPrint = window.open('', '', 'width=100%,height=100%,toolbar=0,scrollbars=-1,status=0,resizable=-1,location=0,directories=0');
            myWindowToPrint.document.write(myContentToPrint.innerHTML);
            myWindowToPrint.document.close();
            myWindowToPrint.focus();
            myWindowToPrint.print();
            myWindowToPrint.close();
        }
    </script>
</head>
<body>
    <form runat="server">
        <div id="printDiv">

            <table width="100%" height="45" cellpadding="2" cellspacing="1" bordercolor="#00CCCC" style='border: 1px solid #333333;'>
                <tr bgcolor="BDD3A5">
                    <td height="20" colspan="7" align="left" valign="middle">
                        <div align="center">
                            <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
              <div id="titleDiv" runat="server" style="text-align: center; font-weight: bold; font-size: 14px">Individual TAT Report (in Calender days) </div>
              
          </font>
                        </div>
                    </td>
                    <td align="right" valign="middle" bgcolor="BDD3A5" colspan="3">
                        <asp:ImageButton ID="imgexpand" runat="server" ImageAlign="Bottom" ImageUrl="images\printer.gif" Height="25px" Width="78px" OnClientClick="PrintDiv()" />

                    </td>

                </tr>
                <tr bgcolor="BDD3A5">
                    <td width="10%" height="20" bgcolor="#D5E2C5" colspan="1">
                        <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><B>PRODUCT:</B></font></div>
                    </td>
                    <td width="23%" bgcolor="#E7F7C6" colspan="2">
                        <div align="left" id="productDiv" runat="server">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="2"> 
             
              </font>
                        </div>
                    </td>
                    <td width="10%" align="center" bgcolor="#E7F7C6" colspan="1">
                        <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><B> From</B> 
            </font></td>
                    <td width="15%" align="center" bgcolor="#D5E2C5" colspan="2">
                        <div id="formDateDiv" runat="server"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></div>
                    </td>
                    <td width="10%" align="center" bgcolor="#E7F7C6" colspan="1">
                        <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><B> TO</B> 
            </font></td>
                    <td width="11%" bgcolor="#D5E2C5">
                        <div id="toDateDiv" runat="server" colspan="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"></font></div>
                    </td>

                </tr>
            </table>



            <asp:Table ID="myTable" Width="100%" border="1" align="center" CellPadding="0" CellSpacing="0" bgcolor="#ccffcc" runat="server">
                <asp:TableRow Height="43">
                    <asp:TableCell Height="43" Width="53">LLID</asp:TableCell>
                    <asp:TableCell Width="58">Product</asp:TableCell>
                    <asp:TableCell Width="157" ColumnSpan="3">Source&nbsp;</asp:TableCell>
                    <asp:TableCell Width="368" ColumnSpan="7">Credit Operations&nbsp;</asp:TableCell>
                    <asp:TableCell Width="318" ColumnSpan="4">Asset Operations&nbsp;</asp:TableCell>
                    <asp:TableCell Width="92">Transition</asp:TableCell>
                    <asp:TableCell Width="69">Other&nbsp;</asp:TableCell>
                    <asp:TableCell Width="64">End to End TAT&nbsp;</asp:TableCell>
                </asp:TableRow>

                <asp:TableRow Height="41">
                    <asp:TableCell Height="41" Width="53">&nbsp;</asp:TableCell>
                    <asp:TableCell Width="58">&nbsp;</asp:TableCell>
                    <asp:TableCell Width="53">1</asp:TableCell>
                    <asp:TableCell Width="52">2</asp:TableCell>
                    <asp:TableCell Width="53">3</asp:TableCell>
                    <asp:TableCell Width="53">1</asp:TableCell>
                    <asp:TableCell Width="57">2</asp:TableCell>
                    <asp:TableCell Width="51">3</asp:TableCell>
                    <asp:TableCell Width="55">Hold</asp:TableCell>
                    <asp:TableCell Width="35">Valuation</asp:TableCell>
                    <asp:TableCell Width="35">Legal</asp:TableCell>
                    <asp:TableCell Width="82">Verification</asp:TableCell>
                    <asp:TableCell Width="53">1</asp:TableCell>
                    <asp:TableCell Width="57">2</asp:TableCell>
                    <asp:TableCell Width="55">3</asp:TableCell>
                    <asp:TableCell>Hold</asp:TableCell>
                    <asp:TableCell Width="92">&nbsp;</asp:TableCell>
                    <asp:TableCell Width="69">&nbsp;</asp:TableCell>
                    <asp:TableCell Width="64">&nbsp;</asp:TableCell>
                </asp:TableRow>


            </asp:Table>



            <br />






        </div>
    </form>
</body>
</html>
