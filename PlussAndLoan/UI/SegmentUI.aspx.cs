﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_SegmentUI : System.Web.UI.Page
    {
        public SegmentManager segmentManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            segmentManagerObj = new SegmentManager();
            if (!Page.IsPostBack)
            {
                LoadProfession();
                LoadData("");
            }
        }
        private void LoadProfession()
        {
            List<Profession> professionObjList = segmentManagerObj.SelectProfessionList();
            Profession professionObj = new Profession();
            professionObj.Id = 0;
            professionObj.Name = "Select Profession";
            professionObjList.Insert(0,professionObj);
            professionDropDownList.DataSource = professionObjList;
            professionDropDownList.DataTextField = "Name";
            professionDropDownList.DataValueField = "Id";
            professionDropDownList.DataBind();
        }
        private void LoadData(string searchKey)
        {
            string profession = null; 
           
            List<Segment> segmentObjList = new List<Segment>();
     
            if (searchKey == "")
            {
                segmentObjList = segmentManagerObj.SelectSegmentList();
            }
            else
            {
                segmentObjList = segmentManagerObj.SelectSegmentList();
            }
            try
            {
                var xml = "<xml id='xml_data'><rows>";
                foreach (Segment segmentObj in segmentObjList)
                {
                    xml+="<row id='" + segmentObj.Id.ToString() + "'>";

                    xml+="<cell>";
                    xml+=segmentObj.Id.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.Name.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.Code.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.ProfessionId.ToString();
                    xml+="</cell>";
                    switch (segmentObj.ProfessionId.ToString())
                    {
                        case "1":
                            profession = "Business";
                            break;
                        case "2":
                            profession = "Salaried";
                            break;
                        case "3":
                            profession = "Self Employed";
                            break;
                        case "4":
                            profession = "Doctor";
                            break;
                        case "5":
                            profession = "Landlord";
                            break;
                        default:
                            profession = "Others";
                            break;
                    }
                    xml+="<cell>";
                    xml+=profession.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.Description.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MaxLoanAmoutn.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MinLoanAmoutn.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.IR.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.Tenor.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.FirstPay.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MultiPlire.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MinAge.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MaxAge.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.DBR29.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.DBR50.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.DBR99.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.DBR1K.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L1MultiPlire29.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L1MultiPlire50.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L1MultiPlire99.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L1MultiPlire1K.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L2MultiPlire29.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L2MultiPlire50.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L2MultiPlire99.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L2MultiPlire1K.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L2TopMultiPlire29.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L2TopMultiPlire50.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L2TopMultiPlire99.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.L2TopMultiPlire1K.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MUEMul.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MUELar.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MUELoc.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MinIncome.ToString();
                    xml+="</cell>";

                    xml+="<cell>";
                    xml+=segmentObj.Experience.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.CriteriaCode.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.AssetCode.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.ACRel.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.PhysicalVerification.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.GuranteeReferenc.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.TelePhone.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=segmentObj.MaxAgeRemarks.ToString();
                    xml+="</cell>";
                    xml+="</row>";
                }
                
                xml += "</rows></xml>";
                dvXml.InnerHtml = xml;
            }
            finally
            {
              
            }
            
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                int saveSegment = 0;
                Segment segmentObj = new Segment();
                if (IsBlank())
                {
                    if (segmentIdHiddenField.Value != "")
                    {
                        segmentObj.Id = Convert.ToInt32(segmentIdHiddenField.Value);
                    }
                    segmentObj.Name = Convert.ToString(segmentNameTextBox.Text.Trim());
                    segmentObj.Code = Convert.ToString(segmentCodeTextBox.Text.Trim());
                    segmentObj.ProfessionId = Convert.ToInt32(professionDropDownList.SelectedValue.ToString());
                    segmentObj.IR = Convert.ToDouble(IRTextBox.Text.Trim());
                    segmentObj.Tenor = Convert.ToInt32(tenorTextBox.Text.Trim());
                    segmentObj.FirstPay = Convert.ToInt32(firstPayTextBox.Text.Trim());
                    segmentObj.MinLoanAmoutn = Convert.ToInt64("0" + minLoanTextBox.Text);
                    segmentObj.MaxLoanAmoutn = Convert.ToInt64("0" + maxLoanTextBox.Text);
                    segmentObj.MinIncome = Convert.ToInt64("0" + miniMumIncomeTextBox.Text);
                    segmentObj.MaxAge = Convert.ToInt32("0" + maxAgeTextBox.Text);
                    segmentObj.MinAge = Convert.ToInt32("0" + minAgeTextBox.Text);
                    segmentObj.MultiPlire = Convert.ToInt32("0" + multiplierTextBox.Text);

                    segmentObj.DBR29 = Convert.ToInt32("0" + DBR1TextBox.Text);
                    segmentObj.DBR50 = Convert.ToInt32("0" + DBR2TextBox.Text);
                    segmentObj.DBR99 = Convert.ToInt32("0" + DBR3TextBox.Text);
                    segmentObj.DBR1K = Convert.ToInt32("0" + DBR4TextBox.Text);

                    segmentObj.L1MultiPlire29 = Convert.ToInt32("0" + L1DBR1TextBox.Text);
                    segmentObj.L1MultiPlire50 = Convert.ToInt32("0" + L1DBR2TextBox.Text);
                    segmentObj.L1MultiPlire99 = Convert.ToInt32("0" + L1DBR3TextBox.Text);
                    segmentObj.L1MultiPlire1K = Convert.ToInt32("0" + L1DBR4TextBox.Text);

                    segmentObj.L2MultiPlire29 = Convert.ToInt32("0" + L2DBR1TextBox.Text);
                    segmentObj.L2MultiPlire50 = Convert.ToInt32("0" + L2DBR2TextBox.Text);
                    segmentObj.L2MultiPlire99 = Convert.ToInt32("0" + L2DBR3TextBox.Text);
                    segmentObj.L2MultiPlire1K = Convert.ToInt32("0" + L2DBR4TextBox.Text);

                    segmentObj.L2TopMultiPlire29 = Convert.ToInt32("0" + L2TopDBR1TextBox.Text);
                    segmentObj.L2TopMultiPlire50 = Convert.ToInt32("0" + L2TopDBR2TextBox.Text);
                    segmentObj.L2TopMultiPlire99 = Convert.ToInt32("0" + L2TopDBR3TextBox.Text);
                    segmentObj.L2TopMultiPlire1K = Convert.ToInt32("0" + L2TopDBR4TextBox.Text);

                    segmentObj.MUEMul = Convert.ToInt32("0" + MNCTextBox.Text);
                    segmentObj.MUELar = Convert.ToInt32("0" + LARDBRTextBox.Text);
                    segmentObj.MUELoc = Convert.ToInt32("0" + LOCDBRTextBox.Text);
                    segmentObj.MaxAgeRemarks = maxAgeRemarksTextBox.Text;
                    segmentObj.PhysicalVerification = physicalVarificationTextBox.Text;
                    segmentObj.GuranteeReferenc = guranteeReferanceTextBox.Text;
                    segmentObj.Experience = experienceTextBox.Text;
                    segmentObj.ACRel = acRelTextBox.Text;
                    segmentObj.CriteriaCode = criteriaCodeTextBox.Text;
                    segmentObj.AssetCode = assetCodeTextBox.Text;
                    segmentObj.TelePhone = telephoneTextBox.Text;
                    segmentObj.Description = segmentDescriptionTextBox.Text;
                    saveSegment = segmentManagerObj.SendSegmentInfoInToDB(segmentObj);
                    var msg = "";
                    if (saveSegment == INSERTE)
                    {
                        msg = "Save successfully";
                        LoadData("");
                        ClearField();
                    }
                    else if (saveSegment == UPDATE)
                    {
                        msg = "Update successfully";
                        LoadData("");
                        ClearField();
                    }
                    else if (saveSegment == ERROR)
                    {
                        msg = "Error in data!";
                    }
                    new AT(this).AuditAndTraial("Segment", msg);
                    Alert.Show(msg);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Segment");
            }

        }
        private bool IsBlank()
        {
            if (segmentNameTextBox.Text == "")
            {
                Alert.Show("Please input segment name");
                segmentNameTextBox.Focus();
                return false;
            }
            else if (segmentCodeTextBox.Text == "")
            {
                Alert.Show("Please input segment code");
                segmentCodeTextBox.Focus();
                return false;
            }
            else if (professionDropDownList.SelectedIndex == 0)
            {
                Alert.Show("Please select profession");
                professionDropDownList.Focus();
                return false;
            }
            else if (maxLoanTextBox.Text == "")
            {
                Alert.Show("Please input max loan amount");
                maxLoanTextBox.Focus();
                return false;
            }
            else if (IRTextBox.Text == "")
            {
                Alert.Show("Please input Interest Rate");
                IRTextBox.Focus();
                return false;
            }
            else if (tenorTextBox.Text == "")
            {
                Alert.Show("Please input Tenor");
                tenorTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearField();
        }
        private void ClearField()
        {
            segmentIdHiddenField.Value = "";
            segmentNameTextBox.Text = "";
            segmentCodeTextBox.Text = "";
            minLoanTextBox.Text = "";
            maxLoanTextBox.Text = "";
            IRTextBox.Text = "";
            tenorTextBox.Text = "";
            firstPayTextBox.Text = "";
            maxAgeTextBox.Text = "";
            minAgeTextBox.Text = "";
            multiplierTextBox.Text = "";
            DBR1TextBox.Text = "";
            DBR2TextBox.Text = "";
            DBR3TextBox.Text = "";
            DBR4TextBox.Text = "";
            L1DBR1TextBox.Text = "";
            L1DBR2TextBox.Text = "";
            L1DBR3TextBox.Text = "";
            L1DBR4TextBox.Text = "";
            L2DBR1TextBox.Text = "";
            L2DBR2TextBox.Text = "";
            L2DBR3TextBox.Text = "";
            L2DBR4TextBox.Text = "";
            L2TopDBR1TextBox.Text = "";
            L2TopDBR2TextBox.Text = "";
            L2TopDBR3TextBox.Text = "";
            L2TopDBR4TextBox.Text = "";
            MNCTextBox.Text = "";
            LARDBRTextBox.Text = "";
            LOCDBRTextBox.Text = "";
            maxAgeRemarksTextBox.Text = "";
            physicalVarificationTextBox.Text = "";
            guranteeReferanceTextBox.Text = "";
            experienceTextBox.Text = "";
            acRelTextBox.Text = "";
            criteriaCodeTextBox.Text = "";
            assetCodeTextBox.Text = "";
            telephoneTextBox.Text = "";
            segmentDescriptionTextBox.Text = "";
            miniMumIncomeTextBox.Text = "";
            professionDropDownList.SelectedIndex = 0;
        }
    }
}
