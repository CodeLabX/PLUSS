﻿using System;
using System.Collections.Generic;
using System.Web;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_SalaryUI : System.Web.UI.Page
    {
        private static double x;
        private Salary tPlussSalaryObj;
        private SalaryManager salaryManagerObj = null;
        public PLManager pLManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            printButton.Attributes.Add("onclick", "PrintDiv()");
            pLManagerObj = new PLManager();
            salaryManagerObj = new SalaryManager();

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);

            if (!IsPostBack)
            {
                orgTypeDropDownList.SelectedIndex = 0;
                x = .5;

                month1DropDownList.DataSource = MonthList();
                month1DropDownList.DataBind();
                month2DropDownList.DataSource = MonthList();
                month2DropDownList.DataBind();
                month3DropDownList.DataSource = MonthList();
                month3DropDownList.DataBind();
                mealMonthDropDownList1.DataSource = MonthList();
                mealMonthDropDownList1.DataBind();
                mealMonthDropDownList2.DataSource = MonthList();
                mealMonthDropDownList2.DataBind();
                mealMonthDropDownList3.DataSource = MonthList();
                mealMonthDropDownList3.DataBind();
                TextBox66.Text = "";
                TextBox66.Text =(string) Session["UserName"];
            }
        }


        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (llidTextBox.Text != "")
                {
                    LoanInformationManager loanInformationManagerObj = new LoanInformationManager();

                    if (loanInformationManagerObj.GetParticularApplicantLoanInfo(Convert.ToInt32(llidTextBox.Text)) != null)
                    {
                        tPlussSalaryObj = new Salary();
                        if (HiddenField1.Value.Length > 0)
                        { tPlussSalaryObj.SalaID = Convert.ToInt32(HiddenField1.Value); }
                        tPlussSalaryObj.SalaLlid = Convert.ToString(llidTextBox.Text);
                        tPlussSalaryObj.SalaCustomername = Convert.ToString(customerNameTextBox.Text);
                        tPlussSalaryObj.SalaCompanyType = Convert.ToInt32(orgTypeDropDownList.SelectedIndex);
                        tPlussSalaryObj.SalaAmount1 = Convert.ToDouble(0 + fixedSalaryTextBox.Text);
                        tPlussSalaryObj.SalaAmount2 = Convert.ToDouble(0 + lastAvgTextBox.Text);
                        tPlussSalaryObj.SalaNetincome = Convert.ToDouble(0 + netIncomeTextBox.Text);
                        tPlussSalaryObj.SalaVariablesalarymonth1 = Convert.ToString(month1DropDownList.SelectedItem.Text);
                        tPlussSalaryObj.SalaAverage1 = Convert.ToDouble(0 + total50TextBox1.Text);
                        tPlussSalaryObj.Sala5Oot1 = Convert.ToDouble(0 + ot50TextBox1.Text);
                        tPlussSalaryObj.Sala5Oconv1 = Convert.ToDouble(0 + conv50TextBox1.Text);
                        tPlussSalaryObj.Sala2Oot1 = Convert.ToDouble(0 + ot20TextBox1.Text);
                        tPlussSalaryObj.Sala2Otshift1 = Convert.ToDouble(0 + t20TextBox1.Text);
                        tPlussSalaryObj.Sala2Oothers1 = Convert.ToDouble(0 + o20TextBox1.Text);
                        tPlussSalaryObj.SalaVariablesalarymonth2 = Convert.ToString(month2DropDownList.SelectedItem.Text);
                        tPlussSalaryObj.SalaAverage2 = Convert.ToDouble(0 + total50TextBox2.Text);
                        tPlussSalaryObj.Sala5Oot2 = Convert.ToDouble(0 + ot50TextBox2.Text);
                        tPlussSalaryObj.Sala5Oconv2 = Convert.ToDouble(0 + conv50TextBox2.Text);
                        tPlussSalaryObj.Sala2Oot2 = Convert.ToDouble(0 + ot20TextBox2.Text);
                        tPlussSalaryObj.Sala2Otshift2 = Convert.ToDouble(0 + t20TextBox2.Text);
                        tPlussSalaryObj.Sala2Oothers2 = Convert.ToDouble(0 + o20TextBox2.Text);
                        tPlussSalaryObj.SalaVariablesalarymonth3 = Convert.ToString(month3DropDownList.SelectedItem.Text);
                        tPlussSalaryObj.SalaAverage3 = Convert.ToDouble(0 + total50TextBox3.Text);
                        tPlussSalaryObj.Sala5Oot3 = Convert.ToDouble(0 + ot50TextBox3.Text);
                        tPlussSalaryObj.Sala5Oconv3 = Convert.ToDouble(0 + conv50TextBox3.Text);
                        tPlussSalaryObj.Sala2Oot3 = Convert.ToDouble(0 + ot20TextBox3.Text);
                        tPlussSalaryObj.Sala2Otshift3 = Convert.ToDouble(0 + t20TextBox3.Text);
                        tPlussSalaryObj.Sala2Oothers3 = Convert.ToDouble(0 + o20TextBox3.Text);
                        tPlussSalaryObj.SalaSumofaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text);
                        tPlussSalaryObj.Sala5Ootaverage = Convert.ToDouble(0 + ot50avgTextBox.Text);
                        tPlussSalaryObj.Sala5Oconvaerage = Convert.ToDouble(0 + conv50avgTextBox.Text);
                        tPlussSalaryObj.Sala2Ootaverage = Convert.ToDouble(0 + ot20AvgTextBox.Text);
                        tPlussSalaryObj.SalaTshiftaverage = Convert.ToDouble(0 + t20AvgTextBox.Text);
                        tPlussSalaryObj.SalaOthersaverage = Convert.ToDouble(0 + o20AvgTextBox.Text);
                        tPlussSalaryObj.SalaAvgotconv = Convert.ToDouble(0 + avg50TotalTextBox2.Text);
                        tPlussSalaryObj.SalaAvgothervariable = Convert.ToDouble(0 + avg20TotalTextBox.Text);
                        tPlussSalaryObj.SalaMealallowancemonth1 = Convert.ToString(mealMonthDropDownList1.SelectedItem.Text);
                        tPlussSalaryObj.SalaUsdall1 = Convert.ToDouble(0 + usdTextBox1.Text);
                        tPlussSalaryObj.SalaBdtapp1 = Convert.ToDouble(0 + bdtTextBox1.Text);
                        tPlussSalaryObj.SalaMealallowancemonth2 = Convert.ToString(mealMonthDropDownList2.SelectedItem.Text);
                        tPlussSalaryObj.SalaUsdall2 = Convert.ToDouble(0 + usdTextBox2.Text);
                        tPlussSalaryObj.SalaBdtapp2 = Convert.ToDouble(0 + bdtTextBox2.Text);
                        tPlussSalaryObj.SalaMealallowancemonth3 = Convert.ToString(mealMonthDropDownList3.SelectedItem.Text);
                        tPlussSalaryObj.SalaUsdall3 = Convert.ToDouble(0 + usdTextBox3.Text);
                        tPlussSalaryObj.SalaBdtapp3 = Convert.ToDouble(0 + bdtTextBox3.Text);
                        tPlussSalaryObj.SalaUsdaverage = Convert.ToDouble(0 + usdAvgTextBox.Text);
                        tPlussSalaryObj.SalaBdtaverage = Convert.ToDouble(0 + bdtAvgTextBox.Text);
                        tPlussSalaryObj.SalaTkrate = Convert.ToDouble(0 + rateTextBox.Text);


                        if (saveButton.Text != "Update")
                        {
                            int status = salaryManagerObj.InsertSalary(tPlussSalaryObj);
                            if (status == 1)
                            {
                                Alert.Show("Save successful !");
                                new AT(this).AuditAndTraial("Salary", "Save successful");
                                this.clearButton_Click(sender, e);
                            }
                            else
                            {
                                Alert.Show("Un-successful !");
                                new AT(this).AuditAndTraial("Salary", "Save Un-successful");
                            }
                        }
                        else
                        {
                            int status = salaryManagerObj.UpdateSalary(tPlussSalaryObj);
                            if (status == 1)
                            {
                                Alert.Show("Update successful !");
                                new AT(this).AuditAndTraial("Salary", "Update successful");
                                this.clearButton_Click(sender, e);
                            }
                            else
                            {
                                Alert.Show("Un-successful !");
                                new AT(this).AuditAndTraial("Salary", "Update Un-successful");
                            }
                        }
                    }
                    else
                    {
                        Alert.Show("No such LLID exist in Database");
                        llidTextBox.Focus();
                    }
                }
                else
                { Alert.Show("Select an Id first !"); }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Salary");
            }
        }


        protected void searchButton_Click(object sender, EventArgs e)
        {
            if (llidTextBox.Text != "")
            {
                LoanInformationManager loanInformationManagerObj = new LoanInformationManager();
                LoanInformation loanInformationObj = new LoanInformation();
                loanInformationObj = loanInformationManagerObj.GetParticularApplicantLoanInfo(Convert.ToInt32(llidTextBox.Text));
                if (loanInformationObj != null)
                {
                    customerNameTextBox.Text = loanInformationObj.ApplicantName;
                    if (loanInformationObj.Business != null)
                    {
                        PayRollManager payRollManagerObj = new PayRollManager();
                        List<PayRoll> payRollObjList = new List<PayRoll>();
                        payRollObjList = payRollManagerObj.SearchPayRoll(loanInformationObj.Business);

                        if (payRollObjList.Count > 0)
                        {
                            if (payRollObjList[0].CompStructure.Contains("LLC") || payRollObjList[0].CompStructure.Contains("MNC"))
                            {
                                orgTypeDropDownList.SelectedIndex = 1;
                            }
                            else
                            {
                                orgTypeDropDownList.SelectedIndex = 0;
                            }
                        }
                    }
                    else
                    {
                        orgTypeDropDownList.SelectedIndex = 0;
                    }

                    tPlussSalaryObj = new Salary();
                    List<Salary> salaryObjList = new List<Salary>();
                    salaryObjList = salaryManagerObj.GetAllSalaryInfoByLlId(llidTextBox.Text.Trim());
                    if (salaryObjList.Count > 0)
                    {
                        tPlussSalaryObj = salaryObjList[0];
                        customerNameTextBox.Text = tPlussSalaryObj.SalaCustomername;
                        orgTypeDropDownList.SelectedIndex = tPlussSalaryObj.SalaCompanyType.Value;

                        if (tPlussSalaryObj.SalaAmount1 != 0)
                        { fixedSalaryTextBox.Text = tPlussSalaryObj.SalaAmount1.ToString(); }
                        if (tPlussSalaryObj.SalaAmount2 != 0)
                        { lastAvgTextBox.Text = tPlussSalaryObj.SalaAmount2.ToString(); }
                        if (tPlussSalaryObj.SalaNetincome != 0)
                        { netIncomeTextBox.Text = tPlussSalaryObj.SalaNetincome.ToString(); }
                        month1DropDownList.SelectedItem.Text = tPlussSalaryObj.SalaVariablesalarymonth1;
                    
                        if (tPlussSalaryObj.SalaAverage1 != 0)
                        { total50TextBox1.Text = tPlussSalaryObj.SalaAverage1.ToString(); }
                        if (tPlussSalaryObj.Sala5Oot1 != 0)
                        { ot50TextBox1.Text = tPlussSalaryObj.Sala5Oot1.ToString(); }
                        if (tPlussSalaryObj.Sala5Oconv1 != 0)
                        { conv50TextBox1.Text = tPlussSalaryObj.Sala5Oconv1.ToString(); }
                        if (tPlussSalaryObj.Sala2Oot1 != 0)
                        { ot20TextBox1.Text = tPlussSalaryObj.Sala2Oot1.ToString(); }
                        if (tPlussSalaryObj.Sala2Otshift1 != 0)
                        { t20TextBox1.Text = tPlussSalaryObj.Sala2Otshift1.ToString(); }
                        if (tPlussSalaryObj.Sala2Oothers1 != 0)
                        { o20TextBox1.Text = tPlussSalaryObj.Sala2Oothers1.ToString(); }
                        month2DropDownList.SelectedItem.Text = tPlussSalaryObj.SalaVariablesalarymonth2;
                    
                        if (tPlussSalaryObj.SalaAverage2 != 0)
                        { total50TextBox2.Text = tPlussSalaryObj.SalaAverage2.ToString(); }
                        if (tPlussSalaryObj.Sala5Oot2 != 0)
                        { ot50TextBox2.Text = tPlussSalaryObj.Sala5Oot2.ToString(); }
                        if (tPlussSalaryObj.Sala5Oconv2 != 0)
                        { conv50TextBox2.Text = tPlussSalaryObj.Sala5Oconv2.ToString(); }
                        if (tPlussSalaryObj.Sala2Oot2 != 0)
                        { ot20TextBox2.Text = tPlussSalaryObj.Sala2Oot2.ToString(); }
                        if (tPlussSalaryObj.Sala2Otshift2 != 0)
                        { t20TextBox2.Text = tPlussSalaryObj.Sala2Otshift2.ToString(); }
                        if (tPlussSalaryObj.Sala2Oothers2 != 0)
                        { o20TextBox2.Text = tPlussSalaryObj.Sala2Oothers2.ToString(); }
                        if (tPlussSalaryObj.Sala2Oothers2 != 0)
                        { o20TextBox2.Text = tPlussSalaryObj.Sala2Oothers2.ToString(); }
                        if (tPlussSalaryObj.Sala2Oothers2 != 0)
                        { o20TextBox2.Text = tPlussSalaryObj.Sala2Oothers2.ToString(); }
                    
                        month3DropDownList.SelectedItem.Text = tPlussSalaryObj.SalaVariablesalarymonth3;
                        if (tPlussSalaryObj.SalaAverage3 != 0)
                        { total50TextBox3.Text = tPlussSalaryObj.SalaAverage3.ToString(); }
                        if (tPlussSalaryObj.Sala5Oot3 != 0)
                        { ot50TextBox3.Text = tPlussSalaryObj.Sala5Oot3.ToString(); }
                        if (tPlussSalaryObj.Sala5Oconv3!=0)
                        { conv50TextBox3.Text = tPlussSalaryObj.Sala5Oconv3.ToString(); }
                        if (tPlussSalaryObj.Sala2Oot3!=0)
                        { ot20TextBox3.Text = tPlussSalaryObj.Sala2Oot3.ToString(); }
                        if (tPlussSalaryObj.Sala2Otshift3!=0)
                        { t20TextBox3.Text = tPlussSalaryObj.Sala2Otshift3.ToString(); }
                        if (tPlussSalaryObj.Sala2Oothers3!=0)
                        { o20TextBox3.Text = tPlussSalaryObj.Sala2Oothers3.ToString(); }
                        if (tPlussSalaryObj.SalaSumofaverage!=0)
                        { avg50TotalTextBox1.Text = tPlussSalaryObj.SalaSumofaverage.ToString(); }
                        if (tPlussSalaryObj.Sala5Ootaverage!=0)
                        { ot50avgTextBox.Text = tPlussSalaryObj.Sala5Ootaverage.ToString(); }
                        if (tPlussSalaryObj.Sala5Oconvaerage != 0)
                        { conv50avgTextBox.Text = tPlussSalaryObj.Sala5Oconvaerage.ToString(); }
                        if (tPlussSalaryObj.Sala2Ootaverage != 0)
                        { ot20AvgTextBox.Text = tPlussSalaryObj.Sala2Ootaverage.ToString(); }
                        if (tPlussSalaryObj.SalaTshiftaverage!=0)
                        { t20AvgTextBox.Text = tPlussSalaryObj.SalaTshiftaverage.ToString(); }
                        if (tPlussSalaryObj.SalaOthersaverage != 0)
                        { o20AvgTextBox.Text = tPlussSalaryObj.SalaOthersaverage.ToString(); }
                        if (tPlussSalaryObj.SalaAvgotconv != 0)
                        { avg50TotalTextBox2.Text = tPlussSalaryObj.SalaAvgotconv.ToString(); }
                        if (tPlussSalaryObj.SalaAvgothervariable!=0)
                        { avg20TotalTextBox.Text = tPlussSalaryObj.SalaAvgothervariable.ToString(); }
                        mealMonthDropDownList1.SelectedItem.Text = tPlussSalaryObj.SalaMealallowancemonth1;
                    
                        if (tPlussSalaryObj.SalaUsdall1 != 0)
                        { usdTextBox1.Text = tPlussSalaryObj.SalaUsdall1.ToString(); }
                        if (tPlussSalaryObj.SalaBdtapp1 != 0)
                        { bdtTextBox1.Text = tPlussSalaryObj.SalaBdtapp1.ToString(); }
                        mealMonthDropDownList2.SelectedItem.Text = tPlussSalaryObj.SalaMealallowancemonth2;
                    
                        if (tPlussSalaryObj.SalaUsdall2 != 0)
                        { usdTextBox2.Text = tPlussSalaryObj.SalaUsdall2.ToString(); }
                        if (tPlussSalaryObj.SalaBdtapp2 != 0)
                        { bdtTextBox2.Text = tPlussSalaryObj.SalaBdtapp2.ToString(); }
                        mealMonthDropDownList3.SelectedItem.Text = tPlussSalaryObj.SalaMealallowancemonth3;
                    
                        if (tPlussSalaryObj.SalaUsdall3 != 0)
                        { usdTextBox3.Text = tPlussSalaryObj.SalaUsdall3.ToString(); }
                        if (tPlussSalaryObj.SalaBdtapp3 != 0)
                        { bdtTextBox3.Text = tPlussSalaryObj.SalaBdtapp3.ToString(); }
                        if (tPlussSalaryObj.SalaUsdaverage != 0)
                        { usdAvgTextBox.Text = tPlussSalaryObj.SalaUsdaverage.ToString(); }
                        if (tPlussSalaryObj.SalaBdtaverage != 0)
                        { bdtAvgTextBox.Text = tPlussSalaryObj.SalaBdtaverage.ToString(); }
                        if (tPlussSalaryObj.SalaTkrate != 0)
                        { rateTextBox.Text = tPlussSalaryObj.SalaTkrate.ToString(); }


                        HiddenField1.Value = tPlussSalaryObj.SalaID.ToString();

                        orgTypeDropDownList_SelectedIndexChanged(sender, e);

                        saveButton.Text = "Update";
                        PL plObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llidTextBox.Text.Trim()));
                        if (plObj != null)
                        {
                            if (plObj.Status == 0 && plObj.UserId.ToString() == Session["Id"].ToString())
                            {
                                saveButton.Enabled = true;
                            }
                            else
                            {
                                saveButton.Enabled = false;
                            }
                        }
                    }
                }
                else
                {
                    Alert.Show("No such LLID exist in Database");
                    saveButton.Enabled = true;
                    llidTextBox.Focus();
                }
            }
        }


        protected void orgTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (orgTypeDropDownList.SelectedIndex == 0)
            { 
                x = .5;
                ParcentChangeLabel.Text = "50%";
                OtFlightAllLabel.Text="50% of (OT+FlightAll) + 50% of others";
            }
            else
            {
                ParcentChangeLabel.Text = "20%";
                OtFlightAllLabel.Text = "50% of (OT+FlightAll) + 20% of others";
                x = .2;
            }

            /*-------------------------------------------*/
            ot50TextBox1_TextChanged(sender, e);
            ot50TextBox2_TextChanged(sender, e);
            ot50TextBox3_TextChanged(sender, e);
            conv50TextBox1_TextChanged(sender, e);
            conv50TextBox2_TextChanged(sender, e);
            conv50TextBox3_TextChanged(sender, e);

            ot20TextBox1_TextChanged(sender, e);
            ot20TextBox2_TextChanged(sender, e);
            ot20TextBox3_TextChanged(sender, e);
            t20TextBox1_TextChanged(sender, e);
            t20TextBox2_TextChanged(sender, e);
            t20TextBox3_TextChanged(sender, e);
            o20TextBox1_TextChanged(sender, e);
            o20TextBox2_TextChanged(sender, e);
            o20TextBox3_TextChanged(sender, e);

            rateTextBox_TextChanged(sender, e);
            usdTextBox1_TextChanged(sender, e);
            usdTextBox2_TextChanged(sender, e);
            usdTextBox3_TextChanged(sender, e);
            fixedSalaryTextBox_TextChanged(sender, e);
            /*---------------------------------------------*/
        }


        protected void ot50TextBox1_TextChanged(object sender, EventArgs e)
        {
            //if (ot50TextBox1.Text == "")
            //{ ot50TextBox1.Text = "0"; }
            ot50avgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + ot50TextBox1.Text) + Convert.ToDouble(0 + ot50TextBox2.Text) + Convert.ToDouble(0 + ot50TextBox3.Text)) / 3);
            total50TextBox1.Text = Convert.ToString(Convert.ToDouble(0 + ot50TextBox1.Text) + Convert.ToDouble(0 + conv50TextBox1.Text));
            avg50TotalTextBox1.Text = Convert.ToString((Convert.ToDouble(0 + total50TextBox1.Text) + Convert.ToDouble(0 + total50TextBox2.Text) + Convert.ToDouble(0 + total50TextBox3.Text)) / 3);
            avg50TotalTextBox2.Text = avg50TotalTextBox1.Text;

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            ot50TextBox2.Focus();
        }
        protected void ot50TextBox2_TextChanged(object sender, EventArgs e)
        {
            //if (ot50TextBox2.Text == "")
            //{ ot50TextBox2.Text = "0"; }
            ot50avgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + ot50TextBox1.Text) + Convert.ToDouble(0 + ot50TextBox2.Text) + Convert.ToDouble(0 + ot50TextBox3.Text)) / 3);
            total50TextBox2.Text = Convert.ToString(Convert.ToDouble(0 + ot50TextBox2.Text) + Convert.ToDouble(0 + conv50TextBox2.Text));
            avg50TotalTextBox1.Text = Convert.ToString((Convert.ToDouble(0 + total50TextBox1.Text) + Convert.ToDouble(0 + total50TextBox2.Text) + Convert.ToDouble(0 + total50TextBox3.Text)) / 3);
            avg50TotalTextBox2.Text = avg50TotalTextBox1.Text;

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            ot50TextBox3.Focus();
        }
        protected void ot50TextBox3_TextChanged(object sender, EventArgs e)
        {
            //if (ot50TextBox3.Text == "")
            //{ ot50TextBox3.Text = "0"; }
            ot50avgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + ot50TextBox1.Text) + Convert.ToDouble(0 + ot50TextBox2.Text) + Convert.ToDouble(0 + ot50TextBox3.Text)) / 3);
            total50TextBox3.Text = Convert.ToString(Convert.ToDouble(0 + ot50TextBox3.Text) + Convert.ToDouble(0 + conv50TextBox3.Text));
            avg50TotalTextBox1.Text = Convert.ToString((Convert.ToDouble(0 + total50TextBox1.Text) + Convert.ToDouble(0 + total50TextBox2.Text) + Convert.ToDouble(0 + total50TextBox3.Text)) / 3);
            avg50TotalTextBox2.Text = avg50TotalTextBox1.Text;

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            conv50TextBox1.Focus();
        }

        protected void conv50TextBox1_TextChanged(object sender, EventArgs e)
        {
            //if (conv50TextBox1.Text == "")
            //{ conv50TextBox1.Text = "0"; }
            conv50avgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + conv50TextBox1.Text) + Convert.ToDouble(0 + conv50TextBox2.Text) + Convert.ToDouble(0 + conv50TextBox3.Text)) / 3);
            total50TextBox1.Text = Convert.ToString(Convert.ToDouble(0 + ot50TextBox1.Text) + Convert.ToDouble(0 + conv50TextBox1.Text));
            avg50TotalTextBox1.Text = Convert.ToString((Convert.ToDouble(0 + total50TextBox1.Text) + Convert.ToDouble(0 + total50TextBox2.Text) + Convert.ToDouble(0 + total50TextBox3.Text)) / 3);
            avg50TotalTextBox2.Text = avg50TotalTextBox1.Text;

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            conv50TextBox2.Focus();
        }
        protected void conv50TextBox2_TextChanged(object sender, EventArgs e)
        {
            //if (conv50TextBox2.Text == "")
            //{ conv50TextBox2.Text = "0"; }
            conv50avgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + conv50TextBox1.Text) + Convert.ToDouble(0 + conv50TextBox2.Text) + Convert.ToDouble(0 + conv50TextBox3.Text)) / 3);
            total50TextBox2.Text = Convert.ToString(Convert.ToDouble(0 + ot50TextBox2.Text) + Convert.ToDouble(0 + conv50TextBox2.Text));
            avg50TotalTextBox1.Text = Convert.ToString((Convert.ToDouble(0 + total50TextBox1.Text) + Convert.ToDouble(0 + total50TextBox2.Text) + Convert.ToDouble(0 + total50TextBox3.Text)) / 3);
            avg50TotalTextBox2.Text = avg50TotalTextBox1.Text;

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            conv50TextBox3.Focus();
        }
        protected void conv50TextBox3_TextChanged(object sender, EventArgs e)
        {
            //if (conv50TextBox3.Text == "")
            //{ conv50TextBox3.Text = "0"; }
            conv50avgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + conv50TextBox1.Text) + Convert.ToDouble(0 + conv50TextBox2.Text) + Convert.ToDouble(0 + conv50TextBox3.Text)) / 3);
            total50TextBox3.Text = Convert.ToString(Convert.ToDouble(0 + ot50TextBox3.Text) + Convert.ToDouble(0 + conv50TextBox3.Text));
            avg50TotalTextBox1.Text = Convert.ToString((Convert.ToDouble(0 + total50TextBox1.Text) + Convert.ToDouble(0 + total50TextBox2.Text) + Convert.ToDouble(0 + total50TextBox3.Text)) / 3);
            avg50TotalTextBox2.Text = avg50TotalTextBox1.Text;

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            ot20TextBox1.Focus();
        }


        protected void ot20TextBox1_TextChanged(object sender, EventArgs e)
        {
            //if (ot20TextBox1.Text == "")
            //{ ot20TextBox1.Text = "0"; }
            ot20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + ot20TextBox1.Text) + Convert.ToDouble(0 + ot20TextBox2.Text) + Convert.ToDouble(0 + ot20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            ot20TextBox2.Focus();
        }
        protected void ot20TextBox2_TextChanged(object sender, EventArgs e)
        {
            //if (ot20TextBox2.Text == "")
            //{ ot20TextBox2.Text = "0"; }
            ot20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + ot20TextBox1.Text) + Convert.ToDouble(0 + ot20TextBox2.Text) + Convert.ToDouble(0 + ot20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            ot20TextBox3.Focus();
        }
        protected void ot20TextBox3_TextChanged(object sender, EventArgs e)
        {
            //if (ot20TextBox3.Text == "")
            //{ ot20TextBox3.Text = "0"; }
            ot20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + ot20TextBox1.Text) + Convert.ToDouble(0 + ot20TextBox2.Text) + Convert.ToDouble(0 + ot20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            t20TextBox1.Focus();
        }

        protected void t20TextBox1_TextChanged(object sender, EventArgs e)
        {
            //if (t20TextBox1.Text == "")
            //{ t20TextBox1.Text = "0"; }
            t20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + t20TextBox1.Text) + Convert.ToDouble(0 + t20TextBox2.Text) + Convert.ToDouble(0 + t20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            t20TextBox2.Focus();
        }
        protected void t20TextBox2_TextChanged(object sender, EventArgs e)
        {
            //if (t20TextBox2.Text == "")
            //{ t20TextBox2.Text = "0"; }
            t20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + t20TextBox1.Text) + Convert.ToDouble(0 + t20TextBox2.Text) + Convert.ToDouble(0 + t20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            t20TextBox3.Focus();
        }
        protected void t20TextBox3_TextChanged(object sender, EventArgs e)
        {
            //if (t20TextBox3.Text == "")
            //{ t20TextBox3.Text = "0"; }
            t20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + t20TextBox1.Text) + Convert.ToDouble(0 + t20TextBox2.Text) + Convert.ToDouble(0 + t20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            o20TextBox1.Focus();
        }

        protected void o20TextBox1_TextChanged(object sender, EventArgs e)
        {
            //if (o20TextBox1.Text == "")
            //{ o20TextBox1.Text = "0"; }
            o20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + o20TextBox1.Text) + Convert.ToDouble(0 + o20TextBox2.Text) + Convert.ToDouble(0 + o20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            o20TextBox2.Focus();
        }
        protected void o20TextBox2_TextChanged(object sender, EventArgs e)
        {
            //if (o20TextBox2.Text == "")
            //{ o20TextBox2.Text = "0"; }
            o20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + o20TextBox1.Text) + Convert.ToDouble(0 + o20TextBox2.Text) + Convert.ToDouble(0 + o20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            o20TextBox3.Focus();
        }
        protected void o20TextBox3_TextChanged(object sender, EventArgs e)
        {
            //if (o20TextBox3.Text == "")
            //{ o20TextBox3.Text = "0"; }
            o20AvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + o20TextBox1.Text) + Convert.ToDouble(0 + o20TextBox2.Text) + Convert.ToDouble(0 + o20TextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            rateTextBox.Focus();
        }



        protected void usdTextBox1_TextChanged(object sender, EventArgs e)
        {
            //if (usdTextBox1.Text == "")
            //{ usdTextBox1.Text = "0"; }
            usdAvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + usdTextBox1.Text) + Convert.ToDouble(0 + usdTextBox2.Text) + Convert.ToDouble(0 + usdTextBox3.Text)) / 3);
            bdtTextBox1.Text = Convert.ToString(Convert.ToDouble(0 + usdTextBox1.Text) * Convert.ToDouble(0 + rateTextBox.Text));
            bdtAvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + bdtTextBox1.Text) + Convert.ToDouble(0 + bdtTextBox2.Text) + Convert.ToDouble(0 + bdtTextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            usdTextBox2.Focus();
        }
        protected void usdTextBox2_TextChanged(object sender, EventArgs e)
        {
            //if (usdTextBox2.Text == "")
            //{ usdTextBox2.Text = "0"; }
            usdAvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + usdTextBox1.Text) + Convert.ToDouble(0 + usdTextBox2.Text) + Convert.ToDouble(0 + usdTextBox3.Text)) / 3);
            bdtTextBox2.Text = Convert.ToString(Convert.ToDouble(0 + usdTextBox2.Text) * Convert.ToDouble(0 + rateTextBox.Text));
            bdtAvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + bdtTextBox1.Text) + Convert.ToDouble(0 + bdtTextBox2.Text) + Convert.ToDouble(0 + bdtTextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            usdTextBox3.Focus();
        }
        protected void usdTextBox3_TextChanged(object sender, EventArgs e)
        {
            //if (usdTextBox3.Text == "")
            //{ usdTextBox3.Text = "0"; }
            usdAvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + usdTextBox1.Text) + Convert.ToDouble(0 + usdTextBox2.Text) + Convert.ToDouble(0 + usdTextBox3.Text)) / 3);
            bdtTextBox3.Text = Convert.ToString(Convert.ToDouble(0 + usdTextBox3.Text) * Convert.ToDouble(0 + rateTextBox.Text));
            bdtAvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + bdtTextBox1.Text) + Convert.ToDouble(0 + bdtTextBox2.Text) + Convert.ToDouble(0 + bdtTextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            saveButton.Focus();
        }


        protected void rateTextBox_TextChanged(object sender, EventArgs e)
        {
            //if (rateTextBox.Text == "")
            //{ rateTextBox.Text = "0"; }
            usdAvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + usdTextBox1.Text) + Convert.ToDouble(0 + usdTextBox2.Text) + Convert.ToDouble(0 + usdTextBox3.Text)) / 3);
            bdtTextBox1.Text = Convert.ToString(Convert.ToDouble(0 + usdTextBox1.Text) * Convert.ToDouble(0 + rateTextBox.Text));
            bdtTextBox2.Text = Convert.ToString(Convert.ToDouble(0 + usdTextBox2.Text) * Convert.ToDouble(0 + rateTextBox.Text));
            bdtTextBox3.Text = Convert.ToString(Convert.ToDouble(0 + usdTextBox3.Text) * Convert.ToDouble(0 + rateTextBox.Text));
            bdtAvgTextBox.Text = Convert.ToString((Convert.ToDouble(0 + bdtTextBox1.Text) + Convert.ToDouble(0 + bdtTextBox2.Text) + Convert.ToDouble(0 + bdtTextBox3.Text)) / 3);
            avg20TotalTextBox.Text = Convert.ToString(Convert.ToDouble(0 + ot20AvgTextBox.Text) + Convert.ToDouble(0 + o20AvgTextBox.Text) + Convert.ToDouble(0 + t20AvgTextBox.Text) + Convert.ToDouble(0 + bdtAvgTextBox.Text));

            double lastaverage = Convert.ToDouble(0 + avg50TotalTextBox1.Text) / 2;
            lastaverage = lastaverage + Convert.ToDouble(0 + avg20TotalTextBox.Text) * x;
            lastAvgTextBox.Text = Math.Round(lastaverage, MidpointRounding.AwayFromZero).ToString();
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(lastAvgTextBox.Text));
            usdTextBox1.Focus();
        }


        protected void fixedSalaryTextBox_TextChanged(object sender, EventArgs e)
        {
            //if (fixedSalaryTextBox.Text == "")
            //{ fixedSalaryTextBox.Text = "0"; }
            //if (lastAvgTextBox.Text == "")
            //{ netIncomeTextBox.Text = }
            //else { }
            netIncomeTextBox.Text = Convert.ToString(Convert.ToInt32(Convert.ToDouble(0 + fixedSalaryTextBox.Text)) + Convert.ToInt32(0+lastAvgTextBox.Text));
        }


        protected void clearButton_Click(object sender, EventArgs e)
        {
            ot50TextBox1.Text = "";
            ot50TextBox2.Text = "";
            ot50TextBox3.Text = "";
            conv50TextBox1.Text = "";
            conv50TextBox2.Text = "";
            conv50TextBox3.Text = "";

            ot20TextBox1.Text = "";
            ot20TextBox2.Text = "";
            ot20TextBox3.Text = "";
            t20TextBox1.Text = "";
            t20TextBox2.Text = "";
            t20TextBox3.Text = "";
            o20TextBox1.Text = "";
            o20TextBox2.Text = "";
            o20TextBox3.Text = "";

            ot50avgTextBox.Text = "";
            conv50avgTextBox.Text = "";
            ot20AvgTextBox.Text = "";
            o20AvgTextBox.Text = "";
            t20AvgTextBox.Text = "";

            total50TextBox1.Text = "";
            total50TextBox2.Text = "";
            total50TextBox3.Text = "";
            avg50TotalTextBox1.Text = "";
            avg50TotalTextBox2.Text = "";
            avg20TotalTextBox.Text = "";

            rateTextBox.Text = "";
            usdTextBox1.Text = "";
            usdTextBox2.Text = "";
            usdTextBox3.Text = "";
            usdAvgTextBox.Text = "";
            bdtTextBox1.Text = "";
            bdtTextBox2.Text = "";
            bdtTextBox3.Text = "";
            bdtAvgTextBox.Text = "";

            fixedSalaryTextBox.Text = "";
            lastAvgTextBox.Text = "";
            netIncomeTextBox.Text = "";

            llidTextBox.Text = "";
            customerNameTextBox.Text = "";

            saveButton.Text = "Save";

            orgTypeDropDownList.SelectedIndex = 0;
            saveButton.Enabled = true;
        }

        public List<string> MonthList()
        {
            List<string> monthList = new List<string>();
            DateTime dt = new DateTime();
            dt = DateTime.Now;

            for (int i = 5; i > 0; i--)
            {
                monthList.Add(dt.ToString("MMM-yy"));
                dt = dt.AddMonths(-1);
            }
            return monthList;
        }

        protected void inputToPLButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (netIncomeTextBox.Text != "" && lastAvgTextBox.Text != "")
                {
                    Session["CreditTurnOver"] = "";
                    Session["AverageBalance"] = "";
                    Session["CreditTurnOver"] = netIncomeTextBox.Text.Trim();
                    Session["AverageBalance"] = lastAvgTextBox.Text.Trim();

                    Alert.Show("OK Go to PL and Click Refresh");
                }
                else
                {
                    Alert.Show("Please Input necessary data");
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Input to PL");
            }
        }
    }
}
