﻿using System;
using BLL;
using BusinessEntities;
using DAL;
using AzUtilities;
using System.IO;
using AzUtilities.Common;
using System.Data;
using System.Linq;
using BLL.Report;

namespace PlussAndLoan.UI
{
    public partial class UI_SecurityMatrixReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User userT = (User)Session["UserDetails"];
            if (userT == null)
            {
                Response.Redirect("~/LoginUI.aspx");
            }
        }
        protected void btnSecurityMatrix_Click(object sender, EventArgs e)
        {
            try
            {
                Session["path"] = "SecurityMatrixReport.rdlc";
                Session["ReportDataSourceName"] = "DataSet1";
                Session["rdlcData"] = new SecurityMatrixReportManager().GetSecurityMatrixReportData();
                new AT(this).AuditAndTraial("Security Matix (PDF)", "Report");
                Response.Redirect("~/ReportRDLC/ReportWizards/RdlcReportViewer.aspx");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

       
        protected void btnSecurityMatrixCSV_Click(object sender, EventArgs e)
        {
            try
            {
                var data = new SecurityMatrixReportManager().GetSecurityMatrixReportData();
                new AT(this).AuditAndTraial("Security Matix (CSV)", "Report");
                PrintDump(data, "Security-Matrix-Report");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        private void PrintDump(DataTable dt, string reportName)
        {
            try
            {
                var filename = reportName + DateTime.Now.Ticks + ".csv";
                var csv = string.Empty;
                if (dt != null)
                {
                    csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ','));
                    csv += "\r\n";

                    foreach (DataRow row in dt.Rows)
                    {
                        csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", " ").Replace(";", " ").Replace("&amp", "&").Replace("&AMP", "&").Replace("amp", " ").Replace("AMP", " ") + ','));
                        csv += "\r\n";
                    }
                }

                MemoryStream memoryStream = new MemoryStream();
                TextWriter textWriter = new StreamWriter(memoryStream);
                textWriter.Write(csv);
                textWriter.Flush(); // added this line
                byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.BinaryWrite(bytesInStream);

            }
            catch (Exception e)
            {
                CustomException.Save(e, "Security Matrix Report :: - PrintDump");
                LogFile.WriteLog(e);
            }
            Response.End();
        }


        
    }
}
