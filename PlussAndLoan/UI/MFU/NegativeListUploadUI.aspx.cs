﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using BLL;
using BusinessEntities;
using DAL;
using System.Web.Configuration;
using AzUtilities.Common;

namespace PlussAndLoan.UI.MFU
{
    public partial class UI_NegativeListUploadUI : System.Web.UI.Page
    {
        public DeDupInfoManager deDupInfoManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        Int32 rowsPerPage = 100;
        Int32 pageNum = 1;
        Int32 offset = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            deDupInfoManagerObj = new DeDupInfoManager();
            if (Request.QueryString.Count != 0)
            {
                pageNum = Convert.ToInt32(Request.QueryString["pageIndex"]);
                offset = (pageNum - 1) * rowsPerPage;
            }
            BindData();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "onLoadPage();", true);
        }
        #region Old Bad Code
        //private void BindData(Int32 offset, Int32 rowsPerPage)
        //{
        //    List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
        //    string type = "";
        //    type = criteriaDropDownList.Text;
        //    deDupInfoObjList = deDupInfoManagerObj.GetUploadResult(DateTime.Now, DateTime.Now, type, offset, rowsPerPage);
        //    StringBuilder headTable = new StringBuilder();
        //    StringBuilder viewTable = new StringBuilder();
        //    StringBuilder pagingTable = new StringBuilder();
        //    if (deDupInfoObjList != null)
        //    {
        //        headTable.Append("<table id='dataTable' name='dataTable' bordercolor='#F3F2F7' width='100%'  border='0px' cellpadding='0px' cellspacing='0px' >");
        //        headTable.Append("<thead>");
        //        headTable.Append("<tr bgcolor='#FOEEE4'>");
        //        headTable.Append("<td style='width:50px;'><b>SL.</b></td>");
        //        headTable.Append("<td style='width:200px;'><b>Name</b></td>");
        //        headTable.Append("<td style='width:200px;'><b>Father's Name</b></td>");
        //        headTable.Append("<td style='width:200px;'><b>Mother's Name</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>DOB</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Master No</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Declin Reson</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Declin Date</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Business Name</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Add1</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Add2</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Ph1</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Ph2</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Mob1</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Mob2</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Source</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Status</b></td>");
        //        headTable.Append("<td style='width:90px;'><b>Remarks</b></td>");
        //        headTable.Append("</tr>");
        //        headTable.Append("</thead>");
        //        //headTable.Append("</table>");
        //        //tableHeadDiv.InnerHtml = headTable.ToString();
        //        //viewTable.Append("<table id='dataTable' width='100%'>");
        //        viewTable.Append(headTable);
        //        viewTable.Append("<tbody>");
        //        foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
        //        {
        //            viewTable.Append("<tr>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Id.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Name.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.FatherName.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.MotherName.ToString() + "</td>");
        //            if (deDupInfoObj.DateOfBirth.ToString() != "")
        //            {
        //                viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DateOfBirth.ToString("dd-MM-yyyy") + "</td>");
        //            }
        //            else
        //            {
        //                viewTable.Append("<td class='TdStyle'>" + "" + "</td>");
        //            }
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.AccountNo.MasterAccNo1.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DeclineReason.ToString() + "</td>");
        //            if (deDupInfoObj.DeclineDate.ToString() != "")
        //            {
        //                viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DeclineDate.ToString("dd-MM-yyyy") + "</td>");
        //            }
        //            else
        //            {
        //                viewTable.Append("<td class='TdStyle'>" + "" + "</td>");
        //            }
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.BusinessEmployeeName.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Address1.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Address2.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo1.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo2.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo3.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo4.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Source.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Status.ToString() + "</td>");
        //            viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Remarks.ToString() + "</td>");
        //            viewTable.Append("</tr>");
        //        }
        //        viewTable.Append("</tbody>");
        //        viewTable.Append("</table>");
        //        recordViewDiv.InnerHtml = viewTable.ToString();
        //        Int32 numrows = deDupInfoManagerObj.GetTolalRecord(DateTime.Now, DateTime.Now, type);
        //        Int32 maxPage = numrows / rowsPerPage;
        //        maxPage = maxPage + 1;
        //        string nav = "";
        //        string prev = "";
        //        string first = "";
        //        string next = "";
        //        string last = "";
        //        Int32 page = 0;
        //        for (page = 1; page <= maxPage; page++)
        //        {
        //            if (page == pageNum)
        //            {
        //                nav = page.ToString();
        //            }
        //            else
        //            {
        //                nav += " <a href=../UI/NegativeListUploadUI.aspx?pageIndex=" + page.ToString() + ">" + page.ToString() + "</a> ";
        //            }
        //        }
        //        if (pageNum > 1)
        //        {
        //            page = pageNum - 1;
        //            prev = " <a href=../UI/NegativeListUploadUI.aspx?pageIndex=" + page.ToString() + ">Prev</a> ";
        //            first = " <a href=../UI/NegativeListUploadUI.aspx?pageIndex=1>First Page</a>";
        //        }
        //        else
        //        {
        //            prev = "&nbsp;";
        //            first = "&nbsp;";
        //        }

        //        if (pageNum < maxPage)
        //        {
        //            page = pageNum + 1;
        //            next = " <a href=../UI/NegativeListUploadUI.aspx?pageIndex=" + page + ">Next</a> ";

        //            last = " <a href=../UI/NegativeListUploadUI.aspx?pageIndex=" + maxPage + ">Last Page</a> ";
        //        }
        //        else
        //        {
        //            next = "&nbsp;";
        //            last = "&nbsp;";
        //        }
        //        pagingTable.Append("<table width='100%'>");
        //        pagingTable.Append("<tr>");
        //        pagingTable.Append("<td>" + first.ToString() + prev.ToString() + nav.ToString() + next.ToString() + last.ToString() + "</td>");
        //        pagingTable.Append("</tr>");
        //        pagingTable.Append("</table>");
        //        pagingDiv.InnerHtml = pagingTable.ToString();

        //    }
        //} 
        #endregion

        private void BindData()
        {
            List<DeDupInfo> list = new List<DeDupInfo>();
            string type = "";
            type = criteriaDropDownList.Text;
            list = deDupInfoManagerObj.GetTempDedupNegativeList();


            gvRecords.DataSource = list;
            gvRecords.DataBind();

        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string path = WebConfigurationManager.AppSettings["NegativeList_Uploaded_File_Path"];
                if (FileUpload.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload.FileName);
                    path += fileName;
                    FileUpload.SaveAs(path);
                    ReadExcleFile(path);
                    BindData();
                    new AT(this).AuditAndTraial("Negative List File Upload", "Upload");
                }
                else
                {
                    //Alert.Show("Please select Excel file");
                    modalPopup.ShowSuccess("Please select the Excel(xlsx) file ", "Negative List Upload");
                    FileUpload.Focus();
                }
            }
            catch (Exception exception)
            {
                modalPopup.ShowError(exception.Message, "Negative List Upload");
                CustomException.Save(exception, "Negative List Upload");
            }
        }
        private void ReadExcleFile(string fileName)
        {
            long slNo = 1;
            int returnvalue = 0;
            long recordCount = 0;
            bool emptyRow = true;
            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            if (fileName != "")
            {
                //string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source=" + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));

                //string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source=" + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));
                string cnnStr = ("Provider=Microsoft.ACE.OLEDB.12.0;" + ("Data Source=" + (fileName + (";" + "Extended Properties=\"Excel 12.0;HDR=YES\";"))));
                //string cnnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=Excel 12.0;";

                DataTable table = new DataTable();
                OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);
                var error = "";
                int row = 0;
                try
                {
                    dataAdapterObj.Fill(table);
                    foreach (DataRow dr in table.Rows)
                    {
                        error = "";
                        row++;
                        DeDupInfo deDupInfoObj = new DeDupInfo();
                        //if (Convert.ToString(dr[0]) != "")
                        //{
                        if (dr[0].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Name = dr[0].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Name is not in correct format" + "( at Row #" + row + ")");
                            }
                        }
                        if (dr[2].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.FatherName = dr[2].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Father Name is not in correct format" + "( at Row #" + row + ")");
                            }

                        }

                        if (dr[3].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.MotherName = dr[3].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Mother Name is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[4].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                if (!String.IsNullOrEmpty(dr[4].ToString()))
                                {
                                    deDupInfoObj.DateOfBirth = Convert.ToDateTime(dr[4].ToString());
                                }
                                else
                                {
                                    deDupInfoObj.DateOfBirth = DateTime.Now;
                                }
                            }
                            catch (Exception e)
                            {
                                error = "Date Of Birth is not in correct format in Negative List upload excel" + "( at Row #" + row + ")";
                                CustomException.Save(e, error);
                                throw new Exception(error);

                            }

                        }
                        if (dr[1].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.ScbMaterNo = dr[1].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("SCB Mater No is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[5].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.DeclineReason = dr[5].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Decline Reason is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[6].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                if (!String.IsNullOrEmpty(dr[6].ToString()))
                                {
                                    deDupInfoObj.DeclineDate = Convert.ToDateTime(dr[6].ToString());
                                }
                                else
                                {
                                    deDupInfoObj.DeclineDate = DateTime.Now;
                                }
                            }
                            catch (Exception e)
                            {
                                error = "Decline Date is not in correct format in Negative List upload excel" + "( at Row #" + row + ")";
                                CustomException.Save(e, error);
                                throw new Exception(error);

                            }

                        }
                        if (dr[7].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.BusinessEmployeeName = dr[7].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Business Employee Name is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[8].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Address1 = dr[8].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Address1 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[9].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Address2 = dr[9].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Address2 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[10].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Phone1 = dr[10].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Phone1 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[11].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Phone2 = dr[11].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Phone2 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[12].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Mobile1 = dr[12].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Mobile1 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[13].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Mobile2 = dr[13].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Mobile2 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[14].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                if (!String.IsNullOrEmpty(dr[14].ToString()))
                                {
                                    deDupInfoObj.ProductId = deDupInfoManagerObj.GetProductId(dr[14].ToString());
                                }
                                else
                                {
                                    deDupInfoObj.ProductId = 0;
                                }
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Product is not in correct" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[15].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.BankBranch = dr[15].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Branch is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[16].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Source = dr[16].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Source is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        deDupInfoObj.Status = "N";
                        if (deDupInfoObj.Status != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Status = criteriaDropDownList.Text;//dr[17].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Status is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[17].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.CriteriaType = dr[17].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Criteria Type is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[18].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.Remarks = dr[18].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Remarks is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[19].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                if (!String.IsNullOrEmpty(dr[19].ToString()))
                                {
                                    deDupInfoObj.MfuTin = dr[19].ToString();
                                }
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Tin is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[20].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdType = dr[20].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdType is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[21].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdNo = dr[21].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdNo is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[22].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdType1 = dr[22].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdType1 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[23].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdNo1 = dr[23].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdNo1 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[24].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdType2 = dr[24].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdType2 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[25].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdNo2 = dr[25].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdNo2 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[26].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdType3 = dr[26].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdType3 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[27].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdNo3 = dr[27].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdNo3 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[28].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdType4 = dr[28].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdType4 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[29].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.IdNo4 = dr[29].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("IdNo4 is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[30].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.ResidentaileStatus = dr[30].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Residentail Status is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[31].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.EducationalLevel = dr[31].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Educational Level is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        if (dr[32].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.MaritalStatus = dr[32].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Marital Status is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        //if (dr[33].ToString() != "")
                        //{
                            try
                            {
                                emptyRow = false;
                                deDupInfoObj.LoanAccountNo = dr[33].ToString();
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Loan Account No is not in correct format" + "( at Row #" + row + ")");
                            }

                        //}
                        if (deDupInfoObj.Active != "")
                        {
                            try
                            {
                                deDupInfoObj.Active = "Y";
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Active is not in correct format" + "( at Row #" + row + ")");
                            }

                        }
                        
                        if (emptyRow == false)
                        {

                            try
                            {
                                deDupInfoObj.EntryDate = DateTime.Now;
                                deDupInfoObjList.Add(deDupInfoObj);
                                emptyRow = true;
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Entry date is not in correct format" + "( at Row #" + row + ")");
                            }
                        }
                    } // foreach ends here

                    if (deDupInfoObjList.Count > 0)
                    {
                        UploadDNCInfoList(deDupInfoObjList);
                        deDupInfoObjList.Clear();
                    }
                    else
                    {
                        modalPopup.ShowSuccess("The list came empty", "Negative List");
                    }

                }
                catch (Exception ex)
                {
                    var message = ex.Message;
                    modalPopup.ShowSuccess(message, "Negative List");
                    CustomException.Save(ex, "Negative List Upload Failed");
                }
            }
            
        }
        private void UploadDNCInfoList(List<DeDupInfo> deDupInfoObjList)
        {
            int ListInsert = 0;
            if (deDupInfoObjList.Count > 0)
            {
                User actionUser = (User)Session["UserDetails"];
                ListInsert = deDupInfoManagerObj.InsertIntoDeDupNegativeTemp(deDupInfoObjList, actionUser);
            }
            if (ListInsert == INSERTE || ListInsert == UPDATE)
            {
                modalPopup.ShowSuccess("File has been Successfully Uploaded", "Negative List File Upload");
            }
            else
            {
                modalPopup.ShowSuccess("Error in data Please check excel file", "Negative List");
            }
        }
        
        protected void btnFlush_Click(object sender, EventArgs e)
        {
            try
            {
                string result = deDupInfoManagerObj.FlushDeDupNegativListTemp();

                if (result == Operation.Success.ToString())
                {

                    BindData();
                    modalPopup.ShowSuccess("Unapproved data has been successfully removed", "Negative List Upload");
                }
                else
                {
                    modalPopup.ShowError("There was an error while Flushing the temp data", "Negative List Upload");
                }
            }
            catch (Exception ex)
            {
                //modalPopup.ShowSuccess(ex.Message, "Negative List File Upload");
                modalPopup.ShowError("Error :: " + ex.Message, "Negative List File Upload");
            }
        }

        
    }
}
