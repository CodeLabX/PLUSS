﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;
using System.Web.Configuration;
using Microsoft.VisualBasic.FileIO;
using System.Text.RegularExpressions;
using AzUtilities;
using CsvUtilities;

namespace PlussAndLoan.UI.MFU
{
    public partial class MFUApprove : System.Web.UI.Page
    {
        public DeDupInfoManager _manager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _manager = new DeDupInfoManager();
            SetData();
        }
        #region Commented
        //protected void uploadButton_Click(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        string fileName = "";
        //        string message = "";
        //        string activity = "";
        //        string path = WebConfigurationManager.AppSettings["MFU_Uploaded_File_Path"];
        //        var uploadDate = new DateTime();
        //        var fileUploadDate = txtUploadDate.Text;
        //        if (fileUploadDate != "")
        //        {
        //            uploadDate = Convert.ToDateTime(fileUploadDate);
        //        }
        //        else
        //        {
        //            uploadDate = DateTime.Now;
        //        }
        //        var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
        //        if (FileUpload.HasFile)
        //        {
        //            fileName = Server.HtmlEncode(FileUpload.FileName);
        //            if (!Directory.Exists(path))
        //                Directory.CreateDirectory(path);

        //            path = Path.Combine(path, fileName);


        //            FileUpload.SaveAs(path);

        //            LogFile.WriteLine(DateTime.Now.ToString() + " File has been successfully uploaded");

        //            var rowNom = ReadExcleFile(path);
        //            if (rowNom > 0)
        //            {
        //                message = rowNom + " MFU data uploaded successfully....";
        //                Alert.Show(message);
        //                activity = "Success";
        //            }
        //            else
        //            {
        //                message = "MFU upload Failed";
        //                Alert.Show(message);
        //                activity = "Failed";
        //            }
        //            _manager.SaveMFU_UploadResult(fileName, uploadDate, userId, rowNom, activity);
        //            new AT(this).AuditAndTraial("MFU Uploaded", "Upload");
        //        }
        //        else
        //        {
        //            Alert.Show("Please select CSV file");
        //            FileUpload.Focus();
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        CustomException.Save(exception, "MFU Upload");
        //    }

        //    UploadButton.Enabled = true;
        //    messageLabel.Text = "";
        //}
        //private int ReadExcleFile(string fileName)
        //{
        //    User userSession = (User)Session["UserDetails"];
        //    int rowIndex = 0;
        //    int inserted = 0;
        //    try
        //    {
        //        if (fileName != "")
        //        {
        //            #region Old blamed CODE
        //            //string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source=" + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));
        //            //string cnnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=Excel 12.0;";
        //            //string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source=" + (fileName + (";" + "Extended Properties='Text;HDR=Yes;FMT=CSVDelimited'"))));
        //            //string cnnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}\\;Extended Properties='Text;HDR=Yes;FMT=CSVDelimited'";
        //            //cnnStr = String.Format(cnnStr, Path.GetDirectoryName(fileName));
        //            //var dataTableObj = new DataTable();
        //            //var dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);

        //            //OleDbDataAdapter dataAdapterObj;
        //            //dataAdapterObj = new OleDbDataAdapter("SELECT * FROM [" + Path.GetFileName(fileName) + "]", cnnStr);
        //            //table = GetDataTableFromCSVFile(fileName); 
        //            #endregion

        //            TxtFileUtility utility = new TxtFileUtility();

        //            DataTable table = GetTableFromFile(fileName);
        //            inserted = utility.InsertIntoDataBaseFromTable(table, userSession);

        //            #region Second Old
        //            //DataTable table = new DataTable();
        //            //using (StreamReader reader = new StreamReader(fileName))
        //            //{
        //            //    string line = "";

        //            //    if ((line = reader.ReadLine()) == null)
        //            //    {
        //            //        return 0;
        //            //    }

        //            //    rowIndex++;

        //            //    while ((line = reader.ReadLine()) != null)
        //            //    {
        //            //        rowIndex++;

        //            //        bool temp = Regex.IsMatch(line, "\"[^\"]*(?:\"\"[^\"]*)*\"");

        //            //        string line2 = Regex.Replace(line, "^\"(?:[^\"\n]|(\n+))*\"", "/");

        //            //        string[] array = ParseLine(new MemoryStream(Encoding.ASCII.GetBytes(line2)));

        //            //        list.Add(GetDeDupInfoFromArray(array, rowIndex));

        //            //        if (list.Count >= 999)
        //            //        {
        //            //            inserted += DeDupInfoManagerObj.SendDedupeInToFinalTable(list);
        //            //            list = new List<DeDupInfo>();
        //            //        }
        //            //    }
        //            //    if (list != null)
        //            //    {
        //            //        inserted += DeDupInfoManagerObj.SendDedupeInToFinalTable(list);
        //            //    }
        //            //}
        //            //using (TextFieldParser csvParser = new TextFieldParser(fileName))
        //            //{
        //            //    csvParser.CommentTokens = new string[] { "#" };
        //            //    csvParser.SetDelimiters(new string[] { "," });
        //            //    csvParser.HasFieldsEnclosedInQuotes = true;

        //            //    // Skip the row with the column names
        //            //    csvParser.ReadLine();
        //            //    rowIndex++;

        //            //    while (!csvParser.EndOfData)
        //            //    {
        //            //        rowIndex++;

        //            //        // Read current line fields, pointer moves to the next line.
        //            //        string[] line = csvParser.ReadFields();

        //            //        if (line.Length != 38)
        //            //            return -1;

        //            //        list.Add(GetDeDupInfoFromArray(line, rowIndex));

        //            //        if (list.Count >= 999)
        //            //        {
        //            //            inserted += DeDupInfoManagerObj.SendDedupeInToFinalTable(list);
        //            //            list = new List<DeDupInfo>();
        //            //        }
        //            //    }
        //            //    if (list != null)
        //            //    {
        //            //        inserted += DeDupInfoManagerObj.SendDedupeInToFinalTable(list);
        //            //    }
        //            //} 
        //            #endregion

        //            #region Commented
        //            //using (StreamReader sr = new StreamReader(fileName))
        //            //{
        //            //    string line;

        //            //    if ((line = sr.ReadLine()) == null)
        //            //    {
        //            //        rowIndex++;
        //            //        Alert.Show("There is now row to insert");
        //            //        return 0;
        //            //    }

        //            //    List<DeDupInfo> list = new List<DeDupInfo>();
        //            //    // currentLine will be null when the StreamReader reaches the end of file
        //            //    while ((line = sr.ReadLine()) != null)
        //            //    {
        //            //        rowIndex++;
        //            //        //TextReader reader = new StringReader(line);
        //            //        //TextFieldParser fieldParser = new TextFieldParser(reader);

        //            //        //fieldParser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
        //            //        //fieldParser.SetDelimiters(",");

        //            //        string[] seperatedStringArray = Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*^[\/]*$)");

        //            //        for (int i = 0; i < seperatedStringArray.Length; i++)
        //            //        {
        //            //            seperatedStringArray[i] = seperatedStringArray[i].Replace("\"", "");
        //            //        }

        //            //        if (seperatedStringArray.Length < 38)
        //            //            return 0;

        //            //        list.Add(GetDeDupInfoFromArray(seperatedStringArray, rowIndex));

        //            //        if (list.Count >= 999)
        //            //        {
        //            //            inserted += DeDupInfoManagerObj.SendDedupeInToFinalTable(list);
        //            //            list = new List<DeDupInfo>();
        //            //        }
        //            //    }

        //            //    if (list != null)
        //            //    {
        //            //        inserted += DeDupInfoManagerObj.SendDedupeInToFinalTable(list);
        //            //    }
        //            //} 
        //            #endregion

        //        }
        //        return inserted;
        //    }
        //    catch (Exception ex)
        //    {
        //        string mess = ex.Message;
        //        Alert.Show(mess);
        //        LogFile.WriteLine("MFU Upload Failed at row index " + rowIndex);
        //        LogFile.WriteLog(ex);
        //        CustomException.Save(ex, "MFU Upload Failed at row index " + rowIndex);
        //        return 0;
        //    }
        //}

        //#region Old Slow Upload
        //private DeDupInfo GetDeDupInfoFromArray(string[] row, int rowIndex)
        //{
        //    string error = "";
        //    DeDupInfo deDupInfo = new DeDupInfo();

        //    if (row[0].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Name = row[0].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[1].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.FatherName = row[1].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Father Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[2].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.MotherName = row[2].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Mother Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[3].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.ScbMaterNo = row[3].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Scb Mater No is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[4].ToString() != "")
        //    {
        //        try
        //        {
        //            if (!String.IsNullOrEmpty(row[4].ToString()))
        //            {
        //                deDupInfo.DateOfBirth = Convert.ToDateTime(row[4].ToString());
        //            }
        //            else
        //            {
        //                deDupInfo.DateOfBirth = DateTime.Now;
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            error = "Date Of Birth is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[5].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Profession = !String.IsNullOrEmpty(row[5].ToString())
        //                ? row[5].ToString()
        //                : "";
        //        }
        //        catch (Exception)
        //        {
        //            error = "Customer profession not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[6].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.ProductId = !String.IsNullOrEmpty(row[6].ToString())
        //                ? _manager.GetProductId(row[6].ToString())
        //                : 0;
        //        }
        //        catch (Exception)
        //        {
        //            error = "Product is not in correct in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[7].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.DeclineReason = row[7].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Decline Reason is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[8].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.DeclineDate = !String.IsNullOrEmpty(row[8].ToString())
        //                ? Convert.ToDateTime(row[8].ToString())
        //                : DateTime.Now;
        //        }
        //        catch (Exception)
        //        {
        //            error = "Decline Date is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[9].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.BusinessEmployeeName = row[9].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Business Employee Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[10].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Address1 = row[10].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Address1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[11].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Address2 = row[11].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Address2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[12].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Phone1 = row[12].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Phone1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[13].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Phone2 = row[13].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Phone2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[14].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Mobile1 = row[14].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Mobile1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[15].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Mobile2 = row[15].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Mobile2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[16].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.BankBranch = row[16].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Branch is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[17].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Source = row[17].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Source is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[18].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Status = row[18].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[19].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Remarks = row[19].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Remarks is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[20].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.MfuTin = row[20];
        //        }
        //        catch (Exception)
        //        {
        //            error = "Tin is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[21].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdType = row[21].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdType is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[22].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdType1 = row[22].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdType1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[23].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdType2 = row[23].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdType3 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[24].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdType3 = row[24].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdType4 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[25].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdType4 = row[25].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdType5 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[26].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdNo = row[26].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdNo1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[27].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdNo1 = row[27].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdNo2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[28].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdType2 = row[28].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdType3 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[29].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdNo3 = row[29].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdNo4 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception();
        //        }
        //    }
        //    if (row[30].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.IdType4 = row[30].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "IdType5 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[31].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.ResidentaileStatus = row[31].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Residentail Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[32].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.EducationalLevel = row[32].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Educational Level is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[33].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.MaritalStatus = row[33].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Marital Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[34].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.CriteriaType = row[34].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Criteria Type is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[35].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.EntryDate = Convert.ToDateTime(row[35].ToString());
        //        }
        //        catch (Exception)
        //        {
        //            error = "Entry date is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[36].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.Active = row[36].ToString();
        //        }
        //        catch (Exception)
        //        {
        //            error = "Active is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    if (row[37].ToString() != "")
        //    {
        //        try
        //        {
        //            deDupInfo.LoanAccountNo = row[37].ToString().Replace(",", "");
        //        }
        //        catch (Exception)
        //        {
        //            error = "Loan Account No is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
        //            //CustomException.Save(e, error);
        //            throw new Exception(error);
        //        }
        //    }
        //    return deDupInfo;
        //}

        //#endregion

        //private DataTable GetTableFromFile(string fileName)
        //{
        //    DataTable table = new DataTable("MFU");
        //    FileInfo file = new FileInfo(fileName);

        //    ConstructSchema(file);

        //    string cString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"{0}\";Extended Properties=\"text;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text;FMT=Delimited(|);\";", Path.GetDirectoryName(fileName));


        //    try
        //    {
        //        using (OleDbConnection connection = new OleDbConnection(cString))
        //        {
        //            connection.Open();
        //            DataSet set = new DataSet();
        //            string sql = string.Format("SELECT * FROM [{0}]", file.Name);
        //            using (OleDbCommand cmd = new OleDbCommand(sql, connection))
        //            {
        //                using (OleDbDataAdapter adapter = new OleDbDataAdapter(cmd))
        //                {
        //                    adapter.Fill(table);

        //                    string[] names = ListDataTableManager.GetColumnNamesFromClass<DeDupInfo>("");

        //                    names = names.Skip(4).ToArray();

        //                    int i = 0;
        //                    foreach (DataColumn column in table.Columns)
        //                    {
        //                        table.Columns[i].ColumnName = i + "";
        //                        i++;
        //                    }


        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return table;
        //}

        //public void ConstructSchema(FileInfo theFile)
        //{
        //    try
        //    {
        //        StringBuilder schema = new StringBuilder();

        //        string[] data = null;

        //        using (StreamReader reader = new StreamReader(Path.Combine(theFile.DirectoryName, theFile.Name)))
        //        {
        //            string temp = reader.ReadLine();
        //            data = temp.Split('|');
        //            //data = temp.Split('|');
        //            reader.Close();
        //            reader.Dispose();
        //        }

        //        schema.AppendLine("[" + theFile.Name + "]");
        //        schema.AppendLine("ColNameHeader=True");
        //        //schema.AppendLine("Format=TabDelimited");
        //        schema.AppendLine("Format=Delimited(|)");

        //        for (int i = 0; i < data.Length; i++)
        //        {
        //            schema.AppendLine("col" + (i + 1).ToString() + "=" + data[i] + " Text");
        //        }
        //        string schemaFileName = theFile.DirectoryName + @"\Schema.ini";
        //        TextWriter tw = new StreamWriter(schemaFileName);
        //        tw.WriteLine(schema.ToString());
        //        tw.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //protected void ClearTempBtn_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _manager.ClearTempTable();
        //        Alert.Show("Temp Data Cleared");
        //    }
        //    catch (Exception ex)
        //    {
        //        Alert.Show(ex.Message);
        //    }
        //}

        //protected void CheckDataBtn_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _manager.CheckData();
        //        Alert.Show("Data Check Complete");
        //    }
        //    catch (Exception ex)
        //    {
        //        Alert.Show(ex.Message);
        //        LogFile.WriteLog(ex);
        //    }
        //}

        //protected void DownLoadErrorBtn_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string csv = _manager.GetCSVStringWithError();

        //        Response.Clear();
        //        Response.Buffer = true;
        //        Response.AddHeader("content-disposition", "attachment;filename=DataWithError.csv");
        //        Response.Charset = "";
        //        Response.ContentType = "application/text";
        //        Response.Output.Write(csv);
        //        Response.Flush();
        //        Response.End();
        //    }
        //    catch (Exception ex)
        //    {
        //        Alert.Show(ex.Message);
        //        LogFile.WriteLog(ex);
        //    }
        //} 
        #endregion


        public void SetData()
        {
            DataTable dt;
            try
            {
                dt = _manager.GetMFU_FileUploadStatus();
                if (dt.Rows.Count > 0)
                {
                    txtDateCC_CHARGEOFF.Text = dt.Rows[0]["CC_CHARGEOFF_MAKE_DATE"].ToString();
                    txtRowCountCC_CHARGEOFF.Text = dt.Rows[0]["CC_CHARGEOFF_Uploaded"].ToString();
                    txtErrorCountCC_CHARGEOFF.Text = dt.Rows[0]["CC_CHARGEOFF_Error"].ToString();
                    txtMakerIDCC_CHARGEOFF.Text = dt.Rows[0]["CC_CHARGEOFF_MAKER_ID"].ToString();

                    txtDateCC_DISBURSED.Text = dt.Rows[0]["CC_DISBURSED_MAKE_DATE"].ToString();
                    txtRowCountCC_DISBURSED.Text = dt.Rows[0]["CC_DISBURSED_Uploaded"].ToString();
                    txtErrorCountCC_DISBURSED.Text = dt.Rows[0]["CC_DISBURSED_Error"].ToString();
                    txtMakerIDCC_DISBURSED.Text = dt.Rows[0]["CC_DISBURSED_MAKER_ID"].ToString();

                    txtDateLOAN_CHARGEOFF.Text = dt.Rows[0]["LOAN_CHARGEOFF_MAKE_DATE"].ToString();
                    txtRowCountLOAN_CHARGEOFF.Text = dt.Rows[0]["LOAN_CHARGEOFF_Uploaded"].ToString();
                    txtErrorCountLOAN_CHARGEOFF.Text = dt.Rows[0]["LOAN_CHARGEOFF_Error"].ToString();
                    txtMakerIDLOAN_CHARGEOFF.Text = dt.Rows[0]["LOAN_CHARGEOFF_MAKER_ID"].ToString();

                    txtDateLOAN_DISBURSED.Text = dt.Rows[0]["LOAN_DISBURSED_MAKE_DATE"].ToString();
                    txtRowCountLOAN_DISBURSED.Text = dt.Rows[0]["LOAN_DISBURSED_Uploaded"].ToString();
                    txtErrorCountLOAN_DISBURSED.Text = dt.Rows[0]["LOAN_DISBURSED_Error"].ToString();
                    txtMakerIDLOAN_DISBURSED.Text = dt.Rows[0]["LOAN_DISBURSED_MAKER_ID"].ToString();

                    txtDateLOAN_EBBS_CHARGEOFF.Text = dt.Rows[0]["LOAN_EBBS_CHARGEOFF_MAKE_DATE"].ToString();
                    txtRowCountLOAN_EBBS_CHARGEOFF.Text = dt.Rows[0]["LOAN_EBBS_CHARGEOFF_Uploaded"].ToString();
                    txtErrorCountLOAN_EBBS_CHARGEOFF.Text = dt.Rows[0]["LOAN_EBBS_CHARGEOFF_Error"].ToString();
                    txtMakerIDLOAN_EBBS_CHARGEOFF.Text = dt.Rows[0]["LOAN_EBBS_CHARGEOFF_MAKER_ID"].ToString();

                    txtDateLOAN_EBBS_DISBURSED.Text = dt.Rows[0]["LOAN_EBBS_DISBURSED_MAKE_DATE"].ToString();
                    txtRowCountLOAN_EBBS_DISBURSED.Text = dt.Rows[0]["LOAN_EBBS_DISBURSED_Uploaded"].ToString();
                    txtErrorCountLOAN_EBBS_DISBURSED.Text = dt.Rows[0]["LOAN_EBBS_DISBURSED_Error"].ToString();
                    txtMakerIDLOAN_EBBS_DISBURSED.Text = dt.Rows[0]["LOAN_EBBS_DISBURSED_MAKER_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowSuccess(ex.Message, "MFU Import - Page Loading..");
                LogFile.WriteLog(ex);
            }
        }

        #region Approve or Move to main table 

        protected void MoveToMainBtnCC_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountCC_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to approve (CC CHARGEOFF)", "MFU Approve");
                    return;
                }
                else
                {
                    var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                    bool moved = _manager.MoveToMainForCC_CHARGEOFF(userId);
                    if (moved)
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data was approved successfully", "MFU Approve");
                        return;
                    }
                    modalPopup.ShowSuccess("There was error in data.", "MFU Approve");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Approve");
                LogFile.WriteLog(ex);
            }
        }

        protected void MoveToMainBtnCC_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountCC_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to approve (CC DISBURSED)", "MFU Approve");
                    return;
                }
                else
                {
                    var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                    bool moved = _manager.MoveToMainForCC_DISBURSED(userId);
                    if (moved)
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data was approved successfully", "MFU Approve");
                        return;
                    }
                    modalPopup.ShowSuccess("There was error in data.", "MFU Approve");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Approve");
                LogFile.WriteLog(ex);
            }
        }

        protected void MoveToMainBtnLOAN_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to approve (LOAN CHARGEOFF)", "MFU Approve");
                    return;
                }
                else
                {
                    var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                    bool moved = _manager.MoveToMainForLOAN_CHARGEOFF(userId);
                    if (moved)
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data was approved successfully", "MFU Approve");
                        return;
                    }
                    modalPopup.ShowSuccess("There was error in data.", "MFU Approve");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Approve");
                LogFile.WriteLog(ex);
            }
        }

        protected void MoveToMainBtnLOAN_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to approve (LOAN DISBURSED)", "MFU Approve");
                    return;
                }
                else
                {
                    var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                    bool moved = _manager.MoveToMainForLOAN_DISBURSED(userId);
                    if (moved)
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data was approved successfully", "MFU Approve");
                        return;
                    }
                    modalPopup.ShowSuccess("There was error in data.", "MFU Approve");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Approve");
                LogFile.WriteLog(ex);
            }
        }

        protected void MoveToMainBtnLOAN_EBBS_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_EBBS_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to approve (LOAN EBBS CHARGEOFF)", "MFU Approve");
                    return;
                }
                else
                {
                    var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                    bool moved = _manager.MoveToMainForLOAN_EBBS_CHARGEOFF(userId);
                    if (moved)
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data was approved successfully", "MFU Approve");
                        return;
                    }
                    modalPopup.ShowSuccess("There was error in data.", "MFU Approve");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Approve");
                LogFile.WriteLog(ex);
            }
        }

        protected void MoveToMainBtnLOAN_EBBS_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_EBBS_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to approve (LOAN EBBS DISBURSED)", "MFU Approve");
                    return;
                }
                else
                {
                    var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                    bool moved = _manager.MoveToMainForLOAN_EBBS_DISBURSED(userId);
                    if (moved)
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data was approved successfully", "MFU Approve");
                        return;
                    }
                    modalPopup.ShowSuccess("There was error in data.", "MFU Approve");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Approve");
                LogFile.WriteLog(ex);
            }
        }
        #endregion



        #region Upload Data download..

        protected void DownloadUploadedBtnCC_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountCC_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (CC CHARGEOFF)", "MFU Data Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetTempDataCSVForCC_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=CC_CHARGEOFF_DataFile.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Found error", "MFU Data Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownloadUploadedBtnCC_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountCC_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (CC DISBURSED)", "MFU Data Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetTempDataCSVForCC_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=CC_DISBURSED_DataFile.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Found error", "MFU Data Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownloadUploadedBtnLOAN_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN CHARGEOFF)", "MFU Data Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetTempDataCSVForLOAN_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_CHARGEOFF_DataFile.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Found error", "MFU Data Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownloadUploadedBtnLOAN_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN DISBURSED)", "MFU Data Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetTempDataCSVForLOAN_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_DISBURSED_DataFile.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Found error", "MFU Data Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownloadUploadedBtnLOAN_EBBS_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_EBBS_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN EBBS CHARGEOFF)", "MFU Data Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetTempDataCSVForLOAN_EBBS_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_EBBS_CHARGEOFF_DataFile.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Found error", "MFU Data Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownloadUploadedBtnLOAN_EBBS_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_EBBS_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN EBBS DISBURSED)", "MFU Data Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetTempDataCSVForLOAN_EBBS_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_EBBS_DISBURSED_DataFile.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Found error", "MFU Data Download");
                LogFile.WriteLog(ex);
            }
        }
        #endregion


        #region Doanload Error File.

        protected void DownLoadErrorBtnCC_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCountCC_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (CC CHARGEOFF)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForCC_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=CC_CHARGEOFF_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnCC_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCountCC_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (CC DISBURSED)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForCC_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=CC_DISBURSED_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnLOAN_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCountLOAN_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN CHARGEOFF)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForLOAN_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_CHARGEOFF_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnLOAN_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCountLOAN_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN DISBURSED)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForLOAN_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_DISBURSED_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnLOAN_EBBS_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCountLOAN_EBBS_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN EBBS CHARGEOFF)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForLOAN_EBBS_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_EBBS_CHARGEOFF_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnLOAN_EBBS_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCountLOAN_EBBS_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN EBBS DISBURSED)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForLOAN_EBBS_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_EBBS_DISBURSED_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        #endregion

        #region  Clear Data

        protected void ClearTempBtnCC_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountCC_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (CC CHARGEOFF)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForCC_CHARGEOFF())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been deleted", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowSuccess("There was an error in clearing temp", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnCC_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountCC_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (CC DISBURSED)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForCC_DISBURSED())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been cleared", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowSuccess("There was an error in clearing temp", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnLOAN_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (LOAN CHARGEOFF)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForLOAN_CHARGEOFF())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been cleared", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowSuccess("There was an error in clearing temp", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnLOAN_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (LOAN DISBURSED)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForLOAN_DISBURSED())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been cleared", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowSuccess("There was an error in clearing temp", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnLOAN_EBBS_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_EBBS_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (LOAN EBBS CHARGEOFF)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForLOAN_EBBS_CHARGEOFF())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been cleared", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowSuccess("There was an error in clearing temp", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnLOAN_EBBS_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCountLOAN_EBBS_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (LOAN EBBS DISBURSED)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForLOAN_EBBS_DISBURSED())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been cleared", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowSuccess("There was an error in clearing temp", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        #endregion
    }

}