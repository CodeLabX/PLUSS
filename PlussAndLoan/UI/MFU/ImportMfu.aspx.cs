﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;
using System.Web.Configuration;
using Microsoft.VisualBasic.FileIO;
using System.Text.RegularExpressions;
using AzUtilities;
using CsvUtilities;

namespace PlussAndLoan.UI.MFU
{
    public partial class ImportMfu : System.Web.UI.Page
    {
        public DeDupInfoManager _manager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _manager = new DeDupInfoManager();
            if (!Page.IsPostBack)
            {

                SetData();
            }
        }

        public void SetData()
        {
            DataTable dt;
            try
            {
                dt = _manager.GetMFU_FileUploadStatus();
                if (dt.Rows.Count > 0)
                {
                    txtErrorCount_CC_CHARGEOFF.Text = dt.Rows[0]["CC_CHARGEOFF_Error"].ToString();
                    txtRowCount_CC_CHARGEOFF.Text = dt.Rows[0]["CC_CHARGEOFF_Uploaded"].ToString();
                    txtUploadDate.Text = dt.Rows[0]["CC_CHARGEOFF_MAKE_DATE"].ToString();

                    txtErrorCount_CC_DISBURSED.Text = dt.Rows[0]["CC_DISBURSED_Error"].ToString();
                    txtRowCount_CC_DISBURSED.Text = dt.Rows[0]["CC_DISBURSED_Uploaded"].ToString();
                    txtUploadDateCC_DISBURSED.Text = dt.Rows[0]["CC_DISBURSED_MAKE_DATE"].ToString();

                    txtErrorCount_LOAN_CHARGEOFF.Text = dt.Rows[0]["LOAN_CHARGEOFF_Error"].ToString();
                    txtRowCount_LOAN_CHARGEOFF.Text = dt.Rows[0]["LOAN_CHARGEOFF_Uploaded"].ToString();
                    txtUploadDateLOAN_CHARGEOFF.Text = dt.Rows[0]["LOAN_CHARGEOFF_MAKE_DATE"].ToString();

                    txtErrorCount_LOAN_DISBURSED.Text = dt.Rows[0]["LOAN_DISBURSED_Error"].ToString();
                    txtRowCount_LOAN_DISBURSED.Text = dt.Rows[0]["LOAN_DISBURSED_Uploaded"].ToString();
                    txtUploadDateLOAN_DISBURSED.Text = dt.Rows[0]["LOAN_DISBURSED_MAKE_DATE"].ToString();

                    txtErrorCount_LOAN_EBBS_CHARGEOFF.Text = dt.Rows[0]["LOAN_EBBS_CHARGEOFF_Error"].ToString();
                    txtRowCount_LOAN_EBBS_CHARGEOFF.Text = dt.Rows[0]["LOAN_EBBS_CHARGEOFF_Uploaded"].ToString();
                    txtUploadDateLOAN_EBBS_CHARGEOFF.Text = dt.Rows[0]["LOAN_EBBS_CHARGEOFF_MAKE_DATE"].ToString();

                    txtErrorCount_LOAN_EBBS_DISBURSED.Text = dt.Rows[0]["LOAN_EBBS_DISBURSED_Error"].ToString();
                    txtRowCount_LOAN_EBBS_DISBURSED.Text = dt.Rows[0]["LOAN_EBBS_DISBURSED_Uploaded"].ToString();
                    txtUploadDateLOAN_EBBS_DISBURSED.Text = dt.Rows[0]["LOAN_EBBS_DISBURSED_MAKE_DATE"].ToString();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowSuccess(ex.Message, "MFU Page Loading.");
                LogFile.WriteLog(ex);
            }
        }


        #region File Upload Control Implememtation

        protected void UploadButtonCC_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string message = "";
                string activity = "";
                string path = WebConfigurationManager.AppSettings["MFU_Uploaded_File_Path"];
                var uploadDate = new DateTime();
                var fileUploadDate = txtUploadDate.Text;
                if (fileUploadDate != "")
                {
                    uploadDate = Convert.ToDateTime(fileUploadDate);
                }
                else
                {
                    uploadDate = DateTime.Now;
                }
                var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                if (FileUpload_CC_CHARGEOFF.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload_CC_CHARGEOFF.FileName);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    path = Path.Combine(path, fileName);
                    FileUpload_CC_CHARGEOFF.SaveAs(path);

                    LogFile.WriteLine(DateTime.Now.ToString() + " File has been successfully uploaded");
                    var rowNom = ReadExcleFile(path, "CC_CHARGEOFF", uploadDate);  // need to change
                    if (rowNom > 0)
                    {
                        message = rowNom + " MFU data uploaded successfully....";
                        modalPopup.ShowSuccess(message, "MFU Import");
                        activity = "Success";
                        SetData();
                        FileUpload_CC_CHARGEOFF.Attributes.Clear();
                    }
                    else
                    {
                        message = "MFU upload Failed";
                        modalPopup.ShowError(message, "MFU Import");
                        activity = "Failed";
                    }
                    _manager.SaveMFU_UploadResult(fileName, uploadDate, userId, rowNom, activity, "CC_CHARGEOFF");
                    // need to change
                    new AT(this).AuditAndTraial("MFU Uploaded", "Upload");
                }
                else
                {
                    modalPopup.ShowWarning("Please select CSV file", "MFU Import");
                    FileUpload_CC_CHARGEOFF.Focus();
                }
            }
            catch (Exception exception)
            {
                LogFile.WriteLog(exception);
                CustomException.Save(exception, "MFU Upload");
            }
            UploadButtonCC_CHARGEOFF.Enabled = true;
            messageLabeCC_CHARGEOFF.Text = "";
        }

        protected void UploadButtonCC_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string message = "";
                string activity = "";
                string path = WebConfigurationManager.AppSettings["MFU_Uploaded_File_Path"];
                var uploadDate = new DateTime();
                var fileUploadDate = txtUploadDateCC_DISBURSED.Text;
                if (fileUploadDate != "")
                {
                    uploadDate = Convert.ToDateTime(fileUploadDate);
                }
                else
                {
                    uploadDate = DateTime.Now;
                }
                var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                if (FileUpload_CC_DISBURSED.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload_CC_DISBURSED.FileName);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    path = Path.Combine(path, fileName);
                    FileUpload_CC_DISBURSED.SaveAs(path);
                    LogFile.WriteLine(DateTime.Now.ToString() + " File has been successfully uploaded");
                    var rowNom = ReadExcleFile(path, "CC_DISBURSED", uploadDate);
                    if (rowNom > 0)
                    {
                        message = rowNom + " MFU data uploaded successfully....";
                        modalPopup.ShowSuccess(message, "MFU Import");
                        activity = "Success";
                        SetData();
                        FileUpload_CC_CHARGEOFF.Attributes.Clear();
                    }
                    else
                    {
                        message = "MFU upload Failed";
                        modalPopup.ShowError(message, "MFU Import");
                        activity = "Failed";
                    }
                    _manager.SaveMFU_UploadResult(fileName, uploadDate, userId, rowNom, activity, "CC_DISBURSED");
                    new AT(this).AuditAndTraial("MFU Uploaded", "Upload");
                }
                else
                {
                    modalPopup.ShowWarning("Please select CSV file", "MFU Import");
                    FileUpload_CC_DISBURSED.Focus();
                }
            }
            catch (Exception exception)
            {
                LogFile.WriteLog(exception);
                CustomException.Save(exception, "MFU Upload");
            }
            UploadButtonCC_DISBURSED.Enabled = true;
            messageLabeCC_DISBURSED.Text = "";
        }

        protected void UploadButtonLOAN_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string message = "";
                string activity = "";
                string path = WebConfigurationManager.AppSettings["MFU_Uploaded_File_Path"];
                var uploadDate = new DateTime();
                var fileUploadDate = txtUploadDateLOAN_CHARGEOFF.Text;
                if (fileUploadDate != "")
                {
                    uploadDate = Convert.ToDateTime(fileUploadDate);
                }
                else
                {
                    uploadDate = DateTime.Now;
                }
                var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                if (FileUpload_LOAN_CHARGEOFF.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload_LOAN_CHARGEOFF.FileName);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    path = Path.Combine(path, fileName);
                    FileUpload_LOAN_CHARGEOFF.SaveAs(path);
                    LogFile.WriteLine(DateTime.Now.ToString() + " File has been successfully uploaded");
                    var rowNom = ReadExcleFile(path, "LOAN_CHARGEOFF",uploadDate);
                    if (rowNom > 0)
                    {
                        message = rowNom + " MFU data uploaded successfully....";
                        modalPopup.ShowSuccess(message, "MFU Import");
                        activity = "Success";
                        SetData();
                        FileUpload_CC_CHARGEOFF.Attributes.Clear();
                    }
                    else
                    {
                        message = "MFU upload Failed";
                        modalPopup.ShowError(message, "MFU Import");
                        activity = "Failed";
                    }
                    _manager.SaveMFU_UploadResult(fileName, uploadDate, userId, rowNom, activity, "LOAN_CHARGEOFF");
                    new AT(this).AuditAndTraial("MFU Uploaded", "Upload");
                }
                else
                {
                    modalPopup.ShowWarning("Please select CSV file", "MFU Import");
                    FileUpload_LOAN_CHARGEOFF.Focus();
                }
            }
            catch (Exception exception)
            {
                LogFile.WriteLog(exception);
                CustomException.Save(exception, "MFU Upload");
            }

            UploadButtonLOAN_CHARGEOFF.Enabled = true;
            messageLabeLOAN_CHARGEOFF.Text = "";
        }

        protected void UploadButtonLOAN_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string message = "";
                string activity = "";
                string path = WebConfigurationManager.AppSettings["MFU_Uploaded_File_Path"];
                var uploadDate = new DateTime();
                var fileUploadDate = txtUploadDateLOAN_DISBURSED.Text;
                if (fileUploadDate != "")
                {
                    uploadDate = Convert.ToDateTime(fileUploadDate);
                }
                else
                {
                    uploadDate = DateTime.Now;
                }
                var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                if (FileUpload_LOAN_DISBURSED.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload_LOAN_DISBURSED.FileName);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    path = Path.Combine(path, fileName);
                    FileUpload_LOAN_DISBURSED.SaveAs(path);
                    LogFile.WriteLine(DateTime.Now.ToString() + " File has been successfully uploaded");
                    var rowNom = ReadExcleFile(path, "LOAN_DISBURSED", uploadDate);
                    if (rowNom > 0)
                    {
                        message = rowNom + " MFU data uploaded successfully....";
                        modalPopup.ShowSuccess(message, "MFU Import");
                        activity = "Success";
                        SetData();
                        FileUpload_CC_CHARGEOFF.Attributes.Clear();
                    }
                    else
                    {
                        message = "MFU upload Failed";
                        modalPopup.ShowError(message, "MFU Import");
                        activity = "Failed";
                    }
                    _manager.SaveMFU_UploadResult(fileName, uploadDate, userId, rowNom, activity, "LOAN_DISBURSED");
                    new AT(this).AuditAndTraial("MFU Uploaded", "Upload");
                }
                else
                {
                    modalPopup.ShowWarning("Please select CSV file", "MFU Import");
                    FileUpload_LOAN_DISBURSED.Focus();
                }
            }
            catch (Exception exception)
            {
                LogFile.WriteLog(exception);
                CustomException.Save(exception, "MFU Upload");
            }

            UploadButtonLOAN_DISBURSED.Enabled = true;
            messageLabeLOAN_DISBURSED.Text = "";
        }

        protected void UploadButtonLOAN_EBBS_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string message = "";
                string activity = "";
                string path = WebConfigurationManager.AppSettings["MFU_Uploaded_File_Path"];
                var uploadDate = new DateTime();
                var fileUploadDate = txtUploadDateLOAN_EBBS_CHARGEOFF.Text;
                if (fileUploadDate != "")
                {
                    uploadDate = Convert.ToDateTime(fileUploadDate);
                }
                else
                {
                    uploadDate = DateTime.Now;
                }
                var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                if (FileUpload_LOAN_EBBS_CHARGEOFF.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload_LOAN_EBBS_CHARGEOFF.FileName);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    path = Path.Combine(path, fileName);
                    FileUpload_LOAN_EBBS_CHARGEOFF.SaveAs(path);
                    LogFile.WriteLine(DateTime.Now.ToString() + " File has been successfully uploaded");
                    var rowNom = ReadExcleFile(path, "LOAN_EBBS_CHARGEOFF", uploadDate);
                    if (rowNom > 0)
                    {
                        message = rowNom + " MFU data uploaded successfully....";
                        modalPopup.ShowSuccess(message, "MFU Import");
                        activity = "Success";
                        SetData();
                        FileUpload_CC_CHARGEOFF.Attributes.Clear();
                    }
                    else
                    {
                        message = "MFU upload Failed";
                        modalPopup.ShowError(message, "MFU Import");
                        activity = "Failed";
                    }
                    _manager.SaveMFU_UploadResult(fileName, uploadDate, userId, rowNom, activity, "LOAN_EBBS_CHARGEOFF");
                    new AT(this).AuditAndTraial("MFU Uploaded", "Upload");
                }
                else
                {
                    modalPopup.ShowWarning("Please select CSV file", "MFU Import");
                    FileUpload_LOAN_EBBS_CHARGEOFF.Focus();
                }
            }
            catch (Exception exception)
            {
                LogFile.WriteLog(exception);
                CustomException.Save(exception, "MFU Upload");
            }

            UploadButtonLOAN_EBBS_CHARGEOFF.Enabled = true;
            messageLabeLOAN_EBBS_CHARGEOFF.Text = "";
        }
        
        protected void UploadButtonLOAN_EBBS_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string message = "";
                string activity = "";
                string path = WebConfigurationManager.AppSettings["MFU_Uploaded_File_Path"];
                var uploadDate = new DateTime();
                var fileUploadDate = txtUploadDateLOAN_EBBS_DISBURSED.Text;
                if (fileUploadDate != "")
                {
                    uploadDate = Convert.ToDateTime(fileUploadDate);
                }
                else
                {
                    uploadDate = DateTime.Now;
                }
                var userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                if (FileUpload_LOAN_EBBS_DISBURSED.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload_LOAN_EBBS_DISBURSED.FileName);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    path = Path.Combine(path, fileName);
                    FileUpload_LOAN_EBBS_DISBURSED.SaveAs(path);
                    LogFile.WriteLine(DateTime.Now.ToString() + " File has been successfully uploaded");
                    var rowNom = ReadExcleFile(path, "LOAN_EBBS_DISBURSED", uploadDate);
                    if (rowNom > 0)
                    {
                        message = rowNom + " MFU data uploaded successfully....";
                        modalPopup.ShowSuccess(message, "MFU Import");
                        activity = "Success";
                        SetData();
                        FileUpload_CC_CHARGEOFF.Attributes.Clear();
                    }
                    else
                    {
                        message = "MFU upload Failed";
                        modalPopup.ShowError(message, "MFU Import");
                        activity = "Failed";
                    }
                    _manager.SaveMFU_UploadResult(fileName, uploadDate, userId, rowNom, activity, "LOAN_EBBS_DISBURSED");
                    new AT(this).AuditAndTraial("MFU Uploaded", "Upload");
                }
                else
                {
                    modalPopup.ShowWarning("Please select CSV file", "MFU Import");
                    FileUpload_LOAN_EBBS_DISBURSED.Focus();
                }
            }
            catch (Exception exception)
            {
                LogFile.WriteLog(exception);
                CustomException.Save(exception, "MFU Upload");
            }

            UploadButtonLOAN_EBBS_DISBURSED.Enabled = true;
            messageLabeLOAN_EBBS_DISBURSED.Text = "";
        }

        private int ReadExcleFile(string fileName, string fileType, DateTime uploadDate)
        {
            User userSession = (User)Session["UserDetails"];
            int rowIndex = 0;
            int inserted = 0;
            try
            {
                if (fileName != "")
                {
                    TxtFileUtility utility = new TxtFileUtility();
                    DataTable table = GetTableFromFile(fileName);
                    inserted = utility.InsertIntoDataBaseFromTable(table, fileType , userSession, uploadDate);
                    fileName = null;
                }
                return inserted;
            }
            catch (Exception ex)
            {
                string mess = ex.Message;
                modalPopup.ShowSuccess(mess, "MFU Import");
                LogFile.WriteLine("MFU Upload Failed at row index " + rowIndex);
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "MFU Upload Failed at row index " + rowIndex);
                return 0;
            }
        }

        #region Old Slow Upload
        private DeDupInfo GetDeDupInfoFromArray(string[] row, int rowIndex)
        {
            string error = "";
            DeDupInfo deDupInfo = new DeDupInfo();

            if (row[0].ToString() != "")
            {
                try
                {
                    deDupInfo.Name = row[0].ToString();
                }
                catch (Exception)
                {
                    error = "Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[1].ToString() != "")
            {
                try
                {
                    deDupInfo.FatherName = row[1].ToString();
                }
                catch (Exception)
                {
                    error = "Father Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[2].ToString() != "")
            {
                try
                {
                    deDupInfo.MotherName = row[2].ToString();
                }
                catch (Exception)
                {
                    error = "Mother Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[3].ToString() != "")
            {
                try
                {
                    deDupInfo.ScbMaterNo = row[3].ToString();
                }
                catch (Exception)
                {
                    error = "Scb Mater No is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[4].ToString() != "")
            {
                try
                {
                    if (!String.IsNullOrEmpty(row[4].ToString()))
                    {
                        deDupInfo.DateOfBirth = Convert.ToDateTime(row[4].ToString());
                    }
                    else
                    {
                        deDupInfo.DateOfBirth = DateTime.Now;
                    }
                }
                catch (Exception)
                {
                    error = "Date Of Birth is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[5].ToString() != "")
            {
                try
                {
                    deDupInfo.Profession = !String.IsNullOrEmpty(row[5].ToString())
                        ? row[5].ToString()
                        : "";
                }
                catch (Exception)
                {
                    error = "Customer profession not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[6].ToString() != "")
            {
                try
                {
                    deDupInfo.ProductId = !String.IsNullOrEmpty(row[6].ToString())
                        ? _manager.GetProductId(row[6].ToString())
                        : 0;
                }
                catch (Exception)
                {
                    error = "Product is not in correct in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[7].ToString() != "")
            {
                try
                {
                    deDupInfo.DeclineReason = row[7].ToString();
                }
                catch (Exception)
                {
                    error = "Decline Reason is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[8].ToString() != "")
            {
                try
                {
                    deDupInfo.DeclineDate = !String.IsNullOrEmpty(row[8].ToString())
                        ? Convert.ToDateTime(row[8].ToString())
                        : DateTime.Now;
                }
                catch (Exception)
                {
                    error = "Decline Date is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[9].ToString() != "")
            {
                try
                {
                    deDupInfo.BusinessEmployeeName = row[9].ToString();
                }
                catch (Exception)
                {
                    error = "Business Employee Name is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[10].ToString() != "")
            {
                try
                {
                    deDupInfo.Address1 = row[10].ToString();
                }
                catch (Exception)
                {
                    error = "Address1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[11].ToString() != "")
            {
                try
                {
                    deDupInfo.Address2 = row[11].ToString();
                }
                catch (Exception)
                {
                    error = "Address2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[12].ToString() != "")
            {
                try
                {
                    deDupInfo.Phone1 = row[12].ToString();
                }
                catch (Exception)
                {
                    error = "Phone1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[13].ToString() != "")
            {
                try
                {
                    deDupInfo.Phone2 = row[13].ToString();
                }
                catch (Exception)
                {
                    error = "Phone2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[14].ToString() != "")
            {
                try
                {
                    deDupInfo.Mobile1 = row[14].ToString();
                }
                catch (Exception)
                {
                    error = "Mobile1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[15].ToString() != "")
            {
                try
                {
                    deDupInfo.Mobile2 = row[15].ToString();
                }
                catch (Exception)
                {
                    error = "Mobile2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[16].ToString() != "")
            {
                try
                {
                    deDupInfo.BankBranch = row[16].ToString();
                }
                catch (Exception)
                {
                    error = "Branch is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[17].ToString() != "")
            {
                try
                {
                    deDupInfo.Source = row[17].ToString();
                }
                catch (Exception)
                {
                    error = "Source is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[18].ToString() != "")
            {
                try
                {
                    deDupInfo.Status = row[18].ToString();
                }
                catch (Exception)
                {
                    error = "Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[19].ToString() != "")
            {
                try
                {
                    deDupInfo.Remarks = row[19].ToString();
                }
                catch (Exception)
                {
                    error = "Remarks is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[20].ToString() != "")
            {
                try
                {
                    deDupInfo.MfuTin = row[20];
                }
                catch (Exception)
                {
                    error = "Tin is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[21].ToString() != "")
            {
                try
                {
                    deDupInfo.IdType = row[21].ToString();
                }
                catch (Exception)
                {
                    error = "IdType is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[22].ToString() != "")
            {
                try
                {
                    deDupInfo.IdType1 = row[22].ToString();
                }
                catch (Exception)
                {
                    error = "IdType1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[23].ToString() != "")
            {
                try
                {
                    deDupInfo.IdType2 = row[23].ToString();
                }
                catch (Exception)
                {
                    error = "IdType3 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[24].ToString() != "")
            {
                try
                {
                    deDupInfo.IdType3 = row[24].ToString();
                }
                catch (Exception)
                {
                    error = "IdType4 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[25].ToString() != "")
            {
                try
                {
                    deDupInfo.IdType4 = row[25].ToString();
                }
                catch (Exception)
                {
                    error = "IdType5 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[26].ToString() != "")
            {
                try
                {
                    deDupInfo.IdNo = row[26].ToString();
                }
                catch (Exception)
                {
                    error = "IdNo1 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[27].ToString() != "")
            {
                try
                {
                    deDupInfo.IdNo1 = row[27].ToString();
                }
                catch (Exception)
                {
                    error = "IdNo2 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[28].ToString() != "")
            {
                try
                {
                    deDupInfo.IdType2 = row[28].ToString();
                }
                catch (Exception)
                {
                    error = "IdType3 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[29].ToString() != "")
            {
                try
                {
                    deDupInfo.IdNo3 = row[29].ToString();
                }
                catch (Exception)
                {
                    error = "IdNo4 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception();
                }
            }
            if (row[30].ToString() != "")
            {
                try
                {
                    deDupInfo.IdType4 = row[30].ToString();
                }
                catch (Exception)
                {
                    error = "IdType5 is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[31].ToString() != "")
            {
                try
                {
                    deDupInfo.ResidentaileStatus = row[31].ToString();
                }
                catch (Exception)
                {
                    error = "Residentail Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[32].ToString() != "")
            {
                try
                {
                    deDupInfo.EducationalLevel = row[32].ToString();
                }
                catch (Exception)
                {
                    error = "Educational Level is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[33].ToString() != "")
            {
                try
                {
                    deDupInfo.MaritalStatus = row[33].ToString();
                }
                catch (Exception)
                {
                    error = "Marital Status is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[34].ToString() != "")
            {
                try
                {
                    deDupInfo.CriteriaType = row[34].ToString();
                }
                catch (Exception)
                {
                    error = "Criteria Type is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[35].ToString() != "")
            {
                try
                {
                    deDupInfo.EntryDate = Convert.ToDateTime(row[35].ToString());
                }
                catch (Exception)
                {
                    error = "Entry date is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[36].ToString() != "")
            {
                try
                {
                    deDupInfo.Active = row[36].ToString();
                }
                catch (Exception)
                {
                    error = "Active is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            if (row[37].ToString() != "")
            {
                try
                {
                    deDupInfo.LoanAccountNo = row[37].ToString().Replace(",", "");
                }
                catch (Exception)
                {
                    error = "Loan Account No is not in correct format in MFU upload CSV file" + "( at Row #" + rowIndex + ")";
                    //CustomException.Save(e, error);
                    throw new Exception(error);
                }
            }
            return deDupInfo;
        }

        #endregion

        private DataTable GetTableFromFile(string fileName)
        {
            DataTable table = new DataTable("MFU");
            FileInfo file = new FileInfo(fileName);

            ConstructSchema(file);

            //string cString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"{0}\";Extended Properties=\"text;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text;FMT=Delimited(|);\";", Path.GetDirectoryName(fileName)); //Pipe Delimited
            string cString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"{0}\";Extended Properties=\"text;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text;FMT=TabDelimited;\";", Path.GetDirectoryName(fileName));
            try
            {
                using (OleDbConnection connection = new OleDbConnection(cString))
                {
                    connection.Open();
                    DataSet set = new DataSet();
                    string sql = string.Format("SELECT * FROM [{0}]", file.Name);
                    using (OleDbCommand cmd = new OleDbCommand(sql, connection))
                    {
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(cmd))
                        {
                            adapter.Fill(table);
                            string[] names = ListDataTableManager.GetColumnNamesFromClass<DeDupInfo>("");
                            names = names.Skip(4).ToArray();

                            int i = 0;
                            foreach (DataColumn column in table.Columns)
                            {
                                table.Columns[i].ColumnName = i + "";
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return table;
        }

        public void ConstructSchema(FileInfo theFile)
        {
            try
            {
                StringBuilder schema = new StringBuilder();

                string[] data = null;

                using (StreamReader reader = new StreamReader(Path.Combine(theFile.DirectoryName, theFile.Name)))
                {
                    string temp = reader.ReadLine();
                    //data = temp.Split('|'); //Pipe Delimited
                    data = temp.Split('\t');
                    //data = temp.Split('|');
                    reader.Close();
                    reader.Dispose();
                }

                schema.AppendLine("[" + theFile.Name + "]");
                schema.AppendLine("ColNameHeader=True");
                //schema.AppendLine("Format=Delimited(|)");//Pipe Delimited
                schema.AppendLine("Format=TabDelimited");

                for (int i = 0; i < data.Length; i++)
                {
                    schema.AppendLine("col" + (i + 1).ToString() + "=" + data[i] + " Text");
                }
                string schemaFileName = theFile.DirectoryName + @"\Schema.ini";
                TextWriter tw = new StreamWriter(schemaFileName);
                tw.WriteLine(schema.ToString());
                tw.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion



        #region  Clear Data

        protected void ClearTempBtnCC_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_CC_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (CC CHARGEOFF)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForCC_CHARGEOFF())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been deleted", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowError("There was an error while clearing temp data", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnCC_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_CC_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (CC DISBURSED)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForCC_DISBURSED())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been deleted", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowError("There was an error while clearing temp data", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnLOAN_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_LOAN_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (LOAN CHARGEOFF)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForLOAN_CHARGEOFF())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been deleted", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowError("There was an error while clearing temp data", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnLOAN_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_LOAN_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (LOAN DISBURSED)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForLOAN_DISBURSED())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been deleted", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowError("There was an error while clearing temp data", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnLOAN_EBBS_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_LOAN_EBBS_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (LOAN EBBS CHARGEOFF)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForLOAN_EBBS_CHARGEOFF())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been deleted", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowError("There was an error while clearing temp data", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        protected void ClearTempBtnLOAN_EBBS_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_LOAN_EBBS_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to delete (LOAN EBBS DISBURSED)", "MFU - Clear Data");
                    return;
                }
                else if (_manager.ClearTempTableForLOAN_EBBS_DISBURSED())
                {
                    SetData();
                    modalPopup.ShowSuccess("Temp Data has been deleted", "MFU - Clear Data");
                    return;
                }
                else
                {
                    modalPopup.ShowError("There was an error while clearing temp data", "MFU - Clear Data");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU - Clear Data");
                LogFile.WriteLog(ex);
            }
        }

        #endregion


        #region  Check / Process data

        protected void CheckDataBtnCC_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_CC_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to process.", "MFU Data Checking");
                }
                else
                {
                    if (_manager.CheckDataForCC_CHARGEOFF())
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data process has been done", "MFU Data Checking");
                        return;
                    }
                    else
                    {
                        modalPopup.ShowError("There was a problem in checking data.", "MFU Data Checking");
                    }
                    //Alert.Show("Data Check Complete");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Data Checking");
                LogFile.WriteLog(ex);
            }
        }

        protected void CheckDataBtnCC_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_CC_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to process.", "MFU Data Checking");
                }
                else
                {
                    if (_manager.CheckDataForCC_DISBURSED())
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data process has been done", "MFU Data Checking");
                        return;
                    }
                    else
                    {
                        modalPopup.ShowError("There was a problem in checking data.", "MFU Data Checking");
                    }
                    //Alert.Show("Data Check Complete");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Data Checking");
                LogFile.WriteLog(ex);
            }
        }

        protected void CheckDataBtnLOAN_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_LOAN_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to process.", "MFU Data Checking");
                }
                else
                {
                    if (_manager.CheckDataForLOAN_CHARGEOFF())
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data process has been done", "MFU Data Checking");
                        return;
                    }
                    else
                    {
                        modalPopup.ShowError("There was a problem in checking data.", "MFU Data Checking");
                    }
                    //Alert.Show("Data Check Complete");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Data Checking");
                LogFile.WriteLog(ex);
            }
        }

        protected void CheckDataBtnLOAN_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_LOAN_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to process.", "MFU Data Checking");
                }
                else
                {
                    if (_manager.CheckDataForLOAN_DISBURSED())
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data process has been done", "MFU Data Checking");
                        return;
                    }
                    else
                    {
                        modalPopup.ShowError("There was a problem in checking data.", "MFU Data Checking");
                    }
                    //Alert.Show("Data Check Complete");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Data Checking");
                LogFile.WriteLog(ex);
            }
        }

        protected void CheckDataBtnLOAN_EBBS_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_LOAN_EBBS_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to process.", "MFU Data Checking");
                }
                else
                {
                    if (_manager.CheckDataForLOAN_EBBS_CHARGEOFF())
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data process has been done", "MFU Data Checking");
                        return;
                    }
                    else
                    {
                        modalPopup.ShowError("There was a problem in checking data.", "MFU Data Checking");
                    }
                    //Alert.Show("Data Check Complete");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Data Checking");
                LogFile.WriteLog(ex);
            }
        }

        protected void CheckDataBtnLOAN_EBBS_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtRowCount_LOAN_EBBS_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to process.", "MFU Data Checking");
                }
                else
                {
                    if (_manager.CheckDataForLOAN_EBBS_DISBURSED())
                    {
                        SetData();
                        modalPopup.ShowSuccess("Data process has been done", "MFU Data Checking");
                        return;
                    }
                    else
                    {
                        modalPopup.ShowError("There was a problem in checking data.", "MFU Data Checking");
                    }
                    //Alert.Show("Data Check Complete");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError(ex.Message, "MFU Data Checking");
                LogFile.WriteLog(ex);
            }
        }

        #endregion


        #region Doanload Error File.

        protected void DownLoadErrorBtnCC_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCount_CC_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (CC CHARGEOFF)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForCC_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=CC_CHARGEOFF_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowSuccess(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnCC_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCount_CC_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (CC DISBURSED)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForCC_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=CC_DISBURSED_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowSuccess(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnLOAN_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCount_LOAN_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN CHARGEOFF)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForLOAN_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_CHARGEOFF_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowSuccess(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnLOAN_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCount_LOAN_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN DISBURSED)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForLOAN_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_DISBURSED_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowSuccess(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnLOAN_EBBS_CHARGEOFF_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCount_LOAN_EBBS_CHARGEOFF.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN EBBS CHARGEOFF)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForLOAN_EBBS_CHARGEOFF();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_EBBS_CHARGEOFF_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowSuccess(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        protected void DownLoadErrorBtnLOAN_EBBS_DISBURSED_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtErrorCount_LOAN_EBBS_DISBURSED.Text) == 0)
                {
                    modalPopup.ShowWarning("There has no data to download (LOAN EBBS DISBURSED)", "MFU Error Download");
                    return;
                }
                else
                {
                    string csv = _manager.GetCSVStringWithErrorForLOAN_EBBS_DISBURSED();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LOAN_EBBS_DISBURSED_DataWithError.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowSuccess(ex.Message, "MFU Error Download");
                LogFile.WriteLog(ex);
            }
        }

        #endregion


        protected void RemoveErrorsFromTempBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (_manager.DeleteErrorsFromtemp())
                {
                    SetData();
                    modalPopup.ShowSuccess("Errors Removed From Temporary Data", "MFU Import");
                }
                else
                {
                    modalPopup.ShowSuccess("There was a problem", "MFU Import");
                }
            }
            catch (Exception ex)
            {
                //Alert.Show("Failed");
                modalPopup.ShowSuccess(ex.Message, "MFU Import");
                LogFile.WriteLog(ex);
            }
        }
    }

}