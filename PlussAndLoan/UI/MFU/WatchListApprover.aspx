﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.MFU.UI_WatchListApprover" Title="WatchList Approve" CodeBehind="WatchListApprover.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/azGridTableCss.ascx" TagPrefix="uc1" TagName="azGridTableCss" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>

<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <uc1:azGridTableCss runat="server" ID="azGridTableCss" />
    <modal:modalPopup runat="server" ID="modalPopup" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Watch List Approve</div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr style="font-weight: bold; font-size: 16px" align="center">
                <td>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvRecords" runat="server" GridLines="None" CssClass="myGridStyle"
                        Font-Size="11px" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="ID" />
                            <asp:BoundField DataField="BankCompany" HeaderText="BANK / COMPANY" />
                            <asp:BoundField DataField="Branch" HeaderText="BRANCH" />
                            <asp:BoundField DataField="Address" HeaderText="ADDRESS" />
                            <asp:BoundField DataField="NegetiveList" HeaderText="NEGATIVE LIST" />
                            <asp:BoundField DataField="WatchList" HeaderText="WATCH LIST" />
                            <asp:BoundField DataField="Exclusion" HeaderText="EXCLUSION" />
                            <asp:BoundField DataField="Delist" HeaderText="DELIST" />
                            <asp:BoundField DataField="Restriction" HeaderText="RESTRICTION" />
                            <asp:BoundField DataField="Remarks" HeaderText="REMARKS" />
                            <asp:TemplateField HeaderText="ENTRY DATE">
                                <ItemTemplate>
                                    <%# Convert.ToDateTime(Eval("EntryDate")).ToString("dd-MMM-yyyy") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="MakerPsId" HeaderText="MAKER PS" />
                        </Columns>
                        <EmptyDataTemplate>
                            No record found
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <br />
                    <br />

                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnApprove" runat="server" CssClass="btn primarybtn" OnClick="btnApprove_Click" Text="Approve" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="btn clearbtn" Text="Delete From Temp" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnDownload" runat="server" CssClass="btn primarybtn" Text="Download" OnClick="btnDownload_Click" />
                </td>
            </tr>
        </table>
    </div>
    
    <script type="text/javascript">
        function JS_FunctionClear() {
            mygrid.clearAll();
        }
    </script>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</asp:Content>