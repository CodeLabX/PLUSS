﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Text;
using Bat.Common;
using BLL;
using BusinessEntities;
using DAL;
using System.Web.Configuration;
using AzUtilities;
using System.IO;
using System.Web.UI;
using System.Web;
using AzUtilities.Common;

namespace PlussAndLoan.UI.MFU
{
    public partial class UI_WatchListApprover : System.Web.UI.Page
    {
        public NegetiveListEmployerManager _negativeList = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            _negativeList = new NegetiveListEmployerManager();
            BindData();
        }

        private void BindData()
        {
            List<NegetiveListEmployer> nList = new List<NegetiveListEmployer>();
            nList = _negativeList.GetUploadedWatchListTempData();

            gvRecords.DataSource = nList;
            gvRecords.DataBind();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            User actionUser = (User)Session["UserDetails"];
            string result = _negativeList.ApproveNegativeList(actionUser);

           
            if (result == Operation.Success.ToString())
            {
                BindData();
                modalPopup.ShowSuccess("Watchlist list data has been approved successfull", "Watch List");

            }
            else
            {
                modalPopup.ShowError(result, "Watch List");
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string result = _negativeList.FlushTemp();
                if (result == Operation.Success.ToString())
                {
                    BindData();
                    modalPopup.ShowSuccess("Unapproved data has been removed Successfully", "Watch List Approve");
                }
                else
                {
                    modalPopup.ShowError("Flushing Temporary Data Error :: " + result, "Watch List Approve");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Error Level Code -2 :: " + ex.Message, "Watch List Approve");
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string csv = _negativeList.GetWatchListTempDataCSV();
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=WatchList_Data.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(csv);
            Response.Flush();
            Response.End();
        }
    }
}
