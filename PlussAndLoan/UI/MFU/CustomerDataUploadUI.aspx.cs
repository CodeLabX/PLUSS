﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using BLL;
using BusinessEntities;
using DAL;
using System.Web.Configuration;
using AzUtilities.Common;
using Bat.Common;

namespace PlussAndLoan.UI.MFU
{
    public partial class CustomerDataUploadUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            CustomerBulkDataManager _manager = new CustomerBulkDataManager();
            List<CustomersBulkData> nList = new List<CustomersBulkData>();
            nList = _manager.GetAllTemp();

            gvRecords.DataSource = nList;
            gvRecords.DataBind();
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string path = WebConfigurationManager.AppSettings["CustomerBulk_Uploaded_File_Path"];
                if (FileUpload.HasFile)
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    fileName = Server.HtmlEncode(FileUpload.FileName);
                    path += fileName;
                    FileUpload.SaveAs(path);

                    ReadExcleFile(path, "csv", DateTime.Now);

                    BindData();
                }
                else
                {
                    modalPopup.ShowSuccess("Please Select the CSV file", "Customer Bulk Data");
                    FileUpload.Focus();
                }
            }
            catch (Exception exception)
            {
                modalPopup.ShowError(exception.Message, Title);
                WriteException(-1, exception.Message);
            }
        }

        private void ReadExcleFile(string fileName, string fileType, DateTime uploadDate)
        {
            User userSession = (User)Session["UserDetails"];
            try
            {
                if (fileName != "")
                {
                    DataTable table = GetTableFromFile(fileName);

                    CustomerBulkDataManager _manager = new CustomerBulkDataManager();

                    Operation isSuccess = _manager.Save(table, userSession);

                    if (isSuccess == Operation.Success)
                    {
                        modalPopup.ShowSuccess("Customers Data has been inserted properly", Title);
                    }
                    else
                    {
                        modalPopup.ShowError("There was an error while uploading the customer data.", Title);
                    }
                    _manager.InsertAudit(this, "Customer Bulk Data upload", "Upload");
                }
            }
            catch (Exception ex)
            {
                string mess = ex.Message;
                modalPopup.ShowError(mess, Title);
                CustomException.Save(ex, "Cusomer data Upload Failed");
            }
        }

        private DataTable GetTableFromFile(string fileName)
        {
            DataTable table = new DataTable("CUSTOMER_DATA");
            FileInfo file = new FileInfo(fileName);

            ConstructSchema(file);

            //string cString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"{0}\";Extended Properties=\"text;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text;FMT=Delimited(|);\";", Path.GetDirectoryName(fileName)); //Pipe Delimited
            string cString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"{0}\";Extended Properties=\"text;IMEX=1;HDR=YES;\";", Path.GetDirectoryName(fileName));
            try
            {
                using (OleDbConnection connection = new OleDbConnection(cString))
                {
                    connection.Open();
                    DataSet set = new DataSet();
                    string sql = string.Format("SELECT * FROM [{0}]", file.Name);
                    using (OleDbCommand cmd = new OleDbCommand(sql, connection))
                    {
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(cmd))
                        {
                            adapter.Fill(table);
                            table.Columns.Add("MakerId");
                            table.Columns.Add("MakeDate");
                        }

                        string dtr = table.Columns[0].ColumnName;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return table;
        }

        public void ConstructSchema(FileInfo theFile)
        {
            try
            {
                string path = theFile.DirectoryName + @"\Schema.ini";
                if (!File.Exists(path))
                {
                    StringBuilder schema = new StringBuilder();

                    string[] data = null;

                    using (StreamReader reader = new StreamReader(Path.Combine(theFile.DirectoryName, theFile.Name)))
                    {
                        string temp = reader.ReadLine();
                        data = temp.Split(',');
                        reader.Close();
                        reader.Dispose();
                    }

                    schema.AppendLine("[" + theFile.Name + "]");
                    schema.AppendLine("ColNameHeader=True");
                    //schema.AppendLine("Format=Delimited(|)");//Pipe Delimited
                    //schema.AppendLine("Format=TabDelimited"); // tab seperated
                    schema.AppendLine("Format=CSVDelimited"); // Comma Seperated

                    for (int i = 0; i < data.Length; i++)
                    {
                        schema.AppendLine("col" + (i + 1).ToString() + "=" + data[i] + " Text");
                    }
                    string schemaFileName = theFile.DirectoryName + @"\Schema.ini";
                    TextWriter tw = new StreamWriter(schemaFileName);
                    tw.WriteLine(schema.ToString());
                    tw.Close(); 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void btnFlush_Click(object sender, EventArgs e)
        {
            try
            {
                DeDupInfoManager deDupInfoManagerObj = new DeDupInfoManager();

                string result = deDupInfoManagerObj.FlushCustomerBulkDataTemp();
                if (result == Operation.Success.ToString())
                {
                    modalPopup.ShowSuccess("Unapproved data has been successfully removed", "Customer Bulk List");
                    gvRecords.DataSource = null;
                    gvRecords.DataBind();
                }
                else
                {
                    modalPopup.ShowError("There was an error while Flushing the temp data :: " + result, "Customer Bulk List");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Error :: " + ex.Message, "Customer Bulk List");
            }
        }

        private void WriteException(int row, string error)
        {
            try
            {
                error = error.Replace("'", "''");
                string sql = string.Format(@"insert into WatchListException (RowNo,Msg,UloadTime) values({0},'{1}','{2}')", row, error, DateTime.Now);
                DbQueryManager.ExecuteNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}