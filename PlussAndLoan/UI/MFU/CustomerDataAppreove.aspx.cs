﻿using System;
using System.Collections.Generic;
using BusinessEntities;
using AzUtilities.Common;
using BLL;

namespace PlussAndLoan.UI.MFU
{
    public partial class CustomerDataAppreove : System.Web.UI.Page
    {
        public CustomersBulkData _customersBulkDataList = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _customersBulkDataList = new CustomersBulkData();
                BindData();
            }
        }

        private void BindData()
        {
            CustomerBulkDataManager _manager = new CustomerBulkDataManager();
            List<CustomersBulkData> list = _manager.GetAllTemp();

            gvRecords.DataSource = list;
            gvRecords.DataBind();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            User actionUser = (User)Session["UserDetails"];
            CustomerBulkDataManager _manager = new CustomerBulkDataManager();
            string result =  _manager.ApproveCustomerBulkDataTemp(actionUser);


            if (result == Operation.Success.ToString())
            {
                BindData();
                modalPopup.ShowSuccess("Watchlist list data has been approved successfull", "Cust Bulk List Approve");

            }
            else
            {
                modalPopup.ShowError(result, "Customer Bulk List");
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeDupInfoManager deDupInfoManagerObj = new DeDupInfoManager();

                string result = deDupInfoManagerObj.FlushCustomerBulkDataTemp();
                if (result == Operation.Success.ToString())
                {
                    modalPopup.ShowSuccess("Unapproved data has been successfully removed", "Customer Bulk List");
                    gvRecords.DataSource = null;
                    gvRecords.DataBind();
                }
                else
                {
                    modalPopup.ShowError("There was an error while Flushing the temp data :: " + result, "Customer Bulk List");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Error :: " + ex.Message, "Customer Bulk List");
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            DeDupInfoManager deDupInfoManagerObj = new DeDupInfoManager();
            string csv = deDupInfoManagerObj.GetDCustomerBulkDataTempDataCSV();
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=CustomerBulk_Data.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(csv);
            Response.Flush();
            Response.End();
        }
    }
}