﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Text;
using Bat.Common;
using BLL;
using BusinessEntities;
using DAL;
using System.Web.Configuration;
using AzUtilities;
using AzUtilities.Common;

namespace PlussAndLoan.UI.MFU
{
    public partial class UI_WatchListUploadUI : System.Web.UI.Page
    {
        public NegetiveListEmployerManager _negativeList = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        Int32 rowsPerPage = 500;
        Int32 pageNum = 1;
        Int32 offset = 0;
        public DatabaseConnection dbMySQL = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            _negativeList = new NegetiveListEmployerManager();
            if (Request.QueryString.Count != 0)
            {
                pageNum = Convert.ToInt32(Request.QueryString["pageIndex"]);
                offset = (pageNum - 1) * rowsPerPage;
            }
            BindData(offset, rowsPerPage);
        }

        private void BindData(Int32 offset, Int32 rowsPerPage)
        {
            List<NegetiveListEmployer> nList = new List<NegetiveListEmployer>();
            nList = _negativeList.GetUploadResult(DateTime.Now, DateTime.Now, offset, rowsPerPage);

            gvRecords.DataSource = nList;
            gvRecords.DataBind();
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string path = WebConfigurationManager.AppSettings["WatchList_Uploaded_File_Path"];
                if (FileUpload.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload.FileName);
                    path += fileName;
                    FileUpload.SaveAs(path);
                    ReadExcleFile(path);
                    BindData(offset, rowsPerPage);
                    new AT(this).AuditAndTraial("Watch List Upload", "Upload");
                }
                else
                {
                    //Alert.Show("Please select Excel file");
                    modalPopup.ShowSuccess("Please Select the Excel(xlsx) file", "Watch List Upload");
                    FileUpload.Focus();
                }
            }
            catch (Exception exception)
            {
                //CustomException.Save(exception, "Upload Watch List");
                WriteException(-1, exception.Message);
            }
        }

        private void ReadExcleFile(string fileName)
        {
            User userT = (User)Session["UserDetails"];

            long slNo = 0;
            bool emptyRow = true;
            List<NegetiveListEmployer> negetiveListEmployerObjList = new List<NegetiveListEmployer>();
            var error = "";
            int row = 0;
            try
            {
                if (fileName != "")
                {
                    string cnnStr = ("Provider=Microsoft.ACE.OLEDB.12.0;" + ("Data Source=" + (fileName + (";" + "Extended Properties=\"Excel 12.0;HDR=YES\";"))));

                    DataTable dataTableObj = new DataTable();
                    OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);

                    dataAdapterObj.Fill(dataTableObj);
                    foreach (DataRow dataRow in dataTableObj.Rows)
                    {
                        error = "";
                        row++;
                        slNo = slNo + 1;
                        NegetiveListEmployer negetiveListEmployerObj = new NegetiveListEmployer();

                        if (dataRow[0].ToString() != "")
                        {
                            try
                            {
                                negetiveListEmployerObj.BankCompany = AddSlash(dataRow[0].ToString());
                            }
                            catch (Exception e)
                            {
                                error = "Bank Company is not in correct formate";
                                WriteException(row, error);
                            }
                        }

                        if (dataRow[1].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.Branch = AddSlash(dataRow[1].ToString());
                            }
                            catch (Exception e)
                            {
                                error = "Branch is not in correct formate";
                                WriteException(row, error);
                            }

                        }
                        else
                        {
                            negetiveListEmployerObj.Branch = "";
                        }
                        if (dataRow[2].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.Address = AddSlash(dataRow[2].ToString());
                            }
                            catch (Exception e)
                            {
                                error = "Address is not in correct formate";
                                WriteException(row, error);
                            }

                        }
                        else
                        {
                            negetiveListEmployerObj.Address = "";
                        }
                        if (dataRow[3].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.NegetiveList = dataRow[3].ToString();
                            }
                            catch (Exception e)
                            {
                                error = "Negetive List is not in correct formate";
                                WriteException(row, error);
                            }
                        }
                        else
                        {
                            negetiveListEmployerObj.NegetiveList = "";
                        }
                        if (dataRow[4].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.WatchList = dataRow[4].ToString();
                            }
                            catch (Exception e)
                            {
                                error = "WatchList is not in correct formate";
                                WriteException(row, error);
                            }
                        }
                        else
                        {
                            negetiveListEmployerObj.WatchList = "";
                        }
                        if (dataRow[5].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.Exclusion = dataRow[5].ToString();
                            }
                            catch (Exception e)
                            {
                                error = "Exclusion is not in correct formate";
                                WriteException(row, error);
                            }

                        }
                        else
                        {
                            negetiveListEmployerObj.Exclusion = "";
                        }

                        if (dataRow[6].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.Delist = dataRow[6].ToString();
                            }
                            catch (Exception e)
                            {
                                error = "Delist is not in correct formate";
                                WriteException(row, error);
                            }

                        }
                        else
                        {
                            negetiveListEmployerObj.Delist = "";
                        }

                        if (dataRow[7].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.Restriction = dataRow[7].ToString();
                            }
                            catch (Exception e)
                            {
                                error = "Restriction is not in correct formate";
                                WriteException(row, error);
                            }

                        }
                        else
                        {

                            negetiveListEmployerObj.Restriction = "";
                        }
                        if (dataRow[8].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.Remarks = AddSlash(dataRow[8].ToString());
                            }
                            catch (Exception e)
                            {
                                error = "Remarks is not in correct formate";
                                WriteException(row, error);
                            }

                        }
                        else
                        {
                            negetiveListEmployerObj.Remarks = "";
                        }

                        if (dataRow[9].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.InclusionDate = Convert.ToDateTime(dataRow[9].ToString());
                            }
                            catch (Exception e)
                            {
                                error = "Inclusion Date is not in correct formate";
                                WriteException(row, error);
                            }

                        }
                        else
                        {
                            negetiveListEmployerObj.InclusionDate = DateTime.Now;
                        }

                        if (dataRow[10].ToString() != "")
                        {
                            try
                            {
                                emptyRow = false;
                                negetiveListEmployerObj.ChangeDate = Convert.ToDateTime(dataRow[10].ToString());
                            }
                            catch (Exception e)
                            {
                                error = "Change Date is not in correct formate";
                                WriteException(row, error);
                            }
                        }
                        else
                        {
                            negetiveListEmployerObj.ChangeDate = DateTime.Now;
                        }
                        if (emptyRow == false)
                        {
                            negetiveListEmployerObj.EntryDate = DateTime.Now;
                            negetiveListEmployerObj.UserId = Convert.ToInt32(Session["Id"].ToString());
                            negetiveListEmployerObjList.Add(negetiveListEmployerObj);
                            emptyRow = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Alert.Show(error + " in data row # " + row);
                modalPopup.ShowSuccess(error + " in data row # " + row, "Watch List Upload");
                WriteException(row, ex.Message);
            }
            if (negetiveListEmployerObjList.Count > 0)
            {
                UploadNegetiveListEmployer(negetiveListEmployerObjList);
            }
        }

        private void WriteException(int row, string error)
        {
            try
            {
                error = error.Replace("'", "''");
                string sql = string.Format(@"insert into WatchListException (RowNo,Msg,UloadTime) values({0},'{1}','{2}')", row, error, DateTime.Now);
                DbQueryManager.ExecuteNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0 && sMessage != "")
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        private void UploadNegetiveListEmployer(List<NegetiveListEmployer> negetiveListEmployerObjList)
        {
            User sessionUser = (User)Session["UserDetails"];
            if (sessionUser == null)
            {
                LogFile.WriteLine("Session User is null");
                return;
            }
            int ListInsert = 0;
            if (negetiveListEmployerObjList.Count > 0)
            {
                ListInsert = SendWatchListInToDB(negetiveListEmployerObjList, sessionUser);
            }
            if (ListInsert == INSERTE || ListInsert == UPDATE)
            {
                modalPopup.ShowSuccess("File has been Successfully Uploaded", "Watch List Upload");
            }
            else
            {
                Alert.Show("Error in data ! \n Please check excel file");
            }
        }

        #region tarek04062011
        //tarek 04062011
        public int SendWatchListInToDB(List<NegetiveListEmployer> negetiveListEmployerObjList, User user)
        {
            int returnValue = 0;
            List<NegetiveListEmployer> insertNegetiveListEmployerList = new List<NegetiveListEmployer>();
            List<NegetiveListEmployer> updateNegetiveListEmployerList = new List<NegetiveListEmployer>();
            if (negetiveListEmployerObjList.Count > 0)
            {
                foreach (NegetiveListEmployer negetiveListEmployerObj in negetiveListEmployerObjList)
                {
                    returnValue = InsertNegetiveListEmployerList(negetiveListEmployerObj, user);
                }
            }
            return returnValue;
        }

        public NegetiveListEmployer SelectNegetiveListEmployer(string bankCompanyName, string branch)
        {
            DataTable negetiveEmployerDataTableObj = null;
            NegetiveListEmployer negetiveListEmployerObj = new NegetiveListEmployer();
            string searchCondition = "";
            if (!String.IsNullOrEmpty(bankCompanyName) && !String.IsNullOrEmpty(branch))
            {
                searchCondition += " WHERE  UPPER(NELE_BANKCOMPANY) = UPPER('" + AddSlash(bankCompanyName) + "') ";
                searchCondition += " AND UPPER(NELE_BRANCH) = UPPER('" + AddSlash(branch) + "') ";
            }
            else
            {
                if (!String.IsNullOrEmpty(bankCompanyName))
                {
                    searchCondition += " WHERE UPPER(NELE_BANKCOMPANY) = UPPER('" + AddSlash(bankCompanyName) + "') ";
                }
                if (!String.IsNullOrEmpty(branch))
                {
                    searchCondition += " WHERE UPPER(NELE_BRANCH) = UPPER('" + AddSlash(branch) + "') ";
                }
            }
            string queryString = "SELECT NELE_ID, NELE_BANKCOMPANY, NELE_BRANCH, NELE_ADDRESS, NELE_NEGATIVELIST, " +
                                 "NELE_WATCHLIST, NELE_EXCLUSION, NELE_DELIST, NELE_RESTRICTION, NELE_REMARKS, " +
                                 "CAST(NELE_INCLUSIONDATE AS CHAR) AS NELE_INCLUSIONDATE, CAST(NELE_CHANGEDATE AS CHAR) AS NELE_CHANGEDATE, CAST(NELE_ENTRYDATE AS CHAR) AS NELE_ENTRYDATE " +
                                 "FROM t_pluss_negativelistemployer " + searchCondition;

            negetiveEmployerDataTableObj = DbQueryManager.GetTable(queryString);
            try
            {
                if (negetiveEmployerDataTableObj != null)
                {
                    DataTableReader negetiveEmployerDataReaderObj = negetiveEmployerDataTableObj.CreateDataReader();
                    if (negetiveEmployerDataReaderObj != null)
                    {
                        while (negetiveEmployerDataReaderObj.Read())
                        {
                            negetiveListEmployerObj = new NegetiveListEmployer();
                            negetiveListEmployerObj.ID = Convert.ToInt32(negetiveEmployerDataReaderObj["NELE_ID"].ToString());
                            negetiveListEmployerObj.BankCompany = negetiveEmployerDataReaderObj["NELE_BANKCOMPANY"].ToString();
                            negetiveListEmployerObj.Branch = negetiveEmployerDataReaderObj["NELE_BRANCH"].ToString();
                            negetiveListEmployerObj.Address = negetiveEmployerDataReaderObj["NELE_ADDRESS"].ToString();
                            negetiveListEmployerObj.NegetiveList = negetiveEmployerDataReaderObj["NELE_NEGATIVELIST"].ToString();
                            negetiveListEmployerObj.WatchList = negetiveEmployerDataReaderObj["NELE_WATCHLIST"].ToString();
                            negetiveListEmployerObj.Exclusion = negetiveEmployerDataReaderObj["NELE_EXCLUSION"].ToString();
                            negetiveListEmployerObj.Delist = negetiveEmployerDataReaderObj["NELE_DELIST"].ToString();
                            negetiveListEmployerObj.Restriction = negetiveEmployerDataReaderObj["NELE_RESTRICTION"].ToString();
                            negetiveListEmployerObj.Remarks = negetiveEmployerDataReaderObj["NELE_REMARKS"].ToString();
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.InclusionDate = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_INCLUSIONDATE"]);
                                }
                                catch { negetiveListEmployerObj.InclusionDate = DateTime.Now; }
                            }
                            if (!String.IsNullOrEmpty(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"].ToString()))
                            {
                                try
                                {
                                    negetiveListEmployerObj.ChangeDate2 = Convert.ToDateTime(negetiveEmployerDataReaderObj["NELE_CHANGEDATE"]).ToString("dd-MM-yyyy");
                                }
                                catch { negetiveListEmployerObj.ChangeDate2 = DateTime.Now.ToString("dd-MM-yyyy"); }
                            }

                        }
                        negetiveEmployerDataReaderObj.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return negetiveListEmployerObj;
        }
        private int UpdateNegetiveListEmployerList(NegetiveListEmployer negetiveListEmployerObj, User user)
        {
            dbMySQL = new DatabaseConnection();
            bool flag = false;
            try
            {
                string mSQL = "";
                mSQL = "UPDATE t_pluss_negativelistemployer SET " +
                       "NELE_BANKCOMPANY='" + AddSlash(negetiveListEmployerObj.BankCompany.Replace("'", "''")) + "'," +
                       "NELE_BRANCH='" + AddSlash(negetiveListEmployerObj.Branch.Replace("'", "''")) + "'," +
                       "NELE_ADDRESS='" + AddSlash(negetiveListEmployerObj.Address.Replace("'", "''")) + "'," +
                       "NELE_NEGATIVELIST='" + negetiveListEmployerObj.NegetiveList + "'," +
                       "NELE_WATCHLIST='" + negetiveListEmployerObj.WatchList + "'," +
                       "NELE_EXCLUSION='" + negetiveListEmployerObj.Exclusion + "'," +
                       "NELE_DELIST='" + negetiveListEmployerObj.Delist.Replace("'", "''") + "'," +
                       "NELE_RESTRICTION='" + negetiveListEmployerObj.Restriction.Replace("'", "''") + "'," +
                       "NELE_REMARKS='" + AddSlash(negetiveListEmployerObj.Remarks.Replace("'", "''")) + "'," +
                       "NELE_INCLUSIONDATE='" + negetiveListEmployerObj.InclusionDate.ToString("yyyy-MM-dd") + "'," +
                       "NELE_CHANGEDATE='" + negetiveListEmployerObj.ChangeDate.ToString("yyyy-MM-dd") + "'," +
                       "NELE_ENTRYDATE ='" + negetiveListEmployerObj.EntryDate.ToString("yyyy-MM-dd") + "'," +
                       "NELE_USER_ID =" + negetiveListEmployerObj.UserId + "," +
                       "MAKER_PS_ID =" + negetiveListEmployerObj.UserId +
                       " WHERE NELE_ID=" + negetiveListEmployerObj.ID;

                WriteException(0, mSQL);

                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    flag = true;
                }
                //}
                if (flag == true)
                {
                    return UPDATE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR;
            }
        }
        private int InsertNegetiveListEmployerList(NegetiveListEmployer obj, User user)
        {
            dbMySQL = new DatabaseConnection();
            try
            {
                string mSQL = string.Format(@"INSERT INTO t_pluss_negativelistemployer_temp (NELE_BANKCOMPANY, NELE_BRANCH," +
                              "NELE_ADDRESS, NELE_NEGATIVELIST, " +
                              "NELE_WATCHLIST, NELE_EXCLUSION," +
                              "NELE_DELIST, NELE_RESTRICTION," +
                              "NELE_REMARKS, NELE_INCLUSIONDATE," +
                              "NELE_CHANGEDATE,NELE_ENTRYDATE," +
                              "NELE_USER_ID, MAKER_PS_ID, MAKE_DATE) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}', '{13}', '{14}')",
                              obj.BankCompany.Replace("'", "''"), obj.Branch.Replace("'", "''"), obj.Address.Replace("'", "''"), obj.NegetiveList, obj.WatchList, obj.Exclusion, obj.Delist.Replace("'", "''"), obj.Restriction.Replace("'", "''"), obj.Remarks.Replace("'", "''"), obj.InclusionDate.ToString("yyyy-MM-dd"), obj.ChangeDate.ToString("yyyy-MM-dd"), obj.EntryDate.ToString("yyyy-MM-dd"), obj.UserId, user.UserId, DateTime.Now.ToString());

                WriteException(0, mSQL);

                if (DbQueryManager.ExecuteNonQuery(mSQL) > -1)
                {
                    return INSERTE;
                }
                else
                {
                    return ERROR;
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("InsertNegetiveListEmployerList:: WatchListUploadUI :: ");
                LogFile.WriteLog(ex);
                return ERROR;
            }
        }
        //
        #endregion

        protected void btnFlush_Click(object sender, EventArgs e)
        {
            try
            {
                string result = _negativeList.FlushTemp();
                if (result == Operation.Success.ToString())
                {
                    modalPopup.ShowSuccess("Unapproved data has been successfully removed", "Watch List Upload");
                    BindData(1, 1);
                }
                else
                {
                    modalPopup.ShowError("There was an error while Flushing the temp data :: " + result, "Watch List Upload");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Error :: " + ex.Message, "Watch List Upload");
            }
        }
    }
}