﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Text;
using Bat.Common;
using BLL;
using BusinessEntities;
using DAL;
using System.Web.Configuration;
using AzUtilities;
using System.IO;
using System.Web.UI;
using System.Web;
using AzUtilities.Common;

namespace PlussAndLoan.UI.MFU
{
    public partial class NegativeListApprove : System.Web.UI.Page
    {
        public DeDupInfoManager _negativeList = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            _negativeList = new DeDupInfoManager();
            BindData();
        }

        private void BindData()
        {
            List<DeDupInfo> nList = new List<DeDupInfo>();
            nList = _negativeList.GetTempDedupNegativeList();

            gvRecords.DataSource = nList;
            gvRecords.DataBind();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            User actionUser = (User)Session["UserDetails"];

            string result = _negativeList.ApproveDeDupNegativListTemp(actionUser);

            if (result == Operation.Success.ToString())
            {
                BindData();
                modalPopup.ShowSuccess("Negative list data has been approved successfull", "Negative List");
            }
            else
            {
                modalPopup.ShowError(result, "Negative List");
            }
            BindData();
        }

        //protected void btnDelete_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string result = _negativeList.FlushDeDupNegativListTemp();
        //        if (result == Operation.Success.ToString())
        //        {
        //            modalPopup.Show("Flushing Temporary Data Finished Successfully", "Watch List Approve");
        //        }
        //        else
        //        {
        //            modalPopup.Show("Flushing Temporary Data Error :: " + result, "Watch List Approve");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        modalPopup.Show("Error Level Code -2 :: " + ex.Message, "Watch List Approve");
        //    }
        //}

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string csv = _negativeList.GetDedupNegativeListTempDataCSV();
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=NegativeList_Data.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(csv);
            Response.Flush();
            Response.End();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string result = _negativeList.FlushDeDupNegativListTemp();
                if (result == Operation.Success.ToString())
                {
                    BindData();
                    modalPopup.ShowSuccess("Unapproved data has been removed Successfully", "Negative List");
                }
                else
                {
                    modalPopup.ShowError("Flushing Temporary Data Error :: " + result, "Negative List");
                }
            }
            catch (Exception ex)
            {
                modalPopup.ShowError("Error Level Code -2 :: " + ex.Message, "Negative List");
            }
        }
    }
}
