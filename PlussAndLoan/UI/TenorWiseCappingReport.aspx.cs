﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI
{
    public partial class TenorWiseCappingReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            divDate.Text = Session["date1"].ToString() + " TO " + Session["date2"].ToString();
            
            DataTable dt = (DataTable)Session["tenorCap"];
                decimal tn1=0;
                decimal tn2=0;
                decimal ptc_36=0;
                decimal ptc_60=0;
                decimal amou=0;
                int tpc=0;
                decimal perc=0;
                decimal s_ptc_36=0;
                decimal s_ptc_60=0;
                decimal percTn2=0;
                string prodName="";

            if (dt != null)
            {
                
                for (int j = 0; j < dt.Rows.Count; j++)
                {

                    ptc_36 = Convert.ToDecimal(dt.Rows[j]["TENO_CAPP_36"]);

                    ptc_60 = Convert.ToDecimal(dt.Rows[j]["TENO_CAPP_60"]);

                    //if (ptc_36 == 0)
                    //{
                    //    ptc_36 = Convert.ToDecimal(Convert.ToDecimal("").ToString());
                    //}


                    //if (ptc_60 == 0)
                    //{
                    //    ptc_60 = Convert.ToDecimal("");
                    //}

                    tn1 = Convert.ToDecimal(dt.Rows[j]["tn1"]) + Convert.ToDecimal(dt.Rows[j]["TCOD_36"]);
                    tn2 = Convert.ToDecimal(dt.Rows[j]["tn2"]) + Convert.ToDecimal(dt.Rows[j]["TCOD_60"]);
                    prodName = dt.Rows[j]["PROD_NAME"].ToString();


                    amou = tn1 + tn2;
                    

                    if (amou == 0)
                    {
                        amou = 1;
                        tpc = 0;
                    }
                    else
                    {
                        tpc = 100;
                    }


                    perc = ((tn1/amou)*100);
                    

                    if (ptc_36 > 0)
                    {

                        ptc_36 = Convert.ToDecimal(ptc_36 + "%");

                        s_ptc_36 = perc - ptc_36;

                        s_ptc_36 = Math.Round(s_ptc_36, 3);

                        s_ptc_36 = Convert.ToDecimal(s_ptc_36 + "%");
                    }
                    //decimal s_ptc_60 = Convert.ToDecimal("");
                   
                    if (ptc_60 > 0)
                    {

                        ptc_60 = Convert.ToDecimal(ptc_60 + "%");
                        s_ptc_60 = perc - ptc_60;
                        s_ptc_60 = Math.Round(s_ptc_60, 3);
                        s_ptc_60 = Convert.ToDecimal(s_ptc_60 + "%");
                    }
                    //  $perc=preci($perc);
                    perc = Math.Round(perc, 3);
                    // s_ptc_36 = Convert.ToDecimal("");

                    percTn2 = ((tn2/amou)*100);
                    percTn2 = Math.Round(percTn2, 3);
                }

            }

            TableRow row = new TableRow();
            row.Cells.Add(new TableCell { Text = "<=36" });
            row.Cells.Add(new TableCell { Text = tn1.ToString() });
            row.Cells.Add(new TableCell { Text = perc.ToString() + " %" });
            row.Cells.Add(new TableCell { Text = ptc_36.ToString() });
            row.Cells.Add(new TableCell { Text = s_ptc_36.ToString() });
            TenorReport.Rows.Add(row);
            TableRow row2 = new TableRow();
            row2.Cells.Add(new TableCell { Text = ">36" });
            row2.Cells.Add(new TableCell { Text = percTn2.ToString() });
            row2.Cells.Add(new TableCell { Text = perc.ToString() + " %"});
            row2.Cells.Add(new TableCell { Text = ptc_60.ToString() });
            row2.Cells.Add(new TableCell { Text = s_ptc_60.ToString() });
            TenorReport.Rows.Add(row2);
            TableRow row3 = new TableRow();
            row3.Cells.Add(new TableCell { Text = "Total" });
            row3.Cells.Add(new TableCell { Text = amou.ToString() });
            row3.Cells.Add(new TableCell { Text = tpc.ToString() + " %" });
            row3.Cells.Add(new TableCell { Text = "&nbsp" });
            row3.Cells.Add(new TableCell { Text = "&nbsp" });
            TenorReport.Rows.Add(row3);

            ProdName.Text = prodName;
        }

        protected void Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
            Session.Clear();
        }

        protected void Close_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
            Session.Clear();
        }
    }
}