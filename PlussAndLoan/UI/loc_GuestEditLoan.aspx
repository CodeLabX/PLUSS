﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/GuestMaster.Master" AutoEventWireup="true" CodeBehind="loc_GuestEditLoan.aspx.cs" Inherits="PlussAndLoan.UI.loc_GuestEditLoan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>::: LOAN LOCATOR :::</title>
    <style>
        table.az-gridTb th .part1 {
            background-color: #f49970;
        }

        table.az-gridTb th {
            border-width: 1px;
            padding: 3px;
            border-style: solid;
            border: #c7c7c7 1px solid;
            background-color: #9fbce3;
        }

        .az-gridTb {
            width: 100%;
            border-collapse: collapse;
        }

            .az-gridTb td {
                padding: 1px;
                border: #c7c7c7 1px solid;
            }

            .az-gridTb tr {
                background: #d3e3f9;
            }

                .az-gridTb tr:nth-child(odd) {
                    background: #d3e3f9;
                }

                .az-gridTb tr:nth-child(even) {
                    background: #e4eaf3;
                }

        .azBold {
            font-weight: bold;
        }

        .az-pageTitle {
            color: whitesmoke;
            margin-top: 15px;
            margin-bottom: 5px;
            font-weight: bold;
            font-size: 1em;
            background-color: #017AA7;
            padding: 2px 2px 2px 2px;
            box-shadow: 1px 1px black;
        }

        .leftDiv {
            float: left;
        }

        .rightDiv {
            float: right;
        }

        .centerAlign {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div id="divViewApplication" class="centerAlign" runat="server">
        <input id="status_update" type="hidden" value="100" runat="server" />
        <div style="width: 100%; background-color: rgb(5, 110, 132); color: white; margin-top: 1px">
            <h3>Loan Information</h3>
        </div>
        <table width="100%" class="az-gridTb">
            <tr>
                <td style="width: 60%">
                    <table width="100%">
                        <tr>
                            <td style="width: 40%" class="rightAlign azBold">Application ID:</td>
                            <td class="leftAlign">&nbsp;<span id="llid" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Product:</td>
                            <td class="leftAlign">&nbsp;<span id="spProduct" runat="server"></span></td>

                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Source Name:</td>
                            <td class="leftAlign">&nbsp;<span id="spSource" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Department Name:</td>
                            <td class="leftAlign">&nbsp;<span id="spDept" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Submission Date:</td>
                            <td class="leftAlign">&nbsp;<span id="spSubmissionDate" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Application Date:</td>
                            <td class="leftAlign">&nbsp;<span id="spApplicationDate" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">A/C No.:</td>
                            <td class="leftAlign">&nbsp;<span id="spAccNo" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Loan A/C No.:</td>
                            <td class="leftAlign">&nbsp;<span id="spLoanAcc" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">A/C Type:</td>
                            <td class="leftAlign">&nbsp;<span id="spAccType" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Submitted By:</td>
                            <td class="leftAlign">&nbsp;<span id="spSubmitBy" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Applied Loan Amount:</td>
                            <td class="leftAlign">&nbsp;<span id="spAmount" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Conditional Approved Amount:</td>
                            <td class="leftAlign">&nbsp;<span id="spConApproAmnt" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Approved Amount:</td>
                            <td class="leftAlign">&nbsp;<span id="spApproAmnt" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Customer Name:</td>
                            <td class="leftAlign">&nbsp;<span id="spCustomer" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Organization:</td>
                            <td class="leftAlign">&nbsp;<span id="spOrganization" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Profession:</td>
                            <td class="leftAlign">&nbsp;<span id="spProfession" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Loan Status:</td>
                            <td class="leftAlign">&nbsp;<span id="spLoanStatus" runat="server"></span></td>
                        </tr>
                        <tr>
                            <td class="rightAlign azBold">Decision Status:</td>
                            <td class="leftAlign">&nbsp;<span id="spDecission" runat="server"></span></td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <fieldset class="leftAlign">
                        <legend style="font-weight: normal; font-size: 1em">CIB Report Status</legend>
                        <%--  <asp:checkbox runat="server" text="Report Received" id="cibRec" autopostback="true" OnCheckedChanged="cibRec_CheckedChanged" />
                        <br />
                        <asp:checkbox runat="server" text="System Error" id="systemErr" autopostback="true" OnCheckedChanged="systemErr_CheckedChanged" />
                        <br />
                        <asp:checkbox runat="server" text="Undertaking Returned" id="underTaking" autopostback='false' OnCheckedChanged="underTaking_CheckedChanged" />
                        <br />--%>

                        <input runat="server" type="checkbox" id="cibRec" onclick="checkcibRec()" />Report Received<br />
                        <input runat="server" type="checkbox" id="systemErr" onclick="checksystemErr()" />System Error<br />
                        <input runat="server" type="checkbox" id="underTaking" onclick="checkunderTaking()" />Undertaking Returned

                    </fieldset>
                    <div class="centerAlign" style="margin-top: 50px; width: 60%; margin: auto" id="dynamicButton11" runat="server">
                        <br />
                        <br />
                        <button runat="server" style="width: 170px" id="btnOtherVc" onserverclick="OnClickOtherVc">TO OTHER VC</button>
                        <button runat='server' style='width: 170px; margin-top: 2px' id='btnUpdate' onserverclick='OnClickUpdate'>UPDATE</button>
                        <button runat='server' style='width: 170px; margin-top: 2px' id='btnLoanOps' onserverclick='OnClickLoanOps'>TO LOAN-OPS</button>
                        <button runat='server' style='width: 170px; margin-top: 2px' id='btnLoanOpsCr' onserverclick='OnClickToCredit'>TO CREDIT</button>
                        <button runat='server' id='btnOnProcess' style='width: 170px' onserverclick='OnClickOnProcess'>On Process</button>
                        <button runat='server' style='width: 170px; margin-top: 2px' id='btnUpdateProfile' onserverclick='OnClickUpdateProfile'>Update Loan Profile</button>
                        <button runat='server' style='width: 170px' id='btnOnStatus' onserverclick='OnClickOnStatus'>loanStateName</button>
                        <button runat='server' style='width: 170px' id='btnToSource' onserverclick='OnClickToSource'>To Source</button>
                        <button runat='server' style='width: 170px; margin-top: 2px' id='btnOnHold' onserverclick='OnClickOnHold'>On Hold</button>
                        <button runat='server' style='width: 170px' id='btnDisbursed' onserverclick='OnClickDisbursed'>Disbursed</button>
                        <button runat='server' style='width: 170px' id='btnLoanDisbursed' onserverclick='OnClickLoanDisbursed'>Loan Disbursed</button>
                        <button runat='server' style='width: 170px' id='btnToMaker' onserverclick='OnClickToMaker'>To Maker</button>
                        <button runat='server' style='width: 170px' id='btnCheckList' onserverclick='OnClickToCheckList'>Check List</button>
                    </div>
                </td>
            </tr>
        </table>
        <div class="az-pageTitle" style="padding: 0 0 0 0; box-shadow: none; margin-bottom: 0; margin-top: 0; color: black">
            <div class="leftDiv widthSize25_per leftAlign">Loan Request Update Details</div>
            <div class="rightDiv widthSize25_per rightAlign">TOTAL TAT#&nbsp;<span id="spTotatlTat" runat="server">0</span>&nbsp;(days)&nbsp;</div>
            <div class="clear"></div>
        </div>
        <div id="tdDetails" runat="server"></div>
        <%-- <table class="az-gridTb" style="margin-top: -1px">
            <thead>
                <tr>
                    <th>Date Time</th>
                    <th>Person Name</th>
                    <th>Dept Name</th>
                    <th>Process status</th>
                    <th>Remarks</th>
                    <th>Individual TAT</th>
                </tr>
            </thead>
            <tr>
                <td><span id="spDate" runat="server">&nbsp;</span></td>
                <td><span id="spPerson" runat="server"></span></td>
                <td><span id="spOtherDept" runat="server"></span></td>
                <td><span id="spProcess" runat="server"></span></td>
                <td><span id="spRemarks" runat="server"></span></td>
                <td><span id="spTat" runat="server"></span></td>
            </tr>
        </table>--%>

        <button style="margin-top: 5px"><a href="/UI/LocLoanHome.aspx" style="color: black">Close</a></button>

    </div>

    <div id="toOtherVc" runat="server">
        <div style="width: 100%; background-color: rgb(5, 110, 132); color: white; margin-top: 1px">
            <h3>To Other VC</h3>
        </div>
        <table>
            <tr>
                <td colspan="2" style="font-weight: bold">For Application ID # <span runat="server" id="otherVcAppId"></span></td>
            </tr>
            <tr>
                <td style="font-weight: bold">USER NUMBER</td>
                <td>
                    <asp:TextBox runat="server" ID="otherUserNumber"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <button runat='server' onserverclick="OnClickOtherVcOk">OK</button>
                    &nbsp;
                    <button runat='server' onserverclick="CloseOtherVc">Close</button>
                </td>
            </tr>
        </table>
    </div>

    <div id="divUpdate" runat="server">
        <div style="width: 100%; background-color: rgb(5, 110, 132); color: white; margin-top: 1px">
            <h3>Update Loan Applicatio Information</h3>
        </div>
        <fieldset>

            <table width="60%">
                <tr>
                    <td style="width: 30%">PRODUCT:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="adProduct" Width="202px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%">APPLICATION DATE:
                    </td>
                    <td>
                        <input id="adApplicationDate" type="text" runat="server" style="width: 200px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%">ENTRY FROM SOURCE:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="adSource" Width="202px" />
                        <input type="checkbox" id="chkAdCpf" runat="server" />
                        CPF
                    <input type="checkbox" id="chkAdPriority" runat="server" />
                        Priority
                    </td>
                </tr>
                <tr>
                    <td>ACCOUNT NUMBER:
                    </td>
                    <td>
                        <input type="text" runat="server" id="adAccPre" style="width: 55px;" maxlength="2" min="0" />
                        <input type="text" runat="server" id="adAccMid" style="width: 81px;" maxlength="7" min="0" />
                        <input type="text" runat="server" id="adAccLast" style="width: 55px;" min="0" maxlength="2" /><span class="az-required">*</span>
                    </td>
                </tr>
                <tr>
                    <td>CUSTOMER NAME:
                    </td>
                    <td>
                        <input type="text" id="txtAdCust" runat="server" style="width: 200px" />
                    </td>
                </tr>
                <tr>
                    <td>AMOUNT OF LOAN APPLIED:
                    </td>
                    <td>
                        <input id="txtAdAmount" type="text" runat="server" style="width: 200px" />
                    </td>
                </tr>
                <tr>
                    <td>APPLICANT PROFESSION:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="adProfession" Width="202px" />
                    </td>
                </tr>
                <tr>
                    <td>ORGANIZATION:
                    </td>
                    <td>
                        <input id="txtAdOrganization" type="text" runat="server" style="width: 200px" />
                    </td>
                </tr>
                <tr>
                    <td>If Previously Decliend ( APP. ID):
                    </td>
                    <td>
                        <input id="adDeclined" type="text" runat="server" style="width: 200px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="widthSize30_per">&nbsp;</label>
                    </td>
                    <td>
                        <input type="button" value="Update" id="btnAdUpdate" runat="server" style='width: 100px; margin-top: 2px' onserverclick="UpdateLoanInfo" />
                        <input type="button" value="Close" id="btnAdClose" runat="server" onserverclick="CloseUpdate" style='width: 100px; margin-top: 2px' />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>

    <div id="divLoanOps" runat="server">
        <div style="width: 100%; background-color: rgb(5, 110, 132); color: white; margin-top: 1px">
            <h3>To Loan Ops</h3>
        </div>
        <fieldset>
            <table>
                <tr>
                    <td><b>Remarks (If Any)</b></td>
                    <td>
                        <textarea id="lopsRemarks" runat="server" cols="100"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="button" id="btnLoanOpsRe" runat="server" onserverclick="OnClickLoanOpsPost" />
                        <input type="button" id="Button1" runat="server" onserverclick="OnClickLoanOpsClose" value="Close" />
                    </td>
                </tr>
            </table>
        </fieldset>

    </div>

    <div id="divToCredit" runat="server">
        <div style="width: 100%; background-color: rgb(5, 110, 132); color: white; margin-top: 1px">
            <h3>Forward to credit</h3>
        </div>
        <fieldset>
            <table>
                <tr>
                    <td><b>Remarks (If Any)</b></td>
                    <td>
                        <textarea id="toCrRemarks" runat="server" cols="100"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="button" id="btnToCrSave" runat="server" onserverclick="OnClickToCreditPost" />
                        <input type="button" id="btnToCrClose" runat="server" onserverclick="OnClickToCreditClose" value="Close" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div id="divOnProcess" runat="server"></div>
    <div id="divUpdateProfile" runat="server">
        <table width="780" height="177" border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
            <tr>
                <td height="49" colspan="2">
                    <div align="center">
                        <font face="Verdana, Arial, Helvetica, sans-serif"><strong>Edit Loan Applicant Person's Information<span runat="server" id="cust"></span>
                        </strong></font>
                    </div>
                </td>
            </tr>
            <tr>
                <td width="34%" height="30" bgcolor="#D5E2C5">
                    <div align="right">
                        <font face="Verdana, Arial, Helvetica, sans-serif">Owner(s):</font>
                    </div>
                </td>
                <td width="66%" height="30" bgcolor="#E7F7C6">
                    <div align="left">
                        <font face="Verdana, Arial, Helvetica, sans-serif"> 
                            <input name="owner" type="text" runat="server" id="owner" style="border:1px solid #000033; background-color:#F5F8FC; "  value="" size="35"/>
                      </font>
                    </div>
                </td>
            </tr>
            <tr>
                <td height="30" bgcolor="#D5E2C5">
                    <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif">Mother's Name : </font></div>
                </td>
                <td height="30" bgcolor="#E7F7C6">
                    <font face="Verdana, Arial, Helvetica, sans-serif"> 
                           <input name="mother" type="text"  runat="server" id="mother" style="border:1px solid #000033; background-color:#F5F8FC;" value="" size="35"/>
                   </font>
                </td>
            </tr>
            <tr>
                <td height="30" bgcolor="#D5E2C5">
                    <div align="right">
                        <font face="Verdana, Arial, Helvetica, sans-serif"> (IL):</font>
                    </div>
                </td>
                <td height="30" bgcolor="#E7F7C6"><font face="Verdana, Arial, Helvetica, sans-serif"> 
                    <input name="tenor" type="text"  runat="server"  id="tenor" style="border:1px solid #000033; background-color:#F5F8FC; " value="" size="20"/>
                 </font></td>
            </tr>
            <tr>
                <td height="30" bgcolor="#D5E2C5">
                    <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif">User Define Field 1: </font></div>
                </td>
                <td height="30" bgcolor="#E7F7C6"><font face="Verdana, Arial, Helvetica, sans-serif"> 
                 <asp:DropDownList runat="server" ID="ud">
                      <asp:ListItem></asp:ListItem>
                      <asp:ListItem>X1</asp:ListItem>
                      <asp:ListItem>Y1</asp:ListItem>
                      <asp:ListItem>Z1</asp:ListItem>
                      <asp:ListItem>Z2</asp:ListItem>
                      <asp:ListItem>Z3</asp:ListItem>
                      <asp:ListItem>Z4</asp:ListItem>
                      <asp:ListItem>Z5</asp:ListItem>
                      <asp:ListItem>Z6</asp:ListItem>
                      <asp:ListItem>Z7</asp:ListItem>
                      <asp:ListItem>Z8</asp:ListItem>
                    </asp:DropDownList>
             </font></td>
            </tr>
            <tr>
                <td height="30" bgcolor="#D5E2C5">
                    <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif">UDF2:</font></div>
                </td>
                <td height="30" bgcolor="#E7F7C6"><font face="Verdana, Arial, Helvetica, sans-serif"> 
                     <input name="ud2" type="text"  runat="server"  id="ud2" style="border:1px solid #000033; background-color:#F5F8FC; " value="" size="20"/>
                 </font></td>
            </tr>
            <tr>
                <td height="30" bgcolor="#D5E2C5">
                    <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif">UDF3:</font></div>
                </td>
                <td height="30" bgcolor="#E7F7C6"><font face="Verdana, Arial, Helvetica, sans-serif"> 
                     <input name="ud3" type="text"  runat="server"  id="ud3" style="border:1px solid #000033; background-color:#F5F8FC; " value="" size="20"/>
                 </font></td>
            </tr>
            <tr>
                <td height="30" bgcolor="#D5E2C5">
                    <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif">UDF4:</font></div>
                </td>
                <td height="30" bgcolor="#E7F7C6"><font face="Verdana, Arial, Helvetica, sans-serif"> 
                   <input name="ud4" type="text" runat="server"  id="ud4" style="border:1px solid #000033; background-color:#F5F8FC; " value="" size="20"/>
           </font></td>
            </tr>
            <tr>
                <td>
                    <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif"></font></div>
                </td>
                <td><font face="Verdana, Arial, Helvetica, sans-serif"> 
                     <input type="button" runat="server" name="Submit" value="Submit" OnServerClick="UpdateProfile" style="border:1px solid #999990; background-color:#F5F8FC"/>
                    <input type="button" runat="server" name="Close" value="Close" OnServerClick="CloseProfile" style="border:1px solid #999990; background-color:#F5F8FC"/>
            </font></td>
            </tr>
        </table>

    </div>


    <div id="divOnStatus" runat="server">
        <div id="onStatusHtm" runat="server">
            <table width='90%' border='0' align='center' cellpadding='0' cellspacing='1' bordercolor='#00CCCC' bgcolor='#e7f7c6'>
                <tr align='right' valign='middle' bgcolor='#BDD3A5'><td height='24' colspan='2'>&nbsp;</td></tr> 
                <tr align='right' valign='middle' bgcolor='#BDD3A5'>
                    <td width='33%' height='25' bgcolor='#D5E2C5'>
                        <div align=right><font color='#000000' size='2' face='Verdana, Arial, Helvetica, sans-serif'>Loan Status:</font></div>
                    </td>
                    <td height='25' align=left valign='middle' bgcolor='#E7F7C6'>
                         <div id="dvStateName" runat="server">
                             <font size='2' face='Verdana, Arial, Helvetica, sans-serif'> <font color=#000000><b>&nbsp;&nbsp;Initiate</b></font></font>
                         </div>
                    </td>
                </tr>
                <tr bgcolor=#e7f7c6 id='r210' runat='server'>
                    <td >
                        <div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif><font color=#FF0000></font>Approved amount: </font></font></div>
                    </td>
                    <td colspan=3><input type=text name=ap_amou id='ap_amou' runat='server' style='border:1px solid #003300; background-color:#F5F8FC' value='' /></td>
                </tr>
                <tr bgcolor=#e7f7c6 id='r211' runat='server'>
                     <td >
                         <div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif><font color=#FF0000></font>Approved Level: </font></font></div>
                     </td>
                    <td colspan=3>
                        <select name='appr_lvl' id='appr_lvl' runat='server'>
                            <option value=''></option>
                            <option value=1>1</option>
                            <option value=2>2</option>
                            <option value=3>3</option>
                        </select>
                    </td>
                </tr><tr id='r212' runat='server'>
                         <td height='30' bgcolor='#D5E2C5'>
                             <div align=right><font face='Verdana, Arial, Helvetica, sans-serif'><font color=#FF0000></font>Term:</font></div>
                         </td>
                         <td height='30' bgcolor='#E7F7C6'><font face='Verdana, Arial, Helvetica, sans-serif'><input name='tenor'  runat='server' type='text' id='tenor1' style='border:1px solid #000033; background-color:#F5F8FC; ' value='' size='20'/> </font>
                             </td>
                     </tr>
                <tr id='r220' runat='server'>
                    <td bgcolor=#D5E2C5 >
                        <div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif><font color=#FF0000></font>Amount: </font></font></div>
                    </td>
                    <td colspan=3><input type=text name=cond_approv_amou  id='cond_approv_amou' runat='server' style='border:1px solid #003300; background-color:#F5F8FC' value='' />
                    </td>
                </tr>
                <tr id='r40'  runat='server' bgcolor=#e7f7c6>
                    <td bgcolor='#D5E2C5'>
                        <div align='right'>Loan Disbursement date: </div>
                    </td>
                    <td colspan='2' >
                        <font color=#000000 face=Courier New, Courier, mono>
                            <select name=d1 id=d1  runat='server'>
                                 <option value=i> 1 </option> <option value=i> 2 </option> <option value=i> 3 </option> <option value=i> 4 </option> <option value=i> 5 </option> <option value=i> 6 </option> <option value=i> 7 </option> <option value=i> 8 </option> <option value=i> 9 </option> <option value=i> 10 </option> <option value=i> 11 </option> <option value=i> 12 </option> <option value=i> 13 </option> <option value=i> 14 </option> <option value=i> 15 </option> <option value=i> 16 </option> <option value=i> 17 </option> <option value=i> 18 </option> <option value=19 selected> 19 </option> <option value=i> 20 </option> <option value=i> 21 </option> <option value=i> 22 </option> <option value=i> 23 </option> <option value=i> 24 </option> <option value=i> 25 </option> <option value=i> 26 </option> <option value=i> 27 </option> <option value=i> 28 </option> <option value=i> 29 </option> <option value=i> 30 </option> <option value=i> 31 </option>
                            </select>
                        </font>
                        <font size=2 color=#000000 face=Courier new, courier, mono>
                             <select name=m1 id='m1'  runat='server' style='border:1px solid #4179C0; background-color:#F5F8FC'> <option value=1>January</option> <option value=2>February</option> <option value=3>March</option> <option value=4>April</option> <option value=5 selected>May</option> <option value=6>June</option> <option value=7>July</option> <option value=8>August</option> <option value=9>September</option> <option value=10>October</option> <option value=11>November</option> <option value=12>December</option></select> 
                             <select name=y1 id='y1'  runat='server'> <option value=2004> 2004 </option> <option value=2005> 2005 </option> <option value=2006> 2006 </option> <option value=2007> 2007 </option> <option value=2008> 2008 </option> <option value=2009> 2009 </option> <option value=2010> 2010 </option> <option value=2011> 2011 </option> <option value=2012> 2012 </option> <option value=2013> 2013 </option> <option value=2014> 2014 </option> <option value=2015> 2015 </option> <option value=2016> 2016 </option> <option value=2017> 2017 </option> <option value=2018> 2018 </option> <option value=2019 selected> 2019 </option> <option value=2020> 2020 </option></select>
                        </font>
                        <font Size=2 color=#000000 face=Courier New, Courier, mono>&nbsp;</font>
                    </td>
                </tr>
                <tr id='r41'  runat='server' bgcolor=#e7f7c6>
                    <td height=29 bgcolor='#D5E2C5'><div align=right><font color=#000000><font Size=2 face=Verdana, Arial, Helvetica, sans-serif><font color=#FF0000></font>Disburse amount: </font></font></div></td><td colspan=2><input type=text name=disb_amou  id='disb_amou' runat='server' style='border:1px solid #003300; background-color:#F5F8FC' value='' /></td> 
                </tr>
                <tr bgcolor=#e7f7c6  id='r42'  runat='server'>
                    <td height=29 bgcolor='#D5E2C5'>
                         <div align=right>
                             <font size=2 color=#000000 face=Verdana, Arial, Helvetica, sans-serif>Loan A\C No:</font>
                         </div>
                    </td>
                    <td colspan=2>
                        <table width='50%' border='0' id='tblAcc' runat='server'>
                            <tr>
                                <td width='5%'> <input name='pre_acc'  id='pre_acc' runat='server' type='text'   style='border:1px solid #000033; background-color:#F5F8FC;' size='4' maxlength='2' onKeyup='autotab(this, document.form1.mid_acc)'/></td><td width='2%' align='center' valign='middle'><strong><font size='4'>-</font></strong></td><td width='18%'> <input name='mid_acc'  id='mid_acc' runat='server' type='text'   style='border:1px solid #000033; background-color:#F5F8FC; ' size='15' maxlength='7' onKeyup='autotab(this, document.form1.post_acc)'/></td><td width='2%' align='center' valign='middle'><strong><font size='4'>-</font></strong></td><td width='25%'> <input name='post_acc'  id='post_acc' runat='server' type='text'  style='border:1px solid #000033; background-color:#F5F8FC; ' size='4' maxlength='2'/></td>  
                            </tr>
                        </table>
                        <input name=loan_ac  id='loan_ac' runat='server' type=text value='32-3435353-32' style='border:1px solid #000033; background-color:#F5F8FC;' disabled/>
                    </td>
                </tr>
                <tr bgcolor=#e7f7c6 id='r260' runat='server'>
                    <td width=38%>
                        <div align=right id="r260Text" runat="server"><font color=#000000>
                        <font Size=2 face=Verdana, Arial, Helvetica, sans-serif>Deferred Reason:</font></font>
                        </div>
                    </td>
                    <td colspan=3>
                        <select name=def_dec id='def_dec'  runat='server'></select>
                    </td>
                </tr>
                <tr bgcolor='#BDD3A5'><td align='right' valign='middle' bgcolor='#D5E2C5'>
                           <div align='right'><font color='#000000' size='2' face='Verdana, Arial, Helvetica, sans-serif'>Remarks (If any) :</font></div></td>
                    <td bgcolor='#E7F7C6'> <font size='2' face='Verdana, Arial, Helvetica, sans-serif'>
                            <textarea name='remarks'  id='remarks' runat='server' cols='50' rows='3' style='border:1px solid #000033; background-color:#F5F8FC'></textarea>
                   </font></td>
                </tr>
                <tr bgcolor='#BDD3A5'>
                    <td height='40'><font size='2' face='Verdana, Arial, Helvetica, sans-serif'>&nbsp;</font></td>
                    <td height='40' > <font size='2' face='Verdana, Arial, Helvetica, sans-serif'>
                                          <input name='Submit' id="hdnSubmit" runat="server" type='hidden'  value='Initiate'/></font>
                        </td> 
                </tr>
            </table>
            

        </div>
        <table width='90%' border='0' align='center' cellpadding='0' cellspacing='1' bordercolor='#00CCCC' bgcolor='#e7f7c6'>
            <tr align='right' valign='middle' bgcolor='#BDD3A5'>
                <td height='24' width='33%'>&nbsp;</td>
                <td>&nbsp;<input type='button' name='Submit' id='submit4' runat='server' value='' onserverclick='SaveOnStatus' style='border: 1px solid #83b67a; background-color: #D8EED5' />
                    &nbsp;<input name='Submit2' id='submit2' runat='server' type='button' style='border: 1px solid #83b67a; background-color: #D8EED5' onserverclick='CloseOnStatus' value='Close' />
                </td>
            </tr>

        </table>

    </div>
    <div id="divToSource" runat="server">
          <table width="780" height="142" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#00CCCC" bgcolor="E7F7C6">
            <tr bgcolor="#BDD3A5"> 
              <td width="21%" height="28">&nbsp;</td>
              <td align="left"><font face="Verdana, Arial, Helvetica, sans-serif"> 
                <b><asp:Label runat="server" ID="status"></asp:Label> </b> 
                </font></td>
            </tr>
            <tr> 
              <td height="29" bgcolor="#D5E2C5">
                <div align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Remarks 
                  (If any):</font></div></td>
              <td><textarea id="toSourceRemarksArea" cols="70" rows="4" runat="server" style="border:1px solid #666666; background-color:#F5F8FC"></textarea></td>
            </tr>
            <tr bgcolor="#BDD3A5"> 
              <td height="40">&nbsp;</td>
              <td height="40">
                  <font face="Verdana, Arial, Helvetica, sans-serif"> 
                     <input type="Submit" name="submit" runat="server" OnServerClick="SubmitToOnSource" value="  Done " style="border:1px solid #999999; background-color:#F5F8FC"/>
                </font> &nbsp;
                  <font face="Verdana, Arial, Helvetica, sans-serif"> 
                     <input type="Submit" name="submit" runat="server" OnServerClick="CloseToOnSource" value="  Close " style="border:1px solid #999999; background-color:#F5F8FC"/>
                </font>
              </td>
 
            </tr>
          </table>

    </div>

    <div id="divOnHold" runat="server">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="#e7f7c6" tr >
            <tr align="right" valign="middle" bgcolor="#BDD3A5"> 
              <td height="24" colspan="2">&nbsp;</td>
            </tr>
            <tr bgcolor="#BDD3A5"> 
              <td align="right" valign="middle" bgcolor="#D5E2C5"> <div align="right"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">On Hold for:</font></div></td>
              <td bgcolor="#E7F7C6"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
                <select name="sel_onhold" id="sel_onhold" runat="server"></select></font>
              </td>
            </tr>
   
        <tr bgcolor="#BDD3A5"> 
          <td align="right" valign="middle" bgcolor="#D5E2C5"> <div align="right"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif">Remarks (If any) :</font></div></td>
          <td bgcolor="#E7F7C6"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
            <textarea name="remarks" id="onHoldRemarks" runat="server" cols="50" rows="3" style='border:1px solid #000033; background-color:#F5F8FC'></textarea>
            </font></td>
        </tr>
	
        <tr bgcolor="#BDD3A5">
          <td height="40"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp; </font></td>
          <td height="40" >
               <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
               <input type="submit" style='border:1px solid #83b67a; background-color:#D8EED5'  value="On Hold" runat="server" OnServerClick="OnHoldSubmit"/>&nbsp; 
               <input name="Submit2" type="button" style='border:1px solid #83b67a; background-color:#D8EED5' runat="server" OnServerClick="OnHodlClose" value="Close"/>
            </font>
          </td>
		<%--      <input type="hidden" name="id" value="<? echo $id; ?>">
              <input type="hidden" name="status_update" value="<? echo $type; ?>">
              <input type="hidden" name="showsearch" value="<? echo $showsearch; ?>">--%>
         </tr>
  </table>
        

    </div>
    <div id="divisbursed" runat="server"></div>
    <div id="divLoanDisbursed" runat="server"></div>
    <div id="divToMaker" runat="server"></div>
    <span></span>
    <script type="text/javascript">
        $("#toOtherVc").hide();
        function goBack() {
            window.history.back();
        }
        function checkcibRec() {
            if ($('input[id=cibRec]').is(':checked')) {
                $('input[id=cibRec]').attr('checked', true);
                $('input[id=systemErr]').attr('checked', false);
                $('input[id=underTaking]').attr('checked', false);
            }
            else {
                $('input[id=cibRec]').attr('checked', false);
                $('input[id=systemErr]').attr('checked', true);
                $('input[id=underTaking]').attr('checked', false);
            }
        }
        function checksystemErr() {

            if ($('input[id=systemErr]').is(':checked')) {
                $('input[id=cibRec]').attr('checked', false);
                $('input[id=systemErr]').attr('checked', true);
                $('input[id=underTaking]').attr('checked', false);
            }
            else {
                $('input[id=cibRec]').attr('checked', false);
                $('input[id=systemErr]').attr('checked', false);
                $('input[id=underTaking]').attr('checked', true);
            }

        }
        function checkunderTaking() {

            if ($('input[id=underTaking]').is(':checked')) {
                $('input[id=cibRec]').attr('checked', false);
                $('input[id=systemErr]').attr('checked', false);
                $('input[id=underTaking]').attr('checked', true);
            }
            else {
                $('input[id=cibRec]').attr('checked', true);
                $('input[id=systemErr]').attr('checked', false);
                $('input[id=underTaking]').attr('checked', false);
            }
        }
        function day() {
            var conent = "<select name=d1 id=d1>";
            var d = date("d");
            for (var i = 1; i < 32; i++) {
                if (d == i)
                    conent += " <option value='" + i + "' selected> $i </option>";
                else
                    conent += " <option value=i> i </option>";
            }
            $("#spDay").html(conent);
        }

    </script>
</asp:Content>