﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI
{
    public partial class LocLoanInformationUpdate : System.Web.UI.Page
    {
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        readonly LocatorApplicationService _service = new LocatorApplicationService();

        protected void Page_Load(object sender, EventArgs e)
        {
            msgLabel.Text = "";
            if (!IsPostBack)
            {

                ProductDropDownListItem();
                SourceDropDownListItem();
                ProfessionDropDownListItems();
            }
            LoanAppInfoDiv.Visible = true;
            LoanAppViewDiv.Visible = false;
            dealersDiv.Visible = false;
        }

        private void ProfessionDropDownListItems()
        {
            List<Profession> professionListObj = _loanApplicationManagerObj.GetAllProfession();
            Profession professionObj = new Profession();
            professionObj.Id = 0;
            professionObj.Name = "";
            professionListObj.Insert(0, professionObj);
            professionDropdownList.DataSource = professionListObj;
            professionDropdownList.DataTextField = "Name";
            professionDropdownList.DataValueField = "Id";
            professionDropdownList.DataBind();
        }

        private void SourceDropDownListItem()
        {
            List<Source> sourceLisObj = _loanApplicationManagerObj.GetAllSource();
            Source sourceObj = new Source();
            sourceObj.SourceId = 0;
            sourceObj.SourceName = "";
            sourceLisObj.Insert(0, sourceObj);
            sourceDropDownList.DataTextField = "SourceName";
            sourceDropDownList.DataValueField = "SourceId";
            sourceDropDownList.DataSource = sourceLisObj;
            sourceDropDownList.DataBind();
        }

        private void ProductDropDownListItem()
        {
            List<Product> productListObj = _loanApplicationManagerObj.GetAllProduct();
            var productObj = new Product { ProductId = 0, ProductName = "" };
            productListObj.Insert(0, productObj);
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "ProductId";
            productDropDownList.DataSource = productListObj;
            productDropDownList.DataBind();
        }


        protected void GoButton_Click(object sender, EventArgs e)
        {
            LoanAppInfoDiv.Visible = false;
            LoanAppViewDiv.Visible = true;
            if (loanAppIdTextBox.Text == "")
            {
                errorMsgLabel.Text = "Please Enter Loan Application ID";
            }
            var id = Convert.ToInt32(loanAppIdTextBox.Text);
            DateTime dateTime = Convert.ToDateTime(WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"]);
            var loan = _service.GetLoanApplications(dateTime, id);
            if (loan != null)
            {
                productDropDownList.SelectedValue = loan.Rows[0]["ProductId"].ToString();
                if (loan.Rows[0]["ProductId"].ToString() == 2.ToString())
                {
                    dealersDiv.Visible = true;
                }
                customerNameTextBox.Text = loan.Rows[0]["ApplicantName"].ToString();
                dateTextBox.Text = Convert.ToDateTime(loan.Rows[0]["AppliedDate"].ToString()).ToString("yyyy-MM-dd");
                sourceDropDownList.SelectedValue = loan.Rows[0]["SourceId"].ToString();
                if (!String.IsNullOrEmpty(loan.Rows[0]["ProfessionId"].ToString()))
                {
                    professionDropdownList.SelectedValue = loan.Rows[0]["ProfessionId"].ToString();
                }
                else
                {
                    professionDropdownList.SelectedValue = 0.ToString();
                }
                appliedAmountTextBox.Text = loan.Rows[0]["AppliedAmount"].ToString();
                companyNameTextBox.Text = loan.Rows[0]["Organization"].ToString();
                txtDecliendLoan.Text = loan.Rows[0]["PreDeclined"].ToString();
                DealearsTextBox.Text = loan.Rows[0]["dealer_name"].ToString();

                var accTypeId = Convert.ToInt32(loan.Rows[0]["AccTypeId"]);
                if (accTypeId == 1)
                {
                    chkCpf.Checked = true;
                }
                else if (accTypeId == 2)
                {
                    chkPriority.Checked = true;
                }
                else if (accTypeId == 3)
                {
                    chkCpf.Checked = true;
                    chkPriority.Checked = true;
                }
                else
                {
                    chkCpf.Checked = false;
                    chkPriority.Checked = false;
                }

                string AccNo = loan.Rows[0]["AccNo"].ToString();
                string[] accno = AccNo.Split(Convert.ToChar("-"));
                txtPreAcc.Text = accno[0];
                txtMidAcc.Text = accno[1];
                txtPostAcc.Text = accno[2];
                
            }
            
        }

        protected void btnUpdateLoanApplication_Click(object sender, EventArgs e)
        {

            var id = Convert.ToInt32(loanAppIdTextBox.Text);
            var loan = GetLoanObj();
            DateTime dateTime = Convert.ToDateTime(WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"]);
            var selectedLoan = _service.GetLoanApplications(dateTime, id);
            var obj = DataTableToList.GetList<LoanInformation>(selectedLoan).FirstOrDefault();
            var res = _service.UpdateLoanInfo(loan, obj);
            if (res == "1")
            {
                Response.Write("<script>alert('Loan Updated Successfully')</script>");
                Clear();
            }
        }
        private Loan GetLoanObj()
        {
            try
            {
                var obj = new Loan();
                obj.SubmissionDate = DateTime.Now;
                obj.ProductId = Convert.ToInt32(productDropDownList.SelectedValue);
                obj.PreAccNo = txtPreAcc.Text;
                obj.MidAccNo = txtMidAcc.Text;
                obj.PostAccNo = txtPostAcc.Text;
                obj.CustomerName = customerNameTextBox.Text;
                obj.AppliedAmount = Convert.ToInt32(appliedAmountTextBox.Text);
                obj.SubmitedBy = Convert.ToInt32(Session["Id"].ToString());
                obj.EntryBranch = Convert.ToInt32(sourceDropDownList.SelectedValue);
                obj.PreDeclined = txtDecliendLoan.Text;
                obj.ApplicationDate = Convert.ToDateTime(dateTextBox.Text); ;
                obj.Cep = chkCpf.Checked;
                obj.Prio = chkPriority.Checked;
                if (obj.Cep && obj.Prio) { obj.AccType = 3; }
                else if (obj.Prio) { obj.AccType = 2; }
                else if (obj.Cep) { obj.AccType = 1; }
                else { obj.AccType = 4; }
                obj.ApplicantProfession = Convert.ToInt32(professionDropdownList.SelectedValue);
                obj.Organization = companyNameTextBox.Text;
                obj.AutoDelarName = DealearsTextBox.Text;
                return obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void productDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (productDropDownList.SelectedValue == 2.ToString())
            {
                dealersDiv.Visible = true;
            }
            else
            {
                dealersDiv.Visible = false;
            }
            LoanAppInfoDiv.Visible = false;
            LoanAppViewDiv.Visible = true;
        }

        public void Clear()
        {
            productDropDownList.SelectedValue = "0";
            DealearsTextBox.Text = "";
            dateTextBox.Text = "";
            sourceDropDownList.SelectedValue = "0";
            txtPreAcc.Text = "";
            txtMidAcc.Text = "";
            txtPostAcc.Text = "";
            customerNameTextBox.Text = "";
            appliedAmountTextBox.Text = "";
            professionDropdownList.SelectedValue = "0";
            companyNameTextBox.Text = "";
            txtDecliendLoan.Text = "";
            loanAppIdTextBox.Text = "";
        }
    }
}