﻿using AzUtilities;
using AzUtilities.Common;
using BLL;
using BusinessEntities;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI.UserCreation
{
    public partial class UserActivation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User userT = (User)Session["UserDetails"];
            if (userT == null)
            {
                Response.Redirect("~/LoginUI.aspx");
            }
            if (!Page.IsPostBack)
            {
                InitInActiveUserGrid();
            }
        }

        private void InitInActiveUserGrid()
        {
            UserManager _user = new UserManager();

            try
            {
                userGridView.DataSource = null;
                userGridView.DataBind();

                List<UserTModel> users = _user.GetAllInActiveUsers();

                userGridView.DataSource = users;
                userGridView.DataBind();
                
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            UserManager _user = new UserManager();
            string sPSID = tbSearchPsId.Text;
            try
            {
                if (!string.IsNullOrEmpty(sPSID))
                {
                    userGridView.DataSource = null;
                    userGridView.DataBind();

                    List<UserTModel> users = _user.GetAllInActiveUsers(sPSID);

                    userGridView.DataSource = users;
                    userGridView.DataBind();
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        protected void userGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Activate")
                {
                    string tempId = e.CommandArgument.ToString();

                    UserManager _user = new UserManager();

                    string result = _user.ActivateUser(tempId);

                    if (result == Operation.Success.ToString())
                    {
                        var auditManager = new AT(this);
                        auditManager.AuditAndTraial("User Activation", "User Activation");

                        popupmodal.ShowSuccess("User has been ACTIVATED successfully", "User Activation");
                        InitInActiveUserGrid();
                        return;
                    }
                    popupmodal.ShowSuccess(result, "User Activation");
                    return;
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }
    }
}