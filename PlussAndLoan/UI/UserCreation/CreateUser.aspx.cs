﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AzUtilities;
using AzUtilities.Common;
using BLL;
using BLL.SecurityMatrixBLL;
using BusinessEntities;
using BusinessEntities.SecurityMatrixEntities;
using DAL;
using DAL.SecurityMatrixGateways;

namespace PlussAndLoan.UI.UserCreation
{
    public partial class UI_CreateUser : System.Web.UI.Page
    {
        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2
        }
        #endregion

        private UserManager _user;
        public string showMessage = "";

        private RoleGateway _roles = new RoleGateway();
        protected void Page_Load(object sender, EventArgs e)
        {
            User userT = (User)Session["UserDetails"];
            if (userT == null)
            {
                Response.Redirect("~/LoginUI.aspx");
            }
            if (!IsPostBack)
            {
                InitRolesDropdown();
                InitStatusDropdown();
                InitPendingUserGrid();
            }
        }
        public void InitPendingUserGrid()
        {
            UserManager _user = new UserManager();

            try
            {
                userGridView.DataSource = null;
                userGridView.DataBind();

                LogFile.WriteLine("Going to retrive pending User List - ");
                List<UserTModel> users = _user.GetAllPendingUsers();
                LogFile.WriteLine("Back from retrive pending User List - ");

                userGridView.DataSource = users;
                userGridView.DataBind();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }
        private void InitRolesDropdown()
        {
            List<RoleModel> roles = _roles.Get();

            userRoleDDL.DataSource = roles;
            userRoleDDL.DataValueField = "Id";
            userRoleDDL.DataTextField = "Name";
            userRoleDDL.DataBind();
        }
        private void InitStatusDropdown()
        {
            UserStatusManager _status = new UserStatusManager();

            List<UserStatusModel> statuses = _status.Get();

            statusDDL.DataSource = statuses;
            statusDDL.DataValueField = "Value";
            statusDDL.DataTextField = "Name";
            statusDDL.DataBind();
        }
        #region Old Bullshits

       
        
        #endregion

        protected void addButton_Click(object sender, EventArgs e)
        {
            UserTModel user = GetUserTModelObject();
            string vMsg = ValidateUserTModel(user);

            User userSession = (User)Session["UserDetails"];

            if (string.IsNullOrEmpty(vMsg))
            {
                _user = new UserManager();
                if (user.USER_STATUS == 2)
                {
                    user.DELETED_BY = userSession.UserId;
                    user.IS_DELETED = 1;
                }
                else
                {
                    user.DELETED_BY = "";
                    user.IS_DELETED = 0;
                }

                string userOperation = _user.CreateUser(user, userSession);

                if (userOperation == Operation.Success.ToString())
                {
                    showMessage = Operation.Success.ToString().ToLower();
                    ClearForm();
                    modalPopup.ShowSuccess("User has been Created successfully ! Waiting for Approval.", Title);
                    user.USER_USER_ID = userSession.Id;

                    var auditManager = new AT(this);

                    if (user.USER_ID <= 0)
                    {
                        auditManager.AuditAndTraial(Page.Title, "New User Creation", "", user.ToString());
                    }
                    else
                    {
                        UserTModel olduser = _user.GetUserByLoginIdFromMain(user.USER_CODE);

                        auditManager.AuditAndTraial(Page.Title, "Existing User UPDATE", olduser.ToString(), user.ToString());
                    }
                }
                else
                {
                    modalPopup.ShowWarning(userOperation, "Create New User");
                }
                InitPendingUserGrid();
            }
            else
            {
                modalPopup.ShowError(vMsg, "Create New User");
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }
        private UserTModel GetUserTModelObject()
        {
            try
            {
                _user = new UserManager();
                string id = UserTempIdHdn.Value != "" ? UserTempIdHdn.Value : "0";

                UserTModel user = _user.GetUserByTempId(id);

                if (user == null)
                {
                    user = new UserTModel();

                    string uid = UserIdHdn.Value != "" ? UserIdHdn.Value : "0";
                    user.Id = Convert.ToInt32(id);
                    user.USER_ID = Convert.ToInt32(uid);
                }
                user.USER_NAME = this.nameTextBox.Text.Trim().ToUpper();
                user.USER_DESIGNATION = this.designationTextBox.Text.Trim().ToUpper();
                user.USER_CODE = this.staffIdTextBox.Text.Trim().ToUpper();
                user.USER_LEVEL = Convert.ToInt32(this.userRoleDDL.SelectedValue);
                user.USER_LEVELCODE = this.codeTextBox.Text.Trim().ToUpper();
                user.USER_EMAILADDRESS = this.emailTextBox.Text.Trim().ToUpper();
                user.USER_STATUS = Convert.ToInt16(this.statusDDL.SelectedValue);

                if (user.USER_ID > 0 && passwordTextBox.Text.Trim().Length <= 0)
                {
                    UserTModel mainModel = _user.GetUserByLoginIdFromMain(user.USER_CODE);

                    user.USER_PASSWORD = mainModel.USER_PASSWORD;
                }
                else
                {
                    string pass = passwordTextBox.Text.Trim().Length <= 0 ? "123456" : passwordTextBox.Text.Trim();

                    user.USER_PASSWORD = ConvertionUtilities.CreateMD5(pass);
                }

                RoleModel role = _roles.Get(user.USER_LEVEL);

                user.ROLE_NAME = role.Name;

                UserStatusManager _status = new UserStatusManager();

                user.USER_STATUS_NAME = _status.Get(user.USER_STATUS).Name;

                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private string ValidateUserTModel(UserTModel user)
        {
            string message = String.Empty;

            if (user == null)
                return "User Object Creation Problem";

            _user = new UserManager();

            if (string.IsNullOrEmpty(SearchedOrSelectedHdn.Value))
            {
                UserTModel userT = _user.GetUserByLoginId(user.USER_CODE);

                if (userT != null)
                {
                    message += "Found Duplicate ID Please search or Update User";
                }
            }
            if (string.IsNullOrEmpty(user.USER_NAME))
            {
                nameTextBox.CssClass += " invalid";
                message += "User Name Cannot Be Empty";
            }
            if (string.IsNullOrEmpty(user.USER_DESIGNATION))
            {
                designationTextBox.CssClass += " invalid";
                message += "Designation cannot be Empty.";

            }
            if (user.USER_LEVELCODE.Length > 3)
            {
                staffIdTextBox.CssClass += " invalid";
                message += "User Code Must be less than 3 Character.";
            }
            if (string.IsNullOrEmpty(user.USER_EMAILADDRESS))
            {
                emailTextBox.CssClass += " invalid";
                message += "User Email Cannot Be Empty.";
            }
            return message;
        }

        private void ClearForm()
        {
            searchBtn.Enabled = true;
            staffIdTextBox.Enabled = true;

            addButton.Visible = true;
            updateButton.Visible = false;
            SearchedOrSelectedHdn.Value = "";
            UserIdHdn.Value = "";
            UserTempIdHdn.Value = "";
            staffIdTextBox.Text = "";
            nameTextBox.Text = "";
            designationTextBox.Text = "";
            userRoleDDL.SelectedIndex = 0;
            emailTextBox.Text = "";
            codeTextBox.Text = "";
            statusDDL.SelectedIndex = 0;
        }

        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        protected void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                UserManager _user = new UserManager();

                var loginId = staffIdTextBox.Text;

                UserTModel user = _user.GetUserByLoginId(loginId);

                if (user != null)
                {
                    SearchedOrSelectedHdn.Value = "dsadfjsh";
                    searchBtn.Enabled = false;
                    staffIdTextBox.Enabled = false;

                    addButton.Visible = false;
                    updateButton.Visible = true;

                    UserIdHdn.Value = user.USER_ID.ToString().Trim();
                    UserTempIdHdn.Value = user.Id.ToString().Trim();

                    staffIdTextBox.Text = user.USER_CODE.Trim();
                    nameTextBox.Text = user.USER_NAME.Trim();
                    designationTextBox.Text = user.USER_DESIGNATION.Trim();
                    userRoleDDL.SelectedValue = user.USER_LEVEL.ToString().Trim();
                    emailTextBox.Text = user.USER_EMAILADDRESS.Trim();
                    codeTextBox.Text = user.USER_LEVELCODE.Trim();
                    statusDDL.SelectedValue = user.USER_STATUS.ToString().Trim();
                }
                else
                {
                    searchBtn.Enabled = true;
                    staffIdTextBox.Enabled = true;

                    addButton.Visible = true;
                    updateButton.Visible = false;
                    ClearForm();
                    modalPopup.ShowSuccess("No user found", "User Creation");
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        protected void userGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Select")
                {
                    string tempId = e.CommandArgument.ToString();
                    UserManager _user = new UserManager();
                    UserTModel user = _user.GetUserByTempId(tempId);
                    SearchedOrSelectedHdn.Value = "dsadfjsh";
                    LogFile.WriteLine("CreateUserASPX::970 ---tempId:" + tempId);
                    //UserTModel user = _user.GetUserByTempId(loginId);

                    if (user == null)
                    {
                        LogFile.WriteLine("CreateUserASPX::976 --- User Object:null");
                        return;
                    }

                    addButton.Visible = false;
                    updateButton.Visible = true;

                    UserIdHdn.Value = user.USER_ID.ToString();
                    UserTempIdHdn.Value = user.Id.ToString();
                    staffIdTextBox.Enabled = false;
                    staffIdTextBox.Text = user.USER_CODE;
                    nameTextBox.Text = user.USER_NAME;
                    designationTextBox.Text = user.USER_DESIGNATION;
                    userRoleDDL.SelectedValue = user.USER_LEVEL.ToString();
                    emailTextBox.Text = user.USER_EMAILADDRESS;
                    codeTextBox.Text = user.USER_LEVELCODE;
                    statusDDL.SelectedValue = user.USER_STATUS.ToString();
                }
                if (e.CommandName == "Del")
                {
                    string tempId = e.CommandArgument.ToString();
                    UserManager _user = new UserManager();
                    _user.UserDeleteFromTemp(tempId);
                    modalPopup.ShowSuccess("User has been deleted successfully .", Title);
                    InitPendingUserGrid();
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        protected void updateButton_Click(object sender, EventArgs e)
        {
            UserTModel user = GetUserTModelObject();
            string vMsg = ValidateUserTModel(user);

            if (string.IsNullOrEmpty(vMsg))
            {
                _user = new UserManager();
                User userSession = (User)Session["UserDetails"];

                if (user.USER_STATUS == 2)
                {
                    user.DELETED_BY = userSession.UserId;
                    user.IS_DELETED = 1;
                }
                else
                {
                    user.DELETED_BY = "";
                    user.IS_DELETED = 0;
                }

                string value = _user.CreateUser(user, userSession);

                if (value == Operation.Success.ToString())
                {
                    showMessage = Operation.Success.ToString().ToLower();
                    ClearForm();

                    var auditManager = new AT(this);

                    if (user.USER_ID <= 0)
                    {
                        auditManager.AuditAndTraial("Create", "New User Creation", "", user.ToString());
                    }
                    else
                    {
                        UserTModel olduser = _user.GetUserByLoginIdFromMain(user.USER_CODE);

                        auditManager.AuditAndTraial("Modify", "Existing User UPDATE", olduser.ToString(), user.ToString());
                    }

                    modalPopup.ShowSuccess("User Update is successful", Title);
                }
                else
                {
                    modalPopup.ShowError(value, Title);
                }
                InitPendingUserGrid();
            }
            else
            {
                modalPopup.ShowWarning(vMsg, "User Creation");
            }
        }

        protected void deleteButton_Click(object sender, EventArgs e)
        {

        }

    }
}
