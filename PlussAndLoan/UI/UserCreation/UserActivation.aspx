﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" CodeBehind="UserActivation.aspx.cs" Inherits="PlussAndLoan.UI.UserCreation.UserActivation" %>

<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="tomato" TagName="modalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <tomato:modalPopup runat="server" ID="popupmodal" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">User Activation</div>

        <form name="myform" method="post">
            <label for="field1">
                <span>User ID (PSID) </span>
                <asp:TextBox ID="tbSearchPsId" runat="server" CssClass="input-field" Width="200px" />&nbsp;&nbsp;
                <asp:Button ID="btnSearch" CssClass="btn primarybtn" runat="server" Text="Search" OnClick="btnSearch_Click" />
            </label>
        </form>

    </div>
    <div style="height: 500px; padding-left: 20px; padding-right: 20px; overflow: auto;">
        <asp:GridView ID="userGridView" runat="server" GridLines="None" CssClass="myGridStyle"
            Font-Size="11px" AutoGenerateColumns="False" OnRowCommand="userGridView_RowCommand"
            DataKeyNames="Id">
            <Columns>
                <%--<asp:BoundField DataField="Id" HeaderText="Id" Visible="false"/>--%>
                <%--<asp:BoundField DataField="USER_ID" HeaderText="User Id" Visible="false"/>--%>
                <asp:BoundField DataField="USER_CODE" HeaderText="PS ID" />
                <asp:BoundField DataField="USER_NAME" HeaderText="Name" />
                <asp:BoundField DataField="USER_DESIGNATION" HeaderText="Designation" />
                <asp:BoundField DataField="USER_EMAILADDRESS" HeaderText="Email" />
                <asp:BoundField DataField="USER_LEVELCODE" HeaderText="ARM Code" />
                <asp:BoundField DataField="ROLE_NAME" HeaderText="Role" />
                <asp:BoundField DataField="USER_STATUS_NAME" HeaderText="Current Status" />
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button CssClass="btn primarybtn" Text="Activate" runat="server" CommandName="Activate" CommandArgument='<%# Eval("USER_ID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No Record Found
            </EmptyDataTemplate>
        </asp:GridView>
        <br /><br />
    </div>
</asp:Content>
