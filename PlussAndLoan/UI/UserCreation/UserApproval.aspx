﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="UserApproval.aspx.cs" Inherits="PlussAndLoan.UI.UserCreation.UserApproval" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="tomato" TagName="modalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <tomato:modalPopup runat="server" ID="popupmodal" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">User Approval</div>
        <form name="myform" method="post">
            <table width="85%" border="0" cellspacing="4" cellpadding="1">
                <tr>
                    <td style="font-weight: bold; width: 200px; text-align:right"><span>Staff ID</span></td>
                    <td class="auto-style2"><asp:TextBox ID="staffIdTextBox" runat="server" CssClass="input-field" Enabled="false" /></td>
                    <td style="font-weight: bold; width: 200px; text-align:right"><span>Name</span></td>
                    <td class="auto-style2"><asp:TextBox ID="nameTextBox" runat="server" CssClass="input-field" Enabled="false" /></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;  text-align:right"><span>Designation </span></td>
                    <td class="auto-style2"><asp:TextBox ID="designationTextBox" runat="server" CssClass="input-field" Enabled="false" /></td>
                    <td style="font-weight: bold;  text-align:right"><span>Role</span></td>
                    <td class="auto-style2"><asp:DropDownList ID="userRoleDDL" AutoPostBack="false" runat="server" CssClass="select-field" Enabled="false"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;  text-align:right"><span>ARM Code</span></td>
                    <td class="auto-style2"><asp:TextBox ID="codeTextBox" runat="server" CssClass="input-field" Enabled="false" MaxLength="3" /></td>
                    <td style="font-weight: bold;  text-align:right"><span>Email</span></td>
                    <td class="auto-style2"><asp:TextBox ID="emailTextBox" runat="server" CssClass="input-field" Enabled="false" /></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; text-align:right"><span>Status</span></td>
                    <td class="auto-style2"><asp:DropDownList ID="statusDDL" AutoPostBack="false" runat="server" CssClass="select-field" Enabled="false"></asp:DropDownList></td>
                    <td style="font-weight: bold;  text-align:right"><span>Comment</span></td>
                    <td class="auto-style2"><asp:TextBox ID="approverCommentTextBox" runat="server" CssClass="textarea-field" Rows="3" TextMode="MultiLine" /></td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <br />
                        <asp:HiddenField ID="UserIdHdn" runat="server" Value="" />
                        <asp:HiddenField ID="UserTempIdHdn" runat="server" Value="" />
                        <asp:Button ID="approveButton" runat="server" Text="Approve" CssClass="btn primarybtn" OnClick="approveButton_Click" />
                        <asp:Button ID="clearButton" runat="server" Text="Clear" CssClass="btn clearbtn" OnClick="clearButton_Click" />
                        <asp:Button ID="DeleteButton" runat="server" Text="Reject" CssClass="btn clearbtn" OnClick="DeleteButton_Click" />
                    </td>
                </tr>
            </table>

        </form>
    </div>

    <br /><br />
    <div style="height: 20%; width: 95%; padding-left: 20px; overflow: auto;">
        <asp:GridView ID="userGridView" runat="server" GridLines="None" CssClass="myGridStyle"
            Font-Size="11px" AutoGenerateColumns="False" OnRowCommand="userGridView_RowCommand"
            DataKeyNames="Id">
            <Columns>
                <%--<asp:BoundField DataField="Id" HeaderText="Id" Visible="false"/>--%>
                <%--<asp:BoundField DataField="USER_ID" HeaderText="User Id" Visible="false"/>--%>
                <asp:BoundField DataField="USER_CODE" HeaderText="PS ID" />
                <asp:BoundField DataField="USER_NAME" HeaderText="Name" />
                <asp:BoundField DataField="USER_DESIGNATION" HeaderText="Designation" />
                <asp:BoundField DataField="USER_EMAILADDRESS" HeaderText="Email" />
                <asp:BoundField DataField="USER_LEVELCODE" HeaderText="ARM Code" />
                <asp:BoundField DataField="USER_LEVEL_NAME" HeaderText="Role" />
                <asp:BoundField DataField="USER_STATUS_NAME" HeaderText="Status" />
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button Text="Select" runat="server" CommandName="Select" CommandArgument='<%# Eval("Id") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No Record Found
            </EmptyDataTemplate>
        </asp:GridView>
        <br /><br /><br />
    </div>
</asp:Content>
