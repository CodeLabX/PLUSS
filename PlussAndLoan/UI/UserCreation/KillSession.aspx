﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" EnableEventValidation="false" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UserCreation.UI_KillSession" Title="Kill Session" CodeBehind="KillSession.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Session Kill of Active User</div>
        <form name="myform" method="post">
            <label for="field1">
                <span>User ID (PSID) </span>
                <asp:TextBox ID="tbSearchPsId" runat="server" CssClass="input-field" Width="200px" MaxLength="20" />&nbsp;&nbsp;
                <asp:Button ID="btnSearch" CssClass="btn primarybtn" runat="server" Text="Search" OnClick="btnSearch_Click" />
            </label>
        </form>
    </div>
    <div style="width: 95%; height: 400px; padding-left: 20px; overflow: auto;">
        <asp:GridView ID="gvData" OnRowCommand="gvData_RowCommand" OnRowDataBound="gvData_RowDataBound" OnPageIndexChanging="gvData_PageIndexChanging"
            runat="server" GridLines="None" CssClass="myGridStyle"
            Font-Size="11px" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="IsLogged" HeaderText="Is Logged" />
                <asp:BoundField DataField="UserId" HeaderText="User ID" />
                <asp:BoundField DataField="UserName" HeaderText="User Name" />
                <asp:BoundField DataField="RoleName" HeaderText="Role Name" />
                <asp:TemplateField HeaderText="Last Login Date & Time">
                    <ItemTemplate>
                        <%# Convert.ToDateTime(Eval("LoggedInTime")).ToString("dd-MMM-yyyy: hh:mm:ss tt") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserSessionID" HeaderText="User Session" />
                <asp:BoundField DataField="HostName" HeaderText="Host Name / IP Address" />
                <asp:TemplateField HeaderText="Select" HeaderStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Button CssClass="btn primarybtn" Height="25px" OnClientClick="CloseErrorPanel()" CommandName="KillUser"
                            CommandArgument='<%# Eval("UserId") %>'
                            runat="server" ID="btnSelect" Text="Kill Session" />
                    </ItemTemplate>

                    <HeaderStyle Width="5%"></HeaderStyle>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No record found
            </EmptyDataTemplate>
            <AlternatingRowStyle CssClass="odd" />
        </asp:GridView>
    </div>
</asp:Content>

