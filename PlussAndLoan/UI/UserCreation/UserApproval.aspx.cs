﻿using AzUtilities;
using AzUtilities.Common;
using BLL;
using BLL.SecurityMatrixBLL;
using BusinessEntities;
using BusinessEntities.SecurityMatrixEntities;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI.UserCreation
{
    public partial class UserApproval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User userT = (User)Session["UserDetails"];
            if (userT == null)
            {
                Response.Redirect("~/LoginUI.aspx");
            }
            if (!Page.IsPostBack)
            {
                InitPendingUserGrid();
                InitRolesDropdown();
                InitStatusDropdown();
            }
        }


        public void InitPendingUserGrid()
        {
            UserManager _user = new UserManager();

            try
            {
                userGridView.DataSource = null;
                userGridView.DataBind();

                List<UserTModel> users = _user.GetAllPendingUsers();

                userGridView.DataSource = users;
                userGridView.DataBind();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        private void InitRolesDropdown()
        {
            RoleManager _roles = new RoleManager();

            List<RoleModel> roles = _roles.Get();

            userRoleDDL.DataSource = roles;
            userRoleDDL.DataValueField = "Id";
            userRoleDDL.DataTextField = "Name";
            userRoleDDL.DataBind();
        }
        private void InitStatusDropdown()
        {
            UserStatusManager _status = new UserStatusManager();

            List<UserStatusModel> statuses = _status.Get();

            statusDDL.DataSource = statuses;
            statusDDL.DataValueField = "Value";
            statusDDL.DataTextField = "Name";
            statusDDL.DataBind();
        }

        protected void userGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Select")
                {
                    string tempId = e.CommandArgument.ToString();

                    UserManager _user = new UserManager();

                    UserTModel user = _user.GetUserByTempId(tempId);

                    if (user == null)
                    {
                        popupmodal.ShowSuccess("User is already rejected", "User Approval");
                        return;
                    }

                    UserTempIdHdn.Value = user.Id.ToString();

                    staffIdTextBox.Text = user.USER_CODE;
                    nameTextBox.Text = user.USER_NAME;
                    designationTextBox.Text = user.USER_DESIGNATION;
                    userRoleDDL.SelectedValue = user.USER_LEVEL.ToString();
                    emailTextBox.Text = user.USER_EMAILADDRESS;
                    codeTextBox.Text = user.USER_LEVELCODE;
                    statusDDL.SelectedValue = user.USER_STATUS.ToString();
                    approverCommentTextBox.Text = user.APPROVER_COMMENT != null ? user.APPROVER_COMMENT.ToString() : "";
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        protected void approveButton_Click(object sender, EventArgs e)
        {
            try
            {
                UserManager _user = new UserManager();

                string tempId = UserTempIdHdn.Value;

                if (string.IsNullOrEmpty(tempId))
                {
                    popupmodal.ShowSuccess("Please select a user from the grid", "User Approval");
                    return;
                }

                UserTModel user = _user.GetUserByTempId(tempId);
                if (user != null)
                {
                    user.APPROVER_COMMENT = approverCommentTextBox.Text.ToUpper();
                }
                else
                {
                    LogFile.WriteLine("UserApproval:approveButton_Click --- User object Empty");
                    popupmodal.ShowSuccess("There was a problem in approving user. User not found.", "User Approval");
                    return;
                }
                User userSession = (User)Session["UserDetails"];

                string str = _user.ApproveUser(user, userSession);

                if (str == Operation.Success.ToString())
                {
                    ClearForm();
                    staffIdTextBox.Enabled = true;
                    approverCommentTextBox.Text = "";


                    var auditManager = new AT(this);

                    if (user.USER_ID <= 0)
                    {
                        auditManager.AuditAndTraial(Page.Title, "NEW USER APPROVE", "", user.ToString());
                    }
                    else
                    {
                        UserTModel olduser = _user.GetUserByLoginIdFromMain(user.USER_CODE);

                        auditManager.AuditAndTraial(Page.Title, "EXISTING USER  APPROVE", olduser.ToString(), user.ToString());
                    }

                    popupmodal.ShowSuccess("User Approval is successful", "User Approval");
                }
                else
                {
                    popupmodal.ShowSuccess(str, "User Approval");
                }
                InitPendingUserGrid();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        private void ClearForm()
        {
            //searchBtn.Enabled = true;
            staffIdTextBox.Enabled = true;

            UserIdHdn.Value = "";
            staffIdTextBox.Text = "";
            nameTextBox.Text = "";
            designationTextBox.Text = "";
            userRoleDDL.SelectedIndex = 0;
            emailTextBox.Text = "";
            codeTextBox.Text = "";
            statusDDL.SelectedIndex = 0;
            approverCommentTextBox.Text = "";
        }

        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            User userT = (User)Session["UserDetails"];

            UserManager _user = new UserManager();

            var id = UserTempIdHdn.Value;

            if (string.IsNullOrEmpty(id))
            {
                popupmodal.ShowSuccess("There was a problem in rejecting user. User not found.", "User Approval");
                return;
            }

            UserTModel user = _user.GetUserByTempId(id);
            if (user != null)
            {
                user.APPROVER_COMMENT = approverCommentTextBox.Text.ToUpper();
            }

            if (user == null)
            {
                popupmodal.ShowSuccess("There was a problem in rejecting user. User not found.", "User Approval");
                return;
            }

            string str = _user.RejectUserCreation(user, userT);

            if (str == Operation.Success.ToString())
            {
                popupmodal.ShowSuccess("Operation was successful.", "User Approval");
                ClearForm();
                InitPendingUserGrid();
            }
            else
            {
                popupmodal.ShowSuccess(str, "User Approval");
            }
        }

        protected void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                UserManager _user = new UserManager();

                var loginId = staffIdTextBox.Text;

                UserTModel user = _user.GetUserByLoginIdFromMain(loginId);

                if (user != null)
                {
                    //searchBtn.Enabled = false;
                    staffIdTextBox.Enabled = false;

                    UserIdHdn.Value = user.USER_ID.ToString();

                    staffIdTextBox.Text = user.USER_CODE;
                    nameTextBox.Text = user.USER_NAME;
                    designationTextBox.Text = user.USER_DESIGNATION;
                    userRoleDDL.SelectedValue = user.USER_LEVEL.ToString();
                    emailTextBox.Text = user.USER_EMAILADDRESS;
                    codeTextBox.Text = user.USER_LEVELCODE;
                    statusDDL.SelectedValue = user.USER_STATUS.ToString();
                }
                else
                {
                    //searchBtn.Enabled = true;
                    staffIdTextBox.Enabled = true;
                    popupmodal.ShowSuccess("User Not Found.", "User Approval");
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }
    }
}