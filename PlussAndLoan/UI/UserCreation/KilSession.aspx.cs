﻿using AzUtilities;
using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;


namespace PlussAndLoan.UI.UserCreation
{
    public partial class KilSession : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadGrid();
            }
        }

        private void LoadGrid()
        {
            try
            {
                var loginId = (string)Session["UserId"];

                List<LogedUser> list = new List<LogedUser>();
                foreach (var item in LoginUser.LoginUserToSystem)
                {
                    if (item.UserId != loginId && item.UserId != "1499074")
                    {
                        list.Add(item);
                    }
                }

                DataTable dtTmpList = ConvertToDatatable(list);
                gvData.DataSource = dtTmpList;
                gvData.DataBind();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                LogFile.WriteLine("UI_KillSession - :LoadGrid ");
            }

        }

        static DataTable ConvertToDatatable(List<LogedUser> list)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("IsLogged");
            dt.Columns.Add("UserId");
            dt.Columns.Add("UserName");
            dt.Columns.Add("RoleName");
            dt.Columns.Add("IPAddress");
            dt.Columns.Add("HostName");
            dt.Columns.Add("LoggedInTime");
            dt.Columns.Add("UserSessionID");
            foreach (var item in list)
            {
                if (Convert.ToString(item.UserId) != "1499074")
                {
                    var row = dt.NewRow();

                    row["IsLogged"] = item.IsLogged;
                    row["UserId"] = Convert.ToString(item.UserId);
                    row["UserName"] = Convert.ToString(item.UserName);
                    row["RoleName"] = Convert.ToString(item.RoleName);
                    row["IPAddress"] = Convert.ToString(item.IPAddress);
                    row["HostName"] = item.HostName;
                    row["LoggedInTime"] = item.LoggedInTime;
                    row["UserSessionID"] = item.UserSessionID;
                    dt.Rows.Add(row);
                }
            }

            return dt;
        }
        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            List<LogedUser> list = LoginUser.LoginUserToSystem;
            string psId = tbSearchPsId.Text;

            if (!string.IsNullOrEmpty(psId))
            {
                list = null;
                list = LoginUser.LoginUserToSystem.FindAll(u => u.UserId.Contains(psId));
            }

            gvData.PageIndex = e.NewPageIndex;
            gvData.DataSource = list;// dtTmpList;
            gvData.DataBind();
        }

        protected void gvData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (!e.CommandName.Equals("Page"))
            {
                GridViewRow gvRow = (GridViewRow)((Button)e.CommandSource).NamingContainer;
                var userId = e.CommandArgument.ToString();
                LoginUser.LoginUserToSystem.RemoveAll(u => u.UserId == userId);
                LoadGrid();
            }
        }
        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //  Util.GridDateFormat(e, gvData, null);
        }

        protected void tbSearchPsId_TextChanged(object sender, EventArgs e)
        {
            List<LogedUser> list = LoginUser.LoginUserToSystem;
            string psId = tbSearchPsId.Text;
            var loginId = (string)Session["UserId"];


            if (!string.IsNullOrEmpty(psId))
            {
                list = null;
                list = LoginUser.LoginUserToSystem.FindAll(u => u.UserId.Contains(psId) && u.UserId != loginId);
            }

            gvData.DataSource = list;// dtTmpList;
            gvData.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            List<LogedUser> list = LoginUser.LoginUserToSystem;
            string psId = tbSearchPsId.Text.TrimEnd().TrimStart();

            if (!string.IsNullOrEmpty(psId))
            {
                list = null;
                list = LoginUser.LoginUserToSystem.FindAll(u => u.UserId.Contains(psId));
            }
            gvData.DataSource = ConvertToDatatable(list);// dtTmpList;
            gvData.DataBind();
        }
    }
}