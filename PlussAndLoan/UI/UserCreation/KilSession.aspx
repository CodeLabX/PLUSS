﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KilSession.aspx.cs" Inherits="PlussAndLoan.UI.UserCreation.KilSession" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .TRStyle {
            background-image: url(../images/pageFrameWork_Top.gif);
        }
    </style>
    <style type="text/css">
        /*.ctl00_MenuSpace_2 a, .ChildMenu, #<= MenuSpace.ClientID %> {
            color: white;
            font-size: 15px !important;
            font-weight: 600;
        }*/
        .ParentMenu, .ParentMenu:hover {
            width: 150px;
            background-color: #3dae38;
            text-align: center;
            height: 30px;
            line-height: 30px;
            /*margin-right: 5px;*/
            padding: 5px;
        }

            .ParentMenu:hover {
                background-color: #2b7b27;
            }

        .ChildMenu, .ChildMenu:hover {
            width: 150px;
            background-color: #3dae38;
            text-align: center;
            height: 30px;
            line-height: 30px;
        }

            .ChildMenu:hover {
                background-color: #2b7b27;
            }

        .selected, .selected:hover {
            background-color: #2b7b27 !important;
            color: #fff;
        }

        .level2 {
            background-color: #3dae38;
        }
        /*
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            color: white;
            text-align: center;
        }*/

        #userCreationMessage {
            opacity: 1;
            transition: opacity 500ms;
        }

        #userCreationMessage {
            opacity: 0;
        }
    </style>
    <style type="text/css">
        body {
            font: 12px verdana;
        }

        .myGridStyle {
            border-collapse: collapse;
            width: 100%;
        }

            .myGridStyle tr th {
                padding: 8px;
                border: 1px solid black;
            }

            .myGridStyle tr:nth-child(even) {
                background-color: #E1FFEF;
            }

            .myGridStyle tr:nth-child(odd) {
                background-color: #bce3ff;
            }

            .myGridStyle td {
                border: 1px solid black;
                padding: 8px;
            }

            .myGridStyle tr:last-child td {
            }

            .myGridStyle.no-border {
                border: none;
            }

                .myGridStyle.no-border td, .myGridStyle.no-border th {
                    border: 0px;
                    padding: 3px;
                }

        .form-style-2 {
            max-width: 500px;
            padding: 20px 12px 10px 20px;
            font: 13px Arial, Helvetica, sans-serif;
        }

        .form-style-2-heading {
            font-weight: bold;
            font-style: italic;
            border-bottom: 2px solid #ddd;
            margin-bottom: 20px;
            font-size: 15px;
            padding-bottom: 3px;
        }

        .form-style-2 label {
            display: block;
            margin: 0px 0px 15px 0px;
        }

            .form-style-2 label > span {
                width: 100px;
                font-weight: bold;
                float: left;
                padding-top: 8px;
                padding-right: 5px;
            }

        .form-style-2 span.required {
            color: red;
        }

        .form-style-2 .tel-number-field {
            width: 40px;
            text-align: center;
        }

        .form-style-2 input.input-field {
            width: 240px;
        }

        .form-style-2 input.input-field,
        .form-style-2 .tel-number-field,
        .form-style-2 .textarea-field,
        .form-style-2 .select-field {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            border: 1px solid #C2C2C2;
            box-shadow: 1px 1px 4px #EBEBEB;
            -moz-box-shadow: 1px 1px 4px #EBEBEB;
            -webkit-box-shadow: 1px 1px 4px #EBEBEB;
            border-radius: 3px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            padding: 7px;
            outline: none;
        }

            .form-style-2 input.input-field.invalid,
            .form-style-2 .tel-number-field.invalid,
            .form-style-2 .textarea-field.invalid,
            .form-style-2 .select-field.invalid {
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                border: 1px solid red;
                box-shadow: 1px 1px 4px red;
                -moz-box-shadow: 1px 1px 4px red;
                -webkit-box-shadow: 1px 1px 4px red;
                border-radius: 3px;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                padding: 7px;
                outline: none;
            }

        .form-style-2 .select-field, .form-style-2 .textarea-field {
            width: 240px;
            text-transform: uppercase;
        }

            .form-style-2 .input-field:focus,
            .form-style-2 .tel-number-field:focus,
            .form-style-2 .textarea-field:focus,
            .form-style-2 .select-field:focus {
                border: 1px solid #0C0;
            }

        .form-style-2 .btn {
            border: none;
            padding: 8px 15px 8px 15px;
            color: #fff;
            box-shadow: 1px 1px 4px #DADADA;
            -moz-box-shadow: 1px 1px 4px #DADADA;
            -webkit-box-shadow: 1px 1px 4px #DADADA;
            border-radius: 3px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
        }

        .form-style-2 .btn {
            border: none;
            padding: 8px 15px 8px 15px;
            color: #fff;
            box-shadow: 1px 1px 4px #DADADA;
            -moz-box-shadow: 1px 1px 4px #DADADA;
            -webkit-box-shadow: 1px 1px 4px #DADADA;
            border-radius: 3px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
        }

        .is-uppercase {
            text-transform: uppercase;
        }

        .clearbtn:not([disabled]) {
            background: #FF8500;
        }

        .primarybtn:not([disabled]) {
            background: #00c157;
        }

        .form-style-2 input[type=submit]:not([disabled]):hover,
        .form-style-2 input[type=button]:not([disabled]):hover {
            background: #EA7B00;
            color: #fff;
        }

        .container-fluid {
            max-width: 100%;
        }

        fieldset {
            position: relative;
            border: 1px solid #ddd;
            padding: 10px;
            margin-top: 10px;
        }

            fieldset legend {
                position: absolute;
                top: 0;
                font-size: 18px;
                line-height: 1;
                margin: -9px 0 0; /* half of font-size */
                background: #fff;
                padding: 0 3px;
            }

        .Background {
            background-color: Black;
            /*filter: alpha(opacity=90);*/
            opacity: 0.8;
        }

        .Popup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 400px;
            height: 350px;
        }

        .lbl {
            font-size: 16px;
            font-style: italic;
            font-weight: bold;
        }

        .collapsible-btn {
            background-color: #777;
            color: white;
            cursor: pointer;
            padding: 18px;
            width: 95vw;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
        }

            .collapsible-active, .collapsible-btn:hover {
                background-color: #555;
            }

        .collapsible-content {
            padding: 0 18px;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
            background-color: #f1f1f1;
        }

        .is-margin-5 {
            margin: 5px;
        }

        .is-margin-10 {
            margin: 10px;
        }

        .is-margin-20 {
            margin: 20px;
        }

        .has-width-100px {
            width: 100px !important;
        }

        .has-width-150px {
            width: 150px !important;
        }

        .has-width-190px {
            width: 190px !important;
        }

        .has-width-200px {
            width: 200px !important;
        }

        footer {
            color: white;
            background: #005d9a;
            height: 40px;
            text-align: center;
            font-size: 13px;
            font-weight: 500;
            width: 97vw;
            padding: 10px;
            margin-top: auto;
        }

        .parent {
            display: block;
            position: relative;
            float: left;
            line-height: 30px;
            background-color: #4FA0D8;
            border-right: #CCC 1px solid;
        }

            .parent a {
                margin: 10px;
                color: #FFFFFF;
                text-decoration: none;
            }

            .parent:hover > ul {
                display: block;
                position: absolute;
            }

        .child {
            display: none;
            z-index: 10;
        }

            .child li {
                background-color: #E4EFF7;
                line-height: 30px;
                border-bottom: #CCC 1px solid;
                border-right: #CCC 1px solid;
                width: 100%;
            }

                .child li a {
                    color: #000000;
                }

        ul {
            list-style: none;
            margin: 0;
            padding: 0px;
            min-width: 10em;
        }

            ul ul ul {
                left: 100%;
                top: 0;
                margin-left: 1px;
            }

        li:hover {
            background-color: #95B4CA;
        }

        .parent li:hover {
            background-color: #F0F0F0;
        }

        .expand {
            font-size: 12px;
            float: right;
            margin-right: 5px;
        }

        #navHolder {
            display: flex;
        }

        .swal-icon {
            float: left;
            margin: 32px;
        }

        .swal-title {
            margin-bottom: 13px;
            margin-top: 32px;
        }

        .swal-modal {
            width: 500px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Session Kill of Active User</div>
        
            <label for="field1">
                <span>User ID (PSID) </span>
                <asp:TextBox ID="tbSearchPsId" runat="server" CssClass="input-field" Width="200px" MaxLength="20" />&nbsp;&nbsp;
                <asp:Button ID="btnSearch" CssClass="btn primarybtn" runat="server" Text="Search" OnClick="btnSearch_Click" />
            </label>
    </div>
    <div style="width: 95%; height: 400px; padding-left: 20px; overflow: auto;">
        <asp:GridView ID="gvData" OnRowCommand="gvData_RowCommand" OnRowDataBound="gvData_RowDataBound" OnPageIndexChanging="gvData_PageIndexChanging"
            runat="server" GridLines="None" CssClass="myGridStyle"
            Font-Size="11px" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="IsLogged" HeaderText="Is Logged" />
                <asp:BoundField DataField="UserId" HeaderText="User ID" />
                <asp:BoundField DataField="UserName" HeaderText="User Name" />
                <asp:BoundField DataField="RoleName" HeaderText="Role Name" />
                <asp:TemplateField HeaderText="Last Login Date & Time">
                    <ItemTemplate>
                        <%# Convert.ToDateTime(Eval("LoggedInTime")).ToString("dd-MMM-yyyy: hh:mm:ss tt") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserSessionID" HeaderText="User Session" />
                <asp:BoundField DataField="HostName" HeaderText="Host Name / IP Address" />
                <asp:TemplateField HeaderText="Select" HeaderStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Button CssClass="btn primarybtn" Height="25px" OnClientClick="CloseErrorPanel()" CommandName="KillUser"
                            CommandArgument='<%# Eval("UserId") %>'
                            runat="server" ID="btnSelect" Text="Kill Session" />
                    </ItemTemplate>

                    <HeaderStyle Width="5%"></HeaderStyle>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No record found
            </EmptyDataTemplate>
            <AlternatingRowStyle CssClass="odd" />
        </asp:GridView>
    </div>
    </form>
</body>
</html>
