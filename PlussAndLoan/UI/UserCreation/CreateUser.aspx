﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" EnableEventValidation="false" Inherits="PlussAndLoan.UI.UserCreation.UI_CreateUser" Title="User Setup" CodeBehind="CreateUser.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <modal:modalPopup runat="server" ID="modalPopup" />
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">Manage User</div>
        <form name="myform" method="post">

            <table width="85%" border="0" cellspacing="4" cellpadding="1">
                <tr>
                    <td style="font-weight: bold; width: 200px; text-align:right"><span>Staff ID *</span></td>
                    <td class="auto-style2">
                        <asp:TextBox ID="staffIdTextBox" runat="server" CssClass="input-field is-uppercase" MaxLength="20" />
                        <asp:Button ID="searchBtn" runat="server" CssClass="btn primarybtn" Text="Search" OnClick="searchBtn_Click" />
                    </td>
                    <td style="font-weight: bold; text-align:right"><span>Role *</span></td>
                    <td class="auto-style2"><asp:DropDownList ID="userRoleDDL" AutoPostBack="false" runat="server" CssClass="select-field"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; text-align:right"><span>Name *</span></td>
                    <td class="auto-style2"><asp:TextBox ID="nameTextBox" runat="server" CssClass="input-field is-uppercase" /></td>
                    <td style="font-weight: bold; text-align:right"><span>Email *</span></td>
                    <td class="auto-style2"><asp:TextBox ID="emailTextBox" runat="server" CssClass="input-field is-uppercase" TextMode="Email" /></td>
                </tr>

                <tr>
                    <td style="font-weight: bold; text-align:right"><span>Designation *</span></td>
                    <td class="auto-style2"><asp:TextBox ID="designationTextBox" runat="server" CssClass="input-field is-uppercase" /></td>
                    <td style="font-weight: bold; text-align:right"><span>Status *</span></td>
                    <td class="auto-style2"><asp:DropDownList ID="statusDDL" AutoPostBack="false" runat="server" CssClass="select-field"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; text-align:right"><span>ARM Code</span></td>
                    <td class="auto-style2"><asp:TextBox ID="codeTextBox" runat="server" CssClass="input-field is-uppercase" MaxLength="3" /></td>
                    <td style="font-weight: bold; text-align:right; visibility:hidden"><span>Password</span></td>
                    <td class="auto-style2" style="visibility:hidden"><asp:TextBox ID="passwordTextBox" runat="server" CssClass="input-field is-uppercase" TextMode="Password" /></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <br />
                        <div style="text-align: center">
                            <asp:HiddenField ID="UserIdHdn" runat="server" Value="" />
                            <asp:HiddenField ID="UserTempIdHdn" runat="server" Value="" />
                            <asp:HiddenField ID="SearchedOrSelectedHdn" runat="server" Value="" />
                            <asp:Button ID="addButton" runat="server" Text="Submit" CssClass="btn primarybtn" OnClick="addButton_Click" />
                            <asp:Button ID="updateButton" runat="server" Text="Update" CssClass="btn primarybtn" OnClick="updateButton_Click" Visible="false" />
                            <asp:Button ID="Button1" runat="server" Text="Clear" CssClass="btn clearbtn" OnClick="clearButton_Click" />
                        </div>
                    </td>
                </tr>
            </table>
            
        </form>
    </div><br /><br />
    <div style="width: 95%; height: 300px; padding-left: 20px; overflow: auto;">
        <asp:GridView ID="userGridView" runat="server" GridLines="None" CssClass="myGridStyle"
            Font-Size="11px" AutoGenerateColumns="False" OnRowCommand="userGridView_RowCommand"
            DataKeyNames="Id">
            <Columns>
                <asp:BoundField DataField="USER_CODE" HeaderText="PS ID" />
                <asp:BoundField DataField="USER_NAME" HeaderText="Name" />
                <asp:BoundField DataField="USER_DESIGNATION" HeaderText="Designation" />
                <asp:BoundField DataField="USER_EMAILADDRESS" HeaderText="Email" />
                <asp:BoundField DataField="USER_LEVELCODE" HeaderText="ARM Code" />
                <asp:BoundField DataField="USER_LEVEL_NAME" HeaderText="Role" />
                <asp:BoundField DataField="USER_STATUS_NAME" HeaderText="Status" />
                <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Button Text="Select" runat="server" CommandName="Select" CssClass="btn primarybtn" CommandArgument='<%# Eval("Id") %>' />
                        <asp:Button Text="Delete" runat="server" CommandName="Del" CssClass="btn primarybtn" CommandArgument='<%# Eval("Id") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No record found
            </EmptyDataTemplate>
            <AlternatingRowStyle CssClass="odd" />
        </asp:GridView>

    </div>
    <br />
    <br />
    <br />
</asp:Content>
