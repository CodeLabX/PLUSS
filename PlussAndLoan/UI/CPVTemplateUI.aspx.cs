﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net.Mail;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_CPVTemplateUI : System.Web.UI.Page
    {
        #region page load
        protected void Page_Load(object sender, EventArgs e)
        {
            printButton.Attributes.Add("onclick", "PrintDiv()");
            RequiredFieldValidator1.Enabled = false;
            //RequiredFieldValidator2.Enabled = false;
            //RequiredFieldValidator3.Enabled = false;
            RequiredFieldValidator4.Enabled = false;
            dateTextBox1.Text = DateTime.Now.Date.ToString("dd-MM-yyyy");
        
        }
        #endregion


        #region private members
        private LoanInformation loanInformationObj;
        private LoanApplicationManager loanApplicationManagerObj;
        private CpvTemplateManager cpvTemplateManagerObj;
        private CpvTemplate cpvTemplateObj;
        #endregion


        #region searching condition
        protected void findButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.clearButton_Click(sender, e);
                RequiredFieldValidator1.Enabled = true;
                //RequiredFieldValidator2.Enabled = false;
                //RequiredFieldValidator3.Enabled = false;
                RequiredFieldValidator4.Enabled = false;
                if (loanLocatorIdTextBox.Text.Length == 0)
                {
                    RequiredFieldValidator1.IsValid = false;
                }
                if (Page.IsValid)
                {
                    int loadFromLoanApplicationStatus = this.LoadCpvTemplateMain(loanLocatorIdTextBox.Text);
                    if (loadFromLoanApplicationStatus == 1)
                    {
                        //Alert.Show("Found in both Loan Application Table and Cpv Template");
                        int j = this.LoadCpvTemplate(Convert.ToInt64(loanLocatorIdTextBox.Text));
                        if (j == 1)
                        {
                            //Alert.Show("Found in both Loan Application Table and Cpv Template");
                            saveButton.Text = "Update";
                        }
                    }
                    else
                    {
                        //Alert.Show("Not Found in Loan Application Table");
                        dateTextBox1.Text = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Searching CPV Template");
            }
        }
        #endregion


        #region searching from loan application info
        private int LoadCpvTemplateMain(string llId)
        {
            if (String.IsNullOrEmpty(loanLocatorIdTextBox.Text))
            {
                //errorMsgLabel.Text = "Please enter LLID no.";
                return 0;
            }
            else
            {
                //errorMsgLabel.Text = "";
                loanInformationObj = new LoanInformation();
                loanApplicationManagerObj = new LoanApplicationManager();

                loanInformationObj = loanApplicationManagerObj.GetLoanInformation(Convert.ToInt32(llId));
                if (loanInformationObj == null)
                {
                    //errorMsgLabel.Text = "Not Found in Loan Application Information Table.";
                    //ClearTextBoxGridView();
                    return 0;
                }
                else
                {
                    //errorMsgLabel.Text = "";                
                    applicantNameTextBox.Text = loanInformationObj.ApplicantName;
                    professionTextBox.Text = loanInformationObj.Profession;
                    resAddressTextBox.Text = loanInformationObj.ResidentAddress;
                    resPhoneTextBox.Text = loanInformationObj.ContactNumber.ContactNo1;    // res_phone
                    cellPhoneTextBox.Text = loanInformationObj.ContactNumber.ContactNo2;   // mobil

                    officeNameTextBox.Text = loanInformationObj.Business;
                    officeAddTextBox.Text = loanInformationObj.OfficeAddress;
                    officeTelNoTextBox.Text = loanInformationObj.ContactNumber.ContactNo3;  // office_fh
                    officeCellNoTextBox.Text = loanInformationObj.ContactNumber.ContactNo4;
                }
                return 1;
            }
        }
        #endregion


        #region searchng from cpvtemplate
        private int LoadCpvTemplate(Int64 llId)
        {
            List<CpvTemplate> cpvTemplateList = new List<CpvTemplate>();
            cpvTemplateManagerObj = new CpvTemplateManager();
            cpvTemplateList = cpvTemplateManagerObj.SelectCpvTemplateList(llId);
            if (cpvTemplateList.Count > 0)
            {
                foreach (CpvTemplate cpvTemplateObj in cpvTemplateList)
                {
                    HiddenField1.Value = cpvTemplateObj.CpvtID.ToString();
                    guarantorNameTextBox.Text = cpvTemplateObj.CpvtGuarantorsname;
                    designationTextBox.Text = cpvTemplateObj.CpvtDesignation;
                    contactAddressTextBox.Text = cpvTemplateObj.CpvtContactaddress;
                    telNoTextBox.Text = cpvTemplateObj.CpvtTelephoneno;
                    cellNoTextBox.Text = cpvTemplateObj.CpvtCellphone;
                    guaranteeAmountTextBox.Text = cpvTemplateObj.CpvtGuarantoreeamount;
                    relationshipTextBox.Text = cpvTemplateObj.CpvtRelationshipwithapplicant;
                    emailAddressTextBox.Text = cpvTemplateObj.CpvtEmailaddress;
                    if (DateFormat.IsDate(cpvTemplateObj.CpvtDate))
                    {
                        dateTextBox1.Text = cpvTemplateObj.CpvtDate.Date.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        dateTextBox1.Text = DateTime.Now.ToString("dd-MM-yyyy");
                    }
                }
                return 1;
            }
            else
            {
                //Alert.Show("No data in cpv template for this id");
                dateTextBox1.Text = DateTime.Now.ToString("dd-MM-yyyy");
                return 0;
            }
        }
        #endregion


        #region saving and updating
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                RequiredFieldValidator1.Enabled = true;
                //RequiredFieldValidator2.Enabled = true;
                //RequiredFieldValidator3.Enabled = true;
                RequiredFieldValidator4.Enabled = true;
                //if (dateTextBox1.Text.Length == 0)
                //{
                //    RequiredFieldValidator2.IsValid = false;
                //}
                if (Page.IsValid)
                {
                    if (!IsDuplicate())
                    {
                        var msg = "";
                        if (saveButton.Text == "Save")
                        {
                            cpvTemplateManagerObj = new CpvTemplateManager();
                            cpvTemplateObj = new CpvTemplate();

                            cpvTemplateObj.CpvtLlid = loanLocatorIdTextBox.Text;
                            cpvTemplateObj.CpvtGuarantorsname = guarantorNameTextBox.Text;
                            cpvTemplateObj.CpvtDesignation = designationTextBox.Text;
                            cpvTemplateObj.CpvtContactaddress = contactAddressTextBox.Text;
                            cpvTemplateObj.CpvtTelephoneno = telNoTextBox.Text;
                            cpvTemplateObj.CpvtCellphone = cellNoTextBox.Text;
                            cpvTemplateObj.CpvtGuarantoreeamount = guaranteeAmountTextBox.Text;
                            cpvTemplateObj.CpvtRelationshipwithapplicant = relationshipTextBox.Text;
                            cpvTemplateObj.CpvtEmailaddress = emailAddressTextBox.Text;
                            if (DateFormat.IsDate(Convert.ToDateTime(dateTextBox1.Text, new CultureInfo("ru-RU"))))
                            {
                                cpvTemplateObj.CpvtDate = Convert.ToDateTime(Convert.ToDateTime(dateTextBox1.Text, new CultureInfo("ru-RU")).ToString("yyyy-MM-dd")).Date;
                            }

                            cpvTemplateObj.CpvtEntrydate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")).Date;
                            cpvTemplateObj.CpvtUserID = 1;

                            int executionStatus = cpvTemplateManagerObj.InsertCpvTemplate(cpvTemplateObj);
                            if (executionStatus == 1)
                            {
                                msg="Save sucessful";
                                saveButton.Text = "Update";
                            }
                        }
                        else
                        {
                            cpvTemplateManagerObj = new CpvTemplateManager();
                            cpvTemplateObj = new CpvTemplate();

                            cpvTemplateObj.CpvtLlid = loanLocatorIdTextBox.Text;
                            cpvTemplateObj.CpvtGuarantorsname = guarantorNameTextBox.Text;
                            cpvTemplateObj.CpvtDesignation = designationTextBox.Text;
                            cpvTemplateObj.CpvtContactaddress = contactAddressTextBox.Text;
                            cpvTemplateObj.CpvtTelephoneno = telNoTextBox.Text;
                            cpvTemplateObj.CpvtCellphone = cellNoTextBox.Text;
                            cpvTemplateObj.CpvtGuarantoreeamount = guaranteeAmountTextBox.Text;
                            cpvTemplateObj.CpvtRelationshipwithapplicant = relationshipTextBox.Text;
                            cpvTemplateObj.CpvtEmailaddress = emailAddressTextBox.Text;
                            if (DateFormat.IsDate(Convert.ToDateTime(dateTextBox1.Text, new CultureInfo("ru-RU"))))
                            {
                                cpvTemplateObj.CpvtDate = Convert.ToDateTime(Convert.ToDateTime(dateTextBox1.Text, new CultureInfo("ru-RU")).ToString("yyyy-MM-dd")).Date;
                            }

                            cpvTemplateObj.CpvtEntrydate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")).Date;
                            cpvTemplateObj.CpvtUserID = Convert.ToInt32(Session["Id"].ToString());

                            int executionStatus = cpvTemplateManagerObj.UpdateCpvTemplate(cpvTemplateObj);
                            if (executionStatus == 1)
                            {
                                msg="Update successful";
                            }
                        }
                        new AT(this).AuditAndTraial("CPV Template", msg);
                        Alert.Show(msg);
                    }
                    else { Alert.Show("'" + loanLocatorIdTextBox.Text + "' already exist"); }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save/Update CPV Template");
            }
        }

        private bool IsDuplicate()
        {
            cpvTemplateManagerObj = new CpvTemplateManager();
            if (HiddenField1.Value == "")// duplicate checking while inserting
            {
                List<CpvTemplate> dummyCpvTemplateObjList = new List<CpvTemplate>();
                dummyCpvTemplateObjList = cpvTemplateManagerObj.SelectCpvTemplateList(Convert.ToInt64(loanLocatorIdTextBox.Text));

                CpvTemplate dummyCpvTemplate = new CpvTemplate();
                dummyCpvTemplate = dummyCpvTemplateObjList.Find(p => p.CpvtLlid == loanLocatorIdTextBox.Text);//VS-2008

                if (dummyCpvTemplate != null)
                { return true; }
                else
                { return false; }
            }
            else             // duplicate checking while updating
            {
                List<CpvTemplate> dummyCpvTemplateObjList = new List<CpvTemplate>();
                dummyCpvTemplateObjList = cpvTemplateManagerObj.SelectCpvTemplateList(Convert.ToInt64(loanLocatorIdTextBox.Text));

                CpvTemplate dummyCpvTemplate = new CpvTemplate();
                dummyCpvTemplate = dummyCpvTemplateObjList.Find(p => p.CpvtLlid == loanLocatorIdTextBox.Text);
                if (dummyCpvTemplate != null)
                {
                    if (dummyCpvTemplate.CpvtID.ToString() == HiddenField1.Value)
                    { return false; }
                    else
                    { return true; }
                }
                else { return false; }
            }
        }
        #endregion


        #region clearing fields
        protected void clearButton_Click(object sender, EventArgs e)
        {
            //RequiredFieldValidator2.IsValid = true;
            applicantNameTextBox.Text = "";
            professionTextBox.Text = "";
            resAddressTextBox.Text = "";
            resPhoneTextBox.Text = "";
            cellPhoneTextBox.Text = "";
            officeNameTextBox.Text = "";
            officeAddTextBox.Text = "";
            officeTelNoTextBox.Text = "";
            officeCellNoTextBox.Text = "";

            guarantorNameTextBox.Text = "";
            designationTextBox.Text = "";
            contactAddressTextBox.Text = "";
            telNoTextBox.Text = "";
            cellNoTextBox.Text = "";
            guaranteeAmountTextBox.Text = "";
            relationshipTextBox.Text = "";
            emailAddressTextBox.Text = "";
            //dateTextBox1.Text = "";

            saveButton.Text = "Save";
        }
        #endregion


        #region printing
        protected void printButton_Click(object sender, EventArgs e)
        {
            //string script = "<script type=text/javascript>window.print();</script>";

            //Page page = (Page)HttpContext.Current.CurrentHandler;

            //page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "script", script);
        }
        #endregion


        #region sending mail
        protected void sendButton_Click(object sender, EventArgs e)
        {

            try
            {
                if (emailAddressTextBox.Text != "")
                {
                    var msg = "";
                    try
                    {
                        string from = (string) ConfigurationManager.AppSettings.Get("fromEmailAddress");
                        MailMessage message = new MailMessage();
                        MailAddress mailAddressObj = new MailAddress(from);
                        message.From = mailAddressObj;
                        message.To.Add(emailAddressTextBox.Text.Trim());
                        message.Subject = "CPV Template";
                        ;
                        string body = MailMessage();
                        message.Body = body;
                        message.Priority = MailPriority.High;
                        message.IsBodyHtml = true;
                        SmtpClient smtpClientObj = new SmtpClient();
                        smtpClientObj.EnableSsl = false;
                        smtpClientObj.Send(message);
                        msg = "Sent successfully";
                        saveButton_Click(sender, e);
                        loanLocatorIdTextBox.Focus();
                    }
                    catch
                    {
                        msg = "Check your email address";
                    }
                    new AT(this).AuditAndTraial("CPV Template", msg);
                    Alert.Show(msg);
                }
                else
                {
                    Alert.Show("Please input email address");
                    emailAddressTextBox.Focus();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "CPV Template, sending mail");
            }

        }
        #endregion    


        #region mail sending method with body building
        private string MailMessage()
        {
            string message = null;

            /*----------replace <tr> with  style=""border-bottom-width:thin; border-bottom-color:Black"" for border*/
            // no border.... told by hiru vi
            message = @"<html xmlns=""http://www.w3.org/1999/xhtml"">" +
                      " <head>" +
                      "     <title>Cpv Template Information</title>" +
                      " </head>" +
                      " <body>" +
                      @"     <table border=""0px"">" +
                      "         <tr>" +
                      "             <td>" +
                      "                 Loan Locator ID :" +
                      "             </td>" +
                      "             <td>" +
                      loanLocatorIdTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      "         <tr>" +
                      "             <td>" +
                      "                 Date :" +
                      "             </td>" +
                      "             <td>" +
                      dateTextBox1.Text +
                      "             </td>" +
                      "         </tr>" +
                      // lately added
                      "           <tr>" +
                      @"               <td align=""left"" bgcolor=""#999999"" colspan=""2"" style=""font-weight: bold; color: White;"">" +
                      "                   Personal Details" +
                      "               </td> " +
                      "           </tr> " +
                    
                      "         <tr>" +
                      "             <td>" +
                      "                 Applicant's Name :" +
                      "             </td>" +
                      "             <td>" +
                      applicantNameTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      "         <tr>" +
                      "             <td>" +
                      "                 Profession :" +
                      "             </td>" +
                      "             <td>" +
                      professionTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      @" 	  <tr style=""border-bottom-width: thin; border-bottom-color: Black"">" +
                      "             <td>" +
                      "                 Current Residence Address : " +
                      "             </td>" +
                      "             <td>" +
                      resAddressTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Residence Telephone No. :" +
                      "             </td>" +
                      "             <td>" +
                      resPhoneTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Cell Phone No. :" +
                      "             </td>" +
                      "             <td>" +
                      cellPhoneTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      // added lately
                      "        <tr>" +
                      @"          <td align=""left"" bgcolor=""#999999"" colspan=""2"" style=""font-weight: bold; color: White;"">" +
                      "               Work Place Details" +
                      "           </td>" +
                      "       </tr>" +

                      " 	  <tr>" +
                      "             <td>" +
                      "                 Office Name :" +
                      "             </td>" +
                      "             <td>" +
                      officeNameTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Office Address :" +
                      "             </td>" +
                      "             <td>" +
                      officeAddTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Office Telephone No. :" +
                      "             </td>" +
                      "             <td>" +
                      officeTelNoTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Office Cell Phone No. :" +
                      "             </td>" +
                      "             <td>" +
                      officeCellNoTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      // added lately
                      "           <tr>" +
                      @"              <td align=""left"" bgcolor=""#999999"" colspan=""2"" style=""font-weight: bold; color: White;"">" +
                      "                   Guarantor Details" +
                      "               </td>" +
                      "           </tr>" +

                      " 	  <tr>" +
                      "             <td>" +
                      "                 Guarantor's Name :" +
                      "             </td>" +
                      "             <td>" +
                      guarantorNameTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Contact Address : " +
                      "             </td>" +
                      "             <td>" +
                      contactAddressTextBox.Text +
                      "             </td>" +
                      "         </tr>" +                    
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Designation :" +
                      "             </td>" +
                      "             <td>" +
                      designationTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Telephone No. :" +
                      "             </td>" +
                      "             <td>" +
                      telNoTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Cell Phone No. :" +
                      "             </td>" +
                      "             <td>" +
                      cellNoTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Guarantee Amount :" +
                      "             </td>" +
                      "             <td>" +
                      guaranteeAmountTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " 	  <tr>" +
                      "             <td>" +
                      "                 Relationship with Applicant :" +
                      "             </td>" +
                      "             <td>" +
                      relationshipTextBox.Text +
                      "             </td>" +
                      "         </tr>" +
                      " " +
                      "     </table>" +
                      " </body>" +
                      " </html>";
            return message;
        }
        #endregion


    }
}