﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using AutoLoanDataService;
using AutoLoanService;
using AutoLoanService.Entity;
using AutoLoanService.Interface;
using AzUtilities;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI.Dedups
{
    public partial class DedupDecline : System.Web.UI.Page
    {
        List<string> viewList;
        public LoanInformationManager loanInformationManagerObj = null;
        public LoanInformation loanInformationObj = null;
        List<DeDupInfo> dedupInfoList;

        List<DeDupInfo> multiMatchdedupInfoList;
        List<DeDupInfo> singalMatchdedupInfoList;

        List<DeDupInfo> tempDedupInfoList;
        public DeDupInfo dedupSaveObj = null;
        public ProductManager productManagerObj = null;
        public RemarksManager remarksManagerObj = null;
        public string otherStatus1 = string.Empty;
        public string otherStatus2 = string.Empty;
        public string otherStatus3 = string.Empty;
        public string otherStatus4 = string.Empty;
        public string otherStatus5 = string.Empty;
        public Color othStatusForeColor1 = Color.Black;
        public Color othStatusForeColor2 = Color.Black;
        public Color othStatusForeColor3 = Color.Black;
        public Color othStatusForeColor4 = Color.Black;
        public Color othStatusForeColor5 = Color.Black;
        string alert = "";

        string nameFirstPartcondUI = "";
        string nameSecondPartcondUI = "";

        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            STATUSUPDATE = 3,
            INSERTERROR = -1,
            UPDATEERROR = -2,
            STATUSUPDATEERROR = -3
        }

        enum ForwardOrDeferFlag : int
        {
            FORWARDID = 8,
            DEFERID = 6
        }

        private LoanApplicationManager loanApplicationManagerObj = null;
        public LoanLocatorLoanApplicantManager loanLocatorLoanApplicantManagerObj = null;
        public CIBResultManager cIBResultManagerObj = null;
        List<string> contact = new List<string>();
        List<string> id = new List<string>();
        List<string> ac = new List<string>();
        string name = "";
        string fatherName = "";
        string motherName = "";
        string businessName = "";
        string dob = "";
        string tin = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            loanInformationManagerObj = new LoanInformationManager();
            loanInformationObj = new LoanInformation();
            remarksManagerObj = new RemarksManager();
            loanApplicationManagerObj = new LoanApplicationManager();
            loanLocatorLoanApplicantManagerObj = new LoanLocatorLoanApplicantManager();
            cIBResultManagerObj = new CIBResultManager();

            string viewData = "";
            viewList = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                viewData = "";
                viewList.Add(viewData);
            }

            if (!Page.IsPostBack)
            {
                accountNoEntryGridView.DataSource = viewList;
                accountNoEntryGridView.DataBind();

                scbAccountEntryGridView.DataSource = viewList;
                scbAccountEntryGridView.DataBind();

                idEntryGridView.DataSource = viewList;
                idEntryGridView.DataBind();

                contactNoEntryGridView.DataSource = viewList;
                contactNoEntryGridView.DataBind();

                negativeListPersonContactNoGridView.DataSource = viewList;
                negativeListPersonContactNoGridView.DataBind();

                negativeListPersonIdGridView.DataSource = viewList;
                negativeListPersonIdGridView.DataBind();

                negativeListPersonScbAccountGridView.DataSource = viewList;
                negativeListPersonScbAccountGridView.DataBind();

                disbursedLoanAcForSCBGridView.DataSource = viewList;
                disbursedLoanAcForSCBGridView.DataBind();

                disbursedLoanContactNoGridView.DataSource = viewList;
                disbursedLoanContactNoGridView.DataBind();

                disbursedLoanIdGridView.DataSource = viewList;
                disbursedLoanIdGridView.DataBind();

                FillRemarksGridView();

            }
            oKButton.Attributes.Add("onclick", "return JS_FunctionCheckRequest()");
        }

        private void FillRemarksGridView()
        {
            try
            {
                List<Remarks> remarksListObj = remarksManagerObj.SelectRemarksList(2);
                remarksGridView.DataSource = remarksListObj;
                remarksGridView.DataBind();
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }
        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {
            MultiView1.ActiveViewIndex = -1;
            MultiView1.ActiveViewIndex = Convert.ToInt32(e.Item.Value);
            if (Menu1.Items[0].Selected)
            {
                Tab1_Activate();//negetive list person
            }
            if (Menu1.Items[1].Selected)
            {
                Tab2_Activate(); //neg employer
            }
            if (Menu1.Items[2].Selected)
            {
                View1_Activate(); // loan applicant
            }
            if (Menu1.Items[3].Selected)
            {
                Tab5_Activate(); //DisbursedLoan
            }
            //if (Menu1.Items[4].Selected)
            //{
            //    Tab4_Activate();//CIBResult
            //}

        }


        private void ClearNegativeListPersonTabDedailSection()
        {
            try
            {
                negativeListPersonApplicantNameTextBox.BackColor = System.Drawing.Color.White;
                negativeListPersonFatherNameTextBox.BackColor = System.Drawing.Color.White;
                negativeListPersonMotherNameTextBox.BackColor = System.Drawing.Color.White;
                negativeListPersonDobTextBox.BackColor = System.Drawing.Color.White;
                negativeListPersonTinNoTextBox.BackColor = System.Drawing.Color.White;
                negativeListPersonCompanyNameTextBox.BackColor = System.Drawing.Color.White;
                negativeListPersonCompanyNameTextBox.BackColor = System.Drawing.Color.White;
                foreach (GridViewRow gvr in negativeListPersonContactNoGridView.Rows)
                {
                    gvr.BackColor = System.Drawing.Color.White;
                }
                foreach (GridViewRow gvr in negativeListPersonIdGridView.Rows)
                {
                    gvr.BackColor = System.Drawing.Color.White;
                }
                foreach (GridViewRow gvr in negativeListPersonScbAccountGridView.Rows)
                {
                    gvr.BackColor = System.Drawing.Color.White;
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }


        public List<string> NameSplitter(string stringToBeSplitted)
        {
            string[] splitKey = { " " };
            string[] splittedName = applicantNameTextBox0.Text.Split(splitKey, StringSplitOptions.RemoveEmptyEntries);
            List<string> tempBrokenNameList = new List<string>();
            for (int i = 0; i < splittedName.Length; i++)
            {
                if (splittedName[i].ToLower() != "md " && splittedName[i].ToLower() != "mr " && splittedName[i].ToLower() != "mrs " && splittedName[i].ToLower() != "miss " && splittedName[i].ToLower() != "md." && splittedName[i].ToLower() != "mr." && splittedName[i].ToLower() != "mrs." && splittedName[i].ToLower() != "miss.")
                {
                    tempBrokenNameList.Add(splittedName[i].ToString());
                }
            }
            return tempBrokenNameList;
        }

        private void NegetivePersonListLoad()
        {
            try
            {
                ParameterCondition();
                firstTotalRecordLabel.Text = "Total Record : ";
                DeDupInfoManager deDupInfoManagerObj = new DeDupInfoManager();
                tempDedupInfoList = deDupInfoManagerObj.GetAllDeDupInfo(name, fatherName, motherName, businessName, dob, contact, id, ac, /*bankBranch,*/ tin, 1);

                LogFile.WriteLine("tempDedupInfoList count :: " + tempDedupInfoList.Count);

                if (exactNameSearchChckBox.Checked == true)
                {
                    List<DeDupInfo> dedupListObj = new List<DeDupInfo>();

                    IEnumerable<DeDupInfo> userListExact = from u in tempDedupInfoList
                                                           where u.Name.ToUpper().Trim().Equals(name)
                                                           select u;
                    IEnumerable<DeDupInfo> userList = from u in tempDedupInfoList
                                                      where u.Name.ToUpper().Trim().Contains(name) && !u.Name.ToUpper().Trim().Equals(name)
                                                      select u;


                    tempDedupInfoList = new List<DeDupInfo>();
                    tempDedupInfoList.AddRange(userListExact);

                    //REMOVE EXACT NAME FROM userlist TO REMOVE DUPLICATE NAME
                    List<DeDupInfo> userListFiltered = new List<DeDupInfo>();
                    userListFiltered.AddRange(userList);
                    //

                    tempDedupInfoList.AddRange(userListFiltered);
                }



                dedupInfoList = new List<DeDupInfo>();
                multiMatchdedupInfoList = new List<DeDupInfo>();
                singalMatchdedupInfoList = new List<DeDupInfo>();
                if (tempDedupInfoList != null)
                {
                    dedupInfoList = MatchFound(tempDedupInfoList);
                    dedupInfoList.Sort(delegate (DeDupInfo d1, DeDupInfo d2) { return d1.MatchFoundNumber.CompareTo(d2.MatchFoundNumber); });
                    dedupInfoList.Reverse();
                    //dedupInfoList.OrderByDescending(dedup => dedup.MatchFoundNumber).ThenByDescending(dedup => dedup.Name);
                    Session["dedupInfoListS"] = dedupInfoList;
                    negativeListPersonGridView.DataSource = dedupInfoList;
                    negativeListPersonGridView.PageIndex = 0;
                    negativeListPersonGridView.DataBind();
                    firstTotalRecordLabel.Text = "Temp:" + tempDedupInfoList.Count.ToString() + "Total Record : " + dedupInfoList.Count.ToString();
                    totalRecordLabel.Text = dedupInfoList.Count.ToString();
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                throw new Exception(ex.Message);
            }

        }



        private List<string> CommaSplitterMethod(string givenString)
        {
            if (!String.IsNullOrEmpty(givenString) && givenString.Contains(","))
            {
                givenString = givenString.Replace(" ", "");
                List<string> splittedStringList = new List<string>();
                if (givenString != "")
                {
                    string[] key = { "," };
                    string[] splitedString = givenString.Split(key, StringSplitOptions.RemoveEmptyEntries);
                    for (int initial = 0; initial < splitedString.Length; initial++)
                    {
                        splittedStringList.Add(splitedString[initial].ToString());
                    }
                }
                return splittedStringList;
            }
            else
            {
                var list = new List<string>();
                if (!String.IsNullOrEmpty(givenString))
                    list.Add(givenString);

                return list;
            }

        }


        protected void negativeListPersonGridView_SelectedIndexChanged(object sender, EventArgs e)
        {

            ClearNegativeListPersonTabDedailSection();
            FillTextBox(negativeListPersonGridView.SelectedIndex);
        }

        private int _i = 0;
        protected void negativeListPersonGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onclick",
                    ClientScript.GetPostBackEventReference(negativeListPersonGridView, "Select$" +
                                                                                       e.Row.RowIndex.ToString()));
            }
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Alternate || e.Row.RowState == DataControlRowState.Normal))
            {
                //e.Row.Attributes.Add("id", _i.ToString());

                //e.Row.Attributes.Add("onKeyDown", "SelectRow();");

                //e.Row.Attributes.Add("onclick", "MarkRow(" + _i.ToString() + ");");
                //e.Row.Attributes.Add("onKeyDown", ClientScript.GetPostBackEventReference(negativeListPersonGridView, "Select$" + e.Row.RowIndex.ToString()));
                //e.Row.Attributes.Add("onKeyUp", ClientScript.GetPostBackEventReference(negativeListPersonGridView, "Select$" + e.Row.RowIndex.ToString()));
                //e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(negativeListPersonGridView, "Select$" + e.Row.RowIndex.ToString()));


                _i++;

            }

        }

        //protected override void Render(HtmlTextWriter htmlTextW)
        //{
        //    for (int i = 0; i < negativeListPersonGridView.Rows.Count; i++)
        //    {
        //        Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(negativeListPersonGridView, "Select$" + i.ToString()));
        //    }
        //    for (int i = 0; i < disbursedLoanGridView.Rows.Count; i++)
        //    {
        //        Page.ClientScript.RegisterForEventValidation(new System.Web.UI.PostBackOptions(disbursedLoanGridView, "Select$" + i.ToString()));
        //    }
        //    string t = (string)System.Web.HttpContext.Current.Session["UserId"];

        //    base.Render(htmlTextW);
        //}

        private void FillTextBox(int index)
        {
            DeDupInfo dedupObj = new DeDupInfo();
            dedupInfoList = (List<DeDupInfo>)Session["dedupInfoListS"];
            try
            {
                dedupObj = dedupInfoList[index + negativeListPersonGridView.PageIndex * negativeListPersonGridView.PageSize];
            }
            catch
            {
            }
            Session["dedupSelectedObjS"] = dedupObj;

            negativeListPersonStatusTextBox.Text = dedupObj.Status;

            negativeListPersonApplicantNameTextBox.Text = dedupObj.Name;

            if (dedupObj.DateOfBirth.ToString("dd-MM-yyyy") == "01-01-0001")
            {
                negativeListPersonDobTextBox.Text = "";
            }
            else
            {
                negativeListPersonDobTextBox.Text = dedupObj.DateOfBirth.ToString("dd-MM-yyyy");
            }
            negativeListPersonFatherNameTextBox.Text = dedupObj.FatherName;
            negativeListPersonMotherNameTextBox.Text = dedupObj.MotherName;
            negativeListPersonEducationalTextBox.Text = dedupObj.EducationalLevel;
            negativeListPersonMaritalStatusTextBox.Text = dedupObj.MaritalStatus;
            negativeListPersonResidentialAddressTextBox.Text = dedupObj.Address1;
            negativeListPersonResidentialStatusTextBox.Text = dedupObj.ResidentaileStatus;
            negativeListPersonTinNoTextBox.Text = dedupObj.Tin;
            negativeListPersonCompanyNameTextBox.Text = dedupObj.BusinessEmployeeName;
            negativeListPersonOfficeAddressTextBox.Text = dedupObj.Address2;
            try      // because if he have only 2 number then list dont have [3]/[4]....
            {
                negativeListPersonContactNoGridView.Rows[0].Cells[0].Text = dedupObj.SplittedContactList[0];
                negativeListPersonContactNoGridView.Rows[1].Cells[0].Text = dedupObj.SplittedContactList[1];
                negativeListPersonContactNoGridView.Rows[2].Cells[0].Text = dedupObj.SplittedContactList[2];
                negativeListPersonContactNoGridView.Rows[3].Cells[0].Text = dedupObj.SplittedContactList[3];
                negativeListPersonContactNoGridView.Rows[4].Cells[0].Text = dedupObj.SplittedContactList[4];
            }
            catch { }

            negativeListPersonIdGridView.Rows[0].Cells[0].Text = dedupObj.ApplicantId.Type1.ToString();
            negativeListPersonIdGridView.Rows[0].Cells[1].Text = dedupObj.ApplicantId.Id1;
            negativeListPersonIdGridView.Rows[1].Cells[0].Text = dedupObj.ApplicantId.Type2;
            negativeListPersonIdGridView.Rows[1].Cells[1].Text = dedupObj.ApplicantId.Id2;
            negativeListPersonIdGridView.Rows[2].Cells[0].Text = dedupObj.ApplicantId.Type3;
            negativeListPersonIdGridView.Rows[2].Cells[1].Text = dedupObj.ApplicantId.Id3;
            negativeListPersonIdGridView.Rows[3].Cells[0].Text = dedupObj.ApplicantId.Type4;
            negativeListPersonIdGridView.Rows[3].Cells[1].Text = dedupObj.ApplicantId.Id4;
            negativeListPersonIdGridView.Rows[4].Cells[0].Text = dedupObj.ApplicantId.Type5;
            negativeListPersonIdGridView.Rows[4].Cells[1].Text = dedupObj.ApplicantId.Id5;

            if (dedupObj.AccountNo.MasterAccNo1.Length != 16)
            {
                if (!dedupObj.AccountNo.MasterAccNo1.Contains(","))
                {
                    if (dedupObj.AccountNo.PreAccNo1 != null)
                    {
                        negativeListPersonScbAccountGridView.Rows[0].Cells[0].Text = dedupObj.AccountNo.PreAccNo1.ToString();
                    }
                    else
                    {
                        negativeListPersonScbAccountGridView.Rows[0].Cells[0].Text = "";
                    }
                    if (dedupObj.AccountNo.MasterAccNo1 != null)
                    {
                        negativeListPersonScbAccountGridView.Rows[0].Cells[1].Text = dedupObj.AccountNo.MasterAccNo1.ToString();
                    }
                    else
                    {
                        negativeListPersonScbAccountGridView.Rows[0].Cells[1].Text = "";
                    }
                    if (dedupObj.AccountNo.PostAccNo1 != null)
                    {
                        negativeListPersonScbAccountGridView.Rows[0].Cells[2].Text = dedupObj.AccountNo.PostAccNo1.ToString();
                    }
                    else
                    {
                        negativeListPersonScbAccountGridView.Rows[0].Cells[2].Text = "";
                    }
                    creditCardNoTextBox.Text = "";
                }
                else
                {
                    for (int i = 0; i < dedupObj.SplittedAccountList.Count; i++)
                    {
                        if (dedupObj.SplittedAccountList[i].Length == 11)
                        {
                            negativeListPersonScbAccountGridView.Rows[i].Cells[0].Text = dedupObj.SplittedAccountList[i].Substring(0, 2).ToString();
                            negativeListPersonScbAccountGridView.Rows[i].Cells[1].Text = dedupObj.SplittedAccountList[i].Substring(2, 7).ToString();
                            negativeListPersonScbAccountGridView.Rows[i].Cells[2].Text = dedupObj.SplittedAccountList[i].Substring(9, 2).ToString();
                            creditCardNoTextBox.Text = "";
                        }
                        else if (dedupObj.SplittedAccountList[i].Length < 11)
                        {
                            negativeListPersonScbAccountGridView.Rows[i].Cells[0].Text = "";
                            negativeListPersonScbAccountGridView.Rows[i].Cells[1].Text = dedupObj.SplittedAccountList[i].ToString();
                            negativeListPersonScbAccountGridView.Rows[i].Cells[2].Text = "";
                            creditCardNoTextBox.Text = "";
                        }
                    }
                }
            }
            else
            {
                if (dedupObj.AccountNo.MasterAccNo1 != null)
                {
                    creditCardNoTextBox.Text = dedupObj.AccountNo.MasterAccNo1.ToString();
                }
            }
            if (dedupObj.DeclineDate.ToString("dd-MM-yyyy") == "01-01-0001")
            {
                negativeListPersonDeclineDateTextBox.Text = "";
            }
            else
            {
                negativeListPersonDeclineDateTextBox.Text = dedupObj.DeclineDate.ToString("dd-MM-yyyy");
            }

            negativeListPersonDeclineReasonTextBox.Text = dedupObj.DeclineReason.ToString() + "" + dedupObj.Remarks.ToString();
            if (dedupObj.ProductId > 0)
            {
                productManagerObj = new ProductManager();
                negativeListPersonAccountNameTextBox.Text = productManagerObj.GetProductName(dedupObj.ProductId);
            }
            else
            {
                negativeListPersonAccountNameTextBox.Text = "";
            }

            negativeListPersonDedupIdTextBox.Text = dedupObj.Id.ToString();
            foreach (string s in dedupObj.MatchFound)
            {
                if (s == ("Name_"))
                {
                    negativeListPersonApplicantNameTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("FatherName_"))
                {
                    negativeListPersonFatherNameTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("MotherName_"))
                {
                    negativeListPersonMotherNameTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("BusinessEmployeeName_"))
                {
                    negativeListPersonCompanyNameTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("DateOfBirth_"))
                {
                    negativeListPersonDobTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("ContactNo1_"))
                {
                    negativeListPersonContactNoGridView.Rows[0].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("ContactNo2_"))
                {
                    negativeListPersonContactNoGridView.Rows[1].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("ContactNo3_"))
                {
                    negativeListPersonContactNoGridView.Rows[2].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("ContactNo4_"))
                {
                    negativeListPersonContactNoGridView.Rows[3].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id1_"))
                {
                    negativeListPersonIdGridView.Rows[0].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id2_"))
                {
                    negativeListPersonIdGridView.Rows[1].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id3_"))
                {
                    negativeListPersonIdGridView.Rows[2].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id4_"))
                {
                    negativeListPersonIdGridView.Rows[3].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id5_"))
                {
                    negativeListPersonIdGridView.Rows[4].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac1_"))
                {
                    negativeListPersonScbAccountGridView.Rows[0].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac2_"))
                {
                    negativeListPersonScbAccountGridView.Rows[1].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac3_"))
                {
                    negativeListPersonScbAccountGridView.Rows[2].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac4_"))
                {
                    negativeListPersonScbAccountGridView.Rows[3].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac5_"))
                {
                    negativeListPersonScbAccountGridView.Rows[4].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Tin_"))
                {
                    negativeListPersonTinNoTextBox.BackColor = System.Drawing.Color.Red;
                }
            }

        }

        protected void findButton_Click(object sender, EventArgs e)
        {
            try
            {
                CheckUncheckAllCheckBox(false);
                selectAllCheckBox.Checked = false;

                if (String.IsNullOrEmpty(loanLocatorIdTextBox.Text))
                {
                    errorMsgLabel.Text = "Please enter LLID no.";
                }
                else
                {
                    errorMsgLabel.Text = "";
                    loanInformationObj = loanInformationManagerObj.GetParticularApplicantLoanInfo(Convert.ToInt32(loanLocatorIdTextBox.Text));
                    if (loanInformationObj == null)
                    {
                        errorMsgLabel.Text = "No data found.";
                        ClearTextBoxGridView();
                        ClearAllResultedGrid();
                        accountNoEntryGridView.DataSource = null;
                        accountNoEntryGridView.DataBind();

                        modalPopup.ShowWarning("No Data Found with LLID : " + loanLocatorIdTextBox.Text, Title);
                        return;
                    }
                    else
                    {
                        errorMsgLabel.Text = "";
                        Session["loanInformationObjS"] = loanInformationObj;
                        applicantNameTextBox0.Text = loanInformationObj.ApplicantName.ToString();
                        fatherNameTextBox10.Text = loanInformationObj.FatherName.ToString();
                        motherNameTextBox0.Text = loanInformationObj.MotherName.ToString();
                        businessNameTextBox0.Text = loanInformationObj.Business.ToString();
                        if (DateFormat.IsDate(loanInformationObj.DOB))
                        {
                            dateOfBirthTextBox0.Text = loanInformationObj.DOB.ToString("dd-MM-yyyy");
                        }
                        else { dateOfBirthTextBox0.Text = ""; }
                        tinNoTextBox.Text = loanInformationObj.TINNo.ToString();

                        (contactNoEntryGridView.Rows[0].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo1.ToString();
                        (contactNoEntryGridView.Rows[1].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo2.ToString();
                        (contactNoEntryGridView.Rows[2].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo3.ToString();
                        (contactNoEntryGridView.Rows[3].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo4.ToString();
                        (contactNoEntryGridView.Rows[4].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo5.ToString();

                        if (loanInformationObj.ApplicantId.Type1.ToString() != "Null")
                        {
                            (idEntryGridView.Rows[0].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type1.ToString();
                        }
                        else
                        {
                            (idEntryGridView.Rows[0].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = "";
                        }
                        (idEntryGridView.Rows[0].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id1.ToString();
                        if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate1))
                        {
                            (idEntryGridView.Rows[0].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate1.ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            (idEntryGridView.Rows[0].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                        }
                        if (loanInformationObj.ApplicantId.Type2.ToString() != "Null")
                        {
                            (idEntryGridView.Rows[1].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type2.ToString();
                        }
                        else
                        {
                            (idEntryGridView.Rows[1].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = "";
                        }
                        (idEntryGridView.Rows[1].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id2.ToString();
                        if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate2))
                        {
                            (idEntryGridView.Rows[1].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate2.ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            (idEntryGridView.Rows[1].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                        }
                        if (loanInformationObj.ApplicantId.Type3.ToString() != "Null")
                        {
                            (idEntryGridView.Rows[2].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type3.ToString();
                        }
                        else
                        {
                            (idEntryGridView.Rows[2].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = "";
                        }
                        (idEntryGridView.Rows[2].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id3.ToString();
                        if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate3))
                        {
                            (idEntryGridView.Rows[2].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate3.ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            (idEntryGridView.Rows[2].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                        }
                        if (loanInformationObj.ApplicantId.Type4.ToString() != "Null")
                        {
                            (idEntryGridView.Rows[3].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type4.ToString();
                        }
                        else
                        {
                            (idEntryGridView.Rows[3].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = "";
                        }
                        (idEntryGridView.Rows[3].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id4.ToString();
                        if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate4))
                        {
                            (idEntryGridView.Rows[3].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate4.ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            (idEntryGridView.Rows[3].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                        }
                        if (loanInformationObj.ApplicantId.Type5.ToString() != "Null")
                        {
                            (idEntryGridView.Rows[4].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type5.ToString();
                        }
                        else
                        {
                            (idEntryGridView.Rows[4].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = "";
                        }
                        (idEntryGridView.Rows[4].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id5.ToString();
                        if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate5))
                        {
                            (idEntryGridView.Rows[4].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate5.ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            (idEntryGridView.Rows[4].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                        }

                        if (loanInformationObj.AccountNo.PreAccNo1.ToString() == "" && loanInformationObj.AccountNo.PostAccNo1.ToString() == "")
                        {
                            (scbAccountEntryGridView.Rows[0].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                            (scbAccountEntryGridView.Rows[0].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo1.ToString();
                            (scbAccountEntryGridView.Rows[0].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                        }
                        else
                        {
                            (scbAccountEntryGridView.Rows[0].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo1.ToString();
                            (scbAccountEntryGridView.Rows[0].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo1.ToString();
                            (scbAccountEntryGridView.Rows[0].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo1.ToString();
                        }
                        if (loanInformationObj.AccountNo.PreAccNo2.ToString() == "" && loanInformationObj.AccountNo.PostAccNo2.ToString() == "")
                        {
                            (scbAccountEntryGridView.Rows[1].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                            (scbAccountEntryGridView.Rows[1].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo2.ToString();
                            (scbAccountEntryGridView.Rows[1].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                        }
                        else
                        {
                            (scbAccountEntryGridView.Rows[1].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo2.ToString();
                            (scbAccountEntryGridView.Rows[1].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo2.ToString();
                            (scbAccountEntryGridView.Rows[1].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo2.ToString();
                        }
                        if (loanInformationObj.AccountNo.PreAccNo3.ToString() == "" && loanInformationObj.AccountNo.PostAccNo3.ToString() == "")
                        {
                            (scbAccountEntryGridView.Rows[2].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                            (scbAccountEntryGridView.Rows[2].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo3.ToString();
                            (scbAccountEntryGridView.Rows[2].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                        }
                        else
                        {
                            (scbAccountEntryGridView.Rows[2].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo3.ToString();
                            (scbAccountEntryGridView.Rows[2].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo3.ToString();
                            (scbAccountEntryGridView.Rows[2].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo3.ToString();
                        }
                        if (loanInformationObj.AccountNo.PreAccNo4.ToString() == "" && loanInformationObj.AccountNo.PostAccNo4.ToString() == "")
                        {
                            (scbAccountEntryGridView.Rows[3].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                            (scbAccountEntryGridView.Rows[3].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo4.ToString();
                            (scbAccountEntryGridView.Rows[3].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                        }
                        else
                        {
                            (scbAccountEntryGridView.Rows[3].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo4.ToString();
                            (scbAccountEntryGridView.Rows[3].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo4.ToString();
                            (scbAccountEntryGridView.Rows[3].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo4.ToString();
                        }
                        if (loanInformationObj.AccountNo.PreAccNo5.ToString() == "" && loanInformationObj.AccountNo.PostAccNo5.ToString() == "")
                        {
                            (scbAccountEntryGridView.Rows[4].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                            (scbAccountEntryGridView.Rows[4].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo5.ToString();
                            (scbAccountEntryGridView.Rows[4].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                        }
                        else
                        {
                            (scbAccountEntryGridView.Rows[4].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo5.ToString();
                            (scbAccountEntryGridView.Rows[4].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo5.ToString();
                            (scbAccountEntryGridView.Rows[4].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo5.ToString();
                        }


                        switch (loanInformationObj.OtherBankAccount.OtherStatus1)
                        {
                            case "0":
                                otherStatus1 = "Offline Clear";
                                othStatusForeColor1 = Color.Green;
                                break;
                            case "1":
                                otherStatus1 = "Online Clear";
                                othStatusForeColor1 = Color.Green;
                                break;
                            case "2":
                                otherStatus1 = "Offline Not Clear";
                                othStatusForeColor1 = Color.Red;
                                break;
                        }
                        switch (loanInformationObj.OtherBankAccount.OtherStatus2)
                        {
                            case "0":
                                otherStatus2 = "Offline Clear";
                                othStatusForeColor2 = Color.Green;
                                break;
                            case "1":
                                otherStatus2 = "Online Clear";
                                othStatusForeColor2 = Color.Green;
                                break;
                            case "2":
                                otherStatus2 = "Offline Not Clear";
                                othStatusForeColor2 = Color.Red;
                                break;
                        }
                        switch (loanInformationObj.OtherBankAccount.OtherStatus3)
                        {
                            case "0":
                                otherStatus3 = "Offline Clear";
                                othStatusForeColor3 = Color.Green;
                                break;
                            case "1":
                                otherStatus3 = "Online Clear";
                                othStatusForeColor3 = Color.Green;
                                break;
                            case "2":
                                otherStatus3 = "Offline Not Clear";
                                othStatusForeColor3 = Color.Red;
                                break;
                        }
                        switch (loanInformationObj.OtherBankAccount.OtherStatus4)
                        {
                            case "0":
                                otherStatus4 = "Offline Clear";
                                othStatusForeColor4 = Color.Green;
                                break;
                            case "1":
                                otherStatus4 = "Online Clear";
                                othStatusForeColor4 = Color.Green;
                                break;
                            case "2":
                                otherStatus4 = "Offline Not Clear";
                                othStatusForeColor4 = Color.Red;
                                break;
                        }
                        switch (loanInformationObj.OtherBankAccount.OtherStatus5)
                        {
                            case "0":
                                otherStatus5 = "Offline Clear";
                                othStatusForeColor5 = Color.Green;
                                break;
                            case "1":
                                otherStatus5 = "Online Clear";
                                othStatusForeColor5 = Color.Green;
                                break;
                            case "2":
                                otherStatus5 = "Offline Not Clear";
                                othStatusForeColor5 = Color.Red;
                                break;
                        }
                        try
                        {
                            if (loanInformationObj.OtherBankAccount.OtherBank1 != null && loanInformationObj.OtherBankAccount.OtherBranch1 != null)
                            {
                                (accountNoEntryGridView.Rows[0].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank1.ToString();
                                (accountNoEntryGridView.Rows[0].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch1.ToString();
                                (accountNoEntryGridView.Rows[0].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo1.ToString();
                                (accountNoEntryGridView.Rows[0].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType1.ToString();
                                (accountNoEntryGridView.Rows[0].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus1;
                                accountNoEntryGridView.Rows[0].Cells[4].ForeColor = othStatusForeColor1;
                            }
                            if (loanInformationObj.OtherBankAccount.OtherBank2 != null && loanInformationObj.OtherBankAccount.OtherBranch2 != null)
                            {
                                (accountNoEntryGridView.Rows[1].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank2.ToString();
                                (accountNoEntryGridView.Rows[1].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch2.ToString();
                                (accountNoEntryGridView.Rows[1].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo2.ToString();
                                (accountNoEntryGridView.Rows[1].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType2.ToString();
                                (accountNoEntryGridView.Rows[1].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus2;
                                accountNoEntryGridView.Rows[1].Cells[4].ForeColor = othStatusForeColor2;
                            }
                            if (loanInformationObj.OtherBankAccount.OtherBank3 != null && loanInformationObj.OtherBankAccount.OtherBranch3 != null)
                            {
                                (accountNoEntryGridView.Rows[2].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank3.ToString();
                                (accountNoEntryGridView.Rows[2].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch3.ToString();
                                (accountNoEntryGridView.Rows[2].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo3.ToString();
                                (accountNoEntryGridView.Rows[2].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType3.ToString();
                                (accountNoEntryGridView.Rows[2].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus3;
                                accountNoEntryGridView.Rows[2].Cells[4].ForeColor = othStatusForeColor3;
                            }
                            if (loanInformationObj.OtherBankAccount.OtherBank4 != null && loanInformationObj.OtherBankAccount.OtherBranch4 != null)
                            {
                                (accountNoEntryGridView.Rows[3].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank4.ToString();
                                (accountNoEntryGridView.Rows[3].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch4.ToString();
                                (accountNoEntryGridView.Rows[3].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo4.ToString();
                                (accountNoEntryGridView.Rows[3].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType4.ToString();
                                (accountNoEntryGridView.Rows[3].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus4;
                                accountNoEntryGridView.Rows[3].Cells[4].ForeColor = othStatusForeColor4;
                            }
                            if (loanInformationObj.OtherBankAccount.OtherBank5 != null && loanInformationObj.OtherBankAccount.OtherBranch5 != null)
                            {
                                (accountNoEntryGridView.Rows[4].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank5.ToString();
                                (accountNoEntryGridView.Rows[4].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch5.ToString();
                                (accountNoEntryGridView.Rows[4].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo5.ToString();
                                (accountNoEntryGridView.Rows[4].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType5.ToString();
                                (accountNoEntryGridView.Rows[4].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus5;
                                accountNoEntryGridView.Rows[4].Cells[4].ForeColor = othStatusForeColor5;
                            }
                        }
                        catch
                        {
                        }
                        int totalRow = 0;
                        foreach (GridViewRow gvr in accountNoEntryGridView.Rows)
                        {
                            if ((gvr.FindControl("bankEntryLabel") as Label).Text != "")
                            {
                                totalRow = totalRow + 1;
                            }
                        }
                        sixTotalRecordLabel.Text = "Total Record : " + totalRow.ToString();
                        ClearAllResultedGrid();

                    }
                    //call search related method
                    selectAllCheckBox_CheckedChanged(sender, e);
                    //NegetivePersonListLoad();
                    DisbursedLoanListLoad();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Auto Loan Decline");
            }
        }

        private void ClearAllResultedGrid()
        {
            loanApplicantGridView.DataSource = null;
            loanApplicantGridView.DataBind();
            cibResultGridView.DataSource = null;
            cibResultGridView.DataBind();
            negativeEmployersGridView.DataSource = null;
            negativeEmployersGridView.DataBind();
            negativeListPersonGridView.DataSource = null;
            negativeListPersonGridView.DataBind();
            disbursedLoanGridView.DataSource = null;
            disbursedLoanGridView.DataBind();
        }

        private void ClearTextBoxGridView()
        {
            applicantNameTextBox0.Text = "";
            fatherNameTextBox10.Text = "";
            motherNameTextBox0.Text = "";
            businessNameTextBox0.Text = "";
            dateOfBirthTextBox0.Text = "";
            tinNoTextBox.Text = "";
            creditCardNoTextBox.Text = "";
            foreach (GridViewRow gvr in contactNoEntryGridView.Rows)
            {
                (gvr.Cells[0].FindControl("contactnoentrytextbox") as TextBox).Text = "";
            }

            foreach (GridViewRow gvr in idEntryGridView.Rows)
            {
                (gvr.Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = "";
                (gvr.Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = "";
                (gvr.Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
            }
            foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
            {
                (gvr.Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                (gvr.Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = "";
                (gvr.Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
            }
            foreach (GridViewRow gvr in accountNoEntryGridView.Rows)
            {
                (gvr.Cells[0].FindControl("bankEntryLabel") as Label).Text = "";
                (gvr.Cells[1].FindControl("branchEntryLabel") as Label).Text = "";
                (gvr.Cells[2].FindControl("acNoEntryLabel") as Label).Text = "";
                (gvr.Cells[3].FindControl("acTypeEntryLabel") as Label).Text = "";
                (gvr.Cells[4].FindControl("statusLabel") as Label).Text = "";
            }
        }

        protected void Tab2_Activate()
        {
            NegetiveListEmployerManager negetiveListEmployerManagerObj = new NegetiveListEmployerManager();
            List<NegetiveListEmployer> negetiveListEmployerObj = new List<NegetiveListEmployer>();
            string business = String.Empty;
            List<string> bank = new List<string>();
            List<string> branch = new List<string>();
            foreach (GridViewRow gvr in accountNoEntryGridView.Rows)
            {
                bank.Add((gvr.Cells[0].FindControl("bankEntryLabel") as Label).Text);
            }
            foreach (GridViewRow gvr in accountNoEntryGridView.Rows)
            {
                branch.Add((gvr.Cells[1].FindControl("branchEntryLabel") as Label).Text);
            }
            if (businessNameCheckbox.Checked)
            {
                business = businessNameTextBox0.Text;
            }
            else
            {
                business = "";
            }
            if (business != "" || bank.Count > 0 || branch.Count > 0)
            {
                negetiveListEmployerObj = negetiveListEmployerManagerObj.GetNegetiveListEmployer(business, bank, branch);
                #region tarek 29_4_2011
                if (exactNameSearchChckBox.Checked == true)
                {
                    IEnumerable<NegetiveListEmployer> userListExact = from u in negetiveListEmployerObj
                                                                      where u.BankCompany.ToUpper().Trim().Equals(name)
                                                                      select u;
                    IEnumerable<NegetiveListEmployer> userList = from u in negetiveListEmployerObj
                                                                 where u.BankCompany.ToUpper().Trim().Contains(name) && !u.BankCompany.ToUpper().Trim().Equals(name)
                                                                 select u;


                    negetiveListEmployerObj = new List<NegetiveListEmployer>();
                    negetiveListEmployerObj.AddRange(userListExact);

                    //REMOVE EXACT NAME FROM userlist TO REMOVE DUPLICATE NAME
                    List<NegetiveListEmployer> userListFiltered = new List<NegetiveListEmployer>();
                    userListFiltered.AddRange(userList);
                    //

                    negetiveListEmployerObj.AddRange(userListFiltered);
                }
                #endregion
                negativeEmployersGridView.DataSource = negetiveListEmployerObj;
                negativeEmployersGridView.DataBind();
                secondTotalRecordLabel.Text = "Total Record : " + negetiveListEmployerObj.Count.ToString();
            }
            else
            {
                negativeEmployersGridView.DataSource = null;
                negativeEmployersGridView.DataBind();
                secondTotalRecordLabel.Text = "Total Record : ";
            }
        }

        protected void negativeListPersonGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void Tab1_Activate()
        {
            try
            {
                ClearNegativeListPersonTabDedailSection();
                if (!IsEmptyParameter())
                {
                    NegetivePersonListLoad();

                    string t = (string)System.Web.HttpContext.Current.Session["UserId"];

                }
                else
                {
                    negativeListPersonGridView.DataSource = null;
                    negativeListPersonGridView.DataBind();
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
                throw new Exception(ex.Message);
            }
        }
        private bool IsEmptyParameter()
        {
            int flag = 0;
            if (applicantNameCheckbox.Checked == true)
            {
                flag = 1;
            }
            if (fatherNameCheckBox.Checked == true)
            {
                flag = 1;
            }
            if (motherNameCheckbox.Checked == true)
            {
                flag = 1;
            }
            if (businessNameCheckbox.Checked == true)
            {
                flag = 1;
            }
            if (dateOfBirthCheckBox0.Checked == true)
            {
                flag = 1;
            }
            if (tinNoCheckBox.Checked == true)
            {
                flag = 1;
            }


            foreach (GridViewRow gvr in idEntryGridView.Rows)
            {
                if ((gvr.FindControl("idEntryCheckBox") as CheckBox).Checked == true)
                {
                    flag = 1;
                }
            }
            foreach (GridViewRow gvr in contactNoEntryGridView.Rows)
            {
                if ((gvr.FindControl("contactNoEntryCheckBox") as CheckBox).Checked == true)
                {
                    flag = 1;
                }
            }
            foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
            {
                if ((gvr.FindControl("scbAccEntryCheckBox") as CheckBox).Checked == true)
                {
                    flag = 1;
                }
            }

            if (flag > 0)
            {
                return false;
            }
            return true;
        }

        protected void Tab5_Activate()
        {
            ClearDisbursedTabDedailSection();
            if (IsPostBack)
            {
                DisbursedLoanListLoad();
            }
        }

        private void ClearDisbursedTabDedailSection()
        {
            disbursedLoanApplicantNameTextBox.BackColor = System.Drawing.Color.White;
            disbursedLoanFatherNameTextBox.BackColor = System.Drawing.Color.White;
            disbursedLoanMotherNameTextBox.BackColor = System.Drawing.Color.White;
            disbursedLoanDobTextBox.BackColor = System.Drawing.Color.White;
            disbursedLoanTinTextBox.BackColor = System.Drawing.Color.White;
            disbursedLoanNameOfCompanyTextBox.BackColor = System.Drawing.Color.White;
            foreach (GridViewRow gvr in disbursedLoanContactNoGridView.Rows)
            {
                gvr.BackColor = System.Drawing.Color.White;
            }
            foreach (GridViewRow gvr in disbursedLoanIdGridView.Rows)
            {
                gvr.BackColor = System.Drawing.Color.White;
            }
            foreach (GridViewRow gvr in disbursedLoanAcForSCBGridView.Rows)
            {
                gvr.BackColor = System.Drawing.Color.White;
            }


            disbursedDedupIdTextBox.Text = "";
            disbursedLoanStatusTextBox.Text = "";
            disbursedLoanApplicantNameTextBox.Text = "";
            disbursedLoanDobTextBox.Text = "";
            disbursedLoanFatherNameTextBox.Text = "";
            disbursedLoanMotherNameTextBox.Text = "";
            disbursedLoanEducationalLevelTextBox.Text = "";
            disbursedLoanMaritalStatusTextBox.Text = "";
            disbursedLoanResidentialAddressTextBox.Text = "";
            disbursedLoanTinTextBox.Text = "";
            disbursedLoanNameOfCompanyTextBox.Text = "";
            disbursedLoanOfficeAddressTextBox.Text = "";
            disbursedLoanIdGridView.DataSource = viewList;
            disbursedLoanIdGridView.DataBind();

            disbursedLoanAcForSCBGridView.DataSource = viewList;
            disbursedLoanAcForSCBGridView.DataBind();

        }

        private void DisbursedLoanListLoad()
        {
            dedupInfoList = new List<DeDupInfo>();
            ParameterCondition();
            DeDupInfoManager deDupInfoManagerObj = new DeDupInfoManager();
            tempDedupInfoList = null;
            fourthTotalRecordLabel.Text = "Total Record : ";
            tempDedupInfoList = deDupInfoManagerObj.GetAllDeDupInfo(name, fatherName, motherName, businessName, dob, contact, id, ac, /*bankBranch,*/ tin, 5);
            #region tarek 29_4_2011
            if (exactNameSearchChckBox.Checked == true)
            {
                List<DeDupInfo> dedupListObj = new List<DeDupInfo>();

                IEnumerable<DeDupInfo> userListExact = from u in tempDedupInfoList
                                                       where u.Name.ToUpper().Trim().Equals(name)
                                                       select u;
                IEnumerable<DeDupInfo> userList = from u in tempDedupInfoList
                                                  where u.Name.ToUpper().Trim().Contains(name) && !u.Name.ToUpper().Trim().Equals(name)
                                                  select u;


                tempDedupInfoList = new List<DeDupInfo>();
                tempDedupInfoList.AddRange(userListExact);

                //REMOVE EXACT NAME FROM userlist TO REMOVE DUPLICATE NAME
                List<DeDupInfo> userListFiltered = new List<DeDupInfo>();
                userListFiltered.AddRange(userList);
                //

                tempDedupInfoList.AddRange(userListFiltered);
            }
            #endregion
            if (tempDedupInfoList != null)
            {
                dedupInfoList = MatchFound(tempDedupInfoList);
                dedupInfoList.Sort(delegate (DeDupInfo d1, DeDupInfo d2) { return d1.MatchFoundNumber.CompareTo(d2.MatchFoundNumber); });
                dedupInfoList.Reverse();
                Session["dedupInfoListS"] = dedupInfoList;
                disbursedLoanGridView.DataSource = dedupInfoList;
                disbursedLoanGridView.DataBind();
                fourthTotalRecordLabel.Text = "Total Record : " + dedupInfoList.Count.ToString();
            }
            else
            {
                return;
            }
        }
        private void SplitUIName(string name)
        {
            int firstIndex = 0;
            string nameSecondPart = "";
            string[] splitName = name.Trim().Split(' ');
            if (splitName.Length > 0)
            {
                foreach (string nameObj in splitName)
                {
                    if (nameObj.Length > 2)
                    {
                        if (firstIndex == 0)
                        {
                            nameFirstPartcondUI = nameObj;
                            firstIndex++;
                        }
                        else
                        {
                            if (splitName.Length > 2)
                            {
                                if (nameObj.Length > 2)
                                {
                                    nameSecondPart += nameObj + " ";
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                nameFirstPartcondUI = name;
            }
            if (nameSecondPart.Length > 0)
            {
                nameSecondPartcondUI = nameSecondPart;
            }
        }
        private List<DeDupInfo> MatchFound(List<DeDupInfo> tempDedupInfoList)
        {
            try
            {
                dedupInfoList = new List<DeDupInfo>();
                name = ReplaseString(applicantNameTextBox0.Text.ToUpper().Trim());
                fatherName = ReplaseString(fatherNameTextBox10.Text.ToUpper().Trim());
                motherName = ReplaseString(motherNameTextBox0.Text.ToUpper().Trim());
                businessName = ReplaseString(businessNameTextBox0.Text.ToUpper().Trim());
                dob = dateOfBirthTextBox0.Text;
                tin = tinNoTextBox.Text;
                contact.Clear();
                id.Clear();
                ac.Clear();
                foreach (GridViewRow gvr in contactNoEntryGridView.Rows)
                {
                    contact.Add((gvr.FindControl("contactNoEntryTextBox") as TextBox).Text);
                }
                foreach (GridViewRow gvr in idEntryGridView.Rows)
                {
                    id.Add((gvr.FindControl("idNoEntryTextBox") as TextBox).Text);
                }
                foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
                {
                    ac.Add((gvr.FindControl("accountNoEntryTextBox") as TextBox).Text);
                }

                foreach (DeDupInfo deDupInfoObj in tempDedupInfoList)
                {
                    List<String> matchedField = new List<String>();

                    List<string> dummyList = new List<string>();
                    List<string> dummyList2 = new List<string>();
                    deDupInfoObj.SplittedContactList = dummyList;
                    deDupInfoObj.SplittedAccountList = dummyList2;
                    if (name != "" && deDupInfoObj.Name != "")
                    {
                        SplitUIName(name);
                        int firstIndex = 0;
                        string nameSecondPart = "";
                        string[] splitName = ReplaseString(deDupInfoObj.Name.Trim().ToUpper()).Split(' ');
                        if (splitName.Length > 0)
                        {
                            foreach (string nameObj in splitName)
                            {
                                if (nameObj.Length > 2)
                                {
                                    if (firstIndex == 0)
                                    {
                                        if (class_Soundx.ToSoundexCode(nameFirstPartcondUI) == class_Soundx.ToSoundexCode(nameObj.ToUpper()) || nameObj.ToUpper().Contains(nameFirstPartcondUI))
                                        {
                                            matchedField.Add("Name_");//0
                                        }
                                        else
                                        {
                                            matchedField.Add("");
                                        }
                                        firstIndex++;
                                    }
                                    else
                                    {
                                        if (splitName.Length >= 2)
                                        {
                                            if (nameObj.Length > 2)
                                            {
                                                nameSecondPart += nameObj + " ";

                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (class_Soundx.ToSoundexCode(name) == class_Soundx.ToSoundexCode(ReplaseString(deDupInfoObj.Name.ToUpper())) || deDupInfoObj.Name.ToUpper().Contains(name))
                            {
                                matchedField.Add("Name_");//0
                            }
                            else
                            {
                                matchedField.Add("");
                            }
                        }
                        if (nameSecondPart.Length > 0)
                        {
                            if (nameSecondPartcondUI.Length > 0)
                            {
                                if (class_Soundx.ToSoundexCode(nameSecondPartcondUI) == class_Soundx.ToSoundexCode(nameSecondPart.Trim().ToUpper()) || nameSecondPart.Trim().ToUpper().Contains(nameSecondPartcondUI))
                                {
                                    matchedField.Add("Name_");//0
                                }
                                else
                                {
                                    matchedField.Add("");
                                }
                            }
                            else
                            {
                                if (class_Soundx.ToSoundexCode(nameFirstPartcondUI) == class_Soundx.ToSoundexCode(nameSecondPart.ToUpper()) || nameSecondPart.ToUpper().Contains(nameFirstPartcondUI))
                                {
                                    matchedField.Add("Name_");//0
                                }
                                else
                                {
                                    matchedField.Add("");
                                }
                            }
                        }


                    }
                    else
                    {
                        matchedField.Add("");
                    }

                    if (fatherName != "" && deDupInfoObj.FatherName != "")
                    {
                        if (class_Soundx.ToSoundexCode(fatherName) == class_Soundx.ToSoundexCode(ReplaseString(deDupInfoObj.FatherName.ToUpper())) || deDupInfoObj.FatherName.ToUpper().Contains(fatherName))
                        {
                            matchedField.Add("FatherName_");//1
                        }
                        else
                        { matchedField.Add(""); }
                    }
                    if (motherName != "" && deDupInfoObj.MotherName != "")
                    {
                        if (class_Soundx.ToSoundexCode(motherName) == class_Soundx.ToSoundexCode(ReplaseString(deDupInfoObj.MotherName.ToUpper())) || deDupInfoObj.MotherName.ToUpper().Contains(motherName))
                        {
                            matchedField.Add("MotherName_");   //2
                        }
                        else
                        { matchedField.Add(""); }
                    }
                    if (businessName != "" && deDupInfoObj.BusinessEmployeeName != "")
                    {
                        if (class_Soundx.ToSoundexCode(businessName) == class_Soundx.ToSoundexCode(ReplaseString(deDupInfoObj.BusinessEmployeeName.ToUpper())) || deDupInfoObj.BusinessEmployeeName.ToUpper().Contains(businessName))
                        {
                            matchedField.Add("BusinessEmployeeName_");//3
                            if (class_Soundx.ToSoundexCode(businessName) == class_Soundx.ToSoundexCode(ReplaseString(deDupInfoObj.Name.ToUpper())) || deDupInfoObj.Name.ToUpper().Contains(businessName))
                            {
                                matchedField.Add("Name_");
                            }
                        }
                        else
                        {
                            if (class_Soundx.ToSoundexCode(businessName) == class_Soundx.ToSoundexCode(ReplaseString(deDupInfoObj.Name.ToUpper())) || deDupInfoObj.Name.ToUpper().Contains(businessName))
                            {
                                matchedField.Add("Name_");
                            }
                            matchedField.Add("");
                        }
                    }
                    else
                    { matchedField.Add(""); }

                    if (dateOfBirthTextBox0.Text != "" && deDupInfoObj.DateOfBirth.ToString("dd-MM-yyyy").Contains(dateOfBirthTextBox0.Text))
                    {
                        matchedField.Add("DateOfBirth_");//4
                    }
                    else
                    { matchedField.Add(""); }
                    if (tinNoTextBox.Text != "" && deDupInfoObj.Tin.Contains(tinNoTextBox.Text))
                    {
                        matchedField.Add("Tin_");//4
                    }
                    else
                    { matchedField.Add(""); }


                    /*----------------------loading contact number--------------------------------------------*/
                    List<string> temporaryContactNumber = new List<string>();
                    List<string> notMatchedContactNumber = new List<string>();
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.ContactNumber.ContactNo1))
                    {
                        temporaryContactNumber.Add(s);
                    }
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.ContactNumber.ContactNo2))
                    {
                        temporaryContactNumber.Add(s);
                    }
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.ContactNumber.ContactNo3))
                    {
                        temporaryContactNumber.Add(s);
                    }
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.ContactNumber.ContactNo4))
                    {
                        temporaryContactNumber.Add(s);
                    }
                    if (temporaryContactNumber.Count > 0)
                    {
                        for (int i = 0; i < temporaryContactNumber.Count; i++)
                        {
                            if (contact[0] != "" && temporaryContactNumber[i].Contains(contact[0]))
                            {
                                matchedField.Add("ContactNo" + (deDupInfoObj.SplittedContactList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedContactList.Add(temporaryContactNumber[i]);
                            }
                            else if (contact[1] != "" && temporaryContactNumber[i].Contains(contact[1]))
                            {
                                matchedField.Add("ContactNo" + (deDupInfoObj.SplittedContactList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedContactList.Add(temporaryContactNumber[i]);
                            }
                            else if (contact[2] != "" && temporaryContactNumber[i].Contains(contact[2]))
                            {
                                matchedField.Add("ContactNo" + (deDupInfoObj.SplittedContactList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedContactList.Add(temporaryContactNumber[i]);
                            }
                            else if (contact[3] != "" && temporaryContactNumber[i].Contains(contact[3]))
                            {
                                matchedField.Add("ContactNo" + (deDupInfoObj.SplittedContactList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedContactList.Add(temporaryContactNumber[i]);
                            }
                            else
                            {
                                notMatchedContactNumber.Add(temporaryContactNumber[i]);
                            }
                        }

                        int count = deDupInfoObj.SplittedContactList.Count;
                        deDupInfoObj.SplittedContactList.InsertRange(count, notMatchedContactNumber);
                    }

                    List<string> temporaryAccountNumber = new List<string>();
                    List<string> notMatchedAccountNumber = new List<string>();
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.AccountNo.PreAccNo1 + deDupInfoObj.AccountNo.MasterAccNo1 + deDupInfoObj.AccountNo.PostAccNo1))
                    {
                        temporaryAccountNumber.Add(s);
                    }
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.AccountNo.PreAccNo2 + deDupInfoObj.AccountNo.MasterAccNo2 + deDupInfoObj.AccountNo.PostAccNo2))
                    {
                        temporaryAccountNumber.Add(s);
                    }
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.AccountNo.PreAccNo3 + deDupInfoObj.AccountNo.MasterAccNo3 + deDupInfoObj.AccountNo.PostAccNo3))
                    {
                        temporaryAccountNumber.Add(s);
                    }
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.AccountNo.PreAccNo4 + deDupInfoObj.AccountNo.MasterAccNo4 + deDupInfoObj.AccountNo.PostAccNo4))
                    {
                        temporaryAccountNumber.Add(s);
                    }
                    foreach (string s in this.CommaSplitterMethod(deDupInfoObj.AccountNo.PreAccNo5 + deDupInfoObj.AccountNo.MasterAccNo5 + deDupInfoObj.AccountNo.PostAccNo5))
                    {
                        temporaryAccountNumber.Add(s);
                    }
                    if (temporaryAccountNumber.Count > 0)
                    {
                        for (int i = 0; i < temporaryAccountNumber.Count; i++)
                        {
                            if (ac[0] != "" && temporaryAccountNumber[i].Contains(ac[0]))
                            {
                                matchedField.Add("Ac" + (deDupInfoObj.SplittedAccountList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedAccountList.Add(temporaryAccountNumber[i]);
                            }
                            else if (ac[1] != "" && temporaryAccountNumber[i].Contains(ac[1]))
                            {
                                matchedField.Add("Ac" + (deDupInfoObj.SplittedAccountList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedAccountList.Add(temporaryAccountNumber[i]);
                            }
                            else if (ac[2] != "" && temporaryAccountNumber[i].Contains(ac[2]))
                            {
                                matchedField.Add("Ac" + (deDupInfoObj.SplittedAccountList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedAccountList.Add(temporaryAccountNumber[i]);
                            }
                            else if (ac[3] != "" && temporaryAccountNumber[i].Contains(ac[3]))
                            {
                                matchedField.Add("Ac" + (deDupInfoObj.SplittedAccountList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedAccountList.Add(temporaryAccountNumber[i]);
                            }
                            else if (ac[4] != "" && temporaryAccountNumber[i].Contains(ac[4]))
                            {
                                matchedField.Add("Ac" + (deDupInfoObj.SplittedAccountList.Count + 1) + "_");//5
                                deDupInfoObj.SplittedAccountList.Add(temporaryAccountNumber[i]);
                            }
                            else
                            {
                                notMatchedAccountNumber.Add(temporaryAccountNumber[i]);
                            }
                        }

                        int count = deDupInfoObj.SplittedAccountList.Count;
                        deDupInfoObj.SplittedAccountList.InsertRange(count, notMatchedAccountNumber);
                    }/*-------------------------------END-----------------------------------*/



                    if (id[0] != "" && deDupInfoObj.ApplicantId.Id1.Contains(id[0]))
                    {
                        matchedField.Add("Id1_");//9
                    }
                    else
                    { matchedField.Add(""); }
                    if (id[1] != "" && deDupInfoObj.ApplicantId.Id2.Contains(id[1]))
                    {
                        matchedField.Add("Id2_");//10
                    }
                    else
                    { matchedField.Add(""); }
                    if (id[2] != "" && deDupInfoObj.ApplicantId.Id3.Contains(id[2]))
                    {
                        matchedField.Add("Id3_");//11
                    }
                    else
                    { matchedField.Add(""); }
                    if (id[3] != "" && deDupInfoObj.ApplicantId.Id4.Contains(id[3]))
                    {
                        matchedField.Add("Id4_");//12
                    }
                    else
                    { matchedField.Add(""); }
                    if (id[4] != "" && deDupInfoObj.ApplicantId.Id5.Contains(id[4]))
                    {
                        matchedField.Add("Id5_");//13
                    }
                    else
                    { matchedField.Add(""); }


                    deDupInfoObj.MatchFound = matchedField;


                    deDupInfoObj.MatchFoundNumber = 0;
                    foreach (string s in matchedField.Distinct().ToList())
                    {
                        if (s.Length > 0)
                            deDupInfoObj.MatchFoundNumber++;
                    }


                    dedupInfoList.Add(deDupInfoObj);
                }
                dedupInfoList = dedupInfoList.FindAll(findZeroMatch => findZeroMatch.MatchFoundNumber != 0);

                string t = (string)System.Web.HttpContext.Current.Session["UserId"];

                return dedupInfoList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void disbursedLoanGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearDisbursedTabDedailSection();
            FillDisbursedTextBox(disbursedLoanGridView.SelectedRow.RowIndex);
        }

        private void FillDisbursedTextBox(int index)
        {
            DeDupInfo dedupObj = new DeDupInfo();
            dedupInfoList = (List<DeDupInfo>)Session["dedupInfoListS"];
            try
            {
                dedupObj = dedupInfoList[index + disbursedLoanGridView.PageIndex * disbursedLoanGridView.PageSize];
            }
            catch
            {
            }
            Session["dedupSelectedObjS"] = dedupObj;

            disbursedDedupIdTextBox.Text = dedupObj.Id.ToString();
            disbursedLoanStatusTextBox.Text = dedupObj.Status;
            disbursedLoanApplicantNameTextBox.Text = dedupObj.Name;
            if (DateFormat.IsDate(dedupObj.DateOfBirth))
            {
                disbursedLoanDobTextBox.Text = dedupObj.DateOfBirth.ToString("dd-MM-yyyy");
            }
            else
            {
                disbursedLoanDobTextBox.Text = "";
            }

            disbursedLoanFatherNameTextBox.Text = dedupObj.FatherName;
            disbursedLoanMotherNameTextBox.Text = dedupObj.MotherName;
            disbursedLoanEducationalLevelTextBox.Text = dedupObj.EducationalLevel;
            disbursedLoanMaritalStatusTextBox.Text = dedupObj.MaritalStatus;
            disbursedLoanResidentialAddressTextBox.Text = dedupObj.Address1 + (string.IsNullOrEmpty(dedupObj.Address2) ? "" : " - " + dedupObj.Address2);
            disbursedLoanResidentialStatusTextBox.Text = dedupObj.ResidentaileStatus;
            disbursedLoanTinTextBox.Text = dedupObj.Tin;
            disbursedLoanNameOfCompanyTextBox.Text = dedupObj.BusinessEmployeeName;
            disbursedLoanOfficeAddressTextBox.Text = dedupObj.Address2;


            try      // because if he have only 2 number then list dont have [3]/[4]....
            {
                if (dedupObj.SplittedContactList.Count > 0)
                    disbursedLoanContactNoGridView.Rows[0].Cells[0].Text = dedupObj.SplittedContactList[0];
                if (dedupObj.SplittedContactList.Count > 1)
                    disbursedLoanContactNoGridView.Rows[1].Cells[0].Text = dedupObj.SplittedContactList[1];
                if (dedupObj.SplittedContactList.Count > 2)
                    disbursedLoanContactNoGridView.Rows[2].Cells[0].Text = dedupObj.SplittedContactList[2];
                if (dedupObj.SplittedContactList.Count > 3)
                    disbursedLoanContactNoGridView.Rows[3].Cells[0].Text = dedupObj.SplittedContactList[3];
                if (dedupObj.SplittedContactList.Count > 4)
                    disbursedLoanContactNoGridView.Rows[4].Cells[0].Text = dedupObj.SplittedContactList[4];
            }
            catch { }

            //ID Gridview
            disbursedLoanIdGridView.Rows[0].Cells[0].Text = dedupObj.ApplicantId.Type1.ToString();
            disbursedLoanIdGridView.Rows[0].Cells[1].Text = dedupObj.ApplicantId.Id1.ToString();
            disbursedLoanIdGridView.Rows[1].Cells[0].Text = dedupObj.ApplicantId.Type2.ToString();
            disbursedLoanIdGridView.Rows[1].Cells[1].Text = dedupObj.ApplicantId.Id2.ToString();
            disbursedLoanIdGridView.Rows[2].Cells[0].Text = dedupObj.ApplicantId.Type3.ToString();
            disbursedLoanIdGridView.Rows[2].Cells[1].Text = dedupObj.ApplicantId.Id3.ToString();
            disbursedLoanIdGridView.Rows[3].Cells[0].Text = dedupObj.ApplicantId.Type4.ToString();
            disbursedLoanIdGridView.Rows[3].Cells[1].Text = dedupObj.ApplicantId.Id4.ToString();
            disbursedLoanIdGridView.Rows[4].Cells[0].Text = dedupObj.ApplicantId.Type5.ToString();
            disbursedLoanIdGridView.Rows[4].Cells[1].Text = dedupObj.ApplicantId.Id5.ToString();

            if (dedupObj.AccountNo.MasterAccNo1.Length != 16)
            {
                if (!dedupObj.AccountNo.MasterAccNo1.Contains(","))
                {
                    if (dedupObj.AccountNo.PreAccNo1 != null)
                    {
                        disbursedLoanAcForSCBGridView.Rows[0].Cells[0].Text = dedupObj.AccountNo.PreAccNo1.ToString();
                    }
                    else
                    {
                        disbursedLoanAcForSCBGridView.Rows[0].Cells[0].Text = "";
                    }
                    if (dedupObj.AccountNo.MasterAccNo1 != null)
                    {
                        disbursedLoanAcForSCBGridView.Rows[0].Cells[1].Text = dedupObj.AccountNo.MasterAccNo1.ToString();
                    }
                    else
                    {
                        disbursedLoanAcForSCBGridView.Rows[0].Cells[1].Text = "";
                    }
                    if (dedupObj.AccountNo.PostAccNo1 != null)
                    {
                        disbursedLoanAcForSCBGridView.Rows[0].Cells[2].Text = dedupObj.AccountNo.PostAccNo1.ToString();
                    }
                    else
                    {
                        disbursedLoanAcForSCBGridView.Rows[0].Cells[2].Text = "";
                    }
                    disbursedLoanCreditCardTextBox.Text = "";
                }
                else
                {
                    for (int i = 0; i < dedupObj.SplittedAccountList.Count; i++)
                    {
                        if (dedupObj.SplittedAccountList[i].Length == 11)
                        {
                            disbursedLoanAcForSCBGridView.Rows[i].Cells[0].Text = dedupObj.SplittedAccountList[i].Substring(0, 2).ToString();
                            disbursedLoanAcForSCBGridView.Rows[i].Cells[1].Text = dedupObj.SplittedAccountList[i].Substring(2, 7).ToString();
                            disbursedLoanAcForSCBGridView.Rows[i].Cells[2].Text = dedupObj.SplittedAccountList[i].Substring(9, 2).ToString();
                            disbursedLoanCreditCardTextBox.Text = "";
                        }
                        else if (dedupObj.SplittedAccountList[i].Length < 11)
                        {
                            disbursedLoanAcForSCBGridView.Rows[i].Cells[0].Text = "";
                            disbursedLoanAcForSCBGridView.Rows[i].Cells[1].Text = dedupObj.SplittedAccountList[i].ToString();
                            disbursedLoanAcForSCBGridView.Rows[i].Cells[2].Text = "";
                            disbursedLoanCreditCardTextBox.Text = "";
                        }
                    }
                }
            }
            else
            {
                if (dedupObj.AccountNo.MasterAccNo1 != null)
                {
                    disbursedLoanCreditCardTextBox.Text = dedupObj.AccountNo.MasterAccNo1.ToString();
                }
            }

            if (dedupObj.DeclineDate.ToString("dd-MM-yyyy") == "01-01-0001")
            {
                disbursedLoanDeclineDateTextBox.Text = "";
            }
            else
            {
                disbursedLoanDeclineDateTextBox.Text = dedupObj.DeclineDate.ToString("dd-MM-yyyy");
            }

            disbursedLoanDeclineReasonTextBox.Text = dedupObj.DeclineReason.ToString() + " " + dedupObj.Remarks.ToString();
            negativeListPersonDeclineReasonTextBox.Text = dedupObj.DeclineReason.ToString();
            if (dedupObj.ProductId > 0)
            {
                productManagerObj = new ProductManager();
                disbursedLoanProductNameTextBox.Text = productManagerObj.GetProductName(dedupObj.ProductId);
            }
            else
            {
                disbursedLoanProductNameTextBox.Text = "";
            }

            foreach (string s in dedupObj.MatchFound)
            {
                if (s == "Name_")
                {
                    disbursedLoanApplicantNameTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("FatherName_"))
                {
                    disbursedLoanFatherNameTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("MotherName_"))
                {
                    disbursedLoanMotherNameTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("BusinessEmployeeName_"))
                {
                    disbursedLoanNameOfCompanyTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("DateOfBirth_"))
                {
                    disbursedLoanDobTextBox.BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("ContactNo1_"))
                {
                    disbursedLoanContactNoGridView.Rows[0].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("ContactNo2_"))
                {
                    disbursedLoanContactNoGridView.Rows[1].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("ContactNo3_"))
                {
                    disbursedLoanContactNoGridView.Rows[2].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("ContactNo4_"))
                {
                    disbursedLoanContactNoGridView.Rows[3].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id1_"))
                {
                    disbursedLoanIdGridView.Rows[0].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id2_"))
                {
                    disbursedLoanIdGridView.Rows[1].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id3_"))
                {
                    disbursedLoanIdGridView.Rows[2].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id4_"))
                {
                    disbursedLoanIdGridView.Rows[3].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Id5_"))
                {
                    disbursedLoanIdGridView.Rows[4].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac1_"))
                {
                    disbursedLoanAcForSCBGridView.Rows[0].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac2_"))
                {
                    disbursedLoanAcForSCBGridView.Rows[1].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac3_"))
                {
                    disbursedLoanAcForSCBGridView.Rows[2].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac4_"))
                {
                    disbursedLoanAcForSCBGridView.Rows[3].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Ac5_"))
                {
                    disbursedLoanAcForSCBGridView.Rows[4].BackColor = System.Drawing.Color.Red;
                }
                if (s.Contains("Tin_"))
                {
                    disbursedLoanTinTextBox.BackColor = System.Drawing.Color.Red;
                }
            }

        }



        protected void disbursedLoanGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onclick",
                    ClientScript.GetPostBackEventReference(disbursedLoanGridView, "Select$" +
                                                                                  e.Row.RowIndex.ToString()));
            }
        }

        protected void declineButton_Click(object sender, EventArgs e)
        {
            #region Auto Loan Decline

            try
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                var masterId = autoLoanAssessmentService.CheckLLIDIsAuto(loanLocatorIdTextBox.Text);
                if (masterId > 0)
                {

                    AutoPanel.Visible = true;
                    plussPanel.Visible = false;
                    //pluss1stPanel.Visible = false;

                    return;
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Auto Loan Decline");
            }


            #endregion Auto loan Decline


            string targetURL = null;
            if (loanLocatorIdTextBox.Text != "")
            {
                targetURL = "DeclineUI.aspx?lLId=" + loanLocatorIdTextBox.Text.Trim();

                int llId = Convert.ToInt32(loanLocatorIdTextBox.Text.Trim());

                LoanInformation loanInformation = loanApplicationManagerObj.GetLoanInformationCreate(llId);

                if (loanInformation == null)
                {
                    modalPopup.ShowWarning("No Data Found with LLID : " + loanLocatorIdTextBox.Text, Title);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(declineButton, this.GetType(), "declineButton", "<script type='text/javascript'>window.open('"+targetURL+"','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1')</script>", false);
                }
            }
            else
            {
                Alert.Show("Please input LLId");
                modalPopup.ShowError("Please input LLID", Title);
                loanLocatorIdTextBox.Focus();
            }
        }

        private void SendData(string remark)
        {

            var All = new Object();
            var statePermission = (List<AutoUserTypeVsWorkflow>)Session["AutoWorkFlowPermission"];

            var llId = Convert.ToInt32(loanLocatorIdTextBox.Text.Trim());
            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            All = autoLoanAssessmentService.getLoanLocetorInfoByLLID(llId, statePermission);
            AutoLoanApplicationAll AutoAll = new AutoLoanApplicationAll();
            AutoAll = (AutoLoanApplicationAll)All;



            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            DeDupInfo deDupInfoObj = new DeDupInfo();
            string masterAccNo = "";
            deDupInfoObj.Name = AutoAll.objAutoPRApplicant.PrName;
            if (!String.IsNullOrEmpty(Convert.ToString(AutoAll.objAutoPRApplicant.DateofBirth)))
            {
                try
                {
                    deDupInfoObj.DateOfBirth =
                        Convert.ToDateTime(Convert.ToString(AutoAll.objAutoPRApplicant.DateofBirth),
                            new System.Globalization.CultureInfo("ru-RU"));
                }
                catch (Exception exception)
                {
                    CustomException.Save(exception, "Send Data");
                }
            }
            deDupInfoObj.FatherName = AutoAll.objAutoPRApplicant.FathersName;
            deDupInfoObj.MotherName = AutoAll.objAutoPRApplicant.MothersName;
            deDupInfoObj.EducationalLevel = AutoAll.objAutoPRApplicant.HighestEducation;
            deDupInfoObj.MaritalStatus = AutoAll.objAutoPRApplicant.MaritalStatus;
            deDupInfoObj.ResidentaileStatus = AutoAll.objAutoPRApplicant.ResidenceStatus;
            deDupInfoObj.Address2 = AutoAll.objAutoPRApplicant.PermanentAddress;
            if (!String.IsNullOrEmpty(AutoAll.objAutoPRApplicant.TINNumber))
            {
                deDupInfoObj.Tin = AutoAll.objAutoPRApplicant.TINNumber;
            }

            if ((AutoAll.objAutoPRProfession) != null)
            {
                deDupInfoObj.BusinessEmployeeName = AutoAll.objAutoPRProfession.NameofOrganization;
                deDupInfoObj.Address1 = AutoAll.objAutoPRProfession.Address;
                deDupInfoObj.Phone1 = AutoAll.objAutoPRProfession.OfficePhone;
            }

            deDupInfoObj.Phone2 = AutoAll.objAutoPRApplicant.PhoneResidence;
            deDupInfoObj.Mobile1 = AutoAll.objAutoPRApplicant.Mobile;
            deDupInfoObj.Mobile2 = AutoAll.objAutoPRApplicant.SpouseContactNumber;

            if ((AutoAll.AutoApplicantSCBAccountList) != null)
            {

                for (int i = 0; i < AutoAll.AutoApplicantSCBAccountList.Count; i++)
                {
                    masterAccNo += AutoAll.AutoApplicantSCBAccountList[i].AccountNumberSCB;
                    masterAccNo += ",";

                }
            }
            if (masterAccNo != "")
            {
                if (masterAccNo.Length > 1)
                {
                    masterAccNo = masterAccNo.Substring(0, masterAccNo.Length - 1);
                }
            }
            deDupInfoObj.ScbMaterNo = masterAccNo.ToString();

            deDupInfoObj.IdType = AutoAll.objAutoPRApplicant.IdentificationNumberType;
            deDupInfoObj.IdNo = AutoAll.objAutoPRApplicant.IdentificationNumber;
            deDupInfoObj.IdType1 = "";
            deDupInfoObj.IdNo1 = "";
            deDupInfoObj.IdType2 = "";
            deDupInfoObj.IdNo2 = "";
            deDupInfoObj.IdType3 = "";
            deDupInfoObj.IdNo3 = "";
            deDupInfoObj.IdType4 = "";
            deDupInfoObj.IdNo4 = "";
            deDupInfoObj.ProductId = AutoAll.objAutoLoanMaster.ProductId;
            deDupInfoObj.Source = AutoAll.objAutoLoanMaster.SourceName;
            if (AutoAll.AutoLoanAnalystApplication != null)
            {
                deDupInfoObj.BankBranch = AutoAll.AutoLoanAnalystApplication.BranchName;
            }

            deDupInfoObj.DeclineDate = DateTime.Now; //Convert.ToDateTime(declineDateTextBox.Text.Trim());
            deDupInfoObj.DeclineReason = remark;
            deDupInfoObj.Remarks = remark;
            deDupInfoObj.Status = ddStatus.SelectedValue;
            deDupInfoObj.Active = "Y";
            deDupInfoObj.LoanAccountNo = Convert.ToString(llId);
            deDupInfoObj.CriteriaType = "D";
            deDupInfoObj.EntryDate = DateTime.Now;
            deDupInfoObjList.Add(deDupInfoObj);

            int ListInsert = 0;
            int countCheckedCheckBox = 0;

            var deDupInfoManagerObj = new DeDupInfoManager();
            ListInsert = deDupInfoManagerObj.SendDedupInfoInToDB(deDupInfoObjList);

            //if (ListInsert == INSERTE || ListInsert == UPDATE)
            //{
            //    UpdateStatus();
            //}
        }



        protected void deferButton_Click(object sender, EventArgs e)
        {
            #region Auto Loan Decline

            AutoLoanAssessmentRrepository repository = new AutoLoanDataService.AutoLoanAssessmentDataService();
            var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
            var masterId = autoLoanAssessmentService.CheckLLIDIsAuto(loanLocatorIdTextBox.Text);
            if (masterId > 0)
            {
                var userID = Convert.ToInt32(Session["Id"].ToString());
                var UserType = Convert.ToInt32(Session["UserType"].ToString());

                AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                objAutoStatusHistory.AutoLoanMasterId = masterId;
                objAutoStatusHistory.StatusID = 6;
                objAutoStatusHistory.UserID = userID;


                var res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
                var msg = "";
                if (res == "Success")
                {
                    msg = "Defer Success";
                    ScriptManager.RegisterStartupScript(declineButton, this.GetType(), "declineButton", "alert('Defer Success');", true);

                }
                else
                {
                    msg = "Defer Failed";
                    ScriptManager.RegisterStartupScript(declineButton, this.GetType(), "declineButton", "alert('Defer Failed');", true);
                }


                return;
            }



            #endregion Auto loan Decline
           // ScriptManager.RegisterStartupScript(deferButton, this.GetType(), "deferButton", "ShowNewItemPopUp('defareFrame',pg);", true);
        }
        private void Defer()
        {
            if (String.IsNullOrEmpty(loanLocatorIdTextBox.Text.Trim()))
            {
                alert = "Please enter LLID.";
                loanLocatorIdTextBox.Focus();
            }
            else if (IsLoanLocatorUserEmpty())
            {
                alert = "Pluss and Loan Locator user Id no match!";
                return;
            }
            else
            {
                int countCheckedCheckBox = 0;
                foreach (GridViewRow gvr in remarksGridView.Rows)
                {
                    if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked)
                    {
                        countCheckedCheckBox++;
                        break;
                    }
                }
                if (countCheckedCheckBox > 0)
                {
                    int llid = Convert.ToInt32(loanLocatorIdTextBox.Text);
                    int deferFlag = (int)ForwardOrDeferFlag.DEFERID;
                    int updateFlag = loanApplicationManagerObj.SetForwardStatus(llid, deferFlag, (string)Session["UserId"]);
                    if (updateFlag > -3)
                    {
                        int deleteFlag = loanApplicationManagerObj.DeleteDeferReason(llid);
                        if (!String.IsNullOrEmpty(deleteFlag.ToString()))
                        {
                            List<Remarks> remarksListObj = new List<Remarks>();
                            List<Int32> remarksIdListObj = new List<Int32>();
                            foreach (GridViewRow gvr in remarksGridView.Rows)
                            {
                                if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked)
                                {
                                    Remarks remarksObj = new Remarks();
                                    Int32 remarksIdObj;
                                    remarksIdObj = Convert.ToInt32((gvr.FindControl("idLabel") as Label).Text);
                                    remarksIdListObj.Add(remarksIdObj);
                                    remarksObj.Id = remarksIdObj;
                                    remarksObj.Name = (gvr.FindControl("remarksTextBox") as TextBox).Text;
                                    remarksListObj.Add(remarksObj);
                                }
                            }
                            int insertFlag = loanApplicationManagerObj.InsertDeferReason(llid, remarksIdListObj);
                            if (insertFlag > -1)
                            {
                                LoanApplicationDetails loanApplicationDetailsObj = new LoanApplicationDetails();
                                loanApplicationDetailsObj.LAId = llid;

                                loanApplicationDetailsObj.PersonId = (string)Session["UserId"];
                                loanApplicationDetailsObj.DeptId = 4;
                                loanApplicationDetailsObj.ProcessStatusId = 6;

                                int insertLoanAppDetailsFlag = loanApplicationManagerObj.InsertDeferReasonDetails(loanApplicationDetailsObj, remarksListObj);
                                if (insertLoanAppDetailsFlag.Equals((int)InsertUpdateFlag.INSERT))
                                {
                                    alert = "Defer sucessfully.";
                                    loanLocatorIdTextBox.Focus();
                                    foreach (GridViewRow gvr in remarksGridView.Rows)
                                    {
                                        (gvr.FindControl("IdCheckBox") as CheckBox).Checked = false;
                                    }
                                }
                                else if (insertLoanAppDetailsFlag.Equals((int)InsertUpdateFlag.INSERTERROR))
                                {
                                    alert = "Defer not sucessfully.";
                                    loanLocatorIdTextBox.Focus();
                                }
                            }
                        }
                    }
                    else
                    {
                        alert = "Defer not sucessfully.";
                        loanLocatorIdTextBox.Focus();
                    }
                }
                else
                {
                    alert = "Please select remarks.";

                }
            }
        }

        private bool IsLoanLocatorUserEmpty()
        {
            bool returnValue = true;
            string loanLocatorUserId = "";
            string userId = (string)Session["UserId"];
            DeDupInfoManager deDupInfoManagerObj = new DeDupInfoManager();
            loanLocatorUserId = deDupInfoManagerObj.GetLoanLocatorUserId(userId);
            if (loanLocatorUserId.Length > 0)
            {
                returnValue = false;
            }
            return returnValue;
        }

        protected void applicantNameCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (applicantNameCheckbox.Checked)
            {
                Session["applicantName"] = applicantNameTextBox0.Text;
            }
            else Session["applicantName"] = "";

        }
        protected void fatherNameCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (fatherNameCheckBox.Checked)
            {
                Session["fatherName"] = fatherNameTextBox10.Text;
            }
            else Session["fatherName"] = "";
        }
        protected void motherNameCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (motherNameCheckbox.Checked)
            {
                Session["motherName"] = motherNameTextBox0.Text;
            }
            else Session["motherName"] = "";
        }
        protected void businessNameCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (businessNameCheckbox.Checked)
            {
                Session["organization"] = businessNameTextBox0.Text;
            }
            else Session["organization"] = "";
        }
        protected void selectAllCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (selectAllCheckBox.Checked)
            {
                CheckUncheckAllCheckBox(true);
            }
            else
            {
                CheckUncheckAllCheckBox(false);
            }
        }

        private void CheckUncheckAllCheckBox(bool checkUncheckValue)
        {
            if (String.IsNullOrEmpty(applicantNameTextBox0.Text))
            {
                applicantNameCheckbox.Checked = false;
                Session["applicantName"] = "";
            }
            else
            {
                applicantNameCheckbox.Checked = checkUncheckValue;
                if (applicantNameCheckbox.Checked == true)
                {
                    Session["applicantName"] = applicantNameTextBox0.Text;
                }
                else
                {
                    Session["applicantName"] = "";
                }

            }
            if (String.IsNullOrEmpty(fatherNameTextBox10.Text))
            {
                fatherNameCheckBox.Checked = false;
                Session["fatherName"] = "";
            }
            else
            {
                fatherNameCheckBox.Checked = checkUncheckValue;
                if (fatherNameCheckBox.Checked == true)
                {
                    Session["fatherName"] = fatherNameTextBox10.Text;
                }
                else
                {
                    Session["fatherName"] = "";
                }
            }
            if (String.IsNullOrEmpty(motherNameTextBox0.Text))
            {
                motherNameCheckbox.Checked = false;
                Session["motherName"] = "";
            }
            else
            {
                motherNameCheckbox.Checked = checkUncheckValue;
                if (motherNameCheckbox.Checked == true)
                {
                    Session["motherName"] = motherNameTextBox0.Text;
                }
                else
                {
                    Session["motherName"] = "";
                }
            }
            if (String.IsNullOrEmpty(dateOfBirthTextBox0.Text))
            {
                dateOfBirthCheckBox0.Checked = false;
            }
            else
            {
                dateOfBirthCheckBox0.Checked = checkUncheckValue;
            }
            if (String.IsNullOrEmpty(businessNameTextBox0.Text))
            {
                businessNameCheckbox.Checked = false;
                Session["organization"] = "";
            }
            else
            {
                businessNameCheckbox.Checked = checkUncheckValue;
                if (businessNameCheckbox.Checked == true)
                {
                    Session["organization"] = businessNameTextBox0.Text;
                }
                else
                {
                    Session["organization"] = "";
                }
            }
            foreach (GridViewRow gvr in contactNoEntryGridView.Rows)
            {
                if (String.IsNullOrEmpty((gvr.FindControl("contactNoEntryTextBox") as TextBox).Text))
                {
                    (gvr.FindControl("contactNoEntryCheckBox") as CheckBox).Checked = false;
                }
                else
                {
                    (gvr.FindControl("contactNoEntryCheckBox") as CheckBox).Checked = checkUncheckValue;
                }
            }
            foreach (GridViewRow gvr in idEntryGridView.Rows)
            {
                if (String.IsNullOrEmpty((gvr.FindControl("idNoEntryTextBox") as TextBox).Text))
                {
                    (gvr.FindControl("idEntryCheckBox") as CheckBox).Checked = false;
                }
                else
                {
                    (gvr.FindControl("idEntryCheckBox") as CheckBox).Checked = checkUncheckValue;
                }
            }
            string accountNo = "";
            foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
            {

                if (String.IsNullOrEmpty((gvr.FindControl("accountNoEntryTextBox") as TextBox).Text))
                {
                    (gvr.FindControl("scbAccEntryCheckBox") as CheckBox).Checked = false;
                    accountNo += "";
                }
                else
                {
                    (gvr.FindControl("scbAccEntryCheckBox") as CheckBox).Checked = checkUncheckValue;
                    accountNo += (gvr.FindControl("accountNoEntryTextBox") as TextBox).Text + ",";
                }
            }
            if (accountNo != "")
            {
                if (accountNo.Length > 1)
                {
                    Session["accountNo"] = accountNo.Substring(0, accountNo.Length - 1);
                }
                else
                {
                    Session["accountNo"] = "";
                }
            }
            else
            {
                Session["accountNo"] = "";
            }
            if (String.IsNullOrEmpty(tinNoTextBox.Text))
            {
                tinNoCheckBox.Checked = false;
            }
            else
            {
                tinNoCheckBox.Checked = checkUncheckValue;
            }
        }

        private void ClearNegativeEmployer()
        {

        }
        protected void negativeListPersonGridView_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex >= 0)
            {
                negativeListPersonGridView.PageIndex = e.NewPageIndex;
                List<DeDupInfo> dummyDedupInfoList = new List<DeDupInfo>();
                dummyDedupInfoList = (List<DeDupInfo>)Session["dedupInfoListS"];
                negativeListPersonGridView.DataSource = dummyDedupInfoList;
                negativeListPersonGridView.DataBind();
            }
        }
        protected void printButton_Click(object sender, EventArgs e)
        {
            //ScriptManager.RegisterClientScriptBlock(printButton, this.GetType(), "printButton", "<script type='text/javascript'>window.open('DedupResultPrint.aspx','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=1024,height=768')</script>", false);
        }
        protected void scbAccEntryCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            string accountNo = "";
            foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
            {

                if ((gvr.FindControl("scbAccEntryCheckBox") as CheckBox).Checked == true)
                {

                    accountNo += (gvr.FindControl("accountNoEntryTextBox") as TextBox).Text + ",";
                }
                else
                {
                    accountNo += "";
                }
            }
            if (accountNo != "")
            {
                if (accountNo.Length > 1)
                {
                    Session["accountNo"] = accountNo.Substring(0, accountNo.Length - 1);
                }
                else
                {
                    Session["accountNo"] = "";
                }
            }
            else
            {
                Session["accountNo"] = "";
            }


        }
        protected void loanApplicantGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex >= 0)
            {
                loanApplicantGridView.PageIndex = e.NewPageIndex;
                List<LoanLocatorLoanApplicant> loanLocatorLoanApplicantObjList = (List<LoanLocatorLoanApplicant>)Session["LoanLocatorApplicantData"];
                loanApplicantGridView.DataSource = loanLocatorLoanApplicantObjList;
                loanApplicantGridView.DataBind();
            }

        }
        protected void cibResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex >= 0)
            {
                cibResultGridView.PageIndex = e.NewPageIndex;
                List<CIBResult> cIBResultObjList = (List<CIBResult>)Session["CIBResultData"];
                cibResultGridView.DataSource = cIBResultObjList;
                cibResultGridView.DataBind();
            }
        }
        protected void disbursedLoanGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex >= 0)
            {
                disbursedLoanGridView.PageIndex = e.NewPageIndex;
                List<DeDupInfo> dummyDedupInfoList = new List<DeDupInfo>();
                dummyDedupInfoList = (List<DeDupInfo>)Session["dedupInfoListS"];
                disbursedLoanGridView.DataSource = dummyDedupInfoList;
                disbursedLoanGridView.DataBind();
            }
        }
        protected void oKButton_Click(object sender, EventArgs e)
        {
            Defer();
            ScriptManager.RegisterStartupScript(oKButton, this.GetType(), "oKButton", "alert('" + alert + "');closeDialog('defareFrame');", true);
        }
        protected void cancleButton_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(cancleButton, this.GetType(), "cancleButton", "closeDialog('defareFrame');", true);
        }
        protected void View1_Activate()
        {
            try
            {
                ParameterCondition();
                List<LoanLocatorLoanApplicant> loanLocatorLoanApplicantObjList = loanLocatorLoanApplicantManagerObj.GetLoanLocatorLoanApplicantList(name, motherName, ac, businessName);
                Session["LoanLocatorApplicantData"] = loanLocatorLoanApplicantObjList;

                //if (exactNameSearchChckBox.Checked == true)
                //{
                //    IEnumerable<LoanLocatorLoanApplicant> userListExact = from u in loanLocatorLoanApplicantObjList
                //                                                          where u.ApplicantName.ToUpper().Trim().Equals(name)
                //                                                          select u;
                //    IEnumerable<LoanLocatorLoanApplicant> userList = from u in loanLocatorLoanApplicantObjList
                //                                                     where u.ApplicantName.ToUpper().Trim().Contains(name) && !u.ApplicantName.ToUpper().Trim().Equals(name)
                //                                                     select u;


                //    loanLocatorLoanApplicantObjList = new List<LoanLocatorLoanApplicant>();
                //    loanLocatorLoanApplicantObjList.AddRange(userListExact);

                //    //REMOVE EXACT NAME FROM userlist TO REMOVE DUPLICATE NAME
                //    List<LoanLocatorLoanApplicant> userListFiltered = new List<LoanLocatorLoanApplicant>();
                //    userListFiltered.AddRange(userList);
                //    //

                //    loanLocatorLoanApplicantObjList.AddRange(userListFiltered);
                //}

                loanApplicantGridView.DataSource = loanLocatorLoanApplicantObjList;
                loanApplicantGridView.DataBind();
                if (loanApplicantGridView.Rows.Count > 0)
                {
                    thirdTotalRecorLabel.Text = "Total Record : " + loanLocatorLoanApplicantObjList.Count.ToString();
                }
            }
            catch
            {
            }
        }
        private void ParameterCondition()
        {
            if (applicantNameCheckbox.Checked == true)
            {
                name = ReplaseString(applicantNameTextBox0.Text);
            }
            if (fatherNameCheckBox.Checked == true)
            {
                fatherName = ReplaseString(fatherNameTextBox10.Text);
            }
            if (motherNameCheckbox.Checked == true)
            {
                motherName = ReplaseString(motherNameTextBox0.Text);
            }
            if (businessNameCheckbox.Checked == true)
            {
                businessName = ReplaseString(businessNameTextBox0.Text);
            }
            if (dateOfBirthCheckBox0.Checked == true)
            {
                dob = dateOfBirthTextBox0.Text;
            }
            if (tinNoCheckBox.Checked == true)
            {
                tin = tinNoTextBox.Text;
            }
            foreach (GridViewRow gvr in contactNoEntryGridView.Rows)
            {
                if ((gvr.FindControl("contactNoEntryCheckBox") as CheckBox).Checked == true)
                {
                    contact.Add((gvr.FindControl("contactNoEntryTextBox") as TextBox).Text);
                }
            }
            foreach (GridViewRow gvr in idEntryGridView.Rows)
            {
                if ((gvr.FindControl("idEntryCheckBox") as CheckBox).Checked == true)
                {
                    id.Add((gvr.FindControl("idNoEntryTextBox") as TextBox).Text);
                }
            }
            foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
            {
                if ((gvr.FindControl("scbAccEntryCheckBox") as CheckBox).Checked == true)
                {
                    ac.Add((gvr.FindControl("accountNoEntryTextBox") as TextBox).Text);
                }
            }
        }
        protected void Tab4_Activate()
        {
            ParameterCondition();
            List<CIBResult> cIBResultObjList = cIBResultManagerObj.GetCIBResultList(name, fatherName, motherName, businessName);
            Session["CIBResultData"] = cIBResultObjList;
            #region tarek 26_04_2011
            if (exactNameSearchChckBox.Checked == true)
            {
                List<CIBResult> dedupListObj = new List<CIBResult>();

                IEnumerable<CIBResult> userListExact = from u in cIBResultObjList
                                                       where u.CustomerName.ToUpper().Trim().Equals(name)
                                                       select u;
                IEnumerable<CIBResult> userList = from u in cIBResultObjList
                                                  where u.CustomerName.ToUpper().Trim().Contains(name) && !u.CustomerName.ToUpper().Trim().Equals(name)
                                                  select u;


                cIBResultObjList = new List<CIBResult>();
                cIBResultObjList.AddRange(userListExact);

                //REMOVE EXACT NAME FROM userlist TO REMOVE DUPLICATE NAME
                List<CIBResult> userListFiltered = new List<CIBResult>();
                userListFiltered.AddRange(userList);
                //

                cIBResultObjList.AddRange(userListFiltered);
            }
            #endregion
            cibResultGridView.DataSource = cIBResultObjList;
            cibResultGridView.DataBind();
            if (cIBResultObjList.Count > 0)
            {
                fiveTotalRecordLabel.Text = "Total Record : " + cIBResultObjList.Count.ToString();
            }
        }
        public static string FormatedGridCellText(object objString)
        {
            string returnString = "";
            return returnString = objString != null ? objString.ToString().Replace(",", ", ") : "";
        }
        public static string ReplaseString(string objString)
        {
            string returnString = "";
            returnString = objString.ToUpper().Replace("MD", "").Replace("MR.", "").Replace("MRS", "").Replace("MISS", "").Replace("DR.", "").Replace("M/S", "").Replace(".", "").Replace("LATE", "").Replace("MOHAMMAD", "").Replace("MOHAMAD", "");
            return returnString.Trim();
        }

        protected void negativeListPersonGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            //{
            //    e.Row.TabIndex = -1;
            //    e.Row.Attributes["onclick"] = string.Format("javascript:SelectRow(this, {0});", e.Row.RowIndex);
            //    e.Row.Attributes["onkeydown"] = "javascript:return SelectSibling(event);";
            //    e.Row.Attributes["onselectstart"] = "javascript:return false;";
            //    negativeListPersonGridView_RowDataBound(sender, e);
            //}

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void bAutoDecline_Click(object sender, EventArgs e)
        {
            try
            {
                AutoLoanAssessmentRrepository repository = new AutoLoanAssessmentDataService();
                var autoLoanAssessmentService = new AutoLoanAssessmentService(repository);
                var masterId = autoLoanAssessmentService.CheckLLIDIsAuto(loanLocatorIdTextBox.Text);
                if (masterId > 0)
                {
                    var userID = Convert.ToInt32(Session["Id"].ToString());
                    var UserType = Convert.ToInt32(Session["UserType"].ToString());

                    AutoStatusHistory objAutoStatusHistory = new AutoStatusHistory();
                    objAutoStatusHistory.AutoLoanMasterId = masterId;
                    objAutoStatusHistory.StatusID = 2;
                    objAutoStatusHistory.UserID = userID;


                    var res = autoLoanAssessmentService.AppealedByAutoLoanMasterId(objAutoStatusHistory);
                    var msg = "";
                    if (res == "Success")
                    {
                        msg = "Declined Success";
                        SendData(tReason.Text.Trim());
                        AutoPanel.Visible = false;
                        plussPanel.Visible = true;
                        //pluss1stPanel.Visible = true;
                        ScriptManager.RegisterStartupScript(declineButton, this.GetType(), "declineButton", "alert('Declined Success');", true);
                    }
                    else
                    {
                        msg = "Declined Failed";
                        ScriptManager.RegisterStartupScript(declineButton, this.GetType(), "declineButton", "alert('Declined Failed');", true);
                    }
                    return;
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Auto Loan Decline");
            }


        }

        protected void bAutoClose_Click(object sender, EventArgs e)
        {
            AutoPanel.Visible = false;
            plussPanel.Visible = true;
            //pluss1stPanel.Visible = true;
        }
    }
}

