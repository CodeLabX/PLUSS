﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using AzUtilities;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI.Dedups
{
    public partial class UI_DedupResultPrint : System.Web.UI.Page


    {
        public LoanInformation loanInformationObj = null;
        public List<DeDupInfo> dedupInfoListObj=null;
        public DeDupInfo dedupObj =null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                dedupObj = new DeDupInfo();
                loanInformationObj = new LoanInformation();
                dedupInfoListObj = new List<DeDupInfo>();
                dedupObj = (DeDupInfo)Session["dedupSelectedObjS"];
                dedupInfoListObj = (List<DeDupInfo>)Session["dedupInfoListS"];
                loanInformationObj=(LoanInformation)Session["loanInformationObjS"];
                if (dedupObj != null && dedupInfoListObj != null && loanInformationObj != null)
                {
                    LoadData();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Load Disburse Info"); 
            }

        }
        private void LoadData()
        {

            loanLocatorIdLabel.Text = loanInformationObj.LLId.ToString();
            applicantNameLabel.Text = loanInformationObj.ApplicantName.ToString();
            fathersNameLabel.Text = loanInformationObj.FatherName.ToString();
            mothresNameLabel.Text = loanInformationObj.MotherName.ToString();
            businessNameLabel.Text = loanInformationObj.Business.ToString();
            if (DateFormat.IsDate(loanInformationObj.DOB))
            {
                try
                {
                    dateOfBirthLabel.Text = loanInformationObj.DOB.ToString("dd-MM-yyyy");
                }
                catch
                {
                    dateOfBirthLabel.Text = "";
                }
            }
            scbAccNoLabel1.Text = loanInformationObj.AccountNo.PreAccNo1.ToString() + "  " + loanInformationObj.AccountNo.MasterAccNo1.ToString() + "  " + loanInformationObj.AccountNo.PostAccNo1.ToString();
            scbAccNoLabel2.Text = loanInformationObj.AccountNo.PreAccNo2.ToString() + "  " + loanInformationObj.AccountNo.MasterAccNo2.ToString() + "  " + loanInformationObj.AccountNo.PostAccNo2.ToString();
            scbAccNoLabel3.Text = loanInformationObj.AccountNo.PreAccNo3.ToString() + "  " + loanInformationObj.AccountNo.MasterAccNo3.ToString() + "  " + loanInformationObj.AccountNo.PostAccNo3.ToString();
            scbAccNoLabel4.Text = loanInformationObj.AccountNo.PreAccNo4.ToString() + "  " + loanInformationObj.AccountNo.MasterAccNo4.ToString() + "  " + loanInformationObj.AccountNo.PostAccNo4.ToString();
            scbAccNoLabel5.Text = loanInformationObj.AccountNo.PreAccNo5.ToString() + "  " + loanInformationObj.AccountNo.MasterAccNo5.ToString() + "  " + loanInformationObj.AccountNo.PostAccNo5.ToString();
            contactNoLabel1.Text = loanInformationObj.ContactNumber.ContactNo1.ToString();
            contactNoLabel2.Text = loanInformationObj.ContactNumber.ContactNo2.ToString();
            contactNoLabel3.Text = loanInformationObj.ContactNumber.ContactNo3.ToString();
            contactNoLabel4.Text = loanInformationObj.ContactNumber.ContactNo4.ToString();
            contactNoLabel5.Text = loanInformationObj.ContactNumber.ContactNo5.ToString();
            idTypeLabel1.Text = loanInformationObj.ApplicantId.Type1.ToString();
            idTypeLabel2.Text = loanInformationObj.ApplicantId.Type2.ToString();
            idTypeLabel3.Text = loanInformationObj.ApplicantId.Type3.ToString();
            idTypeLabel4.Text = loanInformationObj.ApplicantId.Type4.ToString();
            idTypeLabel5.Text = loanInformationObj.ApplicantId.Type5.ToString();
            idNoLabel1.Text = loanInformationObj.ApplicantId.Id1.ToString();
            idNoLabel2.Text = loanInformationObj.ApplicantId.Id2.ToString();
            idNoLabel3.Text = loanInformationObj.ApplicantId.Id3.ToString();
            idNoLabel4.Text = loanInformationObj.ApplicantId.Id4.ToString();
            idNoLabel5.Text = loanInformationObj.ApplicantId.Id5.ToString();
            if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate1))
            {
                expiryDateLabel1.Text = loanInformationObj.ApplicantId.ExpiryDate1.ToString("dd-MM-yyyy");
            }
            else
            {
                expiryDateLabel1.Text = "";
            }
            if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate2))
            {
                expiryDateLabel2.Text = loanInformationObj.ApplicantId.ExpiryDate2.ToString("dd-MM-yyyy");
            }
            else
            {
                expiryDateLabel2.Text = "";
            }
            if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate3))
            {
                expiryDateLabel3.Text = loanInformationObj.ApplicantId.ExpiryDate3.ToString("dd-MM-yyyy");
            }
            else
            {
                expiryDateLabel3.Text = "";
            }
            if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate4))
            {
                expiryDateLabel4.Text = loanInformationObj.ApplicantId.ExpiryDate4.ToString("dd-MM-yyyy");
            }
            else
            {
                expiryDateLabel4.Text = "";
            }
            if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate5))
            {
                expiryDateLabel5.Text = loanInformationObj.ApplicantId.ExpiryDate5.ToString("dd-MM-yyyy");
            }
            else
            {
                expiryDateLabel5.Text = "";
            }

            tinNoLabel.Text = loanInformationObj.TINNo.ToString();
            bankLabel1.Text = loanInformationObj.OtherBankAccount.OtherBank1.ToString();
            bankLabel2.Text = loanInformationObj.OtherBankAccount.OtherBank2.ToString();
            bankLabel3.Text = loanInformationObj.OtherBankAccount.OtherBank3.ToString();
            bankLabel4.Text = loanInformationObj.OtherBankAccount.OtherBank4.ToString();
            bankLabel5.Text = loanInformationObj.OtherBankAccount.OtherBank5.ToString();
            branchLabel1.Text = loanInformationObj.OtherBankAccount.OtherBranch1.ToString();
            branchLabel2.Text = loanInformationObj.OtherBankAccount.OtherBranch2.ToString();
            branchLabel3.Text = loanInformationObj.OtherBankAccount.OtherBranch3.ToString();
            branchLabel4.Text = loanInformationObj.OtherBankAccount.OtherBranch4.ToString();
            branchLabel5.Text = loanInformationObj.OtherBankAccount.OtherBranch5.ToString();
            if (loanInformationObj.OtherBankAccount.OtherAccNo1 != "0")
            {
                acNoLabel1.Text = loanInformationObj.OtherBankAccount.OtherAccNo1.ToString();
            }
            else
            {
                acNoLabel1.Text = "";
            }
            if (loanInformationObj.OtherBankAccount.OtherAccNo2.ToString() != "0")
            {
                acNoLabel2.Text = loanInformationObj.OtherBankAccount.OtherAccNo2.ToString();
            }
            else
            {
                acNoLabel2.Text = "";
            }
            if (loanInformationObj.OtherBankAccount.OtherAccNo3.ToString() != "0")
            {
                acNoLabel3.Text = loanInformationObj.OtherBankAccount.OtherAccNo3.ToString();
            }
            else
            {
                acNoLabel3.Text = "";
            }
            if (loanInformationObj.OtherBankAccount.OtherAccNo4.ToString() != "0")
            {
                acNoLabel4.Text = loanInformationObj.OtherBankAccount.OtherAccNo4.ToString();
            }
            else
            {
                acNoLabel4.Text = "";
            }
            if (loanInformationObj.OtherBankAccount.OtherAccNo5.ToString() != "0")
            {
                acNoLabel5.Text = loanInformationObj.OtherBankAccount.OtherAccNo5.ToString();
            }
            else
            {
                acNoLabel5.Text = "";
            }
            acTypeLabel1.Text = loanInformationObj.OtherBankAccount.OtherAccType1.ToString();
            acTypeLabel2.Text = loanInformationObj.OtherBankAccount.OtherAccType2.ToString();
            acTypeLabel3.Text = loanInformationObj.OtherBankAccount.OtherAccType3.ToString();
            acTypeLabel4.Text = loanInformationObj.OtherBankAccount.OtherAccType4.ToString();
            acTypeLabel5.Text = loanInformationObj.OtherBankAccount.OtherAccType5.ToString();
            switch (loanInformationObj.OtherBankAccount.OtherStatus1.ToString())
            {
                case "0":
                    statusLabel1.Text = "Offline Clear";
                    break;
                case "1":
                    statusLabel1.Text = "Online Clear";
                    break;
                case "2":
                    statusLabel1.Text = "Offline Not Clear";
                    break;
                default:
                    statusLabel1.Text = "";
                    break;
            }
            switch (loanInformationObj.OtherBankAccount.OtherStatus2.ToString())
            {
                case "0":
                    statusLabel2.Text = "Offline Clear";
                    break;
                case "1":
                    statusLabel2.Text = "Online Clear";
                    break;
                case "2":
                    statusLabel2.Text = "Offline Not Clear";
                    break;
                default:
                    statusLabel2.Text = "";
                    break;
            }
            switch (loanInformationObj.OtherBankAccount.OtherStatus3.ToString())
            {
                case "0":
                    statusLabel3.Text = "Offline Clear";
                    break;
                case "1":
                    statusLabel3.Text = "Online Clear";
                    break;
                case "2":
                    statusLabel3.Text = "Offline Not Clear";
                    break;
                default:
                    statusLabel3.Text = "";
                    break;
            }
            switch (loanInformationObj.OtherBankAccount.OtherStatus4.ToString())
            {
                case "0":
                    statusLabel4.Text = "Offline Clear";
                    break;
                case "1":
                    statusLabel4.Text = "Online Clear";
                    break;
                case "2":
                    statusLabel4.Text = "Offline Not Clear";
                    break;
                default:
                    statusLabel4.Text = "";
                    break;
            }
            switch (loanInformationObj.OtherBankAccount.OtherStatus5.ToString())
            {
                case "0":
                    statusLabel5.Text = "Offline Clear";
                    break;
                case "1":
                    statusLabel5.Text = "Online Clear";
                    break;
                case "2":
                    statusLabel5.Text = "Offline Not Clear";
                    break;
                default:
                    statusLabel5.Text = "";
                    break;
            }

            string viewData = "";
            List<string> viewList = new List<string>();
            for (int i = 0; i < 3; i++)
            {
                viewData = "";
                viewList.Add(viewData);
            }
            negativeListPersonGridView.DataSource = viewList;
            negativeListPersonGridView.DataBind();
            //NegetiveListPerson
            int selectedindex = 0;
            bool selectedIndexFound = false;
            List<DeDupInfo> selectedDedupInfoList = new List<DeDupInfo>();
            foreach (DeDupInfo temp in dedupInfoListObj)
            {
                if (dedupObj.Id == temp.Id)
                {
                    selectedindex = dedupInfoListObj.IndexOf(temp);
                    selectedIndexFound = true;
                }
                if (selectedIndexFound == true)
                {
                    if (selectedindex <= dedupInfoListObj.IndexOf(temp) && selectedDedupInfoList.Count < 3)
                    {
                        selectedDedupInfoList.Add(temp);
                    }
                    if (selectedDedupInfoList.Count == 3)
                    {
                        selectedIndexFound = false;
                        break;
                    }
                }
            }
            for (int i = 0; i < selectedDedupInfoList.Count; i++)
            {
                negativeListPersonGridView.Rows[i].Cells[0].Text = selectedDedupInfoList[i].MatchFoundNumber.ToString();
                negativeListPersonGridView.Rows[i].Cells[1].Text = selectedDedupInfoList[i].Id.ToString();
                negativeListPersonGridView.Rows[i].Cells[2].Text = selectedDedupInfoList[i].Name.ToString();
                negativeListPersonGridView.Rows[i].Cells[3].Text = FormatedGridCellText(selectedDedupInfoList[i].AccountNo.MasterAccNo1.ToString());
                negativeListPersonGridView.Rows[i].Cells[4].Text = selectedDedupInfoList[i].Status.ToString();
                negativeListPersonGridView.Rows[i].Cells[5].Text = selectedDedupInfoList[i].Tin.ToString();
                negativeListPersonGridView.Rows[i].Cells[6].Text = selectedDedupInfoList[i].ApplicantId.Id1.ToString();
                negativeListPersonGridView.Rows[i].Cells[7].Text = FormatedGridCellText(selectedDedupInfoList[i].ContactNumber.ContactNo1.ToString());
                negativeListPersonGridView.Rows[i].Cells[8].Text = FormatedGridCellText(selectedDedupInfoList[i].ContactNumber.ContactNo3.ToString());
                try
                {
                    if (DateFormat.IsDate(selectedDedupInfoList[i].DateOfBirth))
                    {
                        negativeListPersonGridView.Rows[i].Cells[9].Text = selectedDedupInfoList[i].DateOfBirth.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        negativeListPersonGridView.Rows[i].Cells[9].Text = "";
                    }
                }
                catch { }
            }

            totalRecordCount.Text = dedupInfoListObj.Count.ToString();

            LLIDTextBox1.Text = dedupObj.Id.ToString();
            statusTextBox.Text = dedupObj.Status.ToString();
            nameTextBox.Text = dedupObj.Name.ToString();
            if (DateFormat.IsDate(dedupObj.DateOfBirth))
            {
                dobTextBox.Text = dedupObj.DateOfBirth.ToString("dd-MM-yyyy");
            }
            else
            {
                dobTextBox.Text = "";
            }
            fatherNameTextBox.Text = dedupObj.FatherName.ToString();
            motherNameTextBox.Text = dedupObj.MotherName.ToString();
            educationalTextBox.Text = dedupObj.EducationalLevel.ToString();
            maritalStatusTextBox.Text = dedupObj.MaritalStatus.ToString();
            ResidentialAddressTextBox.Text = dedupObj.Address1.ToString();
            residentialStatusTextBox.Text = dedupObj.ResidentaileStatus.ToString();
            conractNoTextBox1.Text = dedupObj.ContactNumber.ContactNo1.ToString();
            conractNoTextBox2.Text = dedupObj.ContactNumber.ContactNo2.ToString();
            conractNoTextBox3.Text = dedupObj.ContactNumber.ContactNo3.ToString();
            conractNoTextBox4.Text = dedupObj.ContactNumber.ContactNo4.ToString();
            idTypeTextBox1.Text = dedupObj.ApplicantId.Type1.ToString();
            idTypeTextBox2.Text = dedupObj.ApplicantId.Type2.ToString();
            idTypeTextBox3.Text = dedupObj.ApplicantId.Type3.ToString();
            idTypeTextBox4.Text = dedupObj.ApplicantId.Type4.ToString();
            idTypeTextBox5.Text = dedupObj.ApplicantId.Type5.ToString();
            idNoTextBox1.Text = dedupObj.ApplicantId.Id1.ToString();
            idNoTextBox2.Text = dedupObj.ApplicantId.Id2.ToString();
            idNoTextBox3.Text = dedupObj.ApplicantId.Id3.ToString();
            idNoTextBox4.Text = dedupObj.ApplicantId.Id4.ToString();
            idNoTextBox5.Text = dedupObj.ApplicantId.Id5.ToString();
            if (DateFormat.IsDate(dedupObj.ApplicantId.ExpiryDate1))
            {
                idExpiryDateTextBox1.Text = dedupObj.ApplicantId.ExpiryDate1.ToString("dd-MM-yyyy");
            }
            else
            {
                idExpiryDateTextBox1.Text = "";
            }
            if (DateFormat.IsDate(dedupObj.ApplicantId.ExpiryDate2))
            {
                idExpiryDateTextBox2.Text = dedupObj.ApplicantId.ExpiryDate2.ToString("dd-MM-yyyy");
            }
            else
            {
                idExpiryDateTextBox2.Text = "";
            }
            if (DateFormat.IsDate(dedupObj.ApplicantId.ExpiryDate3))
            {
                idExpiryDateTextBox3.Text = dedupObj.ApplicantId.ExpiryDate3.ToString("dd-MM-yyyy");
            }
            else
            {
                idExpiryDateTextBox3.Text = "";
            }
            if (DateFormat.IsDate(dedupObj.ApplicantId.ExpiryDate4))
            {
                idExpiryDateTextBox4.Text = dedupObj.ApplicantId.ExpiryDate4.ToString("dd-MM-yyyy");
            }
            else
            {
                idExpiryDateTextBox4.Text = "";
            }
            if (DateFormat.IsDate(dedupObj.ApplicantId.ExpiryDate5))
            {
                idExpiryDateTextBox5.Text = dedupObj.ApplicantId.ExpiryDate5.ToString("dd-MM-yyyy");
            }
            else
            {
                idExpiryDateTextBox5.Text = "";
            }
            tinNoTextBox.Text = dedupObj.Tin.ToString();
            if (dedupObj.AccountNo.MasterAccNo1.Length != 16)
            {
                if (!dedupObj.AccountNo.MasterAccNo1.Contains(","))
                {
                    if (dedupObj.AccountNo.PreAccNo1 != null)
                    {
                        preTextBox1.Text = dedupObj.AccountNo.PreAccNo1.ToString();
                    }
                    else
                    {
                        preTextBox1.Text = "";
                    }
                    if (dedupObj.AccountNo.MasterAccNo1 != null)
                    {
                        scbACNoTextBox1.Text = dedupObj.AccountNo.MasterAccNo1.ToString();
                    }
                    else
                    {
                        scbACNoTextBox1.Text = "";
                    }
                    if (dedupObj.AccountNo.PostAccNo1 != null)
                    {
                        postTextBox1.Text = dedupObj.AccountNo.PostAccNo1.ToString();
                    }
                    else
                    {
                        postTextBox1.Text = "";
                    }
                    creditCardNoTextBox.Text = "";
                }
                else
                {
                    for (int i = 0; i < dedupObj.SplittedAccountList.Count; i++)
                    {
                        if (dedupObj.SplittedAccountList[i].Length == 11)
                        {
                            if (i == 1)
                            {
                                preTextBox1.Text = dedupObj.SplittedAccountList[i].Substring(0, 2).ToString();
                                scbACNoTextBox1.Text = dedupObj.SplittedAccountList[i].Substring(2, 7).ToString();
                                postTextBox1.Text = dedupObj.SplittedAccountList[i].Substring(9, 2).ToString();
                            }
                            if (i == 2)
                            {
                                preTextBox2.Text = dedupObj.SplittedAccountList[i].Substring(0, 2).ToString();
                                scbACNoTextBox2.Text = dedupObj.SplittedAccountList[i].Substring(2, 7).ToString();
                                postTextBox2.Text = dedupObj.SplittedAccountList[i].Substring(9, 2).ToString();
                            }
                            if (i == 3)
                            {
                                preTextBox3.Text = dedupObj.SplittedAccountList[i].Substring(0, 2).ToString();
                                scbACNoTextBox3.Text = dedupObj.SplittedAccountList[i].Substring(2, 7).ToString();
                                postTextBox3.Text = dedupObj.SplittedAccountList[i].Substring(9, 2).ToString();
                            }
                            if (i == 4)
                            {
                                preTextBox4.Text = dedupObj.SplittedAccountList[i].Substring(0, 2).ToString();
                                scbACNoTextBox4.Text = dedupObj.SplittedAccountList[i].Substring(2, 7).ToString();
                                postTextBox4.Text = dedupObj.SplittedAccountList[i].Substring(9, 2).ToString();
                            }
                            if (i == 5)
                            {
                                preTextBox5.Text = dedupObj.SplittedAccountList[i].Substring(0, 2).ToString();
                                scbACNoTextBox5.Text = dedupObj.SplittedAccountList[i].Substring(2, 7).ToString();
                                postTextBox5.Text = dedupObj.SplittedAccountList[i].Substring(9, 2).ToString();
                            }
                            creditCardNoTextBox.Text = "";
                        }
                        else if (dedupObj.SplittedAccountList[i].Length < 11)
                        {
                            if (i == 1)
                            {
                                preTextBox1.Text = "";
                                scbACNoTextBox1.Text = dedupObj.SplittedAccountList[i].ToString();
                                postTextBox1.Text = "";
                            }
                            if (i == 2)
                            {
                                preTextBox1.Text = "";
                                scbACNoTextBox1.Text = dedupObj.SplittedAccountList[i].ToString();
                                postTextBox1.Text = "";
                            }
                            if (i == 3)
                            {
                                preTextBox1.Text = "";
                                scbACNoTextBox1.Text = dedupObj.SplittedAccountList[i].ToString();
                                postTextBox1.Text = "";
                            }
                            if (i == 4)
                            {
                                preTextBox1.Text = "";
                                scbACNoTextBox1.Text = dedupObj.SplittedAccountList[i].ToString();
                                postTextBox1.Text = "";
                            }
                            if (i == 5)
                            {
                                preTextBox1.Text = "";
                                scbACNoTextBox1.Text = dedupObj.SplittedAccountList[i].ToString();
                                postTextBox1.Text = "";
                            }
                            creditCardNoTextBox.Text = "";
                        }
                    }
                }
            }
            else
            {
                if (dedupObj.AccountNo.MasterAccNo1 != null)
                {
                    creditCardNoTextBox.Text = dedupObj.AccountNo.MasterAccNo1.ToString();
                }
            }

            if (!String.IsNullOrEmpty(dedupObj.NameOfCompany))
            {
                companyNameTextBox.Text = dedupObj.NameOfCompany.ToString();
            }
            else
            {
                companyNameTextBox.Text = "";
            }
            if (!String.IsNullOrEmpty(dedupObj.OfficeAddress))
            {
                companyAddressTextBox.Text = dedupObj.OfficeAddress.ToString();
            }
            else
            {
                companyAddressTextBox.Text = "";
            }
            if (!String.IsNullOrEmpty(dedupObj.DeclineReason))
            {
                declineResonTextBox.Text = dedupObj.DeclineReason.ToString();
            }
            else
            {
                declineResonTextBox.Text = "";
            }
            if (DateFormat.IsDate(dedupObj.DeclineDate))
            {
                declineDateTextBox.Text = dedupObj.DeclineDate.ToString("dd-MM-yyyy");
            }
            else
            {
                declineDateTextBox.Text = "";
            }
        }
        public static string FormatedGridCellText(string objString)
        {
            string returnString = "";
            return returnString = objString.ToString().Replace(",", ", ");
        }
    }
}
