﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.Dedups.UI_DEDUP" EnableEventValidation="false"
    MasterPageFile="~/UI/ITAdminMasterPage.master" ValidateRequest="false" CodeBehind="DEDUP.aspx.cs" Title="Dedup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style2 {
            width: 130px;
        }

        .style3 {
            width: 268px;
        }

        #defareFrame {
            width: 628px;
        }

        .translucent {
            filter: alpha(opacity=50);
            -moz-opacity: 0.5;
            opacity: 0.5;
        }

        .style1 {
            width: 138px;
        }

        .modal {
            background: #FDFDFD none repeat scroll 0 0;
            border: 2px outset #CCD76E;
            display: inline;
            height: 250px;
            xleft: 360px;
            overflow: auto;
            position: absolute;
            xtop: 160px;
            width: 440px;
            z-index: 999;
        }

        table.reference {
            background-color: #FFFFFF;
            border: 1px solid #C3C3C3;
            border-collapse: collapse;
            width: 100%;
        }

        .btnRight {
            float: right;
            margin-right: 15px;
            text-align: center;
        }
    </style>

    <script type="text/javascript">
        var currentRowId = 0;
        function SelectRow() {
            if (event.keyCode == 40) {
                MarkRow(currentRowId + 1);
            }
            else if (event.keyCode == 38) {
                MarkRow(currentRowId - 1);
            }
        }

        function MarkRow(rowId) {
            if (document.getElementById(rowId) == null)
                return;
            if (document.getElementById(currentRowId) != null)
                document.getElementById(currentRowId).style.backgroundColor = '#ffffff';
            currentRowId = rowId;
            document.getElementById(rowId).style.backgroundColor = '#C3DAF9';
        }
        //-----------------------------------------------------------------------
        var SelectedRow = null;
        var SelectedRowIndex = null;
        var UpperBound = null;
        var LowerBound = null;

        window.onload = function () {
            UpperBound = parseInt('<%= this.negativeListPersonGridView.Rows.Count %>') - 1;
            LowerBound = 0;
            SelectedRowIndex = -1;
        }

        function SelectRow(CurrentRow, RowIndex) {
            if (SelectedRow == CurrentRow || RowIndex > UpperBound || RowIndex < LowerBound) return;

            if (SelectedRow != null) {
                SelectedRow.style.backgroundColor = SelectedRow.originalBackgroundColor;
                SelectedRow.style.color = SelectedRow.originalForeColor;
            }

            if (CurrentRow != null) {
                CurrentRow.originalBackgroundColor = CurrentRow.style.backgroundColor;
                CurrentRow.originalForeColor = CurrentRow.style.color;
                CurrentRow.style.backgroundColor = '#DCFC5C';
                CurrentRow.style.color = 'Black';
            }

            SelectedRow = CurrentRow;
            SelectedRowIndex = RowIndex;
            setTimeout("SelectedRow.focus();", 0);
        }

        function SelectSibling(e) {
            var e = e ? e : window.event;
            var KeyCode = e.which ? e.which : e.keyCode;

            if (KeyCode == 40)
                SelectRow(SelectedRow.nextSibling, SelectedRowIndex + 1);
            else if (KeyCode == 38)
                SelectRow(SelectedRow.previousSibling, SelectedRowIndex - 1);

            return false;
        }
        (function () {
            //  $('#ctl00_ContentPlaceHolder_loanLocatorIdTextBox').numberOnly();
        }

        );
    //--------------------------------------------------------------
    </script>


    <script type="text/javascript">

        function JS_FunctionCheckRequest() {
            if (confirm("Are you sure want to Defer ?")) {
                return true;
            }
            else {
                return false;
            }
        }

        //-----------------------------------------------------------------------------------------
        function ShowNewItemPopUp(dlg, pg) {





            document.getElementById("blockUI").style.height = pg.scrollHeight;
            document.getElementById("blockUI").style.width = pg.scrollWidth;
            document.getElementById("blockUI").style.display = "block";

            document.getElementById(dlg).style.zIndex = 50002;
            document.getElementById(dlg).style.left = pg.scrollLeft + 600;
            document.getElementById(dlg).style.display = "block";
            document.getElementById(dlg).style.left = (pg.clientWidth - document.getElementById(dlg).clientWidth) / 2;
            document.getElementById(dlg).style.top = pg.scrollTop + 350;
            document.getElementById(dlg).style.top = (pg.clientHeight - document.getElementById(dlg).clientHeight) / 2;

        }
        function closeDialog(dlg) {
            debugger;
            document.getElementById(dlg).style.display = "none";
            document.getElementById("blockUI").style.display = "none";
        }
        function ShowmessageForAuto(msg) {
            debugger;
            alert(msg);
        }
    </script>

    <style type="text/css">
        #nav_DEDUP a {
            background-color: #F2F6F6;
            color: #3DAE38;
        }

        .auto-style1 {
            width: 249px;
        }
    </style>

    <script type="text/javascript">

        function Testalert() {
            $("#divDedup").hide();
            $("#requiredInfoPopupDiv").show();
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">DE-DUP Verification</div>
        <asp:Panel ID="pluss1stPanel" runat="server">
            <fieldset>
                <legend>Search Parameter</legend>
                <table border="0" cellspacing="1" cellpadding="1">
                    <tr>
                        <td align="right">
                            <asp:Label ID="loanLocatorIdLabel" runat="server" Text="Loan Locator ID :"></asp:Label>
                        </td>
                        <td align="left" colspan="2">
                            <asp:TextBox CssClass="input-field has-width-150px" ID="loanLocatorIdTextBox" runat="server" Width="160px" MaxLength="9"></asp:TextBox>
                            <asp:Button ID="findButton" runat="server" Text="..." OnClick="findButton_Click" />
                            &nbsp;<asp:Label ID="errorMsgLabel" runat="server" ForeColor="#FF3300"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="exactNameSearchChckBox" runat="server" Text="Exact Name Wise Search" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="applicantNameLabel0" runat="server" Text="Applicant Name :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="applicantNameTextBox0" runat="server" Width="230px"></asp:TextBox>
                            <asp:CheckBox ID="applicantNameCheckbox" runat="server" OnCheckedChanged="applicantNameCheckbox_CheckedChanged" />
                        </td>
                        <td align="right">
                            <asp:Label ID="dateOfBirthLabel0" runat="server" Text="Date of Birth :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="dateOfBirthTextBox0" runat="server" Width="230px"></asp:TextBox>
                            <asp:CheckBox ID="dateOfBirthCheckBox0" runat="server" />&nbsp;(dd-mm-yyyy)&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="fatherNameLabel0" runat="server" Text="Father's Name :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="fatherNameTextBox10" runat="server" Width="230px"></asp:TextBox>
                            <asp:CheckBox ID="fatherNameCheckBox" runat="server" OnCheckedChanged="fatherNameCheckBox_CheckedChanged"></asp:CheckBox>
                        </td>
                        <td align="right">
                            <asp:Label ID="motherNameLabel0" runat="server" Text="Mother's Name :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="motherNameTextBox0" runat="server" Width="230px"></asp:TextBox>
                            <asp:CheckBox ID="motherNameCheckbox" runat="server" OnCheckedChanged="motherNameCheckbox_CheckedChanged"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="businessNameLabel0" runat="server" Text="Business Name :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="businessNameTextBox0" runat="server" Width="230px"></asp:TextBox>
                            <asp:CheckBox ID="businessNameCheckbox" runat="server" OnCheckedChanged="businessNameCheckbox_CheckedChanged"></asp:CheckBox>
                        </td>
                        <td align="right">
                            <asp:Label ID="tinNoLabel0" runat="server" Text="TIN No. :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="input-field" ID="tinNoTextBox" runat="server" Width="160px"></asp:TextBox>
                            <asp:CheckBox ID="tinNoCheckBox" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="bottom">
                            <asp:CheckBox ID="selectAllCheckBox" runat="server" Text="select all" AutoPostBack="True"
                                OnCheckedChanged="selectAllCheckBox_CheckedChanged" />
                        </td>
                        <td align="left" valign="top">
                            <asp:GridView CssClass="myGridStyle" ID="contactNoEntryGridView" runat="server" AutoGenerateColumns="False"
                                Font-Size="11px" PageSize="3" Width="220px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Contact Number">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="input-field" ID="contactNoEntryTextBox" runat="server" Width="205px" BorderWidth="0px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="contactNoEntryCheckBox" runat="server" Width="25px" BorderWidth="0px"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<HeaderStyle CssClass="ssHeader" />
                                <AlternatingRowStyle BackColor="AliceBlue" />--%>
                            </asp:GridView>
                        </td>
                        <td align="left" valign="top">
                            <asp:GridView CssClass="myGridStyle" ID="idEntryGridView" runat="server" AutoGenerateColumns="False" Font-Size="11px"
                                PageSize="3" Width="220px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="input-field" ID="typeEntryTextBox" runat="server" Width="75px" BorderWidth="0px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID No.">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="input-field" ID="idNoEntryTextBox" runat="server" Width="75px" BorderWidth="0px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Expiry Date">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="input-field" ID="expiryDateEntyTextBox" runat="server" Width="75px" BorderWidth="0px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="idEntryCheckBox" runat="server" Width="20px" BorderWidth="0px"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<HeaderStyle CssClass="ssHeader" />
                                <AlternatingRowStyle BackColor="AliceBlue" />--%>
                            </asp:GridView>
                        </td>
                        <td align="left" valign="top">
                            <asp:GridView CssClass="myGridStyle" ID="scbAccountEntryGridView" runat="server" AutoGenerateColumns="False"
                                Font-Size="11px" PageSize="3" Width="220px">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="input-field" ID="prefixEntryTextBox" runat="server" Width="30px" MaxLength="2" BorderWidth="0px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SCB A/C No.">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="input-field" ID="accountNoEntryTextBox" runat="server" Width="140px" BorderWidth="0px"
                                                MaxLength="7"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="input-field" ID="postfixEntryTextBox" runat="server" Width="30px" MaxLength="2" BorderWidth="0px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="scbAccEntryCheckBox" runat="server" Width="20px" BorderWidth="0px"
                                                AutoPostBack="True" OnCheckedChanged="scbAccEntryCheckBox_CheckedChanged"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<HeaderStyle CssClass="ssHeader" />
                                <AlternatingRowStyle BackColor="AliceBlue" />--%>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
        <table>
            <tr>
                <td>
                    <%--  <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>--%>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/1.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Panel ID="plussPanel" runat="server">
                                <div>
                                    <asp:Menu ID="Menu1" Width="1059px" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
                                        BackColor="#00CC00" BorderStyle="Outset" DynamicHorizontalOffset="2" Font-Names="Verdana"
                                        Font-Size="11pt" ForeColor="White" StaticSubMenuIndent="10px" OnMenuItemClick="Menu1_MenuItemClick"
                                        Height="16px" Style="margin-bottom: 0px">
                                        <%--  <StaticMenuStyle HorizontalPadding="20px" />
                                    <StaticSelectedStyle BackColor="Blue" BorderStyle="Groove" ForeColor="White" />
                                    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                                    <DynamicHoverStyle BackColor="#FFFFFF" ForeColor="White" />
                                    <DynamicMenuStyle BackColor="#FFFBD6" />
                                    <DynamicSelectedStyle BackColor="Blue" />
                                    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />--%>
                                        <StaticSelectedStyle BackColor="Blue" BorderStyle="Groove" ForeColor="White" />
                                        <StaticHoverStyle BackColor="Blue" ForeColor="White" />
                                        <Items>
                                            <asp:MenuItem Text="Negative List Person" Value="0"></asp:MenuItem>
                                            <asp:MenuItem Text="Negative Employers/Bank" Value="1"></asp:MenuItem>
                                            <asp:MenuItem Text="Loan Applicant" Value="2"></asp:MenuItem>
                                            <asp:MenuItem Text="Disbursed Loan" Value="3"></asp:MenuItem>
                                            <%-- <asp:MenuItem Text="CIB Result" Value="4"></asp:MenuItem>
                                            <asp:MenuItem Text="Clearing Zone" Value="5"></asp:MenuItem>--%>
                                        </Items>
                                        <%--<StaticItemTemplate>
                                    <%# Eval("Text") %>
                                    </StaticItemTemplate>--%>
                                    </asp:Menu>
                                    <asp:MultiView ID="MultiView1" runat="server" EnableTheming="False">
                                        <%-- Tab 1 --%>
                                        <asp:View ID="Tab1" runat="server">
                                            <table style="width: 100%">
                                                <tr valign="top">
                                                    <td>
                                                        <div>
                                                            <fieldset>
                                                                <legend>Search Result</legend>
                                                                <table class="myGridStyle no-border" width="100%">
                                                                    <tr align="left">
                                                                        <td width="2%">MF
                                                                        </td>
                                                                        <td width="8%">DEDUPID
                                                                        </td>
                                                                        <td width="15%">Name
                                                                        </td>
                                                                        <td width="12%">A/C No.
                                                                        </td>
                                                                        <td width="10%">Status
                                                                        </td>
                                                                        <td width="10%">TIN NO.
                                                                        </td>
                                                                        <td width="10%">ID
                                                                        </td>
                                                                        <td width="10%">Phone 1
                                                                        </td>
                                                                        <td width="10%">Phone 2
                                                                        </td>
                                                                        <td width="10%">DOB
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div style="overflow-y: scroll; height: 165px; z-index: 100; padding-top: 0px; border: solid 1px gray;">
                                                                    <asp:GridView CssClass="myGridStyle" ID="negativeListPersonGridView" runat="server" AutoGenerateColumns="False"
                                                                        CellPadding="4" Font-Size="11px" ForeColor="#333333" OnRowDataBound="negativeListPersonGridView_RowDataBound"
                                                                        OnSelectedIndexChanged="negativeListPersonGridView_SelectedIndexChanged" Width="98%"
                                                                        ShowHeader="False" AllowPaging="True" OnPageIndexChanging="negativeListPersonGridView_PageIndexChanging1"
                                                                        PageIndex="0" PageSize="5" Style="margin-bottom: 0px" PagerSettings-Mode="NextPreviousFirstLast"
                                                                        PagerSettings-FirstPageText="First" PagerSettings-LastPageText="Last" PagerSettings-NextPageText="Next"
                                                                        PagerSettings-PreviousPageText="Previous" OnRowCreated="negativeListPersonGridView_RowCreated">
                                                                        <RowStyle BackColor="#FFFFFF" BorderStyle="Solid" ForeColor="#333333" />
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="MF" SortExpression="MF">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewMfLabel" Text='<%#Eval("MatchFoundNumber")%>'
                                                                                        runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="2%" />
                                                                                <ItemStyle Width="2%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="8%" HeaderText="DEDUPID" SortExpression="DEDUPID">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewLlidLabel" Text='<%#Eval("Id")%>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="8%" />
                                                                                <ItemStyle Width="8%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Name" SortExpression="Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewNameLabel" Text='<%#Eval("Name")%>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%" />
                                                                                <ItemStyle Width="15%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="12%" HeaderText="A/C No." SortExpression="ACNo">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewAcNoLabel" Text='<%# FormatedGridCellText(DataBinder.Eval(Container.DataItem,"AccountNo.MasterAccNo1"))%>'
                                                                                        runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="12%" />
                                                                                <ItemStyle Width="12%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Status" SortExpression="DReason">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewStatusLabel" Text='<%#Eval("Status")%>'
                                                                                        runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="TIN NO." SortExpression="DDate">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewTinNoLabel" Text='<%#Eval("Tin")%>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="ID" SortExpression="ID">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewNationalIDLabel" Text='<%#Eval("IdNo")%>'
                                                                                        runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Phone 1" SortExpression="Phone1">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewMobileNoLabel" runat="server" Text='<%# FormatedGridCellText(DataBinder.Eval(Container.DataItem,"ContactNumber.ContactNo1"))%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Phone 2" SortExpression="Phone2">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewOfficePhoneLabel" runat="server" Text='<%# FormatedGridCellText(DataBinder.Eval(Container.DataItem,"ContactNumber.ContactNo2"))%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="DOB" SortExpression="DOB">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="negativeListPersonGridViewDobLabel" runat="server" Text='<%#Eval("DateOfBirth","{0:dd-MM-yyyy}")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <PagerSettings FirstPageText="First" LastPageText="Last" NextPageText="Next" PageButtonCount="5"
                                                                            PreviousPageText="Previous" />
                                                                        <FooterStyle CssClass="FixedFooter" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#FFFFFF" ForeColor="DarkRed" HorizontalAlign="Right" />
                                                                        <EmptyDataTemplate>
                                                                            &nbsp;&nbsp;No data found.
                                                                        </EmptyDataTemplate>
                                                                        <EmptyDataRowStyle Font-Names="Tahoma" Font-Size="11px" ForeColor="DarkRed" />
                                                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                        <%--<HeaderStyle CssClass="FixedHeader1" />
                                                                        <EditRowStyle BackColor="#999999" />
                                                                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                                                    </asp:GridView>
                                                                </div>
                                                                <div>
                                                                    <asp:Label ID="firstTotalRecordLabel" runat="server" Font-Bold="true" Font-Size="10px"
                                                                        Text="Total Record :"></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                            <br />

                                                            <div style="padding-left: 5px; width: 800px;">
                                                                <fieldset>
                                                                    <legend>Applicant Information</legend>
                                                                    <table style="text-align: left; padding-left: 0px" border="0" width="1020 px">
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Label ID="Label1" runat="server" Text="Total Record :"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 220px">
                                                                                <asp:Label ID="totalRecordLabel" runat="server" Text=""></asp:Label>
                                                                            </td>
                                                                            <td align="right" valign="top" colspan="2"></td>
                                                                            <td valign="top">&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Label ID="negativeListPersonDedupIdLabel" runat="server" Text="DEDUP ID :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox Style="width: 220px" CssClass="input-field" ID="negativeListPersonDedupIdTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                            <td align="right" colspan="2" valign="top">
                                                                                <asp:Label ID="negativeListPersonStatusLabel" runat="server" Text="Status :"></asp:Label>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:TextBox CssClass="input-field" Style="width: 220px" ID="negativeListPersonStatusTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Label ID="negativeListPersonApplicantNameLabel" runat="server" Text="Applicant Name :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox Style="width: 220px" CssClass="input-field" ID="negativeListPersonApplicantNameTextBox" runat="server" Width="220px"
                                                                                    ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                            <td align="right" style="width: 150px" colspan="2">
                                                                                <asp:Label ID="negativeListPersonDateOfBirthLabel" runat="server" Text="Date of Birth :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox CssClass="input-field" Style="width: 220px" ID="negativeListPersonDobTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Label ID="negativeListPersonFatherNameLabel" runat="server" Text="Father's Name :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonFatherNameTextBox" runat="server" Width="220px"
                                                                                    ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                            <td align="right" colspan="2">
                                                                                <asp:Label ID="negativeListPersonMotherNameLabel" runat="server" Text="Mother's Name :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonMotherNameTextBox" runat="server" Width="220px"
                                                                                    ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Label ID="negativeListPersonEducationalLevelLabel" runat="server" Text="Educational Level :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonEducationalTextBox" runat="server" Width="220px"
                                                                                    ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                            <td align="right" colspan="2">
                                                                                <asp:Label ID="negativeListPersonMaritalStatusLabel" runat="server" Text="Marital Status :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonMaritalStatusTextBox" runat="server" Width="220px"
                                                                                    ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" valign="top">
                                                                                <asp:Label ID="negativeListPersonResidentialAddressLabel" runat="server" Text="Residential Address :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonResidentialAddressTextBox" runat="server" TextMode="MultiLine"
                                                                                    Width="220px" ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                            <td align="right" colspan="2" valign="top">
                                                                                <asp:Label ID="negativeListPersonResidentailStatusLabel" runat="server" Text="Residentail Status :"></asp:Label>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:TextBox Style="width: 220px" CssClass="input-field" ID="negativeListPersonResidentialStatusTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" valign="top">
                                                                                <asp:Label ID="negativeListPersonContactNoLabel" runat="server" Text="Contact No. :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:GridView CssClass="myGridStyle" ID="negativeListPersonContactNoGridView" runat="server" AutoGenerateColumns="False"
                                                                                    Font-Size="11px" PageSize="3" Width="220px">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Number">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="negativeListPersonContactNoGridViewContactNoLabel" runat="server"
                                                                                                    Width="220px" Height="15px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle CssClass="ssHeader" />
                                                                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                            <td align="right" valign="top" colspan="2">&nbsp;
                                                                                <asp:Label ID="negativeListPersonIdLabel" runat="server" Text="ID :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:GridView CssClass="myGridStyle" ID="negativeListPersonIdGridView" runat="server" AutoGenerateColumns="False"
                                                                                    Font-Size="11px" PageSize="3" Width="220px">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Type">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="negativeListPersonIdGridViewTypeLabel" runat="server" Width="75px"
                                                                                                    Height="15px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="ID No.">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="negativeListPersonIdGridViewIdNoLabel" runat="server" Width="75px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Expiry Date">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="negativeListPersonIdGridViewExpiryDateLabel" runat="server" Width="70px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle CssClass="ssHeader" />
                                                                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" valign="top">
                                                                                <asp:Label ID="negativeListPersonAcNoSCBLabel" runat="server" Text="A/C No. for SCB :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:GridView class="style1" CssClass="myGridStyle" ID="negativeListPersonScbAccountGridView" runat="server" AutoGenerateColumns="False"
                                                                                    Font-Size="11px" PageSize="3" Width="220px">
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="negativeListPersonScbAccountGridViewPrefixLabel" runat="server" Height="15px"
                                                                                                    Width="30px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="A/C No.">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="negativeListPersonScbAccountGridViewAccountNoLabel" runat="server"
                                                                                                    Width="155px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="negativeListPersonScbAccountGridViewPostfixLabel" runat="server" Width="30px"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle CssClass="ssHeader" />
                                                                                    <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                            <td align="right" valign="top" colspan="2">
                                                                                <asp:Label ID="negativeListPersonTinNoLabel" runat="server" Text="TIN No. :"></asp:Label>
                                                                                <br />
                                                                                <br />
                                                                                <br />
                                                                                <br />
                                                                                <asp:Label ID="negativeListPersonAccountNameLabel" runat="server" Text="Product Name :"></asp:Label>
                                                                                <br />
                                                                                <br />
                                                                                <br />
                                                                                <asp:Label ID="creditCardNoLabel" runat="server" Text="Card Account No :"></asp:Label>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonTinNoTextBox" runat="server" ReadOnly="True" Width="220px"></asp:TextBox>
                                                                                <br />
                                                                                <br />
                                                                                <br />
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonAccountNameTextBox" runat="server" ReadOnly="True"
                                                                                    Width="220px"></asp:TextBox>
                                                                                <br />
                                                                                <br />
                                                                                <asp:TextBox CssClass="input-field" ID="creditCardNoTextBox" runat="server" ReadOnly="True" Width="220px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Label ID="negativeListPersonNameOfCompanyLabel" runat="server" Text="Name of Company :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonCompanyNameTextBox" runat="server" Width="220px"
                                                                                    ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                            <td align="right" colspan="2">
                                                                                <asp:Label ID="negativeListPersonDeclineDateLabel" runat="server" Text="Decline Date :"></asp:Label>
                                                                            </td>
                                                                            <td>&nbsp;
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonDeclineDateTextBox" runat="server" ReadOnly="True"
                                                                                    Width="220px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Label ID="negativeListPersonOfficeAddressLabel" runat="server" Text="Office Address :"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonOfficeAddressTextBox" runat="server" TextMode="MultiLine"
                                                                                    Width="220px" ReadOnly="True"></asp:TextBox>
                                                                            </td>
                                                                            <td align="right" colspan="2">
                                                                                <asp:Label ID="negativeListPersonDeclineReasonLabel" runat="server" Text="Decline Reason :"></asp:Label>
                                                                            </td>
                                                                            <td>&nbsp;
                                                                                <asp:TextBox CssClass="input-field" ID="negativeListPersonDeclineReasonTextBox" runat="server" ReadOnly="True"
                                                                                    TextMode="MultiLine" Width="220px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <%-- Tab 2 --%>
                                        <asp:View ID="Tab2" runat="server">
                                            <div>
                                                <fieldset>
                                                    <legend>Search Result</legend>
                                                    <div style="overflow-y: scroll; height: 220px; z-index: 100; padding-top: 0px; border: solid 1px gray;">
                                                        <asp:GridView CssClass="myGridStyle" ID="negativeEmployersGridView" runat="server" AutoGenerateColumns="False"
                                                            CellPadding="4" ForeColor="#333333" PageSize="5" GridLines="Both" Font-Size="11px"
                                                            PagerSettings-Mode="NextPreviousFirstLast" PagerSettings-FirstPageText="First"
                                                            PagerSettings-LastPageText="Last" PagerSettings-NextPageText="Next" PagerSettings-PreviousPageText="Previous">
                                                            <RowStyle BackColor="#FFFFFF" ForeColor="#333333" BorderStyle="Solid" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Bank/Company" HeaderStyle-Width="100">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewBankCompanyLabel" runat="server" Text='<%# Eval("BankCompany") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="100px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Branch" HeaderStyle-Width="100">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewBranchLabel" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="100px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="NegativeListed" HeaderStyle-Width="60">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewNegetiveListedLabel" runat="server" Text='<%# Eval("NegetiveList") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="60px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="WatchListed" HeaderStyle-Width="60">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewWatchListedLabel" runat="server" Text='<%# Eval("WatchList") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="60px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ExclusionListed" HeaderStyle-Width="60">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewExclusionListedLabel" runat="server" Text='<%# Eval("Exclusion") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="60px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Delisted" HeaderStyle-Width="60">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewDelistedLabel" runat="server" Text='<%# Eval("Delist") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="60px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Restricted" HeaderStyle-Width="60">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewRestrictedLabel" runat="server" Text='<%# Eval("Restriction") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="60px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="80">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewRemarksLabel" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="80px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ChangeDate" HeaderStyle-Width="50">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="negativeEmployersGridViewChangeDateLabel" runat="server" Text='<%# Eval("ChangeDate2") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="50px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                            <EmptyDataTemplate>
                                                                &nbsp;&nbsp;No data found.
                                                            </EmptyDataTemplate>
                                                            <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                            <%--<HeaderStyle CssClass="FixedHeader" />
                                                            <EditRowStyle BackColor="#999999" />
                                                            <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                                        </asp:GridView>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="secondTotalRecordLabel" runat="server" Font-Bold="true" Font-Size="10px"
                                                            Text="Total Record :"></asp:Label>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </asp:View>
                                        <%-- Tab 3 --%>
                                        <asp:View ID="View1" runat="server">
                                            <fieldset>
                                                <legend>Search Result</legend>
                                                <table style="width: 100%">
                                                    <tr valign="top">
                                                        <td>
                                                            <div>
                                                                <table class="myGridStyle no-border">
                                                                    <tr align="left">
                                                                        <td width="10%">LLId
                                                                        </td>
                                                                        <td width="25%">Name
                                                                        </td>
                                                                        <td width="10%">A/C No.
                                                                        </td>
                                                                        <td width="10%">Loan Status
                                                                        </td>
                                                                        <td width="10%">Product
                                                                        </td>
                                                                        <td width="35%">Branch
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div style="overflow-y: scroll; height: 220px; z-index: 100; padding-top: 0px; border: solid 1px gray;"
                                                                    visible="true">
                                                                    <asp:GridView CssClass="myGridStyle" ID="loanApplicantGridView" runat="server" AutoGenerateColumns="False"
                                                                        CellPadding="4" Font-Size="11px" ForeColor="#333333" AllowPaging="True" OnPageIndexChanging="loanApplicantGridView_PageIndexChanging"
                                                                        PageSize="5" ShowHeader="False" PagerSettings-Mode="NextPreviousFirstLast" PagerSettings-FirstPageText="First"
                                                                        PagerSettings-LastPageText="Last" PagerSettings-NextPageText="Next" PagerSettings-PreviousPageText="Previous" Width="744px">
                                                                        <RowStyle BackColor="#FFFFFF" BorderStyle="Solid" ForeColor="#333333" />
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="LLID" SortExpression="LLID">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="loanApplicantLlidLabel" runat="server" Text='<%# Eval("LLID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Name" SortExpression="Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="loanApplicantNameLabel" runat="server" Text='<%# Eval("ApplicantName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="20%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="20%" HeaderText="A/C No." SortExpression="ACNo">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="loanApplicantAcNoLabel" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="20%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Loan Status" SortExpression="Loan Status">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="statusLabel" runat="server" Text='<%# Eval("LoanStatus") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Product" SortExpression="Product">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="productLabel" runat="server" Text='<%# Eval("Product") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Branch" SortExpression="Branch">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="branchLabel" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="20%" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <FooterStyle CssClass="FixedFooter" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#FFFFFF" ForeColor="DarkRed" HorizontalAlign="Right" />
                                                                        <EmptyDataTemplate>
                                                                            &nbsp;&nbsp;No data found.
                                                                        </EmptyDataTemplate>
                                                                        <EmptyDataRowStyle Font-Names="Tahoma" Font-Size="11px" ForeColor="DarkRed" />
                                                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                        <%--<HeaderStyle CssClass="FixedHeader" />
                                                                        <EditRowStyle BackColor="#999999" />
                                                                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <asp:Label ID="thirdTotalRecorLabel" runat="server" Font-Bold="true" Font-Size="10px"
                                                        Text="Total Record :"></asp:Label>
                                                </div>
                                            </fieldset>
                                        </asp:View>
                                        <%-- Tab4 --%>
                                        <asp:View ID="Tab5" runat="server">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <div>
                                                            <fieldset>
                                                                <legend>Search Result</legend>
                                                                <table class="myGridStyle no-border" width="100%">
                                                                    <tr align="left">
                                                                        <th width="2%">MF</th>
                                                                        <th width="8%">DEDUPID</th>
                                                                        <th width="15%">Name</th>
                                                                        <th width="12%">A/C No.</th>
                                                                        <th width="10%">Status</th>
                                                                        <th width="10%">TIN NO.</th>
                                                                        <th width="10%">ID</th>
                                                                        <th width="10%">Phone 1</th>
                                                                        <th width="10%">Phone 1</th>
                                                                        <th width="10%">DOB</th>
                                                                    </tr>
                                                                </table>
                                                                <div style="overflow-y: scroll; height: 150px; z-index: 100; padding-top: 0px; border: solid 1px gray;"
                                                                    visible="true">
                                                                    <asp:GridView CssClass="myGridStyle" ID="disbursedLoanGridView" runat="server" AutoGenerateColumns="False"
                                                                        CellPadding="4" Font-Size="11px" ForeColor="#333333" PageSize="5" OnRowDataBound="disbursedLoanGridView_RowDataBound"
                                                                        OnSelectedIndexChanged="disbursedLoanGridView_SelectedIndexChanged" AllowPaging="True"
                                                                        OnPageIndexChanging="disbursedLoanGridView_PageIndexChanging" ShowHeader="False"
                                                                        PagerSettings-Mode="NextPreviousFirstLast" PagerSettings-FirstPageText="First"
                                                                        PagerSettings-LastPageText="Last" PagerSettings-NextPageText="Next" PagerSettings-PreviousPageText="Previous"
                                                                        Width="98%">
                                                                        <RowStyle BackColor="#FFFFFF" BorderStyle="Solid" ForeColor="#333333" />
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="2%" HeaderText="MF" SortExpression="MF">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewMfLabel" Text='<%#Eval("MatchFoundNumber")%>'
                                                                                        runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="2%" />
                                                                                <ItemStyle Width="2%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="8%" HeaderText="DEDUPID" SortExpression="DEDUPID">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewLlidLabel" Text='<%#Eval("Id")%>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="8%" />
                                                                                <ItemStyle Width="8%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="15%" HeaderText="Name" SortExpression="Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewNameLabel" Text='<%#Eval("Name")%>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%" />
                                                                                <ItemStyle Width="15%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="12%" HeaderText="A/C No." SortExpression="ACNo">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewAcNoLabel" Text='<%# FormatedGridCellText(DataBinder.Eval(Container.DataItem,"AccountNo.MasterAccNo1"))%>'
                                                                                        runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="12%" />
                                                                                <ItemStyle Width="12%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Status" SortExpression="DReason">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewStatusLabel" Text='<%#Eval("Status")%>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="TIN NO." SortExpression="DDate">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewTinNoLabel" Text='<%#Eval("Tin")%>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="ID" SortExpression="ID">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewNationalIDLabel" Text='<%#Eval("IdNo")%>' runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Phone 1" SortExpression="Phone1">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewMobileNoLabel" runat="server" Text='<%# FormatedGridCellText(DataBinder.Eval(Container.DataItem,"ContactNumber.ContactNo1"))%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="Phone 2" SortExpression="Phone2">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewOfficePhoneLabel" runat="server" Text='<%# FormatedGridCellText(DataBinder.Eval(Container.DataItem,"ContactNumber.ContactNo2"))%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" HeaderText="DOB" SortExpression="DOB">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="disbursedLoanGridViewDOBLabel" runat="server" Text='<%#Eval("DateOfBirth","{0:dd-MM-yyyy}")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%" />
                                                                                <ItemStyle Width="10%" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <FooterStyle CssClass="FixedFooter" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#FFFFFF" ForeColor="DarkRed" HorizontalAlign="Right" />
                                                                        <EmptyDataTemplate>
                                                                            &nbsp;&nbsp;No data found.
                                                                        </EmptyDataTemplate>
                                                                        <EmptyDataRowStyle Font-Names="Tahoma" Font-Size="11px" ForeColor="DarkRed" />
                                                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                        <%--<HeaderStyle CssClass="FixedHeader" />
                                                                        <EditRowStyle BackColor="#999999" />
                                                                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                                                    </asp:GridView>
                                                                </div>
                                                                <div>
                                                                    <asp:Label ID="fourthTotalRecordLabel" runat="server" Font-Bold="true" Font-Size="10px"
                                                                        Text="Total Record :"></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div style="width: 800px;">
                                                            <fieldset>
                                                                <legend>Applicant Information</legend>
                                                                <table style="text-align: left; padding-left: 0px">
                                                                    <tr>
                                                                        <td align="right" class="style2">
                                                                            <asp:Label ID="disbursedDedupIdLabel" runat="server" Text="DEDUP ID :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedDedupIdTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                        <td align="right" valign="top" colspan="2">
                                                                            <asp:Label ID="disbursedLoanStatusLabel" runat="server" Text="Status :"></asp:Label>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanStatusTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="style2">
                                                                            <asp:Label ID="disbursedLoanApplicantNameLabel" runat="server" Text="Applicant Name :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanApplicantNameTextBox" runat="server" Width="220px"
                                                                                ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                        <td align="right" colspan="2">
                                                                            <asp:Label ID="disbursedLoanDateOfBirthLabel" runat="server" Text="Date of Birth :"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanDobTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="style2">
                                                                            <asp:Label ID="disbursedLoanFatherNameLabel" runat="server" Text="Father's Name :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanFatherNameTextBox" runat="server" Width="220px" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                        <td align="right" colspan="2">
                                                                            <asp:Label ID="disbursedLoanMotherNameLabel" runat="server" Text="Mother's Name :"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanMotherNameTextBox" runat="server" Width="220px" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="style2">
                                                                            <asp:Label ID="disbursedLoanEducationalLevelLabel" runat="server" Text="Educational Level :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanEducationalLevelTextBox" runat="server" Width="220px"
                                                                                ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                        <td align="right" colspan="2">
                                                                            <asp:Label ID="disbursedLoanMaritalStatusLabel" runat="server" Text="Marital Status :"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanMaritalStatusTextBox" runat="server" Width="200px"
                                                                                ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" valign="top" class="style2">
                                                                            <asp:Label ID="disbursedLoanResidentialAddressLabel" runat="server" Text="Residential Address :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanResidentialAddressTextBox" runat="server" TextMode="MultiLine"
                                                                                Width="220px" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                        <td align="right" colspan="2" valign="top">
                                                                            <asp:Label ID="disbursedLoanResidentialStatusLabel" runat="server" Text="Residentail Status :"></asp:Label>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanResidentialStatusTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" valign="top" class="style2">
                                                                            <asp:Label ID="disbursedLoanContactNoLabel" runat="server" Text="Contact No. :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:GridView CssClass="myGridStyle" ID="disbursedLoanContactNoGridView" runat="server" AutoGenerateColumns="False"
                                                                                Font-Size="11px" PageSize="3" Width="220px">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Number">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="disbursedLoanContactNoGridViewContactNoLabel" runat="server" Width="220px"
                                                                                                Height="15px"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <%--<HeaderStyle CssClass="ssHeader" />
                                                                                <AlternatingRowStyle BackColor="AliceBlue" />--%>
                                                                            </asp:GridView>
                                                                        </td>
                                                                        <td align="right" valign="top" colspan="2">&nbsp;
                                                                            <asp:Label ID="disbursedLoanIdLabel" runat="server" Text="ID :"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:GridView CssClass="myGridStyle" ID="disbursedLoanIdGridView" runat="server" AutoGenerateColumns="False"
                                                                                Font-Size="11px" PageSize="3" Width="220px">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Type">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="disbursedLoanIdGridViewTypeLabel" runat="server" Width="75px" Height="15px"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="ID No.">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="disbursedLoanIdGridViewIdNoLabel" runat="server" Width="75px"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Expiry Date">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="disbursedLoanIdGridViewExpiryDateLabel" runat="server" Width="70px"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                               <HeaderStyle CssClass="ssHeader" />
                                                                                <AlternatingRowStyle BackColor="AliceBlue" />
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" valign="top" class="style2">
                                                                            <asp:Label ID="disbursedLoanAcForSCBLabel" runat="server" Text="A/C No. for SCB :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:GridView CssClass="myGridStyle" ID="disbursedLoanAcForSCBGridView" runat="server" AutoGenerateColumns="False"
                                                                                Font-Size="11px" PageSize="3" Width="220px">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="disbursedLoanAcForSCBGridViewPrefixLabel" runat="server" Height="15px"
                                                                                                Width="30px"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="A/C No.">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="disbursedLoanAcForSCBGridViewAccountNoLabel" runat="server" Width="155px"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="disbursedLoanAcForSCBGridViewPostfixLabel" runat="server" Width="30px"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <%--<HeaderStyle CssClass="ssHeader" />
                                                                                <AlternatingRowStyle BackColor="AliceBlue" />--%>
                                                                            </asp:GridView>
                                                                        </td>
                                                                        <td align="right" valign="top" colspan="2">
                                                                            <asp:Label ID="disbursedLoanTinNoLabel" runat="server" Text="TIN No. :"></asp:Label>
                                                                            <br />
                                                                            <br />
                                                                            <br />
                                                                            <br />
                                                                            <asp:Label ID="disbursedLoanProductNameLabel" runat="server" Text="Product Name :"></asp:Label>
                                                                            <br />
                                                                            <br />
                                                                            <asp:Label ID="disbursedLoanCreditCardLabel" runat="server" Text="Card Account No :"></asp:Label>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanTinTextBox" runat="server" ReadOnly="True" Width="200px"></asp:TextBox>
                                                                            <br />
                                                                            <br />
                                                                            <br />
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanProductNameTextBox" runat="server" ReadOnly="True"
                                                                                Width="200px"></asp:TextBox>
                                                                            <br />
                                                                            <br />
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanCreditCardTextBox" runat="server" ReadOnly="True" Width="200px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" valign="top" class="style2">&nbsp;
                                                                        </td>
                                                                        <td colspan="2">&nbsp;
                                                                        </td>
                                                                        <td align="right">&nbsp;
                                                                        </td>
                                                                        <td>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" valign="top" class="style2">&nbsp;
                                                                        </td>
                                                                        <td colspan="4">&nbsp;
                                                                        </td>
                                                                        <td>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="style2">
                                                                            <asp:Label ID="disbursedLoanNameOfCompanyLabel" runat="server" Text="Name of Company :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanNameOfCompanyTextBox" runat="server" Width="220px"
                                                                                ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                        <td align="right" colspan="2">
                                                                            <asp:Label ID="disbursedLoanDeclineDateLabel" runat="server" Text="Decline Date :"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanDeclineDateTextBox" runat="server" Width="220px" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" class="style2">
                                                                            <asp:Label ID="disbursedLoanOfficeAddressLabel" runat="server" Text="Office Address :"></asp:Label>
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanOfficeAddressTextBox" runat="server" TextMode="MultiLine"
                                                                                Width="220px" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                        <td align="right" colspan="2">
                                                                            <asp:Label ID="disbursedLoanDeclineReasonLabel" runat="server" Text="Decline Reason :"></asp:Label>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox CssClass="input-field" ID="disbursedLoanDeclineReasonTextBox" runat="server" Width="220px"
                                                                                TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <%-- Tab5 --%>
                                        <asp:View ID="Tab4" runat="server">
                                            <fieldset>
                                                <legend>Search Result</legend>
                                                <table style="width: 100%">
                                                    <tr valign="top">
                                                        <td>
                                                            <div>
                                                                <div style="overflow-y: scroll; height: 220px; z-index: 100; padding-top: 0px; border: solid 1px gray;"
                                                                    visible="true">
                                                                    <asp:GridView CssClass="myGridStyle" ID="cibResultGridView" runat="server" CellPadding="4" ForeColor="#333333"
                                                                        AutoGenerateColumns="False" Font-Size="11px" AllowPaging="True" OnPageIndexChanging="cibResultGridView_PageIndexChanging"
                                                                        PageSize="5" PagerSettings-Mode="NextPreviousFirstLast" PagerSettings-FirstPageText="First"
                                                                        PagerSettings-LastPageText="Last" PagerSettings-NextPageText="Next" PagerSettings-PreviousPageText="Previous">
                                                                        <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="LLID" HeaderStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="cibResultGridViewLlidLabel" runat="server" Text='<%# Eval("LLID") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="5%"></HeaderStyle>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Customer Name" HeaderStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="cibResultGridViewCustomerNameLabel" runat="server" Text='<%# Eval("CustomerName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="18%"></HeaderStyle>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="cibResultGridViewStatusLabel" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%"></HeaderStyle>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="cibResultGridViewRemarksLabel" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%"></HeaderStyle>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Father's Name" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="cibResultGridViewFatherNameLabel" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%"></HeaderStyle>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Mother's Name" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="cibResultGridViewMotherNameLabel" runat="server" Text='<%# Eval("MotherName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="15%"></HeaderStyle>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ReferenceNo." HeaderStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="cibResultGridViewReferenceNoLabel" runat="server" Text='<%# Eval("ReferenceNumber") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="10%"></HeaderStyle>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Type" HeaderStyle-Width="7%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="cibResultGridViewTypeLabel" runat="server" Text='<%# Eval("Type") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="7%"></HeaderStyle>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <HeaderStyle CssClass="FixedHeader" />
                                                                        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast"
                                                                            NextPageText="Next" PreviousPageText="Previous" />
                                                                        <FooterStyle CssClass="FixedFooter" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#FFFFFF" ForeColor="DarkRed" HorizontalAlign="Right" />
                                                                        <EmptyDataTemplate>
                                                                            &nbsp;&nbsp;No data found.
                                                                        </EmptyDataTemplate>
                                                                        <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                                                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                        <%--<EditRowStyle BackColor="#999999" />
                                                                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />--%>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <asp:Label ID="fiveTotalRecordLabel" runat="server" Font-Bold="true" Font-Size="10px"
                                                        Text="Total Record :"></asp:Label>
                                                </div>
                                            </fieldset>
                                        </asp:View>
                                        <%-- Tab6 --%>
                                        <asp:View ID="Tab6" runat="server">
                                            <fieldset>
                                                <legend>Search Result</legend>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:GridView CssClass="myGridStyle" ID="accountNoEntryGridView" runat="server" AutoGenerateColumns="False"
                                                                Font-Size="11px" PageSize="5" Width="500px">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Bank">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="bankEntryLabel" runat="server" Width="120px" Height="15px"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Branch">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="branchEntryLabel" runat="server" Width="130px"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="A/C No.">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="acNoEntryLabel" runat="server" Width="95px"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="A/C Type">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="acTypeEntryLabel" runat="server" Width="85px"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Status">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="statusLabel" runat="server" Width="100px"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="ssHeader" />
                                                                <AlternatingRowStyle BackColor="AliceBlue" />
                                                            </asp:GridView>
                                                            <asp:GridView ID="remarksGridView" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                                                                Width="100px">
                                                                <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="20px" HeaderText="">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="IdCheckBox" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-Width="20px" HeaderText="Id" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-Width="580px" HeaderText="Remarks">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="remarksTextBox" runat="server" CssClass="ssTextBox" Text='<%# Eval("Name") %>'
                                                                                Width="580px"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="ssHeader" />
                                                                <AlternatingRowStyle BackColor="AliceBlue" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <asp:Label ID="sixTotalRecordLabel" runat="server" Font-Bold="true" Font-Size="10px"
                                                        Text="Total Record :"></asp:Label>
                                                </div>
                                            </fieldset>
                                        </asp:View>
                                    </asp:MultiView>


                                    <div id="defareFrame" style="display: none; position: absolute; border: solid 1px gray;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
                                            align="center">
                                            <tr bgcolor="#B70000">
                                                <td></td>
                                                <td align="right">
                                                    <img src="../Images/Close.gif" alt="" width="15" height="15" style="cursor: pointer; cursor: hand;"
                                                        onclick="closeDialog('defareFrame')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div style="overflow-y: scroll; width: 625px; height: 180px; z-index: 5000; border: solid 1px gray;"
                                                        visible="true">
                                                    </div>
                                                    <br />
                                                    <div style="padding-left: 220px;">
                                                        <asp:Button ID="oKButton" runat="server" Text="OK" Width="85px" Height="30px" OnClick="oKButton_Click" />
                                                        <asp:Button ID="cancleButton" runat="server" Text="Cancel" Width="85px" Height="30px"
                                                            OnClick="cancleButton_Click" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td>&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td>&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="padding-left: 300px;">
                                        <br />
                                        <asp:Button CssClass="btn" ID="approvedButton" runat="server" Text="Approved" Width="85px" Height="30px"
                                            Visible="False" />
                                        <asp:Button CssClass="btn clearbtn" ID="declineButton" runat="server" Text="Decline" Width="85px" Height="30px"
                                            OnClick="declineButton_Click" Visible="false" />
                                        <asp:Button CssClass="btn primarybtn" ID="deferButton" runat="server" Text="Defer" Width="85px" Height="30px"
                                            OnClick="deferButton_Click" />
                                        <asp:Button CssClass="btn primarybtn" ID="printButton" runat="server" Text="Print" Width="85px" Height="30px"
                                            OnClick="printButton_Click" />
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="AutoPanel" runat="server" Visible="False">
                                <b>Please Select Status and Decline Reason.<br />
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="text-align: right">Status
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddStatus" runat="server" Width="231px" Height="16px">
                                                    <asp:ListItem Value=""> </asp:ListItem>
                                                    <asp:ListItem Value="N">N</asp:ListItem>
                                                    <asp:ListItem Value="C">C</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">Decline Reason
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox1" runat="server" Height="59px" TextMode="MultiLine" Width="359px"></asp:TextBox>
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="text-align: center">
                                                <br />
                                                <asp:Button ID="bAutoDecline" runat="server" Height="30px" Text="Decline" Width="84px"
                                                    OnClick="bAutoDecline_Click" />
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="bAutoClose" runat="server" Height="30px" Text="Close" Width="67px"
                                                    OnClick="bAutoClose_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                </b>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>



    <div onmouseup="return false" class="translucent" onmousemove="return false" onmousedown="return false"
        id="blockUI" ondblclick="return false" style="display: none; z-index: 50000; left: 0px; position: absolute; top: 0px; background-color: gray"
        onclick="return false">
    </div>
    <br designer:mapid="2979" />

    <div class="xmodal" id="requiredInfoPopupDiv" style="xtop: 125px; height: 300px; _height: 350px; width: 530px; *width: 530px; _width: 530px; top: 15px; left: 10px; display: none">
        <h3>Required Information</h3>
        <div style="padding: 8px; padding-top: 0px;">
            <div style="width: 5.3in; height: auto; text-align: left;">
                <div style="width: 50%; float: left;">
                    <b>Required InformationPlease Select Status and Reason</b>
                </div>
                <table class="reference" style="width: 100%;">
                    <tr>
                        <td class="style1">Status
                        </td>
                        <td>
                            <asp:DropDownList ID="ddStaus" runat="server" Width="177px" Height="16px">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">Decline reason
                        </td>
                        <td>
                            <asp:TextBox ID="tReason" runat="server" Height="80px" TextMode="MultiLine" Width="356px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Label ID="lMsg" runat="server"></asp:Label>
            </div>
        </div>
        <div class="btns" style="vertical-align: bottom;">
            <hr style="width: 98%; left: 0px; display: block;" />
            <br />
            <input id="btnClose" type="button" value="Close" class="btnRight" style="height: 30px; width: 50px; font-weight: bold;" />
            <asp:Button ID="btnSave" CssClass="btnRight" runat="server" Text="Decline" Width="50px"
                Height="30px" Font-Bold="True" OnClick="btnSave_Click" />
        </div>
    </div>
</asp:Content>
