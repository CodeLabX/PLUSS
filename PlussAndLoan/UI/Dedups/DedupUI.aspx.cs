﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_DedupUI : System.Web.UI.Page
    {
        List<string> viewList;
        public LoanInformationManager loanInformationManagerObj = null;
        public LoanInformation loanInformationObj = null;
        List<DeDupInfo> dedupInfoList;
        List<DeDupInfo> tempDedupInfoList;
        public DeDupInfo dedupSaveObj = null;
        //Onlineclear=1,offlineclear=0,offlinenotclear=2
        public string otherStatus1 = string.Empty;
        public string otherStatus2 = string.Empty;
        public string otherStatus3 = string.Empty;
        public string otherStatus4 = string.Empty;
        public string otherStatus5 = string.Empty;
        public Color othStatusForeColor1 = Color.Black;
        public Color othStatusForeColor2 = Color.Black;
        public Color othStatusForeColor3 = Color.Black;
        public Color othStatusForeColor4 = Color.Black;
        public Color othStatusForeColor5 = Color.Black;
        protected void Page_Load(object sender, EventArgs e)
        {
            loanInformationManagerObj = new LoanInformationManager();
            loanInformationObj = new LoanInformation();
            string viewData = "";
            viewList = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                viewData = "";
                viewList.Add(viewData);
            }

            Session["accountNo"] = "";
            if (Page.IsPostBack)
            {

            }
            else
            {
                //accountNoEntryGridView.DataSource = viewList;
                //accountNoEntryGridView.DataBind();

                scbAccountEntryGridView.DataSource = viewList;
                scbAccountEntryGridView.DataBind();

                idEntryGridView.DataSource = viewList;
                idEntryGridView.DataBind();

                contactNoEntryGridView.DataSource = viewList;
                contactNoEntryGridView.DataBind();

                //NegetiveListPerson

                negativeListPersonContactNoGridView.DataSource = viewList;
                negativeListPersonContactNoGridView.DataBind();

                negativeListPersonIdGridView.DataSource = viewList;
                negativeListPersonIdGridView.DataBind();


                negativeListPersonScbAccountGridView.DataSource = viewList;
                negativeListPersonScbAccountGridView.DataBind();

            }
        }
        /// <summary>
        /// Find the loan applicant based on LLID.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void findButton_Click(object sender, EventArgs e)
        {
            CheckUncheckAllCheckBox(false);
            selectAllCheckBox.Checked = false;

            if (String.IsNullOrEmpty(loanLocatorIdTextBox.Text))
            {
                errorMsgLabel.Text = "Please enter LLID no.";
            }
            else
            {
                errorMsgLabel.Text = "";
                loanInformationObj = loanInformationManagerObj.GetParticularApplicantLoanInfo(Convert.ToInt32(loanLocatorIdTextBox.Text));
                if (loanInformationObj == null)
                {
                    errorMsgLabel.Text = "No data found.";
                    ClearTextBoxGridView();
                }
                else
                {
                    errorMsgLabel.Text = "";
                    Session["loanInformationObjS"] = loanInformationObj;
                    applicantNameTextBox0.Text = loanInformationObj.ApplicantName.ToString();
                    fatherNameTextBox10.Text = loanInformationObj.FatherName.ToString();
                    motherNameTextBox0.Text = loanInformationObj.MotherName.ToString();
                    businessNameTextBox0.Text = loanInformationObj.Business.ToString();
                    if (DateFormat.IsDate(loanInformationObj.DOB))
                    {
                        dateOfBirthTextBox0.Text = loanInformationObj.DOB.ToString("dd-MM-yyyy");
                    }
                    else { dateOfBirthTextBox0.Text = ""; }
                    tinNoTextBox.Text = loanInformationObj.TINNo.ToString();

                    (contactNoEntryGridView.Rows[0].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo1.ToString();
                    //contactNoEntryGridView.Rows[0].Cells[0].Text = loanInformationObj.ContactNumber.ContactNo1.ToString();
                    (contactNoEntryGridView.Rows[1].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo2.ToString();
                    (contactNoEntryGridView.Rows[2].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo3.ToString();
                    (contactNoEntryGridView.Rows[3].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo4.ToString();
                    (contactNoEntryGridView.Rows[4].Cells[0].FindControl("contactNoEntryTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo5.ToString();
                    //End ContactGridview

                    //ID Gridview
                    (idEntryGridView.Rows[0].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type1.ToString();
                    (idEntryGridView.Rows[0].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id1.ToString();
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate1))
                    {
                        (idEntryGridView.Rows[0].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate1.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idEntryGridView.Rows[0].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                    }
                    (idEntryGridView.Rows[1].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type2.ToString();
                    (idEntryGridView.Rows[1].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id2.ToString();
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate2))
                    {
                        (idEntryGridView.Rows[1].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate2.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idEntryGridView.Rows[1].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                    }
                    (idEntryGridView.Rows[2].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type3.ToString();
                    (idEntryGridView.Rows[2].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id3.ToString();
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate3))
                    {
                        (idEntryGridView.Rows[2].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate3.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idEntryGridView.Rows[2].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                    }
                    (idEntryGridView.Rows[3].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type4.ToString();
                    (idEntryGridView.Rows[3].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id4.ToString();
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate4))
                    {
                        (idEntryGridView.Rows[3].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate4.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idEntryGridView.Rows[3].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                    }
                    (idEntryGridView.Rows[4].Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Type5.ToString();
                    (idEntryGridView.Rows[4].Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id5.ToString();
                    if (DateFormat.IsDate(loanInformationObj.ApplicantId.ExpiryDate5))
                    {
                        (idEntryGridView.Rows[4].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = loanInformationObj.ApplicantId.ExpiryDate5.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        (idEntryGridView.Rows[4].Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
                    }

                    //End Gridview

                    //SCBAccount Gridview
                    Session["accountNo"] = loanInformationObj.AccountNo.PreAccNo1.ToString();
                    if (loanInformationObj.AccountNo.PreAccNo1.ToString() == "" && loanInformationObj.AccountNo.PostAccNo1.ToString() == "")
                    {
                        (scbAccountEntryGridView.Rows[0].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                        (scbAccountEntryGridView.Rows[0].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo1.ToString();
                        (scbAccountEntryGridView.Rows[0].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                    }
                    else
                    {
                        (scbAccountEntryGridView.Rows[0].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo1.ToString();
                        (scbAccountEntryGridView.Rows[0].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo1.ToString();
                        (scbAccountEntryGridView.Rows[0].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo1.ToString();
                    }
                    if (loanInformationObj.AccountNo.PreAccNo2.ToString() == "" && loanInformationObj.AccountNo.PostAccNo2.ToString() == "")
                    {
                        (scbAccountEntryGridView.Rows[1].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                        (scbAccountEntryGridView.Rows[1].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo2.ToString();
                        (scbAccountEntryGridView.Rows[1].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                    }
                    else
                    {
                        (scbAccountEntryGridView.Rows[1].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo2.ToString();
                        (scbAccountEntryGridView.Rows[1].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo2.ToString();
                        (scbAccountEntryGridView.Rows[1].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo2.ToString();
                    }
                    if (loanInformationObj.AccountNo.PreAccNo3.ToString() == "" && loanInformationObj.AccountNo.PostAccNo3.ToString() == "")
                    {
                        (scbAccountEntryGridView.Rows[2].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                        (scbAccountEntryGridView.Rows[2].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo3.ToString();
                        (scbAccountEntryGridView.Rows[2].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                    }
                    else
                    {
                        (scbAccountEntryGridView.Rows[2].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo3.ToString();
                        (scbAccountEntryGridView.Rows[2].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo3.ToString();
                        (scbAccountEntryGridView.Rows[2].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo3.ToString();
                    }
                    if (loanInformationObj.AccountNo.PreAccNo4.ToString() == "" && loanInformationObj.AccountNo.PostAccNo4.ToString() == "")
                    {
                        (scbAccountEntryGridView.Rows[3].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                        (scbAccountEntryGridView.Rows[3].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo4.ToString();
                        (scbAccountEntryGridView.Rows[3].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                    }
                    else
                    {
                        (scbAccountEntryGridView.Rows[3].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo4.ToString();
                        (scbAccountEntryGridView.Rows[3].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo4.ToString();
                        (scbAccountEntryGridView.Rows[3].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo4.ToString();
                    }
                    if (loanInformationObj.AccountNo.PreAccNo5.ToString() == "" && loanInformationObj.AccountNo.PostAccNo5.ToString() == "")
                    {
                        (scbAccountEntryGridView.Rows[4].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                        (scbAccountEntryGridView.Rows[4].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo5.ToString();
                        (scbAccountEntryGridView.Rows[4].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
                    }
                    else
                    {
                        (scbAccountEntryGridView.Rows[4].Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo5.ToString();
                        (scbAccountEntryGridView.Rows[4].Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo5.ToString();
                        (scbAccountEntryGridView.Rows[4].Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo5.ToString();
                    }
                    //End SCBAccountGridview


                    switch (loanInformationObj.OtherBankAccount.OtherStatus1)
                    {
                        case "0":
                            otherStatus1 = "Offline Clear";
                            othStatusForeColor1 = System.Drawing.Color.Green;
                            break;
                        case "1":
                            otherStatus1 = "Online Clear";
                            othStatusForeColor1 = System.Drawing.Color.Green;
                            break;
                        case "2":
                            otherStatus1 = "Offline Not Clear";
                            othStatusForeColor1 = System.Drawing.Color.Red;
                            break;
                    }
                    switch (loanInformationObj.OtherBankAccount.OtherStatus2)
                    {
                        case "0":
                            otherStatus2 = "Offline Clear";
                            othStatusForeColor2 = System.Drawing.Color.Green;
                            break;
                        case "1":
                            otherStatus2 = "Online Clear";
                            othStatusForeColor2 = System.Drawing.Color.Green;
                            break;
                        case "2":
                            otherStatus2 = "Offline Not Clear";
                            othStatusForeColor2 = System.Drawing.Color.Red;
                            break;
                    }
                    switch (loanInformationObj.OtherBankAccount.OtherStatus3)
                    {
                        case "0":
                            otherStatus3 = "Offline Clear";
                            othStatusForeColor3 = System.Drawing.Color.Green;
                            break;
                        case "1":
                            otherStatus3 = "Online Clear";
                            othStatusForeColor3 = System.Drawing.Color.Green;
                            break;
                        case "2":
                            otherStatus3 = "Offline Not Clear";
                            othStatusForeColor3 = System.Drawing.Color.Red;
                            break;
                    }
                    switch (loanInformationObj.OtherBankAccount.OtherStatus4)
                    {
                        case "0":
                            otherStatus4 = "Offline Clear";
                            othStatusForeColor4 = System.Drawing.Color.Green;
                            break;
                        case "1":
                            otherStatus4 = "Online Clear";
                            othStatusForeColor4 = System.Drawing.Color.Green;
                            break;
                        case "2":
                            otherStatus4 = "Offline Not Clear";
                            othStatusForeColor4 = System.Drawing.Color.Red;
                            break;
                    }
                    switch (loanInformationObj.OtherBankAccount.OtherStatus5)
                    {
                        case "0":
                            otherStatus5 = "Offline Clear";
                            othStatusForeColor5 = System.Drawing.Color.Green;
                            break;
                        case "1":
                            otherStatus5 = "Online Clear";
                            othStatusForeColor5 = System.Drawing.Color.Green;
                            break;
                        case "2":
                            otherStatus5 = "Offline Not Clear";
                            othStatusForeColor5 = System.Drawing.Color.Red;
                            break;
                    }
                    //Other BankAccount Gridview
                    //(accountNoEntryGridView.Rows[0].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank1.ToString();
                    //(accountNoEntryGridView.Rows[0].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch1.ToString();
                    //(accountNoEntryGridView.Rows[0].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo1.ToString();
                    //(accountNoEntryGridView.Rows[0].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType1.ToString();
                    //(accountNoEntryGridView.Rows[0].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus1;
                    //accountNoEntryGridView.Rows[0].Cells[4].ForeColor = othStatusForeColor1;
                    //(accountNoEntryGridView.Rows[1].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank2.ToString();
                    //(accountNoEntryGridView.Rows[1].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch2.ToString();
                    //(accountNoEntryGridView.Rows[1].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo2.ToString();
                    //(accountNoEntryGridView.Rows[1].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType2.ToString();
                    //(accountNoEntryGridView.Rows[1].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus2;
                    //accountNoEntryGridView.Rows[1].Cells[4].ForeColor = othStatusForeColor2;
                    //(accountNoEntryGridView.Rows[2].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank3.ToString();
                    //(accountNoEntryGridView.Rows[2].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch3.ToString();
                    //(accountNoEntryGridView.Rows[2].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo3.ToString();
                    //(accountNoEntryGridView.Rows[2].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType3.ToString();
                    //(accountNoEntryGridView.Rows[2].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus3;
                    //accountNoEntryGridView.Rows[2].Cells[4].ForeColor = othStatusForeColor3;
                    //(accountNoEntryGridView.Rows[3].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank4.ToString();
                    //(accountNoEntryGridView.Rows[3].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch4.ToString();
                    //(accountNoEntryGridView.Rows[3].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo4.ToString();
                    //(accountNoEntryGridView.Rows[3].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType4.ToString();
                    //(accountNoEntryGridView.Rows[3].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus4;
                    //accountNoEntryGridView.Rows[3].Cells[4].ForeColor = othStatusForeColor4;
                    //(accountNoEntryGridView.Rows[4].Cells[0].FindControl("bankEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBank5.ToString();
                    //(accountNoEntryGridView.Rows[4].Cells[1].FindControl("branchEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherBranch5.ToString();
                    //(accountNoEntryGridView.Rows[4].Cells[2].FindControl("acNoEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccNo5.ToString();
                    //(accountNoEntryGridView.Rows[4].Cells[3].FindControl("acTypeEntryLabel") as Label).Text = loanInformationObj.OtherBankAccount.OtherAccType5.ToString();
                    //(accountNoEntryGridView.Rows[4].Cells[4].FindControl("statusLabel") as Label).Text = otherStatus5;
                    //accountNoEntryGridView.Rows[4].Cells[4].ForeColor = othStatusForeColor5;
                    //End Gridview
                    // ClearNegativeEmployer();
                }
            }
        }

        /// <summary>
        /// This method Check the all Check box depends  on 
        /// input value.
        /// </summary>
        /// <param name="checkUncheckValue">Takes true or false value.</param>
        private void CheckUncheckAllCheckBox(bool checkUncheckValue)
        {
            if (String.IsNullOrEmpty(applicantNameTextBox0.Text))
            {
                applicantNameCheckbox.Checked = false;
            }
            else
            {
                applicantNameCheckbox.Checked = checkUncheckValue;
            }
            if (String.IsNullOrEmpty(fatherNameTextBox10.Text))
            {
                fatherNameCheckBox.Checked = false;
            }
            else
            {
                fatherNameCheckBox.Checked = checkUncheckValue;
            }
            if (String.IsNullOrEmpty(motherNameTextBox0.Text))
            {
                motherNameCheckbox.Checked = false;
            }
            else
            {
                motherNameCheckbox.Checked = checkUncheckValue;
            }
            if (String.IsNullOrEmpty(dateOfBirthTextBox0.Text))
            {
                dateOfBirthCheckBox0.Checked = false;
            }
            else
            {
                dateOfBirthCheckBox0.Checked = checkUncheckValue;
            }
            if (String.IsNullOrEmpty(businessNameTextBox0.Text))
            {
                businessNameCheckbox.Checked = false;
            }
            else
            {
                businessNameCheckbox.Checked = checkUncheckValue;
            }
            //Contact gridview
            foreach (GridViewRow gvr in contactNoEntryGridView.Rows)
            {
                if (String.IsNullOrEmpty((gvr.FindControl("contactNoEntryTextBox") as TextBox).Text))
                {
                    (gvr.FindControl("contactNoEntryCheckBox") as CheckBox).Checked = false;
                }
                else
                {
                    (gvr.FindControl("contactNoEntryCheckBox") as CheckBox).Checked = checkUncheckValue;
                }
            }
            //Id gridview
            foreach (GridViewRow gvr in idEntryGridView.Rows)
            {
                if (String.IsNullOrEmpty((gvr.FindControl("typeEntryTextBox") as TextBox).Text) && String.IsNullOrEmpty((gvr.FindControl("idNoEntryTextBox") as TextBox).Text))
                {
                    (gvr.FindControl("idEntryCheckBox") as CheckBox).Checked = false;
                }
                else
                {
                    (gvr.FindControl("idEntryCheckBox") as CheckBox).Checked = checkUncheckValue;
                }
            }
            //SCB account gridview
            //SCB account gridview
            string accountNo = "";
            foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
            {

                if (String.IsNullOrEmpty((gvr.FindControl("accountNoEntryTextBox") as TextBox).Text))
                {
                    (gvr.FindControl("scbAccEntryCheckBox") as CheckBox).Checked = false;
                    accountNo += "";
                }
                else
                {
                    (gvr.FindControl("scbAccEntryCheckBox") as CheckBox).Checked = checkUncheckValue;
                    //accountNo += (gvr.FindControl("prefixEntryTextBox") as TextBox).Text + (gvr.FindControl("accountNoEntryTextBox") as TextBox).Text + (gvr.FindControl("postfixEntryTextBox") as TextBox).Text + ",";
                    accountNo += (gvr.FindControl("accountNoEntryTextBox") as TextBox).Text + ",";
                }
            }
            if (accountNo != "")
            {
                Session["accountNo"] = accountNo.Substring(0, accountNo.Length - 1);
            }
            else
            {
                Session["accountNo"] = "";
            }
            if (String.IsNullOrEmpty(tinNoTextBox.Text))
            {
                tinNoCheckBox.Checked = false;
            }
            else
            {
                tinNoCheckBox.Checked = checkUncheckValue;
            }
        }

        /// <summary>
        /// Clear the textbox and gridview when no applicant find.
        /// </summary>
        private void ClearTextBoxGridView()
        {
            applicantNameTextBox0.Text = "";
            fatherNameTextBox10.Text = "";
            motherNameTextBox0.Text = "";
            businessNameTextBox0.Text = "";
            dateOfBirthTextBox0.Text = "";
            tinNoTextBox.Text = "";

            //Contact gridview
            foreach (GridViewRow gvr in contactNoEntryGridView.Rows)
            {
                (gvr.Cells[0].FindControl("contactnoentrytextbox") as TextBox).Text = "";
            }

            //ID gridview
            foreach (GridViewRow gvr in idEntryGridView.Rows)
            {
                (gvr.Cells[0].FindControl("typeEntryTextBox") as TextBox).Text = "";
                (gvr.Cells[1].FindControl("idNoEntryTextBox") as TextBox).Text = "";
                (gvr.Cells[2].FindControl("expiryDateEntyTextBox") as TextBox).Text = "";
            }
            //Scb account gridview
            foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
            {
                (gvr.Cells[0].FindControl("prefixEntryTextBox") as TextBox).Text = "";
                (gvr.Cells[1].FindControl("accountNoEntryTextBox") as TextBox).Text = "";
                (gvr.Cells[2].FindControl("postfixEntryTextBox") as TextBox).Text = "";
            }
            //Other account gridview
            //foreach (GridViewRow gvr in accountNoEntryGridView.Rows)
            //{
            //    (gvr.Cells[0].FindControl("bankEntryLabel") as Label).Text = "";
            //    (gvr.Cells[1].FindControl("branchEntryLabel") as Label).Text = "";
            //    (gvr.Cells[2].FindControl("acNoEntryLabel") as Label).Text = "";
            //    (gvr.Cells[3].FindControl("acTypeEntryLabel") as Label).Text = "";
            //    (gvr.Cells[4].FindControl("statusLabel") as Label).Text = "";
            //}
        }
        protected void applicantNameCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (applicantNameCheckbox.Checked)
            {
                Session["applicantName"] = applicantNameTextBox0.Text;
            }
            else Session["applicantName"] = "";

        }
        protected void fatherNameCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (fatherNameCheckBox.Checked)
            {
                Session["fatherName"] = fatherNameTextBox10.Text;
            }
            else Session["fatherName"] = "";
        }
        protected void motherNameCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (motherNameCheckbox.Checked)
            {
                Session["motherName"] = motherNameTextBox0.Text;
            }
            else Session["motherName"] = "";
        }
        protected void businessNameCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (businessNameCheckbox.Checked)
            {
                Session["organization"] = businessNameTextBox0.Text;
            }
            else Session["organization"] = "";
        }
        protected void selectAllCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (selectAllCheckBox.Checked)
            {
                CheckUncheckAllCheckBox(true);
            }
            else
            {
                CheckUncheckAllCheckBox(false);
            }
        }

        protected void scbAccEntryCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            string accountNo = "";
            foreach (GridViewRow gvr in scbAccountEntryGridView.Rows)
            {

                if ((gvr.FindControl("scbAccEntryCheckBox") as CheckBox).Checked == true)
                {

                    accountNo += (gvr.FindControl("accountNoEntryTextBox") as TextBox).Text + ",";
                }
                else
                {
                    accountNo += "";
                }
            }
            if (accountNo != "")
            {
                Session["accountNo"] = accountNo.Substring(0, accountNo.Length - 1);
            }
            else
            {
                Session["accountNo"] = "";
            }
        }
    }
}
