﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using AzUtilities;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI.Dedups
{
    public partial class UI_DeclineUI : System.Web.UI.Page
    {
        public List<EducationLevel> educationLevelListObj = null;
        public LoanInformation loanInformationObj = null;
        public List<IdType> idTypeListObj = null;
        public LoanApplicationManager loanApplicationManagerObj = null;
        public RemarksManager remarksManagerObj = null;
        Int32 lLId = 0;
        public DeDupInfoManager deDupInfoManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            remarksManagerObj = new RemarksManager();
            loanApplicationManagerObj = new LoanApplicationManager();
            deDupInfoManagerObj = new DeDupInfoManager();
            idTypeListObj = loanApplicationManagerObj.GetAllIdTypes();

            if (Request.QueryString.Count != 0)
            {
                lLId = Convert.ToInt32(Request.QueryString["lLId"].ToString());
            }
            if (lLId > 0)
            {
                negativeListPersonLLIdTextBox.Text = lLId.ToString();
            }

            string viewData = "";
            List<string> viewList = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                viewData = "";
                viewList.Add(viewData);
            }
            if (!IsPostBack)
            {
                scbAccGridView.DataSource = viewList;
                scbAccGridView.DataBind();

                contactGridView.DataSource = viewList;
                contactGridView.DataBind();
                idGridView.DataSource = viewList;
                idGridView.DataBind();

                IdTypeDropDownListItem();
                LoadInitial();
                LoadLoanApplicationData(lLId);
                declineDateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            }
            saveButtor.Attributes.Add("onclick", "return JS_FunctionCheckRequest()");
        }
        private void LoadInitial()
        {
            List<string> maritalStatus = new List<string>();
            maritalStatus.Insert(0, "Married");
            maritalStatus.Insert(1, "Single");
            negativeListPersonMaritalStatusDropDownList.DataSource = maritalStatus;
            negativeListPersonMaritalStatusDropDownList.DataBind();

            List<string> residentialStatus = new List<string>();
            residentialStatus.Insert(0, "Own");
            residentialStatus.Insert(1, "Family Owned");
            residentialStatus.Insert(2, "Others");
            negativeListPersonResidentialStatusDropDownList.DataSource = residentialStatus;
            negativeListPersonResidentialStatusDropDownList.DataBind();

            EducationLevelDropDownListItems();
            ProductDropDownListItem();
            FillRemarksGridView();
        }
        #region FillRemarksGridView
        /// <summary>
        /// This method fill remarks gridview.
        /// </summary>
        private void FillRemarksGridView()
        {
            List<Remarks> remarksListObj = remarksManagerObj.SelectRemarksList(1);
            remarksGridView.DataSource = remarksListObj;
            remarksGridView.DataBind();
        }
        #endregion
        private void EducationLevelDropDownListItems()
        {
            educationLevelListObj = loanApplicationManagerObj.GetAllEducationLevel();
            EducationLevel educationLevelObj = new EducationLevel();
            educationLevelObj.Id = 0;
            educationLevelObj.Name = "Null";
            educationLevelListObj.Add(educationLevelObj);
            negativeListPersonEducationalDropDownList.DataSource = educationLevelListObj;
            negativeListPersonEducationalDropDownList.DataTextField = "Name";
            negativeListPersonEducationalDropDownList.DataValueField = "Name";
            negativeListPersonEducationalDropDownList.DataBind();
        }

        private void ProductDropDownListItem()
        {
            List<Product> productListObj = loanApplicationManagerObj.GetAllProduct();
            Product productObj = new Product();
            productObj.ProductId = 0;
            productObj.ProductName = "Null";
            productListObj.Add(productObj);
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "ProductId";
            productDropDownList.DataSource = productListObj;
            productDropDownList.DataBind();
        }
        private void IdTypeDropDownListItem()
        {
            IdType idTypeObj = new IdType();
            idTypeObj.Id = 0;
            idTypeObj.Name = "Null";
            idTypeListObj.Add(idTypeObj);
            foreach (GridViewRow gvr in idGridView.Rows)
            {
                (gvr.FindControl("idTypeDropDownList") as DropDownList).DataSource = idTypeListObj;
                (gvr.FindControl("idTypeDropDownList") as DropDownList).DataTextField = "Name";
                (gvr.FindControl("idTypeDropDownList") as DropDownList).DataValueField = "Name";
                (gvr.FindControl("idTypeDropDownList") as DropDownList).DataBind();
            }
        }
        private void LoadLoanApplicationData(Int32 lLId)
        {
            LoanInformation loanInformationObj = loanApplicationManagerObj.GetLoanInformationCreate(lLId);
            if (loanInformationObj != null)
            {
                negativeListPersonLLIdTextBox.Text = loanInformationObj.LLId.ToString();
                negativeListPersonApplicantNameTextBox.Text = loanInformationObj.ApplicantName.ToString();
                remarksTextBox.Text = loanInformationObj.REMAR.ToString();
                declineReasonTextBox.Text = loanInformationObj.DECLI_REASO.ToString();

                if (loanInformationObj.DOB.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    negativeListPersonDobTextBox.Text = "";
                }
                else
                {
                    negativeListPersonDobTextBox.Text = loanInformationObj.DOB.ToString("dd-MM-yyyy");
                }

                negativeListPersonFatherNameTextBox.Text = loanInformationObj.FatherName.ToString();
                negativeListPersonMotherNameTextBox.Text = loanInformationObj.MotherName.ToString();
                switch (loanInformationObj.EducationalLevel.Trim())
                {
                    case "SSC":
                        negativeListPersonEducationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "HSC":
                        negativeListPersonEducationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "DIP":
                        negativeListPersonEducationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "Graduate":
                        negativeListPersonEducationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "Post-Graduate":
                        negativeListPersonEducationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    case "Others":
                        negativeListPersonEducationalDropDownList.Text = loanInformationObj.EducationalLevel;
                        break;
                    default:
                        negativeListPersonEducationalDropDownList.Text = "Null";
                        break;
                }

                switch (loanInformationObj.MaritalStatus.Trim())
                {
                    case "Married":
                        negativeListPersonMaritalStatusDropDownList.Text = loanInformationObj.MaritalStatus;
                        break;
                    case "Single":
                        negativeListPersonMaritalStatusDropDownList.Text = loanInformationObj.MaritalStatus;
                        break;
                    default:
                        negativeListPersonMaritalStatusDropDownList.Text = "Single";
                        break;
                }


                string a = loanInformationObj.MultiLocatorRemarksID;
                string[] splitted = a.Split(',');
                foreach (GridViewRow gvr in remarksGridView.Rows)
                {
                    Int32 i = 0;
                    for (i = 0; i < splitted.Length; i++)
                    {
                        if ((gvr.FindControl("idLabel") as Label).Text == splitted[i])
                        {
                            (gvr.FindControl("IdCheckBox") as CheckBox).Checked = true;
                            break;
                        }
                    }
                }

                switch (loanInformationObj.ResidentialStatus.Trim())
                {
                    case "Own":
                        negativeListPersonResidentialStatusDropDownList.Text = loanInformationObj.ResidentialStatus;
                        break;
                    case "Family Owned":
                        negativeListPersonResidentialStatusDropDownList.Text = loanInformationObj.ResidentialStatus;
                        break;
                    case "Others":
                        negativeListPersonResidentialStatusDropDownList.Text = loanInformationObj.ResidentialStatus;
                        break;
                    default:
                        negativeListPersonResidentialStatusDropDownList.Text = "Others";
                        break;
                }
                negativeListPersonResidentialAddressTextBox.Text = loanInformationObj.ResidentAddress;
                if (String.IsNullOrEmpty(loanInformationObj.TINNo.ToString()))
                {
                    negativeListPersonTinNoTextBox.Text = "";
                    negativeListPersonTinNo2TextBox.Text = "";
                    negativeListPersonTinNo3TextBox.Text = "";
                }
                else
                {
                    string tin = loanInformationObj.TINNo.ToString();
                    string tinNo = tin.Replace("-", "");
                    negativeListPersonTinNoTextBox.Text = tinNo.Substring(0, 3);
                    negativeListPersonTinNo2TextBox.Text = tinNo.Substring(3, 3);
                    negativeListPersonTinNo3TextBox.Text = tinNo.Substring(6);
                }
                negativeListPersonCompanyNameTextBox.Text = loanInformationObj.Business;
                negativeListPersonOfficeAddressTextBox.Text = loanInformationObj.OfficeAddress;

                try     
                {
                    (contactGridView.Rows[0].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo1;
                    (contactGridView.Rows[1].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo2;
                    (contactGridView.Rows[2].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo3;
                    (contactGridView.Rows[3].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo4;
                    (contactGridView.Rows[4].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = loanInformationObj.ContactNumber.ContactNo5;

                }
                catch { }

                (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo1;
                (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo1;
                (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo1;

                (scbAccGridView.Rows[1].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo2;
                (scbAccGridView.Rows[1].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo2;
                (scbAccGridView.Rows[1].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo2;

                (scbAccGridView.Rows[2].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo3;
                (scbAccGridView.Rows[2].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo3;
                (scbAccGridView.Rows[2].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo3;

                (scbAccGridView.Rows[3].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo3;
                (scbAccGridView.Rows[3].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo3;
                (scbAccGridView.Rows[3].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo3;

                (scbAccGridView.Rows[4].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = loanInformationObj.AccountNo.PreAccNo4;
                (scbAccGridView.Rows[4].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = loanInformationObj.AccountNo.MasterAccNo4;
                (scbAccGridView.Rows[4].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = loanInformationObj.AccountNo.PostAccNo4;


                bool idTyeFlag1 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (loanInformationObj.ApplicantId.Type1.Trim() == idTypeListObj[i].Name)
                    {
                        idTyeFlag1 = true;
                        break;
                    }
                }
                if (idTyeFlag1)
                {
                    (idGridView.Rows[0].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type1;
                }
                else
                {
                    (idGridView.Rows[0].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "Null";
                }

                (idGridView.Rows[0].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id1;
                bool idTyeFlag2 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (loanInformationObj.ApplicantId.Type2.Trim() == idTypeListObj[i].Name)
                    {
                        idTyeFlag2 = true;
                        break;
                    }
                }
                if (idTyeFlag2)
                {
                    (idGridView.Rows[1].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type2;
                }
                else
                {
                    (idGridView.Rows[1].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "Null";
                }
                (idGridView.Rows[1].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id2;

                bool idTyeFlag3 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (loanInformationObj.ApplicantId.Type3.Trim() == idTypeListObj[i].Name)
                    {
                        idTyeFlag3 = true;
                        break;
                    }
                }
                if (idTyeFlag3)
                {
                    (idGridView.Rows[2].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type3;
                }
                else
                {
                    (idGridView.Rows[2].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "Null";
                }
                (idGridView.Rows[2].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id3;
                bool idTyeFlag4 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (loanInformationObj.ApplicantId.Type4.Trim() == idTypeListObj[i].Name)
                    {
                        idTyeFlag4 = true;
                        break;
                    }
                }
                if (idTyeFlag4)
                {
                    (idGridView.Rows[3].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type4;
                }
                else
                {
                    (idGridView.Rows[3].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "Null";
                }
                (idGridView.Rows[3].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id4;
                bool idTyeFlag5 = false;
                for (int i = 0; i < idTypeListObj.Count; i++)
                {
                    if (loanInformationObj.ApplicantId.Type5.Trim() == idTypeListObj[i].Name)
                    {
                        idTyeFlag5 = true;
                        break;
                    }
                }
                if (idTyeFlag5)
                {
                    (idGridView.Rows[4].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = loanInformationObj.ApplicantId.Type5;
                }
                else
                {
                    (idGridView.Rows[4].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedValue = "Null";
                }
                (idGridView.Rows[4].Cells[1].FindControl("idNoTextBox") as TextBox).Text = loanInformationObj.ApplicantId.Id5;
                productDropDownList.SelectedValue = Convert.ToString(loanInformationObj.ProductId);

                sourchTextBox.Text = loanInformationObj.SourcePersonName.ToString();

            }
        }
        protected void clearButtor_Click(object sender, EventArgs e)
        {
            FildClear();
        }
        private void FildClear()
        {
            negativeListPersonLLIdTextBox.Text = "";
            negativeListPersonApplicantNameTextBox.Text = "";
            negativeListPersonDobTextBox.Text = "";
            negativeListPersonFatherNameTextBox.Text = "";
            negativeListPersonMotherNameTextBox.Text = "";
            negativeListPersonEducationalDropDownList.SelectedIndex = 0;
            negativeListPersonMaritalStatusDropDownList.SelectedIndex = 0;
            negativeListPersonResidentialStatusDropDownList.SelectedIndex = 0;
            negativeListPersonResidentialAddressTextBox.Text = "";
            negativeListPersonTinNoTextBox.Text = "";
            negativeListPersonTinNo2TextBox.Text = "";
            negativeListPersonTinNo3TextBox.Text = "";

            negativeListPersonCompanyNameTextBox.Text = "";
            negativeListPersonOfficeAddressTextBox.Text = "";
            (contactGridView.Rows[0].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = "";
            (contactGridView.Rows[1].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = "";
            (contactGridView.Rows[2].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = "";
            (contactGridView.Rows[3].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = "";
            (contactGridView.Rows[4].Cells[0].FindControl("contactNoTextBox") as TextBox).Text = "";

            (scbAccGridView.Rows[0].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[0].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[0].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = "";

            (scbAccGridView.Rows[1].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[1].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[1].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = "";

            (scbAccGridView.Rows[2].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[2].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[2].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = "";

            (scbAccGridView.Rows[3].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[3].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[3].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = "";

            (scbAccGridView.Rows[4].Cells[0].FindControl("scbPreTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[4].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text = "";
            (scbAccGridView.Rows[4].Cells[2].FindControl("scbPostTextBox") as TextBox).Text = "";

            (idGridView.Rows[0].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedIndex = 0;

            (idGridView.Rows[0].Cells[1].FindControl("idNoTextBox") as TextBox).Text = "";
            (idGridView.Rows[1].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedIndex = 0;
            (idGridView.Rows[1].Cells[1].FindControl("idNoTextBox") as TextBox).Text = "";
            (idGridView.Rows[2].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedIndex = 0;
            (idGridView.Rows[2].Cells[1].FindControl("idNoTextBox") as TextBox).Text = "";
            (idGridView.Rows[3].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedIndex = 0;
            (idGridView.Rows[3].Cells[1].FindControl("idNoTextBox") as TextBox).Text = "";
            (idGridView.Rows[4].Cells[0].FindControl("idTypeDropDownList") as DropDownList).SelectedIndex = 0;
            (idGridView.Rows[4].Cells[1].FindControl("idNoTextBox") as TextBox).Text = "";
            productDropDownList.SelectedIndex = 0;
            sourchTextBox.Text = "";
            declineDateTextBox.Text = DateTime.Now.ToString("dd-MM-yyyy");
            declineReasonTextBox.Text = "";
            remarksTextBox.Text = "";
        }

        protected void saveButtor_Click(object sender, EventArgs e)
        {
            LogFile.WriteLine("Testing line 422  :: " + "Decline Start");
            SendData();
            LogFile.WriteLine("Testing line 424  :: " + "Decline End");
        }
        private void SendData()
        {
            try
            {
                List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
                DeDupInfo deDupInfoObj = new DeDupInfo();
                string masterAccNo = "";
                deDupInfoObj.Name = negativeListPersonApplicantNameTextBox.Text;
                if (!String.IsNullOrEmpty(negativeListPersonDobTextBox.Text.Trim()))
                {
                    try
                    {
                        deDupInfoObj.DateOfBirth = Convert.ToDateTime(negativeListPersonDobTextBox.Text.Trim(),new CultureInfo("ru-RU"));
                    }
                    catch
                    {
                    }
                }
                deDupInfoObj.FatherName = negativeListPersonFatherNameTextBox.Text;
                deDupInfoObj.MotherName = negativeListPersonMotherNameTextBox.Text;
                deDupInfoObj.EducationalLevel = negativeListPersonEducationalDropDownList.Text;
                deDupInfoObj.MaritalStatus = negativeListPersonMaritalStatusDropDownList.Text;
                deDupInfoObj.ResidentaileStatus = negativeListPersonResidentialStatusDropDownList.Text;
                deDupInfoObj.Address2 = negativeListPersonResidentialAddressTextBox.Text;
                deDupInfoObj.Tin = negativeListPersonTinNoTextBox.Text + negativeListPersonTinNo2TextBox.Text + negativeListPersonTinNo3TextBox.Text;

                deDupInfoObj.BusinessEmployeeName = negativeListPersonCompanyNameTextBox.Text;
                deDupInfoObj.Address1 = negativeListPersonOfficeAddressTextBox.Text;
                deDupInfoObj.Phone1 = (contactGridView.Rows[0].Cells[0].FindControl("contactNoTextBox") as TextBox).Text;
                deDupInfoObj.Phone2 = (contactGridView.Rows[1].Cells[0].FindControl("contactNoTextBox") as TextBox).Text;
                deDupInfoObj.Mobile1 = (contactGridView.Rows[2].Cells[0].FindControl("contactNoTextBox") as TextBox).Text;
                deDupInfoObj.Mobile2 = (contactGridView.Rows[3].Cells[0].FindControl("contactNoTextBox") as TextBox).Text;

                for (int i = 0; i <= 4; i++)
                {
                    if ((scbAccGridView.Rows[i].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text != "")
                    {
                        masterAccNo += (scbAccGridView.Rows[i].Cells[0].FindControl("scbPreTextBox") as TextBox).Text;
                        masterAccNo += (scbAccGridView.Rows[i].Cells[1].FindControl("scbMasterTextBox") as TextBox).Text;
                        masterAccNo += (scbAccGridView.Rows[i].Cells[2].FindControl("scbPostTextBox") as TextBox).Text;
                        masterAccNo += ",";
                    }
                    else
                    {
                        masterAccNo += "";
                    }

                }
                if (masterAccNo!="")
                {
                    if (masterAccNo.Length > 1)
                    {
                        masterAccNo = masterAccNo.Substring(0, masterAccNo.Length - 1);
                    }
                }
                deDupInfoObj.ScbMaterNo = masterAccNo.ToString();

                deDupInfoObj.IdType = (idGridView.Rows[0].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
                deDupInfoObj.IdNo = (idGridView.Rows[0].Cells[1].FindControl("idNoTextBox") as TextBox).Text;
                deDupInfoObj.IdType1 = (idGridView.Rows[1].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
                deDupInfoObj.IdNo1 = (idGridView.Rows[1].Cells[1].FindControl("idNoTextBox") as TextBox).Text;
                deDupInfoObj.IdType2 = (idGridView.Rows[2].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
                deDupInfoObj.IdNo2 = (idGridView.Rows[2].Cells[1].FindControl("idNoTextBox") as TextBox).Text;
                deDupInfoObj.IdType3 = (idGridView.Rows[3].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
                deDupInfoObj.IdNo3 = (idGridView.Rows[3].Cells[1].FindControl("idNoTextBox") as TextBox).Text;
                deDupInfoObj.IdType4 = (idGridView.Rows[4].Cells[0].FindControl("idTypeDropDownList") as DropDownList).Text;
                deDupInfoObj.IdNo4 = (idGridView.Rows[4].Cells[1].FindControl("idNoTextBox") as TextBox).Text;
                deDupInfoObj.ProductId = Convert.ToInt32('0' + productDropDownList.SelectedValue);
                deDupInfoObj.Source = sourchTextBox.Text;
                deDupInfoObj.BankBranch = bankBranchTextBox.Text;
                if (!String.IsNullOrEmpty(declineDateTextBox.Text.Trim()))
                {
                    try
                    {
                        deDupInfoObj.DeclineDate = DateTime.Now;//Convert.ToDateTime(declineDateTextBox.Text.Trim());
                    }
                    catch { }
                }
                deDupInfoObj.DeclineReason = declineReasonTextBox.Text;
                deDupInfoObj.Remarks = remarksTextBox.Text;
                deDupInfoObj.Status = negativeListPersonStatusDropDownList.Text;
                deDupInfoObj.Active = "Y";
                deDupInfoObj.LoanAccountNo = negativeListPersonLLIdTextBox.Text;
                deDupInfoObj.CriteriaType = negativeListPersonStatusDropDownList.Text.ToString();
                deDupInfoObj.EntryDate = DateTime.Now;
                deDupInfoObjList.Add(deDupInfoObj);

                int ListInsert = 0;
                int countCheckedCheckBox = 0;
                foreach (GridViewRow gvr in remarksGridView.Rows)
                {
                    if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked == true)
                    {
                        countCheckedCheckBox++;
                        break;
                    }
                }

                var checkbox = "";
                List<Remarks> remarksListObj = new List<Remarks>();
                List<Int32> remarksIdListObj = new List<Int32>();
                foreach (GridViewRow gvr in remarksGridView.Rows)
                {
                    if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked == true)
                    {
                        Remarks remarksObj = new Remarks();
                        Int32 remarksIdObj;
                        remarksIdObj = Convert.ToInt32((gvr.FindControl("idLabel") as Label).Text);
                        remarksIdListObj.Add(remarksIdObj);
                        remarksObj.Id = remarksIdObj;

                        remarksObj.Name = (gvr.FindControl("remarksTextBox") as TextBox).Text;
                        checkbox += checkbox == "" ? remarksIdObj.ToString() : "," + remarksIdObj.ToString();
                        remarksListObj.Add(remarksObj);

                    }
                }
                deDupInfoObj.MultiLocatorRemarksID = checkbox;


                if (countCheckedCheckBox > 0)
                {
                    LogFile.WriteLine("Testing line 525  :: " + "Decline");
                    User user = (User)Session["UserDetails"];
                    LogFile.WriteLine("Testing line 550  :: " + user);
                    if (negativeListPersonStatusDropDownList.Text != "")
                    {
                        LogFile.WriteLine("Testing line 530  :: " + "Decline");
                        ListInsert = deDupInfoManagerObj.SendDedupDeclineInfoInToDB(deDupInfoObjList, user);

                        if (ListInsert == INSERTE || ListInsert == UPDATE)
                        {
                            //UpdateStatus();
                            new AT(this).AuditAndTraial("Decline", "Save or Update Decline Make");
                            //Alert.Show("Decline Sucess!");
                            //modalPopup.Visible = false;
                        }
                        else
                        {
                            new AT(this).AuditAndTraial("Decline", "Decline Make Failed!");
                            Alert.Show("Decline Failed!");
                        }
                    }
                    else
                    {
                        LogFile.WriteLine("Testing line 548  :: " + "Decline asi");

                        LogFile.WriteLine("Testing line 548 User :: " + user.UserId);

                        LogFile.WriteLine("Testing line 548 list :: " + deDupInfoObjList);

                        LogFile.WriteLine("Testing line 548 Count :: " + deDupInfoObjList.Count);
                        //UpdateStatus();
                        deDupInfoManagerObj.SendDedupDeclineInfoInToDB(deDupInfoObjList, user);

                        LogFile.WriteLine("Testing line 550  :: " + "Decline asi");
                        modalPopup.ShowSuccess("Person Status Has Been Made!", Title);
                    }
                }
                else
                {
                    modalPopup.ShowSuccess("PLease Select Locator Remarks", Title);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Send");
                LogFile.WriteLine("Testing line 563  :: " + exception);
            }
        }

        private void UpdateStatus()
        {
            try
            {
                int llid = Convert.ToInt32(negativeListPersonLLIdTextBox.Text);
                int declineFlag = 2;
                if (negativeListPersonStatusDropDownList.Text != "")
                {
                    if (negativeListPersonStatusDropDownList.Text.Equals("N"))
                    {
                        declineFlag = 2;
                    }
                    else
                    {
                        declineFlag = 17;
                    }
                }
                else
                {
                    declineFlag = 17;
                }
                int updateFlag = 1;
                if (declineFlag != 17)
                {
                    updateFlag = loanApplicationManagerObj.SetForwardStatus(llid, declineFlag, (string)Session["UserId"]);
                }
                if (updateFlag > -3)
                {
                    int deleteFlag = loanApplicationManagerObj.DeleteDeferReason(llid);
                    if (!String.IsNullOrEmpty(deleteFlag.ToString()))
                    {
                        List<Remarks> remarksListObj = new List<Remarks>();
                        List<Int32> remarksIdListObj = new List<Int32>();
                        foreach (GridViewRow gvr in remarksGridView.Rows)
                        {
                            if ((gvr.FindControl("IdCheckBox") as CheckBox).Checked)
                            {
                                Remarks remarksObj = new Remarks();
                                Int32 remarksIdObj;
                                remarksIdObj = Convert.ToInt32((gvr.FindControl("idLabel") as Label).Text);
                                remarksIdListObj.Add(remarksIdObj);
                                remarksObj.Id = remarksIdObj;
                                remarksObj.Name = (gvr.FindControl("remarksTextBox") as TextBox).Text;
                                remarksListObj.Add(remarksObj);
                            }
                        }
                        int insertFlag = loanApplicationManagerObj.InsertDeferReason(llid, remarksIdListObj);
                        if (insertFlag > -1)
                        {
                            LoanApplicationDetails loanApplicationDetailsObj = new LoanApplicationDetails();
                            loanApplicationDetailsObj.LAId = llid;

                            loanApplicationDetailsObj.PersonId = (string)Session["UserId"].ToString();
                            loanApplicationDetailsObj.DeptId = 4;
                            if (negativeListPersonStatusDropDownList.Text != "")
                            {
                                if (negativeListPersonStatusDropDownList.Text.Equals("N"))
                                {
                                    loanApplicationDetailsObj.ProcessStatusId = 2;
                                }
                                else
                                {
                                    loanApplicationDetailsObj.ProcessStatusId = 17;
                                }
                            }
                            else
                            {
                                loanApplicationDetailsObj.ProcessStatusId = 17;
                            }

                            int insertLoanAppDetailsFlag = loanApplicationManagerObj.InsertDeferReasonDetails(loanApplicationDetailsObj, remarksListObj);
                            if (insertLoanAppDetailsFlag > 0)
                            {
                                modalPopup.ShowSuccess("Declined Successfully", Title);
                                FildClear();
                            }
                            else
                            {
                                modalPopup.ShowError("Decline Failed! Please Check Status in the Loan Locator System", Title);
                            }
                        }
                    }
                }
                else
                {
                    Alert.Show("Decline Failed! Please Check Status in the Loan Locator System");
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Update Data");
            }
        }

    }
}
