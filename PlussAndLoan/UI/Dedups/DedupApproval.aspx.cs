﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PlussAndLoan.UI.Dedups
{
    public partial class DedupApproval : System.Web.UI.Page
    {
        private DeDupInfoManager _manager;
        protected void Page_Load(object sender, EventArgs e)
        {
            InitVariables();///------------------

            if (!Page.IsPostBack)
            {
                InitGrid();
            }
        }

        private void InitVariables()
        {
            _manager = new DeDupInfoManager();

        }

        private void InitGrid()
        {
            DataTable table = _manager.GetDeDupDeclinedData();

            DedupDeclineInfo.DataSource = table;
            DedupDeclineInfo.DataBind();
        }
        protected void DedupDeclineInfo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string str = e.CommandArgument.ToString().Trim();

            string[] splitted = str.Split('-');

            if (e.CommandName == "Select") {

                if (splitted.Length > 0)
                {
                    string targetURL = "DeclineApprovalUI.aspx?llId=" + splitted[0].Trim() + "&deDupId=" + splitted[1].Trim();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "declineButton", "<script type='text/javascript'>window.open('" + targetURL + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1')</script>", false);
                }

            }
            else if (e.CommandName == "Delete") {
                string deleteDeclinetemp = "";
                string DE_DUP_ID = splitted[1].Trim();
                DeDupInfoManager deDupInfoManagerObj = new DeDupInfoManager();
                deleteDeclinetemp = deDupInfoManagerObj.DeleteDeclineTemp(DE_DUP_ID);


            }

            
        }
    }
}