﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AzUtilities;
using BLL;
using BusinessEntities;
using DAL;
using DAL.SecurityMatrixGateways;

namespace PlussAndLoan.UI
{
    public partial class LoanLocatorAdmin : System.Web.UI.MasterPage
    {
        public UserManager userManagerObj = null;
        private UserAccessLogManager userAccessLogManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            userAccessLogManagerObj = new UserAccessLogManager();
            userManagerObj = new UserManager();
            //userIdHiddenField.Value = (string)Session["UserId"];

            //for session kill
            var loginId = (string)Session["UserId"];
            userIdHiddenField.Value = loginId;


            _permissions = new PageAccessPermission();

            User userT = (User)Session["UserDetails"];

            if (!_permissions.HasUserAccess(Context, userT))
                Response.Redirect("~/LoginUI.aspx");

            DataTable dt = _permissions.BindMenuData(0, userT);
            _permissions.DynamicMenuControlPopulation(dt, 0, null, MenuSpace, userT);


            var user = LoginUser.LoginUserToSystem.Where(u => u.UserId == loginId);
            if (!user.Any())
            {
                Response.Redirect("~/LoginUI.aspx");
            }
            //-------end

            if (Session.Count == 0)
            {
                Response.Redirect("~/ErrorPage/Timout.htm");
            }
            if (!IsPostBack)
            {
                userNameLabel.Text = (string)Session["UserName"];

                short userType = (short)Session["UserType"];
                if (userType.Equals(5))
                {
                    // MainMenu.Items[5].Enabled = true;
                }
                else
                {
                    //MainMenu.Items[5].Enabled = false;
                }
            }
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            var userId = Session["UserId"].ToString();
            LoginUser.LoginUserToSystem.RemoveAll(u => u.UserId == userId);

            SaveAccessLog(userIdHiddenField.Value, 0, 0, 0);
            new AT(this).AuditAndTraial("Logout", "Logout");
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            removeCookie();
            Response.Redirect("~/LoginUI.aspx");
        }

        private void SaveAccessLog(string loginId, int successStatus, int unSuccessStatus, int userLoginStatus)
        {
            int insertAccessLog = 0;
            UserAccessLog userAccessLogObj = new UserAccessLog();
            userAccessLogObj.UserLogInId = loginId.ToString();
            userAccessLogObj.IP = Request.ServerVariables["REMOTE_ADDR"].ToString();
            userAccessLogObj.LogInCount = 0;
            userAccessLogObj.LogInDateTime = DateTime.Now;
            userAccessLogObj.LogOutDateTime = DateTime.Now;
            userAccessLogObj.userLoginSuccessStatus = 0;
            userAccessLogObj.userLoginUnSuccessStatus = 0;
            userAccessLogObj.userLoginStatus = 0;
            userAccessLogObj.AccessFor = "LOGOUT";
            insertAccessLog = userAccessLogManagerObj.SendDataInToDB(userAccessLogObj);
        }

        private void removeCookie()
        {
            HttpCookie myCookie = new HttpCookie("LOGIN");
            Response.Cookies["LOGIN"].Expires = DateTime.Now.AddDays(-365);

            string currentCookieValue = "";
            if (Response.Cookies["LOGIN"].Value == "" && Response.Cookies["LOGIN"].Expires == DateTime.MinValue)
            {
                currentCookieValue = Request.Cookies["LOGIN"].Value;
                Response.Cookies.Remove("LOGIN");
            }
            else
            {
                myCookie = new HttpCookie("LOGIN");
                myCookie.Values.Add("LoginId", "");
                myCookie.Values.Add("status", "0");
                currentCookieValue = Response.Cookies["LOGIN"].Value;
            }
        }
        protected void MainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            string s = e.Item.Text;
        }

        #region Redirect to PLUSS

        private enum USERTYPE : short
        {
            Admin = 1,
            Analyst = 2,
            SupportUser = 3,
            Approver = 4,
            ScoreAdmin = 5,

            //-----------Added by Mazhar-------------
            AutoAdmin = 6,
            AutoApprover = 7,
            AutoAnalyst = 8,
            AutoSupport = 9,
            AutoOperations = 10,
            AutoSales = 11,
            AutoOperationChecker = 12,
            AutoAnalystApprover = 13,

            AdminMaker = 17,
            AdminChecker = 18,
            LoanLocatorOperation = 19,
            LoanLocatorAdmin = 20
        }

        public Int32 userType = 0;
        public Int32 autoUserType = 0;
        private PageAccessPermission _permissions;

        protected void GoToPluss(object o, EventArgs e)
        {
            int both = 0;

            try
            {
                if (Session["UserType"] != null)
                {
                    userType = Convert.ToInt32(Session["UserType"].ToString());
                    both = Convert.ToInt32(Session["both"]);
                }
                if (Session["AutoUserType"] != null)
                {
                    autoUserType = Convert.ToInt32(Session["AutoUserType"].ToString());
                }

                RedirectPage();

            }
            catch (Exception exception)
            {
                new AT(this).AuditAndTraial("switch user", "SW UI -" + exception.Message);
            }
        }

        private void RedirectPage()
        {
            if (userType.Equals((short)USERTYPE.Admin) || userType.Equals((short)USERTYPE.ScoreAdmin))
            {
                Response.Redirect("~/UI/AdminHomeUI.aspx");
            }
            else if (userType.Equals((short)USERTYPE.Analyst) || userType.Equals((short)USERTYPE.Admin) ||
                     userType.Equals((short)USERTYPE.Approver) || userType.Equals((short)USERTYPE.ScoreAdmin))
            {
                Response.Redirect("~/UI/AnalystHomeUI.aspx");
            }
            else if (userType.Equals((short)USERTYPE.SupportUser) || userType.Equals((short)USERTYPE.Admin) ||
                     userType.Equals((short)USERTYPE.Analyst))
            {
                Response.Redirect("~/UI/BlankPage.aspx");
            }
            else if (userType.Equals((short)USERTYPE.AdminMaker) ||
                     userType.Equals((short)USERTYPE.AdminChecker))
            {
                Response.Redirect("~/UI/ITAdminHome.aspx");

            }
        }

        #endregion
    }
}