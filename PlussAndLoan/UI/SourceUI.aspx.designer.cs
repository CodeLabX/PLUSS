﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace PlussAndLoan.UI {
    
    
    public partial class UI_SourceUI {
        
        /// <summary>
        /// dhtmlxgrid_codebase control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PlussAndLoan.UI.UserControls.css.UI_UserControls_css_dhtmlxgrid_codebase dhtmlxgrid_codebase;
        
        /// <summary>
        /// dhtmlxgrid_skinscss control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PlussAndLoan.UI.UserControls.css.UI_UserControls_css_dhtmlxgrid_skinscss dhtmlxgrid_skinscss;
        
        /// <summary>
        /// dhtmlxcommon control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PlussAndLoan.UI.UserControls.js.UI_UserControls_js_dhtmlxcommon dhtmlxcommon;
        
        /// <summary>
        /// dhtmlxgrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PlussAndLoan.UI.UserControls.js.UI_UserControls_js_dhtmlxgrid dhtmlxgrid;
        
        /// <summary>
        /// dhtmlxgrid_srnd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PlussAndLoan.UI.UserControls.js.UI_UserControls_js_dhtmlxgrid_srnd dhtmlxgrid_srnd;
        
        /// <summary>
        /// dhtmlxgridcell control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::PlussAndLoan.UI.UserControls.js.UI_UserControls_js_dhtmlxgridcell dhtmlxgridcell;
        
        /// <summary>
        /// Label1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;
        
        /// <summary>
        /// sourceIdLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label sourceIdLabel;
        
        /// <summary>
        /// sourceNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label sourceNameLabel;
        
        /// <summary>
        /// Label6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label6;
        
        /// <summary>
        /// regionNameLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label regionNameLabel;
        
        /// <summary>
        /// xmlDataHiddenField control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField xmlDataHiddenField;
        
        /// <summary>
        /// dvXml control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl dvXml;
    }
}
