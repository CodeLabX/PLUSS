﻿using System;
using System.Web;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_UpdateLoginUserStatus : System.Web.UI.Page
    {
        public UserManager userManagerObj = null;
        private UserAccessLogManager userAccessLogManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            userManagerObj = new UserManager();
            userAccessLogManagerObj = new UserAccessLogManager();
            if (!Page.IsPostBack)
            {
                string loginId = Request.QueryString["loginUserId"].ToString();
                if (loginId.Length > 0)
                {
                    SaveAccessLog(loginId, 0, 0, 0);
                }
            }
        }
        #region
        private void removeCookie()
        {
            HttpCookie myCookie = new HttpCookie("LOGIN");
            Response.Cookies["LOGIN"].Expires = DateTime.Now.AddDays(-365);

            string currentCookieValue = "";
            if (Response.Cookies["LOGIN"].Value == "" && Response.Cookies["LOGIN"].Expires == DateTime.MinValue)
            {
                currentCookieValue = Request.Cookies["LOGIN"].Value;
                Response.Cookies.Remove("LOGIN");
            }
            else
            {
                myCookie = new HttpCookie("LOGIN");
                myCookie.Values.Add("LoginId", "");
                myCookie.Values.Add("status", "0");
                currentCookieValue = Response.Cookies["LOGIN"].Value;
            }
        }
        #endregion
        private void SaveAccessLog(string loginId, int successStatus, int unSuccessStatus, int userLoginStatus)
        {
            try
            {
                int insertAccessLog = 0;
                UserAccessLog userAccessLogObj = new UserAccessLog();
                userAccessLogObj.UserLogInId = loginId.ToString();
                userAccessLogObj.IP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                userAccessLogObj.LogInCount = 0;
                userAccessLogObj.LogInDateTime = DateTime.Now;
                userAccessLogObj.LogOutDateTime = DateTime.Now;
                userAccessLogObj.userLoginSuccessStatus = 0;
                userAccessLogObj.userLoginUnSuccessStatus = 0;
                userAccessLogObj.userLoginStatus = 0;
                userAccessLogObj.AccessFor = "LOGOUT";
                removeCookie();//tarek
                insertAccessLog = userAccessLogManagerObj.SendDataInToDB(userAccessLogObj);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Upload Login User Status");
            }
        
        }
    }
}
