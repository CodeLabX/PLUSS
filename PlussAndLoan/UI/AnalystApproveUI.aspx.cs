﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_AnalystApproveUI : System.Web.UI.Page
    {
        public AnalystApproveManager analystApproveManagerObj = null;
        public ApproverManager approverManagerObj = null;
        public LoanApplicationManager loanApplicationManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetAllowResponseInBrowserHistory(false);        
            analystApproveManagerObj = new AnalystApproveManager();
            approverManagerObj = new ApproverManager();
            loanApplicationManagerObj = new LoanApplicationManager();
            Int32 UserId = Convert.ToInt32(Session["Id"].ToString());
            loanAmountTextBox.Text = Request[this.loanAmountTextBox.UniqueID];
            loanTypeTextBox.Text = Request[this.loanTypeTextBox.UniqueID];
            resultTextBox.Text = Request[this.resultTextBox.UniqueID];
            lavelTextBox.Text = Request[this.lavelTextBox.UniqueID];
            scoreTextBox.Text = Request[this.scoreTextBox.UniqueID];
            customerNameTextBox.Text = Request[this.customerNameTextBox.UniqueID];
            decisionDropDownList.Text = Request[this.decisionDropDownList.UniqueID];
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();", true);
            searchTextBox.Attributes.Add("onkeyup", "DataSearch();");
            //LoadData(UserId);

            string searchKey = searchTextBox.Text;
            LoadData(UserId, searchKey);
        }

     
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                double existingLoanAmount = 0;
                double totalLoanAmount = 0;
                int analystApproveInsert = 0;
                int updateStatus = 0;
                Int32 compairValue = 0;
                AnalystApprove analystApproveObj = new AnalystApprove();
                var msg = "";
                if (IsBalnk())
                {
                    Approver approverObj = approverManagerObj.GetApproverLimit(Convert.ToInt32(Session["Id"].ToString()), loanTypeTextBox.Text);
                    if (approverObj != null)
                    {
                        switch (lavelTextBox.Text.Trim().ToUpper())
                        {
                            case "L1":
                                compairValue = approverObj.Level1Amt;
                                break;
                            case "L2":
                                compairValue = approverObj.Level2Amt;
                                break;
                            default:
                                compairValue = approverObj.Level3Amt;
                                break;
                        }
                        existingLoanAmount = analystApproveManagerObj.GetCustomerTotalExistingLoanAmount(Convert.ToInt64(lLIdTextBox.Text));
                        totalLoanAmount = Convert.ToDouble(loanAmountTextBox.Text) + existingLoanAmount;
                        if (compairValue >= totalLoanAmount)
                        {
                            analystApproveObj.Id = 0;
                            analystApproveObj.LlId = Convert.ToInt64(lLIdTextBox.Text);
                            analystApproveObj.Score = Convert.ToInt64(scoreTextBox.Text);
                            analystApproveObj.CustomerName = customerNameTextBox.Text;
                            analystApproveObj.LoanAmount = Convert.ToDouble("0" + loanAmountTextBox.Text);
                            analystApproveObj.Level = lavelTextBox.Text;
                            analystApproveObj.LoanTag = loanTypeTextBox.Text;
                            analystApproveObj.Result = resultTextBox.Text;
                            analystApproveObj.Decision = decisionDropDownList.Text;
                            analystApproveObj.Remarks = remarksTextBox.Text;
                            analystApproveObj.UserId = Convert.ToInt32(Session["Id"].ToString());
                            analystApproveObj.EntryDate = DateTime.Now;
                            analystApproveInsert = analystApproveManagerObj.SendAnalystApproveInToDB(analystApproveObj);
                            if (resultTextBox.Text.ToUpper().Trim() == "APPROVED" && decisionDropDownList.Text == "Approved")
                            {
                                updateStatus = 5;
                            }
                            else if (resultTextBox.Text.ToUpper().Trim() == "REJECTED" && decisionDropDownList.Text == "Approved")
                            {
                                updateStatus = 15;
                            }
                            else
                            {
                                updateStatus = 2;
                            }
                            if (analystApproveInsert == INSERTE)
                            {
                                analystApproveManagerObj.UpdateLoanLocatorLoanStatus(analystApproveObj.LlId, updateStatus, Convert.ToDouble("0" + loanAmountTextBox.Text));
                                UpdateLoanStatusAndDetails(updateStatus);
                                analystApproveManagerObj.UpdatePlussLoanStatus(analystApproveObj.LlId, updateStatus);
                                analystApproveManagerObj.UpdatePLStatus(analystApproveObj.LlId);
                                msg = "Save Successfuly";
                                //LoadData(analystApproveObj.UserId);
                                FieldClear();
                            }
                            else if (analystApproveInsert == UPDATE)
                            {
                                analystApproveManagerObj.UpdateLoanLocatorLoanStatus(analystApproveObj.LlId, updateStatus, Convert.ToDouble("0" + loanAmountTextBox.Text));
                                UpdateLoanStatusAndDetails(updateStatus);
                                analystApproveManagerObj.UpdatePlussLoanStatus(analystApproveObj.LlId, updateStatus);
                                analystApproveManagerObj.UpdatePLStatus(analystApproveObj.LlId);
                                msg = "Update Successfuly";
                                //LoadData(analystApproveObj.UserId);
                                FieldClear();
                            }
                            else if (analystApproveInsert == ERROR)
                            {
                                msg = "Error in Data !";
                            }
                            Alert.Show(msg);
                            new AT(this).AuditAndTraial("Analyst Approve",msg);
                        }
                        else
                        {
                            Alert.Show("You do not have permission to approve this file !. Please cotact System Administrator");
                        }
                    }
                    else
                    {
                        Alert.Show("You do not have permission to approve this file !. Please cotact System Administrator");
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Analyst Approve");
            }

        }
        private void UpdateLoanStatusAndDetails(int loanStatusId)
        {
            int llid = Convert.ToInt32(lLIdTextBox.Text); 
            LoanApplicationDetails loanApplicationDetailsObj = new LoanApplicationDetails();
            List<Remarks> remarksListObj = new List<Remarks>();
            Remarks remarksObj = new Remarks();
            remarksObj.Id = 0;
            remarksObj.Name = remarksTextBox.Text;
            remarksListObj.Add(remarksObj);

            loanApplicationDetailsObj.LAId = llid;
            loanApplicationDetailsObj.PersonId = (string)Session["UserId"];
            loanApplicationDetailsObj.DeptId = 4;
            loanApplicationDetailsObj.ProcessStatusId = loanStatusId;
            int insertLoanAppDetailsFlag = loanApplicationManagerObj.InsertDeferReasonDetails(loanApplicationDetailsObj, remarksListObj);

        }
        private void FieldClear()
        {
            lLIdTextBox.Text = "";
            customerNameTextBox.Text = "";
            decisionDropDownList.SelectedIndex = 0;
            loanAmountTextBox.Text = "";
            loanTypeTextBox.Text = "";
            lavelTextBox.Text = "";
            scoreTextBox.Text = "";
            resultTextBox.Text = "";
            remarksTextBox.Text = "";
        }
        private bool IsBalnk()
        {
            if (lLIdTextBox.Text == "")
            {
                Alert.Show("Please input LLId");
                lLIdTextBox.Focus();
                return false;
            }
            else if (decisionDropDownList.SelectedIndex == 0)
            {
                Alert.Show("Please Select decision");
                decisionDropDownList.Focus();
                return false;
            }
            else if (loanAmountTextBox.Text == "")
            {
                Alert.Show("Loan Amount Empty!");
                loanAmountTextBox.Focus();
                return false;

            }
            else if (loanTypeTextBox.Text == "")
            {
                Alert.Show("Loan Tag Empty!");
                loanTypeTextBox.Focus();
                return false;

            }
            else if (resultTextBox.Text == "REJECTED" && decisionDropDownList.Text == "Approved")
            {
                Alert.Show("Please input remarks for approved");
                remarksTextBox.Focus();
                return false;
            }
            else if (resultTextBox.Text == "APPROVED" && decisionDropDownList.Text == "Rejected")
            {
                Alert.Show("Please input remarks for Rejected");
                remarksTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            FieldClear();
        }
        protected void findButton_Click(object sender, EventArgs e)
        {
            Int32 UserId = Convert.ToInt32(Session["Id"].ToString());
            if (lLIdTextBox.Text != "")
            {
                AnalystApprove analystApproveObj = analystApproveManagerObj.SelectAnalystApprove(Convert.ToInt64(lLIdTextBox.Text), UserId);
                if (analystApproveObj != null)
                {
                    lLIdTextBox.Text = analystApproveObj.LlId.ToString();
                    decisionDropDownList.SelectedValue = analystApproveObj.Decision.ToString();
                    scoreTextBox.Text = analystApproveObj.Score.ToString();
                    resultTextBox.Text = analystApproveObj.Result.ToString();
                    remarksTextBox.Text = analystApproveObj.Remarks.ToString();

                    customerNameTextBox.Text = analystApproveObj.CustomerName.ToString();
                    loanAmountTextBox.Text=analystApproveObj.LoanAmount.ToString() ;
                    lavelTextBox.Text=analystApproveObj.Level.ToString();
                    loanTypeTextBox.Text=analystApproveObj.LoanTag.ToString();

                }
                else
                {
                    Alert.Show("Not Found Data");
                }
            }
            else
            {
                Alert.Show("Please Input LLId");
                lLIdTextBox.Focus();
            }
        }


        private void LoadData(Int32 UserId, string searchKey)
        {
            int i = 1;
            List<AnalystApprove> analystApproveObjList = new List<AnalystApprove>();
            analystApproveObjList = analystApproveManagerObj.GetAllPL(UserId, searchKey);
            var xml = "<xml id='xml_data'><rows>";
            try
            {
                if (analystApproveObjList != null)
                {
                    foreach (AnalystApprove analystApproveObj in analystApproveObjList)
                    {
                       xml+="<row id='" + i.ToString() + "'>";
                       xml+="<cell>";
                       xml+=i.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=analystApproveObj.PL.PlLlid.ToString();
                       xml+="</cell>";

                        if (analystApproveObj.LoanInformation.ApplicantName != null)
                        {
                           xml+="<cell>";
                           xml+=AddSlash(analystApproveObj.LoanInformation.ApplicantName.ToString());
                           xml+="</cell>";
                        }
                        if (analystApproveObj.Product.ProdName != null)
                        {
                           xml+="<cell>";
                           xml+=AddSlash(analystApproveObj.Product.ProdName.ToString());
                           xml+="</cell>";
                        }
                        if (analystApproveObj.LoanType.Name != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.LoanType.Name.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.LoanInformation.AppliedAmount != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.LoanInformation.AppliedAmount.ToString();
                           xml+="</cell>";
                        }

                        if (analystApproveObj.PL.PlProposedloanamount != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.PL.PlProposedloanamount.ToString();
                           xml+="</cell>";
                        }

                        if (analystApproveObj.EntryDate != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.EntryDate.ToString("dd-MM-yyyy");
                           xml+="</cell>";
                        }
                        if (analystApproveObj.Segment.Name != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.Segment.Name.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.PL.PlNetincome != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.PL.PlNetincome.ToString();
                           xml+="</cell>";
                        }

                        if (analystApproveObj.PL.PlInterestrat != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.PL.PlInterestrat.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.PL.PlTenure != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.PL.PlTenure.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.PL.PlEmi != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.PL.PlEmi.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.PL.PlDbr != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.PL.PlDbr.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.PL.PlMue != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.PL.PlMue.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.PL.PlLevel != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.PL.PlLevel.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.LoanScorePoint.TotalPoint != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.LoanScorePoint.TotalPoint.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.LoanScorePoint.Result != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.LoanScorePoint.Result.ToString();
                           xml+="</cell>";
                        }
                        if (analystApproveObj.User.Name != null)
                        {
                           xml+="<cell>";
                           xml+=AddSlash(analystApproveObj.User.Name.ToString());
                           xml+="</cell>";
                        }
                        if (analystApproveObj.PL.PlRemarks != null)
                        {
                           xml+="<cell>";
                           xml+=AddSlash(analystApproveObj.PL.PlRemarks.ToString());
                           xml+="</cell>";
                        }
                        if (analystApproveObj.Product.ProdType != null)
                        {
                           xml+="<cell>";
                           xml+=analystApproveObj.Product.ProdType.ToString();
                           xml+="</cell>";
                        }

                       xml+="</row>";
                        i = i + 1;
                    }
                }


                xml += "</rows></xml>";
                dvXml.InnerHtml = xml;
            }
            finally
            {
            }
          
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0 && sMessage != "")
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }



    }
}
