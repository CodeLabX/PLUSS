﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_ProductUI : System.Web.UI.Page
    {
        #region private members
        private ProductManager productManagerObj=null;
        private Product productObj;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            productManagerObj = new ProductManager();
            if (!IsPostBack)
            {
                LoadProductTypes();
                LoadData("");
            }
        }
        private void LoadData(string searchKey)
        {
           
            List<Product> productObjList = new List<Product>();
           
            if (searchKey == "")
            {
                productObjList = productManagerObj.GetAllProducts();
            }
            else
            {
                productObjList = productManagerObj.GetAllProductsBySearchingKey(searchKey);
            }
            try
            {
                var xml = "<xml id='xml_data'><rows>";

                foreach (Product productObj in productObjList)
                {
                    xml+="<row id='" + productObj.ProdID.ToString() + "'>";

                
                    xml+="<cell>";
                    xml+=productObj.ProdID.ToString();
                    xml+="</cell>";


                    var productType = "";

                    switch (productObj.ProdType.ToString())
                    {
                        case "U":
                            productType = "Unsecure";
                            break;
                        case "S":
                            productType = "Secure";
                            break;
                        case "C":
                            productType = "Cash Secure";
                            break;
                        
                    }
                    xml+="<cell>";
                    xml+=productObj.ProdCode.ToString();
                    xml+="</cell>";

                    xml+="<cell>";
                    xml+=AddSlash(productObj.ProdName.ToString());
                    xml+="</cell>";

                    xml+="<cell>";
                    //xml+=productObj.ProdType.ToString());
                    xml+=productType;
                    xml+="</cell>";
                    if (productObj.ProdStatus == 1)
                    {
                        xml+="<cell>";
                        xml+="Active";
                        xml+="</cell>";
                    }
                    else
                    {
                        xml+="<cell>";
                        xml+="Inactive";
                        xml+="</cell>";
                    }
                    xml+="</row>";
                }
                xml+="</rows>";

                dvXml.InnerHtml = xml;
            }
            finally
            {
               
            }
          
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0)
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        private void LoadProductTypes()
        {
            productManagerObj = new ProductManager();

            //var res = productManagerObj.GetAllProductTypes();
            //var objList = new List<String>();
            //var obj = new Object();

            //obj.Id = "sdsdffs";
            //obj.Name = "dfsdfsdf";

            //objList.Add(obj);
        
             
            productTypeDropDownList.DataSource = productManagerObj.GetAllProductTypes();
            productTypeDropDownList.DataTextField = "ProductName";
            productTypeDropDownList.DataValueField = "ProdType";
            productTypeDropDownList.DataBind();
        }    

        protected void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 ProductId = 0;
                if (IsBlank())
                {
                    ProductId = productManagerObj.GetProductId(productNameTextBox.Text, productTypeDropDownList.SelectedItem.Value);
                    if (ProductId == 0)
                    {
                        InsertData();
                    }
                    else
                    {
                        UpdateData(ProductId);
                    }
           
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Product");
            }
        }

        private bool IsBlank()
        {
            if (productIdTextBox.Text == "")
            {
                Alert.Show("Please enter product code");
                productIdTextBox.Focus();
                return false;
            }
            else if (productNameTextBox.Text == "")
            {
                Alert.Show("Please enter product name");
                productNameTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void UpdateData(Int32 ProductId)
        {
            productManagerObj = new ProductManager();
            productObj = new Product();

            productObj.ProdID = ProductId;//Convert.ToInt32(HiddenField1.Value);
            productObj.ProdCode = productIdTextBox.Text;
            productObj.ProdName = productNameTextBox.Text;
            productObj.ProdType = productTypeDropDownList.SelectedItem.Value;

            if (activeDropdownList.SelectedItem.Value == "Active")
            { 
                productObj.ProdStatus = 1;
            }
            else 
            { 
                productObj.ProdStatus = 0;
            }
            productObj.IdLearner = Convert.ToInt32(Session["Id"].ToString());
            int updateStatus = productManagerObj.UpdateProduct(productObj);
            if (updateStatus == 1)
            {
                Alert.Show("Update successfully !");
                new AT(this).AuditAndTraial("Product", "Update successfully");
                LoadData("");
                ClearBoxes();
            }
            else
            { 
                Alert.Show("Update-not successfully !");
                new AT(this).AuditAndTraial("Product", "Update not successfully");
            }
        }

        private void InsertData()
        {
            productManagerObj = new ProductManager();
            productObj = new Product();

            productObj.ProdCode = productIdTextBox.Text;
            productObj.ProdName = productNameTextBox.Text;
            productObj.ProdType = productTypeDropDownList.SelectedItem.Value;

            if (activeDropdownList.SelectedItem.Value == "Active")
            { productObj.ProdStatus = 1; }
            else { productObj.ProdStatus = 0; }
            productObj.IdLearner = Convert.ToInt32(Session["Id"].ToString());
            int insertStatus = productManagerObj.InsertProduct(productObj);
            if (insertStatus == 1)
            {
                Alert.Show("Save successfully !");
                new AT(this).AuditAndTraial("Product", "Save successfully");
                LoadData("");
                ClearBoxes();
            }
            else
            {
                Alert.Show("Save not successfully !");
                new AT(this).AuditAndTraial("Product", "Save not successfully");
            }

        
        } 


        protected void searchbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (searchTextBox.Text != "")
                {
                    LoadData(searchTextBox.Text);
                }
                else
                {
                    LoadData("");
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Search Product");
            }

        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearBoxes();
        }

        private void ClearBoxes()
        {
            productIdTextBox.Text = "";
            productNameTextBox.Text = "";
            searchTextBox.Text = "";
            productTypeDropDownList.SelectedIndex = 0;
            activeDropdownList.SelectedIndex = 1;
            addButton.Text = "Add";
        }


       
    }
}
  