﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BankStvPersonUI : System.Web.UI.Page
    {
        BankStvPerson bankStvPersonObj = null;
        BankStvPersonManager bankStvPersonManagerObj = null;
        BankInformationManager bankInformationManagerObj = null;
        BranchInformationManager branchInformationManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            bankInformationManagerObj = new BankInformationManager();
            branchInformationManagerObj = new BranchInformationManager();
            bankStvPersonManagerObj = new BankStvPersonManager();
            bankStvPersonObj = new BankStvPerson();

            if (!Page.IsPostBack)
            {
                BankDropDownListItem();
                LoadBSVPersonExistingData("");
            }
        }
        private void LoadBSVPersonExistingData(string searchKey)
        {

           
            List<BankStvPerson> bankStvPersonObjList = new List<BankStvPerson>();
           
            if (searchKey == "")
            {
                bankStvPersonObjList = bankStvPersonManagerObj.SelectBankStvPersonList();
            }
            else
            {
                bankStvPersonObjList = bankStvPersonManagerObj.SelectBankStvPersonList(searchKey);
            }
            try
            {
                var xml = "<xml id='xml_data'><rows>";
                foreach (BankStvPerson bankStvPersonObj in bankStvPersonObjList)
                {
                    xml+="<row id='" + bankStvPersonObj.Id.ToString() + "'>";
                    xml+="<cell>";
                    xml+=bankStvPersonObj.Id.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=bankStvPersonObj.Bank_Id.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=bankStvPersonObj.BankName.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=bankStvPersonObj.Name.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=bankStvPersonObj.EmailAddress.ToString();
                    xml+="</cell>";
                    if (bankStvPersonObj.Status == 1)
                    {
                        xml+="<cell>";
                        xml+="Active";
                        xml+="</cell>";
                    }
                    else
                    {
                        xml+="<cell>";
                        xml+="Inactive";
                        xml+="</cell>";
                    }
                    xml += "</row>";
                }
                xml += "</rows></xml>";
                dvXml.InnerHtml = xml;
            }

            finally
            {
               
            }
            
        }
        private void BankDropDownListItem()
        {
            List<BankInformation> bankInformationListObj = bankInformationManagerObj.GetAllBankInfo();
            bankInformationListObj = bankInformationListObj.FindAll(findInactiveBank => findInactiveBank.Status != 0);
            BankInformation bankInformationObj = new BankInformation();
            bankInformationObj.Name = "Select Bank name";
            bankInformationObj.Id = 0;
            bankInformationListObj.Insert(0, bankInformationObj);
            bankNameDropDownList.DataSource = bankInformationListObj;
            bankNameDropDownList.DataTextField = "Name";
            bankNameDropDownList.DataValueField = "Id";
            bankNameDropDownList.DataBind();
        }
        private void BranchDropDownListItem(Int32 bankId)
        {
            List<BranchInformation> branchInformationListObj = branchInformationManagerObj.GetAllBranchInfoByBankId(bankId);
            BranchInformation branchInformationObj = new BranchInformation();
            branchInformationObj.Id = 0;
            branchInformationObj.Name = "Select Branch Name";
            branchInformationListObj.Insert(0, branchInformationObj);
        }

        protected void bankNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void bankNameDropDownList_TextChanged(object sender, EventArgs e)
        {
        }

        protected void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 bSVERPersonId = 0;
                if (IsValidPage())
                {
                    bSVERPersonId = bankStvPersonManagerObj.GetBSVPersonId(Convert.ToInt32(bankNameDropDownList.SelectedValue.ToString()));
                    if (bSVERPersonId == 0)
                    {
                        SaveBankStvPersonObj();
                    }
                    else
                    {
                        UpdateBankStvPersonObj(bSVERPersonId);
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Bank Statment Verification");
            }
        }



        private void UpdateBankStvPersonObj(Int32 bSVERPersonId)
        {
            bankStvPersonObj = new BankStvPerson();
            bankStvPersonManagerObj = new BankStvPersonManager();
            bankStvPersonObj.Id = bSVERPersonId;
            bankStvPersonObj.Bank_Id = Convert.ToInt16(bankNameDropDownList.SelectedValue.ToString());
            bankStvPersonObj.Branch_Id = 0;
            bankStvPersonObj.Name = nameTextBox.Text;
            bankStvPersonObj.EmailAddress = emailTextBox.Text;
            if (statusDropDownList.Text == "Active")
            {
                bankStvPersonObj.Status = 1;
            }
            else
            {
                bankStvPersonObj.Status = 0;
            }
            int status = bankStvPersonManagerObj.UpdateBankStvPerson(bankStvPersonObj);
            var msg = "";
            if (status == 1)
            {
                msg = "Update successful !";
                LoadBSVPersonExistingData("");
                Clear();
            }
            else
            {
                msg = "Un-successful !";
            }
            new AT(this).AuditAndTraial("Update Bank Statement Verification", msg);
            Alert.Show(msg);
        }
        private void SaveBankStvPersonObj()
        {
            try
            {
                bankStvPersonObj = new BankStvPerson();
                bankStvPersonManagerObj = new BankStvPersonManager();
                bankStvPersonObj.Bank_Id = Convert.ToInt16(bankNameDropDownList.SelectedValue.ToString());
                bankStvPersonObj.Branch_Id = 0;
                bankStvPersonObj.Name = nameTextBox.Text;
                bankStvPersonObj.EmailAddress = emailTextBox.Text;
                if (statusDropDownList.Text == "Active")
                {
                    bankStvPersonObj.Status = 1;
                }
                else
                {
                    bankStvPersonObj.Status = 0;
                }
                int status = bankStvPersonManagerObj.InsertBankStvPerson(bankStvPersonObj);
                var msg = "";
                if (status == 1)
                {
                    msg = "Save successful !";
                    LoadBSVPersonExistingData("");
                    Clear();
                }
                else
                {
                    msg = "Un-successful !";
                }
                new AT(this).AuditAndTraial("Save Bank Statement Verification", msg);
                Alert.Show(msg);
            }
            catch (Exception e)
            {
                CustomException.Save(e, "Save Bank Statement verification");  
                throw new Exception(e.Message);
            }
        }

        private bool IsValidPage()
        {
            if (bankNameDropDownList.Text == "0" || bankNameDropDownList.Text == "Select Bank name")
            { return false; }
                //else if (branchDropDownList.Text == "0" || branchDropDownList.Text == "Select Branch Name")
                //{ return false; }
            else if (nameTextBox.Text == "")
            { return false; }
            else if (emailTextBox.Text == "")
            { return false; }
            else { return true; }
        }
        private string GetBankBrnchNameById(int bankId, int branchId, int type)
        {
            List<string> info = new List<string>();
            bankStvPersonManagerObj = new BankStvPersonManager();
            info = bankStvPersonManagerObj.GetBankBrnchNameById(bankId, branchId);
            if (type == 0)
            { return info[0]; }         //bank name
            else { return info[1]; }    //branch name
        }
        private string GetStatus(int status)
        {
            if (status == 0)
            { return "Inactive"; }
            else { return "Active"; }
        }


        protected void searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (searchTextBox.Text != "")
                {
                    LoadBSVPersonExistingData(searchTextBox.Text);
                }
                else
                {
                    LoadBSVPersonExistingData("");
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Load Bank Statement Verification");  
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            nameTextBox.Text = "";
            emailTextBox.Text = "";
            addButton.Text = "Add";
            searchTextBox.Text = "";
            bankNameDropDownList.SelectedIndex = 0;
            //branchDropDownList.Items.Clear();
            statusDropDownList.SelectedIndex = 1;
        }
    }
}
