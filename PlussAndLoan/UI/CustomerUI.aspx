﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_CustomerUI"
    MasterPageFile="~/UI/AnalystMasterPage.master" CodeBehind="CustomerUI.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .gridDeleteBtn {
            position: absolute;
            top: 491px;
            left: 774px;
            width: 99px;
        }

        .style1 {
            height: 32px;
        }

        .style2 {
            width: 250px;
            height: 32px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function CalculateCashSecurity() {
            for (var i = 2 ; i <= 6 ; i++) {
                var myRegExp = /\%/;
                var value1 = document.getElementById('ctl00_ContentPlaceHolder_customerGridView_ctl0' + i + '_taskfacilityDropDownList').value;

                if (value1.search(myRegExp) > -1) {
                    var tempValue1 = value1.substr(value1.indexOf('[') + 1, ((value1.indexOf('%') - 1) - (value1.indexOf('['))));
                    var value2 = document.getElementById('ctl00_ContentPlaceHolder_customerGridView_ctl0' + i + '_taskOrginalLimitTextBox').value;
                    var result = (parseFloat('0' + value2) * parseFloat('0' + tempValue1)) / 100;
                    document.getElementById('ctl00_ContentPlaceHolder_customerGridView_ctl0' + i + '_taskCashsecurityTextBox').value = result;
                }
                else {
                    document.getElementById('ctl00_ContentPlaceHolder_customerGridView_ctl0' + i + '_taskCashsecurityTextBox').value = 0;
                }
            }
        }
        function TopUpDivVisibility() {
            var topUpId = document.getElementById('ctl00_ContentPlaceHolder_topUpIdHiddenField').value;
            var divObj = document.getElementById('ctl00_ContentPlaceHolder_topUpDiv');

            //alert(divObj);
            if (topUpId == 3) {    //alert(topUpId);
                document.getElementById("topUpDiv").style.display = "block";
                //return true;
                //divObj.style.visibility  = "visible";


            }
            else {
                //alert('Else '+topUpId);
                //divObj.style.display = "none";
                document.getElementById("topUpDiv").style.display = "none";
            }
        }
        function TopUpFieldShowHide() {
            var topUpCount = document.getElementById('ctl00_ContentPlaceHolder_topUpCountDropDownList').value;
            document.getElementById('ctl00_ContentPlaceHolder_topUpCountHiddenField').value = topUpCount;
            switch (topUpCount) {
                case "0":
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount1Label').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount2TextBox').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_date2Label').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_topUpDate2TextBox').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount3Label').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount3TextBox').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_date3Label').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_topUpDate3TextBox').style.visibility = "hidden";
                    break;
                case "1":
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount1Label').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount2TextBox').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_date2Label').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_topUpDate2TextBox').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount3Label').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount3TextBox').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_date3Label').style.visibility = "hidden";
                    document.getElementById('ctl00_ContentPlaceHolder_topUpDate3TextBox').style.visibility = "hidden";
                    break;
                case "2":
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount1Label').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount2TextBox').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_date2Label').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_topUpDate2TextBox').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount3Label').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_topupAmount3TextBox').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_date3Label').style.visibility = "visible";
                    document.getElementById('ctl00_ContentPlaceHolder_topUpDate3TextBox').style.visibility = "visible";
                    break;

            }
        }

    </script>
    <script type="text/javascript">
        function CheckTopupLoanAmount() {
            $.ajax({
                type: "POST",
                url: "../UI/CheckTopupLoanAmount.aspx/GetLoanAmount",
                data: "{'LLId': '" + $('#ctl00_ContentPlaceHolder_llIdTextBox').val() + "','outStanding':'" + $('#ctl00_ContentPlaceHolder_topupAmount1TextBox').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d.length > 10)
                        alert(msg.d);

                }
            });
        }

    </script>
</asp:Content>
<asp:Content ID="ContentPlaceHolder" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>--%>
    <table border="0">
        <tr>
            <td>&nbsp;
            </td>
            <td align="center" colspan="2" style="font-size: 18px; font-weight: bold;">ACCOUNT INFO
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 145px;">LLID :
            </td>
            <td style="width: 320px;">
                <asp:TextBox ID="llIdTextBox" runat="server" Width="123px" MaxLength="9"></asp:TextBox>
                <asp:Button ID="searchButton" runat="server" OnClick="searchButton_Click" Text="..." />
                &nbsp;
                <asp:Label ID="msgLabel" runat="server" ForeColor="Red"></asp:Label>
            </td>
            <td colspan="2" rowspan="6" valign="top">
                <div id="gridbox" style="background-color: white; width: 520px; height: 130px;">
                </div>
                <asp:HiddenField ID="bankListNameHiddenField" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Customer&#39;s name :
            </td>
            <td>
                <asp:TextBox ID="customerNameTextBox" runat="server" Width="284px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Master :
            </td>
            <td>
                <asp:TextBox ID="masterTextBox" runat="server" Width="284px" MaxLength="7"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>PDC/ SI from :
            </td>
            <td>
                <asp:TextBox ID="pdcTextBox" runat="server" Width="284px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>PDC Account :
            </td>
            <td>
                <asp:TextBox ID="pdcAccNoTextBox" runat="server" Width="284px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>1st payment :
            </td>
            <td>
                <asp:DropDownList ID="firstPaymentDropDownList" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style1">Other Statement :
            </td>
            <td class="style1">
                <asp:TextBox ID="otherStBank1TextBox" runat="server" Width="140px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="otherStAcc1TextBox" runat="server" Width="140px"></asp:TextBox>
            </td>
            <td class="style2">&nbsp;
            </td>
            <td class="style1">&nbsp;
            </td>
        </tr>
        <tr>
            <td>Other Statement :
            </td>
            <td>
                <asp:TextBox ID="otherStBank2TextBox" runat="server" Width="140px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="otherStAcc2TextBox" runat="server" Width="140px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
                <asp:DropDownList ID="ccplBundleOfferDropDownList" runat="server" Width="80px" Visible="False">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Other Statement :
            </td>
            <td>
                <asp:TextBox ID="otherStBank3TextBox" runat="server" Width="140px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="otherStAcc3TextBox" runat="server" Width="140px"></asp:TextBox>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
                <asp:DropDownList ID="otherDirLoanDropDownList" runat="server" Width="80px" Visible="False">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Other Statement :
            </td>
            <td>
                <asp:TextBox ID="otherStBank4TextBox" runat="server" Width="140px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="otherStAcc4TextBox" runat="server" Width="140px"></asp:TextBox>
            </td>
            <td>&nbsp;
                <asp:HiddenField ID="idHiddenField" runat="server" />
            </td>
            <td>&nbsp;
                <asp:HiddenField ID="topUpIdHiddenField" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Other Statement :
            </td>
            <td>
                <asp:TextBox ID="otherStBank5TextBox" runat="server" Width="140px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="otherStAcc5TextBox" runat="server" Width="140px"></asp:TextBox>
            </td>
            <td>MOB12+
                <asp:DropDownList ID="mob12DropDownList" runat="server" Width="80px">
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="DeleteFromGrid" runat="server" Text="Clear grid"
                    CssClass="gridDeleteBtn" OnClick="DeleteFromGrid_Click" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">

                <table width="605px">
                    <tr bgcolor="#339933">
                        <td align="center" style="color: White; font-weight: bold;">EXPOSURE
                        </td>
                        <td align="center" style="color: White; font-weight: bold;">dd-mm-yyyy
                        </td>
                    </tr>
                    <tr>

                        <td colspan="2">
                            <%-- <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />--%>
                            <div class="GridviewBorder2">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                </asp:UpdatePanel>
                                <asp:GridView ID="customerGridView" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    ForeColor="#333333" PageSize="5" Width="100px" Font-Size="11px" GridLines="Both"
                                    ShowFooter="True">
                                    <RowStyle BackColor="#FFFFFF" ForeColor="#333333" Font-Size="11px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id" HeaderStyle-Width="50px" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="idTextBox" runat="server" Width="50px" CssClass="gvTextBox"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Facility" HeaderStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="taskfacilityDropDownList" runat="server" Width="120px" AutoPostBack="True"
                                                    OnSelectedIndexChanged="taskfacilityDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <HeaderStyle Width="120px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Flag" HeaderStyle-Width="35px">
                                            <ItemTemplate>
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="taskFlagTextBox" runat="server" Width="35px" CssClass="gvTextBox"
                                                            ReadOnly="true"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="taskfacilityDropDownList" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                            <HeaderStyle Width="35px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Outstanding" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="taskOutstandingTextBox" runat="server" Width="100px" CssClass="gvTextBox"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Original Limit" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="taskOrginalLimitTextBox" runat="server" Width="100px" CssClass="gvTextBox"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Repmt" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="taskRepmtTextBox" runat="server" Width="100px" CssClass="gvTextBox"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dis Date" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="taskDisDateTextBox" runat="server" Width="100px" CssClass="gvTextBox"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Exp Date" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="taskExpDateTextBox" runat="server" Width="100px" CssClass="gvTextBox"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cash security" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="taskCashsecurityTextBox" runat="server" Width="100px" CssClass="gvTextBox"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Percentage" HeaderStyle-Width="50px" Visible ="false">
                                            <ItemTemplate>
                                                <asp:TextBox ID="percentageTextBox" runat="server" Width="50px" CssClass="gvTextBox"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                    <%-- <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />--%>
                                    <FooterStyle CssClass="gvFooter" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <EmptyDataTemplate>
                                        &nbsp;&nbsp;No data found.
                                    </EmptyDataTemplate>
                                    <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle CssClass="ssHeader" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="topUpDiv" class="GridviewBorder2" runat="server" style="width: 775px">
                    <table border="0" style="width: 775px">
                        <tr>
                            <td align="right" width="120">
                                <asp:HiddenField ID="topUpCountHiddenField" runat="server" />
                            </td>
                            <td align="center" width="100">
                                <asp:Label ID="outStandingLabel" runat="server" Text="Outstanding"></asp:Label>
                            </td>
                            <td align="right" style="width: 60px">&nbsp;
                            </td>
                            <td align="center" width="90">
                                <asp:Label ID="originalLimitLabel" runat="server" Text="Original Limit  "></asp:Label>
                            </td>
                            <td width="120">&nbsp;
                            </td>
                            <td width="120">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="120">
                                <asp:Label ID="Label1" runat="server" Text="TOP UP COUNT:"></asp:Label>
                                <asp:DropDownList ID="topUpCountDropDownList" runat="server">
                                    <asp:ListItem Value="0">1</asp:ListItem>
                                    <asp:ListItem Value="1">2</asp:ListItem>
                                    <asp:ListItem Value="2">3</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="topupAmount1TextBox" runat="server"></asp:TextBox>
                            </td>
                            <td align="right"></td>
                            <td>
                                <asp:TextBox ID="outStandingTextBox" runat="server"></asp:TextBox>
                            </td>
                            <td width="120">
                                <asp:Label ID="date1Label" runat="server" Text="Disb date of this PL:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="topUpDate1TextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="topupAmount1Label" runat="server" Text="Topup 1 Amount :"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="topupAmount2TextBox" runat="server"></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Label ID="date2Label" runat="server" Text="Date :"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="topUpDate2TextBox" runat="server"></asp:TextBox>
                            </td>
                            <td>&nbsp;&nbsp;
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="topupAmount3Label" runat="server" Text="Topup 2 Amount :"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="topupAmount3TextBox" runat="server"></asp:TextBox>
                            </td>
                            <td align="right">
                                <asp:Label ID="date3Label" runat="server" Text="Date :"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="topUpDate3TextBox" runat="server"></asp:TextBox>
                            </td>
                            <td>&nbsp;&nbsp;
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td colspan="3" align="center">
                <table>
                    <tr>
                        <td align="right">
                            <asp:Button ID="saveButton" runat="server" Text="Save" Width="100px" OnClick="saveButton_Click"
                                Height="25px" />
                        </td>
                        <td align="center">
                            <asp:Button ID="clearButton" runat="server" Text="Clear" Width="100px" OnClick="clearButton_Click"
                                Height="25px" />
                        </td>
                        <td>&nbsp;
                            <asp:Label ID="ExceedLimitLbl" runat="server" ForeColor="Red" Text="."></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td colspan="2">&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
    </table>
     <div style="display: none" runat="server" id="dvXml"></div>
    <script type="text/javascript" language="javascript">
        var mygrid;
        mygrid = new dhtmlXGridObject('gridbox');
        function doOnLoad() {
            mygrid.setImagePath("../codebase/imgs/");
            mygrid.setHeader("Sl,BankId,Bank Name,BranchId,Branch Name,A/C No");
            mygrid.setInitWidths("25,0,200,0,155,120");
            mygrid.setColAlign("left,left,left,left,left,left");
            mygrid.setColTypes("ro,ro,ro,ro,ro,ro");
            mygrid.setColSorting("int,str,str,str,str,str");
            mygrid.setSkin("light");
            mygrid.init();
            mygrid.enableSmartRendering(true, 5);
            mygrid.clearAll();
            JS_FunctionLoadBankNameList();
            mygrid.attachEvent("onRowDblClicked", DoOnRowSelected);
        }
        function DoOnRowSelected() {
            if (mygrid.cells(mygrid.getSelectedId(), 0).getValue() > 0) {
                document.getElementById('ctl00_ContentPlaceHolder_pdcTextBox').value = mygrid.cells(mygrid.getSelectedId(), 2).getValue();
                document.getElementById('ctl00_ContentPlaceHolder_pdcAccNoTextBox').value = mygrid.cells(mygrid.getSelectedId(), 5).getValue();
            }
        }
        function JS_GridClear() {
            mygrid.clearAll();
        }
        var ajax_list_objects = new Array();
        var ajax_objects = new Array();

        function JS_FunctionLoadBankNameList() {
            var LLId = document.getElementById('ctl00_ContentPlaceHolder_llIdTextBox').value;
            if (LLId.length > 0) {
                mygrid.clearAll();
               
              // mygrid.loadXML('../UI/CustomerUI.aspx?&LLId='+LLId);
                mygrid.parse(document.getElementById("myGridxml_data"));
            }
            else {
                document.getElementById('ctl00_ContentPlaceHolder_llIdTextBox').focus();
            }

        }
        //function ajax_LoadBankNameList(ajaxIndex) {
        //    var TypeData = new Array();
        //    var responseData = ajax_objects[ajaxIndex].response;
        //    TypeData = responseData.split(',');
        //    if (TypeData[0] != 0) {
        //        mygrid.clearAll();
        //        mygrid.loadXML("../includes/EntryBankList.xml");
        //    }
        //    else {
        //        alert("No other bank entry yet!");
        //        mygrid.clearAll();
        //    }
        //}

        //function LoadBankNameList() {
        //    try {
        //        var LLId = document.getElementById('ctl00_ContentPlaceHolder_llIdTextBox').value;
        //        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        //        if (XMLHttpRequestObject) {
        //            XMLHttpRequestObject.open("POST", "CustomerUI.aspx?operation=LoadBankNameList&LLID=" + LLId);
        //            XMLHttpRequestObject.onreadystatechange = function () {
        //                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
        //                    spreadsheet.XMLData = XMLHttpRequestObject.responseText;
        //                }
        //            }
        //            XMLHttpRequestObject.send(null);
        //        }
        //    }
        //    catch (err)
        //    { }
        //}
    </script>

</asp:Content>
