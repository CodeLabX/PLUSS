﻿using AzUtilities;
using BusinessEntities;
using DAL.SecurityMatrixGateways;
using System;
using System.Data;
using System.Linq;
using System.Web;

namespace PlussAndLoan.UI
{
    public partial class UI_MasterPage : System.Web.UI.MasterPage
    {
        private PageAccessPermission _permissions;

        protected void Page_Load(object sender, EventArgs e)
        {
            //for session kill
            var loginId = (string)Session["UserId"];
            var user = LoginUser.LoginUserToSystem.Where(u => u.UserId == loginId);
            if (!user.Any())
            {
                Response.Redirect("~/LoginUI.aspx");
            }
            //-------end
            _permissions = new PageAccessPermission();


            User userT = (User)Session["UserDetails"];

            if (!_permissions.HasUserAccess(Context, userT))
                Response.Redirect("~/UI/PageAccessDenied.aspx");

            DataTable dt = _permissions.BindMenuData(0, userT);
            _permissions.DynamicMenuControlPopulation(dt, 0, null, MenuSpace, userT);
        }
        #region
        private void removeCookie()
        {
            HttpCookie myCookie = new HttpCookie("LOGIN");
            Response.Cookies["LOGIN"].Expires = DateTime.Now.AddDays(-365);

            string currentCookieValue = "";
            if (Response.Cookies["LOGIN"].Value == "" && Response.Cookies["LOGIN"].Expires == DateTime.MinValue)
            {
                currentCookieValue = Request.Cookies["LOGIN"].Value;
                Response.Cookies.Remove("LOGIN");
            }
            else
            {
                myCookie = new HttpCookie("LOGIN");
                myCookie.Values.Add("LoginId", "");
                myCookie.Values.Add("status", "0");
                currentCookieValue = Response.Cookies["LOGIN"].Value;
            }
        }
        #endregion
        protected void logout_Click(object sender, EventArgs e)
        {
            var userId = Session["UserId"].ToString();
            LoginUser.LoginUserToSystem.RemoveAll(u => u.UserId == userId);
            removeCookie();
        }
    }
}
