﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.LoanLocatorReports;

namespace PlussAndLoan.UI
{
    public partial class AuditReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                MonthDropDownList.DataSource = AllMonthList();
                MonthDropDownList.DataTextField = "Value";
                MonthDropDownList.DataValueField = "Key";
                MonthDropDownList.DataBind();

                YearDropDownList.DataSource = AllyearList();
                YearDropDownList.DataBind();
                YearDropDownList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
                MonthDropDownList.SelectedIndex = DateTime.Now.Month - 1;
            }
            reportDiv.Visible = true;
            ShowReportDiv.Visible = false;

        }

        #region DropDown Value List

        public List<int> AllyearList()
        {
            List<int> yearList = new List<int>();
            for (int i = 2004; i < 2051; i++)
            {
                yearList.Add(i);
            }
            return yearList;

        }

        public Dictionary<int, string> AllMonthList()
        {
            IDictionary<int, string> months = new Dictionary<int, string>();
            months.Add(1, "January");
            months.Add(2, "February");
            months.Add(3, "March");
            months.Add(4, "April");
            months.Add(5, "May");
            months.Add(6, "June");
            months.Add(7, "July");
            months.Add(8, "August");
            months.Add(9, "September");
            months.Add(10, "October");
            months.Add(11, "November");
            months.Add(12, "December");

            return (Dictionary<int, string>)months;
        }


       
        #endregion

      LocLoanReportManager locLoanReportManager = new LocLoanReportManager();
        protected void btnView_OnClick(object sender, EventArgs e)
        {
            reportDiv.Visible = false;
            ShowReportDiv.Visible = true;

            int year = Convert.ToInt32(YearDropDownList.SelectedValue);
            int month = Convert.ToInt32(MonthDropDownList.SelectedValue);
            int action = Convert.ToInt32(action_name.SelectedValue);
            var GetAllLoanAuditData = locLoanReportManager.LocAdminAuditReport(year, month, action);
            AuditGridView.DataSource = GetAllLoanAuditData;
            AuditGridView.DataBind();
            if (action == 1)
            {
                actionDiv.InnerText = "ADD";
            }
            else if (action==2)
            {
                actionDiv.InnerText = "UPDATE";
            }
            else
            {
                actionDiv.InnerText = "ALL";
            }
            yearDiv.InnerText = year.ToString();
            if (month==1)
            {
                monthDiv.InnerText = "January";
            }
            else if (month==2)
            {
                monthDiv.InnerText = "February";
            }
            else if (month==3)
            {
                monthDiv.InnerText = "March";
            }
            else if (month==4)
            {
                monthDiv.InnerText = "April";
            }
            else if (month==5)
            {
                monthDiv.InnerText = "May";
            }
            else if (month==6)
            {
                monthDiv.InnerText = "June";
            }
            else if (month==7)
            {
                monthDiv.InnerText = "July";
            }
            else if (month==8)
            {
                monthDiv.InnerText = "August";
            }
            else if (month==9)
            {
                monthDiv.InnerText = "September";
            }
            else if (month==10)
            {
                monthDiv.InnerText = "October";
            }
            else if (month==11)
            {
                monthDiv.InnerText = "November";
            }
            else
            {
                monthDiv.InnerText = "December";
            }
           
        }
        public string Format12Hour(Object time)
        {
            //Checks for nulls
            if (Convert.IsDBNull(time))
            {
                return "N/A";
            }
            else
            {
                //Grabs your Timespan
                TimeSpan ts = (TimeSpan)time;

                //Determines AM/PM and outputs accordingly
                if (ts.Hours >= 12)
                {
                    return String.Format("{0:#0}:{1:00} PM", ts.Hours - 12, ts.Minutes);
                }
                else
                {
                    return String.Format("{0:#0}:{1:00} AM", ts.Hours, ts.Minutes);
                }
            }
        }

        protected void closebutton_OnClick(object sender, EventArgs e)
        {
            reportDiv.Visible = true;
            ShowReportDiv.Visible = false;
        }

        #region printing
        protected void printButton_Click(object sender, EventArgs e)
        {
        }
        #endregion



    }
}