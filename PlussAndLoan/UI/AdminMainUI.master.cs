﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AzUtilities;
using BLL;
using BusinessEntities;
using DAL;
using DAL.SecurityMatrixGateways;

namespace PlussAndLoan.UI
{
    public partial class UI_AdminMainUI : System.Web.UI.MasterPage
    {
        public UserManager userManagerObj=null;
        private UserAccessLogManager userAccessLogManagerObj = null;
        private PageAccessPermission _permissions;

        protected void Page_Load(object sender, EventArgs e)
        {
        
            userAccessLogManagerObj = new UserAccessLogManager();
            userManagerObj = new UserManager();
            //userIdHiddenField.Value = (string)Session["UserId"];

            //for session kill
            var loginId = (string)Session["UserId"];
            userIdHiddenField.Value = loginId;
            var user = LoginUser.LoginUserToSystem.Where(u => u.UserId == loginId);
            if (!user.Any())
            {
                Response.Redirect("~/LoginUI.aspx");
            }

            userIdHiddenField.Value = (string)Session["UserId"];

            _permissions = new PageAccessPermission();

            User userT = (User)Session["UserDetails"];

            if (!_permissions.HasUserAccess(Context, userT))
                Response.Redirect("~/UI/PageAccessDenied.aspx");

            DataTable dt = _permissions.BindMenuData(0, userT);
            _permissions.DynamicMenuControlPopulation(dt, 0, null, MenuSpace, userT);

            //-------end
            if (Session.Count == 0)
            {
                Response.Redirect("~/ErrorPage/Timout.htm");
            }
            if (!IsPostBack)
            {
                userNameLabel.Text = (string)Session["UserName"];

                short userType = (short)Session["UserType"];
                if (userType.Equals(5))
                {
                    //MainMenu.Items[5].Enabled = true;
                }
                else
                {
                    //MainMenu.Items[5].Enabled = false;
                }
            }
        }

        #region
        private void removeCookie()
        {
            HttpCookie myCookie = new HttpCookie("LOGIN");
            Response.Cookies["LOGIN"].Expires = DateTime.Now.AddDays(-365);

            string currentCookieValue = "";
            if (Response.Cookies["LOGIN"].Value == "" && Response.Cookies["LOGIN"].Expires == DateTime.MinValue)
            {
                currentCookieValue = Request.Cookies["LOGIN"].Value;
                Response.Cookies.Remove("LOGIN");
            }
            else
            {
                myCookie = new HttpCookie("LOGIN");
                myCookie.Values.Add("LoginId", "");
                myCookie.Values.Add("status", "0");
                currentCookieValue = Response.Cookies["LOGIN"].Value;
            }
        }
        #endregion
        protected void logout_Click(object sender, EventArgs e)
        {
            var userId= Session["UserId"].ToString();
            LoginUser.LoginUserToSystem.RemoveAll(u => u.UserId == userId);

            SaveAccessLog(userIdHiddenField.Value, 0, 0, 0);
            new AT(this).AuditAndTraial("Logout", "Logout");
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            removeCookie();
            Response.Redirect("~/LoginUI.aspx");

       
        }
        private void SaveAccessLog(string loginId, int successStatus, int unSuccessStatus, int userLoginStatus)
        {
            int insertAccessLog = 0;
            UserAccessLog userAccessLogObj = new UserAccessLog();
            userAccessLogObj.UserLogInId = loginId.ToString();
            userAccessLogObj.IP = System.Net.Dns.GetHostName(); ;
            userAccessLogObj.LogInCount = 0;
            //userAccessLogObj.LogInDateTime = DateTime.Now;
            userAccessLogObj.LogOutDateTime = DateTime.Now;
            userAccessLogObj.userLoginSuccessStatus = 0;
            userAccessLogObj.userLoginUnSuccessStatus = 0;
            userAccessLogObj.userLoginStatus = 0;
            userAccessLogObj.AccessFor = "LOGOUT";
            insertAccessLog = userAccessLogManagerObj.SendDataInToDB(userAccessLogObj);
        }
        protected void MainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            string s = e.Item.Text;
        }       
    }
}
