﻿using System;
using System.Collections.Generic;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BSVERPrintUI : System.Web.UI.Page
    {
        public BSVERManager bSVERManagerObj = null;
        public BankStvPersonManager bankStvPersonManagerObj = null;
        public WordDocumentManager wordDocumentManagerObj = null;
        public BASVERReportInfo bASVERReportInfoObj = null;
        string CustomerName = null;
        string BusinessName = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bSVERManagerObj = new BSVERManager();
                bankStvPersonManagerObj = new BankStvPersonManager();
                wordDocumentManagerObj = new WordDocumentManager();
                bASVERReportInfoObj = new BASVERReportInfo();
                Int64 bsverMasterId = 0;
                if (Request.QueryString.Count != 0)
                {
                    bsverMasterId = Convert.ToInt64(Request.QueryString["bsverMasterId"]);
                }
                if (bsverMasterId > 0)
                {
                    LoadBSVERDetailsInfo(bsverMasterId);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "LoadBSVERDetailsInfo");
            }
        }
        private void LoadBSVERDetailsInfo(Int64 bsverMasterId)
        {
            if (bsverMasterId > 0)
            {
                List<BASVERReportInfo> bASVERDetailsObjList = bSVERManagerObj.SelectBASVERReportInfoList(bsverMasterId);
                if (bASVERDetailsObjList.Count > 0)
                {
                    int i = 0;
                    foreach (BASVERReportInfo bSVERDetailsObj in bASVERDetailsObjList)
                    {
                        if (i == 0)
                        {
                            accountNameValueLabel.Text = bSVERDetailsObj.AcName.ToString();
                            accountNoValueLabel.Text = bSVERDetailsObj.AcNo.ToString();
                            bankNameLabel.Text = bSVERDetailsObj.BankName.ToString();
                            branchNameLabel.Text = bSVERDetailsObj.BranchName.ToString();
                            branchAddressLabel.Text = bSVERDetailsObj.BranchAddress.ToString();
                            branchLocationLabel.Text = bSVERDetailsObj.LocationName.ToString();
                            subjectLabel.Text = bSVERDetailsObj.SubjectName.ToString();
                            approverNameLabel.Text = bSVERDetailsObj.ApproverName.ToString();
                            designationLabel.Text = bSVERDetailsObj.ApproverDesignation.ToString();
                            CustomerName = bSVERDetailsObj.CustomerName.ToString();
                            BusinessName = bSVERDetailsObj.BusinessName.ToString();
                            refLabel.Text ="Ref.No:"+ bSVERDetailsObj.LlId.ToString();
                            dateLabel.Text = "Date:" + bSVERDetailsObj.EntryDate.ToString("dd-MM-yyyy");
                            if (bSVERDetailsObj.Subject_Id > 0)
                            {
                                loadWordDocument(bSVERDetailsObj.Subject_Id);
                            }
                            break;
                        }
                    }
                }

            }
        }
        private void loadWordDocument(int subjectId)
        {
            if (subjectId > 0)
            {
                WordDocument eordDocumentObj = wordDocumentManagerObj.SelectWordDocument(subjectId);
                if (eordDocumentObj.Id > 0)
                {
                    string ReplaceText = eordDocumentObj.Body.Replace("*", CustomerName + " ");
                    ReplaceText = ReplaceText.Replace("#", " " + BusinessName + " ");
                    bodyLabel.Text ="Mr./Mrs. "+ ReplaceText.ToString();
                    bodyOneLabel.Text = eordDocumentObj.Body1.ToString();
                    bodyTwoLabel.Text = eordDocumentObj.Body2.ToString();
                    bodyThreeLabel.Text = eordDocumentObj.Body3.ToString();
                    bodyFourLabel.Text = eordDocumentObj.Body4.ToString();
                    bodyFiveLabel.Text = eordDocumentObj.Body5.ToString();
                    bodySixLabel.Text = eordDocumentObj.Body6.ToString();
                }

            }
        }
    }
}
