﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_PayrollUI" Title="Payroll Information Entry" Codebehind="PayrollUI.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server" ID="content">
    <div>
            <table border="0" cellpadding="1" cellspacing="1">
                <tr>
                    <td rowspan="13" valign="top">
                    <br />
                    <br />
                    <div style="border: solid 1px gray;">
                        <div id="gridbox" style="background-color: white; width: 190px; height: 230px;"></div>
                         <div style="display: none" runat="server" id="companyDiv"></div>
                        <%--<asp:GridView Width="100%" ID="gvrPayroll" runat="server" SkinID="SCBLGridGreen" OnSelectedIndexChanged="dataGridView_SelectionChanged"
                        AllowPaging="True" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" EnableModelValidation="True" AutoGenerateColumns="False" CellSpacing="1" GridLines="None" AutoGenerateSelectButton="True">
                        <Columns>
                            <asp:BoundField DataField="CompID" HeaderText="SL." ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                           <asp:BoundField DataField="CompName" HeaderText="Company Name" ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                        </Columns>
                        <EmptyDataTemplate>
                            No Payroll Data found
                        </EmptyDataTemplate>
                        <AlternatingRowStyle CssClass="odd" />
                        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                        <RowStyle ForeColor="Black" BackColor="#DEDFDE" />
                        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>--%>
                        <asp:Button ID="deleteCompanyNameButton" runat="server" 
                            Text="Delete Duplicate Company"  Width="190px" Height="27px" 
                            onclick="deleteCompanyNameButton_Click" />
                    </div>
                    </td>
                    <td style="font-weight: bold; font-size: 14px;" align="center" colspan="2">
                        <asp:Label ID="payRoll" runat="server" Text="Pay Roll Information"></asp:Label>
                    </td>
                    <td style="font-weight: bold; font-size: 18px;" colspan="2">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelCompanyName" runat="server" Text="Company Name :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="companyNameTextBox" runat="server" Text="" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="labelSegemntCode" runat="server" Text="Segemnt Code :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="SegmentCodedropDownList" runat="server" Width="80px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelCompanyStructure" runat="server" Text="Company Structure :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="CompanyStructuredropDownList" runat="server" Width="80px">
                            <asp:ListItem>LC</asp:ListItem>
                            <asp:ListItem>LLC</asp:ListItem>
                            <asp:ListItem>MNC</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="labelDoCategorized" runat="server" Text="DO Categorized :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DoCategorizeddropDownList" runat="server" Width="80px">
                            <asp:ListItem>No</asp:ListItem>
                            <asp:ListItem>Yes</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelCatStatus" runat="server" Text="Cat Status :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="CatStatusdropDownList" runat="server" Width="80px">
                            <asp:ListItem>A</asp:ListItem>
                            <asp:ListItem>B</asp:ListItem>
                            <asp:ListItem>BB</asp:ListItem>
                            <asp:ListItem>BBB</asp:ListItem>
                            <asp:ListItem>C</asp:ListItem>
                            <asp:ListItem>Declined</asp:ListItem>
                            <asp:ListItem>Pending</asp:ListItem>
			    <asp:ListItem>Approved</asp:ListItem>	
                            <asp:ListItem>Unapproved</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="mueLabel" runat="server" Text="MUE :"></asp:Label>
                    </td>
                    <td width="80">
                        <asp:TextBox ID="mueTextBox" runat="server" Width="120px" MaxLength="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelIRwithEOSB" runat="server" Text="IR with EOSB :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="IRwithEOSBTextBox" runat="server" Width="120px" MaxLength="5"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="labelIRwithoutEOSB" runat="server" Text="IR without EOSB :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="IRwithoutEOSBTextBox" runat="server" Width="120px" 
                            MaxLength="5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelCCUCondition" runat="server" Text="CCU Condition :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="CCUConditionTextBox" runat="server" Width="180px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="labelCCURemarks" runat="server" Text="CCU Remarks :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="CCURemarksTextBox" runat="server" Width="180px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelProposdBy" runat="server" Text="Proposed by :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="ProposdByTextBox" runat="server" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="labelCatApprovalDate" runat="server" Text="Approval Date :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="CatApprovalDateTextBox" runat="server" Width="120px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelStatus" runat="server" Text="Status :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="StatusdropDownList" runat="server" Width="80px">
                            <asp:ListItem>Active</asp:ListItem>
                            <asp:ListItem>Inactive</asp:ListItem>                            
                        </asp:DropDownList>
                    </td>
                    <td>
                       <asp:Label ID="label1" runat="server" Text="Extra Benefit:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="extraBenefitDropDownList" runat="server" Width="80px">
                            <asp:ListItem>No</asp:ListItem>
                            <asp:ListItem>Yes</asp:ListItem>                            
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="label2" runat="server" Text="Extra Field1:"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="extraField1TextBox" runat="server" Width="120px" MaxLength="5"></asp:TextBox></td>
                    <td>
                        <asp:Label ID="label3" runat="server" Text="Extra Field2:"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="extraField2TextBox" runat="server" Width="120px" MaxLength="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="label4" runat="server" Text="Extra Field3:"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="extraField3TextBox" runat="server" Width="120px" MaxLength="5"></asp:TextBox></td>
                    <td>
                        <asp:Label ID="label5" runat="server" Text="Extra Field4:"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="extraField4TextBox" runat="server" Width="120px" MaxLength="5"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td colspan="3" align="center">
                                    <asp:Button ID="saveButton" runat="server" Text="Save" OnClick="saveButton_Click"
                                        UseSubmitBehavior="False" Width="100px" Height="27px" />
                                    <asp:Button ID="clearButton" runat="server" Text="Clear" UseSubmitBehavior="False"
                                        OnClick="clearButton_Click" Width="100px" Height="27px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        
                    </td>
                </tr>
            </table>

        <table border="0" cellpadding="1" cellspacing="1">
            <tr>
                <td align="left" style="padding-left: 200px;">
                    <asp:TextBox ID="searchTextBox" runat="server"></asp:TextBox>
                    <asp:Button ID="searchButton" runat="server" Text="Search" OnClick="searchButton_Click" />
                    &nbsp;
                    <asp:FileUpload ID="payrollFileUpload" runat="server" />&nbsp;
                    <asp:Button ID="uploadButton" runat="server" Text="Upload" OnClick="uploadButton_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                <div style="border: solid 1px gray;">
                    <div id="gridbox2" style="background-color: white; width: 800px; height: 250px;"></div>
                     <div style="display: none" runat="server" id="payrollDiv"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
   
    <script type="text/javascript">
	var mygrid;
	mygrid = new dhtmlXGridObject('gridbox');
	mygrid.setImagePath("../codebase/imgs/");
	mygrid.setHeader("SL.,Company Name");
	mygrid.setInitWidths("25,145");
	mygrid.setColAlign("right,left");
	mygrid.setColTypes("ro,ro");
	mygrid.setColSorting("int,str");
	mygrid.setSkin("light");	 
	mygrid.init();
	mygrid.enableSmartRendering(true,20);
	//mygrid.loadXML("../includes/CompanyEntryList.xml");
	mygrid.parse(document.getElementById("xml_data"));
	mygrid.attachEvent("onRowSelect",doOnRowSelected);
    function doOnRowSelected(id)
    {
        if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0)
        {
          document.getElementById('ctl00_ContentPlaceHolder_saveButton').value='Update';
          document.getElementById('ctl00_ContentPlaceHolder_HiddenField1').value = mygrid.cells(mygrid.getSelectedId(),0).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_companyNameTextBox').value =RemoveSpecialChar(mygrid.cells(mygrid.getSelectedId(),1).getValue());
        }
         else
         {
            document.getElementById('ctl00_ContentPlaceHolder_saveButton').value='Save';
         }
    }
    var mygrid2;
	mygrid2 = new dhtmlXGridObject('gridbox2');
	mygrid2.setImagePath("../codebase/imgs/");
	mygrid2.setHeader("SL.,CompanyName,SegmentCode,CompanyStructure,DoCategorized,CatStatus,MUE,IRwithEOSB,IRWithoutEOSB,CCUCondition,CCURemarks,PropostBy,ApprovedDate,Status,ExtraBenefit,ExtraField1,ExtraField2,ExtraField3,ExtraField4");
	mygrid2.setInitWidths("25,130,55,55,55,55,55,50,50,50,50,50,50,50,50,50,50,50,50");
	mygrid2.setColAlign("right,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
	mygrid2.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	mygrid2.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
	//mygrid2.setSkin("Gray");
	mygrid2.setSkin("light");	 
	mygrid2.init();
	mygrid2.enableSmartRendering(true,20);
        //mygrid2.loadXML("../includes/PayRollList.xml");
	mygrid2.parse(document.getElementById("payroll_data"));
	mygrid2.attachEvent("onRowSelect",doOnRowSelected2);
    function doOnRowSelected2(id) {
        debugger;
        if(mygrid2.cells(mygrid2.getSelectedId(),0).getValue()>0)
        {
          document.getElementById('ctl00_ContentPlaceHolder_saveButton').value='Update';
          document.getElementById('ctl00_ContentPlaceHolder_HiddenField1').value = mygrid2.cells(mygrid2.getSelectedId(),0).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_companyNameTextBox').value = RemoveSpecialChar(mygrid2.cells(mygrid2.getSelectedId(),1).getValue());
          document.getElementById('ctl00_ContentPlaceHolder_SegmentCodedropDownList').value = mygrid2.cells(mygrid2.getSelectedId(), 2).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_CompanyStructuredropDownList').value = mygrid2.cells(mygrid2.getSelectedId(), 3).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_DoCategorizeddropDownList').value = mygrid2.cells(mygrid2.getSelectedId(), 4).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_CatStatusdropDownList').value = mygrid2.cells(mygrid2.getSelectedId(), 5).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_mueTextBox').value = mygrid2.cells(mygrid2.getSelectedId(),6).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_IRwithEOSBTextBox').value = mygrid2.cells(mygrid2.getSelectedId(),7).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_IRwithoutEOSBTextBox').value = mygrid2.cells(mygrid2.getSelectedId(),8).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_CCUConditionTextBox').value = RemoveSpecialChar(mygrid2.cells(mygrid2.getSelectedId(),9).getValue());
          document.getElementById('ctl00_ContentPlaceHolder_CCURemarksTextBox').value = RemoveSpecialChar(mygrid2.cells(mygrid2.getSelectedId(),10).getValue());
          document.getElementById('ctl00_ContentPlaceHolder_ProposdByTextBox').value = mygrid2.cells(mygrid2.getSelectedId(),11).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_CatApprovalDateTextBox').value = mygrid2.cells(mygrid2.getSelectedId(),12).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_statusDropDownList').value = mygrid2.cells(mygrid2.getSelectedId(), 13).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_extraBenefitDropDownList').value = mygrid2.cells(mygrid2.getSelectedId(), 14).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_extraField1TextBox').value = RemoveSpecialChar(mygrid2.cells(mygrid2.getSelectedId(),15).getValue());
          document.getElementById('ctl00_ContentPlaceHolder_extraField2TextBox').value = RemoveSpecialChar(mygrid2.cells(mygrid2.getSelectedId(),16).getValue());
          document.getElementById('ctl00_ContentPlaceHolder_extraField3TextBox').value = RemoveSpecialChar(mygrid2.cells(mygrid2.getSelectedId(),17).getValue());
          document.getElementById('ctl00_ContentPlaceHolder_extraField4TextBox').value = RemoveSpecialChar(mygrid2.cells(mygrid2.getSelectedId(),18).getValue());

        }
         else
         {
          document.getElementById('ctl00_ContentPlaceHolder_saveButton').value='Save';
         }
    }
    function LoadExcelData()
    {
        mygrid2.clearAll();
        // mygrid2.loadXML("../includes/PayRollList.xml");
        mygrid2.parse(document.getElementById("payroll_data"));
    }
    function RemoveSpecialChar(sMessage)
    {
        var returnMessage = "";
        if (sMessage!=null)
        {
            sMessage = sMessage.replace("&amp;","&");
            sMessage = sMessage.replace("&gt;","<");
            sMessage = sMessage.replace("&lt;",">");
            returnMessage = sMessage;
        }
        return returnMessage;
    }
    </script>

</asp:Content>
