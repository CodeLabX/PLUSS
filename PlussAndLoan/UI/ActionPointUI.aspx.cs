﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_ActionPointUI : System.Web.UI.Page
    {
        public ActionPointsManager actionPointsManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;


        protected void Page_Load(object sender, EventArgs e)
        {
            actionPointsManagerObj = new ActionPointsManager();
            if (!Page.IsPostBack)
            {
                LoadData("");
            }
        }

        private void LoadData(string searchKey)
        {
           
            List<ActionPoints> actionPointsObjList = new List<ActionPoints>();
            if (searchKey == "")
            {
                actionPointsObjList = actionPointsManagerObj.SelectActionPointsList();
            }
            else
            {
                actionPointsObjList = actionPointsManagerObj.SelectActionPointsList(searchKey);
            }
            try
            {

                var xml = "<xml id='xml_data'><rows>";
                foreach (ActionPoints actionPointsObj in actionPointsObjList)
                {
                   xml+="<row id='" + actionPointsObj.Id.ToString() + "'>";

                   xml+="<cell>";
                   xml+=actionPointsObj.Id.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=actionPointsObj.ColorName.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=actionPointsObj.NewSourcing.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=actionPointsObj.IndustryProfitMargin.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=actionPointsObj.Deviation.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=actionPointsObj.Topup.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=actionPointsObj.BureauPolicy.ToString();
                   xml+="</cell>";
                    if (actionPointsObj.Status == 1)
                    {
                       xml+="<cell>";
                       xml+="Active";
                       xml+="</cell>";
                    }
                    else
                    {
                       xml+="<cell>";
                       xml+="Inactive";
                       xml+="</cell>";
                    }
                xml+="</row>";
                }
                 xml += "</rows></xml>";
                actionDiv.InnerHtml = xml;
            }
            finally
            {
            }
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                var msg = "";
                int actionPoints = 0;
                ActionPoints actionPointsObj = new ActionPoints();
                if (IsBlank())
                {
                    actionPointsObj.Id = Convert.ToInt64(idHiddenField.Value);
                    actionPointsObj.ColorName = Convert.ToString(colorNameTextBox.Text.Trim());
                    actionPointsObj.NewSourcing = Convert.ToString(newSourceTextBox.Text.Trim());
                    actionPointsObj.IndustryProfitMargin = Convert.ToString(indstryPrfMrgTextBox.Text.Trim());
                    actionPointsObj.Deviation = Convert.ToString(deviationTextBox.Text.Trim());
                    actionPointsObj.Topup = Convert.ToString(topupTextBox.Text.Trim());
                    actionPointsObj.BureauPolicy = Convert.ToString(bureauPolicyTextBox.Text.Trim());
                    if (statusDropDowonList.Text == "Active")
                    {
                        actionPointsObj.Status = 1;
                    }
                    else
                    {
                        actionPointsObj.Status = 0;
                    }
                    actionPoints = actionPointsManagerObj.SendActionPointsInToDB(actionPointsObj);
                    if (actionPoints == INSERTE)
                    {
                        msg="Save successfully";
                        LoadData("");
                        ClearField();
                    }
                    else if (actionPoints == UPDATE)
                    {
                        msg="Update successfully";
                        LoadData("");
                        ClearField();
                    }
                    else if (actionPoints == ERROR)
                    {
                        msg="Error in data!";
                    }
                    Alert.Show(msg);
                    new AT(this).AuditAndTraial("Action Point," + System.Web.HttpContext.Current.Session["UserId"],"");

                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save ActionPoint");
            }

        }
        private bool IsBlank()
        {
            if (colorNameTextBox.Text == "")
            {
                Alert.Show("Please input Color name");
                colorNameTextBox.Focus();
                return false;
            }
            else if (newSourceTextBox.Text == "")
            {
                Alert.Show("Please input New Sourcing");
                newSourceTextBox.Focus();
                return false;
            }
            else if (indstryPrfMrgTextBox.Text == "")
            {
                Alert.Show("Please input Industry Profit Margin");
                indstryPrfMrgTextBox.Focus();
                return false;
            }
            else if (deviationTextBox.Text == "")
            {
                Alert.Show("Please input Deviation");
                deviationTextBox.Focus();
                return false;
            }
            else if (topupTextBox.Text == "")
            {
                Alert.Show("Please input Top-up");
                topupTextBox.Focus();
                return false;
            }
            else if (bureauPolicyTextBox.Text == "")
            {
                Alert.Show("Please input Bureau Policy");
                bureauPolicyTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearField();
        }
        private void ClearField()
        {
            idHiddenField.Value = actionPointsManagerObj.SelectMaxActionPointsId().ToString();
            colorNameTextBox.Text = "";
            newSourceTextBox.Text = "";
            indstryPrfMrgTextBox.Text = "";
            deviationTextBox.Text = "";
            topupTextBox.Text = "";
            bureauPolicyTextBox.Text = "";
            statusDropDowonList.SelectedIndex = 0;
        }

    }
}
