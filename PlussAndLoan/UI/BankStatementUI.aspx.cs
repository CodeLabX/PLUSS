﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Xml;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BankStatementUI : System.Web.UI.Page
    {
        private string lLId = string.Empty;
        private string bankId = string.Empty;
        private string branchId = string.Empty;
        private string aCNo = string.Empty;
        private string aCName = string.Empty;
        private string acTypeId = string.Empty;
        private BankStatementManager bankStatementManagerObj = new BankStatementManager();
        private ExcelTemplateManager excelTemplateManagerObj = new ExcelTemplateManager();
        public ApplicantOtherBankAccount otherBankAccountObj = null;
        public BankStatement bankStatementObj = null;
        public SCBBankNameListManager SCBBNListManagerObj = null;


        private XmlDocument xmlDoc = new XmlDocument();
        private XmlNode msgDataNode;

        protected void Page_Load(object sender, EventArgs e)
        {
            //
            try
            {
                string Id = string.Empty;
                string updatedBy = string.Empty;
                string strOperation = string.Empty;
                strOperation = Convert.ToString(Request.QueryString["operation"]);
                lLId = Convert.ToString(Request.QueryString["LLId"]);
                bankId = Convert.ToString(Request.QueryString["BANKID"]);
                branchId = Convert.ToString(Request.QueryString["BRANCHID"]);
                aCNo = Convert.ToString(Request.QueryString["ACNO"]);
                aCName = Convert.ToString(Request.QueryString["ACNAME"]);
                acTypeId = Convert.ToString(Request.QueryString["ACTYPEID"]);
                Id = Convert.ToString(Request.QueryString["ID"]);
                if (strOperation == "save")
                {
                    StreamReader streamReader = new StreamReader(Request.InputStream);
                    string spreadsheetdata = streamReader.ReadToEnd();
                    Response.Write(SaveStatementData(spreadsheetdata));
                    Response.End();
                }
                else if (strOperation == "loadTemplate")
                {
                    if (lLId.Length > 0 & bankId.Length > 0 & branchId.Length > 0 & aCNo.Length > 0)
                    {
                        Response.Write(GetStatementData(Convert.ToInt64(lLId), Convert.ToInt16(bankId), Convert.ToInt16(branchId), aCNo));
                    }
                }
                else if (strOperation == "LoadDefaultTemplate")
                {
                    if (Id.Length > 0)
                    {
                        Response.ContentType = "Application/vnd.ms-excel";
                        Response.Write(GetTemplate(6));
                    }
                }
                otherBankAccountObj = new ApplicantOtherBankAccount();
                bankStatementManagerObj = new BankStatementManager();
                SCBBNListManagerObj = new SCBBankNameListManager();
               
            }
            catch (Exception ex)
            {
                CustomException.Save(ex, "Bank Statment");
                Response.Write("error occured");
                Response.End();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();", true);
        }

        private String GetTemplate(Int32 Id)
        {
            string sReturnData = String.Empty;
            List<ExcelTemplate> excelTemplateObjList = new List<ExcelTemplate>();
            try
            {
                excelTemplateObjList = excelTemplateManagerObj.GetAllExcelTemplate(Id);
            }
            catch (Exception ex)
            {
            }
            return sReturnData = excelTemplateObjList[0].SheetText;
        }
        private String SaveStatementData(string sData)
        {
            String sReturnData = String.Empty;
            int returnVlue = -1;
            try
            {
                XmlNodeList msgDataNodeList;
                string parseData = ParseValue(sData, "ss:Table");
                parseData = parseData.Replace("ss:table", "Table").Replace("ss:", "");
                xmlDoc.LoadXml(parseData);
                msgDataNodeList = xmlDoc.SelectNodes("//Row");
                string data = msgDataNodeList[1].SelectNodes("//Data")[12].InnerText;
                BankStatement bankStatementObj = new BankStatement();
                bankStatementObj.BankStatementId = Convert.ToInt32("0");
                bankStatementObj.LlId = Convert.ToInt64("0" + lLId);
                bankStatementObj.Bank_ID = Convert.ToInt16("0" + bankId);
                bankStatementObj.Branch_ID = Convert.ToInt16("0" + branchId);
                bankStatementObj.ACNO = aCNo;
                bankStatementObj.ACType_Id = Convert.ToInt16("0" + acTypeId);
                bankStatementObj.ACName = aCName;
                bankStatementObj.StartMonth = msgDataNodeList[1].SelectNodes("//Data")[1].InnerText;
                bankStatementObj.EndMonth = msgDataNodeList[1].SelectNodes("//Data")[12].InnerText;
                bankStatementObj.Statement = sData;
                bankStatementObj.EntryDate = DateTime.Now;
                bankStatementObj.UserId = 0;
                returnVlue = bankStatementManagerObj.SendBankStatementInToDB(bankStatementObj);
                if (returnVlue > -1)
                {
                    sReturnData = "Save Successfully  #";
                }
                else
                {
                    sReturnData = "Save Error  #";
                }
            }
            catch (Exception ex)
            {
                sReturnData = ex.Message+ " #";
                CustomException.Save(ex, "Save Bank Statment");
            }
            new AT(this).AuditAndTraial("Save Bank Statement", sReturnData);
            return sReturnData;
        }
        public string ParseValue(string xmlString, string xmlTag)
        {
            string TagValue = null;
            string StringXML = xmlString;
            string TagXML = xmlTag;
            xmlString = xmlString.ToUpper();
            string StartTag = xmlTag.ToUpper();
            string EndTag = "/" + xmlTag.ToUpper(); 
            int j = 0;
            int i = xmlString.IndexOf(StartTag, 0);
            if (i >= 0)
                j = xmlString.IndexOf(EndTag, 0);
            if (i >= 0 && j >= i)
            {
                i = i + StartTag.Length;
                TagValue = "<" + StartTag.ToLower() + " " + StringXML.Substring(i, j - i) + EndTag.ToLower() + ">";
            }
            else
            {
                TagValue = "";
            }
            return TagValue;
        }
        private String GetStatementData(Int64 LLId,Int16 BankId,Int16 BranchId,string ACNo)
        {
            string sReturnData = String.Empty;
            BankStatement bankStatementObj = new BankStatement();
            try
            {
                bankStatementObj = bankStatementManagerObj.GetBankStatement(LLId, BankId, BranchId, ACNo);
                if (bankStatementObj != null)
                {
                    sReturnData = bankStatementObj.Statement;
                }
                else
                {
                    sReturnData = GetTemplate(6);
                }
            }
            catch (Exception ex)
            {
            }
            return sReturnData;
        }

   
      
    }
}
