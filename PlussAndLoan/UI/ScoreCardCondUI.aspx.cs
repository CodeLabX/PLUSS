﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_ScoreCardCondUI : System.Web.UI.Page
    {
        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2
        }
        #endregion

        private List<ScoreCardCondition> scoreCardConditionListObj;
        private ScoreCardConditionManager scoreCardConditionManagerObj;
        private List<VariableName> variableNameListObj;
        protected void Page_Load(object sender, EventArgs e)
        {
            scoreCardConditionManagerObj = new ScoreCardConditionManager();
            variableNameListObj = scoreCardConditionManagerObj.GetAllVariableName();

            cutOffScoreTextBox.Text = "400";
            interceptValueTextBox.Text = "423";
            if (!IsPostBack)
            {
                FillScoreCardConditionGridView();
                PopulateVariableNameDropDownList();
                variableNameDropDownList.SelectedValue = "0";
            }
        }

        private void FillScoreCardConditionGridView()
        {
            SearchScoreCardCondition("");
        }

        private void SearchScoreCardCondition(string searchKey)
        {
            scoreCardConditionListObj = scoreCardConditionManagerObj.GetScoreCardCondition(searchKey);
            conditionGridView.DataSource = scoreCardConditionListObj;
            conditionGridView.DataBind();
        }

        private void PopulateVariableNameDropDownList()
        {
            VariableName variableNameObj = new VariableName();
            variableNameObj.Id = 0;
            variableNameObj.Name = "Null";
            variableNameListObj.Add(variableNameObj);
            variableNameDropDownList.DataSource = variableNameListObj;
            variableNameDropDownList.DataTextField = "Name";
            variableNameDropDownList.DataValueField = "Id";
            variableNameDropDownList.DataBind();
        }

        protected void conditionGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(conditionGridView.SelectedIndex);
            if (index > -1)
            {
                errorMsgLabel.Text = "";
                addButton.Text = "Update";
                int rowIndex = Convert.ToInt32(conditionGridView.SelectedRow.RowIndex);
                idHiddenField.Value = (conditionGridView.Rows[rowIndex].Cells[1].FindControl("idLabel") as Label).Text;
                variableNameDropDownList.SelectedValue = (conditionGridView.Rows[rowIndex].Cells[2].FindControl("variableIdLabel") as Label).Text;
                compOpDropDownList.Text = (conditionGridView.Rows[rowIndex].Cells[3].FindControl("compOperatorLabel") as Label).Text;
                compValueTextBox.Text = Convert.ToString((conditionGridView.Rows[rowIndex].Cells[4].FindControl("compValueLabel") as Label).Text);
                scoreValueTextBox.Text = (conditionGridView.Rows[rowIndex].Cells[5].FindControl("scoreValueLabel") as Label).Text;
                negativeValueTextBox.Text = (conditionGridView.Rows[rowIndex].Cells[6].FindControl("negativeValueLabel") as Label).Text;
                logicalOpDropDownList.Text = (conditionGridView.Rows[rowIndex].Cells[7].FindControl("logicalOperatorLabel") as Label).Text;
            }
        }
        protected void searchbutton_Click(object sender, EventArgs e)
        {
            try
            {
                errorMsgLabel.Text = "";
                string searchKey = "";
                if (String.IsNullOrEmpty(searchTextBox.Text))
                {
                    searchKey = "";
                }
                else
                {            
                    searchKey = searchTextBox.Text.Trim();
                }
                SearchScoreCardCondition(searchKey);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Search Score card");
            }
        }
        protected void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                int insertOrUpdateRow = -1;
                ScoreCardCondition scoreCardConditionObj = new ScoreCardCondition();
                if (variableNameDropDownList.SelectedValue.Equals("0") || compOpDropDownList.Text.Equals("Null"))
                {
                    errorMsgLabel.Text = "Please input score card condition information.";
                }
                else
                {
                    errorMsgLabel.Text = "";

                    if (String.IsNullOrEmpty(idHiddenField.Value))
                    {
                        scoreCardConditionObj.Id = 0;
                    }
                    else
                    {
                        scoreCardConditionObj.Id = Convert.ToInt32(idHiddenField.Value);
                    }
                    scoreCardConditionObj.VariableId = Convert.ToInt32(variableNameDropDownList.SelectedValue);
                    scoreCardConditionObj.CompareOperator = compOpDropDownList.Text.Trim();
                    scoreCardConditionObj.CompareValue = Convert.ToDouble(compValueTextBox.Text.Trim());
                    scoreCardConditionObj.LogicalOperator = "";
                    scoreCardConditionObj.ScoreValue = Convert.ToDouble(scoreValueTextBox.Text.Trim());
                    if (!String.IsNullOrEmpty(negativeValueTextBox.Text.Trim()))
                    {
                        scoreCardConditionObj.NegativeValue = Convert.ToDouble(negativeValueTextBox.Text.Trim());
                    }
                    else
                    {
                        scoreCardConditionObj.NegativeValue = Convert.ToDouble("0" + negativeValueTextBox.Text.Trim());
                    }
                    scoreCardConditionObj.CuttOffScore = Convert.ToDouble(cutOffScoreTextBox.Text.Trim());
                    scoreCardConditionObj.InterceptValue = Convert.ToDouble(interceptValueTextBox.Text.Trim());
                    scoreCardConditionObj.Status = logicalOpDropDownList.Text.Trim(); 

                    insertOrUpdateRow = scoreCardConditionManagerObj.InsertORUpdateScoreCardConditionInfo(scoreCardConditionObj);

                    if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT))
                    {
                        errorMsgLabel.Text = "Data save sucessfully.";
                        addButton.Text = "Add";
                        ClearField();
                        FillScoreCardConditionGridView();

                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERTERROR))
                    {
                        errorMsgLabel.Text = "Data not saved sucessfully.";
                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                    {
                        errorMsgLabel.Text = "Data update sucessfully.";
                        addButton.Text = "Add";
                        ClearField();
                        FillScoreCardConditionGridView();
                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                    {
                        errorMsgLabel.Text = "Data not update sucessfully.";
                    }
                    new AT(this).AuditAndTraial("Score Card", errorMsgLabel.Text);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Score Card");
            }
        }

        private void ClearField()
        {
            idHiddenField.Value = "";
            variableNameDropDownList.SelectedValue = "0";
            compOpDropDownList.Text = "Null";
            compValueTextBox.Text = "";
            logicalOpDropDownList.Text = "Null";
            scoreValueTextBox.Text = "";
            negativeValueTextBox.Text = "";
        }

        protected void clearButton_Click(object sender, EventArgs e)
        {
            if (addButton.Text.Equals("Add"))
            {
                errorMsgLabel.Text = "";
                ClearField();
            }
            if (addButton.Text.Equals("Update"))
            {
                errorMsgLabel.Text = "";
                variableNameDropDownList.SelectedValue = "0";
                compOpDropDownList.Text = "Null";
                compValueTextBox.Text = "";
                scoreValueTextBox.Text = "";
                negativeValueTextBox.Text = "";
            }
        }
        protected void conditionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(conditionGridView, "Select$" + e.Row.RowIndex.ToString()));
                e.Row.Attributes["onmouseover"] = "this.style.cursor = 'pointer'";
                e.Row.Focus();
            }
        }
        protected override void Render(HtmlTextWriter writer)
        {
            for (int i = 0; i < conditionGridView.Rows.Count; i++)
            {
                Page.ClientScript.RegisterForEventValidation(new PostBackOptions(conditionGridView, "Select$" + i.ToString()));
            }
            base.Render(writer);
        }
    }
}
