﻿<%@ Page Language="C#" MasterPageFile="~/UI/SupportMasterPage.master" AutoEventWireup="true"
    Inherits="PlussAndLoan.UI.UI_BankStatementUI" Title="Bank Statement" Codebehind="BankStatementUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/dhtmlxgrid_skinscss.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_skinscss" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgridcss.ascx" TagPrefix="uc1" TagName="dhtmlxgridcss" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon.ascx" TagPrefix="uc1" TagName="dhtmlxcommon" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid.ascx" TagPrefix="uc1" TagName="dhtmlxgrid" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_srnd.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_srnd" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgridcell.ascx" TagPrefix="uc1" TagName="dhtmlxgridcell" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="Server">
    <uc1:dhtmlxgridcss runat="server" ID="dhtmlxgridcss" />
    <uc1:dhtmlxgrid_skinscss runat="server" ID="dhtmlxgrid_skinscss" />
    <uc1:dhtmlxcommon runat="server" ID="dhtmlxcommon" />
    <uc1:dhtmlxgrid runat="server" ID="dhtmlxgrid" />
    <uc1:dhtmlxgrid_srnd runat="server" ID="dhtmlxgrid_srnd" />
    <uc1:dhtmlxgridcell runat="server" ID="dhtmlxgridcell" />

</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
   
        <table width="100%" border="0" cellpadding="1" cellspacing="1">
             
            <tr>
                <td>
                    <asp:Label runat="server" ID="Label1" Text="Bank Statement"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td style="width: 15%;">
                                <asp:Label runat="server" ID="lLIdLabe" Text="LLID :"></asp:Label>
                            </td>
                            <td style="width: 35%;">
                                <asp:textbox id="LLIdTextBox"  width="30%" MaxLength="9" runat="server"></asp:textbox>
                               <%-- <asp:textbox ID="Textbox1" size="30%" MaxLength="9" runat="server"></asp:textbox>/>--%>
                                <input type="button" value="..." onclick="JS_FunctionLoadGrid();" />
                            </td>
                            <td rowspan="6" style="width: 50%;" valign="top">
                                <div id="gridbox" style="background-color: white; width: 100%; height: 145px;">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="bankNameLabe" Text="Bank Name :"></asp:Label>
                            </td>
                            <td>
                                <input id="bankNameTextBox" type="text" size="40%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="branchNameLabe" Text="Branch Name :"></asp:Label>
                            </td>
                            <td>
                                <input id="branchNameTextBox" type="text" size="40%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="acNoLabe" Text="A/C No :"></asp:Label>
                            </td>
                            <td>
                                <input id="acNoTextBox" type="text" size="40%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="acTypeLabe" Text="A/C Type :"></asp:Label>
                            </td>
                            <td>
                                <input id="acTypeTextBox" type="text" size="40%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="acNameLabe" Text="A/C Name :"></asp:Label>
                            </td>
                            <td>
                                <input id="acNameTextBox" type="text" size="40%" />
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <object classid="clsid:0002E559-0000-0000-C000-000000000046" id="sp" width="100%"
                        style="height: 250px">
                        <param name="XMLData" value="'<?xml version='1.0'?><ss:Workbook xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:c='urn:schemas-microsoft-com:office:component:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'><x:ExcelWorkbook><x:ProtectStructure>False</x:ProtectStructure><x:ActiveSheet>0</x:ActiveSheet></x:ExcelWorkbook><ss:Worksheet ss:Name='Sheet0'><x:WorksheetOptions><x:ViewableRange>R1:R262144</x:ViewableRange><x:Selection>R1C1</x:Selection><x:TopRowVisible>0</x:TopRowVisible><x:LeftColumnVisible>0</x:LeftColumnVisible><x:ProtectContents>False</x:ProtectContents></x:WorksheetOptions><c:WorksheetOptions></c:WorksheetOptions></ss:Worksheet></ss:Workbook>'" />
                    </object>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" value="Save" onclick="SaveTemplate()" style="Width:100px; Height:28px;" />
                    <input type="button" value="Clear all" onclick="Js_functionClear()" style="Width:100px; Height:28px;" />
                </td>
            </tr>
           
        </table>
     <script type="text/javascript">
         var mygrid;
         mygrid = new dhtmlXGridObject('gridbox');
         function doOnLoad() {
             debugger;
             mygrid.setImagePath("../../codebase/imgs/");
             mygrid.setHeader("Sl,BankId,BankName,BranchId,BranchName,A/C No,ACTypeId,ACName,From,To,StartManth,LLID,ACType");
             mygrid.setInitWidths("25,0,100,0,100,90,0,0,80,80,0,0,0");
             mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left");
             mygrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,co,co,ro,ro,ro");
             mygrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str");
             mygrid.setSkin("light");

             mygrid.enableSmartRendering(true, 5);
             mygrid.init();
             mygrid.parse("../UI/BankListShowXML.aspx");
             mygrid.attachEvent("onRowSelect", DoOnRowSelected);
         }

         function DoOnRowSelected() {
             if (mygrid.cells(mygrid.getSelectedId(), 0).getValue() > 0) {

                 //Js_functionClear();
                 document.getElementById('ctl00_ContentPlaceHolder_LLIdTextBox').value = mygrid.cells(mygrid.getSelectedId(), 11).getValue();
                 document.getElementById('bankNameTextBox').value = mygrid.cells(mygrid.getSelectedId(), 2).getValue();
                 //document.getElementById('ctl00_ContentPlaceHolder_bankIdHiddenField').value = mygrid.cells(mygrid.getSelectedId(),1).getValue();
                 document.getElementById('branchNameTextBox').value = mygrid.cells(mygrid.getSelectedId(), 4).getValue();
                 //document.getElementById('ctl00_ContentPlaceHolder_branchIdHiddenField').value = mygrid.cells(mygrid.getSelectedId(),3).getValue();
                 document.getElementById('acNoTextBox').value = mygrid.cells(mygrid.getSelectedId(), 5).getValue();
                 document.getElementById('acTypeTextBox').value = mygrid.cells(mygrid.getSelectedId(), 12).getValue();
                 //document.getElementById('ctl00_ContentPlaceHolder_acTypeIdHiddenField').value = mygrid.cells(mygrid.getSelectedId(),6).getValue();
                 document.getElementById('acNameTextBox').value = mygrid.cells(mygrid.getSelectedId(), 7).getValue();
                 //document.getElementById('ctl00_ContentPlaceHolder_startMonthDropDownList').value = mygrid.cells(mygrid.getSelectedId(),10).getValue();
                 //document.getElementById('ctl00_ContentPlaceHolder_mesageLabel').innerHTML ="Please wait Loading...";
                 LoadTemplate();
             }
         }
         function JS_FunctionLoadGrid() {
             debugger;
             var LLId = document.getElementById('ctl00_ContentPlaceHolder_LLIdTextBox').value;
             if (LLId.length != "") {
                 //mygrid.clearAll();
                 //mygrid.init();
                 mygrid.load("../UI/BankListShowXML.aspx?numLLId=" + LLId);
                 //mygrid.loadXML("../includes/bankStatement.xml");
                 LoadDefaultTemplate();
             }
             else {
                 alert("Please enter LLID");
             }
         }

         function Js_functionClear() {
             document.getElementById('ctl00_ContentPlaceHolder_LLIdTextBox').value = "";
             document.getElementById('bankNameTextBox').value = "";
             document.getElementById('branchNameTextBox').value = "";
             document.getElementById('acNoTextBox').value = "";
             document.getElementById('acTypeTextBox').value = "";
             document.getElementById('acNameTextBox').value = "";
             mygrid.clearAll();
             LoadDefaultTemplate();
         }

         function LoadTemplate() {
             try {

                 XMLHttpRequestObject = new XMLHttpRequest();
                 var spreadsheet = document.getElementById("sp");

                 if (XMLHttpRequestObject) {
                     var lLId = mygrid.cells(mygrid.getSelectedId(), 12).getValue();
                     var bankId = mygrid.cells(mygrid.getSelectedId(), 1).getValue();
                     var branchId = mygrid.cells(mygrid.getSelectedId(), 3).getValue();
                     var acNo = mygrid.cells(mygrid.getSelectedId(), 5).getValue();
                     XMLHttpRequestObject.open("POST", "BankStatementUI.aspx?operation=loadTemplate&LLID=" + lLId + "&BANKID=" + bankId + "&BRANCHID=" + branchId + "&ACNO=" + acNo);
                     XMLHttpRequestObject.onreadystatechange = function () {
                         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                             spreadsheet.XMLData = XMLHttpRequestObject.responseText;
                         }
                     }
                     XMLHttpRequestObject.send(null);
                     //document.getElementById('ctl00_bodyContentPlaceHolder_lblMessage').innerText=XMLHttpRequestObject.responseText;
                 }
             }
             catch (err)
             { }
         }
         function LoadDefaultTemplate() {
             try {
                 debugger;
                 var XMLHttpRequestObject1 = new XMLHttpRequest();
                 var spreadsheet = document.getElementById("sp");
                 if (XMLHttpRequestObject1) {
                     XMLHttpRequestObject1.open("POST", "BankStatementUI.aspx?operation=LoadDefaultTemplate&ID=6");
                     XMLHttpRequestObject1.onreadystatechange = function () {
                         if (XMLHttpRequestObject1.readyState == 4 && XMLHttpRequestObject1.status == 200) {
                             spreadsheet.XMLData = XMLHttpRequestObject1.responseText;
                         }
                     }
                     XMLHttpRequestObject1.send(null);
                 }
             } catch (err) {
                 console.log(err);
                 console.log(err.statusText);

             }
         }
         function SaveTemplate() {
             try {
                 debugger;
                 var spreadsheet = document.getElementById("sp");
                 var acName = document.getElementById("acNameTextBox").value;
                 XMLHttpRequestObject = new XMLHttpRequest();
                 if (XMLHttpRequestObject) {
                     var lLId = parseInt(mygrid.cells(mygrid.getSelectedId(), 12).getValue());
                     var bankId = parseInt(mygrid.cells(mygrid.getSelectedId(), 1).getValue());
                     var branchId = parseInt(mygrid.cells(mygrid.getSelectedId(), 3).getValue());
                     var acNo = parseInt(mygrid.cells(mygrid.getSelectedId(), 5).getValue());
                     var acTypeId = parseInt(mygrid.cells(mygrid.getSelectedId(), 6).getValue());
                     XMLHttpRequestObject.open("POST", "BankStatementUI.aspx?operation=save&LLID=" + lLId + "&BANKID=" + bankId + "&BRANCHID=" + branchId + "&ACNO=" + acNo + "&ACNAME=" + acName + "&ACTYPEID=" + acTypeId);
                     XMLHttpRequestObject.onreadystatechange = function () {
                         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                             document.getElementById('sp').XMLData = spreadsheet.XMLData;
                             var TypeData = new Array();
                             TypeData = XMLHttpRequestObject.responseText.split('#');
                             alert(TypeData[0]);
                         }
                     }
                     XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                     //XMLHttpRequestObject.setRequestHeader("Content-length", spreadsheet.XMLData.length);
                     XMLHttpRequestObject.send(spreadsheet.XMLData);
                 }
             }
             catch (err)
             { }
         }
		</script>
</asp:Content>
