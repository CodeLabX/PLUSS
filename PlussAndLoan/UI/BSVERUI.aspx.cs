﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BSVERUI : System.Web.UI.Page
    {
        public SubjectManager subjectManagerObj = null;
        public ApproverManager approverManagerObj = null;
        public LoanApplicationManager loanApplicationManagerObj = null;
        public ApplicantOtherBankAccount otherBankAccountObj = null;
        public BankStatementManager bankStatementManagerObj = null;
        public BankStatement bankStatementObj = null;
        public BSVERManager bSVERManagerObj = null;
        public BankStvPersonManager bankStvPersonManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            subjectManagerObj = new SubjectManager();
            approverManagerObj = new ApproverManager();
            loanApplicationManagerObj = new LoanApplicationManager();
            otherBankAccountObj = new ApplicantOtherBankAccount();
            bankStatementManagerObj = new BankStatementManager();
            bankStatementObj = new BankStatement();
            bSVERManagerObj = new BSVERManager();
            bankStvPersonManagerObj = new BankStvPersonManager();
           // ScriptManager.RegisterClientScriptInclude(this, typeof(Page), "BSVERValidation", ResolveClientUrl("~/Scripts/BSVERValidation.js"));

            customerAddressTextBox.Text = Request[this.customerAddressTextBox.UniqueID];
            customerNameTextBox.Text = Request[this.customerNameTextBox.UniqueID];
            acNameTextBox.Text = Request[this.acNameTextBox.UniqueID];
            acNoTextBox.Text = Request[this.acNoTextBox.UniqueID];
            dearTextBox.Text = Request[this.dearTextBox.UniqueID];
            emailAddressTextBox.Text = Request[this.emailAddressTextBox.UniqueID];
            if (!Page.IsPostBack)
            {
                int i = 0;
                List<string> stringList = new List<string>();
                for (i = 0; i < 4; i++)
                {
                    stringList.Add(i.ToString());
                }
                BSVARGridView.DataSource = stringList;
                BSVARGridView.DataBind();
                i = 0;
                string[] sArray = new string[4];
                sArray[0] = "Opening balance";
                sArray[1] = "Sample txns randomly selected";
                sArray[2] = "Sample txns randomly selected";
                sArray[3] = "Closing balance";
                foreach (GridViewRow gr in BSVARGridView.Rows)
                {
                    (gr.FindControl("particularLabel") as Label).Text = sArray[i].ToString();
                    i++;
                }
                LoadInt();
                AccountTypeDropDownListItem();
                LoadBankNameList(0);
                approverDropDownList.Attributes.Add("onChange", "JS_FunctionLoadDesignation()");
                statementTypeDropDownList.Attributes.Add("onChange", "JS_functionButtonTextChange()");
                (BSVARGridView.Rows[0].Cells[2].FindControl("dateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (BSVARGridView.Rows[0].Cells[2].FindControl("dateTextBox") as TextBox).ClientID + "');");
                (BSVARGridView.Rows[1].Cells[2].FindControl("dateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (BSVARGridView.Rows[1].Cells[2].FindControl("dateTextBox") as TextBox).ClientID + "');");
                (BSVARGridView.Rows[2].Cells[2].FindControl("dateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (BSVARGridView.Rows[2].Cells[2].FindControl("dateTextBox") as TextBox).ClientID + "');");
                (BSVARGridView.Rows[3].Cells[2].FindControl("dateTextBox") as TextBox).Attributes.Add("onblur", "IdDateFormate('" + (BSVARGridView.Rows[3].Cells[2].FindControl("dateTextBox") as TextBox).ClientID + "');");

                (BSVARGridView.Rows[0].Cells[2].FindControl("dateTextBox") as TextBox).Attributes.Add("onkeydown", "AutoFormatDate('" + (BSVARGridView.Rows[0].Cells[2].FindControl("dateTextBox") as TextBox).ClientID + "');");
                (BSVARGridView.Rows[1].Cells[2].FindControl("dateTextBox") as TextBox).Attributes.Add("onkeydown", "AutoFormatDate('" + (BSVARGridView.Rows[1].Cells[2].FindControl("dateTextBox") as TextBox).ClientID + "');");
                (BSVARGridView.Rows[2].Cells[2].FindControl("dateTextBox") as TextBox).Attributes.Add("onkeydown", "AutoFormatDate('" + (BSVARGridView.Rows[2].Cells[2].FindControl("dateTextBox") as TextBox).ClientID + "');");
                (BSVARGridView.Rows[3].Cells[2].FindControl("dateTextBox") as TextBox).Attributes.Add("onkeydown", "AutoFormatDate('" + (BSVARGridView.Rows[3].Cells[2].FindControl("dateTextBox") as TextBox).ClientID + "');");

            }
        }
        private void LoadInt()
        {
            BankDropDownListItem();
            BranchDropDownListItem(0);
            List<Subject> subjectObjList = subjectManagerObj.SelectSubjectList();
            Subject subjectObj = new Subject();
            subjectObj.Id = 0;
            subjectObj.Name = "Select subject name";
            subjectObjList.Insert(0, subjectObj);
            subjectDropDownList.DataSource = subjectObjList;
            subjectDropDownList.DataValueField = "Id";
            subjectDropDownList.DataTextField = "Name";
            subjectDropDownList.DataBind();

            List<Approver> approverObjList = approverManagerObj.SelectApproverList();
            Session["approverS"] = approverObjList;
            Approver approverObj = new Approver();
            approverObj.Id = 0;
            approverObj.Name = "Select approver name";
            approverObjList.Insert(0, approverObj);
            approverDropDownList.DataSource = approverObjList;
            approverDropDownList.DataValueField = "Id";
            approverDropDownList.DataTextField = "Name";
            approverDropDownList.DataBind();
        
        }
        private void LoadBankNameList(Int64 LLId)
        {
            int ApplicationStatus = 0;
            string entryUserName = "";
            List<BankNameList> otherBankAccountObjList = new List<BankNameList>();
            otherBankAccountObj = bankStatementManagerObj.SelectLoanApplicationOthreBankInfo(LLId);
            if (otherBankAccountObj != null)
            {
                if (otherBankAccountObj.OtherBankId1 > 0)
                {
                    BankNameList bankNameList1Obj = new BankNameList();
                    bankNameList1Obj.OtherBankId = otherBankAccountObj.OtherBankId1;
                    bankNameList1Obj.OtherBank = otherBankAccountObj.OtherBank1.ToString();
                    bankNameList1Obj.OtherBranchId = otherBankAccountObj.OtherBranchId1;
                    bankNameList1Obj.OtherBranch = otherBankAccountObj.OtherBranch1;
                    bankNameList1Obj.OtherAccNo = otherBankAccountObj.OtherAccNo1.ToString();
                    bankNameList1Obj.OtherAccType = otherBankAccountObj.OtherAccType1.ToString();
                    otherBankAccountObjList.Add(bankNameList1Obj);
                }
                if (otherBankAccountObj.OtherBankId2 > 0)
                {
                    BankNameList bankNameList2Obj = new BankNameList();
                    bankNameList2Obj.OtherBankId = otherBankAccountObj.OtherBankId2;
                    bankNameList2Obj.OtherBank = otherBankAccountObj.OtherBank2.ToString();
                    bankNameList2Obj.OtherBranchId = otherBankAccountObj.OtherBranchId2;
                    bankNameList2Obj.OtherBranch = otherBankAccountObj.OtherBranch2;
                    bankNameList2Obj.OtherAccNo = otherBankAccountObj.OtherAccNo2.ToString();
                    bankNameList2Obj.OtherAccType = otherBankAccountObj.OtherAccType2.ToString();
                    otherBankAccountObjList.Add(bankNameList2Obj);
                }
                if (otherBankAccountObj.OtherBankId3 > 0)
                {
                    BankNameList bankNameList3Obj = new BankNameList();
                    bankNameList3Obj.OtherBankId = otherBankAccountObj.OtherBankId3;
                    bankNameList3Obj.OtherBank = otherBankAccountObj.OtherBank3.ToString();
                    bankNameList3Obj.OtherBranchId = otherBankAccountObj.OtherBranchId3;
                    bankNameList3Obj.OtherBranch = otherBankAccountObj.OtherBranch3;
                    bankNameList3Obj.OtherAccNo = otherBankAccountObj.OtherAccNo3.ToString();
                    bankNameList3Obj.OtherAccType = otherBankAccountObj.OtherAccType3.ToString();
                    otherBankAccountObjList.Add(bankNameList3Obj);
                }

                if (otherBankAccountObj.OtherBankId4 > 0)
                {
                    BankNameList bankNameList4Obj = new BankNameList();
                    bankNameList4Obj.OtherBankId = otherBankAccountObj.OtherBankId4;
                    bankNameList4Obj.OtherBank = otherBankAccountObj.OtherBank4.ToString();
                    bankNameList4Obj.OtherBranchId = otherBankAccountObj.OtherBranchId4;
                    bankNameList4Obj.OtherBranch = otherBankAccountObj.OtherBranch4;
                    bankNameList4Obj.OtherAccNo = otherBankAccountObj.OtherAccNo4.ToString();
                    bankNameList4Obj.OtherAccType = otherBankAccountObj.OtherAccType4.ToString();
                    otherBankAccountObjList.Add(bankNameList4Obj);
                }
                if (otherBankAccountObj.OtherBankId5 > 0)
                {
                    BankNameList bankNameList5Obj = new BankNameList();
                    bankNameList5Obj.OtherBankId = otherBankAccountObj.OtherBankId5;
                    bankNameList5Obj.OtherBank = otherBankAccountObj.OtherBank5.ToString();
                    bankNameList5Obj.OtherBranchId = otherBankAccountObj.OtherBranchId5;
                    bankNameList5Obj.OtherBranch = otherBankAccountObj.OtherBranch5;
                    bankNameList5Obj.OtherAccNo = otherBankAccountObj.OtherAccNo5.ToString();
                    bankNameList5Obj.OtherAccType = otherBankAccountObj.OtherAccType5.ToString();
                    otherBankAccountObjList.Add(bankNameList5Obj);
                }
                if (otherBankAccountObj.ApplicationStatus == 5 || otherBankAccountObj.ApplicationStatus == 15)
                {
                    saveButton.Enabled = false;
                }
                else
                {
                    saveButton.Enabled = true;
                }
            }

            
            int rowIndex = 1;
            var xml = "<xml id='bsver_data'><rows>";
            if (otherBankAccountObjList.Count > 0)
            {
                try
                {
                    foreach (BankNameList bankNameObj in otherBankAccountObjList)
                    {
                       xml+="<row id='" + rowIndex.ToString() + "'>" ;
                       xml+="<cell>";
                       xml+=rowIndex.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=bankNameObj.OtherBankId.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=bankNameObj.OtherBank.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=bankNameObj.OtherBranchId.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=bankNameObj.OtherBranch.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=bankNameObj.OtherAccNo.ToString();
                       xml+="</cell>";
                        int AccountTypeId = 0;
                        switch (bankNameObj.OtherAccType.Trim().ToUpper().Replace(" ", ""))
                        {
                            case "CURRENT":
                                AccountTypeId = 1;
                                break;
                            case "SAVING":
                                AccountTypeId = 2;
                                break;
                            case "CALL":
                                AccountTypeId = 3;
                                break;
                            case "OD":
                                AccountTypeId = 4;
                                break;
                            case "SOD":
                                AccountTypeId = 5;
                                break;
                            case "SALARY":
                                AccountTypeId = 7;
                                break;
                            case "Others":
                                AccountTypeId = 6;
                                break;
                            default:
                                AccountTypeId = 6;
                                break;
                        }
                        BASVERReportInfo bASVERReportInfoObj = new BASVERReportInfo();
                        string AcName = "";
                        string statementTypeId = "";
                        string statementType = "";
                        string subjectId = "";
                        string subjectName = "";

                        bASVERReportInfoObj = bSVERManagerObj.SelectBASVERReportInfo(LLId, (Int16)bankNameObj.OtherBankId, (Int16)bankNameObj.OtherBranchId, bankNameObj.OtherAccNo);
                        if (bASVERReportInfoObj != null)
                        {
                            statementTypeId = bASVERReportInfoObj.StatementType_Id.ToString();
                            switch (statementTypeId)
                            {
                                case "1":
                                    statementType = "Document";
                                    break;
                                case "2":
                                    statementType = "Email";
                                    break;
                            }
                            subjectId = bASVERReportInfoObj.Subject_Id.ToString();
                            subjectName = bASVERReportInfoObj.SubjectName.ToString();
                        }
                       xml+="<cell>";
                       xml+=statementTypeId.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=subjectId.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=subjectName.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=AccountTypeId.ToString();
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=AcName;
                       xml+="</cell>";
                       xml+="<cell>";
                       xml+=LLId.ToString();
                       xml+="</cell>";
                       xml+="</row>";
                        rowIndex = rowIndex + 1;
                    }
                    xml += "</rows></xml>";
                    bsverDiv.InnerHtml = xml;
                }
                finally
                {
                }
            }
            else
            {
                xml += "</rows></xml>";
            }
        }
        #region Bank DropDown Items
        /// <summary>
        /// This method gives bank dropdown items.
        /// </summary>
        private void BankDropDownListItem()
        {

            List<BankInformation> bankInformationListObj = loanApplicationManagerObj.GetAllBanks();
            BankInformation bankInformationObj = new BankInformation();
            bankInformationObj.Id = 0;
            bankInformationObj.Name = "";
            bankInformationListObj.Insert(0, bankInformationObj);
            bankNameDropDownList.DataSource = bankInformationListObj;
            bankNameDropDownList.DataTextField = "Name";
            bankNameDropDownList.DataValueField = "Id";
            bankNameDropDownList.DataBind();
        }
        #endregion

        #region Branch DropDown Items
        /// <summary>
        /// This method gives branch dropdown items.
        /// </summary>
        private void BranchDropDownListItem(Int32 bankId)
        {
            List<BranchInformation> branchInformationListObj = loanApplicationManagerObj.GetAllBranchs();
            BranchInformation branchInformationObj = new BranchInformation();
            branchInformationObj.Id = 0;
            branchInformationObj.Name = "";
            branchInformationListObj.Insert(0, branchInformationObj);
            branchNameDropDownList.DataSource = branchInformationListObj;
            branchNameDropDownList.DataTextField = "Name";
            branchNameDropDownList.DataValueField = "Id";
            branchNameDropDownList.DataBind();
        }
        #endregion

        #region Account Type DropDown Items
        /// <summary>
        /// This method gives account type dropdown items.
        /// </summary>
        private void AccountTypeDropDownListItem()
        {
            List<AccountType> accountTypeListObj = loanApplicationManagerObj.GetAllAccountType();
            AccountType acTypeIdObj = new AccountType();
            acTypeIdObj.Id = 0;
            acTypeIdObj.Name = "Null";
            accountTypeListObj.Insert(0, acTypeIdObj);
            acTypeDropDownList.DataSource = accountTypeListObj;
            acTypeDropDownList.DataTextField = "Name";
            acTypeDropDownList.DataValueField = "Id";
            acTypeDropDownList.DataBind();
        }
        #endregion
        protected void searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                string customerNameAddress = null;
                if (String.IsNullOrEmpty(lLIdTextBox.Text))
                {
                    errorMsgLabel.Text = "Please enter LLID no.";
                }
                else
                {
                    errorMsgLabel.Text = "";
                    otherBankAccountObj = bankStatementManagerObj.SelectLoanApplicationOthreBankInfo(Convert.ToInt64(lLIdTextBox.Text));
                    if (String.IsNullOrEmpty(otherBankAccountObj.OtherBank1))
                    {
                
                        errorMsgLabel.Text = "No data found.";
                    }
                    else
                    {
                        Session["BankInfo"] = otherBankAccountObj;
                        List<BankInformation> bankListId = new List<BankInformation>();
                        BankInformation bankinfo0Obj = new BankInformation();
                        BankInformation bankinfo1Obj = new BankInformation();
                        BankInformation bankinfo2Obj = new BankInformation();
                        BankInformation bankinfo3Obj = new BankInformation();
                        BankInformation bankinfo4Obj = new BankInformation();
                        BankInformation bankinfo5Obj = new BankInformation();
                        bankinfo1Obj.Id = Convert.ToInt32(otherBankAccountObj.OtherBankId1.ToString());
                        bankinfo1Obj.Name = otherBankAccountObj.OtherBank1.ToString();
                        bankListId.Add(bankinfo1Obj);
                        bankinfo2Obj.Id = Convert.ToInt32(otherBankAccountObj.OtherBankId2.ToString());
                        bankinfo2Obj.Name = otherBankAccountObj.OtherBank2.ToString();
                        bankListId.Add(bankinfo2Obj);
                        bankinfo3Obj.Id = Convert.ToInt32(otherBankAccountObj.OtherBankId3.ToString());
                        bankinfo3Obj.Name = otherBankAccountObj.OtherBank3.ToString();
                        bankListId.Add(bankinfo3Obj);
                        bankinfo4Obj.Id = Convert.ToInt32(otherBankAccountObj.OtherBankId4.ToString());
                        bankinfo4Obj.Name = otherBankAccountObj.OtherBank4.ToString();
                        bankListId.Add(bankinfo4Obj);
                        bankinfo5Obj.Id = Convert.ToInt32(otherBankAccountObj.OtherBankId5.ToString());
                        bankinfo5Obj.Name = otherBankAccountObj.OtherBank5.ToString();
                        bankListId.Add(bankinfo5Obj);
                        bankNameDropDownList.Items.Clear();
                        bankinfo0Obj.Id = 0;
                        bankinfo0Obj.Name = "Select Bank Name";
                        bankListId.Insert(0, bankinfo0Obj);
                        bankNameDropDownList.DataSource = bankListId;
                        bankNameDropDownList.DataTextField = "Name";
                        bankNameDropDownList.DataValueField = "Id";
                        bankNameDropDownList.DataBind();

                        customerNameAddress = bSVERManagerObj.SelectCustomerNameAddress(Convert.ToInt64(lLIdTextBox.Text));
                        if (customerNameAddress.Length > 0)
                        {
                            string[] splitData = customerNameAddress.Split('#');
                            customerNameTextBox.Text = splitData[0].ToString();
                            customerAddressTextBox.Text =splitData[1].ToString();
                        }
                    }
            
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Search BSVER");
            }
        }

        private bool IsBlank()
        {
            if (lLIdTextBox.Text == "")
            {
                Alert.Show("Please Inseart LLID");
                lLIdTextBox.Focus();
                return false;
            }
            else if (statementTypeDropDownList.SelectedIndex == 0)
            {
                Alert.Show("Please select statement Type ");
                statementTypeDropDownList.Focus();
                return false;
            }
            else if (bankIdHiddenField.Value == "" || bankIdHiddenField.Value == null)
            {
                Alert.Show("Please select bank name");
                bankNameDropDownList.Focus();
                return false;
            }
            else if (subjectDropDownList.SelectedIndex == 0)
            {
                Alert.Show("Please select subject name");
                subjectDropDownList.Focus();
                return false;
            }
            else if (statementTypeDropDownList.SelectedValue.ToString() == "Email" && emailAddressTextBox.Text == "")
            {
                Alert.Show("Please Input Email Address");
                emailAddressTextBox.Focus();
                return false;
            }
            else if (statementTypeDropDownList.SelectedValue.ToString() == "Email" && scbAddressTextBox.Text == "")
            {
                Alert.Show("Please Input SCB Email Address");
                scbAddressTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                int bsVerMaster = 0;
                int bsVarDetails = 0;
                int slNo = 1;
                Int64 bsVarMasterId = 0;
                BSVERMaster bSVERMasterObj = new BSVERMaster();
                BSVERDetails bSVERDetailsObj = null;
                List<BSVERDetails> bSVERDetailsObjList = new List<BSVERDetails>();
                if (!IsBlank())
                {
                }
                else
                {
                    bSVERMasterObj.Id = 0;
                    bSVERMasterObj.LlId = Convert.ToInt64(lLIdTextBox.Text);
                    bSVERMasterObj.StatementType_Id = Convert.ToInt16(statementTypeDropDownList.SelectedValue.ToString());
                    bSVERMasterObj.Bank_Id = Convert.ToInt16(bankIdHiddenField.Value);
                    bSVERMasterObj.Branch_Id = Convert.ToInt16(branchIdHiddenField.Value);
                    bSVERMasterObj.Subject_Id = Convert.ToInt16(subjectDropDownList.SelectedValue.ToString());
                    bSVERMasterObj.Dear = Convert.ToString(dearTextBox.Text);
                    bSVERMasterObj.EmailAddress = Convert.ToString(emailAddressTextBox.Text);
                    bSVERMasterObj.SCBEmailAddress = Convert.ToString(scbAddressTextBox.Text);
                    bSVERMasterObj.EmailText = Convert.ToString(emailTextLabel.Text);
                    bSVERMasterObj.CustomerName = Convert.ToString(customerNameTextBox.Text);
                    bSVERMasterObj.CustomerAddress = Convert.ToString(customerAddressTextBox.Text);
                    bSVERMasterObj.AcNo = Convert.ToString(acNoTextBox.Text);
                    bSVERMasterObj.AcType_Id = Convert.ToInt16(acTypeIdHiddenField.Value);
                    bSVERMasterObj.AcName = Convert.ToString(acNameTextBox.Text);
                    bSVERMasterObj.Approved_Id = Convert.ToInt16(approverDropDownList.SelectedValue.ToString());
                    bSVERMasterObj.User_Id = Convert.ToInt32(Session["Id"].ToString());
                    bSVERMasterObj.EntryDate = DateTime.Now;
                    bsVerMaster = bSVERManagerObj.SendBSVERMasterInfoInToDB(bSVERMasterObj);
                    if (bsVerMaster == INSERTE || bsVerMaster == UPDATE)
                    {
                        bsVarMasterId = bSVERManagerObj.SelectBSVerMasterId(bSVERMasterObj.LlId, bSVERMasterObj.StatementType_Id, bSVERMasterObj.Bank_Id, bSVERMasterObj.Branch_Id, bSVERMasterObj.Subject_Id, bSVERMasterObj.AcNo);
                        Session["bsverMasterId"] = bsVarMasterId;
                        for (int i = 0; i <= 3; i++)
                        {
                            bSVERDetailsObj = new BSVERDetails();
                            bSVERDetailsObj.BasVerMaster_Id = bsVarMasterId;
                            bSVERDetailsObj.SlNo = (Int16)slNo;
                            bSVERDetailsObj.Particular = Convert.ToString((BSVARGridView.Rows[i].Cells[1].FindControl("particularLabel") as Label).Text);
                            if ((BSVARGridView.Rows[i].Cells[2].FindControl("dateTextBox") as TextBox).Text != "")
                            {
                                bSVERDetailsObj.Date = Convert.ToDateTime((BSVARGridView.Rows[i].Cells[2].FindControl("dateTextBox") as TextBox).Text.Trim(), new CultureInfo("ru-RU"));
                            }
                            else
                            {
                                bSVERDetailsObj.Date = DateTime.Now;
                            }
                            bSVERDetailsObj.Description = Convert.ToString((BSVARGridView.Rows[i].Cells[3].FindControl("descriptionTextBox") as TextBox).Text);
                            bSVERDetailsObj.Withdrawl = Convert.ToDouble("0" + (BSVARGridView.Rows[i].Cells[4].FindControl("withdrawlTextBox") as TextBox).Text);
                            bSVERDetailsObj.Diposit = Convert.ToDouble("0" + (BSVARGridView.Rows[i].Cells[5].FindControl("dipositTextBox") as TextBox).Text);
                            bSVERDetailsObj.Balance = Convert.ToDouble("0" + (BSVARGridView.Rows[i].Cells[6].FindControl("balanceTextBox") as TextBox).Text);
                            bSVERDetailsObj.OldId = Convert.ToInt64("0" + (BSVARGridView.Rows[i].Cells[7].FindControl("oldIdTextBox")as HiddenField ).Value);
                            bSVERDetailsObjList.Add(bSVERDetailsObj);
                            slNo = slNo + 1;
                        }
                        bsVarDetails = bSVERManagerObj.SendBSVERDetailsInfoInToDB(bSVERDetailsObjList);


                    }
                    else
                    {
                    }
                    if (Convert.ToInt16(statementTypeDropDownList.SelectedValue.ToString()) == 2)
                    {
                        SendMail();
                    }
                    var msg = "";
                    if (bsVerMaster == INSERTE)
                    {
                        if (Convert.ToInt16(statementTypeDropDownList.SelectedValue.ToString()) == 2)
                        {
                            msg = "Send Successfuly";
                            LoadBankNameList(bSVERMasterObj.LlId);
                        }
                        else
                        {
                            msg = "Save Successfuly";
                            LoadBankNameList(bSVERMasterObj.LlId);
                        }
                    }
                    else if (bsVerMaster == UPDATE)
                    {
                        if (Convert.ToInt16(statementTypeDropDownList.SelectedValue.ToString()) == 2)
                        {
                            msg = "Send Successfuly";
                            LoadBankNameList(bSVERMasterObj.LlId);
                        }
                        else
                        {
                            msg = "Update Successfuly";
                            LoadBankNameList(bSVERMasterObj.LlId);
                        }                
                    }
                    else if (bsVerMaster == ERROR)
                    {
                        msg = "Error in Data !";
                    }
                    new AT(this).AuditAndTraial("Bank Statement Verification", msg);
                    Alert.Show(msg);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save BSVER");
            }
        }
        private void SendMail()
        {

            if (Convert.ToInt16(statementTypeDropDownList.SelectedValue.ToString()) == 2)
            {
                try
                {
                    string from = (string)ConfigurationManager.AppSettings.Get("fromEmailAddress");
                    MailMessage message = new MailMessage();
                    MailAddress mailAddressObj = new MailAddress(from);
                    message.From = mailAddressObj;
                    message.To.Add(scbAddressTextBox.Text.Trim());
                    message.Subject = subjectDropDownList.SelectedItem.Text.Trim();
                    string body = EMailMessage();
                    message.Body = body;
                    message.Priority = MailPriority.High;
                    message.IsBodyHtml = true;
                    SmtpClient smtpClientObj = new SmtpClient();
                    smtpClientObj.EnableSsl = false;
                    smtpClientObj.Send(message);

                }
                catch (Exception ex)
                {
                    Alert.Show("Check your email address " + ex.Message.ToString());
                }
            }

        }
        private string EMailMessage()
        {
            string message = null;
            string Value1 = dearTextBox.Text;
            string Value2 = emailAddressTextBox.Text;
            string Value3 = customerNameTextBox.Text;
            string Value4 = bankNameDropDownList.SelectedItem.Text;
            string Value5 = branchNameDropDownList.SelectedItem.Text;
            string Value6 = acNoTextBox.Text + "," + acTypeDropDownList.SelectedItem.Text;
            string Value7 = acNameTextBox.Text;
            string ValueAddress = customerAddressTextBox.Text;
            string[] date = new string[4];
            string[] description = new string[4];
            string[] withdrawal = new string[4];
            string[] deposit = new string[4];
            string[] balance = new string[4];
            int i = 0;
            foreach (GridViewRow gvr in BSVARGridView.Rows)
            {
                date[i]=(gvr.FindControl("dateTextBox") as TextBox).Text;
                description[i]=(gvr.FindControl("descriptionTextBox") as TextBox).Text;
                withdrawal[i]=(gvr.FindControl("withdrawlTextBox") as TextBox).Text;
                deposit[i]=(gvr.FindControl("dipositTextBox") as TextBox).Text;
                balance[i]=(gvr.FindControl("balanceTextBox") as TextBox).Text;
                i = i + 1;
            }
            message ="<table border=0 cellspacing=0 cellpadding=0 width=1009" +
                     "style='width:757.0pt;border-collapse:collapse'>"+
                     "<col width=67 style='mso-width-source:userset;mso-width-alt:2450;width:50pt'><col width=278 style='mso-width-source:userset;mso-width-alt:10166;width:209pt'><col width=128 style='mso-width-source:userset;mso-width-alt:4681;width:96pt'><col width=170 style='mso-width-source:userset;mso-width-alt:6217;width:128pt'><col width=73 style='mso-width-source:userset;mso-width-alt:2669;width:55pt'><col width=76 style='mso-width-source:userset;mso-width-alt:2779;width:57pt'><col width=77 style='mso-width-source:userset;mso-width-alt:2816;width:58pt'><col width=64 style='width:48pt'><col width=75 style='mso-width-source:userset;mso-width-alt:2742;width:56pt'>"+
                     "<tr style='height:12.75pt'>"+
                     "<td width=67 nowrap valign=bottom style='width:50.0pt;background:white;"+
                     "padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=right style='text-align:right'><b><span"+
                     "style='font-size:10.0pt;font-family:Arial'>Dear</span></b></p>  </td>"+
                     "<td width=279 nowrap valign=bottom style='width:209.0pt;background:white;"+
                     "padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><strong><span>"+ Value1 +"</span></strong></p>  </td>"+
                     "<td width=128 nowrap valign=bottom style='width:96.0pt;background:white;"+
                     "padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal><b><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></b></p>  </td>"+
                     "<td width=320 nowrap valign=bottom style='width:128.0pt;background:white;"+
                     "padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal><i><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></i></p>  </td>"+
                     "<td width=320 nowrap valign=bottom style='width:55.0pt;background:white;"+
                     "padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td width=320 nowrap valign=bottom style='width:57.0pt;background:white;"+
                     "padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td width=77 nowrap valign=bottom style='width:58.0pt;background:white;"+
                     "padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><u style='visibility:hidden'><span style='font-size:10.0pt;"+
                     "font-family:Arial;color:blue'>&nbsp;</span></u></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap colspan=3 valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><i><span style='font-size:10.0pt;font-family:Arial'>"+ Value2 +"</span></i></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap colspan=3 valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>Will "+
                     "Appreciate if you kindly confirm the transactions mentioned below.</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><b><span style='font-size:9.0pt;font-family:Arial'>1."+
                     "Customer Name: </span></b>"+ Value3 +"</p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><b><span style='font-size:9.0pt;font-family:Arial'>2. Bank"+
                     "name: "+ Value4 +" </span></b></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal align=right style='text-align:right'><span"+
                     "style='font-size:10.0pt;font-family:Arial'>Customer's Address:</span></p>  </td>"+
                     "<td width=320 colspan=3 rowspan=4 valign=top style='width:240.0pt;border-top:"+
                     "windowtext;border-left:windowtext;border-bottom:black;border-right:black;"+
                     "border-style:solid;border-width:1.0pt;background:silver;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span>"+ ValueAddress +"</p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><b><span style='font-size:9.0pt;font-family:Arial'>3."+
                     "Branch: </span></b>"+ Value5 +"</p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><b><span style='font-size:9.0pt;font-family:Arial'>4. A/c"+
                     "No &amp; Type: "+ Value6 +"</span></b></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><b><span style='font-size:9.0pt;font-family:Arial'>5. A/C"+
                     "Name: </span></b>"+ Value7 +"</p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:12.75pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:13.5pt'>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:13.5pt'>"+
                     "<p class=MsoNormal><b><span style='font-size:9.0pt;font-family:Arial'>&nbsp;</span></b></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:9.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='background:white;padding:.75pt .75pt 0in .75pt;"+
                     "height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td width=67 valign=bottom style='width:50.0pt;border:solid windowtext 1.0pt;"+
                     "background:silver;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><b><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Sl.</span></b></p>  </td>"+
                     "<td width=279 valign=bottom style='width:209.0pt;border:solid windowtext 1.0pt;"+
                     "border-left:none;background:silver;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><b><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Particular</span></b></p>  </td>"+
                     "<td width=128 valign=bottom style='width:96.0pt;border:solid windowtext 1.0pt;"+
                     "border-left:none;background:silver;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><b><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Date</span></b></p>  </td>"+
                     "<td width=320 valign=bottom style='width:128.0pt;border:solid windowtext 1.0pt;"+
                     "border-left:none;background:silver;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><b><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Description</span></b></p>  </td>"+
                     "<td width=320 valign=bottom style='width:55.0pt;border:solid windowtext 1.0pt;"+
                     "border-left:none;background:silver;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><b><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Withdrawal</span></b></p>  </td>"+
                     "<td width=320 valign=bottom style='width:57.0pt;border:solid windowtext 1.0pt;"+
                     "border-left:none;background:silver;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><b><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Deposit</span></b></p>  </td>"+
                     "<td width=77 valign=bottom style='width:58.0pt;border:solid windowtext 1.0pt;"+
                     "border-left:none;background:silver;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><b><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Balance</span></b></p>  </td>"+
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td width=67 style='width:50.0pt;border:solid windowtext 1.0pt;border-top:"+
                     "none;background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>1</span></p>  </td>"+
                     "<td width=279 style='width:209.0pt;border-top:none;border-left:none;"+
                     "border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Opening balance</span></p>  </td>"+
                     "<td width=128 valign=bottom style='width:96.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>" + date[0] + "&nbsp;</span></p>  </td>" +
                     "<td width=320 valign=bottom style='width:128.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>" + description[0] + "&nbsp;</span></p>  </td>" +
                     "<td width=320 valign=bottom style='width:55.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + withdrawal[0] + "</p>  </td>" +
                     "<td width=320 valign=bottom style='width:57.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + deposit[0] + "</p>  </td>" +
                     "<td width=77 valign=bottom style='width:58.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>" + balance[0] + "&nbsp;</span></p>  </td>" +
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td width=67 style='width:50.0pt;border:solid windowtext 1.0pt;"+
                     "border-top:none;background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>2</span></p>  </td>"+
                     "<td width=279 rowspan=2 style='width:209.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid black 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Sample txns randomly selected</span></p>  </td>"+
                     "<td width=128 valign=bottom style='width:96.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + date[1] + "</p>  </td>" +
                     "<td width=320 valign=bottom style='width:128.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>" + description[1] + "&nbsp;</span></p>  </td>" +
                     "<td width=320 valign=bottom style='width:55.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>" + withdrawal[1] + "&nbsp;</span></p>  </td>" +
                     "<td width=320 valign=bottom style='width:57.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + deposit[1] + "</p>  </td>" +
                     "<td width=77 valign=bottom style='width:58.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + balance[1] + "</p>  </td>" +
                     "</tr>"+
                     "<tr style='height:12.75pt'>"+
                     "<td width=67 style='width:50.0pt;border:solid windowtext 1.0pt;"+
                     "border-top:none;background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>3</span></p>  </td>"+
                     "<td width=128 valign=bottom style='width:96.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + date[2] + "</p>  </td>" +
                     "<td width=320 valign=bottom style='width:128.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>" + description[2] + "&nbsp;</span></p>  </td>" +
                     "<td width=320 valign=bottom style='width:55.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>" + withdrawal[2] + "&nbsp;</span></p>  </td>" +
                     "<td width=320 valign=bottom style='width:57.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + deposit[2] + "</p>  </td>" +
                     "<td width=77 valign=bottom style='width:58.0pt;border-top:none;"+
                     "border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:12.75pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + balance[2] + "</p>  </td>" +
                     "</tr>"+         
                     "<tr style='height:13.5pt'>"+
                     "<td width=67 style='width:50.0pt;border:solid windowtext 1.0pt;border-top:"+
                     "none;background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>4</span></p>  </td>"+
                     "<td width=279 style='width:209.0pt;border-top:none;border-left:none;"+
                     "border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>Closing balance</span></p>  </td>"+
                     "<td width=128 valign=bottom style='width:96.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + date[3] + "</p>  </td>" +
                     "<td width=320 valign=bottom style='width:128.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + description[3] + "</p>  </td>" +
                     "<td width=320 valign=bottom style='width:55.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + withdrawal[3] + "</p>  </td>" +
                     "<td width=320 valign=bottom style='width:57.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + deposit[3] + "</p>  </td>" +
                     "<td width=77 valign=bottom style='width:58.0pt;border-top:none;border-left:"+
                     "none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal align=center style='text-align:center'><span"+
                     "style='font-size:9.0pt;font-family:Arial'>&nbsp;</span>" + balance[3] + "</p>  </td>" +
                     "</tr>"+
                     "<tr style='height:13.5pt'>"+
                     "<td nowrap valign=bottom style='border:none;border-bottom:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='border:none;border-bottom:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='border:none;border-bottom:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='border:none;border-bottom:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='border:none;border-bottom:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='border:none;border-bottom:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>"+
                     "<td nowrap valign=bottom style='border:none;border-bottom:solid windowtext 1.0pt;"+
                     "background:white;padding:.75pt .75pt 0in .75pt;height:13.5pt'>"+
                     "<p class=MsoNormal><span style='font-size:10.0pt;font-family:Arial'>&nbsp;</span></p>  </td>" +
                     "</tr>"+
                     "</table>";
            return message;
        }
        protected void printButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int64 bsverMasterId = (Int64)Session["bsverMasterId"];
                new AT(this).AuditAndTraial("Bank Statement Verification", "Print");
                ScriptManager.RegisterClientScriptBlock(printButton, this.GetType(), "printButton", "<script type='text/javascript'>window.open('BSVERPrintUI.aspx?bsverMasterId=" + bsverMasterId + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=1024,height=768')</script>", false);
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Print BSVER");
            }
        }


    }
}
