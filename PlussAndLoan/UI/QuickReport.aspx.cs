﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.LoanLocatorReports;

namespace PlussAndLoan.UI
{
    public partial class QuickReport : System.Web.UI.Page
    {
        private readonly LocAdminTatReportManager _locAdminManager = new LocAdminTatReportManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            string productName = Session["ProName"].ToString();
            string month = Session["monthName"].ToString();
            string year = Session["year"].ToString();
            int button = (int)Session["button"];
            proDiv.InnerText = productName;

            int productId = (int)Session["proId"];
            string monthNo = Session["monthNo"].ToString();
            string date = "%" + year + "-" + monthNo + "%";
            if (button == 1)
            {
                string title = "[Application only received in: " + month + "-" + year + " ]";
                monthDiv.InnerText = title;
            }
            else if (button == 2)
            {
                string title = "[Application only received in: January-" + year + " ]";
                monthDiv.InnerText = title;
            }

            DataTable dt = (DataTable)Session["res"];
            int tot = 0;
            int re_receieved = 0;
            int subrate = 0;
            int verifi = 0;
            int on_hold = 0;
            int cond_appr = 0;
            int appr = 0;
            int aprate = 0;
            int defer = 0;
            int decl = 0;
            int disb = 0;
            int disbrate = 0;

            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var c = dt.Rows[i]["tot"] == null || dt.Rows[i]["tot"].ToString() == "" ? 0 : dt.Rows[i]["tot"];
                    tot = Convert.ToInt32(c);
                    subrate = tot;
                    re_receieved = Convert.ToInt32(dt.Rows[i]["re_receieved"]);
                    verifi = Convert.ToInt32(dt.Rows[i]["verifi"]);
                    verifi = Convert.ToInt32(dt.Rows[i]["on_hold"]);
                    cond_appr = Convert.ToInt32(dt.Rows[i]["cond_appr"]);
                    appr = Convert.ToInt32(dt.Rows[i]["appr"]);
                    aprate = appr;
                    defer = Convert.ToInt32(dt.Rows[i]["defer"]);
                    decl = Convert.ToInt32(dt.Rows[i]["decl"]);
                    disb = Convert.ToInt32(dt.Rows[i]["disb"]);
                    disbrate = disb;


                }
            }

            TableRow row = new TableRow();
            row.Cells.Add(new TableCell { Text = "NUMBER" });
            row.Cells.Add(new TableCell { Text = tot.ToString() });
            row.Cells.Add(new TableCell { Text = re_receieved.ToString() });
            row.Cells.Add(new TableCell { Text = verifi.ToString() });
            row.Cells.Add(new TableCell { Text = on_hold.ToString() });
            row.Cells.Add(new TableCell { Text = cond_appr.ToString() });
            row.Cells.Add(new TableCell { Text = appr.ToString() });
            row.Cells.Add(new TableCell { Text = defer.ToString() });
            row.Cells.Add(new TableCell { Text = decl.ToString() });
            row.Cells.Add(new TableCell { Text = disb.ToString() });
            qTable.Rows.Add(row);

            DataTable resDt = (DataTable)Session["resData"];

            Decimal total = 0;
            Decimal verify = 0;
            Decimal cond_approv = 0;
            Decimal approv = 0;
            Decimal decline = 0;
            Decimal disbur = 0;


            if (resDt != null)
            {
                for (int i = 0; i < resDt.Rows.Count; i++)
                {
                    var c = resDt.Rows[i]["tot"] == null || resDt.Rows[i]["tot"].ToString() == "" ? 0 : resDt.Rows[i]["tot"];
                    Decimal tota = Convert.ToDecimal(c);
                    total = tota / 1000000;
                    total = Math.Round(total, 3);

                    Decimal veri = Convert.ToDecimal(resDt.Rows[i]["verifi"]);
                    verify = veri / 1000000;
                    verify = Math.Round(verify, 3);

                    Decimal cond = Convert.ToDecimal(resDt.Rows[i]["cond_appr"]);
                    cond_approv = cond / 1000000;
                    cond_approv = Math.Round(cond_approv, 3);

                    Decimal apr = Convert.ToDecimal(resDt.Rows[i]["appr"]);
                    approv = apr / 1000000;
                    approv = Math.Round(approv, 3);
                    Decimal decln = Convert.ToDecimal(resDt.Rows[i]["decl"]);
                    decline = decln / 1000000;
                    decline = Math.Round(decline, 3);

                    Decimal dis = Convert.ToDecimal(resDt.Rows[i]["disb"]);
                    disbur = dis / 1000000;
                    disbur = Math.Round(disbur, 3);

                }
            }


            TableRow row1 = new TableRow();
            row1.Cells.Add(new TableCell { Text = "Vol(BDT Mill.)" });
            row1.Cells.Add(new TableCell { Text = total.ToString() });
            row1.Cells.Add(new TableCell { Text = "NA" });
            row1.Cells.Add(new TableCell { Text = verify.ToString() });
            row1.Cells.Add(new TableCell { Text = "NA" });
            row1.Cells.Add(new TableCell { Text = cond_approv.ToString() });
            row1.Cells.Add(new TableCell { Text = approv.ToString() });
            row1.Cells.Add(new TableCell { Text = "NA" });
            row1.Cells.Add(new TableCell { Text = decline.ToString() });
            row1.Cells.Add(new TableCell { Text = disbur.ToString() });
            qTable.Rows.Add(row1);

            Decimal p = 0;
            if (subrate == 0)
            {
                p = 0;
            }
            else
            {
                p = ((aprate / subrate) * 100);
            }
            p = Math.Round(p, 3);
            string approvalRate = "  " + p + " %";
            appDiv.InnerText = approvalRate;
            Decimal d = 0;
            if (aprate == 0)
            {
                d = 0;
            }
            else
            {
                d = ((disbrate / aprate) * 100);
            }
            d = Math.Round(d, 3);
            string disbursementRate = "  " + d + " %";
            disbDiv.InnerText = disbursementRate;

            // Next Page Start ############################################################################################3
            decimal[] array1 = new decimal[9];
            decimal[] array2 = TatVar(year, monthNo, productId, 1);//Standard 
            decimal[] array3 = TatVar(year, monthNo, productId, 2); //Deff
            decimal x;
            decimal y;
            if (array2[0] > 0)
            {
                x = array2[0];
            }
            else
            {
                x = 1;
            }
            if (array3[0] > 0)
            {
                y = array3[0];
            }
            else
            {
                y = 1;
            }
            for (int m = 0; m < 9; m++)
            {
                array1[m] = ((array2[m] / x) + (array3[m] / y));

            }
            if (button == 1)
            {
                string title2 = "[Application only disbursed in: " + month + "-" + year + " ]";
                tittle2Div.InnerText = title2;
            }
            else if (button == 2)
            {
                string title2 = "[Application only disbursed in: January-" + year + " ]";
                tittle2Div.InnerText = title2;
            }
            app1.InnerText = array2[0].ToString();
            app2.InnerText = array3[0].ToString();
            Decimal total_no_of_appl = (array2[0] + array3[0]);
            if (total_no_of_appl == 0)
            {
                total_no_of_appl = 1;
            }
            totApp.InnerText = total_no_of_appl.ToString();

            Decimal standard_app = ((array2[0] / total_no_of_appl) * 100);
            standard_app = Math.Round(standard_app, 2);
            string stanApp = standard_app + " %";
            standardApp.InnerText = stanApp;

            Decimal deffered_app = ((array3[0] / total_no_of_appl) * 100);
            deffered_app = Math.Round(deffered_app, 2);
            string deffApp = deffered_app + " %";
            deferApp.InnerText = deffApp;

            Decimal sales = (array2[1] / x);
            sales = Math.Round(sales, 2);
            StanTat.InnerText = sales.ToString();

            Decimal differTat = (array3[1] / y);
            differTat = Math.Round(differTat, 2);
            DeffTat.InnerText = differTat.ToString();

            Decimal stanCredit = (array2[2] / x);
            stanCredit = Math.Round(stanCredit, 2);
            CreditDiv1.InnerText = stanCredit.ToString();

            Decimal defferCredit = (array3[2] / y);
            defferCredit = Math.Round(defferCredit, 2);
            CreditDiv2.InnerText = defferCredit.ToString();

            Decimal operation1 = (array2[3] / x);
            operation1 = Math.Round(operation1, 2);
            Operation11.InnerText = operation1.ToString();

            Decimal operation2 = (array3[3] / y);
            operation2 = Math.Round(operation2, 2);
            Operation22.InnerText = operation2.ToString();

            Decimal dtCredit = GetCredit(date, productId);
            decimal credit_tat = 0;
            string show_credit_tat;

            if (dtCredit > 0)
            {
                decimal optat = dtCredit / 100;
                optat = Math.Round(optat, 2);
                credit_tat = optat;
                show_credit_tat = optat.ToString();
            }
            else
            {
                show_credit_tat = "--";

            }

            divGetCredit.InnerText = show_credit_tat;

            decimal verification1 = (array2[4] / x);
            verification1 = Math.Round(verification1, 2);
            divVerification1.InnerText = verification1.ToString();

            decimal verification2 = (array3[4] / y);
            verification2 = Math.Round(verification2, 2);
            divVerification2.InnerText = verification2.ToString();

            decimal asset1 = (array2[5] / x);
            asset1 = Math.Round(asset1, 2);
            divAsset1.InnerHtml = asset1.ToString();

            decimal asset2 = (array3[5] / y);
            asset2 = Math.Round(asset2, 2);
            divAsset2.InnerHtml = asset2.ToString();

            decimal assetOp1 = (array2[6] / x);
            assetOp1 = Math.Round(assetOp1, 2);
            divAssetOp1.InnerText = assetOp1.ToString();

            decimal assetOp2 = (array3[6] / y);
            assetOp2 = Math.Round(assetOp2, 2);
            divAssetOp2.InnerText = assetOp2.ToString();

            decimal dtAsset = GetAsset(date, productId);
            decimal asset_tat = 0;
            string show_asset_tat;
            if (dtAsset > 0)
            {
                decimal optat = dtAsset / 100;
                optat = Math.Round(optat, 2);
                asset_tat = optat;
                show_asset_tat = optat.ToString();
            }
            else
            {
                show_asset_tat = "--";
            }

            divGetAsset.InnerText = show_asset_tat;

            decimal total1 = (array2[8] / x);
            total1 = Math.Round(total1, 2);
            string show_total1 = total1 + " days";
            divTotal1.InnerText = show_total1;

            decimal total2 = (array3[8] / y);
            total2 = Math.Round(total2, 2);
            string show_total2 = total2 + " days";
            divTotal2.InnerText = show_total2;

            string grandTotal;
            Decimal finalTot;
            if (credit_tat > 0 || asset_tat > 0)
            {
                finalTot = credit_tat + asset_tat;
                grandTotal = finalTot + " days";
            }
            else
            {
                grandTotal = "--";
            }
            divTotal.InnerText = grandTotal;


        }




        // flag=1 for Standard and flag=2 for DEFFERED
        public int GetApplications(string year, string monthNo, int productId, int flag)
        {
            var noApp = _locAdminManager.GetApplications(year, monthNo, productId, flag);
            return noApp;
        }

        public DataTable GetTatVar(string year, string monthNo, int productId, int flag)
        {
            var dt = _locAdminManager.GetTatVar(year, monthNo, productId, flag);
            return dt;
        }
        public decimal[] TatVar(string year, string monthNo, int productId, int flag)
        {
            decimal[] tatret = new decimal[9];
            int noApp = GetApplications(year, monthNo, productId, flag);
            tatret[0] = noApp;
            tatret[8] = 0;
            if (noApp > 0)
            {

                DataTable dt = GetTatVar(year, monthNo, productId, flag);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string val = dt.Rows[i]["TrType"].ToString();
                        int j = Convert.ToInt32(val.Substring(0, 1));
                        tatret[j] = Convert.ToDecimal(dt.Rows[i]["TAT"]);
                        tatret[8] = tatret[8] + Convert.ToDecimal(dt.Rows[i]["TAT"]);

                    }

                }

            }

            return tatret;
        }

        public decimal GetCredit(string date, int productId)
        {
            var res = _locAdminManager.GetCredit(date, productId);
            return res;
        }

        public decimal GetAsset(string date, int productId)
        {
            var res = _locAdminManager.GetAsset(date, productId);
            return res;
        }
    }
}