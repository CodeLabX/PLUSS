﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductWiseCappingReport.aspx.cs" Inherits="PlussAndLoan.UI.ProductWiseCappingReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="ShowReportDiv" runat="server">
            <div id="viewDiv">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="100%" align="center" valign="top">
                            <table width="780" border="0" cellspacing="0" cellpadding="0">
                                <tr align="left" valign="top">
                                    <td height="47" background="image/blueline.gif">
                                        <img src="images/blueline.gif" width="554" height="47"></td>
                                    <td width="226" height="47">
                                        <img src="images/logo.gif" width="226" height="47"></td>
                                </tr>
                            </table>
                            <table width="780" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="780" height="47" align="left" valign="middle">
                                        <div align="left">
                                            <font color="#333333" size="2" face="Verdana, Arial, Helvetica, sans-serif"><img src="images/ltopbg.jpg" width="450" height="47"></font>
                                        </div>
                                        <div align="left"></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="0">
                    <tr>
                        <td width="31%" height="20" align="left"><font size="4" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font>
                        </td>
                        <td width="36%" align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product 
      wise Capping</font></strong>
                            <br>
                            <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Period :<asp:Label runat="server" ID="date"></asp:Label><asp:Label runat="server" ID="date2"></asp:Label><asp:Label runat="server" ID="date3"></asp:Label>
      </font></td>
                        <td width="33%" height="20" align="left" valign="middle" bgcolor="#FFFFFF">
                            <asp:Button runat="server" Text=" BACK"  style='border: 1px solid #83b67a; background-color: #BDD9A5' OnClick="Back_Click"/>
                            <asp:Button runat="server" text=" CLOSE"  style='border: 1px solid #83b67a; background-color: #BDD9A5' OnClick="Close_Click"/>
                            <asp:Button runat="server" text=" PRINT " style='border: 1px solid #83b67a; background-color: #BDD9A5' OnClientClick="javascript:window.print();"/>
                            <%--<input type="button" name="Submit1" value=" BACK" onclick="history.go(-1)" style='border: 1px solid #83b67a; background-color: #BDD9A5'>--%>
                            <%--<input type="button" name="Submit1" value=" CLOSE" onclick="window.close();" style='border: 1px solid #83b67a; background-color: #BDD9A5'>
                            <input type="button" name="Submit1" value=" PRINT " onclick="window.print();" style='border: 1px solid #83b67a; background-color: #BDD9A5'>--%>
                        </td>
                    </tr>
                </table>
                <br />
                <div id="divProdWiseCappingReport" style="overflow-y: scroll; height: 380px; width: 100%;" class="GridviewBorder">
                    <asp:GridView ID="ProdWiseCappingGridView" runat="server" AutoGenerateColumns="False" CellPadding="4" 
                        Font-Size="11px" PageSize="8" RowStyle-BorderWidth="1" RowStyle-BorderColor="Black" OnRowDataBound="ProdWiseCappingGridView_RowDataBound"
                        Width="100%" EnableModelValidation="True" GridLines="Both" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" ShowFooter="True">
                        <RowStyle BackColor="White" ForeColor="#333333" />
                        <Columns>
                            <asp:TemplateField HeaderText="Product Name" HeaderStyle-Width="40%" HeaderStyle-Height="25px">
                                <ItemTemplate>
                                    <asp:Label ID="productName" runat="server" Text='<%# Eval("PROD_NAME") %>'></asp:Label>
                            </ItemTemplate> 
                            <FooterTemplate>
                                <asp:Label runat="server" Text="Total"></asp:Label>
                            </FooterTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="40%">
                                <ItemTemplate>
                                    <asp:Label ID="amount" runat="server" Text='<%# Convert.ToInt32(Eval("tot")) + Convert.ToInt32(Eval("PCOD_")) %>'></asp:Label>
                                 <asp:Label runat="server" Text='<%# Bind("total") %>' ID="hdlabelTotal" Visible="false"> </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label runat="server" ID="labelTotal"> </asp:Label>
                                </FooterTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                          
                            <asp:TemplateField HeaderText="% of Total" HeaderStyle-Width="40%">
                                <ItemTemplate>
                                    <asp:Label ID="total" runat="server" Text='<%# ((Convert.ToDecimal(Eval("tot"))/Convert.ToDecimal(Eval("total")))*100)  %>'></asp:Label>
                                </ItemTemplate>
                                 <FooterTemplate>
                                    <asp:Label runat="server" Text="100%"></asp:Label>
                                </FooterTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="capping %" HeaderStyle-Width="40%">
                                <ItemTemplate>
                                    <asp:Label ID="PROD_CAPP_M" runat="server" Text='<%# Eval("PROD_CAPP_M") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Space Available" HeaderStyle-Width="40%">
                                <ItemTemplate>
                                    <asp:Label ID="space" runat="server" Text='<%# Convert.ToInt32(Eval("PROD_CAPP_M"))-(Convert.ToDecimal(Eval("tot"))/Convert.ToDecimal(Eval("total")))*100 %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="15%"></HeaderStyle>
                            </asp:TemplateField>
                           
                        </Columns>
                       
                        <HeaderStyle BackColor="#BDD3A5" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            &nbsp;&nbsp;No data found.
                        </EmptyDataTemplate>
                        <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <FooterStyle CssClass="GvFixedFooter" BackColor="White" ForeColor="#333333" BorderStyle="Solid" Font-Size="Medium" />
                    </asp:GridView>
                </div>
               
            </div>
        </div>
    </form>
</body>
</html>
