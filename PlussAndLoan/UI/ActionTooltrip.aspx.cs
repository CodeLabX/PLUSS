﻿using System;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_ActionTooltrip : System.Web.UI.Page
    {
        public SubSectorManager subSectorManagerObj = null;
        public ActionPointsManager actionPointsManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            subSectorManagerObj = new SubSectorManager();
            actionPointsManagerObj = new ActionPointsManager();
            if (Request.QueryString.Count != 0)
            {
                Int64 subsectorId = Convert.ToInt64(Request.QueryString["subSectorId"]);
                LoadActionPoint(subsectorId);
            }
        
        }
        private void LoadActionPoint(Int64 subsectorId)
        {
            if (subsectorId > 0)
            {
                SubSector SubSectorObj = subSectorManagerObj.SelectSubSector(subsectorId);//(Convert.ToInt64(subSectorDropDownList.SelectedValue.ToString()));
                if (SubSectorObj != null)
                {
                    ActionPoints actionPointsObj = actionPointsManagerObj.SelectActionPoints(SubSectorObj.Color.ToString());
                    if (actionPointsObj != null)
                    {
                        Response.Write("<table width='100%'>");
                        Response.Write("<tr>");
                        Response.Write("<td><b>Segment/Action Point</b></td>");
                        Response.Write("<td>" + SubSectorObj.Color.ToString() + "</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr>" );
                        Response.Write("<td><b>New Sourcing</b></td>" );
                        Response.Write("<td>" + actionPointsObj.NewSourcing.ToString() + "</td>" );
                        Response.Write("</tr>" );
                        Response.Write("<tr>" );
                        Response.Write("<td><b>Industry Profit Margin</b></td>"  );
                        Response.Write("<td>" + actionPointsObj.IndustryProfitMargin.ToString() + "</td>"  );
                        Response.Write("</tr>" );
                        Response.Write("<tr>"  );
                        Response.Write("<td><b>Deviation</b></td>" );
                        Response.Write("<td>" + actionPointsObj.Deviation.ToString() + "</td>" );
                        Response.Write("</tr>" );
                        Response.Write("<tr>" );
                        Response.Write("<td><b>Top-up</b></td>" );
                        Response.Write("<td>" + actionPointsObj.Topup.ToString() + "</td>" );
                        Response.Write("</tr>" );
                        Response.Write("<tr>" );
                        Response.Write("<td><b>Bureau Policy</b></td>"  );
                        Response.Write( "<td>" + actionPointsObj.BureauPolicy.ToString() + "</td>"  );
                        Response.Write("</tr>" );
                        Response.Write("</table>");
                    }
                }
            }
        }
    }
}
