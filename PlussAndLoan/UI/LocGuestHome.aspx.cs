﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bat.Common;
using BLL;
using BusinessEntities;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI
{
    public partial class LocGuestHome : System.Web.UI.Page
    {
        readonly LocatorApplicationService _service = new LocatorApplicationService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadProduct();
                LoadLoanState();
                LoadGrid("");
            }
        }

        private void LoadLoanState()
        {
            var loanApplicationManagerObj = new LoanApplicationManager();
            var dt = loanApplicationManagerObj.GetLoanStates();
            var loanStates = DataTableToList.GetList<Loan>(dt);
            var state = new Loan() { LoanState = 0, StateName = "" };
            loanStates.Insert(0, state);
            sel_loanStatus.DataTextField = "StateName";
            sel_loanStatus.DataValueField = "LoanState";
            sel_loanStatus.DataSource = loanStates;
            sel_loanStatus.DataBind();
        }

        private void LoadProduct()
        {
            List<Product> productListObj = new LoanApplicationManager().GetAllProduct();
            var productObj = new Product { ProductId = 0, ProductName = "" };
            productListObj.Insert(0, productObj);
            findByProduct.DataTextField = "ProductName";
            findByProduct.DataValueField = "ProductId";
            findByProduct.DataSource = productListObj;
            findByProduct.DataBind();
        }

        private void LoadGrid(string filter)
        {

            var dtTmpList = _service.GetLoanApplications(0, filter);
            totalLoan.Text = dtTmpList == null ? string.Format("Total 0 Loan Applications") : string.Format("Total {0} Loan Applications", dtTmpList.Rows.Count);
            gvData.DataSource = dtTmpList;
            gvData.DataBind();
        }


        protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvData.PageIndex = e.NewPageIndex;
            LoadGrid("");
        }


        protected void Edit(object sender, EventArgs e)
        {
            ////
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var llid = findById.Text;
            var productId = findByProduct.SelectedValue;
            var acc = findAcBy.Text;
            var customer = findByCustomer.Text;
            var state = sel_loanStatus.SelectedValue;
            var filterBy = "";
            //where l.LOAN_APPL_ID=30 and l.PROD_ID=7 and l.ACC_NUMB='32-2424343-32' and l.CUST_NAME='SE'
            if (!string.IsNullOrEmpty(llid))
            {
                filterBy += string.IsNullOrEmpty(filterBy) ? string.Format(" where l.LOAN_APPL_ID={0}", llid) : string.Format(" and l.LOAN_APPL_ID={0}", llid);
            }
            if (productId != "0")
            {
                filterBy += string.IsNullOrEmpty(filterBy) ? string.Format(" where l.PROD_ID={0}", productId) : string.Format(" and l.PROD_ID={0}", productId);
            }
            if (!string.IsNullOrEmpty(acc))
            {
                filterBy += string.IsNullOrEmpty(filterBy) ? string.Format(" where l.ACC_NUMB='{0}'", acc) : string.Format(" and l.ACC_NUMB='{0}'", acc);
            }
            if (!string.IsNullOrEmpty(customer))
            {
                filterBy += string.IsNullOrEmpty(filterBy) ? string.Format(" where l.CUST_NAME like '%{0}%'", customer) : string.Format(" and l.CUST_NAME like '%{0}%'", customer);
            }
            if (state != "0")
            {
                filterBy += string.IsNullOrEmpty(filterBy) ? string.Format(" where l.LOAN_STATUS_ID='{0}'", state) : string.Format(" and l.LOAN_STATUS_ID='{0}'", state);
            }
            LoadGrid(filterBy);
        }

        protected void refresh_Click(object sender, EventArgs e)
        {
            findById.Text = "";
            findByProduct.SelectedValue = "0";
            findAcBy.Text = "";
            findByCustomer.Text = "";
            LoadGrid("");
        }
    }
}