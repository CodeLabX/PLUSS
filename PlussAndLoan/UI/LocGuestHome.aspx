﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/GuestMaster.Master" AutoEventWireup="true" CodeBehind="LocGuestHome.aspx.cs" Inherits="PlussAndLoan.UI.LocGuestHome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Loan Locator Home</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="100%">
        <tr border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
            <td colspan="6"><b>Search By </b></td>
        </tr>
        <tr border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#00CCCC" bgcolor="BDD3A5">
            <td style="width: 15%"><b>Loan ID:</b><asp:TextBox runat="server" ID="findById" Width="100px"></asp:TextBox></td>
            <td><b>Product:</b><asp:DropDownList runat="server" ID="findByProduct" Width="150px"/></td>
            <td><b>A/C No.:</b><asp:TextBox runat="server" ID="findAcBy" Width="120px"></asp:TextBox></td>
            <td><b>Customer:</b><asp:TextBox runat="server" ID="findByCustomer"></asp:TextBox></td>
            <td>Status:
                <asp:DropDownList runat="server" ID="sel_loanStatus" Width="150px"/>
               
            <td>
                <asp:Button runat="server" ID="btnSearch" Text="GO" OnClick="btnSearch_Click"/>
                <asp:Button runat="server" ID="refresh" Text="Refresh" OnClick="refresh_Click"/>
            </td>
        </tr>
        <tr>
           <td colspan="5"><asp:Label runat="server" ID="totalLoan" Font-Bold="True">Total 0 Loan Applications</asp:Label></td>
        </tr>
    </table>
    <asp:GridView Width="100%"
        ID="gvData" runat="server" SkinID="SCBLGridGreen" OnPageIndexChanging="gvData_PageIndexChanging"
        AllowPaging="True" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" EnableModelValidation="True" AutoGenerateColumns="False" CellSpacing="1" GridLines="None">
        <Columns>
            <asp:TemplateField HeaderText="APPL. ID">
                <ItemTemplate>
                    <a href='loc_GuestEditLoan.aspx?id=<%#Eval("LLID")%>' style="color: black;text-decoration: underline"><%#Eval("LLID")%></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductName" HeaderText="Product Name" ItemStyle-HorizontalAlign="Center">
                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="AppliedDate" HeaderText="Application Date" DataFormatString="{0:yyyy-MM-dd}">
                <ControlStyle Width="50px" />
                <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="RecieveDate" HeaderText="Submission Date" DataFormatString="{0:yyyy-MM-dd}">
                <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="AccNo" HeaderText="A/C NO.">
                <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="AccType" HeaderText="Type">
                <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="AppliedAmount" HeaderText="Amount (Applied)">
                <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="ApplicantName" HeaderText="Customer Name">
                <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="StateName" HeaderText="State">
                <HeaderStyle Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductId" HeaderText="ProductId" Visible="False" />
            <asp:BoundField DataField="DecisionStatusId" HeaderText="DecisionStatusId" Visible="False" />
            <asp:BoundField DataField="SourceId" HeaderText="SourceId" Visible="False" />
            <asp:BoundField DataField="LoanStatusId" HeaderText="LoanStatusId" Visible="False" />
            <asp:BoundField DataField="DeliDeptId" HeaderText="DeliDeptId" Visible="False" />
            <asp:BoundField DataField="DeptType" HeaderText="DeptType" Visible="False" />
            <asp:BoundField DataField="PersonId" HeaderText="PersonId" Visible="False" />
            <asp:BoundField DataField="Maker" HeaderText="Maker" Visible="False" />

        </Columns>

        <EmptyDataTemplate>
            No loan application found
        </EmptyDataTemplate>
        <AlternatingRowStyle CssClass="odd" />
        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
        <PagerSettings PageButtonCount="4" />
        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle ForeColor="Black" BackColor="#DEDFDE" />
        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
    </asp:GridView>

</asp:Content>
