﻿using System;
using System.Globalization;
using System.Web.UI;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_MISReportUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            activityReportDiv.Visible = false;
        }
        protected void docListButton_Click(object sender, EventArgs e)
        {
            if (lLIdTextBox.Text != "")
            {
                ScriptManager.RegisterClientScriptBlock(docListButton, this.GetType(), "docListButton", "<script type='text/javascript'>window.open('../Reports/DocumentListReport.aspx?llId=" + lLIdTextBox.Text + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=924,height=768')</script>", false);
            }
            else
            {
                Alert.Show("Please input Loan Locator Id");
                lLIdTextBox.Focus();
            }
        }
        protected void plchecklistButton_Click(object sender, EventArgs e)
        {
            if (lLIdTextBox.Text != "")
            {
                ScriptManager.RegisterClientScriptBlock(plchecklistButton, this.GetType(), "plchecklistButton", "<script type='text/javascript'>window.open('../Reports/PLCheckListReport.aspx?lLId=" + lLIdTextBox.Text + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=924,height=768')</script>", false);
            }
            else
            {
                Alert.Show("Please input Loan Locator Id");
                lLIdTextBox.Focus();
            }
        }
        protected void LLINewButton_Click(object sender, EventArgs e)
        {
            if (lLIdTextBox.Text != "")
            {
                ScriptManager.RegisterClientScriptBlock(LLINewButton, this.GetType(), "LLINewButton", "<script type='text/javascript'>window.open('../Reports/LLINew.aspx?lLId=" + lLIdTextBox.Text + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=924,height=768')</script>", false);
            }
            else
            {
                Alert.Show("Please input Loan Locator Id");
                lLIdTextBox.Focus();
            }
        }
        protected void baselButton_Click(object sender, EventArgs e)
        {
            if (lLIdTextBox.Text != "")
            {
                ScriptManager.RegisterClientScriptBlock(baselButton, this.GetType(), "baselButton", "<script type='text/javascript'>window.open('../Reports/BASEL2DataCheckList.aspx?lLId=" + lLIdTextBox.Text + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=924,height=768')</script>", false);
            }
            else
            {
                Alert.Show("Please input Loan Locator Id");
                lLIdTextBox.Focus();
            }
        }
        protected void approvalButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (lLIdTextBox.Text != "")
                {
                    ScriptManager.RegisterClientScriptBlock(approvalButton, this.GetType(), "approvalButton", "<script type='text/javascript'>window.open('../Reports/ApprovalInfoReport.aspx?lLId=" + lLIdTextBox.Text + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=924,height=768')</script>", false);
                }
                else
                {
                    Alert.Show("Please input Loan Locator Id");
                    lLIdTextBox.Focus();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Approve MIS Report");
            }
        }
        protected void declineBaselButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (lLIdTextBox.Text != "")
                {
                    ScriptManager.RegisterClientScriptBlock(declineBaselButton, this.GetType(), "declineBaselButton", "<script type='text/javascript'>window.open('../Reports/DeclinedBASEL2DataCheckList.aspx?lLId=" + lLIdTextBox.Text + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=924,height=768')</script>", false);
                }
                else
                {
                    Alert.Show("Please input Loan Locator Id");
                    lLIdTextBox.Focus();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Declined MIS Report");
            }
        }
        protected void showAllButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (lLIdTextBox.Text != "")
                {
                    //ScriptManager.RegisterClientScriptBlock(declineBaselButton, this.GetType(), "declineBaselButton", "<script type='text/javascript'>window.open('../Reports/ShowAllReport.aspx?lLId=" + lLIdTextBox.Text + "','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=1024,height=768')</script>", false);                      
                    baselButton_Click(sender, e);
                    LLINewButton_Click(sender, e);
                    plchecklistButton_Click(sender, e);
                    //docListButton_Click(sender, e);
                    approvalButton_Click(sender, e);
                }
                else
                {
                    Alert.Show("Please input Loan Locator Id");
                    lLIdTextBox.Focus();
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Show MIS Report");
            }
        }
        protected void activityReportButton_Click(object sender, EventArgs e)
        {
            activityReportDiv.Visible = true;        
        }
        protected void showButton_Click(object sender, EventArgs e)
        {
            //List<ActivityReportObject> activityReportObject =
            //  new ActivityReportManager().GetAllActivity(Convert.ToDateTime(dateTextBox1.Text, new CultureInfo("ru-RU")), Convert.ToDateTime(dateTextBox2.Text, new CultureInfo("ru-RU")));
        
            Session["fromDate"] = Convert.ToDateTime(DateTime.Now, new CultureInfo("ru-RU"));
            Session["toDate"] = Convert.ToDateTime(DateTime.Now, new CultureInfo("ru-RU"));

            ScriptManager.RegisterClientScriptBlock(declineBaselButton, this.GetType(), "declineBaselButton", @"<script type='text/javascript'>window.open('../Reports/ActivityReport.aspx','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=1024,height=768')</script>", false);
        }
        protected void exceptionRerptBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/ExceptionReportUI.aspx");
        }
        protected void userRerptBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/SystemGeneratedUserReport.aspx");
        }
        protected void systemMatrixRerptBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/SecurityMatrixReport.aspx");
        }

  
    }
}
