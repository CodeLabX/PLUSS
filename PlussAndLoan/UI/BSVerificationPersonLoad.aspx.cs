﻿using System;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_BSVerificationPersonLoad : System.Web.UI.Page
    {
        public BankStvPersonManager bankStvPersonManagerObj = null;
        public BSVERManager bSVERManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bankStvPersonManagerObj = new BankStvPersonManager();
                bSVERManagerObj = new BSVERManager();
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString.Count != 0)
                    {
                        Int64 LLId = Convert.ToInt64(Request.QueryString["LLId"]);
                        Int16 bankId = Convert.ToInt16(Request.QueryString["bankId"]);
                        Int16 branchId = Convert.ToInt16(Request.QueryString["branchId"]);
                        string aCNo = Convert.ToString(Request.QueryString["aCNo"]);
                        LoadNameEmailAddress(LLId,bankId,branchId,aCNo);
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Load Name Email Address");  
            }
        }
        private void LoadNameEmailAddress(Int64 LLId,Int16 bankId,Int16 branchId,string aCNo)
        {
            string AcName = "";
            string customerNameAddress = null;
            if (bankId > 0)
            {
                BankStvPerson bankStvPersonObj = bankStvPersonManagerObj.SelectBankStvPersonInfo(bankId);
                AcName = bSVERManagerObj.SelectAccountName(LLId,bankId ,branchId, aCNo);
                customerNameAddress = bSVERManagerObj.SelectCustomerNameAddress(LLId);
                if (bankStvPersonObj != null && AcName != "" && customerNameAddress!=null)
                {
                    Response.Write(bankStvPersonObj.Name.ToString() + "#" + bankStvPersonObj.EmailAddress.ToString() + "#" + AcName + "#" + customerNameAddress);
                }
                else
                {
                    Response.Write( "#"+"#" + AcName +"#"+customerNameAddress);
                }
            }
        }
    }
}
