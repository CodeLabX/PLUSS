﻿using System;
using System.Text.RegularExpressions;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_ChangePassword : System.Web.UI.Page
    {
        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2
        }
        #endregion

        private UserManager userManagerObj;
        private UserInfoHistoryManager userInfoHistoryManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            userManagerObj = new UserManager();
            userInfoHistoryManagerObj = new UserInfoHistoryManager();
        }
        protected void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                int insertHistory = 0;
                int insertOrUpdateRow = -1;
                if (CheckPasswordPolyce() == false)
                {
                    return;
                }
                else
                {
                    if (Page.IsValid)
                    {
                        bool isMatch = userManagerObj.IsOldPasswordMatch(userIdTextBox.Text.Trim(), oldPasswordTextBox.Text.Trim());
                        bool isUserInfoHistory = userInfoHistoryManagerObj.GetLastChangeUserInfoHistory(userIdTextBox.Text.Trim(),newPasswordTextBox.Text.Trim(), 10);
                        if (!userManagerObj.IsExistsUserId(userIdTextBox.Text.Trim()))
                        {
                            errorMsgLabel.Text = "Login id not exists.";
                        }
                        else if (!isMatch)
                        {
                            errorMsgLabel.Text = "Old password not match.";
                        }
                        else if (isUserInfoHistory)
                        {
                            errorMsgLabel.Text = "Don't use previous 10 password.";
                        }
                        else
                        {
                            var msg = "";
                            errorMsgLabel.Text = "";
                            insertOrUpdateRow = userManagerObj.ChangePassword(userIdTextBox.Text.Trim(), newPasswordTextBox.Text.Trim());
                            if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                            {
                                UserInfoHistory userInfoHistoryObj = new UserInfoHistory();
                                userInfoHistoryObj.UserLogInId = userIdTextBox.Text.Trim();
                                userInfoHistoryObj.PassWord = newPasswordTextBox.Text.Trim();
                                userInfoHistoryObj.ChangeDate = DateTime.Now;
                                insertHistory = userInfoHistoryManagerObj.SendDataInToDB(userInfoHistoryObj);
                                msg = "Password change successfully.";
                                ClientScript.RegisterStartupScript(GetType(), "tst", "<script>alert('Password change successfully.');</script>");
                                //Response.Redirect("LoginUI.aspx");

                            }
                            else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                            {
                                msg = "Password  not change successfully.";
                                errorMsgLabel.Text = "Password  not change successfully.";
                            }
                            new AT(this).AuditAndTraial("Change Password", msg);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Print BSVER");
            }
        }
        private bool CheckPasswordPolyce()
        {
            bool returnValue = true;
            if (String.IsNullOrEmpty(userIdTextBox.Text.Trim()))
            {
                errorMsgLabel.Text = "Please input user id.";
                returnValue = false;
            }
            else if (String.IsNullOrEmpty(oldPasswordTextBox.Text.Trim()))
            {
                errorMsgLabel.Text = "Please input old password.";
                returnValue = false;
            }
            else if (String.IsNullOrEmpty(newPasswordTextBox.Text.Trim()))
            {
                errorMsgLabel.Text = "Please input new password.";
                returnValue = false;
            }
            else if (String.IsNullOrEmpty(confrimPasswordTextBox.Text.Trim()))
            {
                errorMsgLabel.Text = "Please input cofirm password.";
                returnValue = false;
            }
            else if (IsOnlyLetters(newPasswordTextBox.Text.Trim()))
            {
                errorMsgLabel.Text = "Password should be string neumeric with Identyfy character.";
                returnValue = false;
            }
            else if (newPasswordTextBox.Text.Trim().Length <= 5 || newPasswordTextBox.Text.Trim().Length > 14)
            {
                errorMsgLabel.Text = "Password should be more then 6 character and less then 14.";
                returnValue = false;
            }
            else if (newPasswordTextBox.Text.Trim() != confrimPasswordTextBox.Text.Trim())
            {
                errorMsgLabel.Text = "No match new passwoerd with confirm password";
                returnValue = false;
            }
            else
            {
                returnValue = true;
            }
            return returnValue;
        }

        private bool IsOnlyLetters(string passwordValue)
        {
            string letters = "^[a-zA-Z0-9]*$";
            if (Regex.Match(passwordValue, letters).Success)
            {
                return true;
            }           
            return false;
        }
        protected void LogInPageLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/LoginUI.aspx");
        }
    }
}
