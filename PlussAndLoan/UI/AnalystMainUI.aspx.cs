﻿using System;
using System.Web;

namespace PlussAndLoan.UI
{
    public partial class UI_AnalystMainUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            if (Session.Count == 0)
            {
                Response.Redirect("~/ErrorPage/Timout.htm");
            }
            userNameLabel.Text = (string)Session["UserName"];

        }
        protected void logOutLinkButton_Click(object sender, EventArgs e)
        {
            Session.Remove("Id");
            Session.Remove("UserId");
            Session.Remove("UserName");
            Session.Remove("UserType");
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            Response.Redirect("~/LoginUI.aspx");
        }
    }
}
