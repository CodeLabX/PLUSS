﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BLL;
using BusinessEntities;
using DAL;
using Label = System.Web.UI.WebControls.Label;

namespace PlussAndLoan.UI
{
    public partial class CheckList : System.Web.UI.Page
    {

        #region InsertUpdateFlag
        /// <summary>
        /// Enum for Insert or update flag.
        /// </summary>
        enum InsertUpdateFlag : int
        {
            INSERT = 1,
            UPDATE = 2,
            INSERTERROR = -1,
            UPDATEERROR = -2,
            DELETE=4,
            DELETEERROR=-4
        }
        #endregion

        public LoanApplicationManager loanApplicationManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            DeleteButton.Visible = false;
            saveButton.Text = "Save";
            loanApplicationManager = new LoanApplicationManager();
            if (!IsPostBack)
            {
                LoadProduct();
                LoadCheckList();
            }
        }

        private void LoadProduct()
        {
            //List<Product> productListObj = new LoanApplicationManager().GetAllProduct();
            cmbProduct.DataSource = loanApplicationManager.GetAllProduct();
            cmbProduct.DataTextField = "ProductName";
            cmbProduct.DataValueField = "ProductId";
            cmbProduct.DataBind();

        }

        private void LoadCheckList()
        {
            var checkListObj = loanApplicationManager.GetAllCheckLists();
            CheckListGrid.DataSource = checkListObj;
            CheckListGrid.DataBind();
        }

        protected void CheckListGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DeleteButton.Visible = true;
            saveButton.Text = "Edit";
            int index = CheckListGrid.SelectedIndex;
            if (index > -1)
            {
                int rowIndex = Convert.ToInt32(CheckListGrid.SelectedRow.RowIndex);
                idHiddenField.Value = (CheckListGrid.Rows[rowIndex].Cells[3].FindControl("Id") as Label).Text;
                titleTextBox.Text = (CheckListGrid.Rows[rowIndex].Cells[1].FindControl("titleLabel") as Label).Text;
                cmbProduct.SelectedValue = (CheckListGrid.Rows[rowIndex].Cells[3].FindControl("ProdIdLabel") as Label).Text;
                statusDropDowonList.SelectedValue = (CheckListGrid.Rows[rowIndex].Cells[2].FindControl("status") as Label).Text;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int insertOrUpdateRow = -1;
                BusinessEntities.CheckList checkList = new BusinessEntities.CheckList();

                if (String.IsNullOrEmpty(titleTextBox.Text.Trim()))
                {
                    errorMsgLabel.Text = "You Must Enter Check List Title";
                }
                else
                {
                    errorMsgLabel.Text = "";
                    if (idHiddenField.Value != "")
                    {
                        checkList.Id = Convert.ToInt32(idHiddenField.Value);
                    }
                    checkList.Title = titleTextBox.Text.Trim();
                    checkList.ProductId = Convert.ToInt32(cmbProduct.Text.Trim());
                    checkList.Status = Convert.ToInt32(statusDropDowonList.Text.Trim());



                    insertOrUpdateRow = loanApplicationManager.InsertORUpdateCheckList(checkList);

                    if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERT))
                    {
                        errorMsgLabel.Text = "Data save sucessfully.";
                        ClearField();
                        LoadCheckList();


                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.INSERTERROR))
                    {
                        errorMsgLabel.Text = "Data not saved sucessfully.";
                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATE))
                    {
                        errorMsgLabel.Text = "Data update sucessfully.";
                        saveButton.Text = "Save";
                        ClearField();
                        LoadCheckList();
                    }
                    else if (insertOrUpdateRow.Equals((int)InsertUpdateFlag.UPDATEERROR))
                    {
                        errorMsgLabel.Text = "Data not update sucessfully.";
                    }
                }
                new AT(this).AuditAndTraial("Check List Information", errorMsgLabel.Text);

            }
            catch (Exception exception)
            {

                CustomException.Save(exception, "Save CheckList");
            }
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            var id = 0;
            int deleteRow = -1;
            if (idHiddenField.Value != "")
            {
                id = Convert.ToInt32(idHiddenField.Value);
            }
            deleteRow = loanApplicationManager.DeleteCheckList(id);
            if (deleteRow.Equals((int)InsertUpdateFlag.DELETE))
            {
                errorMsgLabel.Text = "Delete sucessfully.";
                ClearField();
                LoadCheckList();
                }
            else if (deleteRow.Equals((int)InsertUpdateFlag.DELETEERROR))
            {
                errorMsgLabel.Text = "Not Delete sucessfully.";
            }
        }

        public void ClearField()
        {
            idHiddenField.Value = "0";
            titleTextBox.Text = "";
            cmbProduct.SelectedValue = "1";
            statusDropDowonList.SelectedValue = "1";
            errorMsgLabel.Text = "";
        }
    }
}