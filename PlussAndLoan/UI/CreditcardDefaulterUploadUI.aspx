﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_CreditcardDefalterUploadUI" Title="Credit Card Defaulter Upload" Codebehind="CreditcardDefaulterUploadUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/PopupWindows.ascx" TagPrefix="uc1" TagName="PopupWindows" %>
<%@ Register Src="~/UI/UserControls/js/PopupWindowsJs.ascx" TagPrefix="uc1" TagName="PopupWindowsJs" %>
<%@ Register Src="~/UI/UserControls/js/CalendarInitialJs.ascx" TagPrefix="uc1" TagName="CalendarInitialJs" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="Server">
    <uc1:PopupWindows runat="server" ID="PopupWindows" />
    <uc1:PopupWindowsJs runat="server" ID="PopupWindowsJs" />
    <uc1:CalendarInitialJs runat="server" ID="CalendarInitialJs" />
    <script type="text/javascript">
        window.setInterval("updateTime()", 1000);
        function updateTime() {
            var now = new Date();
            var tHrs = now.getHours();
            var tMin = now.getMinutes();
            var tSec = now.getSeconds();
            var tTime = ((tHrs < 10) ? "0" + tHrs : tHrs) + ":" + ((tMin < 10) ? "0" + tMin : tMin) + ":" + ((tSec < 10) ? "0" + tSec : tSec);
            GetUploadCount();
        }
        function GetUploadCount() {
            var uploadCount = '<%=Session["uploadCount"]%>';
        document.getElementById('ctl00_ContentPlaceHolder_waitLabel').innerHTML = "Please wait data uploading...=" + uploadCount;

    }
    function LoadPopupWindows() {
        debugger;
        var uploadCount = '<%=Session["uploadCount"]%>';
        document.getElementById('ctl00_ContentPlaceHolder_waitLabel').innerHTML = "Please wait data uploading...=" + uploadCount;
        ShowPopUpWindows('pleaseWaidDiv', mainBody);
    }
    function PopUpWindowsClose() {
        closeDialog('pleaseWaidDiv');
    }

    </script>

</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr style="font-weight: bold; font-size: 16px" align="center">
            <td>
                <asp:Label ID="nameLabel" runat="server" Text="Credit Card Info Upload"></asp:Label></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:FileUpload ID="FileUpload" runat="server" Width="350px" Height="24px" />
                <asp:Label ID="criteriaLable" runat="server" Text="Criteria :"></asp:Label>
                <asp:DropDownList ID="criteriaDropDownList" runat="server" Width="80px">
                    <asp:ListItem>C</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="UploadButton" runat="server" Text="Upload" Width="100px"
                    Height="24px" OnClick="uploadButton_Click" />
                <asp:HiddenField ID="HiddenFieldcount" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <div id="recordViewDiv" runat="server" style="width: 805px; height: 470px; z-index: 5000; border: solid 1px gray;"></div>
                <div id="pagingDiv" runat="server"></div>

            </td>
        </tr>
        <tr>
            <td>
                <div id="pleaseWaidDiv" class="pleaseWaidDivwith" style="display: none; position: absolute; border: solid 1px gray;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
                        align="center">
                        <tr bgcolor="#B70000">
                            <td colspan="2">
                                <asp:Label runat="server" ID="TotalRecordLabel" Text="Data upload" Font-Bold="true"
                                    Font-Size="14px" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center">
                                <img id="loadingImg" src="../Images/ajax-loader.gif" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center">
                                <asp:Label runat="server" ID="waitLabel" Text="Please wait data uploading..." Font-Names="Times New Roman"
                                    Font-Bold="false" Font-Size="13px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td align="center">
                                <input id="okButton" type="button" value="OK" onclick="PopUpWindowsClose();" />
                            </td>
                        </tr>
                    </table>
                </div>

            </td>
        </tr>
    </table>
    <div onmouseup="return false" class="translucent" onmousemove="return false" onmousedown="return false"
        id="blockUI" ondblclick="return false" style="display: none; z-index: 50000; left: 0px; position: absolute; top: 0px; background-color: gray"
        onclick="return false">
    </div>
    <script type="text/javascript">

        function onLoadPage() {
            init('dataTable');
        }
    </script>
</asp:Content>


