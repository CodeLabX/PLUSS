﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI
{
    public partial class ReportNegativeList : System.Web.UI.Page
    {
        private readonly LoanReportService _service = new LoanReportService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                startDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
                endDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
        protected void ExportNegative_Click(object sender, EventArgs e)
        {
            var filename = "NegativeListEmployer_" + DateTime.Now.Ticks + ".csv";
            var csv = string.Empty;
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Charset = "";
            Response.ContentType = "application/text";

            var startDate = Convert.ToDateTime(startDateTextBox.Text);
            var endDate = Convert.ToDateTime(endDateTextBox.Text);
            var dt = _service.ExportNegativeList(startDate, endDate);
            if (dt != null)
            {
                csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ';'));
                csv += "\r\n";

                foreach (DataRow row in dt.Rows)
                {

                    csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace("'", "").Replace("\t", "").Replace(";", "").Replace("\"","") + '\t'));
                    csv += "\r\n";
                }

                //Download the CSV file.

                Response.Output.Write(csv);
                Response.Flush();
                Response.End();
            }
        }
    }
}