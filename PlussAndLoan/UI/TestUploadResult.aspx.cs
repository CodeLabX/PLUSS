﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_TestUploadResult : System.Web.UI.Page
    {
        List<DeDupInfo> deDupInfoObjList = null;
        DeDupInfoManager dedupeManagerObj = null;
        ProductManager productManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            deDupInfoObjList = new List<DeDupInfo>();
            dedupeManagerObj = new DeDupInfoManager();
            try
            {
                string stype = string.Empty;
                string noOfRecord = string.Empty;
                string lowIndex = string.Empty;
                string strOperation = string.Empty;
                string strsearchKey = string.Empty;
                string qDate = string.Empty;
                strOperation = Convert.ToString(Request.QueryString["operation"]);
                noOfRecord = Convert.ToString(Request.QueryString["NoofRecord"]);
                lowIndex = Convert.ToString(Request.QueryString["LowIndex"]);
                strsearchKey = Convert.ToString(Request.QueryString["SEARCHKEY"]);
                qDate = Convert.ToString(Request.QueryString["QUERYDATE"]);
                if (strOperation == "upload")
                {
                    StreamReader streamReader = new StreamReader(Request.InputStream);
                    string fileNameAndPath = streamReader.ReadToEnd();
                    this.Response.Write("");
                    this.Response.Flush();
                    this.Response.Close();
                    this.Response.End();

                }
                else if (strOperation == "LoadUploadData" && strsearchKey != "")
                {
                    this.Response.Write(LoadUploadData(strsearchKey, qDate));
                    this.Response.Flush();
                    this.Response.Close();
                    this.Response.End();
                }

            }
            catch (Exception ex)
            {
                this.Response.Write("error occured" + ex.Message);
                this.Response.Flush();
                this.Response.Close();
                this.Response.End();
            }
            //LoadUploadData("","");

        }
        private String LoadUploadData(string strsearchKey, string qDate)
        {
            StringBuilder sbSourceData = new StringBuilder();
            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            if (strsearchKey.Length > 0)
            {
                deDupInfoObjList = dedupeManagerObj.GetUploadResult(Convert.ToDateTime(qDate, new System.Globalization.CultureInfo("ru-RU")), Convert.ToDateTime(qDate, new System.Globalization.CultureInfo("ru-RU")), strsearchKey, 0, 65000);

            }
            else
            {
                deDupInfoObjList = dedupeManagerObj.GetUploadResult(DateTime.Now, DateTime.Now, "D", 0, 65000);
            }
            try
            {
                sbSourceData.Append("<?xml version='1.0' encoding='iso-8859-1'?>");
                sbSourceData.Append("<rows>");
                foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
                {
                    sbSourceData.Append("<row id='" + deDupInfoObj.Id.ToString() + "'>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(deDupInfoObj.Id.ToString());
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.Name));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.AccountNo.MasterAccNo1));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.FatherName));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.MotherName));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(deDupInfoObj.DateOfBirth.ToString("dd-MM-yyyy"));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.DeclineReason));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(deDupInfoObj.DeclineDate.ToString("dd-MM-yyyy"));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.BusinessEmployeeName));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.Address1));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.Address2));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ContactNumber.ContactNo1));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ContactNumber.ContactNo2));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ContactNumber.ContactNo3));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ContactNumber.ContactNo4));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    string productName = string.Empty;
                    if (deDupInfoObj.ProductId > 0)
                    {
                        productManagerObj = new ProductManager();
                        productName = productManagerObj.GetProductName(deDupInfoObj.ProductId);
                    }
                    else
                    {
                        productName = "";
                    }
                    sbSourceData.Append(AddSlash(productName));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.BankBranch));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.Source));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.Status));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.Remarks));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.Tin));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Type1));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Id1));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Type2));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Id2));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Type3));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Id3));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Type4));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Id4));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Type5));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ApplicantId.Id5));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.ResidentaileStatus));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.EducationalLevel));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.MaritalStatus));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(deDupInfoObj.LoanAccountNo));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("</row>");
                }
                sbSourceData.Append("</rows>");

                Response.Write(sbSourceData.ToString());
                Response.End();

            }
            catch (Exception ex)
            {

            }
            return sbSourceData.ToString() + "#";
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (!String.IsNullOrEmpty(sMessage))
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
