﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_UserLoginStatusUI : System.Web.UI.Page
    {
        private UserAccessLogManager userAccessLogManagerObj = null;
        int returnValue = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            userAccessLogManagerObj = new UserAccessLogManager();
            accessLogIdHiddenField.Value = Request[this.accessLogIdHiddenField.UniqueID];
            searchTextBox.Attributes.Add("onkeyup", "DataSearch();");
            string searchKey = searchTextBox.Text;
            LoadLoginUserStatus(searchKey);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();", true);
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                UserAccessLog userAccessLogObj = new UserAccessLog();
                if (accessLogIdHiddenField.Value != "")
                {
                    userAccessLogObj.AcessLogId = Convert.ToInt32(accessLogIdHiddenField.Value);
                    userAccessLogObj.userLoginStatus = 0;
                }
                returnValue = userAccessLogManagerObj.UpdateLoginStatus(userAccessLogObj);
                if (returnValue > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();", true);
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save User Login Status");
            }
        }


        private void LoadLoginUserStatus(string searchKey)
        {
            string LoginStatus = "";
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            if (searchKey == "" || searchKey == null)
            {
                userAccessLogObjList = userAccessLogManagerObj.GetAccessLog(DateTime.Now, DateTime.Now);
            }
            else
            {
                userAccessLogObjList = userAccessLogManagerObj.GetAccessLog(searchKey, DateTime.Now, DateTime.Now);
            }
            if (userAccessLogObjList != null)
            {
                //Response.ContentType = "text/xml";
                //Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>");
                //Response.Write("<rows>");
                var xml = "<xml id='xml_data'><rows>";
                foreach (UserAccessLog userAccessLogObj in userAccessLogObjList)
                {
                   xml+="<row id='" + userAccessLogObj.AcessLogId.ToString() + "'>";
                   xml+="<cell>";
                   xml+=userAccessLogObj.AcessLogId.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=userAccessLogObj.UserLogInId.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=userAccessLogObj.IP.ToString();
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+=userAccessLogObj.LogInDateTime.ToString("dd-MM-yyyy HH:mm:ss");
                   xml+="</cell>";
                   xml+="<cell>";

                    if (userAccessLogObj.userLoginStatus == 1)
                    {
                        LoginStatus = "Login";
                    }
                    else
                    {
                        LoginStatus = "Logout";
                    }
                   xml+=LoginStatus;
                   xml+="</cell>";
                   xml+="<cell>";
                   xml+="Action^javascript:LoadUserPopUp(" + userAccessLogObj.AcessLogId.ToString() + ")^_self";
                   xml+="</cell>";

                   xml+="</row>";
                }
                xml += "</rows></xml>";
                dvXml.InnerHtml = xml;
            }
        }
    }
}
