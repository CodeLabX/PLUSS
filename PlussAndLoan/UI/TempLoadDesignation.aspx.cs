﻿using System;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_TempLoadDesignation : System.Web.UI.Page
    {
        public UserManager userManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                userManagerObj = new UserManager();
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString.Count != 0)
                    {
                        Int32 UserId = Convert.ToInt32(Request.QueryString["userId"]);
                        LoadDesignation(UserId);
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Load Temp Designation");
            }

        }
        private void LoadDesignation(Int32 UserId)
        {
            User userObj = userManagerObj.GetUserInfo(UserId);
            if (userObj != null)
            {
                Response.Write(userObj.Designation.ToString() + "#");
                Response.Write(userObj.Email.ToString());
            }

        }
    }
}
