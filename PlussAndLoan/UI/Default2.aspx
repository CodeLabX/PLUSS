﻿<%@ Page Language="C#" MasterPageFile="~/UI/AutoLoan/AutoSales.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_Default2" Title="Untitled Page" Codebehind="Default2.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
            height: 60px;
        }
        .style3
        {
        }
        .style4
        {
            width: 241px;
        }
        .style5
        {
            width: 179px;
        }
        .style7
        {
            font-weight: bold;
            font-size: large;
            height: 13px;
        }
        .style8
        {
            width: 100%;
        }
        .style10
        {
            width: 48px;
        }
        .style11
        {
            width: 11px;
            text-align: center;
        }
        .style12
        {
            height: 26px;
            font-size: small;
            font-weight: bold;
        }
        .style13
        {
        }
        .style14
        {
            width: 291px;
        }
        .style15
        {
        }
        table.reference {
    background-color: #FFFFFF;
    border: 1px solid #C3C3C3;
    border-collapse: collapse;
    width: 100%;
}
table.reference th {
    background-color: #E5EECC;
    border: 1px solid #C3C3C3;
    padding: 0px;
    vertical-align: top;
}
table.reference td {
    border: 1px solid #C3C3C3;
    padding: 0px;
    vertical-align: top;
    font-size:12px;
} 
        
        .style16
        {
            width: 100px;
            height: 16px;
        }
        .style17
        {
            width: 291px;
            height: 16px;
        }
        .style18
        {
            width: 104px;
            height: 16px;
        }
        .style19
        {
            height: 16px;
        }
        .style20
        {
            width: 104px;
        }
        
        .style21
        {
            width: 12px;
        }
        .style22
        {
        }
        .style23
        {
        }
        .style24
        {
        }
        .style25
        {
        }
        .style27
        {
            width: 49px;
            text-align: center;
        }
        .style28
        {
        }
        .style29
        {
            width: 45px;
        }
        .style30
        {
        }
        
        .style31
        {
        }
        
        .style32
        {
            font-weight: bold;
        }
        .style33
        {
            font-weight: bold;
            }
        
        .style34
        {
            width: 175px;
        }
        
        .style35
        {
        }
        
        .style36
        {
            width: 212px;
        }
        
        .style37
        {
            text-align: left;
            width: 83px;
        }
        
        .style38
        {
            width: 99px;
        }
        .style39
        {
            text-align: left;
            width: 218px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <div id="dipPrintBALetter" class="widthSize100_per">
        
        <table class="style1">
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
            <td class="style3" rowspan="3">
                <table class="style1">
                    <tr>
                        <td class="style7">
                            Standard Chartered</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="floatRight" Font-Bold="True" 
                                Font-Size="Larger" ForeColor="#666666" style="font-weight: 700" Text="Saadiq"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="5">
                <asp:Image ID="Image1" runat="server" Height="66px" 
                    ImageUrl="~/Images/scb_logo.jpg" Width="90%"  CssClass="floatLeft"/>
            </td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style4">
                &nbsp;</td>
        </tr>

    </table> 
    
       <table class="style8">
            <tr>
                <td class="style5">
                    <asp:Label ID="Label2" runat="server" 
                        Text=".........................................................."></asp:Label>
                </td>
                <td class="style5">
                    &nbsp;</td>
                <td class="style10">
                    <asp:Label ID="Label5" runat="server" Text="A/C No"></asp:Label>
                </td>
                <td class="style11">
                    :</td>
                <td>
                    <asp:Label ID="Label39" runat="server" 
                        Text=".........................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    <asp:Label ID="Label37" runat="server" 
                        Text=".........................................................."></asp:Label>
                </td>
                <td class="style5">
                    &nbsp;</td>
                <td class="style10">
                    <asp:Label ID="Label6" runat="server" Text="Branch"></asp:Label>
                </td>
                <td class="style11">
                    :</td>
                <td>
                    <asp:Label ID="Label40" runat="server" 
                        Text=".........................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    <asp:Label ID="Label38" runat="server" 
                        Text=".........................................................."></asp:Label>
                </td>
                <td class="style5">
                    &nbsp;</td>
                <td class="style10">
                    <asp:Label ID="Label7" runat="server" Text="Date"></asp:Label>
                </td>
                <td class="style11">
                    :</td>
                <td>
                    <asp:Label ID="Label41" runat="server" 
                        Text=".........................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    &nbsp;</td>
                <td class="style5">
                    &nbsp;</td>
                <td class="style10">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table> 
        <br/> 
          <table class="style8">
              <tr>
                  <td>
                      SAADIQ AUTO FINANCE BANKING ARRANGEMENT LETTER</td>
              </tr>
              <tr>
                  <td>
                      Dear Sir/Madam,<br />
                      We are pleased to advice that the following facillity that has been granted to 
                      you on the basis of your application dated<br />
                      <asp:Label ID="Label11" runat="server" 
                          Text="....................................................................................................................................................................................................................."></asp:Label>
                  </td>
              </tr>
        </table>
          <table class="reference">
              <tr>
                  <td bgcolor="#999999" class="style12" colspan="4">
                      FACILITY DETAILS</td>
              </tr>
              <tr>
                  <td class="style13">
                      Finance Amount</td>
                  <td class="style14">
                      <asp:Label ID="Label12" runat="server" Text="BDT"></asp:Label>
                  </td>
                  <td class="style20">
                      Rate of Rent</td>
                  <td>
                      <asp:Label ID="Label16" runat="server" Text="Label" CssClass="floatLeft"></asp:Label>
                      <asp:Label ID="Label17" runat="server" Text="% p.a" CssClass="floatRight"></asp:Label>
                  </td>
              </tr>
              <tr>
                  <td class="style13">
                      EMI Amount</td>
                  <td class="style14">
                      <asp:Label ID="Label13" runat="server" Text="BDT"></asp:Label>
                  </td>
                  <td class="style20">
                      No of Installments</td>
                  <td>
                      &nbsp;</td>
              </tr>
              <tr>
                  <td class="style16">
                      Tenor</td>
                  <td class="style17">
                      <asp:Label ID="Label14" runat="server" 
                          Text="............................................."></asp:Label>
                  </td>
                  <td class="style18">
                      Expiry Date</td>
                  <td class="style19">
                      </td>
              </tr>
              <tr>
                  <td class="style13">
                      Processing Fee</td>
                  <td class="style14">
                      <asp:Label ID="Label15" runat="server" Text="BDT"></asp:Label>
                  </td>
                  <td class="style15" colspan="2">
                      &nbsp;</td>
              </tr>
              <tr>
                  <td class="style13" colspan="2">
                      &nbsp;</td>
                  <td class="style15" colspan="2">
                      &nbsp;</td>
              </tr>
              <tr>
                  <td colspan="2">
                      Brand&nbsp;&nbsp;&nbsp;
                      <asp:Label ID="lblBrand" runat="server"></asp:Label>
                  </td>
                  <td class="style15" colspan="2">
                      Model&nbsp;&nbsp;&nbsp;
                      <asp:Label ID="lblModel" runat="server"></asp:Label>
                  </td>
              </tr>
              </table>
          <br />
        <table class="reference">
            <tr>
                <td colspan="5" >
                    You are to execute the following documents before draw down of the facillity:</td>
            </tr>
            <tr>
                <td class="style22" colspan="5">
                    * texttexttexttexttexttexttexttexttexttext</td>
            </tr>
            <tr>
                <td class="style22" colspan="5">
                    * Undertaking to Purchase</td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style23" colspan="4">
                    OP and LetterContinuation for BDT&nbsp;&nbsp;
                    <asp:Label ID="Label18" runat="server" 
                        Text=".............................................................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style30">
                    Letter of Line and Set-off over Deposit A/C No.&nbsp;&nbsp;&nbsp; </td>
                <td class="style29">
                    <asp:Label ID="Label19" runat="server" 
                        Text="......................................................................."></asp:Label>
                </td>
                <td class="style27">
                    &nbsp;for BDT</td>
                <td class="style21">
                    <asp:Label ID="Label20" runat="server" 
                        Text="............................................................"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style25" colspan="2">
                    Memorandum of Deposit over&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label21" runat="server" 
                        Text="......................................................................................................"></asp:Label>
                </td>
                <td class="style27">
                    for BDT</td>
                <td class="style21">
                    <asp:Label ID="Label22" runat="server" 
                        Text="............................................................"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style28" colspan="2">
                    TextTextTextTextTextTextTextT&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label23" runat="server" 
                        Text="......................................................................................................"></asp:Label>
                </td>
                <td class="style27">
                    &nbsp;</td>
                <td class="style21">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style28" colspan="2">
                    TextTextTextTextTextTextTextT&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label24" runat="server" 
                        Text="......................................................................................................"></asp:Label>
                </td>
                <td class="style27">
                    &nbsp;</td>
                <td class="style21">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style28" colspan="4">
                    TextTextTextTextTextTextTextT&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label25" runat="server" 
                        Text=".................................................................................................................................................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style28" colspan="4">
                    TextTextTextTextTextTextTextT&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label26" runat="server" 
                        Text=".................................................................................................................................................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style28" colspan="4">
                    TextTextTextTextTextTextTextT&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label27" runat="server" 
                        Text=".................................................................................................................................................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style30" colspan="4">
                    TextTextTextTextTextTextTextT&nbsp;&nbsp;&nbsp; </td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style30" colspan="4">
                    TextTextTextTextTextTextTextT&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label28" runat="server" 
                        Text=".................................................................................................................................................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style24">
                    *</td>
                <td class="style30" colspan="4">
                    Other&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label29" runat="server" 
                        Text=".................................................................................................................................................................................."></asp:Label>
                    ............................................</td>
            </tr>
            <tr>
                <td class="style24">
                    &nbsp;</td>
                <td class="style30">
                    &nbsp;</td>
                <td class="style29">
                    &nbsp;</td>
                <td class="style27">
                    &nbsp;</td>
                <td class="style21">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style24" colspan="5">
                    The rent may be Changed by the Bank as per the provision of Diminishing 
                    Musharaka Agreement. The details of the
                    <br />
                    terms and conditions of the facility are 
                    TextTextTextTextTextTextTextTextTextTextTextTextTextTextTextTextTextText.</td>
            </tr>
        </table>
          <br />
        <br />
        <table class="reference">
            <tr>
                <td class="style31">
                    ...............................................</td>
                <td class="rightAlign">
                    .......................................</td>
            </tr>
            <tr>
                <td class="style31">
                    Authorised Signature</td>
                <td class="rightAlign">
                    Authorised Signature</td>
            </tr>
            </table>
          <br />
        <table class="reference">
            <tr>
                <td class="style31">
                    ...............................................</td>
                <td class="rightAlign">
                    .......................................</td>
            </tr>
            <tr>
                <td class="style31">
                    Customer&#39;s TextTextTextText(Primary Applicant)</td>
                <td class="rightAlign">
                    Customer&#39;s TextTextTextText(Joint Applicant)</td>
            </tr>
            </table>
            <table class="reference">
            <tr>
                <td class="style31" colspan="4">
                <div>
                    <hr style="width: 100%; position: relative;" />
                    </div>
                </tr>
            <tr>
                <td class="style32" colspan="4">
                    TextTextTextTextTextT&nbsp;</td>
            </tr>
            <tr>
                <td class="style33" colspan="2">
                    TextTextTextTextTextT&nbsp;</td>
                <td class="leftAlign" colspan="2">
                    <asp:Label ID="Label30" runat="server" 
                        Text="................................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style33" colspan="2">
                    TextTextTextTextTextT&nbsp;</td>
                <td class="leftAlign" colspan="2">
                    <asp:Label ID="Label31" runat="server" 
                        Text="................................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style33" colspan="4">
                    TextTextTextTextTextT&nbsp;&nbsp;
                    <asp:Label ID="Label32" runat="server" 
                        Text=".................................."></asp:Label>
                &nbsp;&nbsp;
                    <asp:Label ID="Label33" runat="server" 
                        Text="............"></asp:Label>
                &nbsp; in&nbsp;
                    <asp:Label ID="Label34" runat="server" 
                        Text="............"></asp:Label>
                &nbsp; TextTextTextTextTextT&nbsp;
                    <asp:Label ID="Label35" runat="server" 
                        Text="............"></asp:Label>
                &nbsp;&nbsp; TextTextTextTextTextT</td>
            </tr>
            <tr>
                <td class="style33" colspan="4">
                    TextTextTextTextTextT&nbsp;
                    <asp:Label ID="Label36" runat="server" 
                        
                        Text=".................................................................................................................................................................................."></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style34 rightAlign">
                    Month</td>
                <td class="style33" colspan="2">
                    &nbsp;</td>
                <td class="style33 leftAlign">
                    Year</td>
            </tr>
            </table>
          <br />
        <table class="reference">
            <tr>
                <td class="style38">
                    Signature<br />
                    Primary Applicant</td>
                <td class="style36">
                    <div style="border: 1px solid #000000; height: 50px;" class="widthSize100_per">
                    </div>
                                        </td>
                <td class="style37">
                    Signature<br />
                    Joint Applicant</td>
                <td class="style39">
                    <div style="border: 1px solid #000000; height: 50px;" class="widthSize100_per">
                    </div>
                                        </td>
                <td class="leftAlign">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style38">
                    Date</td>
                <td class="style36">
                    <div style="border: 1px solid #000000; height: 20px;" class="widthSize100_per">
                    </div>
                                        </td>
                <td class="style37">
                    Date</td>
                <td class="style39">
                    <div style="border: 1px solid #000000; height: 20px;" class="widthSize100_per">
                    </div>
                                        </td>
                <td class="leftAlign">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style35" colspan="5">
                    <div>
                    <hr style="width: 99%; position: relative; top: 0px; height: -12px;" />
                    </div></td>
            </tr>
            </table>
        <br />
          <br/>  
    </div>
</asp:Content>

