﻿<%@ Page Language="C#"  AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_ChangePassword" Codebehind="ChangePassword.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password Page</title>
    <style type="text/css">
        .WidthHeight
        {
            width: 500px;  
            height: 230px;          
        }
        .style1
        {
            height: 20px;
        }
    </style>
</head>
<body style="margin-top:0; margin-bottom:0; margin-left:0; margin-right:0;">
    <form id="form1" runat="server">
    <div style="margin-left:50px; margin-top:100px;">
    
    
    
    <center>
    <fieldset class="WidthHeight" style="border:1px;border-color:Black;"> 
        <table class="WidthHeight">
            <tr>
                <td align="left" colspan="3" style="padding-left:160px;">
                    <h1><b>Change Password</b></h1></td>
            </tr>
            <tr>
                <td align="center"  width="350" colspan="3">
                <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right"  width="350">
                                        User Id:&nbsp;</td>
                <td>
                    <asp:TextBox ID="userIdTextBox" runat="server" Width="150px" MaxLength="7"></asp:TextBox>
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    Old Password:&nbsp; </td>
                <td>
                    <asp:TextBox ID="oldPasswordTextBox" runat="server" Width="150px" 
                        TextMode="Password" MaxLength="14"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    New Password:&nbsp; </td>
                <td>
                    <asp:TextBox ID="newPasswordTextBox" runat="server" Width="150px" 
                        TextMode="Password" MaxLength="14"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right" width="250">
                    Confirm Password:&nbsp; </td>
                <td>
                    <asp:TextBox ID="confrimPasswordTextBox" runat="server" Width="150px" 
                        TextMode="Password" MaxLength="14"></asp:TextBox>
                </td>
                <td width="300">
                    <asp:CompareValidator ID="passwordCompareValidator" runat="server" 
                        ErrorMessage="Password does not match" Width="180px" 
                        ControlToCompare="newPasswordTextBox" 
                        ControlToValidate="confrimPasswordTextBox"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    &nbsp;</td>
                <td align="left" colspan="2">
                    <asp:Button ID="submitButton" runat="server" Text="Submit" Width="100px" 
                        onclick="submitButton_Click" />&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="LogInPageLinkButton" runat="server" Font-Bold="True" 
                        onclick="LogInPageLinkButton_Click" Font-Size="14pt">Log In</asp:LinkButton>
                </td>                
            </tr>
            <tr>
                <td align="right" colspan="3">
                    
                </td>
            </tr>
        </table>
        </fieldset>
    </center>   
    
    </div>
    </form>
</body>
</html>
