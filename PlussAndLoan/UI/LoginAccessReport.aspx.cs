﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class LoginAccessReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                startDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
                endDateTextBox.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
        protected void showAuditTrailReport_Click(object sender, EventArgs e)
        {


            if (startDateTextBox.Text == null)
            {
                errMsgLabel.Text = "Please inser Start Date";
            }
            else if (endDateTextBox.Text == null)
            {
                errMsgLabel.Text = "Please inser End Date";
            }
            else
            {
                Session["path"] = "LoginAccess.rdlc";
                var data = new UserManager().GetLoginAccess(Convert.ToDateTime(startDateTextBox.Text), Convert.ToDateTime(endDateTextBox.Text));
                //if (data != null)
                //{
                //    data.Columns.Add("fromDate", typeof(System.DateTime));
                //    data.Columns.Add("toDate", typeof(System.DateTime));
                //    foreach (DataRow row in data.Rows)
                //    {
                //        row["fromDate"] = Convert.ToDateTime(startDateTextBox.Text).ToString("dd-MMMM-yyyy");
                //        row["toDate"] = Convert.ToDateTime(endDateTextBox.Text).ToString("dd-MMMM-yyyy");
                //    }

                //}

                Session["rdlcData"] = data;
                new AT(this).AuditAndTraial("Login Access Report", "Report");
                Response.Redirect("~/ReportRDLC/ReportWizards/RdlcReportViewer.aspx");
            }

        }
    }
}