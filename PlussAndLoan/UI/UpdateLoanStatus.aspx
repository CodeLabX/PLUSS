﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorAdmin.master" AutoEventWireup="true" CodeBehind="UpdateLoanStatus.aspx.cs" Inherits="PlussAndLoan.UI.UpdateLoanStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="divUpdateStatus">
        <table>
            <tr>
                <td colspan="3" align="left" style="padding-left:270px">
                    <asp:Label ID="Label2" runat="server" Text="Update Status Name" Font-Bold="true" Font-Size="18px"></asp:Label>
                </td>
            </tr>
           <tr>
               <td>
                 <asp:Label ID="Label1" runat="server" Text="Status Name"></asp:Label>
               </td>
               <td>
                   <asp:TextBox ID="statusTextBox" runat="server" Width="250px"></asp:TextBox>
                   <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                   <asp:HiddenField ID="idHiddenField" runat="server" />
               </td>
           </tr> 
             <tr>
                <td>
                    &nbsp;</td>
                <td>
                <asp:Button ID="updateButton" runat="server" Text="Update" Width="100px" Height="27px" OnClick="updateButton_Click" 
                        />&nbsp;
               
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div>
    <div id="divViewStatus" style="overflow-y: scroll; height: 380px; width: 100%;" class="GridviewBorder">
         <asp:GridView ID="StatusGridView" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            Font-Size="11px" ForeColor="#333333" PageSize="8"
                            Width="100%" EnableModelValidation="True" GridLines="None" OnSelectedIndexChanged="StatusGridView_SelectedIndexChanged">
                            <RowStyle BackColor="#EFF3FB" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Status Name" HeaderStyle-Width="60%">
                                    <ItemTemplate>
                                        <asp:Label ID="statusLabel" runat="server" Text='<%# Bind("LOAN_STATUS_NAME") %>' Width="70%"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="60%"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Id" HeaderStyle-Width="0%" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="Id" runat="server" Text='<%# Bind("LOAN_STATUS_ID") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="15%"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" Visible="true">
                                    <ItemTemplate>
                                        <asp:LinkButton Text="Update" ID="lnkSelect" runat="server" CommandName="Select"/>
                                    </ItemTemplate>
                                    <HeaderStyle Width="5%" HorizontalAlign="right"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                &nbsp;&nbsp;No data found.
                            </EmptyDataTemplate>
                            <EditRowStyle BackColor="#2461BF" />
                            <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <FooterStyle CssClass="GvFixedFooter" BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
    </div>
</asp:Content>
