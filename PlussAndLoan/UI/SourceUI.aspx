﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_SourceUI" Title="Untitled Page" CodeBehind="SourceUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon.ascx" TagPrefix="uc1" TagName="dhtmlxcommon" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid.ascx" TagPrefix="uc1" TagName="dhtmlxgrid" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_srnd.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_srnd" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgridcell.ascx" TagPrefix="uc1" TagName="dhtmlxgridcell" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgrid_codebase.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_codebase" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgrid_skinscss.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_skinscss" %>



<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="Server">
    <uc1:dhtmlxgrid_codebase runat="server" ID="dhtmlxgrid_codebase" />
    <uc1:dhtmlxgrid_skinscss runat="server" ID="dhtmlxgrid_skinscss" />
    <uc1:dhtmlxcommon runat="server" ID="dhtmlxcommon" />
    <uc1:dhtmlxgrid runat="server" ID="dhtmlxgrid" />
    <uc1:dhtmlxgrid_srnd runat="server" ID="dhtmlxgrid_srnd" />
    <uc1:dhtmlxgridcell runat="server" ID="dhtmlxgridcell" />
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <center>
        <table width="100%" border="0" cellpadding="1" cellspacing="1">
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Text="Soruce Information" Font-Bold="true" Font-Size="18px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 40%">&nbsp;</td>
                <td style="width: 60%">&nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="sourceIdLabel" runat="server" Text="Source Id :"></asp:Label></td>
                <td align="left">
                    <input id="sourceIdText" type="text" readonly="readonly" /></td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="sourceNameLabel" runat="server" Text="Source Name :"></asp:Label>
                </td>
                <td align="left">
                    <input id="sourceNameText" type="text" style="width: 300px;" />
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="regionNameLabel" runat="server" Text="Region Name :"></asp:Label>
                </td>
                <td align="left">
                    <input id="regionNameText" type="text" style="width: 300px;" /></td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="xmlDataHiddenField" runat="server" />
                </td>
                <td align="left">
                    <input id="saveButton" type="button" value="Save" style="width: 90px; height: 25px;" onclick="SaveData()" />
                    <input id="clearButton" type="button" value="Clear" style="width: 90px; height: 25px;" onclick="ClearField()" />
                    <textarea id="str_xml" cols="0" rows="0" style="display: none;"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left" style="padding-left: 50px;">
                    <input id="searchText" type="text" style="width: 200px;" onkeyup="LoadSaveData()" />
                    <input id="searchButton" type="button"
                        value="Go" style="height: 20px;" onclick="LoadSaveData()" /></td>
            </tr>
            <tr>
                <td colspan="2" align="left" style="padding-left: 50px;">
                    <div id="gridbox" style="background-color: white; width: 650px; height: 350px;"></div>
                </td>
            </tr>
        </table>
    </center>
     <div style="display: none" runat="server" id="dvXml"></div>
    <script type="text/javascript">
        var mygrid;
        mygrid = new dhtmlXGridObject('gridbox');
        function doOnLoad() {
            mygrid.setImagePath("../codebase/imgs/");
            mygrid.setHeader("Source Id,Source Name,Region Name");
            mygrid.setInitWidths("80,300,250");
            mygrid.setColAlign("right,left,left");
            mygrid.setColTypes("ro,ro,ro");
            mygrid.setColSorting("int,str,str");
            mygrid.setSkin("light");
            mygrid.init();
            mygrid.enableSmartRendering(true, 20);
            mygrid.attachEvent("onRowSelect", doOnRowSelected);
        }
        function LoadMaxId(sourceMaxId) {
        }
        function LoadGridData() {
            mygrid.clearAll();
            mygrid.parse(document.getElementById('ctl00_ContentPlaceHolder_xmlDataHiddenField').value);
            document.getElementById('ctl00_ContentPlaceHolder_xmlDataHiddenField').value = "";
        }
        function doOnRowSelected(id) {
            if (mygrid.cells(mygrid.getSelectedId(), 0).getValue() > 0) {
                document.getElementById('saveButton').value = "Update";
                document.getElementById('sourceIdText').value = mygrid.cells(mygrid.getSelectedId(), 0).getValue();
                document.getElementById('sourceNameText').value = mygrid.cells(mygrid.getSelectedId(), 1).getValue();
                document.getElementById('regionNameText').value = mygrid.cells(mygrid.getSelectedId(), 2).getValue();
            }
            else {
                document.getElementById('saveButton').value = "Save";
            }
        }
        function SaveData() {
            debugger;
            try {
                var sendData;
                var sourceId = document.getElementById('sourceIdText').value;
                var sourceName = document.getElementById('sourceNameText').value;
                var regionName = document.getElementById('regionNameText').value;
                if (sourceName == "") {
                    alert("Empty Source name");
                    document.getElementById('sourceNameText').focus();
                    return;
                }
                XMLHttpRequestObject = new XMLHttpRequest();
                if (XMLHttpRequestObject) {
                    XMLHttpRequestObject.open("POST", "SourceUI.aspx?operation=save");
                    sendData = sourceId + "#" + sourceName + "#" + regionName;
                    XMLHttpRequestObject.onreadystatechange = function () {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                            var TypeData = new Array();
                            TypeData = XMLHttpRequestObject.responseText.split('#');
                            alert(TypeData[0]);
                            LoadSaveData();
                            ClearField();
                        }
                    }
                    XMLHttpRequestObject.setRequestHeader("Content-type", "multipart/form-data");
                    //XMLHttpRequestObject.setRequestHeader("Content-length", sendData.length);
                    XMLHttpRequestObject.send(sendData);
                }
            } catch (err) {

            }
        }
        function LoadSaveData() {
            debugger;
            try {
                var searchKey = document.getElementById('searchText').value;
                XMLHttpRequestObject = new XMLHttpRequest();
                if (XMLHttpRequestObject) {
                    if (searchKey != null) {
                        XMLHttpRequestObject.open("POST", "SourceUI.aspx?operation=loadData&SEARCHKEY=" + searchKey);
                    }
                    else {
                        XMLHttpRequestObject.open("POST", "SourceUI.aspx?operation=loadData");
                    }
                    XMLHttpRequestObject.onreadystatechange = function () {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                            var TypeData = new Array();
                            TypeData = XMLHttpRequestObject.responseText.split('#');
                            document.getElementById('str_xml').value = TypeData[0];
                            mygrid.clearAll();
                            mygrid.parse(document.getElementById('str_xml').value);
                        }
                    }
                    XMLHttpRequestObject.send(null);
                }
            }
            catch (err)
            { }
        }
        function ClearField() {
            LoadMaxId();
            document.getElementById('sourceNameText').value = "";
            document.getElementById('regionNameText').value = "";
            document.getElementById('searchText').value = "";
            document.getElementById('saveButton').value = "Save";
        }
        function LoadMaxId() {
            try {
                XMLHttpRequestObject = new XMLHttpRequest();
                if (XMLHttpRequestObject) {
                    XMLHttpRequestObject.open("POST", "SourceUI.aspx?operation=GetMaxId");
                    XMLHttpRequestObject.onreadystatechange = function () {
                        if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                            var TypeData = new Array();
                            TypeData = XMLHttpRequestObject.responseText.split('#');
                            document.getElementById('sourceIdText').value = "";
                            document.getElementById('sourceIdText').value = TypeData[0];

                        }
                    }
                    XMLHttpRequestObject.send(null);
                }
            }
            catch (err)
            { }
        }

    </script>
</asp:Content>

