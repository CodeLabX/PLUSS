﻿using System;
using System.Collections.Generic;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class UI_ApprovePLList : System.Web.UI.Page
    {
        public AnalystApproveManager analystApproveManagerObj = null;
        string searchKey = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            analystApproveManagerObj = new AnalystApproveManager();
            if (Request.QueryString.Count != 0)
            {
                searchKey = Convert.ToString(Request.QueryString["SearchKey"]);
            }
            Int32 userId = Convert.ToInt32(Session["Id"].ToString());
            LoadData(userId,searchKey);
        }
        private void LoadData(Int32 UserId,string searchKey)
        {
            int i = 1;
            List<AnalystApprove> analystApproveObjList = new List<AnalystApprove>();
            analystApproveObjList = analystApproveManagerObj.GetAllPL(UserId, searchKey);
            Response.ContentType = "text/xml";
            Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>");
            Response.Write("<rows>");
            try
            {
                if (analystApproveObjList!=null) 
                {
                    foreach (AnalystApprove analystApproveObj in analystApproveObjList)
                    {
                        Response.Write("<row id='" + i.ToString() + "'>");
                        Response.Write("<cell>");
                        Response.Write(i.ToString());
                        Response.Write("</cell>");
                        Response.Write("<cell>");
                        Response.Write(analystApproveObj.PL.PlLlid.ToString());
                        Response.Write("</cell>");

                        if (analystApproveObj.LoanInformation.ApplicantName!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(AddSlash(analystApproveObj.LoanInformation.ApplicantName.ToString()));
                            Response.Write("</cell>"); 
                        }
                        if (analystApproveObj.Product.ProdName!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(AddSlash(analystApproveObj.Product.ProdName.ToString()));
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.LoanType.Name!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.LoanType.Name.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.LoanInformation.AppliedAmount!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.LoanInformation.AppliedAmount.ToString());
                            Response.Write("</cell>");
                        }

                        if (analystApproveObj.PL.PlProposedloanamount!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.PL.PlProposedloanamount.ToString());
                            Response.Write("</cell>"); 
                        }

                        if (analystApproveObj.EntryDate!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.EntryDate.ToString("dd-MM-yyyy"));
                            Response.Write("</cell>");   
                        }
                        if (analystApproveObj.Segment.Name!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.Segment.Name.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.PL.PlNetincome!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.PL.PlNetincome.ToString());
                            Response.Write("</cell>");  
                        }

                        if (analystApproveObj.PL.PlInterestrat!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.PL.PlInterestrat.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.PL.PlTenure!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.PL.PlTenure.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.PL.PlEmi!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.PL.PlEmi.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.PL.PlDbr!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.PL.PlDbr.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.PL.PlMue!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.PL.PlMue.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.PL.PlLevel!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.PL.PlLevel.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.LoanScorePoint.TotalPoint!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.LoanScorePoint.TotalPoint.ToString());
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.LoanScorePoint.Result!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.LoanScorePoint.Result.ToString());
                            Response.Write("</cell>"); 
                        }
                        if (analystApproveObj.User.Name!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(AddSlash(analystApproveObj.User.Name.ToString()));
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.PL.PlRemarks!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(AddSlash(analystApproveObj.PL.PlRemarks.ToString()));
                            Response.Write("</cell>");
                        }
                        if (analystApproveObj.Product.ProdType!=null)
                        {
                            Response.Write("<cell>");
                            Response.Write(analystApproveObj.Product.ProdType.ToString());
                            Response.Write("</cell>");
                        }
                   
                        Response.Write("</row>");
                        i = i + 1;
                    }  
                }

           
            
            }
            finally
            {
            }
            Response.Write("</rows>");
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0 && sMessage != "")
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
    }
}
