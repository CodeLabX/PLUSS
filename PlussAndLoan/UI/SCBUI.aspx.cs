﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_SCBUI : System.Web.UI.Page
    {
        #region attributes
        List<string> stringList = new List<string>();
        private ScbManager scbManagerObj;
        private ScbMaster scbMasterObj;
        private ScbDetails scbDetailsObj;
        private List<ScbDetails> scbDetailsObjList;
        private List<ScbMaster> scbMasterObjList;

        public int totalFor12Month = 0;
        public int averageFor12Month = 0;
        public int percent50 = 0;
        public int totalFor6Month = 0;
        public int averageFor6Month = 0;
        public int percent50For6Month = 0;    

        public int totalCtoFor12Month = 0;
        public int averageCtoFor12Month = 0;
        public int totalCtoFor6Month = 0;
        public int averageCtoFor6Month = 0;

        public int divisor12 = 0;
        public int divisor6 = 0;
        #endregion
        //protected static readonly ILog log = LogManager.GetLogger(typeof(UI_SCBUI));
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);

            int maxLengthFirstTextBox = 2;
            int maxLengthSecondTextBox = 7;
            //log4net.Config.XmlConfigurator.Configure();
            //log4net.Config.BasicConfigurator.Configure();
            acNoTextBox1.Attributes.Add("onkeyup", "GetKeyPress('" + acNoTextBox1.ClientID + "','" + acNoTextBox2.ClientID + "','" + maxLengthFirstTextBox + "')");
            acNoTextBox2.Attributes.Add("onkeyup", "GetKeyPress('" + acNoTextBox2.ClientID + "','" + acNoTextBox3.ClientID + "','" + maxLengthSecondTextBox + "')");


            int i = 0;
            for (i = 0; i < 12; i++)
            {
                stringList.Add(i.ToString());
            }
            if (!IsPostBack)
            {
                SCBGridView.DataSource = stringList;
                SCBGridView.DataBind();
            }

            browseButton.Attributes.Add("onclick", "getSelText()");
            foreach (GridViewRow gvr in SCBGridView.Rows)
            {
                (gvr.FindControl("adjustmentTextBox") as TextBox).Attributes.Add("onkeyup", "GetAdjustmentTextBoxKeyPress('" + (gvr.FindControl("creditTurnoverLabel") as Label).ClientID + "','" + (gvr.FindControl("adjustmentTextBox") as TextBox).ClientID + "','" + (gvr.FindControl("adjustedCTOLabel") as Label).ClientID + "')");
            }
        }

        protected void browseButton_Click(object sender, EventArgs e)
        {
            divisor12 = 0;
            divisor6 = 0;
            int returnStatus = 0;
            string initialString = null;
            try
            {
                if (hiddenField.Value != "")
                {
                    initialString = hiddenField.Value;
                    if (initialString.Contains("\t\r\n"))
                    {
                        initialString = initialString.Replace("\t", "");
                        returnStatus = PasteData(initialString, " ");
                    }
                    else if (initialString.Contains(" \r\n"))
                    {
                        returnStatus = PasteData(initialString, " ");
                    }
                }
                else
                {
                    Alert.Show("Can not copy data from your file !");
                    //log.Debug("Can not copy data from your file !");
                    LOG.GenericError("Can not copy data from your file !");
                }
            }
            catch (Exception ex)
            {
                Alert.Show("Invalid data copy");
                //log.Debug(ex.Message.ToString());
                LOG.GenericError(ex.Message.ToString());
            }

            /*------------------------------ calculation part -------------------------------------*/
            if (returnStatus > 0)
            {
                foreach (GridViewRow gvr in SCBGridView.Rows)
                {
                    try
                    {
                        if (gvr.RowIndex > 5)
                        {
                            divisor12++;
                            totalFor6Month += Convert.ToInt32(Convert.ToDouble((gvr.FindControl("averageLedgerBalanceLabel") as Label).Text));
                        }
                        else
                        {
                            divisor6++;
                            totalFor12Month += Convert.ToInt32(Convert.ToDouble((gvr.FindControl("averageLedgerBalanceLabel") as Label).Text));
                        }
                    }
                    catch(Exception ex)
                    {
                        Alert.Show("Wrong data to Paste !\nSelect some valid data");
                        //log.Debug(ex.Message.ToString());
                        LOG.GenericError(ex.Message.ToString());
                    }
                }
                totalFor12Month = totalFor6Month + totalFor12Month;
                if (divisor12 + divisor6 != 0)
                {
                    averageFor12Month = totalFor12Month / (divisor12 + divisor6);
                }
                percent50 = averageFor12Month / 2;
                if (divisor12 != 0)
                {
                    averageFor6Month = totalFor6Month / divisor12;
                }
                percent50For6Month = averageFor6Month / 2;

                divisor12 = 0;
                divisor6 = 0;
                foreach (GridViewRow gvr in SCBGridView.Rows)
                {
                    try
                    {
                        if (gvr.RowIndex > 5)
                        {
                            divisor12++;
                            totalCtoFor6Month += Convert.ToInt32(Convert.ToDouble((gvr.FindControl("adjustedCTOLabel") as Label).Text));
                        }
                        else
                        {
                            divisor6++;
                            totalCtoFor12Month += Convert.ToInt32(Convert.ToDouble((gvr.FindControl("adjustedCTOLabel") as Label).Text));
                        }
                    }
                    catch(Exception ex)
                    {
                        Alert.Show("Wrong data to Paste !\nSelect some valid data");
                        //log.Debug(ex.Message.ToString());
                        LOG.GenericError(ex.Message.ToString());
                    }
                }
                totalCtoFor12Month = totalCtoFor6Month + totalCtoFor12Month;
                if (divisor12 + divisor6 != 0)
                {
                    averageCtoFor12Month = totalCtoFor12Month / (divisor12 + divisor6);
                }
                if (divisor12 != 0)
                {
                    averageCtoFor6Month = totalCtoFor6Month / divisor12;
                }
                /*-------------------- end of calculation part ---------------------------------------*/


                total12ValueLabel.Text = totalFor12Month.ToString("##,###");
                totalValue6Label.Text = totalFor6Month.ToString("##,###");
                totalValueCto12Label.Text = totalCtoFor12Month.ToString("##,###");
                totalvalueCto6Label.Text = totalCtoFor6Month.ToString("##,###");

                average12ValueLabel.Text = averageFor12Month.ToString("##,###");
                averageValue6Label.Text = averageFor6Month.ToString("##,###");
                averageValueCto12Label.Text = averageCtoFor12Month.ToString("##,###");
                averageValueCto6Label.Text = averageCtoFor6Month.ToString("##,###");

                parcentValue12Label.Text = percent50.ToString("##,###");
                percentValue6Label.Text = percent50For6Month.ToString("##,###");
            }
            else
            {
                Alert.Show("Wrong data to Paste! Select some valid data");
                //log.Debug("Wrong data to Paste! Select some valid data");
                LOG.GenericError("Wrong data to Paste! Select some valid data");
            }
        }
        private int PasteData(string initialString, string splitString)
        {
            try
            {
                string AccountNoKeyString = "Account No";
                string NameKeyString = "Name";
                string CreditTurnoverKeyString = "Credit Turnover";

                string countNoData = CuttingBeforeKey(initialString, AccountNoKeyString);
                string nameData = CuttingBeforeKey(initialString, NameKeyString);
                string gridData = CuttingBeforeKey(initialString, CreditTurnoverKeyString);

                string accountNumber = GiveAccountNumber(countNoData, splitString);
                if (accountNumber.Length > 9)
                {
                    acNoTextBox1.Text = accountNumber.Substring(0, 2);
                    acNoTextBox2.Text = accountNumber.Substring(3, 7);
                    acNoTextBox3.Text = accountNumber.Substring(11, 2);
                }
                string name = GiveName(nameData, splitString);
                if (name.Length > 0)
                {
                    acNameTextBox.Text = name;
                }
                gridData = gridData.Replace("-  ", "0.000\r\n");
                gridData = gridData.Replace(" - ", "-");
                if (gridData.Length > 0)
                {
                    List<string> showList = new List<string>();
                    List<string> aList = new List<string>();
                    string[] a = { "\r\n" };
                    string[] gridRowData = gridData.Split(a, StringSplitOptions.RemoveEmptyEntries);
                    string[] cellData = new string[gridData.Length];
                    for (int i = 0; i < gridRowData.Length; i++)
                    {
                        showList.Add(gridRowData[i].ToString());
                    }
                    foreach (string tempRow in gridRowData)
                    {
                        string[] cellSplit = { splitString };
                        cellData = tempRow.Split(cellSplit, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < cellData.Length; i++)
                        {
                            aList.Add(cellData[i].Replace("\\r\\n", "").ToString());
                        }
                    }

                    int x = 0;
                    foreach (GridViewRow gvr in SCBGridView.Rows)
                    {
                        if (x < aList.Count)
                        {
                            (gvr.FindControl("periodLabel") as Label).Text = aList[x].ToString();
                            (gvr.FindControl("monthEndLedgerBalLabel") as Label).Text = aList[x + 1].ToString();
                            (gvr.FindControl("maxLedgerBalanceLabel") as Label).Text = aList[x + 2].ToString();
                            (gvr.FindControl("minLedgerBalanceLabel") as Label).Text = aList[x + 3].ToString();
                            (gvr.FindControl("averageLedgerBalanceLabel") as Label).Text = aList[x + 4].ToString();
                            (gvr.FindControl("creditTransactionCountLabel") as Label).Text = aList[x + 6].ToString();
                            (gvr.FindControl("totalTranscationCountLabel") as Label).Text = aList[x + 7].ToString();
                            (gvr.FindControl("debitTurnoverLabel") as Label).Text = aList[x + 8].ToString();
                            (gvr.FindControl("creditTurnoverLabel") as Label).Text = aList[x + 9].ToString();
                            (gvr.FindControl("adjustedCTOLabel") as Label).Text = (gvr.FindControl("adjustedCTOLabel") as Label).Text = Convert.ToString(Convert.ToDouble((gvr.FindControl("creditTurnoverLabel") as Label).Text) - Convert.ToDouble((gvr.FindControl("adjustmentTextBox") as TextBox).Text)); ;
                            x = x + 10;
                        }
                    }                
                }
                return 1;
            }
            catch (Exception ex)
            {
                return -1;
                //log.Debug(ex.Message.ToString());
                LOG.GenericError(ex.Message.ToString());
            }
        }

        private string GiveName(string newString2, string splitString)
        {
            string[] splitter = { splitString+"\r\n" };
            string[] temString2 = newString2.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            return temString2[0];
        }

        private string GiveAccountNumber(string newString1, string splitString)
        {
            string[] splitter = { splitString };
            string[] temString1 = newString1.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            return temString1[0];
        }

        private string CuttingBeforeKey(string mainString, string keyString1)
        {
            int firstCuttingIndex = 0;
            firstCuttingIndex = mainString.LastIndexOf(keyString1);
            string temString1 = mainString.Substring(firstCuttingIndex, mainString.Length - firstCuttingIndex);
            temString1 = temString1.Substring(keyString1.Length);
            return temString1;
        }
        private string KeyData(string mainString, string keyString)
        {
            string[] returnData=null;
            string[] splitter = { keyString };
            if(mainString.Length>0)
            {
                returnData = mainString.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            }
            return returnData[1].ToString();
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (llidTextBox.Text != "")
                {
                    scbManagerObj = new ScbManager();
                    scbMasterObj = new ScbMaster();
                    scbDetailsObjList = new List<ScbDetails>();


                    scbMasterObj.scbmLlid = llidTextBox.Text;
                    scbMasterObj.scbmAcno = acNoTextBox1.Text + acNoTextBox2.Text + acNoTextBox3.Text;
                    scbMasterObj.scbmAcname = acNameTextBox.Text;

                    scbMasterObj.scbmLedgertotal = Convert.ToDouble(total12ValueLabel.Text);
                    scbMasterObj.scbmLedgeraverage = Convert.ToDouble(average12ValueLabel.Text);
                    scbMasterObj.scbmLedger5oper = Convert.ToDouble(parcentValue12Label.Text);

                    scbMasterObj.scbmLedger6monthtotal = Convert.ToDouble(totalValue6Label.Text);
                    scbMasterObj.scbmLedger6monthaverage = Convert.ToDouble(averageValue6Label.Text);
                    scbMasterObj.scbmLedger6month5oper = Convert.ToDouble(percentValue6Label.Text);

                    scbMasterObj.scbmAdjustedctototal = Convert.ToDouble(totalValueCto12Label.Text);
                    scbMasterObj.scbmAdjustedctoaverage = Convert.ToDouble(averageValueCto12Label.Text);

                    scbMasterObj.scbmAdjustedcto6monthtotal = Convert.ToDouble(totalvalueCto6Label.Text);
                    scbMasterObj.scbmAdjustedcto6monthaverage = Convert.ToDouble(averageValueCto6Label.Text);

                    scbMasterObj.scbmEntrydate = DateTime.Now;
                    scbMasterObj.scbmUserID = 1;

                    foreach (GridViewRow gvr in SCBGridView.Rows)
                    {
                        if ((gvr.FindControl("periodLabel") as Label).Text != "")
                        {
                            scbDetailsObj = new ScbDetails();
                            string s = Convert.ToDateTime("01" + "-" + (gvr.FindControl("periodLabel") as Label).Text).ToString();

                            scbDetailsObj.scbdPeriod = Convert.ToDateTime("01" + "-" + (gvr.FindControl("periodLabel") as Label).Text);
                            scbDetailsObj.scbdMonthendledgerbalance = Convert.ToDouble((gvr.FindControl("monthEndLedgerBalLabel") as Label).Text);
                            scbDetailsObj.scbdMaxledgerbalance = Convert.ToDouble((gvr.FindControl("maxLedgerBalanceLabel") as Label).Text);
                            scbDetailsObj.scbdMinledgerbalance = Convert.ToDouble((gvr.FindControl("minLedgerBalanceLabel") as Label).Text);
                            scbDetailsObj.scbdAverageledgerbalance = Convert.ToDouble((gvr.FindControl("averageLedgerBalanceLabel") as Label).Text);
                            scbDetailsObj.scbdAdjmontaveragebalance = Convert.ToInt32((gvr.FindControl("adjmonthAvgBalDropdownList") as DropDownList).SelectedValue);
                            scbDetailsObj.scbdCredittransactioncount = Convert.ToDouble((gvr.FindControl("creditTransactionCountLabel") as Label).Text);
                            scbDetailsObj.scbdTotaltransactioncount = Convert.ToDouble((gvr.FindControl("totalTranscationCountLabel") as Label).Text);
                            scbDetailsObj.scbdDebitturnover = Convert.ToDouble((gvr.FindControl("debitTurnoverLabel") as Label).Text);
                            scbDetailsObj.scbdCreditturnover = Convert.ToDouble((gvr.FindControl("creditTurnoverLabel") as Label).Text);
                            scbDetailsObj.scbdAdjustment = Convert.ToDouble((gvr.FindControl("adjustmentTextBox") as TextBox).Text);
                            scbDetailsObj.scbdAdjustmentcto = Convert.ToDouble((gvr.FindControl("adjustedCTOLabel") as Label).Text);
                            scbDetailsObj.scbdAdjmonthcto = Convert.ToInt32((gvr.FindControl("adjmonthCTODropDownList") as DropDownList).SelectedValue);
                            scbDetailsObjList.Add(scbDetailsObj);
                        }
                    }

                    try
                    {
                        scbMasterObjList = scbManagerObj.GetAllScbMasterInfoByLlidAndAccountNumber(llidTextBox.Text, acNoTextBox1.Text + acNoTextBox2.Text + acNoTextBox3.Text);

                        if (scbMasterObjList.Count > 0)
                        {
                            // TODO: Delete or update method call for both ScbMaster and ScbDetails table
                            // now calling delete, so have to insert again.
                            //if calling update then dont have insert again like below
                            int detailsDeleteInfo = scbManagerObj.DeleteScbDetailsInfo(scbMasterObjList[0].scbmID);
                            int masterDeleteInfo = scbManagerObj.DeleteScbMasterInfo(scbMasterObjList[0].scbmID);

                            int masterInsertInfo = scbManagerObj.InsertScbMasterInfo(scbMasterObj);
                            if (masterInsertInfo == 1)
                            {        // TODO: insert each row in data base of the SCBGridView         
                                int detailsInsertInfo = scbManagerObj.InsertScbDetailsInfo(scbDetailsObjList);
                                if (detailsInsertInfo == 1)
                                {
                                    Alert.Show("Save Successful");

                                }
                                else
                                {
                                    Alert.Show("Save Un-Successful");
                            
                                }
                            }
                            else
                            {
                                // TODO: Delete Last Inserted Master Info
                            }
                        }
                        else
                        {
                            int masterInsertInfo = scbManagerObj.InsertScbMasterInfo(scbMasterObj);
                            if (masterInsertInfo == 1)
                            {        // TODO: insert each row in data base of the SCBGridView         
                                int detailsInsertInfo = scbManagerObj.InsertScbDetailsInfo(scbDetailsObjList);
                                if (detailsInsertInfo == 1)
                                { Alert.Show("Save Successful"); }
                                else { Alert.Show("Save Un-Successful"); }
                            }
                            else
                            {
                                // TODO: Delete Last Inserted Master Info
                            }
                        }
                    }
                    catch { Alert.Show("Database Error !"); }
                }
                else { Alert.Show("Give an Id"); }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "STATISTICAL HISTORY SAVE");
            }
        }

        protected void clearAllButton_Click(object sender, EventArgs e)
        {
            this.ClearAllFields();
            this.ClearAcNumberDropDownList();
            this.ClearTotalLabels();
            this.ClearCtoTotalLabels();
        }

        private void ClearAcNumberDropDownList()
        {
            acNumberDropDownList.Items.Clear();
        }

        private void ClearAllFields()
        {
            //llidTextBox.Text = "";
            acNameTextBox.Text = "";
            acNoTextBox1.Text = "";
            acNoTextBox2.Text = "";
            acNoTextBox3.Text = "";

            SCBGridView.DataSource = stringList;
            SCBGridView.DataBind();
        }

        protected void printButton_Click(object sender, EventArgs e)
        {
            string script = "<script type=text/javascript>window.print();</script>";

            Page page = (Page)HttpContext.Current.CurrentHandler;

            page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "script", script);
        }

        protected void adjmonthAvgBalDropdownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            divisor12 = 0;
            divisor6 = 0;
            foreach (GridViewRow gvr in SCBGridView.Rows)
            {
                if ((gvr.FindControl("periodLabel") as Label).Text != "")
                {
                    if ((gvr.FindControl("adjmonthAvgBalDropdownList") as DropDownList).SelectedValue == "0")
                    {
                        if (gvr.RowIndex > 5)
                        {
                            divisor12++;    
                            totalFor6Month += Convert.ToInt32(Convert.ToDouble((gvr.FindControl("averageLedgerBalanceLabel") as Label).Text));
                        }
                        else
                        {
                            divisor6++;    
                            totalFor12Month += Convert.ToInt32(Convert.ToDouble((gvr.FindControl("averageLedgerBalanceLabel") as Label).Text));
                        }
                    }
                    else
                    {
                        // this data will nor Be added
                    }
                }
            }
            totalFor12Month = totalFor6Month + totalFor12Month;
            if (divisor12 + divisor6 != 0)
            {
                averageFor12Month = totalFor12Month / (divisor12 + divisor6);
            }
            percent50 = averageFor12Month / 2;
            if (divisor12 != 0)
            {
                averageFor6Month = totalFor6Month / divisor12;
            }
            percent50For6Month = averageFor6Month / 2;


            total12ValueLabel.Text = totalFor12Month.ToString();
            average12ValueLabel.Text = averageFor12Month.ToString();
            parcentValue12Label.Text = percent50.ToString();

            totalValue6Label.Text = totalFor6Month.ToString();
            averageValue6Label.Text = averageFor6Month.ToString();
            percentValue6Label.Text = percent50For6Month.ToString();
        }

        protected void adjmonthCTODropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            divisor12 = 0;
            divisor6 = 0;

            this.ClearCtoTotalLabels();

            foreach (GridViewRow gvr in SCBGridView.Rows)
            {
                if ((gvr.FindControl("periodLabel") as Label).Text != "")
                {
                    if ((gvr.FindControl("adjmonthCTODropDownList") as DropDownList).SelectedValue == "0")
                    {
                        try
                        {
                            if (gvr.RowIndex > 5)
                            {
                                divisor12++;    
                                totalCtoFor6Month += Convert.ToInt32(Convert.ToDouble((gvr.FindControl("adjustedCTOLabel") as Label).Text)); 
                            }
                            else
                            {
                                divisor6++;    
                                totalCtoFor12Month += Convert.ToInt32(Convert.ToDouble((gvr.FindControl("adjustedCTOLabel") as Label).Text)); 
                            }
                        }
                        catch { }
                    }
                    else
                    {
                    }
                }
            }
            totalCtoFor12Month = totalCtoFor6Month + totalCtoFor12Month;
            if (divisor12 + divisor6 != 0)
            {
                averageCtoFor12Month = totalCtoFor12Month / (divisor12 + divisor6);
            }
            if (divisor12 != 0)
            {
                averageCtoFor6Month = totalCtoFor6Month / divisor12;
            }
        


            totalvalueCto6Label.Text = totalCtoFor6Month.ToString();
            averageValueCto12Label.Text = averageCtoFor12Month.ToString();

            totalValueCto12Label.Text = totalCtoFor12Month.ToString();
            averageValueCto6Label.Text = averageCtoFor6Month.ToString();
        }

        protected void adjustmentTextBox_TextChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in SCBGridView.Rows)
            {
                if ((gvr.FindControl("adjustmentTextBox") as TextBox).Text == "")
                {
                    (gvr.FindControl("adjustmentTextBox") as TextBox).Text = "0";
                }
            
                try
                {
                    if ((gvr.FindControl("creditTurnoverLabel") as Label).Text != "")
                    {
                        (gvr.FindControl("adjustedCTOLabel") as Label).Text = Convert.ToString(Convert.ToDouble((gvr.FindControl("creditTurnoverLabel") as Label).Text) - Convert.ToDouble((gvr.FindControl("adjustmentTextBox") as TextBox).Text));
                    }
                }
                catch { }
            }

            this.adjmonthCTODropDownList_SelectedIndexChanged(sender, e);
        }

        private void ClearTotalLabels()
        {
            total12ValueLabel.Text = "";
            totalValue6Label.Text = "";
            average12ValueLabel.Text = "";
            averageValue6Label.Text = "";
            parcentValue12Label.Text = "";
            percentValue6Label.Text = "";
        }
    
        private void ClearCtoTotalLabels()
        {
            totalvalueCto6Label.Text = "";
            averageValueCto12Label.Text = "";

            totalValueCto12Label.Text = "";
            averageValueCto6Label.Text = "";
        }

        protected void findButton_Click(object sender, EventArgs e)
        {
            ClearAllFields();
            if (llidTextBox.Text != "")
            {
                scbManagerObj = new ScbManager();
                List<string> accountList = scbManagerObj.GetAllAccountNumberByLlid(llidTextBox.Text);

                this.ClearAcNumberDropDownList();

                foreach (string s in accountList)
                {
                    acNumberDropDownList.Items.Add(s);
                }
                this.acNumberDropDownList_SelectedIndexChanged(sender, e);
            }
            else
            {
                Alert.Show("Please insert valid LLId");
            }
        }

        private void LoadScbMaster()
        {
            scbManagerObj = new ScbManager();

            List<ScbMaster> scbMasterObjList = new List<ScbMaster>();
            ScbMaster scbMasterObj = new ScbMaster();

            string mId = "";

            scbMasterObjList = scbManagerObj.GetAllScbMasterInfo();
            foreach (ScbMaster scbMaObj in scbMasterObjList)
            {
                if (scbMaObj.scbmLlid == llidTextBox.Text && scbMaObj.scbmAcno == acNumberDropDownList.SelectedItem.Text)
                {
                    //hiddenField.Value = scbMaObj.scbmID.ToString();
                    acNameTextBox.Text = scbMaObj.scbmAcname;
                    acNoTextBox1.Text = scbMaObj.scbmAcno.Substring(0, 2);
                    acNoTextBox2.Text = scbMaObj.scbmAcno.Substring(2, 7);
                    acNoTextBox3.Text = scbMaObj.scbmAcno.Substring(9, 2);

                    total12ValueLabel.Text = scbMaObj.scbmLedgertotal.ToString("##,###");
                    average12ValueLabel.Text = scbMaObj.scbmLedgeraverage.ToString("##,###");
                    parcentValue12Label.Text = scbMaObj.scbmLedger5oper.ToString("##,###");

                    totalValue6Label.Text = scbMaObj.scbmLedger6monthtotal.ToString("##,###");
                    averageValue6Label.Text = scbMaObj.scbmLedger6monthaverage.ToString("##,###");
                    percentValue6Label.Text = scbMaObj.scbmLedger6month5oper.ToString("##,###");

                    totalValueCto12Label.Text = scbMaObj.scbmAdjustedctototal.ToString("##,###");
                    averageValueCto12Label.Text = scbMaObj.scbmAdjustedctoaverage.ToString("##,###");

                    totalvalueCto6Label.Text = scbMaObj.scbmAdjustedcto6monthtotal.ToString("##,###");
                    averageValueCto6Label.Text = scbMaObj.scbmAdjustedcto6monthaverage.ToString("##,###");


                    mId = scbMaObj.scbmID.ToString();
                    break;
                }
            }
            LoadScbDetails(mId);

        }    

        public void LoadScbDetails(string mId)
        {
            scbManagerObj = new ScbManager();
            List<ScbDetails> scbDetailsObjList = new List<ScbDetails>();
            List<ScbDetails> tempScbDetailsObjList = new List<ScbDetails>();
            ScbDetails scbDetailsObj = new ScbDetails();

            scbDetailsObjList = scbManagerObj.GetAllScbDetailsInfo();
            foreach (ScbDetails scbDeObj in scbDetailsObjList)
            {
                if (scbDeObj.scbdScbmasterID.ToString() == mId)
                {
                    tempScbDetailsObjList.Add(scbDeObj);
                }
            }
            //tempScbDetailsObjList.Sort(delegate(ScbDetails d1, ScbDetails d2) { return d1.scbdPeriod.CompareTo(d2.scbdPeriod); });
            tempScbDetailsObjList.Reverse();
            int i = tempScbDetailsObjList.Count - 1;
            foreach (ScbDetails scbDeObj in tempScbDetailsObjList)
            {
                (SCBGridView.Rows[i].Cells[0].FindControl("periodLabel") as Label).Text= Convert.ToString(scbDeObj.scbdPeriod.Value.ToString("MMM-yy"));
                (SCBGridView.Rows[i].Cells[1].FindControl("monthEndLedgerBalLabel") as Label).Text = Convert.ToString(scbDeObj.scbdMonthendledgerbalance);
                (SCBGridView.Rows[i].Cells[2].FindControl("maxLedgerBalanceLabel") as Label).Text = Convert.ToString(scbDeObj.scbdMaxledgerbalance);
                (SCBGridView.Rows[i].Cells[3].FindControl("minLedgerBalanceLabel") as Label).Text = Convert.ToString(scbDeObj.scbdMinledgerbalance);
                (SCBGridView.Rows[i].Cells[4].FindControl("averageLedgerBalanceLabel") as Label).Text = Convert.ToString(scbDeObj.scbdAverageledgerbalance);
            
                (SCBGridView.Rows[i].Cells[5].FindControl("adjmonthAvgBalDropdownList") as DropDownList).SelectedValue = Convert.ToString(scbDeObj.scbdAdjmontaveragebalance);
                (SCBGridView.Rows[i].Cells[6].FindControl("creditTransactionCountLabel") as Label).Text = Convert.ToString(scbDeObj.scbdCredittransactioncount);
                (SCBGridView.Rows[i].Cells[7].FindControl("totalTranscationCountLabel") as Label).Text = Convert.ToString(scbDeObj.scbdTotaltransactioncount);
                (SCBGridView.Rows[i].Cells[8].FindControl("debitTurnoverLabel") as Label).Text = Convert.ToString(scbDeObj.scbdDebitturnover);
                (SCBGridView.Rows[i].Cells[9].FindControl("creditTurnoverLabel") as Label).Text = Convert.ToString(scbDeObj.scbdCreditturnover);
            
                (SCBGridView.Rows[i].Cells[10].FindControl("adjustmentTextBox") as TextBox).Text = Convert.ToString(scbDeObj.scbdAdjustment);
                (SCBGridView.Rows[i].Cells[11].FindControl("adjustedCTOLabel") as Label).Text = Convert.ToString(scbDeObj.scbdAdjustmentcto);
            
                (SCBGridView.Rows[i].Cells[12].FindControl("adjmonthCTODropDownList") as DropDownList).SelectedValue = Convert.ToString(scbDeObj.scbdAdjmonthcto);
                i--;
            }
        }

        protected void acNumberDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadScbMaster();
        }
    }
}