﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_SectorUI" Title="Sector" Codebehind="SectorUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/dhtmlxgridcss.ascx" TagPrefix="uc1" TagName="dhtmlxgridcss" %>
<%@ Register Src="~/UI/UserControls/css/dhtmlxgrid_skinscss.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_skinscss" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxcommon.ascx" TagPrefix="uc1" TagName="dhtmlxcommon" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid.ascx" TagPrefix="uc1" TagName="dhtmlxgrid" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgrid_srnd.ascx" TagPrefix="uc1" TagName="dhtmlxgrid_srnd" %>
<%@ Register Src="~/UI/UserControls/js/dhtmlxgridcell.ascx" TagPrefix="uc1" TagName="dhtmlxgridcell" %>


<asp:Content ID="Content1" runat="server" contentplaceholderid="head">
    <uc1:dhtmlxgridcss runat="server" ID="dhtmlxgridcss" />
    <uc1:dhtmlxgrid_skinscss runat="server" ID="dhtmlxgrid_skinscss" />
    <uc1:dhtmlxcommon runat="server" ID="dhtmlxcommon" />
    <uc1:dhtmlxgrid runat="server" ID="dhtmlxgrid" />
    <uc1:dhtmlxgrid_srnd runat="server" ID="dhtmlxgrid_srnd" />
    <uc1:dhtmlxgridcell runat="server" ID="dhtmlxgridcell" />
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="sectorInfo" style="padding-left:100px;">
    <table>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="titleLabel" runat="server" Text="Sector Information" Font-Bold="true" Font-Size="18px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label1" runat="server" Text="Sector Id :"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="sectorIdTextBox" runat="server" Width="220px" ReadOnly="true"></asp:TextBox>
                <asp:HiddenField ID="sectorIdHidden" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label2" runat="server" Text="Sector Name : "></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="sectorNameTextBox" runat="server" Width="220px"></asp:TextBox>
                <asp:Label ID="Label" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label3" runat="server" Text="Sector Status : "></asp:Label>
            </td>
            <td align="left">
                <asp:DropDownList ID="sectrorStatusDropDownList" runat="server" Width="150px">
                    <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>Inactive</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                <asp:Button ID="saveButton" runat="server" Width="100px" Height="27px" Text="Save" 
                    onclick="saveButton_Click" />&nbsp;&nbsp;
                <asp:Button ID="clearButton" runat="server" Width="100px" Height="27px" Text="Clear" 
                    onclick="clearButton_Click" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" colspan="2">
              <asp:TextBox ID="searchTextBox" runat="server" Width="150px"></asp:TextBox>
                      <asp:Button ID="searchButton" runat="server" Width="100px" Height="24px" Text="Search" 
                    onclick="searchButton_Click" />
               </td>
        </tr>
        <tr>
            <td colspan="2">                
                <div id="gridbox"  style="background-color:white; width:500px; height:335px;"></div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
         <div style="display: none" runat="server" id="dvXml"></div>
</div>
    <script type="text/javascript">
	var mygrid;
	mygrid = new dhtmlXGridObject('gridbox');
	mygrid.setImagePath("../codebase/imgs/");
	mygrid.setHeader("Sector Id,Sector Name,Status");
	mygrid.setInitWidths("65,335,80");
	mygrid.setColAlign("right,left,left");
	mygrid.setColTypes("ro,ro,ro");
	mygrid.setColSorting("int,str,str");
	//mygrid.setSkin("modern");
	mygrid.setSkin("light");	 
	mygrid.init();
	mygrid.enableSmartRendering(true,20);
	mygrid.parse(document.getElementById("xml_data"));
	mygrid.attachEvent("onRowSelect",doOnRowSelected);
    function doOnRowSelected(id)
    {
        if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0)
        {
          document.getElementById('ctl00_ContentPlaceHolder_saveButton').value="Update";
          document.getElementById('ctl00_ContentPlaceHolder_sectorIdHidden').value = mygrid.cells(mygrid.getSelectedId(),0).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_sectorIdTextBox').value = mygrid.cells(mygrid.getSelectedId(),0).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_sectorNameTextBox').value = mygrid.cells(mygrid.getSelectedId(),1).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_sectrorStatusDropDownList').value=mygrid.cells(mygrid.getSelectedId(),2).getValue();
         }
         else
         {
          document.getElementById('ctl00_ContentPlaceHolder_saveButton').value="Save";
         }
    }
	</script>
</asp:Content>


