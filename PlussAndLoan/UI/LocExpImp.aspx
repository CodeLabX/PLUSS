﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorOperationMaster.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_LocExpImp" Codebehind="LocExpImp.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style type="text/css">
        A {
            COLOR: #333333;
            TEXT-DECORATION: none;
        }

            A:hover {
                TEXT-DECORATION: underline;
            }

        .LabelHead {
            font-size: 14px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .LabelSubHead {
            font-size: 12px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .TableHead {
            font-size: 16px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .label {
            font-size: 18px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .tableWidth {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }

            .tableWidth td {
                border-width: 1px;
                border-style: solid;
                border-collapse: collapse;
                border-color: Black;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
     <table align="center" width="780" border="0" cellpadding="0" cellspacing="0">
        <tr>
            
            <td align="center" valign="top">
                 <fieldset style="width: 768px;">
                    <legend>Process Status Download</legend>
                    <table width="100%" border="0">
                         <tr>
                            <td width="25%" align="right">
                                <label>Start Date:</label>&nbsp;&nbsp;</td>
                            <td width="25%"><font color="#FFFFFF" face="Courier New, Courier, mono"> 
      	      <input name="txt_startDate" type="text" id="txt_startDate" maxlength="12" runat="server" class="required datePicCal"/>
        </font></td>
                            <td width="15%" align="right">
                                <label>End Date:</label>&nbsp;&nbsp;</td>
                            <td width="35%"><font color="#FFFFFF" face="Courier New, Courier, mono"> 
         	<input name="txt_toDate" type="text" id="txt_toDate" maxlength="12" runat="server" class="required datePicCal"/>
        </font></td>
                        </tr>
                        <tr>
                            <td align="right">
                                <label>Product Name:</label>&nbsp;&nbsp;</td>
                            <td><font face="Verdana, Arial, Helvetica, sans-serif">
                <select id="sel_product" runat="server"></select>
              </font></td>
                            <td align="right">&nbsp;<label>Process Status:</label>&nbsp;&nbsp;</td>
                            <td>
                                <select id="sel_loanStatus" runat="server"></select></td>
                        </tr>
                       
                        <tr>
                            <td align="right">
                                <label align="right">Department Name:</label>&nbsp;&nbsp;</td>
                               <td><select id="cmbDepartment" runat="server"></select>
                            </td>
                            <td colspan="2" align="center">
                                <%--<input type="button" name="btn_search" id="btn_search" onclick="Search_onClick()" value="SEARCH" runat="server" style="border: 1px solid #84C3E7; background-color: #C2DBC2" />--%>
                                <asp:Button id="btn_search" onclick="Search_onClick__" Text="SEARCH" runat="server" style="border: 1px solid #84C3E7; background-color: #C2DBC2"/>
                                &nbsp;<a href="javascript:void(0);"><input type="submit" name="btn_export" OnServerClick="ExportCsv" runat="server" id="btn_export" value="Export CSV" style="border: 1px solid #84C3E7; background-color: #C2DBC2" /></a></td>
                        </tr>
                    </table>
                    <input type="hidden" id="hid_cleanupstatus" name="hid_cleanupstatus" value="" />
                    <input type="hidden" id="hid_personId" name="hid_personId" />
                    <input type="hidden" name="hid_deptid" id="hid_deptid" />
                </fieldset>
                <%--		  </form>--%>
                <fieldset style="width: 768px;">
                    <legend id="leg_processResultDisplay">Download Process Result</legend>
                    <table width="100%" border="0">
                        <tr>
                            <td><progress></progress>
                                <label id="lab_pleasewait"></label>
                                <div id="div_processResultDisplay" style="width: 768px; height: 260px; border: 1px solid #999999; overflow: auto;" runat="server"></div>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
</asp:Content>

