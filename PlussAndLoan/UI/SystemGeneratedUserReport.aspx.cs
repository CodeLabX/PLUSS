﻿using System;
using BLL;
using BusinessEntities;
using DAL;
using AzUtilities;
using System.IO;
using AzUtilities.Common;
using System.Data;
using System.Linq;

namespace PlussAndLoan.UI
{
    public partial class UI_SystemGeneratedUserReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User userT = (User)Session["UserDetails"];
            if (userT == null)
            {
                Response.Redirect("~/LoginUI.aspx");
            }
        }

        protected void showUserReport_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["UserDetails"];
                DataTable data = new UserManager().GetUserListReportData(user);

                for (int i = data.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = data.Rows[i];
                    if (dr["UserCode"].Equals("1499074"))
                    {
                        dr.Delete();
                    }
                }
                data.AcceptChanges();


                Session["path"] = "~/ReportRDLC/ReportWizards/ListOfUsers.rdlc";
                Session["rdlcData"] = data;
                Session["ReportDataSourceName"] = "ListOfUsers";
                new AT(this).AuditAndTraial("User List (PDF)", "Report");
                Response.Redirect("~/ReportRDLC/ReportWizards/RdlcReportViewer.aspx");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        protected void btnUserReportCSV_Click(object sender, EventArgs e)
        {
            try
            {
                User user = (User)Session["UserDetails"];
                DataTable data = new UserManager().GetUserListReportData(user);
                for (int i = data.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = data.Rows[i];
                    if (dr["UserCode"].Equals("1499074"))
                    {
                        dr.Delete();
                    }
                }
                data.AcceptChanges();
                new AT(this).AuditAndTraial("User List (CSV)", "Report");
                PrintDump(data, "UserListReport");
            }
            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }
        }

        private void PrintDump(DataTable dt, string reportName)
        {
            try
            {
                var filename = reportName + DateTime.Now.Ticks + ".csv";
                var csv = string.Empty;
                if (dt != null)
                {
                    csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ','));
                    csv += "\r\n";

                    foreach (DataRow row in dt.Rows)
                    {
                        csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", " ").Replace(";", " ").Replace("&amp", "&").Replace("&AMP", "&").Replace("amp", " ").Replace("AMP", " ") + ','));
                        csv += "\r\n";
                    }
                }

                MemoryStream memoryStream = new MemoryStream();
                TextWriter textWriter = new StreamWriter(memoryStream);
                textWriter.Write(csv);
                textWriter.Flush(); // added this line
                byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.BinaryWrite(bytesInStream);

            }
            catch (Exception e)
            {
                CustomException.Save(e, "User List Report :: - PrintDump");
                LogFile.WriteLog(e);
            }
            Response.End();
        }
    }
}
