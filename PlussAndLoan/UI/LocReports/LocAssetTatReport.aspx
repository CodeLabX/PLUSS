﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.Master" AutoEventWireup="true" CodeBehind="LocAssetTatReport.aspx.cs" Inherits="PlussAndLoan.UI.LocReports.LocAssetTatReport" %>

<%@ Register TagPrefix="uc1" TagName="TextBoxDatePicker" Src="~/UI/UserControls/js/TextBoxDatePicker.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <uc1:TextBoxDatePicker runat="server" ID="TextBoxDatePicker" />
    <title>TAT Report</title>
    <style type="text/css">
        A {
            COLOR: #333333;
            TEXT-DECORATION: none;
        }

            A:hover {
                TEXT-DECORATION: underline;
            }

        .LabelHead {
            font-size: 14px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .LabelSubHead {
            font-size: 12px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .TableHead {
            font-size: 16px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .label {
            font-size: 18px;
            font-family: Verdana,Arial,Helvetica,sans-serif;
            font-weight: none;
            margin-left: 5px;
        }

        .tableWidth {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }

            .tableWidth td {
                border-width: 1px;
                border-style: solid;
                border-collapse: collapse;
                border-color: Black;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">TAT Report</div>
        <table width="80%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" valign="top">
                    <!--<form action="E2ETATReport.php " method="post" target="_blank">-->
                    <%-- <form name="frm_tatReport" id="frm_tatReport" method="post" action="tatExportToCSV.php">--%>



                    <fieldset style="width: 100%;">
                        <legend>E2E TAT Report</legend>
                        <table width="100%" border="0">
                            <%--<tr>
                                <td align="right">
                                    <label>Product Name:</label>&nbsp;&nbsp;</td>
                                <td><font face="Verdana, Arial, Helvetica, sans-serif">
                                    <select id="sel_product" runat="server"></select>
                                </font></td>
                                <td align="right">&nbsp;<label>Process Status:</label>&nbsp;&nbsp;</td>
                                <td>
                                    <select id="sel_loanStatus" runat="server"></select></td>
                            </tr>--%>
                            <tr>
                                <td>
                                    <label for="field1">
                                        <span>Product Name:</span>
                                        <asp:DropDownList ID="sel_product" AutoPostBack="false" runat="server" CssClass="select-field"></asp:DropDownList>
                                    </label>
                                </td> 
                                <td>
                                    <label for="field1">
                                        <span>Product Name:</span>
                                        <asp:DropDownList ID="sel_loanStatus" AutoPostBack="false" runat="server" CssClass="select-field "></asp:DropDownList>
                                    </label>
                                </td>
                            </tr> 
                            <tr>
                                <td>
                                    <label for="field1">
                                        <span>Start Date:</span>
                                        <asp:TextBox ID="txt_startDate" runat="server" CssClass="input-field" maxlength="12" />
                                    </label>
                                </td> 
                                <td>
                                    <label for="field1">
                                        <span>End Date:</span>
                                        <asp:TextBox ID="txt_toDate" runat="server" CssClass="input-field" maxlength="12" />
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="button" name="btn_duplicatesearch" id="btn_duplicatesearch" value="Search Duplicate Data" runat="server" onserverclick="DuplicateSearch" class="btn primarybtn" />
                                    &nbsp;
                                  <asp:Button CssClass="btn clearbtn" runat="server" Text="Clean up" ID="btnCleanup" OnClick="cleanDuplicateData_" />

                                </td>
                                <td>
                                    <asp:Button CssClass="btn primarybtn" runat="server" Text="SEARCH" ID="btnSearch" OnClick="btnSearch_Click" />
                                    &nbsp;<a href="javascript:void(0);"><input type="submit" name="btn_export" onserverclick="ExportCsv" runat="server" id="btn_export" value="Export CSV"  class="btn primarybtn"/></a></td>
                            </tr>
                        </table>
                        <input type="hidden" id="hid_cleanupstatus" name="hid_cleanupstatus" value="" />
                        <input type="hidden" id="hid_personId" name="hid_personId" />
                        <input type="hidden" name="hid_deptid" id="hid_deptid" />
                    </fieldset>




                    <%--<fieldset style="width: 100%;">
                        <legend>E2E TAT Report</legend>
                        <table width="100%" border="0">
                            <tr>
                                <td align="right">
                                    <label>Product Name:</label>&nbsp;&nbsp;</td>
                                <td><font face="Verdana, Arial, Helvetica, sans-serif">
                                    <select id="sel_product" runat="server"></select>
                                </font></td>
                                <td align="right">&nbsp;<label>Process Status:</label>&nbsp;&nbsp;</td>
                                <td>
                                    <select id="sel_loanStatus" runat="server"></select></td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <label>Start Date:</label>&nbsp;&nbsp;</td>
                                <td ><font color="#FFFFFF" face="Courier New, Courier, mono">
                                    <input name="txt_startDate" type="text" id="txt_startDate" readonly maxlength="12" runat="server" class="required datePicCal" />
                                </font></td>
                                <td  align="right">
                                    <label>End Date:</label>&nbsp;&nbsp;</td>
                                <td ><font color="#FFFFFF" face="Courier New, Courier, mono">
                                    <input name="txt_toDate" type="text" id="txt_toDate" readonly maxlength="12" runat="server" class="required datePicCal" />
                                </font></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <input type="button" name="btn_duplicatesearch" id="btn_duplicatesearch" value="Search Duplicate Data" runat="server" onserverclick="DuplicateSearch" style="border: 1px solid #84C3E7; background-color: #C2DBC2" />
                                    &nbsp;
                                  <asp:Button runat="server" Text="Clean up" ID="btnCleanup" Style="border: 1px solid #84C3E7; background-color: #C2DBC2;" OnClick="cleanDuplicateData_" />

                                </td>
                                <td colspan="2" align="center">
                                    <asp:Button runat="server" Text="SEARCH" ID="btnSearch" Style="border: 1px solid #84C3E7; background-color: #C2DBC2;" OnClick="btnSearch_Click" />
                                    &nbsp;<a href="javascript:void(0);"><input type="submit" name="btn_export" onserverclick="ExportCsv" runat="server" id="btn_export" value="Export CSV" style="border: 1px solid #84C3E7; background-color: #C2DBC2" /></a></td>
                            </tr>
                        </table>
                        <input type="hidden" id="hid_cleanupstatus" name="hid_cleanupstatus" value="" />
                        <input type="hidden" id="hid_personId" name="hid_personId" />
                        <input type="hidden" name="hid_deptid" id="hid_deptid" />
                    </fieldset>--%>
                    <%--		  </form>--%>
                    <fieldset style="width: 100%;">
                        <legend id="leg_processResultDisplay">Search Result</legend>
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <label id="lab_pleasewait"></label>
                                    <div id="div_processResultDisplay" style="width: 100%; height: 260px; border: 1px solid #ddd; overflow: auto;" runat="server"></div>
                                    <div id="divSearchResultShow" style="width: 100%; height: 260px; border: 1px solid #ddd; overflow: auto;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>


                </td>
            </tr>
        </table>
        <%--    <table style="border: 1px; width: 100%">
        <tr style="text-align: center; background-color: #CCCCCC"> 
            <div runat="server" id="headDiv"></div>
        </tr>
        <tr>
             <div style="width: 1100px;overflow-y: scroll;overflow-x: scroll;">
       <asp:GridView ID="gridView" runat="server" ></asp:GridView> 
        <br/>
        </div>
        </tr>
    </table>--%>
    </div>
</asp:Content>
