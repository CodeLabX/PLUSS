﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AzUtilities;
using Bat.Common;
using BLL;
using BusinessEntities;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI.LocReports
{
    public partial class LocAssetExpImp90Days : System.Web.UI.Page
    {
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        private readonly LoanReportService _service = new LoanReportService();
        public DateTime ExpImpMaxMinDateAllowed;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadProduct();
                LoadLoanState();
                LoadDepartment();
            }

            ExpImpMaxMinDateAllowed = AppSettingUtility.ExpImpMinDateForReport;
        }

        private void LoadProduct()
        {
            List<Product> productListObj = _loanApplicationManagerObj.GetAllProductByLearner();
            var productObj = new Product { ProductId = 0, ProductName = "Select Product" };
            productListObj.Insert(0, productObj);
            sel_product.DataTextField = "ProductName";
            sel_product.DataValueField = "ProductId";
            sel_product.DataSource = productListObj;
            sel_product.DataBind();
        }

        private void LoadLoanState()
        {
            var dt = _loanApplicationManagerObj.GetLoanStates();
            var loanStates = DataTableToList.GetList<Loan>(dt);
            var state = new Loan() { LoanState = 0, StateName = "Select Loan Status" };
            loanStates.Insert(0, state);
            sel_loanStatus.DataTextField = "StateName";
            sel_loanStatus.DataValueField = "LoanState";
            sel_loanStatus.DataSource = loanStates;
            sel_loanStatus.DataBind();
        }

        private void LoadDepartment()
        {
            var dt = _loanApplicationManagerObj.GetDepartment();
            var departmentList = DataTableToList.GetList<Department>(dt);
            var state = new Department() { LOAN_DEPT_ID = 0, LOAN_DEPT_NAME = "Select Department Name" };
            departmentList.Insert(0, state);
            cmbDepartment.DataTextField = "LOAN_DEPT_NAME";
            cmbDepartment.DataValueField = "LOAN_DEPT_ID";
            cmbDepartment.DataSource = departmentList;
            cmbDepartment.DataBind();
        }

        protected void Search_onClick__(object sender, EventArgs e)
        {
            int productId = Convert.ToInt32(sel_product.Text);
            int processId = Convert.ToInt32(sel_loanStatus.Text);
            int departmentId = Convert.ToInt32(cmbDepartment.Text);
            string startDate = txtStartDateLoc.Text;
            string endDate = txtToDateLoc.Text;
            var dt = _service.GetExpImp(productId, departmentId, processId, startDate, endDate);

            if (dt == null)
            {
                modalPopup.ShowWarning("No Record Found.", Title);
                return;
            }


            if (dt != null)
            {
                div_processResultDisplay.InnerHtml = "";

                var noofField = dt != null ? dt.Columns.Count : 100;
                var colWith = noofField != 0 ? 100 / noofField : 0;
                if (colWith * noofField > 100)
                {
                    colWith = colWith - 1;
                }
                var successCount = 0;
                var chckIndex = 0;
                var tableBodyData = "";
                var tabelHeadRowName = "";
                var fieldData = "";
                var index = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    fieldData = "";
                    for (index = 0; index < noofField; index++)
                    {
                        string name = dt.Columns[index].ColumnName;
                        if (chckIndex == 0)
                        {
                            if (name != "noofCount" && name != "DEPT_ID" && name != "PROCESS_STATUS_ID")
                            {
                                tabelHeadRowName += "<td width='" + colWith + "%'><label class='LabelHead'>" + name +
                                                    "</label></td>";
                            }
                            else
                            {

                            }
                        }
                        if (name != "noofCount" && name != "DEPT_ID" && name != "PROCESS_STATUS_ID")
                        {
                            fieldData += "<td><label>" + dt.Rows[i][index] + "</label></td>	";
                        }
                        else { }


                    }

                    tableBodyData += "<tr align='center'>" + fieldData + "</tr>";
                    successCount = successCount + 1;
                    chckIndex++;

                }

                var tableHeadData = "<tr align='center'  bgcolor='#CCCCCC'><td colspan='" + noofField + "' > <label class='LabelHead' >" + successCount + " Records found </td></tr><tr align='center'  bgcolor='#CCCCCC'>" + tabelHeadRowName + "</tr>";
                div_processResultDisplay.InnerHtml = "<table border='1' cellpadding='0' cellspacing='0' width='100%' class='tableWidth'>" + tableHeadData + tableBodyData + "</table>";
                
            }

        }

        protected void ExportCsv(object o, EventArgs e)
        {
            var csv = string.Empty;

            try
            {
                int productId = Convert.ToInt32(sel_product.Text);
                int processId = Convert.ToInt32(sel_loanStatus.Text);
                int departmentId = Convert.ToInt32(cmbDepartment.Text);
                string startDate = txtStartDateLoc.Text;
                string endDate = txtToDateLoc.Text;
                var dt = _service.GetExpImp(productId, departmentId, processId, startDate, endDate);

                if (dt == null)
                {
                    modalPopup.ShowWarning("No Records Found.", Title);
                    return;
                }

                var filename = "ExpImpReportExport_" + DateTime.Now.Ticks + ".csv";
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                Response.Charset = "";
                Response.ContentType = "application/text";

                if (dt != null)
                {
                    csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + '\t'));
                    csv += "\r\n";

                    foreach (DataRow row in dt.Rows)
                    {
                        csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace("\t", ",").Replace(",", ";") + '\t'));

                        csv += "\r\n";
                    }

                    //Download the CSV file.
                }
            }

            catch (Exception ex)
            {
                LogFile.WriteLog(ex);
            }

            Response.Output.Write(csv);
            Response.Flush();
            Response.End();
        
        }
    }
}
