﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.LoanLocatorReports;
using BusinessEntities;

namespace PlussAndLoan.UI.LocReports
{
    public partial class ApplicationStatusWiseReprot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string fromDate = (string) Session["dateOne"];

                string toDate = (string) Session["dateTwo"];

                string productName = (string) Session["product"];

                string status = (string) Session["status"];
                string title = status + " Application List";

                titleDiv.InnerText = title;
                productDiv.InnerText = productName;
                formDateDiv.InnerText = fromDate;
                toDateDiv.InnerText = toDate;

                DataTable dt = (DataTable)Session["results"];
                if (status == "Approved")
                {
                    divDisbReport.Visible = false;
                    div1.Visible = false;
                    approvedgridView.DataSource = dt;
                    approvedgridView.DataBind();
                }
                else if (status == "Disbursed")
                {
                    divApprovedReport.Visible = false;
                    div1.Visible = false;
                    disbgridView.DataSource = dt;
                    disbgridView.DataBind();
                }
                else
                {
                    divApprovedReport.Visible = false;
                    divDisbReport.Visible = false;

                    ApplyGrid.DataSource = dt;
                    ApplyGrid.DataBind();
                }


               




            }
           
        }
    }
}