﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.LoanLocatorReports;
using BusinessEntities;


namespace PlussAndLoan.UI.LocReports
{
    public partial class ApplicationDetailsReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fromDate = (string)Session["dateOne"];

            string toDate = (string)Session["dateTwo"];

            string productName = (string)Session["product"];

            string status = (string)Session["status"];
            productDiv.InnerText = productName;
            formDateDiv.InnerText = fromDate;
            toDateDiv.InnerText = toDate;
            DataTable dt = (DataTable)Session["table"];

            gridView.DataSource = dt;
            gridView.DataBind();

        }
    }
}