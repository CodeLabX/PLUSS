﻿<%@ Page Language="C#" MasterPageFile="~/UI/AdminMainUI.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_ProductUI" Title="Product Entry" Codebehind="ProductUI.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div style="margin-left: 0px; margin-top: 0px">
        <table>
            <tr style="font-weight: bold">
                <td align="center" style="font-size: 18px">
                    Product
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp
                </td>
            </tr>
            <tr align="center">
                <td>
                    <table>
                        <tr>
                            <td style="width: 100px" align="right">
                                Product Code:
                            </td>
                            <td>
                                <asp:TextBox ID="productIdTextBox" runat="server" Width="150px" MaxLength="4"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ControlToValidate="productIdTextBox" Enabled="false" EnableClientScript="false"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                Product Name:
                            </td>
                            <td>
                                <asp:TextBox ID="productNameTextBox" runat="server" Width="150px" 
                                    MaxLength="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                    ControlToValidate="productNameTextBox" Enabled="false" EnableClientScript="false"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" align="right">
                                Product Type:
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="productTypeDropDownList" runat="server" Width="156px">
                                </asp:DropDownList>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="activeLabel" runat="server" Text="Status: "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="activeDropdownList" runat="server">
                                    <asp:ListItem>Active</asp:ListItem>
                                    <asp:ListItem>Inactive</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr align="center">
                <td>
                    &nbsp
                </td>
            </tr>
            <tr align="center">
                <td>
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="addButton" runat="server" Text="Add" OnClick="addButton_Click" Width="65px"
                                    Height="26px" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="clearButton" runat="server" Text="Clear" OnClick="clearButton_Click"
                                    Width="65px" Height="26px" />
                            </td>
                            <td>
                                &nbsp
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="searchTextBox" runat="server" Width="150px"></asp:TextBox>
                    &nbsp
                    <asp:Button ID="searchbutton" runat="server" Text="Search" Width="65px" OnClick="searchbutton_Click"
                        Height="20px" />
                </td>
            </tr>
            <tr>
                    <td>
                         <div id="gridbox" style="background-color: white; width: 760px; height: 250px;"> </div>
                    </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td style="height: 50px">
                    &nbsp
                </td>
            </tr>
        </table>
        
         <div style="display: none" runat="server" id="dvXml"></div>
    </div>

    <script type="text/javascript">
	var mygrid;
	mygrid = new dhtmlXGridObject('gridbox');
	mygrid.setImagePath("../codebase/imgs/");
	mygrid.setHeader("SL,ProductCode,ProductName,ProductType,Status");
	mygrid.setInitWidths("40,150,300,150,100");
	mygrid.setColAlign("right,left,left,left,left");
	mygrid.setColTypes("ro,ro,ro,ro,ro");
	mygrid.setColSorting("int,str,str,str,str");
	mygrid.setSkin("light");	 
	mygrid.init();
	mygrid.enableSmartRendering(true,20);
	mygrid.parse(document.getElementById("xml_data"));
	mygrid.attachEvent("onRowSelect",doOnRowSelected);
    function doOnRowSelected(id)
    {
        if(mygrid.cells(mygrid.getSelectedId(),0).getValue()>0)
        {
          document.getElementById('ctl00_ContentPlaceHolder_addButton').value="Update";
          document.getElementById('ctl00_ContentPlaceHolder_productIdTextBox').value = mygrid.cells(mygrid.getSelectedId(),1).getValue();
          document.getElementById('ctl00_ContentPlaceHolder_productNameTextBox').value = mygrid.cells(mygrid.getSelectedId(), 2).getValue();
            var productType = "";
            switch (mygrid.cells(mygrid.getSelectedId(), 3).getValue()) {
              case "Unsecure":
                  productType = "U";
                  break;
              case "Secure":
                  productType = "S";
                  break;
              case "Cash Secure":
                  productType = "C";
                  break;
            }

          document.getElementById('ctl00_ContentPlaceHolder_productTypeDropDownList').value = productType;
            document.getElementById('ctl00_ContentPlaceHolder_activeDropdownList').value = mygrid.cells(mygrid.getSelectedId(), 4).getValue();
         }
         else
         {
          document.getElementById('ctl00_ContentPlaceHolder_addButton').value="Add";
         }
    }
    </script>

</asp:Content>
