﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_ClearingZone" Codebehind="ClearingZoneUI.aspx.cs" %>

<%@ Register Src="~/UI/UserControls/css/spreadsheet.ascx" TagPrefix="uc1" TagName="spreadsheet" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Clearing Zone</title>
    <uc1:spreadsheet runat="server" ID="spreadsheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%" border="0" cellpadding="1" cellspacing="1">
        <tr valign="top">
            <td>
                <div>
                    <asp:GridView ID="clearingZoneGridView" runat="server" PageSize="15" Font-Size="11px"
                        CellPadding="4" ForeColor="#333333" AutoGenerateColumns="False" Width="100%">
                        <RowStyle BackColor="#FFFFFF" ForeColor="#333333" />
                        <Columns>
                            <asp:TemplateField HeaderText="SL No." Visible="false" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:Label ID="slNoLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="5%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bank Name" HeaderStyle-Width="35%">
                                <ItemTemplate>
                                    <asp:Label ID="bankNameLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="35%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Branch" HeaderStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:Label ID="branchLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="30%"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="25%">
                                <ItemTemplate>
                                    <asp:Label ID="statusLabel" runat="server"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="25%"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle CssClass="ssHeader" />
                        <EditRowStyle BackColor="#999999" />
                        <AlternatingRowStyle BackColor="AliceBlue" ForeColor="#284775" />
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
