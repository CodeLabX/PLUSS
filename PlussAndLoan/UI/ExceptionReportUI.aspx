﻿<%@ Page Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_ExceptionReportUI" Title="Exception Report" Codebehind="ExceptionReportUI.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>--%>
    <%--    <script src="../Scripts/ui.datepicker.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <script type="text/javascript">
            function pageLoad() {
                $('#ctl00_ContentPlaceHolder_startDateTextBox').unbind();
                $('#ctl00_ContentPlaceHolder_endDateTextBox').unbind();
                $('#ctl00_ContentPlaceHolder_startDateTextBox').datepicker({
                    showOn: 'button',
                    buttonImage: '../Image/Calendar_scheduleHS.png',
                    buttonImageOnly: true,
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
                $('#ctl00_ContentPlaceHolder_endDateTextBox').datepicker({
                    showOn: 'button',
                    buttonImage: '../Image/Calendar_scheduleHS.png',
                    buttonImageOnly: true,
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            }
    </script>

    <table>
        <tr>
            <td style="width:50px">
               <asp:Label runat="server" ID="errMsgLabel"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="color: Red; padding-right: 5px; text-align: right;">
                &nbsp;
            </td>
            <td>
                <span id="startDatetext" runat="server">Start Date</span>
            </td>
            <td>
                <asp:TextBox ID="startDateTextBox" runat="server" TabIndex="4" Width="160px" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="startDateRequiredFieldValidator" runat="server" ControlToValidate="startDateTextBox"
                    ErrorMessage="Please select a date" SetFocusOnError="true" EnableClientScript="false"
                    CssClass="validation"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="sdateRegularExpressionValidator" runat="server"
                    ErrorMessage="Please select a valid date" ControlToValidate="startDateTextBox"
                    ValidationExpression="^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$"
                    SetFocusOnError="true" EnableClientScript="false" CssClass="validation"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr id="endDateRow" runat="server">
            <td style="color: Red; padding-right: 5px; text-align: right;">
                &nbsp;
            </td>
            <td>
                End Date
            </td>
            <td>
                <asp:TextBox ID="endDateTextBox" runat="server" TabIndex="4" Width="160px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="endDateRequiredFieldValidator" runat="server"
                    ErrorMessage="Please select a valid date" ControlToValidate="endDateTextBox"
                    ValidationExpression="^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$"
                    SetFocusOnError="true" EnableClientScript="false" CssClass="validation"></asp:RegularExpressionValidator>
                    
               
            </td>
        </tr>
        <tr>
            <td style="width:50px">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td colspan="2" align="center">
                <asp:Button ID="nextButton" runat="server" Width="100px" Text="Show Report" TabIndex="5"
                    OnClick="nextButton_Click" />
                <asp:Button ID="cancelButton" runat="server" Width="100px" Text="Cancel" CausesValidation="false"
                    TabIndex="6" />
            </td>
        </tr>
    </table>
</asp:Content>
