﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using AzUtilities;
using BLL;
using BusinessEntities;
using BusinessEntities.SecurityMatrixEntities;
using DAL;
using DAL.SecurityMatrixGateways;

namespace PlussAndLoan.UI
{

    public partial class UserSwitch : System.Web.UI.MasterPage
    {
        public UserManager userManagerObj = null;
        private UserAccessLogManager userAccessLogManagerObj = null;
        private PageAccessPermission _permissions;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                User userT = (User)Session["UserDetails"];
                if (userT == null)
                {
                    Response.Redirect("~/LoginUI.aspx");
                }

                if (userT.Type == 6 || userT.Type == 7)
                {
                    search_boxes_header.Visible = false;
                }

                userAccessLogManagerObj = new UserAccessLogManager();
                userManagerObj = new UserManager();

                //for session kill
                var loginId = (string)Session["UserId"];
                userIdHiddenField.Value = loginId;
                var user = LoginUser.LoginUserToSystem.Where(u => u.UserId == loginId);
                if (!user.Any())
                {
                    //Comment By Shaone
                    //Response.Redirect("~/LoginUI.aspx");
                }
                //-------end

                _permissions = new PageAccessPermission();

                if (Session.Count == 0)
                {
                    Response.Redirect("~/ErrorPage/Timout.htm");
                }
                if (!IsPostBack)
                {
                    GetMenuString();
                    userNameLabel.Text = (string)Session["UserName"];

                    short userType = (short)Session["UserType"];
                
                }
                Session["MasterPageUrlForQueryTracker"] = "~/UI/AutoLoan/AutoApprover.master";
            }
            catch (Exception exception)
            {
                new AT(this).AuditAndTraial("switch user", "SW Master -"+exception.Message);
            }
        }

        protected void btnSearchHeader_Click(object sender, EventArgs e)
        {
            try
            {
                string llid = txtLlidHeader.Text;
                string arm = txtARMCodeHeader.Text;
                string acc = txtAccHeader.Text;
                string name = txtNameHeader.Text;

                double outputValue = 0;
                bool isNumber = false;
                isNumber = double.TryParse(txtLlidHeader.Text, out outputValue);
                if (!isNumber)
                {
                    txtLlidHeader.Text = "";
                    llid = "";
                }
                Response.Redirect("~/UI/LoanApplication/LocLoanHome.aspx?llid=" + llid + "&arm=" + arm + "&acc=" + acc + "&name=" + name);
            }
            catch (Exception ex)
            {
                txtLlidHeader.Text = "";
                txtARMCodeHeader.Text = "";
                txtAccHeader.Text = "";
                txtNameHeader.Text = "";
                LogFile.WriteLine("Hearder Search - " + ex);
            }
        }

        protected void btnSearchDetailHeader_Click(object sender, EventArgs e)
        {
            try
            {
                double outputValue = 0;
                bool isNumber = false;
                isNumber = double.TryParse(txtLlidHeader.Text, out outputValue);
                if (!isNumber)
                {
                    txtLlidHeader.Text = "";
                }
                else
                {
                    string llid = txtLlidHeader.Text;
                    Response.Redirect("~/UI/LoanApplication/loc_EditLoan.aspx?id=" + llid);
                }
            }
            catch (Exception ex)
            {
                txtLlidHeader.Text = "";
                LogFile.WriteLine("LLID Search - " + ex);
            }
        }

        private List<MenuModel> GetMenuItems()
        {
            User user = (User)Session["UserDetails"];
            List<MenuModel> list = _permissions.GetMenuListForUser(user);

            return list;
        }
        public void GetMenuString()
        {
            List<MenuModel> list = GetMenuItems();
            User user = (User)Session["UserDetails"];

            string menuStr = "";

            if (list.Count > 0)
            {
                menuStr = GetSubMenus(list, 0);
            }
            NavItem.InnerHtml = "<ul id='menu menu-ul'>" + menuStr + "</ul>";
        }

        private string GetSubMenus(IEnumerable<MenuModel> siteMenu, Nullable<int> parentId)
        {
            StringBuilder strBuilder = new StringBuilder();

            foreach (var item in siteMenu.Where(a => a.ParentId.Equals(parentId)))
            {
                strBuilder.Append(string.Format("<li class='parent'><a href='{0}'>{1}</a>", item.Url, item.Description));
                int subMenuCount = siteMenu.Where(a => a.ParentId.Equals(parentId)).Count();

                if (subMenuCount > 0)
                {
                    strBuilder.Append("<ul class='child'>");
                    strBuilder.Append(GetSubMenus(siteMenu, item.MenuId));
                    strBuilder.Append("</ul>");
                }
                strBuilder.Append("</li>");
            }

            return strBuilder.ToString();
        }

        #region

        private void removeCookie()
        {
            HttpCookie myCookie = new HttpCookie("LOGIN");
            Response.Cookies["LOGIN"].Expires = DateTime.Now.AddDays(-365);

            string currentCookieValue = "";
            if (Response.Cookies["LOGIN"].Value == "" && Response.Cookies["LOGIN"].Expires == DateTime.MinValue)
            {
                currentCookieValue = Request.Cookies["LOGIN"].Value;
                Response.Cookies.Remove("LOGIN");
            }
            else
            {
                myCookie = new HttpCookie("LOGIN");
                myCookie.Values.Add("LoginId", "");
                myCookie.Values.Add("status", "0");
                currentCookieValue = Response.Cookies["LOGIN"].Value;
            }

            HttpCookie PlussCookie = Request.Cookies["PlussUserSession"];
            PlussCookie.Expires = DateTime.Now.AddDays(-1);  
            Response.Cookies.Add(PlussCookie);
        }

        #endregion

        protected void logout_Click(object sender, EventArgs e)
        {
            var userId = Session["UserId"].ToString();
            LoginUser.LoginUserToSystem.RemoveAll(u => u.UserId == userId);

            SaveAccessLog(userIdHiddenField.Value.ToString(), 0, 0, 0);
            new AT(this).AuditAndTraial("Logout", "Logout");
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            removeCookie(); //tarek
            Response.Redirect("~/LoginUI.aspx");


        }

        private void SaveAccessLog(string loginId, int successStatus, int unSuccessStatus, int userLoginStatus)
        {
            int insertAccessLog = 0;
            UserAccessLog userAccessLogObj = new UserAccessLog();
            userAccessLogObj.UserLogInId = loginId.ToString();
            userAccessLogObj.IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            userAccessLogObj.LogInCount = 0;
            userAccessLogObj.LogInDateTime = DateTime.Now;
            userAccessLogObj.LogOutDateTime = DateTime.Now;
            userAccessLogObj.userLoginSuccessStatus = 0;
            userAccessLogObj.userLoginUnSuccessStatus = 0;
            userAccessLogObj.userLoginStatus = 0;
            userAccessLogObj.AccessFor = "LOGOUT";
            insertAccessLog = userAccessLogManagerObj.SendDataInToDB(userAccessLogObj);
        }

        protected void MainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            string s = e.Item.Text;
        }
    }
}
