﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_DedupInfoUploadUI : System.Web.UI.Page
    {
        public DeDupInfoManager deDupInfoManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        Int32 rowsPerPage = 100;
        Int32 pageNum = 1;
        Int32 offset = 0;
        public static int decideRun = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            deDupInfoManagerObj = new DeDupInfoManager();
            if (Request.QueryString.Count != 0)
            {
                pageNum = Convert.ToInt32(Request.QueryString["pageIndex"]);
                offset = (pageNum - 1) * rowsPerPage;
            }
            BindData(offset, rowsPerPage);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "onLoadPage();", true);
        }

        [WebMethod]
        public static string UploadedDataStatus()
        {
            //if (decideRun > 0)
            //{
            return DeDupInfoManager.GetCurrentUploadedDataNumber().ToString();
            // }
            // return "0";
        }

        protected void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "";
                string path = Request.PhysicalApplicationPath;
                path += @"Uploads\";
                if (FileUpload.HasFile)
                {
                    fileName = Server.HtmlEncode(FileUpload.FileName);
                    path += fileName;
                    FileUpload.SaveAs(path);
                    ReadExcleFile(path);
                    new AT(this).AuditAndTraial("Disburse Info Upload","Upload");
                }
                else
                {
                    Alert.Show("Please select Excel file");
                    FileUpload.Focus();
                }
                // decideRun = 1;
                // UploadFile(FileUpload);
                // UploadButton.Attributes.Add("onclick", "DisplayInterval();");
            }catch (Exception exception) {
                CustomException.Save(exception, "Disburse Info Upload");
            }

        }
        private void UploadFile(FileUpload fileUpload)
        {
            string path = Request.PhysicalApplicationPath;
            path += @"uploads\";

            if (fileUpload.HasFile)
            {
                string fileName = Server.HtmlEncode(fileUpload.FileName);
                string extension = System.IO.Path.GetExtension(fileName);

                if (extension == ".xls")
                {
                    path += fileName;
                    fileUpload.SaveAs(path);
                    DataTable dt = GetDataFromExcelFile(path);

                    if (dt.Rows.Count > 0)
                    {
                        messageLabel.Text = deDupInfoManagerObj.SendUploadDataToDB(dt, criteriaDropDownList.Text).ToString();
                    }
                }
                else
                {
                    messageLabel.Text = "Select excel file";
                }
            }
        }
        public static DataTable GetDataFromExcelFile(string fileName)
        {
            bool hdr = true;
            string extended = "";
            if (hdr)
                extended = @";Extended Properties=""Excel 8.0;HDR=YES;FMT=Delimited""";
            else extended = @";Extended Properties=""Excel 8.0;HDR=NO;FMT=Delimited""";

            string conString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                               "Data Source=" + fileName + "; Jet OLEDB:Engine Type=5;" + extended;

            OleDbConnection oleDbCon = new OleDbConnection(conString);
            DataTable dataTable = new DataTable();
            try
            {
                oleDbCon.Open();
                dataTable = oleDbCon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = dataTable.Rows[0]["TABLE_NAME"].ToString();
                dataTable.Clear();
                dataTable.Columns.Clear();
                string mSQL = "SELECT * FROM [" + sheetName + "]";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(mSQL, oleDbCon);
                dataAdapter.Fill(dataTable);
            }
            catch (OleDbException exp)
            {
                return null;
            }
            finally
            {
                oleDbCon.Close();
            }
            return dataTable;
        }

        private void BindData(Int32 offset, Int32 rowsPerPage)
        {
            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            string type = "";
            type = criteriaDropDownList.Text;
            deDupInfoObjList = deDupInfoManagerObj.GetUploadResult(DateTime.Now, DateTime.Now, type, offset, rowsPerPage);
            StringBuilder headTable = new StringBuilder();
            StringBuilder viewTable = new StringBuilder();
            StringBuilder pagingTable = new StringBuilder();
            if (deDupInfoObjList != null)
            {
                headTable.Append("<table id='dataTable' name='dataTable' bordercolor='#F3F2F7' width='100%'  border='0px' cellpadding='0px' cellspacing='0px' >");
                headTable.Append("<thead>");
                headTable.Append("<tr bgcolor='#FOEEE4'>");
                headTable.Append("<td style='width:50px;'><b>SL.</b></td>");
                headTable.Append("<td style='width:200px;'><b>Name</b></td>");
                headTable.Append("<td style='width:200px;'><b>Father's Name</b></td>");
                headTable.Append("<td style='width:200px;'><b>Mother's Name</b></td>");
                headTable.Append("<td style='width:90px;'><b>DOB</b></td>");
                headTable.Append("<td style='width:90px;'><b>Master No</b></td>");
                headTable.Append("<td style='width:90px;'><b>Declin Reson</b></td>");
                headTable.Append("<td style='width:90px;'><b>Declin Date</b></td>");
                headTable.Append("<td style='width:90px;'><b>Business Name</b></td>");
                headTable.Append("<td style='width:90px;'><b>Add1</b></td>");
                headTable.Append("<td style='width:90px;'><b>Add2</b></td>");
                headTable.Append("<td style='width:90px;'><b>Ph1</b></td>");
                headTable.Append("<td style='width:90px;'><b>Ph2</b></td>");
                headTable.Append("<td style='width:90px;'><b>Mob1</b></td>");
                headTable.Append("<td style='width:90px;'><b>Mob2</b></td>");
                headTable.Append("<td style='width:90px;'><b>Source</b></td>");
                headTable.Append("<td style='width:90px;'><b>Status</b></td>");
                headTable.Append("<td style='width:90px;'><b>Remarks</b></td>");
                headTable.Append("</tr>");
                headTable.Append("</thead>");
                //headTable.Append("</table>");
                //tableHeadDiv.InnerHtml = headTable.ToString();
                //viewTable.Append("<table id='dataTable' width='100%'>");
                viewTable.Append(headTable);
                viewTable.Append("<tbody>");
                foreach (DeDupInfo deDupInfoObj in deDupInfoObjList)
                {
                    viewTable.Append("<tr>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Id.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Name.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.FatherName.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.MotherName.ToString() + "</td>");
                    if (deDupInfoObj.DateOfBirth.ToString() != "")
                    {
                        viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DateOfBirth.ToString("dd-MM-yyyy") + "</td>");
                    }
                    else
                    {
                        viewTable.Append("<td class='TdStyle'>" + "" + "</td>");
                    }
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.AccountNo.MasterAccNo1.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DeclineReason.ToString() + "</td>");
                    if (deDupInfoObj.DeclineDate.ToString() != "")
                    {
                        viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.DeclineDate.ToString("dd-MM-yyyy") + "</td>");
                    }
                    else
                    {
                        viewTable.Append("<td class='TdStyle'>" + "" + "</td>");
                    }
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.BusinessEmployeeName.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Address1.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Address2.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo1.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo2.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo3.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.ContactNumber.ContactNo4.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Source.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Status.ToString() + "</td>");
                    viewTable.Append("<td class='TdStyle'>" + deDupInfoObj.Remarks.ToString() + "</td>");
                    viewTable.Append("</tr>");
                }
                viewTable.Append("</tbody>");
                viewTable.Append("</table>");
                recordViewDiv.InnerHtml = viewTable.ToString();
                Int32 numrows = deDupInfoManagerObj.GetTolalRecord(DateTime.Now, DateTime.Now, type);
                Int32 maxPage = numrows / rowsPerPage;
                maxPage = maxPage + 1;
                string nav = "";
                string prev = "";
                string first = "";
                string next = "";
                string last = "";
                Int32 page = 0;
                for (page = 1; page <= maxPage; page++)
                {
                    if (page == pageNum)
                    {
                        nav = page.ToString();
                    }
                    else
                    {
                        nav += " <a href=../UI/DedupInfoUploadUI.aspx?pageIndex=" + page.ToString() + ">" + page.ToString() + "</a> ";
                    }
                }
                if (pageNum > 1)
                {
                    page = pageNum - 1;
                    prev = " <a href=../UI/DedupInfoUploadUI.aspx?pageIndex=" + page.ToString() + ">Prev</a> ";
                    first = " <a href=../UI/DedupInfoUploadUI.aspx?pageIndex=1>First Page</a>";
                }
                else
                {
                    prev = "&nbsp;";
                    first = "&nbsp;";
                }

                if (pageNum < maxPage)
                {
                    page = pageNum + 1;
                    next = " <a href=../UI/DedupInfoUploadUI.aspx?pageIndex=" + page + ">Next</a> ";

                    last = " <a href=../UI/DedupInfoUploadUI.aspx?pageIndex=" + maxPage + ">Last Page</a> ";
                }
                else
                {
                    next = "&nbsp;";
                    last = "&nbsp;";
                }
                pagingTable.Append("<table width='100%'>");
                pagingTable.Append("<tr>");
                pagingTable.Append("<td>" + first.ToString() + prev.ToString() + nav.ToString() + next.ToString() + last.ToString() + "</td>");
                pagingTable.Append("</tr>");
                pagingTable.Append("</table>");
                pagingDiv.InnerHtml = pagingTable.ToString();

            }
        }
        private void ReadExcleFile(string fileName)
        {
            long slNo = 1;
            int returnvalue=0;
            long recordCount = 0;
        
            List<DeDupInfo> deDupInfoObjList = new List<DeDupInfo>();
            if (fileName != "")
            {
                string cnnStr = ("Provider=Microsoft.Jet.OLEDB.4.0;" + ("Data Source="
                                                                        + (fileName + (";" + "Extended Properties=\"Excel 8.0;\""))));

                DataTable dataTableObj = new DataTable();
                OleDbDataAdapter dataAdapterObj = new OleDbDataAdapter("Select * from [Sheet1$]", cnnStr);
                try
                {
                    dataAdapterObj.Fill(dataTableObj);
                    foreach (DataRow dr in dataTableObj.Rows)
                    {
                        DeDupInfo deDupInfoObj = new DeDupInfo();
                        if (Convert.ToString(dr[0]) != "")
                        {
                            deDupInfoObj.Name = dr[0].ToString();
                            deDupInfoObj.FatherName = dr[2].ToString();
                            deDupInfoObj.MotherName = dr[3].ToString();
                            if (!String.IsNullOrEmpty(dr[4].ToString()))
                            {
                                deDupInfoObj.DateOfBirth = Convert.ToDateTime(dr[4].ToString());
                            }
                            else
                            {
                                deDupInfoObj.DateOfBirth = DateTime.Now;
                            }
                            deDupInfoObj.ScbMaterNo = dr[1].ToString();
                            deDupInfoObj.DeclineReason = dr[5].ToString();
                            if (!String.IsNullOrEmpty(dr[6].ToString()))
                            {
                                deDupInfoObj.DeclineDate = Convert.ToDateTime(dr[6].ToString());
                            }
                            else
                            {
                                deDupInfoObj.DeclineDate = DateTime.Now;
                            }
                            deDupInfoObj.BusinessEmployeeName = dr[7].ToString();
                            deDupInfoObj.Address1 = dr[8].ToString();
                            deDupInfoObj.Address2 = dr[9].ToString();
                            deDupInfoObj.Phone1 = dr[10].ToString();
                            deDupInfoObj.Phone2 = dr[11].ToString();
                            deDupInfoObj.Mobile1 = dr[12].ToString();
                            deDupInfoObj.Mobile2 = dr[13].ToString();
                            if (!String.IsNullOrEmpty(dr[14].ToString()))
                            {
                                deDupInfoObj.ProductId = deDupInfoManagerObj.GetProductId(dr[14].ToString());
                            }
                            else
                            {
                                deDupInfoObj.ProductId = 0;
                            }

                            deDupInfoObj.BankBranch=dr[15].ToString();
                            deDupInfoObj.Source = dr[16].ToString();
                            deDupInfoObj.Status = criteriaDropDownList.Text;//dr[17].ToString();
                            deDupInfoObj.Remarks = dr[18].ToString();
                            if (!String.IsNullOrEmpty(dr[19].ToString()))
                            {
                                deDupInfoObj.Tin = dr[19].ToString();
                            }
                            deDupInfoObj.IdType = dr[20].ToString();
                            deDupInfoObj.IdNo = dr[21].ToString();
                            deDupInfoObj.IdType1 = dr[22].ToString();
                            deDupInfoObj.IdNo1 = dr[23].ToString();
                            deDupInfoObj.IdType2 = dr[24].ToString();
                            deDupInfoObj.IdNo2 = dr[25].ToString();
                            deDupInfoObj.IdType3 = dr[26].ToString();
                            deDupInfoObj.IdNo3 = dr[27].ToString();
                            deDupInfoObj.IdType4 = dr[28].ToString();
                            deDupInfoObj.IdNo4 = dr[29].ToString();
                            deDupInfoObj.ResidentaileStatus = dr[30].ToString();
                            deDupInfoObj.EducationalLevel = dr[31].ToString();
                            deDupInfoObj.MaritalStatus = dr[32].ToString();
                            deDupInfoObj.LoanAccountNo = dr[33].ToString();
                            deDupInfoObj.Active = "Y";
                            deDupInfoObj.CriteriaType = dr[17].ToString();
                            deDupInfoObj.EntryDate = DateTime.Now;
                            deDupInfoObjList.Add(deDupInfoObj);
                            slNo = slNo + 1;
                            recordCount = recordCount + 1;
                            if (recordCount >= 500)
                            {
                                StringBuilder responseTable = new StringBuilder();
                                //PrintProgressBar();
                                returnvalue += deDupInfoManagerObj.SendDedupeInToDB(deDupInfoObjList);
                                HiddenFieldcount.Value = returnvalue.ToString();
                                //Session["uploadCount"] = returnvalue.ToString();
                                deDupInfoObjList.Clear();
                                recordCount = 0;
                                //ClearProgressBar();
                                responseTable.Append("<table id='responseTable' name='responseTable' bordercolor='#F3F2F7' width='50%'  border='2' cellpadding='0px' cellspacing='0px' >");
                                responseTable.Append("<tr><td><b>Please wait data uploading..</b></td></tr>");
                                responseTable.Append("<tr><td><b>" + returnvalue.ToString() + " Data has been upload complete</b></td></tr>");
                                responseTable.Append("<tr><td><b>&nbsp</b></td></tr></table>");
                                HttpContext.Current.Response.Write(responseTable.ToString());
                                HttpContext.Current.Response.Flush();
                            }
                        }
                    }
                    if (recordCount < 500 && deDupInfoObjList.Count > 0)
                    {
                        StringBuilder responseTable1 = new StringBuilder();
                        //PrintProgressBar();
                        returnvalue += deDupInfoManagerObj.SendDedupeInToDB(deDupInfoObjList);
                        HiddenFieldcount.Value = returnvalue.ToString();
                        deDupInfoObjList.Clear();
                        recordCount = 0;
                        //ClearProgressBar();
                        responseTable1.Append("<table id='responseTable' name='responseTable' bordercolor='#F3F2F7' width='50%'  border='0px' cellpadding='0px' cellspacing='0px' >");
                        responseTable1.Append("<tr><td><b>Please wait data uploading..</b></td></tr>");
                        responseTable1.Append("<tr><td><b>" + returnvalue.ToString() + " Data has been upload complete</b></td></tr>");
                        responseTable1.Append("<tr><td><b>&nbsp</b></td></tr></table>");
                        HttpContext.Current.Response.Write(responseTable1.ToString());
                        HttpContext.Current.Response.Flush();

                        //Session["uploadCount"] = returnvalue.ToString();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "LoadPopupWindows();", true);

                    }
                


                }
                catch (Exception ex)
                {
                    Alert.Show(ex.Message);
                    return;
                }
            }
            //if (deDupInfoObjList.Count > 0)
            //{
            //    UploadDNCInfoList(deDupInfoObjList);
            //}
        }
        public static void PrintProgressBar()
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("<div id='updiv' style='Font-weight:bold;font-size:11pt;Left:320px;COLOR:black;font-family:verdana;Position:absolute;Top:140px;Text-Align:center;'>");
            sb.Append("&nbsp;<script> var up_div=document.getElementById('updiv');up_div.innerText='';</script>");
            sb.Append("<script language=javascript>");
            sb.Append("var dts=0; var dtmax=20;");
            sb.Append("function ShowWait(){var output;output='Please wait while uploading!';dts++;if(dts>=dtmax)dts=1;");
            sb.Append("for(var x=0;x < dts; x++){output+='';}up_div.innerText=output;up_div.style.color='red';}");
            sb.Append("function StartShowWait(){up_div.style.visibility='visible';ShowWait();window.setInterval('ShowWait()',100);}");
            sb.Append("StartShowWait();</script>");
            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.Flush();
        }
        public static void ClearProgressBar()
        {

            StringBuilder sbc = new StringBuilder();
            sbc.Append("<script language='javascript'>");
            sbc.Append("alert('Upload process completed successfully!');");
            sbc.Append("up_div.style.visibility='hidden';");
            sbc.Append("history.go(-1)");
            sbc.Append("</script>");
            HttpContext.Current.Response.Write(sbc);

        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0 && sMessage != "")
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }
        private void UploadDNCInfoList(List<DeDupInfo> deDupInfoObjList)
        {
            int ListInsert = 0;
            if (deDupInfoObjList.Count > 0)
            {
                ListInsert = deDupInfoManagerObj.SendDedupInfoInToDB(deDupInfoObjList);
            }
            if (ListInsert == INSERTE || ListInsert == UPDATE)
            {
                Alert.Show("Upload Successfully");
            }
            else
            {
                Alert.Show("Error in data Please check excel file");
            }
        }
        public static string UploadFile(string destUrl, string sourcePath)
        {

            try
            {

                Uri destUri = new Uri(destUrl);

                FileStream inStream = File.OpenRead(sourcePath);

                WebRequest req = WebRequest.Create(destUri);

                req.Method = "PUT";

                req.Headers.Add("Overwrite", "F");

                req.Timeout = System.Threading.Timeout.Infinite;

                req.Credentials = CredentialCache.DefaultCredentials;

                Stream outStream = req.GetRequestStream();

                string status = CopyStream(inStream, outStream);

                if (status == "success")
                {

                    outStream.Close();

                    WebResponse ores = req.GetResponse();

                    return "success";

                }

                else
                {

                    return status;

                }

            }

            catch (WebException we)
            {

                return we.Message;

            }

            catch (System.Exception ee)
            {

                return ee.Message;

            }

        }

        public static string DownloadFile(string sourceUrl, string destFolder)
        {

            try
            {

                System.Uri sourceUri = new System.Uri(sourceUrl);

                WebRequest req = WebRequest.Create(sourceUri);

                req.Method = "GET";

                req.Timeout = System.Threading.Timeout.Infinite;

                req.Credentials = CredentialCache.DefaultCredentials;

                WebResponse res = req.GetResponse();

                Stream inStream = res.GetResponseStream();

                FileStream fs = new FileStream(destFolder, FileMode.OpenOrCreate);

                string status = CopyStream(inStream, fs);

                if (status == "success")
                {

                    inStream.Close();

                    return "success";

                }

                else
                {

                    inStream.Close();

                    return status;

                }

            }

            catch (WebException we)
            {

                return we.Message;

            }

            catch (System.Exception ee)
            {

                return ee.Message;

            }

        }

        private static string CopyStream(Stream inStream, Stream outStream)
        {

            try
            {

                byte[] buffer = new byte[1024];

                for (; ; )
                {

                    int numBytesRead = inStream.Read(buffer, 0, buffer.Length);

                    if (numBytesRead <= 0)

                        break;

                    outStream.Write(buffer, 0, numBytesRead);

                }

                inStream.Close();

                return "success";

            }

            catch (System.Exception ee)
            {

                return ee.Message;

            }

        }
        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    System.Net.WebClient client = new WebClient();
        //    Uri uri = new Uri(Request.Url.ToString());
        //    //uri = Request.Url;
        //    string destFolder = "";
        //    //if (Request.IsLocal)
        //    //{
        //    //    destFolder = "C:\\BranchInformation.xls";
        //    //}
        //    string fileName = "";
        //    destFolder = "C:\\BranchInformation.xls";
        //    string path =Request.PhysicalApplicationPath;
        //    path += @"Uploads\";
        //    if (FileUpload.HasFile)
        //    {
        //        fileName = Server.HtmlEncode(FileUpload.FileName);
        //        path += fileName;
        //        FileUpload.SaveAs(path);
        //        //DownloadFile(path, destFolder);
        //        client.DownloadFile(uri, "BranchInformation.xls");
        //    }
        //}
    }
}
