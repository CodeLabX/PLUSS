﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_CPVTemplateUI" MasterPageFile="~/UI/SupportMasterPage.master" Codebehind="CPVTemplateUI.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>CPV Template</title>
    <script language="javascript" type="text/javascript">
        function PrintDiv()
        {
            var myContentToPrint = document.getElementById("div1");
            var myWindowToPrint = window.open('','','width=800,height=600,toolbar=0,scrollbars=-1,status=0,resizable=-1,location=0,directories=0');
            myWindowToPrint.document.write(myContentToPrint.innerHTML);
            myWindowToPrint.document.close();
            myWindowToPrint.focus();
            myWindowToPrint.print();
            myWindowToPrint.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <div id="div1">
        <table class="style1">
            <tr>
                <td colspan="2" align="center" style="font-size: 16px; font-weight: bold;">
                    CPV Template
                </td>
            </tr>
            <tr>
                <td align="left">
                    Loan Locator ID :
                </td>
                <td align="left">
                    <asp:TextBox ID="loanLocatorIdTextBox" runat="server" Width="150px" MaxLength="9"></asp:TextBox>
                    <asp:Button ID="findButton" runat="server" Width="20px" Text="..." OnClick="findButton_Click" />
                    &nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="loanLocatorIdTextBox" SetFocusOnError="true">
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="digit only"
                        ControlToValidate="loanLocatorIdTextBox" ValidationExpression="^[0-9]+$">
                    </asp:RegularExpressionValidator>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    Date :
                </td>
                <td align="left">
                    <asp:TextBox ID="dateTextBox1" runat="server" Width="150px"></asp:TextBox>

      
                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                        ControlToValidate="dateTextBox1" Enabled="false" SetFocusOnError="true">
                    </asp:RequiredFieldValidator>--%>

                </td>
            </tr>
            <tr>
                <td align="left" bgcolor="#999999" colspan="2" style="font-weight: bold; color: White;">
                    Personal Details
                </td>
            </tr>
            <tr>
                <td align="left">
                    Applicant's Name :
                </td>
                <td align="left">
                    <asp:TextBox ID="applicantNameTextBox" runat="server" Width="250px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Profession :
                </td>
                <td align="left">
                    <asp:TextBox ID="professionTextBox" runat="server" Width="250px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Current Residence Address :
                </td>
                <td align="left" class="style2" valign="top">
                    <asp:TextBox ID="resAddressTextBox" runat="server" Rows="2" TextMode="MultiLine"
                        Width="450px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Residence Telephone No. :
                </td>
                <td align="left">
                    <asp:TextBox ID="resPhoneTextBox" runat="server" Width="250px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Cell Phone No. :
                </td>
                <td align="left">
                    <asp:TextBox ID="cellPhoneTextBox" runat="server" Width="250px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" bgcolor="#999999" colspan="2" style="font-weight: bold; color: White;">
                    Work Place Details
                </td>
            </tr>
            <tr>
                <td>
                    Office Name :
                </td>
                <td align="left">
                    <asp:TextBox ID="officeNameTextBox" runat="server" Width="250px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Office Address :
                </td>
                <td>
                    <asp:TextBox ID="officeAddTextBox" runat="server" Rows="2" TextMode="MultiLine" Width="450px"
                        ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Office Telephone No. :
                </td>
                <td align="left">
                    <asp:TextBox ID="officeTelNoTextBox" runat="server" Width="250px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Office Cell Phone No. :
                </td>
                <td align="left">
                    <asp:TextBox ID="officeCellNoTextBox" runat="server" Width="250px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" bgcolor="#999999" colspan="2" style="font-weight: bold; color: White;">
                    Guarantor Details
                </td>
            </tr>
            <tr>
                <td>
                    Guarantor&#39;s Name :
                </td>
                <td align="left">
                    <asp:TextBox ID="guarantorNameTextBox" runat="server" Width="250px" 
                        MaxLength="249"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                        EnableClientScript="false" ControlToValidate="guarantorNameTextBox" SetFocusOnError="true"> 
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Designation :
                </td>
                <td align="left">
                    <asp:TextBox ID="designationTextBox" runat="server" Width="250px" 
                        MaxLength="99"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Contact Address :
                </td>
                <td align="left">
                    <asp:TextBox ID="contactAddressTextBox" runat="server" TextMode="MultiLine" Width="450px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone No. :
                </td>
                <td align="left">
                    <asp:TextBox ID="telNoTextBox" runat="server" Width="250px" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="^[0-9]+$"
                        EnableClientScript="true" ControlToValidate="telNoTextBox" 
                        ErrorMessage="invalid number given"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Cell Phone No. :
                </td>
                <td align="left">
                    <asp:TextBox ID="cellNoTextBox" runat="server" Width="250px" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^[0-9]+$"
                        EnableClientScript="true" ControlToValidate="cellNoTextBox" 
                        ErrorMessage="invalid number given"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Guarantee Amount :
                </td>
                <td align="left">
                    <asp:TextBox ID="guaranteeAmountTextBox" runat="server" Width="250px" 
                        MaxLength="100"></asp:TextBox>
<%--                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                        EnableClientScript="false" ControlToValidate="guaranteeAmountTextBox" SetFocusOnError="true"> 
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^[0-9]+$"
                        ControlToValidate="guaranteeAmountTextBox" ErrorMessage="invalid amount given"
                        >
                    </asp:RegularExpressionValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    Relationship with Applicant
                    :</td>
                <td align="left">
                    <asp:TextBox ID="relationshipTextBox" runat="server" Width="250px" 
                        MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="emailAddressLabel" runat="server" Text="Email Address:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="emailAddressTextBox" runat="server" Width="250px" 
                        MaxLength="250"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|[a-zA-Z]{2})$"
                        EnableClientScript="true" ControlToValidate="emailAddressTextBox" ErrorMessage="invalid e-mail address">
                    </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="sendButton" runat="server" Text="Send" Width="95px" Height="30px"
                        OnClick="sendButton_Click" />
                    <asp:Button ID="saveButton" runat="server" Text="Save" Width="95px" Height="30px"
                        OnClick="saveButton_Click" />
                    <asp:Button ID="printButton" runat="server" Text="Print" Width="95px" Height="30px"
                        OnClick="printButton_Click" />
                    <asp:Button ID="clearButton" runat="server" Text="Clear" Width="95px" Height="30px"
                        OnClick="clearButton_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
