﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/LoanLocatorAdmin.master" AutoEventWireup="true" CodeBehind="ProductInfo.aspx.cs" Inherits="PlussAndLoan.UI.ProductInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }

        .auto-style2 {
            height: 21px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div style="margin-left: 100px;" id="productViewDiv" runat="server">
        <table cellpadding="0" cellspacing="0">
            <%--           <tr>
                <td colspan="4">                   
                    <table class="GvTableHeader" width="444px">
                        <thead>
                            <th class="auto-style1"></th>
                            <th class="auto-style1">Product wise supervisor</th>
                            <th class="auto-style1"></th>
                        </thead>
                        <tbody>
                            <tr>
                            <td align="center" width="184px" class="auto-style2">
                                Product Name</td>
                           
                            <td align="center" width="220px" class="auto-style2">
                                Supervisor name</td>
                            <td align="center" width="45px" class="auto-style2">
                                Action</td>
                        </tr>
                        </tbody>
                        
                    </table>
                </td>                
            </tr>--%>
            <tr>
                <td colspan="4">
                    <table class="GvTableHeader" width="444px">
                        <thead>
                            <th class="auto-style1" width="120px"></th>
                            <th class="auto-style1">Product wise supervisor</th>
                            <th class="auto-style1"></th>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                     <asp:HiddenField ID="idHiddenField" runat="server" />
                    <div style="overflow-y: scroll; height: 380px; width: 444px;" class="GridviewBorder">

                        <asp:GridView ID="ProductGrid" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            Font-Size="11px" ForeColor="#333333" PageSize="8"
                            
                            Width="431px" EnableModelValidation="True" GridLines="None" OnSelectedIndexChanged="ProductGrid_SelectedIndexChanged">
                            <RowStyle BackColor="#EFF3FB" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <%--  <asp:TemplateField HeaderText="Id" HeaderStyle-Width="150px" Visible="false">
                                            <ItemTemplate>
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="150px"></HeaderStyle>
                                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <a href='ProductAssign.aspx?id=<%#Eval("ProductId")%>' style="color: black; text-decoration: underline"> Assign </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    --%>

                                <asp:TemplateField HeaderText="Product Name" HeaderStyle-Width="184px">
                                    <ItemTemplate>

                                        <asp:Label Visible="false" ID="ProdIdLabel" runat="server" Text='<%# Bind("ProductId") %>'></asp:Label>
                                        <asp:Label ID="productNameLabel" runat="server" Text='<%# Eval("ProductName") %>' Width="180px"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="184px"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Supervisor name" HeaderStyle-Width="220px">
                                    <ItemTemplate>
                                        <asp:Label ID="learnerId" runat="server" Text='<%# Bind("IdLearner") %>' Width="0px" Visible="false"></asp:Label>
                                        <asp:Label ID="NameLearner" runat="server" Text='<%# Eval("NameLearner") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="220px"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-Width="40px" Visible="true">
                                    <ItemTemplate>
                                        <asp:LinkButton Text="Assign" ID="lnkSelect" runat="server" CommandName="Select"/>
                                    </ItemTemplate>
                                    <HeaderStyle Width="45px" HorizontalAlign="right"></HeaderStyle>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="APPL. ID">
                                    <ItemTemplate>
                                        <a href='loc_EditLoan.aspx?id=<%#Eval("LLID")%>' style="color: black; text-decoration: underline"><%#Eval("LLID")%></a>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                &nbsp;&nbsp;No data found.
                            </EmptyDataTemplate>
                            <EditRowStyle BackColor="#2461BF" />
                            <EmptyDataRowStyle Font-Size="11px" Font-Names="Tahoma" ForeColor="DarkRed" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <FooterStyle CssClass="GvFixedFooter" BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
        <br />


    </div>
    <div style="margin-left: 100px;" id="productEditDiv" runat="server">
        <table width="444px">
            <thead class="GvTableHeader" width="444px">
                <th>
                    <%--<td width="120px"></td>--%>
                    <td>Set product supervisor
                    </td>
                </th>
            </thead>
            <tbody>
                <tr>
                    <td>Product Name
                    </td>
                    <td>
                        <asp:Label runat="server" ID="prodName"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Staff Id
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="learnerName"></asp:TextBox>
                         <asp:Label ID="errorMsgLabel" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnSave" Text="Submit" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>
