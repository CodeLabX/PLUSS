﻿using AzUtilities;
using Bat.Common;
using BusinessEntities;
using DAL.SecurityMatrixGateways;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI.Deletable
{
    public partial class MultiLevelMenu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        public void GetMenu()
        {
            string menus = @"<nav class='navbar' role='navigation' aria-label='main navigation'><div class='navbar-menu'>";

            User userT = (User)Session["UserDetails"];
            PageAccessPermission _repo = new PageAccessPermission();
            DataTable table = _repo.GetMenusForUser(userT);

            foreach (DataRow item in table.Rows)
            {
                string mItem = GetChildMenuItems(item["ParentId"].ToString(), table);

                if (!string.IsNullOrEmpty(mItem))
                {

                }
                else
                {
                    menus += "<a class='navbar-item' " + item["URL"] + ">" + item["Description"] + "</a>";
                }
            }

            menus += "</div></nav>";
        }

        public string GetChildMenuItems(string parentId, DataTable table)
        {

            return "";
        }
    }
}