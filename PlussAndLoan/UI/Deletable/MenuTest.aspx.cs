﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Text;
using BusinessEntities;
using BLL.SecurityMatrixBLL;
using BusinessEntities.SecurityMatrixEntities;
using System.Web.UI.HtmlControls;

namespace PlussAndLoan.UI.Deletable
{
    public partial class MenuTest : System.Web.UI.Page
    {
        private PageAccessPermissionManager _permissions = null;
        DataTable allCategories = new DataTable();
        string conString = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            _permissions = new PageAccessPermissionManager();
            if (!IsPostBack)
            {
            }
            conString = WebConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            GetMenuString();
        }


        private List<MenuModel> GetMenuItems()
        {
            User user = (User)Session["UserDetails"];
            List<MenuModel> list = _permissions.GetMenuListForUser(user);

            return list;
        }

        public void GetMenuString()
        {
            List<MenuModel> list = GetMenuItems();
            User user = (User)Session["UserDetails"];

            string menuStr = "";

            if (list.Count > 0)
            {
                menuStr = GetSubMenus(list, 0);
            }
            NavItem.InnerHtml = "<ul id='menu menu-ul'>" + menuStr + "</ul>";    
        }

        private string GetSubMenus(IEnumerable<MenuModel> siteMenu, Nullable<int> parentId)
        {
            StringBuilder strBuilder = new StringBuilder();

            foreach (var item in siteMenu.Where(a => a.ParentId.Equals(parentId)))
            {
                strBuilder.Append(string.Format("<li class='parent'><a href='{0}'>{1}</a>", item.Url, item.Description));
                int subMenuCount = siteMenu.Where(a => a.ParentId.Equals(parentId)).Count();

                if (subMenuCount > 0)
                {
                    strBuilder.Append("<ul class='child'>");
                    strBuilder.Append(GetSubMenus(siteMenu, item.MenuId));
                    strBuilder.Append("</ul>");
                }
                strBuilder.Append("</li>");
            }

            return strBuilder.ToString();
        }
    }
}