﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiLevelMenu.aspx.cs" Inherits="PlussAndLoan.UI.Deletable.MultiLevelMenu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .menu {
            width: 500px;
            margin: 0px auto;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: 14px;
        }

            .menu ul li a:link, div ul li a:visited {
                display: block;
                background-color: #f1f1f1;
                color: #000;
                text-align: center;
                text-decoration: none;
                padding: 4px;
                border-bottom: 1px solid #fff;
                width: 150px;
            }

            .menu ul li a:hover {
                background-color: #ccc;
            }

            .menu ul li ul li a:link, li ul li a:visited {
                display: block;
                background-color: #f1f1f1;
                color: #000;
                text-align: center;
                text-decoration: none;
                padding: 4px;
                border-bottom: 1px solid #fff;
                width: 150px;
            }

            .menu ul li ul li a:hover {
                background-color: #ccc;
            }

            .menu ul {
                list-style-type: none;
                margin: 0px;
                padding: 0px;
            }

                .menu ul li {
                    float: left;
                    margin-left: 5px;
                }

                    .menu ul li ul li {
                        float: none;
                        margin-left: 0px;
                    }

                    .menu ul li ul {
                        display: none;
                    }

            .menu li:hover ul {
                display: block;
            }
    </style>
</head>
<body>
    
    <form id="form1" runat="server">
        <asp:Repeater ID="rptCategories" runat="server"
        OnItemDataBound="rptCategories_ItemDataBound">
        <HeaderTemplate>
            <div class="menu">
                <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <a href='#'>< 
                            %#Eval("Name") %></a>
                <asp:Literal ID="ltrlSubMenu"
                    runat="server"></asp:Literal>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul></div>
        </FooterTemplate>
    </asp:Repeater>
        <div>
        </div>
    </form>
</body>
</html>
