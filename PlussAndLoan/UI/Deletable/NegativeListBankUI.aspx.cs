﻿using System;
using System.Collections.Generic;
using DAL;

namespace PlussAndLoan.UI.MFU
{
    public partial class UI_NegativeListBank : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string viewData = "";
                List<string> viewList = new List<string> ();
                for ( int i = 0; i < 3; i++ )
                {
                    viewData = "";
                    viewList.Add ( viewData );
                }

                negativeListBankGridView.DataSource = viewList;
                negativeListBankGridView.DataBind ();
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Negative Bank List");
            }
        }
    }
}
