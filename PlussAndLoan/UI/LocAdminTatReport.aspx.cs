﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.LoanLocatorReports;
using BusinessEntities;

namespace PlussAndLoan.UI
{
    public partial class LocAdminTatReport : System.Web.UI.Page
    {
        private readonly LoanApplicationManager _loanApplicationManagerObj = new LoanApplicationManager();
        private readonly LocLoanReportManager _loanReportManager = new LocLoanReportManager();
        private readonly LocAdminTatReportManager _locAdminManager = new LocAdminTatReportManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                productDropDownList.DataSource = AllProductList();
                productDropDownList.DataTextField = "ProductName";
                productDropDownList.DataValueField = "ProductId";
                productDropDownList.DataBind();

                DayDropDownList.DataSource = AllDayList();
                DayDropDownList.DataBind();
                DayDropDownList.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;
                MonthDropDownList.DataSource = AllMonthList();
                MonthDropDownList.DataTextField = "Value";
                MonthDropDownList.DataValueField = "Key";
                MonthDropDownList.DataBind();
                MonthDropDownList.SelectedIndex = DateTime.Now.Month - 1;
                YearDropDownList.DataSource = AllyearList();
                YearDropDownList.DataBind();
                YearDropDownList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;

                ToDateDayList.DataSource = AllDayList();
                ToDateDayList.DataBind();
                ToDateDayList.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;
                ToDateMonthList.DataSource = AllMonthList();
                ToDateMonthList.DataTextField = "Value";
                ToDateMonthList.DataValueField = "Key";
                ToDateMonthList.DataBind();
                ToDateMonthList.SelectedIndex = DateTime.Now.Month - 1;
                ToDateYearList.DataSource = AllyearList();
                ToDateYearList.DataBind();
                ToDateYearList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;

                TatProductDropDown.DataSource = AllProductList();
                TatProductDropDown.DataTextField = "ProductName";
                TatProductDropDown.DataValueField = "ProductId";
                TatProductDropDown.DataBind();

                TatDayDropDown.DataSource = AllDayList();
                TatDayDropDown.DataBind();
                TatDayDropDown.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;
                TatMonthDropDown.DataSource = AllMonthList();
                TatMonthDropDown.DataTextField = "Value";
                TatMonthDropDown.DataValueField = "Key";
                TatMonthDropDown.DataBind();
                TatMonthDropDown.SelectedIndex = DateTime.Now.Month - 1;
                TatYearDropDown.DataSource = AllyearList();
                TatYearDropDown.DataBind();
                TatYearDropDown.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;


                ByDayDropDown.DataSource = AllDayList();
                ByDayDropDown.DataBind();
                ByDayDropDown.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;
                ByMonthDropDown.DataSource = AllMonthList();
                ByMonthDropDown.DataTextField = "Value";
                ByMonthDropDown.DataValueField = "Key";
                ByMonthDropDown.DataBind();
                ByMonthDropDown.SelectedIndex = DateTime.Now.Month - 1;
                ByYearDropDown.DataSource = AllyearList();
                ByYearDropDown.DataBind();
                ByYearDropDown.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;

                InProductDropDown.DataSource = AllProductList();
                InProductDropDown.DataTextField = "ProductName";
                InProductDropDown.DataValueField = "ProductId";
                InProductDropDown.DataBind();

                InDayDropDown.DataSource = AllDayList();
                InDayDropDown.DataBind();
                InDayDropDown.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;
                InMonthDropDown.DataSource = AllMonthList();
                InMonthDropDown.DataTextField = "Value";
                InMonthDropDown.DataValueField = "Key";
                InMonthDropDown.DataBind();
                InMonthDropDown.SelectedIndex = DateTime.Now.Month - 1;
                InYearDropDown.DataSource = AllyearList();
                InYearDropDown.DataBind();
                InYearDropDown.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;


                InToDayDropDown.DataSource = AllDayList();
                InToDayDropDown.DataBind();
                InToDayDropDown.Items.FindByText(DateTime.Now.Day.ToString()).Selected = true;
                InToMonthDropDown.DataSource = AllMonthList();
                InToMonthDropDown.DataTextField = "Value";
                InToMonthDropDown.DataValueField = "Key";
                InToMonthDropDown.DataBind();
                InToMonthDropDown.SelectedIndex = DateTime.Now.Month - 1;
                InToYearDropDown.DataSource = AllyearList();
                InToYearDropDown.DataBind();
                InToYearDropDown.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;

                DropDownYearList.DataSource = AllyearList();
                DropDownYearList.DataBind();
                DropDownYearList.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
                DropDownMonthList.DataSource = AllMonthList();
                DropDownMonthList.DataTextField = "Value";
                DropDownMonthList.DataValueField = "Key";
                DropDownMonthList.DataBind();
                DropDownMonthList.SelectedIndex = DateTime.Now.Month - 1;

                DropDownProductList.DataSource = AllProductList();
                DropDownProductList.DataTextField = "ProductName";
                DropDownProductList.DataValueField = "ProductId";
                DropDownProductList.DataBind();

                ProductListDropDown.DataSource = AllProductList();
                ProductListDropDown.DataTextField = "ProductName";
                ProductListDropDown.DataValueField = "ProductId";
                ProductListDropDown.DataBind();
                AppMonthDropDown.DataSource = AllMonthList();
                AppMonthDropDown.DataTextField = "Value";
                AppMonthDropDown.DataValueField = "Key";
                AppMonthDropDown.DataBind();
                AppMonthDropDown.SelectedIndex = DateTime.Now.Month - 1;
                AppYearDropDown.DataSource = AllyearList();
                AppYearDropDown.DataBind();
                AppYearDropDown.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
        }

        #region DropDown Value List

        public List<int> AllyearList()
        {
            List<int> yearList = new List<int>();
            for (int i = 2004; i < 2051; i++)
            {
                yearList.Add(i);
            }
            return yearList;

        }

        public List<int> AllDayList()
        {
            List<int> dayList = new List<int>();
            for (int i = 1; i < 32; i++)
            {
                dayList.Add(i);
            }
            return dayList;

        }

        public Dictionary<int, string> AllMonthList()
        {
            IDictionary<int, string> months = new Dictionary<int, string>();
            months.Add(1, "January");
            months.Add(2, "February");
            months.Add(3, "March");
            months.Add(4, "April");
            months.Add(5, "May");
            months.Add(6, "June");
            months.Add(7, "July");
            months.Add(8, "August");
            months.Add(9, "September");
            months.Add(10, "October");
            months.Add(11, "November");
            months.Add(12, "December");

            return (Dictionary<int, string>) months;
        }


        public List<Product> AllProductList()
        {
            List<Product> productListObj = _loanApplicationManagerObj.GetAllProduct();
            var productObj = new Product {ProductId = 0, ProductName = "Select Product"};
            productListObj.Insert(0, productObj);
            return productListObj;
        }

        #endregion

        #region Application Status Wise Reprot

        protected void btnShow_Click(object sender, EventArgs e)
        {
            int y1 = Convert.ToInt32(YearDropDownList.SelectedValue);
            string m1 = MonthDropDownList.SelectedValue;
            string d1 = DayDropDownList.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;

            Session["dateOne"] = date1;
            int y2 = Convert.ToInt32(ToDateYearList.SelectedValue);
            string m2 = ToDateMonthList.SelectedValue;
            string d2 = ToDateDayList.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;
            Session["dateTwo"] = date2;
            int productId = Convert.ToInt32(productDropDownList.SelectedValue);
            string productName = productDropDownList.SelectedItem.ToString();
            Session["product"] = productName;
            int status = Convert.ToInt32(appr_disb.SelectedValue);
            string statusName = appr_disb.SelectedItem.ToString();
            Session["status"] = statusName;

            var roleCutDate = WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"];

            var ss = ApplicationStatusWiseReprot(date1, date2, productId, status, roleCutDate);

            Session["results"] = ss;
            Response.Redirect("ApplicationStatusWiseReprot.aspx");
        }

        public DataTable ApplicationStatusWiseReprot(string date1, string date2, int productId, int status, string roleCutDate)
        {
            var results = _loanReportManager.ApplicationStatusWiseReprot(date1, date2, productId, status, roleCutDate);
            return results;
        }

        protected void btnDetailReport_Click(object sender, EventArgs e)
        {
            int y1 = Convert.ToInt32(YearDropDownList.SelectedValue);
            string m1 = MonthDropDownList.SelectedValue;
            string d1 = DayDropDownList.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string date1 = y1 + "-" + m1 + "-" + d1;

            Session["dateOne"] = date1;
            int y2 = Convert.ToInt32(ToDateYearList.SelectedValue);
            string m2 = ToDateMonthList.SelectedValue;
            string d2 = ToDateDayList.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string date2 = y2 + "-" + m2 + "-" + d2;
            Session["dateTwo"] = date2;
            int productId = Convert.ToInt32(productDropDownList.SelectedValue);
            string productName = productDropDownList.SelectedItem.ToString();
            Session["product"] = productName;

            var roleCutDate = WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"];

            var tables = ApplicationDetailsReprot(date1, date2, productId, roleCutDate);
            Session["table"] = tables;
            Response.Redirect("ApplicationDetailsReport.aspx");
        }

        public DataTable ApplicationDetailsReprot(string date1, string date2, int productId, string roleCutDate)
        {
            var results = _loanReportManager.ApplicationDetailsReprot(date1, date2, productId, roleCutDate);
            return results;
        }

        #endregion

        #region Individual TAT Report

        protected void btnIndividualTat_Click(object sender, EventArgs e)
        {
            int productId = Convert.ToInt32(TatProductDropDown.SelectedValue);
            string productName = TatProductDropDown.SelectedItem.ToString();
            Session["pro"] = productName;
            string d1 = TatDayDropDown.SelectedValue;
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string m1 = TatMonthDropDown.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            int y1 = Convert.ToInt32(TatYearDropDown.SelectedValue);

            string date1 = y1 + "-" + m1 + "-" + d1;
            Session["dateOne"] = date1;

            string d2 = ByDayDropDown.SelectedValue;
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string m2 = ByMonthDropDown.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            int y2 = Convert.ToInt32(ByYearDropDown.SelectedValue);
            string date2 = y2 + "-" + m2 + "-" + d2;
            Session["dateTwo"] = date2;

            var roleCutDate = WebConfigurationManager.AppSettings["RoleChangeConsiderationDateTime"];

            var tat = IndividualTatReport(date1, date2, productId, roleCutDate);
            Session["tatReport"] = tat;
            Response.Redirect("IndividualTatReport.aspx");
        }

        public DataTable IndividualTatReport(string date1, string date2, int productId, string roleCutDate)
        {
            var data = _locAdminManager.IndividualTatReport(date1, date2, productId, roleCutDate);
            return data;
        }

        #endregion

        #region Individual TAT by Date

        protected void btnTatByDate_Click(object sender, EventArgs e)
        {
            int productId = Convert.ToInt32(InProductDropDown.SelectedValue);
            string productName = InProductDropDown.SelectedItem.ToString();
            Session["prod"] = productName;
            string d1 = InDayDropDown.SelectedValue;
            if (d1.Length == 1)
            {
                d1 = "0" + d1;
            }
            string m1 = InMonthDropDown.SelectedValue;
            if (m1.Length == 1)
            {
                m1 = "0" + m1;
            }
            int y1 = Convert.ToInt32(InYearDropDown.SelectedValue);

            string date1 = y1 + "-" + m1 + "-" + d1;
            Session["d1"] = date1;
            string d2 = InToDayDropDown.SelectedValue;
            if (d2.Length == 1)
            {
                d2 = "0" + d2;
            }
            string m2 = InToMonthDropDown.SelectedValue;
            if (m2.Length == 1)
            {
                m2 = "0" + m2;
            }
            int y2 = Convert.ToInt32(InToYearDropDown.SelectedValue);
            string date2 = y2 + "-" + m2 + "-" + d2;
            Session["d2"] = date2;
            var inTat = IndividualTatReportByDate(date1, date2, productId);
            Session["inTAT"] = inTat;
            Response.Redirect("IndividualTatByDate.aspx");
        }

        public DataTable IndividualTatReportByDate(string date1, string date2, int productId)
        {
            var data = _locAdminManager.IndividualTatReportByDate(date1, date2, productId);
            return data;
        }

        #endregion

        #region View Quick  Report

        protected void btnQuickReportMonthly_Click(object sender, EventArgs e)
        {
            int productId = Convert.ToInt32(DropDownProductList.SelectedValue);
            Session["proId"] = productId;
            string productName = DropDownProductList.SelectedItem.ToString();
            Session["ProName"] = productName;
            string month = DropDownMonthList.SelectedValue;
            Session["monthNo"] = month;
            string monthName = DropDownMonthList.SelectedItem.ToString();
            Session["monthName"] = monthName;
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            int year = Convert.ToInt32(DropDownYearList.SelectedValue);
            Session["year"] = year;
           
            int buttonId = 1;
            Session["button"] = buttonId;
            var res = GetQuickReport(year, month, productId, buttonId);
            Session["res"] = res;
            var resData = GetCalculatingVol(year, month, productId, buttonId);
            Session["resData"] = resData;
            Response.Redirect("QuickReport.aspx");
        }

        protected void btnQuickReportYearly_Click(object sender, EventArgs e) //Submit222 
        {
            int productId = Convert.ToInt32(DropDownProductList.SelectedValue);
            Session["proId"] = productId;
            string productName = DropDownProductList.SelectedItem.ToString();
            Session["ProName"] = productName;
            string month = DropDownMonthList.SelectedValue;
            Session["monthNo"] = month;
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            string monthName = DropDownMonthList.SelectedItem.ToString();
            Session["monthName"] = monthName;
            int year = Convert.ToInt32(DropDownYearList.SelectedValue);
            Session["year"] = year;
          
            int buttonId = 2;
            Session["button"] = buttonId;
            var res = GetQuickReport(year, month, productId, buttonId);
            Session["res"] = res;
            var resData = GetCalculatingVol(year, month, productId, buttonId);
            Session["resData"] = resData;
            Response.Redirect("QuickReport.aspx");
        }

        public DataTable GetQuickReport(int year, string month, int productId, int buttonId)
        {
            var res = _locAdminManager.GetQuickReport(year, month, productId, buttonId);
            return res;
        }

        public DataTable GetCalculatingVol(int year, string month, int productId, int buttonId)
        {
            var resData = _locAdminManager.GetCalculatingVol(year, month, productId, buttonId);
            return resData;
        }

      
     
    #endregion


        #region List Of Application Deferred/Declined- According To Reasons
        protected void btnAppShow_Click(object sender, EventArgs e)
        {
            int producId = Convert.ToInt32(ProductListDropDown.SelectedValue);
            string productName = ProductListDropDown.SelectedItem.ToString();
            Session["proName"] = productName;
            int status = Convert.ToInt32(statDropDownList.SelectedValue);
            string stat = statDropDownList.SelectedItem.ToString();
            Session["state"] = stat;
            string month = AppMonthDropDown.SelectedValue;
            string m = AppMonthDropDown.SelectedItem.ToString();
            Session["month"] = m; 
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            int year = Convert.ToInt32(AppYearDropDown.SelectedValue);
            Session["year"] = year;
            string date = "%" + year + "-" + month + "%";

            var app = ApplicationDeferredOrDeclinedReprot(producId, status, date);
            Session["appReport"] = app;
            Response.Redirect("ApplicationDeferredOrDeclined.aspx");

        }

        public DataTable ApplicationDeferredOrDeclinedReprot(int producId, int status, string date) 
        {
            var results = _loanReportManager.ApplicationDeferredOrDeclinedReprot(producId, status, date);

            return results;
        }

        #endregion

    }
}