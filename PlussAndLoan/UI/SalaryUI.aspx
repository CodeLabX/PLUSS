﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.UI.UI_SalaryUI" MasterPageFile="~/UI/AnalystMasterPage.master" Codebehind="SalaryUI.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function PrintDiv()
        {
            var myContentToPrint = document.getElementById("div1");
            var myWindowToPrint = window.open('','','width=800,height=600,toolbar=0,scrollbars=-1,status=0,resizable=-1,location=0,directories=0');
            myWindowToPrint.document.write(myContentToPrint.innerHTML);
            myWindowToPrint.document.close();
            myWindowToPrint.focus();
            myWindowToPrint.print();
            myWindowToPrint.close();
        }
    </script>

</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
    <div id="div1">
        <table>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    LLID:
                </td>
                <td>
                    <asp:TextBox ID="llidTextBox" runat="server" Width="177px" MaxLength="9"></asp:TextBox>
                    <asp:Button ID="searchButton" runat="server" Text="..." OnClick="searchButton_Click" />
                    &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                        ErrorMessage="digit only" ControlToValidate="llidTextBox" ValidationExpression="^[0-9]+$">
                    </asp:RegularExpressionValidator>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Customer Name:
                </td>
                <td>
                    <asp:TextBox ID="customerNameTextBox" runat="server" Width="355px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Organization Type:
                </td>
                <td>
                    <asp:UpdatePanel runat="server" ID="UpdatePanel39">
                        <ContentTemplate>
                            <asp:DropDownList ID="orgTypeDropDownList" runat="server" Width="355px" OnSelectedIndexChanged="orgTypeDropDownList_SelectedIndexChanged"
                                AutoPostBack="True">
                                <asp:ListItem>MNC/LLC</asp:ListItem>
                                <asp:ListItem>Others</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:TextBox ID="TextBox51" runat="server" Width="240px" ReadOnly="True" Visible="False"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                AMOUNT
                            </td>
                        </tr>
                        <tr>
                            <td>
                                FIXED SALARY
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                    <ContentTemplate>
                                        <asp:TextBox ID="fixedSalaryTextBox" runat="server" OnTextChanged="fixedSalaryTextBox_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel46">
                                    <ContentTemplate>
                                        <asp:Label ID="OtFlightAllLabel" runat="server" Text="50% of (OT+FlightAll) + 50% of others"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel38">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lastAvgTextBox" runat="server" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                NET INCOME
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                                    <ContentTemplate>
                                        <asp:TextBox ID="netIncomeTextBox" runat="server" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td colspan="2">
                    <table border="0">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td bgcolor="#339933" colspan="2" align="center" style="color: White">
                                50%
                            </td>
                            <td bgcolor="#339933" colspan="3" align="center" style="color: White">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel47">
                                    <ContentTemplate>
                                        <asp:Label ID="ParcentChangeLabel" runat="server" Text="50%"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                VARIABLE SALARY
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td bgcolor="#666666" align="center" style="color: White">
                                OT
                            </td>
                            <td bgcolor="#666666" align="center" style="color: White">
                                Conv
                            </td>
                            <td bgcolor="#666666" align="center" style="color: White">
                                OT
                            </td>
                            <td bgcolor="#666666" align="center" style="color: White">
                                T Shift
                            </td>
                            <td bgcolor="#666666" align="center" style="color: White">
                                Others
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel40">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="month1DropDownList" runat="server" Style="margin-right: 3px"
                                            Width="144px" AutoPostBack="True">
                                            <asp:ListItem>Jan</asp:ListItem>
                                            <asp:ListItem>Feb</asp:ListItem>
                                            <asp:ListItem>Mar</asp:ListItem>
                                            <asp:ListItem>Apr</asp:ListItem>
                                            <asp:ListItem>May</asp:ListItem>
                                            <asp:ListItem>Jun</asp:ListItem>
                                            <asp:ListItem>Jul</asp:ListItem>
                                            <asp:ListItem>Aug</asp:ListItem>
                                            <asp:ListItem>Sep</asp:ListItem>
                                            <asp:ListItem>Act</asp:ListItem>
                                            <asp:ListItem>Nov</asp:ListItem>
                                            <asp:ListItem>Dec</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel3">
                                    <ContentTemplate>
                                        <asp:TextBox ID="total50TextBox1" runat="server" Width="82px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel4">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ot50TextBox1" runat="server" Width="65px" OnTextChanged="ot50TextBox1_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel5">
                                    <ContentTemplate>
                                        <asp:TextBox ID="conv50TextBox1" runat="server" Width="65px" OnTextChanged="conv50TextBox1_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel6">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ot20TextBox1" runat="server" Width="65px" OnTextChanged="ot20TextBox1_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel7">
                                    <ContentTemplate>
                                        <asp:TextBox ID="t20TextBox1" runat="server" Width="65px" OnTextChanged="t20TextBox1_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel8">
                                    <ContentTemplate>
                                        <asp:TextBox ID="o20TextBox1" runat="server" Width="65px" OnTextChanged="o20TextBox1_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel41">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="month2DropDownList" runat="server" Width="144px" AutoPostBack="True">
                                            <asp:ListItem>Jan</asp:ListItem>
                                            <asp:ListItem>Feb</asp:ListItem>
                                            <asp:ListItem>Mar</asp:ListItem>
                                            <asp:ListItem>Apr</asp:ListItem>
                                            <asp:ListItem>May</asp:ListItem>
                                            <asp:ListItem>Jun</asp:ListItem>
                                            <asp:ListItem>Jul</asp:ListItem>
                                            <asp:ListItem>Aug</asp:ListItem>
                                            <asp:ListItem>Sep</asp:ListItem>
                                            <asp:ListItem>Act</asp:ListItem>
                                            <asp:ListItem>Nov</asp:ListItem>
                                            <asp:ListItem>Dec</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel9">
                                    <ContentTemplate>
                                        <asp:TextBox ID="total50TextBox2" runat="server" Width="82px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel10">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ot50TextBox2" runat="server" Width="65px" OnTextChanged="ot50TextBox2_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel11">
                                    <ContentTemplate>
                                        <asp:TextBox ID="conv50TextBox2" runat="server" Width="65px" OnTextChanged="conv50TextBox2_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel12">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ot20TextBox2" runat="server" Width="65px" OnTextChanged="ot20TextBox2_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel13">
                                    <ContentTemplate>
                                        <asp:TextBox ID="t20TextBox2" runat="server" Width="65px" OnTextChanged="t20TextBox2_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel14">
                                    <ContentTemplate>
                                        <asp:TextBox ID="o20TextBox2" runat="server" Width="65px" OnTextChanged="o20TextBox2_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel42">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="month3DropDownList" runat="server" Width="144px" AutoPostBack="True">
                                            <asp:ListItem>Jan</asp:ListItem>
                                            <asp:ListItem>Feb</asp:ListItem>
                                            <asp:ListItem>Mar</asp:ListItem>
                                            <asp:ListItem>Apr</asp:ListItem>
                                            <asp:ListItem>May</asp:ListItem>
                                            <asp:ListItem>Jun</asp:ListItem>
                                            <asp:ListItem>Jul</asp:ListItem>
                                            <asp:ListItem>Aug</asp:ListItem>
                                            <asp:ListItem>Sep</asp:ListItem>
                                            <asp:ListItem>Act</asp:ListItem>
                                            <asp:ListItem>Nov</asp:ListItem>
                                            <asp:ListItem>Dec</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel15">
                                    <ContentTemplate>
                                        <asp:TextBox ID="total50TextBox3" runat="server" Width="82px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel16">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ot50TextBox3" runat="server" Width="65px" OnTextChanged="ot50TextBox3_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel17">
                                    <ContentTemplate>
                                        <asp:TextBox ID="conv50TextBox3" runat="server" Width="65px" Height="22px" OnTextChanged="conv50TextBox3_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel18">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ot20TextBox3" runat="server" Width="65px" OnTextChanged="ot20TextBox3_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel19">
                                    <ContentTemplate>
                                        <asp:TextBox ID="t20TextBox3" runat="server" Width="65px" OnTextChanged="t20TextBox3_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel20">
                                    <ContentTemplate>
                                        <asp:TextBox ID="o20TextBox3" runat="server" Width="65px" OnTextChanged="o20TextBox3_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AVERAGE
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel21">
                                    <ContentTemplate>
                                        <asp:TextBox ID="avg50TotalTextBox1" runat="server" Width="82px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel22">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ot50avgTextBox" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel23">
                                    <ContentTemplate>
                                        <asp:TextBox ID="conv50avgTextBox" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel24">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ot20AvgTextBox" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel25">
                                    <ContentTemplate>
                                        <asp:TextBox ID="t20AvgTextBox" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel26">
                                    <ContentTemplate>
                                        <asp:TextBox ID="o20AvgTextBox" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Avg. OT Conv
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel27">
                                    <ContentTemplate>
                                        <asp:TextBox ID="avg50TotalTextBox2" runat="server" Width="107px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox52" runat="server" ReadOnly="True" Visible="False" Width="190px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Avg other variable
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel28">
                                    <ContentTemplate>
                                        <asp:TextBox ID="avg20TotalTextBox" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    *AS PER PAYSLIPS PROVIDED
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox66" runat="server" Width="190px" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Credit Signature
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table style="vertical-align: middle">
                        <tr>
                            <td colspan="3" align="center" bgcolor="#339933" style="color: White">
                                Meal Allowance
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#669999" style="color: White">
                                Month
                            </td>
                            <td bgcolor="#669999" style="color: White">
                                USD All.
                            </td>
                            <td bgcolor="#669999" style="color: White">
                                BDT App
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel43">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="mealMonthDropDownList1" runat="server" Width="65px" AutoPostBack="True">
                                            <asp:ListItem>Jan</asp:ListItem>
                                            <asp:ListItem>Feb</asp:ListItem>
                                            <asp:ListItem>Mar</asp:ListItem>
                                            <asp:ListItem>Apr</asp:ListItem>
                                            <asp:ListItem>May</asp:ListItem>
                                            <asp:ListItem>Jun</asp:ListItem>
                                            <asp:ListItem>Jul</asp:ListItem>
                                            <asp:ListItem>Aug</asp:ListItem>
                                            <asp:ListItem>Sep</asp:ListItem>
                                            <asp:ListItem>Act</asp:ListItem>
                                            <asp:ListItem>Nov</asp:ListItem>
                                            <asp:ListItem>Dec</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel29">
                                    <ContentTemplate>
                                        <asp:TextBox ID="usdTextBox1" runat="server" Width="65px" OnTextChanged="usdTextBox1_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel30">
                                    <ContentTemplate>
                                        <asp:TextBox ID="bdtTextBox1" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel44">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="mealMonthDropDownList2" runat="server" Width="65px" AutoPostBack="True">
                                            <asp:ListItem>Jan</asp:ListItem>
                                            <asp:ListItem>Feb</asp:ListItem>
                                            <asp:ListItem>Mar</asp:ListItem>
                                            <asp:ListItem>Apr</asp:ListItem>
                                            <asp:ListItem>May</asp:ListItem>
                                            <asp:ListItem>Jun</asp:ListItem>
                                            <asp:ListItem>Jul</asp:ListItem>
                                            <asp:ListItem>Aug</asp:ListItem>
                                            <asp:ListItem>Sep</asp:ListItem>
                                            <asp:ListItem>Act</asp:ListItem>
                                            <asp:ListItem>Nov</asp:ListItem>
                                            <asp:ListItem>Dec</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel31">
                                    <ContentTemplate>
                                        <asp:TextBox ID="usdTextBox2" runat="server" Width="65px" OnTextChanged="usdTextBox2_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel32">
                                    <ContentTemplate>
                                        <asp:TextBox ID="bdtTextBox2" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel45">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="mealMonthDropDownList3" runat="server" Width="65px" AutoPostBack="True">
                                            <asp:ListItem>Jan</asp:ListItem>
                                            <asp:ListItem>Feb</asp:ListItem>
                                            <asp:ListItem>Mar</asp:ListItem>
                                            <asp:ListItem>Apr</asp:ListItem>
                                            <asp:ListItem>May</asp:ListItem>
                                            <asp:ListItem>Jun</asp:ListItem>
                                            <asp:ListItem>Jul</asp:ListItem>
                                            <asp:ListItem>Aug</asp:ListItem>
                                            <asp:ListItem>Sep</asp:ListItem>
                                            <asp:ListItem>Act</asp:ListItem>
                                            <asp:ListItem>Nov</asp:ListItem>
                                            <asp:ListItem>Dec</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel33">
                                    <ContentTemplate>
                                        <asp:TextBox ID="usdTextBox3" runat="server" Width="65px" OnTextChanged="usdTextBox3_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel34">
                                    <ContentTemplate>
                                        <asp:TextBox ID="bdtTextBox3" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Avg.
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel35">
                                    <ContentTemplate>
                                        <asp:TextBox ID="usdAvgTextBox" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel36">
                                    <ContentTemplate>
                                        <asp:TextBox ID="bdtAvgTextBox" runat="server" Width="65px" Enabled="False"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table style="vertical-align: middle">
                        <tr>
                            <td colspan="3" align="center">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#669999" colspan="3" align="center" style="color: White">
                                TK rate against $1
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel37">
                                    <ContentTemplate>
                                        <asp:TextBox ID="rateTextBox" runat="server" Width="190px" OnTextChanged="rateTextBox_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" runat="server" Text="Save" Width="75px" OnClick="saveButton_Click"
                                    UseSubmitBehavior="False" />
                            </td>
                            <td>
                                <asp:Button ID="inputToPLButton" runat="server" Text="Input To PL" Width="75px" UseSubmitBehavior="False"
                                    OnClick="inputToPLButton_Click" Visible="False" />
                            </td>
                            <td>
                                <asp:Button ID="clearButton" runat="server" Text="Clear" Width="75px" OnClick="clearButton_Click"
                                    UseSubmitBehavior="False" />
                            </td>
                            <td>
                                <asp:Button ID="printButton" runat="server" Text="Print" Width="75px" UseSubmitBehavior="False"/>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
  </asp:Content>
