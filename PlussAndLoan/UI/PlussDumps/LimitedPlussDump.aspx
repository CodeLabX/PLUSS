﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UI/ITAdminMasterPage.master" AutoEventWireup="true" CodeBehind="LimitedPlussDump.aspx.cs" Inherits="PlussAndLoan.UI.PlussDumps.LimitedPlussDump" %>

<%@ Register Src="~/UI/UserControls/js/TextBoxDatePicker.ascx" TagPrefix="uc1" TagName="TextBoxDatePicker" %>
<%@ Register Src="~/UI/UserControls/UCs/ModalUserControl.ascx" TagPrefix="modal" TagName="modalPopupUC" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <uc1:TextBoxDatePicker runat="server" ID="TextBoxDatePicker" />

    <script type="text/javascript">
        function pageLoad() {
            $('#ctl00_ContentPlaceHolder_txtCiFromDate').unbind();
            $('#ctl00_ContentPlaceHolder_txtCiToDate').unbind();

            $('#ctl00_ContentPlaceHolder_txtCiFromDate').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });

            $('#ctl00_ContentPlaceHolder_txtCiToDate').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });


            $('#ctl00_ContentPlaceHolder_txtMisFrom').unbind();
            $('#ctl00_ContentPlaceHolder_txtMisTo').unbind();

            $('#ctl00_ContentPlaceHolder_txtMisFrom').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });
            $('#ctl00_ContentPlaceHolder_txtMisTo').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });


            $('#ctl00_ContentPlaceHolder_txtLamsFrom').unbind();
            $('#ctl00_ContentPlaceHolder_txtLamsTo').unbind();

            $('#ctl00_ContentPlaceHolder_txtLamsFrom').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });
            $('#ctl00_ContentPlaceHolder_txtLamsTo').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });

            $('#ctl00_ContentPlaceHolder_txtNegFrom').unbind();
            $('#ctl00_ContentPlaceHolder_txtNegTo').unbind();

            $('#ctl00_ContentPlaceHolder_txtNegFrom').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });
            $('#ctl00_ContentPlaceHolder_txtNegTo').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });

            $('#ctl00_ContentPlaceHolder_txtDedupFrom').unbind();
            $('#ctl00_ContentPlaceHolder_txtDedupTo').unbind();

            $('#ctl00_ContentPlaceHolder_txtDedupFrom').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });
            $('#ctl00_ContentPlaceHolder_txtDedupTo').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });


            $('#ctl00_ContentPlaceHolder_txtLoanFrom').unbind();
            $('#ctl00_ContentPlaceHolder_txtLoanTo').unbind();

            $('#ctl00_ContentPlaceHolder_txtLoanFrom').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });
            $('#ctl00_ContentPlaceHolder_txtLoanTo').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });


            $('#ctl00_ContentPlaceHolder_txtRtobFrom').unbind();
            $('#ctl00_ContentPlaceHolder_txtRtobTo').unbind();
            $('#<%= txtRtobFrom.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });
            $('#<%= txtRtobTo.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });

            $('#ctl00_ContentPlaceHolder_txtMFUHistoryFrom').unbind();
            $('#ctl00_ContentPlaceHolder_txtMFUHistoryTo').unbind();
            $('#<%= txtMFUHistoryFrom.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });
            $('#<%= txtMFUHistoryTo.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });


            $('#ctl00_ContentPlaceHolder_txtRVLoanFrom').unbind();
            $('#ctl00_ContentPlaceHolder_txtRVLoanTo').unbind();
            $('#<%= txtRVLoanFrom.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });
            $('#<%= txtRVLoanTo.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date("<%= MaxMinDateAllowed.ToString("yyyy-MM-dd") %>")
            });

            $('#ctl00_ContentPlaceHolder_txtMFUTo').unbind();
            $('#ctl00_ContentPlaceHolder_txtMFUFrom').unbind();
            $('#<%= txtMFUTo.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            $('#<%= txtMFUFrom.ClientID %>').datepicker({
                showOn: 'button',
                buttonImage: '../../Image/Calendar_scheduleHS.png',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
        }
    </script>

    <style type="text/css">
        .reports-holder table tr td fieldset {
            margin-bottom: 20px;
            text-align: left;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <modal:modalPopupUC runat="server" ID="modalPopup" />

    <div class="form-style-2 container-fluid">
        <div class="form-style-2-heading">PLUSS Dump</div>
        <div class="reports-holder" style="text-align: center">
            <asp:Label runat="server" ID="lblMsg"></asp:Label>
            <table width="100%">
                <tr>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Export CI Check List</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtCiFromDate"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtCiToDate"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnCiDump" Text="CI Checklist" OnClick="btnCiDump_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Export Daily MIS</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtMisFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtMisTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnMis" Text="Daily MIS" OnClick="btnMis_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Export LAMS Dump</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtLamsFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtLamsTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnLams" Text="LAMS Dump" OnClick="btnLams_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Export Negative List</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtNegFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtNegTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnExportNeg" Text="Export Negative List" OnClick="btnExportNeg_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Export Dedup Information</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtDedupFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtDedupTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnExportDedup" Text="Export Dedup" OnClick="btnExportDedup_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Export Loan Applications</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtLoanFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtLoanTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnExportLoan" Text="Export Loan Applications" OnClick="btnExportLoan_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Export RTOB Data</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtRtobFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtRtobTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnRtobExportNeg" Text="RTOB Download" OnClick="btnRtobExport_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Export MFU Upload History</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtMFUHistoryFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtMFUHistoryTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnMFUUploadHistoryDump" Text="MFU Upload History" OnClick="btnMFUUploadHistoryDump_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>Report Viewer's List</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtRVLoanFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtRVLoanTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnRVExportLoan" Text="Report Viewer's List" OnClick="btnReportViewerList_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="width: 33%">
                        <fieldset>
                            <legend>MFU Dump</legend>
                            <table cellpadding="4" cellspacing="2">
                                <tr>
                                    <td>From Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtMFUFrom"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td>To Date</td>
                                    <td>:&nbsp;&nbsp;<asp:TextBox CssClass="input-field" Width="150px" runat="server" ID="txtMFUTo"></asp:TextBox>
                                        (yyyy-mm-dd)
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="text-align: left">&nbsp;<asp:Button CssClass="btn primarybtn" runat="server" ID="btnMFUDump" Text="MFU Dump Download" OnClick="btnMFUDump_Click" /></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td></td>
                    <td></td>
                </tr>

            </table>
        </div>
    </div>

</asp:Content>
