﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AzUtilities;
using BusinessEntities;
using DAL;
using LoanLocatorSerevice;

namespace PlussAndLoan.UI.PlussDumps
{
    public partial class PlussDump : System.Web.UI.Page
    {
        private DumpGateway dumpGateway;
        private readonly LoanReportService _service = new LoanReportService();
        protected void Page_Load(object sender, EventArgs e)
        {

            dumpGateway = new DumpGateway();

        }

        protected void btnCiDump_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var fromDate = new DateTime();
                var toDate = new DateTime();
                try
                {
                    fromDate = Convert.ToDateTime(txtCiFromDate.Text);
                    toDate = Convert.ToDateTime(txtCiToDate.Text);
                }
                catch (Exception ex)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                    CustomException.Save(ex, "PLUSS Dump :: - btnCiDump_Click");
                    LogFile.WriteLine(" PLUSS Dump :: - btnCiDump_Click");
                }
                User user = (User)Session["UserDetails"];

                var data = dumpGateway.GetCiDump(fromDate, toDate, user);
                string csv = PrintCsv(data, "CICheckList");

                MemoryStream memoryStream = new MemoryStream();
                TextWriter textWriter = new StreamWriter(memoryStream);
                textWriter.Write(csv);
                textWriter.Flush(); // added this line
                byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=CICheckList_Dump_" + DateTime.Now.Ticks + ".csv");
                Response.BinaryWrite(bytesInStream);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("CI Dump");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "PLUSS Dump :: - btnCiDump_Click");
            }
            Response.End();
        }

        protected void btnMis_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var fromDate = new DateTime();
                var toDate = new DateTime();
                try
                {
                    fromDate = Convert.ToDateTime(txtMisFrom.Text);
                    toDate = Convert.ToDateTime(txtMisTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var data = dumpGateway.GetMisDump(fromDate, toDate, user);

                string csv = PrintCsv(data, "MISDump");

                MemoryStream memoryStream = new MemoryStream();
                TextWriter textWriter = new StreamWriter(memoryStream);
                textWriter.Write(csv);
                textWriter.Flush(); // added this line
                byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=MIS_DUMP_" + DateTime.Now.Ticks + ".csv");
                Response.BinaryWrite(bytesInStream);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("MIS DUMP");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "PLUSS Dump :: - btnMis_Click");
            }
            Response.End();
        }

        protected void btnLams_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var fromDate = new DateTime();
                var toDate = new DateTime();
                try
                {
                    fromDate = Convert.ToDateTime(txtLamsFrom.Text);
                    toDate = Convert.ToDateTime(txtLamsTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var data = dumpGateway.GetLamsDump(fromDate, toDate, user);


                string csv = PrintCsv(data, "lamsdump");

                MemoryStream memoryStream = new MemoryStream();
                TextWriter textWriter = new StreamWriter(memoryStream);
                textWriter.Write(csv);
                textWriter.Flush(); // added this line
                byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=LAMS_DUMP_" + DateTime.Now.Ticks + ".csv");
                Response.BinaryWrite(bytesInStream);
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("LAMS Dump");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "PLUSS Dump :: - btnLams_Click");
            }
            Response.End();
        }

        protected void btnExportNeg_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var startDate = new DateTime();
                var endDate = new DateTime();
                try
                {
                    startDate = Convert.ToDateTime(txtNegFrom.Text);
                    endDate = Convert.ToDateTime(txtNegTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var dt = dumpGateway.GetNegativeListDump(startDate, endDate, user);
                PrintDump(dt, "NegativeListEmployer");
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("btnExportNeg_Click");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "PLUSS Dump :: - btnExportNeg_Click");
            }
        }

        protected void btnExportDedup_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var startDate = new DateTime();
                var endDate = new DateTime();
                try
                {
                    startDate = Convert.ToDateTime(txtDedupFrom.Text);
                    endDate = Convert.ToDateTime(txtDedupTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var dt = dumpGateway.GetDedupInfoDump(startDate, endDate, user);
                PrintDump(dt, "DedupInfo");
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("DedupInfo");
                LogFile.WriteLog(ex);
                CustomException.Save(ex, "PLUSS Dump :: - btnExportDedup_Click");
            }
        }

        protected void btnExportLoan_Click(object sender, EventArgs e)
        {

            try
            {
                lblMsg.Text = "";
                var startDate = new DateTime();
                var endDate = new DateTime();
                try
                {
                    startDate = Convert.ToDateTime(txtLoanFrom.Text);
                    endDate = Convert.ToDateTime(txtLoanTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var dt = dumpGateway.GetLoanAppInfoDump(startDate, endDate, user);
                PrintDump(dt, "LoanApplications");
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("LoanApplications");
                LogFile.WriteLog(ex);

                CustomException.Save(ex, "PLUSS Dump :: - btnExportLoan_Click");
            }

        }


        private void PrintDump(DataTable dt, string reportName)
        {
            try
            {
                var filename = reportName + DateTime.Now.Ticks + ".csv";
                var csv = string.Empty;
                if (dt != null)
                {
                    csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ','));
                    csv += "\r\n";

                    foreach (DataRow row in dt.Rows)
                    {
                        csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", " ").Replace(";", " ").Replace("&amp", "&").Replace("&AMP", "&").Replace("amp", " ").Replace("AMP", " ") + ','));
                        csv += "\r\n";
                    }
                }
                
                MemoryStream memoryStream = new MemoryStream();
                TextWriter textWriter = new StreamWriter(memoryStream);
                textWriter.Write(csv);
                textWriter.Flush(); // added this line
                byte[] bytesInStream = memoryStream.ToArray(); // simpler way of converting to array
                memoryStream.Close();
                Response.Clear();
                Response.ContentType = "application/force-download";
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.BinaryWrite(bytesInStream);

            }
            catch (Exception e)
            {
                lblMsg.Text = e.Message;

                CustomException.Save(e, "PLUSS Dump :: - PrintDump");
            }
            Response.End();
        }

        protected void btnRtobExport_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var startDate = new DateTime();
                var endDate = new DateTime();
                try
                {
                    startDate = Convert.ToDateTime(txtRtobFrom.Text);
                    endDate = Convert.ToDateTime(txtRtobTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var dt = dumpGateway.GetRTOBInfoDump(startDate, endDate, user);
                PrintDump(dt, "RTOB_DUMP_");
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("RTOP Dump");
                LogFile.WriteLog(ex);

                CustomException.Save(ex, "PLUSS Dump :: - btnRtobExport_Click");
            }
        }

        protected void btnMFUUploadHistoryDump_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var startDate = new DateTime();
                var endDate = new DateTime();
                try
                {
                    startDate = Convert.ToDateTime(txtMFUHistoryFrom.Text);
                    endDate = Convert.ToDateTime(txtMFUHistoryTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var dt = dumpGateway.GetMFUUploadHistoryDump(startDate, endDate, user);
                PrintDump(dt, "MFU_Upload_History");
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("MFU Upload History");
                LogFile.WriteLog(ex);

                CustomException.Save(ex, "PLUSS Dump :: - btnMFUUploadHistoryDump_Click");
            }
        }

        
        protected void btnMFUDump_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var startDate = new DateTime();
                var endDate = new DateTime();
                try
                {
                    startDate = Convert.ToDateTime(txtMFUFrom.Text);
                    endDate = Convert.ToDateTime(txtMFUTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var dt = dumpGateway.GetMFUDump(startDate, endDate, user);
                PrintDump(dt, "MFU_DUMP");
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("MFU Upload DUMP");
                LogFile.WriteLog(ex);

                CustomException.Save(ex, "PLUSS Dump :: - btnMFU_Click");
            }
        }

        protected void btnReportViewerList_Click(object sender, EventArgs e)
        {
            try
            {
                lblMsg.Text = "";
                var startDate = new DateTime();
                var endDate = new DateTime();
                try
                {
                    startDate = Convert.ToDateTime(txtRVLoanFrom.Text);
                    endDate = Convert.ToDateTime(txtRVLoanTo.Text);
                }
                catch (Exception)
                {
                    lblMsg.Text = "Please input valid date format";
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Font.Bold = true;
                }
                User user = (User)Session["UserDetails"];
                var dt = dumpGateway.GetReportViewersInfoDump(startDate, endDate, user);
                PrintDump(dt, "ReportViewer");
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("ReportViewer");
                LogFile.WriteLog(ex);

                CustomException.Save(ex, "PLUSS Dump :: - btnReportViewerList_Click");
            }
        }

        private string PrintCsv(DataTable dt, string reportName)
        {
            try
            {
                var filename = reportName + DateTime.Now.Ticks + ".csv";
                var csv = string.Empty;
                if (dt != null)
                {
                    csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (column.ColumnName + ','));
                    csv += "\r\n";

                    foreach (DataRow row in dt.Rows)
                    {
                        csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", ";") + ','));
                        //csv = dt.Columns.Cast<DataColumn>().Aggregate(csv, (current, column) => current + (row[column.ColumnName].ToString().Replace(",", " ").Replace(";", " ").Replace("&amp", "&").Replace("amp", " ").Replace('\t', ' ') + ','));
                        csv += "\r\n";
                    }
                }

                return csv;

            }
            catch (Exception e)
            {
                lblMsg.Text = e.Message;

                CustomException.Save(e, "PLUSS Dump :: - PrintCsv..");
            }

            return "";
        }

        private void PushDownloadFileFromString(string csv)
        {

        }

    }
}