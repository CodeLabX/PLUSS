﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_SourceUI : System.Web.UI.Page
    {
        public SourceManager sourceManagerObj = null;
        const int ERROR = -1;
        const int INSERTE = 1;
        const int UPDATE = 2;
        const int DELETE = 3;
        protected void Page_Load(object sender, EventArgs e)
        {
            sourceManagerObj = new SourceManager();
            Int32 sourceMaxId;
            try
            {
                string sourceId = string.Empty;
                string updatedBy = string.Empty;
                string strOperation = string.Empty;
                string strsearchKey = string.Empty;
                strOperation = Convert.ToString(Request.QueryString["operation"]);
                sourceId = Convert.ToString(Request.QueryString["sourceId"]);
                strsearchKey = Convert.ToString(Request.QueryString["SEARCHKEY"]);
                if (strOperation == "save")
                {
                    StreamReader streamReader = new StreamReader(Request.InputStream);
                    string sourceData = streamReader.ReadToEnd();
                    Response.Write(SaveData(sourceData));
                    Response.End();
                }
                else if (strOperation == "loadData" && strsearchKey == "")
                {
                    Response.Write(SearchData(""));
                    Response.End();
                }
                else if (strOperation == "loadData" && strsearchKey!=null)
                {
                    if (strsearchKey.Length > 0)
                    {
                        Response.Write(SearchData(strsearchKey));
                        Response.End();
                    }
                }
                else if (strOperation == "GetMaxId")
                {
                    Response.Write(GetMaxId().ToString() + "#");
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
                Response.End();
            }
            xmlDataHiddenField.Value = SearchData("").Replace("#","");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "doOnLoad();LoadGridData();ClearField();", true);
        
        }
        private Int32 GetMaxId()
        {
            Int32 returnValue = 0;
            return returnValue = sourceManagerObj.GetSourceMaxId() + 1;
        }
        private string SaveData(string sourceData)
        {
            try
            {
                string returnValue=string.Empty;
                int getValue = 0;
                Source sourceObj = new Source();
                string[] splitData = sourceData.Split('#');
                sourceObj.SourceId = Convert.ToInt32('0' + splitData[0].ToString());
                sourceObj.SourceName = Convert.ToString(splitData[1].ToString());
                sourceObj.SourceRegionName = Convert.ToString(splitData[2].ToString());
                // for loan locator audit trail
                var userID = Convert.ToInt32(Session["Id"].ToString());
                sourceObj.UserId = userID;
                getValue = sourceManagerObj.SendSourceToDB(sourceObj);
        
                if (getValue == INSERTE)
                {
                    returnValue = "Save successfully #";
                }
                else if (getValue == UPDATE)
                {
                    returnValue = "Update successfully #";
                }
                else
                {
                    returnValue = "Unable to save data in Database #";
                }
                new AT(this).AuditAndTraial("Source Save", returnValue);
                xmlDataHiddenField.Value = SearchData("");
                return returnValue;
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Source");
                throw new Exception("");
            }
        }

        private String SearchData(string strsearchKey)
        {
            StringBuilder sbSourceData = new StringBuilder();
            List<Source> sourceObjList = new List<Source>();
            if (strsearchKey.Length > 0)
            {
                sourceObjList = sourceManagerObj.GetSourceList(strsearchKey);
            }
            else
            {
                sourceObjList = sourceManagerObj.GetSourceList();
            }
            try
            {
                sbSourceData.Append("<rows>");
                foreach (Source sourceObj in sourceObjList)
                {
                    sbSourceData.Append("<row id='" + sourceObj.SourceId.ToString() + "'>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(sourceObj.SourceId.ToString());
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(sourceObj.SourceName));
                    sbSourceData.Append("</cell>");
                    sbSourceData.Append("<cell>");
                    sbSourceData.Append(AddSlash(sourceObj.SourceRegionName));
                    sbSourceData.Append("</cell>");                
                    sbSourceData.Append("</row>");
                }
                sbSourceData.Append("</rows>");

            }
            catch (Exception ex)
            {
            
            }
            return sbSourceData.ToString()+"#";
        }
        public string AddSlash(string sMessage)
        {
            string returnMessage = "";
            if (sMessage.Length > 0 && sMessage != "")
            {
                sMessage = sMessage.Replace("&", "&amp;");
                sMessage = sMessage.Replace(@"<", @"&gt;");
                sMessage = sMessage.Replace(@">", @"&lt;");
                returnMessage = sMessage;
            }
            return returnMessage;
        }

        public void Audit()
        {
            var userID = Convert.ToInt32(Session["Id"].ToString());
            
        }
    }
}
