﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI
{
    public partial class SegmentWiseCapping : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            divDate.Text = Session["m1"].ToString();

            switch (Session["m1"].ToString())
            {
                case "01":
                    divDate.Text="January";
                    break;
                case "02":
                    divDate.Text = "February";
                    break;
                case "03":
                    divDate.Text = "March";
                    break;
                case "04":
                    divDate.Text = "April";
                    break;
                case "05":
                    divDate.Text = "May";
                    break;
                case "06":
                    divDate.Text = "June";
                    break;
                case "07":
                    divDate.Text = "July";
                    break;
                case "08":
                    divDate.Text = "August";
                    break;
                case "09":
                    divDate.Text = "September";
                    break;
                case "10":
                    divDate.Text = "October";
                    break;
                case "11":
                    divDate.Text = "November";
                    break;
                case "12":
                    divDate.Text = "December";
                    break;
            }
            DataTable pl = (DataTable) Session["pl"];
            DataTable segment = (DataTable) Session["segmnt"];

            var amou1=0;
	        var tot1=0;
	        var tot2=0;
	        var amou2=0;
            decimal perc = 0;
            decimal perc2 = 0;

            if (pl != null || segment != null )
            {
                for (int i = 0; i < pl.Rows.Count; i++)
                {
                    tot1=tot1+1;
	                amou1+=Convert.ToInt32(pl.Rows[0]["LOAN_APRV_AMOU"]);
                }

                for (int j = 0; j < segment.Rows.Count; j++)
                {
                    tot2=tot2+1;
	                amou2+=Convert.ToInt32(segment.Rows[0]["LOAN_APRV_AMOU"]);
                }

                perc=((amou1/tot1)*100);
		        perc=Math.Round(perc,3);

                perc2=((tot2/amou2)*100);
		        perc2=Math.Round(perc2,3);
            }
            TableRow row = new TableRow();
            row.Cells.Add(new TableCell { Text = "Sales A/C (X1,Y1,Z1,Z2)" });
            row.Cells.Add(new TableCell { Text = amou1.ToString() });
            row.Cells.Add(new TableCell { Text = perc.ToString() + "%" });
            LevelReport.Rows.Add(row);

            TableRow row1 = new TableRow();
            row1.Cells.Add(new TableCell { Text = "Cash+B2 (Z3,Z4,Z5,Z6,Z7,Z8)" });
            row1.Cells.Add(new TableCell { Text = amou2.ToString() });
            row1.Cells.Add(new TableCell { Text = perc2.ToString() + "%" });
            LevelReport.Rows.Add(row1);
        }

        protected void Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
            Session.Clear();
        }

        protected void Close_Click(object sender, EventArgs e)
        {
            Response.Redirect("LocLoanReport.aspx");
            Session.Clear();
        }
    }
}