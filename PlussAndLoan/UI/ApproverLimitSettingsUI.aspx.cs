﻿using System;
using System.Collections.Generic;
using System.IO;
using BLL;
using BusinessEntities;
using DAL;

namespace PlussAndLoan.UI
{
    public partial class UI_ApproverLimitSettingsUI : System.Web.UI.Page
    {
        ApproverManager approverManagerObj=null;
        UserManager userManagerObj=null;
        ProductManager productManagerObj=null;
        Approver approverObj;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Bat.Common.Connection.ConnectionStringName = "conString";
            approverManagerObj = new ApproverManager();
            userManagerObj = new UserManager();
            productManagerObj = new ProductManager();

            if (!Page.IsPostBack)
            {
                LoadInitialData();
                LoadData("");
            }

        }
        private void LoadInitialData()
        {
            List<User>userObjList=userManagerObj.GetAllUserInfo();
            User userObj = new User();
            userObj.Id = 0;
            userObj.Name = "Select approver name";
            userObjList.Insert(0, userObj);
            approvalNameDropDownList.DataSource = userObjList;// userManagerObj.GetAllUserInfo();
            approvalNameDropDownList.DataTextField = "Name";
            approvalNameDropDownList.DataValueField = "Id";
            approvalNameDropDownList.DataBind();


            var productObjectList = productManagerObj.GetAllProducts();
            var objproduct = new Product();
            objproduct.ProdID = 0;
            objproduct.ProdName = "Select product name";
            productObjectList.Insert(0, objproduct);
            ddlProduct.DataSource = productObjectList;
            ddlProduct.DataTextField = "ProdName";
            ddlProduct.DataValueField = "ProdID";
            ddlProduct.DataBind();
            LoadProductTypes();
            //productDropDownList.DataSource = productManagerObj.GetAllDistinctProductInfo();
        


            //productDropDownList.DataBind();
        }
        private void LoadData(string searchKey)
        {
           
            List<Approver> approverObjList = new List<Approver>();
            if (searchKey == "")
            {
                approverObjList = approverManagerObj.SelectAllApproverList();
            }
            else
            {
                approverObjList = approverManagerObj.SelectAllApproverList(searchKey);
            }
            try
            {
                var xml = "<xml id='xml_data'><rows>";
                
                foreach (Approver approverObj in approverObjList)
                {
                    xml+="<row id='" + approverObj.UserAppID.ToString() + "'>";
                    xml+="<cell>";
                    xml+=approverObj.UserAppID.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=approverObj.UserID.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=approverObj.ProductId.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=approverObj.UserName.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=approverObj.ProductName.ToString();
                    xml+="</cell>";
                    var productTag = "";
                    var productType = "";

                    switch (approverObj.ProdTag.ToString())
                    {
                        case "U":
                            productType = "Unsecure";
                            break;
                        case "S":
                            productType = "Secure";
                            break;
                        case "C":
                            productType = "Cash Secure";
                            break;

                    }

                    xml+="<cell>";
                    xml+=productType;
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=approverObj.Level1Amt.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=approverObj.Level2Amt.ToString();
                    xml+="</cell>";
                    xml+="<cell>";
                    xml+=approverObj.Level3Amt.ToString();
                    xml+="</cell>";
                    if (approverObj.Status == 1)
                    {
                        xml+="<cell>";
                        xml+="Active";
                        xml+="</cell>";
                    }
                    else
                    {
                        xml+="<cell>";
                        xml+="Inactive";
                        xml+="</cell>";
                    }
                    xml += "</row>";
                }
                xml += "</rows></xml>";
                approveDiv.InnerHtml = xml;
            }
            finally
            {
            }
          
        }
        protected void searchbutton_Click(object sender, EventArgs e)
        {
            if (searchTextBox.Text != "")
            {
                LoadData(searchTextBox.Text.Trim());
            }
            else
            {
                LoadData("");
            }
        }
        protected void clearButton_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void ClearAll()
        {
            approvalNameDropDownList.SelectedIndex = 0;
            ddlProduct.SelectedIndex = 0;
            productDropDownList.SelectedIndex = 0;
            level1AmountTextBox.Text = "";
            level2AmountTextBox.Text = "";
            level3AmountTextBox.Text = "";
            statusDropdownList.SelectedIndex = 1;
            approverManagerObj = new ApproverManager();
            addButton.Text = "Add";
            searchTextBox.Text = "";
        }

        protected void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidPage())
                {
                    var msg = "";
                    Int32 ApproverId = 0;
                    approverObj = new Approver();
                    approverManagerObj = new ApproverManager();
                    ApproverId = approverManagerObj.GetExistiongApproverId(Convert.ToInt32(approvalNameDropDownList.SelectedValue.ToString()), Convert.ToString(productDropDownList.SelectedValue.ToString()));
                    if (ApproverId == 0)
                    {
                        approverObj.UserAppID = 0;
                        approverObj.UserID = Convert.ToInt32(approvalNameDropDownList.SelectedValue.ToString());
                        approverObj.ProdTag = Convert.ToString(productDropDownList.SelectedValue.ToString());
                        approverObj.Level1Amt = Convert.ToInt32(level1AmountTextBox.Text);
                        approverObj.Level2Amt = Convert.ToInt32(level2AmountTextBox.Text);
                        approverObj.Level3Amt = Convert.ToInt32(level3AmountTextBox.Text);
                        approverObj.ProductId = Convert.ToInt32(ddlProduct.SelectedValue.ToString());
                        if (statusDropdownList.Text == "Active")
                        {
                            approverObj.Status = 1;
                        }
                        else
                        {
                            approverObj.Status = 0;
                        }
                        approverObj.ChangDt = DateTime.Now;
                        approverObj.ChangBy = Convert.ToInt32(Session["Id"]);
                        int status = approverManagerObj.InsertApprover(approverObj);
                
                        if (status == 1)
                        {
                            msg = "Save successful !";
                            LoadData("");
                            this.ClearAll();
                        }
                        else
                        {
                            msg = "Un-successful !";
                        }
                        Alert.Show(msg);
                        new AT(this).AuditAndTraial("Approver Limit Settings", msg);
                    }
                    else
                    {
                        approverObj.UserAppID = ApproverId;
                        approverObj.UserID = Convert.ToInt32(approvalNameDropDownList.SelectedValue.ToString());
                        approverObj.ProdTag = Convert.ToString(productDropDownList.SelectedValue.ToString());
                        approverObj.Level1Amt = Convert.ToInt32(level1AmountTextBox.Text);
                        approverObj.Level2Amt = Convert.ToInt32(level2AmountTextBox.Text);
                        approverObj.Level3Amt = Convert.ToInt32(level3AmountTextBox.Text);
                        approverObj.ProductName = ddlProduct.Text;
                        approverObj.ProductId = Convert.ToInt32(ddlProduct.SelectedValue);
                        if (statusDropdownList.Text == "Active")
                        {
                            approverObj.Status = 1;
                        }
                        else
                        {
                            approverObj.Status = 0;
                        }
                        approverObj.ChangDt = DateTime.Now;
                        approverObj.ChangBy = Convert.ToInt32(Session["Id"]);
                        int status = approverManagerObj.UpdateApprover(approverObj);
                        if (status == 2)
                        {
                            msg = "Update successful !";
                            LoadData("");
                            this.ClearAll();
                        }
                        else
                        {
                            msg="Un-successful !";
                        }
                        Alert.Show(msg);
                        new AT(this).AuditAndTraial("Approver Limit Settings", msg);
                    }
                }
            }
            catch (Exception exception)
            {
                CustomException.Save(exception, "Save Approver Limit");
            }
        }

        private bool IsValidPage()
        {
            if (approvalNameDropDownList.SelectedIndex == 0)
            {
                Alert.Show("Please select approver name");
                approvalNameDropDownList.Focus();
                return false;
            }
            if (ddlProduct.SelectedIndex == 0)
            {
                Alert.Show("Please select a product name");
                ddlProduct.Focus();
                return false;
            }
            else if (level1AmountTextBox.Text == "")
            {
                Alert.Show("Please enter Level1 Amount");
                level1AmountTextBox.Focus();
                return false;
            }
            else if (level2AmountTextBox.Text == "")
            {
                Alert.Show("Please enter Level2 Amount");
                level2AmountTextBox.Focus();
                return false;
            }
            else if (level3AmountTextBox.Text == "")
            {
                Alert.Show("Please enter Level3 Amount");
                level3AmountTextBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void LoadProductTypeByProductID(object sender, EventArgs e)
        {
            var productId = Convert.ToInt32(ddlProduct.SelectedValue);
            var res = productManagerObj.GetAllDistinctProductInfoById(productId);
            //productDropDownList.DataSource = productManagerObj.GetAllDistinctProductInfoById(productId);
            //productDropDownList.DataTextField = "ProdTypeName";
            //productDropDownList.DataValueField = "ProdType";
            //productDropDownList.DataBind();
            productDropDownList.SelectedValue = res[0].ProdType;
        }
        private void LoadProductTypes()
        {
            productManagerObj = new ProductManager();

            //var res = productManagerObj.GetAllProductTypes();
            //var objList = new List<String>();
            //var obj = new Object();

            //obj.Id = "sdsdffs";
            //obj.Name = "dfsdfsdf";

            //objList.Add(obj);


            productDropDownList.DataSource = productManagerObj.GetAllProductTypes();
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "ProdType";
            productDropDownList.DataBind();
        } 
    }
}
