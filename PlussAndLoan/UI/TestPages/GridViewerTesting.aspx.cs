﻿using BLL;
using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlussAndLoan.UI.TestPages
{
    public partial class GridViewerTesting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User userT = (User)Session["UserDetails"];
            if (!IsPostBack)
            {
                InitPendingUserGrid();
            }
        }

        public void InitPendingUserGrid()
        {
            UserManager _user = new UserManager();

            try
            {
                //userGridView.DataSource = null;
                //userGridView.DataBind();

                List<UserTModel> users = _user.GetAllPendingUsers();

                //userGridView.DataSource = users;
                //userGridView.DataBind();

                //var us = users;
                //GridView1.DataSource = us;
                //GridView1.DataBind();

            }
            catch (Exception)
            {
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
    }
}