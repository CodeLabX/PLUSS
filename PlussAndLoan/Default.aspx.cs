﻿using AzUtilities;
using System;
using System.Web;

namespace PlussAndLoan
{
    public partial class _Default : System.Web.UI.Page 
    {
        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            Exception exc = Server.GetLastError();

            if (exc is HttpUnhandledException)
            {
                if (exc.InnerException != null)
                {
                    exc = new Exception(exc.InnerException.Message);
                    LogFile.WriteLog(exc);
                }
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("LoginUI.aspx");
        }
        protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
        {

        }
    }
}
