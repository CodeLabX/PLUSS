﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DateFormat
/// </summary>
public static class DateFormat
{
    public static bool IsDate(DateTime myDateString)
    {
        string[] sdate;
        try
        {

            string tempDate = Convert.ToString(myDateString);
            if (tempDate.Length == 0)
            {
                return false;
            }
            else
            {
                sdate = tempDate.Split('/');
                sdate[2] = sdate[2].Substring(0, 4);
                int year = Convert.ToInt32(sdate[2]);
                if (year <= 1900)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        catch
        {
            return false;
        }
    }

    public static bool IsDate(string myDateString)
    {
        try
        {
            DateTime dt = Convert.ToDateTime(myDateString, new System.Globalization.CultureInfo("ru-RU"));
            return true;
        }
        catch
        {
            //try
            //{
            //    DateTime dt = Convert.ToDateTime(myDateString);
            //    return true;
            //}
            //catch 
            //{
            //    return false;
            //}
            return false;
        }
    }
}
