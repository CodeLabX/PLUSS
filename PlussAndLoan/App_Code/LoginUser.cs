﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for LoginUser
/// </summary>
public class LoginUser
{
    public static List<LogedUser> LoginUserToSystem = new List<LogedUser>();    
}

public class LogedUser
{
    public string UserId { get; set; }
    public string UserName { get; set; }
    public string RoleName { get; set; }
    public string UserSessionID { get; set; }
    public bool IsLogged { get; set; }
    public string IPAddress { get; set; }
    public string HostName { get; set; }
    public DateTime LoggedInTime { get; set; }

    public string GetLocalHostName(string ip)
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');

            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

}