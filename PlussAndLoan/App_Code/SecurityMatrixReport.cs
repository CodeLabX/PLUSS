﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SecurityMatrixReport
/// </summary>
public class SecurityMatrixReport
{

    public string Module { get; set; }
    public string Approver { get; set; }
    public string Analyst { get; set; }
    public string BusinessAdmin { get; set; }
    public string ScoreAdmin { get; set; }
    public string SupportUser { get; set; }
    public string Adminmaker { get; set; }
    public string Adminchecker { get; set; }
}
