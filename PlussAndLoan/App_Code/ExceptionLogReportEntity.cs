﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for ExceptionLogReportEntity
/// </summary>
public class ExceptionLogReportEntity
{
    public ExceptionLogReportEntity()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string OperationType { get; set; }
    public string ExceptionDescription { get; set; }
    public DateTime ExceptionDate { get; set; }
    public string UserId { get; set; }
}
