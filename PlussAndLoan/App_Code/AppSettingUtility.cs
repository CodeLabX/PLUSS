﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Configuration;

/// <summary>
/// Summary description for SecurityMatrixReport
/// </summary>
public class AppSettingUtility
{
    private static DateTime _plussDumpMinDate;

    public static DateTime LimitedPlussDumpMaxMinDays
    {
        get
        {
            DateTime now = DateTime.Now;

            now = now.Date + TimeSpan.Zero;

            try
            {
                int maxMinDays = Convert.ToInt32(WebConfigurationManager.AppSettings["LimitedPlussDumpMaxMinDays"]);

                _plussDumpMinDate = now.AddDays(-maxMinDays);

                return _plussDumpMinDate;
            }
            catch (Exception ex)
            {
                return now.AddDays(-7);
            }
        }
        private set { }
    }
    
    private static DateTime _expImpMinDate;

    public static DateTime ExpImpMinDateForReport
    {
        get
        {
            DateTime now = DateTime.Now;

            now = now.Date + TimeSpan.Zero;

            try
            {
                int maxMinDays = Convert.ToInt32(WebConfigurationManager.AppSettings["LimitedExpImpMaxMinDays"]);

                _expImpMinDate = now.AddDays(-maxMinDays);

                return _expImpMinDate;
            }
            catch (Exception ex)
            {
                return now.AddDays(-90);
            }
        }
        private set { }
    }

}
