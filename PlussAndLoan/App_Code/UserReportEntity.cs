﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BusinessEntities;

/// <summary>
/// Summary description for UserReportEntity
/// </summary>
public class UserReportEntity
{
    public string UserId { get; set; }
    public string Name { get; set; }
    public string Designation { get; set; }
    public string Email { get; set; }
    public DateTime EntryDate { get; set; }
    public string StatusMsg { get; set; }
    public string UserTypeName { get; set; }
    public DateTime LastLoginDateTime { get; set; }
}
