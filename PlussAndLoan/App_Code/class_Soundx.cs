﻿using System;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for class_Soundx
/// </summary>
public static class class_Soundx
{
    static ArrayList aList1 = new ArrayList(4);
    static ArrayList aList2 = new ArrayList(8);
    static ArrayList aList3 = new ArrayList(2);
    static ArrayList aList4 = new ArrayList(1);
    static ArrayList aList5 = new ArrayList(2);
    static ArrayList aList6 = new ArrayList(1);
    static ArrayList aListExcep = new ArrayList(6);
   
    private static void setArrayLists()
    {

        aListExcep.Add("A");
        aListExcep.Add("E");
        aListExcep.Add("I");
        aListExcep.Add("O");
        aListExcep.Add("U");
        aListExcep.Add("H");

        aList1.Add("B");
        aList1.Add("F");
        aList1.Add("P");
        aList1.Add("V");

        aList2.Add("C");
        aList2.Add("G");
        aList2.Add("J");
        aList2.Add("K");
        aList2.Add("Q");
        aList2.Add("S");
        aList2.Add("X");
        aList2.Add("Z");

        aList3.Add("D");
        aList3.Add("T");

        aList4.Add("L");

        aList5.Add("M");
        aList5.Add("N");

        aList6.Add("R");

    }
    public static String getSoundEX(String strInput)
    {
        setArrayLists();
        String strReturn = "";
        try
        {
            String strTemp = strInput.Substring(0, 1).ToUpper();
            if (strInput.Length > 0)
            {
                int iStrLen = 0;
                if (strInput.Length >= 4)
                {
                    iStrLen = 4;
                }
                else
                {
                    iStrLen = strInput.Length;
                }
                char[] chrEachChar = strInput.ToCharArray(0, iStrLen);
                ArrayList aListTemp = new ArrayList(iStrLen);
                foreach (char chr in chrEachChar)
                {
                    String strTempChar = chr.ToString().ToUpper();
                    bool blnGoOn = true;
                    //check if the character has been already counted 
                    if (aListTemp.Count > 0)
                    {
                        if (aListTemp.IndexOf(strTempChar) >= 0)
                        {
                            blnGoOn = false;
                        }
                    }
                    else
                    {
                        //do not go in for the first character 
                        blnGoOn = false;
                    }
                    //add the char to the array so that 
                    //the consequetive entries are checked for. 
                    aListTemp.Add(strTempChar);
                    if (blnGoOn == true)
                    {
                        if (aListExcep.IndexOf(strTempChar) < 0) //if the char is not a vowel or H 
                        {
                            if (aList1.IndexOf(strTempChar) < 0)
                            {
                                if (aList2.IndexOf(strTempChar) < 0)
                                {
                                    if (aList3.IndexOf(strTempChar) < 0)
                                    {
                                        if (aList4.IndexOf(strTempChar) < 0)
                                        {
                                            if (aList5.IndexOf(strTempChar) < 0)
                                            {
                                                if (aList6.IndexOf(strTempChar) >= 0)
                                                {
                                                    strTemp = strTemp + "6";
                                                }
                                            }
                                            else //array5 
                                            {
                                                strTemp = strTemp + "5";
                                            }
                                        }
                                        else //array4 
                                        {
                                            strTemp = strTemp + "4";
                                        }
                                    }
                                    else //array3 
                                    {
                                        strTemp = strTemp + "3";
                                    }
                                }
                                else //array2 
                                {
                                    strTemp = strTemp + "2";
                                }
                            }
                            else //array1 
                            {
                                strTemp = strTemp + "1";
                            }
                        }//if the char is a vowel or H 
                    }//if the char has already been taken into account 
                }//end for each 
                if (strTemp.Length < 5)
                {
                    int iLen = strTemp.Length;
                    if (iLen == 1)
                    {
                        strTemp = strTemp + "0000";
                    }
                    else if (iLen == 2)
                    {
                        strTemp = strTemp + "000";
                    }
                    else if (iLen == 3)
                    {
                        strTemp = strTemp + "00";
                    }
                    else if (iLen == 4)
                    {
                        strTemp = strTemp + "0";
                    }
                }
                strReturn = strTemp.Trim();
            }
            else
            {
                strReturn = "";
            }
        }
        catch (Exception exp)
        {
            strReturn = "ERROR";
        }
        return (strReturn);
    }
 
    public static String ToSoundexCode(String aString)
    {

      String word = aString.ToUpper();
      StringBuilder soundexCode = new StringBuilder();
      int wordLength = word.Length;
      try
      {
          // Rule 1. Keep the first character of the word
          soundexCode.Append(word.Substring(0, 1));

          // Rule 2. Perform a transformation on each remaining characters
          for (int i = 1; i < wordLength; i++)
          {
              String transformedChar = Transform(word.Substring(i, 1));

              // Rule 3. If a character is the same as the previous, do not include in code
              if (!transformedChar.Equals(soundexCode.ToString().Substring(soundexCode.Length - 1)))
              {

                  // Rule 4. If character is "A" or "S" do not include in code
                  if (!transformedChar.Equals("A") && !transformedChar.Equals("S"))
                  {

                      // Rule 5. If a character is blank, then do not include in code 
                      if (!transformedChar.Equals(" "))
                      {
                          soundexCode.Append(transformedChar);
                      }
                  }
              }
          }

          // Rule 6. A soundex code must be exactly 4 characters long.  If the
          //         code is too short then pad with zeros, otherwise truncate.
          soundexCode.Append("0000");
      }
      catch (Exception exp)
      {
          soundexCode.Append("ERROR");
      }
      return soundexCode.ToString().Substring(0,4);
    }

    /// <summary>
    /// Transform the A-Z alphabetic characters to the appropriate soundex code.
    /// </summary>
    private static String Transform(String aString)
    {
      
      switch (aString)
      {
        case "A":
        case "E":
        case "I":
        case "O":
        case "U":
        case "Y":
          return "A";
        case "H":
        case "W":
          return "S";
        case "B":
        case "F":
        case "P":
        case "V":
          return "1";
        case "C":
        case "G":
        case "J":
        case "K":
        case "Q":
        case "S":
        case "X":
        case "Z":
          return "2";
        case "D":
        case "T":
          return "3";
        case "L":
          return "4";
        case "M":
        case "N":
          return "5";
        case "R":
          return "6";
      }

      return " ";  
    }
  }

