﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SecurityMatrixReport
/// </summary>
public class PageControllManager
{
    public static void EnableControlls(Control parent, bool state)
    {
        foreach (Control c in parent.Controls)
            foreach (Control ctrl in c.Controls)

                if (ctrl is TextBox)

                    ((TextBox)ctrl).Enabled = state;

                else if (ctrl is GridView)

                    ((GridView)ctrl).Enabled = state;

                else if (ctrl is RadioButton)

                    ((RadioButton)ctrl).Enabled = state;

                else if (ctrl is ImageButton)

                    ((ImageButton)ctrl).Enabled = state;

                else if (ctrl is CheckBox)

                    ((CheckBox)ctrl).Enabled = state;

                else if (ctrl is DropDownList)

                    ((DropDownList)ctrl).Enabled = state;

                else if (ctrl is HyperLink)

                    ((HyperLink)ctrl).Enabled = state;
    }
}
