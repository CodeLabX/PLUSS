﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for AuditTrailReportEntity
/// </summary>
public class AuditTrailReportEntity
{
    public AuditTrailReportEntity()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int AuditId { get; set; }
    public string UserId { get; set; }
    public DateTime ActionDateAndTime { get; set; }
    public string RequestedUrl { get; set; }

    public string IpAddress { get; set; }

    public string HostName { get; set; }

    public string Activity { get; set; }

    public string Description { get; set; }

    public string UserRole { get; set; }
}
