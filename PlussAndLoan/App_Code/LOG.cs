﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using System.Text;

public static class LOG
{
    public static void GenericError(string strData)
    {
        string path = "";
        int idx;
        path = (string)ConfigurationManager.AppSettings.Get("ErrorLogPath"); 
        path += @"ErrorLog\";
        string strFilename = path + "PLUSS " + DateTime.Now.ToString("MM-yyyy") + ".Err";
        FileInfo fiError = new FileInfo(strFilename);
        StreamWriter stwError = null;

        if (!fiError.Exists)
            stwError = fiError.CreateText();
        else
            stwError = fiError.AppendText();
        try
        {
            stwError.WriteLine("[ " + DateTime.Now.ToString("dd-MM-yyyy h:mm:ss tt") + " ]");
            stwError.WriteLine("Error: [" + strData + "]\r\n");
        }
        finally
        {
            stwError.Close();
        }
    }
}


