﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SecurityMatrix_AutoReportEntity
/// </summary>
public class SecurityMatrix_AutoReportEntity
{
    public string Module { get; set; }
    public string AutoAdmin { get; set; }
    public string AutoAnalystAndApprover { get; set; }
    public string AutoApprover { get; set; }
    public string CIAnalyst { get; set; }
    public string CISupport { get; set; }
    public string OpsChecker { get; set; }
    public string OpsMaker { get; set; }
    public string Sales { get; set; }
}
