﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.includes
{
    public partial class includes_UserLoginDataList : System.Web.UI.Page
    {
        private UserAccessLogManager userAccessLogManagerObj = null;
        string searchKey = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            userAccessLogManagerObj = new UserAccessLogManager();
            if (Request.QueryString.Count != 0)
            {
                searchKey = Convert.ToString(Request.QueryString["SearchKey"]);
            }
            LoadLoginUserStatus(searchKey);
        
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "onLoadPage();", true);
        }
        private void LoadLoginUserStatus(string searchKey)
        {
            string LoginStatus = "";
            List<UserAccessLog> userAccessLogObjList = new List<UserAccessLog>();
            if (searchKey == "" || searchKey == null)
            {
                userAccessLogObjList = userAccessLogManagerObj.GetAccessLog(DateTime.Now, DateTime.Now);
            }
            else
            {
                userAccessLogObjList = userAccessLogManagerObj.GetAccessLog(searchKey, DateTime.Now, DateTime.Now);
            }
            if (userAccessLogObjList != null)
            {
                Response.ContentType = "text/xml";
                Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>");
                Response.Write("<rows>");
                foreach (UserAccessLog userAccessLogObj in userAccessLogObjList)
                {
                    Response.Write("<row id='" + userAccessLogObj.AcessLogId.ToString() + "'>");
                    Response.Write("<cell>");
                    Response.Write(userAccessLogObj.AcessLogId.ToString());
                    Response.Write("</cell>");
                    Response.Write("<cell>");
                    Response.Write(userAccessLogObj.UserLogInId.ToString());
                    Response.Write("</cell>");
                    Response.Write("<cell>");
                    Response.Write(userAccessLogObj.IP.ToString());
                    Response.Write("</cell>");
                    Response.Write("<cell>");
                    Response.Write(userAccessLogObj.LogInDateTime.ToString("dd-MM-yyyy HH:mm:ss"));
                    Response.Write("</cell>");
                    Response.Write("<cell>");

                    if (userAccessLogObj.userLoginStatus == 1)
                    {
                        LoginStatus = "Login";
                    }
                    else
                    {
                        LoginStatus = "Logout";
                    }
                    Response.Write(LoginStatus);
                    Response.Write("</cell>");
                    Response.Write("<cell>");
                    Response.Write("Action^javascript:LoadUserPopUp(" + userAccessLogObj.AcessLogId.ToString() + ")^_self");
                    Response.Write("</cell>");
                    Response.Write("</row>");
                }
                Response.Write("</rows>");        
            }
        }
    }
}
