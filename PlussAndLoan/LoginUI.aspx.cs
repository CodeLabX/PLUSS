﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using AzUtilities;
using BLL;
using BusinessEntities;
using DAL;
using WebConfigEncryption;

namespace PlussAndLoan
{
    public partial class LoginUI : System.Web.UI.Page
    {
        enum USERTYPE : short
        {
            Admin = 1,
            Analyst = 2,
            SupportUser = 3,
            Approver = 4,
            ScoreAdmin = 5,

            //-----------Added by Mazhar-------------
            AutoAdmin = 6,
            AutoApprover = 7,
            AutoAnalyst = 8,
            AutoSupport = 9,
            AutoOperations = 10,
            AutoSales = 11,
            AutoOperationChecker = 12,
            ITAdmin = 17

        }
        enum ERRORMSGTYPE : short
        {
            DatabaseFail = 1,
            InvalidUserId = 2,
            InvalidPassword = 3
        }

        UserManager userManagerObj;
        private UserAccessLogManager _userAccessLogManager = null;
        private UserInfoHistoryManager _userInfoHistoryManager = null;
        private UserAccessLog userAccessLogObj = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            messageTable.Visible = false;
            try
            {
                string path = Request.ApplicationPath;
                ConnectionStringManager manager = new ConnectionStringManager(path);
                manager.EncryptConstring("conString");
                manager.EncryptConstring("conStringLoanLocator");

            }
            catch (Exception ex)
            {

            }

            _userAccessLogManager = new UserAccessLogManager();
            _userInfoHistoryManager = new UserInfoHistoryManager();
            Page.Form.DefaultButton = logInButton.UniqueID;
            Page.Form.DefaultFocus = userIdTextBox.ClientID;

            // changePasswordLinkButton.Attributes.Add("onclick", "newWindow()");
            Bat.Common.Connection.ConnectionStringName = "conString";
            Bat.Common.Connection.dateFormat = "yyyy-MM-dd";
            if (Session["loginErrorCount"] == null)
            {
                Session["loginErrorCount"] = 0;
            }

        }
        protected void logInButton_Click(object sender, EventArgs e)
        {
            string loginId = userIdTextBox.Text.Trim();
            string password = passwordTextBox.Value.Trim();
            if (string.IsNullOrEmpty(loginId) && string.IsNullOrEmpty(password))
            {
                conMsgLabel.Text = "UserID & Password cannot be null !";
                return;
            }
            if (string.IsNullOrEmpty(loginId))
            {
                conMsgLabel.Text = "UserID cannot be null !";
                return;
            }
            if (string.IsNullOrEmpty(password))
            {
                conMsgLabel.Text = "Password cannot be null !";
                return;
            }

            // var h = System.Net.Dns.GetHostName();
            var host = new LogedUser().GetLocalHostName(Request.ServerVariables["REMOTE_ADDR"]);
            LogFile.WriteLine("PC HostName / IP Address - " + host.ToString());


            //var thisPclogedUser = LoginUser.LoginUserToSystem.Where(u => u.UserId == userIdTextBox.Text.Trim() && (u.HostName.ToLower() == host.ToLower()));
            HttpCookieCollection cookies = Request.Cookies;


            // ZQ
            IEnumerable<LogedUser> thisPclogedUserBySession = new List<LogedUser>();
            if (cookies.Get("PlussUserSession") != null)
            {
                string PCSessionID = Request.Cookies["PlussUserSession"].Value.Replace("=", "");
                thisPclogedUserBySession = LoginUser.LoginUserToSystem.Where(u => u.UserId == userIdTextBox.Text.Trim() && (u.UserSessionID == PCSessionID));
            }
            var thisPclogedUser = LoginUser.LoginUserToSystem.Where(u => u.UserId == userIdTextBox.Text.Trim() && (u.HostName.ToLower() == host.ToLower()));
            //ZQ

            if (thisPclogedUser.Any() || thisPclogedUserBySession.Any())
            {
                ViewState["password"] = passwordTextBox.Value.Trim();
                lblMessage.Text = "This user already Logged in this System. Are you sure want to relogin?";
                loginTable.Visible = false;
                messageTable.Visible = true;
            }

            var logedInAnotherPc = LoginUser.LoginUserToSystem.Where(u => u.UserId == userIdTextBox.Text.Trim() && (u.HostName.ToLower() != host.ToLower()));


            // ZQ
            IEnumerable<LogedUser> logedInAnotherSession = new List<LogedUser>();
            if (cookies.Get("PlussUserSession") != null)
            {
                string PCSessionID = Request.Cookies["PlussUserSession"].Value;
                logedInAnotherSession = LoginUser.LoginUserToSystem.Where(u => u.UserId == userIdTextBox.Text.Trim() && (u.UserSessionID != PCSessionID));
            }
            // ZQ

            if (logedInAnotherPc.Any() && logedInAnotherSession.Any())
            {
                lblMessage.Text = "This user already Logged in Another PC. Host Name:" + logedInAnotherPc.FirstOrDefault().HostName;
                loginTable.Visible = false;
                messageTable.Visible = true;
                btnOK.Visible = false;
                btnCancel.Text = "Go Back";
            }

            var newUser = LoginUser.LoginUserToSystem.Where(u => u.UserId == userIdTextBox.Text.Trim());
            if (!newUser.Any())
            {
                loginTable.Visible = true;
                messageTable.Visible = false;
                UserLoginProcess();
            }
            _userAccessLogManager.DisableUserIf90DaysNotLogin();
        }

        private void UserLoginProcess()
        {
            try
            {
                var errLoc = "";
                int successStatus = 0;
                int unSuccessStatus = 0;
                Int32 userLoginStatus = 0;
                int loginErrorCount = 0;
                if (Session["loginErrorCount"] != null)
                {
                    loginErrorCount = (int)Session["loginErrorCount"];

                }
                int dayDifference = 0;

                userManagerObj = new UserManager();
                string loginId = userIdTextBox.Text.Trim();
                string password = passwordTextBox.Value.Trim();
                if (String.IsNullOrEmpty(loginId))
                {
                    conMsgLabel.Text = "Please enter user id.";
                    return;
                }
                else if (String.IsNullOrEmpty(password))
                {
                    conMsgLabel.Text = "Please enter password.";
                    return;
                }
                else
                {
                    string isAdsActive = WebConfigurationManager.AppSettings["IsADSActive"];
                    string adsDomainName = WebConfigurationManager.AppSettings["ADSDomainName"];
                    if (isAdsActive == "1")
                    {
                        #region ADS Login

                        //adsDomainName = adsDropdown.Text.Trim();
                        try
                        {
                            using (var pc = new PrincipalContext(ContextType.Domain, adsDomainName))
                            {
                                if (loginId != null)
                                {
                                    User user = userManagerObj.GetUserLoginInfo(loginId);

                                    if (user != null)
                                    {

                                        //isValid = true;
                                        errLoc = "ValidateCredentials";
                                        var isValid = pc.ValidateCredentials(loginId, password);
                                        if (isValid)
                                        {
                                            errLoc = "get access log";
                                            UserAccessLog userAccessLogObj = _userAccessLogManager.GetAccessLog(loginId, DateTime.Now);
                                            if (user.ErrorMsgType.Equals((short)ERRORMSGTYPE.DatabaseFail))
                                            {
                                                conMsgLabel.Text = "AD - Database Connection Failed.";
                                                return;
                                            }
                                            else if (user.ErrorMsgType.Equals((short)ERRORMSGTYPE.InvalidUserId))
                                            {
                                                conMsgLabel.Text = "AD - Invalid User ID.";
                                                unSuccessStatus = 1;
                                                SaveAccessLog(loginId, successStatus, unSuccessStatus, userLoginStatus);
                                                loginErrorCount++;
                                            }
                                            else
                                            {
                                                if (user.IsExists)
                                                {
                                                    conMsgLabel.Text = "";
                                                    Session["Id"] = user.Id.ToString();
                                                    Session["UserId"] = user.UserId.ToString();
                                                    Session["UserName"] = user.Name.ToString();
                                                    Session["Designation"] = user.Designation.ToString();
                                                    Session["UserType"] = user.Type;
                                                    Session["AutoUserType"] = user.AutoUserLevel_Id;
                                                    Session["both"] = user.IsAccessBoth;

                                                    Session["UserDetails"] = user;

                                                    var sortAutotype = user.AutoUserLevel_Id;

                                                    Session["loginErrorCount"] = 0;

                                                    successStatus = 1;
                                                    userLoginStatus = 1;
                                                    loginErrorCount = 0;

                                                    Session["loginStatus"] = new UserAccessLogManager().GetLoginHistory(loginId);

                                                    errLoc = "save access log";
                                                    SaveAccessLog(loginId, successStatus, unSuccessStatus, userLoginStatus);
                                                    errLoc = "audit trail";
                                                    new AT(this).AuditAndTraial("Login", "Login");

                                                    LogedUser loggedUser = new LogedUser();
                                                    loggedUser.UserId = userIdTextBox.Text.Trim();
                                                    loggedUser.IsLogged = true;

                                                    loggedUser.HostName = loggedUser.GetLocalHostName(Request.ServerVariables["REMOTE_ADDR"]);
                                                    loggedUser.LoggedInTime = DateTime.Now;

                                                    // ZQ
                                                    loggedUser.UserName = user.Name.ToString();
                                                    loggedUser.RoleName = user.RoleCode.ToString();
                                                    var CookiesVal = Request.Cookies.Get("PlussUserSession");
                                                    if (CookiesVal == null)
                                                    {
                                                        loggedUser.UserSessionID = HttpContext.Current.Session.SessionID;
                                                        HttpCookie PlussUserCookie = new HttpCookie("PlussUserSession");
                                                        PlussUserCookie.Value = loggedUser.UserSessionID.Replace("=", "");
                                                        PlussUserCookie.Expires = DateTime.Now.AddDays(1);
                                                        Response.Cookies.Add(PlussUserCookie);
                                                    }
                                                    else
                                                    {
                                                        loggedUser.UserSessionID = Request.Cookies["PlussUserSession"].Value.Replace("=", "");
                                                        HttpCookie PlussUserCookie = new HttpCookie("PlussUserSession");
                                                        PlussUserCookie.Value = loggedUser.UserSessionID.ToString();
                                                        PlussUserCookie.Expires = DateTime.Now.AddDays(1);
                                                        Response.Cookies.Add(PlussUserCookie);
                                                    }
                                                    // ZQ
                                                    LoginUser.LoginUserToSystem.Add(loggedUser);
                                                    Response.Redirect("~/UI/UserSwitchHome.aspx", false);
                                                }
                                                else
                                                {
                                                    unSuccessStatus = 1;
                                                    conMsgLabel.Text =
                                                        "User Not Found or May be User locked by system, please contact with System Adminstator";
                                                }
                                            }
                                            Session["loginErrorCount"] = loginErrorCount;
                                            if (loginErrorCount == 3)
                                            {
                                                userManagerObj.UpdateLoginStatus(loginId);
                                                conMsgLabel.Text = "This user locked by system, please contact System Adminstator ! ";
                                                Session["loginErrorCount"] = 0;
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            conMsgLabel.Text = "Login Failed ! Please Check User Id and Password";

                                            loginErrorCount++;
                                            unSuccessStatus = loginErrorCount;
                                            Session["loginErrorCount"] = loginErrorCount;
                                            if (loginErrorCount == 3)
                                            {
                                                userManagerObj.UpdateLoginStatus(loginId);
                                                conMsgLabel.Text =
                                                    "This user locked by system, please contact with System Adminstator ! ";
                                                Session["loginErrorCount"] = 0;
                                                return;
                                            }
                                            SaveAccessLog(loginId, successStatus, unSuccessStatus, userLoginStatus);
                                        }
                                    }
                                    else
                                    {
                                        conMsgLabel.Text = "User is Locked or Invalid.Please Contact with System Administrator ! ";
                                        return;
                                    }
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            LogFile.WriteLine("LoginPage:LoginBtnClicked 1");
                            LogFile.WriteLog(exception);

                            conMsgLabel.Text = exception.Message;
                            new AT(this).AuditAndTraial("switch user", errLoc);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal login validate with DB
                        User userObj = userManagerObj.GetUserLoginInfo(loginId, password);
                        if (userObj != null)
                        {
                            var accessLogObj = _userAccessLogManager.GetAccessLog(loginId, DateTime.Now);
                            if (userObj.ErrorMsgType.Equals((short)ERRORMSGTYPE.DatabaseFail))
                            {
                                conMsgLabel.Text = "NAD - Database Connection Failed.";
                                return;
                            }
                            else if (userObj.ErrorMsgType.Equals((short)ERRORMSGTYPE.InvalidUserId))
                            {
                                conMsgLabel.Text = "NAD - Invalid User Id or Wrong Password.";
                                unSuccessStatus = 1;
                                SaveAccessLog(loginId, successStatus, unSuccessStatus, userLoginStatus);
                                loginErrorCount++;
                            }
                            else
                            {
                                if (userObj.IsExists)
                                {
                                    conMsgLabel.Text = "";
                                    Session["Id"] = userObj.Id.ToString();
                                    Session["UserId"] = userObj.UserId.ToString();
                                    Session["UserName"] = userObj.Name.ToString();
                                    Session["Designation"] = userObj.Designation.ToString();
                                    Session["UserType"] = userObj.Type;
                                    Session["AutoUserType"] = userObj.AutoUserLevel_Id;
                                    Session["both"] = userObj.IsAccessBoth;
                                    var sortAutotype = userObj.AutoUserLevel_Id;
                                    Session["loginErrorCount"] = 0;

                                    successStatus = 1;
                                    userLoginStatus = 1;
                                    loginErrorCount = 0;

                                    Session["UserDetails"] = userObj;

                                    var userManagerObjT = new UserManager();
                                    var loanUser = userManagerObjT.GetLoanLocatorUser(Session["UserId"].ToString());
                                    if (loanUser != null)
                                    {
                                        loanUser.logedintime = DateTime.Now;
                                        Session["loanUser"] = loanUser;
                                    }

                                    Session["loginStatus"] = new UserAccessLogManager().GetLoginHistory(loginId);

                                    errLoc = "save access log";
                                    SaveAccessLog(loginId, successStatus, unSuccessStatus, userLoginStatus);
                                    errLoc = "audit trail";
                                    new AT(this).AuditAndTraial("Login", "Login");

                                    LogedUser user = new LogedUser();
                                    user.UserId = userIdTextBox.Text.Trim();
                                    user.IsLogged = true;
                                    user.LoggedInTime = DateTime.Now;
                                    user.HostName = user.GetLocalHostName(Request.ServerVariables["REMOTE_ADDR"]);


                                    // ZQ
                                    user.UserName = userObj.Name.ToString();
                                    user.RoleName = userObj.RoleCode.ToString();
                                    var CookiesVal = Request.Cookies.Get("PlussUserSession");
                                    if (CookiesVal == null)
                                    {
                                        user.UserSessionID = HttpContext.Current.Session.SessionID;
                                        HttpCookie PlussUserCookie = new HttpCookie("PlussUserSession");
                                        PlussUserCookie.Value = user.UserSessionID.Replace("=", "");
                                      //  PlussUserCookie.Expires = DateTime.Now.AddDays(1);
                                        Response.Cookies.Add(PlussUserCookie);
                                    }
                                    else
                                    {
                                        user.UserSessionID = Request.Cookies["PlussUserSession"].Value.Replace("=", "");
                                        HttpCookie PlussUserCookie = new HttpCookie("PlussUserSession");
                                        PlussUserCookie.Value = user.UserSessionID.ToString();
                                        //PlussUserCookie.Expires = DateTime.Now.AddDays(1);
                                        Response.Cookies.Add(PlussUserCookie);
                                    }
                                    // ZQ


                                    LoginUser.LoginUserToSystem.Add(user);
                                    Response.Redirect("~/UI/UserSwitchHome.aspx", false);
                                }
                                else
                                {
                                    unSuccessStatus = 1;
                                    conMsgLabel.Text = "NAD - User Not Found or May be User locked by system, please contact with System Adminstator";
                                }
                            }
                            Session["loginErrorCount"] = loginErrorCount;
                            if (loginErrorCount == 3)
                            {
                                userManagerObj.UpdateLoginStatus(loginId);
                                conMsgLabel.Text = "NAD - This user locked by system, please contact with System Adminstator! ";
                                Session["loginErrorCount"] = 0;
                                return;
                            }
                        }
                        else
                        {
                            conMsgLabel.Text = "NAD - User is Locked or Invalid. Please Contact with System Administrator ! ";
                            return;
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile.WriteLine("LoginPage::LoginBtnClicked");
                LogFile.WriteLog(ex);
            }
        }

        private void SaveAccessLog(string loginId, int successStatus, int unSuccessStatus, int userLoginStatus)
        {
            int insertAccessLog = 0;
            userAccessLogObj = new UserAccessLog();
            userAccessLogObj.UserLogInId = loginId.ToString();
            // ZQ  userAccessLogObj.IP = System.Net.Dns.GetHostName();
            userAccessLogObj.IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]; 
            userAccessLogObj.LogInCount = 0;
            userAccessLogObj.LogInDateTime = DateTime.Now;
            userAccessLogObj.userLoginSuccessStatus = successStatus;
            userAccessLogObj.userLoginUnSuccessStatus = unSuccessStatus;
            userAccessLogObj.userLoginStatus = userLoginStatus;
            userAccessLogObj.AccessFor = "LOGIN";
            insertAccessLog = _userAccessLogManager.SendDataInToDB(userAccessLogObj);
        }
        private void updateUserLoginStatus(Int32 userId, Int32 loginStatus)
        {
            //userManagerObj.UpdateLoginStatus(userId,loginStatus);
        }
        protected void changePasswordLinkButton_Click1(object sender, EventArgs e)
        {

            //StringBuilder sb = new StringBuilder();
            //string popWindow = "window.open('UI/ChangePassword.aspx','','toolbar=no,scrollbars=yes,location=no,statusbar=no,menubar=yes,resizable=yes,width=500,height=300');";
            ////string popWindow = "alert('aa')";
            //sb.Append("<script type='text/javascript'>");
            //sb.Append(popWindow);
            //sb.Append("</script>");
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", sb.ToString(), true);       

        }

        //protected void changePasswordLinkButton_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("~/UI/ChangePassword.aspx");
        //}
        protected void btnOK_Click(object sender, EventArgs e)
        {
            new AT(this).AuditAndTraial("Re-login", "");
            SaveAccessLog(userIdTextBox.Text, 0, 0, 0);
            passwordTextBox.Value = ViewState["password"].ToString();
            messageTable.Visible = false;
            loginTable.Visible = true;
            LoginUser.LoginUserToSystem.RemoveAll(u => u.UserId == userIdTextBox.Text.Trim());
            UserLoginProcess();
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loginTable.Visible = true;
            messageTable.Visible = false;
            btnCancel.Text = "CANCEL";
        }
    }
}
