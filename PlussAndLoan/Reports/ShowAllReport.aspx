﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.Reports.Reports_ShowAllReport" Codebehind="ShowAllReport.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Show All Report Page</title>
    <style type="text/css">
        .Report1
        {
            padding: 4px;
            width: 100%;
            height: 900px;
            border: 1px dotted #808080;
        }
    </style>
    <style type="text/css">
        .Report2
        {
            padding: 4px;
            width: 100%;
            height:1400px;
            border: 1px dotted #808080;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <%--<iframe runat="server" id="plCheckListFrame" class="Report1" src=""></iframe>
        <iframe runat="server" id="lliNewFrame" class="Report2" src=""></iframe>--%>
        <br />
        
        <div runat="server" id="dvContent">
        </div>
        <br />
    </div>
    </form>
</body>
</html>
