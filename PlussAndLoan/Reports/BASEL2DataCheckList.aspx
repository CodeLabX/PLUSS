﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PlussAndLoan.Reports.Reports_BASEL2DataCheckList" Codebehind="BASEL2DataCheckList.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BASEL II Data CheckList</title>
    <style type="text/css">
        .tableWidth
        {
            width: 98%;
            border: 1px solid #000000;
            border-collapse: collapse;
            font-size: 12px;
        }
        .tableWidth td
        {
            border-width: 1px;
            border-style: solid;
            border-color: Black;
        }
        .style1
        {
            height: 18px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function PrintBasel()
        {
            window.print();
        }
        function ColorChange()
        {
             document.getElementById('printButton').style.backgroundColor = 'red';
        }
    </script>
</head>
<body style="margin-top:0.25px; left:0.25px; right:0px; margin-bottom:0.25px;" onload="PrintBasel();">
    <form id="form1" runat="server">
        <center>
        <table width="98%" border="0" cellpadding="0" cellspacing="1"><tr><td align="left">
        <table width="98%">
            <tr>
                <td style="font-weight: bold; width:38%;">
                    LL ID :<asp:Label ID="llidLabel" runat="server"></asp:Label>
                </td>
                <td align="left" style="font-size: large; width:58% ">
                    BASEL II DATA CHECKLIST
                    &nbsp;
                </td>
            </tr>
        </table>
        <table class="tableWidth" border="0" cellpadding="2" cellspacing="1">
            <tr style="font-weight: bold; text-decoration: underline;">
                <td style="width: 48%">
                    FIELD NAME
                </td>
                <td style="width: 48%">
                    ACCEPTABLE VALUE/SOURCE
                </td>
            </tr>
        </table>
        <table class="tableWidth" border="0" cellpadding="2" cellspacing="1">
            <tr>
                <td style="width: 50%">
                    Customer Name
                </td>
                <td>
                    <asp:Label ID="customerNameLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Customer Master
                </td>
                <td>
                    <asp:Label ID="customerMasterLabel" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="tableWidth" border="1" cellpadding="2" cellspacing="1">
            <tr>
                <td style="width: 50%">
                    Nationality
                </td>
                <td style="width: 50%">
                    <asp:Label ID="nationalityLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Gender Code
                </td>
                <td>
                    <asp:Label ID="genderCodeLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Marital Status
                </td>
                <td>
                    ♦&nbsp;<asp:Label ID="maritalStatusLabel1" runat="server">Married</asp:Label>
                    ♦&nbsp;<asp:Label ID="maritalStatusLabel2" runat="server">Single</asp:Label>
                    ♦&nbsp;<asp:Label ID="maritalStatusLabel3" runat="server">Other</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Highest Education Qulification
                </td>
                <td>
                    <asp:Label ID="highestEducationQulificationLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Employment Status Code
                </td>
                <td>
                    ♦&nbsp;<asp:Label ID="employmentStatusCodeLabel1" runat="server">Salaried&nbsp;&nbsp;</asp:Label>
                    ♦&nbsp;<asp:Label ID="employmentStatusCodeLabel2" runat="server">Self-Employed&nbsp;&nbsp;</asp:Label>
                    ♦&nbsp;<asp:Label ID="employmentStatusCodeLabel3" runat="server">Student&nbsp;&nbsp;</asp:Label>
                    ♦&nbsp;<asp:Label ID="employmentStatusCodeLabel4" runat="server">Business&nbsp;&nbsp;</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Business Establish Date
                </td>
                <td>
                    <asp:Label ID="businessEstablishDateLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Month's of Total Work Experience
                </td>
                <td>
                    <asp:Label ID="monthsTotalWorkExperienceLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Month's in Current Job
                </td>
                <td>
                    <asp:Label ID="monthsCurrentJobLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Date of Joining
                </td>
                <td>
                    <asp:Label ID="dateJoiningLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Monthly Income
                </td>
                <td>
                    <asp:Label ID="monthlyIncomeLabel" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Other Monthly Income
                </td>
                <td>
                    <asp:Label ID="otherMonthlyIncomeLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Monthly Loan Repayments ( Loan with Other Bank)
                </td>
                <td>
                    <asp:Label ID="monthlyLoanRepaymentsLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Outstanding Amount ( Loan With Other Bank)
                </td>
                <td>
                    <asp:Label ID="outstandingAmountLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Type of Account with Other Bank
                </td>
                <td>
                    <asp:Label ID="TypeAccountLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Loan Account Number
                </td>
                <td align="right">
                    <asp:Label ID="loanAccountNumberLabel" runat="server" Text="(Loan Admin)"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Date Loan Booked
                </td>
                <td align="right">
                    <asp:Label ID="dateLoanBookedLabel" runat="server" Text="(Loan Admin)"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Designation / Title
                </td>
                <td>
                    <asp:Label ID="designationTitleLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Department
                </td>
                <td>
                    <asp:Label ID="DepartmentLabel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Date Of Birth
                </td>
                <td>
                    <asp:Label ID="DateOfBirthLabel" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="tableWidth" class="tableWidth" border="1" cellpadding="2" cellspacing="1">
            <tr>
                <td colspan="4" style="font-weight:bold;">
                    CREDIT INITIATION SHEET :
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 48%">
                    Withdrawal Date
                </td>
                <td style="width: 24%">
                    &nbsp;
                </td>
                <td style="width: 12%">
                    <asp:Label ID="withdrawalDateLabel" runat="server" Font-Bold="True">NA</asp:Label>
                </td>
                <td style="width: 12%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Withdrawal Reason
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="withdrawalReasonLabel" runat="server" Font-Bold="True">NA</asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Override Flag
                </td>
                <td>
                    <asp:Label ID="overrideFlagLabel1" runat="server">♦&nbsp YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="overrideFlagLabel2" runat="server" Font-Bold="True">♦&nbsp NO</asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Pre Approval Flag (Always No, if Yes CCU to mention)
                </td>
                <td>
                    <asp:Label ID="preApprovalFlagLabel1" runat="server">♦&nbsp YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="preApprovalFlagLabel2" runat="server" Font-Bold="True">♦&nbsp NO</asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Income Verification Flag
                </td>
                <td>
                    <asp:Label ID="incomeVerificationLabel1" runat="server" Font-Bold="True">♦&nbsp YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="incomeVerificationLabe2" runat="server">♦&nbsp NO</asp:Label>
                </td>
                <td>
                    <asp:Label ID="incomeVerificationLabe3" runat="server">♦&nbsp NA</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Employment Status Verification Flag
                </td>
                <td>
                    <asp:Label ID="employmentStatusVerifyLabel1" runat="server" Font-Bold="True">♦&nbsp YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="employmentStatusVerifyLabel2" runat="server">♦&nbsp NO</asp:Label>
                </td>
                <td>
                    <asp:Label ID="employmentStatusVerifyLabel3" runat="server">♦&nbsp NA</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Credit Policy
                </td>
                <td class="style1">
                    <asp:Label ID="creditPolicyLabel1" runat="server" Font-Bold="true"></asp:Label>
                </td>
                <td class="style1">
                </td>
                <td class="style1">
                </td>
            </tr>
            <tr>
                <td>
                    SCB Unsecured Exposure
                </td>
                <td>
                    <asp:Label ID="sCBUnsecuredLabel" runat="server" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    SCB Secured Exposure
                </td>
                <td>
                    <asp:Label ID="sCBSecuredExposureLabel" runat="server" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    LTV Ratio
                </td>
                <td>
                    <asp:Label ID="lTVRatioLabel" runat="server">NA</asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Total LTV Ratio
                </td>
                <td>
                    <asp:Label ID="totalLTVRatioLabel" runat="server">NA</asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Residence Address Verification Flag
                </td>
                <td>
                    <asp:Label ID="ResidenceAddressVerifyLabeL1" runat="server" Font-Bold="True">♦&nbsp YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="ResidenceAddressVerifyLabeL2" runat="server">♦&nbsp NO</asp:Label>
                </td>
                <td>
                    <asp:Label ID="ResidenceAddressVerifyLabeL3" runat="server">♦&nbsp NA</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Home Telephone Verification Flag
                </td>
                <td>
                    <asp:Label ID="homeTelephoneVerifyLabel1" runat="server" Font-Bold="True">♦&nbsp YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="homeTelephoneVerifyLabel2" runat="server">♦&nbsp NO</asp:Label>
                </td>
                <td>
                    <asp:Label ID="homeTelephoneVerifyLabel3" runat="server">♦&nbsp NA</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Office Telephone Verification Flag
                </td>
                <td>
                    <asp:Label ID="officeTelephoneVerifyLabel1" runat="server" Font-Bold="True">♦&nbsp YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="officeTelephoneVerifyLabel2" runat="server">♦&nbsp NO</asp:Label>
                </td>
                <td>
                    <asp:Label ID="officeTelephoneVerifyLabel3" runat="server">♦&nbsp NA</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Mobile Telephone Verification Flag
                </td>
                <td>
                    <asp:Label ID="mobileTelephoneVerifyLabel1" runat="server" Font-Bold="True">♦&nbsp YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="mobileTelephoneVerifyLabel2" runat="server">♦&nbsp NO</asp:Label>
                </td>
                <td>
                    <asp:Label ID="mobileTelephoneVerifyLabel3" runat="server">♦&nbsp NA</asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Deviation Code (Asset Ops to fill)
                </td>
                <td>
                    <asp:Label ID="deviationCodeLabel" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                    DI Ratio
                </td>
                <td class="style1">
                    <asp:Label ID="dIRatioLabel" runat="server"></asp:Label>
                </td>
                <td class="style1">
                    &nbsp; (Proposed)
                </td>
                <td class="style1">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Internal Blacklist Flag
                </td>
                <td>
                    <asp:Label ID="internalBlacklistLabel1" runat="server">♦&nbsp; YES</asp:Label>
                </td>
                <td>
                    <asp:Label ID="internalBlacklistLabel2" runat="server" Font-Bold="True">♦&nbsp; NO</asp:Label>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Internal Blacklist Reason
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="internalBlacklistReasonLabel" runat="server" Font-Bold="True">NA</asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <table class="tableWidth" class="tableWidth" border="1" cellpadding="2" cellspacing="1">
            <tr style="height: 30px">
                <td style="width: 48%">
                    Residence State
                </td>
                <td style="width: 24%">
                    <asp:Label ID="residenceStateLabel1" runat="server">♦&nbsp; Dhaka</asp:Label>
                    <br />
                    <asp:Label ID="residenceStateLabel4" runat="server">♦&nbsp; Khulna</asp:Label>
                </td>
                <td style="width: 12%">
                    <asp:Label ID="residenceStateLabel2" runat="server">♦&nbsp; Chittagong</asp:Label>
                    <br />
                    <asp:Label ID="residenceStateLabel5" runat="server">♦&nbsp; Bogra</asp:Label>
                </td>
                <td style="width: 12%">
                    <asp:Label ID="Label1" runat="server">♦&nbsp; Sylhet</asp:Label>
                </td>
            </tr>
            <tr style="height: 30px">
                <td>
                    Residence Type
                </td>
                <td>
                    <asp:Label ID="residenceTypeLabel1" runat="server">♦&nbsp; Owned</asp:Label>
                    <br />
                    <asp:Label ID="residenceTypeLabel3" runat="server">♦&nbsp; Paremtal</asp:Label>
                </td>
                <td>
                    <asp:Label ID="residenceTypeLabel2" runat="server">♦&nbsp; Company</asp:Label>
                    <br />
                    <asp:Label ID="residenceTypeLabel4" runat="server">♦&nbsp; Rented</asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            </table>
            </td></tr></table>
            </center>
            <br />
            <br />
            <br />
            <br />
            <br />
	    <br />
	    <br />
            <center>
            <table width="98%" border="0" cellpadding="0" cellspacing="1"><tr><td align="left" style="font-weight:bold;">
                 <asp:Label ID="Label2" runat="server" Text="LL ID:"></asp:Label><asp:Label ID="secondLLIdLabel" runat="server" Text=""></asp:Label>   
            </td></tr></table>
            <table width="98%" border="0" cellpadding="0" cellspacing="1"><tr><td align="left">
            <table class="tableWidth" border="1" cellpadding="2" cellspacing="1">

            <tr>
                <td style="width: 50%">
                    Years of Residence at this Address
                </td>
                <td>
                    <asp:Label ID="yearsOfResidenceAtLabel" runat="server"></asp:Label>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    If Rented Rental Per Month
                </td>
                <td>
                    <asp:Label ID="ifRentedRentalLabel" runat="server"></asp:Label>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    No of Dependents
                </td>
                <td>
                    <asp:Label ID="noOfDependencyLabel" runat="server"></asp:Label>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Home Tel No
                </td>
                <td>
                    <asp:Label ID="homeTelNoLabel" runat="server"></asp:Label>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Office Tel No
                </td>
                <td>
                    <asp:Label ID="officeTelNoLabel" runat="server"></asp:Label>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Mobile Tel No
                </td>
                <td>
                    <asp:Label ID="mobileTelNoLabel" runat="server"></asp:Label>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Number of Cars
                </td>
                <td>
                    <asp:Label ID="numberOfCarsLabel" runat="server"></asp:Label>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Package / Promotion / Pre Approval Code
                </td>
                <td>
                    <asp:Label ID="packagePromotionLabel" runat="server"></asp:Label>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
        </table>
            <table class="tableWidth" border="1" cellpadding="2" cellspacing="1">
                <tr>
                    <td style="width: 50%">
                        Guarantor 1 SCB Flag
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="gurantor1ScbLabel1" runat="server">♦&nbsp YES</asp:Label>
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="gurantor1ScbLabel2" runat="server" Font-Bold="True">♦&nbsp NO</asp:Label>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table class="tableWidth" border="1px">
                <tr>
                    <td style="width: 50%">
                        Co-Borrower 1 Relationship
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="coBorrowerRelationshipLabel" runat="server">Always Spouse</asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Co-Borrower 1 SCB Flag
                    </td>
                    <td>
                        <asp:Label ID="coBorrower1SCBLabel" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Purchase Date (For Auto & Mortgage Only)
                    </td>
                    <td>
                        <asp:Label ID="purchaseDateLabel" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Application Date
                    </td>
                    <td>
                        <asp:Label ID="applicationDateLabel" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 30px">
                    <td>
                        Application Channel
                    </td>
                    <td>
                        <asp:Label ID="aplicationChannelLabel1" runat="server">♦&nbsp Branch</asp:Label>
                        <br />
                        <asp:Label ID="aplicationChannelLabel3" runat="server">♦&nbsp Direct Sales</asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="aplicationChannelLabel2" runat="server">♦&nbsp Telesales</asp:Label>
                        <br />
                        <asp:Label ID="aplicationChannelLabel4" runat="server">♦&nbsp Others</asp:Label>
                    </td>
                </tr>
                <tr style="height: 30px">
                    <td>
                        Secured Account Flag
                    </td>
                    <td>
                        <asp:Label ID="securedAccountLabel1" runat="server">♦&nbsp YES - For Auto, Mortgage</asp:Label>
                        <br />
                        <asp:Label ID="securedAccountLabel2" runat="server" Font-Bold="True">♦&nbsp NO - For Personal Loan</asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table class="tableWidth" border="1" cellpadding="2" cellspacing="1">
                <tr>
                    <td style="width: 50%">
                        Top Up Amount
                    </td>
                    <td style="width: 50%">
                        <asp:Label ID="topUpAmountLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Number of Top Up
                    </td>
                    <td>
                        <asp:Label ID="numberOfTopUpLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Top Up 1 Amount
                    </td>
                    <td>
                        <asp:Label ID="topUp1AmountLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Top Up 1 Date
                    </td>
                    <td>
                        <asp:Label ID="topUp1DateLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Top Up 2 Amount
                    </td>
                    <td>
                        <asp:Label ID="topUp2AmountLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Top Up 2 Date
                    </td>
                    <td>
                        <asp:Label ID="topUp2DateLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Top Up 3 Amount
                    </td>
                    <td>
                        <asp:Label ID="topUp3AmountLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Top Up 3 Date
                    </td>
                    <td>
                        <asp:Label ID="topUp3DateLabel" runat="server"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table class="tableWidth" border="1" cellpadding="2" cellspacing="1">
                <tr>
                    <td style="width: 50%">
                        Price of Car
                    </td>
                    <td colspan="2" style="width: 50%" align="right">
                        <asp:Label ID="priceOfCarLabel" runat="server" Text="(For Auto Loan)"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Current Market Value ( for used cars)
                    </td>
                    <td colspan="2" align="right">
                        <asp:Label ID="currentMatketValueLabel" runat="server" Text="(For Auto Loan)"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Car Model
                    </td>
                    <td colspan="2" align="right">
                        <asp:Label ID="carModelLabel" runat="server" Text="(For Auto Loan)"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Car Made Year
                    </td>
                    <td colspan="2" align="right">
                        <asp:Label ID="carMadeYearLabel" runat="server" Text="(For Auto Loan)"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Engine Size
                    </td>
                    <td colspan="2" align="right">
                        <asp:Label ID="engineSizeLabel" runat="server" Text="(For Auto Loan)"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Car Usage
                    </td>
                    <td colspan="2" align="right">
                        <asp:Label ID="carUsageLabel" runat="server" Text="(For Auto Loan)"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Dealer Code
                    </td>
                    <td colspan="2" align="right">
                        <asp:Label ID="dealerCodeLabel" runat="server" Text="(For Auto Loan)"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 45px">
                    <td>
                        Account Close Reason
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="accountCloseReasonLabel1" runat="server">♦&nbsp Charge Off</asp:Label>
                        <br />
                        <asp:Label ID="accountCloseReasonLabel3" runat="server">♦&nbsp Property Sold</asp:Label>
                        <br />
                        <asp:Label ID="accountCloseReasonLabel5" runat="server">♦&nbsp Loan Matured</asp:Label>
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="accountCloseReasonLabel2" runat="server">♦&nbsp Property Redeemed</asp:Label>
                        <br />
                        <asp:Label ID="accountCloseReasonLabel4" runat="server">♦&nbsp Refinance</asp:Label>
                        <br />
                        <asp:Label ID="accountCloseReasonLabel6" runat="server">♦&nbsp Other Early Redemption</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2">
                        Charge Off Reason Code
                    </td>
                    <td colspan="2">
                        To be input only upon charging off an account & not otherwise
                    </td>
                </tr>
                <tr style="height: 60px">
                    <td style="width: 25%">
                        <asp:Label ID="chargeOffReasonCodeLabel1" runat="server">♦&nbsp C = Credit Loss</asp:Label>
                        <br />
                        <asp:Label ID="chargeOffReasonCodeLabel3" runat="server">♦&nbsp F = Fraud Loss</asp:Label>
                        <br />
                        <asp:Label ID="chargeOffReasonCodeLabel5" runat="server">♦&nbsp L = Job Lost</asp:Label>
                        <br />
                        <asp:Label ID="chargeOffReasonCodeLabel7" runat="server">♦&nbsp S = Skipped</asp:Label>
                    </td>
                    <td width="25%">
                        <asp:Label ID="chargeOffReasonCodeLabel2" runat="server">♦&nbsp B = Bankruptcy</asp:Label>
                        <br />
                        <asp:Label ID="chargeOffReasonCodeLabel4" runat="server">♦&nbsp T = Counterfeit</asp:Label>
                        <br />
                        <asp:Label ID="chargeOffReasonCodeLabel6" runat="server">♦&nbsp P = Propoerty Damaged</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Name of Friend / Relative (Father’s / Mother’s Name)
                    </td>
                    <td colspan="2">
                        <asp:Label ID="nameOfFriendLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Friend / Relative Contact
                    </td>
                    <td colspan="2">
                        <asp:Label ID="friendRelativeContactLabel" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table width="98%" style="font-size: smaller;">
                <tr style="height: 30px; vertical-align: bottom">
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td align="center">                    
                        __________________________
                    </td>
                    <td align="center">
                        __________________________
                    </td>
                    <td align="center">
                        __________________________
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        Authorized Signature (CCU)
                    </td>
                    <td align="center">
                        Authorized Signature (Docs)
                    </td>
                    <td align="center">
                        Input by (Loan Admin)
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="signatureLabel1" runat="server"></asp:Label>
                     </td>
                    <td align="center">
                       <asp:Label ID="signatureLabel2" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Label ID="signatureLabel3" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="designationLabel1" runat="server"></asp:Label></td>
                    <td align="center">
                        <asp:Label ID="designationLabel2" runat="server"></asp:Label></td>
                    <td align="center">
                        <asp:Label ID="designationLabel3" runat="server"></asp:Label></td>
                </tr>
            </table>
            </td></tr></table>
            </center>
    </form>
</body>
</html>
