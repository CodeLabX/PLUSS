﻿using System;
using System.Collections.Generic;
using System.Web;
using BLL;
using BLL.Report;
using BusinessEntities;
using BusinessEntities.Report;

namespace PlussAndLoan.Reports
{
    public partial class Reports_DocumentListReport : System.Web.UI.Page
    {
        private DocumentManager documentManagerObj;
        public PLManager pLManagerObj = null;
        public UserManager userManagerObj = null;
        public AnalystApproveManager analystApproveManagerObj = null;
        private CustomerManager customerManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            documentManagerObj = new DocumentManager();
            pLManagerObj = new PLManager();
            userManagerObj = new UserManager();
            analystApproveManagerObj = new AnalystApproveManager();
            customerManagerObj = new CustomerManager();
            string llIdObj = "";
            if (Request.QueryString.Count != 0)
            {
                llIdObj = Request.QueryString["llId"].ToString();
                if(llIdObj.Length>0)
                {
                    LoadDataForDocomentList(llIdObj);
                }
            }
        }
        private void LoadDataForDocomentList(string lLId)
        {
            Document documentObj = documentManagerObj.GetDocumentsInfo(lLId);
            llIdLabel.Text = lLId;
            Customer customerObj = customerManagerObj.GetCustomerInfo(lLId);
            if (customerObj != null)
            {
                if (customerObj.MasterNo != null)
                {
                    if (customerObj.MasterNo.Length > 9)
                    {
                        accTypeLabel.Text = customerObj.MasterNo.Substring(2, 7).ToString();
                    }
                    else
                    {
                        accTypeLabel.Text = customerObj.MasterNo.ToString();
                    }
                }
                else
                {
                    accTypeLabel.Text = "To be opened";
                }
                bank1Label.Text = customerObj.PDC.ToString() + " - " + customerObj.PDCAccount.ToString();
                bank2Label.Text = customerObj.OtherStatementBank1.ToString() + " - " + customerObj.OtherStatementAcc1.ToString();
                bank3Label.Text = customerObj.OtherStatementBank2.ToString() + " - " + customerObj.OtherStatementAcc2.ToString();
                bank4Label.Text = customerObj.OtherStatementBank3.ToString() + " - " + customerObj.OtherStatementAcc3.ToString();
                bank5Label.Text = customerObj.OtherStatementBank4.ToString() + " - " + customerObj.OtherStatementAcc4.ToString();
                bank6Label.Text = customerObj.OtherStatementBank5.ToString() + " - " + customerObj.OtherStatementAcc5.ToString();

            }
            if (documentObj != null)
            {
                customerNameLabel.Text = documentObj.CustomerName.ToString();
            }

            PL pLObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(lLId));
            if (pLObj != null)
            {
                switch (pLObj.PlSegmentId.ToString())
                {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                        ForSalarideSegmentPart();
                        break;
                    default:
                        ForDefaltPart();
                        break;
                }
                Deviation deviation = null;// new Deviation();
                List<Deviation> deviationObjList = new List<Deviation>();
                DeviationManager deviationManagerObj = new DeviationManager();
                try
                {
                    deviationObjList = deviationManagerObj.GetAllDeviationInfoByLlId(lLId.ToString());
                    if (deviationObjList.Count > 0)
                    {
                        deviation = new Deviation();
                        deviation = deviationObjList[0];
                        User userObj = userManagerObj.GetUserInfo((int)deviation.UserId);
                        if (userObj != null)
                        {
                            creditSigNameLabel.Text = userObj.Name.ToString();
                            designationLabel.Text = userObj.Designation.ToString();
                        }
                    }

                }
                catch { }
                //Alert.Show("Invalid
                //User userObj = userManagerObj.GetUserInfo((int)pLObj.UserId);
                //if (userObj != null)
                //{
                //    creditSigNameLabel.Text = userObj.Name.ToString();
                //    designationLabel.Text = userObj.Designation.ToString();
                //}
            }
        }
        private void ForSalarideSegmentPart()
        {
            yes11Label.Font.Bold = true;
            yes2Label.Font.Bold = true;
            yes3Label.Font.Bold = true;
            yes11Label.Font.Size = 10;
            yes2Label.Font.Size = 10;
            yes3Label.Font.Size = 10;


            yes17Label.Font.Bold = true;
            yes18Label.Font.Bold = true;
            yes19Label.Font.Bold = true;
            yes17Label.Font.Size = 10;
            yes18Label.Font.Size = 10;
            yes19Label.Font.Size = 10;

            NA4Label.Font.Bold = true;
            NA5Label.Font.Bold = true;
            NA6Label.Font.Bold = true;
            NA7Label.Font.Bold = true;
            NA8Label.Font.Bold = true;
            NA9Label.Font.Bold = true;
            NA10Label.Font.Bold = true;

            NA4Label.Font.Size = 10;
            NA5Label.Font.Size = 10;
            NA6Label.Font.Size = 10;
            NA7Label.Font.Size = 10;
            NA8Label.Font.Size = 10;
            NA9Label.Font.Size = 10;
            NA10Label.Font.Size = 10;

        }
        private void ForDefaltPart()
        {
            yes1Label.Font.Bold = true;
            yes2Label.Font.Bold = true;
            yes3Label.Font.Bold = true;
            yes11Label.Font.Size = 10;
            yes2Label.Font.Size = 10;
            yes3Label.Font.Size = 10;

            yes17Label.Font.Bold = false;
            yes18Label.Font.Bold = false;
            yes19Label.Font.Bold = false;

            NA17Label.Font.Bold = true;
            NA18Label.Font.Bold = true;
            NA19Label.Font.Bold = true;

            NA17Label.Font.Size = 10;
            NA18Label.Font.Size = 10;
            NA19Label.Font.Size = 10;

            NA4Label.Font.Bold = false;
            NA5Label.Font.Bold = false;
            NA6Label.Font.Bold = false;
            NA7Label.Font.Bold = false;
            NA8Label.Font.Bold = false;
            NA9Label.Font.Bold = false;
            NA10Label.Font.Bold = false;

        }
    }
}
