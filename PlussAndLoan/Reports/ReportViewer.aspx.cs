﻿using System;
using BLL;
using BLL.Report;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace PlussAndLoan.Reports
{
    public partial class Reports_ReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadReport();
            }
        }

        private void LoadReport()
        {
            var reportType = Session["ReportType"].ToString();
            var dataSource=new object();
            var reportPath = "";

            if (reportType == "SecurityMatrix")
            {
                dataSource = new SecurityMatrixReportManager().GetSystemGeneratedSecurityMatrix();
                reportPath = Server.MapPath(@"rptSecurityMatrix.rpt");
            }

            if (reportType == "SecurityMatrix_Auto")
            {
                dataSource = new SecurityMatrixReportManager().GetSystemGeneratedSecurityMatrix_Auto();
                reportPath = Server.MapPath(@"rptSecurityMatrix_Auto.rpt");
            }
            //if (reportType == "ExceptionReport")
            //{
            //    var frmDate = Convert.ToDateTime(Session["fromDate"]);
            //    var toDate = Convert.ToDateTime(Session["toDate"]);
            //    dataSource = CustomException.GetAllExceptions(frmDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"));

            //    reportPath=Server.MapPath(@"rptException.rpt");
            //}
            if (reportType == "UserListReport")
            {
                dataSource = new UserManager().GetAllUsers();
                reportPath= Server.MapPath(@"UserListReport.rpt");
            }

            if (reportType == "AuditTrailReport")
            {
                var frmDate = Convert.ToDateTime(Session["FromDate"]);
                var toDate = Convert.ToDateTime(Session["ToDate"]);
                dataSource = new UserManager().GetAuditTrailData(frmDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"));
                reportPath = Server.MapPath(@"AuditTrailReport.rpt");
            }

            ReportDocument rd = new ReportDocument();
            var path = reportPath;
            rd.Load(path);
            rd.SetDataSource(dataSource);
            string userId = Convert.ToString(Session["UserId"]);
            var userName = Convert.ToString(Session["UserName"]);

            rd.SetParameterValue("@usrId", userId);
            rd.SetParameterValue("@usrName", userName);
            rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, false, reportType+"_" + DateTime.Now.ToString());
            //crViewer.ReportSource = rd;

    
            // Session["ReportType"] = null;
            // Session["fromDate"] = null;
            //Session["toDate"] = null;
        }

    }
}
