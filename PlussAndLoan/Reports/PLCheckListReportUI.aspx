﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.Reports.Reports_PLCheckListReportUI" Codebehind="PLCheckListReportUI.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        .tableWidth
        {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }
        .tableWidth td
        {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }
        .style1
        {
            /*width: 571px;*/
        }
        .style2
        {
            height: 14px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function PrintPLCheckList()
        {
            window.print();
        }
        function ColorChange()
        {
             document.getElementById('printButton').style.backgroundColor = 'red';
        }
    </script>
</head>
<body style="margin-top:0.05px; left:0.25px; right:0.05px; margin-bottom:0.05px;" onload="PrintPLCheckList();">
    <fieldset style="width:901px; height:812px;">
    <center>
        <table style="height: 18px; width:95%;">
            <tr>
                <td style="font-size: larger;" align="center" colspan="2">
                    CREDIT CHECKLIST - PERSONAL LOAN
                </td>
            </tr>
            <tr>
                <td style="font-size: 12px; font-weight:bold; width:80%" align="right">
                    LL ID
                </td>
                <td style="font-size: 12px;" align="right">
                    <asp:Label ID="llIdLabel" runat="server" Font-Bold="true" Text=""></asp:Label>&nbsp&nbsp
                </td>
            </tr>
            <tr>
                <td style="font-size: 12px; font-weight:bold;" align="right" class="style1">
                    Date
                </td>
                <td style="font-size: 12px;" align="right">
                    <asp:Label ID="dateLabel" runat="server" Font-Bold="true" Text=""></asp:Label>&nbsp&nbsp
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table style="width:95%">
            <tr>
                <td style="font-size: 13px; width:20%; font-weight:bold;" align="left">
                    Customer Name
                </td>
                <td style="font-size: 13px; width:25%;" align="left">
                    <asp:Label ID="customerNameLabel" runat="server" Font-Bold="true" Text=""></asp:Label>&nbsp&nbsp
                </td>
                <td style="width:23%;">&nbsp;</td>
                <td style="width:22%;">&nbsp;</td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-weight:bold;" align="left">
                 Age:
                </td>
                <td style="font-size: 13px;" align="left">
                    <asp:Label ID="ageLabel" runat="server" Font-Bold="true" Text=""></asp:Label>&nbsp&nbsp
                </td>
                <td style="font-size: 13px; font-weight:bold;" align="right">
                    Master No:
                </td>
                <td style="font-size: 13px;" align="right">
                    <asp:Label ID="masterNoLabel" runat="server" Font-Bold="true" Text=""></asp:Label>&nbsp&nbsp
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style="padding: 2px; width:95%; font-size: xx-small">
            <tr style="font-weight: bold; font-size: x-small">
                <td style="width: 150px" align="center">
                    PDD COMPLIANCE
                </td>
                <td colspan="2" style="font-weight: bold; text-align: center; width: 250px">
                    <asp:Label ID="segmentCodeLabel" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="4" style="font-weight: bold; width: 470px" align="center">
                    <asp:Label ID="segmentDescriptionLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; font-size: 10.5px;">
                    Max Loan
                </td>
                <td align="center">
                    <asp:Label ID="maxLoanLabel" runat="server" Text="" Style="text-align: center"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="maxLoanYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    Experience
                </td>
                <td colspan="2" align="center">
                    <asp:Label ID="experienceLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="experienceYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; font-size: 10.5px;">
                    Min Loan
                </td>
                <td align="center">
                    <asp:Label ID="minLoanLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="minLoanYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    A/C Relationship
                </td>
                <td colspan="2" align="center">
                    <asp:Label ID="acRelationshipLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="acRelationshipYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; font-size: 10.5px;">
                    Purpose
                </td>
                <td align="center">
                    <asp:Label ID="purposeLabel" runat="server" Text="As Per PDD"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="purposeYesNoLabel" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    Minimum Income
                </td>
                <td colspan="2" align="center">
                    <asp:Label ID="minimumIncomeLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="minimumIncomeYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Duplication List &amp; NORKOM Checked
                </td>
                <td align="center">
                    <asp:Label ID="durationListYesNoLabel" runat="server" Text="Yes"></asp:Label>
                </td>
                <td rowspan="2" align="center">
                    DB RATIO<br />
                    <asp:Label ID="dbRationLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    Income Range
                </td>
                <td style="text-align: center">
                    DBR
                </td>
                <td align="center">
                    <asp:Label ID="dbRationLabel2" runat="server" Text="As Per Grid"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; font-size: 10.5px;">
                    Min Age
                </td>
                <td align="center">
                    <asp:Label ID="minAgeLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="minageYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="incomeRangeLabel" runat="server" Text="50K-100K"></asp:Label>
                </td>
                <td style="text-align: center">
                    <asp:Label ID="DBRLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="dbRationYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; font-size: 10.5px;">
                    Max Age at Maturity
                </td>
                <td align="center">
                    <asp:Label ID="maxAgeLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="maxAgeYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
                <td rowspan="2" align="center">
                    TENUR<br />
                    <asp:Label ID="tenurLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    Loan Amt Range
                </td>
                <td align="center">
                    TNR
                </td>
                <td align="center">
                    <asp:Label ID="tenurLabel2" runat="server" Text="As Per Grid"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; font-size: 10.5px;">
                    DOB Matches with
                </td>
                <td align="center">
                    <asp:Label ID="dobMatchLabel" runat="server" Text="eBBS/Cards 400/ Photo ID"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="dobMatchYesNoLabel" runat="server" Text="Yes"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="loanAmountRangeLabel" runat="server" Text="500K+"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="TNRLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="tenurYesNoLabel" runat="server" Text="As Per Grid"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; font-size: 10.5px;">
                    Physical Verificarion
                </td>
                <td align="center">
                    <asp:Label ID="physicalVerificationLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="physicalVerificationYesNoLabel" runat="server" Text="Yes"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="mueLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="mueLabel1" runat="server" Text="<="></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="mueLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="mueYesNoLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; font-size: 10.5px;">
                    Telephone
                </td>
                <td align="center">
                    <asp:Label ID="telephoneLabel" runat="server" Text="at Residence / Mobile"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="telephoneYesNoLabel" runat="server" Text="Yes"></asp:Label>
                </td>
                <td colspan="3">
                    <asp:Label ID="guaranteeLabel" runat="server" Text="Guarantee"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="guaranteeYesNoLabel" runat="server" Text="Yes"></asp:Label>
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style="font-size: 10.5px; width: 95%;">
            <tr>
                <td colspan="6" >
                    <asp:Label ID="accountTypeHeaderLabel" runat="server" Font-Bold="true" Text="Account Type" Style="font-size: x-small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                <asp:Label ID="topUpLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td>
                 <asp:Label ID="topUpYesLabel1" runat="server" Text="Yes"></asp:Label>
                 </td>
                <td>
                <asp:Label ID="topUpNoLabel1" runat="server" Text="No"></asp:Label>
                </td>
                <td  align="left">
                 <asp:Label ID="topUpLabel4" runat="server" Text=""></asp:Label>
                 </td>
                <td>
                  <asp:Label ID="topUpYesLabel4" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                 <asp:Label ID="topUpNoLabel4" runat="server" Text="No"></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="topUpLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="topUpYesLabel2" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="topUpNoLabel2" runat="server" Text="No"></asp:Label>
                </td>
                <td  align="left">
                    <asp:Label ID="topUpLabel5" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="topUpYesLabel5" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="topUpNoLabel5" runat="server" Text="No"></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="topUpLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="topUpYesLabel3" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="topUpNoLabel3" runat="server" Text="No"></asp:Label>
                </td>
                <td  align="left">
                    <asp:Label ID="topUpLabel6" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Label ID="topUpYesLabel6" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="topUpNoLabel6" runat="server" Text="No"></asp:Label>
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style=" width:95%; font-size: 10.5px;">
            <tr>
                <td colspan="2" style="font-size: 10.5px; font-weight:bold; background-color:#C0C0C0;"  align="center">
                    CREDIT REMARKS
                </td>
            </tr>
            <tr>
                <td style="width: 335px; text-align: center;">
                    <b>STRENGTH</b>
                </td>
                <td style="text-align: center; width: 335px;">
                    <b>WEAKNESS</b>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="strengthLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td  align="left">
                    <asp:Label ID="weaknessLabel1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="strengthLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td  align="left">
                    <asp:Label ID="weaknessLabel2" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="strengthLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td  align="left">
                    <asp:Label ID="weaknessLabel3" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="strengthLabel4" runat="server" Text=""></asp:Label>
                </td>
                <td  align="left">
                    <asp:Label ID="weaknessLabel4" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="strengthLabel5" runat="server" Text=""></asp:Label>
                </td>
                <td  align="left">
                    <asp:Label ID="weaknessLabel5" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="strengthLabel6" runat="server" Text=""></asp:Label>
                </td>
                <td  align="left">
                    <asp:Label ID="weaknessLabel6" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style="width:95%; font-size: 10.5px;">
            <tr>
                <td colspan="5" style="font-weight: bold; text-align: center; font-size: 10.5px;">
                    VERIFICATION BY AGENCY
                </td>
            </tr>
            <tr style="font-size: 10.5px;">
                <td style="width: 200px;" colspan="2" align="center">
                    Telephone : Residence / Office
                </td>
                <td style="width: 150px;" align="center">
                    Residence Address
                </td>
                <td style="width: 170px;" align="center">
                    Office / Business Address
                </td>
                <td style="text-align: center; width: 150px;">
                    Guarantee
                </td>
            </tr>
            <tr style="font-size: 10.5px;">
                <td style="width: 100px;" align="center">
                    Mobile
                </td>
                <td style="width: 100px;" align="center">
                    YES | NO | NA
                </td>
                <td style="text-align: center">
                    YES&nbsp; |&nbsp; NO&nbsp; |&nbsp; NA
                </td>
                <td align="center">
                    <%--<asp:Label ID="Label41" runat="server" Style="text-align: center" Text=""></asp:Label>--%>
                    YES | NO | NA
                </td>
                <td style="text-align: center">
                    YES | NO | NA
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style="font-size: 10.5px; width:95%;">
            <tr>
                <td colspan="7" align="center" style="font-size: small; font-weight: bold;">
                    EXPOSURE
                </td>
            </tr>
            <tr>
                <td>
                    <b>Facility Type</b>
                </td>
                <td>
                    <b>Outstanding</b>
                </td>
                <td>
                    <b>Original</b> <b>Limit</b>
                </td>
                <td rowspan="2">
                    <b>Disb Date</b>
                </td>
                <td rowspan="2">
                    <b>Is Repmt Regular</b>
                </td>
                <td rowspan="2">
                    <b>EMI Factor</b>
                </td>
                <td align="center">
                    <b>BANCA</b>
                </td>
            </tr>
            <tr>
                <td  align="left" class="style2">
                    Existing PL O/S
                </td>
                <td align="center" class="style2">
                    <asp:Label ID="existingPLOutstandingLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center" class="style2">
                    <asp:Label ID="existingPLOriginalLimitLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center" class="style2">
                 <asp:Label ID="bancaStatusLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr style="color: #FFFFFF; background-color: #000000">
                <td  align="left">
                    <asp:Label ID="topUpAmountLabel" runat="server" Text="" Style="text-align: left"></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="topUpAmountOutstandingLabel" runat="server" Text=""></asp:Label>
                </td>
                <td style="background-color: #C0C0C0" align="center">
                    <asp:Label ID="topUpAmountOriginalLimitLabel" runat="server" Text=""></asp:Label>
                </td>
                <td style="background-color: #C0C0C0" align="center">
                    <asp:Label ID="topUpAmountRepaymentLabel" runat="server" Text=""></asp:Label>
                </td>
                <td style="background-color: #C0C0C0" align="center">
                    &nbsp;
                </td>
                <td style="background-color: #C0C0C0" align="center">
                    <asp:Label ID="topUpAmountEMIFactorLabel" runat="server" Text=""></asp:Label>
                </td>
                <td style="background-color: #C0C0C0" align="center">
                    <asp:Label ID="topUpAmountSafetyPlusNoLabel" runat="server" Text=""></asp:Label>
                    <asp:Label ID="includedBancaLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="thisTotalPLLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="thisTotalOutstandingLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="thisTotalOriginalLimitLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="thisTotalRepaymentLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td align="center">
                    Disburse date
                </td>
                <td align="center">
                    Expiry Date
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="facalityLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOutstandingLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOriginalLimitLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityRepaymentLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    &nbsp; YES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    NO
                </td>
                <td align="center">
                    <asp:Label ID="facalityDisburseDateLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityExpiryDateLabel1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="facalityLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOutstandingLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOriginalLimitLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityRepaymentLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    &nbsp; YES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    NO
                </td>
                <td align="center">
                    <asp:Label ID="facalityDisburseDateLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityExpiryDateLabel2" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="facalityLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOutstandingLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOriginalLimitLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityRepaymentLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    &nbsp; YES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    NO
                </td>
                <td align="center">
                    <asp:Label ID="facalityDisburseDateLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityExpiryDateLabel3" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="facalityLabel4" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOutstandingLabel4" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOriginalLimitLabel4" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityRepaymentLabel4" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    &nbsp; YES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    NO
                </td>
                <td align="center">
                    <asp:Label ID="facalityDisburseDateLabel4" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityExpiryDateLabel4" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    <asp:Label ID="facalityLabel5" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOutstandingLabel5" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityOriginalLimitLabel5" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityRepaymentLabel5" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    &nbsp; YES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    NO
                </td>
                <td align="center">
                    <asp:Label ID="facalityDisburseDateLabel5" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="facalityExpiryDateLabel5" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    Credit Card Limit
                </td>
                <td align="center">
                    <asp:Label ID="creditCardLimitOutstandingLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="creditCardLimitOriginalLimitLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="creditCardLimitRepaymentLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    &nbsp; YES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    NO
                </td>
                <td align="center">
                    <asp:Label ID="creditCardLimitDisburseDateLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="creditCardLimitExpiryDateLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td  align="left">
                    Total
                </td>
                <td align="center">
                    <asp:Label ID="totalOutstandingLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td align="center">
                    <asp:Label ID="totalRepaymentLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3" align="right">
                <asp:Label ID="otherBankLiabilitiesLabel" runat="server" Text="Other Bank Liabilities"></asp:Label></td>
                <td align="center">
                <asp:Label ID="otherBankLiabilitiesAmountLabel" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="3">
                    &nbsp;</td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style=" width:95%; font-size: 10.5px;">
            <tr style="font-size: small; background-color:#C0C0C0;">
                <td style="width: 50px;" align="center">
                    No.
                </td>
                <td style="width: 350px;" align="center">
                    PDD DEVIATION
                </td>
                <td style="width: 150px;" align="center">
                    CODE
                </td>
                <td style="width: 120px;" align="center">
                    LEVEL
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; text-align: center">
                    1.
                </td>
                <td align="center">
                    <asp:Label ID="pddDeviationLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="deviationCodeLabel1" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="deviationLevelLabel1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; text-align: center">
                    2.
                </td>
                <td align="center">
                    <asp:Label ID="pddDeviationLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="deviationCodeLabel2" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="deviationLevelLabel2" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; text-align: center">
                    3.
                </td>
                <td align="center">
                    <asp:Label ID="pddDeviationLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="deviationCodeLabel3" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="deviationLevelLabel3" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; text-align: center">
                    4.
                </td>
                <td align="center">
                    <asp:Label ID="pddDeviationLabel4" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="deviationCodeLabel4" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="deviationLevelLabel4" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style="width:95%; font-size: 10.5px;">
            <tr>
                <td style="text-align: center; width: 150px;">
                    <b>NET INCOME</b>
                </td>
                <td style="text-align: center; width: 80px;">
                    <b>DBR</b>
                </td>
                <td style="text-align: center; width: 80px;">
                    <b>MUE</b>
                </td>
                <td style="text-align: center; width: 150px;">
                    <b>LOAN AMOUNT</b>
                </td>
                <td style="text-align: center; width: 100px;">
                    <b>EMI</b>
                </td>
                <td style="text-align: center; width: 60px;">
                    <b>TENDOR</b>
                </td>
                <td style="text-align: center; width: 50px;">
                    <b>IR</b>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="netIncomeLowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="DBRLowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="MUELowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="LoanAmountLowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="EMILowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="TenorLowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
                <td align="center">
                    <asp:Label ID="IRLowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style=" width:95%; font-size: 10.5px; font-weight: bold; text-align: center">
            <tr>
                <td style="width: 100px;" align="center">
                    <asp:Label ID="standingOrderOrPDCLabel" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 250px;" align="center">
                    <asp:Label ID="standingOrderLabel" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 50px;" align="center">
                    A/C :
                </td>
                <td style="width: 150px;" align="center">
                    <asp:Label ID="acLowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 70px;" align="center">
                    Rpmnt Date
                </td>
                <td style="width: 50px;" align="center">
                    <asp:Label ID="rpmntLowerPartLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table style="width: 95%; font-size: 10.5px;">
            <tr>
                <td align="center">
                <br />
                <br />
                    ________________________
                </td>
                <td align="center">
                <br />
                <br />
                    ________________________
                </td>
                <td align="center">
                <br />
                <br />
                    ________________________
                </td>
                <td align="center">
                <br />
                <br />
                    ________________________
                </td>
            </tr>
            <tr>
                <td align="center">
                    Appraised By
                </td>
                <td align="center">
                    Supported By
                </td>
                <td align="center">
                    Approved/Declined By
                </td>
                <td align="center">
                    Approver's Seal
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="appraisedLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td align="center">
                    <asp:Label ID="approvedNameLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="designationLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>&nbsp</td>
                <td align="center">
                <asp:Label ID="approvedDesignationLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>&nbsp</td>
            </tr>
            <tr>
                <td>
                    &nbsp
                </td>
                <td>
                    &nbsp;</td>
                <td style="font-weight:bold;">
                DLA CODE:<asp:Label ID="approvedDalCodeLabel" runat="server" Text=""></asp:Label>
                </td>
                <td>&nbsp</td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tableWidth" style=" width:95%;background-color: #C0C0C0;
            border-style: solid none solid none; border-bottom-width: thin; border-top-width: thin;">
            <tr>
                <td align="left" style="font-weight:bold;">
                    Remarks / Condition :
                </td>
            </tr>
        </table>
    </center>
    <center>
        <div id="lastRemarksDiv" runat="server"></div>  
    </center> 
    </fieldset>
</body>
</html>
