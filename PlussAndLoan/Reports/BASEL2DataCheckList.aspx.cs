﻿using System;
using System.Collections.Generic;
using System.Web;
using BLL;
using BusinessEntities;

namespace PlussAndLoan.Reports
{
    public partial class Reports_BASEL2DataCheckList : System.Web.UI.Page
    {
        LoanInformationManager loanInformationManagerObj =null;
        PLManager pLManagerObj =null;
        LoanApplicationManager loanApplicationManagerObj =null;
        LoanInformation loanInformationObj =null;
        PL pLObj =null;
        Customer customerObj =null;
        CustomerManager customerManagerObj =null;
        UserManager userManagerObj = null;
        int lLId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            loanApplicationManagerObj = new LoanApplicationManager();
            pLManagerObj = new PLManager();
            loanInformationManagerObj = new LoanInformationManager();
            loanInformationObj = new LoanInformation();
            pLObj = new PL();
            customerObj = new Customer();
            customerManagerObj = new CustomerManager();
            userManagerObj = new UserManager();
            if (Request.QueryString.Count != 0)
            {
                lLId = Convert.ToInt32(Request.QueryString["lLId"].ToString());
            }
            if (lLId > 0)
            {
                LoadData(lLId);
            }
        }
        private void LoadData(Int32 lLId)
        {
            string ACNo = "";
            loanInformationObj = loanApplicationManagerObj.GetLoanInformation(lLId);
            //loanInformationObj = loanInformationManagerObj.GetParticularApplicantLoanInfo(lLId);
            pLObj = pLManagerObj.SelectPLInfo(lLId);
            customerObj = customerManagerObj.GetCustomerInfo(lLId.ToString());
            llidLabel.Text = lLId.ToString();
            secondLLIdLabel.Text=lLId.ToString();
            customerNameLabel.Text = loanInformationObj.ApplicantName;
            ACNo = loanInformationObj.AccountNo.PreAccNo1 + loanInformationObj.AccountNo.MasterAccNo1 + loanInformationObj.AccountNo.PostAccNo1;
            if (ACNo.Length>9)
            {
                customerMasterLabel.Text = ACNo.Substring(2, 7).ToString(); //loanInformationObj.AccountNo.PreAccNo1 + loanInformationObj.AccountNo.MasterAccNo1 + loanInformationObj.AccountNo.PostAccNo1;
            }
            nationalityLabel.Text = "";
            if (loanInformationObj.ExtraField1.ToString() != null)
            {
                genderCodeLabel.Text = loanInformationObj.ExtraField1.ToString();
            }
            if (loanInformationObj.MaritalStatus.Contains("Single"))
            {
                maritalStatusLabel2.Font.Bold = true;
                maritalStatusLabel1.Font.Bold = false;
                maritalStatusLabel3.Font.Bold = false;
            }
            if (loanInformationObj.MaritalStatus.Contains("Married"))
            {
                maritalStatusLabel1.Font.Bold = true;
                maritalStatusLabel3.Font.Bold = false;
                maritalStatusLabel2.Font.Bold = false;
            }
            if (loanInformationObj.MaritalStatus.Contains("Other"))
            {
                maritalStatusLabel3.Font.Bold = true;
                maritalStatusLabel1.Font.Bold = false;
                maritalStatusLabel2.Font.Bold = false;
            }
            switch (loanInformationObj.Profession.ToString())
            {
                case "Salaried":
                    employmentStatusCodeLabel1.Font.Bold = true;
                    employmentStatusCodeLabel2.Font.Bold = false;
                    employmentStatusCodeLabel3.Font.Bold = false;
                    employmentStatusCodeLabel4.Font.Bold = false;
                    break;
                case "Self Employed":
                    employmentStatusCodeLabel2.Font.Bold = true;
                    employmentStatusCodeLabel1.Font.Bold = false;
                    employmentStatusCodeLabel3.Font.Bold = false;
                    employmentStatusCodeLabel4.Font.Bold = false;
                    break;
                case "Business":
                    employmentStatusCodeLabel1.Font.Bold = false;
                    employmentStatusCodeLabel2.Font.Bold = false;
                    employmentStatusCodeLabel3.Font.Bold = false;
                    employmentStatusCodeLabel4.Font.Bold = true;
                    break;
                default:
                
                    break;

            }
            highestEducationQulificationLabel.Text = loanInformationObj.EducationalLevel;
            businessEstablishDateLabel.Text = "";
            monthsTotalWorkExperienceLabel.Text = loanInformationObj.JobDuration.ToString();
            monthsCurrentJobLabel.Text = loanInformationObj.JobDuration.ToString();    // not sure
            dateJoiningLabel.Text = "";
            double sCBUnsecured=0;
            double sCBsecuredExposure=0;
            if (pLObj != null)
            {
                monthlyIncomeLabel.Text = pLObj.PlIncome.ToString("#,###");
                otherMonthlyIncomeLabel.Text = pLObj.PlOtherincome.ToString("#,###");
                List<double> exposureList = customerManagerObj.GetExposure(lLId.ToString());
                if (pLObj.PlSafetyplusId == 1)
                {
                    sCBUnsecured = exposureList[1] + pLObj.PlScbcreditcardlimit + pLObj.PlBancaLoanAmount;
                    sCBUnsecuredLabel.Text = sCBUnsecured.ToString("#,###"); //Convert.ToString(exposureList[1] + pLObj.PlScbcreditcardlimit + pLObj.PlBancaLoanAmount);
                }
                else
                {
                    sCBUnsecured = exposureList[1] + pLObj.PlScbcreditcardlimit + pLObj.PlProposedloanamount;
                    sCBUnsecuredLabel.Text = sCBUnsecured.ToString("#,###");// Convert.ToString(exposureList[1] + pLObj.PlScbcreditcardlimit + pLObj.PlProposedloanamount);
                }
                sCBsecuredExposure = exposureList[0];
                sCBSecuredExposureLabel.Text = sCBsecuredExposure.ToString("#,###"); //Convert.ToString(exposureList[0]);

                dIRatioLabel.Text = Convert.ToString(pLObj.PlDbr) + " %";
            
                Deviation deviation = null;// new Deviation();
                List<Deviation> deviationObjList = new List<Deviation>();
                DeviationManager deviationManagerObj = new DeviationManager();
                try
                {
                    deviationObjList = deviationManagerObj.GetAllDeviationInfoByLlId(lLId.ToString());
                    if (deviationObjList.Count > 0)
                    {
                        deviation = new Deviation();
                        deviation = deviationObjList[0];
                        LoadUserInfo((int)deviation.UserId);
                        if (deviation.DeviLevel1 > 0)
                        {
                            creditPolicyLabel1.Text = "L" + deviation.DeviLevel1.ToString();
                        }
                        else
                        {
                            creditPolicyLabel1.Text = "L1";
                        }
                    }

                }
                catch { }//Alert.Show("Invalid

            }

            TypeAccountLabel.Text = "";
            designationTitleLabel.Text = "";
            DepartmentLabel.Text = "";
            DateOfBirthLabel.Text = "";


            homeTelNoLabel.Text = loanInformationObj.ContactNumber.ContactNo3;
            officeTelNoLabel.Text = loanInformationObj.ContactNumber.ContactNo2;
            mobileTelNoLabel.Text = loanInformationObj.ContactNumber.ContactNo1;
            if (DateFormat.IsDate(loanInformationObj.RecieveDate))
            {
                applicationDateLabel.Text = loanInformationObj.RecieveDate.ToString("dd-MM-yyyy");
            }

            LoadLoanApplictionEntryUser(loanInformationObj.EntryUserId);
            double topUpAmount1 = 0;
            double topUpAmount2 = 0;
            double topUpAmount3 = 0;
            if (pLObj.PlLoantypeId == 3)
            {
                if (customerObj != null)
                {
                    int count = 0;
                    switch (customerObj.TopUpCount.ToString())
                    {
                        case "0":
                            if (pLObj.PlSafetyplusId == 1)
                            {
                                topUp1AmountLabel.Text = Convert.ToDouble(pLObj.PlBancaLoanAmount - customerObj.TopUpAmount1).ToString("#,###");
                            }
                            else
                            {
                                topUp1AmountLabel.Text = Convert.ToDouble(pLObj.PlProposedloanamount - customerObj.TopUpAmount1).ToString("#,###");
                            }
                            //topUp1DateLabel.Text = customerObj.TopUpDate1.ToString("dd-MM-yyyy");
                            topUp1DateLabel.Text = "N/A";
                            topUp2AmountLabel.Text = "N/A";
                            topUp2DateLabel.Text = "N/A";
                            topUp3AmountLabel.Text = "N/A";
                            topUp3DateLabel.Text = "N/A";
                            count = 1;
                            break;
                        case "1":
                            topUp1AmountLabel.Text = customerObj.TopUpAmount2.ToString("#,###");
                            if (pLObj.PlSafetyplusId == 1)
                            {
                                topUpAmount2 = (pLObj.PlBancaLoanAmount - customerObj.TopUpAmount1);
                            }
                            else
                            {
                                topUpAmount2 = (pLObj.PlProposedloanamount - customerObj.TopUpAmount1);
                            }
                            topUp2AmountLabel.Text = topUpAmount2.ToString("#,###");
                            //topUp2DateLabel.Text = customerObj.TopUpDate2.ToString("dd-MM-yyyy");
                            topUp1DateLabel.Text = customerObj.TopUpDate2.ToString("dd-MM-yyyy");
                            topUp2DateLabel.Text = "N/A";
                            topUp3AmountLabel.Text = "N/A";
                            topUp3DateLabel.Text = "N/A";

                            count = 2;
                            break;
                        case "2":
                            topUp1AmountLabel.Text = customerObj.TopUpAmount2.ToString("#,###");
                            topUp2AmountLabel.Text = customerObj.TopUpAmount3.ToString("#,###");
                            if (pLObj.PlSafetyplusId == 1)
                            {
                                topUpAmount3 = (pLObj.PlBancaLoanAmount - customerObj.TopUpAmount1);
                            }
                            else
                            {
                                topUpAmount3 = (pLObj.PlProposedloanamount - customerObj.TopUpAmount1);
                            }
                            topUp3AmountLabel.Text = topUpAmount3.ToString("#,###");
                            //topUp3DateLabel.Text = customerObj.TopUpDate3.ToString("dd-MM-yyyy");
                            topUp3DateLabel.Text = "N/A";
                            topUp2DateLabel.Text = customerObj.TopUpDate3.ToString("dd-MM-yyyy");
                            topUp1DateLabel.Text = customerObj.TopUpDate2.ToString("dd-MM-yyyy");
                            count = 3;
                            break;

                    }
                    numberOfTopUpLabel.Text = count.ToString();
               
                    if (pLObj.PlSafetyplusId == 1)
                    {
                        topUpAmountLabel.Text = Convert.ToDouble(pLObj.PlBancaLoanAmount - customerObj.TopUpAmount1 + customerObj.TopUpAmount2 + customerObj.TopUpAmount3).ToString("#,###");
                    }
                    else
                    {
                        topUpAmountLabel.Text = Convert.ToDouble(pLObj.PlProposedloanamount - customerObj.TopUpAmount1 + customerObj.TopUpAmount2 + customerObj.TopUpAmount3).ToString("#,###");
                    }

                }
            }
            else
            {
                topUpAmountLabel.Text = "N/A";
                numberOfTopUpLabel.Text = "N/A";
                topUp1AmountLabel.Text = "N/A";
                topUp1DateLabel.Text = "N/A";
                topUp2AmountLabel.Text = "N/A";
                topUp2DateLabel.Text = "N/A";
                topUp3AmountLabel.Text= "N/A";
                topUp3DateLabel.Text= "N/A";
            }
        }

        private void LoadUserInfo(int llID)
        {
            User userObj = userManagerObj.GetUserInfo(llID);
            if (userObj != null)
            {
                signatureLabel1.Text = userObj.Name.ToString();
                designationLabel1.Text = userObj.Designation.ToString();
            }
        }
        private void LoadLoanApplictionEntryUser(int llID)
        {
            User userObj = userManagerObj.GetUserInfo(llID);
            if (userObj != null)
            {
                //signatureLabel3.Text = userObj.Name.ToString();
                //designationLabel3.Text = userObj.Designation.ToString();
            }
        }
    

    }
}
