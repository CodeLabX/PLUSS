﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PlussAndLoan.Reports.Reports_ActivityReport" Codebehind="ActivityReport.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Activity Report</title>
        <%--/*<style type="text/css" media="print">    
        /*.landscape
        {
            margin: 0% 0% 0% 0%; filter: progid:DXImageTransform.Microsoft.BasicImage(Rotation=1);
        } 
          @media print
           {
                    @page rotated {size: landscape}
           }*/
          /*@page
            {
	            size: landscape;
	            /*margin: 2cm;
            }*/
            * {
            size: landscape;
            } 
        </style>*/--%>
        <style type="text/css"> 
        .line_bottom
         {
	        BORDER-BOTTOM: #202266 1px solid;
         }

    </style>
    <style type="text/css">
        @media print { 
            page rotated {size: landscape;}
        }
    </style>
    <script type="text/javascript">
    var shell = null;
    function SetprintProperties()
    {
       /* try
        {
            shell = new ActiveXObject("WScript.Shell");
            shell.sendKeys("%fu");
            window.setTimeout("javascript:SetPaperSize();", 1200);
            window.setTimeout("javascript:setlandscape();", 2000);
        } 
        catch (ex)
        {
            alert('Please verify that your print settings have a landscape orientation and minimum margins.'+ex);
        }*/
            try
            {
                //XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP"); 
                shell = WScript.CreateObject("WScript.Shell");
               // shell.sendKeys("^p");
                shell.sendKeys("%fu");
                //shell = new ActiveXObject("WScript.Shell");
                //shell.sendKeys("^p");
                setTimeout("shell.sendKeys('%R')",1000);
                setTimeout("shell.sendKeys('%L')",1100);
            }
            catch (e)
            {
                alert ("An exception occured: " + e + "\nCode is: " + e.number + "\nDescription is: " + e.description);
            }
    }
    function SetPaperSize()
    {
        shell.sendKeys("%a{TAB}.2{TAB}0{TAB}0{TAB}0{ENTER}");
    }
    function setlandscape()
    {
        shell.sendKeys("%fp");
        window.print();
    }
    
    function printerSeetings()
    {
        alert("Ok");
        try{
        
       var PrinterSettings = dialog.PrinterSettings(); 
       }
       catch(e)
       {
       alert ("An exception occured: " + e + "\nCode is: " + e.number + "\nDescription is: " + e.description);
       }
       
    }
</script> 
</head>
<body onload="">
    <form id="form1" runat="server">
    <center>
        <table width="100%" border="0" >
            <tr>
                <td colspan="4" class="line_bottom">
                    <asp:Label runat="server" ID="formNameLabel" Text="Daily Underwriting Activity Report" Font-Bold="true" Font-Names="Arial" Font-Size="16px" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width:25%" align="left">                    
                <asp:Label runat="server" ID="Label1" Text="Recived & Re Received" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>
                </td>
                <td style="width:25%" align="left">
                <asp:Label runat="server" ID="Label2" Text="Pipe line & Pending" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                    
                </td>
                <td style="width:25%" align="left">
                <asp:Label runat="server" ID="Label3" Text="Process" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                    
                </td>
                <td style="width:25%" align="left" >
                <asp:Label runat="server" ID="Label4" Text="Analyst wise process" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                    
                </td>
            </tr>
            <tr>
                <td style="width:25%">
                 <div>                        
                    <asp:GridView ID="receivedGridView" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" OnRowDataBound="receivedGridView_RowDataBound" 
                        ShowFooter="True">
                        <FooterStyle Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="Date" HeaderText="Date"   SortExpression="Date" DataFormatString="{0:dd-MMM-yy}" />
                            <asp:BoundField DataField="RcFL" HeaderText="FL"    SortExpression="FL" />
                            <asp:BoundField DataField="RcPLSAL" HeaderText="PL-SAL"    SortExpression="PL-SAL" />
                            <asp:BoundField DataField="RcPLBIZ" HeaderText="PL-BIZ"    SortExpression="PL-BIZ" />
                            <asp:BoundField DataField="RcIPF" HeaderText="IPF"    SortExpression="IPF" />
                            <asp:BoundField DataField="RcSTFL" HeaderText="STFL"    SortExpression="STFL" />
                            <asp:BoundField DataField="ReTotal" HeaderText="TOTAL"    SortExpression="TOTAL" />
                        </Columns>
                    </asp:GridView>
                </div>
                </td>
                <td style="width:25%">
                <div>                        
                    <asp:GridView ID="pendingGridView" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" ShowFooter="True" OnRowDataBound="pendingGridView_RowDataBound">
                        <FooterStyle Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="Date" HeaderText="Date"   SortExpression="Date" DataFormatString="{0:dd-MMM-yy}" />
                            <asp:BoundField DataField="PeFL" HeaderText="FL"   SortExpression="FL" />
                            <asp:BoundField DataField="PePLSAL" HeaderText="PL-SAL"   SortExpression="PL-SAL" />
                            <asp:BoundField DataField="PePLBIZ" HeaderText="PL-BIZ"   SortExpression="PL-BIZ" />
                            <asp:BoundField DataField="PeIPF" HeaderText="IPF"   SortExpression="IPF" />
                            <asp:BoundField DataField="PeSTFL" HeaderText="STFL"   SortExpression="STFL" />
                            <asp:BoundField DataField="PeTotal" HeaderText="TOTAL"   SortExpression="TOTAL" />
                        </Columns>
                    </asp:GridView>
                </div>
                </td>
                <td style="width:25%">
                    <div>
                    <asp:GridView ID="processGridView" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" ShowFooter="True" OnRowDataBound="processGridView_RowDataBound">
                        <FooterStyle Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="Date" HeaderText="Date"   SortExpression="Date" DataFormatString="{0:dd-MMM-yy}" />
                            <asp:BoundField DataField="PoFL" HeaderText="FL"   SortExpression="FL" />
                            <asp:BoundField DataField="PoPLSAL" HeaderText="PL-SAL"   SortExpression="PL-SAL" />
                            <asp:BoundField DataField="PoPLBIZ" HeaderText="PL-BIZ"   SortExpression="PL-BIZ" />
                            <asp:BoundField DataField="PoIPF" HeaderText="IPF"   SortExpression="IPF" />
                            <asp:BoundField DataField="PoSTFL" HeaderText="STFL"   SortExpression="STFL" />
                            <asp:BoundField DataField="PoTotal" HeaderText="TOTAL"   SortExpression="TOTAL" />
                        </Columns>
                    </asp:GridView>
                </div>
                </td>
                <td style="width:25%" valign="top" align="left" rowspan="3">
                    <div>
                    <asp:GridView ID="userProcessGridView" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" ShowFooter="True" OnRowDataBound="userProcessGridView_RowDataBound"> 
                        <FooterStyle Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name"   SortExpression="Name" />
                            <asp:BoundField DataField="PoFL" HeaderText="FL"    SortExpression="FL" />
                            <asp:BoundField DataField="PoPLSAL" HeaderText="PL-SAL"    SortExpression="PL-SAL" />
                            <asp:BoundField DataField="PoPLBIZ" HeaderText="PL-BIZ"    SortExpression="PL-BIZ" />
                            <asp:BoundField DataField="PoIPF" HeaderText="IPF"    SortExpression="IPF" />
                            <asp:BoundField DataField="PoSTFL" HeaderText="STFL"    SortExpression="STFL" />
                            <asp:BoundField DataField="PoTotal" HeaderText="TOTAL"   SortExpression="TOTAL" />
                        </Columns>
                    </asp:GridView>
                </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Label ID="part1label" runat="server" Font-Bold="true" Font-Names="Arial" Font-Size="10px" Text="Daily No. of approved & Volume ( Sent to assetops)**"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                   <table width="100%" border="0" cellpadding="1" cellspacing="1">
                    <tr align="left">
                    <td style="width:17%;" >
                    &nbsp;
                    </td>
                    <td style="width:26%;" >
                    <asp:Label runat="server" ID="Label5" Text="LWD" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>
                    </td>
                    <td style="width:25%;" >
                    <asp:Label runat="server" ID="Label6" Text="MTD" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>
                    </td>
                    <td style="width:32%;" >
                    <asp:Label runat="server" ID="Label7" Text="YTD" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="4" align="left">
                    <div>                        
                    <asp:GridView ID="dailyNoofApprovedVolumeGridView" class="tableWidth" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" OnRowDataBound="dailyNoofApprovedVolumeGridView_RowDataBound" 
                        ShowFooter="True">
                        <FooterStyle HorizontalAlign="Right" Font-Bold="True" />
                        <Columns>
                            <asp:BoundField DataField="ProductName" HeaderText="Product" SortExpression="Product" >
                            </asp:BoundField>
                            <asp:BoundField DataField="LWDQuantity" HeaderText="No of Approved"  SortExpression="No of Approved" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LWDVolume" HeaderText="Approved volume"  SortExpression="Approved volume" DataFormatString="{0:N}" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MTDQuantity" HeaderText="No of Approved"  SortExpression="No of Approved" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MTDVolume" HeaderText="Approved volume"  SortExpression="Approved volume" DataFormatString="{0:N}" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YTDQuantity" HeaderText="No of Approved" SortExpression="No of Approved" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YTDVolume" HeaderText="Approved volume"  SortExpression="Approved volume" DataFormatString="{0:N}" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="4">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1"> 
                    <tr>
                    <td style="width:43%">
                    <asp:Label runat="server" ID="Label8" Text="Direct Sales" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                
                    </td>
                    <td style="width:57%" align="left">
                    <asp:Label runat="server" ID="Label9" Text="Shared Distribution" Font-Bold="true" Font-Names="Arial" Font-Size="12px" ></asp:Label>                 
                    </td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="4" align="left">
                    <div>
                    <asp:GridView ID="channelWiseGridView" class="tableWidth" runat="server" AutoGenerateColumns="False" 
                        Font-Size="8pt" Font-Names="Arial" OnRowDataBound="channelWiseGridView_RowDataBound" 
                        ShowFooter="True">
                        <FooterStyle Font-Bold="True" HorizontalAlign="Right" />
                        <Columns>
                            <asp:BoundField DataField="ProductName" HeaderText="Product" SortExpression="Product" >
                            </asp:BoundField>
                            <asp:BoundField DataField="LWDQuantity" HeaderText="No of Approved" SortExpression="No of Approved" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LWDVolume" HeaderText="Approved volume" SortExpression="Approved volume" DataFormatString="{0:N}" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MTDQuantity" HeaderText="No of Approved" SortExpression="No of Approved" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MTDVolume" HeaderText="Approved volume" SortExpression="Approved volume" DataFormatString="{0:N}" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>                          
                        </Columns>
                    </asp:GridView>
                    </div>
                    </td>
                    </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                
                </td>
            </tr>
        </table>
    </center>
    </form>
</body>
</html>
