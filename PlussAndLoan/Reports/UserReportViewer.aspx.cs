﻿using System;
using BLL;
using CrystalDecisions.CrystalReports.Engine;

namespace PlussAndLoan.Reports
{
    public partial class Reports_UserReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadReport();
            }
        }

        private void LoadReport()
        {
            ReportDocument rd = new ReportDocument();
            var path = Server.MapPath(@"UserListReport.rpt");
            rd.Load(path);
            var data = new UserManager().GetAllUsers();
            rd.SetDataSource(data);
            crUserViewer.ReportSource = rd;
        }
    }
}
