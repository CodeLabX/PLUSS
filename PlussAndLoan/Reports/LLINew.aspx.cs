﻿using System;
using System.Collections.Generic;
using System.Web;
using BLL;
using BLL.Report;
using BusinessEntities;
using BusinessEntities.Report;

namespace PlussAndLoan.Reports
{
    public partial class Reports_LLINew : System.Web.UI.Page
    {
        PL plObj;
        PLManager plManagerObj;
        Segment segmentObj;
        SegmentManager segmentManagerObj;
        Deviation deviationObj;
        DeviationManager deviationManagerObj;
        Customer customerObj;
        CustomerManager customerManagerObj;
        Incomeacesmentmethod incomeacesmentmethodObj;
        IncomeAcessmentMethodManager incomeAcessmentMethodManagerObj;
        SubSector subSectorObj;
        SubSectorManager subSectorManagerObj;
        UserManager userManagerObj = null;
        AnalystApproveManager analystApproveManagerObj = null;
        PLCheckListReportManager plCheckListReportManager =null;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            userManagerObj = new UserManager();
            plObj = new PL();
            segmentObj = new Segment();
            deviationObj = new Deviation();
            customerObj = new Customer();
            incomeacesmentmethodObj = new Incomeacesmentmethod();
            subSectorObj = new SubSector();
            plManagerObj = new PLManager();
            segmentManagerObj = new SegmentManager();
            deviationManagerObj = new DeviationManager();
            customerManagerObj = new CustomerManager();
            incomeAcessmentMethodManagerObj = new IncomeAcessmentMethodManager();
            subSectorManagerObj = new SubSectorManager();
            analystApproveManagerObj = new AnalystApproveManager();
            plCheckListReportManager = new PLCheckListReportManager();
            Int64 lLId = 0;
            if(!Page.IsPostBack)
            {

                if (Request.QueryString.Count != 0)
                {
                    lLId =Convert.ToInt64(Request.QueryString["lLId"].ToString());
                }
                if (lLId > 0)
                {
                    llidLabel.Text = lLId.ToString();
                    LoadPLInfo(lLId);
                    LoadSegmentInfo();
                    LoadDeviationInfo();
                    LoadMisc();
                }
            }
        }

        private void LoadMisc()
        {
            customerObj = customerManagerObj.GetCustomerInfo(llidLabel.Text);
            if(customerObj != null)
            {
                if(customerObj.PDC.Contains("SCB") || customerObj.PDC.Contains("Standard Chartered Bank"))
                {
                    discretionCodeLabel.Text = "";
                    noOfPDCLabel.Text = string.Empty;
                    pdcBDTLabel.Text = string.Empty;
                }
                else
                {
                    //discretionCodeLabel.Text = string.Empty;
                    discretionCodeLabel.Text = "D1";
                    noOfPDCLabel.Text = plObj.PlTenure.ToString();
                    pdcBDTLabel.Text = plObj.PlEmi.ToString("##,###.##");
                }

                nameLabel.Text = customerObj.Name;
                customersNameLabel.Text = customerObj.Name;
            }
        }

        private void LoadDeviationInfo()
        {
            List<Deviation> deviationList = new List<Deviation>();
            deviationList = deviationManagerObj.GetAllDeviationInfoByLlId(llidLabel.Text);
            if(deviationList.Count > 0)
            {
                deviationObj = deviationList[0];
                if(deviationObj.DeviDescription1ID == 0)
                {
                    deviationCodeLabel.Text = "PNL";
                }
                else
                {
                    deviationCodeLabel.Text = deviationObj.DeviCode1;
                }
            }
        }

        private void LoadSegmentInfo()
        {
            criteriaCodeLabel.Text = segmentObj.CriteriaCode;
            assetCodeLabel.Text = segmentObj.AssetCode;
        
        }

        private void LoadPLInfo(Int64 llID)
        {
            plObj = plManagerObj.SelectPLInfo(llID);
            if(plObj != null)
            {
                if (plObj.PlProductId == 1)
                {
                    reportNameLabel.Text = "LIMIT LOADING INSTRUCTION - PERSONAL LOAN";
                    InterestRateupLabel.Text = "Interest Rate:";
                }
                else if (plObj.PlProductId == 4)
                {
                    reportNameLabel.Text = "LIMIT LOADING INSTRUCTION - PERSONAL LOAN";
                    InterestRateupLabel.Text = "Interest Rate:";
                }
                else if (plObj.PlProductId == 12)
                {
                    reportNameLabel.Text = "LIMIT LOADING INSTRUCTION - ISLAMIC PERSONAL FINANCE";
                    InterestRateupLabel.Text = "Profit Rate:";
                }
                if (plObj.PlSafetyplusId == 1)
                {
                    loanAmountLabel.Text = "Tk. " + plObj.PlBancaLoanAmount.ToString("###,###");
                    inWordsLabel.Text = ConvertInWords.Convert2Words.ConvertInWords(plObj.PlBancaLoanAmount);
                }
                else
                {
                    loanAmountLabel.Text = "Tk. " + plObj.PlProposedloanamount.ToString("###,###");
                    inWordsLabel.Text = ConvertInWords.Convert2Words.ConvertInWords(plObj.PlProposedloanamount);
                }
                tenureLabel.Text = plObj.PlTenure.ToString();
                emiLabel.Text = "Tk. " + plObj.PlEmi.ToString("###,###.##");
                incomeLabel.Text = "Tk. " + Math.Min(plObj.PlNetincome, plObj.PlDeclaredincome).ToString("##,###");
                dbrLabel.Text = plObj.PlDbr.ToString("##.##") + "%";
                InterestRateLabel.Text = plObj.PlInterestrat.ToString("##.00") + "%";
                if (plObj.PlLoantypeId == 1)
                {
                    loanCategoryLabel.Text = "NL";
                }
                else if (plObj.PlLoantypeId == 2)
                {
                    loanCategoryLabel.Text = "AD";
                }
                else
                {
                    loanCategoryLabel.Text = "TP";
                }
                segmentObj = segmentManagerObj.SelectSegment(plObj.PlSegmentId);

                loanTypeLabel.Text = segmentObj.Code;
                if (plObj.PlSafetyplusId == 1)
                {
                    promissoryNoteLabel.Text = "Tk. " + plObj.PlBancaLoanAmount.ToString("###,###");
                    letterOfContinuationLabel.Text = "Tk. " + plObj.PlBancaLoanAmount.ToString("###,###");
                }
                else
                {
                    promissoryNoteLabel.Text = "Tk. " + plObj.PlProposedloanamount.ToString("###,###");
                    letterOfContinuationLabel.Text = "Tk. " + plObj.PlProposedloanamount.ToString("###,###");
                }
                double udc = plObj.PlTenure * plObj.PlEmi;
                udcForBDTLabel.Text = udc.ToString("###,###");
                string AnalystCode = "",ApproverCode="";
                if(plObj.PlIncomeacessmentmethodId != 0)
                {
                    incomeacesmentmethodObj = incomeAcessmentMethodManagerObj.PopIncomeAcesmentMethod(plObj.PlIncomeacessmentmethodId);
                    companyCodeLabel.Text = incomeacesmentmethodObj.Code + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + LoadAnalystUserInfo((int)plObj.UserId) + " " + LoadApproverUserInfo(plObj.PlLlid);
                }
                else
                {
                    companyCodeLabel.Text = string.Empty;
                }
                if(plObj.PlSubsectorId > 0)
                {
                    subSectorObj = subSectorManagerObj.SelectSubSector(Convert.ToInt64(plObj.PlSubsectorId));
                    empCategoryCodeLabel.Text = subSectorObj.Code;
                }
                else
                {
                    empCategoryCodeLabel.Text = string.Empty;
                }
                ExposureInfo exposureInfo = new ExposureInfo();
                exposureInfo = plCheckListReportManager.GetExposureInfo(Convert.ToInt32(llID));
                debtsLabel.Text = "Tk." + (exposureInfo.ExistingPLOutstanding + exposureInfo.TopUpAmountOutstanding + exposureInfo.FacalityOutstanding1 + exposureInfo.FacalityOutstanding2 + exposureInfo.FacalityOutstanding3 + exposureInfo.FacalityOutstanding4 + exposureInfo.FacalityOutstanding5 + exposureInfo.CreditCardLimitOutstanding).ToString("##,###");
                LoadStandingOrder(plObj.PlLlid);
            }
        }
        private void LoadStandingOrder(Int64 llID)
        {
            StandingOrderInfo standingOrderInfo = new StandingOrderInfo();
            standingOrderInfo = plCheckListReportManager.GetStandingOrderInfo((int)llID);
            if (standingOrderInfo.StandingOrder != "No Data")
            {
                if (standingOrderInfo.StandingOrder.Contains("SCB"))
                {
                    linkAccountLabel.Text = standingOrderInfo.ACNo.ToString(); 
                }
                else if (standingOrderInfo.StandingOrder.Contains("Standard Chartered Bank"))
                {
                    linkAccountLabel.Text = standingOrderInfo.ACNo.ToString(); 
                }
                else
                {
                    linkAccountLabel.Text = ""; 
                }
            }
        }
        private string LoadAnalystUserInfo(int llID)
        {
            string returnValue = "";
            User userObj = userManagerObj.GetUserInfo(llID);
            if (userObj != null)
            {
                //signatureLabel1.Text = userObj.Name.ToString();
                //designationLabel1.Text = userObj.Designation.ToString();
                returnValue = userObj.UserLevelCode.ToString();
            }
            return returnValue;
        }
        private string LoadApproverUserInfo(Int64 llID)
        {
            string returnValue = "";
            AnalystApprove analystApproveObj = analystApproveManagerObj.SelectAnalystApprove(llID);
            if (analystApproveObj != null)
            {
                if (analystApproveObj.UserId > 0)
                {
                    User userObj = userManagerObj.GetUserInfo(analystApproveObj.UserId);
                    if (userObj != null)
                    {
                        //signatureLabel2.Text = userObj.Name.ToString();
                        //designationLabel2.Text = userObj.Designation.ToString();
                        returnValue = userObj.UserLevelCode.ToString();
                    }
                }
            }
            return returnValue;
        }
    }
}
