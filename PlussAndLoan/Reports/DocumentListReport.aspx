﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="PlussAndLoan.Reports.Reports_DocumentListReport" Codebehind="DocumentListReport.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document list report</title>
    <style type="text/css">
        .tableWidth
        {
            border-width: 1px;
            border-style: solid;
            border-collapse: collapse;
            border-color: Black;
        }
        .tableWidth td
        {
            border-width: 1px;
            border-style: solid;
            border-color: Black;
        }
        .fontSize
        {
            font-size: 12px;
        }
        .tableHeaderFont
        {
            font-size: 13px;
            font-weight: bold;
        }
        .bankNameFontSize
        {
            font-size: 13px;
            font-weight: bold;
        }
        .sigName
        {
            font-size: 11px;
            font-weight: bold;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function PrintDeclinedBasel()
        {
            window.print();
        }
        function ColorChange()
        {
             document.getElementById('printButton').style.backgroundColor = 'red';
        }
    </script>
</head>
<body style="margin-top:0.25px; left:0.25px; right:0.10px; margin-bottom:0.25px;" class="fontSize" onload="PrintDeclinedBasel();">
    <form id="form1" runat="server">
    <center>
    <table width="98%" border="0" cellpadding="1" cellspacing="1"><tr><td>
    <div style="width: 610px;">       
        <label style="font-weight: bold; font-size: 12px; padding-left: 380px;">
            Loan Loacator Id:</label>
        <asp:Label ID="llIdLabel" runat="server" 
            Style="font-weight: bold; font-size: 11px" Font-Bold="True" Font-Size="10px" />
    </div>
    <div style="width: 610px; font-size: 20px; font-weight: bold;">
        <img alt="" src="../Images/scb_logo.jpg" style="width: 20px; height: 25px" />
        <label>
            Checklist for Credit Related Documents
        </label>
    </div>
    <div style="padding-top: 10px; width: 610px;">
        <label style="font-weight: bold; font-size: 14px">
            Customer Name :
        </label>
        <asp:Label ID="customerNameLabel" runat="server" Style="font-weight: bold; font-size: 14px" />
        <label style="font-weight: bold; font-size: 14px; padding-left: 200px;">
            A/C :
        </label>
        <asp:Label ID="accTypeLabel" runat="server" Style="font-weight: bold; font-size: 14px" />
    </div>
        <table width="98%" class="tableWidth" border="0" cellpadding="2" cellspacing="0" align="left">
            <tr class="tableHeaderFont">
                <td align="center" width="10%">
                    SL No.
                </td>
                <td width="50%" align="left">
                    Documents
                </td>
                <td width="10%">
                    &nbsp;
                </td>
                <td width="10%">
                    &nbsp;
                </td>
                <td width="10%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    1
                </td>
                <td align="left">
                    Credit Checklist
                </td>
                <td>
                    <asp:Label ID="yes1Label" runat="server" Text="Yes" Font-Bold="True"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    2
                </td>
                <td align="left">
                    BASEL II Data Checklist
                </td>
                <td>
                   <asp:Label ID="yes2Label" runat="server" Text="Yes" Font-Bold="True"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    3
                </td>
                <td align="left">
                    Income Assessment Sheet
                </td>
                <td >
                    <asp:Label ID="yes3Label" runat="server" Text="Yes" Font-Bold="True"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    4
                </td>
                <td align="left">
                    Trade Licence
                </td>
                <td>
                  <asp:Label ID="yes4Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                   <asp:Label ID="no4Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA4Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    5
                </td>
                <td align="left">
                    Memorandum and Article of Association
                </td>
                <td>
                    <asp:Label ID="yes5Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no5Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA5Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    6
                </td>
                <td align="left">
                    Summary of Share Capital (Form X)
                </td>
                <td>
                    <asp:Label ID="yes6Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no6Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA6Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    7
                </td>
                <td align="left">
                    Certicficate of Registration of Firm
                </td>
                <td>
                    <asp:Label ID="yes7Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no7Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA7Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    8
                </td>
                <td align="left">
                    Partnership Deed (Certified by Registrar of Firm) or Form I
                </td>
                <td>
                    <asp:Label ID="yes8Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no8Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA8Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    9
                </td>
                <td align="left">
                    Rental Agreement / Rental Receipt
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="NA9Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    10
                </td>
                <td align="left">
                    Municipal Tax Receipt
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="NA10Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    11
                </td>
                <td align="left">
                    Bank Statement of the following a/cs
                </td>
                <td>
                    <asp:Label ID="yes11Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="bankNameFontSize" align="left">
                    &nbsp;&nbsp;&nbsp;&nbsp; i.&nbsp;&nbsp;
                    <asp:Label ID="bank1Label" runat="server"></asp:Label>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="bankNameFontSize" align="left">
                    &nbsp;&nbsp;&nbsp;&nbsp;ii.&nbsp;&nbsp;
                    <asp:Label ID="bank2Label" runat="server"></asp:Label>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="bankNameFontSize" align="left">
                    &nbsp;&nbsp;&nbsp;iii.&nbsp;&nbsp;
                    <asp:Label ID="bank3Label" runat="server"></asp:Label>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="bankNameFontSize" align="left">
                    &nbsp;&nbsp;&nbsp; iv.&nbsp;&nbsp;
                    <asp:Label ID="bank4Label" runat="server"></asp:Label>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="bankNameFontSize" align="left">
                    &nbsp;&nbsp;&nbsp;&nbsp; v.&nbsp;&nbsp;
                    <asp:Label ID="bank5Label" runat="server"></asp:Label>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="bankNameFontSize" align="left">
                    &nbsp;&nbsp;&nbsp; vi.&nbsp;&nbsp;
                    <asp:Label ID="bank6Label" runat="server"></asp:Label>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    12
                </td>
                <td align="left">
                    Credit Report From Other Bank
                </td>
                <td>
                    <asp:Label ID="yes12Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no12Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA12Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    13
                </td>
                <td align="left">
                    Bank Statement Verification Report From Other Bank
                </td>
                <td>
                    <asp:Label ID="yes13Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no13Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA13Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    14
                </td>
                <td align="left">
                    Copy of Offer Letter of Other Bank Facility
                </td>
                <td>
                    <asp:Label ID="yes14Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no14Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA14Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    15
                </td>
                <td align="left">
                    No Liability Certificate from other Bank/Employer
                </td>
                <td>
                    <asp:Label ID="yes15Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no15Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA15Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    16
                </td>
                <td align="left">
                    Verification Sheet
                </td>
                <td>
                    <asp:Label ID="yes16Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="NA16Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    17
                </td>
                <td align="left">
                    Letter of Introduction
                </td>
                <td>
                    <asp:Label ID="yes17Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="NA17Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    18
                </td>
                <td align="left">
                    Payslip
                </td>
                <td>
                    <asp:Label ID="yes18Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="NA18Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    19
                </td>
                <td align="left">
                    Copy of Cash Voucher
                </td>
                <td>
                    <asp:Label ID="yes19Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="NA19Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    20
                </td>
                <td align="left">
                    Copy of Tax Return/TIN Certificate
                </td>
                <td>
                    <asp:Label ID="yes20Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no20Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA20Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    21
                </td>
                <td align="left">
                    Credit Card Repayment History
                </td>
                <td>
                    <asp:Label ID="yes21Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no21Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    22
                </td>
                <td align="left">
                    Most Important Document
                </td>
                <td>
                    <asp:Label ID="yes22Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    23
                </td>
                <td align="left">
                    De-duplication Check Sheets(s)
                </td>
                <td>
                    <asp:Label ID="yes23Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    24
                </td>
                <td align="left">
                    Master A/C Listing
                </td>
                <td>
                    <asp:Label ID="yes24Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    25
                </td>
                <td align="left">
                    RLS - Customer Loan Details
                </td>
                <td>
                    <asp:Label ID="yes25Label" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="no25Label" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="NA25Label" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    26
                </td>
                <td align="left">
                    Others :
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                    i.&nbsp; National ID Card
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                    ii.&nbsp; Utility Bill Copy
                </td>
                <td>
                    <asp:Label ID="Label20" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label37" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label59" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="left">
                    iii.&nbsp; Other Photo ID
                </td>
                <td>
                    <asp:Label ID="Label21" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                   <asp:Label ID="Label38" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label60" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="Label22" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label39" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label61" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="Label23" runat="server" Text="Yes"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label40" runat="server" Text="No"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label62" runat="server" Text="N/A"></asp:Label>
                </td>
            </tr>
        </table>
    </td></tr></table>
    <table width="95%" border="0" cellpadding="1" cellspacing="1">
    <tr>
    <td align="left">
        <asp:Label ID="noteLabel" runat="server" Text="Note : Assets Ops to ensure that all documents marked in Yes column are attached with application before loan drawdown." />
    </td>
    </tr>
    <tr>
    <td align="left">
        <br />
        <br />
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" CssClass="sigName" Text="-----------------------------------" />
        <br />
        <label>
            &nbsp;&nbsp;&nbsp;&nbsp; Credit Signature</label><br />
        <asp:Label ID="creditSigNameLabel" runat="server" CssClass="sigName" /><br />
        <asp:Label ID="designationLabel" runat="server" />
    </td>
    </tr>
    </table>
    </center>
    </form>
</body>
</html>
