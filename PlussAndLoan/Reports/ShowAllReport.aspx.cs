using System;
using System.Net;
using System.Text;

namespace PlussAndLoan.Reports
{
    public partial class Reports_ShowAllReport : System.Web.UI.Page
    {
        private Int64 lLId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count != 0)
                {
                    lLId = Convert.ToInt64(Request.QueryString["lLId"].ToString());
                }
                //plCheckListFrame.Attributes["src"] = "PLCheckListReport.aspx?lLId=" + lLId + "";
                //lliNewFrame.Attributes["src"] = "LLINew.aspx?lLId=" + lLId + "";
                LoadAllHTML();
            }
        }

        private void LoadAllHTML()
        {
            StringBuilder sb = new StringBuilder();

            UTF8Encoding objUTF8 = new UTF8Encoding();
            string[] urls = new string[5];

            urls[0] = @"http://localhost/SCBPLUSS/Reports/PLCheckListReport.aspx?lLId=" + lLId + "";
            urls[1] = @"http://localhost/SCBPLUSS/Reports/LLINew.aspx?lLId=" + lLId + "";
            urls[2] = @"http://localhost/SCBPLUSS/Reports/BASEL2DataCheckList.aspx?lLId=" + lLId + "";
            urls[3] = @"http://localhost/SCBPLUSS/Reports/ApprovalInfoReport.aspx?lLId=" + lLId + "";
            urls[4] = @"http://localhost/SCBPLUSS/Reports/DocumentListReport.aspx?lLId=" + lLId + "";

            for (int i = 0; i < urls.Length; i++)
            {
                byte[] bContent;

                WebClient wcContent = new WebClient();
                string url = urls[i];

                bContent = wcContent.DownloadData(url);

                sb = sb.AppendLine(objUTF8.GetString(bContent));
                if (i==0)
                {
                    sb = sb.AppendLine("<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />");
                }
                if (i == 1)
                {
                    sb = sb.AppendLine("<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />");
                }
                if (i == 2)
                {
                    sb = sb.AppendLine("<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />");
                }
                if (i == 3)
                {
                    sb = sb.AppendLine("<br /><br /><br /><br /><br /><br /><br />");
                }

            }

            dvContent.InnerHtml = sb.ToString();
        }
    }
}
