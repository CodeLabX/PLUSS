﻿using System;
using BLL;
using BLL.Report;
using BusinessEntities;
using BusinessEntities.Report;
using ConvertInWords;

namespace PlussAndLoan.Reports
{
    public partial class Reports_ApprovalInfoReport : System.Web.UI.Page
    {
        private PLCheckListReportManager plCheckListReportManagerObj;
        private ApprovalInformationManager approvalInformationManagerObj;
        public PLManager pLManagerObj =null;
        public BILMUEManager bilMUEManagerObj=null;
        public UserManager userManagerObj = null;
        public AnalystApproveManager analystApproveManagerObj = null;
        private CustomerManager customerManagerObj = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            plCheckListReportManagerObj = new PLCheckListReportManager();
            approvalInformationManagerObj = new ApprovalInformationManager();
            analystApproveManagerObj = new AnalystApproveManager();
            pLManagerObj = new PLManager();
            bilMUEManagerObj = new BILMUEManager();
            userManagerObj = new UserManager();
            customerManagerObj = new CustomerManager();
            string llIdObj = "";
            if (Request.QueryString.Count != 0)
            {
                llIdObj = Request.QueryString["llId"].ToString();
                llIdLabel.Text = llIdObj;
                LoadDeviationReason(llIdObj);

                ApprovalInformation approvalInformationObj = approvalInformationManagerObj.GetApprovalReportInformation(llIdObj);
                if (approvalInformationObj != null)
                {
                    if (DateFormat.IsDate(approvalInformationObj.AppraisalDate))
                    {
                        appraisalDateLabel.Text = approvalInformationObj.AppraisalDate.ToString("dd-MM-yyyy");
                    }
                    else
                    {
                        appraisalDateLabel.Text = "##-##-#####";
                    }
                    customerLabel.Text = approvalInformationObj.Customer;
                    aaprovedAmntLabel.Text = approvalInformationObj.ApprovedAmt.ConvertInWords().ToString().ToUpper();
                    //incomeLabel.Text = "Tk." + approvalInformationObj.Income.ToString();
                    dbrLabel.Text = approvalInformationObj.DBR.ToString() + "%";
                    mueLabel.Text = approvalInformationObj.MUE.ToString() + "X";
                    segmentLabel.Text = approvalInformationObj.Segment.ToString();

                    approvedAmtLabel.Text = "Tk." + approvalInformationObj.ApprovedAmt.ToString("##,###");;

                    PL pLObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llIdObj.ToString()));
                    if (pLObj != null)
                    {
                        if (pLObj.PlProductId == 1)
                        {
                            reportNameLabel.Text = "PERSONAL LOAN";
                        }
                        else if (pLObj.PlProductId == 4)
                        {
                            reportNameLabel.Text = "PERSONAL LOAN";
                        }
                        else if (pLObj.PlProductId == 12)
                        {
                            reportNameLabel.Text = "ISLAMIC PERSONAL FINANCE";
                        }
                        Customer customerObj = customerManagerObj.GetCustomerInfo(llIdObj.ToString());
                        if (customerObj != null)
                        {
                            if (customerObj.MasterNo != null)
                            {
                                if (customerObj.MasterNo.Length > 9)
                                {
                                    masterACNoLabel.Text = "MASTER:" + customerObj.MasterNo.Substring(2, 7).ToString();
                                }
                                else
                                {
                                    masterACNoLabel.Text = "MASTER:" + customerObj.MasterNo.ToString();
                                }
                            }
                            else
                            {
                                masterACNoLabel.Text = "MASTER:To be opened";
                            }
                        }

                        incomeLabel.Text = "Tk. " + Math.Min(pLObj.PlNetincome, pLObj.PlDeclaredincome).ToString("##,###");
                        double dbr = Convert.ToDouble(pLObj.PlDbr) - Convert.ToDouble(pLObj.PlDbrmultiplier);
                        switch (pLObj.PlSafetyplusId.ToString())
                        {
                            case "0":
                                bancaWithEMILabel.Text = "";
                                bancaWithLoanLabel.Text = "";
                                break;
                            case "1":
                                double IR = pLObj.PlInterestrat / 100;
                                bancaWithEMILabel.Text = "Banca installment TK " + Math.Ceiling(PMT(IR / 12, pLObj.PlTenure, pLObj.PlBanca)) + " incorporated with EMI";
                                bancaWithLoanLabel.Text = "Banca premium TK " + pLObj.PlBanca.ToString() + " incorporated with Loan";
                                break;
                            case "2":
                                double Banca = Math.Ceiling((((pLObj.PlProposedloanamount) * 0.57 * pLObj.PlTenure) / 12) / 100);
                                bancaWithEMILabel.Text = "Banca Premium additional TK " + Banca.ToString() + " incorporated with EMI";
                                break;
                        }
                        LoadUserInfo((int)pLObj.PlLlid);
                        //User userObj = userManagerObj.GetUserInfo((int)pLObj.UserId);
                        //if (userObj != null)
                        //{
                        //    appraisedByLabel.Text = userObj.Name.ToString();
                        //    designationLabel.Text = userObj.Designation.ToString();
                        //}
                        //LoadApprovedInfo(Convert.ToInt32(pLObj.PlLlid.ToString()));
                    }
                    //levelLabel.Text = approvalInformationObj.Level.ToString();
                    irLabel.Text = approvalInformationObj.IR.ToString("##.00") + "%";
                    emiLabel.Text = "Tk." + approvalInformationObj.EMI.ToString("##,###");
                    udcAmntLabel.Text = "Tk." + approvalInformationObj.UDCAmount.ToString("##,###");
                    tenureLabel.Text = approvalInformationObj.Tenure.ToString();
                    BILMUE bilMueObj = bilMUEManagerObj.GetBILMUEData(llIdObj.ToString());
                    if (bilMueObj != null)
                    {
                        if (bilMueObj.NetNar > 0)
                        {
                            exposureAmtLabel.Text = bilMueObj.NetNar.ToString();
                        }
                    }
                    else
                    {
                        exposureAmtLabel.Text = approvalInformationObj.SCBExposure.ToString();
                    }
                    crdtCrdLabel.Text = approvalInformationObj.CrdtCrdLimit.ToString();
                    if (approvalInformationObj.LoanTypeId == 3)
                    {
                        topupLabel.Visible = true;
                        topupAmntLabel.Text = (Convert.ToDouble(approvalInformationObj.ApprovedAmt)-Convert.ToDouble(approvalInformationObj.TopUpAmount)).ToString();
                    }
                    else
                    {
                        topupLabel.Visible = false;
                        topupAmntLabel.Text = "";
                    }
                    modeLabel.Text = approvalInformationObj.Mode;
                    if (approvalInformationObj.Mode == "SI")
                    {
                        PDCDATEupLabel.Text = "SI DATE";
                    }
                    else
                    {
                        PDCDATEupLabel.Text = "PDC DATE";
                    }
                    bankNameLabel.Text = approvalInformationObj.BankName;
                    accNoLabel.Text = approvalInformationObj.AccountNo;
                    if (approvalInformationObj.PdcDate == 0)
                    {
                        pdcDateLabel.Text = "***";
                    }
                    else if (approvalInformationObj.PdcDate == 1)
                    {
                        pdcDateLabel.Text = approvalInformationObj.PdcDate.ToString() + "ST";
                    }
                    else if (approvalInformationObj.PdcDate == 2)
                    {
                        pdcDateLabel.Text = approvalInformationObj.PdcDate.ToString() + "ND";
                    }
                    else if (approvalInformationObj.PdcDate == 3)
                    {
                        pdcDateLabel.Text = approvalInformationObj.PdcDate.ToString() + "RD";
                    }
                    else
                    {
                        pdcDateLabel.Text = approvalInformationObj.PdcDate.ToString() + "TH";
                    }
                
                    preCndtionLabel1.Text = approvalInformationObj.PreCondition1;
                    preCndtionLabel2.Text = approvalInformationObj.PreCondition2;
                    preCndtionLabel3.Text = approvalInformationObj.PreCondition3;
                    preCndtionLabel4.Text = approvalInformationObj.PreCondition4;
                    preCndtionLabel5.Text = approvalInformationObj.PreCondition5;
                }
                if (pddLevelLabel1.Text == "" || pddLevelLabel1.Text == null)
                {
                    levelLabel.Text = "1";
                }
                else
                {
                    levelLabel.Text = pddLevelLabel1.Text;
                }
            }
        }
        private double PMT(double IR,double NP,double PV)
        {
            double vPMT = (PV * IR) / (1 - Math.Pow(1 + IR, -NP));
            return Math.Round(vPMT,6);
        }
        private void LoadDeviationReason(string llIdObj)
        {
            DeviationInfo deviationInfoObj = plCheckListReportManagerObj.GetDeviationInfo(Convert.ToInt32(llIdObj));
            if (deviationInfoObj == null)
            {
                return;
            }
            if (deviationInfoObj.Description1.ToString() != "N/A")
            {
                reasonLabel1.Text = deviationInfoObj.Description1;
                pddLevelLabel1.Text = deviationInfoObj.Level1;
            }

            if (deviationInfoObj.Description2.ToString() != "N/A")
            {
                reasonLabel2.Text = deviationInfoObj.Description2;
                pddLevelLabel2.Text = deviationInfoObj.Level2;
            }
            if (deviationInfoObj.Description3.ToString() != "N/A")
            {
                reasonLabel3.Text = deviationInfoObj.Description3;
                pddLevelLabel3.Text = deviationInfoObj.Level3;
            }
            if (deviationInfoObj.Description4.ToString() != "N/A")
            {
                reasonLabel4.Text = deviationInfoObj.Description4;
                pddLevelLabel4.Text = deviationInfoObj.Level4;
            }
        }
        private void LoadUserInfo(int llID)
        {
            UserInfo userInfo = new UserInfo();
            userInfo = plCheckListReportManagerObj.GetUserInfo(llID);
            if (userInfo != null)
            {
                appraisedByLabel.Text = userInfo.UserName;
                designationLabel.Text = userInfo.Designation;
            }
            LoadApprovedInfo(llID);
        }
        private void LoadApprovedInfo(Int32 lLId)
        {
            AnalystApprove analystApproveObj = analystApproveManagerObj.SelectAnalystApprove(lLId);
            if (analystApproveObj != null)
            {
                if (analystApproveObj.UserId > 0)
                {
                    User userObj = userManagerObj.GetUserInfo(analystApproveObj.UserId);
                    if (userObj != null)
                    {
                        approvedNameLabel.Text = userObj.Name.ToString();
                        approvedDesignationLabel.Text = userObj.Designation.ToString();
                        // approvedDalCodeLabel.Text = userObj.UserLevelCode.ToString();
                        //analystApproveObj.EntryDate.ToString("dd-MM-yyyyy");
                    }
                }
            }
        }
    }
}
