﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using BLL;
using BLL.Report;
using BusinessEntities;
using BusinessEntities.Report;

namespace PlussAndLoan.Reports
{
    public partial class Reports_PLCheckListReportUI : System.Web.UI.Page
    {
        PLCheckListReportManager plCheckListReportManager;
        int age = 0;
        string loanType = "";
        PLManager pLManagerObj = null;
        PL pLObj = null;
        public UserManager userManagerObj = null;
        public AnalystApproveManager analystApproveManagerObj = null;
        double tempTenor = 0;
        string checkRepament = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetAllowResponseInBrowserHistory(false);
            plCheckListReportManager = new PLCheckListReportManager();
            pLManagerObj = new PLManager();
            pLObj = new PL();
            userManagerObj = new UserManager();
            analystApproveManagerObj = new AnalystApproveManager();
            int lLId = 0;
            if (Request.QueryString.Count != 0)
            {
                lLId = Convert.ToInt32(Request.QueryString["lLId"].ToString());
            }
            if (lLId > 0)
            {
                llIdLabel.Text = lLId.ToString();
                LoadInitialInfo(lLId);
                LoadSegmentPart(lLId);
                LoadExposurePart(lLId);
                LoadStandingOrder(lLId);
                LoadDeviation(lLId);
                LoadNetIncome(lLId);
                LoadUserInfo(lLId);
                LoadCreditRemarks(lLId);
                LoadApprovedInfo(lLId);
            }
        }

        private void LoadCreditRemarks(int llID)
        {
            StrengthWeaknessInfo strengthWeeknessInfo = new StrengthWeaknessInfo();
            strengthWeeknessInfo = plCheckListReportManager.GetStrengthWeaknessInfo(llID);

            strengthLabel1.Text = strengthWeeknessInfo.StrengthAcRelationshipWith;
            strengthLabel2.Text = strengthWeeknessInfo.StrengthResidentialStatus;
            strengthLabel3.Text = strengthWeeknessInfo.StrengthCreditCardRepayment;
            strengthLabel4.Text = strengthWeeknessInfo.StrengthPreviousRepayment1;
            strengthLabel5.Text = strengthWeeknessInfo.StrengthPreviousRepayment2;
            strengthLabel6.Text = strengthWeeknessInfo.StrengthOthers;

            weaknessLabel1.Text = strengthWeeknessInfo.WeaknessRepaymentNotSatisfactory;
            weaknessLabel2.Text = strengthWeeknessInfo.WeaknessCashSalaried;
            weaknessLabel3.Text = strengthWeeknessInfo.WeaknessFrequentChequeReturn;
            weaknessLabel4.Text = "";
            weaknessLabel5.Text = "";
            weaknessLabel6.Text = strengthWeeknessInfo.WeaknessOthers;
        }

        private void LoadUserInfo(int llID)
        {
            UserInfo userInfo = new UserInfo();
            userInfo = plCheckListReportManager.GetUserInfo(llID);
            appraisedLabel.Text = userInfo.UserName;
            designationLabel.Text = userInfo.Designation;
        }
        private void LoadApprovedInfo(Int32 lLId)
        {
            AnalystApprove analystApproveObj = analystApproveManagerObj.SelectAnalystApprove(lLId);
            if (analystApproveObj != null)
            {
                if (analystApproveObj.UserId > 0)
                {
                    User userObj = userManagerObj.GetUserInfo(analystApproveObj.UserId);
                    if (userObj != null)
                    {
                        approvedNameLabel.Text = userObj.Name.ToString();
                        approvedDesignationLabel.Text = userObj.Designation.ToString();
                        approvedDalCodeLabel.Text = userObj.UserLevelCode.ToString();
                    }
                }
            }
        }

        private void LoadStandingOrder(int llID)
        {
            StandingOrderInfo standingOrderInfo = new StandingOrderInfo();
            standingOrderInfo = plCheckListReportManager.GetStandingOrderInfo(llID);
            if (standingOrderInfo.StandingOrder != "No Data")
            {
                if (standingOrderInfo.StandingOrder.Contains("SCB") || standingOrderInfo.StandingOrder.Contains("Standard Chartered Bank"))
                {
                    standingOrderOrPDCLabel.Text = "Standing Order";
                }
                else
                {
                    standingOrderOrPDCLabel.Text = "PDC";
                }
                standingOrderLabel.Text = standingOrderInfo.StandingOrder;
                if (standingOrderInfo.ACNo != "")
                {
                    acLowerPartLabel.Text = standingOrderInfo.ACNo;
                }
                else
                {
                    acLowerPartLabel.Text = "to be opened";
                }
                if (standingOrderInfo.RpmnthDate == "0")
                {
                    rpmntLowerPartLabel.Text = "***";
                    checkRepament = "0";
                }
                else
                {
                    if (standingOrderInfo.RpmnthDate == "1")
                    {
                        rpmntLowerPartLabel.Text = standingOrderInfo.RpmnthDate + "ST";
                    }
                    else if (standingOrderInfo.RpmnthDate == "2")
                    {
                        rpmntLowerPartLabel.Text = standingOrderInfo.RpmnthDate + "ND";
                    }
                    else if (standingOrderInfo.RpmnthDate == "3")
                    {
                        rpmntLowerPartLabel.Text = standingOrderInfo.RpmnthDate + "RD";
                    }
                    else
                    {
                        rpmntLowerPartLabel.Text = standingOrderInfo.RpmnthDate + "TH";
                    }
                    checkRepament = standingOrderInfo.RpmnthDate;
                }
            }
        }

        private void LoadNetIncome(int llID)
        {
            NetIncomeInfo netIncomeInfo = new NetIncomeInfo();
            netIncomeInfo = plCheckListReportManager.GetNetIncomeInfo(llID);

            if (netIncomeInfo.NetIncome > 0)
            {
                netIncomeLowerPartLabel.Text = "Tk. " + netIncomeInfo.NetIncome.ToString("#,###");
            }
            else
            {
                netIncomeLowerPartLabel.Text = string.Empty;
            }
            if (netIncomeInfo.DBR > 0)
            {
                DBRLowerPartLabel.Text = netIncomeInfo.DBR.ToString() + "%";
            }
            else
            {
                DBRLowerPartLabel.Text = string.Empty;
            }
            if (netIncomeInfo.MUE > 0)
            {
                MUELowerPartLabel.Text = netIncomeInfo.MUE + "X";
            }
            else
            {
                MUELowerPartLabel.Text = string.Empty;
            }
            if (netIncomeInfo.LoanAmount > 0)
            {
                LoanAmountLowerPartLabel.Text = "Tk. " + netIncomeInfo.LoanAmount.ToString("##,###");
            }
            else
            {
                LoanAmountLowerPartLabel.Text = string.Empty;
            }
            if (netIncomeInfo.EMI > 0)
            {
                EMILowerPartLabel.Text = "Tk. " + netIncomeInfo.EMI.ToString("##,###");
            }
            else
            {
                EMILowerPartLabel.Text = string.Empty;
            }
            TenorLowerPartLabel.Text = netIncomeInfo.Tenor.ToString();
            if (netIncomeInfo.IR > 0)
            {
                IRLowerPartLabel.Text = netIncomeInfo.IR.ToString("##.00") + "%";
            }
            else
            {
                IRLowerPartLabel.Text = string.Empty;
            }
        }

        private void LoadDeviation(int llID)
        {
            DeviationInfo deviationInfo = new DeviationInfo();
            deviationInfo = plCheckListReportManager.GetDeviationInfo(llID);
            if (deviationInfo == null)
            {
                return;
            }
            else
            {
                if (deviationInfo.Description1.ToString() != "N/A")
                {
                    pddDeviationLabel1.Text = deviationInfo.Description1;
                    deviationLevelLabel1.Text = deviationInfo.Level1;
                    deviationCodeLabel1.Text = deviationInfo.Code1;
                }
                if (deviationInfo.Description2.ToString() != "N/A")
                {
                    pddDeviationLabel2.Text = deviationInfo.Description2;
                    deviationLevelLabel2.Text = deviationInfo.Level2;
                    deviationCodeLabel2.Text = deviationInfo.Code2;
                }
                if (deviationInfo.Description3.ToString() != "N/A")
                {
                    pddDeviationLabel3.Text = deviationInfo.Description3;
                    deviationLevelLabel3.Text = deviationInfo.Level3;
                    deviationCodeLabel3.Text = deviationInfo.Code3;
                }
                if (deviationInfo.Description4.ToString() != "N/A")
                {
                    pddDeviationLabel4.Text = deviationInfo.Description4;
                    deviationLevelLabel4.Text = deviationInfo.Level4;
                    deviationCodeLabel4.Text = deviationInfo.Code4;
                }
                StringBuilder remarksTable = new StringBuilder();
                List<string> remarksObjList = new List<string>();
                Deviation deviation = null;// new Deviation();
                List<Deviation> deviationObjList = new List<Deviation>();
                DeviationManager deviationManagerObj = new DeviationManager();
                deviationObjList = deviationManagerObj.GetAllDeviationInfoByLlId(llID.ToString());
                if (deviationObjList.Count > 0)
                {
                    deviation = new Deviation();
                    deviation = deviationObjList[0];
                    if (deviation.DeviRemarkscondition1ID.Length > 0)
                    {
                        string[] rowData = deviation.DeviRemarkscondition1ID.Split('*');
                        for (int i = 0; i < rowData.Length; i++)
                        {
                            string[] idData = rowData[i].Split('#');
                            if (idData[0].Length > 0)
                            {
                                remarksObjList.Add(idData[1].ToString());
                            }
                        }
                    }
                }
                if (remarksObjList.Count > 0)
                {
                    remarksTable.Append("<table class='tableWidth' style='width:95%; font-size: 10px;'>");
                    foreach (string remarks in remarksObjList)
                    {
                        remarksTable.Append("<tr>");
                        remarksTable.Append("<td align='left'>" + remarks + "</td>");
                        remarksTable.Append("</tr>");
                    }
                    remarksTable.Append("<tr>");
                    remarksTable.Append("<td align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Residence address & phone numbers [all] to be updated in eBBS as per CPV.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PG / Reference details to be updated in RLS</td>");
                    remarksTable.Append("</tr>");
                    if (checkRepament == "0")
                    {
                        remarksTable.Append("<tr>");
                        remarksTable.Append("<td align='left'>*** : Rpmnt date to be set as per customer's request between 1st, 8th, 16th, 25th day for PDC & 1st - 28th day for SI. 1st EMI date should be within 30 days from disbursement date</td>");
                        remarksTable.Append("</tr>");
                    }
                    else
                    {
                        remarksTable.Append("<tr>");
                        remarksTable.Append("<td align='center'>***</td>");
                        remarksTable.Append("</tr>");
                    }

                    remarksTable.Append("</table>");
                }
                lastRemarksDiv.InnerHtml = remarksTable.ToString();
            }

        }

        private void LoadExposurePart(int llID)
        {
            if (loanType == "3")
            {
                thisTotalPLLabel.Text = "This PL";
                topUpAmountLabel.Text = "Top up Amount";
            }
            else
            {
                thisTotalPLLabel.Text = "Total PL";
                topUpAmountLabel.Text = "This Loan";
            }

            ExposureInfo exposureInfo = new ExposureInfo();
            exposureInfo = plCheckListReportManager.GetExposureInfo(llID);
            if (exposureInfo.ExistingPLOutstanding > 0)
            {
                existingPLOutstandingLabel.Text = "Tk." + exposureInfo.ExistingPLOutstanding.ToString("#");
            }
            else
            {
                existingPLOutstandingLabel.Text = string.Empty;
            }
            if (exposureInfo.ExistingPLOriginalLimit > 0)
            {
                existingPLOriginalLimitLabel.Text = "Tk." + exposureInfo.ExistingPLOriginalLimit.ToString("#");
            }
            else
            {
                existingPLOriginalLimitLabel.Text = string.Empty;
            }
            topUpAmountOutstandingLabel.Text = "Tk." + exposureInfo.TopUpAmountOutstanding.ToString("#");
            if (exposureInfo.TopUpAmountRepayment > 0)
            {
                topUpAmountRepaymentLabel.Text = exposureInfo.TopUpAmountRepayment.ToString("#");
            }
            else
            {
                topUpAmountRepaymentLabel.Text = string.Empty;
            }
            if (exposureInfo.TopUpAmountEMIFactor > 0)
            {
                topUpAmountEMIFactorLabel.Text = exposureInfo.TopUpAmountEMIFactor.ToString("##.##");
            }
            else
            {
                topUpAmountEMIFactorLabel.Text = string.Empty;
            }
            if (exposureInfo.ThisPLOutstanding > 0)
            {
                thisTotalOutstandingLabel.Text = "Tk." + exposureInfo.ThisPLOutstanding.ToString("#");
            }
            else
            {
                thisTotalOutstandingLabel.Text = string.Empty;
            }
            if (exposureInfo.ThisPLRepayment > 0)
            {
                thisTotalRepaymentLabel.Text = exposureInfo.ThisPLRepayment.ToString("#");
            }
            else
            {
                thisTotalRepaymentLabel.Text = string.Empty;
            }
            if (exposureInfo.CreditCardLimitOutstanding > 0)
            {
                creditCardLimitOutstandingLabel1.Text = "Tk. " + exposureInfo.CreditCardLimitOutstanding.ToString("#,###");
            }
            else
            {
                creditCardLimitOutstandingLabel1.Text = string.Empty;
            }
            if (exposureInfo.CreditCardLimitRepayment > 0)
            {
                creditCardLimitRepaymentLabel.Text = "Tk. " + exposureInfo.CreditCardLimitRepayment.ToString("#,###");
            }
            else
            {
                creditCardLimitRepaymentLabel.Text = string.Empty;
            }
            if (exposureInfo.FacalityName1 == "N/A")
            {
                facalityDisburseDateLabel1.Text = string.Empty;
                facalityExpiryDateLabel1.Text = string.Empty;
                facalityLabel1.Text = string.Empty;
                facalityOriginalLimitLabel1.Text = string.Empty;
                facalityOutstandingLabel1.Text = string.Empty;
                facalityRepaymentLabel1.Text = string.Empty;
            }
            else
            {
                facalityLabel1.Text = exposureInfo.FacalityName1;
                facalityOutstandingLabel1.Text = exposureInfo.FacalityOutstanding1.ToString("#");
                facalityOriginalLimitLabel1.Text = exposureInfo.FacalityOriginalLimit1.ToString("#");
                facalityRepaymentLabel1.Text = exposureInfo.FacalityRepayment1.ToString("#");
                if (exposureInfo.FacalityDisburseDate1.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityDisburseDateLabel1.Text = string.Empty;
                }
                else
                {
                    facalityDisburseDateLabel1.Text = exposureInfo.FacalityDisburseDate1.ToString("dd-MM-yyyy");
                }
                if (exposureInfo.FacalityExpiryDate1.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityExpiryDateLabel1.Text = string.Empty;
                }
                else
                {
                    facalityExpiryDateLabel1.Text = exposureInfo.FacalityExpiryDate1.ToString("dd-MM-yyyy");
                }
            }
            if (exposureInfo.FacalityName2 == "N/A")
            {
                facalityDisburseDateLabel2.Text = string.Empty;
                facalityExpiryDateLabel2.Text = string.Empty;
                facalityLabel2.Text = string.Empty;
                facalityOriginalLimitLabel2.Text = string.Empty;
                facalityOutstandingLabel2.Text = string.Empty;
                facalityRepaymentLabel2.Text = string.Empty;
            }
            else
            {
                facalityLabel2.Text = exposureInfo.FacalityName2;
                facalityOutstandingLabel2.Text = exposureInfo.FacalityOutstanding2.ToString("#");
                facalityOriginalLimitLabel2.Text = exposureInfo.FacalityOriginalLimit2.ToString("#");
                facalityRepaymentLabel2.Text = exposureInfo.FacalityRepayment2.ToString("#");
                if (exposureInfo.FacalityDisburseDate2.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityDisburseDateLabel2.Text = string.Empty;
                }
                else
                {
                    facalityDisburseDateLabel2.Text = exposureInfo.FacalityDisburseDate2.ToString("dd-MM-yyyy");
                }
                if (exposureInfo.FacalityExpiryDate2.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityExpiryDateLabel2.Text = string.Empty;
                }
                else
                {
                    facalityExpiryDateLabel2.Text = exposureInfo.FacalityExpiryDate2.ToString("dd-MM-yyyy");
                }
            }
            if (exposureInfo.FacalityName3 == "N/A")
            {
                facalityDisburseDateLabel3.Text = string.Empty;
                facalityExpiryDateLabel3.Text = string.Empty;
                facalityLabel3.Text = string.Empty;
                facalityOriginalLimitLabel3.Text = string.Empty;
                facalityOutstandingLabel3.Text = string.Empty;
                facalityRepaymentLabel3.Text = string.Empty;
            }
            else
            {
                facalityLabel3.Text = exposureInfo.FacalityName3;
                facalityOutstandingLabel3.Text = exposureInfo.FacalityOutstanding3.ToString("#");
                facalityOriginalLimitLabel3.Text = exposureInfo.FacalityOriginalLimit3.ToString("#");
                facalityRepaymentLabel3.Text = exposureInfo.FacalityRepayment3.ToString("#");
                if (exposureInfo.FacalityDisburseDate3.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityDisburseDateLabel3.Text = string.Empty;
                }
                else
                {
                    facalityDisburseDateLabel3.Text = exposureInfo.FacalityDisburseDate3.ToString("dd-MM-yyyy");
                }
                if (exposureInfo.FacalityExpiryDate3.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityExpiryDateLabel3.Text = string.Empty;
                }
                else
                {
                    facalityExpiryDateLabel3.Text = exposureInfo.FacalityExpiryDate3.ToString("dd-MM-yyyy");
                }
            }
            if (exposureInfo.FacalityName4 == "N/A")
            {
                facalityDisburseDateLabel4.Text = string.Empty;
                facalityExpiryDateLabel4.Text = string.Empty;
                facalityLabel4.Text = string.Empty;
                facalityOriginalLimitLabel4.Text = string.Empty;
                facalityOutstandingLabel4.Text = string.Empty;
                facalityRepaymentLabel4.Text = string.Empty;
            }
            else
            {
                facalityLabel4.Text = exposureInfo.FacalityName4;
                facalityOutstandingLabel4.Text = exposureInfo.FacalityOutstanding4.ToString("#");
                facalityOriginalLimitLabel4.Text = exposureInfo.FacalityOriginalLimit4.ToString("#");
                facalityRepaymentLabel4.Text = exposureInfo.FacalityRepayment4.ToString("#");
                if (exposureInfo.FacalityDisburseDate4.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityDisburseDateLabel4.Text = string.Empty;
                }
                else
                {
                    facalityDisburseDateLabel4.Text = exposureInfo.FacalityDisburseDate4.ToString("dd-MM-yyyy");
                }
                if (exposureInfo.FacalityExpiryDate4.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityExpiryDateLabel4.Text = string.Empty;
                }
                else
                {
                    facalityExpiryDateLabel4.Text = exposureInfo.FacalityExpiryDate4.ToString("dd-MM-yyyy");
                }
            }
            if (exposureInfo.FacalityName5 == "N/A")
            {
                facalityDisburseDateLabel5.Text = string.Empty;
                facalityExpiryDateLabel5.Text = string.Empty;
                facalityLabel5.Text = string.Empty;
                facalityOriginalLimitLabel5.Text = string.Empty;
                facalityOutstandingLabel5.Text = string.Empty;
                facalityRepaymentLabel5.Text = string.Empty;
            }
            else
            {
                facalityLabel5.Text = exposureInfo.FacalityName5;
                facalityOutstandingLabel5.Text = exposureInfo.FacalityOutstanding5.ToString("#");
                facalityOriginalLimitLabel5.Text = exposureInfo.FacalityOriginalLimit5.ToString("#");
                facalityRepaymentLabel5.Text = exposureInfo.FacalityRepayment5.ToString("#");
                if (exposureInfo.FacalityDisburseDate5.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityDisburseDateLabel5.Text = string.Empty;
                }
                else
                {
                    facalityDisburseDateLabel5.Text = exposureInfo.FacalityDisburseDate5.ToString("dd-MM-yyyy");
                }
                if (exposureInfo.FacalityExpiryDate5.ToString("dd-MM-yyyy") == "01-01-0001")
                {
                    facalityExpiryDateLabel5.Text = string.Empty;
                }
                else
                {
                    facalityExpiryDateLabel5.Text = exposureInfo.FacalityExpiryDate5.ToString("dd-MM-yyyy");
                }
            }

            totalOutstandingLabel.Text = "Tk." + (exposureInfo.ExistingPLOutstanding + exposureInfo.TopUpAmountOutstanding + exposureInfo.FacalityOutstanding1 + exposureInfo.FacalityOutstanding2 + exposureInfo.FacalityOutstanding3 + exposureInfo.FacalityOutstanding4 + exposureInfo.FacalityOutstanding5 + exposureInfo.CreditCardLimitOutstanding).ToString("##,###");
            //totalRepaymentLabel.Text = "Tk." + (exposureInfo.TopUpAmountOutstanding + exposureInfo.FacalityRepayment1 + exposureInfo.FacalityRepayment2 + exposureInfo.FacalityRepayment3 + exposureInfo.FacalityRepayment4 + exposureInfo.FacalityRepayment5 + exposureInfo.CreditCardLimitRepayment).ToString("##,###");
            totalRepaymentLabel.Text = "Tk." + (exposureInfo.ThisPLRepayment + exposureInfo.FacalityRepayment1 + exposureInfo.FacalityRepayment2 + exposureInfo.FacalityRepayment3 + exposureInfo.FacalityRepayment4 + exposureInfo.FacalityRepayment5 + exposureInfo.CreditCardLimitRepayment).ToString("##,###");
            //thisTotalRepaymentLabel//exposureInfo.ThisPLRepayment
        }

        private void LoadTopUpPart(string loanType)
        {
            if (loanType == "1")
            {
                accountTypeHeaderLabel.Text = "NEW";
                topUpLabel1.Text = string.Empty;
                topUpLabel2.Text = string.Empty;
                topUpLabel3.Text = string.Empty;
                topUpLabel4.Text = string.Empty;
                topUpLabel5.Text = string.Empty;
                topUpLabel6.Text = string.Empty;
                topUpNoLabel1.Text = string.Empty;
                topUpNoLabel2.Text = string.Empty;
                topUpNoLabel3.Text = string.Empty;
                topUpNoLabel4.Text = string.Empty;
                topUpNoLabel5.Text = string.Empty;
                topUpNoLabel6.Text = string.Empty;
                topUpYesLabel1.Text = string.Empty;
                topUpYesLabel2.Text = string.Empty;
                topUpYesLabel3.Text = string.Empty;
                topUpYesLabel4.Text = string.Empty;
                topUpYesLabel5.Text = string.Empty;
                topUpYesLabel6.Text = string.Empty;
            }
            else if (loanType == "2")
            {
                accountTypeHeaderLabel.Text = "ADDITIONAL";
                topUpLabel1.Text = "No 30+ DPD in last 6 months";
                topUpLabel2.Text = "Duration of Loan / Previous TOP UP > 6 months";
                topUpLabel3.Text = "Existing Loan Regular";
                topUpLabel4.Text = string.Empty;
                topUpLabel5.Text = string.Empty;
                topUpLabel6.Text = string.Empty;
                topUpNoLabel1.Text = "NO";
                topUpNoLabel2.Text = "NO";
                topUpNoLabel3.Text = "NO";
                topUpNoLabel4.Text = string.Empty;
                topUpNoLabel5.Text = string.Empty;
                topUpNoLabel6.Text = string.Empty;
                topUpYesLabel1.Text = "YES";
                topUpYesLabel2.Text = "YES";
                topUpYesLabel3.Text = "YES";
                topUpYesLabel4.Text = string.Empty;
                topUpYesLabel5.Text = string.Empty;
                topUpYesLabel6.Text = string.Empty;
            }
            else if (loanType == "3")
            {
                accountTypeHeaderLabel.Text = "TOPUP";
                topUpLabel1.Text = "a/c was never 60+DPD";
                topUpLabel2.Text = "a/c was never 30+DPD in last 6 months";
                topUpLabel3.Text = "a/c was never 30+DPD in last 9 months > Once";
                topUpLabel4.Text = "Duration of Loan / Previous Top Up > 9 Months";
                topUpLabel5.Text = "Previous Top Up not more than Twice";
                topUpLabel6.Text = "Minimum TOP UP amount = Tk. 50,000";
                topUpNoLabel1.Text = "NO";
                topUpNoLabel2.Text = "NO";
                topUpNoLabel3.Text = "NO";
                topUpNoLabel4.Text = "NO";
                topUpNoLabel5.Text = "NO";
                topUpNoLabel6.Text = "NO";
                topUpYesLabel1.Text = "YES";
                topUpYesLabel2.Text = "YES";
                topUpYesLabel3.Text = "YES";
                topUpYesLabel4.Text = "YES";
                topUpYesLabel5.Text = "YES";
                topUpYesLabel6.Text = "YES";
            }
            else if (loanType == "4")
            {
                accountTypeHeaderLabel.Text = "TAKE OVER";
                topUpLabel1.Text = string.Empty;
                topUpLabel2.Text = string.Empty;
                topUpLabel3.Text = string.Empty;
                topUpLabel4.Text = string.Empty;
                topUpLabel5.Text = string.Empty;
                topUpLabel6.Text = string.Empty;
                topUpNoLabel1.Text = string.Empty;
                topUpNoLabel2.Text = string.Empty;
                topUpNoLabel3.Text = string.Empty;
                topUpNoLabel4.Text = string.Empty;
                topUpNoLabel5.Text = string.Empty;
                topUpNoLabel6.Text = string.Empty;
                topUpYesLabel1.Text = string.Empty;
                topUpYesLabel2.Text = string.Empty;
                topUpYesLabel3.Text = string.Empty;
                topUpYesLabel4.Text = string.Empty;
                topUpYesLabel5.Text = string.Empty;
                topUpYesLabel6.Text = string.Empty;
            }
        }

        private void LoadSegmentPart(int llID)
        {
            SegmentInfo segmentInfo = new SegmentInfo();
            segmentInfo = plCheckListReportManager.GetSegmentInfo(llID);
            pLObj = pLManagerObj.SelectPLInfo(Convert.ToInt64(llID));
            if (pLObj != null)
            {
                if (segmentInfo.SegmentCode != "No Data")
                {
                    segmentCodeLabel.Text = segmentInfo.SegmentCode;
                    segmentDescriptionLabel.Text = segmentInfo.SegmentDescription;

                    maxLoanLabel.Text = "BDT " + segmentInfo.MaxLoan.ToString();
                    minLoanLabel.Text = "Tk. " + segmentInfo.MinLoan.ToString("#,#");
                    minAgeLabel.Text = segmentInfo.MinAge.ToString() + " yrs";
                    if (segmentInfo.MaxAgeRemarks == "N/A")
                    {
                        segmentInfo.MaxAgeRemarks = "";
                    }
                    maxAgeLabel.Text = segmentInfo.MaxAge.ToString() + segmentInfo.MaxAgeRemarks.ToString();
                    physicalVerificationLabel.Text = segmentInfo.PhysicalVerification;
                    telephoneYesNoLabel.Text = segmentInfo.Telephone;
                    if (pLObj.PlSegmentId == 12 || pLObj.PlSegmentId == 13)
                    {
                        experienceLabel.Text = segmentInfo.Experience + " " + pLObj.PlDoctorCategory.ToString();
                    }
                    else
                    {
                        experienceLabel.Text = segmentInfo.Experience;
                    }
                    acRelationshipLabel.Text = segmentInfo.ACRelationship;
                    minimumIncomeLabel.Text = segmentInfo.MinIncome.ToString("#,#") + " - agg (10K+8K)";

                    dbRationLabel1.Text = segmentInfo.DBRatio.ToString("#.##") + "%";
                    double value = 0;
                    string DBRvalue = "";
                    value = Math.Min(Convert.ToDouble(pLObj.PlNetincome), Convert.ToDouble(pLObj.PlDeclaredincome));
                    if (value < 29999)
                    {
                        DBRvalue = segmentInfo.DBR29.ToString();
                        incomeRangeLabel.Text = "14K-30K";
                    }
                    else if (value < 50000)
                    {
                        DBRvalue = segmentInfo.DBR50.ToString();
                        incomeRangeLabel.Text = "30K-50K";
                    }
                    else if (value < 99999)
                    {
                        DBRvalue = segmentInfo.DBR99.ToString();
                        incomeRangeLabel.Text = "50K-100k";
                    }
                    else
                    {
                        DBRvalue = segmentInfo.DBR1K.ToString();
                        incomeRangeLabel.Text = "100k + ";
                    }
                    DBRLabel.Text = DBRvalue.ToString() + "%"; //pLObj.PlDbrmultiplier.ToString() + "%";   //

                    tenurLabel.Text = pLObj.PlTenure.ToString();//segmentInfo.Tenur.ToString();
                    if (pLObj.PlSafetyplusId == 0)
                    {
                        bancaStatusLabel.Text = "No";
                    }
                    else if (pLObj.PlSafetyplusId == 1)
                    {
                        bancaStatusLabel.Text = "Yes";
                    }
                    else if (pLObj.PlSafetyplusId == 2)
                    {
                        bancaStatusLabel.Text = "SELF FIN";
                    }
                    switch (pLObj.PlSafetyplusId.ToString())
                    {
                        case "0":
                            includedBancaLabel.Text = "";
                            break;
                        case "1":
                            double IR = pLObj.PlInterestrat / 100;
                            includedBancaLabel.Text = "TK " + PMT(IR / 12, pLObj.PlTenure, pLObj.PlBanca) + " included";
                            break;
                        case "2":
                            double Banca = Math.Ceiling((((pLObj.PlProposedloanamount) * 0.57 * pLObj.PlTenure) / 12) / 100);
                            includedBancaLabel.Text = "TK " + Banca.ToString() + " included";
                            break;
                    }
                    if (pLObj.PlProposedloanamount < segmentInfo.MinLoan)
                    {
                        tempTenor = 24;
                    }
                    else
                    {
                        if (pLObj.PlProposedloanamount < 100000)
                        {
                            tempTenor = 24;
                            //loanAmountRangeLabel.Text = "<100K";
                            loanAmountRangeLabel.Text = "50K-100K";
                        }
                        else if (pLObj.PlProposedloanamount < 250000)
                        {
                            tempTenor = 48;//36
                            //loanAmountRangeLabel.Text = "100K-250K";
                            loanAmountRangeLabel.Text = "100K-249K";
                        }
                            //else if (pLObj.PlProposedloanamount >= 250000)
                            //{
                            //    tempTenor = 48;
                            //    loanAmountRangeLabel.Text = "250K-500K";
                            //}
                            //else if (pLObj.PlProposedloanamount < 500000)
                            //{
                            //    tempTenor = 48;
                            //    loanAmountRangeLabel.Text = "250K-500K";
                            //}
                        else
                        {
                            tempTenor = 60;
                            //loanAmountRangeLabel.Text = "500K +";
                            loanAmountRangeLabel.Text = "250K-1000K";
                        }
                    }
                    TNRLabel.Text = tempTenor.ToString();
                    //tenurLabel2.Text = segmentInfo.TNR.ToString();
                    mueLabel.Text = "MUE " + segmentInfo.MUE.ToString() + "X";
                    mueLabel2.Text = pLObj.PlMuemultiplier.ToString() + "X";
                    guaranteeYesNoLabel.Text = segmentInfo.GuaranteeRef;
                    if (pLObj.PlOtheremi > 0)
                    {
                        otherBankLiabilitiesAmountLabel.Text = pLObj.PlOtheremi.ToString();
                    }
                    else
                    {
                        otherBankLiabilitiesAmountLabel.Text = "Not Declared";
                    }

                    //Fill up Yes/No Labels
                    if (segmentInfo.MaxLoan > 1000000)
                    {
                        maxLoanYesNoLabel.Text = "No";
                    }
                    else
                    {
                        maxLoanYesNoLabel.Text = "Yes";
                    }

                    if (segmentInfo.ProposedLoanAmount > segmentInfo.MinLoan)
                    {
                        minLoanYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        minLoanYesNoLabel.Text = "No";
                    }

                    if (age > segmentInfo.MinAge)
                    {
                        minageYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        minageYesNoLabel.Text = "No";
                    }

                    int tenorAge = (pLObj.PlTenure / 12);
                    if ((age + tenorAge) <= segmentInfo.MaxAge)
                    {
                        maxAgeYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        maxAgeYesNoLabel.Text = "No";
                    }
                    if (segmentInfo.Experience.Length > 0)
                    {
                        experienceYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        experienceYesNoLabel.Text = "No";
                    }
                    if (segmentInfo.ACRelationship.Length > 0)
                    {
                        acRelationshipYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        acRelationshipYesNoLabel.Text = "No";
                    }

                    if (segmentInfo.MinIncome > 1)
                    {
                        minimumIncomeYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        minimumIncomeYesNoLabel.Text = "No";
                    }
                    if (pLObj.PlDbr <= segmentInfo.DBR)
                    {
                        dbRationYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        dbRationYesNoLabel.Text = "No";
                    }
                    if (pLObj.PlTenure <= tempTenor)
                    {
                        tenurYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        tenurYesNoLabel.Text = "No";
                    }
                    if (pLObj.PlMue <= pLObj.PlMuemultiplier)
                    {
                        mueYesNoLabel.Text = "Yes";
                    }
                    else
                    {
                        mueYesNoLabel.Text = "No";
                    }
                }
            }
        }
        private double PMT(double IR, double NP, double PV)
        {
            double vPMT = (PV * IR) / (1 - Math.Pow(1 + IR, -NP));
            return Math.Round(vPMT, 6);
        }
        public string AgeCalculation(DateTime myDOB, DateTime FutureDate)
        {
            int years = 0;
            int months = 0;
            int days = 0;

            DateTime tmpMyDOB = new DateTime(myDOB.Year, myDOB.Month, 1);

            DateTime tmpFutureDate = new DateTime(FutureDate.Year, FutureDate.Month, 1);

            while (tmpMyDOB.AddYears(years).AddMonths(months) < tmpFutureDate)
            {
                months++;
                if (months > 12)
                {
                    years++;
                    months = months - 12;
                }
            }

            if (FutureDate.Day >= myDOB.Day)
            {
                days = days + FutureDate.Day - myDOB.Day;
            }
            else
            {
                months--;
                if (months < 0)
                {
                    years--;
                    months = months + 12;
                }
                days +=
                    DateTime.DaysInMonth(
                        FutureDate.AddMonths(-1).Year, FutureDate.AddMonths(-1).Month
                        ) + FutureDate.Day - myDOB.Day;

            }

            if (DateTime.IsLeapYear(myDOB.Year) && myDOB.Month == 2 && myDOB.Day == 29)
            {
                if (FutureDate >= new DateTime(FutureDate.Year, 3, 1))
                    days++;
            }
            age = years;
            string strAge = years + " Years " + months + " Months";
            return strAge;
        }
        private void LoadInitialInfo(int llID)
        {
            InitialInfo initialInfo = new InitialInfo();
            initialInfo = plCheckListReportManager.GetInitialInfo(llID);
            loanType = initialInfo.LoanType;
            LoadTopUpPart(loanType);

            ageLabel.Text = AgeCalculation(initialInfo.DOB, DateTime.Now);

            dateLabel.Text = DateTime.Now.ToString("dd-MMM-yyyy");

            customerNameLabel.Text = initialInfo.CustomerName;
            if (initialInfo.MasterNo != null)
            {
                if (initialInfo.MasterNo.Length > 9)
                {
                    masterNoLabel.Text = initialInfo.MasterNo.Substring(2, 7).ToString();
                }
            }
        }
    }
}
