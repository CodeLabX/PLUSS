﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace AzUtilities
{
    public static class ListDataTableManager
    {
        public static DataTable GetListToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static string[] GetColumnNamesFromClass<T>(string className)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

            List<string> str = new List<string>();

            int i = 0;
            foreach (PropertyDescriptor prop in properties)
            {
                if (!prop.PropertyType.FullName.Contains("Business") && !prop.PropertyType.FullName.Contains("System.Collection"))
                {
                    str.Add(prop.Name);
                }
                i++;
            }

            return str.ToArray();
        }

    }
}
