﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace AzUtilities
{
    public class LogFile
    {
        private static string GetPath()
        {
            string dir = ConfigurationManager.AppSettings["LogFilePath"].ToString();

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            return Path.Combine(dir, DateTime.Now.ToString("dd-MM-yyyy") + ".log");
        }


        public static void Write(string str)
        {
            string file = GetPath();
            using (StreamWriter writer = new StreamWriter(file, true))
            {
                try
                {
                    writer.Write(str);
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }            
        }

        public static void WriteLine(string str)
        {
            string file = GetPath();
            using (StreamWriter writer = new StreamWriter(file, true))
            {
                try
                {
                    writer.Write(DateTime.Now.ToString() + "\t" + str + "\r\n");
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }                
        }

        public static void WriteLog(Exception ex)
        {
            string file = GetPath();
            using (StreamWriter writer = new StreamWriter(file, true))
            {
                try
                {
                    StackTrace trace = new StackTrace(ex);

                    writer.Write(DateTime.Now.ToString() + "\t" + "Method Name :" + trace.GetFrame(0).GetMethod().Name + "\r\n");
                    writer.Write("\t\t" + "Message : " + ex.Message + "\r\n");
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            }
        }



        public static void DebugWrite(string str)
        {
            string file = GetPath();
            using (StreamWriter writer = new StreamWriter(file, true))
            {
                try
                {
                    writer.Write(str);
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        //Debug Mode
        public static void DebugWriteLine(string str)
        {
            string file = GetPath();
            using (StreamWriter writer = new StreamWriter(file, true))
            {
                try
                {
                    writer.Write(DateTime.Now.ToString() + "\t" + str + "\r\n");
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
