﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace AzUtilities
{
    public class ConvertionUtilities
    {
        public static DateTime GetDateTime(string provided)
        {
            DateTime dateTime = DateTime.MinValue;

            if (string.IsNullOrEmpty(provided))
                return dateTime;

            try
            {
                dateTime = Convert.ToDateTime(provided);
            }
            catch (Exception ex)
            {
                try
                {
                    var cultureInfo = new CultureInfo("ru-RU");
                    dateTime = DateTime.Parse(provided, cultureInfo);
                }
                catch (Exception exp)
                {
                    LogFile.WriteLine("Secodary DateTime String :: " + provided);
                }
                LogFile.WriteLine("Direct DateTime String :: " + provided);
            }

            return dateTime;
        }

        public static int GetInteger(string provided)
        {
            int integer = 0;

            if (string.IsNullOrEmpty(provided))
                return integer;

            try
            {
                integer = Convert.ToInt32(provided);
            }
            catch (Exception ex)
            {
                try //if direct conversion fails then parse to decimal and then to int
                {
                    decimal d = decimal.Parse(provided, NumberStyles.Float);
                    integer = decimal.ToInt32(d);
                }
                catch (Exception exp)
                {
                    LogFile.WriteLine("Provided DateTime String :: " + provided);
                    LogFile.WriteLog(exp);
                }

                LogFile.WriteLine("Direct Conversion Failed. Provided DateTime String :: " + provided);
                LogFile.WriteLog(ex);
            }

            return integer;
        }


        public static string ConvertDataTableToCsvString(DataTable dtData)
        {
            if (dtData == null)
            {
                return String.Empty;
            }
            StringBuilder data = new StringBuilder();

            //Taking the column names.
            for (int column = 0; column < dtData.Columns.Count; column++)
            {
                //Making sure that end of the line, shoould not have comma delimiter.
                if (column == dtData.Columns.Count - 1)
                    data.Append(dtData.Columns[column].ColumnName.ToString().Replace(",", ";"));
                else
                    data.Append(dtData.Columns[column].ColumnName.ToString().Replace(",", ";") + ',');
            }

            data.Append(Environment.NewLine);//New line after appending columns.

            for (int row = 0; row < dtData.Rows.Count; row++)
            {
                for (int column = 0; column < dtData.Columns.Count; column++)
                {
                    ////Making sure that end of the line, shoould not have comma delimiter.
                    if (column == dtData.Columns.Count - 1)
                        data.Append(dtData.Rows[row][column].ToString().Replace(",", ";"));
                    else
                        data.Append('"' + dtData.Rows[row][column].ToString().Replace(",", ";") + '"' + ',');
                }

                //Making sure that end of the file, should not have a new line.
                if (row != dtData.Rows.Count - 1)
                    data.Append(Environment.NewLine);
            }
            return data.ToString();
        }

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);
                
                // Convert the byte array to hexadecimal string prior to .NET 5
                StringBuilder sb = new System.Text.StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static DataTable TransposeDataTable(DataTable dt)
        {
            DataTable dtNew = new DataTable();

            //adding columns    
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtNew.Columns.Add(i.ToString());
            }



            //Changing Column Captions: 
            dtNew.Columns[0].ColumnName = " ";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //For dateTime columns use like below
                dtNew.Columns[i + 1].ColumnName = dt.Rows[i].ItemArray[0].ToString();
                //Else just assign the ItermArry[0] to the columnName prooperty
            }

            //Adding Row Data
            for (int k = 1; k < dt.Columns.Count; k++)
            {
                DataRow r = dtNew.NewRow();
                r[0] = dt.Columns[k].ToString();
                for (int j = 1; j <= dt.Rows.Count; j++)
                    r[j] = dt.Rows[j - 1][k];
                dtNew.Rows.Add(r);
            }

            return dtNew;
        }
    }
}
