using System.Collections.Generic;

namespace AzolutionDbUtility
{
	public class GridResult<T>
	{
		public GridEntity<T> Data(List<T> list, int totalCount)
		{
			GridEntity<T> gridEntity = new GridEntity<T>();
			gridEntity.Items = list ?? new List<T>();
			gridEntity.TotalCount = totalCount;
			return gridEntity;
		}
	}
}
