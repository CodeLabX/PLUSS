using System;
using System.Collections.Generic;
using System.Data;

namespace AzolutionDbUtility
{
	public class Data<T>
	{
		public static List<T> DataSource(string query)
		{
			AzDbConnection commonConnection = new AzDbConnection();
			try
			{
				DataTable dataTable = commonConnection.GetDataTable(query);
				return (List<T>)ListConversion.ConvertTo<T>(dataTable);
			}
			catch (Exception ex)
			{
			}
			finally
			{
				commonConnection.Close();
			}
			return null;
		}

		public static List<T> GenericDataSource(string query)
		{
			AzDbConnection commonConnection = new AzDbConnection();
			try
			{
				DataTable dataTable = commonConnection.GetDataTable(query);
				return (List<T>)GenericListGenerator.GetList<T>(dataTable);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				commonConnection.Close();
			}
		}
	}
}
