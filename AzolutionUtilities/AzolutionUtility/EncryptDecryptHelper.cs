using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace AzolutionDbUtility
{
	public class EncryptDecryptHelper
	{
		public static string Encrypt(string plainText)
		{
			string strPassword = "#*!@";
			string s = "123!@*";
			string strHashName = "MD5";
			int iterations = 1;
			string s2 = "@1B2c3D4e5F6g7H8";
			int num = 256;
			byte[] bytes = Encoding.ASCII.GetBytes(s2);
			byte[] bytes2 = Encoding.ASCII.GetBytes(s);
			byte[] bytes3 = Encoding.UTF8.GetBytes(plainText);
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(strPassword, bytes2, strHashName, iterations);
			byte[] bytes4 = passwordDeriveBytes.GetBytes(num / 8);
			RijndaelManaged rijndaelManaged = new RijndaelManaged();
			rijndaelManaged.Mode = CipherMode.CBC;
			ICryptoTransform transform = rijndaelManaged.CreateEncryptor(bytes4, bytes);
			MemoryStream memoryStream = new MemoryStream();
			CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
			cryptoStream.Write(bytes3, 0, bytes3.Length);
			cryptoStream.FlushFinalBlock();
			byte[] inArray = memoryStream.ToArray();
			memoryStream.Close();
			cryptoStream.Close();
			return Convert.ToBase64String(inArray);
		}

		public static string Decrypt(string cipherText)
		{
			string strPassword = "#*!@";
			string s = "123!@*";
			string strHashName = "MD5";
			int iterations = 1;
			string s2 = "@1B2c3D4e5F6g7H8";
			int num = 256;
			byte[] bytes = Encoding.ASCII.GetBytes(s2);
			byte[] bytes2 = Encoding.ASCII.GetBytes(s);
			byte[] array = Convert.FromBase64String(cipherText);
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(strPassword, bytes2, strHashName, iterations);
			byte[] bytes3 = passwordDeriveBytes.GetBytes(num / 8);
			RijndaelManaged rijndaelManaged = new RijndaelManaged();
			rijndaelManaged.Mode = CipherMode.CBC;
			ICryptoTransform transform = rijndaelManaged.CreateDecryptor(bytes3, bytes);
			MemoryStream memoryStream = new MemoryStream(array);
			CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read);
			byte[] array2 = new byte[array.Length];
			int count = cryptoStream.Read(array2, 0, array2.Length);
			memoryStream.Close();
			cryptoStream.Close();
			return Encoding.UTF8.GetString(array2, 0, count);
		}

		public static bool ValidateLoginPassword(string inputPassword, string dbPassword, bool encryption)
		{
			string text = "";
			text = ((!encryption) ? inputPassword : Encrypt(inputPassword));
			if (text == dbPassword)
			{
				return true;
			}
			return false;
		}
	}
}
