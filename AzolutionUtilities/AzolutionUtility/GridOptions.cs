using System.Collections.Generic;

namespace AzolutionDbUtility
{
	public class GridOptions
	{
		public int skip { get; set; }

		public int take { get; set; }

		public int page { get; set; }

		public int pageSize { get; set; }

		public List<AzFilter.GridSort> sort { get; set; }

		public AzFilter.GridFilters filter { get; set; }
	}
}
