using Bat.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace AzolutionDbUtility
{
	public class AzDbConnection
	{
		private string ConnectionType = "";

		private string connectionString = "";

		private DbCommand dbCommand = null;

		private DbConnection dbConnection = null;

		private IsolationLevel rIsolationLevel;

		private DbTransaction dbTransaction = null;

		private DbDataAdapter dbAdapter = null;

		public DatabaseType DatabaseType { get; set; }

		public bool IsConfigureDb { get; set; }

		public string ConnectionString => connectionString;

		public string serverAddress { get; private set; }

		public DatabaseProvider Provider { get; set; }

		private bool isIsolation { get; set; }
		private ConstringManager constringManager = new ConstringManager();


		public AzDbConnection()
		{
			//IL_0126: Unknown result type (might be due to invalid IL or missing references)
			//IL_0130: Expected O, but got Unknown
			if (ConfigurationManager.AppSettings != null)
			{
				string text = "SQL";
				if (!string.IsNullOrEmpty(text))
				{
					ConnectionType = text.Trim();
				}
			}
			if (ConnectionType != null && ConnectionType == "SQL")
			{
				connectionString = constringManager.DecryptConstring("conString");
				DatabaseType = DatabaseType.SQL;
				dbConnection = new SqlConnection(connectionString);
				IsConfigureDb = true;
			}
			else
			{
				IsConfigureDb = false;
			}
			serverAddress = dbConnection.DataSource;
		}

		public AzDbConnection(string connectionName)
		{
			if (ConfigurationManager.AppSettings != null)
			{
				string text = "SQL";
				if (!string.IsNullOrEmpty(text))
				{
					ConnectionType = text.Trim();
				}
			}
			if (ConnectionType != null && ConnectionType == "SQL")
			{
				ConnectionStringSettingsCollection connectionStrings = ConfigurationManager.ConnectionStrings;
				connectionString = connectionStrings[connectionName].ConnectionString;
				DatabaseType = DatabaseType.SQL;
				dbConnection = new SqlConnection(connectionString);
				IsConfigureDb = true;
			}
			else if (ConnectionType != null && ConnectionType == "MySql")
			{
				ConnectionStringSettingsCollection connectionStrings = ConfigurationManager.ConnectionStrings;
				connectionString = connectionStrings[connectionName].ConnectionString;
				DatabaseType = DatabaseType.MySql;
				dbConnection = new OdbcConnection(connectionString);
				IsConfigureDb = true;
			}
			else if (ConnectionType != null && ConnectionType == "Oracle")
			{
				connectionString = connectionName;
				ConnectionStringSettingsCollection connectionStrings = ConfigurationManager.ConnectionStrings;
				DatabaseType = DatabaseType.Oracle;
				dbConnection = new OleDbConnection(connectionString);
				IsConfigureDb = true;
			}
			else
			{
				IsConfigureDb = false;
			}
		}

		public AzDbConnection(IsolationLevel readCommitted)
		{
			rIsolationLevel = readCommitted;
			isIsolation = true;
			if (ConfigurationManager.AppSettings != null)
			{
				string text = "SQL";
				if (!string.IsNullOrEmpty(text))
				{
					ConnectionType = text.Trim();
				}
			}
			if (ConnectionType != null && ConnectionType == "SQL")
			{
				ConnectionStringSettingsCollection connectionStrings = ConfigurationManager.ConnectionStrings;
				connectionString = "conString";
				DatabaseType = DatabaseType.SQL;
				dbConnection = new SqlConnection(connectionStrings[connectionString].ConnectionString);
				IsConfigureDb = true;
			}
			else
			{
				IsConfigureDb = false;
			}
		}

		public void ExecuteNonQuery(string sql)
		{
			//IL_005a: Unknown result type (might be due to invalid IL or missing references)
			//IL_0064: Expected O, but got Unknown
			if (dbConnection.State == ConnectionState.Closed)
			{
				dbConnection.Open();
			}
			switch (DatabaseType)
			{
				case DatabaseType.SQL:
					dbCommand = new SqlCommand();
					break;
			}
			dbCommand.Connection = dbConnection;
			dbCommand.CommandText = sql;
			dbCommand.CommandType = CommandType.Text;
			dbCommand.ExecuteNonQuery();
		}

		public void RollBack()
		{
			dbTransaction.Rollback();
		}

		public void Close()
		{
			dbConnection.Close();
		}

		public void Dispose()
		{
			dbCommand.Dispose();
			dbConnection.Close();
			dbConnection.Dispose();
		}

		public void BeginTransaction()
		{
			if (dbConnection.State == ConnectionState.Closed)
			{
				dbConnection.Open();
			}
			switch (DatabaseType)
			{
			case DatabaseType.SQL:
				dbCommand = new SqlCommand();
				break;
			
			}
			dbTransaction = dbConnection.BeginTransaction();
			dbCommand.Connection = dbConnection;
			dbCommand.Transaction = dbTransaction;
		}

		public void BeginTransaction(IsolationLevel readCommitted)
		{
			if (dbConnection.State == ConnectionState.Closed)
			{
				dbConnection.Open();
			}
			switch (DatabaseType)
			{
			case DatabaseType.SQL:
				dbCommand = new SqlCommand();
				break;
			}
			dbTransaction = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted);
			dbCommand.Connection = dbConnection;
			dbCommand.Transaction = dbTransaction;
		}

		public void CommitTransaction()
		{
			if (dbConnection.State == ConnectionState.Open)
			{
				dbTransaction.Commit();
			}
		}

		public DataTable GetDataTable(string sql)
		{
			//IL_004c: Unknown result type (might be due to invalid IL or missing references)
			//IL_0056: Expected O, but got Unknown
			switch (DatabaseType)
			{
			case DatabaseType.SQL:
				dbAdapter = new SqlDataAdapter(sql, connectionString);
				break;
			}
			DataTable dataTable = new DataTable();
			dbAdapter.Fill(dataTable);
			return dataTable;
		}

		public int GetScaler(string sql)
		{
			//IL_005c: Unknown result type (might be due to invalid IL or missing references)
			//IL_0066: Expected O, but got Unknown
			int result = 0;
			if (dbConnection.State == ConnectionState.Closed)
			{
				dbConnection.Open();
			}
			switch (DatabaseType)
			{
			case DatabaseType.SQL:
				dbCommand = new SqlCommand();
				break;
			}
			dbCommand.Connection = dbConnection;
			dbCommand.CommandText = sql;
			dbCommand.CommandType = CommandType.Text;
			dbCommand.ExecuteScalar();
			return result;
		}

		public List<T> Data<T>(string query)
		{
			DataTable dataTable = GetDataTable(query);
			return (List<T>)ListConversion.ConvertTo<T>(dataTable);
		}

		public int ExecuteAfterReturnId(string query, string columnName)
		{
			switch (DatabaseType)
			{
			case DatabaseType.SQL:
				return ExcuteSqlAfterReturn(query);
			case DatabaseType.Oracle:
				return 0;
			default:
				return 0;
			}
		}

		private int ExcuteSqlAfterReturn(string query)
		{
			int num = 0;
			try
			{
				dbCommand = dbConnection.CreateCommand();
				dbCommand.Transaction = dbTransaction;
				dbCommand.CommandText = query + " select @@identity outId ";
				object obj = dbCommand.ExecuteScalar();
				if (obj != null)
				{
					return Convert.ToInt32(obj);
				}
				throw new Exception("Return value is null");
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private void ExecuteDbCommand(string sqlTxt)
		{
			dbCommand.CommandText = sqlTxt;
			dbCommand.Connection = dbConnection;
			if (dbConnection.State == ConnectionState.Closed)
			{
				dbConnection.Open();
			}
			dbCommand.ExecuteNonQuery();
		}

		public bool InsertTable(DataTable dtTable)
		{
			bool result = false;
			try
			{
				if (ConnectionType == "SQL")
				{
					using (SqlConnection sqlConnection = new SqlConnection(dbConnection.ConnectionString))
					{
						sqlConnection.Open();
						SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);
						sqlBulkCopy.DestinationTableName = dtTable.TableName;
						sqlBulkCopy.WriteToServer(dtTable);
						result = true;
					}
				}
			}
			catch (SqlException)
			{
				result = false;
			}
			return result;
		}

		public bool InsertTable(DataTable dtTable, string destinationTableName)
		{
			bool result = false;
			try
			{
				if (ConnectionType == "SQL")
				{
					using (SqlConnection sqlConnection = new SqlConnection(dbConnection.ConnectionString))
					{
						sqlConnection.Open();
						SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(sqlConnection, SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);
						sqlBulkCopy.DestinationTableName = destinationTableName;
						sqlBulkCopy.WriteToServer(dtTable);
						sqlConnection.Close();
						result = true;
					}
				}
			}
			catch (Exception)
			{
				result = false;
			}
			return result;
		}
	}
}
