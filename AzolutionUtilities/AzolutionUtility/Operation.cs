namespace AzolutionDbUtility
{
	public enum Operation
	{
		Success,
		Failed,
		Error,
		Exists,
		DataNotFound
	}
}
