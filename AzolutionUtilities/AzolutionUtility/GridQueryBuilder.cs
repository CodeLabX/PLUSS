using AzolutionDbUtility.Common;
using System;
using System.Collections.Generic;

namespace AzolutionDbUtility
{
	public static class GridQueryBuilder<T>
	{
		public static string Query(GridOptions options, string query, string orderBy, string gridCondition)
		{
			string text = "";
			if (options != null)
			{
				text = FilterCondition(options.filter);
			}
			if (!string.IsNullOrEmpty(text))
			{
				text = " WHERE " + text;
			}
			if (!string.IsNullOrEmpty(gridCondition))
			{
				text = ((!string.IsNullOrEmpty(text)) ? (text + " AND " + gridCondition) : (" WHERE " + gridCondition));
			}
			string text2 = "";
			if (options != null && options.sort != null)
			{
				foreach (AzFilter.GridSort item in options.sort)
				{
					if (text2 == "")
					{
						string text3 = text2;
						text2 = text3 + "ORDER by " + item.field + " " + item.dir;
					}
					else
					{
						string text3 = text2;
						text2 = text3 + " , " + item.field + " " + item.dir;
					}
				}
			}
			if (text2 == "")
			{
				if (string.IsNullOrEmpty(orderBy))
				{
					throw new Exception("Must be set Orderby column Name");
				}
				text2 = " ORDER BY " + orderBy;
			}
			int num = 0;
			int num2 = 0;
			if (options != null)
			{
				num2 = options.skip;
				num = num2 + options.take;
			}
			return string.Format("SELECT * FROM (SELECT ROW_NUMBER() OVER({4}) AS ROWINDEX, T.* FROM ({0}) T {2}) tbl WHERE ROWINDEX >{1} AND ROWINDEX <={3}", query, num2, text, num, text2);
		}

		public static string FilterCondition(AzFilter.GridFilters filter)
		{
			string text = "";
			if (filter != null && filter.Filters != null && filter.Filters.Count > 0)
			{
				List<object> parameters = new List<object>();
				List<AzFilter.GridFilter> filters = filter.Filters;
				for (int i = 0; i < filters.Count; i++)
				{
					if (i == 0)
					{
						if (filters[i].Value == null)
						{
							i++;
							if (filters.Count == i)
							{
								break;
							}
						}
						text += $" {UtilityCommon.BuildWhereClause<T>(i, filter.Logic, filters[i], parameters)}";
					}
					else if (filters[i].Value != null)
					{
						text += $" {UtilityCommon.ToLinqOperator(filter.Logic)} {UtilityCommon.BuildWhereClause<T>(i, filter.Logic, filters[i], parameters)}";
					}
				}
			}
			return text;
		}

		public static string FilterCondition(AzFilter.AutoCompFilters filter)
		{
			string text = "";
			if (filter != null && filter.Filters != null && filter.Filters.Count > 0)
			{
				List<object> parameters = new List<object>();
				List<AzFilter.AutoCompFilter> filters = filter.Filters;
				for (int i = 0; i < filters.Count; i++)
				{
					if (i == 0)
					{
						if (filters[i].Value == null)
						{
							i++;
							if (filters.Count == i)
							{
								break;
							}
						}
						text += $" {UtilityCommon.BuildWhereClause<T>(i, filter.Logic, filters[i], parameters)}";
					}
					else if (filters[i].Value != null)
					{
						text += $" {UtilityCommon.ToLinqOperator(filter.Logic)} {UtilityCommon.BuildWhereClause<T>(i, filter.Logic, filters[i], parameters)}";
					}
				}
			}
			return text;
		}

		public static string Query(AutoCompOptions options, string query, string orderBy, string gridCondition)
		{
			string text = "";
			text = FilterCondition(options.filter);
			if (!string.IsNullOrEmpty(text))
			{
				text = " WHERE " + text;
			}
			if (!string.IsNullOrEmpty(gridCondition))
			{
				text = ((!string.IsNullOrEmpty(text)) ? (text + " AND " + gridCondition) : (" WHERE " + gridCondition));
			}
			string text2 = "";
			if (text2 == "")
			{
				if (string.IsNullOrEmpty(orderBy))
				{
					throw new Exception("Must be set Orderby column Name");
				}
				text2 = " ORDER BY " + orderBy;
			}
			int num = options.skip + options.take;
			return string.Format("SELECT * FROM (SELECT ROW_NUMBER() OVER({4}) AS ROWINDEX, T.* FROM ({0}) T {2}) tbl WHERE ROWINDEX >{1} AND ROWINDEX <={3}", query, options.skip, text, num, text2);
		}
	}
}
