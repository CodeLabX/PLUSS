using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace AzolutionDbUtility
{
	public class IPChecker
	{
		public static bool IsIPv4(string ip)
		{
			if (IPAddress.TryParse(ip, out var address) && address.AddressFamily == AddressFamily.InterNetwork)
			{
				Ping ping = new Ping();
				PingReply pingReply = ping.Send(ip);
				if (pingReply.Status.ToString() == "Success")
				{
					return true;
				}
			}
			return false;
		}
	}
}
