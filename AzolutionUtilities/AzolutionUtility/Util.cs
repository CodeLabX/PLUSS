using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace AzolutionDbUtility
{
	public static class Util
	{
		public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
		{
			DataTable dataTable = new DataTable("DataTable");
			Type typeFromHandle = typeof(T);
			PropertyInfo[] properties = typeFromHandle.GetProperties();
			PropertyInfo[] array = properties;
			foreach (PropertyInfo propertyInfo in array)
			{
				Type type = propertyInfo.PropertyType;
				if (type.IsGenericType)
				{
					type = type.GetGenericArguments()[0];
				}
				dataTable.Columns.Add(propertyInfo.Name, type);
			}
			foreach (T item in collection)
			{
				DataRow dataRow = dataTable.NewRow();
				dataRow.BeginEdit();
				array = properties;
				foreach (PropertyInfo propertyInfo in array)
				{
					if (propertyInfo.GetValue(item, null) != null)
					{
						dataRow[propertyInfo.Name] = propertyInfo.GetValue(item, null);
					}
				}
				dataRow.EndEdit();
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		public static DataSet ToDataSet<T>(this IList<T> list)
		{
			Type typeFromHandle = typeof(T);
			DataSet dataSet = new DataSet();
			DataTable dataTable = new DataTable();
			dataSet.Tables.Add(dataTable);
			PropertyInfo[] properties = typeFromHandle.GetProperties();
			foreach (PropertyInfo propertyInfo in properties)
			{
				Type type = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;
				dataTable.Columns.Add(propertyInfo.Name, type);
			}
			foreach (T item in list)
			{
				DataRow dataRow = dataTable.NewRow();
				properties = typeFromHandle.GetProperties();
				foreach (PropertyInfo propertyInfo in properties)
				{
					dataRow[propertyInfo.Name] = propertyInfo.GetValue(item, null) ?? DBNull.Value;
				}
				dataTable.Rows.Add(dataRow);
			}
			return dataSet;
		}
	}
}
