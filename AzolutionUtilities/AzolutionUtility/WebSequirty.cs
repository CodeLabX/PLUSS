using System.Configuration;

namespace AzolutionDbUtility
{
	public static class WebSequirty
	{
		private static string provider = "DataProtectionConfigurationProvider";

		private static string section = "connectionStrings";

		public static bool EncryptWebConfig(Configuration config)
		{
			ConfigurationSection configurationSection = config.GetSection("connectionStrings");
			if (!configurationSection.SectionInformation.IsProtected)
			{
				configurationSection.SectionInformation.ProtectSection(provider);
				config.Save();
				return true;
			}
			return false;
		}
	}
}
