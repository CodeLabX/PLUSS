using System.Collections.Generic;

namespace AzolutionDbUtility
{
	public class GridEntity<T>
	{
		public IList<T> Items { get; set; }

		public int TotalCount { get; set; }
	}
}
