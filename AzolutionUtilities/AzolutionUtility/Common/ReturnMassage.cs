namespace AzolutionDbUtility.Common
{
	public class ReturnMassage
	{
		public string Operation { get; set; }

		public string Message { get; set; }

		public object DataResult { get; set; }
	}
}
