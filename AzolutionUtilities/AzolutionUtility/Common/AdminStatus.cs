namespace AzolutionDbUtility.Common
{
	public enum AdminStatus
	{
		PendingApproval = 1,
		Approved,
		Reject
	}
}
