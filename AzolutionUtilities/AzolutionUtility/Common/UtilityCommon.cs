using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace AzolutionDbUtility.Common
{
	public class UtilityCommon
	{
		public enum Status
		{
			Active = 1,
			Inactive = 0
		}

		public static string BuildWhereClause<T>(int index, string logic, AzFilter.GridFilter filter, List<object> parameters)
		{
			Type typeFromHandle = typeof(T);
			PropertyInfo property = typeFromHandle.GetProperty(filter.Field);
			switch (filter.Operator.ToLower())
			{
			case "eq":
				if (typeof(DateTime).IsAssignableFrom(property.PropertyType))
				{
					parameters.Add(filter.Value.Trim());
					return $"{filter.Field} = '{filter.Value.Trim().ToLower()}'";
				}
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) = '{filter.Value.Trim().ToLower()}'";
			case "neq":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) != '{filter.Value.Trim().ToLower()}'";
			case "gte":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) >= '{filter.Value.Trim().ToLower()}'";
			case "gt":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) > '{filter.Value.Trim().ToLower()}'";
			case "lte":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) <= '{filter.Value.Trim().ToLower()}%'";
			case "lt":
				if (typeof(DateTime).IsAssignableFrom(property.PropertyType))
				{
					parameters.Add(DateTime.Parse(filter.Value).Date);
					return $"EntityFunctions.TruncateTime(Lower({filter.Field})){ToLinqOperator(filter.Operator)}@{filter.Value.Trim().ToLower()}";
				}
				if (typeof(int).IsAssignableFrom(property.PropertyType))
				{
					parameters.Add(int.Parse(filter.Value));
					return $"Lower({filter.Field}){ToLinqOperator(filter.Operator)} '{filter.Value.Trim().ToLower()}'";
				}
				parameters.Add(filter.Value);
				return $"Lower({filter.Field}){ToLinqOperator(filter.Operator)}'{filter.Value.Trim().ToLower()}'";
			case "startswith":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) like '{filter.Value.Trim().ToLower()}%'";
			case "endswith":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) like '%{filter.Value.Trim()}'";
			case "contains":
				parameters.Add(filter.Value);
				return $"Lower({filter.Field}) like '%{filter.Value.Trim().ToLower()}%'";
			case "doesnotcontain":
				parameters.Add(filter.Value);
				return $"Lower({filter.Field}) not like '%{filter.Value.Trim().ToLower()}%'";
			default:
				throw new ArgumentException("This operator is not yet supported for this Grid", filter.Operator);
			}
		}

		public static string BuildWhereClause<T>(int index, string logic, AzFilter.AutoCompFilter filter, List<object> parameters)
		{
			Type typeFromHandle = typeof(T);
			PropertyInfo property = typeFromHandle.GetProperty(filter.Field);
			switch (filter.Operator.ToLower())
			{
			case "eq":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) = '{filter.Value.Trim().ToLower()}'";
			case "neq":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) != '{filter.Value.Trim().ToLower()}'";
			case "gte":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) >= '{filter.Value.Trim().ToLower()}'";
			case "gt":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) > '{filter.Value.Trim().ToLower()}'";
			case "lte":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) <= '{filter.Value.Trim().ToLower()}%'";
			case "lt":
				if (typeof(DateTime).IsAssignableFrom(property.PropertyType))
				{
					parameters.Add(DateTime.Parse(filter.Value).Date);
					return $"EntityFunctions.TruncateTime(Lower({filter.Field})){ToLinqOperator(filter.Operator)}@{filter.Value.Trim().ToLower()}";
				}
				if (typeof(int).IsAssignableFrom(property.PropertyType))
				{
					parameters.Add(int.Parse(filter.Value));
					return $"Lower({filter.Field}){ToLinqOperator(filter.Operator)} '{filter.Value.Trim().ToLower()}'";
				}
				parameters.Add(filter.Value);
				return $"Lower({filter.Field}){ToLinqOperator(filter.Operator)}'{filter.Value.Trim().ToLower()}'";
			case "startswith":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) like '{filter.Value.Trim().ToLower()}%'";
			case "endswith":
				parameters.Add(filter.Value.Trim());
				return $"Lower({filter.Field}) like '%{filter.Value.Trim()}'";
			case "contains":
				parameters.Add(filter.Value);
				return $"Lower({filter.Field}) like '%{filter.Value.Trim().ToLower()}%'";
			case "doesnotcontain":
				parameters.Add(filter.Value);
				return $"Lower({filter.Field}) not like '%{filter.Value.Trim().ToLower()}%'";
			default:
				throw new ArgumentException("This operator is not yet supported for this Grid", filter.Operator);
			}
		}

		public static string ToLinqOperator(string @operator)
		{
			switch (@operator.ToLower())
			{
			case "eq":
				return " = ";
			case "neq":
				return " != ";
			case "gte":
				return " >= ";
			case "gt":
				return " > ";
			case "lte":
				return " <= ";
			case "lt":
				return " < ";
			case "or":
				return " or ";
			case "and":
				return " and ";
			default:
				return null;
			}
		}

		public static bool IsFileExistInPath(string filePath)
		{
			try
			{
				return File.Exists(filePath);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
