using System;
using System.Data;
using System.IO;
using System.Text;

namespace AzolutionDbUtility
{
	public class Az_ExportToTxt
	{
		public void GenerateTxt(DataTable dsExprot, DataTable dtEbbsData, string filePath)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < dsExprot.Rows.Count; i++)
			{
				string text = GenerateStringWithZero(dtEbbsData.Rows[0]["EbbsAccountNo"].ToString(), 13);
				string text2 = GenerateStringWithZero(dsExprot.Rows[i][0].ToString(), 13);
				string text3 = GenerateStringWithZero(dsExprot.Rows[i][7].ToString(), 9);
				string[] array = dsExprot.Rows[i]["Amount"].ToString().Split('.');
				string text4 = GenerateStringWithZero(Convert.ToInt32(array[0]).ToString(), 15);
				string text5 = array[1].ToString();
				string value = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8} {9}{10}", text + "     ", text4, text5, "D", dtEbbsData.Rows[0]["EbbsCode"].ToString(), DateTime.Now.ToString("yyMMdd"), dsExprot.Rows[i]["CardNo"].ToString() + "                   ", "CARD PAYMENT                       ", "BANK CODE", text3 + "                              ", Environment.NewLine);
				stringBuilder.Append(value);
				string value2 = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8} {9}{10}", text2 + "     ", text4, text5, "C", dtEbbsData.Rows[0]["BankCode"].ToString(), DateTime.Now.ToString("yyMMdd"), dsExprot.Rows[i]["CardNo"].ToString() + "                   ", "CARD PAYMENT                       ", "BANK CODE", text3 + "                              ", Environment.NewLine);
				stringBuilder.Append(value2);
			}
			File.WriteAllText(filePath, stringBuilder.ToString());
		}

		private string GenerateStringWithZero(string fieldValue, int fieldlength)
		{
			string text = "";
			if (fieldValue.Length < fieldlength)
			{
				int num = fieldlength - fieldValue.Length;
				for (int i = 0; i < num; i++)
				{
					text += "0";
				}
				return text + fieldValue;
			}
			return fieldValue;
		}
	}
}
