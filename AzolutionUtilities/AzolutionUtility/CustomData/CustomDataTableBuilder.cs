using AzolutionDbUtility;
using System;
using System.Data;

namespace AzolutionUtility.CustomData
{
	public class CustomDataTableBuilder
	{
		public static DataTable GetCustomTable(DataTable dataTable, string destinationName)
		{
			return GetCustomTable(dataTable, destinationName, 0);
		}

		private static DataRow CustomDataRow(DataTable tableStructure, DataRow rowWithValue, int userId)
		{
			DataRow dataRow = tableStructure.NewRow();
			foreach (DataColumn column in tableStructure.Columns)
			{
				object obj = "";
				if (rowWithValue.Table.Columns.Contains(column.ColumnName))
				{
					object value = rowWithValue[column.ColumnName];
					obj = CustomValueBind.ValueConverter(column.DataType, value);
					dataRow[column.ColumnName] = obj;
				}
				else if (column.ColumnName == "UserId" && userId > 0)
				{
					obj = userId;
					dataRow[column.ColumnName] = obj;
				}
			}
			return dataRow;
		}

		public static DataTable GetCustomTable(DataTable dataTable, string tempTableName, int userId)
		{
			DataTable dataTable2 = new DataTable();
			AzDbConnection commonConnection = new AzDbConnection();
			try
			{
				string sql = "Select top (0) * from " + tempTableName;
				DataTable dataTable3 = commonConnection.GetDataTable(sql);
				foreach (DataRow row in dataTable.Rows)
				{
					dataTable3.Rows.Add(CustomDataRow(dataTable3, row, userId));
				}
				dataTable2 = dataTable3;
				dataTable2.TableName = tempTableName;
			}
			catch (Exception ex)
			{
			}
			finally
			{
				commonConnection.Close();
			}
			return dataTable2;
		}
	}
}
