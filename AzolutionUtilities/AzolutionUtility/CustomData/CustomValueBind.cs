using System;

namespace AzolutionUtility.CustomData
{
	public class CustomValueBind
	{
		public static object ValueConverter(Type prop, object value)
		{
			try
			{
				switch (prop.Name)
				{
				case "Int16":
					value = Convert.ToInt16(value);
					break;
				case "Int32":
					value = Convert.ToInt32(value);
					break;
				case "Int64":
					value = Convert.ToInt64(value);
					break;
				case "Double":
					value = Convert.ToDouble(value);
					break;
				case "Decimal":
					value = Convert.ToDecimal(value);
					break;
				case "SByte":
					value = Convert.ToSByte(value);
					break;
				case "Single":
					value = Convert.ToSingle(value);
					break;
				case "String":
					value = Convert.ToString(value);
					break;
				case "Char":
					value = Convert.ToChar(value);
					break;
				case "Byte":
					value = Convert.ToByte(value);
					break;
				case "Boolean":
					value = Convert.ToBoolean(value);
					break;
				case "DateTime":
					value = Convert.ToDateTime(value);
					break;
				}
			}
			catch (Exception)
			{
				return value = string.Empty;
			}
			return value;
		}
	}
}
