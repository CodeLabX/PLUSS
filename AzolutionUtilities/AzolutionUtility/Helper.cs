namespace AzolutionDbUtility
{
	public class Helper
	{
		public static string ReplaceUnwantedCharWithShrinkString(string text)
		{
			string text2 = text;
			while (text2.Length > 31)
			{
				text2 = text2.Remove(7, 8);
			}
			text2 = text2.Replace("\\", " ");
			text2 = text2.Replace("/", " ");
			text2 = text2.Replace("?", " ");
			text2 = text2.Replace("*", " ");
			text2 = text2.Replace("[", " ");
			text2 = text2.Replace("]", " ");
			return text2.Replace(",", " ");
		}

		public static string ReplaceUnwantedChar(string text)
		{
			string text2 = text;
			text2 = text2.Replace("\\", " ");
			text2 = text2.Replace("/", " ");
			text2 = text2.Replace("?", " ");
			text2 = text2.Replace("*", " ");
			text2 = text2.Replace("[", " ");
			text2 = text2.Replace("]", " ");
			return text2.Replace(",", " ");
		}

		public static string ExcelColumnLetter(int intCol)
		{
			int num = intCol / 26;
			int num2 = intCol % 26;
			num += 64;
			num2 += 65;
			char c = ((num <= 64) ? ' ' : ((char)num));
			char c2 = (char)num2;
			string text = string.Concat(c, c2);
			return text.Trim();
		}
	}
}
