using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;

namespace AzolutionDbUtility
{
	public class ListConversion
	{
		public static DataTable ConvertTo<T>(IList<T> list)
		{
			DataTable dataTable = CreateTable<T>();
			Type typeFromHandle = typeof(T);
			PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeFromHandle);
			foreach (T item in list)
			{
				DataRow dataRow = dataTable.NewRow();
				foreach (PropertyDescriptor item2 in properties)
				{
					dataRow[item2.Name.ToUpper()] = item2.GetValue(item);
				}
				dataTable.Rows.Add(dataRow);
			}
			return dataTable;
		}

		public static IList<T> ConvertTo<T>(IList<DataRow> rows)
		{
			IList<T> list = null;
			if (rows != null)
			{
				list = new List<T>();
				foreach (DataRow row in rows)
				{
					T item = CreateItem<T>(row);
					list.Add(item);
				}
			}
			return list;
		}

		public static IList<T> ConvertTo<T>(DataTable table)
		{
			if (table == null)
			{
				return new List<T>();
			}
			List<DataRow> rows = table.Rows.Cast<DataRow>().ToList();
			return ConvertTo<T>(rows);
		}

		public static T CreateItem<T>(DataRow row)
		{
			T val = default(T);
			if (row != null)
			{
				val = Activator.CreateInstance<T>();
				foreach (DataColumn column in row.Table.Columns)
				{
					if (!(column.ColumnName.ToLower() != "rowindex"))
					{
						continue;
					}
					PropertyInfo[] properties = val.GetType().GetProperties();
					PropertyInfo[] array = properties;
					foreach (PropertyInfo propertyInfo in array)
					{
						PropertyInfo propertyInfo2 = propertyInfo;
						if (!(propertyInfo.Name.ToLower() == column.ColumnName.ToLower()))
						{
							continue;
						}
						try
						{
							if (row[column.ColumnName] != DBNull.Value)
							{
								object value = row[column.ColumnName];
								value = ValueConverter(propertyInfo2, value);
								propertyInfo2.SetValue(val, value, null);
							}
						}
						catch (Exception ex)
						{
							throw new Exception($"Please check your entity and query column name({column.ColumnName}) {ex.Message}");
						}
						break;
					}
				}
			}
			return val;
		}

		public static DataTable CreateTable<T>()
		{
			Type typeFromHandle = typeof(T);
			DataTable dataTable = new DataTable(typeFromHandle.Name.ToUpper());
			PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeFromHandle);
			foreach (PropertyDescriptor item in properties)
			{
				dataTable.Columns.Add(item.Name.ToUpper(), item.PropertyType);
			}
			return dataTable;
		}

		private static object ValueConverter(PropertyInfo prop, object value)
		{
			try
			{
				switch (prop.PropertyType.Name)
				{
				case "Int16":
					value = Convert.ToInt16(value);
					break;
				case "Int32":
					value = Convert.ToInt32(value);
					break;
				case "Int64":
					value = Convert.ToInt64(value);
					break;
				case "Double":
					value = Convert.ToDouble(value);
					break;
				case "Decimal":
					value = Convert.ToDecimal(value);
					break;
				case "SByte":
					value = Convert.ToSByte(value);
					break;
				case "Single":
					value = Convert.ToSingle(value);
					break;
				case "String":
					value = Convert.ToString(value);
					break;
				case "Char":
					value = Convert.ToChar(value);
					break;
				case "Byte":
					value = Convert.ToByte(value);
					break;
				case "Byte[]":
				{
					string text = value.ToString();
					string[] array = text.Split(',');
					value = Convert.FromBase64String(array[1]);
					break;
				}
				case "Boolean":
					value = Convert.ToBoolean(value);
					break;
				case "DateTime":
					try
					{
						value = Convert.ToDateTime(value);
					}
					catch (Exception)
					{
						value = DateTime.Now;
					}
					break;
				}
			}
			catch (Exception)
			{
				return value = string.Empty;
			}
			return value;
		}
	}
}
