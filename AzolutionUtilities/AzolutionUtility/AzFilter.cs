using System.Collections.Generic;

namespace AzolutionDbUtility
{
	public class AzFilter
	{
		public class GridFilter
		{
			public string Operator { get; set; }

			public string Field { get; set; }

			public string Value { get; set; }
		}

		public class GridFilters
		{
			public List<GridFilter> Filters { get; set; }

			public string Logic { get; set; }
		}

		public class GridSort
		{
			public string field { get; set; }

			public string dir { get; set; }
		}

		public class AutoCompFilter
		{
			public string Field { get; set; }

			public bool Ignore { get; set; }

			public string Operator { get; set; }

			public string Value { get; set; }
		}

		public class AutoCompFilters
		{
			public List<AutoCompFilter> Filters { get; set; }

			public string Logic { get; set; }
		}

		public class SortDescription
		{
			public string field { get; set; }

			public string dir { get; set; }
		}

		public class FilterContainer
		{
			public List<FilterDescription> filters { get; set; }

			public string logic { get; set; }
		}

		public class FilterDescription
		{
			public string @operator { get; set; }

			public string field { get; set; }

			public string value { get; set; }
		}

		public class OrderForPaggging
		{
			public string OrderByField { get; set; }

			public string OrderByType { get; set; }
		}
	}
}
