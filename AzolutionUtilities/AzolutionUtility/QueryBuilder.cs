using System;
using System.Reflection;

namespace AzolutionDbUtility
{
	public class QueryBuilder<T>
	{
		public static string QueryString(T objEntity, string tableName, string[] columns)
		{
			string result = "";
			string text = "INSERT INTO " + tableName;
			foreach (string text2 in columns)
			{
				PropertyInfo[] properties = objEntity.GetType().GetProperties();
				PropertyInfo[] array = properties;
				foreach (PropertyInfo propertyInfo in array)
				{
					if (propertyInfo.Name.ToLower() == text2.ToLower())
					{
						PropertyInfo prop = propertyInfo;
						ValueChecker(prop, objEntity);
						break;
					}
				}
			}
			return result;
		}

		private static object ValueChecker(PropertyInfo prop, object value)
		{
			switch (prop.PropertyType.Name)
			{
			case "Int32":
				value = Convert.ToInt32(value);
				break;
			case "Int64":
				value = Convert.ToInt64(value);
				break;
			case "Double":
				value = Convert.ToDouble(value);
				break;
			case "Decimal":
				value = Convert.ToDecimal(value);
				break;
			case "Single":
				value = Convert.ToSingle(value);
				break;
			case "String":
				value = "'" + Convert.ToString(value) + "'";
				break;
			case "Char":
				value = Convert.ToChar(value);
				break;
			case "Byte":
				value = Convert.ToByte(value);
				break;
			case "Boolean":
				value = Convert.ToBoolean(value);
				break;
			case "DateTime":
				value = string.Concat("'", Convert.ToDateTime(value), "'");
				break;
			}
			return value;
		}
	}
}
