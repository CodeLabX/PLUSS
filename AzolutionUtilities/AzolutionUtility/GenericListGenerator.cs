using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace AzolutionDbUtility
{
	public class GenericListGenerator
	{
		public static IList<T> GetList<T>(DataTable table)
		{
			if (table == null)
			{
				return null;
			}
			List<DataRow> rows = table.Rows.Cast<DataRow>().ToList();
			return ConvertTo<T>(rows);
		}

		private static IList<T> ConvertTo<T>(IList<DataRow> rows)
		{
			IList<T> list = null;
			if (rows != null)
			{
				list = new List<T>();
				foreach (DataRow row in rows)
				{
					T item = CreateItem<T>(row);
					list.Add(item);
				}
			}
			return list;
		}

		private static T CreateItem<T>(DataRow row)
		{
			T val = default(T);
			if (row != null)
			{
				val = Activator.CreateInstance<T>();
				PropertyInfo[] properties = val.GetType().GetProperties();
				PropertyInfo[] array = properties;
				foreach (PropertyInfo propertyInfo in array)
				{
					PropertyInfo propertyInfo2 = propertyInfo;
					object obj = new object();
					bool flag = TypeArray().Contains(propertyInfo2.PropertyType.Name);
					if (!flag)
					{
						Type propertyType = propertyInfo2.PropertyType;
						obj = Activator.CreateInstance(propertyType);
					}
					foreach (DataColumn column in row.Table.Columns)
					{
						if (!(column.ColumnName.ToLower() != "rowindex"))
						{
							continue;
						}
						if (propertyInfo.Name.ToLower() == column.ColumnName.ToLower())
						{
							try
							{
								if (row[column.ColumnName] != DBNull.Value)
								{
									object value = row[column.ColumnName];
									value = ValueConverter(propertyInfo2, value);
									propertyInfo2.SetValue(val, value, null);
								}
							}
							catch (Exception ex)
							{
								throw new Exception("Please check your entity and query column name " + ex.Message);
							}
							break;
						}
						if (flag)
						{
							continue;
						}
						PropertyInfo[] properties2 = obj.GetType().GetProperties();
						PropertyInfo[] array2 = properties2;
						foreach (PropertyInfo propertyInfo3 in array2)
						{
							if (!(propertyInfo3.Name.ToLower() == column.ColumnName.ToLower()))
							{
								continue;
							}
							try
							{
								if (row[column.ColumnName] != DBNull.Value)
								{
									object value = row[column.ColumnName];
									value = ValueConverter(propertyInfo3, value);
									propertyInfo3.SetValue(obj, value, null);
								}
							}
							catch (Exception ex)
							{
								throw new Exception("Please check your entity and query column name " + ex.Message);
							}
							break;
						}
						propertyInfo2.SetValue(val, obj, null);
					}
				}
			}
			return val;
		}

		private static object ValueConverter(PropertyInfo prop, object value)
		{
			try
			{
				switch (prop.PropertyType.Name)
				{
				case "Int16":
					value = Convert.ToInt16(value);
					break;
				case "Int32":
					value = Convert.ToInt32(value);
					break;
				case "Int64":
					value = Convert.ToInt64(value);
					break;
				case "Double":
					value = Convert.ToDouble(value);
					break;
				case "Decimal":
					value = Convert.ToDecimal(value);
					break;
				case "SByte":
					value = Convert.ToSByte(value);
					break;
				case "Single":
					value = Convert.ToSingle(value);
					break;
				case "String":
					value = Convert.ToString(value);
					break;
				case "Char":
					value = Convert.ToChar(value);
					break;
				case "Byte":
					value = Convert.ToByte(value);
					break;
				case "Boolean":
					value = Convert.ToBoolean(value);
					break;
				case "DateTime":
					value = Convert.ToDateTime(value);
					break;
				}
			}
			catch (Exception)
			{
				return value = string.Empty;
			}
			return value;
		}

		private static string[] TypeArray()
		{
			return new string[10] { "String", "Int32", "Int64", "Double", "Decimal", "Single", "Char", "Byte", "Boolean", "DateTime" };
		}
	}
}
