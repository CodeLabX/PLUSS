using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AzolutionDbUtility
{
	public static class Kendo<T>
	{
		public class Grid
		{
			public static GridEntity<T> DataSource(GridOptions options, string query, string orderBy, string condition)
			{
				AzDbConnection commonConnection = new AzDbConnection();
				try
				{
					query = query.Replace(';', ' ');
					string sql = ((options != null) ? GridQueryBuilder<T>.Query(options, query, orderBy, condition) : query);
					if (!string.IsNullOrEmpty(condition))
					{
						condition = " WHERE " + condition;
					}
					string text = ((options != null) ? GridQueryBuilder<T>.FilterCondition(options.filter) : "");
					if (!string.IsNullOrEmpty(text))
					{
						condition = (string.IsNullOrEmpty(condition) ? (" WHERE " + text) : (condition + " And " + text));
					}
					DataTable dataTable = commonConnection.GetDataTable(sql);
					string sql2 = "";
					if (commonConnection.DatabaseType == DatabaseType.SQL)
					{
						sql2 = "SELECT COUNT(*) FROM (" + query + " ) As tbl " + condition;
					}
					else if (commonConnection.DatabaseType == DatabaseType.Oracle)
					{
						sql2 = "SELECT COUNT(*) FROM (" + query + " )" + condition;
					}
					int scaler = commonConnection.GetScaler(sql2);
					List<T> list = (List<T>)ListConversion.ConvertTo<T>(dataTable);
					return new GridResult<T>().Data(list, scaler);
				}
				catch (Exception ex)
				{
					throw ex;
				}
				finally
				{
					commonConnection.Close();
				}
			}

			public static GridEntity<T> DataSource(GridOptions options, string query, string orderBy)
			{
				return DataSource(options, query, orderBy, "");
			}

			public static GridEntity<T> GenericDataSource(GridOptions options, string query, string orderBy, string condition)
			{
				AzDbConnection commonConnection = new AzDbConnection();
				GetGridPagingQuery(options, query, orderBy, condition, out var gridQuery, out var totalQuery, commonConnection.DatabaseType);
				DataTable dataTable = commonConnection.GetDataTable(gridQuery.ToString());
				int scaler = commonConnection.GetScaler(totalQuery.ToString());
				List<T> list = (List<T>)GenericListGenerator.GetList<T>(dataTable);
				return new GridResult<T>().Data(list, scaler);
			}

			public static GridEntity<T> GenericDataSource(GridOptions options, string query, string orderBy)
			{
				return GenericDataSource(options, query, orderBy, "");
			}

			private static void GetGridPagingQuery(GridOptions options, string query, string orderBy, string condition, out StringBuilder gridQuery, out StringBuilder totalQuery, DatabaseType databaseType)
			{
				try
				{
					gridQuery = new StringBuilder();
					totalQuery = new StringBuilder();
					query = query.Replace(';', ' ');
					string value = ((options != null) ? GridQueryBuilder<T>.Query(options, query, orderBy, condition) : query);
					if (!string.IsNullOrEmpty(condition))
					{
						condition = " WHERE " + condition;
					}
					string text = ((options != null) ? GridQueryBuilder<T>.FilterCondition(options.filter) : "");
					if (!string.IsNullOrEmpty(text))
					{
						condition = (string.IsNullOrEmpty(condition) ? (" WHERE " + text) : (condition + " And " + text));
					}
					string value2 = "";
					switch (databaseType)
					{
					case DatabaseType.SQL:
						value2 = "SELECT COUNT(*) FROM (" + query + " ) As tbl " + condition;
						break;
					case DatabaseType.Oracle:
						value2 = "SELECT COUNT(*) FROM (" + query + " )" + condition;
						break;
					}
					gridQuery.Append(value);
					totalQuery.Append(value2);
				}
				catch (Exception ex)
				{
					throw ex;
				}
				finally
				{
				}
			}
		}

		public class Combo
		{
			public static List<T> DataSource(string query)
			{
				List<T> result = new List<T>();
				AzDbConnection commonConnection = new AzDbConnection();
				try
				{
					DataTable dataTable = commonConnection.GetDataTable(query);
					List<T> list = (List<T>)ListConversion.ConvertTo<T>(dataTable);
					if (list != null)
					{
						return list;
					}
				}
				catch (Exception ex)
				{
					throw ex;
				}
				finally
				{
					commonConnection.Close();
				}
				return result;
			}
		}

		public class AutoComplete
		{
			public static object DataSource(AutoCompOptions options, string query, string orderBy)
			{
				AzDbConnection commonConnection = new AzDbConnection();
				try
				{
					query = query.Replace(';', ' ');
					string sql = GridQueryBuilder<T>.Query(options, query, orderBy, "");
					DataTable dataTable = commonConnection.GetDataTable(sql);
					return (List<T>)ListConversion.ConvertTo<T>(dataTable);
				}
				catch (Exception)
				{
					throw;
				}
				finally
				{
					commonConnection.Close();
				}
			}
		}
	}
}
