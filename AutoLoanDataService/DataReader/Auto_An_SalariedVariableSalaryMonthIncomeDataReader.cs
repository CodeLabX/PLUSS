﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_SalariedVariableSalaryMonthIncomeDataReader : EntityDataReader<Auto_An_SalariedVariableSalaryMonthIncome>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int SalaiedIDColumn = -1;
        public int MonthColumn = -1;
        public int AmountColumn = -1;



        public Auto_An_SalariedVariableSalaryMonthIncomeDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_SalariedVariableSalaryMonthIncome Read()
        {
            var objAuto_An_SalariedVariableSalaryMonthIncome = new Auto_An_SalariedVariableSalaryMonthIncome();

            objAuto_An_SalariedVariableSalaryMonthIncome.ID = GetInt(IDColumn);
            objAuto_An_SalariedVariableSalaryMonthIncome.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_SalariedVariableSalaryMonthIncome.SalaiedID = GetInt(SalaiedIDColumn);
            objAuto_An_SalariedVariableSalaryMonthIncome.Month = GetInt(MonthColumn);
            objAuto_An_SalariedVariableSalaryMonthIncome.Amount = GetDouble(AmountColumn);

            return objAuto_An_SalariedVariableSalaryMonthIncome;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "SALAIEDID": { SalaiedIDColumn = i; break; }
                    case "MONTH": { MonthColumn = i; break; }
                    case "AMOUNT": { AmountColumn = i; break; }
                }
            }
        }


    }
}
