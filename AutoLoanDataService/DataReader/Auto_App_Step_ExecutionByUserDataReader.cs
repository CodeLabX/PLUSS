﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_App_Step_ExecutionByUserDataReader : EntityDataReader<Auto_App_Step_ExecutionByUser>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int Auto_LoanMasterIdColumn = -1;
        public int AppRaiseByColumn = -1;
        public int AppSupportByColumn = -1;
        public int AppAnalysisByColumn = -1;
        public int AppApprovedByColumn = -1;

        public int AppRaiseByNameColumn = -1;
        public int AppSupportByNameColumn = -1;
        public int AppAnalysisByNameColumn = -1;
        public int AppApprovedByNameColumn = -1;


        public Auto_App_Step_ExecutionByUserDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_App_Step_ExecutionByUser Read()
        {
            var objAuto_App_Step_ExecutionByUser = new Auto_App_Step_ExecutionByUser();

            objAuto_App_Step_ExecutionByUser.ID = GetInt(IDColumn);
            objAuto_App_Step_ExecutionByUser.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_App_Step_ExecutionByUser.Auto_LoanMasterId = GetInt(Auto_LoanMasterIdColumn);
            objAuto_App_Step_ExecutionByUser.AppRaiseBy = GetInt(AppRaiseByColumn);
            objAuto_App_Step_ExecutionByUser.AppSupportBy = GetInt(AppSupportByColumn);
            objAuto_App_Step_ExecutionByUser.AppAnalysisBy = GetInt(AppAnalysisByColumn);
            objAuto_App_Step_ExecutionByUser.AppApprovedBy = GetInt(AppApprovedByColumn);

            objAuto_App_Step_ExecutionByUser.AppRaiseByName = GetString(AppRaiseByNameColumn);
            objAuto_App_Step_ExecutionByUser.AppSupportByName = GetString(AppSupportByNameColumn);
            objAuto_App_Step_ExecutionByUser.AppAnalysisByName = GetString(AppAnalysisByNameColumn);
            objAuto_App_Step_ExecutionByUser.AppApprovedByName = GetString(AppApprovedByNameColumn);

            return objAuto_App_Step_ExecutionByUser;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "AUTO_LOANMASTERID":
                        {
                            Auto_LoanMasterIdColumn = i;
                            break;
                        }
                    case "APPRAISEBY":
                        {
                            AppRaiseByColumn = i;
                            break;
                        }
                    case "APPSUPPORTBY":
                        {
                            AppSupportByColumn = i;
                            break;
                        }
                    case "APPANALYSISBY":
                        {
                            AppAnalysisByColumn = i;
                            break;
                        }
                    case "APPAPPROVEDBY":
                        {
                            AppApprovedByColumn = i;
                            break;
                        }
                    case "APPRAISEBYNAME":
                        {
                            AppRaiseByNameColumn = i;
                            break;
                        }
                    case "APPSUPPORTBYNAME":
                        {
                            AppSupportByNameColumn = i;
                            break;
                        }
                    case "APPANALYSISBYNAME":
                        {
                            AppAnalysisByNameColumn = i;
                            break;
                        }
                    case "APPAPPROVEDBYNAME":
                        {
                            AppApprovedByNameColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
