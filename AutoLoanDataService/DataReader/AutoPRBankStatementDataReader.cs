﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoPRBankStatementDataReader : EntityDataReader<AutoPRBankStatement>
    {
        public int PrBankStatementIdColumn = -1;
        public int LLIDColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int BankIdColumn = -1;
        public int BranchIdColumn = -1;
        public int AccountNameColumn = -1;
        public int AccountNumberColumn = -1;
        public int AccountCategoryColumn = -1;
        public int AccountTypeColumn = -1;
        public int StatementColumn = -1;
        public int LastUpdateDateColumn = -1;
        public int StatementDateFromColumn = -1;
        public int StatementDateToColumn = -1;
        public int BANK_NMColumn = -1;
        public int BRAN_NAMEColumn = -1;


        public AutoPRBankStatementDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoPRBankStatement Read()
        {
            var objAutoPRBankStatement = new AutoPRBankStatement();
            objAutoPRBankStatement.PrBankStatementId = GetInt(PrBankStatementIdColumn);
            objAutoPRBankStatement.LLID = GetInt(LLIDColumn);
            objAutoPRBankStatement.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoPRBankStatement.BankId = GetInt(BankIdColumn);
            objAutoPRBankStatement.BranchId = GetInt(BranchIdColumn);
            objAutoPRBankStatement.AccountName = GetString(AccountNameColumn);
            objAutoPRBankStatement.AccountNumber = GetString(AccountNumberColumn);
            objAutoPRBankStatement.AccountCategory = GetString(AccountCategoryColumn);
            objAutoPRBankStatement.AccountType = GetString(AccountTypeColumn);
            objAutoPRBankStatement.Statement = GetString(StatementColumn);
            objAutoPRBankStatement.LastUpdateDate = GetDate(LastUpdateDateColumn);
            objAutoPRBankStatement.StatementDateFrom = GetDate(StatementDateFromColumn);
            objAutoPRBankStatement.StatementDateTo = GetDate(StatementDateToColumn);
            objAutoPRBankStatement.BANK_NM = GetString(BANK_NMColumn);
            objAutoPRBankStatement.BRAN_NAME = GetString(BRAN_NAMEColumn);

            return objAutoPRBankStatement;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "PRBANKSTATEMENTID":
                        {
                            PrBankStatementIdColumn = i;
                            break;
                        }
                    case "LLID":
                        {
                            LLIDColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "BANKID":
                        {
                            BankIdColumn = i;
                            break;
                        }
                    case "BRANCHID":
                        {
                            BranchIdColumn = i;
                            break;
                        }
                    case "ACCOUNTNAME":
                        {
                            AccountNameColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBER":
                        {
                            AccountNumberColumn = i;
                            break;
                        }
                    case "ACCOUNTCATEGORY":
                        {
                            AccountCategoryColumn = i;
                            break;
                        }
                    case "ACCOUNTTYPE":
                        {
                            AccountTypeColumn = i;
                            break;
                        }
                    case "STATEMENT":
                        {
                            StatementColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "STATEMENTDATEFROM":
                        {
                            StatementDateFromColumn = i;
                            break;
                        }
                    case "STATEMENTDATETO":
                        {
                            StatementDateToColumn = i;
                            break;
                        }
                    case "BANK_NM":
                        {
                            BANK_NMColumn = i;
                            break;
                        }
                    case "BRAN_NAME":
                        {
                            BRAN_NAMEColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
