﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoApplicantSCBAccountDataReader : EntityDataReader<AutoApplicantSCBAccount>
    {
        public int ApplicantAccountIDColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int LLIDColumn = -1;
        public int AccountNumberSCBColumn = -1;
        public int AccountStatusColumn = -1;
        public int PriorityHolderColumn = -1;



        public AutoApplicantSCBAccountDataReader(IDataReader reader)
            : base(reader)
        { }


        public override AutoApplicantSCBAccount Read()
        {
            var objAutoApplicantSCBAccount = new AutoApplicantSCBAccount();
            objAutoApplicantSCBAccount.ApplicantAccountID = GetInt(ApplicantAccountIDColumn);
            objAutoApplicantSCBAccount.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoApplicantSCBAccount.LLID = GetInt(LLIDColumn);
            objAutoApplicantSCBAccount.AccountNumberSCB = GetString(AccountNumberSCBColumn);
            objAutoApplicantSCBAccount.AccountStatus = GetInt(AccountStatusColumn);
            objAutoApplicantSCBAccount.PriorityHolder = GetInt(PriorityHolderColumn);

            return objAutoApplicantSCBAccount;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "APPLICANTACCOUNTID":
                        {
                            ApplicantAccountIDColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "LLID":
                        {
                            LLIDColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBERSCB":
                        {
                            AccountNumberSCBColumn = i;
                            break;
                        }
                    case "ACCOUNTSTATUS":
                        {
                            AccountStatusColumn = i;
                            break;
                        }
                    case "PRIORITYHOLDER":
                        {
                            PriorityHolderColumn = i;
                            break;
                        }
                }
            }
        }



    }
}
