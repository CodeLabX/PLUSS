﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_AnalystIncomBankStatementSumDataReader : EntityDataReader<Auto_AnalystIncomBankStatementSum>
    {
        public int IdColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int BankStatementIdColumn = -1;
        public int TotalCTO12Column = -1;
        public int AvgCTO12Column = -1;
        public int AvgBalance12Column = -1;
        public int TotalCTO6Column = -1;
        public int AvgCTO6Column = -1;
        public int AvgBalance6Column = -1;
        public int ConsideredCTOColumn = -1;
        public int ConsideredBalanceColumn = -1;
        public int ConsideredMarginColumn = -1;
        public int GrossIncomeColumn = -1;
        public int NetAfterPaymentColumn = -1;
        public int ShareColumn = -1;
        public int NetIncomeColumn = -1;
        public int IncomeAssessmentMethodColumn = -1;
        public int IndustryColumn = -1;
        public int SubSectorColumn = -1;
        public int PrivateIncomeColumn = -1;
        public int AvgBalance1st9mColumn = -1;
        public int AvgBalance1st6mColumn = -1;
        public int AvgBalanceLast6mColumn = -1;
        public int AvgBalanceLast3mColumn = -1;
        public int TypeColumn = -1;
        public int RemarksColumn = -1;
        public int TabTypeColumn = -1;


        public Auto_AnalystIncomBankStatementSumDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_AnalystIncomBankStatementSum Read()
        {
            var objAutoAnalystIncomBankStatementSum = new Auto_AnalystIncomBankStatementSum();
            objAutoAnalystIncomBankStatementSum.Id = GetInt(IdColumn);
            objAutoAnalystIncomBankStatementSum.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAutoAnalystIncomBankStatementSum.BankStatementId = GetInt(BankStatementIdColumn);
            objAutoAnalystIncomBankStatementSum.TotalCTO12 = GetDouble(TotalCTO12Column);
            objAutoAnalystIncomBankStatementSum.AvgCTO12 = GetDouble(AvgCTO12Column);
            objAutoAnalystIncomBankStatementSum.AvgBalance12 = GetDouble(AvgBalance12Column);
            objAutoAnalystIncomBankStatementSum.TotalCTO6 = GetDouble(TotalCTO6Column);
            objAutoAnalystIncomBankStatementSum.AvgCTO6 = GetDouble(AvgCTO6Column);
            objAutoAnalystIncomBankStatementSum.AvgBalance6 = GetDouble(AvgBalance6Column);
            objAutoAnalystIncomBankStatementSum.ConsideredCTO = GetDouble(ConsideredCTOColumn);
            objAutoAnalystIncomBankStatementSum.ConsideredBalance = GetDouble(ConsideredBalanceColumn);
            objAutoAnalystIncomBankStatementSum.ConsideredMargin = GetDouble(ConsideredMarginColumn);
            objAutoAnalystIncomBankStatementSum.GrossIncome = GetDouble(GrossIncomeColumn);
            objAutoAnalystIncomBankStatementSum.NetAfterPayment = GetDouble(NetAfterPaymentColumn);
            objAutoAnalystIncomBankStatementSum.Share = GetDouble(ShareColumn);
            objAutoAnalystIncomBankStatementSum.NetIncome = GetDouble(NetIncomeColumn);
            objAutoAnalystIncomBankStatementSum.IncomeAssessmentMethod = GetInt(IncomeAssessmentMethodColumn);
            objAutoAnalystIncomBankStatementSum.Industry = GetInt(IndustryColumn);
            objAutoAnalystIncomBankStatementSum.SubSector = GetInt(SubSectorColumn);
            objAutoAnalystIncomBankStatementSum.PrivateIncome = GetInt(PrivateIncomeColumn);
            objAutoAnalystIncomBankStatementSum.AvgBalance1st9m = GetDouble(AvgBalance1st9mColumn);
            objAutoAnalystIncomBankStatementSum.AvgBalance1st6m = GetDouble(AvgBalance1st6mColumn);
            objAutoAnalystIncomBankStatementSum.AvgBalanceLast6m = GetDouble(AvgBalanceLast6mColumn);
            objAutoAnalystIncomBankStatementSum.AvgBalanceLast3m = GetDouble(AvgBalanceLast3mColumn);
            objAutoAnalystIncomBankStatementSum.Type = GetInt(TypeColumn);
            objAutoAnalystIncomBankStatementSum.Remarks = GetString(RemarksColumn);
            objAutoAnalystIncomBankStatementSum.TabType = GetInt(TabTypeColumn);
            return objAutoAnalystIncomBankStatementSum;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IdColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "BANKSTATEMENTID":
                        {
                            BankStatementIdColumn = i;
                            break;
                        }
                    case "TOTALCTO12":
                        {
                            TotalCTO12Column = i;
                            break;
                        }
                    case "AVGCTO12":
                        {
                            AvgCTO12Column = i;
                            break;
                        }
                    case "AVGBALANCE12":
                        {
                            AvgBalance12Column = i;
                            break;
                        }
                    case "TOTALCTO6":
                        {
                            TotalCTO6Column = i;
                            break;
                        }
                    case "AVGCTO6":
                        {
                            AvgCTO6Column = i;
                            break;
                        }
                    case "AVGBALANCE6":
                        {
                            AvgBalance6Column = i;
                            break;
                        }
                    case "CONSIDEREDCTO":
                        {
                            ConsideredCTOColumn = i;
                            break;
                        }
                    case "CONSIDEREDBALANCE":
                        {
                            ConsideredBalanceColumn = i;
                            break;
                        }
                    case "CONSIDEREDMARGIN":
                        {
                            ConsideredMarginColumn = i;
                            break;
                        }
                    case "GROSSINCOME":
                        {
                            GrossIncomeColumn = i;
                            break;
                        }
                    case "NETAFTERPAYMENT":
                        {
                            NetAfterPaymentColumn = i;
                            break;
                        }
                    case "SHARE":
                        {
                            ShareColumn = i;
                            break;
                        }
                    case "NETINCOME":
                        {
                            NetIncomeColumn = i;
                            break;
                        }
                    case "INCOMEASSESSMENTMETHOD":
                        {
                            IncomeAssessmentMethodColumn = i;
                            break;
                        }
                    case "INDUSTRY":
                        {
                            IndustryColumn = i;
                            break;
                        }
                    case "SUBSECTOR":
                        {
                            SubSectorColumn = i;
                            break;
                        }
                    case "PRIVATEINCOME":
                        {
                            PrivateIncomeColumn = i;
                            break;
                        }
                    case "AVGBALANCE1ST9M":
                        {
                            AvgBalance1st9mColumn = i;
                            break;
                        }
                    case "AVGBALANCE1ST6M":
                        {
                            AvgBalance1st6mColumn = i;
                            break;
                        }
                    case "AVGBALANCELAST6M":
                        {
                            AvgBalanceLast6mColumn = i;
                            break;
                        }
                    case "AVGBALANCELAST3M":
                        {
                            AvgBalanceLast3mColumn = i;
                            break;
                        }
                    case "TYPE":
                        {
                            TypeColumn = i;
                            break;
                        }
                    case "REMARKS":
                        {
                            RemarksColumn = i;
                            break;
                        }
                    case "TABTYPE":
                        {
                            TabTypeColumn = i;
                            break;
                        }

                }
            }
        }
    }
}
