﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class Auto_SubPreviousRepaymentHistoryOffSCB_OnSCBDataReader : EntityDataReader<Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB>
   {
       public int IdColumn = -1;
       public int PreviousRepaymentHistoryIdColumn = -1;
       public int BankIdColumn = -1;
       public int TypeColumn = -1;
       public int EMIColumn = -1;
       public int ConsiderColumn = -1;
       

       public Auto_SubPreviousRepaymentHistoryOffSCB_OnSCBDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB Read()
       {
           var objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB = new Auto_SubPreviousRepaymentHistoryOffSCB_OnSCB();
           objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.Id = GetInt(IdColumn);
           objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.PreviousRepaymentHistoryId = GetInt(PreviousRepaymentHistoryIdColumn);
           objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.BankId = GetInt(BankIdColumn);
           objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.Type = GetInt(TypeColumn);
           objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.EMI = GetDouble(EMIColumn);
           objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB.Consider = GetBool(ConsiderColumn);
           return objAuto_SubPreviousRepaymentHistoryOffSCB_OnSCB;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "PREVIOUSREPAYMENTHISTORYID":
                       {
                           PreviousRepaymentHistoryIdColumn = i;
                           break;
                       }
                   case "BANKID":
                       {
                           BankIdColumn = i;
                           break;
                       }
                   case "TYPE":
                       {
                           TypeColumn = i;
                           break;
                       }
                   case "EMI":
                       {
                           EMIColumn = i;
                           break;
                       }
                   case "CONSIDER":
                       {
                           ConsiderColumn = i;
                           break;
                       }
                   
               }
           }
       }
   }
    
}
