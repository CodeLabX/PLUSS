﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
namespace AutoLoanDataService.DataReader
{
    internal class AutoPRFinanceDataReader : EntityDataReader<AutoPRFinance>
    {

        public int PrFinanceIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int DPISourceColumn = -1;
        public int DPIAmountColumn = -1;
        public int DOISourceColumn = -1;
        public int DOIAmountColumn = -1;
        public int EDRentAndUtilitiesColumn = -1;
        public int EDFoodAndClothingColumn = -1;
        public int EDEducationColumn = -1;
        public int EDLoanRePaymentColumn = -1;
        public int EDOtherColumn = -1;
        public int RepaymentToColumn = -1;
        public int PaymentForColumn = -1;


        public AutoPRFinanceDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoPRFinance Read()
        {
            var objAutoPRFinance = new AutoPRFinance();
            objAutoPRFinance.PrFinanceId = GetInt(PrFinanceIdColumn);
            objAutoPRFinance.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoPRFinance.DPISource = GetString(DPISourceColumn);
            objAutoPRFinance.DPIAmount = GetDouble(DPIAmountColumn);
            objAutoPRFinance.DOISource = GetString(DOISourceColumn);
            objAutoPRFinance.DOIAmount = GetDouble(DOIAmountColumn);
            if (objAutoPRFinance.DOIAmount == double.MinValue) {
                objAutoPRFinance.DOIAmount = 0;
            }
            objAutoPRFinance.EDRentAndUtilities = GetDouble(EDRentAndUtilitiesColumn);
            objAutoPRFinance.EDFoodAndClothing = GetDouble(EDFoodAndClothingColumn);
            objAutoPRFinance.EDEducation = GetDouble(EDEducationColumn);
            objAutoPRFinance.EDLoanRePayment = GetDouble(EDLoanRePaymentColumn);
            objAutoPRFinance.EDOther = GetDouble(EDOtherColumn);
            objAutoPRFinance.RepaymentTo = GetString(RepaymentToColumn);
            objAutoPRFinance.PaymentFor = GetString(PaymentForColumn);

            return objAutoPRFinance;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "PRFINANCEID":
                        {
                            PrFinanceIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "DPISOURCE":
                        {
                            DPISourceColumn = i;
                            break;
                        }
                    case "DPIAMOUNT":
                        {
                            DPIAmountColumn = i;
                            break;
                        }
                    case "DOISOURCE":
                        {
                            DOISourceColumn = i;
                            break;
                        }
                    case "DOIAMOUNT":
                        {
                            DOIAmountColumn = i;
                            break;
                        }
                    case "EDRENTANDUTILITIES":
                        {
                            EDRentAndUtilitiesColumn = i;
                            break;
                        }
                    case "EDFOODANDCLOTHING":
                        {
                            EDFoodAndClothingColumn = i;
                            break;
                        }
                    case "EDEDUCATION":
                        {
                            EDEducationColumn = i;
                            break;
                        }
                    case "EDLOANREPAYMENT":
                        {
                            EDLoanRePaymentColumn = i;
                            break;
                        }
                    case "EDOTHER":
                        {
                            EDOtherColumn = i;
                            break;
                        }
                    case "REPAYMENTTO":
                        {
                            RepaymentToColumn = i;
                            break;
                        }
                    case "PAYMENTFOR":
                        {
                            PaymentForColumn = i;
                            break;
                        }
                }
            }
        }




    }

}
