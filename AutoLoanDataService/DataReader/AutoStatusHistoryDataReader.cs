﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoStatusHistoryDataReader : EntityDataReader<AutoStatusHistory>
    {
        public int AutoLoanHistoryIDColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int UserIDColumn = -1;
        public int StatusIDColumn = -1;
        public int ActionDateColumn = -1;
        public int StatusNameColumn = -1;
        public int TotalcountColumn = -1;
        



        public AutoStatusHistoryDataReader(IDataReader reader)
            : base(reader)
        { }


        public override AutoStatusHistory Read()
        {
            var objAutoStatusHistory = new AutoStatusHistory();
            objAutoStatusHistory.AutoLoanHistoryID = GetInt(AutoLoanHistoryIDColumn);
            objAutoStatusHistory.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoStatusHistory.UserID = GetInt(UserIDColumn);
            objAutoStatusHistory.StatusID = GetInt(StatusIDColumn);
            objAutoStatusHistory.ActionDate = GetDate(ActionDateColumn);
            objAutoStatusHistory.StatusName = GetString(StatusNameColumn);
            objAutoStatusHistory.Totalcount = GetInt(TotalcountColumn);
            return objAutoStatusHistory;
        }


        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTOLOANHISTORYID":
                        {
                            AutoLoanHistoryIDColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "USERID":
                        {
                            UserIDColumn = i;
                            break;
                        }
                    case "STATUSID":
                        {
                            StatusIDColumn = i;
                            break;
                        }
                    case "ACTIONDATE":
                        {
                            ActionDateColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalcountColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
