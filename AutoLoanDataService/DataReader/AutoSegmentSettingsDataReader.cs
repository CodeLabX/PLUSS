﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoSegmentSettingsDataReader : EntityDataReader<AutoSegmentSettings>
    {
        public int SegmentIDColumn = -1;
        public int SegmentCodeColumn = -1;
        public int SegmentNameColumn = -1;
        public int DescriptionColumn = -1;
        public int ProfessionColumn = -1;
        public int MinLoanAmtColumn = -1;
        public int MaxLoanAmtColumn = -1;
        public int MinTanorColumn = -1;
        public int MaxTanorColumn = -1;
        public int IrWithARTAColumn = -1;
        public int IrWithoutARTAColumn = -1;
        public int MinAgeL1Column = -1;
        public int MinAgeL2Column = -1;
        public int MaxAgeL1Column = -1;
        public int MaxAgeL2Column = -1;
        public int LTVL1Column = -1;
        public int LTVL2Column = -1;
        public int ExperienceL1Column = -1;
        public int ExperienceL2Column = -1;
        public int AcRelationshipL1Column = -1;
        public int AcRelationshipL2Column = -1;
        public int CriteriaCodeColumn = -1;
        public int AssetCodeColumn = -1;
        public int MinDBRL1Column = -1;
        public int MinDBRL2Column = -1;
        public int MaxDBRL2Column = -1;
        public int DBRL3Column = -1;
        public int FieldVisitColumn = -1;
        public int VerificationColumn = -1;
        public int LandTelephoneColumn = -1;
        public int GuranteeReferenceColumn = -1;
        public int PrimaryMinIncomeL1Column = -1;
        public int PrimaryMinIncomeL2Column = -1;
        public int JointMinIncomeL1Column = -1;
        public int JointMinIncomeL2Column = -1;
        public int CommentsColumn = -1;
        public int UserIdColumn = -1;
        public int LastUpdateDateColumn = -1;

        //Selected Property
        public int TotalCountColumn = -1;
        public int ProfNameColumn = -1;
        public int ProfIDColumn = -1;




        public AutoSegmentSettingsDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoSegmentSettings Read()
        {
            var objAutoSegmentSettings = new AutoSegmentSettings();
            objAutoSegmentSettings.SegmentID = GetInt(SegmentIDColumn);
            objAutoSegmentSettings.SegmentCode = GetString(SegmentCodeColumn);
            objAutoSegmentSettings.SegmentName = GetString(SegmentNameColumn);
            objAutoSegmentSettings.Description = GetString(DescriptionColumn);
            objAutoSegmentSettings.Profession = GetInt(ProfessionColumn);
            objAutoSegmentSettings.MinLoanAmt = GetDouble(MinLoanAmtColumn);
            if (objAutoSegmentSettings.MinLoanAmt == double.MinValue) {
                objAutoSegmentSettings.MinLoanAmt = 0;
            }

            objAutoSegmentSettings.MaxLoanAmt = GetDouble(MaxLoanAmtColumn);
            if (objAutoSegmentSettings.MaxLoanAmt == double.MinValue) {
                objAutoSegmentSettings.MaxLoanAmt = 0;
            }
            objAutoSegmentSettings.MinTanor = GetInt(MinTanorColumn);
            if (objAutoSegmentSettings.MinTanor == int.MinValue) {
                objAutoSegmentSettings.MinTanor = 0;
            }
            objAutoSegmentSettings.MaxTanor = GetInt(MaxTanorColumn);
            if (objAutoSegmentSettings.MaxTanor == int.MinValue) {
                objAutoSegmentSettings.MaxTanor = 0;
            }
            objAutoSegmentSettings.IrWithARTA = GetDouble(IrWithARTAColumn);
            if (objAutoSegmentSettings.IrWithARTA == double.MinValue) {
                objAutoSegmentSettings.IrWithARTA = 0;
            }
            objAutoSegmentSettings.IrWithoutARTA = GetDouble(IrWithoutARTAColumn);
            if (objAutoSegmentSettings.IrWithoutARTA == double.MinValue) {
                objAutoSegmentSettings.IrWithoutARTA = 0;
            }
            objAutoSegmentSettings.MinAgeL1 = GetInt(MinAgeL1Column);
            if (objAutoSegmentSettings.MinAgeL1 == int.MinValue) {
                objAutoSegmentSettings.MinAgeL1 = 0;
            }
            objAutoSegmentSettings.MinAgeL2 = GetInt(MinAgeL2Column);
            if (objAutoSegmentSettings.MinAgeL2 == int.MinValue) {
                objAutoSegmentSettings.MinAgeL2 = 0;
            }
            objAutoSegmentSettings.MaxAgeL1 = GetInt(MaxAgeL1Column);
            if (objAutoSegmentSettings.MaxAgeL1 == int.MinValue) {
                objAutoSegmentSettings.MaxAgeL1 = 0;
            }
            objAutoSegmentSettings.MaxAgeL2 = GetInt(MaxAgeL2Column);
            if (objAutoSegmentSettings.MaxAgeL2 == int.MinValue) {
                objAutoSegmentSettings.MaxAgeL2 = 0;
            }
            objAutoSegmentSettings.LTVL1 = GetDouble(LTVL1Column);
            if (objAutoSegmentSettings.LTVL1 == double.MinValue) {
                objAutoSegmentSettings.LTVL1 = 0;
            }
            objAutoSegmentSettings.LTVL2 = GetDouble(LTVL2Column);
            if (objAutoSegmentSettings.LTVL2 == double.MinValue) {
                objAutoSegmentSettings.LTVL2 = 0;
            }
            objAutoSegmentSettings.ExperienceL1 = GetDouble(ExperienceL1Column);
            if (objAutoSegmentSettings.ExperienceL1 == double.MinValue) {
                objAutoSegmentSettings.ExperienceL1 = 0;
            }
            objAutoSegmentSettings.ExperienceL2 = GetDouble(ExperienceL2Column);
            if (objAutoSegmentSettings.ExperienceL2 == double.MinValue)
            {
                objAutoSegmentSettings.ExperienceL2 = 0;
            }
            objAutoSegmentSettings.AcRelationshipL1 = GetInt(AcRelationshipL1Column);
            if (objAutoSegmentSettings.AcRelationshipL1 == int.MinValue) {
                objAutoSegmentSettings.AcRelationshipL1 = 0;
            }
            objAutoSegmentSettings.AcRelationshipL2 = GetInt(AcRelationshipL2Column);
            if (objAutoSegmentSettings.AcRelationshipL2 == int.MinValue) {
                objAutoSegmentSettings.AcRelationshipL2 = 0;
            }
            objAutoSegmentSettings.CriteriaCode = GetString(CriteriaCodeColumn);

            objAutoSegmentSettings.AssetCode = GetString(AssetCodeColumn);
            objAutoSegmentSettings.MinDBRL1 = GetDouble(MinDBRL1Column);
            if (objAutoSegmentSettings.MinDBRL1 == double.MinValue) {
                objAutoSegmentSettings.MinDBRL1 = 0;
            }
            objAutoSegmentSettings.MinDBRL2 = GetDouble(MinDBRL2Column);
            if (objAutoSegmentSettings.MinDBRL2 == double.MinValue) {
                objAutoSegmentSettings.MinDBRL2 = 0;
            }
            objAutoSegmentSettings.MaxDBRL2 = GetDouble(MaxDBRL2Column);
            if (objAutoSegmentSettings.MaxDBRL2 == double.MinValue) {
                objAutoSegmentSettings.MaxDBRL2 = 0;
            }
            objAutoSegmentSettings.DBRL3 = GetDouble(DBRL3Column);
            if (objAutoSegmentSettings.DBRL3 == double.MinValue) {
                objAutoSegmentSettings.DBRL3 = 0;
            }
            objAutoSegmentSettings.FieldVisit = GetBool(FieldVisitColumn);
            objAutoSegmentSettings.Verification = GetBool(VerificationColumn);
            objAutoSegmentSettings.LandTelephone = GetBool(LandTelephoneColumn);
            objAutoSegmentSettings.GuranteeReference = GetBool(GuranteeReferenceColumn);
            objAutoSegmentSettings.PrimaryMinIncomeL1 = GetDouble(PrimaryMinIncomeL1Column);
            if (objAutoSegmentSettings.PrimaryMinIncomeL1 == double.MinValue) {
                objAutoSegmentSettings.PrimaryMinIncomeL1 = 0;
            }
            objAutoSegmentSettings.PrimaryMinIncomeL2 = GetDouble(PrimaryMinIncomeL2Column);
            if (objAutoSegmentSettings.PrimaryMinIncomeL2 == double.MinValue) {
                objAutoSegmentSettings.PrimaryMinIncomeL2 = 0;
            }
            objAutoSegmentSettings.JointMinIncomeL1 = GetDouble(JointMinIncomeL1Column);
            if (objAutoSegmentSettings.JointMinIncomeL1 == double.MinValue) {
                objAutoSegmentSettings.JointMinIncomeL1 = 0;
            }
            objAutoSegmentSettings.JointMinIncomeL2 = GetDouble(JointMinIncomeL2Column);
            if (objAutoSegmentSettings.JointMinIncomeL2 == double.MinValue) {
                objAutoSegmentSettings.JointMinIncomeL2 = 0;
            }
            objAutoSegmentSettings.Comments = GetString(CommentsColumn);
            objAutoSegmentSettings.UserId = GetInt(UserIdColumn);
            objAutoSegmentSettings.LastUpdateDate = GetDate(LastUpdateDateColumn);

            //Selected Property
            objAutoSegmentSettings.ProfName = GetString(ProfNameColumn);
            objAutoSegmentSettings.ProfID = GetInt(ProfIDColumn);
            objAutoSegmentSettings.TotalCount = GetInt(TotalCountColumn);
            //objAutoSegmentSettings.TotalCount = Convert.ToInt32(reader.GetValue(TotalCountColumn));
            return objAutoSegmentSettings;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "SEGMENTID":
                        {
                            SegmentIDColumn = i;
                            break;
                        }
                    case "SEGMENTCODE":
                        {
                            SegmentCodeColumn = i;
                            break;
                        }
                    case "SEGMENTNAME":
                        {
                            SegmentNameColumn = i;
                            break;
                        }
                    case "DESCRIPTION":
                        {
                            DescriptionColumn = i;
                            break;
                        }
                    case "PROFESSION":
                        {
                            ProfessionColumn = i;
                            break;
                        }
                    case "MINLOANAMT":
                        {
                            MinLoanAmtColumn = i;
                            break;
                        }
                    case "MAXLOANAMT":
                        {
                            MaxLoanAmtColumn = i;
                            break;
                        }
                    case "MINTANOR":
                        {
                            MinTanorColumn = i;
                            break;
                        }
                    case "MAXTANOR":
                        {
                            MaxTanorColumn = i;
                            break;
                        }
                    case "IRWITHARTA":
                        {
                            IrWithARTAColumn = i;
                            break;
                        }
                    case "IRWITHOUTARTA":
                        {
                            IrWithoutARTAColumn = i;
                            break;
                        }
                    case "MINAGEL1":
                        {
                            MinAgeL1Column = i;
                            break;
                        }
                    case "MINAGEL2":
                        {
                            MinAgeL2Column = i;
                            break;
                        }
                    case "MAXAGEL1":
                        {
                            MaxAgeL1Column = i;
                            break;
                        }
                    case "MAXAGEL2":
                        {
                            MaxAgeL2Column = i;
                            break;
                        }
                    case "LTVL1":
                        {
                            LTVL1Column = i;
                            break;
                        }
                    case "LTVL2":
                        {
                            LTVL2Column = i;
                            break;
                        }
                    case "EXPERIENCEL1":
                        {
                            ExperienceL1Column = i;
                            break;
                        }
                    case "EXPERIENCEL2":
                        {
                            ExperienceL2Column = i;
                            break;
                        }
                    case "ACRELATIONSHIPL1":
                        {
                            AcRelationshipL1Column = i;
                            break;
                        }
                    case "ACRELATIONSHIPL2":
                        {
                            AcRelationshipL2Column = i;
                            break;
                        }
                    case "CRITERIACODE":
                        {
                            CriteriaCodeColumn = i;
                            break;
                        }
                    case "ASSETCODE":
                        {
                            AssetCodeColumn = i;
                            break;
                        }
                    case "MINDBRL1":
                        {
                            MinDBRL1Column = i;
                            break;
                        }
                    case "MINDBRL2":
                        {
                            MinDBRL2Column = i;
                            break;
                        }
                    case "MAXDBRL2":
                        {
                            MaxDBRL2Column = i;
                            break;
                        }
                    case "DBRL3":
                        {
                            DBRL3Column = i;
                            break;
                        }
                    case "FIELDVISIT":
                        {
                            FieldVisitColumn = i;
                            break;
                        }
                    case "VERIFICATION":
                        {
                            VerificationColumn = i;
                            break;
                        }
                    case "LANDTELEPHONE":
                        {
                            LandTelephoneColumn = i;
                            break;
                        }
                    case "GURANTEEREFERENCE":
                        {
                            GuranteeReferenceColumn = i;
                            break;
                        }
                    case "PRIMARYMININCOMEL1":
                        {
                            PrimaryMinIncomeL1Column = i;
                            break;
                        }
                    case "PRIMARYMININCOMEL2":
                        {
                            PrimaryMinIncomeL2Column = i;
                            break;
                        }
                    case "JOINTMININCOMEL1":
                        {
                            JointMinIncomeL1Column = i;
                            break;
                        }
                    case "JOINTMININCOMEL2":
                        {
                            JointMinIncomeL2Column = i;
                            break;
                        }
                    case "COMMENTS":
                        {
                            CommentsColumn = i;
                            break;
                        }
                    case "USERID":
                        {
                            UserIdColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                    case "PROFNAME":
                        {
                            ProfNameColumn = i;
                            break;
                        }
                    case "PROFID":
                        {
                            ProfIDColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
