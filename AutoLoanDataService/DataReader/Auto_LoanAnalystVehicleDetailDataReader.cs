﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{

    internal class Auto_LoanAnalystVehicleDetailDataReader : EntityDataReader<Auto_LoanAnalystVehicleDetail>
        {
            public int IdColumn = -1;
            public int AnalystMasterIdColumn = -1;
            public int valuePackAllowedColumn = -1;
            public int SeatingCapacityColumn = -1;
            public int ConsideredPriceColumn = -1;
            public int PriceConsideredOnColumn = -1;
            public int CarVarificationColumn = -1;
            public int DeliveryStatusColumn = -1;
            public int RegistrationYearColumn = -1;


            public Auto_LoanAnalystVehicleDetailDataReader(IDataReader reader)
                : base(reader)
            {
            }

            public override Auto_LoanAnalystVehicleDetail Read()
            {
                var objAutoLoanAnalystVehicleDetail = new Auto_LoanAnalystVehicleDetail();
                objAutoLoanAnalystVehicleDetail.Id = GetInt(IdColumn);
                objAutoLoanAnalystVehicleDetail.AnalystMasterId = GetInt(AnalystMasterIdColumn);
                objAutoLoanAnalystVehicleDetail.valuePackAllowed = GetBool(valuePackAllowedColumn);
                objAutoLoanAnalystVehicleDetail.SeatingCapacity = GetInt(SeatingCapacityColumn);
                objAutoLoanAnalystVehicleDetail.ConsideredPrice = GetDouble(ConsideredPriceColumn);
                objAutoLoanAnalystVehicleDetail.PriceConsideredOn = GetInt(PriceConsideredOnColumn);
                objAutoLoanAnalystVehicleDetail.CarVarification = GetInt(CarVarificationColumn);
                objAutoLoanAnalystVehicleDetail.DeliveryStatus = GetInt(DeliveryStatusColumn);
                objAutoLoanAnalystVehicleDetail.RegistrationYear = GetInt(RegistrationYearColumn);
                if (objAutoLoanAnalystVehicleDetail.RegistrationYear == int.MinValue) {
                    objAutoLoanAnalystVehicleDetail.RegistrationYear = 0;
                }

                return objAutoLoanAnalystVehicleDetail;
            }

            protected override void ResolveOrdinal()
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    switch (reader.GetName(i).ToUpper())
                    {
                        case "ID":
                            {
                                IdColumn = i;
                                break;
                            }
                        case "ANALYSTMASTERID":
                            {
                                AnalystMasterIdColumn = i;
                                break;
                            }
                        case "VALUEPACKALLOWED":
                            {
                                valuePackAllowedColumn = i;
                                break;
                            }
                        case "SEATINGCAPACITY":
                            {
                                SeatingCapacityColumn = i;
                                break;
                            }
                        case "CONSIDEREDPRICE":
                            {
                                ConsideredPriceColumn = i;
                                break;
                            }
                        case "PRICECONSIDEREDON":
                            {
                                PriceConsideredOnColumn = i;
                                break;
                            }
                        case "CARVARIFICATION":
                            {
                                CarVarificationColumn = i;
                                break;
                            }
                        case "DELIVERYSTATUS":
                            {
                                DeliveryStatusColumn = i;
                                break;
                            }
                        case "REGISTRATIONYEAR":
                            {
                                RegistrationYearColumn = i;
                                break;
                            }


                    }
                }
            }
        }
    
}
