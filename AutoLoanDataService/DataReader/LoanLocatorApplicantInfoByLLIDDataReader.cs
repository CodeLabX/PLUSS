﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class LoanLocatorApplicantInfoByLLIDDataReader : EntityDataReader<LoanLocatorApplicantInfoByLLID>
    {
        public int LLIDColumn = -1;
        public int ApplicantNameColumn = -1;
        public int AccountNoColumn = -1;
        public int TinNoColumn = -1;
        public int LoanStatusColumn = -1;
        public int ProductColumn = -1;
        public int BranchColumn = -1;
        public int DateOfBirthColumn = -1;
        public int RecieveDateColumn = -1;
        public int ProductIdColumn = -1;
        public int BranchIdColumn = -1;
        public int LoanApplyAmountColumn = -1;
        public int EmployeTypeColumn = -1;
        public int OrganizationColumn = -1;
        public int SourcePersonNameColumn = -1;

        public LoanLocatorApplicantInfoByLLIDDataReader(IDataReader reader)
            : base(reader)
        { }
        public override LoanLocatorApplicantInfoByLLID Read()
        {
            var objLoanLocatorApplicantInfoByLLID = new LoanLocatorApplicantInfoByLLID();
            objLoanLocatorApplicantInfoByLLID.LLID = GetInt(LLIDColumn);
            objLoanLocatorApplicantInfoByLLID.ApplicantName = GetString(ApplicantNameColumn);
            objLoanLocatorApplicantInfoByLLID.AccountNo = GetString(AccountNoColumn);
            objLoanLocatorApplicantInfoByLLID.TinNo = GetString(TinNoColumn);
            objLoanLocatorApplicantInfoByLLID.LoanStatus = GetString(LoanStatusColumn);
            objLoanLocatorApplicantInfoByLLID.Product = GetString(ProductColumn);
            objLoanLocatorApplicantInfoByLLID.Branch = GetString(BranchColumn);
            objLoanLocatorApplicantInfoByLLID.DateOfBirth = GetString(DateOfBirthColumn);
            objLoanLocatorApplicantInfoByLLID.RecieveDate = GetDate(RecieveDateColumn);
            objLoanLocatorApplicantInfoByLLID.ProductId = GetInt(ProductIdColumn);
            objLoanLocatorApplicantInfoByLLID.BranchId = GetInt(BranchIdColumn);
            objLoanLocatorApplicantInfoByLLID.LoanApplyAmount = GetDouble(LoanApplyAmountColumn);
            objLoanLocatorApplicantInfoByLLID.EmployeType = GetString(EmployeTypeColumn);
            objLoanLocatorApplicantInfoByLLID.Organization = GetString(OrganizationColumn);
            objLoanLocatorApplicantInfoByLLID.SourcePersonName = GetString(SourcePersonNameColumn);

            return objLoanLocatorApplicantInfoByLLID;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "LOAN_APPL_ID":
                        {
                            LLIDColumn = i;
                            break;
                        }
                    case "CUST_NAME":
                        {
                            ApplicantNameColumn = i;
                            break;
                        }
                    case "ACC_NUMB":
                        {
                            AccountNoColumn = i;
                            break;
                        }
                    case "TINNO":
                        {
                            TinNoColumn = i;
                            break;
                        }
                    case "LOANSTATUS":
                        {
                            LoanStatusColumn = i;
                            break;
                        }
                    case "PRODUCT":
                        {
                            ProductColumn = i;
                            break;
                        }
                    case "BRANCH":
                        {
                            BranchColumn = i;
                            break;
                        }
                    case "DATEOFBIRTH":
                        {
                            DateOfBirthColumn = i;
                            break;
                        }
                    case "RECE_DATE":
                        {
                            RecieveDateColumn = i;
                            break;
                        }
                    case "PROD_ID":
                        {
                            ProductIdColumn = i;
                            break;
                        }
                    case "BR_ID":
                        {
                            BranchIdColumn = i;
                            break;
                        }
                    case "LOAN_APP_AMOU":
                        {
                            LoanApplyAmountColumn = i;
                            break;
                        }
                    case "EMPL_TYPE":
                        {
                            EmployeTypeColumn = i;
                            break;
                        }
                    case "ORGAN":
                        {
                            OrganizationColumn = i;
                            break;
                        }
                    case "NAME_LEARNER":
                        {
                            SourcePersonNameColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
