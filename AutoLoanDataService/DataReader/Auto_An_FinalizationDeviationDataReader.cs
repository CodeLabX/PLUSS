﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationDeviationDataReader : EntityDataReader<Auto_An_FinalizationDeviation>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int LevelColumn = -1;
        public int DescriptionColumn = -1;
        public int CodeColumn = -1;
        public int DescriptionTextColumn = -1;



        public Auto_An_FinalizationDeviationDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationDeviation Read()
        {
            var objAuto_An_FinalizationDeviation = new Auto_An_FinalizationDeviation();

            objAuto_An_FinalizationDeviation.ID = GetInt(IDColumn);
            objAuto_An_FinalizationDeviation.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationDeviation.Level = GetInt(LevelColumn);
            objAuto_An_FinalizationDeviation.Description = GetInt(DescriptionColumn);
            objAuto_An_FinalizationDeviation.Code = GetString(CodeColumn);
            objAuto_An_FinalizationDeviation.DescriptionText = GetString(DescriptionTextColumn);

            return objAuto_An_FinalizationDeviation;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "LEVEL": { LevelColumn = i; break; }
                    case "DESCRIPTION": { DescriptionColumn = i; break; }
                    case "CODE": { CodeColumn = i; break; }
                    case "DESCRIPTIONTEXT": { DescriptionTextColumn = i; break; }
                }
            }
        }




    }
}
