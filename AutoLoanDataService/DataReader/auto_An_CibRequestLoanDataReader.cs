﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Azolution.Common.SqlDataService;
using AutoLoanService.Entity;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class auto_An_CibRequestLoanDataReader : EntityDataReader<auto_An_CibRequestLoan>
    {
        public int CibRequestLoanIdColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int CibFacilityIdColumn = -1;
        public int CibAmountColumn = -1;
        public int CibTenorColumn = -1;

        public auto_An_CibRequestLoanDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override auto_An_CibRequestLoan Read()
        {
            var objauto_An_CibRequestLoan = new auto_An_CibRequestLoan();

            objauto_An_CibRequestLoan.CibRequestLoanId = GetInt(CibRequestLoanIdColumn);
            objauto_An_CibRequestLoan.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objauto_An_CibRequestLoan.CibFacilityId = GetInt(CibFacilityIdColumn);
            objauto_An_CibRequestLoan.CibAmount = GetDouble(CibAmountColumn);
            objauto_An_CibRequestLoan.CibTenor = GetDouble(CibTenorColumn);

            return objauto_An_CibRequestLoan;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "CIBREQUESTLOANID": { CibRequestLoanIdColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "CIBFACILITYID": { CibFacilityIdColumn = i; break; }
                    case "CIBAMOUNT": { CibAmountColumn = i; break; }
                    case "CIBTENOR": { CibTenorColumn = i; break; }
                }
            }
        }
    }
}
