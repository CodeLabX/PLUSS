﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class t_Pluss_RemarkDataReader : EntityDataReader<t_Pluss_Remark>
    {
        public int REMA_IDColumn = -1;
        public int REMA_PROD_IDColumn = -1;
        public int REMA_DTYPEColumn = -1;
        public int REMA_NAMEColumn = -1;
        public int REMA_NOColumn = -1;
        public int REMA_FORColumn = -1;
        public int REMA_STATUSColumn = -1;


        public t_Pluss_RemarkDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override t_Pluss_Remark Read()
        {
            var objt_Pluss_Remark = new t_Pluss_Remark();

            objt_Pluss_Remark.REMA_ID = GetInt(REMA_IDColumn);
            objt_Pluss_Remark.REMA_PROD_ID = GetInt(REMA_PROD_IDColumn);
            objt_Pluss_Remark.REMA_DTYPE = GetInt(REMA_DTYPEColumn);
            objt_Pluss_Remark.REMA_NAME = GetString(REMA_NAMEColumn);
            objt_Pluss_Remark.REMA_NO = GetInt(REMA_NOColumn);
            objt_Pluss_Remark.REMA_FOR = GetInt(REMA_FORColumn);
            objt_Pluss_Remark.REMA_STATUS = GetInt(REMA_STATUSColumn);

            return objt_Pluss_Remark;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "REMA_ID": { REMA_IDColumn = i; break; }
                    case "REMA_PROD_ID": { REMA_PROD_IDColumn = i; break; }
                    case "REMA_DTYPE": { REMA_DTYPEColumn = i; break; }
                    case "REMA_NAME": { REMA_NAMEColumn = i; break; }
                    case "REMA_NO": { REMA_NOColumn = i; break; }
                    case "REMA_FOR": { REMA_FORColumn = i; break; }
                    case "REMA_STATUS": { REMA_STATUSColumn = i; break; }
                }
            }
        }

    }
}
