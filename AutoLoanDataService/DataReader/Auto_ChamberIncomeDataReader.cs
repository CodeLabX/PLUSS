﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class Auto_ChamberIncomeDataReader : EntityDataReader<Auto_ChamberIncome>
   {
       public int IdColumn = -1;
       public int AutoPracticingDoctorIdColumn = -1;
       public int ChamberIdColumn = -1;
       public int OldPatentsColumn = -1;
       public int OldFeeColumn = -1;
       public int OldTotalColumn = -1;
       public int NewPatentsColumn = -1;
       public int NewFeeColumn = -1;
       public int NewTotalColumn = -1;
       public int TotalColumn = -1;
       public int DayMonthColumn = -1;
       public int IncomeColumn = -1;


       public Auto_ChamberIncomeDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override Auto_ChamberIncome Read()
       {
           var objAuto_ChamberIncome= new Auto_ChamberIncome();
           objAuto_ChamberIncome.Id = GetInt(IdColumn);
           objAuto_ChamberIncome.AutoPracticingDoctorId = GetInt(AutoPracticingDoctorIdColumn);
           objAuto_ChamberIncome.ChamberId = GetInt(ChamberIdColumn);
           objAuto_ChamberIncome.OldPatents = GetInt(OldPatentsColumn);
           objAuto_ChamberIncome.OldFee = GetDouble(OldFeeColumn);
           objAuto_ChamberIncome.OldTotal = GetDouble(OldTotalColumn);
           objAuto_ChamberIncome.NewPatents = GetInt(NewPatentsColumn);
           objAuto_ChamberIncome.NewFee = GetDouble(NewFeeColumn);
           objAuto_ChamberIncome.NewTotal = GetDouble(NewTotalColumn);
           objAuto_ChamberIncome.Total = GetDouble(TotalColumn);
           objAuto_ChamberIncome.DayMonth = GetInt(DayMonthColumn);
           objAuto_ChamberIncome.Income = GetDouble(IncomeColumn);

           return objAuto_ChamberIncome;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "AUTOPRACTICINGDOCTORID":
                       {
                           AutoPracticingDoctorIdColumn = i;
                           break;
                       }
                   case "CHAMBERID":
                       {
                           ChamberIdColumn = i;
                           break;
                       }
                   case "OLDPATENTS":
                       {
                           OldPatentsColumn = i;
                           break;
                       }
                   case "OLDFEE":
                       {
                           OldFeeColumn = i;
                           break;
                       }
                   case "OLDTOTAL":
                       {
                           OldTotalColumn = i;
                           break;
                       }
                   case "NEWPATENTS":
                       {
                           NewPatentsColumn = i;
                           break;
                       }
                   case "NEWFEE":
                       {
                           NewFeeColumn = i;
                           break;
                       }
                   case "NEWTOTAL":
                       {
                           NewTotalColumn = i;
                           break;
                       }
                   case "TOTAL":
                       {
                           TotalColumn = i;
                           break;
                       }
                   case "DAYMONTH":
                       {
                           DayMonthColumn = i;
                           break;
                       }
                   case "INCOME":
                       {
                           IncomeColumn = i;
                           break;
                       }

               }
           }
       }
   }
    
}
