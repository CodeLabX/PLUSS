﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;


namespace AutoLoanDataService.DataReader
{
    internal class Auto_InsuranceBancaDataReader : EntityDataReader<Auto_InsuranceBanca>
    {
        public int InsCompanyIdColumn = -1;
        public int CompanyNameColumn = -1;
        public int InsTypeColumn = -1;
        public int InsAgeLimitColumn = -1;
        public int InsVehicleCcColumn = -1;
        public int InsurancePercentColumn = -1;
        public int TotalCountColumn = -1;
        public int TenorColumn = -1;
        public int ArtaRateColumn = -1;
        

        public Auto_InsuranceBancaDataReader(IDataReader reader)
            : base(reader)
        { }
        public override Auto_InsuranceBanca Read()
        {
            var objAuto_InsuranceBanca = new Auto_InsuranceBanca();
            objAuto_InsuranceBanca.InsCompanyId = GetInt(InsCompanyIdColumn);
            objAuto_InsuranceBanca.CompanyName = GetString(CompanyNameColumn);
            objAuto_InsuranceBanca.InsType = GetInt(InsTypeColumn);
            objAuto_InsuranceBanca.InsAgeLimit = GetInt(InsAgeLimitColumn);
            objAuto_InsuranceBanca.InsVehicleCc = GetInt(InsVehicleCcColumn);
            objAuto_InsuranceBanca.InsurancePercent = GetDouble(InsurancePercentColumn);
            objAuto_InsuranceBanca.Tenor = GetDouble(TenorColumn);
            if (objAuto_InsuranceBanca.Tenor == double.MinValue)
            {
                objAuto_InsuranceBanca.Tenor = 0;
            }
            objAuto_InsuranceBanca.ArtaRate = GetDouble(ArtaRateColumn);
            if (objAuto_InsuranceBanca.ArtaRate == double.MinValue) {
                objAuto_InsuranceBanca.ArtaRate = 0;
            }
            
            objAuto_InsuranceBanca.TotalCount = Convert.ToInt32(reader.GetValue(TotalCountColumn));
            return objAuto_InsuranceBanca;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "INSCOMPANYID":
                        {
                            InsCompanyIdColumn = i;
                            break;
                        }
                    case "COMPANYNAME":
                        {
                            CompanyNameColumn = i;
                            break;
                        }
                    case "INSTYPE":
                        {
                            InsTypeColumn = i;
                            break;
                        }
                    case "INSAGELIMIT":
                        {
                            InsAgeLimitColumn = i;
                            break;
                        }
                    case "INSVEHICLECC":
                        {
                            InsVehicleCcColumn = i;
                            break;
                        }
                    case "INSURANCEPERCENT":
                        {
                            InsurancePercentColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                    case "TENOR":
                        {
                            TenorColumn = i;
                            break;
                        }
                    case "ARTARATE":
                        {
                            ArtaRateColumn = i;
                            break;
                        } 
                }
            }
        }
    }
}
