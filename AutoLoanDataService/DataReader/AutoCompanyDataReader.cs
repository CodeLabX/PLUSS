﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoCompanyDataReader : EntityDataReader<AutoCompany>
    {
        public int AutoCompanyIdColumn = -1;
        public int CompanyNameColumn = -1;
        public int RemarksColumn = -1;

        public AutoCompanyDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoCompany Read()
        {
            var objAutoCompany = new AutoCompany();
            objAutoCompany.AutoCompanyId = GetInt(AutoCompanyIdColumn);
            objAutoCompany.CompanyName = GetString(CompanyNameColumn);
            objAutoCompany.Remarks = GetString(RemarksColumn);
            return objAutoCompany;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTOCOMPANYID":
                        {
                            AutoCompanyIdColumn = i;
                            break;
                        }
                    case "COMPANYNAME":
                        {
                            CompanyNameColumn = i;
                            break;
                        }
                    case "REMARKS":
                        {
                            RemarksColumn = i;
                            break;
                        }
                }
            }
        }
    }
}
