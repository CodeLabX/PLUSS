﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoLoanFacilityOffSCBDataReader : EntityDataReader<AutoLoanFacilityOffSCB>
    {
        public int OffFacilityIdColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int BankIDColumn = -1;
        public int OriginalLimitColumn = -1;
        public int EMIColumn = -1;
        public int EMIPercentColumn = -1;
        public int EMIShareColumn = -1;
        public int StatusColumn = -1;
        public int ConsiderColumn = -1;
        public int TypeColumn = -1;
        public int InterestRateColumn = -1;
        public int BankNameColumn = -1;


        public AutoLoanFacilityOffSCBDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoLoanFacilityOffSCB Read()
        {
            var objAutoLoanFacilityOffSCB = new AutoLoanFacilityOffSCB();

            objAutoLoanFacilityOffSCB.OffFacilityId = GetInt(OffFacilityIdColumn);
            objAutoLoanFacilityOffSCB.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAutoLoanFacilityOffSCB.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoLoanFacilityOffSCB.BankID = GetInt(BankIDColumn);
            objAutoLoanFacilityOffSCB.OriginalLimit = GetDouble(OriginalLimitColumn);
            objAutoLoanFacilityOffSCB.EMI = GetDouble(EMIColumn);
            objAutoLoanFacilityOffSCB.EMIPercent = GetDouble(EMIPercentColumn);
            objAutoLoanFacilityOffSCB.EMIShare = GetDouble(EMIShareColumn);
            objAutoLoanFacilityOffSCB.Status = GetDouble(StatusColumn);
            objAutoLoanFacilityOffSCB.Consider = GetInt(ConsiderColumn);
            objAutoLoanFacilityOffSCB.Type = GetInt(TypeColumn);
            objAutoLoanFacilityOffSCB.InterestRate = GetDouble(InterestRateColumn);
            objAutoLoanFacilityOffSCB.BankName = GetString(BankNameColumn);

            return objAutoLoanFacilityOffSCB;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "OFFFACILITYID":
                        {
                            OffFacilityIdColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "BANKID":
                        {
                            BankIDColumn = i;
                            break;
                        }
                    case "ORIGINALLIMIT":
                        {
                            OriginalLimitColumn = i;
                            break;
                        }
                    case "EMI":
                        {
                            EMIColumn = i;
                            break;
                        }
                    case "EMIPERCENT":
                        {
                            EMIPercentColumn = i;
                            break;
                        }
                    case "EMISHARE":
                        {
                            EMIShareColumn = i;
                            break;
                        }
                    case "STATUS":
                        {
                            StatusColumn = i;
                            break;
                        }
                    case "CONSIDER":
                        {
                            ConsiderColumn = i;
                            break;
                        }
                    case "TYPE":
                        {
                            TypeColumn = i;
                            break;
                        }
                    case "INTERESTRATE":
                        {
                            InterestRateColumn = i;
                            break;
                        }
                    case "BANKNAME":
                        {
                            BankNameColumn = i;
                            break;
                        }
                }
            }
        }

    
    
    
    
    
    
    
    
    }
}
