﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_AlertVerificationReportDataReader : EntityDataReader<Auto_An_AlertVerificationReport>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int MaxAgeAllowedColumn = -1;
        public int MaxAgeFoundColumn = -1;
        public int MaxAgeFlagColumn = -1;
        public int MinAgeAllowedColumn = -1;
        public int MinAgeFoundColumn = -1;
        public int MinAgeFlagColumn = -1;
        public int MinIncomeAllowedColumn = -1;
        public int MinIncomeFoundColumn = -1;
        public int MinIncomeFlagColumn = -1;
        public int LocationColumn = -1;
        public int LocationCosideredColumn = -1;
        public int NationalityColumn = -1;
        public int NationalityConsideredColumn = -1;
        public int ApplicantTypeColumn = -1;
        public int CarAgeAtTenorEndAllowedColumn = -1;
        public int CarAgeAtTenorEndFoundColumn = -1;
        public int CarAgeAtTenorEndFlagColumn = -1;
        public int SeatingCapacityAllowedColumn = -1;
        public int SeatingCapacityFoundColumn = -1;
        public int SeatingCapacityFlagColumn = -1;
        public int CarVerificationColumn = -1;
        public int CarVerificationFlagColumn = -1;
        public int HypothecationCheckColumn = -1;
        public int HypothecationCheckFlagColumn = -1;
        public int VendorStatusColumn = -1;
        public int VendorFlagColumn = -1;
        public int LoanAmountAllowedColumn = -1;
        public int LoanAmountFoundColumn = -1;
        public int LoanAmountFlagColumn = -1;
        public int BBMaxLimitAllowedColumn = -1;
        public int BBMaxLimitFoundColumn = -1;
        public int BBMaxLimitFlagColumn = -1;
        public int InterestRateAllowedColumn = -1;
        public int InterestRateFoundColumn = -1;
        public int InterestRateFlagColumn = -1;
        public int TenorAllowedColumn = -1;
        public int TenorFoundColumn = -1;
        public int TenorFlagColumn = -1;
        public int ARTAEnrollmentAllowedColumn = -1;
        public int ARTAEnrollmentFoundColumn = -1;
        public int ARTAEnrollmentFlagColumn = -1;
        public int LTVIncBancaAllowedColumn = -1;
        public int LTVIncBancaFoundColumn = -1;
        public int LTVIncBancaFlagColumn = -1;
        public int MaxLTVSpreadAllowedColumn = -1;
        public int MaxLTVSpreadFoundColumn = -1;
        public int MaxLTVSpreadFlagColumn = -1;
        public int DBRAllowedColumn = -1;
        public int DBRFoundColumn = -1;
        public int DBRFlagColumn = -1;
        public int TotalAutoLoanAllowedColumn = -1;
        public int TotalAutoLoanFoundColumn = -1;
        public int TotalAutoLoanFlagColumn = -1;
        public int ApproversDLAAllowedColumn = -1;
        public int ApproversDLAFoundColumn = -1;
        public int ApproversDLAFlagColumn = -1;
        public int CIBOptainedColumn = -1;
        public int CIBOptainedFlagColumn = -1;
        public int JtMaxAgeAllowedColumn = -1;
        public int JtMaxAgeFoundColumn = -1;
        public int JtMaxAgeFlagColumn = -1;
        public int JtMinAgeAllowedColumn = -1;
        public int JtMinAgeFoundColumn = -1;
        public int JtMinAgeFlagColumn = -1;
        public int JtMinIncomeAllowedColumn = -1;
        public int JtMinIncomeFoundColumn = -1;
        public int JtMinIncomeFlagColumn = -1;


        public Auto_An_AlertVerificationReportDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_AlertVerificationReport Read()
        {
            var objAuto_An_AlertVerificationReport = new Auto_An_AlertVerificationReport();

            objAuto_An_AlertVerificationReport.ID = GetInt(IDColumn);
            objAuto_An_AlertVerificationReport.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_AlertVerificationReport.MaxAgeAllowed = GetString(MaxAgeAllowedColumn);
            objAuto_An_AlertVerificationReport.MaxAgeFound = GetString(MaxAgeFoundColumn);
            objAuto_An_AlertVerificationReport.MaxAgeFlag = GetString(MaxAgeFlagColumn);
            objAuto_An_AlertVerificationReport.MinAgeAllowed = GetString(MinAgeAllowedColumn);
            objAuto_An_AlertVerificationReport.MinAgeFound = GetString(MinAgeFoundColumn);
            objAuto_An_AlertVerificationReport.MinAgeFlag = GetString(MinAgeFlagColumn);
            objAuto_An_AlertVerificationReport.MinIncomeAllowed = GetString(MinIncomeAllowedColumn);
            objAuto_An_AlertVerificationReport.MinIncomeFound = GetString(MinIncomeFoundColumn);
            objAuto_An_AlertVerificationReport.MinIncomeFlag = GetString(MinIncomeFlagColumn);
            objAuto_An_AlertVerificationReport.Location = GetString(LocationColumn);
            objAuto_An_AlertVerificationReport.LocationCosidered = GetString(LocationCosideredColumn);
            objAuto_An_AlertVerificationReport.Nationality = GetString(NationalityColumn);
            objAuto_An_AlertVerificationReport.NationalityConsidered = GetString(NationalityConsideredColumn);
            objAuto_An_AlertVerificationReport.CarAgeAtTenorEndAllowed = GetString(CarAgeAtTenorEndAllowedColumn);
            objAuto_An_AlertVerificationReport.CarAgeAtTenorEndFound = GetString(CarAgeAtTenorEndFoundColumn);
            objAuto_An_AlertVerificationReport.CarAgeAtTenorEndFlag = GetString(CarAgeAtTenorEndFlagColumn);
            objAuto_An_AlertVerificationReport.SeatingCapacityAllowed = GetString(SeatingCapacityAllowedColumn);
            objAuto_An_AlertVerificationReport.SeatingCapacityFound = GetString(SeatingCapacityFoundColumn);
            objAuto_An_AlertVerificationReport.SeatingCapacityFlag = GetString(SeatingCapacityFlagColumn);
            objAuto_An_AlertVerificationReport.CarVerification = GetString(CarVerificationColumn);
            objAuto_An_AlertVerificationReport.CarVerificationFlag = GetString(CarVerificationFlagColumn);
            objAuto_An_AlertVerificationReport.HypothecationCheck = GetString(HypothecationCheckColumn);
            objAuto_An_AlertVerificationReport.HypothecationCheckFlag = GetString(HypothecationCheckFlagColumn);
            objAuto_An_AlertVerificationReport.VendorStatus = GetString(VendorStatusColumn);
            objAuto_An_AlertVerificationReport.VendorFlag = GetString(VendorFlagColumn);
            objAuto_An_AlertVerificationReport.LoanAmountAllowed = GetString(LoanAmountAllowedColumn);
            objAuto_An_AlertVerificationReport.LoanAmountFound = GetString(LoanAmountFoundColumn);
            objAuto_An_AlertVerificationReport.LoanAmountFlag = GetString(LoanAmountFlagColumn);
            objAuto_An_AlertVerificationReport.BBMaxLimitAllowed = GetString(BBMaxLimitAllowedColumn);
            objAuto_An_AlertVerificationReport.BBMaxLimitFound = GetString(BBMaxLimitFoundColumn);
            objAuto_An_AlertVerificationReport.BBMaxLimitFlag = GetString(BBMaxLimitFlagColumn);
            objAuto_An_AlertVerificationReport.InterestRateAllowed = GetString(InterestRateAllowedColumn);
            objAuto_An_AlertVerificationReport.InterestRateFound = GetString(InterestRateFoundColumn);
            objAuto_An_AlertVerificationReport.InterestRateFlag = GetString(InterestRateFlagColumn);
            objAuto_An_AlertVerificationReport.TenorAllowed = GetString(TenorAllowedColumn);
            objAuto_An_AlertVerificationReport.TenorFound = GetString(TenorFoundColumn);
            objAuto_An_AlertVerificationReport.TenorFlag = GetString(TenorFlagColumn);
            objAuto_An_AlertVerificationReport.ARTAEnrollmentAllowed = GetString(ARTAEnrollmentAllowedColumn);
            objAuto_An_AlertVerificationReport.ARTAEnrollmentFound = GetString(ARTAEnrollmentFoundColumn);
            objAuto_An_AlertVerificationReport.ARTAEnrollmentFlag = GetString(ARTAEnrollmentFlagColumn);
            objAuto_An_AlertVerificationReport.LTVIncBancaAllowed = GetString(LTVIncBancaAllowedColumn);
            objAuto_An_AlertVerificationReport.LTVIncBancaFound = GetString(LTVIncBancaFoundColumn);
            objAuto_An_AlertVerificationReport.LTVIncBancaFlag = GetString(LTVIncBancaFlagColumn);
            objAuto_An_AlertVerificationReport.MaxLTVSpreadAllowed = GetString(MaxLTVSpreadAllowedColumn);
            objAuto_An_AlertVerificationReport.MaxLTVSpreadFound = GetString(MaxLTVSpreadFoundColumn);
            objAuto_An_AlertVerificationReport.MaxLTVSpreadFlag = GetString(MaxLTVSpreadFlagColumn);
            objAuto_An_AlertVerificationReport.DBRAllowed = GetString(DBRAllowedColumn);
            objAuto_An_AlertVerificationReport.DBRFound = GetString(DBRFoundColumn);
            objAuto_An_AlertVerificationReport.DBRFlag = GetString(DBRFlagColumn);
            objAuto_An_AlertVerificationReport.TotalAutoLoanAllowed = GetString(TotalAutoLoanAllowedColumn);
            objAuto_An_AlertVerificationReport.TotalAutoLoanFound = GetString(TotalAutoLoanFoundColumn);
            objAuto_An_AlertVerificationReport.TotalAutoLoanFlag = GetString(TotalAutoLoanFlagColumn);
            objAuto_An_AlertVerificationReport.ApproversDLAAllowed = GetString(ApproversDLAAllowedColumn);
            objAuto_An_AlertVerificationReport.ApproversDLAFound = GetString(ApproversDLAFoundColumn);
            objAuto_An_AlertVerificationReport.ApproversDLAFlag = GetString(ApproversDLAFlagColumn);
            objAuto_An_AlertVerificationReport.CIBOptained = GetString(CIBOptainedColumn);
            objAuto_An_AlertVerificationReport.CIBOptainedFlag = GetString(CIBOptainedFlagColumn);

            objAuto_An_AlertVerificationReport.JtMaxAgeAllowed = GetString(JtMaxAgeAllowedColumn);
            objAuto_An_AlertVerificationReport.JtMaxAgeFound = GetString(JtMaxAgeFoundColumn);
            objAuto_An_AlertVerificationReport.JtMaxAgeFlag = GetString(JtMaxAgeFlagColumn);
            objAuto_An_AlertVerificationReport.JtMinAgeAllowed = GetString(JtMinAgeAllowedColumn);
            objAuto_An_AlertVerificationReport.JtMinAgeFound = GetString(JtMinAgeFoundColumn);
            objAuto_An_AlertVerificationReport.JtMinAgeFlag = GetString(JtMinAgeFlagColumn);
            objAuto_An_AlertVerificationReport.JtMinIncomeAllowed = GetString(JtMinIncomeAllowedColumn);
            objAuto_An_AlertVerificationReport.JtMinIncomeFound = GetString(JtMinIncomeFoundColumn);
            objAuto_An_AlertVerificationReport.JtMinIncomeFlag = GetString(JtMinIncomeFlagColumn);

            return objAuto_An_AlertVerificationReport;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "MAXAGEALLOWED": { MaxAgeAllowedColumn = i; break; }
                    case "MAXAGEFOUND": { MaxAgeFoundColumn = i; break; }
                    case "MAXAGEFLAG": { MaxAgeFlagColumn = i; break; }
                    case "MINAGEALLOWED": { MinAgeAllowedColumn = i; break; }
                    case "MINAGEFOUND": { MinAgeFoundColumn = i; break; }
                    case "MINAGEFLAG": { MinAgeFlagColumn = i; break; }
                    case "MININCOMEALLOWED": { MinIncomeAllowedColumn = i; break; }
                    case "MININCOMEFOUND": { MinIncomeFoundColumn = i; break; }
                    case "MININCOMEFLAG": { MinIncomeFlagColumn = i; break; }
                    case "LOCATION": { LocationColumn = i; break; }
                    case "LOCATIONCOSIDERED": { LocationCosideredColumn = i; break; }
                    case "NATIONALITY": { NationalityColumn = i; break; }
                    case "NATIONALITYCONSIDERED": { NationalityConsideredColumn = i; break; }
                    case "CARAGEATTENORENDALLOWED": { CarAgeAtTenorEndAllowedColumn = i; break; }
                    case "CARAGEATTENORENDFOUND": { CarAgeAtTenorEndFoundColumn = i; break; }
                    case "CARAGEATTENORENDFLAG": { CarAgeAtTenorEndFlagColumn = i; break; }
                    case "SEATINGCAPACITYALLOWED": { SeatingCapacityAllowedColumn = i; break; }
                    case "SEATINGCAPACITYFOUND": { SeatingCapacityFoundColumn = i; break; }
                    case "SEATINGCAPACITYFLAG": { SeatingCapacityFlagColumn = i; break; }
                    case "CARVERIFICATION": { CarVerificationColumn = i; break; }
                    case "CARVERIFICATIONFLAG": { CarVerificationFlagColumn = i; break; }
                    case "HYPOTHECATIONCHECK": { HypothecationCheckColumn = i; break; }
                    case "HYPOTHECATIONCHECKFLAG": { HypothecationCheckFlagColumn = i; break; }
                    case "VENDORSTATUS": { VendorStatusColumn = i; break; }
                    case "VENDORFLAG": { VendorFlagColumn = i; break; }
                    case "LOANAMOUNTALLOWED": { LoanAmountAllowedColumn = i; break; }
                    case "LOANAMOUNTFOUND": { LoanAmountFoundColumn = i; break; }
                    case "LOANAMOUNTFLAG": { LoanAmountFlagColumn = i; break; }
                    case "BBMAXLIMITALLOWED": { BBMaxLimitAllowedColumn = i; break; }
                    case "BBMAXLIMITFOUND": { BBMaxLimitFoundColumn = i; break; }
                    case "BBMAXLIMITFLAG": { BBMaxLimitFlagColumn = i; break; }
                    case "INTERESTRATEALLOWED": { InterestRateAllowedColumn = i; break; }
                    case "INTERESTRATEFOUND": { InterestRateFoundColumn = i; break; }
                    case "INTERESTRATEFLAG": { InterestRateFlagColumn = i; break; }
                    case "TENORALLOWED": { TenorAllowedColumn = i; break; }
                    case "TENORFOUND": { TenorFoundColumn = i; break; }
                    case "TENORFLAG": { TenorFlagColumn = i; break; }
                    case "ARTAENROLLMENTALLOWED": { ARTAEnrollmentAllowedColumn = i; break; }
                    case "ARTAENROLLMENTFOUND": { ARTAEnrollmentFoundColumn = i; break; }
                    case "ARTAENROLLMENTFLAG": { ARTAEnrollmentFlagColumn = i; break; }
                    case "LTVINCBANCAALLOWED": { LTVIncBancaAllowedColumn = i; break; }
                    case "LTVINCBANCAFOUND": { LTVIncBancaFoundColumn = i; break; }
                    case "LTVINCBANCAFLAG": { LTVIncBancaFlagColumn = i; break; }
                    case "MAXLTVSPREADALLOWED": { MaxLTVSpreadAllowedColumn = i; break; }
                    case "MAXLTVSPREADFOUND": { MaxLTVSpreadFoundColumn = i; break; }
                    case "MAXLTVSPREADFLAG": { MaxLTVSpreadFlagColumn = i; break; }
                    case "DBRALLOWED": { DBRAllowedColumn = i; break; }
                    case "DBRFOUND": { DBRFoundColumn = i; break; }
                    case "DBRFLAG": { DBRFlagColumn = i; break; }
                    case "TOTALAUTOLOANALLOWED": { TotalAutoLoanAllowedColumn = i; break; }
                    case "TOTALAUTOLOANFOUND": { TotalAutoLoanFoundColumn = i; break; }
                    case "TOTALAUTOLOANFLAG": { TotalAutoLoanFlagColumn = i; break; }
                    case "APPROVERSDLAALLOWED": { ApproversDLAAllowedColumn = i; break; }
                    case "APPROVERSDLAFOUND": { ApproversDLAFoundColumn = i; break; }
                    case "APPROVERSDLAFLAG": { ApproversDLAFlagColumn = i; break; }
                    case "CIBOPTAINED": { CIBOptainedColumn = i; break; }
                    case "CIBOPTAINEDFLAG": { CIBOptainedFlagColumn = i; break; }
                    case "JTMAXAGEALLOWED": { JtMaxAgeAllowedColumn = i; break; }
                    case "JTMAXAGEFOUND": { JtMaxAgeFoundColumn = i; break; }
                    case "JTMAXAGEFLAG": { JtMaxAgeFlagColumn = i; break; }
                    case "JTMINAGEALLOWED": { JtMinAgeAllowedColumn = i; break; }
                    case "JTMINAGEFOUND": { JtMinAgeFoundColumn = i; break; }
                    case "JTMINAGEFLAG": { JtMinAgeFlagColumn = i; break; }
                    case "JTMININCOMEALLOWED": { JtMinIncomeAllowedColumn = i; break; }
                    case "JTMININCOMEFOUND": { JtMinIncomeFoundColumn = i; break; }
                    case "JTMININCOMEFLAG": { JtMinIncomeFlagColumn = i; break; }
                }
            }
        }




    }
}
