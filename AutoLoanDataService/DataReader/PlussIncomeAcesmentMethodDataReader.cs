﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class PlussIncomeAcesmentMethodDataReader : EntityDataReader<PlussIncomeAcesmentMethod>
    {

        public int INAM_IDColumn = -1;
        public int INAM_METHODColumn = -1;
        public int INAM_METHODCODEColumn = -1;



        public PlussIncomeAcesmentMethodDataReader(IDataReader reader)
            : base(reader)
        { }


        public override PlussIncomeAcesmentMethod Read()
        {
            var objPlussIncomeAcesmentMethod = new PlussIncomeAcesmentMethod();
            objPlussIncomeAcesmentMethod.INAM_ID = GetInt(INAM_IDColumn);
            objPlussIncomeAcesmentMethod.INAM_METHOD = GetString(INAM_METHODColumn);
            objPlussIncomeAcesmentMethod.INAM_METHODCODE = GetString(INAM_METHODCODEColumn);

            return objPlussIncomeAcesmentMethod;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "INAM_ID":
                        {
                            INAM_IDColumn = i;
                            break;
                        }
                    case "INAM_METHOD":
                        {
                            INAM_METHODColumn = i;
                            break;
                        }
                    case "INAM_METHODCODE":
                        {
                            INAM_METHODCODEColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
