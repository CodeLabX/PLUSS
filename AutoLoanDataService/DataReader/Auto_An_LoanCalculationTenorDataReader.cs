﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_LoanCalculationTenorDataReader : EntityDataReader<Auto_An_LoanCalculationTenor>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int ARTAAllowedColumn = -1;
        public int VehicleAllowedColumn = -1;
        public int AgeAllowedColumn = -1;
        public int AskingTenorColumn = -1;
        public int AppropriateTenorColumn = -1;
        public int ConsideredTenorColumn = -1;
        public int ConsideredInMonthColumn = -1;
        public int LevelColumn = -1;


        public Auto_An_LoanCalculationTenorDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_LoanCalculationTenor Read()
        {
            var objAuto_An_LoanCalculationTenor = new Auto_An_LoanCalculationTenor();

            objAuto_An_LoanCalculationTenor.ID = GetInt(IDColumn);
            objAuto_An_LoanCalculationTenor.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_LoanCalculationTenor.ARTAAllowed = GetDouble(ARTAAllowedColumn);
            objAuto_An_LoanCalculationTenor.VehicleAllowed = GetDouble(VehicleAllowedColumn);
            objAuto_An_LoanCalculationTenor.AgeAllowed = GetDouble(AgeAllowedColumn);
            objAuto_An_LoanCalculationTenor.AskingTenor = GetDouble(AskingTenorColumn);
            objAuto_An_LoanCalculationTenor.AppropriateTenor = GetDouble(AppropriateTenorColumn);
            objAuto_An_LoanCalculationTenor.ConsideredTenor = GetDouble(ConsideredTenorColumn);
            objAuto_An_LoanCalculationTenor.ConsideredInMonth = GetDouble(ConsideredInMonthColumn);
            objAuto_An_LoanCalculationTenor.Level = GetInt(LevelColumn);

            return objAuto_An_LoanCalculationTenor;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "ARTAALLOWED":
                        {
                            ARTAAllowedColumn = i;
                            break;
                        }
                    case "VEHICLEALLOWED":
                        {
                            VehicleAllowedColumn = i;
                            break;
                        }
                    case "AGEALLOWED":
                        {
                            AgeAllowedColumn = i;
                            break;
                        }
                    case "ASKINGTENOR":
                        {
                            AskingTenorColumn = i;
                            break;
                        }
                    case "APPROPRIATETENOR":
                        {
                            AppropriateTenorColumn = i;
                            break;
                        }
                    case "CONSIDEREDTENOR":
                        {
                            ConsideredTenorColumn = i;
                            break;
                        }
                    case "CONSIDEREDINMONTH":
                        {
                            ConsideredInMonthColumn = i;
                            break;
                        }
                    case "LEVEL":
                        {
                            LevelColumn = i;
                            break;
                        }
                }
            }
        }




    }
}
