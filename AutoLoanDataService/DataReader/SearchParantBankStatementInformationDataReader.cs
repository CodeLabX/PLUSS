﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class SearchParantBankStatementInformationDataReader : EntityDataReader<SearchParantBankStatementInformation>
    {
        public int Auto_LoanMasterIdColumn = -1;
        public int LLIDColumn = -1;
        public int PrNameColumn = -1;
        public int PrDateOfBirthColumn = -1;
        public int ResidenceAddressColumn = -1;
        public int PrProfessionColumn = -1;
        public int JtNameColumn = -1;
        public int JtDateOfBirthColumn = -1;
        public int JtprofessionColumn = -1;
        public int JointApplicationColumn = -1;
        public int PrIncomeAmountColumn = -1;
        public int JtIncomeAmountColumn = -1;

        public SearchParantBankStatementInformationDataReader(IDataReader reader)
            : base(reader)
        { }


        public override SearchParantBankStatementInformation Read()
        {
            var objSearchParantBankStatementInformation = new SearchParantBankStatementInformation();

            objSearchParantBankStatementInformation.Auto_LoanMasterId = GetInt(Auto_LoanMasterIdColumn);
            objSearchParantBankStatementInformation.LLID = GetString(LLIDColumn);
            objSearchParantBankStatementInformation.PrName = GetString(PrNameColumn);
            objSearchParantBankStatementInformation.PrDateOfBirth = GetDate(PrDateOfBirthColumn);
            objSearchParantBankStatementInformation.ResidenceAddress = GetString(ResidenceAddressColumn);
            objSearchParantBankStatementInformation.PrProfession = GetString(PrProfessionColumn);
            objSearchParantBankStatementInformation.JtName = GetString(JtNameColumn);
            objSearchParantBankStatementInformation.JtDateOfBirth = GetDate(JtDateOfBirthColumn);
            objSearchParantBankStatementInformation.Jtprofession = GetString(JtprofessionColumn);
            objSearchParantBankStatementInformation.JointApplication = GetInt(JointApplicationColumn);
            objSearchParantBankStatementInformation.PrIncomeAmount = GetDouble(PrIncomeAmountColumn);
            objSearchParantBankStatementInformation.JtIncomeAmount = GetDouble(JtIncomeAmountColumn);

            return objSearchParantBankStatementInformation;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTO_LOANMASTERID":
                        {
                            Auto_LoanMasterIdColumn = i;
                            break;
                        }
                    case "LLID":
                        {
                            LLIDColumn = i;
                            break;
                        }
                    case "PRNAME":
                        {
                            PrNameColumn = i;
                            break;
                        }
                    case "PRDATEOFBIRTH":
                        {
                            PrDateOfBirthColumn = i;
                            break;
                        }
                    case "RESIDENCEADDRESS":
                        {
                            ResidenceAddressColumn = i;
                            break;
                        }
                    case "PRPROFESSION":
                        {
                            PrProfessionColumn = i;
                            break;
                        }
                    case "JTNAME":
                        {
                            JtNameColumn = i;
                            break;
                        }
                    case "JTDATEOFBIRTH":
                        {
                            JtDateOfBirthColumn = i;
                            break;
                        }
                    case "JTPROFESSION":
                        {
                            JtprofessionColumn = i;
                            break;
                        }
                    case "JOINTAPPLICATION":
                        {
                            JointApplicationColumn = i;
                            break;
                        }
                    case "PRINCOMEAMOUNT":
                        {
                            PrIncomeAmountColumn = i;
                            break;
                        }
                    case "JTINCOMEAMOUNT":
                        {
                            JtIncomeAmountColumn = i;
                            break;
                        }


                }
            }
        }


    }
}
