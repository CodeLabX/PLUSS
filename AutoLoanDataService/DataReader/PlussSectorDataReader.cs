﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;


namespace AutoLoanDataService.DataReader
{
    internal class PlussSectorDataReader : EntityDataReader<PlussSector>
    {
        public int SECT_IDColumn = -1;
        public int SECT_NAMEColumn = -1;
        public int SECT_STATUSColumn = -1;


        public PlussSectorDataReader(IDataReader reader)
            : base(reader)
        { }


        public override PlussSector Read()
        {
            var objPlussSector = new PlussSector();
            objPlussSector.SECT_ID = GetInt(SECT_IDColumn);
            objPlussSector.SECT_NAME = GetString(SECT_NAMEColumn);
            objPlussSector.SECT_STATUS = GetInt(SECT_STATUSColumn);

            return objPlussSector;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "SECT_ID":
                        {
                            SECT_IDColumn = i;
                            break;
                        }
                    case "SECT_NAME":
                        {
                            SECT_NAMEColumn = i;
                            break;
                        }
                    case "SECT_STATUS":
                        {
                            SECT_STATUSColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
