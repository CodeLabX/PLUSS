﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoVehicleStatusIRDataReader : EntityDataReader<AutoVehicleStatusIR>
    {
        public int VehicleStatusIRIdColumn = -1;
        public int VehicleStatusColumn = -1;
        public int IrWithArtaColumn = -1;
        public int IrWithoutArtaColumn = -1;
        public int LastUpdateDateColumn = -1;
        public int StatusNameColumn = -1;
        public int TotalCountColumn = -1;



        public AutoVehicleStatusIRDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoVehicleStatusIR Read()
        {
            var objAutoVehicleStatusIR = new AutoVehicleStatusIR();
            objAutoVehicleStatusIR.VehicleStatusIRId = GetInt(VehicleStatusIRIdColumn);
            objAutoVehicleStatusIR.VehicleStatus = GetInt(VehicleStatusColumn);
            objAutoVehicleStatusIR.IrWithArta = GetDouble(IrWithArtaColumn);
            objAutoVehicleStatusIR.IrWithoutArta = GetDouble(IrWithoutArtaColumn);
            objAutoVehicleStatusIR.LastUpdateDate = GetDate(LastUpdateDateColumn);
            objAutoVehicleStatusIR.StatusName = GetString(StatusNameColumn);

            objAutoVehicleStatusIR.TotalCount = Convert.ToInt32(reader.GetValue(TotalCountColumn));
            return objAutoVehicleStatusIR;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "VEHICLESTATUSIRID":
                        {
                            VehicleStatusIRIdColumn = i;
                            break;
                        }
                    case "VEHICLESTATUS":
                        {
                            VehicleStatusColumn = i;
                            break;
                        }
                    case "IRWITHARTA":
                        {
                            IrWithArtaColumn = i;
                            break;
                        }
                    case "IRWITHOUTARTA":
                        {
                            IrWithoutArtaColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        } 
                }
            }
        }
    }
}
