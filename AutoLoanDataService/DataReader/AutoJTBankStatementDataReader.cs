﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoJTBankStatementDataReader : EntityDataReader<AutoJTBankStatement>
    {
        public int JtBankStatementIdColumn = -1;
        public int LLIDColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int BankIdColumn = -1;
        public int BranchIdColumn = -1;
        public int AccountNameColumn = -1;
        public int AccountNumberColumn = -1;
        public int AccountCategoryColumn = -1;
        public int AccountTypeColumn = -1;
        public int StatementColumn = -1;
        public int LastUpdateDateColumn = -1;
        public int StatementDateFromColumn = -1;
        public int StatementDateToColumn = -1;
        public int BANK_NMColumn = -1;
        public int BRAN_NAMEColumn = -1;


        public AutoJTBankStatementDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoJTBankStatement Read()
        {
            var objAutoJTBankStatement = new AutoJTBankStatement();
            objAutoJTBankStatement.JtBankStatementId = GetInt(JtBankStatementIdColumn);
            objAutoJTBankStatement.LLID = GetInt(LLIDColumn);
            objAutoJTBankStatement.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoJTBankStatement.BankId = GetInt(BankIdColumn);
            objAutoJTBankStatement.BranchId = GetInt(BranchIdColumn);
            objAutoJTBankStatement.AccountName = GetString(AccountNameColumn);
            objAutoJTBankStatement.AccountNumber = GetString(AccountNumberColumn);
            objAutoJTBankStatement.AccountCategory = GetString(AccountCategoryColumn);
            objAutoJTBankStatement.AccountType = GetString(AccountTypeColumn);
            objAutoJTBankStatement.Statement = GetString(StatementColumn);
            objAutoJTBankStatement.LastUpdateDate = GetDate(LastUpdateDateColumn);
            objAutoJTBankStatement.StatementDateFrom = GetDate(StatementDateFromColumn);
            objAutoJTBankStatement.StatementDateTo = GetDate(StatementDateToColumn);
            objAutoJTBankStatement.BANK_NM = GetString(BANK_NMColumn);
            objAutoJTBankStatement.BRAN_NAME = GetString(BRAN_NAMEColumn);

            return objAutoJTBankStatement;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "JTBANKSTATEMENTID":
                        {
                            JtBankStatementIdColumn = i;
                            break;
                        }
                    case "LLID":
                        {
                            LLIDColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "BANKID":
                        {
                            BankIdColumn = i;
                            break;
                        }
                    case "BRANCHID":
                        {
                            BranchIdColumn = i;
                            break;
                        }
                    case "ACCOUNTNAME":
                        {
                            AccountNameColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBER":
                        {
                            AccountNumberColumn = i;
                            break;
                        }
                    case "ACCOUNTCATEGORY":
                        {
                            AccountCategoryColumn = i;
                            break;
                        }
                    case "ACCOUNTTYPE":
                        {
                            AccountTypeColumn = i;
                            break;
                        }
                    case "STATEMENT":
                        {
                            StatementColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "STATEMENTDATEFROM":
                        {
                            StatementDateFromColumn = i;
                            break;
                        }
                    case "STATEMENTDATETO":
                        {
                            StatementDateToColumn = i;
                            break;
                        }
                    case "BANK_NM":
                        {
                            BANK_NMColumn = i;
                            break;
                        }
                    case "BRAN_NAME":
                        {
                            BRAN_NAMEColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
