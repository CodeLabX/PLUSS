﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    
   internal class Auto_VariableSalaryCalculationDataReader : EntityDataReader<Auto_VariableSalaryCalculation>
   {
       public int IdColumn = -1;
       public int AutoIncomeSalariedIdColumn = -1;
       public int MonthColumn = -1;
       public int AmountColumn = -1;
       public int SalaiedIDColumn = -1;


       public Auto_VariableSalaryCalculationDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override Auto_VariableSalaryCalculation Read()
       {
           var objAutoVariableSalaryCalculation= new Auto_VariableSalaryCalculation();
           objAutoVariableSalaryCalculation.Id = GetInt(IdColumn);
           objAutoVariableSalaryCalculation.SalaiedID = GetInt(SalaiedIDColumn);
           if (objAutoVariableSalaryCalculation.SalaiedID == int.MinValue) {
               objAutoVariableSalaryCalculation.SalaiedID = 0;
           }
           objAutoVariableSalaryCalculation.Month = GetInt(MonthColumn);
           objAutoVariableSalaryCalculation.Amount = GetDouble(AmountColumn);

           return objAutoVariableSalaryCalculation;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "SALAIEDID":
                       {
                           SalaiedIDColumn = i;
                           break;
                       }
                   case "MONTH":
                       {
                           MonthColumn = i;
                           break;
                       }
                   case "AMOUNT":
                       {
                           AmountColumn = i;
                           break;
                       }


               }
           }
       }
   }
}
