﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoBrandTenorLTVDataReader : EntityDataReader<AutoBrandTenorLTV>
    {




          public int TanorLtvIDColumn = -1;
            public int ManufacturerIdColumn = -1;
            public int VehicleIdColumn = -1;
            public int StatusIdColumn = -1;
            public int TenorColumn = -1;
            public int LTVColumn = -1;
            public int UserIdColumn = -1;
            public int LastUpdateDateColumn = -1;
            public int ManufacturerNameColumn = -1;
            public int ManufacturCountryColumn = -1;
            public int ModelColumn = -1;
            public int StatusNameColumn = -1;
            public int TotalCountColumn = -1;
        public int ValuePackAllowedColumn = -1;

        public int TrimLevelColumn = -1;
        public int VehicleTypeColumn = -1;




       public AutoBrandTenorLTVDataReader(IDataReader reader)
            :base(reader)
        {
        }

        public override AutoBrandTenorLTV  Read()
        {
            var objAutoBrandTraLTV = new AutoBrandTenorLTV();
            objAutoBrandTraLTV.TanorLtvID = GetInt(TanorLtvIDColumn);
            objAutoBrandTraLTV.ManufacturerId = GetInt(ManufacturerIdColumn);
            objAutoBrandTraLTV.VehicleId= GetInt(VehicleIdColumn);
            objAutoBrandTraLTV.StatusId = GetInt(StatusIdColumn);
            objAutoBrandTraLTV.Tenor = GetDouble(TenorColumn);
            objAutoBrandTraLTV.LTV = GetDouble(LTVColumn);
            objAutoBrandTraLTV.UserId = GetInt(UserIdColumn);
            objAutoBrandTraLTV.LastUpdateDate = GetDate(LastUpdateDateColumn);


            objAutoBrandTraLTV.ManufacturerName = GetString(ManufacturerNameColumn);
            objAutoBrandTraLTV.ManufacturCountry = GetString(ManufacturCountryColumn);
            objAutoBrandTraLTV.Model = GetString(ModelColumn);
            objAutoBrandTraLTV.StatusName = GetString(StatusNameColumn);
            objAutoBrandTraLTV.TotalCount = GetInt(TotalCountColumn);
            objAutoBrandTraLTV.ValuePackAllowed = GetInt(ValuePackAllowedColumn);
            objAutoBrandTraLTV.TrimLevel = GetString(TrimLevelColumn);
            objAutoBrandTraLTV.VehicleType = GetString(VehicleTypeColumn);

            return objAutoBrandTraLTV;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++ )
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "TANORLTVID":
                        {
                            TanorLtvIDColumn = i;
                            break;
                        }
                    case "MANUFACTURERID":
                        {
                            ManufacturerIdColumn = i;
                            break;
                        }
                    case "VEHICLEID":
                        {
                            VehicleIdColumn = i;
                            break;
                        }
                    case "STATUSID":
                        {
                            StatusIdColumn = i;
                            break;
                        }
                    case "TENOR":
                        {
                            TenorColumn = i;
                            break;
                        }
                    case "LTV":
                        {
                            LTVColumn = i;
                            break;

                        }
                    case "USERID":
                        {
                            UserIdColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "MODEL":
                        {
                            ModelColumn = i;
                            break;
                        }
                    case "MANUFACTURERNAME":
                        {
                            ManufacturerNameColumn = i;
                            break;
                        }
                    case "MANUFACTURCOUNTRY":
                        {
                            ManufacturCountryColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                    case "VALUEPACKALLOWED":
                        {
                            ValuePackAllowedColumn = i;
                            break;
                        }
                    case "TRIMLEVEL":
                        {
                            TrimLevelColumn = i;
                            break;
                        }
                    case "VEHICLETYPE":
                        {
                            VehicleTypeColumn = i;
                            break;
                        }
                   
                }
            }
        }

    }



}
