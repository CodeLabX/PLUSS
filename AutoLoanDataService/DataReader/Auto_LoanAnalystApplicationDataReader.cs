﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
namespace AutoLoanDataService.DataReader
{

   internal class Auto_LoanAnalystApplicationDataReader : EntityDataReader<Auto_LoanAnalystApplication>
   {
       public int IdColumn = -1;
       public int AnalystMasterIdColumn = -1;
       public int SourceColumn = -1;
       public int BranchIdColumn = -1;
       public int BranchCodeColumn = -1;
       public int ValuePackColumn = -1;
       public int BorrowingRelationShipColumn = -1;
       public int BorrowingRelationShipCodeColumn = -1;
       public int CustomerStatusColumn = -1;
       public int AskingRepaymentMethodColumn = -1;
       public int ARTAColumn = -1;
       public int ARTAStatusColumn = -1;
       public int GeneralInsuranceColumn = -1;
       public int GeneralInsurancePreferenceColumn = -1;
       public int CashSecured100PerColumn = -1;
       public int CCBundleColumn = -1;
       public int SecurityValueColumn = -1;
       public int BranchNameColumn = -1;
       public int ARTANameColumn = -1;


       public Auto_LoanAnalystApplicationDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override Auto_LoanAnalystApplication Read()
       {
           var objAuto_LoanAnalystApplication = new Auto_LoanAnalystApplication();
           objAuto_LoanAnalystApplication.Id = GetInt(IdColumn);
           objAuto_LoanAnalystApplication.AnalystMasterId = GetInt(AnalystMasterIdColumn);
           objAuto_LoanAnalystApplication.Source = GetInt(SourceColumn);
           objAuto_LoanAnalystApplication.BranchId = GetInt(BranchIdColumn);
           objAuto_LoanAnalystApplication.BranchCode = GetString(BranchCodeColumn);
           objAuto_LoanAnalystApplication.ValuePack = GetInt(ValuePackColumn);
           objAuto_LoanAnalystApplication.BorrowingRelationShip = GetString(BorrowingRelationShipColumn);
           objAuto_LoanAnalystApplication.BorrowingRelationShipCode = GetString(BorrowingRelationShipCodeColumn);
           objAuto_LoanAnalystApplication.CustomerStatus = GetInt(CustomerStatusColumn);
           objAuto_LoanAnalystApplication.AskingRepaymentMethod = GetInt(AskingRepaymentMethodColumn);
           objAuto_LoanAnalystApplication.ARTA = GetInt(ARTAColumn);
           objAuto_LoanAnalystApplication.ARTAStatus = GetInt(ARTAStatusColumn);
           objAuto_LoanAnalystApplication.GeneralInsurance = GetInt(GeneralInsuranceColumn);
           objAuto_LoanAnalystApplication.GeneralInsurancePreference = GetInt(GeneralInsurancePreferenceColumn);
           objAuto_LoanAnalystApplication.CashSecured100Per = GetBool(CashSecured100PerColumn);
           objAuto_LoanAnalystApplication.CCBundle = GetBool(CCBundleColumn);
           objAuto_LoanAnalystApplication.SecurityValue = GetDouble(SecurityValueColumn);
           objAuto_LoanAnalystApplication.BranchName = GetString(BranchNameColumn);
           objAuto_LoanAnalystApplication.ARTAName = GetString(ARTANameColumn);

           return objAuto_LoanAnalystApplication;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "ANALYSTMASTERID":
                       {
                           AnalystMasterIdColumn = i;
                           break;
                       }
                   case "SOURCE":
                       {
                           SourceColumn = i;
                           break;
                       }
                   case "BRANCHID":
                       {
                           BranchIdColumn = i;
                           break;
                       }
                   case "BRANCHCODE":
                       {
                           BranchCodeColumn = i;
                           break;
                       }
                   case "VALUEPACK":
                       {
                           ValuePackColumn = i;
                           break;
                       }
                   case "BORROWINGRELATIONSHIP":
                       {
                           BorrowingRelationShipColumn = i;
                           break;
                       }
                   case "BORROWINGRELATIONSHIPCODE":
                       {
                           BorrowingRelationShipCodeColumn = i;
                           break;
                       }
                   case "CUSTOMERSTATUS":
                       {
                           CustomerStatusColumn = i;
                           break;
                       }
                   case "ASKINGREPAYMENTMETHOD":
                       {
                           AskingRepaymentMethodColumn = i;
                           break;
                       }
                   case "ARTA":
                       {
                           ARTAColumn = i;
                           break;
                       }
                   case "ARTASTATUS":
                       {
                           ARTAStatusColumn = i;
                           break;
                       }
                   case "GENERALINSURANCE":
                       {
                           GeneralInsuranceColumn = i;
                           break;
                       }
                   case "GENERALINSURANCEPREFERENCE":
                       {
                           GeneralInsurancePreferenceColumn = i;
                           break;
                       }
                   case "CASHSECURED100PER":
                       {
                           CashSecured100PerColumn = i;
                           break;
                       }
                   case "CCBUNDLE":
                       {
                           CCBundleColumn = i;
                           break;
                       }
                   case "SECURITYVALUE":
                       {
                           SecurityValueColumn = i;
                           break;
                       }
                   case "APPSOURCENAME":
                       {
                           BranchNameColumn = i;
                           break;
                       }
                   case "ARTANAME":
                       {
                           ARTANameColumn = i;
                           break;
                       }

               }
           }
       }
   }
}
