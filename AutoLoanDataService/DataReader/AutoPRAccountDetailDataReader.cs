﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoPRAccountDetailDataReader : EntityDataReader<AutoPRAccountDetail>
    {
        public int PrAccountDetailsIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int BankIdColumn = -1;
        public int BranchIdColumn = -1;
        public int AccountNumberColumn = -1;
        public int AccountCategoryColumn = -1;
        public int AccountTypeColumn = -1;


        public AutoPRAccountDetailDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoPRAccountDetail Read()
        {
            var objAutoPRAccountDetail = new AutoPRAccountDetail();
            objAutoPRAccountDetail.PrAccountDetailsId = GetInt(PrAccountDetailsIdColumn);
            objAutoPRAccountDetail.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoPRAccountDetail.BankId = GetInt(BankIdColumn);
            objAutoPRAccountDetail.BranchId = GetInt(BranchIdColumn);
            objAutoPRAccountDetail.AccountNumber = GetString(AccountNumberColumn);
            objAutoPRAccountDetail.AccountCategory = GetString(AccountCategoryColumn);
            objAutoPRAccountDetail.AccountType = GetString(AccountTypeColumn);

            return objAutoPRAccountDetail;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "PRACCOUNTDETAILS":
                        {
                            PrAccountDetailsIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "BANKID":
                        {
                            BankIdColumn = i;
                            break;
                        }
                    case "BRANCHID":
                        {
                            BranchIdColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBER":
                        {
                            AccountNumberColumn = i;
                            break;
                        }
                    case "ACCOUNTCATEGORY":
                        {
                            AccountCategoryColumn = i;
                            break;
                        }
                    case "ACCOUNTTYPE":
                        {
                            AccountTypeColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
