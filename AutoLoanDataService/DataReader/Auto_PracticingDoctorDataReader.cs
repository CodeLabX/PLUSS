﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class Auto_PracticingDoctorDataReader : EntityDataReader<Auto_PracticingDoctor>
   {
       public int IdColumn = -1;
       public int AutoLoanMasterIdColumn = -1;
       public int BankStatementIdColumn = -1;
       public int TypeColumn = -1;       
       public int TotalChamberIncomeColumn = -1;
       public int OtherOTIncomeColumn = -1;
       public int TotalIncomeColumn = -1;
      public int IncomeSumIdColumn = -1;
       
      


       public Auto_PracticingDoctorDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override Auto_PracticingDoctor Read()
       {
           var objAuto_PracticingDoctor = new Auto_PracticingDoctor();
           objAuto_PracticingDoctor.Id = GetInt(IdColumn);
           objAuto_PracticingDoctor.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
           objAuto_PracticingDoctor.BankStatementId = GetInt(BankStatementIdColumn);
           objAuto_PracticingDoctor.Type = GetInt(TypeColumn);
           objAuto_PracticingDoctor.TotalChamberIncome = GetDouble(TotalChamberIncomeColumn);
           objAuto_PracticingDoctor.OtherOTIncome = GetDouble(OtherOTIncomeColumn);
           objAuto_PracticingDoctor.TotalIncome = GetDouble(TotalIncomeColumn);
           objAuto_PracticingDoctor.IncomeSumId = GetInt(IncomeSumIdColumn);

           return objAuto_PracticingDoctor;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "AUTOLOANMASTERID":
                       {
                           AutoLoanMasterIdColumn = i;
                           break;
                       }
                   case "BANKSTATEMENT":
                       {
                           BankStatementIdColumn = i;
                           break;
                       }
                   case "TYPE":
                       {
                           TypeColumn = i;
                           break;
                       }
                   
                   case "TOTALCHAMBERINCOME":
                       {
                           TotalChamberIncomeColumn = i;
                           break;
                       }
                   case "OTHEROTINCOME":
                       {
                           OtherOTIncomeColumn = i;
                           break;
                       }
                   case "TOTALINCOME":
                       {
                           TotalIncomeColumn = i;
                           break;
                       }
                   case "INCOMESUMID":
                       {
                           IncomeSumIdColumn = i;
                           break;
                       }
                  


               }
           }
       }
   }
    
}
