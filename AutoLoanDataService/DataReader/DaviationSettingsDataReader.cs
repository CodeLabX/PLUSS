﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;


namespace AutoLoanDataService.DataReader
{
    internal class DaviationSettingsDataReader : EntityDataReader<DaviationSettingsEntity>
    {
        public int DeviationIdColumn = -1;
        public int DeviationForColumn = -1;
        public int DeviationCodeColumn = -1;
        public int DescriptionColumn = -1;
        public int UserIdColumn = -1;
        public int LastUpdateDateColumn = -1;
        public int TotalCountColumn = -1;

        public DaviationSettingsDataReader(IDataReader reader)
            : base(reader)
        { }
        public override DaviationSettingsEntity Read()
        {
            var objDaviationSettings = new DaviationSettingsEntity();
            objDaviationSettings.DeviationId = GetInt(DeviationIdColumn);
            objDaviationSettings.DeviationFor = GetInt(DeviationForColumn);
            objDaviationSettings.DeviationCode = GetString(DeviationCodeColumn);
            objDaviationSettings.Description = GetString(DescriptionColumn);
            objDaviationSettings.UserId = GetInt(UserIdColumn);
            objDaviationSettings.LastUpdateDate = GetDate(LastUpdateDateColumn);

            objDaviationSettings.TotalCount = GetInt(TotalCountColumn);
            return objDaviationSettings;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "DEVIATIONID":
                        {
                            DeviationIdColumn = i;
                            break;
                        }
                    case "DEVIATIONFOR":
                        {
                            DeviationForColumn = i;
                            break;
                        }
                    case "DEVIATIONCODE":
                        {
                            DeviationCodeColumn = i;
                            break;
                        }
                    case "DESCRIPTION":
                        {
                            DescriptionColumn = i;
                            break;
                        }
                    case "USERID":
                        {
                            UserIdColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        } 
                }
            }
        }




    }
}
