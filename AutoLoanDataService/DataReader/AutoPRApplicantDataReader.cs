﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoPRApplicantDataReader : EntityDataReader<AutoPRApplicant>
    {
        public int PrApplicantIdColumn = -1;
        public int Auto_LoanMasterIdColumn = -1;
        public int PrNameColumn = -1;
        public int GenderColumn = -1;
        public int RelationShipNumberColumn = -1;

        public int TINNumberColumn = -1;
        public int FathersNameColumn = -1;
        public int MothersNameColumn = -1;
        public int DateofBirthColumn = -1;

        public int MaritalStatusColumn = -1;
        public int SpouseNameColumn = -1;
        public int SpouseProfessionColumn = -1;
        public int SpouseWorkAddressColumn = -1;

        public int SpouseContactNumberColumn = -1;
        public int RelationShipwithPrimaryColumn = -1;
        public int NationalityColumn = -1;
        public int IdentificationNumberTypeColumn = -1;
        public int IdentificationNumberColumn = -1;
        public int IdentificationDocumentColumn = -1;

        public int NumberofDependentsColumn = -1;
        public int HighestEducationColumn = -1;
        public int ResidenceAddressColumn = -1;
        public int MailingAddressColumn = -1;

        public int PermanentAddressColumn = -1;
        public int ResidenceStatusColumn = -1;
        public int OwnershipDocumentColumn = -1;
        public int ResidenceStatusYearColumn = -1;
        public int PhoneResidenceColumn = -1;

        public int MobileColumn = -1;
        public int EmailColumn = -1;
        public int DirectorshipwithanyBankColumn = -1;


        public AutoPRApplicantDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoPRApplicant Read()
        {
            var objAutoPRApplicant = new AutoPRApplicant();
            objAutoPRApplicant.PrApplicantId = GetInt(PrApplicantIdColumn);
            objAutoPRApplicant.Auto_LoanMasterId = GetInt(Auto_LoanMasterIdColumn);
            objAutoPRApplicant.PrName = GetString(PrNameColumn);
            objAutoPRApplicant.Gender = GetInt(GenderColumn);
            objAutoPRApplicant.RelationShipNumber = GetString(RelationShipNumberColumn);
            objAutoPRApplicant.TINNumber = GetString(TINNumberColumn);
            objAutoPRApplicant.FathersName = GetString(FathersNameColumn);
            objAutoPRApplicant.MothersName = GetString(MothersNameColumn);
            objAutoPRApplicant.DateofBirth = GetDate(DateofBirthColumn);
            objAutoPRApplicant.Age = (int) Math.Ceiling((DateTime.Now - objAutoPRApplicant.DateofBirth).TotalDays / 365.255);
            objAutoPRApplicant.MaritalStatus = GetString(MaritalStatusColumn);
            objAutoPRApplicant.SpouseName = GetString(SpouseNameColumn);
            objAutoPRApplicant.SpouseProfession = GetString(SpouseProfessionColumn);
            objAutoPRApplicant.SpouseWorkAddress = GetString(SpouseWorkAddressColumn);
            objAutoPRApplicant.SpouseContactNumber = GetString(SpouseContactNumberColumn);
            objAutoPRApplicant.RelationShipwithPrimary = GetString(RelationShipwithPrimaryColumn);
            objAutoPRApplicant.Nationality = GetString(NationalityColumn);
            objAutoPRApplicant.IdentificationNumberType = GetString(IdentificationNumberTypeColumn);
            objAutoPRApplicant.IdentificationNumber = GetString(IdentificationNumberColumn);
            objAutoPRApplicant.IdentificationDocument = GetString(IdentificationDocumentColumn);
            objAutoPRApplicant.NumberofDependents = GetString(NumberofDependentsColumn);
            objAutoPRApplicant.HighestEducation = GetString(HighestEducationColumn);
            objAutoPRApplicant.ResidenceAddress = GetString(ResidenceAddressColumn);
            objAutoPRApplicant.MailingAddress = GetString(MailingAddressColumn);
            objAutoPRApplicant.PermanentAddress = GetString(PermanentAddressColumn);
            objAutoPRApplicant.ResidenceStatus = GetString(ResidenceStatusColumn);
            objAutoPRApplicant.OwnershipDocument = GetString(OwnershipDocumentColumn);
            objAutoPRApplicant.ResidenceStatusYear = GetString(ResidenceStatusYearColumn);
            objAutoPRApplicant.PhoneResidence = GetString(PhoneResidenceColumn);
            objAutoPRApplicant.Mobile = GetString(MobileColumn);
            objAutoPRApplicant.Email = GetString(EmailColumn);
            objAutoPRApplicant.DirectorshipwithanyBank = GetBool(DirectorshipwithanyBankColumn);

            return objAutoPRApplicant;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "PRAPPLICANTID":
                        {
                            PrApplicantIdColumn = i;
                            break;
                        }
                    case "AUTO_LOANMASTERID":
                        {
                            Auto_LoanMasterIdColumn = i;
                            break;
                        }
                    case "PRNAME":
                        {
                            PrNameColumn = i;
                            break;
                        }
                    case "GENDER":
                        {
                            GenderColumn = i;
                            break;
                        }
                    case "RELATIONSHIPNUMBER":
                        {
                            RelationShipNumberColumn = i;
                            break;
                        }
                    case "TINNUMBER":
                        {
                            TINNumberColumn = i;
                            break;
                        }
                    case "FATHERSNAME":
                        {
                            FathersNameColumn = i;
                            break;
                        }
                    case "MOTHERSNAME":
                        {
                            MothersNameColumn = i;
                            break;
                        }
                    case "DATEOFBIRTH":
                        {
                            DateofBirthColumn = i;
                            break;
                        }
                    case "MARITALSTATUS":
                        {
                            MaritalStatusColumn = i;
                            break;
                        }
                    case "SPOUSENAME":
                        {
                            SpouseNameColumn = i;
                            break;
                        }
                    case "SPOUSEPROFESSION":
                        {
                            SpouseProfessionColumn = i;
                            break;
                        }
                    case "SPOUSEWORKADDRESS":
                        {
                            SpouseWorkAddressColumn = i;
                            break;
                        }
                    case "SPOUSECONTACTNUMBER":
                        {
                            SpouseContactNumberColumn = i;
                            break;
                        }
                    case "RELATIONSHIPWITHPRIMARY":
                        {
                            RelationShipwithPrimaryColumn = i;
                            break;
                        }
                    case "NATIONALITY":
                        {
                            NationalityColumn = i;
                            break;
                        }
                    case "IDENTIFICATIONNUMBERTYPE":
                        {
                            IdentificationNumberTypeColumn = i;
                            break;
                        }
                    case "IDENTIFICATIONNUMBER":
                        {
                            IdentificationNumberColumn = i;
                            break;
                        }
                    case "IDENTIFICATIONDOCUMENT":
                        {
                            IdentificationDocumentColumn = i;
                            break;
                        }
                    case "NUMBEROFDEPENDENTS":
                        {
                            NumberofDependentsColumn = i;
                            break;
                        }
                    case "HIGHESTEDUCATION":
                        {
                            HighestEducationColumn = i;
                            break;
                        }
                    case "RESIDENCEADDRESS":
                        {
                            ResidenceAddressColumn = i;
                            break;
                        }
                    case "MAILINGADDRESS":
                        {
                            MailingAddressColumn = i;
                            break;
                        }
                    case "PERMANENTADDRESS":
                        {
                            PermanentAddressColumn = i;
                            break;
                        }
                    case "RESIDENCESTATUS":
                        {
                            ResidenceStatusColumn = i;
                            break;
                        }
                    case "OWNERSHIPDOCUMENT":
                        {
                            OwnershipDocumentColumn = i;
                            break;
                        }
                    case "RESIDENCESTATUSYEAR":
                        {
                            ResidenceStatusYearColumn = i;
                            break;
                        }
                    case "PHONERESIDENCE":
                        {
                            PhoneResidenceColumn = i;
                            break;
                        }
                    case "MOBILE":
                        {
                            MobileColumn = i;
                            break;
                        }
                    case "EMAIL":
                        {
                            EmailColumn = i;
                            break;
                        }
                    case "DIRECTORSHIPWITHANYBANK":
                        {
                            DirectorshipwithanyBankColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
