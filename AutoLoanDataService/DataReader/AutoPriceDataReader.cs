﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoPriceDataReader:EntityDataReader<AutoPrice>
    {
        public int PriceIdColumn = -1;
        public int VehicleIdColumn = -1;
        public int StatusIdColumn = -1;
        public int ManufacturingYearColumn = -1;
        public int MinimumPriceColumn = -1;
        public int MaximumPriceColumn = -1;
        public int IsActiveColumn = -1;
        public int LastUpdateDateColumn = -1;

        public int StatusNameColumn = -1;

        public AutoPriceDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoPrice Read()
        {
            var objAutoPrice = new AutoPrice();
            objAutoPrice.PriceId = GetInt(PriceIdColumn);
            objAutoPrice.VehicleId = GetInt(VehicleIdColumn);
            objAutoPrice.StatusId = GetInt(StatusIdColumn);
            objAutoPrice.ManufacturingYear = GetInt(ManufacturingYearColumn);
            if (objAutoPrice.ManufacturingYear == int.MinValue) {
                objAutoPrice.ManufacturingYear = 0;
            }
            objAutoPrice.MinimumPrice = GetDouble(MinimumPriceColumn);
            if (objAutoPrice.MinimumPrice == double.MinValue) {
                objAutoPrice.MinimumPrice = 0;
            }
            objAutoPrice.MaximumPrice = GetDouble(MaximumPriceColumn);
            if (objAutoPrice.MaximumPrice == double.MinValue) {
                objAutoPrice.MaximumPrice = 0;
            }
            objAutoPrice.IsActive = GetInt(IsActiveColumn);
            objAutoPrice.LastUpdateDate = GetDate(LastUpdateDateColumn);
            objAutoPrice.StatusName = GetString(StatusNameColumn);
            return objAutoPrice;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "PRICEID":
                        {
                            PriceIdColumn = i;
                            break;
                        }
                    case "VEHICLEID":
                        {
                            VehicleIdColumn = i;
                            break;
                        }
                    case "STATUSID":
                        {
                            StatusIdColumn = i;
                            break;
                        }
                    case "MANUFACTURINGYEAR":
                        {
                            ManufacturingYearColumn = i;
                            break;
                        }
                    case "MINIMUMPRICE":
                        {
                            MinimumPriceColumn = i;
                            break;
                        }
                    case "MAXIMUMPRICE":
                        {
                            MaximumPriceColumn = i;
                            break;
                        }
                    case "ISACTIVE":
                        {
                            IsActiveColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                }
            }
        }
    }
}
