﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_LoanCalculationAmountDataReader : EntityDataReader<Auto_An_LoanCalculationAmount>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int LTVAllowedColumn = -1;
        public int IncomeAllowedColumn = -1;
        public int BB_CADAllowedColumn = -1;
        public int AskingLoanColumn = -1;
        public int ExistingFacilityAllowedColumn = -1;
        public int AppropriateLoanColumn = -1;
        public int ApprovedLoanColumn = -1;


        public Auto_An_LoanCalculationAmountDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_LoanCalculationAmount Read()
        {
            var objAuto_An_LoanCalculationAmount = new Auto_An_LoanCalculationAmount();

            objAuto_An_LoanCalculationAmount.ID = GetInt(IDColumn);
            objAuto_An_LoanCalculationAmount.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_LoanCalculationAmount.LTVAllowed = GetDouble(LTVAllowedColumn);
            objAuto_An_LoanCalculationAmount.IncomeAllowed = GetDouble(IncomeAllowedColumn);
            objAuto_An_LoanCalculationAmount.BB_CADAllowed = GetDouble(BB_CADAllowedColumn);
            objAuto_An_LoanCalculationAmount.AskingLoan = GetDouble(AskingLoanColumn);
            objAuto_An_LoanCalculationAmount.ExistingFacilityAllowed = GetDouble(ExistingFacilityAllowedColumn);
            objAuto_An_LoanCalculationAmount.AppropriateLoan = GetDouble(AppropriateLoanColumn);
            objAuto_An_LoanCalculationAmount.ApprovedLoan = GetDouble(ApprovedLoanColumn);

            return objAuto_An_LoanCalculationAmount;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "LTVALLOWED":
                        {
                            LTVAllowedColumn = i;
                            break;
                        }
                    case "INCOMEALLOWED":
                        {
                            IncomeAllowedColumn = i;
                            break;
                        }
                    case "BB_CADALLOWED":
                        {
                            BB_CADAllowedColumn = i;
                            break;
                        }
                    case "ASKINGLOAN":
                        {
                            AskingLoanColumn = i;
                            break;
                        }
                    case "EXISTINGFACILITYALLOWED":
                        {
                            ExistingFacilityAllowedColumn = i;
                            break;
                        }
                    case "APPROPRIATELOAN":
                        {
                            AppropriateLoanColumn = i;
                            break;
                        }
                    case "APPROVEDLOAN":
                        {
                            ApprovedLoanColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
