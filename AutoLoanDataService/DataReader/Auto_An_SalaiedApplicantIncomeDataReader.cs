﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_SalaiedApplicantIncomeDataReader : EntityDataReader<Auto_An_SalaiedApplicantIncome>
    {


        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int SalaryModeColumn = -1;
        public int EmployerColumn = -1;
        public int EmployerCategoryColumn = -1;
        public int IncomeAssessmentIDColumn = -1;
        public int IncomeAssessmentCodeColumn = -1;
        public int GrossSalaryColumn = -1;
        public int NetSalaryColumn = -1;
        public int VariableSalaryColumn = -1;
        public int VariableSalaryPercentColumn = -1;
        public int VariableSalaryAmountColumn = -1;
        public int CarAllowanceColumn = -1;
        public int CarAllowanceAmountColumn = -1;
        public int CarAllowancePercentColumn = -1;
        public int CarAllowanceCalculatedAmountColumn = -1;
        public int OwnHouseBenefitColumn = -1;
        public int TotalIncomeColumn = -1;
        public int ApplicantTypeColumn = -1;
        public int RemarkColumn = -1;



        public Auto_An_SalaiedApplicantIncomeDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_SalaiedApplicantIncome Read()
        {
            var objAuto_An_SalaiedApplicantIncome = new Auto_An_SalaiedApplicantIncome();

            objAuto_An_SalaiedApplicantIncome.ID = GetInt(IDColumn);
            objAuto_An_SalaiedApplicantIncome.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_SalaiedApplicantIncome.SalaryMode = GetInt(SalaryModeColumn);
            objAuto_An_SalaiedApplicantIncome.Employer = GetInt(EmployerColumn);
            objAuto_An_SalaiedApplicantIncome.EmployerCategory = GetString(EmployerCategoryColumn);
            objAuto_An_SalaiedApplicantIncome.IncomeAssessmentID = GetInt(IncomeAssessmentIDColumn);
            objAuto_An_SalaiedApplicantIncome.IncomeAssessmentCode = GetString(IncomeAssessmentCodeColumn);
            objAuto_An_SalaiedApplicantIncome.GrossSalary = GetDouble(GrossSalaryColumn);
            objAuto_An_SalaiedApplicantIncome.NetSalary = GetDouble(NetSalaryColumn);
            objAuto_An_SalaiedApplicantIncome.VariableSalary = GetInt(VariableSalaryColumn);
            objAuto_An_SalaiedApplicantIncome.VariableSalaryPercent = GetDouble(VariableSalaryPercentColumn);
            objAuto_An_SalaiedApplicantIncome.VariableSalaryAmount = GetDouble(VariableSalaryAmountColumn);
            objAuto_An_SalaiedApplicantIncome.CarAllowance = GetInt(CarAllowanceColumn);
            objAuto_An_SalaiedApplicantIncome.CarAllowanceAmount = GetDouble(CarAllowanceAmountColumn);
            objAuto_An_SalaiedApplicantIncome.CarAllowancePercent = GetDouble(CarAllowancePercentColumn);
            objAuto_An_SalaiedApplicantIncome.CarAllowanceCalculatedAmount = GetDouble(CarAllowanceCalculatedAmountColumn);
            objAuto_An_SalaiedApplicantIncome.OwnHouseBenefit = GetInt(OwnHouseBenefitColumn);
            objAuto_An_SalaiedApplicantIncome.TotalIncome = GetDouble(TotalIncomeColumn);
            objAuto_An_SalaiedApplicantIncome.ApplicantType = GetInt(ApplicantTypeColumn);
            objAuto_An_SalaiedApplicantIncome.Remark = GetString(RemarkColumn);

            return objAuto_An_SalaiedApplicantIncome;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "SALARYMODE": { SalaryModeColumn = i; break; }
                    case "EMPLOYER": { EmployerColumn = i; break; }
                    case "EMPLOYERCATEGORY": { EmployerCategoryColumn = i; break; }
                    case "INCOMEASSESSMENTID": { IncomeAssessmentIDColumn = i; break; }
                    case "INCOMEASSESSMENTCODE": { IncomeAssessmentCodeColumn = i; break; }
                    case "GROSSSALARY": { GrossSalaryColumn = i; break; }
                    case "NETSALARY": { NetSalaryColumn = i; break; }
                    case "VARIABLESALARY": { VariableSalaryColumn = i; break; }
                    case "VARIABLESALARYPERCENT": { VariableSalaryPercentColumn = i; break; }
                    case "VARIABLESALARYAMOUNT": { VariableSalaryAmountColumn = i; break; }
                    case "CARALLOWANCE": { CarAllowanceColumn = i; break; }
                    case "CARALLOWANCEAMOUNT": { CarAllowanceAmountColumn = i; break; }
                    case "CARALLOWANCEPERCENT": { CarAllowancePercentColumn = i; break; }
                    case "CARALLOWANCECALCULATEDAMOUNT": { CarAllowanceCalculatedAmountColumn = i; break; }
                    case "OWNHOUSEBENEFIT": { OwnHouseBenefitColumn = i; break; }
                    case "TOTALINCOME": { TotalIncomeColumn = i; break; }
                    case "APPLICANTTYPE": { ApplicantTypeColumn = i; break; }
                    case "REMARK": { RemarkColumn = i; break; }
                }
            }
        }

    }
}
