﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;


namespace AutoLoanDataService.DataReader
{
    internal class PlussSubSectorDataReader : EntityDataReader<PlussSubSector>
    {
        public int SUSE_IDColumn = -1;
        public int SUSE_NAMEColumn = -1;
        public int SUSE_SECTOR_IDColumn = -1;
        public int SUSE_CODEColumn = -1;
        public int SUSE_INTERESTRATEColumn = -1;
        public int SUSE_IRCODEColumn = -1;
        public int SUSE_COLORColumn = -1;
        public int SUSE_STATUSColumn = -1;


        public PlussSubSectorDataReader(IDataReader reader)
            : base(reader)
        { }


        public override PlussSubSector Read()
        {
            var objPlussSubSector = new PlussSubSector();
            objPlussSubSector.SUSE_ID = GetInt(SUSE_IDColumn);
            objPlussSubSector.SUSE_NAME = GetString(SUSE_NAMEColumn);
            objPlussSubSector.SUSE_SECTOR_ID = GetInt(SUSE_SECTOR_IDColumn);
            objPlussSubSector.SUSE_CODE = GetString(SUSE_CODEColumn);
            objPlussSubSector.SUSE_INTERESTRATE = GetString(SUSE_INTERESTRATEColumn);
            objPlussSubSector.SUSE_IRCODE = GetString(SUSE_IRCODEColumn);
            objPlussSubSector.SUSE_COLOR = GetString(SUSE_COLORColumn);
            objPlussSubSector.SUSE_STATUS = GetInt(SUSE_STATUSColumn);

            return objPlussSubSector;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "SUSE_ID":
                        {
                            SUSE_IDColumn = i;
                            break;
                        }
                    case "SUSE_NAME":
                        {
                            SUSE_NAMEColumn = i;
                            break;
                        }
                    case "SUSE_SECTOR_ID":
                        {
                            SUSE_SECTOR_IDColumn = i;
                            break;
                        }
                    case "SUSE_CODE":
                        {
                            SUSE_CODEColumn = i;
                            break;
                        }
                    case "SUSE_INTERESTRATE":
                        {
                            SUSE_INTERESTRATEColumn = i;
                            break;
                        }
                    case "SUSE_IRCODE":
                        {
                            SUSE_IRCODEColumn = i;
                            break;
                        }
                    case "SUSE_COLOR":
                        {
                            SUSE_COLORColumn = i;
                            break;
                        }
                    case "SUSE_STATUS":
                        {
                            SUSE_STATUSColumn = i;
                            break;
                        }
                }
            }
        }
    }
}
