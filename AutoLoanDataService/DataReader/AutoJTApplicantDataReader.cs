﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoJTApplicantDataReader : EntityDataReader<AutoJTApplicant>
    {
        public int JtApplicantIdColumn = -1;
        public int Auto_LoanMasterIdColumn = -1;
        public int JtNameColumn = -1;
        public int GenderColumn = -1;
        public int RelationShipNumberColumn = -1;

        public int TINNumberColumn = -1;
        public int FathersNameColumn = -1;
        public int MothersNameColumn = -1;
        public int DateofBirthColumn = -1;

        public int MaritalStatusColumn = -1;
        public int SpouseNameColumn = -1;
        public int SpouseProfessionColumn = -1;
        public int SpouseWorkAddressColumn = -1;

        public int SpouseContactNumberColumn = -1;
        public int RelationShipwithPrimaryColumn = -1;
        public int NationalityColumn = -1;
        public int IdentificationNumberTypeColumn = -1;
        public int IdentificationNumberColumn = -1;
        public int IdentificationDocumentColumn = -1;

        public int NumberofDependentsColumn = -1;
        public int HighestEducationColumn = -1;
        public int ResidenceAddressColumn = -1;
        public int MailingAddressColumn = -1;

        public int PermanentAddressColumn = -1;
        public int ResidenceStatusColumn = -1;
        public int OwnershipDocumentColumn = -1;
        public int ResidenceStatusYearColumn = -1;
        public int PhoneResidenceColumn = -1;

        public int MobileColumn = -1;
        public int EmailColumn = -1;
        public int DirectorshipwithanyBankColumn = -1;


        public AutoJTApplicantDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoJTApplicant Read()
        {
            var objAutoJTApplicant = new AutoJTApplicant();
            objAutoJTApplicant.JtApplicantId = GetInt(JtApplicantIdColumn);
            objAutoJTApplicant.Auto_LoanMasterId = GetInt(Auto_LoanMasterIdColumn);
            objAutoJTApplicant.JtName = GetString(JtNameColumn);
            objAutoJTApplicant.Gender = GetInt(GenderColumn);
            objAutoJTApplicant.RelationShipNumber = GetString(RelationShipNumberColumn);
            objAutoJTApplicant.TINNumber = GetString(TINNumberColumn);
            objAutoJTApplicant.FathersName = GetString(FathersNameColumn);
            objAutoJTApplicant.MothersName = GetString(MothersNameColumn);
            objAutoJTApplicant.DateofBirth = GetDate(DateofBirthColumn);
            objAutoJTApplicant.Age = (int)((DateTime.Now - objAutoJTApplicant.DateofBirth).TotalDays / 365.255);
            objAutoJTApplicant.MaritalStatus = GetString(MaritalStatusColumn);
            objAutoJTApplicant.SpouseName = GetString(SpouseNameColumn);
            objAutoJTApplicant.SpouseProfession = GetString(SpouseProfessionColumn);
            objAutoJTApplicant.SpouseWorkAddress = GetString(SpouseWorkAddressColumn);
            objAutoJTApplicant.SpouseContactNumber = GetString(SpouseContactNumberColumn);
            objAutoJTApplicant.RelationShipwithPrimary = GetString(RelationShipwithPrimaryColumn);
            objAutoJTApplicant.Nationality = GetString(NationalityColumn);
            objAutoJTApplicant.IdentificationNumberType = GetString(IdentificationNumberTypeColumn);
            objAutoJTApplicant.IdentificationNumber = GetString(IdentificationNumberColumn);
            objAutoJTApplicant.IdentificationDocument = GetString(IdentificationDocumentColumn);
            objAutoJTApplicant.NumberofDependents = GetString(NumberofDependentsColumn);
            objAutoJTApplicant.HighestEducation = GetString(HighestEducationColumn);
            objAutoJTApplicant.ResidenceAddress = GetString(ResidenceAddressColumn);
            objAutoJTApplicant.MailingAddress = GetString(MailingAddressColumn);
            objAutoJTApplicant.PermanentAddress = GetString(PermanentAddressColumn);
            objAutoJTApplicant.ResidenceStatus = GetString(ResidenceStatusColumn);
            objAutoJTApplicant.OwnershipDocument = GetString(OwnershipDocumentColumn);
            objAutoJTApplicant.ResidenceStatusYear = GetString(ResidenceStatusYearColumn);
            objAutoJTApplicant.PhoneResidence = GetString(PhoneResidenceColumn);
            objAutoJTApplicant.Mobile = GetString(MobileColumn);
            objAutoJTApplicant.Email = GetString(EmailColumn);
            objAutoJTApplicant.DirectorshipwithanyBank = GetBool(DirectorshipwithanyBankColumn);

            return objAutoJTApplicant;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "JTAPPLICANTID":
                        {
                            JtApplicantIdColumn = i;
                            break;
                        }
                    case "AUTO_LOANMASTERID":
                        {
                            Auto_LoanMasterIdColumn = i;
                            break;
                        }
                    case "JTNAME":
                        {
                            JtNameColumn = i;
                            break;
                        }
                    case "GENDER":
                        {
                            GenderColumn = i;
                            break;
                        }
                    case "RELATIONSHIPNUMBER":
                        {
                            RelationShipNumberColumn = i;
                            break;
                        }
                    case "TINNUMBER":
                        {
                            TINNumberColumn = i;
                            break;
                        }
                    case "FATHERSNAME":
                        {
                            FathersNameColumn = i;
                            break;
                        }
                    case "MOTHERSNAME":
                        {
                            MothersNameColumn = i;
                            break;
                        }
                    case "DATEOFBIRTH":
                        {
                            DateofBirthColumn = i;
                            break;
                        }
                    case "MARITALSTATUS":
                        {
                            MaritalStatusColumn = i;
                            break;
                        }
                    case "SPOUSENAME":
                        {
                            SpouseNameColumn = i;
                            break;
                        }
                    case "SPOUSEPROFESSION":
                        {
                            SpouseProfessionColumn = i;
                            break;
                        }
                    case "SPOUSEWORKADDRESS":
                        {
                            SpouseWorkAddressColumn = i;
                            break;
                        }
                    case "SPOUSECONTACTNUMBER":
                        {
                            SpouseContactNumberColumn = i;
                            break;
                        }
                    case "RELATIONSHIPWITHPRIMARY":
                        {
                            RelationShipwithPrimaryColumn = i;
                            break;
                        }
                    case "NATIONALITY":
                        {
                            NationalityColumn = i;
                            break;
                        }
                    case "IDENTIFICATIONNUMBERTYPE":
                        {
                            IdentificationNumberTypeColumn = i;
                            break;
                        }
                    case "IDENTIFICATIONNUMBER":
                        {
                            IdentificationNumberColumn = i;
                            break;
                        }
                    case "IDENTIFICATIONDOCUMENT":
                        {
                            IdentificationDocumentColumn = i;
                            break;
                        }
                    case "NUMBEROFDEPENDENTS":
                        {
                            NumberofDependentsColumn = i;
                            break;
                        }
                    case "HIGHESTEDUCATION":
                        {
                            HighestEducationColumn = i;
                            break;
                        }
                    case "RESIDENCEADDRESS":
                        {
                            ResidenceAddressColumn = i;
                            break;
                        }
                    case "MAILINGADDRESS":
                        {
                            MailingAddressColumn = i;
                            break;
                        }
                    case "PERMANENTADDRESS":
                        {
                            PermanentAddressColumn = i;
                            break;
                        }
                    case "RESIDENCESTATUS":
                        {
                            ResidenceStatusColumn = i;
                            break;
                        }
                    case "OWNERSHIPDOCUMENT":
                        {
                            OwnershipDocumentColumn = i;
                            break;
                        }
                    case "RESIDENCESTATUSYEAR":
                        {
                            ResidenceStatusYearColumn = i;
                            break;
                        }
                    case "PHONERESIDENCE":
                        {
                            PhoneResidenceColumn = i;
                            break;
                        }
                    case "MOBILE":
                        {
                            MobileColumn = i;
                            break;
                        }
                    case "EMAIL":
                        {
                            EmailColumn = i;
                            break;
                        }
                    case "DIRECTORSHIPWITHANYBANK":
                        {
                            DirectorshipwithanyBankColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
