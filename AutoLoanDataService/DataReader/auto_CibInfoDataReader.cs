﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Azolution.Common.SqlDataService;
using System.Data;
using AutoLoanService.Entity;

namespace AutoLoanDataService.DataReader
{
    internal class auto_CibInfoDataReader : EntityDataReader<auto_CibInfo>
    {
        public int CibInfoIdColumn = -1;
        public int CibInfoNameColumn = -1;
        public int CibInfoDescriptionColumn = -1;
        public int CibInfoCodeColumn = -1;

        public auto_CibInfoDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override auto_CibInfo Read()
        {
            var objauto_CibInfo = new auto_CibInfo();

            objauto_CibInfo.CibInfoId = GetInt(CibInfoIdColumn);
            objauto_CibInfo.CibInfoName = GetString(CibInfoNameColumn);
            objauto_CibInfo.CibInfoDescription = GetString(CibInfoDescriptionColumn);
            objauto_CibInfo.CibInfoCode = GetString(CibInfoCodeColumn);

            return objauto_CibInfo;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "CIBINFOID": { CibInfoIdColumn = i; break; }
                    case "CIBINFONAME": { CibInfoNameColumn = i; break; }
                    case "CIBINFODESCRIPTION": { CibInfoDescriptionColumn = i; break; }
                    case "CIBINFOCODE": { CibInfoCodeColumn = i; break; }
                }
            }
        }
    }
}
