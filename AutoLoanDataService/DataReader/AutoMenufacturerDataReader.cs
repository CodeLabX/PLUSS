﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
   internal class AutoMenufacturerDataReader : EntityDataReader<AutoMenufacturer>
    {

       public int ManufacturerIdColumn = -1;
       public int ManufacturerNameColumn = -1;
       public int ManufacturCountryColumn = -1;
       public int REMARKSColumn = -1;
       public int ManufacturerCodeColumn = -1;
       


        public AutoMenufacturerDataReader(IDataReader reader)
            :base(reader)
        {
        }

        public override AutoMenufacturer Read()
        {
            var objAutoManufac = new AutoMenufacturer();
            objAutoManufac.ManufacturerId = GetInt(ManufacturerIdColumn);
            objAutoManufac.ManufacturerName = GetString(ManufacturerNameColumn);
            objAutoManufac.ManufacturCountry = GetString(ManufacturCountryColumn);
            objAutoManufac.Remarks = GetString(REMARKSColumn);
            objAutoManufac.ManufacturerCode = GetString(ManufacturerCodeColumn);
            return objAutoManufac;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++ )
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "MANUFACTURERID":
                        {
                            ManufacturerIdColumn = i;
                            break;
                        }
                    case "MANUFACTURERNAME":
                        {
                            ManufacturerNameColumn = i;
                            break;
                        }
                    case "MANUFACTURCOUNTRY":
                        {
                            ManufacturCountryColumn = i;
                            break;
                        }
                    case "REMARKS":
                        {
                            REMARKSColumn = i;
                            break;
                        }
                    case "MANUFACTURERCODE":
                        {
                            ManufacturerCodeColumn = i;
                            break;
                        }
                   
                }
            }
        }

    }





}
