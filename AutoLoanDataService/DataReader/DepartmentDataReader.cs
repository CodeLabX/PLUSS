﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class DepartmentDataReader : EntityDataReader<Department_Entity>
    {

        public int UsLeIdColumn = -1;
        public int UsLeCodeColumn = -1;
        public int UsLeNameColumn = -1;

        public DepartmentDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Department_Entity Read()
        {
            var objDepartment = new Department_Entity();
            objDepartment.UsLeId = GetInt(UsLeIdColumn);
            objDepartment.UsLeCode = GetString(UsLeCodeColumn);
            objDepartment.UsLeName = GetString(UsLeNameColumn);
            return objDepartment;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "USLE_ID":
                        {
                            UsLeIdColumn = i;
                            break;
                        }

                    case "USLE_CODE":
                        {
                            UsLeCodeColumn = i;
                            break;
                        }
                    case "USLE_NAME":
                        {
                            UsLeNameColumn = i;
                            break;
                        }
                   
                }
            }
        }
    }
}
