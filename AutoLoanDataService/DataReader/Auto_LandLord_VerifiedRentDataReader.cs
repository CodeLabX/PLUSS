﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class Auto_LandLord_VerifiedRentDataReader : EntityDataReader<Auto_LandLord_VerifiedRent>
   {
       public int IdColumn = -1;
       public int AutoLandLordIdColumn = -1;
       public int PropertyColumn = -1;
       public int ShareColumn = -1;

       public Auto_LandLord_VerifiedRentDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override Auto_LandLord_VerifiedRent Read()
       {
           var objAuto_LandLord_VerifiedRent = new Auto_LandLord_VerifiedRent();
           objAuto_LandLord_VerifiedRent.Id = GetInt(IdColumn);
           objAuto_LandLord_VerifiedRent.AutoLandLordId = GetInt(AutoLandLordIdColumn);
           objAuto_LandLord_VerifiedRent.Property = GetDouble(PropertyColumn);
           objAuto_LandLord_VerifiedRent.Share = GetDouble(ShareColumn);

           return objAuto_LandLord_VerifiedRent;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "AUTOLANDLORDID":
                       {
                           AutoLandLordIdColumn = i;
                           break;
                       }
                   case "PROPERTY":
                       {
                           PropertyColumn = i;
                           break;
                       }
                   case "SHARE":
                       {
                           ShareColumn = i;
                           break;
                       }


               }
           }
       }
   }
    
}
