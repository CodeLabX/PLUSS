﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class BALetterDataReader : EntityDataReader<BALetter_Entity>
   {
       public int LlIdColumn = -1;
      public int AutoLoanMasterIdColumn = -1;
       public int AnalystMasterIdColumn = -1;
       public int AutoBranchIdColumn = -1;
       public int AutoBranchNameColumn = -1;
       public int AppliedDateColumn = -1;
       public int TotalLoanAmountColumn = -1;
       public int ProductIdColumn = -1;
       public int ProdNameColumn = -1;
       public int MaxEMIColumn = -1;
       public int ConsideredTenorColumn = -1;
       public int ManufacturerNameColumn = -1;
       public int ConsideredRateColumn = -1;
       public int ConsideredInMonthColumn = -1;
       public int ModelColumn = -1;
       public int HypothecationCheckColumn = -1;
       public int SecurityPDCColumn = -1;
       public int AccountNumberColumn = -1;
       public int SCBAccountColumn = -1;


       public BALetterDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override BALetter_Entity Read()
       {
           var objBALetter = new BALetter_Entity();
           objBALetter.LlId = GetInt(LlIdColumn);
           objBALetter.AnalystMasterId = GetInt(AnalystMasterIdColumn);
           objBALetter.AutoBranchId = GetInt(AutoBranchIdColumn);
           objBALetter.AutoBranchName = GetString(AutoBranchNameColumn);
           objBALetter.AppliedDate = GetDate(AppliedDateColumn);
           objBALetter.TotalLoanAmount = GetDouble(TotalLoanAmountColumn);
           objBALetter.ProductId = GetInt(ProductIdColumn);
           objBALetter.ProdName = GetString(ProdNameColumn);
           objBALetter.MaxEMI = GetDouble(MaxEMIColumn);
           if (objBALetter.MaxEMI == double.MinValue)
           {
               objBALetter.MaxEMI = 0;
           }
           objBALetter.ConsideredTenor = GetInt(ConsideredTenorColumn);
           if (objBALetter.ConsideredTenor == int.MinValue)
           {
               objBALetter.ConsideredTenor = 0;
           }
           objBALetter.ManufacturerName = GetString(ManufacturerNameColumn);
           objBALetter.ConsideredRate = GetDouble(ConsideredRateColumn);
           if (objBALetter.ConsideredRate == double.MinValue)
           {
               objBALetter.ConsideredRate = 0;
           }
           objBALetter.ConsideredInMonth = GetInt(ConsideredInMonthColumn);
           if (objBALetter.ConsideredInMonth == int.MinValue)
           {
               objBALetter.ConsideredInMonth = 0;
           }
           objBALetter.Model = GetString(ModelColumn);
           objBALetter.HypothecationCheck = GetString(HypothecationCheckColumn);
           objBALetter.SecurityPDC = GetInt(SecurityPDCColumn);
           if (objBALetter.SecurityPDC == int.MinValue)
           {
               objBALetter.SecurityPDC = 0;
           }
           objBALetter.AccountNumber = GetString(AccountNumberColumn);
           objBALetter.SCBAccount = GetString(SCBAccountColumn);
           objBALetter.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
           if (objBALetter.AutoLoanMasterId == int.MinValue)
           {
               objBALetter.AutoLoanMasterId = 0;
           }

           return objBALetter;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "LLID":
                       {
                           LlIdColumn = i;
                           break;
                       }
                   case "AUTOLOANMASTERID":
                       {
                           AutoLoanMasterIdColumn = i;
                           break;
                       }
                   case "ANALYSTMASTERID":
                       {
                           AnalystMasterIdColumn = i;
                           break;
                       }
                   case "AUTOBRANCHID":
                       {
                           AutoBranchIdColumn = i;
                           break;
                       }
                   case "AUTOBRANCHNAME":
                       {
                           AutoBranchNameColumn = i;
                           break;
                       }
                   case "APPLIEDDATE":
                       {
                           AppliedDateColumn = i;
                           break;
                       }
                   case "TOTALLOANAMOUNT":
                       {
                           TotalLoanAmountColumn = i;
                           break;
                       }
                   case "PRODUCTID":
                       {
                           ProductIdColumn = i;
                           break;
                       }
                   case "PROD_NAME":
                       {
                           ProdNameColumn = i;
                           break;
                       }
                   case "MAXEMI":
                       {
                           MaxEMIColumn = i;
                           break;
                       }
                   case "CONSIDEREDTENOR":
                       {
                           ConsideredTenorColumn = i;
                           break;
                       }
                   case "MANUFACTURERNAME":
                       {
                           ManufacturerNameColumn = i;
                           break;
                       }
                   case "CONSIDEREDRATE":
                       {
                           ConsideredRateColumn = i;
                           break;
                       }
                   case "CONSIDEREDINMONTH":
                       {
                           ConsideredInMonthColumn = i;
                           break;
                       }
                   case "MODEL":
                       {
                           ModelColumn = i;
                           break;
                       }
                   case "HYPOTHECATIONCHECK":
                       {
                           HypothecationCheckColumn = i;
                           break;
                       }
                   case "SECURITYPDC":
                       {
                           SecurityPDCColumn = i;
                           break;
                       }
                   case "ACCOUNTNUMBER":
                       {
                           AccountNumberColumn = i;
                           break;
                       }
                   case "SCBACCOUNT":
                       {
                           SCBAccountColumn = i;
                           break;
                       }


               }
           }
       }
   }
    
}
