﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoJTAccountDetailDataReader : EntityDataReader<AutoJTAccountDetail>
    {
        public int JtAccountDetailsColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int BankIdColumn = -1;
        public int BranchIdColumn = -1;
        public int AccountNumberColumn = -1;
        public int AccountCategoryColumn = -1;
        public int AccountTypeColumn = -1;


        public AutoJTAccountDetailDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoJTAccountDetail Read()
        {
            var objAutoJTAccountDetail = new AutoJTAccountDetail();
            objAutoJTAccountDetail.JtAccountDetailsId = GetInt(JtAccountDetailsColumn);
            objAutoJTAccountDetail.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoJTAccountDetail.BankId = GetInt(BankIdColumn);
            objAutoJTAccountDetail.BranchId = GetInt(BranchIdColumn);
            objAutoJTAccountDetail.AccountNumber = GetString(AccountNumberColumn);
            objAutoJTAccountDetail.AccountCategory = GetString(AccountCategoryColumn);
            objAutoJTAccountDetail.AccountType = GetString(AccountTypeColumn);

            return objAutoJTAccountDetail;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "JTACCOUNTDETAILS":
                        {
                            JtAccountDetailsColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "BANKID":
                        {
                            BankIdColumn = i;
                            break;
                        }
                    case "BRANCHID":
                        {
                            BranchIdColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBER":
                        {
                            AccountNumberColumn = i;
                            break;
                        }
                    case "ACCOUNTCATEGORY":
                        {
                            AccountCategoryColumn = i;
                            break;
                        }
                    case "ACCOUNTTYPE":
                        {
                            AccountTypeColumn = i;
                            break;
                        }
                }
            }
        }



    }
}
