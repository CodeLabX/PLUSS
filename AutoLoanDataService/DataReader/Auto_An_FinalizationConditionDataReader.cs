﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationConditionDataReader : EntityDataReader<Auto_An_FinalizationCondition>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int ConditionColumn = -1;


        public Auto_An_FinalizationConditionDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationCondition Read()
        {
            var objAuto_An_FinalizationCondition = new Auto_An_FinalizationCondition();

            objAuto_An_FinalizationCondition.ID = GetInt(IDColumn);
            objAuto_An_FinalizationCondition.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationCondition.Condition = GetString(ConditionColumn);

            return objAuto_An_FinalizationCondition;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "CONDITIONS": { ConditionColumn = i; break; }
                }
            }
        }

    }
}
