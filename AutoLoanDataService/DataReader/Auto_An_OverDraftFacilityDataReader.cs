﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_OverDraftFacilityDataReader : EntityDataReader<Auto_An_OverDraftFacility>
    {
        public int IDColumn = -1;
        public int BankStatementIdColumn = -1;
        public int BankIdColumn = -1;
        public int LimitAmount = -1;
        public int InterestColumn = -1;
        public int ConsiderColumn = -1;
        public int TypeColumn = -1;
        public int IncomeSumIdColumn = -1;



        public Auto_An_OverDraftFacilityDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_OverDraftFacility Read()
        {
            var objAuto_An_OverDraftFacility = new Auto_An_OverDraftFacility();

            objAuto_An_OverDraftFacility.ID = GetInt(IDColumn);
            objAuto_An_OverDraftFacility.BankStatementId = GetInt(BankStatementIdColumn);
            objAuto_An_OverDraftFacility.BankId = GetInt(BankIdColumn);
            objAuto_An_OverDraftFacility.LimitAmount = GetDouble(LimitAmount);
            objAuto_An_OverDraftFacility.Interest = GetDouble(InterestColumn);
            objAuto_An_OverDraftFacility.Consider = GetInt(ConsiderColumn);
            objAuto_An_OverDraftFacility.Type = GetInt(TypeColumn);
            objAuto_An_OverDraftFacility.IncomeSumId = GetInt(IncomeSumIdColumn);
            return objAuto_An_OverDraftFacility;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "BANKSTATEMENTID":
                        {
                            BankStatementIdColumn = i;
                            break;
                        }
                    case "BANKID":
                        {
                            BankIdColumn = i;
                            break;
                        }
                    case "LIMITAMOUNT":
                        {
                            LimitAmount = i;
                            break;
                        }
                    case "INTEREST":
                        {
                            InterestColumn = i;
                            break;
                        }
                    case "CONSIDER":
                        {
                            ConsiderColumn = i;
                            break;
                        }
                    case "TYPE":
                        {
                            TypeColumn = i;
                            break;
                        }
                    case "INCOMESUMID":
                        {
                            IncomeSumIdColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
