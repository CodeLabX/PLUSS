﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationVarificationReportDataReader : EntityDataReader<Auto_An_FinalizationVarificationReport>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int CPVReportColumn = -1;
        public int CarPriceVarificationReportColumn = -1;
        public int StatementAuthenticationColumn = -1;
        public int CreditReportColumn = -1;
        public int CarVerificationReportColumn = -1;
        public int UsedCarValuationReportColumn = -1;
        public int UsedCarDocsColumn = -1;
        public int CardRepaymentHistoryColumn = -1;


        public Auto_An_FinalizationVarificationReportDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationVarificationReport Read()
        {
            var objAuto_An_FinalizationVarificationReport = new Auto_An_FinalizationVarificationReport();

            objAuto_An_FinalizationVarificationReport.ID = GetInt(IDColumn);
            objAuto_An_FinalizationVarificationReport.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationVarificationReport.CPVReport = GetInt(CPVReportColumn);
            objAuto_An_FinalizationVarificationReport.CarPriceVarificationReport = GetInt(CarPriceVarificationReportColumn);
            objAuto_An_FinalizationVarificationReport.StatementAuthentication = GetInt(StatementAuthenticationColumn);
            objAuto_An_FinalizationVarificationReport.CreditReport = GetInt(CreditReportColumn);
            objAuto_An_FinalizationVarificationReport.CarVerificationReport = GetInt(CarVerificationReportColumn);
            objAuto_An_FinalizationVarificationReport.UsedCarValuationReport = GetInt(UsedCarValuationReportColumn);
            objAuto_An_FinalizationVarificationReport.UsedCarDocs = GetInt(UsedCarDocsColumn);
            objAuto_An_FinalizationVarificationReport.CardRepaymentHistory = GetInt(CardRepaymentHistoryColumn);

            return objAuto_An_FinalizationVarificationReport;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "CPVREPORT": { CPVReportColumn = i; break; }
                    case "CARPRICEVARIFICATIONREPORT": { CarPriceVarificationReportColumn = i; break; }
                    case "STATEMENTAUTHENTICATION": { StatementAuthenticationColumn = i; break; }
                    case "CREDITREPORT": { CreditReportColumn = i; break; }
                    case "CARVERIFICATIONREPORT": { CarVerificationReportColumn = i; break; }
                    case "USEDCARVALUATIONREPORT": { UsedCarValuationReportColumn = i; break; }
                    case "USEDCARDOCS": { UsedCarDocsColumn = i; break; }
                    case "CARDREPAYMENTHISTORY": { CardRepaymentHistoryColumn = i; break; }
                }
            }
        }



    }
}
