﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoBranchWithSourceDataReader : EntityDataReader<AutoBranchWithSource>
    {
        public int AutoBranchIdColumn = -1;
        public int AutoBranchNameColumn = -1;
        public int AutoSourceIdColumn = -1;
        public int SourceNameColumn = -1;
        public int SourceCodeColumn = -1;
        public int TotalCountColumn = -1;


        public AutoBranchWithSourceDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoBranchWithSource Read()
        {
            var objAutoBranchWithSource = new AutoBranchWithSource();
            objAutoBranchWithSource.AutoBranchId = GetInt(AutoBranchIdColumn);
            objAutoBranchWithSource.AutoBranchName = GetString(AutoBranchNameColumn);
            objAutoBranchWithSource.AutoSourceId = GetInt(AutoSourceIdColumn);
            objAutoBranchWithSource.SourceName = GetString(SourceNameColumn);
            objAutoBranchWithSource.SourceCode = GetString(SourceCodeColumn);
            objAutoBranchWithSource.TotalCount = GetInt(TotalCountColumn);
            return objAutoBranchWithSource;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTOBRANCHID":
                        {
                            AutoBranchIdColumn = i;
                            break;
                        }
                    case "AUTOBRANCHNAME":
                        {
                            AutoBranchNameColumn = i;
                            break;
                        }
                    case "AUTOSOURCEID":
                        {
                            AutoSourceIdColumn = i;
                            break;
                        }
                    case "SOURCENAME":
                        {
                            SourceNameColumn = i;
                            break;
                        }
                    case "SOURCECODE":
                        {
                            SourceCodeColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                }
            }
        }
    }
}
