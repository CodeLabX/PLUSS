﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationReqDocDataReader : EntityDataReader<Auto_An_FinalizationReqDoc>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int LetterOfIntroductionColumn = -1;
        public int PayslipsColumn = -1;
        public int TradeLicenceColumn = -1;
        public int MOA_AOAColumn = -1;
        public int PartnershipDeedColumn = -1;
        public int FormXColumn = -1;
        public int BoardResolutionColumn = -1;
        public int DiplomaCertificateColumn = -1;
        public int ProfessionalCertificateColumn = -1;
        public int MunicipalTaxReciptColumn = -1;
        public int TitleDeedColumn = -1;
        public int RentalDeedColumn = -1;
        public int UtilityBillColumn = -1;
        public int LoanOfferLatterColumn = -1;


        public Auto_An_FinalizationReqDocDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationReqDoc Read()
        {
            var objAuto_An_FinalizationReqDoc = new Auto_An_FinalizationReqDoc();

            objAuto_An_FinalizationReqDoc.ID = GetInt(IDColumn);
            objAuto_An_FinalizationReqDoc.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationReqDoc.LetterOfIntroduction = GetInt(LetterOfIntroductionColumn);
            objAuto_An_FinalizationReqDoc.Payslips = GetInt(PayslipsColumn);
            objAuto_An_FinalizationReqDoc.TradeLicence = GetInt(TradeLicenceColumn);
            objAuto_An_FinalizationReqDoc.MOA_AOA = GetInt(MOA_AOAColumn);
            objAuto_An_FinalizationReqDoc.PartnershipDeed = GetInt(PartnershipDeedColumn);
            objAuto_An_FinalizationReqDoc.FormX = GetInt(FormXColumn);
            objAuto_An_FinalizationReqDoc.BoardResolution = GetInt(BoardResolutionColumn);
            objAuto_An_FinalizationReqDoc.DiplomaCertificate = GetInt(DiplomaCertificateColumn);
            objAuto_An_FinalizationReqDoc.ProfessionalCertificate = GetInt(ProfessionalCertificateColumn);
            objAuto_An_FinalizationReqDoc.MunicipalTaxRecipt = GetInt(MunicipalTaxReciptColumn);
            objAuto_An_FinalizationReqDoc.TitleDeed = GetInt(TitleDeedColumn);
            objAuto_An_FinalizationReqDoc.RentalDeed = GetInt(RentalDeedColumn);
            objAuto_An_FinalizationReqDoc.UtilityBill = GetInt(UtilityBillColumn);
            objAuto_An_FinalizationReqDoc.LoanOfferLatter = GetInt(LoanOfferLatterColumn);

            return objAuto_An_FinalizationReqDoc;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "LETTEROFINTRODUCTION":
                        {
                            LetterOfIntroductionColumn = i;
                            break;
                        }
                    case "PAYSLIPS":
                        {
                            PayslipsColumn = i;
                            break;
                        }
                    case "TRADELICENCE":
                        {
                            TradeLicenceColumn = i;
                            break;
                        }
                    case "MOA_AOA":
                        {
                            MOA_AOAColumn = i;
                            break;
                        }
                    case "PARTNERSHIPDEED":
                        {
                            PartnershipDeedColumn = i;
                            break;
                        }
                    case "FORMX":
                        {
                            FormXColumn = i;
                            break;
                        }
                    case "BOARDRESOLUTION":
                        {
                            BoardResolutionColumn = i;
                            break;
                        }
                    case "DIPLOMACERTIFICATE":
                        {
                            DiplomaCertificateColumn = i;
                            break;
                        }
                    case "PROFESSIONALCERTIFICATE":
                        {
                            ProfessionalCertificateColumn = i;
                            break;
                        }
                    case "MUNICIPALTAXRECIPT":
                        {
                            MunicipalTaxReciptColumn = i;
                            break;
                        }
                    case "TITLEDEED":
                        {
                            TitleDeedColumn = i;
                            break;
                        }
                    case "RENTALDEED":
                        {
                            RentalDeedColumn = i;
                            break;
                        }
                    case "UTILITYBILL":
                        {
                            UtilityBillColumn = i;
                            break;
                        }
                    case "LOANOFFERLATTER":
                        {
                            LoanOfferLatterColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
