﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoPRAccountDataReader : EntityDataReader<AutoPRAccount>
    {
        public int PrAccountIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int AccountNumberForSCBColumn = -1;
        public int CreditCardNumberForSCBColumn = -1;


        public AutoPRAccountDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoPRAccount Read()
        {
            var objAutoPRAccount = new AutoPRAccount();
            objAutoPRAccount.PrAccountId = GetInt(PrAccountIdColumn);
            objAutoPRAccount.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoPRAccount.AccountNumberForSCB = GetString(AccountNumberForSCBColumn);
            objAutoPRAccount.CreditCardNumberForSCB = GetString(CreditCardNumberForSCBColumn);

            return objAutoPRAccount;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "PRACCOUNTID":
                        {
                            PrAccountIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBERFORSCB":
                        {
                            AccountNumberForSCBColumn = i;
                            break;
                        }
                    case "CREDITCARDNUMBERFORSCB":
                        {
                            CreditCardNumberForSCBColumn = i;
                            break;
                        }
                }
            }
        }



    }
}
