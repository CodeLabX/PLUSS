﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoUserTypeVsWorkflowDataReader : EntityDataReader<AutoUserTypeVsWorkflow>
    {
        public int IDColumn = -1;
        public int UserTypeIDColumn = -1;
        public int WorkFlowIDColumn = -1;
        public int StatusNameColumn = -1;


        public AutoUserTypeVsWorkflowDataReader(IDataReader reader)
            : base(reader)
        { }


        public override AutoUserTypeVsWorkflow Read()
        {
            var objAutoUserTypeVsWorkflow = new AutoUserTypeVsWorkflow();
            objAutoUserTypeVsWorkflow.ID = GetInt(IDColumn);
            objAutoUserTypeVsWorkflow.UserTypeID = GetInt(UserTypeIDColumn);
            objAutoUserTypeVsWorkflow.WorkFlowID = GetInt(WorkFlowIDColumn);
            objAutoUserTypeVsWorkflow.StatusName = GetString(StatusNameColumn);

            return objAutoUserTypeVsWorkflow;
        }


        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "USERTYPEID":
                        {
                            UserTypeIDColumn = i;
                            break;
                        }
                    case "WORKFLOWID":
                        {
                            WorkFlowIDColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                }
            }
        }




    }
}
