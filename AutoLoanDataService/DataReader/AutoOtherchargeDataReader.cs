﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoOtherchargeDataReader : EntityDataReader<AutoOthercharge>
    {
        public int OtherChargeIdColumn = -1;
        public int AccidentOrTheftColumn = -1;
        public int CommissionColumn = -1;
        public int VatColumn = -1;
        public int PassengerPerSeatingCapColumn = -1;
        public int StampChargeColumn = -1;
        public int AmountofDriverSeatColumn = -1;
        public int DriverSeatChargeColumn = -1;


        public AutoOtherchargeDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoOthercharge Read()
        {
            var objAutoOthercharge = new AutoOthercharge();
            objAutoOthercharge.OtherChargeId = GetInt(OtherChargeIdColumn);
            objAutoOthercharge.AccidentOrTheft = Convert.ToDouble(GetFloat(AccidentOrTheftColumn));
            objAutoOthercharge.Commission = Convert.ToDouble(GetFloat(CommissionColumn));
            objAutoOthercharge.Vat = Convert.ToDouble(GetFloat(VatColumn));
            objAutoOthercharge.PassengerPerSeatingCap = Convert.ToDouble(GetFloat(PassengerPerSeatingCapColumn));
            objAutoOthercharge.AmountofDriverSeat = Convert.ToDouble(GetFloat(AmountofDriverSeatColumn));
            objAutoOthercharge.StampCharge = Convert.ToDouble(GetFloat(StampChargeColumn));
            objAutoOthercharge.DriverSeatCharge = Convert.ToDouble(GetFloat(DriverSeatChargeColumn));
            if (objAutoOthercharge.DriverSeatCharge == double.MinValue) {
                objAutoOthercharge.DriverSeatCharge = 0;
            }

            return objAutoOthercharge;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "OTHERCHARGEID":
                        {
                            OtherChargeIdColumn = i;
                            break;
                        }
                    case "ACCIDENTORTHEFT":
                        {
                            AccidentOrTheftColumn = i;
                            break;
                        }
                    case "COMMISSION":
                        {
                            CommissionColumn = i;
                            break;
                        }
                    case "VAT":
                        {
                            VatColumn = i;
                            break;
                        }
                    case "PASSENGERPERSEATINGCAP":
                        {
                            PassengerPerSeatingCapColumn = i;
                            break;
                        }
                    case "STAMPCHARGE":
                        {
                            StampChargeColumn = i;
                            break;
                        }
                    case "AMOUNTOFDRIVERSEAT":
                        {
                            AmountofDriverSeatColumn = i;
                            break;
                        }
                    case "DRIVERSEATCHARGE":
                        {
                            DriverSeatChargeColumn = i;
                            break;
                        } 
                }
            }
        }
    }
}
