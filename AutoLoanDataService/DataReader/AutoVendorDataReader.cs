﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
namespace AutoLoanDataService.DataReader
{
    internal class AutoVendorDataReader : EntityDataReader<AutoVendor>
    {

         public int AutoVendorIdColumn = -1;
       public int VendorNameColumn = -1;
       public int DealerTypeColumn = -1;
       public int AddressColumn = -1;
        public int PhoneColumn = -1;
        public int WebsiteColumn = -1;
        public int VendorCodeColumn = -1;
        public int IrWithArtaColumn = -1;
        public int IrWithoutArtaColumn = -1;
        public int IsMouColumn = -1;



        public AutoVendorDataReader(IDataReader reader)
            :base(reader)
        {
        }

        public override AutoVendor Read()
        {
            var objAutoVendor = new AutoVendor();
            objAutoVendor.AutoVendorId = GetInt(AutoVendorIdColumn);
            objAutoVendor.VendorName = GetString(VendorNameColumn);
            objAutoVendor.DealerType = GetInt(DealerTypeColumn);
            objAutoVendor.Address = GetString(AddressColumn);
            objAutoVendor.Phone = GetString(PhoneColumn);
            objAutoVendor.Website = GetString(WebsiteColumn);
            objAutoVendor.VendorCode = GetString(VendorCodeColumn);

            objAutoVendor.IrWithArta = GetDecimal(IrWithArtaColumn);
            if (objAutoVendor.IrWithArta == decimal.MinValue)
            {
                objAutoVendor.IrWithArta = 0;
            }
            objAutoVendor.IrWithoutArta = GetDecimal(IrWithoutArtaColumn);
            if (objAutoVendor.IrWithoutArta == decimal.MinValue)
            {
                objAutoVendor.IrWithoutArta = 0;
            }
            objAutoVendor.IsMou = GetInt(IsMouColumn);

            return objAutoVendor;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++ )
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTOVENDORID":
                        {
                            AutoVendorIdColumn = i;
                            break;
                        }
                    case "VENDORNAME":
                        {
                            VendorNameColumn = i;
                            break;
                        }
                    case "DEALERTYPE":
                        {
                            DealerTypeColumn = i;
                            break;
                        }
                    case "ADDRESS":
                        {
                            AddressColumn = i;
                            break;
                        }
                    case "PHONE":
                        {
                            PhoneColumn = i;
                            break;
                        }
                    case "WEBSITE":
                        {
                            WebsiteColumn = i;
                            break;
                        }
                    case "VENDORCODE":
                        {
                            VendorCodeColumn = i;
                            break;
                        }
                    case "IRWITHARTA":
                        {
                            IrWithArtaColumn = i;
                            break;
                        }
                    case "IRWITHOUTARTA":
                        {
                            IrWithoutArtaColumn = i;
                            break;
                        }
                    case "ISMOU":
                        {
                            IsMouColumn = i;
                            break;
                        }
                   
                }
            }
        }


    }
}
