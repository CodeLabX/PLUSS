﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationRemarkDataReader : EntityDataReader<Auto_An_FinalizationRemark>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int RemarksColumn = -1;


        public Auto_An_FinalizationRemarkDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationRemark Read()
        {
            var objAuto_An_FinalizationRemark = new Auto_An_FinalizationRemark();

            objAuto_An_FinalizationRemark.ID = GetInt(IDColumn);
            objAuto_An_FinalizationRemark.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationRemark.Remarks = GetString(RemarksColumn);

            return objAuto_An_FinalizationRemark;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "REMARKS":
                        {
                            RemarksColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
