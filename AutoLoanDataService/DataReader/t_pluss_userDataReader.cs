﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class t_pluss_userDataReader : EntityDataReader<t_pluss_user>
    {
        public int USER_IDColumn = -1;
        public int USER_CODEColumn = -1;
        public int USER_NAMEColumn = -1;
        public int USER_DESIGNATIONColumn = -1;
        public int USER_EMAILADDRESSColumn = -1;
        public int USER_LEVELCODEColumn = -1;
        public int USER_PASSWORDColumn = -1;
        public int USER_CHANGDATEColumn = -1;
        public int USER_CHANGBYColumn = -1;
        public int USER_LEVELColumn = -1;
        public int USER_STATUSColumn = -1;
        public int USER_ENTRYDATEColumn = -1;
        public int USER_USER_IDColumn = -1;
        public int USER_LOGINSTATUSColumn = -1;
        public int AUTO_USER_LEVELCodeColumn = -1;

        //Approver Limit Settings

       



        


        public t_pluss_userDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override t_pluss_user Read()
        {
            var objt_pluss_user = new t_pluss_user();

            objt_pluss_user.USER_ID = GetInt(USER_IDColumn);
            objt_pluss_user.USER_CODE = GetString(USER_CODEColumn);
            objt_pluss_user.USER_NAME = GetString(USER_NAMEColumn);
            objt_pluss_user.USER_DESIGNATION = GetString(USER_DESIGNATIONColumn);
            objt_pluss_user.USER_EMAILADDRESS = GetString(USER_EMAILADDRESSColumn);
            objt_pluss_user.USER_LEVELCODE = GetString(USER_LEVELCODEColumn);
            objt_pluss_user.USER_PASSWORD = GetString(USER_PASSWORDColumn);
            objt_pluss_user.USER_CHANGDATE = GetDate(USER_CHANGDATEColumn);
            objt_pluss_user.USER_CHANGBY = GetInt(USER_CHANGBYColumn);
            objt_pluss_user.USER_LEVEL = GetInt(USER_LEVELColumn);
            objt_pluss_user.USER_STATUS = GetInt(USER_STATUSColumn);
            objt_pluss_user.USER_ENTRYDATE = GetDate(USER_ENTRYDATEColumn);
            objt_pluss_user.USER_USER_ID = GetInt(USER_USER_IDColumn);
            objt_pluss_user.USER_LOGINSTATUS = GetInt(USER_LOGINSTATUSColumn);
            objt_pluss_user.AUTO_USER_LEVEL = GetInt(AUTO_USER_LEVELCodeColumn);


            return objt_pluss_user;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "USER_ID": { USER_IDColumn = i; break; }
                    case "USER_CODE": { USER_CODEColumn = i; break; }
                    case "USER_NAME": { USER_NAMEColumn = i; break; }
                    case "USER_DESIGNATION": { USER_DESIGNATIONColumn = i; break; }
                    case "USER_EMAILADDRESS": { USER_EMAILADDRESSColumn = i; break; }
                    case "USER_LEVELCODE": { USER_LEVELCODEColumn = i; break; }
                    case "USER_PASSWORD": { USER_PASSWORDColumn = i; break; }
                    case "USER_CHANGDATE": { USER_CHANGDATEColumn = i; break; }
                    case "USER_CHANGBY": { USER_CHANGBYColumn = i; break; }
                    case "USER_LEVEL": { USER_LEVELColumn = i; break; }
                    case "USER_STATUS": { USER_STATUSColumn = i; break; }
                    case "USER_ENTRYDATE": { USER_ENTRYDATEColumn = i; break; }
                    case "USER_USER_ID": { USER_USER_IDColumn = i; break; }
                    case "USER_LOGINSTATUS": { USER_LOGINSTATUSColumn = i; break; }
                    case "AUTO_USER_LEVEL": { AUTO_USER_LEVELCodeColumn = i; break; }
                    
                }
            }
        }



    }
}
