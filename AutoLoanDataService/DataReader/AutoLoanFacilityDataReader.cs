﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoLoanFacilityDataReader : EntityDataReader<AutoLoanFacility>
    {
        public int FacilityIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int FacilityTypeColumn = -1;
        public int InterestRateColumn = -1;
        public int PresentBalanceColumn = -1;
        public int PresentEMIColumn = -1;
        public int PresentLimitColumn = -1;
        public int ProposedLimitColumn = -1;
        public int RepaymentAgrementColumn = -1;
        public int FacilityTypeNameColumn = -1;
        public int ConsiderColumn = -1;
        public int EMIPercentColumn = -1;
        public int EMIShareColumn = -1;
        public int StatusColumn = -1;

        public int NatureOfSecurityColumn = -1;
        public int FaceValueColumn = -1;
        public int XTVRateColumn = -1;
        public int FacilityFlagColumn = -1;//FACI_CODE


        public AutoLoanFacilityDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoLoanFacility Read()
        {
            var objAutoLoanFacility = new AutoLoanFacility();
            objAutoLoanFacility.FacilityId = GetInt(FacilityIdColumn);
            objAutoLoanFacility.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoLoanFacility.FacilityType = GetInt(FacilityTypeColumn);
            objAutoLoanFacility.InterestRate = GetDouble(InterestRateColumn);
            objAutoLoanFacility.PresentBalance = GetDouble(PresentBalanceColumn);
            objAutoLoanFacility.PresentEMI = GetDouble(PresentEMIColumn);
            objAutoLoanFacility.PresentLimit = GetDouble(PresentLimitColumn);
            objAutoLoanFacility.ProposedLimit = GetDouble(ProposedLimitColumn);
            objAutoLoanFacility.RepaymentAgrement = GetDouble(RepaymentAgrementColumn);
            objAutoLoanFacility.FacilityTypeName = GetString(FacilityTypeNameColumn);
            objAutoLoanFacility.Consider = GetInt(ConsiderColumn);
            objAutoLoanFacility.EMIPercent = GetDouble(EMIPercentColumn);
            objAutoLoanFacility.EMIShare = GetDouble(EMIShareColumn);
            objAutoLoanFacility.Status = GetInt(StatusColumn);

            objAutoLoanFacility.NatureOfSecurity = GetInt(NatureOfSecurityColumn);
            objAutoLoanFacility.FaceValue = GetDouble(FaceValueColumn);
            objAutoLoanFacility.XTVRate = GetInt(XTVRateColumn);
            objAutoLoanFacility.FacilityFlag = GetString(FacilityFlagColumn);

            return objAutoLoanFacility;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "FACILITYID":
                        {
                            FacilityIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "FACILITYTYPE":
                        {
                            FacilityTypeColumn = i;
                            break;
                        }
                    case "INTERESTRATE":
                        {
                            InterestRateColumn = i;
                            break;
                        }
                    case "PRESENTBALANCE":
                        {
                            PresentBalanceColumn = i;
                            break;
                        }
                    case "PRESENTEMI":
                        {
                            PresentEMIColumn = i;
                            break;
                        }
                    case "PRESENTLIMIT":
                        {
                            PresentLimitColumn = i;
                            break;
                        }
                    case "PROPOSEDLIMIT":
                        {
                            ProposedLimitColumn = i;
                            break;
                        }
                    case "REPAYMENTAGREMENT":
                        {
                            RepaymentAgrementColumn = i;
                            break;
                        }
                    case "FACI_NAME":
                        {
                            FacilityTypeNameColumn = i;
                            break;
                        }
                    case "CONSIDER":
                        {
                            ConsiderColumn = i;
                            break;
                        }
                    case "EMIPERCENT":
                        {
                            EMIPercentColumn = i;
                            break;
                        }
                    case "EMISHARE":
                        {
                            EMIShareColumn = i;
                            break;
                        }
                    case "STATUS":
                        {
                            StatusColumn = i;
                            break;
                        }

                    case "NATUREOFSECURITY":
                        {
                            NatureOfSecurityColumn = i;
                            break;
                        }
                    case "FACEVALUE":
                        {
                            FaceValueColumn = i;
                            break;
                        }
                    case "XTVRATE":
                        {
                            XTVRateColumn = i;
                            break;
                        }
                    case "FACI_CODE":
                        {
                            FacilityFlagColumn = i;
                            break;
                        }
                }
            }
        }




    }
}
