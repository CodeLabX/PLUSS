﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoJTProfessionDataReader : EntityDataReader<AutoJTProfession>
    {
        public int JtProfessionIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int PrimaryProfessionColumn = -1;
        public int OtherProfessionColumn = -1;
        public int MonthinCurrentProfessionColumn = -1;
        public int NameofCompanyColumn = -1;
        public int NameofOrganizationColumn = -1;
        public int DesignationColumn = -1;
        public int NatureOfBussinessColumn = -1;
        public int YearsInBussinessColumn = -1;
        public int OfficeStatusColumn = -1;
        public int AddressColumn = -1;
        public int OfficePhoneColumn = -1;
        public int OwnerShipTypeColumn = -1;
        public int EmploymentStatusColumn = -1;
        public int TotalProfessionalExperienceColumn = -1;
        public int NumberOfEmployeesColumn = -1;
        public int EquityShareColumn = -1;
        public int JtIncomeSourceColumn = -1;
        public int OtherIncomeSourceColumn = -1;
        public int NoOfFloorsRentedColumn = -1;
        public int NatureOfrentedFloorsColumn = -1;
        public int RentedAresInSFTColumn = -1;
        public int ConstructionCompletingYearColumn = -1;
        public int JtPrimaryProfessionNameColumn = -1;
        public int JtOtherProfessionNameColumn = -1;

        public AutoJTProfessionDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoJTProfession Read()
        {
            var objAutoJTProfession = new AutoJTProfession();
            objAutoJTProfession.JtProfessionId = GetInt(JtProfessionIdColumn);
            objAutoJTProfession.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoJTProfession.PrimaryProfession = GetInt(PrimaryProfessionColumn);
            objAutoJTProfession.OtherProfession = GetInt(OtherProfessionColumn);
            objAutoJTProfession.MonthinCurrentProfession = GetString(MonthinCurrentProfessionColumn);
            objAutoJTProfession.NameofCompany = GetInt(NameofCompanyColumn);
            objAutoJTProfession.NameofOrganization = GetString(NameofOrganizationColumn);
            objAutoJTProfession.Designation = GetString(DesignationColumn);
            objAutoJTProfession.NatureOfBussiness = GetString(NatureOfBussinessColumn);
            objAutoJTProfession.YearsInBussiness = GetString(YearsInBussinessColumn);
            objAutoJTProfession.OfficeStatus = GetString(OfficeStatusColumn);
            objAutoJTProfession.Address = GetString(AddressColumn);
            objAutoJTProfession.OfficePhone = GetString(OfficePhoneColumn);
            objAutoJTProfession.OwnerShipType = GetString(OwnerShipTypeColumn);
            objAutoJTProfession.EmploymentStatus = GetString(EmploymentStatusColumn);
            objAutoJTProfession.TotalProfessionalExperience = GetString(TotalProfessionalExperienceColumn);
            objAutoJTProfession.NumberOfEmployees = GetInt(NumberOfEmployeesColumn);
            objAutoJTProfession.EquityShare = GetDouble(EquityShareColumn);
            objAutoJTProfession.JtIncomeSource = GetString(JtIncomeSourceColumn);
            objAutoJTProfession.OtherIncomeSource = GetString(OtherIncomeSourceColumn);
            objAutoJTProfession.NoOfFloorsRented = GetInt(NoOfFloorsRentedColumn);
            objAutoJTProfession.NatureOfrentedFloors = GetString(NatureOfrentedFloorsColumn);
            objAutoJTProfession.RentedAresInSFT = GetString(RentedAresInSFTColumn);
            objAutoJTProfession.ConstructionCompletingYear = GetString(ConstructionCompletingYearColumn);
            objAutoJTProfession.JtPrimaryProfessionName = GetString(JtPrimaryProfessionNameColumn);
            objAutoJTProfession.JtOtherProfessionName = GetString(JtOtherProfessionNameColumn);
            return objAutoJTProfession;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "JTPROFESSIONID":
                        {
                            JtProfessionIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "PRIMARYPROFESSION":
                        {
                            PrimaryProfessionColumn = i;
                            break;
                        }
                    case "OTHERPROFESSION":
                        {
                            OtherProfessionColumn = i;
                            break;
                        }
                    case "MONTHINCURRENTPROFESSION":
                        {
                            MonthinCurrentProfessionColumn = i;
                            break;
                        }
                    case "NAMEOFCOMPANY":
                        {
                            NameofCompanyColumn = i;
                            break;
                        }
                    case "NAMEOFORGANIZATION":
                        {
                            NameofOrganizationColumn = i;
                            break;
                        }
                    case "DESIGNATION":
                        {
                            DesignationColumn = i;
                            break;
                        }
                    case "NATUREOFBUSSINESS":
                        {
                            NatureOfBussinessColumn = i;
                            break;
                        }
                    case "YEARSINBUSSINESS":
                        {
                            YearsInBussinessColumn = i;
                            break;
                        }
                    case "OFFICESTATUS":
                        {
                            OfficeStatusColumn = i;
                            break;
                        }
                    case "ADDRESS":
                        {
                            AddressColumn = i;
                            break;
                        }
                    case "OFFICEPHONE":
                        {
                            OfficePhoneColumn = i;
                            break;
                        }
                    case "OWNERSHIPTYPE":
                        {
                            OwnerShipTypeColumn = i;
                            break;
                        }
                    case "EMPLOYMENTSTATUS":
                        {
                            EmploymentStatusColumn = i;
                            break;
                        }
                    case "TOTALPROFESSIONALEXPERIENCE":
                        {
                            TotalProfessionalExperienceColumn = i;
                            break;
                        }
                    case "NUMBEROFEMPLOYEES":
                        {
                            NumberOfEmployeesColumn = i;
                            break;
                        }
                    case "EQUITYSHARE":
                        {
                            EquityShareColumn = i;
                            break;
                        }
                    case "JTINCOMESOURCE":
                        {
                            JtIncomeSourceColumn = i;
                            break;
                        }
                    case "OTHERINCOMESOURCE":
                        {
                            OtherIncomeSourceColumn = i;
                            break;
                        }
                    case "NOOFFLOORSRENTED":
                        {
                            NoOfFloorsRentedColumn = i;
                            break;
                        }
                    case "NATUREOFRENTEDFLOORS":
                        {
                            NatureOfrentedFloorsColumn = i;
                            break;
                        }
                    case "RENTEDARESINSFT":
                        {
                            RentedAresInSFTColumn = i;
                            break;
                        }
                    case "CONSTRUCTIONCOMPLETINGYEAR":
                        {
                            ConstructionCompletingYearColumn = i;
                            break;
                        }
                    case "JTPRIMARYPROFESSIONNAME":
                        {
                            JtPrimaryProfessionNameColumn = i;
                            break;
                        }
                    case "JTOTHERPROFESSIONNAME":
                        {
                            JtOtherProfessionNameColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
