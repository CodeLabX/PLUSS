﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationReworkDataReader : EntityDataReader<Auto_An_FinalizationRework>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int ReworkDoneColumn = -1;
        public int ReworkReasonColumn = -1;
        public int LastApprovalDateColumn = -1;
        public int ReworkDateColumn = -1;
        public int ReworkCountColumn = -1;
        public int LastApprovalAmountColumn = -1;


        public Auto_An_FinalizationReworkDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationRework Read()
        {
            var objAuto_An_FinalizationRework = new Auto_An_FinalizationRework();

            objAuto_An_FinalizationRework.ID = GetInt(IDColumn);
            objAuto_An_FinalizationRework.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationRework.ReworkDone = GetInt(ReworkDoneColumn);
            objAuto_An_FinalizationRework.ReworkReason = GetString(ReworkReasonColumn);
            objAuto_An_FinalizationRework.LastApprovalDate = GetString(LastApprovalDateColumn);
            objAuto_An_FinalizationRework.ReworkDate = GetString(ReworkDateColumn);
            objAuto_An_FinalizationRework.ReworkCount = GetInt(ReworkCountColumn);
            objAuto_An_FinalizationRework.LastApprovalAmount = GetDouble(LastApprovalAmountColumn);

            return objAuto_An_FinalizationRework;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "REWORKDONE": { ReworkDoneColumn = i; break; }
                    case "REWORKREASON": { ReworkReasonColumn = i; break; }
                    case "LASTAPPROVALDATE": { LastApprovalDateColumn = i; break; }
                    case "REWORKDATE": { ReworkDateColumn = i; break; }
                    case "REWORKCOUNT": { ReworkCountColumn = i; break; }
                    case "LASTAPPROVALAMOUNT": { LastApprovalAmountColumn = i; break; }
                }
            }
        }







    }
}
