﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_RepaymentCapabilityDataReader : EntityDataReader<Auto_An_RepaymentCapability>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int DBRLevelColumn = -1;
        public int AppropriateDBRColumn = -1;
        public int ConsideredDBRColumn = -1;
        public int MaxEMICapabilityColumn = -1;
        public int ExistingEMIColumn = -1;
        public int CurrentEMICapabilityColumn = -1;
        public int BalanceSupportedColumn = -1;
        public int MaxEMIColumn = -1;


        public Auto_An_RepaymentCapabilityDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_RepaymentCapability Read()
        {
            var objAuto_An_RepaymentCapability = new Auto_An_RepaymentCapability();

            objAuto_An_RepaymentCapability.ID = GetInt(IDColumn);
            objAuto_An_RepaymentCapability.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_RepaymentCapability.DBRLevel = GetInt(DBRLevelColumn);
            objAuto_An_RepaymentCapability.AppropriateDBR = GetDouble(AppropriateDBRColumn);
            objAuto_An_RepaymentCapability.ConsideredDBR = GetDouble(ConsideredDBRColumn);
            objAuto_An_RepaymentCapability.MaxEMICapability = GetDouble(MaxEMICapabilityColumn);
            objAuto_An_RepaymentCapability.ExistingEMI = GetDouble(ExistingEMIColumn);
            objAuto_An_RepaymentCapability.CurrentEMICapability = GetDouble(CurrentEMICapabilityColumn);
            objAuto_An_RepaymentCapability.BalanceSupported = GetDouble(BalanceSupportedColumn);
            objAuto_An_RepaymentCapability.MaxEMI = GetDouble(MaxEMIColumn);

            return objAuto_An_RepaymentCapability;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {

                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "DBRLEVEL":
                        {
                            DBRLevelColumn = i;
                            break;
                        }
                    case "APPROPRIATEDBR":
                        {
                            AppropriateDBRColumn = i;
                            break;
                        }
                    case "CONSIDEREDDBR":
                        {
                            ConsideredDBRColumn = i;
                            break;
                        }
                    case "MAXEMICAPABILITY":
                        {
                            MaxEMICapabilityColumn = i;
                            break;
                        }
                    case "EXISTINGEMI":
                        {
                            ExistingEMIColumn = i;
                            break;
                        }
                    case "CURRENTEMICAPABILITY":
                        {
                            CurrentEMICapabilityColumn = i;
                            break;
                        }
                    case "BALANCESUPPORTED":
                        {
                            BalanceSupportedColumn = i;
                            break;
                        }
                    case "MAXEMI":
                        {
                            MaxEMIColumn = i;
                            break;
                        }
                }
            }
        }



    }
}
