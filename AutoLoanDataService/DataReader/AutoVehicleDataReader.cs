﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoVehicleDataReader : EntityDataReader<AutoVehicle>
    {

        public int VehicleIdColumn=-1;
        public int ManufacturerIdColumn=-1;
        public int ModelColumn = -1;
        public int TrimLevelColumn = -1;
        public int TypeColumn = -1;
        public int CCColumn=-1;
        public int ChassisCodeColumn = -1;
        public int ManufacturerNameColumn = -1;
        public int ManufacturCountryColumn = -1;
        public int REMARKSColumn = -1;
        public int TotalCountColumn = -1;
        public int ModelCodeColumn = -1;
        public int ManufacturerCodeColumn = -1;
        public int TenorColumn = -1;
        public int LtvColumn = -1;



        public AutoVehicleDataReader(IDataReader reader)
            :base(reader)
        {
        }

        public override AutoVehicle Read()
        {
            var objAutoVehicle = new AutoVehicle();
            objAutoVehicle.VehicleId = GetInt(VehicleIdColumn);
            objAutoVehicle.ManufacturerId = GetInt(ManufacturerIdColumn);
            objAutoVehicle.Model = GetString(ModelColumn);
            objAutoVehicle.TrimLevel = GetString(TrimLevelColumn);
            objAutoVehicle.Type = GetString(TypeColumn);
            objAutoVehicle.CC = GetInt(CCColumn);
            if (objAutoVehicle.CC == int.MinValue) {
                objAutoVehicle.CC = 0;
            }
            objAutoVehicle.ChassisCode = GetString(ChassisCodeColumn);
            objAutoVehicle.ManufacturerName = GetString(ManufacturerNameColumn);
            objAutoVehicle.ManufacturCountry = GetString(ManufacturCountryColumn);
            objAutoVehicle.Remarks = GetString(REMARKSColumn);
            objAutoVehicle.TotalCount = GetInt(TotalCountColumn);
            objAutoVehicle.ModelCode = GetString(ModelCodeColumn);
            objAutoVehicle.ManufacturerCode = GetString(ManufacturerCodeColumn);
            objAutoVehicle.Tenor = GetInt(TenorColumn);
            if (objAutoVehicle.Tenor == int.MinValue)
            {
                objAutoVehicle.Tenor = 0;
            }
            objAutoVehicle.Ltv = GetDouble(LtvColumn);
            return objAutoVehicle;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++ )
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "VEHICLEID":
                        
                        {
                            VehicleIdColumn = i;
                            break;
                        }
                    case "MANUFACTURERID":
                        {
                            ManufacturerIdColumn = i;
                            break;
                        }
                    case "MODEL":
                        {
                            ModelColumn = i;
                            break;
                        }
                    case "TRIMLEVEL":
                        {
                            TrimLevelColumn = i;
                            break;
                        }
                    case "TYPE":
                        {
                            TypeColumn = i;
                            break;
                        }
                    case "CC":
                        {
                            CCColumn = i;
                            break;
                        }
                    case "CHASSISCODE":
                        {
                           ChassisCodeColumn = i;
                            break;
                        }

                    case "MANUFACTURERNAME":
                        {
                            ManufacturerNameColumn = i;
                            break;
                        }
                    case "MANUFACTURCOUNTRY":
                        {
                            ManufacturCountryColumn = i;
                            break;
                        }
                    case "REMARKS":
                        {
                            REMARKSColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                    case "MODELCODE":
                        {
                            ModelCodeColumn = i;
                            break;
                        }
                    case "MANUFACTURERCODE":
                        {
                            ManufacturerCodeColumn = i;
                            break;
                        }
                    case "TENOR":
                        {
                            TenorColumn = i;
                            break;
                        }
                    case "LTV":
                        {
                            LtvColumn = i;
                            break;
                        }
                }
            }
        }
    }
}
