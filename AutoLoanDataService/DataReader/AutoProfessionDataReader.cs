﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;


namespace AutoLoanDataService.DataReader
{
    internal class AutoProfessionDataReader : EntityDataReader<AutoProfession>
    {
        public int ProfIDColumn = -1;
        public int ProfNameColumn = -1;

        public AutoProfessionDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoProfession Read()
        {
            var objAutoProfession = new AutoProfession();
            objAutoProfession.ProfID = GetInt(ProfIDColumn);
            objAutoProfession.ProfName = GetString(ProfNameColumn);

            return objAutoProfession;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "PROF_ID":
                        {
                            ProfIDColumn = i;
                            break;
                        }
                    case "PROF_NAME":
                        {
                            ProfNameColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
