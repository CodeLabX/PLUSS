﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
namespace AutoLoanDataService.DataReader
{

        internal class QueryTrackerDetailsDaraReader : EntityDataReader<QueryTrackerDetails_Entity>
        {
            public int LlIdColumn = -1;
            public int AutoLoanMasterIdColumn = -1;
            public int PrNameColumn = -1;
            public int AppliedAmountColumn = -1;
            public int AppliedDateColumn = -1;
            public int DateOfBirthColumn = -1;
            public int AskingTenorColumn = -1;
            public int TotalcountColumn = -1;
            public int AutoUserTypeIdColumn = -1;
            

           
            public QueryTrackerDetailsDaraReader(IDataReader reader)
                : base(reader)
            {
            }

            public override QueryTrackerDetails_Entity Read()
            {
                var objQueryTrackerDetails = new QueryTrackerDetails_Entity();
                objQueryTrackerDetails.LlId = GetInt(LlIdColumn);
                objQueryTrackerDetails.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
                objQueryTrackerDetails.PrName = GetString(PrNameColumn);
                objQueryTrackerDetails.AppliedAmount = GetInt(AppliedAmountColumn);
                objQueryTrackerDetails.AppliedDate = GetDate(AppliedDateColumn);
                objQueryTrackerDetails.DateOfBirth = GetDate(DateOfBirthColumn);
                objQueryTrackerDetails.AskingTenor = GetString(AskingTenorColumn);
                objQueryTrackerDetails.Totalcount = GetInt(TotalcountColumn);
                objQueryTrackerDetails.AutoUserTypeId = GetInt(AutoUserTypeIdColumn);
                return objQueryTrackerDetails;
            }

            protected override void ResolveOrdinal()
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    switch (reader.GetName(i).ToUpper())
                    {
                        case "LLID":
                            {
                                LlIdColumn = i;
                                break;
                            }

                        case "AUTOLOANMASTERID":
                            {
                                AutoLoanMasterIdColumn = i;
                                break;
                            }
                        case "PRNAME":
                            {
                                PrNameColumn = i;
                                break;
                            }

                        case "APPLIEDAMOUNT":
                            {
                                AppliedAmountColumn = i;
                                break;
                            }

                        case "APPLIEDDATE":
                            {
                                AppliedDateColumn = i;
                                break;
                            }
                        case "DATEOFBIRTH":
                            {
                                DateOfBirthColumn = i;
                                break;
                            }
                        case "ASKINGTENOR":
                            {
                                AskingTenorColumn = i;
                                break;
                            }
                        case "TOTALCOUNT":
                            {
                                TotalcountColumn = i;
                                break;
                            }
                        case "USLE_ID":
                            {
                                AutoUserTypeIdColumn = i;
                                break;
                            }
                            
                          
                    }
                }
            }
        }
    
}
