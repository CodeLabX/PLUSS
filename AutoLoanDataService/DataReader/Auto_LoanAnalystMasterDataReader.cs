﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    
    internal class Auto_LoanAnalystMasterDataReader : EntityDataReader<Auto_LoanAnalystMaster>
        {
            public int AnalystMasterIdColumn = -1;
            public int LlIdColumn = -1;
            public int AutoLoanMasterIdColumn = -1;
            public int IsJointColumn = -1;
            public int ReasonOfJointColumn = -1;
            public int AnalystRemarkColumn = -1;
            public int ApproverRemarkColumn = -1;
            public int ColleterizationColumn = -1;
            public int ColleterizationWithColumn = -1;
            public int CibInfoIdColumn = -1;

            public int ColleterizationNameColumn = -1;
            public int ColleterizationWithNameColumn = -1;

            public Auto_LoanAnalystMasterDataReader(IDataReader reader)
                : base(reader)
            {
            }

            public override Auto_LoanAnalystMaster Read()
            {
                var objAutoLoanAnalystMaster = new Auto_LoanAnalystMaster();
                objAutoLoanAnalystMaster.AnalystMasterId = GetInt(AnalystMasterIdColumn);
                objAutoLoanAnalystMaster.LlId = GetInt(LlIdColumn);
                objAutoLoanAnalystMaster.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
                objAutoLoanAnalystMaster.IsJoint = GetBool(IsJointColumn);
                objAutoLoanAnalystMaster.ReasonOfJoint = GetString(ReasonOfJointColumn);

                objAutoLoanAnalystMaster.AnalystRemark = GetString(AnalystRemarkColumn);
                objAutoLoanAnalystMaster.ApproverRemark = GetString(ApproverRemarkColumn);
                objAutoLoanAnalystMaster.Colleterization = GetInt(ColleterizationColumn);
                objAutoLoanAnalystMaster.ColleterizationWith = GetInt(ColleterizationWithColumn);
                objAutoLoanAnalystMaster.CibInfoId = GetInt(CibInfoIdColumn);

                objAutoLoanAnalystMaster.ColleterizationName = GetString(ColleterizationNameColumn);
                objAutoLoanAnalystMaster.ColleterizationWithName = GetString(ColleterizationWithNameColumn);

                return objAutoLoanAnalystMaster;
            }

            protected override void ResolveOrdinal()
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    switch (reader.GetName(i).ToUpper())
                    {
                        case "ANALYSTMASTERID":
                            {
                                AnalystMasterIdColumn = i;
                                break;
                            }
                        case "LLID":
                            {
                                LlIdColumn = i;
                                break;
                            }
                        case "AUTOLOANMASTERID":
                            {
                                AutoLoanMasterIdColumn = i;
                                break;
                            }
                        case "ISJOINT":
                            {
                                IsJointColumn = i;
                                break;
                            }
                        case "REASONOFJOINT":
                            {
                                ReasonOfJointColumn = i;
                                break;
                            }
                        case "ANALYSTREMARK":
                            {
                                AnalystRemarkColumn = i;
                                break;
                            }
                        case "APPROVERREMARK":
                            {
                                ApproverRemarkColumn = i;
                                break;
                            }
                        case "COLLETERIZATION":
                            {
                                ColleterizationColumn = i;
                                break;
                            }
                        case "COLLETERIZATIONWITH":
                            {
                                ColleterizationWithColumn = i;
                                break;
                            }
                        case "CIBINFOID":
                            {
                                CibInfoIdColumn = i;
                                break;
                            }
                        case "COLLETERIZATIONNAME":
                            {
                                ColleterizationNameColumn = i;
                                break;
                            }
                        case "COLLETERIZATIONWITHNAME":
                            {
                                ColleterizationWithNameColumn = i;
                                break;
                            }
                    }
                }
            }
        }
    
}
