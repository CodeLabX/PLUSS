﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class Auto_TutioningTeacherDataReader : EntityDataReader<Auto_TutioningTeacher>
   {
       public int IdColumn= -1;
        public int AutoLoanMasterIdColumn= -1;
        public int BankStatementIdColumn = -1;
        public int TypeColumn = -1;
        public int NoOfStudentColumn= -1;
        public int NoOfDaysForTutionColumn= -1;
        public int FeesColumn= -1;
      public int TotalIncomeColumn = -1;
      public int IncomeSumIdColumn = -1;


public Auto_TutioningTeacherDataReader(IDataReader reader)
           : base(reader)
       {
       }

public override Auto_TutioningTeacher Read()
       {
           var objAuto_TutioningTeacher = new Auto_TutioningTeacher();
           objAuto_TutioningTeacher.Id = GetInt(IdColumn);
           objAuto_TutioningTeacher.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
           objAuto_TutioningTeacher.BankStatementId = GetInt(BankStatementIdColumn);
           objAuto_TutioningTeacher.Type = GetInt(TypeColumn);
           objAuto_TutioningTeacher.NoOfStudent = GetInt(NoOfStudentColumn);
           objAuto_TutioningTeacher.NoOfDaysForTution = GetInt(NoOfDaysForTutionColumn);
           objAuto_TutioningTeacher.Fees = GetDouble(FeesColumn);
           objAuto_TutioningTeacher.TotalIncome = GetDouble(TotalIncomeColumn);
    objAuto_TutioningTeacher.IncomeSumId = GetInt(IncomeSumIdColumn);

           return objAuto_TutioningTeacher;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "AUTOLOANMASTERID":
                       {
                           AutoLoanMasterIdColumn = i;
                           break;
                       }
                   case "BANKSTATEMENTID":
                       {
                           BankStatementIdColumn = i;
                           break;
                       }
                   case "TYPE":
                       {
                           TypeColumn = i;
                           break;
                       }
                   
                   case "NOOFSTUDENT":
                       {
                           NoOfStudentColumn = i;
                           break;
                       }
                   case "NOOFDAYSFORTUTION":
                       {
                           NoOfDaysForTutionColumn = i;
                           break;
                       }
                   case "FEES":
                       {
                           FeesColumn = i;
                           break;
                       }
                   case "TOTALINCOME":
                       {
                           TotalIncomeColumn = i;
                           break;
                       }
                   case "INCOMESUMID":
                       {
                           IncomeSumIdColumn = i;
                           break;
                       }
                   
               }
           }
       }
   }
    
}
