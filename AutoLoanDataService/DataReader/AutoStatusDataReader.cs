﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoStatusDataReader : EntityDataReader<AutoStatus>
    {
        public int StatusIdColumn = -1;
        public int StatusNameColumn = -1;
        public int StatusTypeColumn = -1;
        public int StatusCodeColumn = -1;


        public AutoStatusDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoStatus Read()
        {
            var objAutoStatus = new AutoStatus();
            objAutoStatus.StatusId = GetInt(StatusIdColumn);
            objAutoStatus.StatusName = GetString(StatusNameColumn);
            objAutoStatus.StatusType = GetInt(StatusTypeColumn);
            objAutoStatus.StatusCode = GetString(StatusCodeColumn);

            return objAutoStatus;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "STATUSID":
                        {
                            StatusIdColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                    case "STATUSTYPE":
                        {
                            StatusTypeColumn = i;
                            break;
                        }
                    case "STATUSCODE":
                        {
                            StatusCodeColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
