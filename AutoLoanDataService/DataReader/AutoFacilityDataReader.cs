﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoFacilityDataReader : EntityDataReader<AutoFacility>
    {
        public int FACI_IDColumn = -1;
        public int FACI_NAMEColumn = -1;
        public int FACI_CODEColumn = -1;

        public AutoFacilityDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoFacility Read()
        {
            var objAutoFacility = new AutoFacility();
            objAutoFacility.FACI_ID = GetInt(FACI_IDColumn);
            objAutoFacility.FACI_NAME = GetString(FACI_NAMEColumn);
            objAutoFacility.FACI_CODE = GetString(FACI_CODEColumn);
            return objAutoFacility;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "FACI_ID":
                        {
                            FACI_IDColumn = i;
                            break;
                        }
                    case "FACI_NAME":
                        {
                            FACI_NAMEColumn = i;
                            break;
                        }
                    case "FACI_CODE":
                        {
                            FACI_CODEColumn = i;
                            break;
                        }
                }
            }
        }
    }
}
