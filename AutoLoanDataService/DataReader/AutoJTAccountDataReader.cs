﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;


namespace AutoLoanDataService.DataReader
{
    internal class AutoJTAccountDataReader : EntityDataReader<AutoJTAccount>
    {
        public int jtAccountIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int AccountNumberForSCBColumn = -1;
        public int CreditCardNumberForSCBColumn = -1;


        public AutoJTAccountDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoJTAccount Read()
        {
            var objAutoJTAccount = new AutoJTAccount();
            objAutoJTAccount.jtAccountId = GetInt(jtAccountIdColumn);
            objAutoJTAccount.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoJTAccount.AccountNumberForSCB = GetString(AccountNumberForSCBColumn);
            objAutoJTAccount.CreditCardNumberForSCB = GetString(CreditCardNumberForSCBColumn);

            return objAutoJTAccount;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "JTACCOUNTID":
                        {
                            jtAccountIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBERFORSCB":
                        {
                            AccountNumberForSCBColumn = i;
                            break;
                        }
                    case "CREDITCARDNUMBERFORSCB":
                        {
                            CreditCardNumberForSCBColumn = i;
                            break;
                        }
                }
            }
        }




    }
}
