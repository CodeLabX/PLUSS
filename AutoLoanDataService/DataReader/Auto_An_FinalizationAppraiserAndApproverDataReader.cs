﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationAppraiserAndApproverDataReader : EntityDataReader<Auto_An_FinalizationAppraiserAndApprover>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int AppraisedByUserIDColumn = -1;
        public int AppraisedByUserCodeColumn = -1;
        public int SupportedByUserIDColumn = -1;
        public int SupportedByUserCodeColumn = -1;
        public int ApprovedByUserIDColumn = -1;
        public int ApprovedByUserCodeColumn = -1;
        public int AutoDLALimitColumn = -1;
        public int TotalSecuredLimitColumn = -1;


        public Auto_An_FinalizationAppraiserAndApproverDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationAppraiserAndApprover Read()
        {
            var objAuto_An_FinalizationAppraiserAndApprover = new Auto_An_FinalizationAppraiserAndApprover();

            objAuto_An_FinalizationAppraiserAndApprover.ID = GetInt(IDColumn);
            objAuto_An_FinalizationAppraiserAndApprover.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationAppraiserAndApprover.AppraisedByUserID = GetInt(AppraisedByUserIDColumn);
            objAuto_An_FinalizationAppraiserAndApprover.AppraisedByUserCode = GetInt(AppraisedByUserCodeColumn);
            objAuto_An_FinalizationAppraiserAndApprover.SupportedByUserID = GetInt(SupportedByUserIDColumn);
            objAuto_An_FinalizationAppraiserAndApprover.SupportedByUserCode = GetInt(SupportedByUserCodeColumn);
            objAuto_An_FinalizationAppraiserAndApprover.ApprovedByUserID = GetInt(ApprovedByUserIDColumn);
            objAuto_An_FinalizationAppraiserAndApprover.ApprovedByUserCode = GetInt(ApprovedByUserCodeColumn);
            objAuto_An_FinalizationAppraiserAndApprover.AutoDLALimit = GetDouble(AutoDLALimitColumn);
            objAuto_An_FinalizationAppraiserAndApprover.TotalSecuredLimit = GetDouble(TotalSecuredLimitColumn);

            return objAuto_An_FinalizationAppraiserAndApprover;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "APPRAISEDBYUSERID": { AppraisedByUserIDColumn = i; break; }
                    case "APPRAISEDBYUSERCODE": { AppraisedByUserCodeColumn = i; break; }
                    case "SUPPORTEDBYUSERID": { SupportedByUserIDColumn = i; break; }
                    case "SUPPORTEDBYUSERCODE": { SupportedByUserCodeColumn = i; break; }
                    case "APPROVEDBYUSERID": { ApprovedByUserIDColumn = i; break; }
                    case "APPROVEDBYUSERCODE": { ApprovedByUserCodeColumn = i; break; }
                    case "AUTODLALIMIT": { AutoDLALimitColumn = i; break; }
                    case "TOTALSECUREDLIMIT": { TotalSecuredLimitColumn = i; break; }
                }
            }
        }




    }
}
