﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoLoanMasterDataReader : EntityDataReader<AutoLoanMaster>
    {

        public int Auto_LoanMasterIdColumn = -1;
        public int LLIDColumn = -1;
        public int ProductIdColumn = -1;
        public int SourceColumn = -1;
        public int SourceCodeColumn = -1;
        public int SourceNameColumn = -1;
        public int AppliedDateColumn = -1;
        public int AppliedAmountColumn = -1;
        public int ReceiveDateColumn = -1;
        public int AskingTenorColumn = -1;
        public int JointApplicationColumn = -1;
        public int ReferenceLLIdColumn = -1;
        public int StatusIDColumn = -1;
        public int UserIDColumn = -1;
        public int AskingRateColumn = -1;
        public int ParentLLIDColumn = -1;


        public AutoLoanMasterDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoLoanMaster Read()
        {
            var objAutoLoanMaster = new AutoLoanMaster();
            objAutoLoanMaster.Auto_LoanMasterId = GetInt(Auto_LoanMasterIdColumn);
            if (objAutoLoanMaster.Auto_LoanMasterId == int.MinValue)
            {
                objAutoLoanMaster.Auto_LoanMasterId = 0;
            }
            objAutoLoanMaster.LLID = GetInt(LLIDColumn);
            objAutoLoanMaster.ProductId = GetInt(ProductIdColumn);
            objAutoLoanMaster.Source = GetString(SourceColumn);
            objAutoLoanMaster.SourceCode = GetString(SourceCodeColumn);
            objAutoLoanMaster.SourceName = GetString(SourceNameColumn);
            objAutoLoanMaster.AppliedDate = GetDate(AppliedDateColumn);
            objAutoLoanMaster.AppliedAmount = GetDouble(AppliedAmountColumn);
            objAutoLoanMaster.ReceiveDate = GetDate(ReceiveDateColumn);
            objAutoLoanMaster.AskingTenor = GetString(AskingTenorColumn);
            objAutoLoanMaster.JointApplication = GetBool(JointApplicationColumn);
            objAutoLoanMaster.ReferenceLLId = GetInt(ReferenceLLIdColumn);
            objAutoLoanMaster.StatusID = GetInt(StatusIDColumn);
            objAutoLoanMaster.UserID = GetInt(UserIDColumn);
            objAutoLoanMaster.AskingRate = GetInt(AskingRateColumn);
            objAutoLoanMaster.ParentLLID = GetInt(ParentLLIDColumn);
            if (objAutoLoanMaster.ParentLLID == int.MinValue) {
                objAutoLoanMaster.ParentLLID = 0;
            }

            return objAutoLoanMaster;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTO_LOANMASTERID":
                        {
                            Auto_LoanMasterIdColumn = i;
                            break;
                        }
                    case "LLID":
                        {
                            LLIDColumn = i;
                            break;
                        }
                    case "PRODUCTID":
                        {
                            ProductIdColumn = i;
                            break;
                        }
                    case "SOURCE":
                        {
                            SourceColumn = i;
                            break;
                        }
                    case "SOURCECODE":
                        {
                            SourceCodeColumn = i;
                            break;
                        }
                    case "SOURCENAME":
                        {
                            SourceNameColumn = i;
                            break;
                        }
                    case "APPLIEDDATE":
                        {
                            AppliedDateColumn = i;
                            break;
                        }
                    case "APPLIEDAMOUNT":
                        {
                            AppliedAmountColumn = i;
                            break;
                        }
                    case "RECEIVEDATE":
                        {
                            ReceiveDateColumn = i;
                            break;
                        }
                    case "ASKINGTENOR":
                        {
                            AskingTenorColumn = i;
                            break;
                        }
                    case "JOINTAPPLICATION":
                        {
                            JointApplicationColumn = i;
                            break;
                        }
                    case "REFERENCELLID":
                        {
                            ReferenceLLIdColumn = i;
                            break;
                        }
                    case "STATUSID":
                        {
                            StatusIDColumn = i;
                            break;
                        }
                    case "USERID":
                        {
                            UserIDColumn = i;
                            break;
                        }
                    case "ASKINGRATE":
                        {
                            AskingRateColumn = i;
                            break;
                        }
                    case "PARENTLLID":
                        {
                            ParentLLIDColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
