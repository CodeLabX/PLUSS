﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationDevFoundDataReader : EntityDataReader<Auto_An_FinalizationDevFound>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int AgeColumn = -1;
        public int DBRColumn = -1;
        public int SeatingCapaciltyColumn = -1;
        public int SharePortionColumn = -1;


        public Auto_An_FinalizationDevFoundDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationDevFound Read()
        {
            var objAuto_An_FinalizationDevFound = new Auto_An_FinalizationDevFound();

            objAuto_An_FinalizationDevFound.ID = GetInt(IDColumn);
            objAuto_An_FinalizationDevFound.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationDevFound.Age = GetString(AgeColumn);
            objAuto_An_FinalizationDevFound.DBR = GetString(DBRColumn);
            objAuto_An_FinalizationDevFound.SeatingCapacilty = GetString(SeatingCapaciltyColumn);
            objAuto_An_FinalizationDevFound.SharePortion = GetString(SharePortionColumn);

            return objAuto_An_FinalizationDevFound;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "AGE":
                        {
                            AgeColumn = i;
                            break;
                        }
                    case "DBR":
                        {
                            DBRColumn = i;
                            break;
                        }
                    case "SEATINGCAPACILTY":
                        {
                            SeatingCapaciltyColumn = i;
                            break;
                        }
                    case "SHAREPORTION":
                        {
                            SharePortionColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
