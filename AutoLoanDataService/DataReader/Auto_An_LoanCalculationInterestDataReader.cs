﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_LoanCalculationInterestDataReader : EntityDataReader<Auto_An_LoanCalculationInterest>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int VendorAllowedColumn = -1;
        public int ARTAAllowedColumn = -1;
        public int SegmentAllowedColumn = -1;
        public int AskingRateColumn = -1;
        public int AppropriateRateColumn = -1;
        public int ConsideredRateColumn = -1;


        public Auto_An_LoanCalculationInterestDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_LoanCalculationInterest Read()
        {
            var objAuto_An_LoanCalculationInterest = new Auto_An_LoanCalculationInterest();

            objAuto_An_LoanCalculationInterest.ID = GetInt(IDColumn);
            objAuto_An_LoanCalculationInterest.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_LoanCalculationInterest.VendorAllowed = GetDouble(VendorAllowedColumn);
            objAuto_An_LoanCalculationInterest.ARTAAllowed = GetDouble(ARTAAllowedColumn);
            objAuto_An_LoanCalculationInterest.SegmentAllowed = GetDouble(SegmentAllowedColumn);
            objAuto_An_LoanCalculationInterest.AskingRate = GetDouble(AskingRateColumn);
            objAuto_An_LoanCalculationInterest.AppropriateRate = GetDouble(AppropriateRateColumn);
            objAuto_An_LoanCalculationInterest.ConsideredRate = GetDouble(ConsideredRateColumn);

            return objAuto_An_LoanCalculationInterest;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "VENDORALLOWED":
                        {
                            VendorAllowedColumn = i;
                            break;
                        }
                    case "ARTAALLOWED":
                        {
                            ARTAAllowedColumn = i;
                            break;
                        }
                    case "SEGMENTALLOWED":
                        {
                            SegmentAllowedColumn = i;
                            break;
                        }
                    case "ASKINGRATE":
                        {
                            AskingRateColumn = i;
                            break;
                        }
                    case "APPROPRIATERATE":
                        {
                            AppropriateRateColumn = i;
                            break;
                        }
                    case "CONSIDEREDRATE":
                        {
                            ConsideredRateColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
