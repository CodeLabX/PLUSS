﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoLoanSummaryDataReader : EntityDataReader<AutoLoanSummary>
    {

        public int Auto_LoanMasterIdColumn = -1;
        public int LLIDColumn = -1;
        public int PrNameColumn = -1;
        public int ProductNameColumn = -1;
        public int ManufacturerNameColumn = -1;
        public int ModelColumn = -1;
        public int AskingTenorColumn = -1;
        public int AppliedAmountColumn = -1;
        public int StatusIDColumn = -1;
        public int StatusNameColumn = -1;
        public int TotalCountColumn = -1;

        public AutoLoanSummaryDataReader(IDataReader reader)
            : base(reader)
        {
        }


        public override AutoLoanSummary Read()
        {
            var objAutoLoanSummary = new AutoLoanSummary();

            objAutoLoanSummary.Auto_LoanMasterId = GetInt(Auto_LoanMasterIdColumn);
            objAutoLoanSummary.LLID = GetInt(LLIDColumn);
            objAutoLoanSummary.PrName = GetString(PrNameColumn);
            objAutoLoanSummary.ProductName = GetString(ProductNameColumn);
            objAutoLoanSummary.ManufacturerName = GetString(ManufacturerNameColumn);
            objAutoLoanSummary.Model = GetString(ModelColumn);
            objAutoLoanSummary.AskingTenor = GetInt(AskingTenorColumn);
            objAutoLoanSummary.AppliedAmount = GetDouble(AppliedAmountColumn);
            objAutoLoanSummary.StatusID = GetInt(StatusIDColumn);
            objAutoLoanSummary.StatusName = GetString(StatusNameColumn);
            objAutoLoanSummary.TotalCount = GetInt(TotalCountColumn);

            return objAutoLoanSummary;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTO_LOANMASTERID":
                        {
                            Auto_LoanMasterIdColumn = i;
                            break;
                        }
                    case "LLID":
                        {
                            LLIDColumn = i;
                            break;
                        }
                    case "PRNAME":
                        {
                            PrNameColumn = i;
                            break;
                        }
                    case "PRODUCTNAME":
                        {
                            ProductNameColumn = i;
                            break;
                        }
                    case "MANUFACTURERNAME":
                        {
                            ManufacturerNameColumn = i;
                            break;
                        }
                    case "MODEL":
                        {
                            ModelColumn = i;
                            break;
                        }
                    case "ASKINGTENOR":
                        {
                            AskingTenorColumn = i;
                            break;
                        }
                    case "APPLIEDAMOUNT":
                        {
                            AppliedAmountColumn = i;
                            break;
                        }
                    case "STATUSID":
                        {
                            StatusIDColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                }
            }
        }




    }
}
