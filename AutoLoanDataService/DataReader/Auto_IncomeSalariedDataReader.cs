﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{

 internal class Auto_IncomeSalariedDataReader : EntityDataReader<Auto_IncomeSalaried>
   {
       public int IdColumn= -1;
public int AnalystMasterIdColumn= -1;
public int SalaryModeColumn= -1;
public int EmployerColumn= -1;
public int IncomeAssIdColumn= -1;
public int GrosSalaryColumn= -1;
public int NetSalaryColumn= -1;
public int VariableSalaryColumn= -1;
public int VariableSalaryAmountColumn= -1;
public int CarAllownceColumn= -1;
public int CarAllownceAmountColumn= -1;
public int CarAllowncePerColumn= -1;
public int OwnHouseBenifitColumn= -1;
public int TotalIncomeColumn= -1;
public int RemarksColumn= -1;
public int AccountTypeColumn= -1;
     public int VariableSalaryPercentColumn = -1;


public Auto_IncomeSalariedDataReader(IDataReader reader)
           : base(reader)
       {
       }

public override Auto_IncomeSalaried Read()
       {
           var objAutoIncomeSalaried = new Auto_IncomeSalaried();
           objAutoIncomeSalaried.Id = GetInt(IdColumn);
           objAutoIncomeSalaried.AnalystMasterId = GetInt(AnalystMasterIdColumn);
           objAutoIncomeSalaried.SalaryMode = GetInt(SalaryModeColumn);
           objAutoIncomeSalaried.Employer = GetInt(EmployerColumn);
           objAutoIncomeSalaried.IncomeAssId = GetInt(IncomeAssIdColumn);
           objAutoIncomeSalaried.GrosSalary = GetDouble(GrosSalaryColumn);
           objAutoIncomeSalaried.NetSalary = GetDouble(NetSalaryColumn);
           objAutoIncomeSalaried.VariableSalary = GetBool(VariableSalaryColumn);
           objAutoIncomeSalaried.VariableSalaryAmount = GetDouble(VariableSalaryAmountColumn);
           objAutoIncomeSalaried.CarAllownce = GetBool(CarAllownceColumn);
           objAutoIncomeSalaried.CarAllownceAmount = GetDouble(CarAllownceAmountColumn);
           objAutoIncomeSalaried.CarAllowncePer = GetDouble(CarAllowncePerColumn);
           objAutoIncomeSalaried.OwnHouseBenifit = GetBool(OwnHouseBenifitColumn);
           objAutoIncomeSalaried.TotalIncome = GetDouble(TotalIncomeColumn);
           objAutoIncomeSalaried.Remarks = GetString(RemarksColumn);
    objAutoIncomeSalaried.VariableSalaryPercent = GetInt(VariableSalaryPercentColumn);
           objAutoIncomeSalaried.AccountType = GetInt(AccountTypeColumn);

           return objAutoIncomeSalaried;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "ANALYSTMASTERID":
                       {
                           AnalystMasterIdColumn = i;
                           break;
                       }
                   case "SALARYMODE":
                       {
                           SalaryModeColumn = i;
                           break;
                       }
                   case "EMPLOYER":
                       {
                           EmployerColumn = i;
                           break;
                       }
                   case "INCOMEASSID":
                       {
                           IncomeAssIdColumn = i;
                           break;
                       }
                   case "GROSSALARY":
                       {
                           GrosSalaryColumn = i;
                           break;
                       }
                   case "NETSALARY":
                       {
                           NetSalaryColumn = i;
                           break;
                       }
                   case "VARIABLESALARY":
                       {
                           VariableSalaryColumn = i;
                           break;
                       }
                   case "VARIABLESALARYAMOUNT":
                       {
                           VariableSalaryAmountColumn = i;
                           break;
                       }
                   case "CARALLOWNCE":
                       {
                           CarAllownceColumn = i;
                           break;
                       }
                   case "CARALLOWNCEAMOUNT":
                       {
                           CarAllownceAmountColumn = i;
                           break;
                       }
                   case "CARALLOWNCEPER":
                       {
                           CarAllowncePerColumn = i;
                           break;
                       }
                   case "OWNHOUSEBENIFIT":
                       {
                           OwnHouseBenifitColumn = i;
                           break;
                       }
                   case "TOTALINCOME":
                       {
                           TotalIncomeColumn = i;
                           break;
                       }
                   case "REMARKS":
                       {
                           RemarksColumn = i;
                           break;
                       }
                   case "ACCOUNTTYPE":
                       {
                           AccountTypeColumn = i;
                           break;
                       }
                   case "VARIABLESALARYPERCENT":
                       {
                           VariableSalaryPercentColumn = i;
                           break;
                       }


               }
           }
       }
   }
}
