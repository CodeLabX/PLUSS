﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_IncomeSegmentIncomeDataReader : EntityDataReader<Auto_An_IncomeSegmentIncome>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int TotalIncomeColumn = -1;
        public int DeclaredIncomeColumn = -1;
        public int AppropriateIncomeColumn = -1;
        public int EmployeeSegmentColumn = -1;
        public int EmployeeSegmentCodeColumn = -1;
        public int AssesmentMethodColumn = -1;
        public int AssesmentMethodCodeColumn = -1;
        public int CategoryColumn = -1;
        public int CategoryCodeColumn = -1;
        public int TotalJointIncomeColumn = -1;


        public Auto_An_IncomeSegmentIncomeDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_IncomeSegmentIncome Read()
        {
            var objAuto_An_IncomeSegmentIncome = new Auto_An_IncomeSegmentIncome();

            objAuto_An_IncomeSegmentIncome.ID = GetInt(IDColumn);
            objAuto_An_IncomeSegmentIncome.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_IncomeSegmentIncome.TotalIncome = GetDouble(TotalIncomeColumn);
            objAuto_An_IncomeSegmentIncome.DeclaredIncome = GetDouble(DeclaredIncomeColumn);
            objAuto_An_IncomeSegmentIncome.AppropriateIncome = GetDouble(AppropriateIncomeColumn);
            objAuto_An_IncomeSegmentIncome.EmployeeSegment = GetInt(EmployeeSegmentColumn);
            objAuto_An_IncomeSegmentIncome.EmployeeSegmentCode = GetString(EmployeeSegmentCodeColumn);
            objAuto_An_IncomeSegmentIncome.AssesmentMethod = GetInt(AssesmentMethodColumn);
            objAuto_An_IncomeSegmentIncome.AssesmentMethodCode = GetString(AssesmentMethodCodeColumn);
            objAuto_An_IncomeSegmentIncome.Category = GetInt(CategoryColumn);
            objAuto_An_IncomeSegmentIncome.CategoryCode = GetString(CategoryCodeColumn);
            objAuto_An_IncomeSegmentIncome.TotalJointIncome = GetDouble(TotalJointIncomeColumn);

            return objAuto_An_IncomeSegmentIncome;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "TOTALINCOME":
                        {
                            TotalIncomeColumn = i;
                            break;
                        }
                    case "DECLAREDINCOME":
                        {
                            DeclaredIncomeColumn = i;
                            break;
                        }
                    case "APPROPRIATEINCOME":
                        {
                            AppropriateIncomeColumn = i;
                            break;
                        }
                    case "EMPLOYEESEGMENT":
                        {
                            EmployeeSegmentColumn = i;
                            break;
                        }
                    case "EMPLOYEESEGMENTCODE":
                        {
                            EmployeeSegmentCodeColumn = i;
                            break;
                        }
                    case "ASSESMENTMETHOD":
                        {
                            AssesmentMethodColumn = i;
                            break;
                        }
                    case "ASSESMENTMETHODCODE":
                        {
                            AssesmentMethodCodeColumn = i;
                            break;
                        }
                    case "CATEGORY":
                        {
                            CategoryColumn = i;
                            break;
                        }
                    case "CATEGORYCODE":
                        {
                            CategoryCodeColumn = i;
                            break;
                        }
                    case "TOTALJOINTINCOME":
                        {
                            TotalJointIncomeColumn = i;
                            break;
                        }

                }
            }
        }




    }
}
