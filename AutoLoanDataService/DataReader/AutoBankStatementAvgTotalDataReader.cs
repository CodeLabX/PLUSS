﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoBankStatementAvgTotalDataReader : EntityDataReader<Auto_BankStatementAvgTotal>
    {
        public int BankStatementAvgTotalIdColumn = -1;
        public int LLIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int TypeColumn = -1;
        public int BankStatementIdColumn = -1;
        public int BankIdColumn = -1;
        public int MonthColumn = -1;
        public int AVGTotalColumn = -1;
        public int TrunOverTotalColumn = -1;
        public int AdjustmentColumn = -1;
        public int AccountNumberColumn = -1;


        public AutoBankStatementAvgTotalDataReader(IDataReader reader)
            : base(reader)
        {
        }


        public override Auto_BankStatementAvgTotal Read()
        {
            var objAuto_BankStatementAvgTotal = new Auto_BankStatementAvgTotal();

            objAuto_BankStatementAvgTotal.BankStatementAvgTotalId = GetInt(BankStatementAvgTotalIdColumn);
            objAuto_BankStatementAvgTotal.LLId = GetInt(LLIdColumn);
            objAuto_BankStatementAvgTotal.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAuto_BankStatementAvgTotal.Type = GetInt(TypeColumn);
            objAuto_BankStatementAvgTotal.BankStatementId = GetInt(BankStatementIdColumn);
            objAuto_BankStatementAvgTotal.BankId = GetInt(BankIdColumn);
            objAuto_BankStatementAvgTotal.Month = GetDate(MonthColumn);
            objAuto_BankStatementAvgTotal.AVGTotal = GetDouble(AVGTotalColumn);
            objAuto_BankStatementAvgTotal.TrunOverTotal = GetDouble(TrunOverTotalColumn);
            objAuto_BankStatementAvgTotal.Adjustment = GetDouble(AdjustmentColumn);
            objAuto_BankStatementAvgTotal.AccountNumber = GetString(AccountNumberColumn);

            return objAuto_BankStatementAvgTotal;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "BANKSTATEMENTAVGTOTALID":
                        {
                            BankStatementAvgTotalIdColumn = i;
                            break;
                        }
                    case "LLID":
                        {
                            LLIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "TYPE":
                        {
                            TypeColumn = i;
                            break;
                        }
                    case "BANKSTATEMENTID":
                        {
                            BankStatementIdColumn = i;
                            break;
                        }
                    case "BANKID":
                        {
                            BankIdColumn = i;
                            break;
                        }
                    case "MONTH":
                        {
                            MonthColumn = i;
                            break;
                        }
                    case "AVGTOTAL":
                        {
                            AVGTotalColumn = i;
                            break;
                        }
                    case "TRUNOVERTOTAL":
                        {
                            TrunOverTotalColumn = i;
                            break;
                        }
                    case "ADJUSTMENT":
                        {
                            AdjustmentColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBER":
                        {
                            AccountNumberColumn = i;
                            break;
                        }
                }
            }
        }
    }

}
