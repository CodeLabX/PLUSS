﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoPRProfessionDataReader : EntityDataReader<AutoPRProfession>
    {
        public int PrProfessionIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int PrimaryProfessionColumn = -1;
        public int OtherProfessionColumn = -1;
        public int MonthinCurrentProfessionColumn = -1;
        public int NameofCompanyColumn = -1;
        public int NameofOrganizationColumn = -1;
        public int DesignationColumn = -1;
        public int NatureOfBussinessColumn = -1;
        public int YearsInBussinessColumn = -1;
        public int OfficeStatusColumn = -1;
        public int AddressColumn = -1;
        public int OfficePhoneColumn = -1;
        public int OwnerShipTypeColumn = -1;
        public int EmploymentStatusColumn = -1;
        public int TotalProfessionalExperienceColumn = -1;
        public int NumberOfEmployeesColumn = -1;
        public int EquityShareColumn = -1;
        public int PrIncomeSourceColumn = -1;
        public int OtherIncomeSourceColumn = -1;
        public int NoOfFloorsRentedColumn = -1;
        public int NatureOfrentedFloorsColumn = -1;
        public int RentedAresInSFTColumn = -1;
        public int ConstructionCompletingYearColumn = -1;
        public int PrimaryProfessionNameColumn = -1;
        public int OtherProfessionNameColumn = -1;


        public AutoPRProfessionDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoPRProfession Read()
        {
            var objAutoPRProfession = new AutoPRProfession();
            objAutoPRProfession.PrProfessionId = GetInt(PrProfessionIdColumn);
            objAutoPRProfession.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoPRProfession.PrimaryProfession = GetInt(PrimaryProfessionColumn);
            objAutoPRProfession.OtherProfession = GetInt(OtherProfessionColumn);
            objAutoPRProfession.MonthinCurrentProfession = GetString(MonthinCurrentProfessionColumn);
            objAutoPRProfession.NameofCompany = GetInt(NameofCompanyColumn);
            objAutoPRProfession.NameofOrganization = GetString(NameofOrganizationColumn);
            objAutoPRProfession.Designation = GetString(DesignationColumn);
            objAutoPRProfession.NatureOfBussiness = GetString(NatureOfBussinessColumn);
            objAutoPRProfession.YearsInBussiness = GetString(YearsInBussinessColumn);
            objAutoPRProfession.OfficeStatus = GetString(OfficeStatusColumn);
            objAutoPRProfession.Address = GetString(AddressColumn);
            objAutoPRProfession.OfficePhone = GetString(OfficePhoneColumn);
            objAutoPRProfession.OwnerShipType = GetString(OwnerShipTypeColumn);
            objAutoPRProfession.EmploymentStatus = GetString(EmploymentStatusColumn);
            objAutoPRProfession.TotalProfessionalExperience = GetString(TotalProfessionalExperienceColumn);
            objAutoPRProfession.NumberOfEmployees = GetInt(NumberOfEmployeesColumn);
            objAutoPRProfession.EquityShare = GetDouble(EquityShareColumn);
            objAutoPRProfession.PrIncomeSource = GetString(PrIncomeSourceColumn);
            objAutoPRProfession.OtherIncomeSource = GetString(OtherIncomeSourceColumn);
            objAutoPRProfession.NoOfFloorsRented = GetInt(NoOfFloorsRentedColumn);
            objAutoPRProfession.NatureOfrentedFloors = GetString(NatureOfrentedFloorsColumn);
            objAutoPRProfession.RentedAresInSFT = GetString(RentedAresInSFTColumn);
            objAutoPRProfession.ConstructionCompletingYear = GetString(ConstructionCompletingYearColumn);
            objAutoPRProfession.PrimaryProfessionName = GetString(PrimaryProfessionNameColumn);
            objAutoPRProfession.OtherProfessionName = GetString(OtherProfessionNameColumn);

            return objAutoPRProfession;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "PRPROFESSIONID":
                        {
                            PrProfessionIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "PRIMARYPROFESSION":
                        {
                            PrimaryProfessionColumn = i;
                            break;
                        }
                    case "OTHERPROFESSION":
                        {
                            OtherProfessionColumn = i;
                            break;
                        }
                    case "MONTHINCURRENTPROFESSION":
                        {
                            MonthinCurrentProfessionColumn = i;
                            break;
                        }
                    case "NAMEOFCOMPANY":
                        {
                            NameofCompanyColumn = i;
                            break;
                        }
                    case "NAMEOFORGANIZATION":
                        {
                            NameofOrganizationColumn = i;
                            break;
                        }
                    case "DESIGNATION":
                        {
                            DesignationColumn = i;
                            break;
                        }
                    case "NATUREOFBUSSINESS":
                        {
                            NatureOfBussinessColumn = i;
                            break;
                        }
                    case "YEARSINBUSSINESS":
                        {
                            YearsInBussinessColumn = i;
                            break;
                        }
                    case "OFFICESTATUS":
                        {
                            OfficeStatusColumn = i;
                            break;
                        }
                    case "ADDRESS":
                        {
                            AddressColumn = i;
                            break;
                        }
                    case "OFFICEPHONE":
                        {
                            OfficePhoneColumn = i;
                            break;
                        }
                    case "OWNERSHIPTYPE":
                        {
                            OwnerShipTypeColumn = i;
                            break;
                        }
                    case "EMPLOYMENTSTATUS":
                        {
                            EmploymentStatusColumn = i;
                            break;
                        }
                    case "TOTALPROFESSIONALEXPERIENCE":
                        {
                            TotalProfessionalExperienceColumn = i;
                            break;
                        }
                    case "NUMBEROFEMPLOYEES":
                        {
                            NumberOfEmployeesColumn = i;
                            break;
                        }
                    case "EQUITYSHARE":
                        {
                            EquityShareColumn = i;
                            break;
                        }
                    case "PRINCOMESOURCE":
                        {
                            PrIncomeSourceColumn = i;
                            break;
                        }
                    case "OTHERINCOMESOURCE":
                        {
                            OtherIncomeSourceColumn = i;
                            break;
                        }
                    case "NOOFFLOORSRENTED":
                        {
                            NoOfFloorsRentedColumn = i;
                            break;
                        }
                    case "NATUREOFRENTEDFLOORS":
                        {
                            NatureOfrentedFloorsColumn = i;
                            break;
                        }
                    case "RENTEDARESINSFT":
                        {
                            RentedAresInSFTColumn = i;
                            break;
                        }
                    case "CONSTRUCTIONCOMPLETINGYEAR":
                        {
                            ConstructionCompletingYearColumn = i;
                            break;
                        }
                    case "PRPRIMARYPROFESSIONNAME":
                        {
                            PrimaryProfessionNameColumn = i;
                            break;
                        }
                    case "PROTHERPROFESSIONNAME":
                        {
                            OtherProfessionNameColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
