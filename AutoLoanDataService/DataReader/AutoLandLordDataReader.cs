﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class AutoLandLordDataReader : EntityDataReader<AutoLandLord>
   {
       public int IdColumn = -1;
       public int AutoLoanMasterIdColumn = -1;
       public int EmployerCatagoryIdColumn = -1;
       public int SectorIdColumn = -1;
       public int IncomeAssIdColumn = -1;
       public int TotalRentalIncomeColumn = -1;
       public int ReflectionRequiredColumn = -1;
       public int PercentageColumn = -1;
       public int RemarksColumn = -1;
      public int AccountTypeColumn = -1;


       public AutoLandLordDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override AutoLandLord Read()
       {
           var objAutoLandLord = new AutoLandLord();
           objAutoLandLord.Id = GetInt(IdColumn);
           objAutoLandLord.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
           objAutoLandLord.EmployerCatagoryId = GetInt(EmployerCatagoryIdColumn);
           objAutoLandLord.SectorId = GetInt(SectorIdColumn);
           objAutoLandLord.IncomeAssId = GetInt(IncomeAssIdColumn);
           objAutoLandLord.TotalRentalIncome = GetDouble(TotalRentalIncomeColumn);
           objAutoLandLord.ReflectionRequired = GetDouble(ReflectionRequiredColumn);
           objAutoLandLord.Percentage = GetDouble(PercentageColumn);
           objAutoLandLord.Remarks = GetString(RemarksColumn);
           objAutoLandLord.AccountType = GetInt(AccountTypeColumn);

           return objAutoLandLord;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "AUTOLOANMASTERID":
                       {
                           AutoLoanMasterIdColumn = i;
                           break;
                       }
                   case "EMPLOYERCATAGORYID":
                       {
                           EmployerCatagoryIdColumn = i;
                           break;
                       }
                   case "SECTORID":
                       {
                           SectorIdColumn = i;
                           break;
                       }
                   case "INCOMEASSID":
                       {
                           IncomeAssIdColumn = i;
                           break;
                       }
                   case "TOTALRENTALINCOME":
                       {
                           TotalRentalIncomeColumn = i;
                           break;
                       }
                   case "REFLECTIONREQUIRED":
                       {
                           ReflectionRequiredColumn = i;
                           break;
                       }
                   case "PERCENTAGE":
                       {
                           PercentageColumn = i;
                           break;
                       }
                   case "REMARKS":
                       {
                           RemarksColumn = i;
                           break;
                       }
                   case "ACCOUNTTYPE":
                       {
                           AccountTypeColumn = i;
                           break;
                       }


               }
           }
       }
   }
    
}
