﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationSterengthDataReader : EntityDataReader<Auto_An_FinalizationSterength>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int StrengthColumn = -1;


        public Auto_An_FinalizationSterengthDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationSterength Read()
        {
            var objAuto_An_FinalizationSterength = new Auto_An_FinalizationSterength();

            objAuto_An_FinalizationSterength.ID = GetInt(IDColumn);
            objAuto_An_FinalizationSterength.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationSterength.Strength = GetString(StrengthColumn);

            return objAuto_An_FinalizationSterength;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "STRENGTH":
                        {
                            StrengthColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
