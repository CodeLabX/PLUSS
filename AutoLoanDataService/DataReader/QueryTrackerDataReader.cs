﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class QueryTrackerDataReader : EntityDataReader<QueryTracker_Entity>
   {
       public int TrackerIdColumn = -1;
       public int LLIdColumn = -1;
       public int RefTrackerIdColumn = -1;
       public int PostingByIdColumn = -1;
       public int QueryToIdColumn = -1;
       public int CommentsColumn = -1;
       public int QueryDateColumn = -1;
       public int UserAndDepartmentColumn = -1;
       public int QueriedByColumn = -1;
       public int QueriedOnCommentsColumn = -1;
       public int TotalcountColumn = -1;
       public int AutoUserTypeIdColumn = -1;
       public QueryTrackerDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override QueryTracker_Entity Read()
       {
           var objQueryTracker= new QueryTracker_Entity();
           objQueryTracker.TrackerId = GetInt(TrackerIdColumn);
           objQueryTracker.LLId = GetInt(LLIdColumn);
           objQueryTracker.RefTrackerId = GetInt(RefTrackerIdColumn);
           objQueryTracker.PostingById = GetInt(PostingByIdColumn);
           objQueryTracker.QueryToId = GetInt(QueryToIdColumn);
           objQueryTracker.Comments = GetString(CommentsColumn);
           objQueryTracker.QueryDate = GetDate(QueryDateColumn);
           objQueryTracker.UserAndDepartment = GetString(UserAndDepartmentColumn);
           objQueryTracker.QueriedBy = GetString(QueriedByColumn);
           objQueryTracker.QueriedOnComments = GetString(QueriedOnCommentsColumn);
           objQueryTracker.Totalcount = GetInt(TotalcountColumn);
           objQueryTracker.AutoUserTypeId = GetInt(AutoUserTypeIdColumn);

           return objQueryTracker;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "TRACKERID":
                       {
                           TrackerIdColumn = i;
                           break;
                       }
                    
                   case "LLID":
                       {
                           LLIdColumn = i;
                           break;
                       }
                   case "REFTRACKERID":
                       {
                           RefTrackerIdColumn = i;
                           break;
                       }

                   case "POSTINGBYID":
                       {
                           PostingByIdColumn = i;
                           break;
                       }

                   case "QUERYTOID":
                       {
                           QueryToIdColumn = i;
                           break;
                       }
                   case "COMMENTS":
                       {
                           CommentsColumn = i;
                           break;
                       }
                   case "QUERYDATE":
                       {
                           QueryDateColumn = i;
                           break;
                       }

                   case "USER_AND_DEPARTMENT":
                       {
                           UserAndDepartmentColumn = i;
                           break;
                       }
                   case "QUERIEDBY":
                       {
                           QueriedByColumn = i;
                           break;
                       }
                   case "QUERIEDONCOMMENTS":
                       {
                           QueriedOnCommentsColumn = i;
                           break;
                       }
                   case "TOTALCOUNT":
                       {
                           TotalcountColumn = i;
                           break;
                       }
                   case "USLE_ID":
                       {
                           AutoUserTypeIdColumn = i;
                           break;
                       }
               }
           }
       }
   }
    
}
