﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoApplicationInsuranceDataReader : EntityDataReader<AutoApplicationInsurance>
    {

        public int ApplicationInsuranceIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int GeneralInsuranceColumn = -1;
        public int LifeInsuranceColumn = -1;
        public int InsuranceFinancewithLoanColumn = -1;
        public int InsuranceFinancewithLoanLifeColumn = -1;

        public AutoApplicationInsuranceDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoApplicationInsurance Read()
        {
            var objAutoApplicationInsurance = new AutoApplicationInsurance();
            objAutoApplicationInsurance.ApplicationInsuranceId = GetInt(ApplicationInsuranceIdColumn);
            objAutoApplicationInsurance.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoApplicationInsurance.GeneralInsurance = GetInt(GeneralInsuranceColumn);
            objAutoApplicationInsurance.LifeInsurance = GetInt(LifeInsuranceColumn);
            objAutoApplicationInsurance.InsuranceFinancewithLoan = GetBool(InsuranceFinancewithLoanColumn);
            objAutoApplicationInsurance.InsuranceFinancewithLoanLife = GetBool(InsuranceFinancewithLoanLifeColumn);

            return objAutoApplicationInsurance;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "APPLICATIONINSURANCEID":
                        {
                            ApplicationInsuranceIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "GENERALINSURANCE":
                        {
                            GeneralInsuranceColumn = i;
                            break;
                        }
                    case "LIFEINSURANCE":
                        {
                            LifeInsuranceColumn = i;
                            break;
                        }
                    case "INSURANCEFINANCEWITHLOAN":
                        {
                            InsuranceFinancewithLoanColumn = i;
                            break;
                        }
                    case "INSURANCEFINANCEWITHLOANLIFE":
                        {
                            InsuranceFinancewithLoanLifeColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
