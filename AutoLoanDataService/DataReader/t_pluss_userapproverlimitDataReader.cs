﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Azolution.Common.SqlDataService;
using System.Data;
using AutoLoanService.Entity;

namespace AutoLoanDataService.DataReader
{
    internal class t_pluss_userapproverlimitDataReader : EntityDataReader<t_pluss_userapproverlimit>
    {
        public int LEVEL1_AMTColumn = -1;
        public int LEVEL2_AMTColumn = -1;
        public int LEVEL3_AMTColumn = -1;
        public int TotalDLAAmountL1Column = -1;
        public int TotalDLAAmountL2Column = -1;
        public int TotalDLAAmountL3Column = -1;
        public int PROD_TAGColumn = -1;

        public t_pluss_userapproverlimitDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override t_pluss_userapproverlimit Read()
        {
            var objt_pluss_userapproverlimit = new t_pluss_userapproverlimit();

           
            //DLA LIMT
            objt_pluss_userapproverlimit.LEVEL1_AMT = GetInt(LEVEL1_AMTColumn);
            if (objt_pluss_userapproverlimit.LEVEL1_AMT == int.MinValue) {
                objt_pluss_userapproverlimit.LEVEL1_AMT = 0;
            }
            objt_pluss_userapproverlimit.LEVEL2_AMT = GetInt(LEVEL2_AMTColumn);
            if (objt_pluss_userapproverlimit.LEVEL2_AMT == int.MinValue) {
                objt_pluss_userapproverlimit.LEVEL2_AMT = 0;
            }
            objt_pluss_userapproverlimit.LEVEL3_AMT = GetInt(LEVEL3_AMTColumn);
            if (objt_pluss_userapproverlimit.LEVEL3_AMT == int.MinValue) {
                objt_pluss_userapproverlimit.LEVEL3_AMT = 0;
            }
            objt_pluss_userapproverlimit.TotalDLAAmountL1 = GetInt(TotalDLAAmountL1Column);
            if (objt_pluss_userapproverlimit.TotalDLAAmountL1 == int.MinValue) {
                objt_pluss_userapproverlimit.TotalDLAAmountL1 = 0;
            }
            objt_pluss_userapproverlimit.TotalDLAAmountL2 = GetInt(TotalDLAAmountL2Column);
            if (objt_pluss_userapproverlimit.TotalDLAAmountL2 == int.MinValue) {
                objt_pluss_userapproverlimit.TotalDLAAmountL2 = 0;
            }
            objt_pluss_userapproverlimit.TotalDLAAmountL3 = GetInt(TotalDLAAmountL3Column);
            if (objt_pluss_userapproverlimit.TotalDLAAmountL3 == int.MinValue) {
                objt_pluss_userapproverlimit.TotalDLAAmountL3 = 0;
            }
            objt_pluss_userapproverlimit.PROD_TAG = GetString(PROD_TAGColumn);

            if (objt_pluss_userapproverlimit.PROD_TAG == "U") {
                objt_pluss_userapproverlimit.LEVEL1_AMT = 0;
                objt_pluss_userapproverlimit.LEVEL2_AMT = 0;
                objt_pluss_userapproverlimit.LEVEL3_AMT = 0;

            }

            return objt_pluss_userapproverlimit;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "LEVEL1_AMT": { LEVEL1_AMTColumn= i; break; }
                    case "LEVEL2_AMT": { LEVEL2_AMTColumn= i; break; }
                    case "LEVEL3_AMT": { LEVEL3_AMTColumn = i; break; }
                    case "TOTALDLAAMOUNTL1": { TotalDLAAmountL1Column = i; break; }
                    case "TOTALDLAAMOUNTL2": { TotalDLAAmountL2Column = i; break; }
                    case "TOTALDLAAMOUNTL3": { TotalDLAAmountL3Column= i; break; }
                    case "PROD_TAG": { PROD_TAGColumn = i; break; }
                }
            }
        }
    }
}
