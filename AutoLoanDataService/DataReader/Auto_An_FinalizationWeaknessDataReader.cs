﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationWeaknessDataReader : EntityDataReader<Auto_An_FinalizationWeakness>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int WeaknessColumn = -1;


        public Auto_An_FinalizationWeaknessDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationWeakness Read()
        {
            var objAuto_An_FinalizationWeakness = new Auto_An_FinalizationWeakness();

            objAuto_An_FinalizationWeakness.ID = GetInt(IDColumn);
            objAuto_An_FinalizationWeakness.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationWeakness.Weakness = GetString(WeaknessColumn);

            return objAuto_An_FinalizationWeakness;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "WEAKNESS":
                        {
                            WeaknessColumn = i;
                            break;
                        }
                }
            }
        }



    }
}
