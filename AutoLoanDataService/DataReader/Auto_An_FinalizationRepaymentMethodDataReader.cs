﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_FinalizationRepaymentMethodDataReader : EntityDataReader<Auto_An_FinalizationRepaymentMethod>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int AskingColumn = -1;
        public int InstrumentColumn = -1;
        public int SecurityPDCColumn = -1;
        public int PDCAmountColumn = -1;
        public int BankIDColumn = -1;
        public int SCBAccountColumn = -1;
        public int NoOfPDCColumn = -1;
        public int AccountNumberColumn = -1;
        public int BankNameColumn = -1;



        public Auto_An_FinalizationRepaymentMethodDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_FinalizationRepaymentMethod Read()
        {
            var objAuto_An_FinalizationRepaymentMethod = new Auto_An_FinalizationRepaymentMethod();

            objAuto_An_FinalizationRepaymentMethod.ID = GetInt(IDColumn);
            objAuto_An_FinalizationRepaymentMethod.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_FinalizationRepaymentMethod.Asking = GetString(AskingColumn);
            objAuto_An_FinalizationRepaymentMethod.Instrument = GetInt(InstrumentColumn);
            objAuto_An_FinalizationRepaymentMethod.SecurityPDC = GetInt(SecurityPDCColumn);
            objAuto_An_FinalizationRepaymentMethod.PDCAmount = GetDouble(PDCAmountColumn);
            objAuto_An_FinalizationRepaymentMethod.BankID = GetInt(BankIDColumn);
            objAuto_An_FinalizationRepaymentMethod.SCBAccount = GetString(SCBAccountColumn);
            objAuto_An_FinalizationRepaymentMethod.NoOfPDC = GetInt(NoOfPDCColumn);
            objAuto_An_FinalizationRepaymentMethod.AccountNumber = GetString(AccountNumberColumn);
            objAuto_An_FinalizationRepaymentMethod.BankName = GetString(BankNameColumn);

            return objAuto_An_FinalizationRepaymentMethod;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "ASKING":
                        {
                            AskingColumn = i;
                            break;
                        }
                    case "INSTRUMENT":
                        {
                            InstrumentColumn = i;
                            break;
                        }
                    case "SECURITYPDC":
                        {
                            SecurityPDCColumn = i;
                            break;
                        }
                    case "PDCAMOUNT":
                        {
                            PDCAmountColumn = i;
                            break;
                        }
                    case "BANKID":
                        {
                            BankIDColumn = i;
                            break;
                        }
                    case "SCBACCOUNT":
                        {
                            SCBAccountColumn = i;
                            break;
                        }
                    case "NOOFPDC":
                        {
                            NoOfPDCColumn = i;
                            break;
                        }
                    case "ACCOUNTNUMBER":
                        {
                            AccountNumberColumn = i;
                            break;
                        }
                    case "BANKNAME":
                        {
                            BankNameColumn = i;
                            break;
                        }
                }
            }
        }




    }
}
