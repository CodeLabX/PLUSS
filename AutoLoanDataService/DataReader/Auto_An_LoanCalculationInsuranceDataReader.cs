﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_LoanCalculationInsuranceDataReader : EntityDataReader<Auto_An_LoanCalculationInsurance>
    {

        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int GeneralInsuranceColumn = -1;
        public int ARTAColumn = -1;
        public int TotalInsuranceColumn = -1;


        public Auto_An_LoanCalculationInsuranceDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_LoanCalculationInsurance Read()
        {
            var objAuto_An_LoanCalculationInsurance = new Auto_An_LoanCalculationInsurance();

            objAuto_An_LoanCalculationInsurance.ID = GetInt(IDColumn);
            objAuto_An_LoanCalculationInsurance.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_LoanCalculationInsurance.GeneralInsurance = GetDouble(GeneralInsuranceColumn);
            objAuto_An_LoanCalculationInsurance.ARTA = GetDouble(ARTAColumn);
            objAuto_An_LoanCalculationInsurance.TotalInsurance = GetDouble(TotalInsuranceColumn);

            return objAuto_An_LoanCalculationInsurance;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "GENERALINSURANCE":
                        {
                            GeneralInsuranceColumn = i;
                            break;
                        }
                    case "ARTA":
                        {
                            ARTAColumn = i;
                            break;
                        }
                    case "TOTALINSURANCE":
                        {
                            TotalInsuranceColumn = i;
                            break;
                        }
                }
            }
        }




    }
}
