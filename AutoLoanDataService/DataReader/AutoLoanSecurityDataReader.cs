﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoLoanSecurityDataReader : EntityDataReader<AutoLoanSecurity>
    {

        public int SecurityIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int NatureOfSecurityColumn = -1;
        public int IssuingOfficeColumn = -1;
        public int FaceValueColumn = -1;
        public int XTVRateColumn = -1;
        public int IssueDateColumn = -1;
        public int InterestRateColumn = -1;
        public int FacilityTypeColumn = -1;
        public int StatusColumn = -1;
        public int FacilityIdColumn = -1;

        public AutoLoanSecurityDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoLoanSecurity Read()
        {
            var objAutoLoanSecurity = new AutoLoanSecurity();
            objAutoLoanSecurity.SecurityId = GetInt(SecurityIdColumn);
            objAutoLoanSecurity.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoLoanSecurity.NatureOfSecurity = GetInt(NatureOfSecurityColumn);
            objAutoLoanSecurity.IssuingOffice = GetString(IssuingOfficeColumn);
            objAutoLoanSecurity.FaceValue = GetDouble(FaceValueColumn);
            objAutoLoanSecurity.XTVRate = GetDouble(XTVRateColumn);
            objAutoLoanSecurity.IssueDate = GetDate(IssueDateColumn);
            objAutoLoanSecurity.InterestRate = GetDouble(InterestRateColumn);
            objAutoLoanSecurity.FacilityType = GetInt(FacilityTypeColumn);
            objAutoLoanSecurity.Status = GetInt(StatusColumn);
            objAutoLoanSecurity.FacilityId = GetInt(FacilityIdColumn);
            return objAutoLoanSecurity;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "SECURITYID":
                        {
                            SecurityIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "NATUREOFSECURITY":
                        {
                            NatureOfSecurityColumn = i;
                            break;
                        }
                    case "ISSUINGOFFICE":
                        {
                            IssuingOfficeColumn = i;
                            break;
                        }
                    case "FACEVALUE":
                        {
                            FaceValueColumn = i;
                            break;
                        }
                    case "XTVRATE":
                        {
                            XTVRateColumn = i;
                            break;
                        }
                    case "ISSUEDATE":
                        {
                            IssueDateColumn = i;
                            break;
                        }
                    case "INTERESTRATE":
                        {
                            InterestRateColumn = i;
                            break;
                        }
                    case "FACILITYTYPE":
                        {
                            FacilityTypeColumn = i;
                            break;
                        }
                    case "STATUS":
                        {
                            StatusColumn = i;
                            break;
                        }
                    case "FACILITYID":
                        {
                            FacilityIdColumn = i;
                            break;
                        }
                }
            }
        }

    }
}
