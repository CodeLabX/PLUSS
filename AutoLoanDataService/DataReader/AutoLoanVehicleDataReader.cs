﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoLoanVehicleDataReader : EntityDataReader<AutoLoanVehicle>
    {

        public int AutoLoanVehicleIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int VendorIdColumn = -1;
        public int Auto_ManufactureIdColumn = -1;
        public int ModelColumn = -1;
        public int TrimLevelColumn = -1;
        public int EngineCCColumn = -1;
        public int VehicleTypeColumn = -1;
        public int VehicleStatusColumn = -1;
        public int ManufacturingYearColumn = -1;
        public int QuotedPriceColumn = -1;
        public int VerifiedPriceColumn = -1;
        public int PriceConsideredBasedOnColumn = -1;
        public int SeatingCapacityColumn = -1;
        public int CarVerificationColumn = -1;
        public int StatusCodeColumn = -1;
        public int ManufacturerCodeColumn = -1;
        public int ModelCodeColumn = -1;
        public int VehicleTypeCodeColumn = -1;
        public int VendorCodeColumn = -1;
        public int VendoreRelationshipStatusColumn = -1;
        public int ValuePackAllowedColumn = -1;
        public int TenorColumn = -1;
        public int LTVColumn = -1;
        public int VehicleIdColumn = -1;



        public AutoLoanVehicleDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoLoanVehicle Read()
        {
            var objAutoLoanVehicle = new AutoLoanVehicle();
            objAutoLoanVehicle.AutoLoanVehicleId = GetInt(AutoLoanVehicleIdColumn);
            objAutoLoanVehicle.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoLoanVehicle.VendorId = GetInt(VendorIdColumn);
            objAutoLoanVehicle.Auto_ManufactureId = GetInt(Auto_ManufactureIdColumn);
            objAutoLoanVehicle.Model = GetString(ModelColumn);
            objAutoLoanVehicle.TrimLevel = GetString(TrimLevelColumn);
            objAutoLoanVehicle.EngineCC = GetString(EngineCCColumn);
            objAutoLoanVehicle.VehicleType = GetString(VehicleTypeColumn);
            objAutoLoanVehicle.VehicleStatus = GetInt(VehicleStatusColumn);
            objAutoLoanVehicle.ManufacturingYear = GetInt(ManufacturingYearColumn);
            if (objAutoLoanVehicle.ManufacturingYear == int.MinValue) {
                objAutoLoanVehicle.ManufacturingYear = 0;
            }
            objAutoLoanVehicle.QuotedPrice = GetDouble(QuotedPriceColumn);
            if (objAutoLoanVehicle.QuotedPrice == double.MinValue) {
                objAutoLoanVehicle.QuotedPrice = 0;
            }
            objAutoLoanVehicle.VerifiedPrice = GetDouble(VerifiedPriceColumn);
            if (objAutoLoanVehicle.VerifiedPrice == double.MinValue) {
                objAutoLoanVehicle.VerifiedPrice = 0;
            }
            objAutoLoanVehicle.PriceConsideredBasedOn = GetString(PriceConsideredBasedOnColumn);
            objAutoLoanVehicle.SeatingCapacity = GetString(SeatingCapacityColumn);
            objAutoLoanVehicle.CarVerification = GetString(CarVerificationColumn);
            objAutoLoanVehicle.StatusCode = GetString(StatusCodeColumn);
            objAutoLoanVehicle.ManufacturerCode = GetString(ManufacturerCodeColumn);
            objAutoLoanVehicle.ModelCode = GetString(ModelCodeColumn);
            objAutoLoanVehicle.VehicleTypeCode = GetString(VehicleTypeCodeColumn);
            objAutoLoanVehicle.VendorCode = GetString(VendorCodeColumn);
            objAutoLoanVehicle.VendoreRelationshipStatus = GetInt(VendoreRelationshipStatusColumn);
            objAutoLoanVehicle.ValuePackAllowed = GetBool(ValuePackAllowedColumn);
            objAutoLoanVehicle.Tenor = GetInt(TenorColumn);
            if (objAutoLoanVehicle.Tenor == int.MinValue) {
                objAutoLoanVehicle.Tenor = 0;
            }
            objAutoLoanVehicle.LTV = GetInt(LTVColumn);
            if (objAutoLoanVehicle.LTV == int.MinValue) {
                objAutoLoanVehicle.LTV = 0;
            }
            objAutoLoanVehicle.VehicleId = GetInt(VehicleIdColumn);

            return objAutoLoanVehicle;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTOLOANVEHICLEID":
                        {
                            AutoLoanVehicleIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "VENDORID":
                        {
                            VendorIdColumn = i;
                            break;
                        }
                    case "AUTO_MANUFACTUREID":
                        {
                            Auto_ManufactureIdColumn = i;
                            break;
                        }
                    case "MODEL":
                        {
                            ModelColumn = i;
                            break;
                        }
                    case "TRIMLEVEL":
                        {
                            TrimLevelColumn = i;
                            break;
                        }
                    case "ENGINECC":
                        {
                            EngineCCColumn = i;
                            break;
                        }
                    case "VEHICLETYPE":
                        {
                            VehicleTypeColumn = i;
                            break;
                        }
                    case "VEHICLESTATUS":
                        {
                            VehicleStatusColumn = i;
                            break;
                        }
                    case "MANUFACTURINGYEAR":
                        {
                            ManufacturingYearColumn = i;
                            break;
                        }
                    case "QUOTEDPRICE":
                        {
                            QuotedPriceColumn = i;
                            break;
                        }
                    case "VERIFIEDPRICE":
                        {
                            VerifiedPriceColumn = i;
                            break;
                        }
                    case "PRICECONSIDEREDBASEDON":
                        {
                            PriceConsideredBasedOnColumn = i;
                            break;
                        }
                    case "SEATINGCAPACITY":
                        {
                            SeatingCapacityColumn = i;
                            break;
                        }
                    case "CARVERIFICATION":
                        {
                            CarVerificationColumn = i;
                            break;
                        }
                    case "STATUSCODE":
                        {
                            StatusCodeColumn = i;
                            break;
                        }
                    case "MANUFACTURERCODE":
                        {
                            ManufacturerCodeColumn = i;
                            break;
                        }
                    case "MODELCODE":
                        {
                            ModelCodeColumn = i;
                            break;
                        }
                    case "VEHICLETYPECODE":
                        {
                            VehicleTypeCodeColumn = i;
                            break;
                        }
                    case "VENDORCODE":
                        {
                            VendorCodeColumn = i;
                            break;
                        }
                    case "VENDORERELATIONSHIPSTATUS":
                        {
                            VendoreRelationshipStatusColumn = i;
                            break;
                        }
                    case "VALUEPACKALLOWED":
                        {
                            ValuePackAllowedColumn = i;
                            break;
                        }
                    case "TENOR":
                        {
                            TenorColumn = i;
                            break;
                        }
                    case "LTV":
                        {
                            LTVColumn = i;
                            break;
                        }
                    case "VEHICLEID":
                        {
                            VehicleIdColumn = i;
                            break;
                        }
                }
            }
        }




    }
}
