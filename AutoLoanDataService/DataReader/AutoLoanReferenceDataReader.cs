﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoLoanReferenceDataReader : EntityDataReader<AutoLoanReference>
    {

        public int ReferenceIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int ReferenceName1Column = -1;
        public int ReferenceName2Column = -1;
        public int Relationship1Column = -1;
        public int Relationship2Column = -1;
        public int Occupation1Column = -1;
        public int Occupation2Column = -1;
        public int OrganizationName1Column = -1;
        public int OrganizationName2Column = -1;
        public int Designation1Column = -1;
        public int Designation2Column = -1;
        public int WorkAddress1Column = -1;
        public int WorkAddress2Column = -1;
        public int ResidenceAddress1Column = -1;
        public int ResidenceAddress2Column = -1;
        public int PhoneNumber1Column = -1;
        public int PhoneNumber2Column = -1;
        public int MobileNumber1Column = -1;
        public int MobileNumber2Column = -1;


        public AutoLoanReferenceDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoLoanReference Read()
        {
            var objAutoLoanReference = new AutoLoanReference();
            objAutoLoanReference.ReferenceId = GetInt(ReferenceIdColumn);
            objAutoLoanReference.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoLoanReference.ReferenceName1 = GetString(ReferenceName1Column);
            objAutoLoanReference.ReferenceName2 = GetString(ReferenceName2Column);
            objAutoLoanReference.Relationship1 = GetString(Relationship1Column);
            objAutoLoanReference.Relationship2 = GetString(Relationship2Column);
            objAutoLoanReference.Occupation1 = GetString(Occupation1Column);
            objAutoLoanReference.Occupation2 = GetString(Occupation2Column);
            objAutoLoanReference.OrganizationName1 = GetString(OrganizationName1Column);
            objAutoLoanReference.OrganizationName2 = GetString(OrganizationName2Column);
            objAutoLoanReference.Designation1 = GetString(Designation1Column);
            objAutoLoanReference.Designation2 = GetString(Designation2Column);
            objAutoLoanReference.WorkAddress1 = GetString(WorkAddress1Column);
            objAutoLoanReference.WorkAddress2 = GetString(WorkAddress2Column);
            objAutoLoanReference.ResidenceAddress1 = GetString(ResidenceAddress1Column);
            objAutoLoanReference.ResidenceAddress2 = GetString(ResidenceAddress2Column);
            objAutoLoanReference.PhoneNumber1 = GetString(PhoneNumber1Column);
            objAutoLoanReference.PhoneNumber2 = GetString(PhoneNumber2Column);
            objAutoLoanReference.MobileNumber1 = GetString(MobileNumber1Column);
            objAutoLoanReference.MobileNumber2 = GetString(MobileNumber2Column);

            return objAutoLoanReference;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "REFERENCEID":
                        {
                            ReferenceIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "REFERENCENAME1":
                        {
                            ReferenceName1Column = i;
                            break;
                        }
                    case "REFERENCENAME2":
                        {
                            ReferenceName2Column = i;
                            break;
                        }
                    case "RELATIONSHIP1":
                        {
                            Relationship1Column = i;
                            break;
                        }
                    case "RELATIONSHIP2":
                        {
                            Relationship2Column = i;
                            break;
                        }
                    case "OCCUPATION1":
                        {
                            Occupation1Column = i;
                            break;
                        }
                    case "OCCUPATION2":
                        {
                            Occupation2Column = i;
                            break;
                        }
                    case "ORGANIZATIONNAME1":
                        {
                            OrganizationName1Column = i;
                            break;
                        }
                    case "ORGANIZATIONNAME2":
                        {
                            OrganizationName2Column = i;
                            break;
                        }
                    case "DESIGNATION1":
                        {
                            Designation1Column = i;
                            break;
                        }
                    case "DESIGNATION2":
                        {
                            Designation2Column = i;
                            break;
                        }
                    case "WORKADDRESS1":
                        {
                            WorkAddress1Column = i;
                            break;
                        }
                    case "WORKADDRESS2":
                        {
                            WorkAddress2Column = i;
                            break;
                        }
                    case "RESIDENCEADDRESS1":
                        {
                            ResidenceAddress1Column = i;
                            break;
                        }
                    case "RESIDENCEADDRESS2":
                        {
                            ResidenceAddress2Column = i;
                            break;
                        }
                    case "PHONENUMBER1":
                        {
                            PhoneNumber1Column = i;
                            break;
                        }
                    case "PHONENUMBER2":
                        {
                            PhoneNumber2Column = i;
                            break;
                        }
                    case "MOBILENUMBER1":
                        {
                            MobileNumber1Column = i;
                            break;
                        }
                    case "MOBILENUMBER2":
                        {
                            MobileNumber2Column = i;
                            break;
                        }
                }
            }
        }





    }
}
