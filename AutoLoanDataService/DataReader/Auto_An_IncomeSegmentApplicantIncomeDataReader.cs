﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_IncomeSegmentApplicantIncomeDataReader : EntityDataReader<Auto_An_IncomeSegmentApplicantIncome>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int CalculationSourceColumn = -1;
        public int IncomeColumn = -1;
        public int IncomeAssesmentMethodColumn = -1;
        public int IncomeAssesmentCodeColumn = -1;
        public int EmployerCategoryColumn = -1;
        public int EmployerCategoryCodeColumn = -1;
        public int ConsiderColumn = -1;
        public int ApplicantTypeColumn = -1;



        public Auto_An_IncomeSegmentApplicantIncomeDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_IncomeSegmentApplicantIncome Read()
        {
            var objAuto_An_IncomeSegmentApplicantIncome = new Auto_An_IncomeSegmentApplicantIncome();

            objAuto_An_IncomeSegmentApplicantIncome.ID = GetInt(IDColumn);
            objAuto_An_IncomeSegmentApplicantIncome.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_IncomeSegmentApplicantIncome.CalculationSource = GetInt(CalculationSourceColumn);
            objAuto_An_IncomeSegmentApplicantIncome.Income = GetDouble(IncomeColumn);
            objAuto_An_IncomeSegmentApplicantIncome.IncomeAssesmentMethod = GetInt(IncomeAssesmentMethodColumn);
            objAuto_An_IncomeSegmentApplicantIncome.IncomeAssesmentCode = GetString(IncomeAssesmentCodeColumn);
            objAuto_An_IncomeSegmentApplicantIncome.EmployerCategory = GetInt(EmployerCategoryColumn);
            objAuto_An_IncomeSegmentApplicantIncome.EmployerCategoryCode = GetString(EmployerCategoryCodeColumn);
            objAuto_An_IncomeSegmentApplicantIncome.Consider = GetInt(ConsiderColumn);
            objAuto_An_IncomeSegmentApplicantIncome.ApplicantType = GetInt(ApplicantTypeColumn);

            return objAuto_An_IncomeSegmentApplicantIncome;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID":
                        {
                            IDColumn = i;
                            break;
                        }
                    case "ANALYSTMASTERID":
                        {
                            AnalystMasterIdColumn = i;
                            break;
                        }
                    case "CALCULATIONSOURCE":
                        {
                            CalculationSourceColumn = i;
                            break;
                        }
                    case "INCOME":
                        {
                            IncomeColumn = i;
                            break;
                        }
                    case "INCOMEASSESMENTMETHOD":
                        {
                            IncomeAssesmentMethodColumn = i;
                            break;
                        }
                    case "INCOMEASSESMENTCODE":
                        {
                            IncomeAssesmentCodeColumn = i;
                            break;
                        }
                    case "EMPLOYERCATEGORY":
                        {
                            EmployerCategoryColumn = i;
                            break;
                        }
                    case "EMPLOYERCATEGORYCODE":
                        {
                            EmployerCategoryCodeColumn = i;
                            break;
                        }
                    case "CONSIDER":
                        {
                            ConsiderColumn = i;
                            break;
                        }
                    case "APPLICANTTYPE":
                        {
                            ApplicantTypeColumn = i;
                            break;
                        }

                }
            }
        }




    }
}
