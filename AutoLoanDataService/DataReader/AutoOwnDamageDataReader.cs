﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoOwnDamageDataReader : EntityDataReader<AutoOwnDamage>
    {
        public int OwnDamageIdColumn = -1;
        public int CCFromColumn = -1;
        public int CCToColumn = -1;
        public int OwnDamageChargeColumn = -1;
        public int ActLiabilityColumn = -1;


        public AutoOwnDamageDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoOwnDamage Read()
        {
            var objAutoOwnDamage = new AutoOwnDamage();
            objAutoOwnDamage.OwnDamageId = GetInt(OwnDamageIdColumn);
            objAutoOwnDamage.CCFrom = GetString(CCFromColumn);
            objAutoOwnDamage.CCTo = GetString(CCToColumn);
            objAutoOwnDamage.OwnDamageCharge = Convert.ToDouble(GetFloat(OwnDamageChargeColumn));
            objAutoOwnDamage.ActLiability = Convert.ToDouble(GetFloat(ActLiabilityColumn));
            return objAutoOwnDamage;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "OWNDAMAGEID":
                        {
                            OwnDamageIdColumn = i;
                            break;
                        }
                    case "CCFROM":
                        {
                            CCFromColumn = i;
                            break;
                        }
                    case "CCTO":
                        {
                            CCToColumn = i;
                            break;
                        }
                    case "OWNDAMAGECHARGE":
                        {
                            OwnDamageChargeColumn = i;
                            break;
                        }
                    case "ACTLIABILITY":
                        {
                            ActLiabilityColumn = i;
                            break;
                        }
                }
            }
        }
    }
}
