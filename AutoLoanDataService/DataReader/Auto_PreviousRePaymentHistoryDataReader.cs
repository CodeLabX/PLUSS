﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
  internal class Auto_PreviousRePaymentHistoryDataReader : EntityDataReader<Auto_PreviousRePaymentHistory>
   {
       public int IdColumn = -1;
       public int AutoLoanMasterIdColumn = -1;
       public int BankStatementIdIdColumn = -1;
       public int BankIdColumn = -1;
       public int OffScb_TotalColumn = -1;
       public int OnScb_TotalColumn = -1;
       public int TotalEMIClosingConsideredColumn = -1;
       public int RemarksColumn = -1;
      public int IncomeSumIdColumn = -1;


       public Auto_PreviousRePaymentHistoryDataReader(IDataReader reader)
           : base(reader)
       {
       }

       public override Auto_PreviousRePaymentHistory Read()
       {
           var objAuto_PreviousRePaymentHistory = new Auto_PreviousRePaymentHistory();
           objAuto_PreviousRePaymentHistory.Id = GetInt(IdColumn);
           objAuto_PreviousRePaymentHistory.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
           objAuto_PreviousRePaymentHistory.BankStatementId = GetInt(BankStatementIdIdColumn);
           objAuto_PreviousRePaymentHistory.BankId = GetInt(BankIdColumn);
           objAuto_PreviousRePaymentHistory.OffScb_Total = GetDouble(OffScb_TotalColumn);
           objAuto_PreviousRePaymentHistory.OnScb_Total = GetDouble(OnScb_TotalColumn);
           objAuto_PreviousRePaymentHistory.TotalEMIClosingConsidered = GetDouble(TotalEMIClosingConsideredColumn);
           objAuto_PreviousRePaymentHistory.Remarks = GetString(RemarksColumn);
           objAuto_PreviousRePaymentHistory.IncomeSumId = GetInt(IncomeSumIdColumn);

           return objAuto_PreviousRePaymentHistory;
       }

       protected override void ResolveOrdinal()
       {
           for (int i = 0; i < reader.FieldCount; i++)
           {
               switch (reader.GetName(i).ToUpper())
               {
                   case "ID":
                       {
                           IdColumn = i;
                           break;
                       }
                   case "AUTOLOANMASTERID":
                       {
                           AutoLoanMasterIdColumn = i;
                           break;
                       }
                   case "BANKSTATEMENTID":
                       {
                           BankStatementIdIdColumn = i;
                           break;
                       }
                   case "BANKID":
                       {
                           BankIdColumn = i;
                           break;
                       }
                   case "OFFSCB_TOTAL":
                       {
                           OffScb_TotalColumn = i;
                           break;
                       }
                   case "ONSCB_TOTAL":
                       {
                           OnScb_TotalColumn = i;
                           break;
                       }
                   case "TOTALEMICLOSINGCONSIDERED":
                       {
                           TotalEMIClosingConsideredColumn = i;
                           break;
                       }
                   case "REMARKS":
                       {
                           RemarksColumn = i;
                           break;
                       }
                   case "INCOMESUMID":
                       {
                           IncomeSumIdColumn = i;
                           break;
                       }
               }
           }
       }
   }
    
}
