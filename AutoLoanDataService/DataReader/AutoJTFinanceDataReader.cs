﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;


namespace AutoLoanDataService.DataReader
{
    internal class AutoJTFinanceDataReader : EntityDataReader<AutoJTFinance>
    {

        public int JtFinanceIdColumn = -1;
        public int AutoLoanMasterIdColumn = -1;
        public int DPISourceColumn = -1;
        public int DPIAmountColumn = -1;
        public int DOISourceColumn = -1;
        public int DOIAmountColumn = -1;
        public int EDRentAndUtilitiesColumn = -1;
        public int EDFoodAndClothingColumn = -1;
        public int EDEducationColumn = -1;
        public int EDLoanRePaymentColumn = -1;
        public int EDOtherColumn = -1;
        public int RepaymentToColumn = -1;
        public int PaymentForColumn = -1;


        public AutoJTFinanceDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoJTFinance Read()
        {
            var objAutoJTFinance = new AutoJTFinance();
            objAutoJTFinance.JtFinanceId = GetInt(JtFinanceIdColumn);
            objAutoJTFinance.AutoLoanMasterId = GetInt(AutoLoanMasterIdColumn);
            objAutoJTFinance.DPISource = GetString(DPISourceColumn);
            objAutoJTFinance.DPIAmount = GetDouble(DPIAmountColumn);
            objAutoJTFinance.DOISource = GetString(DOISourceColumn);
            objAutoJTFinance.DOIAmount = GetDouble(DOIAmountColumn);
            objAutoJTFinance.EDRentAndUtilities = GetDouble(EDRentAndUtilitiesColumn);
            objAutoJTFinance.EDFoodAndClothing = GetDouble(EDFoodAndClothingColumn);
            objAutoJTFinance.EDEducation = GetDouble(EDEducationColumn);
            objAutoJTFinance.EDLoanRePayment = GetDouble(EDLoanRePaymentColumn);
            objAutoJTFinance.EDOther = GetDouble(EDOtherColumn);
            objAutoJTFinance.RepaymentTo = GetString(RepaymentToColumn);
            objAutoJTFinance.PaymentFor = GetString(PaymentForColumn);

            return objAutoJTFinance;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "JTFINANCEID":
                        {
                            JtFinanceIdColumn = i;
                            break;
                        }
                    case "AUTOLOANMASTERID":
                        {
                            AutoLoanMasterIdColumn = i;
                            break;
                        }
                    case "DPISOURCE":
                        {
                            DPISourceColumn = i;
                            break;
                        }
                    case "DPIAMOUNT":
                        {
                            DPIAmountColumn = i;
                            break;
                        }
                    case "DOISOURCE":
                        {
                            DOISourceColumn = i;
                            break;
                        }
                    case "DOIAMOUNT":
                        {
                            DOIAmountColumn = i;
                            break;
                        }
                    case "EDRENTANDUTILITIES":
                        {
                            EDRentAndUtilitiesColumn = i;
                            break;
                        }
                    case "EDFOODANDCLOTHING":
                        {
                            EDFoodAndClothingColumn = i;
                            break;
                        }
                    case "EDEDUCATION":
                        {
                            EDEducationColumn = i;
                            break;
                        }
                    case "EDLOANREPAYMENT":
                        {
                            EDLoanRePaymentColumn = i;
                            break;
                        }
                    case "EDOTHER":
                        {
                            EDOtherColumn = i;
                            break;
                        }
                    case "REPAYMENTTO":
                        {
                            RepaymentToColumn = i;
                            break;
                        }
                    case "PAYMENTFOR":
                        {
                            PaymentForColumn = i;
                            break;
                        }
                }
            }
        }


    }
}
