﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    [Serializable]
    internal class AutoWorkFlowStatusDataReader : EntityDataReader<AutoWorkFlowStatus>
    {

        public int StatusIDColumn = -1;
        public int StatusNameColumn = -1;

        public AutoWorkFlowStatusDataReader(IDataReader reader)
            : base(reader)
        { }


        public override AutoWorkFlowStatus Read()
        {
            var objAutoWorkFlowStatus = new AutoWorkFlowStatus();
            objAutoWorkFlowStatus.StatusID = GetInt(StatusIDColumn);
            objAutoWorkFlowStatus.StatusName = GetString(StatusNameColumn);

            return objAutoWorkFlowStatus;
        }


        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "STATUSID":
                        {
                            StatusIDColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                }
            }
        }



    }
}
