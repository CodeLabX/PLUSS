﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class AutoPayrollRateDataReader : EntityDataReader<AutoPayrollRate>
    {
        public int AutoPayRateIdColumn = -1;
        public int AutoCompanyIdColumn = -1;
        public int CategoryNameColumn = -1;
        public int PayrollRateColumn = -1;
        public int ProcessingFeeColumn = -1;
        public int LastUpdateDateColumn = -1;
        public int RemarksColumn = -1;

        //Selected Property
        public int TotalCountColumn = -1;
        public int CompanyNameColumn = -1;
        public int CompanyCodeColumn = -1;


        public AutoPayrollRateDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoPayrollRate Read()
        {
            var objAutoPayrollRate = new AutoPayrollRate();
            objAutoPayrollRate.AutoPayRateId = GetInt(AutoPayRateIdColumn);
            objAutoPayrollRate.AutoCompanyId = GetInt(AutoCompanyIdColumn);
            objAutoPayrollRate.CategoryName = GetString(CategoryNameColumn);
            objAutoPayrollRate.PayrollRate = GetString(PayrollRateColumn);
            objAutoPayrollRate.ProcessingFee = GetString(ProcessingFeeColumn);
            objAutoPayrollRate.LastUpdateDate = GetDate(LastUpdateDateColumn);
            objAutoPayrollRate.Remarks = GetString(RemarksColumn);
            
            objAutoPayrollRate.TotalCount = Convert.ToInt32(reader.GetValue(TotalCountColumn));
            objAutoPayrollRate.CompanyName = GetString(CompanyNameColumn);
            objAutoPayrollRate.CompanyCode = GetString(CompanyCodeColumn);
            return objAutoPayrollRate;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "AUTOPAYRATEID":
                        {
                            AutoPayRateIdColumn = i;
                            break;
                        }
                    case "AUTOCOMPANYID":
                        {
                            AutoCompanyIdColumn = i;
                            break;
                        }
                    case "CATEGORYNAME":
                        {
                            CategoryNameColumn = i;
                            break;
                        }
                    case "PAYROLLRATE":
                        {
                            PayrollRateColumn = i;
                            break;
                        }
                    case "PROCESSINGFEE":
                        {
                            ProcessingFeeColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                    case "COMPANYNAME":
                        {
                            CompanyNameColumn = i;
                            break;
                        }
                    case "REMARK":
                        {
                            RemarksColumn = i;
                            break;
                        }
                    case "COMPANYCODE":
                        {
                            CompanyCodeColumn = i;
                            break;
                        }
                }
            }
        }
    }
}
