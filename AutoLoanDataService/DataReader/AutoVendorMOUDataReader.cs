﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoVendorMOUDataReader : EntityDataReader<AutoVendorMOU>
    {
        public int VendorMouIdColumn = -1;
        public int AutoVendorIdColumn = -1;
        public int IrWithArtaColumn = -1;
        public int IrWithoutArtaColumn = -1;
        public int ProcessingFeeColumn = -1;
        public int ValidFromColumn = -1;
        public int LastUpdateDateColumn = -1;
        public int VendorNameColumn = -1;
        public int DealerStatusColumn = -1;
        public int AddressColumn = -1;
        public int phoneColumn = -1;
        public int WebsiteColumn = -1;
        public int TotalCountColumn = -1;
        public int DealerTypeColumn = -1;
        public int isMouColumn = -1;
        public int VendorCodeColumn = -1;



        public AutoVendorMOUDataReader(IDataReader reader)
            : base(reader)
        { }
        public override AutoVendorMOU Read()
        {
            var objAutoVendorMOU = new AutoVendorMOU();
            objAutoVendorMOU.VendorMouId = GetInt(VendorMouIdColumn);
            objAutoVendorMOU.AutoVendorId = GetInt(AutoVendorIdColumn);
            objAutoVendorMOU.IrWithArta = GetDouble(IrWithArtaColumn);
            if (objAutoVendorMOU.IrWithArta == double.MinValue)
            {
                objAutoVendorMOU.IrWithArta = 0;
            }
            objAutoVendorMOU.IrWithoutArta = GetDouble(IrWithoutArtaColumn);
            if (objAutoVendorMOU.IrWithoutArta == double.MinValue)
            {
                objAutoVendorMOU.IrWithoutArta = 0;
            }
            objAutoVendorMOU.ProcessingFee = GetInt(ProcessingFeeColumn);
            if (objAutoVendorMOU.ProcessingFee == int.MinValue)
            {
                objAutoVendorMOU.ProcessingFee = 0;
            }
            //objAutoVendorMOU.ValidFrom = GetDate(ValidFromColumn);
            //objAutoVendorMOU.LastUpdateDate = GetDate(LastUpdateDateColumn);
            objAutoVendorMOU.VendorName = GetString(VendorNameColumn);
            objAutoVendorMOU.VendorCode = GetString(VendorCodeColumn);
            objAutoVendorMOU.DealerStatus = GetString(DealerStatusColumn);
            objAutoVendorMOU.Address = GetString(AddressColumn);
            objAutoVendorMOU.phone = GetString(phoneColumn);
            objAutoVendorMOU.Website = GetString(WebsiteColumn);
            objAutoVendorMOU.DealerType = GetInt(DealerTypeColumn);

            objAutoVendorMOU.ValidFrom = GetDate(ValidFromColumn).Date;
            objAutoVendorMOU.LastUpdateDate = GetDate(LastUpdateDateColumn).Date;
            objAutoVendorMOU.IsMou = GetInt(isMouColumn);

            objAutoVendorMOU.TotalCount = Convert.ToInt32(reader.GetValue(TotalCountColumn));
            return objAutoVendorMOU;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "VENDORMOUID":
                        {
                            VendorMouIdColumn = i;
                            break;
                        }
                    case "AUTOVENDORID":
                        {
                            AutoVendorIdColumn = i;
                            break;
                        }
                    case "IRWITHARTA":
                        {
                            IrWithArtaColumn = i;
                            break;
                        }
                    case "IRWITHOUTARTA":
                        {
                            IrWithoutArtaColumn = i;
                            break;
                        }
                    case "PROCESSINGFEE":
                        {
                            ProcessingFeeColumn = i;
                            break;
                        }
                    case "VALIDFROM":
                        {
                            ValidFromColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "VENDORNAME":
                        {
                            VendorNameColumn = i;
                            break;
                        }
                    case "VENDORCODE":
                        {
                            VendorCodeColumn = i;
                            break;
                        }
                    case "DEALERSTATUS":
                        {
                            DealerStatusColumn = i;
                            break;
                        }
                    case "ADDRESS":
                        {
                            AddressColumn = i;
                            break;
                        }
                    case "PHONE":
                        {
                            phoneColumn = i;
                            break;
                        }
                    case "WEBSITE":
                        {
                            WebsiteColumn = i;
                            break;
                        } 
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                    case "DEALERTYPE":
                        {
                            DealerTypeColumn = i;
                            break;
                        }
                    case "ISMOU":
                        {
                            isMouColumn = i;
                            break;
                        } 
                }
            }
        }

    }
}
