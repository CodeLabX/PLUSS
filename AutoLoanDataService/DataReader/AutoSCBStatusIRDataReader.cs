﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;
using System.Data;

namespace AutoLoanDataService.DataReader
{
    internal class AutoSCBStatusIRDataReader : EntityDataReader<AutoSCBStatusIR>
    {
        public int SCBStatusIRIdColumn = -1;
        public int SCBStatusColumn = -1;
        public int IrWithArtaColumn = -1;
        public int IrWithoutArtaColumn = -1;
        public int LastUpdateDateColumn = -1;
        public int StatusNameColumn = -1;
        public int TotalCountColumn = -1;

        public AutoSCBStatusIRDataReader(IDataReader reader)
            : base(reader)
        { }

        public override AutoSCBStatusIR Read()
        {
            var objAutoSCBStatusIR = new AutoSCBStatusIR();
            objAutoSCBStatusIR.SCBStatusIRId = GetInt(SCBStatusIRIdColumn);
            objAutoSCBStatusIR.SCBStatus = GetInt(SCBStatusColumn);
            objAutoSCBStatusIR.IrWithArta = GetDouble(IrWithArtaColumn);
            if (objAutoSCBStatusIR.IrWithArta == double.MinValue) {
                objAutoSCBStatusIR.IrWithArta = 0;
            }
            objAutoSCBStatusIR.IrWithoutArta = GetDouble(IrWithoutArtaColumn);
            if (objAutoSCBStatusIR.IrWithoutArta == double.MinValue) {
                objAutoSCBStatusIR.IrWithoutArta = 0;
            }
            objAutoSCBStatusIR.LastUpdateDate = GetDate(LastUpdateDateColumn);
            objAutoSCBStatusIR.StatusName = GetString(StatusNameColumn);

            objAutoSCBStatusIR.TotalCount = Convert.ToInt32(reader.GetValue(TotalCountColumn));
            return objAutoSCBStatusIR;
        }
        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "SCBSTATUSIRID":
                        {
                            SCBStatusIRIdColumn = i;
                            break;
                        }
                    case "SCBSTATUS":
                        {
                            SCBStatusColumn = i;
                            break;
                        }
                    case "IRWITHARTA":
                        {
                            IrWithArtaColumn = i;
                            break;
                        }
                    case "IRWITHOUTARTA":
                        {
                            IrWithoutArtaColumn = i;
                            break;
                        }
                    case "LASTUPDATEDATE":
                        {
                            LastUpdateDateColumn = i;
                            break;
                        }
                    case "STATUSNAME":
                        {
                            StatusNameColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        } 
                }
            }
        }





    }
}
