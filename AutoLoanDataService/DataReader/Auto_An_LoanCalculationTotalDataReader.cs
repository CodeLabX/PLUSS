﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class Auto_An_LoanCalculationTotalDataReader : EntityDataReader<Auto_An_LoanCalculationTotal>
    {
        public int IDColumn = -1;
        public int AnalystMasterIdColumn = -1;
        public int TotalLoanAmountColumn = -1;
        public int LTVExcludingInsuranceColumn = -1;
        public int FBRExcludingInsuranceColumn = -1;
        public int LTVIncludingInsuranceColumn = -1;
        public int FBRIncludingInsuranceColumn = -1;
        public int EMRColumn = -1;
        public int ExtraLTVColumn = -1;
        public int ExtraDBRColumn = -1;


        public Auto_An_LoanCalculationTotalDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override Auto_An_LoanCalculationTotal Read()
        {
            var objAuto_An_LoanCalculationTotal = new Auto_An_LoanCalculationTotal();

            objAuto_An_LoanCalculationTotal.ID = GetInt(IDColumn);
            objAuto_An_LoanCalculationTotal.AnalystMasterId = GetInt(AnalystMasterIdColumn);
            objAuto_An_LoanCalculationTotal.TotalLoanAmount = GetDouble(TotalLoanAmountColumn);
            objAuto_An_LoanCalculationTotal.LTVExcludingInsurance = GetDouble(LTVExcludingInsuranceColumn);
            objAuto_An_LoanCalculationTotal.FBRExcludingInsurance = GetDouble(FBRExcludingInsuranceColumn);
            objAuto_An_LoanCalculationTotal.LTVIncludingInsurance = GetDouble(LTVIncludingInsuranceColumn);
            objAuto_An_LoanCalculationTotal.FBRIncludingInsurance = GetDouble(FBRIncludingInsuranceColumn);
            objAuto_An_LoanCalculationTotal.EMR = GetDouble(EMRColumn);
            objAuto_An_LoanCalculationTotal.ExtraLTV = GetDouble(ExtraLTVColumn);
            objAuto_An_LoanCalculationTotal.ExtraDBR = GetDouble(ExtraDBRColumn);

            return objAuto_An_LoanCalculationTotal;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "ID": { IDColumn = i; break; }
                    case "ANALYSTMASTERID": { AnalystMasterIdColumn = i; break; }
                    case "TOTALLOANAMOUNT": { TotalLoanAmountColumn = i; break; }
                    case "LTVEXCLUDINGINSURANCE": { LTVExcludingInsuranceColumn = i; break; }
                    case "FBREXCLUDINGINSURANCE": { FBRExcludingInsuranceColumn = i; break; }
                    case "LTVINCLUDINGINSURANCE": { LTVIncludingInsuranceColumn = i; break; }
                    case "FBRINCLUDINGINSURANCE": { FBRIncludingInsuranceColumn = i; break; }
                    case "EMR": { EMRColumn = i; break; }
                    case "EXTRALTV": { ExtraLTVColumn = i; break; }
                    case "EXTRADBR": { ExtraDBRColumn = i; break; }
                }
            }
        }


    }
}
