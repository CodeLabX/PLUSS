﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AutoLoanService.Entity;
using Azolution.Common.SqlDataService;

namespace AutoLoanDataService.DataReader
{
    internal class VendorNegativeListingDataReader :  EntityDataReader<AutoVendorNegativeListing>
    {
        public int NegativeVendorIdColumn = -1;
        public int AutoVendorIdColumn = -1;
        public int ListingDateColumn = -1;
        public int VendorNameColumn = -1;
        public int TotalCountColumn = -1; 

        public VendorNegativeListingDataReader(IDataReader reader)
            : base(reader)
        {
        }

        public override AutoVendorNegativeListing Read()
        {
            var objAutoVendorNegative = new AutoVendorNegativeListing();
            objAutoVendorNegative.NegativeVendorId = GetInt(NegativeVendorIdColumn);
            objAutoVendorNegative.AutoVendorId = GetInt(AutoVendorIdColumn);
            objAutoVendorNegative.ListingDate = GetDate(ListingDateColumn);
            objAutoVendorNegative.VendorName = GetString(VendorNameColumn);
            objAutoVendorNegative.Totalcount = GetInt(TotalCountColumn);



            return objAutoVendorNegative;
        }

        protected override void ResolveOrdinal()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i).ToUpper())
                {
                    case "NEGATIVEVENDORID":
                        {
                            NegativeVendorIdColumn = i;
                            break;
                        }

                    case "AUTOVENDORID":
                        {
                            AutoVendorIdColumn = i;
                            break;
                        }
                    case "LISTINGDATE":
                        {
                            ListingDateColumn = i;
                            break;
                        }
                    case "VENDORNAME":
                        {
                            VendorNameColumn = i;
                            break;
                        }
                    case "TOTALCOUNT":
                        {
                            TotalCountColumn = i;
                            break;
                        }
                   

                }
            }
        }

    }

   
}


